#!/bin/bash

EXEC_DIR=${PWD}

cd $EXEC_DIR/docker/main_image && docker build -t apache_and_php .
cd $EXEC_DIR/docker/dev/web && docker build -t atlas_dev .
cd $EXEC_DIR/docker/dev && docker-compose up -d