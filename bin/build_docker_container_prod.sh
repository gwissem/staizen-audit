#!/bin/bash

EXEC_DIR=${PWD}

cd $EXEC_DIR/docker/main_image && docker build -t apache_and_php .
cd $EXEC_DIR/docker/prod/web && docker build -t atlas_prod .
cd $EXEC_DIR/docker/prod && docker-compose up -d