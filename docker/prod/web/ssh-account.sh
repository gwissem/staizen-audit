#!/bin/bash

#useradd "$SSH_LOGIN"
#mkhomedir_helper "$SSH_LOGIN"
echo "starter:$SSH_PASS" | chpasswd

mkdir /home/$SSH_LOGIN/.ssh && chown "$SSH_LOGIN:$SSH_LOGIN" /home/$SSH_LOGIN/.ssh

touch /home/starter/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDApWXmOnWTtAYerTilr2bF0FJrK7i4MqcmClKB/RTMzKvf/XWrvUYxgWQreNeMgUIBk81DBJaJiUhffHkzsfSvE9uWqJcEuROLmPWpzXZiJxIG09MaPnNZ+weq3L6Vbg3RuJ4ZpWqX1GI4g9NYT7h8fVvPc/2ZCZsUqimcjuNerF238P+M0y5EicEjbtlTB/F2vr/tuv+NgN5Eo4je17E39izpU9Q78oDC+l/JeMq+ZPHjIyYh1PqFMPcgvRXVOmht4wy641Dxk8xDpI7tbtRT/UswrxuK8+p8Yx3oPjV8YsKKHJ5iHijfnruvVJT8tlP/gln57z3f3G8lw/MfPkgT dylesiu@dylesiu-Lenovo--Y700" >> /home/$SSH_LOGIN/.ssh/authorized_keys