#!/bin/bash

#RUN mkdir -p /var/log/apache2
#RUN touch /var/log/apache2/error.log

/usr/sbin/sshd -D & service php7.0-fpm start & /usr/sbin/apachectl -DFOREGROUND -k start & service supervisor start & tail -f /var/log/apache2/error.log