#!/bin/bash

U=$1

if [ -z "$U" ]; then

    echo "Please set first argument as user. For example: starter"
    exit 1;

fi

cd /home/${U}/www/atlas_production/current
nodejs /home/${U}/node_modules/gulp/bin/gulp.js $2