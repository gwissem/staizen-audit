#!/bin/bash

U=$1

if [ -z "$U" ]; then

    echo "Please set first argument as user. For example: starter"
    exit 1;

fi

cd /home/${U}/www/atlas_production/current

FILE=/home/${U}/www/atlas_production/current/composer.phar

if [ ! -f "$FILE" ]; then

    EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
    then
        >&2 echo 'ERROR: Invalid installer signature'
        rm composer-setup.php
        exit 1
    fi

    php composer-setup.php --quiet
    RESULT=$?
    rm composer-setup.php

fi

php composer.phar install

exit $RESULT