#!/bin/bash

service php7.0-fpm start & /usr/sbin/apachectl -DFOREGROUND -k start & redis-server /etc/redis/redis.conf & service supervisor start & tail -f /var/log/apache2/error.log