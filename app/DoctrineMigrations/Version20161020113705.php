<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161020113705 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE tool ADD unique_id NVARCHAR(32)');
        $this->addSql('ALTER TABLE tool_group ADD unique_id NVARCHAR(32)');
        $this->addSql('ALTER TABLE user_group ADD unique_id NVARCHAR(32)');

        $this->addSql("UPDATE tool SET unique_id = REPLACE(LOWER(CAST(NEWID() AS varchar(36))),'-','')");
        $this->addSql("UPDATE tool_group SET unique_id = REPLACE(LOWER(CAST(NEWID() AS varchar(36))),'-','')");
        $this->addSql("UPDATE user_group SET unique_id = REPLACE(LOWER(CAST(NEWID() AS varchar(36))),'-','')");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


        $this->addSql('ALTER TABLE tool DROP COLUMN unique_id');
        $this->addSql('ALTER TABLE tool_group DROP COLUMN unique_id');
        $this->addSql('ALTER TABLE user_group DROP COLUMN unique_id');
    }
}