<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161214110911 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_flow ADD process_definition_id INT');
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD29044580AA177E FOREIGN KEY (process_definition_id) REFERENCES process_definition (id) ON DELETE CASCADE'
        );
        $this->addSql('CREATE INDEX IDX_BD29044580AA177E ON process_flow (process_definition_id)');
        $this->addSql('ALTER TABLE step ADD id_suffix NVARCHAR(20)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD29044580AA177E');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_BD29044580AA177E\')
                            ALTER TABLE process_flow DROP CONSTRAINT IDX_BD29044580AA177E
                        ELSE
                            DROP INDEX IDX_BD29044580AA177E ON process_flow'
        );
        $this->addSql('ALTER TABLE process_flow DROP COLUMN process_definition_id');
        $this->addSql('ALTER TABLE step DROP COLUMN id_suffix');
    }
}
