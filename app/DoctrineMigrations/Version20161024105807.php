<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161024105807 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE tool_action ADD unique_id NVARCHAR(32)');
        $this->addSql("UPDATE tool_action SET unique_id = REPLACE(LOWER(CAST(NEWID() AS varchar(36))),'-','')");

        $this->addSql('ALTER TABLE tool_action ALTER COLUMN unique_id NVARCHAR(32) NOT NULL');
        $this->addSql(
            'CREATE UNIQUE INDEX UNIQ_229D808EE3C68343 ON tool_action (unique_id) WHERE unique_id IS NOT NULL'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'UNIQ_229D808EE3C68343\')
                            ALTER TABLE tool_action DROP CONSTRAINT UNIQ_229D808EE3C68343
                        ELSE
                            DROP INDEX UNIQ_229D808EE3C68343 ON tool_action'
        );
        $this->addSql('ALTER TABLE tool_action DROP COLUMN unique_id');

    }
}
