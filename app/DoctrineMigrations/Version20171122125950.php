<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171122125950 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE step DROP CONSTRAINT DF_43B9FE3C_D827A4E7');
        $this->addSql('ALTER TABLE step ALTER COLUMN postpone_count INT');
        $this->addSql('ALTER TABLE process_instance_flow ADD variant BIT');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE process_instance_flow DROP COLUMN variant');
        $this->addSql('ALTER TABLE step ALTER COLUMN postpone_count INT');
        $this->addSql('ALTER TABLE step ADD CONSTRAINT DF_43B9FE3C_D827A4E7 DEFAULT 0 FOR postpone_count');

    }
}
