<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170831165637 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE log (id INT IDENTITY NOT NULL, created_by_id INT, updated_by_id INT, name NVARCHAR(255) NOT NULL, content VARCHAR(MAX) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_8F3F68C5B03A8386 ON log (created_by_id)');
        $this->addSql('CREATE INDEX IDX_8F3F68C5896DBBDE ON log (updated_by_id)');
        $this->addSql('ALTER TABLE log ADD CONSTRAINT FK_8F3F68C5B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE log ADD CONSTRAINT FK_8F3F68C5896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)');;
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE log');
    }
}
