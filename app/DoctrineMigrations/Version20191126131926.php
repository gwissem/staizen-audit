<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191126131926 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            IF NOT EXISTS(SELECT 1 FROM AtlasDB_v.sys.columns WHERE Name = 'document_id' AND Object_ID = Object_ID('AtlasDB_v.dbo.post'))
            BEGIN
                EXEC sp_rename 'post.file_id' , 'document_id', 'COLUMN'
            END"
        );

        $this->addSql("
            IF NOT EXISTS(SELECT 1 FROM AtlasDB_v.sys.columns WHERE Name = 'type' AND Object_ID = Object_ID('AtlasDB_v.dbo.post'))
            BEGIN
                ALTER TABLE post ADD [type] INT
            END"
        );

        $this->addSql('
            ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8D93CB796C'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("
            IF EXISTS(SELECT 1 FROM AtlasDB_v.sys.columns WHERE Name = 'document_id' AND Object_ID = Object_ID('AtlasDB_v.dbo.post'))
            BEGIN
                EXEC sp_rename 'post.document_id' , 'file_id', 'COLUMN'
            END"
        );

        $this->addSql("IF EXISTS(SELECT 1 FROM AtlasDB_v.sys.columns WHERE Name = 'type' AND Object_ID = Object_ID('AtlasDB_v.dbo.post'))
            BEGIN
                ALTER TABLE post DROP COLUMN [type]
            END"
        );

        $this->addSql('
            CONSTRAINT FK_5A8A6C8D93CB796C FOREIGN KEY (document_id) REFERENCES AtlasDB_v.dbo.files(id)'
        );

    }
}
