<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180810142917 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.service_definition_platform (id INT IDENTITY NOT NULL, service_definition_id INT NOT NULL, platform_id INT NOT NULL, position INT, description VARCHAR(MAX), PRIMARY KEY (id))');
//        $this->addSql('CREATE INDEX IDX_1548E08F1A11393 ON AtlasDB_def.dbo.service_definition_platform (service_definition_id)');
//        $this->addSql('CREATE INDEX IDX_1548E08FFFE6496F ON AtlasDB_def.dbo.service_definition_platform (platform_id)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_definition_platform ADD CONSTRAINT DF_1548E08F_462CE4F5 DEFAULT 0 FOR position');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_definition_platform ADD CONSTRAINT FK_1548E08F1A11393 FOREIGN KEY (service_definition_id) REFERENCES service_definition (id)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_definition_platform ADD CONSTRAINT FK_1548E08FFFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id)');

        // Uzupełnienie powiązania pomiędzy aktywnymi platformami i usługami
//        $this->addSql('DECLARE @serviceId INT
//            DECLARE @servicePosition INT
//            DECLARE @platformId INT
//            DECLARE @description NVARCHAR(MAX)
//
//            DECLARE @serviceDescriptions TABLE (
//                id INT,
//                platform_id INT,
//                service_definition_id INT,
//                description NVARCHAR(MAX)
//            )
//
//            INSERT INTO @serviceDescriptions
//            SELECT * FROM AtlasDB_v.dbo.service_platform_description
//
//            DECLARE kur cursor LOCAL for
//            SELECT sd.id as id, sd.[position] as [position] FROM AtlasDB_def.dbo.service_definition as sd
//                OPEN kur;
//                FETCH NEXT FROM kur INTO @serviceId, @servicePosition
//                WHILE @@FETCH_STATUS=0
//                BEGIN
//
//                        DECLARE kur2 cursor LOCAL for
//                        SELECT p.id as localizationId FROM AtlasDB_def.dbo.platform as p WHERE active = 1
//                            OPEN kur2;
//                            FETCH NEXT FROM kur2 INTO @platformId
//                            WHILE @@FETCH_STATUS=0
//                            BEGIN
//
//                                IF (SELECT count(id) FROM AtlasDB_def.dbo.service_definition_platform WHERE platform_id = @platformId AND service_definition_id = @serviceId) = 0
//                                BEGIN
//
//                                    SELECT @description = description FROM @serviceDescriptions WHERE platform_id = @platformId AND service_definition_id = @serviceId
//                                    INSERT INTO AtlasDB_def.dbo.service_definition_platform (service_definition_id, platform_id, [position], description) VALUES (@serviceId, @platformId, @servicePosition, @description)
//
//                                END
//
//                                FETCH NEXT FROM kur2 INTO @platformId
//                            END
//                        CLOSE kur2
//                        DEALLOCATE kur2
//
//                    FETCH NEXT FROM kur INTO @serviceId, @servicePosition
//                END
//            CLOSE kur
//            DEALLOCATE kur');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('DROP TABLE AtlasDB_def.dbo.service_definition_platform');

    }
}
