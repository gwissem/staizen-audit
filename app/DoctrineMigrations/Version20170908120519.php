<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170908120519 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

         $this->addSql('ALTER TABLE process_definition ADD cancel_step_id NVARCHAR(20)');
        $this->addSql('ALTER TABLE process_definition ADD CONSTRAINT FK_C67B0CD0157602CD FOREIGN KEY (cancel_step_id) REFERENCES step (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

       $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_C67B0CD0157602CD\')
                            ALTER TABLE process_definition DROP CONSTRAINT IDX_C67B0CD0157602CD
                        ELSE
                            DROP INDEX IDX_C67B0CD0157602CD ON process_definition');
        $this->addSql('ALTER TABLE process_definition DROP COLUMN cancel_step_id');

    }
}
