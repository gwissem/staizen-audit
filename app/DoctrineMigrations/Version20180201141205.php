<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180201141205 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE service_status_update (id INT IDENTITY NOT NULL, service_id INT, step_id NVARCHAR(20), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_F5D027B7ED5CA9E6 ON service_status_update (service_id)');
        $this->addSql('CREATE INDEX IDX_F5D027B773B21E9C ON service_status_update (step_id)');
        $this->addSql('CREATE TABLE service_status_dictionary_translation (id INT IDENTITY NOT NULL, translatable_id INT, message VARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_2576DB1D2C2AC5D3 ON service_status_dictionary_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX service_status_dictionary_translation_unique_translation ON service_status_dictionary_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE TABLE service_status (id INT IDENTITY NOT NULL, group_process_id INT, status_dictionary_id INT, serviceId INT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_45C7602AA90F94C6 ON service_status (group_process_id)');
        $this->addSql('CREATE INDEX IDX_45C7602A82960AA9 ON service_status (status_dictionary_id)');
        $this->addSql('CREATE TABLE service_status_dictionary (id INT IDENTITY NOT NULL, progress INT, sparxStatus NVARCHAR(64), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('ALTER TABLE service_status_update ADD CONSTRAINT FK_F5D027B7ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service_definition (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service_status_update ADD CONSTRAINT FK_F5D027B773B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service_status_dictionary_translation ADD CONSTRAINT FK_2576DB1D2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES service_status_dictionary (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE service_status ADD CONSTRAINT FK_45C7602AA90F94C6 FOREIGN KEY (group_process_id) REFERENCES process_instance (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service_status ADD CONSTRAINT FK_45C7602A82960AA9 FOREIGN KEY (status_dictionary_id) REFERENCES service_status_dictionary (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service_definition ADD sparx_service_name NVARCHAR(100)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE service_status_dictionary_translation DROP CONSTRAINT FK_2576DB1D2C2AC5D3');
        $this->addSql('ALTER TABLE service_status DROP CONSTRAINT FK_45C7602A82960AA9');
        $this->addSql('DROP TABLE service_status_update');
        $this->addSql('DROP TABLE service_status_dictionary_translation');
        $this->addSql('DROP TABLE service_status');
        $this->addSql('DROP TABLE service_status_dictionary');
        $this->addSql('ALTER TABLE service_definition DROP COLUMN sparx_service_name');

    }
}
