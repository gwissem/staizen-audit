<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170207160945 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B4C1EF5FAD7C3F23\')
                            ALTER TABLE process_instance DROP CONSTRAINT IDX_B4C1EF5FAD7C3F23
                        ELSE
                            DROP INDEX IDX_B4C1EF5FAD7C3F23 ON process_instance'
        );
        $this->addSql('ALTER TABLE vin_header ADD package_id INT');
        $this->addSql(
            'ALTER TABLE vin_header ADD CONSTRAINT FK_275188E3F44CABFF FOREIGN KEY (package_id) REFERENCES vin_package (id) ON DELETE CASCADE'
        );
        $this->addSql('CREATE INDEX IDX_275188E3F44CABFF ON vin_header (package_id)');
        $this->addSql('ALTER TABLE process_instance ALTER COLUMN previous_process_instance_id INT');
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5FAD7C3F23 FOREIGN KEY (previous_process_instance_id) REFERENCES process_instance (id)'
        );
        $this->addSql('CREATE INDEX IDX_B4C1EF5FAD7C3F23 ON process_instance (previous_process_instance_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5FAD7C3F23');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B4C1EF5FAD7C3F23\')
                            ALTER TABLE process_instance DROP CONSTRAINT IDX_B4C1EF5FAD7C3F23
                        ELSE
                            DROP INDEX IDX_B4C1EF5FAD7C3F23 ON process_instance'
        );
        $this->addSql('ALTER TABLE process_instance ADD program_id INT');
        $this->addSql(
            'ALTER TABLE process_instance ALTER COLUMN previous_process_instance_id NVARCHAR(20) COLLATE Polish_CI_AS'
        );
        $this->addSql('ALTER TABLE vin_header DROP CONSTRAINT FK_275188E3F44CABFF');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_275188E3F44CABFF\')
                            ALTER TABLE vin_header DROP CONSTRAINT IDX_275188E3F44CABFF
                        ELSE
                            DROP INDEX IDX_275188E3F44CABFF ON vin_header'
        );
        $this->addSql('ALTER TABLE vin_header DROP COLUMN package_id');
    }
}
