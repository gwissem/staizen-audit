<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161011094352 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE tool_translation (id INT IDENTITY NOT NULL, translatable_id INT, name NVARCHAR(255) NOT NULL, [description] NVARCHAR(MAX) NULL, sub_query_description NVARCHAR(MAX) NULL, locale NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), slug NVARCHAR(255), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE tool_action_translation (id INT IDENTITY NOT NULL, translatable_id INT, name NVARCHAR(255) NULL, locale NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE tool_form_field_translation (id INT IDENTITY NOT NULL, translatable_id INT, label NVARCHAR(255) NOT NULL, locale NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE filter_translation (id INT IDENTITY NOT NULL, translatable_id INT, label NVARCHAR(255) NOT NULL, locale NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE tool_summary_translation (id INT IDENTITY NOT NULL, translatable_id INT, [description] NVARCHAR(255) NULL, locale NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE style_translation (id INT IDENTITY NOT NULL, translatable_id INT, alias NVARCHAR(255) NULL, locale NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );

        $this->addSql('CREATE INDEX IDX_8C91D78C2C2AC5D3 ON tool_action_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX tool_action_translation_unique_translation ON tool_action_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql('CREATE INDEX IDX_2E5B5B312C2AC5D3 ON tool_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX tool_translation_unique_translation ON tool_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql('CREATE INDEX IDX_99D612D52C2AC5D3 ON tool_form_field_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX tool_form_field_translation_unique_translation ON tool_form_field_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql('CREATE INDEX IDX_CD6B90BE2C2AC5D3 ON filter_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX filter_translation_unique_translation ON filter_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql('CREATE INDEX IDX_84BF119C2C2AC5D3 ON tool_summary_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX tool_summary_translation_unique_translation ON tool_summary_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql('CREATE INDEX IDX_A14D10A02C2AC5D3 ON style_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX style_translation_unique_translation ON style_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE tool_action_translation ADD CONSTRAINT FK_8C91D78C2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES tool_action (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE tool_translation ADD CONSTRAINT FK_2E5B5B312C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES tool (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE tool_form_field_translation ADD CONSTRAINT FK_99D612D52C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES tool_form_field (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE filter_translation ADD CONSTRAINT FK_CD6B90BE2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES filter (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE tool_summary_translation ADD CONSTRAINT FK_84BF119C2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES tool_summary (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE style_translation ADD CONSTRAINT FK_A14D10A02C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES style (id) ON DELETE CASCADE'
        );
        $this->addSql('ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN redirect_uri NVARCHAR(MAX) NOT NULL');

        $this->addSql(
            "INSERT INTO tool_translation (translatable_id, name, slug, description, sub_query_description , locale, created_at, updated_at) SELECT id, name, slug, description, sub_query_description, 'pl', created_at, updated_at FROM tool t"
        );
        $this->addSql(
            "INSERT INTO tool_action_translation (translatable_id, name, locale, created_at, updated_at) SELECT id, name, 'pl', created_at, updated_at FROM tool_action ta"
        );
        $this->addSql(
            "INSERT INTO filter_translation (translatable_id, label, locale, created_at, updated_at) SELECT id, label, 'pl', created_at, updated_at FROM filter f"
        );
        $this->addSql(
            "INSERT INTO tool_form_field_translation (translatable_id, label, locale, created_at, updated_at) SELECT id, label, 'pl', created_at, updated_at FROM tool_form_field tff"
        );
        $this->addSql(
            "INSERT INTO tool_summary_translation (translatable_id, description, locale, created_at, updated_at) SELECT id, description, 'pl', GETDATE(), GETDATE() FROM tool_summary ts"
        );
        $this->addSql(
            "INSERT INTO style_translation (translatable_id, alias, locale, created_at, updated_at) SELECT id, alias, 'pl', created_at, updated_at FROM style s"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('DROP TABLE tool_action_translation');
        $this->addSql('DROP TABLE tool_translation');
        $this->addSql('DROP TABLE tool_form_field_translation');
        $this->addSql('DROP TABLE filter_translation');
        $this->addSql('DROP TABLE tool_summary_translation');
        $this->addSql('DROP TABLE style_translation');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN redirect_uri NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
    }
}
