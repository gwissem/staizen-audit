<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170530101001 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE attribute_condition ADD step_id NVARCHAR(20)');
        $this->addSql('ALTER TABLE attribute_condition ADD CONSTRAINT FK_F7F8C573B21E9C FOREIGN KEY (step_id) REFERENCES step (id)');
        $this->addSql('CREATE INDEX IDX_F7F8C573B21E9C ON attribute_condition (step_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE attribute_condition DROP CONSTRAINT FK_F7F8C573B21E9C');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_F7F8C573B21E9C\')
                            ALTER TABLE attribute_condition DROP CONSTRAINT IDX_F7F8C573B21E9C
                        ELSE
                            DROP INDEX IDX_F7F8C573B21E9C ON attribute_condition');
        $this->addSql('ALTER TABLE attribute_condition DROP COLUMN step_id');
    }
}
