<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191011105532 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('CREATE TABLE tool_log (id INT IDENTITY NOT NULL, user_id INT, tool_id INT, time NUMERIC(12, 6) NOT NULL, type_log INT, created_at DATETIME2(6) NOT NULL, PRIMARY KEY (id))');
//
//        $this->addSql('CREATE INDEX IDX_76B6F5EFA76ED395 ON tool_log (user_id)');
//        $this->addSql('CREATE INDEX IDX_76B6F5EF8F7B22CC ON tool_log (tool_id)');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('DROP TABLE tool_log');

    }
}
