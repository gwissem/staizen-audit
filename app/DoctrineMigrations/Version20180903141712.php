<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180903141712 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


//        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_ED7959CD73B21E9C\')
//                            ALTER TABLE AtlasDB_def.dbo.api_call DROP CONSTRAINT IDX_ED7959CD73B21E9C
//                        ELSE
//                            DROP INDEX IDX_ED7959CD73B21E9C ON AtlasDB_def.dbo.api_call');
//
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ADD created_by_id INT');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ADD updated_by_id INT');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ADD updated_at DATETIME2(6)');
//
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ALTER COLUMN url NVARCHAR(255)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ALTER COLUMN created_at DATETIME2(6)');
//
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_ED7959CD73B21E9C ON AtlasDB_def.dbo.api_call (step_id) WHERE step_id IS NOT NULL');
//        $this->addSql('CREATE INDEX IDX_ED7959CDB03A8386 ON AtlasDB_def.dbo.api_call (created_by_id)');
//        $this->addSql('CREATE INDEX IDX_ED7959CD896DBBDE ON AtlasDB_def.dbo.api_call (updated_by_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');



//        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'UNIQ_ED7959CD73B21E9C\')
//                            ALTER TABLE AtlasDB_def.dbo.api_call DROP CONSTRAINT UNIQ_ED7959CD73B21E9C
//                        ELSE
//                            DROP INDEX UNIQ_ED7959CD73B21E9C ON AtlasDB_def.dbo.api_call');
//        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_ED7959CDB03A8386\')
//                            ALTER TABLE AtlasDB_def.dbo.api_call DROP CONSTRAINT IDX_ED7959CDB03A8386
//                        ELSE
//                            DROP INDEX IDX_ED7959CDB03A8386 ON AtlasDB_def.dbo.api_call');
//        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_ED7959CD896DBBDE\')
//                            ALTER TABLE AtlasDB_def.dbo.api_call DROP CONSTRAINT IDX_ED7959CD896DBBDE
//                        ELSE
//                            DROP INDEX IDX_ED7959CD896DBBDE ON AtlasDB_def.dbo.api_call');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call DROP COLUMN created_by_id');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call DROP COLUMN updated_by_id');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call DROP COLUMN updated_at');
//
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ALTER COLUMN url NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ALTER COLUMN created_at DATETIME2(6) NOT NULL');


    }
}
