<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161017134240 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE ticket ADD browser NVARCHAR(255) NULL');
        $this->addSql('ALTER TABLE ticket ADD screen_resolution NVARCHAR(255) NULL');
        $this->addSql('ALTER TABLE ticket ADD request_info NVARCHAR(MAX) NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE ticket DROP COLUMN browser');
        $this->addSql('ALTER TABLE ticket DROP COLUMN screen_resolution');
        $this->addSql('ALTER TABLE ticket DROP COLUMN request_info');

    }
}
