<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160921110034 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $documentDbName = $this->container->getParameter('mssql_documents_database_name');
        $parameters = $this->connection->getParams();
        $this->skipIf(
            $parameters['dbname'] != $documentDbName,
            'This is the other DB\'s migration, pass a correct --em parameter'
        );

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE d_file (id INT IDENTITY NOT NULL, document_id INT, content VARBINARY(MAX) NOT NULL, name NVARCHAR(255) NOT NULL, path NVARCHAR(255) NOT NULL, createdAt DATETIME2(6), updatedAt DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_353F5DF9C33F7837 ON d_file (document_id)');
        $this->addSql(
            'CREATE TABLE d_document (id INT IDENTITY NOT NULL, name NVARCHAR(255), companyId INT NOT NULL, createdAt DATETIME2(6), updatedAt DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'ALTER TABLE d_file ADD CONSTRAINT FK_353F5DF9C33F7837 FOREIGN KEY (document_id) REFERENCES d_document (id)'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $documentDbName = $this->container->getParameter('mssql_documents_database_name');
        $parameters = $this->connection->getParams();
        $this->skipIf(
            $parameters['dbname'] != $documentDbName,
            'This is the other DB\'s migration, pass a correct --em parameter'
        );

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE d_file DROP CONSTRAINT FK_353F5DF9C33F7837');
        $this->addSql(
            'CREATE TABLE [file] (id INT IDENTITY NOT NULL, document_id INT, content VARBINARY(255) NOT NULL, name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, path NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, createdAt DATETIME2(6), updatedAt DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('DROP TABLE d_file');
        $this->addSql('DROP TABLE d_document');
    }
}
