<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180108130107 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE sparx_service_status (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), status_id INT, attribute_path NVARCHAR(255), value_int INT, value_string NVARCHAR(255), value_text VARCHAR(MAX), value_decimal NUMERIC(18, 6), value_date DATETIME2(6), service NVARCHAR(255), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_4D791E5873B21E9C ON sparx_service_status (step_id)');
        $this->addSql('CREATE INDEX IDX_4D791E586BF700BD ON sparx_service_status (status_id)');
        $this->addSql('ALTER TABLE sparx_service_status ADD CONSTRAINT FK_4D791E5873B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE sparx_service_status ADD CONSTRAINT FK_4D791E586BF700BD FOREIGN KEY (status_id) REFERENCES sparx_status (id) ON DELETE SET NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE sparx_service_status');
    }
}
