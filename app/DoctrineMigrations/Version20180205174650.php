<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180205174650 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE service_status_dictionary ADD service INT');
        $this->addSql('ALTER TABLE service_status_dictionary ADD CONSTRAINT FK_74E46475E19D9AD2 FOREIGN KEY (service) REFERENCES service_definition (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_74E46475E19D9AD2 ON service_status_dictionary (service)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE service_status_dictionary DROP CONSTRAINT FK_74E46475E19D9AD2');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_74E46475E19D9AD2\')
                            ALTER TABLE service_status_dictionary DROP CONSTRAINT IDX_74E46475E19D9AD2
                        ELSE
                            DROP INDEX IDX_74E46475E19D9AD2 ON service_status_dictionary');
        $this->addSql('ALTER TABLE service_status_dictionary DROP COLUMN service');

    }
}
