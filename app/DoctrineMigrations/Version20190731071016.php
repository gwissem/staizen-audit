<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190731071016 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("IF NOT EXISTS(SELECT 1 FROM AtlasDB_def.sys.columns WHERE Name = 'is_mobile_form' AND Object_ID = Object_ID('AtlasDB_def.dbo.step'))
                BEGIN
                    
                    ALTER TABLE AtlasDB_def.dbo.step ADD is_mobile_form BIT
                    ALTER TABLE AtlasDB_def.dbo.step ADD CONSTRAINT DF_43B9FE3C_61DC88D8 DEFAULT '0' FOR is_mobile_form
                    
                END");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("IF EXISTS(SELECT 1 FROM AtlasDB_def.sys.columns WHERE Name = 'is_mobile_form' AND Object_ID = Object_ID('AtlasDB_def.dbo.step'))
                BEGIN
                    
                    ALTER TABLE AtlasDB_def.dbo.step DROP CONSTRAINT DF_43B9FE3C_61DC88D8
                    ALTER TABLE AtlasDB_def.dbo.step DROP COLUMN is_mobile_form
                    
                END");

    }
}
