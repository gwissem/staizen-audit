<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use MailboxBundle\Service\MailboxManagementService;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160927103230 extends AbstractMigration implements ContainerAwareInterface
{

    /** @var  ContainerInterface $container */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function postUp(Schema $schema)
    {

        $fs = new Filesystem();
        $pathDirForAttachment = $this->container->get('kernel')->getRootDir() . MailboxManagementService::ATTACHMENT_OF_EMAILS_PATH;
        $pathDirEmailsCache = $this->container->get('kernel')->getRootDir() . MailboxManagementService::EMAILS_CACHE_DIR;

        if(!$fs->exists($pathDirForAttachment))
        {
            $fs->mkdir($pathDirForAttachment);
        }

        if(!$fs->exists($pathDirEmailsCache))
        {
            $fs->mkdir($pathDirEmailsCache);
        }

    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE connected_mailbox ADD enabled BIT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE connected_mailbox DROP COLUMN enabled');

    }
}
