<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170104134934 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE attribute_translation (id INT IDENTITY NOT NULL, translatable_id INT, name NVARCHAR(255), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_89B5B6BF2C2AC5D3 ON attribute_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX attribute_translation_unique_translation ON attribute_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE attribute_translation ADD CONSTRAINT FK_89B5B6BF2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES attribute (id) ON DELETE CASCADE'
        );
        $this->addSql('ALTER TABLE attribute DROP COLUMN name');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


        $this->addSql('DROP TABLE attribute_translation');
        $this->addSql('ALTER TABLE attribute ADD name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');

    }
}
