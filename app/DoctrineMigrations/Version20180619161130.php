<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180619161130 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE postpone_process_instance (id INT IDENTITY NOT NULL, process_instance_id INT, user_id INT, description VARCHAR(MAX), reason NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_158285EC3424B10C ON postpone_process_instance (process_instance_id) WHERE process_instance_id IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_158285ECA76ED395 ON postpone_process_instance (user_id)');
        $this->addSql('ALTER TABLE postpone_process_instance ADD CONSTRAINT FK_158285EC3424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE postpone_process_instance ADD CONSTRAINT FK_158285ECA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE SET NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE postpone_process_instance');

    }
}
