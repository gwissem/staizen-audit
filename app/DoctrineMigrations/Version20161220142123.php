<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161220142123 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE attribute ADD object_definition_id INT');
        $this->addSql(
            'ALTER TABLE attribute ADD CONSTRAINT FK_FA7AEFFB22F2763 FOREIGN KEY (object_definition_id) REFERENCES object_definition (id) ON DELETE CASCADE'
        );
        $this->addSql('CREATE INDEX IDX_FA7AEFFB22F2763 ON attribute (object_definition_id)');
        $this->addSql('ALTER TABLE object_definition DROP CONSTRAINT FK_1BAF8664727ACA70');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_1BAF8664727ACA70\')
                            ALTER TABLE object_definition DROP CONSTRAINT IDX_1BAF8664727ACA70
                        ELSE
                            DROP INDEX IDX_1BAF8664727ACA70 ON object_definition'
        );
        $this->addSql('ALTER TABLE object_definition DROP COLUMN parent_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE attribute DROP CONSTRAINT FK_FA7AEFFB22F2763');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FA7AEFFB22F2763\')
                            ALTER TABLE attribute DROP CONSTRAINT IDX_FA7AEFFB22F2763
                        ELSE
                            DROP INDEX IDX_FA7AEFFB22F2763 ON attribute'
        );
        $this->addSql('ALTER TABLE attribute DROP COLUMN object_definition_id');
        $this->addSql('ALTER TABLE attribute ALTER COLUMN regex NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE object_definition ADD parent_id INT');
        $this->addSql(
            'ALTER TABLE object_definition ADD CONSTRAINT FK_1BAF8664727ACA70 FOREIGN KEY (parent_id) REFERENCES object_definition (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_1BAF8664727ACA70 ON object_definition (parent_id)');
    }
}
