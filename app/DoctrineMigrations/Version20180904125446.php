<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180904125446 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.platform_group', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.platform_group_platforms', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.user_role_group', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.user_role_group_task', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.process_flow_program', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.api_call', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.api_call_type', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.diagnosis_results', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.service_definition_program', 'AtlasDB_def')");
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.rsa_close_event', 'AtlasDB_def')");


//        $this->addSql('TRUNCATE TABLE AtlasDB_def.dbo.service_definition_platform');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_definition_platform DROP CONSTRAINT FK_1548E08FFFE6496F');
//        $this->addSql('DROP INDEX IDX_1548E08FFFE6496F ON AtlasDB_def.dbo.service_definition_platform');
//        $this->addSql("EXECUTE AtlasDB_def.sys.sp_rename 'dbo.service_definition_platform.platform_id', 'program_id', 'COLUMN'");
//        $this->addSql("CREATE INDEX IDX_1548E08FFFE6496F ON AtlasDB_def.dbo.service_definition_platform (program_id)");
//        $this->addSql("ALTER TABLE AtlasDB_def.dbo.service_definition_platform ADD CONSTRAINT FK_1548E08FFFE6496F FOREIGN KEY (program_id) REFERENCES AtlasDB_def.dbo.vin_program (id)");
//        $this->addSql("EXEC AtlasDB_def.sys.sp_rename 'dbo.service_definition_platform', 'service_definition_program'");
//        $this->addSql("CREATE SYNONYM dbo.service_definition_program for AtlasDB_def.dbo.service_definition_program");

        $this->addSql("DECLARE @serviceId INT
                        DECLARE @servicePosition INT 
                        DECLARE @platformId INT
                        DECLARE @programId INT
                        DECLARE @description NVARCHAR(MAX)
                        
                        DECLARE @serviceDescriptions TABLE (
                            id INT,
                            platform_id INT,
                            service_definition_id INT,
                            description NVARCHAR(MAX)
                        )
                        
                        INSERT INTO @serviceDescriptions 
                        SELECT * FROM AtlasDB_v.dbo.service_platform_description
                        
                        DECLARE kur cursor LOCAL for
                        SELECT sd.id as id, sd.[position] as [position] FROM AtlasDB_def.dbo.service_definition as sd
                            OPEN kur;
                            FETCH NEXT FROM kur INTO @serviceId, @servicePosition
                            WHILE @@FETCH_STATUS=0
                            BEGIN
                        
                                    DECLARE kur2 cursor LOCAL for
                                    SELECT p.id as localizationId FROM AtlasDB_def.dbo.platform as p WHERE active = 1
                                        OPEN kur2;
                                        FETCH NEXT FROM kur2 INTO @platformId
                                        WHILE @@FETCH_STATUS=0
                                        BEGIN
                                            
                                            DECLARE kur3 cursor LOCAL for
                                            SELECT vp.id FROM AtlasDB_def.dbo.vin_program as vp WHERE vp.platform_id = @platformId
                                                OPEN kur3;
                                                FETCH NEXT FROM kur3 INTO @programId
                                                WHILE @@FETCH_STATUS=0
                                                BEGIN
                                                    
                                                    IF (SELECT count(id) FROM AtlasDB_def.dbo.service_definition_program as sdp WHERE sdp.program_id = @programId AND service_definition_id = @serviceId) = 0
                                                    BEGIN
                                                        
                                                        SELECT @description = description FROM @serviceDescriptions WHERE platform_id = @platformId AND service_definition_id = @serviceId
                                                        INSERT INTO AtlasDB_def.dbo.service_definition_program (service_definition_id, program_id, [position], description) VALUES (@serviceId, @programId, @servicePosition, @description)
                                
                                                    END
                                                    
                                                    FETCH NEXT FROM kur3 INTO @programId
                                                END
                                                CLOSE kur3
                                            DEALLOCATE kur3
                                        
                                            FETCH NEXT FROM kur2 INTO @platformId
                                        END
                                    CLOSE kur2
                                    DEALLOCATE kur2
                        
                                FETCH NEXT FROM kur INTO @serviceId, @servicePosition
                            END
                        CLOSE kur
                        DEALLOCATE kur");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
