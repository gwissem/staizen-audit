<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420135017 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE phone_call_queue (id INT IDENTITY NOT NULL, process_instance_id INT, record_number INT NOT NULL, phone_number NVARCHAR(255) NOT NULL, active BIT NOT NULL, status BIT, started_at DATETIME2(6), finished_at DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_351C254E3424B10C ON phone_call_queue (process_instance_id)');
        $this->addSql('ALTER TABLE phone_call_queue ADD CONSTRAINT DF_351C254E_4B1EFC02 DEFAULT \'0\' FOR active');
        $this->addSql('ALTER TABLE phone_call_queue ADD CONSTRAINT FK_351C254E3424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE phone_call_queue');

    }
}
