<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181119080237 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.api_call_json_definition (id INT IDENTITY NOT NULL, name NVARCHAR(255), json VARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE SYNONYM dbo.api_call_json_definition for AtlasDB_def.dbo.api_call_json_definition');
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES ('dbo.api_call_json_definition','AtlasDB_def') ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE AtlasDB_def.dbo.api_call_json_definition');

    }
}
