<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180407010338 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE process_enter_history (id INT IDENTITY NOT NULL, process_instance_id INT, user_id INT, date_enter DATETIME2(6), date_leave DATETIME2(6), time INT, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_1DCF65FF3424B10C ON process_enter_history (process_instance_id)');
        $this->addSql('CREATE INDEX IDX_1DCF65FFA76ED395 ON process_enter_history (user_id)');
        $this->addSql('ALTER TABLE process_enter_history ADD CONSTRAINT DF_1DCF65FF_6F949845 DEFAULT 0 FOR time');
        $this->addSql('ALTER TABLE process_enter_history ADD CONSTRAINT FK_1DCF65FF3424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id)');
        $this->addSql('ALTER TABLE process_enter_history ADD CONSTRAINT FK_1DCF65FFA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE SET NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE process_enter_history');

    }
}
