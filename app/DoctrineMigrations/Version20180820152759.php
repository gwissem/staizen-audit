<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180820152759 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        
//        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'unique_name_locale\')
//                            ALTER TABLE AtlasDb_def.dbo.business_config_definition_translation DROP CONSTRAINT unique_name_locale
//                        ELSE
//                            DROP INDEX unique_name_locale ON AtlasDb_def.dbo.business_config_definition_translation');
//        $this->addSql('CREATE UNIQUE INDEX unique_id_name_locale ON AtlasDb_def.dbo.business_config_definition_translation (translatable_id, locale, name) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL AND name IS NOT NULL');
        
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        
//        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_8F2A6086C76F1F52\')
//                            ALTER TABLE business_config DROP CONSTRAINT IDX_8F2A6086C76F1F52
//                        ELSE
//                            DROP INDEX IDX_8F2A6086C76F1F52 ON business_config');
//        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'unique_id_name_locale\')
//                            ALTER TABLE AtlasDb_def.dbo.business_config_definition_translation DROP CONSTRAINT unique_id_name_locale
//                        ELSE
//                            DROP INDEX unique_id_name_locale ON AtlasDb_def.dbo.business_config_definition_translation');
        
    }
}
