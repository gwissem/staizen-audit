<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170327160150 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE custom_content_translation (id INT IDENTITY NOT NULL, translatable_id INT, content NVARCHAR(MAX), locale NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_23B15EE12C2AC5D3 ON custom_content_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX custom_content_translation_unique_translation ON custom_content_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE TABLE custom_content (id INT IDENTITY NOT NULL, [key] NVARCHAR(255) NOT NULL, value NVARCHAR(255), created_at DATETIME2(6), updated_at DATETIME2(6), deleted_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('ALTER TABLE custom_content_translation ADD CONSTRAINT FK_23B15EE12C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES custom_content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fos_user ADD accepted_regulations BIT');

        $this->addSql('INSERT INTO custom_content ([key], created_at) VALUES (\'atlas_access_regulations\', GETDATE())');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE custom_content_translation DROP CONSTRAINT FK_23B15EE12C2AC5D3');
        $this->addSql('DROP TABLE custom_content_translation');
        $this->addSql('DROP TABLE custom_content');
        $this->addSql('ALTER TABLE fos_user DROP COLUMN accepted_regulations');

    }
}
