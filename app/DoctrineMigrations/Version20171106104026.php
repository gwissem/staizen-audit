<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171106104026 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE service_definition (id INT IDENTITY NOT NULL, company_id INT, active BIT, position INT, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_FE61166C979B1AD6 ON service_definition (company_id)');
        $this->addSql('CREATE TABLE service_definition_translation (id INT IDENTITY NOT NULL, translatable_id INT, name VARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_E1DEAFD62C2AC5D3 ON service_definition_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX service_definition_translation_unique_translation ON service_definition_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('ALTER TABLE service_definition ADD CONSTRAINT FK_FE61166C979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service_definition_translation ADD CONSTRAINT FK_E1DEAFD62C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES service_definition (id) ON DELETE CASCADE');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD21A11393');
        $this->addSql('ALTER TABLE service_definition_translation DROP CONSTRAINT FK_E1DEAFD62C2AC5D3');
        $this->addSql('DROP TABLE service_definition');
        $this->addSql('DROP TABLE service_definition_translation');
        $this->addSql('ALTER TABLE service DROP COLUMN service_definition_id');
    }
}
