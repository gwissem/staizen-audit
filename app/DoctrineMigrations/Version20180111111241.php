<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180111111241 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE step ADD can_previous SMALLINT');
        $this->addSql('ALTER TABLE step ADD CONSTRAINT DF_43B9FE3C_B06BC9BE DEFAULT 0 FOR can_previous');
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT DF_B4C1EF5F_B06BC9BE');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN can_previous');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE process_instance ADD can_previous SMALLINT');
        $this->addSql('ALTER TABLE process_instance ADD CONSTRAINT DF_B4C1EF5F_B06BC9BE DEFAULT 0 FOR can_previous');
        $this->addSql('ALTER TABLE step DROP CONSTRAINT DF_B4C1EF5F_B06BC9BE');
        $this->addSql('ALTER TABLE step DROP COLUMN can_previous');

    }
}
