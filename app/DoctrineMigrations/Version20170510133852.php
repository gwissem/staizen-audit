<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170510133852 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_9C7F4A268A4C35B0\')
                            ALTER TABLE vin_dictionary DROP CONSTRAINT IDX_9C7F4A268A4C35B0
                        ELSE
                            DROP INDEX IDX_9C7F4A268A4C35B0 ON vin_dictionary');
        $this->addSql('ALTER TABLE vin_dictionary ADD attribute_path NVARCHAR(255)');
        $this->addSql('ALTER TABLE vin_dictionary DROP COLUMN step_attribute_definition_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE vin_dictionary ADD step_attribute_definition_id NVARCHAR(50) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE vin_dictionary DROP COLUMN attribute_path');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_9C7F4A268A4C35B0 ON vin_dictionary (step_attribute_definition_id)');
    }
}
