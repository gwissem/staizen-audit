<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180711155443 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE process_instance_property (process_instance_id INT NOT NULL, platform NVARCHAR(20), urgent BIT, priority SMALLINT, priority_changed_at DATETIME2(6), platform_id INT, PRIMARY KEY (process_instance_id))');
        $this->addSql('CREATE INDEX IDX_B9182588FFE6496F ON process_instance_property (platform_id)');
        $this->addSql('CREATE INDEX IDX_B91825883952D0CB ON process_instance_property (platform)');

        $this->addSql('DROP TABLE process_instance_priority');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.process_flow_program (process_flow_id INT NOT NULL, program_id INT NOT NULL, PRIMARY KEY (process_flow_id, program_id))');
        $this->addSql('CREATE INDEX IDX_BB63E63836CED7CE ON AtlasDB_def.dbo.process_flow_program (process_flow_id)');
        $this->addSql('CREATE INDEX IDX_BB63E6383EB8070A ON AtlasDB_def.dbo.process_flow_program (program_id)');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.process_flow_program ADD CONSTRAINT FK_BB63E63836CED7CE FOREIGN KEY (process_flow_id) REFERENCES AtlasDB_def.dbo.process_flow (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.process_flow_program ADD CONSTRAINT FK_BB63E6383EB8070A FOREIGN KEY (program_id) REFERENCES AtlasDB_def.dbo.vin_program (id) ON DELETE CASCADE');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.step_program (step_id NVARCHAR(20) NOT NULL, program_id INT NOT NULL, PRIMARY KEY (step_id, program_id))');
        $this->addSql('CREATE INDEX IDX_2210F16B73B21E9C ON AtlasDB_def.dbo.step_program (step_id)');
        $this->addSql('CREATE INDEX IDX_2210F16B3EB8070A ON AtlasDB_def.dbo.step_program (program_id)');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_program ADD CONSTRAINT FK_2210F16B73B21E9C FOREIGN KEY (step_id) REFERENCES AtlasDB_def.dbo.step (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_program ADD CONSTRAINT FK_2210F16B3EB8070A FOREIGN KEY (program_id) REFERENCES AtlasDB_def.dbo.vin_program (id) ON DELETE CASCADE');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('DROP TABLE process_instance_property');
        $this->addSql('CREATE TABLE process_instance_priority (process_instance_id INT NOT NULL, urgent BIT, priority SMALLINT, priority_changed_at DATETIME2(6), PRIMARY KEY (process_instance_id))');
        $this->addSql('ALTER TABLE process_instance_priority ADD CONSTRAINT FK_504CE5713424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id) ON UPDATE NO ACTION ON DELETE NO ACTION');

        $this->addSql('DROP TABLE AtlasDB_def.dbo.process_flow_program');
        $this->addSql('DROP TABLE AtlasDB_def.dbo.step_program');

    }
}
