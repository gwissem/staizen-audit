<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170214102109 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE attribute_value_history (id INT IDENTITY NOT NULL, attribute_value_id INT, value_attribute_id INT, created_by_original_id INT, created_by_id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_decimal NUMERIC(2, 0), value_date DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_69945E4E65A22152 ON attribute_value_history (attribute_value_id)');
        $this->addSql('CREATE INDEX IDX_69945E4E1443AA30 ON attribute_value_history (value_attribute_id)');
        $this->addSql('CREATE INDEX IDX_69945E4EB0B2CEF2 ON attribute_value_history (created_by_original_id)');
        $this->addSql('CREATE INDEX IDX_69945E4EB03A8386 ON attribute_value_history (created_by_id)');
        $this->addSql(
            'ALTER TABLE attribute_value_history ADD CONSTRAINT FK_69945E4E65A22152 FOREIGN KEY (attribute_value_id) REFERENCES attribute_value (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value_history ADD CONSTRAINT FK_69945E4E1443AA30 FOREIGN KEY (value_attribute_id) REFERENCES attribute (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value_history ADD CONSTRAINT FK_69945E4EB0B2CEF2 FOREIGN KEY (created_by_original_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value_history ADD CONSTRAINT FK_69945E4EB03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('ALTER TABLE attribute ADD created_by_original_id INT');
        $this->addSql('ALTER TABLE attribute ADD created_by_id INT');
        $this->addSql('ALTER TABLE attribute ADD updated_by_id INT');
        $this->addSql('ALTER TABLE attribute ADD updated_by_original_id INT');
        $this->addSql(
            'ALTER TABLE attribute ADD CONSTRAINT FK_FA7AEFFBB0B2CEF2 FOREIGN KEY (created_by_original_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute ADD CONSTRAINT FK_FA7AEFFBB03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute ADD CONSTRAINT FK_FA7AEFFB896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute ADD CONSTRAINT FK_FA7AEFFBEA17E408 FOREIGN KEY (updated_by_original_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_FA7AEFFBB0B2CEF2 ON attribute (created_by_original_id)');
        $this->addSql('CREATE INDEX IDX_FA7AEFFBB03A8386 ON attribute (created_by_id)');
        $this->addSql('CREATE INDEX IDX_FA7AEFFB896DBBDE ON attribute (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_FA7AEFFBEA17E408 ON attribute (updated_by_original_id)');
        $this->addSql('ALTER TABLE attribute_value ADD created_by_original_id INT');
        $this->addSql('ALTER TABLE attribute_value ADD created_by_id INT');
        $this->addSql('ALTER TABLE attribute_value ADD updated_by_id INT');
        $this->addSql('ALTER TABLE attribute_value ADD updated_by_original_id INT');
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82B0B2CEF2 FOREIGN KEY (created_by_original_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82EA17E408 FOREIGN KEY (updated_by_original_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_FE4FBB82B0B2CEF2 ON attribute_value (created_by_original_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82B03A8386 ON attribute_value (created_by_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82896DBBDE ON attribute_value (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82EA17E408 ON attribute_value (updated_by_original_id)');
        $this->addSql('ALTER TABLE process_instance ADD updated_by_original_id INT');
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5FEA17E408 FOREIGN KEY (updated_by_original_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_B4C1EF5FEA17E408 ON process_instance (updated_by_original_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('DROP TABLE attribute_value_history');
        $this->addSql('ALTER TABLE attribute DROP CONSTRAINT FK_FA7AEFFBB0B2CEF2');
        $this->addSql('ALTER TABLE attribute DROP CONSTRAINT FK_FA7AEFFBB03A8386');
        $this->addSql('ALTER TABLE attribute DROP CONSTRAINT FK_FA7AEFFB896DBBDE');
        $this->addSql('ALTER TABLE attribute DROP CONSTRAINT FK_FA7AEFFBEA17E408');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FA7AEFFBB0B2CEF2\')
                            ALTER TABLE attribute DROP CONSTRAINT IDX_FA7AEFFBB0B2CEF2
                        ELSE
                            DROP INDEX IDX_FA7AEFFBB0B2CEF2 ON attribute'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FA7AEFFBB03A8386\')
                            ALTER TABLE attribute DROP CONSTRAINT IDX_FA7AEFFBB03A8386
                        ELSE
                            DROP INDEX IDX_FA7AEFFBB03A8386 ON attribute'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FA7AEFFB896DBBDE\')
                            ALTER TABLE attribute DROP CONSTRAINT IDX_FA7AEFFB896DBBDE
                        ELSE
                            DROP INDEX IDX_FA7AEFFB896DBBDE ON attribute'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FA7AEFFBEA17E408\')
                            ALTER TABLE attribute DROP CONSTRAINT IDX_FA7AEFFBEA17E408
                        ELSE
                            DROP INDEX IDX_FA7AEFFBEA17E408 ON attribute'
        );
        $this->addSql('ALTER TABLE attribute DROP COLUMN created_by_original_id');
        $this->addSql('ALTER TABLE attribute DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE attribute DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE attribute DROP COLUMN updated_by_original_id');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB82B0B2CEF2');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB82B03A8386');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB82896DBBDE');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB82EA17E408');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB82B0B2CEF2\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB82B0B2CEF2
                        ELSE
                            DROP INDEX IDX_FE4FBB82B0B2CEF2 ON attribute_value'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB82B03A8386\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB82B03A8386
                        ELSE
                            DROP INDEX IDX_FE4FBB82B03A8386 ON attribute_value'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB82896DBBDE\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB82896DBBDE
                        ELSE
                            DROP INDEX IDX_FE4FBB82896DBBDE ON attribute_value'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB82EA17E408\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB82EA17E408
                        ELSE
                            DROP INDEX IDX_FE4FBB82EA17E408 ON attribute_value'
        );
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN created_by_original_id');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN updated_by_original_id');
        $this->addSql(
            'ALTER TABLE process_flow_translation ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS'
        );
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5FEA17E408');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B4C1EF5FEA17E408\')
                            ALTER TABLE process_instance DROP CONSTRAINT IDX_B4C1EF5FEA17E408
                        ELSE
                            DROP INDEX IDX_B4C1EF5FEA17E408 ON process_instance'
        );
        $this->addSql('sp_RENAME \'process_instance.updated_by_original_id\', \'program_id\', \'COLUMN\'');

    }
}
