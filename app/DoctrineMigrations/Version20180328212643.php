<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180328212643 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN daily_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN cross_border_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN after_hours_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN start_return_costs NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN distance_unit_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN min_days_cost_free_return NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN min_days_return_costs NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN min_days_distance_unit_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN cross_border_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN after_hours_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN start_distance_measure_type NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN start_return_costs NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN distance_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN min_days_cost_free_return NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN min_days_distance_measure_type NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN min_days_return_costs NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN min_days_distance_cost NUMERIC(10, 2)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN daily_cost NUMERIC(10, 0) NOT NULL');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN cross_border_cost NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN after_hours_cost NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN start_return_costs NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN distance_unit_cost NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN min_days_cost_free_return NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN min_days_return_costs NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN min_days_distance_unit_cost NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN cross_border_cost NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN after_hours_cost NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN start_distance_measure_type NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN start_return_costs NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN distance_cost NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN min_days_cost_free_return NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN min_days_distance_measure_type NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN min_days_return_costs NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE rental_price_list_extra ALTER COLUMN min_days_distance_cost NUMERIC(10, 0)');

    }
}
