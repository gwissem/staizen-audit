<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170614111841 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE step_form_control DROP CONSTRAINT FK_E7D8BBA21C1E601E');
        $this->addSql('DROP TABLE step_form');
        $this->addSql('DROP TABLE step_form_control');
        $this->addSql('ALTER TABLE step_attribute_definition ADD attribute_path NVARCHAR(255)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE step_form (id INT IDENTITY NOT NULL, stepId NVARCHAR(20) COLLATE Polish_CI_AS NOT NULL, config VARCHAR(MAX) COLLATE Polish_CI_AS, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('EXEC sp_addextendedproperty N\'MS_Description\', N\'(DC2Type:json_array)\', N\'SCHEMA\', dbo, N\'TABLE\', step_form, N\'COLUMN\', config');
        $this->addSql('CREATE TABLE step_form_control (id INT IDENTITY NOT NULL, step_form INT, attribute INT, width INT NOT NULL, label NVARCHAR(255) COLLATE Polish_CI_AS, placeholder NVARCHAR(255) COLLATE Polish_CI_AS, form_section INT NOT NULL, orderInSection INT NOT NULL, customStyle NVARCHAR(255) COLLATE Polish_CI_AS, PRIMARY KEY (id))');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_E7D8BBA21C1E601E ON step_form_control (step_form)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_E7D8BBA2FA7AEFFB ON step_form_control (attribute)');
        $this->addSql('ALTER TABLE step_form_control ADD CONSTRAINT DF_E7D8BBA2_8C1A452F DEFAULT 4 FOR width');
        $this->addSql('ALTER TABLE step_form_control ADD CONSTRAINT DF_E7D8BBA2_1C1F1AD4 DEFAULT 0 FOR form_section');
        $this->addSql('ALTER TABLE step_form_control ADD CONSTRAINT DF_E7D8BBA2_E1C328A7 DEFAULT 1 FOR orderInSection');
        $this->addSql('ALTER TABLE step_form_control ADD CONSTRAINT FK_E7D8BBA21C1E601E FOREIGN KEY (step_form) REFERENCES step_form (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE step_form_control ADD CONSTRAINT FK_E7D8BBA2FA7AEFFB FOREIGN KEY (attribute) REFERENCES attribute (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE step_attribute_definition DROP COLUMN attribute_path');
    }
}
