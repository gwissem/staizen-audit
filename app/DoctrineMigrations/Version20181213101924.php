<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181213101924 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.dynamic_translation (id INT IDENTITY NOT NULL, created_by_id INT, updated_by_id INT, translationKey NVARCHAR(255) NOT NULL, locale NVARCHAR(255) NOT NULL, validation NVARCHAR(255), text VARCHAR(MAX), deleted_at DATETIME2(6), PRIMARY KEY (id))');



    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


//        $this->addSql('DROP TABLE AtlasDB_def.dbo.dynamic_translation');


    }
}
