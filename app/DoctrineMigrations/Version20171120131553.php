<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171120131553 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE make_model_dictionary (id INT IDENTITY NOT NULL, make_model_id INT, name NVARCHAR(255) NOT NULL, active BIT, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_14642AB5DBE4A4 ON make_model_dictionary (make_model_id)');
        $this->addSql('CREATE TABLE make_model (id INT IDENTITY NOT NULL, name NVARCHAR(255) NOT NULL, active BIT, programs NVARCHAR(255), dmc INT, type INT, PRIMARY KEY (id))');
        $this->addSql('ALTER TABLE make_model_dictionary ADD CONSTRAINT FK_14642AB5DBE4A4 FOREIGN KEY (make_model_id) REFERENCES make_model (id)');

        $this->addSql("insert into make_model (name, active, programs) select textD, active, descriptionD from dictionary where typeD = 'makeModel' and value <> -1 order by value");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE make_model_dictionary DROP CONSTRAINT FK_14642AB5DBE4A4');
        $this->addSql('DROP TABLE make_model_dictionary');
        $this->addSql('DROP TABLE make_model');

    }
}
