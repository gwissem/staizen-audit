<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190423091904 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE query_log (id INT IDENTITY NOT NULL, hash_id_id INT, created_by_id INT, routing NVARCHAR(100) NOT NULL, created_at DATETIME2(6) NOT NULL, amount INT NOT NULL, min_time NUMERIC(12, 6) NOT NULL, avg_time NUMERIC(12, 6) NOT NULL, max_time NUMERIC(12, 6) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_6DD7BFD9B49AFE8C ON query_log (hash_id_id)');
        $this->addSql('CREATE INDEX IDX_6DD7BFD9B03A8386 ON query_log (created_by_id)');
        $this->addSql('CREATE TABLE query_hash_log (id INT IDENTITY NOT NULL, hash NVARCHAR(100) NOT NULL, query VARCHAR(MAX) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX hash_search_index ON query_hash_log (hash)');
        $this->addSql('ALTER TABLE query_log ADD CONSTRAINT FK_6DD7BFD9B49AFE8C FOREIGN KEY (hash_id_id) REFERENCES query_hash_log (id)');
        $this->addSql('ALTER TABLE query_log ADD CONSTRAINT FK_6DD7BFD9B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE query_log DROP CONSTRAINT FK_6DD7BFD9B49AFE8C');
        $this->addSql('DROP TABLE query_log');
        $this->addSql('DROP TABLE query_hash_log');

    }
}
