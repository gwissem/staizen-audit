<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170418140423 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE process_instance_time (process_instance_id INT NOT NULL, life_time INT NOT NULL, percentage INT NOT NULL, PRIMARY KEY (process_instance_id))');
        $this->addSql('ALTER TABLE process_instance_time ADD CONSTRAINT DF_162A76E1_D73625FB DEFAULT 0 FOR life_time');
        $this->addSql('ALTER TABLE process_instance_time ADD CONSTRAINT DF_162A76E1_930B403E DEFAULT 0 FOR percentage');
        $this->addSql('ALTER TABLE process_instance_time ADD CONSTRAINT FK_162A76E13424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id)');
        $this->addSql('ALTER TABLE fos_user ADD groups_token NVARCHAR(4000)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE process_instance_time');
        $this->addSql('ALTER TABLE fos_user DROP COLUMN groups_token');

    }
}
