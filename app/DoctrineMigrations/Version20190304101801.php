<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190304101801 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE dbo.case_log (
            id int IDENTITY(1,1) NOT NULL,
            name nvarchar(255) NOT NULL,
            created_by_id int NULL,
            created_at datetime2 NULL,
            root_id INT NULL,
            group_id INT NULL,
            inputContent nvarchar(MAX) NULL,
            outputContent nvarchar(MAX) NULL,
            param1 nvarchar(255) NULL,
            param2 nvarchar(255) NULL,
            param3 nvarchar(255) NULL,
            CONSTRAINT PK__id__case_log PRIMARY KEY (id),
            CONSTRAINT FK__created_by_id__case_log FOREIGN KEY (created_by_id) REFERENCES dbo.fos_user(id),
            CONSTRAINT FK__root_id__case_log FOREIGN KEY (root_id) REFERENCES dbo.process_instance(id),
            CONSTRAINT FK__group_id__case_log FOREIGN KEY (group_id) REFERENCES dbo.process_instance(id)
          )

        CREATE INDEX IDX__created_by_id__case_log ON dbo.case_log (created_by_id)
        CREATE INDEX IDX__root_id__case_log ON dbo.case_log (root_id)
        CREATE INDEX IDX__group_id__case_log ON dbo.case_log (group_id)
        
        ');

        $this->addSql('CREATE PROCEDURE [dbo].[p_add_case_log] (@name NVARCHAR(255),
	            @createdById INT=NULL,
                @rootId INT=NULL,
                @groupId INT=NULL,
                @inputContent NVARCHAR(MAX)=NULL,
                @outputContent NVARCHAR(MAX)=NULL,
                @param1 NVARCHAR(255)=NULL,
                @param2 NVARCHAR(255)=NULL,
                @param3 NVARCHAR(255)=NULL
            )
            AS
            BEGIN
            
                INSERT INTO dbo.case_log (name,created_by_id,created_at,root_id,group_id,inputContent,outputContent,param1,param2,param3)
                VALUES (@name, @createdById, GETDATE(), @rootId, @groupId, @inputContent, @outputContent, @param1, @param2, @param3)
            
            END');

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE dbo.case_log');
        $this->addSql('DROP PROCEDURE dbo.p_add_case_log');

    }
}
