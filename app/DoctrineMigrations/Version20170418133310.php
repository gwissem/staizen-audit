<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170418133310 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE process_instance ADD work_time INT');
        $this->addSql('ALTER TABLE process_instance ADD CONSTRAINT DF_B4C1EF5F_9657297D DEFAULT 0 FOR work_time');
        $this->addSql('ALTER TABLE process_instance ALTER COLUMN description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE step ADD reference_time INT');
        $this->addSql('ALTER TABLE step ADD CONSTRAINT DF_43B9FE3C_185760E5 DEFAULT 0 FOR reference_time');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE process_instance DROP COLUMN work_time');
        $this->addSql('ALTER TABLE step DROP COLUMN reference_time');
    }
}
