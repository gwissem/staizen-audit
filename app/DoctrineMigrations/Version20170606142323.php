<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170606142323 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE vip (id INT IDENTITY NOT NULL, created_by_id INT, updated_by_id INT, firstname NVARCHAR(255) NOT NULL, lastname NVARCHAR(255) NOT NULL, phone_number NVARCHAR(255) NOT NULL, active BIT NOT NULL, welcome_message VARCHAR(MAX), supervisor_message VARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_4B076C22B03A8386 ON vip (created_by_id)');
        $this->addSql('CREATE INDEX IDX_4B076C22896DBBDE ON vip (updated_by_id)');
        $this->addSql('ALTER TABLE vip ADD CONSTRAINT DF_4B076C22_4B1EFC02 DEFAULT \'1\' FOR active');
        $this->addSql('ALTER TABLE vip ADD CONSTRAINT FK_4B076C22B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE vip ADD CONSTRAINT FK_4B076C22896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE vip');
    }
}
