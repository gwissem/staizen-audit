<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160921135903 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE tool_summary_user_group (summary_id INT NOT NULL, group_id INT NOT NULL, PRIMARY KEY (summary_id, group_id))');
        $this->addSql('CREATE INDEX IDX_ECA9A5722AC2D45C ON tool_summary_user_group (summary_id)');
        $this->addSql('CREATE INDEX IDX_ECA9A572FE54D947 ON tool_summary_user_group (group_id)');
        $this->addSql('ALTER TABLE tool_summary_user_group ADD CONSTRAINT FK_ECA9A5722AC2D45C FOREIGN KEY (summary_id) REFERENCES tool_summary (id)');
        $this->addSql('ALTER TABLE tool_summary_user_group ADD CONSTRAINT FK_ECA9A572FE54D947 FOREIGN KEY (group_id) REFERENCES user_group (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE tool_summary_user_group');
    }
}
