<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170510122852 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE insurance_data_importer ALTER COLUMN is_incremental BIT NOT NULL');
        $this->addSql('ALTER TABLE step_attribute_definition DROP CONSTRAINT PK__step_att__3213E83FAE705088;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
