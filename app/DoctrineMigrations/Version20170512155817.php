<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170512155817 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE step_form_template (step_id NVARCHAR(20) NOT NULL, form_template_id INT NOT NULL, PRIMARY KEY (step_id, form_template_id))');
        $this->addSql('CREATE INDEX IDX_4EBC46C873B21E9C ON step_form_template (step_id)');
        $this->addSql('CREATE INDEX IDX_4EBC46C8F2B19FA9 ON step_form_template (form_template_id)');
        $this->addSql('CREATE TABLE form_control (id INT IDENTITY NOT NULL, template_form INT, attribute INT, width INT NOT NULL, height INT, position_x INT NOT NULL, position_y INT NOT NULL, label NVARCHAR(255), placeholder NVARCHAR(255), widget NVARCHAR(255), customStyle VARCHAR(MAX), extra_data VARCHAR(MAX), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_DCB74C708746D722 ON form_control (template_form)');
        $this->addSql('CREATE INDEX IDX_DCB74C70FA7AEFFB ON form_control (attribute)');
        $this->addSql('EXEC sp_addextendedproperty N\'MS_Description\', N\'(DC2Type:json_array)\', N\'SCHEMA\', dbo, N\'TABLE\', form_control, N\'COLUMN\', extra_data');
        $this->addSql('ALTER TABLE form_control ADD CONSTRAINT DF_DCB74C70_8C1A452F DEFAULT 4 FOR width');
        $this->addSql('ALTER TABLE form_control ADD CONSTRAINT DF_DCB74C70_EC88DFDF DEFAULT 0 FOR position_x');
        $this->addSql('ALTER TABLE form_control ADD CONSTRAINT DF_DCB74C70_9B8FEF49 DEFAULT 0 FOR position_y');
        $this->addSql('CREATE TABLE attribute_form_template (attribute_id INT NOT NULL, form_template_id INT NOT NULL, PRIMARY KEY (attribute_id, form_template_id))');
        $this->addSql('CREATE INDEX IDX_83D6C1A2B6E62EFA ON attribute_form_template (attribute_id)');
        $this->addSql('CREATE INDEX IDX_83D6C1A2F2B19FA9 ON attribute_form_template (form_template_id)');
        $this->addSql('CREATE TABLE form_template (id INT IDENTITY NOT NULL, config VARCHAR(MAX), PRIMARY KEY (id))');
        $this->addSql('EXEC sp_addextendedproperty N\'MS_Description\', N\'(DC2Type:json_array)\', N\'SCHEMA\', dbo, N\'TABLE\', form_template, N\'COLUMN\', config');
        $this->addSql('ALTER TABLE step_form_template ADD CONSTRAINT FK_4EBC46C873B21E9C FOREIGN KEY (step_id) REFERENCES step (id)');
        $this->addSql('ALTER TABLE step_form_template ADD CONSTRAINT FK_4EBC46C8F2B19FA9 FOREIGN KEY (form_template_id) REFERENCES form_template (id)');
        $this->addSql('ALTER TABLE form_control ADD CONSTRAINT FK_DCB74C708746D722 FOREIGN KEY (template_form) REFERENCES form_template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE form_control ADD CONSTRAINT FK_DCB74C70FA7AEFFB FOREIGN KEY (attribute) REFERENCES attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attribute_form_template ADD CONSTRAINT FK_83D6C1A2B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('ALTER TABLE attribute_form_template ADD CONSTRAINT FK_83D6C1A2F2B19FA9 FOREIGN KEY (form_template_id) REFERENCES form_template (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE step_form_template DROP CONSTRAINT FK_4EBC46C8F2B19FA9');
        $this->addSql('ALTER TABLE form_control DROP CONSTRAINT FK_DCB74C708746D722');
        $this->addSql('ALTER TABLE attribute_form_template DROP CONSTRAINT FK_83D6C1A2F2B19FA9');
        $this->addSql('DROP TABLE step_form_template');
        $this->addSql('DROP TABLE form_control');
        $this->addSql('DROP TABLE attribute_form_template');
        $this->addSql('DROP TABLE form_template');
    }
}
