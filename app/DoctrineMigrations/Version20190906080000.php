<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190906080000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.custom_content ADD web_url NVARCHAR(255)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.custom_content ALTER COLUMN value VARCHAR(MAX)');

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.custom_content DROP COLUMN web_url');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.custom_content ALTER COLUMN value NVARCHAR(255) COLLATE Polish_CI_AS');

    }
}
