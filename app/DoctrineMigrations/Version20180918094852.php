<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180918094852 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.vin_program ADD breakdown_priority INT');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.vin_program ADD accident_priority INT');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.vin_program ADD abroad_towing_limit_eur INT');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.vin_program DROP COLUMN accident_priority');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.vin_program DROP COLUMN breakdown_priority');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.vin_program DROP COLUMN abroad_towing_limit_eur');
    }
}
