<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171011125007 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE attribute_condition ADD visible VARCHAR(MAX)');
        $this->addSql('ALTER TABLE attribute_condition ADD required VARCHAR(MAX)');
        $this->addSql('ALTER TABLE attribute_condition ADD semi_required VARCHAR(MAX)');
        $this->addSql('ALTER TABLE attribute_condition ADD read_only VARCHAR(MAX)');

        $this->addSql("UPDATE attribute_condition SET visible = condition ");
        $this->addSql("UPDATE attribute_condition SET required = 'true' WHERE type = 2");
        $this->addSql("UPDATE attribute_condition SET semi_required = 'true' WHERE type = 3");
        $this->addSql("UPDATE attribute_condition SET read_only = 'true' WHERE type = 4");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('sp_RENAME \'attribute_condition.visible\', \'condition\', \'COLUMN\'');
        $this->addSql('ALTER TABLE attribute_condition DROP COLUMN required');
        $this->addSql('ALTER TABLE attribute_condition DROP COLUMN semi_required');
        $this->addSql('ALTER TABLE attribute_condition DROP COLUMN read_only');
    }
}
