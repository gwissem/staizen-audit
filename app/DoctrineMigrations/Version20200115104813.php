<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200115104813 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("insert into AtlasDB_v.dbo.attribute_value (attribute_id, attribute_path, group_process_instance_id, root_process_instance_id, parent_attribute_value_id )
                    select '1145','595,1145',group_process_instance_id, root_process_instance_id, id
                    from dbo.attribute_value av where attribute_id = '595'");

        $this->addSql("insert into AtlasDB_v.dbo.attribute_value (attribute_id, attribute_path, group_process_instance_id, root_process_instance_id, parent_attribute_value_id )
                    select '1146','595,1146',group_process_instance_id, root_process_instance_id, id
                    from dbo.attribute_value av where attribute_id = '595'");

        $this->addSql("insert into AtlasDB_v.dbo.attribute_value (attribute_id, attribute_path, group_process_instance_id, root_process_instance_id, parent_attribute_value_id )
                    select '64','595,1146,64',group_process_instance_id, root_process_instance_id, id
                    from dbo.attribute_value av where attribute_id = '1146'");

        $this->addSql("insert into AtlasDB_v.dbo.attribute_value (attribute_id, attribute_path, group_process_instance_id, root_process_instance_id, parent_attribute_value_id )
                    select '66','595,1146,66',group_process_instance_id, root_process_instance_id, id
                    from dbo.attribute_value av where attribute_id = '1146'");

        $this->addSql("insert into AtlasDB_v.dbo.attribute_value (attribute_id, attribute_path, group_process_instance_id, root_process_instance_id, parent_attribute_value_id )
                    select '197','595,1146,197',group_process_instance_id, root_process_instance_id, id
                    from dbo.attribute_value av where attribute_id = '1146'");

        $this->addSql("insert into AtlasDB_v.dbo.attribute_value (attribute_id, attribute_path, group_process_instance_id, root_process_instance_id, parent_attribute_value_id )
                    select '368','595,1146,368',group_process_instance_id, root_process_instance_id, id
                    from dbo.attribute_value av where attribute_id = '1146'");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("delete from AtlasDB_v.dbo.attribute_value where attribute_path = '595,1145'");
        $this->addSql("delete from AtlasDB_v.dbo.attribute_value where attribute_path = '595,1146'");
        $this->addSql("delete from AtlasDB_v.dbo.attribute_value where attribute_path = '595,1146,64'");
        $this->addSql("delete from AtlasDB_v.dbo.attribute_value where attribute_path = '595,1146,66'");
        $this->addSql("delete from AtlasDB_v.dbo.attribute_value where attribute_path = '595,1146,197'");
        $this->addSql("delete from AtlasDB_v.dbo.attribute_value where attribute_path = '595,1146,368'");
    }
}
