<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181213103244 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.dynamic_translation ADD created_at DATETIME2(6)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.dynamic_translation ADD updated_at DATETIME2(6)');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.dynamic_translation DROP COLUMN created_at');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.dynamic_translation DROP COLUMN updated_at');




    }
}
