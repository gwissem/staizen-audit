<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161128090107 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE process_flow (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), new_process_id INT, is_primary_path BIT NOT NULL, description NVARCHAR(MAX) NOT NULL, variant INT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_BD29044573B21E9C ON process_flow (step_id)');
        $this->addSql('CREATE INDEX IDX_BD290445A3FD16B8 ON process_flow (new_process_id)');
        $this->addSql(
            'CREATE TABLE case_object (id INT IDENTITY NOT NULL, process_object_definition_id INT, group_process_instance_id INT, company_id INT, created_at DATETIME2(6), updated_at DATETIME2(6), type NVARCHAR(255) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_143EC45C9E7AF79 ON case_object (process_object_definition_id)');
        $this->addSql('CREATE INDEX IDX_143EC45EC062BEE ON case_object (group_process_instance_id)');
        $this->addSql('CREATE INDEX IDX_143EC45979B1AD6 ON case_object (company_id)');
        $this->addSql(
            'CREATE TABLE service (id INT NOT NULL, program_id INT, master_id INT, status INT NOT NULL, price INT NOT NULL, currency NVARCHAR(3) NOT NULL, quantity NVARCHAR(3) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_E19D9AD23EB8070A ON service (program_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD213B3DB11 ON service (master_id)');
        $this->addSql(
            'CREATE TABLE vin_dictionary (id INT IDENTITY NOT NULL, program_id INT, step_object_attr_def_id INT, status INT NOT NULL, type NVARCHAR(255) NOT NULL, is_key BIT NOT NULL, creation_date DATETIME2(6) NOT NULL, description NVARCHAR(255), description_org NVARCHAR(100), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_9C7F4A263EB8070A ON vin_dictionary (program_id)');
        $this->addSql('CREATE INDEX IDX_9C7F4A26FA1FFD53 ON vin_dictionary (step_object_attr_def_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX description_program ON vin_dictionary (description, program_id) WHERE description IS NOT NULL AND program_id IS NOT NULL'
        );
        $this->addSql(
            'CREATE TABLE process_object_definition (id INT IDENTITY NOT NULL, process_definition_id INT, object_definition_id INT, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_2BEFB8F580AA177E ON process_object_definition (process_definition_id)');
        $this->addSql('CREATE INDEX IDX_2BEFB8F522F2763 ON process_object_definition (object_definition_id)');
        $this->addSql(
            'CREATE TABLE location (id INT NOT NULL, longitude DOUBLE PRECISION NOT NULL, latitude DOUBLE PRECISION NOT NULL, country NVARCHAR(255) NOT NULL, postal_code NVARCHAR(255) NOT NULL, city NVARCHAR(255) NOT NULL, street NVARCHAR(255) NOT NULL, unit NVARCHAR(5) NOT NULL, apartment NVARCHAR(5) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE platform (id INT IDENTITY NOT NULL, name NVARCHAR(255) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE vin_program (id INT IDENTITY NOT NULL, platform_id INT, name NVARCHAR(255) NOT NULL, status INT NOT NULL, is_incremental BIT, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_47C41540FFE6496F ON vin_program (platform_id)');
        $this->addSql('ALTER TABLE vin_program ADD CONSTRAINT DF_47C41540_7B00651C DEFAULT 1 FOR status');
        $this->addSql(
            'CREATE TABLE program_service_definition (program_id INT NOT NULL, object_definition_id INT NOT NULL, PRIMARY KEY (program_id, object_definition_id))'
        );
        $this->addSql('CREATE INDEX IDX_E335D3C13EB8070A ON program_service_definition (program_id)');
        $this->addSql('CREATE INDEX IDX_E335D3C122F2763 ON program_service_definition (object_definition_id)');
        $this->addSql(
            'CREATE TABLE process_definition (id INT IDENTITY NOT NULL, name NVARCHAR(255) NOT NULL, roles NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE term (id INT NOT NULL, user_id INT, description NVARCHAR(MAX), reminder DATETIME2(6), [from] NVARCHAR(255), [to] NVARCHAR(255), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_A50FE78DA76ED395 ON term (user_id)');
        $this->addSql(
            'CREATE TABLE attribute (id INT IDENTITY NOT NULL, name NVARCHAR(255) NOT NULL, type INT NOT NULL, regex NVARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('ALTER TABLE attribute ADD CONSTRAINT DF_FA7AEFFB_8CDE5729 DEFAULT 1 FOR type');
        $this->addSql(
            'CREATE TABLE step (id NVARCHAR(20) NOT NULL, process_definition_id INT, holding_step_id INT, name NVARCHAR(255) NOT NULL, query NVARCHAR(MAX) NOT NULL, is_technical BIT NOT NULL, is_full_time_accounted BIT NOT NULL, is_returnable BIT NOT NULL, interval INT NOT NULL, roles NVARCHAR(MAX) NOT NULL, reassign BIT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_43B9FE3C80AA177E ON step (process_definition_id)');
        $this->addSql('CREATE INDEX IDX_43B9FE3C815DD515 ON step (holding_step_id)');
        $this->addSql(
            'CREATE TABLE vehicle (id INT NOT NULL, vin NVARCHAR(17) NOT NULL, registration_number NVARCHAR(10) NOT NULL, brand NVARCHAR(255) NOT NULL, model NVARCHAR(255) NOT NULL, production_year INT NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE platform_phone (id INT IDENTITY NOT NULL, platform_id INT, prefix INT NOT NULL, number INT NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_7613F359FFE6496F ON platform_phone (platform_id)');
        $this->addSql(
            'CREATE TABLE contact_subject (id INT NOT NULL, firstname NVARCHAR(255) NOT NULL, lastname NVARCHAR(255) NOT NULL, identity_number NVARCHAR(255) NOT NULL, phone_number INT NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE cost (id INT IDENTITY NOT NULL, service_id INT, company_id INT, proportion INT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_182694FCED5CA9E6 ON cost (service_id)');
        $this->addSql('CREATE INDEX IDX_182694FC979B1AD6 ON cost (company_id)');
        $this->addSql(
            'CREATE TABLE vin_header (id INT IDENTITY NOT NULL, program_id INT, status INT NOT NULL, query NVARCHAR(MAX), crc BIGINT, creation_date DATETIME2(6) NOT NULL, disable_date DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_275188E33EB8070A ON vin_header (program_id)');
        $this->addSql(
            'CREATE TABLE step_object_attribute_definition (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), attribute_id INT, process_object_definition_id INT, default_value NVARCHAR(MAX) NOT NULL, mandatory INT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_B3CAD54873B21E9C ON step_object_attribute_definition (step_id)');
        $this->addSql('CREATE INDEX IDX_B3CAD548B6E62EFA ON step_object_attribute_definition (attribute_id)');
        $this->addSql(
            'CREATE INDEX IDX_B3CAD548C9E7AF79 ON step_object_attribute_definition (process_object_definition_id)'
        );
        $this->addSql(
            'CREATE TABLE object_definition (id INT IDENTITY NOT NULL, parent_id INT, name NVARCHAR(255) NOT NULL, object_type NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_1BAF8664727ACA70 ON object_definition (parent_id)');
        $this->addSql(
            'CREATE TABLE vin_element (id INT IDENTITY NOT NULL, header_id INT, dictionary_id INT, value NVARCHAR(300) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_94693CFD2EF91FD8 ON vin_element (header_id)');
        $this->addSql('CREATE INDEX IDX_94693CFDAF5E5B3C ON vin_element (dictionary_id)');
        $this->addSql(
            'CREATE TABLE attribute_value (id INT IDENTITY NOT NULL, value_object_id INT, attribute_id INT, object_id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_decimal NUMERIC(2, 0), value_date DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_FE4FBB8266BF539D ON attribute_value (value_object_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82B6E62EFA ON attribute_value (attribute_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82232D562B ON attribute_value (object_id)');
        $this->addSql(
            'CREATE TABLE process_instance (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), user_id INT, company_id INT, parent_id INT, group_process_id INT, date_enter DATETIME2(6) NOT NULL, date_leave DATETIME2(6), date_open DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_B4C1EF5F73B21E9C ON process_instance (step_id)');
        $this->addSql('CREATE INDEX IDX_B4C1EF5FA76ED395 ON process_instance (user_id)');
        $this->addSql('CREATE INDEX IDX_B4C1EF5F979B1AD6 ON process_instance (company_id)');
        $this->addSql('CREATE INDEX IDX_B4C1EF5F727ACA70 ON process_instance (parent_id)');
        $this->addSql('CREATE INDEX IDX_B4C1EF5FA90F94C6 ON process_instance (group_process_id)');
        $this->addSql(
            'CREATE TABLE jms_cron_jobs (id INT IDENTITY NOT NULL, command NVARCHAR(200) NOT NULL, lastRunAt DATETIME2(6) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE UNIQUE INDEX UNIQ_55F5ED428ECAEAD4 ON jms_cron_jobs (command) WHERE command IS NOT NULL');
        $this->addSql(
            'CREATE TABLE jms_jobs (id BIGINT IDENTITY NOT NULL, state NVARCHAR(15) NOT NULL, queue NVARCHAR(50) NOT NULL, priority SMALLINT NOT NULL, createdAt DATETIME2(6) NOT NULL, startedAt DATETIME2(6), checkedAt DATETIME2(6), workerName NVARCHAR(50), executeAfter DATETIME2(6), closedAt DATETIME2(6), command NVARCHAR(255) NOT NULL, args NVARCHAR(MAX) NOT NULL, output NVARCHAR(MAX), errorOutput NVARCHAR(MAX), exitCode SMALLINT, maxRuntime SMALLINT NOT NULL, maxRetries SMALLINT NOT NULL, stackTrace NVARCHAR(MAX), runtime SMALLINT, memoryUsage INT, memoryUsageReal INT, originalJob_id BIGINT, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_704ADB9349C447F1 ON jms_jobs (originalJob_id)');
        $this->addSql('CREATE INDEX cmd_search_index ON jms_jobs (command)');
        $this->addSql('CREATE INDEX sorting_index ON jms_jobs (state, priority, id)');
        $this->addSql(
            'EXEC sp_addextendedproperty N\'MS_Description\', N\'(DC2Type:json_array)\', N\'SCHEMA\', dbo, N\'TABLE\', jms_jobs, N\'COLUMN\', args'
        );
        $this->addSql(
            'EXEC sp_addextendedproperty N\'MS_Description\', N\'(DC2Type:jms_job_safe_object)\', N\'SCHEMA\', dbo, N\'TABLE\', jms_jobs, N\'COLUMN\', stackTrace'
        );
        $this->addSql(
            'CREATE TABLE jms_job_dependencies (source_job_id BIGINT NOT NULL, dest_job_id BIGINT NOT NULL, PRIMARY KEY (source_job_id, dest_job_id))'
        );
        $this->addSql('CREATE INDEX IDX_8DCFE92CBD1F6B4F ON jms_job_dependencies (source_job_id)');
        $this->addSql('CREATE INDEX IDX_8DCFE92C32CF8D4C ON jms_job_dependencies (dest_job_id)');
        $this->addSql(
            'CREATE TABLE jms_job_related_entities (job_id BIGINT NOT NULL, related_class NVARCHAR(150) NOT NULL, related_id NVARCHAR(100) NOT NULL, PRIMARY KEY (job_id, related_class, related_id))'
        );
        $this->addSql('CREATE INDEX IDX_E956F4E2BE04EA9 ON jms_job_related_entities (job_id)');
        $this->addSql(
            'CREATE TABLE jms_job_statistics (job_id BIGINT NOT NULL, characteristic NVARCHAR(30) NOT NULL, createdAt DATETIME2(6) NOT NULL, charValue DOUBLE PRECISION NOT NULL, PRIMARY KEY (job_id, characteristic, createdAt))'
        );
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD29044573B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD290445A3FD16B8 FOREIGN KEY (new_process_id) REFERENCES process_instance (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE case_object ADD CONSTRAINT FK_143EC45C9E7AF79 FOREIGN KEY (process_object_definition_id) REFERENCES process_object_definition (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE case_object ADD CONSTRAINT FK_143EC45EC062BEE FOREIGN KEY (group_process_instance_id) REFERENCES process_instance (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE case_object ADD CONSTRAINT FK_143EC45979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)'
        );
        $this->addSql(
            'ALTER TABLE service ADD CONSTRAINT FK_E19D9AD23EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE service ADD CONSTRAINT FK_E19D9AD213B3DB11 FOREIGN KEY (master_id) REFERENCES service (id)'
        );
        $this->addSql(
            'ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2BF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE vin_dictionary ADD CONSTRAINT FK_9C7F4A263EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE vin_dictionary ADD CONSTRAINT FK_9C7F4A26FA1FFD53 FOREIGN KEY (step_object_attr_def_id) REFERENCES step_object_attribute_definition (id)'
        );
        $this->addSql(
            'ALTER TABLE process_object_definition ADD CONSTRAINT FK_2BEFB8F580AA177E FOREIGN KEY (process_definition_id) REFERENCES process_definition (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_object_definition ADD CONSTRAINT FK_2BEFB8F522F2763 FOREIGN KEY (object_definition_id) REFERENCES object_definition (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBBF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE vin_program ADD CONSTRAINT FK_47C41540FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE program_service_definition ADD CONSTRAINT FK_E335D3C13EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id)'
        );
        $this->addSql(
            'ALTER TABLE program_service_definition ADD CONSTRAINT FK_E335D3C122F2763 FOREIGN KEY (object_definition_id) REFERENCES object_definition (id)'
        );
        $this->addSql(
            'ALTER TABLE term ADD CONSTRAINT FK_A50FE78DA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE term ADD CONSTRAINT FK_A50FE78DBF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE step ADD CONSTRAINT FK_43B9FE3C80AA177E FOREIGN KEY (process_definition_id) REFERENCES process_definition (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE step ADD CONSTRAINT FK_43B9FE3C815DD515 FOREIGN KEY (holding_step_id) REFERENCES object_definition (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486BF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE platform_phone ADD CONSTRAINT FK_7613F359FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE contact_subject ADD CONSTRAINT FK_61E11FFBBF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE cost ADD CONSTRAINT FK_182694FCED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE cost ADD CONSTRAINT FK_182694FC979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)'
        );
        $this->addSql(
            'ALTER TABLE vin_header ADD CONSTRAINT FK_275188E33EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD54873B21E9C FOREIGN KEY (step_id) REFERENCES step (id)'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD548B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD548C9E7AF79 FOREIGN KEY (process_object_definition_id) REFERENCES process_object_definition (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE object_definition ADD CONSTRAINT FK_1BAF8664727ACA70 FOREIGN KEY (parent_id) REFERENCES object_definition (id)'
        );
        $this->addSql(
            'ALTER TABLE vin_element ADD CONSTRAINT FK_94693CFD2EF91FD8 FOREIGN KEY (header_id) REFERENCES vin_header (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE vin_element ADD CONSTRAINT FK_94693CFDAF5E5B3C FOREIGN KEY (dictionary_id) REFERENCES vin_dictionary (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB8266BF539D FOREIGN KEY (value_object_id) REFERENCES case_object (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82232D562B FOREIGN KEY (object_id) REFERENCES case_object (id)'
        );
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5F73B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5FA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5F979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5F727ACA70 FOREIGN KEY (parent_id) REFERENCES process_instance (id)'
        );
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5FA90F94C6 FOREIGN KEY (group_process_id) REFERENCES process_instance (id)'
        );
        $this->addSql(
            'ALTER TABLE jms_jobs ADD CONSTRAINT FK_704ADB9349C447F1 FOREIGN KEY (originalJob_id) REFERENCES jms_jobs (id)'
        );
        $this->addSql(
            'ALTER TABLE jms_job_dependencies ADD CONSTRAINT FK_8DCFE92CBD1F6B4F FOREIGN KEY (source_job_id) REFERENCES jms_jobs (id)'
        );
        $this->addSql(
            'ALTER TABLE jms_job_dependencies ADD CONSTRAINT FK_8DCFE92C32CF8D4C FOREIGN KEY (dest_job_id) REFERENCES jms_jobs (id)'
        );
        $this->addSql(
            'ALTER TABLE jms_job_related_entities ADD CONSTRAINT FK_E956F4E2BE04EA9 FOREIGN KEY (job_id) REFERENCES jms_jobs (id)'
        );
        $this->addSql(
            'CREATE TABLE object_definition_attributes (object_definition_id INT NOT NULL, attribute_id INT NOT NULL, PRIMARY KEY (object_definition_id, attribute_id))'
        );
        $this->addSql('CREATE INDEX IDX_430A92B422F2763 ON object_definition_attributes (object_definition_id)');
        $this->addSql('CREATE INDEX IDX_430A92B4B6E62EFA ON object_definition_attributes (attribute_id)');
        $this->addSql(
            'ALTER TABLE object_definition_attributes ADD CONSTRAINT FK_430A92B422F2763 FOREIGN KEY (object_definition_id) REFERENCES object_definition (id)'
        );
        $this->addSql(
            'ALTER TABLE object_definition_attributes ADD CONSTRAINT FK_430A92B4B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD2BF396750');
        $this->addSql('ALTER TABLE location DROP CONSTRAINT FK_5E9E89CBBF396750');
        $this->addSql('ALTER TABLE term DROP CONSTRAINT FK_A50FE78DBF396750');
        $this->addSql('ALTER TABLE vehicle DROP CONSTRAINT FK_1B80E486BF396750');
        $this->addSql('ALTER TABLE contact_subject DROP CONSTRAINT FK_61E11FFBBF396750');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB8266BF539D');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB82232D562B');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD213B3DB11');
        $this->addSql('ALTER TABLE cost DROP CONSTRAINT FK_182694FCED5CA9E6');
        $this->addSql('ALTER TABLE vin_element DROP CONSTRAINT FK_94693CFDAF5E5B3C');
        $this->addSql('ALTER TABLE case_object DROP CONSTRAINT FK_143EC45C9E7AF79');
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP CONSTRAINT FK_B3CAD548C9E7AF79');
        $this->addSql('ALTER TABLE vin_program DROP CONSTRAINT FK_47C41540FFE6496F');
        $this->addSql('ALTER TABLE platform_phone DROP CONSTRAINT FK_7613F359FFE6496F');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD23EB8070A');
        $this->addSql('ALTER TABLE vin_dictionary DROP CONSTRAINT FK_9C7F4A263EB8070A');
        $this->addSql('ALTER TABLE program_service_definition DROP CONSTRAINT FK_E335D3C13EB8070A');
        $this->addSql('ALTER TABLE vin_header DROP CONSTRAINT FK_275188E33EB8070A');
        $this->addSql('ALTER TABLE process_object_definition DROP CONSTRAINT FK_2BEFB8F580AA177E');
        $this->addSql('ALTER TABLE step DROP CONSTRAINT FK_43B9FE3C80AA177E');
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP CONSTRAINT FK_B3CAD548B6E62EFA');
        $this->addSql('ALTER TABLE object_definition_attributes DROP CONSTRAINT FK_430A92B4B6E62EFA');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB82B6E62EFA');
        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD29044573B21E9C');
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP CONSTRAINT FK_B3CAD54873B21E9C');
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5F73B21E9C');
        $this->addSql('ALTER TABLE vin_element DROP CONSTRAINT FK_94693CFD2EF91FD8');
        $this->addSql('ALTER TABLE vin_dictionary DROP CONSTRAINT FK_9C7F4A26FA1FFD53');
        $this->addSql('ALTER TABLE process_object_definition DROP CONSTRAINT FK_2BEFB8F522F2763');
        $this->addSql('ALTER TABLE program_service_definition DROP CONSTRAINT FK_E335D3C122F2763');
        $this->addSql('ALTER TABLE step DROP CONSTRAINT FK_43B9FE3C815DD515');
        $this->addSql('ALTER TABLE object_definition DROP CONSTRAINT FK_1BAF8664727ACA70');
        $this->addSql('ALTER TABLE object_definition_attributes DROP CONSTRAINT FK_430A92B422F2763');
        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD290445A3FD16B8');
        $this->addSql('ALTER TABLE case_object DROP CONSTRAINT FK_143EC45EC062BEE');
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5F727ACA70');
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5FA90F94C6');
        $this->addSql('ALTER TABLE jms_jobs DROP CONSTRAINT FK_704ADB9349C447F1');
        $this->addSql('ALTER TABLE jms_job_dependencies DROP CONSTRAINT FK_8DCFE92CBD1F6B4F');
        $this->addSql('ALTER TABLE jms_job_dependencies DROP CONSTRAINT FK_8DCFE92C32CF8D4C');
        $this->addSql('ALTER TABLE jms_job_related_entities DROP CONSTRAINT FK_E956F4E2BE04EA9');
        $this->addSql('DROP TABLE process_flow');
        $this->addSql('DROP TABLE case_object');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE vin_dictionary');
        $this->addSql('DROP TABLE process_object_definition');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE platform');
        $this->addSql('DROP TABLE vin_program');
        $this->addSql('DROP TABLE program_service_definition');
        $this->addSql('DROP TABLE process_definition');
        $this->addSql('DROP TABLE term');
        $this->addSql('DROP TABLE attribute');
        $this->addSql('DROP TABLE step');
        $this->addSql('DROP TABLE vehicle');
        $this->addSql('DROP TABLE platform_phone');
        $this->addSql('DROP TABLE contact_subject');
        $this->addSql('DROP TABLE cost');
        $this->addSql('DROP TABLE vin_header');
        $this->addSql('DROP TABLE step_object_attribute_definition');
        $this->addSql('DROP TABLE object_definition');
        $this->addSql('DROP TABLE vin_element');
        $this->addSql('DROP TABLE attribute_value');
        $this->addSql('DROP TABLE process_instance');
        $this->addSql('DROP TABLE jms_cron_jobs');
        $this->addSql('DROP TABLE jms_jobs');
        $this->addSql('DROP TABLE jms_job_dependencies');
        $this->addSql('DROP TABLE jms_job_related_entities');
        $this->addSql('DROP TABLE jms_job_statistics');
        $this->addSql('DROP TABLE object_definition_attributes');

    }
}
