<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190528074422 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.help_case (
//            id int IDENTITY(1,1) NOT NULL,
//            name NVARCHAR(255) NOT NULL,
//            diagnosis_summary SMALLINT DEFAULT 0 NULL,
//            info_nh SMALLINT DEFAULT 0 NULL,
//            info_mw SMALLINT DEFAULT 0 NULL,
//            info_transport SMALLINT DEFAULT 0 NULL,
//            mw_forms_003_030 SMALLINT DEFAULT 0 NULL,
//            updated_at DATETIME DEFAULT GETDATE() NULL,
//            note NVARCHAR(MAX) NULL,
//            CONSTRAINT PK__help_case_primary_id PRIMARY KEY (id),
//        )');

//        $this->addSql("CREATE SYNONYM dbo.help_case for AtlasDB_def.dbo.help_case");


    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('DROP TABLE AtlasDB_def.dbo.help_case');

    }
}
