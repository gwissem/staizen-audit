<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180809124748 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config_definition_translation ADD description NVARCHAR(MAX)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config ADD description NVARCHAR(MAX)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config_definition_translation DROP COLUMN description');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config_definition DROP COLUMN description');
    }
}
