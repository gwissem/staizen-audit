<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180814104948 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ALTER COLUMN text VARCHAR(MAX)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ALTER COLUMN locale NVARCHAR(255)');;
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ALTER COLUMN auto_save INT');



          }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ALTER COLUMN text VARCHAR(MAX) COLLATE Polish_CI_AS NOT NULL');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ALTER COLUMN locale NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ALTER COLUMN auto_save INT NOT NULL');

    }
}
