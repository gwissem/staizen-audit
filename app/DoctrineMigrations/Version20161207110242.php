<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161207110242 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_flow ADD created_by_id INT');
        $this->addSql('ALTER TABLE process_flow ADD updated_by_id INT');
        $this->addSql('ALTER TABLE process_flow ADD deleted_at DATETIME2(6)');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN graph_points NVARCHAR(MAX)');
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD290445B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD290445896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_BD290445B03A8386 ON process_flow (created_by_id)');
        $this->addSql('CREATE INDEX IDX_BD290445896DBBDE ON process_flow (updated_by_id)');
        $this->addSql('ALTER TABLE process_object_definition ADD created_by_id INT');
        $this->addSql('ALTER TABLE process_object_definition ADD updated_by_id INT');
        $this->addSql('ALTER TABLE process_object_definition ADD deleted_at DATETIME2(6)');
        $this->addSql(
            'ALTER TABLE process_object_definition ADD CONSTRAINT FK_2BEFB8F5B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE process_object_definition ADD CONSTRAINT FK_2BEFB8F5896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_2BEFB8F5B03A8386 ON process_object_definition (created_by_id)');
        $this->addSql('CREATE INDEX IDX_2BEFB8F5896DBBDE ON process_object_definition (updated_by_id)');
        $this->addSql('ALTER TABLE process_definition ADD created_by_id INT');
        $this->addSql('ALTER TABLE process_definition ADD updated_by_id INT');
        $this->addSql('ALTER TABLE process_definition ADD deleted_at DATETIME2(6)');
        $this->addSql(
            'ALTER TABLE process_definition ADD CONSTRAINT FK_C67B0CD0B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE process_definition ADD CONSTRAINT FK_C67B0CD0896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_C67B0CD0B03A8386 ON process_definition (created_by_id)');
        $this->addSql('CREATE INDEX IDX_C67B0CD0896DBBDE ON process_definition (updated_by_id)');
        $this->addSql('ALTER TABLE term ALTER COLUMN description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE attribute ALTER COLUMN regex NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE step ADD created_by_id INT');
        $this->addSql('ALTER TABLE step ADD updated_by_id INT');
        $this->addSql('ALTER TABLE step ADD deleted_at DATETIME2(6)');
        $this->addSql('ALTER TABLE step ALTER COLUMN query NVARCHAR(MAX)');
        $this->addSql(
            'ALTER TABLE step ADD CONSTRAINT FK_43B9FE3CB03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE step ADD CONSTRAINT FK_43B9FE3C896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_43B9FE3CB03A8386 ON step (created_by_id)');
        $this->addSql('CREATE INDEX IDX_43B9FE3C896DBBDE ON step (updated_by_id)');
        $this->addSql('ALTER TABLE step_object_attribute_definition ADD created_by_id INT');
        $this->addSql('ALTER TABLE step_object_attribute_definition ADD updated_by_id INT');
        $this->addSql('ALTER TABLE step_object_attribute_definition ADD deleted_at DATETIME2(6)');
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD548B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD548896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_B3CAD548B03A8386 ON step_object_attribute_definition (created_by_id)');
        $this->addSql('CREATE INDEX IDX_B3CAD548896DBBDE ON step_object_attribute_definition (updated_by_id)');
        $this->addSql('ALTER TABLE object_definition ADD created_by_id INT');
        $this->addSql('ALTER TABLE object_definition ADD updated_by_id INT');
        $this->addSql('ALTER TABLE object_definition ADD deleted_at DATETIME2(6)');
        $this->addSql(
            'ALTER TABLE object_definition ADD CONSTRAINT FK_1BAF8664B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE object_definition ADD CONSTRAINT FK_1BAF8664896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_1BAF8664B03A8386 ON object_definition (created_by_id)');
        $this->addSql('CREATE INDEX IDX_1BAF8664896DBBDE ON object_definition (updated_by_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE object_definition DROP CONSTRAINT FK_1BAF8664B03A8386');
        $this->addSql('ALTER TABLE object_definition DROP CONSTRAINT FK_1BAF8664896DBBDE');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_1BAF8664B03A8386\')
                            ALTER TABLE object_definition DROP CONSTRAINT IDX_1BAF8664B03A8386
                        ELSE
                            DROP INDEX IDX_1BAF8664B03A8386 ON object_definition'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_1BAF8664896DBBDE\')
                            ALTER TABLE object_definition DROP CONSTRAINT IDX_1BAF8664896DBBDE
                        ELSE
                            DROP INDEX IDX_1BAF8664896DBBDE ON object_definition'
        );
        $this->addSql('ALTER TABLE object_definition DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE object_definition DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE object_definition DROP COLUMN deleted_at');
        $this->addSql('ALTER TABLE process_definition DROP CONSTRAINT FK_C67B0CD0B03A8386');
        $this->addSql('ALTER TABLE process_definition DROP CONSTRAINT FK_C67B0CD0896DBBDE');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_C67B0CD0B03A8386\')
                            ALTER TABLE process_definition DROP CONSTRAINT IDX_C67B0CD0B03A8386
                        ELSE
                            DROP INDEX IDX_C67B0CD0B03A8386 ON process_definition'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_C67B0CD0896DBBDE\')
                            ALTER TABLE process_definition DROP CONSTRAINT IDX_C67B0CD0896DBBDE
                        ELSE
                            DROP INDEX IDX_C67B0CD0896DBBDE ON process_definition'
        );
        $this->addSql('ALTER TABLE process_definition DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE process_definition DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE process_definition DROP COLUMN deleted_at');
        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD290445B03A8386');
        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD290445896DBBDE');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_BD290445B03A8386\')
                            ALTER TABLE process_flow DROP CONSTRAINT IDX_BD290445B03A8386
                        ELSE
                            DROP INDEX IDX_BD290445B03A8386 ON process_flow'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_BD290445896DBBDE\')
                            ALTER TABLE process_flow DROP CONSTRAINT IDX_BD290445896DBBDE
                        ELSE
                            DROP INDEX IDX_BD290445896DBBDE ON process_flow'
        );
        $this->addSql('ALTER TABLE process_flow DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE process_flow DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE process_flow DROP COLUMN deleted_at');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN graph_points NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE process_object_definition DROP CONSTRAINT FK_2BEFB8F5B03A8386');
        $this->addSql('ALTER TABLE process_object_definition DROP CONSTRAINT FK_2BEFB8F5896DBBDE');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_2BEFB8F5B03A8386\')
                            ALTER TABLE process_object_definition DROP CONSTRAINT IDX_2BEFB8F5B03A8386
                        ELSE
                            DROP INDEX IDX_2BEFB8F5B03A8386 ON process_object_definition'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_2BEFB8F5896DBBDE\')
                            ALTER TABLE process_object_definition DROP CONSTRAINT IDX_2BEFB8F5896DBBDE
                        ELSE
                            DROP INDEX IDX_2BEFB8F5896DBBDE ON process_object_definition'
        );
        $this->addSql('ALTER TABLE process_object_definition DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE process_object_definition DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE process_object_definition DROP COLUMN deleted_at');
        $this->addSql('ALTER TABLE step DROP CONSTRAINT FK_43B9FE3CB03A8386');
        $this->addSql('ALTER TABLE step DROP CONSTRAINT FK_43B9FE3C896DBBDE');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_43B9FE3CB03A8386\')
                            ALTER TABLE step DROP CONSTRAINT IDX_43B9FE3CB03A8386
                        ELSE
                            DROP INDEX IDX_43B9FE3CB03A8386 ON step'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_43B9FE3C896DBBDE\')
                            ALTER TABLE step DROP CONSTRAINT IDX_43B9FE3C896DBBDE
                        ELSE
                            DROP INDEX IDX_43B9FE3C896DBBDE ON step'
        );
        $this->addSql('ALTER TABLE step DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE step DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE step DROP COLUMN deleted_at');
        $this->addSql('ALTER TABLE step ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP CONSTRAINT FK_B3CAD548B03A8386');
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP CONSTRAINT FK_B3CAD548896DBBDE');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B3CAD548B03A8386\')
                            ALTER TABLE step_object_attribute_definition DROP CONSTRAINT IDX_B3CAD548B03A8386
                        ELSE
                            DROP INDEX IDX_B3CAD548B03A8386 ON step_object_attribute_definition'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B3CAD548896DBBDE\')
                            ALTER TABLE step_object_attribute_definition DROP CONSTRAINT IDX_B3CAD548896DBBDE
                        ELSE
                            DROP INDEX IDX_B3CAD548896DBBDE ON step_object_attribute_definition'
        );
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP COLUMN deleted_at');

    }
}
