<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170216093735 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (1, 'attributeType', 'string')");
        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (2, 'attributeType', 'integer')");
        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (3, 'attributeType', 'datetime')");
        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (4, 'attributeType', 'text')");
        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (5, 'attributeType', 'decimal')");
        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (6, 'attributeType', 'boolean')");
        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (7, 'attributeType', 'map')");
        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (8, 'attributeType', 'company')");
        $this->addSql("INSERT INTO dictionary (value, typeD, textD) VALUES (9, 'attributeType', 'multi')");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql("DELETE FROM dictionary WHERE typeD = 'attributeType'");
    }
}
