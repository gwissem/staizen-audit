<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161214160003 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE step_waiting_for_step (step_id NVARCHAR(20) NOT NULL, waiting_for_step_id NVARCHAR(20) NOT NULL, PRIMARY KEY (step_id, waiting_for_step_id))'
        );
        $this->addSql('CREATE INDEX IDX_DE38488B73B21E9C ON step_waiting_for_step (step_id)');
        $this->addSql('CREATE INDEX IDX_DE38488B9300E8ED ON step_waiting_for_step (waiting_for_step_id)');
        $this->addSql(
            'ALTER TABLE step_waiting_for_step ADD CONSTRAINT FK_DE38488B73B21E9C FOREIGN KEY (step_id) REFERENCES step (id)'
        );
        $this->addSql(
            'ALTER TABLE step_waiting_for_step ADD CONSTRAINT FK_DE38488B9300E8ED FOREIGN KEY (waiting_for_step_id) REFERENCES step (id)'
        );

        $this->addSql('ALTER TABLE step DROP CONSTRAINT FK_43B9FE3C815DD515');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_43B9FE3C815DD515\')
                            ALTER TABLE step DROP CONSTRAINT IDX_43B9FE3C815DD515
                        ELSE
                            DROP INDEX IDX_43B9FE3C815DD515 ON step'
        );
        $this->addSql('ALTER TABLE step DROP COLUMN holding_step_id');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('DROP TABLE step_waiting_for_step');
        $this->addSql('ALTER TABLE step ADD holding_step_id INT');
        $this->addSql('ALTER TABLE step ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE step ADD CONSTRAINT FK_43B9FE3C815DD515 FOREIGN KEY (holding_step_id) REFERENCES object_definition (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_43B9FE3C815DD515 ON step (holding_step_id)');

    }
}
