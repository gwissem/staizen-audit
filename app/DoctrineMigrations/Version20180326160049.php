<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180326160049 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE rental_price_list (id INT IDENTITY NOT NULL, group_id INT NOT NULL, name NVARCHAR(255), type INT NOT NULL, class INT NOT NULL, gearbox INT, features NVARCHAR(255), daily_cost NUMERIC(10, 0) NOT NULL, day_from INT NOT NULL, day_to INT, cross_border_cost NUMERIC(10, 0), extra_hours_from TIME(0), extra_hours_to TIME(0), extra_hours_saturday_from TIME(0), extra_hours_saturday_to TIME(0), after_hours_cost NUMERIC(10, 0), start_distance_measure_type INT, start_return_costs NUMERIC(10, 0), distance_unit_cost NUMERIC(10, 0), min_days_cost_free_return NUMERIC(10, 0), min_days_distance_measure_type INT, min_days_return_costs NUMERIC(10, 0), min_days_distance_unit_cost NUMERIC(10, 0), PRIMARY KEY (id))');
        $this->addSql('CREATE TABLE rental_price_list_usage (id INT IDENTITY NOT NULL, price_list_group_id INT, nip NVARCHAR(255), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_3B781C35E3CB5592 ON rental_price_list_usage (price_list_group_id)');
        $this->addSql('CREATE TABLE rental_price_list_extra (id INT IDENTITY NOT NULL, price_list_group_id INT, cross_border_cost NUMERIC(10, 0), extra_hours_from TIME(0), extra_hours_to TIME(0), extra_hours_saturday_from TIME(0), extra_hours_saturday_to TIME(0), after_hours_cost NUMERIC(10, 0), start_distance_measure_type NUMERIC(10, 0), start_return_costs NUMERIC(10, 0), distance_cost NUMERIC(10, 0), min_days_cost_free_return NUMERIC(10, 0), min_days_distance_measure_type NUMERIC(10, 0), min_days_return_costs NUMERIC(10, 0), min_days_distance_cost NUMERIC(10, 0), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_A6AC4F20E3CB5592 ON rental_price_list_extra (price_list_group_id)');
        $this->addSql('ALTER TABLE rental_price_list_usage ADD CONSTRAINT FK_3B781C35E3CB5592 FOREIGN KEY (price_list_group_id) REFERENCES rental_price_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rental_price_list_extra ADD CONSTRAINT FK_A6AC4F20E3CB5592 FOREIGN KEY (price_list_group_id) REFERENCES rental_price_list (id) ON DELETE CASCADE');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE rental_price_list');
        $this->addSql('DROP TABLE rental_price_list_usage');
        $this->addSql('DROP TABLE rental_price_list_extra');

    }
}
