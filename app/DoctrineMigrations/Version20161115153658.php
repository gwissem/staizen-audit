<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161115153658 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE tool_xls_hash (hash NVARCHAR(32) NOT NULL, tool_id INT, url NVARCHAR(MAX) NOT NULL, filters NVARCHAR(MAX) NULL, locale NVARCHAR(2) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (hash))'
        );
        $this->addSql('CREATE INDEX IDX_7E499D2B8F7B22CC ON tool_xls_hash (tool_id)');
        $this->addSql(
            'ALTER TABLE tool_xls_hash ADD CONSTRAINT FK_7E499D2B8F7B22CC FOREIGN KEY (tool_id) REFERENCES tool (id)'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


        $this->addSql('DROP TABLE tool_xls_hash');

    }
}
