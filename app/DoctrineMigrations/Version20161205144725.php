<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205144725 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD290445B13C343E');
        $this->addSql('ALTER TABLE process_flow ADD curviness NVARCHAR(255)');
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD290445B13C343E FOREIGN KEY (next_step_id) REFERENCES step (id)'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD290445B13C343E');
        $this->addSql('ALTER TABLE process_flow DROP COLUMN curviness');
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD290445B13C343E FOREIGN KEY (next_step_id) REFERENCES step (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );

    }
}
