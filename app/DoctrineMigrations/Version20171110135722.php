<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171110135722 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE service_definition ADD start_step_id NVARCHAR(20)');
        $this->addSql('CREATE INDEX IDX_FE61166C8377424F ON service_definition (start_step_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE service_definition DROP CONSTRAINT FK_FE61166C8377424F');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE61166C8377424F\')
                            ALTER TABLE service_definition DROP CONSTRAINT IDX_FE61166C8377424F
                        ELSE
                            DROP INDEX IDX_FE61166C8377424F ON service_definition');
        $this->addSql('ALTER TABLE service_definition DROP COLUMN start_step_id');

    }
}
