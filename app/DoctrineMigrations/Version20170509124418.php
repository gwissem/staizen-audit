<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170509124418 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('DROP INDEX tool_group_translation_unique_translation ON tool_group_translation');
        $this->addSql('DROP INDEX tool_action_translation_unique_translation ON tool_action_translation');
        $this->addSql('DROP INDEX tool_summary_translation_unique_translation ON tool_summary_translation');
        $this->addSql('DROP INDEX filter_translation_unique_translation ON filter_translation');
        $this->addSql('DROP INDEX tool_translation_unique_translation ON tool_translation');
        $this->addSql('DROP INDEX style_translation_unique_translation ON style_translation');
        $this->addSql('DROP INDEX tool_form_field_translation_unique_translation ON tool_form_field_translation');
        $this->addSql('ALTER TABLE tool_group_translation ALTER COLUMN translatable_id INT');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN translatable_id INT');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN translatable_id INT');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN translatable_id INT');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN translatable_id INT');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN translatable_id INT');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN translatable_id INT');
        $this->addSql('CREATE UNIQUE INDEX tool_group_translation_unique_translation ON tool_group_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX tool_action_translation_unique_translation ON tool_action_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX tool_summary_translation_unique_translation ON tool_summary_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX filter_translation_unique_translation ON filter_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX tool_translation_unique_translation ON tool_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX style_translation_unique_translation ON style_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX tool_form_field_translation_unique_translation ON tool_form_field_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }
}
