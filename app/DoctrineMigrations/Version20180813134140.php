<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180813134140 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//         $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ADD program_id INT');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ADD CONSTRAINT FK_851FAC063EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON DELETE SET NULL');
//        $this->addSql('CREATE INDEX IDX_851FAC063EB8070A ON AtlasDB_def.dbo.step_info (program_id)');
        }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info DROP CONSTRAINT FK_851FAC063EB8070A');
//        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_851FAC063EB8070A\')
//                            ALTER TABLE step_info DROP CONSTRAINT IDX_851FAC063EB8070A
//                        ELSE
//                            DROP INDEX IDX_851FAC063EB8070A ON step_info');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info DROP COLUMN program_id');
        }
}
