<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180328212940 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN cross_border_cost');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN extra_hours_from');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN extra_hours_to');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN extra_hours_saturday_from');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN extra_hours_saturday_to');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN after_hours_cost');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN start_distance_measure_type');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN start_return_costs');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN distance_unit_cost');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN min_days_cost_free_return');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN min_days_distance_measure_type');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN min_days_return_costs');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN min_days_distance_unit_cost');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE rental_price_list ADD cross_border_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ADD extra_hours_from TIME(0)');
        $this->addSql('ALTER TABLE rental_price_list ADD extra_hours_to TIME(0)');
        $this->addSql('ALTER TABLE rental_price_list ADD extra_hours_saturday_from TIME(0)');
        $this->addSql('ALTER TABLE rental_price_list ADD extra_hours_saturday_to TIME(0)');
        $this->addSql('ALTER TABLE rental_price_list ADD after_hours_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ADD start_distance_measure_type INT');
        $this->addSql('ALTER TABLE rental_price_list ADD start_return_costs NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ADD distance_unit_cost NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ADD min_days_cost_free_return NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ADD min_days_distance_measure_type INT');
        $this->addSql('ALTER TABLE rental_price_list ADD min_days_return_costs NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE rental_price_list ADD min_days_distance_unit_cost NUMERIC(10, 2)');

    }
}
