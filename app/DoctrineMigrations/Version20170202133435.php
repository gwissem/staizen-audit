<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170202133435 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


        $this->addSql('ALTER TABLE process_instance ADD created_by_original_id INT');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5FB0B2CEF2');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B4C1EF5FB0B2CEF2\')
                            ALTER TABLE process_instance DROP CONSTRAINT IDX_B4C1EF5FB0B2CEF2
                        ELSE
                            DROP INDEX IDX_B4C1EF5FB0B2CEF2 ON process_instance'
        );
        $this->addSql('ALTER TABLE process_instance DROP COLUMN created_by_original_id');

    }
}
