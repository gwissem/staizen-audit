<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170216093734 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE dictionary(
            [id] [int] IDENTITY(1,1),
            [typeD] [varchar](50) NOT NULL,
            [value] [int] NOT NULL,
            [textD] [varchar](200) NOT NULL,
            [active] [tinyint] NOT NULL CONSTRAINT [DF_dictionary_active]  DEFAULT ((1)),
            [modDate] [datetime] NOT NULL CONSTRAINT [DF_dictionary_modDate]  DEFAULT (getdate()),
            [orderBy] [int] NULL,
         CONSTRAINT [PK_dictionary] PRIMARY KEY CLUSTERED 
        (
            [typeD] ASC,
            [value] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


    }
}
