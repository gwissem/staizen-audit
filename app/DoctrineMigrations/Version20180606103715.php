<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180606103715 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');
        $this->addSql('ALTER TABLE post ADD created_by_id INT');
        $this->addSql('ALTER TABLE post ADD updated_by_id INT');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8DB03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_5A8A6C8DB03A8386 ON post (created_by_id)');
        $this->addSql('CREATE INDEX IDX_5A8A6C8D896DBBDE ON post (updated_by_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');
        $this->addSql('ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8DB03A8386');
        $this->addSql('ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8D896DBBDE');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_5A8A6C8DB03A8386\')
                            ALTER TABLE post DROP CONSTRAINT IDX_5A8A6C8DB03A8386
                        ELSE
                            DROP INDEX IDX_5A8A6C8DB03A8386 ON post');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_5A8A6C8D896DBBDE\')
                            ALTER TABLE post DROP CONSTRAINT IDX_5A8A6C8D896DBBDE
                        ELSE
                            DROP INDEX IDX_5A8A6C8D896DBBDE ON post');
        $this->addSql('ALTER TABLE post DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE post DROP COLUMN updated_by_id');
    }
}
