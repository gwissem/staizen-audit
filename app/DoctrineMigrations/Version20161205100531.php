<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205100531 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE step_permissions (step_id NVARCHAR(20) NOT NULL, user_group_id INT NOT NULL, PRIMARY KEY (step_id, user_group_id))'
        );
        $this->addSql('CREATE INDEX IDX_2C3AD64F73B21E9C ON step_permissions (step_id)');
        $this->addSql('CREATE INDEX IDX_2C3AD64F1ED93D47 ON step_permissions (user_group_id)');
        $this->addSql(
            'ALTER TABLE step_permissions ADD CONSTRAINT FK_2C3AD64F73B21E9C FOREIGN KEY (step_id) REFERENCES step (id)'
        );
        $this->addSql(
            'ALTER TABLE step_permissions ADD CONSTRAINT FK_2C3AD64F1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)'
        );
        $this->addSql('ALTER TABLE step ADD return_step_id NVARCHAR(20)');
        $this->addSql('ALTER TABLE step ADD owner_group INT');
        $this->addSql('ALTER TABLE step ADD graph_points NVARCHAR(255)');
        $this->addSql('ALTER TABLE step DROP COLUMN is_returnable');
        $this->addSql('ALTER TABLE step DROP COLUMN roles');
        $this->addSql('ALTER TABLE step ALTER COLUMN id NVARCHAR(20) NOT NULL');
        $this->addSql('ALTER TABLE step ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql(
            'ALTER TABLE step ADD CONSTRAINT FK_43B9FE3C659A0C43 FOREIGN KEY (return_step_id) REFERENCES step (id)'
        );
        $this->addSql(
            'ALTER TABLE step ADD CONSTRAINT FK_43B9FE3C9D5971E2 FOREIGN KEY (owner_group) REFERENCES user_group (id) ON DELETE SET NULL'
        );
        $this->addSql('CREATE INDEX IDX_43B9FE3C659A0C43 ON step (return_step_id)');
        $this->addSql('CREATE INDEX IDX_43B9FE3C9D5971E2 ON step (owner_group)');
        $this->addSql('ALTER TABLE process_definition DROP COLUMN roles');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('DROP TABLE step_permissions');
        $this->addSql('ALTER TABLE step DROP CONSTRAINT FK_43B9FE3C659A0C43');
        $this->addSql('ALTER TABLE step DROP CONSTRAINT FK_43B9FE3C9D5971E2');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_43B9FE3C659A0C43\')
                            ALTER TABLE step DROP CONSTRAINT IDX_43B9FE3C659A0C43
                        ELSE
                            DROP INDEX IDX_43B9FE3C659A0C43 ON step'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_43B9FE3C9D5971E2\')
                            ALTER TABLE step DROP CONSTRAINT IDX_43B9FE3C9D5971E2
                        ELSE
                            DROP INDEX IDX_43B9FE3C9D5971E2 ON step'
        );
        $this->addSql('ALTER TABLE step ADD is_returnable BIT NOT NULL');
        $this->addSql('ALTER TABLE step ADD roles NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE step DROP COLUMN return_step_id');
        $this->addSql('ALTER TABLE step DROP COLUMN owner_group');
        $this->addSql('ALTER TABLE step DROP COLUMN graph_points');
        $this->addSql('ALTER TABLE step ALTER COLUMN id NVARCHAR(20) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE step ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE process_definition ADD roles NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
    }
}
