<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161221141717 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB8266BF539D');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB82232D562B');
        $this->addSql('ALTER TABLE contact_subject DROP CONSTRAINT FK_61E11FFBBF396750');
        $this->addSql('ALTER TABLE location DROP CONSTRAINT FK_5E9E89CBBF396750');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD2BF396750');
        $this->addSql('ALTER TABLE term DROP CONSTRAINT FK_A50FE78DBF396750');
        $this->addSql('ALTER TABLE vehicle DROP CONSTRAINT FK_1B80E486BF396750');
        $this->addSql('ALTER TABLE attribute DROP CONSTRAINT FK_FA7AEFFB22F2763');
        $this->addSql('ALTER TABLE object_definition_attributes DROP CONSTRAINT FK_430A92B422F2763');
        $this->addSql('ALTER TABLE process_object_definition DROP CONSTRAINT FK_2BEFB8F522F2763');
        $this->addSql('ALTER TABLE program_service_definition DROP CONSTRAINT FK_E335D3C122F2763');
        $this->addSql('ALTER TABLE case_object DROP CONSTRAINT FK_143EC45C9E7AF79');
        $this->addSql('ALTER TABLE step_object_attribute_definition DROP CONSTRAINT FK_B3CAD548C9E7AF79');
        $this->addSql('ALTER TABLE cost DROP CONSTRAINT FK_182694FCED5CA9E6');
        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD213B3DB11');
        $this->addSql('ALTER TABLE vin_dictionary DROP CONSTRAINT FK_9C7F4A26FA1FFD53');
        $this->addSql(
            'CREATE TABLE attribute_attributes (parent_attribute_id INT NOT NULL, attribute_id INT NOT NULL, PRIMARY KEY (parent_attribute_id, attribute_id))'
        );
        $this->addSql('CREATE INDEX IDX_35FD9915E68873D4 ON attribute_attributes (parent_attribute_id)');
        $this->addSql('CREATE INDEX IDX_35FD9915B6E62EFA ON attribute_attributes (attribute_id)');
        $this->addSql(
            'CREATE TABLE process_attribute_definition (id INT IDENTITY NOT NULL, process_definition_id INT, attribute_id INT, created_by_id INT, updated_by_id INT, deleted_at DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_A79EA0A780AA177E ON process_attribute_definition (process_definition_id)');
        $this->addSql('CREATE INDEX IDX_A79EA0A7B6E62EFA ON process_attribute_definition (attribute_id)');
        $this->addSql('CREATE INDEX IDX_A79EA0A7B03A8386 ON process_attribute_definition (created_by_id)');
        $this->addSql('CREATE INDEX IDX_A79EA0A7896DBBDE ON process_attribute_definition (updated_by_id)');
        $this->addSql(
            'CREATE TABLE step_attribute_definition (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), attribute_id INT, process_attribute_definition_id INT, created_by_id INT, updated_by_id INT, default_value NVARCHAR(MAX), mandatory INT NOT NULL, deleted_at DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_A6E4699373B21E9C ON step_attribute_definition (step_id)');
        $this->addSql('CREATE INDEX IDX_A6E46993B6E62EFA ON step_attribute_definition (attribute_id)');
        $this->addSql(
            'CREATE INDEX IDX_A6E469933C68CD8 ON step_attribute_definition (process_attribute_definition_id)'
        );
        $this->addSql('CREATE INDEX IDX_A6E46993B03A8386 ON step_attribute_definition (created_by_id)');
        $this->addSql('CREATE INDEX IDX_A6E46993896DBBDE ON step_attribute_definition (updated_by_id)');
        $this->addSql(
            'ALTER TABLE attribute_attributes ADD CONSTRAINT FK_35FD9915E68873D4 FOREIGN KEY (parent_attribute_id) REFERENCES attribute (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_attributes ADD CONSTRAINT FK_35FD9915B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)'
        );
        $this->addSql(
            'ALTER TABLE process_attribute_definition ADD CONSTRAINT FK_A79EA0A780AA177E FOREIGN KEY (process_definition_id) REFERENCES process_definition (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_attribute_definition ADD CONSTRAINT FK_A79EA0A7B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_attribute_definition ADD CONSTRAINT FK_A79EA0A7B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE process_attribute_definition ADD CONSTRAINT FK_A79EA0A7896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE step_attribute_definition ADD CONSTRAINT FK_A6E4699373B21E9C FOREIGN KEY (step_id) REFERENCES step (id)'
        );
        $this->addSql(
            'ALTER TABLE step_attribute_definition ADD CONSTRAINT FK_A6E46993B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE step_attribute_definition ADD CONSTRAINT FK_A6E469933C68CD8 FOREIGN KEY (process_attribute_definition_id) REFERENCES process_attribute_definition (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE step_attribute_definition ADD CONSTRAINT FK_A6E46993B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE step_attribute_definition ADD CONSTRAINT FK_A6E46993896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('DROP TABLE case_object');
        $this->addSql('DROP TABLE contact_subject');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE object_definition');
        $this->addSql('DROP TABLE object_definition_attributes');
        $this->addSql('DROP TABLE process_object_definition');
        $this->addSql('DROP TABLE program_service_definition');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE step_object_attribute_definition');
        $this->addSql('DROP TABLE term');
        $this->addSql('DROP TABLE vehicle');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_9C7F4A26FA1FFD53\')
                            ALTER TABLE vin_dictionary DROP CONSTRAINT IDX_9C7F4A26FA1FFD53
                        ELSE
                            DROP INDEX IDX_9C7F4A26FA1FFD53 ON vin_dictionary'
        );
        $this->addSql(
            'sp_RENAME \'vin_dictionary.step_object_attr_def_id\', \'step_attribute_definition_id\', \'COLUMN\''
        );
        $this->addSql(
            'ALTER TABLE vin_dictionary ADD CONSTRAINT FK_9C7F4A268A4C35B0 FOREIGN KEY (step_attribute_definition_id) REFERENCES step_attribute_definition (id)'
        );
        $this->addSql('CREATE INDEX IDX_9C7F4A268A4C35B0 ON vin_dictionary (step_attribute_definition_id)');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FA7AEFFB22F2763\')
                            ALTER TABLE attribute DROP CONSTRAINT IDX_FA7AEFFB22F2763
                        ELSE
                            DROP INDEX IDX_FA7AEFFB22F2763 ON attribute'
        );
        $this->addSql('ALTER TABLE attribute DROP COLUMN object_definition_id');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_182694FCED5CA9E6\')
                            ALTER TABLE cost DROP CONSTRAINT IDX_182694FCED5CA9E6
                        ELSE
                            DROP INDEX IDX_182694FCED5CA9E6 ON cost'
        );
        $this->addSql('ALTER TABLE cost DROP COLUMN service_id');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB8266BF539D\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB8266BF539D
                        ELSE
                            DROP INDEX IDX_FE4FBB8266BF539D ON attribute_value'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB82232D562B\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB82232D562B
                        ELSE
                            DROP INDEX IDX_FE4FBB82232D562B ON attribute_value'
        );
        $this->addSql('ALTER TABLE attribute_value ADD value_attribute_id INT');
        $this->addSql('ALTER TABLE attribute_value ADD group_process_instance_id INT');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN value_object_id');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN object_id');
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB821443AA30 FOREIGN KEY (value_attribute_id) REFERENCES attribute (id)'
        );
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82EC062BEE FOREIGN KEY (group_process_instance_id) REFERENCES process_instance (id) ON DELETE CASCADE'
        );
        $this->addSql('CREATE INDEX IDX_FE4FBB821443AA30 ON attribute_value (value_attribute_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82EC062BEE ON attribute_value (group_process_instance_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE step_attribute_definition DROP CONSTRAINT FK_A6E469933C68CD8');
        $this->addSql('ALTER TABLE vin_dictionary DROP CONSTRAINT FK_9C7F4A268A4C35B0');
        $this->addSql(
            'CREATE TABLE case_object (id INT IDENTITY NOT NULL, process_object_definition_id INT, group_process_instance_id INT, company_id INT, created_at DATETIME2(6), updated_at DATETIME2(6), type NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_143EC45C9E7AF79 ON case_object (process_object_definition_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_143EC45EC062BEE ON case_object (group_process_instance_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_143EC45979B1AD6 ON case_object (company_id)');
        $this->addSql(
            'CREATE TABLE contact_subject (id INT NOT NULL, firstname NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, lastname NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, identity_number NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, phone_number INT NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE location (id INT NOT NULL, longitude DOUBLE PRECISION NOT NULL, latitude DOUBLE PRECISION NOT NULL, country NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, postal_code NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, city NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, street NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, unit NVARCHAR(5) COLLATE Polish_CI_AS NOT NULL, apartment NVARCHAR(5) COLLATE Polish_CI_AS NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE object_definition (id INT IDENTITY NOT NULL, created_by_id INT, updated_by_id INT, name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, object_type NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), deleted_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_1BAF8664B03A8386 ON object_definition (created_by_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_1BAF8664896DBBDE ON object_definition (updated_by_id)');
        $this->addSql(
            'CREATE TABLE object_definition_attributes (object_definition_id INT NOT NULL, attribute_id INT NOT NULL, PRIMARY KEY (object_definition_id, attribute_id))'
        );
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_430A92B422F2763 ON object_definition_attributes (object_definition_id)'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_430A92B4B6E62EFA ON object_definition_attributes (attribute_id)');
        $this->addSql(
            'CREATE TABLE process_object_definition (id INT IDENTITY NOT NULL, process_definition_id INT, object_definition_id INT, created_by_id INT, updated_by_id INT, created_at DATETIME2(6), updated_at DATETIME2(6), deleted_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_2BEFB8F580AA177E ON process_object_definition (process_definition_id)'
        );
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_2BEFB8F522F2763 ON process_object_definition (object_definition_id)'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_2BEFB8F5B03A8386 ON process_object_definition (created_by_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_2BEFB8F5896DBBDE ON process_object_definition (updated_by_id)');
        $this->addSql(
            'CREATE TABLE program_service_definition (program_id INT NOT NULL, object_definition_id INT NOT NULL, PRIMARY KEY (program_id, object_definition_id))'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_E335D3C13EB8070A ON program_service_definition (program_id)');
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_E335D3C122F2763 ON program_service_definition (object_definition_id)'
        );
        $this->addSql(
            'CREATE TABLE service (id INT NOT NULL, program_id INT, master_id INT, status INT NOT NULL, price INT NOT NULL, currency NVARCHAR(3) COLLATE Polish_CI_AS NOT NULL, quantity NVARCHAR(3) COLLATE Polish_CI_AS NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_E19D9AD23EB8070A ON service (program_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_E19D9AD213B3DB11 ON service (master_id)');
        $this->addSql(
            'CREATE TABLE step_object_attribute_definition (id INT IDENTITY NOT NULL, step_id NVARCHAR(20) COLLATE Polish_CI_AS, attribute_id INT, process_object_definition_id INT, created_by_id INT, updated_by_id INT, default_value NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, mandatory INT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), deleted_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_B3CAD54873B21E9C ON step_object_attribute_definition (step_id)');
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_B3CAD548B6E62EFA ON step_object_attribute_definition (attribute_id)'
        );
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_B3CAD548C9E7AF79 ON step_object_attribute_definition (process_object_definition_id)'
        );
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_B3CAD548B03A8386 ON step_object_attribute_definition (created_by_id)'
        );
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_B3CAD548896DBBDE ON step_object_attribute_definition (updated_by_id)'
        );
        $this->addSql(
            'CREATE TABLE term (id INT NOT NULL, user_id INT, description NVARCHAR(255) COLLATE Polish_CI_AS, reminder DATETIME2(6), [from] NVARCHAR(255) COLLATE Polish_CI_AS, [to] NVARCHAR(255) COLLATE Polish_CI_AS, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_A50FE78DA76ED395 ON term (user_id)');
        $this->addSql(
            'CREATE TABLE vehicle (id INT NOT NULL, vin NVARCHAR(17) COLLATE Polish_CI_AS NOT NULL, registration_number NVARCHAR(10) COLLATE Polish_CI_AS NOT NULL, brand NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, model NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, production_year INT NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql(
            'ALTER TABLE case_object ADD CONSTRAINT FK_143EC45C9E7AF79 FOREIGN KEY (process_object_definition_id) REFERENCES process_object_definition (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE case_object ADD CONSTRAINT FK_143EC45EC062BEE FOREIGN KEY (group_process_instance_id) REFERENCES process_instance (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE case_object ADD CONSTRAINT FK_143EC45979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE contact_subject ADD CONSTRAINT FK_61E11FFBBF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBBF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE object_definition ADD CONSTRAINT FK_1BAF8664B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE object_definition ADD CONSTRAINT FK_1BAF8664896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE object_definition_attributes ADD CONSTRAINT FK_430A92B422F2763 FOREIGN KEY (object_definition_id) REFERENCES object_definition (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE object_definition_attributes ADD CONSTRAINT FK_430A92B4B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE process_object_definition ADD CONSTRAINT FK_2BEFB8F580AA177E FOREIGN KEY (process_definition_id) REFERENCES process_definition (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_object_definition ADD CONSTRAINT FK_2BEFB8F522F2763 FOREIGN KEY (object_definition_id) REFERENCES object_definition (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_object_definition ADD CONSTRAINT FK_2BEFB8F5B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE process_object_definition ADD CONSTRAINT FK_2BEFB8F5896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE program_service_definition ADD CONSTRAINT FK_E335D3C13EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE program_service_definition ADD CONSTRAINT FK_E335D3C122F2763 FOREIGN KEY (object_definition_id) REFERENCES object_definition (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2BF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE service ADD CONSTRAINT FK_E19D9AD23EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE service ADD CONSTRAINT FK_E19D9AD213B3DB11 FOREIGN KEY (master_id) REFERENCES service (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD54873B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD548B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD548C9E7AF79 FOREIGN KEY (process_object_definition_id) REFERENCES process_object_definition (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD548B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ADD CONSTRAINT FK_B3CAD548896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE term ADD CONSTRAINT FK_A50FE78DBF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE term ADD CONSTRAINT FK_A50FE78DA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486BF396750 FOREIGN KEY (id) REFERENCES case_object (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql('DROP TABLE attribute_attributes');
        $this->addSql('DROP TABLE process_attribute_definition');
        $this->addSql('DROP TABLE step_attribute_definition');
        $this->addSql('ALTER TABLE attribute ADD object_definition_id INT');
        $this->addSql(
            'ALTER TABLE attribute ADD CONSTRAINT FK_FA7AEFFB22F2763 FOREIGN KEY (object_definition_id) REFERENCES object_definition (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_FA7AEFFB22F2763 ON attribute (object_definition_id)');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB821443AA30');
        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB82EC062BEE');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB821443AA30\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB821443AA30
                        ELSE
                            DROP INDEX IDX_FE4FBB821443AA30 ON attribute_value'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB82EC062BEE\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB82EC062BEE
                        ELSE
                            DROP INDEX IDX_FE4FBB82EC062BEE ON attribute_value'
        );
        $this->addSql('ALTER TABLE attribute_value ADD value_object_id INT');
        $this->addSql('ALTER TABLE attribute_value ADD object_id INT');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN value_attribute_id');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN group_process_instance_id');
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB8266BF539D FOREIGN KEY (value_object_id) REFERENCES case_object (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB82232D562B FOREIGN KEY (object_id) REFERENCES case_object (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_FE4FBB8266BF539D ON attribute_value (value_object_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_FE4FBB82232D562B ON attribute_value (object_id)');
        $this->addSql('ALTER TABLE cost ADD service_id INT');
        $this->addSql(
            'ALTER TABLE cost ADD CONSTRAINT FK_182694FCED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_182694FCED5CA9E6 ON cost (service_id)');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_9C7F4A268A4C35B0\')
                            ALTER TABLE vin_dictionary DROP CONSTRAINT IDX_9C7F4A268A4C35B0
                        ELSE
                            DROP INDEX IDX_9C7F4A268A4C35B0 ON vin_dictionary'
        );
        $this->addSql(
            'sp_RENAME \'vin_dictionary.step_attribute_definition_id\', \'step_object_attr_def_id\', \'COLUMN\''
        );
        $this->addSql(
            'ALTER TABLE vin_dictionary ADD CONSTRAINT FK_9C7F4A26FA1FFD53 FOREIGN KEY (step_object_attr_def_id) REFERENCES step_object_attribute_definition (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_9C7F4A26FA1FFD53 ON vin_dictionary (step_object_attr_def_id)');
        $this->addSql('ALTER TABLE vin_header ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
    }
}
