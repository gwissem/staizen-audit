<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161020121227 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE tool ALTER COLUMN unique_id NVARCHAR(32) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_20F33ED1E3C68343 ON tool (unique_id) WHERE unique_id IS NOT NULL');
        $this->addSql('ALTER TABLE tool_group ALTER COLUMN unique_id NVARCHAR(32) NOT NULL');
        $this->addSql(
            'CREATE UNIQUE INDEX UNIQ_8C159E9DE3C68343 ON tool_group (unique_id) WHERE unique_id IS NOT NULL'
        );
        $this->addSql('ALTER TABLE user_group ALTER COLUMN unique_id NVARCHAR(32) NOT NULL');
        $this->addSql(
            'CREATE UNIQUE INDEX UNIQ_8F02BF9DE3C68343 ON user_group (unique_id) WHERE unique_id IS NOT NULL'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'UNIQ_20F33ED1E3C68343\')
                            ALTER TABLE tool DROP CONSTRAINT UNIQ_20F33ED1E3C68343
                        ELSE
                            DROP INDEX UNIQ_20F33ED1E3C68343 ON tool'
        );
        $this->addSql('ALTER TABLE tool ALTER COLUMN unique_id NVARCHAR(32) COLLATE Polish_CI_AS');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'UNIQ_8C159E9DE3C68343\')
                            ALTER TABLE tool_group DROP CONSTRAINT UNIQ_8C159E9DE3C68343
                        ELSE
                            DROP INDEX UNIQ_8C159E9DE3C68343 ON tool_group'
        );
        $this->addSql('ALTER TABLE tool_group ALTER COLUMN unique_id NVARCHAR(32) COLLATE Polish_CI_AS');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'UNIQ_8F02BF9DE3C68343\')
                            ALTER TABLE user_group DROP CONSTRAINT UNIQ_8F02BF9DE3C68343
                        ELSE
                            DROP INDEX UNIQ_8F02BF9DE3C68343 ON user_group'
        );
        $this->addSql('ALTER TABLE user_group ALTER COLUMN unique_id NVARCHAR(32) COLLATE Polish_CI_AS');
    }
}
