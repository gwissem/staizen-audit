<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180511145502 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE telephony_conversation ADD process_instance_id INT');
        $this->addSql('ALTER TABLE telephony_conversation ADD id_dialog INT');
        $this->addSql('ALTER TABLE telephony_conversation ADD CONSTRAINT FK_877399ED3424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id)');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_877399ED3424B10C ON telephony_conversation (process_instance_id) WHERE process_instance_id IS NOT NULL');
        $this->addSql('CREATE INDEX fk_id_process_instance_key ON telephony_conversation (process_instance_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

    }
}
