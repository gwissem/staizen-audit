<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160923142926 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE files (id INT IDENTITY NOT NULL, document_id INT, name NVARCHAR(255) NOT NULL, path NVARCHAR(255) NOT NULL, mime_type NVARCHAR(255), file_size INT, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_6354059C33F7837 ON files (document_id)');
        $this->addSql(
            'CREATE TABLE documents (id INT IDENTITY NOT NULL, name NVARCHAR(255), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE TABLE document_company (document_id INT NOT NULL, company_id INT NOT NULL, PRIMARY KEY (document_id, company_id))'
        );
        $this->addSql('CREATE INDEX IDX_EBD0AF93C33F7837 ON document_company (document_id)');
        $this->addSql('CREATE INDEX IDX_EBD0AF93979B1AD6 ON document_company (company_id)');
        $this->addSql(
            'ALTER TABLE files ADD CONSTRAINT FK_6354059C33F7837 FOREIGN KEY (document_id) REFERENCES documents (id)'
        );
        $this->addSql(
            'ALTER TABLE document_company ADD CONSTRAINT FK_EBD0AF93C33F7837 FOREIGN KEY (document_id) REFERENCES documents (id)'
        );
        $this->addSql(
            'ALTER TABLE document_company ADD CONSTRAINT FK_EBD0AF93979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)'
        );
        $this->addSql('ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query_description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(MAX)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE files DROP CONSTRAINT FK_6354059C33F7837');
        $this->addSql('ALTER TABLE document_company DROP CONSTRAINT FK_EBD0AF93C33F7837');
        $this->addSql('DROP TABLE files');
        $this->addSql('DROP TABLE documents');
        $this->addSql('DROP TABLE document_company');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query_description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
    }
}
