<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170307151857 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE insurance_data_importer (id INT IDENTITY NOT NULL, package_id INT, name NVARCHAR(255), is_incremental BIT, connection_string NVARCHAR(255), query NVARCHAR(MAX) NOT NULL, interval INT NOT NULL, job_start_time TIME(0), active BIT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_5772E1B5F44CABFF ON insurance_data_importer (package_id)');
        $this->addSql('ALTER TABLE insurance_data_importer ADD CONSTRAINT DF_5772E1B5_19C6EFCB DEFAULT 86400 FOR interval');
        $this->addSql('ALTER TABLE insurance_data_importer ADD CONSTRAINT DF_5772E1B5_4B1EFC02 DEFAULT \'1\' FOR active');
        $this->addSql('ALTER TABLE insurance_data_importer ADD CONSTRAINT FK_5772E1B5F44CABFF FOREIGN KEY (package_id) REFERENCES vin_package (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE insurance_data_importer');

    }
}
