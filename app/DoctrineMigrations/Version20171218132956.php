<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171218132956 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE sparx_attributes ADD selected BIT');
        $this->addSql('UPDATE sparx_attributes SET selected = 1 WHERE (parent_id = (SELECT id FROM sparx_attributes WHERE title = \'Case\') and title != \'Services\') OR '
            .'title = \'SecondaryBenefitsService\' OR title = \'AccommodationService\' OR title = \'CallCenterService\' OR '
            .'title = \'HopService\' OR title = \'OffRoadRecoveryService\' OR title = \'VehicleReplacementService\' OR '
            .'title = \'RoadsideAssistanceService\' OR title = \'TelematicsService\' OR title = \'TravelService\' OR '
            .'title = \'VehicleStorageService\' OR title = \'VehicleTransportationService\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE sparx_attributes DROP COLUMN selected');
    }
}
