<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170925135708 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE sms (sms_id INT IDENTITY NOT NULL, sms_send_by INT, sms_number NVARCHAR(12) NOT NULL, sms_text VARCHAR(MAX) NOT NULL, sms_unicode BIT, delivered BIT NOT NULL, sms_delivered_date DATETIME2(6), sms_delivery_date DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), ext_id INT , PRIMARY KEY (sms_id))');
        $this->addSql('CREATE INDEX IDX_B0A93A77637B40B3 ON sms (sms_send_by)');
        $this->addSql('ALTER TABLE sms ADD CONSTRAINT FK_B0A93A77637B40B3 FOREIGN KEY (sms_send_by) REFERENCES fos_user (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE sms');

    }
}
