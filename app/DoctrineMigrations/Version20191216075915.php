<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191216075915 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("
            IF NOT EXISTS(SELECT 1 FROM AtlasDB_def.sys.columns WHERE Name = 'sms_signature' AND Object_ID = Object_ID('AtlasDB_def.dbo.platform'))
            BEGIN
                
                ALTER TABLE AtlasDB_def.dbo.platform ADD sms_signature NVARCHAR(50)
               
            END"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("
            IF EXISTS(SELECT 1 FROM AtlasDB_def.sys.columns WHERE Name = 'sms_signature' AND Object_ID = Object_ID('AtlasDB_def.dbo.platform'))
            BEGIN
                
                ALTER TABLE AtlasDB_def.dbo.platform DROP COLUMN sms_signature
                
            END"
        );
    }
}
