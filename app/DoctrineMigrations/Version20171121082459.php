<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171121082459 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('ALTER TABLE dictionary ADD argument1 NVARCHAR(500)');
        $this->addSql('ALTER TABLE dictionary ADD argument2 NVARCHAR(500)');
        $this->addSql("UPDATE dictionary SET argument1 = '11' WHERE typeD = 'makeModel' AND textD like '%volkswagen%'");
        $this->addSql("UPDATE dictionary SET argument1 = '6' WHERE typeD = 'makeModel' AND textD like '%skoda%'");
        $this->addSql("UPDATE dictionary SET argument1 = '2' WHERE typeD = 'makeModel' AND textD like '%ford%'");
        $this->addSql("UPDATE dictionary SET argument1 = '10' WHERE typeD = 'makeModel' AND textD like '%seat%'");
        $this->addSql("UPDATE dictionary SET argument1 = '14' WHERE typeD = 'makeModel' AND textD like '%opel%'");
        $this->addSql("UPDATE dictionary SET argument1 = '15' WHERE typeD = 'makeModel' AND textD like '%chevrolet%'");
        $this->addSql("UPDATE dictionary SET argument1 = '19' WHERE typeD = 'makeModel' AND textD like '%mercedes%'");
        $this->addSql("UPDATE dictionary SET argument1 = '24' WHERE typeD = 'makeModel' AND textD like '%Porsche%'");
        $this->addSql("UPDATE dictionary SET argument1 = '31' WHERE typeD = 'makeModel' AND textD like '%audi%'");
        $this->addSql("UPDATE dictionary SET argument1 = '32' WHERE typeD = 'makeModel' AND textD like '%kia %'");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE dictionary DROP COLUMN argument1');
        $this->addSql('ALTER TABLE dictionary DROP COLUMN argument2');
    }
}
