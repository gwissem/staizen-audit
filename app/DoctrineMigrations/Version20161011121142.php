<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161011121142 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE tool_summary DROP COLUMN description');
        $this->addSql('ALTER TABLE style DROP COLUMN alias');
        $this->addSql('ALTER TABLE tool_action DROP COLUMN name');
        $this->addSql('ALTER TABLE tool DROP COLUMN name');
        $this->addSql('ALTER TABLE tool DROP COLUMN description');
        $this->addSql('ALTER TABLE tool DROP COLUMN sub_query_description');
        $this->addSql('ALTER TABLE tool DROP COLUMN slug');
        $this->addSql('ALTER TABLE tool_form_field DROP COLUMN label');
        $this->addSql('ALTER TABLE filter DROP COLUMN label');

        $this->addSql('ALTER TABLE tool_action DROP CONSTRAINT FK_229D808E8F7B22CC');

        $this->addSql(
            'ALTER TABLE tool_action ADD CONSTRAINT FK_229D808E8F7B22CC FOREIGN KEY (tool_id) REFERENCES tool (id) ON DELETE SET NULL'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('CREATE SCHEMA crm');
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('CREATE SCHEMA def');
        $this->addSql('CREATE SCHEMA dok');
        $this->addSql('CREATE SCHEMA hr');
        $this->addSql('CREATE SCHEMA mars');
        $this->addSql('CREATE SCHEMA mbo');
        $this->addSql('CREATE SCHEMA prc');
        $this->addSql('CREATE SCHEMA vin');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN redirect_uri NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE client ALTER COLUMN name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN ssl BIT NOT NULL');
        $this->addSql(
            'ALTER TABLE connected_mailbox ALTER COLUMN connection_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN last_processed DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN last_run DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN document_id INT NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN mime_type NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN file_size INT NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE filter ADD label NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE mail_container ALTER COLUMN mailbox_id INT NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE style ADD alias NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN document_id INT');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool ADD name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool ADD description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ADD sub_query_description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ADD slug NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_action DROP CONSTRAINT FK_229D808E8F7B22CC');
        $this->addSql('ALTER TABLE tool_action ADD name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql(
            'ALTER TABLE tool_action ADD CONSTRAINT FK_229D808E8F7B22CC FOREIGN KEY (tool_id) REFERENCES tool (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ADD label NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_summary ADD description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE tool_translation ALTER COLUMN sub_query_description NVARCHAR(255) COLLATE Polish_CI_AS'
        );
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN slug NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql(
            'ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
    }
}
