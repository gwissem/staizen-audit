<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181019150948 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.scenario (id INT IDENTITY NOT NULL, created_by_id INT, updated_by_id INT, deleted_by_id INT, name NVARCHAR(255) NOT NULL, description VARCHAR(MAX), config VARCHAR(MAX) NOT NULL, version INT NOT NULL, deleted_at DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');

        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES('dbo.scenario', 'AtlasDB_def')");
        $this->addSql('CREATE SYNONYM dbo.scenario for AtlasDB_def.dbo.scenario');

//        $this->addSql('CREATE INDEX IDX_3E45C8D8B03A8386 ON scenario (created_by_id)');
//        $this->addSql('CREATE INDEX IDX_3E45C8D8896DBBDE ON scenario (updated_by_id)');
//        $this->addSql('CREATE INDEX IDX_3E45C8D8C76F1F52 ON scenario (deleted_by_id)');
//
//        $this->addSql('ALTER TABLE scenario ADD CONSTRAINT FK_3E45C8D8B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id) ON DELETE SET NULL');
//        $this->addSql('ALTER TABLE scenario ADD CONSTRAINT FK_3E45C8D8896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id) ON DELETE SET NULL');
//        $this->addSql('ALTER TABLE scenario ADD CONSTRAINT FK_3E45C8D8C76F1F52 FOREIGN KEY (deleted_by_id) REFERENCES fos_user (id) ON DELETE SET NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP SYNONYM dbo.scenario');
//        $this->addSql('DROP TABLE AtlasDB_def.dbo.scenario');

    }
}
