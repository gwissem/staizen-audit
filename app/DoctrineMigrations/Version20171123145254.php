<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171123145254 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE user_process_history (id INT IDENTITY NOT NULL, process_instance_id INT, user_id INT, original_user_id INT, enter_at DATETIME2(6) NOT NULL, time_spent INT NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_7D1EC6113424B10C ON user_process_history (process_instance_id)');
        $this->addSql('CREATE INDEX IDX_7D1EC611A76ED395 ON user_process_history (user_id)');
        $this->addSql('CREATE INDEX IDX_7D1EC6113CCB35AF ON user_process_history (original_user_id)');
        $this->addSql('ALTER TABLE user_process_history ADD CONSTRAINT FK_7D1EC6113424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id)');
        $this->addSql('ALTER TABLE user_process_history ADD CONSTRAINT FK_7D1EC611A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE user_process_history ADD CONSTRAINT FK_7D1EC6113CCB35AF FOREIGN KEY (original_user_id) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE user_process_history');
    }
}
