<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180130125354 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE error_message (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), error_id INT NOT NULL, message NVARCHAR(1000) NOT NULL, variant INT, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_2BF9615373B21E9C ON error_message (step_id)');
        $this->addSql('CREATE UNIQUE INDEX step_error ON error_message (step_id, error_id) WHERE step_id IS NOT NULL AND error_id IS NOT NULL');
        $this->addSql('ALTER TABLE error_message ADD CONSTRAINT FK_2BF9615373B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON DELETE SET NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE error_message');

    }
}
