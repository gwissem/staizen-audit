<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171218165610 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE sparx_attributes_step (step_id NVARCHAR(20) NOT NULL, sparx_attribute_id INT NOT NULL, PRIMARY KEY (step_id, sparx_attribute_id))');
        $this->addSql('CREATE INDEX IDX_33C906B273B21E9C ON sparx_attributes_step (step_id)');
        $this->addSql('CREATE INDEX IDX_33C906B2234D989D ON sparx_attributes_step (sparx_attribute_id)');
        $this->addSql('ALTER TABLE sparx_attributes_step ADD CONSTRAINT FK_33C906B273B21E9C FOREIGN KEY (step_id) REFERENCES step (id)');
        $this->addSql('ALTER TABLE sparx_attributes_step ADD CONSTRAINT FK_33C906B2234D989D FOREIGN KEY (sparx_attribute_id) REFERENCES sparx_attributes (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE sparx_attributes_step');
    }
}
