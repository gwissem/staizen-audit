<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181129113100 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'UNIQ_ED7959CD73B21E9C\')
                            ALTER TABLE api_call DROP CONSTRAINT UNIQ_ED7959CD73B21E9C
                        ELSE
                            DROP INDEX UNIQ_ED7959CD73B21E9C ON api_call');

        $this->addSql('CREATE INDEX IDX_ED7959CD73B21E9C ON api_call (step_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_ED7959CD73B21E9C\')
                            ALTER TABLE api_call DROP CONSTRAINT IDX_ED7959CD73B21E9C
                        ELSE
                            DROP INDEX IDX_ED7959CD73B21E9C ON api_call');
        $this->addSql('CREATE UNIQUE NONCLUSTERED INDEX UNIQ_ED7959CD73B21E9C ON api_call (step_id) WHERE step_id IS NOT NULL');
    }
}
