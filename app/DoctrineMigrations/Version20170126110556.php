<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170126110556 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_instance ADD created_by_id INT');
        $this->addSql('ALTER TABLE process_instance ADD updated_by_id INT');
        $this->addSql('ALTER TABLE process_instance ADD is_active BIT NOT NULL');
        $this->addSql('ALTER TABLE process_instance ADD CONSTRAINT DF_B4C1EF5F_1B5771DD DEFAULT \'1\' FOR is_active');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN date_open');
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5FB03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5F896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_B4C1EF5FB03A8386 ON process_instance (created_by_id)');
        $this->addSql('CREATE INDEX IDX_B4C1EF5F896DBBDE ON process_instance (updated_by_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5FB03A8386');
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5F896DBBDE');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B4C1EF5FB03A8386\')
                            ALTER TABLE process_instance DROP CONSTRAINT IDX_B4C1EF5FB03A8386
                        ELSE
                            DROP INDEX IDX_B4C1EF5FB03A8386 ON process_instance'
        );
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B4C1EF5F896DBBDE\')
                            ALTER TABLE process_instance DROP CONSTRAINT IDX_B4C1EF5F896DBBDE
                        ELSE
                            DROP INDEX IDX_B4C1EF5F896DBBDE ON process_instance'
        );
        $this->addSql('ALTER TABLE process_instance ADD date_open DATETIME2(6)');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN created_by_id');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN updated_by_id');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN is_active');

    }
}
