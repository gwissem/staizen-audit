<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180625100235 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE dev_log (id INT IDENTITY NOT NULL, created_by_id INT, updated_by_id INT, deleted_by_id INT, name NVARCHAR(255) NOT NULL, description VARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_2F2E922DB03A8386 ON dev_log (created_by_id)');
        $this->addSql('CREATE INDEX IDX_2F2E922D896DBBDE ON dev_log (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_2F2E922DC76F1F52 ON dev_log (deleted_by_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE dev_log');

    }
}
