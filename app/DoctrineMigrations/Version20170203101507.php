<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170203101507 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

//        $this->addSql('DROP TABLE arrowchat');
//        $this->addSql('DROP TABLE arrowchat_admin');
//        $this->addSql('DROP TABLE arrowchat_applications');
//        $this->addSql('DROP TABLE arrowchat_banlist');
//        $this->addSql('DROP TABLE arrowchat_chatroom_banlist');
//        $this->addSql('DROP TABLE arrowchat_chatroom_messages');
//        $this->addSql('DROP TABLE arrowchat_chatroom_rooms');
//        $this->addSql('DROP TABLE arrowchat_chatroom_users');
//        $this->addSql('DROP TABLE arrowchat_config');
//        $this->addSql('DROP TABLE arrowchat_graph_log');
//        $this->addSql('DROP TABLE arrowchat_notifications');
//        $this->addSql('DROP TABLE arrowchat_notifications_markup');
//        $this->addSql('DROP TABLE arrowchat_reports');
//        $this->addSql('DROP TABLE arrowchat_smilies');
//        $this->addSql('DROP TABLE arrowchat_status');
//        $this->addSql('DROP TABLE arrowchat_themes');
//        $this->addSql('DROP TABLE arrowchat_trayicons');
//        $this->addSql('DROP TABLE arrowchat_warnings');
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5FAD7C3F23');


    }

    public function postUp(Schema $schema)
    {
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5FAD7C3F23 FOREIGN KEY (previous_process_instance_id) REFERENCES process_instance (id)'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

//        $this->addSql(
//            'CREATE TABLE arrowchat (id INT IDENTITY NOT NULL, [from] NVARCHAR(25) COLLATE Polish_CI_AS, [to] NVARCHAR(25) COLLATE Polish_CI_AS, message NVARCHAR(MAX) COLLATE Polish_CI_AS, sent INT, [read] INT, user_read SMALLINT, direction INT, PRIMARY KEY (id))'
//        );
//        $this->addSql('ALTER TABLE arrowchat ADD CONSTRAINT DF_CE1A7A40_E2D60DAE DEFAULT 0 FOR user_read');
//        $this->addSql('ALTER TABLE arrowchat ADD CONSTRAINT DF_CE1A7A40_3E4AD1B3 DEFAULT 0 FOR direction');
//        $this->addSql(
//            'CREATE TABLE arrowchat_admin (id INT IDENTITY NOT NULL, username NVARCHAR(20) COLLATE Polish_CI_AS, password NVARCHAR(50) COLLATE Polish_CI_AS, email NVARCHAR(50) COLLATE Polish_CI_AS, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_applications (id INT IDENTITY NOT NULL, name NVARCHAR(100) COLLATE Polish_CI_AS, folder NVARCHAR(100) COLLATE Polish_CI_AS, icon NVARCHAR(100) COLLATE Polish_CI_AS, width INT, height INT, bar_width INT, bar_name NVARCHAR(100) COLLATE Polish_CI_AS, dont_reload SMALLINT, default_bookmark SMALLINT, show_to_guests SMALLINT, link NVARCHAR(255) COLLATE Polish_CI_AS, update_link NVARCHAR(255) COLLATE Polish_CI_AS, version NVARCHAR(20) COLLATE Polish_CI_AS, active SMALLINT, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_applications ADD CONSTRAINT DF_F44D5E3A_D13A385F DEFAULT 0 FOR dont_reload'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_applications ADD CONSTRAINT DF_F44D5E3A_DC26A253 DEFAULT 1 FOR default_bookmark'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_applications ADD CONSTRAINT DF_F44D5E3A_5B4F2B27 DEFAULT 1 FOR show_to_guests'
//        );
//        $this->addSql('ALTER TABLE arrowchat_applications ADD CONSTRAINT DF_F44D5E3A_4B1EFC02 DEFAULT 1 FOR active');
//        $this->addSql(
//            'CREATE TABLE arrowchat_banlist (ban_id INT IDENTITY NOT NULL, ban_userid NVARCHAR(25) COLLATE Polish_CI_AS, ban_ip NVARCHAR(50) COLLATE Polish_CI_AS, banned_by NVARCHAR(25) COLLATE Polish_CI_AS, banned_time INT, PRIMARY KEY (ban_id))'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_chatroom_banlist (id INT IDENTITY NOT NULL, user_id NVARCHAR(25) COLLATE Polish_CI_AS, chatroom_id INT, ban_length INT, ban_time INT, ip_address NVARCHAR(40) COLLATE Polish_CI_AS, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_chatroom_messages (id INT IDENTITY NOT NULL, chatroom_id INT, user_id NVARCHAR(25) COLLATE Polish_CI_AS, username NVARCHAR(100) COLLATE Polish_CI_AS, message NVARCHAR(MAX) COLLATE Polish_CI_AS, global_message SMALLINT, is_mod SMALLINT, is_admin SMALLINT, sent INT, action SMALLINT, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_messages ADD CONSTRAINT DF_847F15D1_DD39F09E DEFAULT 0 FOR global_message'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_messages ADD CONSTRAINT DF_847F15D1_5B90F080 DEFAULT 0 FOR is_mod'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_messages ADD CONSTRAINT DF_847F15D1_C4F2EE06 DEFAULT 0 FOR is_admin'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_messages ADD CONSTRAINT DF_847F15D1_47CC8C92 DEFAULT 0 FOR action'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_chatroom_rooms (id INT IDENTITY NOT NULL, author_id NVARCHAR(25) COLLATE Polish_CI_AS, name NVARCHAR(100) COLLATE Polish_CI_AS, description NVARCHAR(100) COLLATE Polish_CI_AS, welcome_message NVARCHAR(255) COLLATE Polish_CI_AS, image NVARCHAR(100) COLLATE Polish_CI_AS, type SMALLINT, password NVARCHAR(25) COLLATE Polish_CI_AS, length INT, is_featured SMALLINT, max_users INT, limit_message_num INT, limit_seconds_num INT, disallowed_groups NVARCHAR(MAX) COLLATE Polish_CI_AS, session_time INT, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_rooms ADD CONSTRAINT DF_447B8C85_A79AA4E2 DEFAULT 0 FOR max_users'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_rooms ADD CONSTRAINT DF_447B8C85_FDC04D3C DEFAULT 3 FOR limit_message_num'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_rooms ADD CONSTRAINT DF_447B8C85_19E8A481 DEFAULT 10 FOR limit_seconds_num'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_chatroom_users (user_id NVARCHAR(25) COLLATE Polish_CI_AS, chatroom_id INT, is_admin SMALLINT, is_mod SMALLINT, block_chats SMALLINT, silence_length INT, silence_time INT, session_time INT)'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_users ADD CONSTRAINT DF_2C5933FA_C4F2EE06 DEFAULT 0 FOR is_admin'
//        );
//        $this->addSql('ALTER TABLE arrowchat_chatroom_users ADD CONSTRAINT DF_2C5933FA_5B90F080 DEFAULT 0 FOR is_mod');
//        $this->addSql(
//            'ALTER TABLE arrowchat_chatroom_users ADD CONSTRAINT DF_2C5933FA_EA79DEE9 DEFAULT 0 FOR block_chats'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_config (config_name NVARCHAR(255) COLLATE Polish_CI_AS, config_value NVARCHAR(MAX) COLLATE Polish_CI_AS, is_dynamic SMALLINT)'
//        );
//        $this->addSql('ALTER TABLE arrowchat_config ADD CONSTRAINT DF_7FC98537_5D978C7C DEFAULT 0 FOR is_dynamic');
//        $this->addSql(
//            'CREATE TABLE arrowchat_graph_log (id INT IDENTITY NOT NULL, date NVARCHAR(30) COLLATE Polish_CI_AS, user_messages INT, chat_room_messages INT, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_graph_log ADD CONSTRAINT DF_9B3D90D2_3B8FFA96 DEFAULT 0 FOR user_messages'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_graph_log ADD CONSTRAINT DF_9B3D90D2_E1A5868D DEFAULT 0 FOR chat_room_messages'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_notifications (id INT IDENTITY NOT NULL, to_id NVARCHAR(25) COLLATE Polish_CI_AS, author_id NVARCHAR(25) COLLATE Polish_CI_AS, author_name NCHAR(100) COLLATE Polish_CI_AS, misc1 NVARCHAR(255) COLLATE Polish_CI_AS, misc2 NVARCHAR(255) COLLATE Polish_CI_AS, misc3 NVARCHAR(255) COLLATE Polish_CI_AS, type INT, alert_read INT, user_read INT, alert_time INT, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_notifications ADD CONSTRAINT DF_1BB21F45_17E793E3 DEFAULT 0 FOR alert_read'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_notifications ADD CONSTRAINT DF_1BB21F45_E2D60DAE DEFAULT 0 FOR user_read'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_notifications_markup (id INT IDENTITY NOT NULL, name NVARCHAR(50) COLLATE Polish_CI_AS, type INT, markup NVARCHAR(MAX) COLLATE Polish_CI_AS, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_reports (id INT IDENTITY NOT NULL, report_from NVARCHAR(25) COLLATE Polish_CI_AS, report_about NVARCHAR(25) COLLATE Polish_CI_AS, report_chatroom INT, report_time INT, working_by NVARCHAR(25) COLLATE Polish_CI_AS, working_time INT, completed_by NVARCHAR(25) COLLATE Polish_CI_AS, completed_time INT, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_smilies (id INT IDENTITY NOT NULL, name NVARCHAR(20) COLLATE Polish_CI_AS, code NVARCHAR(20) COLLATE Polish_CI_AS, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_status (userid NVARCHAR(25) COLLATE Polish_CI_AS NOT NULL, guest_name NVARCHAR(50) COLLATE Polish_CI_AS, message NVARCHAR(MAX) COLLATE Polish_CI_AS, status NVARCHAR(10) COLLATE Polish_CI_AS, theme INT, popout INT, typing NVARCHAR(MAX) COLLATE Polish_CI_AS, hide_bar SMALLINT, play_sound SMALLINT, window_open SMALLINT, only_names SMALLINT, chatroom_window NVARCHAR(6) COLLATE Polish_CI_AS, chatroom_stay NVARCHAR(6) COLLATE Polish_CI_AS, chatroom_unfocus NVARCHAR(MAX) COLLATE Polish_CI_AS, chatroom_show_names SMALLINT, chatroom_block_chats SMALLINT, chatroom_sound SMALLINT, announcement SMALLINT, unfocus_chat NVARCHAR(MAX) COLLATE Polish_CI_AS, focus_chat NVARCHAR(50) COLLATE Polish_CI_AS, last_message NVARCHAR(MAX) COLLATE Polish_CI_AS, clear_chats NVARCHAR(MAX) COLLATE Polish_CI_AS, apps_bookmarks NVARCHAR(MAX) COLLATE Polish_CI_AS, apps_other NVARCHAR(MAX) COLLATE Polish_CI_AS, apps_open INT, apps_load NVARCHAR(MAX) COLLATE Polish_CI_AS, block_chats NVARCHAR(MAX) COLLATE Polish_CI_AS, session_time INT, is_admin SMALLINT, is_mod SMALLINT, hash_id NVARCHAR(20) COLLATE Polish_CI_AS, ip_address NVARCHAR(40) COLLATE Polish_CI_AS, PRIMARY KEY (userid))'
//        );
//        $this->addSql('ALTER TABLE arrowchat_status ADD CONSTRAINT DF_D043CF57_9512438 DEFAULT 1 FOR play_sound');
//        $this->addSql(
//            'ALTER TABLE arrowchat_status ADD CONSTRAINT DF_D043CF57_6E108AC2 DEFAULT \'-1\' FOR chatroom_window'
//        );
//        $this->addSql(
//            'ALTER TABLE arrowchat_status ADD CONSTRAINT DF_D043CF57_27EFFBA8 DEFAULT \'0\' FOR chatroom_stay'
//        );
//        $this->addSql('ALTER TABLE arrowchat_status ADD CONSTRAINT DF_D043CF57_4DB9D91C DEFAULT 1 FOR announcement');
//        $this->addSql('ALTER TABLE arrowchat_status ADD CONSTRAINT DF_D043CF57_C4F2EE06 DEFAULT 0 FOR is_admin');
//        $this->addSql('ALTER TABLE arrowchat_status ADD CONSTRAINT DF_D043CF57_5B90F080 DEFAULT 0 FOR is_mod');
//        $this->addSql(
//            'CREATE TABLE arrowchat_themes (id INT IDENTITY NOT NULL, folder NVARCHAR(25) COLLATE Polish_CI_AS, name NVARCHAR(100) COLLATE Polish_CI_AS, active SMALLINT, update_link NVARCHAR(255) COLLATE Polish_CI_AS, version NVARCHAR(20) COLLATE Polish_CI_AS, [default] SMALLINT, PRIMARY KEY (id))'
//        );
//        $this->addSql(
//            'CREATE TABLE arrowchat_trayicons (id INT IDENTITY NOT NULL, name NVARCHAR(100) COLLATE Polish_CI_AS, icon NVARCHAR(100) COLLATE Polish_CI_AS, location NVARCHAR(255) COLLATE Polish_CI_AS, target NVARCHAR(25) COLLATE Polish_CI_AS, width INT, height INT, tray_width INT, tray_name NVARCHAR(100) COLLATE Polish_CI_AS, tray_location INT, active SMALLINT, PRIMARY KEY (id))'
//        );
//        $this->addSql('ALTER TABLE arrowchat_trayicons ADD CONSTRAINT DF_AF658A62_4B1EFC02 DEFAULT 1 FOR active');
//        $this->addSql(
//            'CREATE TABLE arrowchat_warnings (id INT IDENTITY NOT NULL, user_id NVARCHAR(25) COLLATE Polish_CI_AS, warn_reason NVARCHAR(MAX) COLLATE Polish_CI_AS, warned_by NVARCHAR(25) COLLATE Polish_CI_AS, warning_time INT, user_read SMALLINT, PRIMARY KEY (id))'
//        );
//        $this->addSql('ALTER TABLE arrowchat_warnings ADD CONSTRAINT DF_4CC79169_E2D60DAE DEFAULT 0 FOR user_read');

        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5FAD7C3F23');
        $this->addSql(
            'ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5FAD7C3F23 FOREIGN KEY (previous_process_instance_id) REFERENCES step (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );

    }
}
