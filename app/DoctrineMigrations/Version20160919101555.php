<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160919101555 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('DROP TABLE user_filter');
        $this->addSql('ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query_description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(MAX)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql(
            'CREATE TABLE user_filter (id INT IDENTITY NOT NULL, user_filter_id INT, filter_id INT, operator1 NVARCHAR(2) COLLATE Polish_CI_AS NOT NULL, operator2 NVARCHAR(2) COLLATE Polish_CI_AS NOT NULL, value1 NVARCHAR(255) COLLATE Polish_CI_AS, value2 NVARCHAR(255) COLLATE Polish_CI_AS, query NVARCHAR(255) COLLATE Polish_CI_AS, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_1A964420D3753FBA ON user_filter (user_filter_id)');
        $this->addSql(
            'CREATE UNIQUE NONCLUSTERED INDEX UNIQ_1A964420D395B25E ON user_filter (filter_id) WHERE filter_id IS NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE user_filter ADD CONSTRAINT FK_1A964420D3753FBA FOREIGN KEY (user_filter_id) REFERENCES user_filter_set (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE user_filter ADD CONSTRAINT FK_1A964420D395B25E FOREIGN KEY (filter_id) REFERENCES filter (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query_description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
    }
}
