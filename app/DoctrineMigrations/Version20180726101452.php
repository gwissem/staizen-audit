<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180726101452 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.business_config (id INT IDENTITY NOT NULL, config_definition_id INT, program_id INT, platform_id INT, created_by_id INT, updated_by_id INT, deleted_by_id INT, value NVARCHAR(255), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_8F2A60861FAC492F ON AtlasDB_def.dbo.business_config (config_definition_id)');
        $this->addSql('CREATE INDEX IDX_8F2A60863EB8070A ON AtlasDB_def.dbo.business_config (program_id)');
        $this->addSql('CREATE INDEX IDX_8F2A6086FFE6496F ON AtlasDB_def.dbo.business_config (platform_id)');

        $this->addSql('CREATE UNIQUE INDEX business_config_unique ON AtlasDB_def.dbo.business_config (config_definition_id, program_id, platform_id) WHERE config_definition_id IS NOT NULL AND program_id IS NOT NULL AND platform_id IS NOT NULL');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.business_config_definition_translation (id INT IDENTITY NOT NULL, translatable_id INT, name NVARCHAR(255), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_676653862C2AC5D3 ON AtlasDB_def.dbo.business_config_definition_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_name_locale ON AtlasDB_def.dbo.business_config_definition_translation (locale, name) WHERE locale IS NOT NULL AND name IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX business_config_definition_translation_unique_translation ON AtlasDB_def.dbo.business_config_definition_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.business_config_definition (id INT IDENTITY NOT NULL, [key] NVARCHAR(255) NOT NULL, isProgram BIT, type NVARCHAR(64) NOT NULL, query VARCHAR(MAX), category NVARCHAR(100), PRIMARY KEY (id))');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config ADD CONSTRAINT FK_8F2A60861FAC492F FOREIGN KEY (config_definition_id) REFERENCES AtlasDB_def.dbo.business_config_definition (id)');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config ADD CONSTRAINT FK_8F2A60863EB8070A FOREIGN KEY (program_id) REFERENCES AtlasDB_def.dbo.vin_program (id)');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config ADD CONSTRAINT FK_8F2A6086FFE6496F FOREIGN KEY (platform_id) REFERENCES AtlasDB_def.dbo.platform (id)');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config_definition_translation ADD CONSTRAINT FK_676653862C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES AtlasDB_def.dbo.business_config_definition (id) ON DELETE CASCADE');

        $this->addSql('CREATE SYNONYM dbo.business_config for AtlasDB_def.dbo.business_config');
        $this->addSql('CREATE SYNONYM dbo.business_config_definition for AtlasDB_def.dbo.business_config_definition');
        $this->addSql('CREATE SYNONYM dbo.business_config_definition_translation for AtlasDB_def.dbo.business_config_definition_translation');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config DROP CONSTRAINT FK_8F2A60861FAC492F');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.business_config_definition_translation DROP CONSTRAINT FK_676653862C2AC5D3');

        $this->addSql('DROP TABLE AtlasDB_def.dbo.business_config');
        $this->addSql('DROP TABLE AtlasDB_def.dbo.business_config_definition_translation');
        $this->addSql('DROP TABLE AtlasDB_def.dbo.business_config_definition');

        $this->addSql('DROP SYNONYM dbo.business_config');
        $this->addSql('DROP SYNONYM dbo.business_config_definition');
        $this->addSql('DROP SYNONYM dbo.business_config_definition_translation');

    }
}
