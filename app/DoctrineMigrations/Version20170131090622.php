<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170131090622 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT DF_B4C1EF5F_1B5771DD');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN is_active');
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT DF_B4C1EF5F_4B1EFC02');
        $this->addSql('ALTER TABLE process_instance ALTER COLUMN active SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE process_instance ADD CONSTRAINT DF_B4C1EF5F_4B1EFC02 DEFAULT 1 FOR active');
        $this->addSql('ALTER TABLE process_instance ALTER COLUMN description NVARCHAR(MAX)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_instance ADD is_active BIT NOT NULL');
        $this->addSql('ALTER TABLE process_instance ADD CONSTRAINT DF_B4C1EF5F_1B5771DD DEFAULT \'1\' FOR is_active');
        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT DF_B4C1EF5F_4B1EFC02');
        $this->addSql('ALTER TABLE process_instance ALTER COLUMN active BIT NOT NULL');
        $this->addSql('ALTER TABLE process_instance ADD CONSTRAINT DF_B4C1EF5F_4B1EFC02 DEFAULT \'0\' FOR active');

    }
}
