<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200131084010 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("
            IF NOT EXISTS(SELECT 1 FROM AtlasDB_def.sys.columns WHERE Name = 'pushing_enabled' AND Object_ID = Object_ID('AtlasDB_def.dbo.step'))
            BEGIN
                
                ALTER TABLE AtlasDB_def.dbo.step ADD pushing_enabled bit default 0
               
            END"
        );

        $this->addSql("
        Alter table dbo.post add type int
Alter table dbo.post add document_id int
ALTER TABLE process_instance_property add color nvarchar(30) default null
CREATE SYNONYM dbo.pushing_definition for AtlasDB_def.dbo.pushing_definition

create table dbo.partners_contactInfo_cache
(
  partnerId int not null,
  serviceTypeId int,
  overrideContactType int,
  contactInfo nvarchar(255)
)

create index partners_contactInfo_cache1
  on dbo.partners_contactInfo_cache (partnerId)

create index partners_contactInfo_cache2
  on dbo.partners_contactInfo_cache (partnerId, serviceTypeId, overrideContactType)

create index partners_contactInfo_cache3
  on dbo.partners_contactInfo_cache (partnerId, serviceTypeId)

");
        $this->addSql("create table dbo.restricted_ip_access
(
	id int identity,
	group_id int not null,
	value nvarchar(4000),
	type int default 1 not null
)
go

");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("
            IF EXISTS(SELECT 1 FROM AtlasDB_def.sys.columns WHERE Name = 'pushing_enabled' AND Object_ID = Object_ID('AtlasDB_def.dbo.step'))
            BEGIN
                
                ALTER TABLE AtlasDB_def.dbo.step DROP COLUMN pushing_enabled
               
            END"
        );

        $this->addSql("
        Alter table dbo.post drop type int
Alter table dbo.post drop  document_id 
ALTER TABLE process_instance_property drop color
drop synonym dbo.pushing_definition
drop table dbo.partners_contactInfo_cache

drop index partners_contactInfo_cache1
  on dbo.partners_contactInfo_cache (partnerId)

drop index partners_contactInfo_cache2
  on dbo.partners_contactInfo_cache (partnerId, serviceTypeId, overrideContactType)

drop index partners_contactInfo_cache3
  on dbo.partners_contactInfo_cache (partnerId, serviceTypeId)
");
        $this->addSql('Drop table restricted_ip_access');
    }
}
