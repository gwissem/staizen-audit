<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181221150145 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql("CREATE TABLE AtlasDB_def.dbo.replacement_car_limit_translation (id INT IDENTITY NOT NULL, program_id int not null, event_type int not null, days int, info nvarchar(255), PRIMARY KEY (id))");
        $this->addSql("CREATE SYNONYM dbo.replacement_car_limit_translation for AtlasDB_def.dbo.replacement_car_limit_translation");


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');



    }
}
