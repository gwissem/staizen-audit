<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170216091027 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE attribute_value ADD parent_attribute_value_id INT');
        $this->addSql('ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB822FC2E0EE FOREIGN KEY (parent_attribute_value_id) REFERENCES attribute_value (id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB822FC2E0EE ON attribute_value (parent_attribute_value_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB822FC2E0EE');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB822FC2E0EE\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB822FC2E0EE
                        ELSE
                            DROP INDEX IDX_FE4FBB822FC2E0EE ON attribute_value');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN parent_attribute_value_id');

    }
}
