<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161219153032 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'description_program\')
                            ALTER TABLE vin_dictionary DROP CONSTRAINT description_program
                        ELSE
                            DROP INDEX description_program ON vin_dictionary'
        );
        $this->addSql(
            'CREATE UNIQUE INDEX description_package ON vin_dictionary (description, package_id) WHERE description IS NOT NULL AND package_id IS NOT NULL'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'description_package\')
                            ALTER TABLE vin_dictionary DROP CONSTRAINT description_package
                        ELSE
                            DROP INDEX description_package ON vin_dictionary'
        );
        $this->addSql(
            'CREATE UNIQUE NONCLUSTERED INDEX description_program ON vin_dictionary (description, program_id) WHERE description IS NOT NULL AND program_id IS NOT NULL'
        );

    }
}
