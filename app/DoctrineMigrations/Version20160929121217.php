<?php

namespace Application\Migrations;

use ApiBundle\Entity\Client;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160929121217 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE auth_code (id INT IDENTITY NOT NULL, client_id INT NOT NULL, user_id INT, token NVARCHAR(255) NOT NULL, redirect_uri NVARCHAR(MAX) NOT NULL, expires_at INT, scope NVARCHAR(255), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5933D02C5F37A13B ON auth_code (token) WHERE token IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_5933D02C19EB6921 ON auth_code (client_id)');
        $this->addSql('CREATE INDEX IDX_5933D02CA76ED395 ON auth_code (user_id)');
        $this->addSql(
            'CREATE TABLE refresh_token (id INT IDENTITY NOT NULL, client_id INT NOT NULL, user_id INT, token NVARCHAR(255) NOT NULL, expires_at INT, scope NVARCHAR(255), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C74F21955F37A13B ON refresh_token (token) WHERE token IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_C74F219519EB6921 ON refresh_token (client_id)');
        $this->addSql('CREATE INDEX IDX_C74F2195A76ED395 ON refresh_token (user_id)');
        $this->addSql(
            'CREATE TABLE access_token (id INT IDENTITY NOT NULL, client_id INT NOT NULL, user_id INT, token NVARCHAR(255) NOT NULL, expires_at INT, scope NVARCHAR(255), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B6A2DD685F37A13B ON access_token (token) WHERE token IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_B6A2DD6819EB6921 ON access_token (client_id)');
        $this->addSql('CREATE INDEX IDX_B6A2DD68A76ED395 ON access_token (user_id)');
        $this->addSql(
            'CREATE TABLE client (id INT IDENTITY NOT NULL, random_id NVARCHAR(255) NOT NULL, redirect_uris NVARCHAR(MAX) NOT NULL, secret NVARCHAR(255) NOT NULL, allowed_grant_types NVARCHAR(MAX) NOT NULL, name NVARCHAR(255), PRIMARY KEY (id))'
        );
        $this->addSql(
            'EXEC sp_addextendedproperty N\'MS_Description\', N\'(DC2Type:array)\', N\'SCHEMA\', dbo, N\'TABLE\', client, N\'COLUMN\', redirect_uris'
        );
        $this->addSql(
            'EXEC sp_addextendedproperty N\'MS_Description\', N\'(DC2Type:array)\', N\'SCHEMA\', dbo, N\'TABLE\', client, N\'COLUMN\', allowed_grant_types'
        );
        $this->addSql(
            'ALTER TABLE auth_code ADD CONSTRAINT FK_5933D02C19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)'
        );
        $this->addSql(
            'ALTER TABLE auth_code ADD CONSTRAINT FK_5933D02CA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE refresh_token ADD CONSTRAINT FK_C74F219519EB6921 FOREIGN KEY (client_id) REFERENCES client (id)'
        );
        $this->addSql(
            'ALTER TABLE refresh_token ADD CONSTRAINT FK_C74F2195A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)'
        );
        $this->addSql(
            'ALTER TABLE access_token ADD CONSTRAINT FK_B6A2DD6819EB6921 FOREIGN KEY (client_id) REFERENCES client (id)'
        );
        $this->addSql(
            'ALTER TABLE access_token ADD CONSTRAINT FK_B6A2DD68A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)'
        );

    }

    public function postUp(Schema $schema)
    {
        $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
        $client = $clientManager->createClient();
        $client->setRedirectUris([]);
        $client->setAllowedGrantTypes(['password']);
        $client->setName(Client::NAME_API_TOOLS);
        $clientManager->updateClient($client);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE auth_code DROP CONSTRAINT FK_5933D02C19EB6921');
        $this->addSql('ALTER TABLE refresh_token DROP CONSTRAINT FK_C74F219519EB6921');
        $this->addSql('ALTER TABLE access_token DROP CONSTRAINT FK_B6A2DD6819EB6921');
        $this->addSql('DROP TABLE auth_code');
        $this->addSql('DROP TABLE refresh_token');
        $this->addSql('DROP TABLE access_token');
        $this->addSql('DROP TABLE client');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE mail_container ALTER COLUMN mailbox_id INT NOT NULL');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query_description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
    }
}
