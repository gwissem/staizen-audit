<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181025130913 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('CREATE TABLE AtlasDb_def.dbo.attribute_value_translator (id INT IDENTITY NOT NULL, attribute_path NVARCHAR(255) NOT NULL, value_int INT NOT NULL, output NVARCHAR(1000) NOT NULL, scope NVARCHAR(255), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql("INSERT INTO dbo.table_def (tableName, dbName) VALUES ('dbo.attribute_value_translator','AtlasDB_def') ");
        $this->addSql('CREATE SYNONYM dbo.attribute_value_translator for AtlasDB_def.dbo.attribute_value_translator');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('DROP TABLE AtlasDb_def.dbo.attribute_value_translator');

    }
}
