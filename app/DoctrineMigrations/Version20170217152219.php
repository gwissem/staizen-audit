<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170217152219 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE attribute_condition (id INT IDENTITY NOT NULL, attribute_id INT, created_by_id INT, updated_by_id INT, type INT NOT NULL, condition NVARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_F7F8C5B6E62EFA ON attribute_condition (attribute_id)');
        $this->addSql('CREATE INDEX IDX_F7F8C5B03A8386 ON attribute_condition (created_by_id)');
        $this->addSql('CREATE INDEX IDX_F7F8C5896DBBDE ON attribute_condition (updated_by_id)');
        $this->addSql('ALTER TABLE attribute_condition ADD CONSTRAINT DF_F7F8C5_8CDE5729 DEFAULT 1 FOR type');
        $this->addSql('ALTER TABLE attribute_condition ADD CONSTRAINT FK_F7F8C5B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('ALTER TABLE attribute_condition ADD CONSTRAINT FK_F7F8C5B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE attribute_condition ADD CONSTRAINT FK_F7F8C5896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id)');

        $this->addSql('ALTER TABLE attribute ADD input_type NVARCHAR(255)');
        $this->addSql('ALTER TABLE attribute ADD CONSTRAINT DF_FA7AEFFB_CFB3EC1E DEFAULT \'text\' FOR input_type');

    }

    public function postUp(Schema $schema){
        $this->addSql("UPDATE attribute SET input_type = 'text'");
        $this->addSql('ALTER TABLE attribute ALTER COLUMN input_type NVARCHAR(255) NOT NULL');
    }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE attribute_condition');
        $this->addSql('ALTER TABLE attribute DROP COLUMN input_type');

    }
}
