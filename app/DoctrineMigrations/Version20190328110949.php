<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190328110949 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE AtlasDB_v.dbo.mondial_old_cases(
  id         INT IDENTITY  NOT NULL,
  first_name         nvarchar(255) NOT NULL,
  last_name         nvarchar(255) NOT NULL,
  vin        NVARCHAR(255) NOT NULL,
  reg_number NVARCHAR(255) NOT NULL,
  make_model NVARCHAR(255) NOT NULL,
  location NVARCHAR(255) NOT NULL,
  created_at DATETIME2(6),
  updated_at DATETIME2(6),
  PRIMARY KEY (id)
)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');
        $this->addSql('DROP TABLE AtlasDB_v.dbo.mondial_old_cases');

    }
}
