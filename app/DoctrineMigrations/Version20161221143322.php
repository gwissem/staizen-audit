<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161221143322 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE service (id INT IDENTITY NOT NULL, company_id INT, master_id INT, status INT NOT NULL, price INT NOT NULL, currency NVARCHAR(3) NOT NULL, quantity NVARCHAR(3) NOT NULL, deleted_at DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_E19D9AD2979B1AD6 ON service (company_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD213B3DB11 ON service (master_id)');
        $this->addSql(
            'CREATE TABLE program_services (program_id INT NOT NULL, service_id INT NOT NULL, PRIMARY KEY (program_id, service_id))'
        );
        $this->addSql('CREATE INDEX IDX_F2071D5C3EB8070A ON program_services (program_id)');
        $this->addSql('CREATE INDEX IDX_F2071D5CED5CA9E6 ON program_services (service_id)');
        $this->addSql(
            'ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE service ADD CONSTRAINT FK_E19D9AD213B3DB11 FOREIGN KEY (master_id) REFERENCES service (id)'
        );
        $this->addSql(
            'ALTER TABLE program_services ADD CONSTRAINT FK_F2071D5C3EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id)'
        );
        $this->addSql(
            'ALTER TABLE program_services ADD CONSTRAINT FK_F2071D5CED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id)'
        );
        $this->addSql('ALTER TABLE cost ADD service_id INT');
        $this->addSql(
            'ALTER TABLE cost ADD CONSTRAINT FK_182694FCED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE'
        );
        $this->addSql('CREATE INDEX IDX_182694FCED5CA9E6 ON cost (service_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE service DROP CONSTRAINT FK_E19D9AD213B3DB11');
        $this->addSql('ALTER TABLE program_services DROP CONSTRAINT FK_F2071D5CED5CA9E6');
        $this->addSql('ALTER TABLE cost DROP CONSTRAINT FK_182694FCED5CA9E6');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE program_services');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_182694FCED5CA9E6\')
                            ALTER TABLE cost DROP CONSTRAINT IDX_182694FCED5CA9E6
                        ELSE
                            DROP INDEX IDX_182694FCED5CA9E6 ON cost'
        );
        $this->addSql('ALTER TABLE cost DROP COLUMN service_id');
    }
}
