<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170105110034 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE process_flow_translation (id INT IDENTITY NOT NULL, translatable_id INT, description NVARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_F8AE4B232C2AC5D3 ON process_flow_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX process_flow_translation_unique_translation ON process_flow_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE process_flow_translation ADD CONSTRAINT FK_F8AE4B232C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES process_flow (id) ON DELETE CASCADE'
        );
        $this->addSql('ALTER TABLE process_flow DROP COLUMN description');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('DROP TABLE process_flow_translation');
        $this->addSql('ALTER TABLE process_flow ADD description NVARCHAR(255) COLLATE Polish_CI_AS');

    }
}
