<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171110093524 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE process_import (id INT IDENTITY NOT NULL, process_definition_id INT, name NVARCHAR(255), active INT, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_57A250BA80AA177E ON process_import (process_definition_id)');
        $this->addSql('ALTER TABLE process_import ADD CONSTRAINT FK_57A250BA80AA177E FOREIGN KEY (process_definition_id) REFERENCES process_definition (id)');

        $this->addSql("INSERT INTO process_import (process_definition_id, name, active) VALUES (1062, 'diagnosis', 1)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE process_import');

    }
}
