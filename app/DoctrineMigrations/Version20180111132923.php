<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180111132923 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE step DROP CONSTRAINT DF_43B9FE3C_B06BC9BE');
        $this->addSql('ALTER TABLE step ALTER COLUMN can_previous BIT');
        $this->addSql('ALTER TABLE step ADD CONSTRAINT DF_43B9FE3C_B06BC9BE DEFAULT \'0\' FOR can_previous');

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE step DROP CONSTRAINT DF_43B9FE3C_B06BC9BE');
        $this->addSql('ALTER TABLE step ALTER COLUMN can_previous SMALLINT');
        $this->addSql('ALTER TABLE step ADD CONSTRAINT DF_43B9FE3C_B06BC9BE DEFAULT 0 FOR can_previous');

    }
}
