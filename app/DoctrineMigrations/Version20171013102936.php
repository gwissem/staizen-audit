<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171013102936 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE form_control_translation (id INT IDENTITY NOT NULL, translatable_id INT, label NVARCHAR(4000), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_8CD5DF202C2AC5D3 ON form_control_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_label_locale ON form_control_translation (translatable_id, locale, label) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL AND label IS NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX form_control_translation_unique_translation ON form_control_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('ALTER TABLE form_control_translation ADD CONSTRAINT FK_8CD5DF202C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES form_control (id) ON DELETE CASCADE');
        $this->addSql("INSERT INTO form_control_translation (translatable_id, label, locale, created_at) SELECT id, label, 'pl', GETDATE() from form_control");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE form_control_translation');

    }
}
