<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180903095015 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        
//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.api_call (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), type_id INT, method NVARCHAR(255) NOT NULL, url NVARCHAR(255) NOT NULL, body VARCHAR(MAX), created_at DATETIME2(6) NOT NULL, PRIMARY KEY (id))');
//        $this->addSql('CREATE INDEX IDX_ED7959CD73B21E9C ON AtlasDB_def.dbo.api_call (step_id)');
//        $this->addSql('CREATE INDEX IDX_ED7959CDC54C8C93 ON AtlasDB_def.dbo.api_call (type_id)');
//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.api_call_type (id INT IDENTITY NOT NULL, name NVARCHAR(255) NOT NULL, url NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
//
//
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ADD CONSTRAINT FK_ED7959CD73B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON DELETE SET NULL');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ADD CONSTRAINT FK_ED7959CDC54C8C93 FOREIGN KEY (type_id) REFERENCES AtlasDB_def.dbo.api_call_type (id) ON DELETE SET NULL');
//
        
        $this->addSql('DROP TABLE form_log');

//        $this->addSql('CREATE SYNONYM dbo.api_call for AtlasDB_def.dbo.api_call');
//        $this->addSql('CREATE SYNONYM dbo.api_call_type for AtlasDB_def.dbo.api_call_type');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call DROP CONSTRAINT FK_ED7959CDC54C8C93');
//
        $this->addSql('CREATE TABLE form_log (id INT IDENTITY NOT NULL, step_id NVARCHAR(20) COLLATE Polish_CI_AS, method NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL, headers VARCHAR(MAX) COLLATE Polish_CI_AS, body VARCHAR(MAX) COLLATE Polish_CI_AS, created_at DATETIME2(6) NOT NULL, PRIMARY KEY (id))');
//
//        $this->addSql('DROP TABLE AtlasDB_def.dbo.api_call');
//        $this->addSql('DROP TABLE AtlasDB_def.dbo.api_call_type');
        
        
    }
}
