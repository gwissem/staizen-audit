<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200127072442 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.custom_validation_message_translation (id INT IDENTITY NOT NULL, translatable_id INT NOT NULL, locale NVARCHAR(3) NOT NULL, message VARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX unique_id_locale ON AtlasDB_def.dbo.custom_validation_message_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.attribute_condition DROP COLUMN custom_validation_message');
        $this->addSql('CREATE SYNONYM dbo.custom_validation_message_translation for AtlasDB_def.dbo.custom_validation_message_translation');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.attribute_condition ADD custom_validation_message VARCHAR(MAX) COLLATE Polish_CI_AS');


        $this->addSql('DROP TABLE AtlasDB_def.dbo.custom_validation_message_translation');

    }
}
