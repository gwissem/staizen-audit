<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170515121808 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE form_control DROP CONSTRAINT FK_DCB74C708746D722');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_DCB74C708746D722\')
                            ALTER TABLE form_control DROP CONSTRAINT IDX_DCB74C708746D722
                        ELSE
                            DROP INDEX IDX_DCB74C708746D722 ON form_control');
        $this->addSql('sp_RENAME \'form_control.template_form\', \'form_template\', \'COLUMN\'');
        $this->addSql('ALTER TABLE form_control ADD CONSTRAINT FK_DCB74C70265A9AC7 FOREIGN KEY (form_template) REFERENCES form_template (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_DCB74C70265A9AC7 ON form_control (form_template)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE form_control DROP CONSTRAINT FK_DCB74C70265A9AC7');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_DCB74C70265A9AC7\')
                            ALTER TABLE form_control DROP CONSTRAINT IDX_DCB74C70265A9AC7
                        ELSE
                            DROP INDEX IDX_DCB74C70265A9AC7 ON form_control');
        $this->addSql('sp_RENAME \'form_control.form_template\', \'template_form\', \'COLUMN\'');
        $this->addSql('ALTER TABLE form_control ADD CONSTRAINT FK_DCB74C708746D722 FOREIGN KEY (template_form) REFERENCES form_template (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_DCB74C708746D722 ON form_control (template_form)');
    }
}
