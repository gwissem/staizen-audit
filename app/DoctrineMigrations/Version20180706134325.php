<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180706134325 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE process_instance_priority (process_instance_id INT NOT NULL, urgent BIT, priority SMALLINT, priority_changed_at DATETIME2(6), PRIMARY KEY (process_instance_id))');
        $this->addSql('ALTER TABLE process_instance_priority ADD CONSTRAINT FK_504CE5713424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id)');

        $this->addSql('ALTER TABLE process_instance DROP COLUMN urgent');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN priority_changed_at');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.platform_group_platforms (platform_id INT NOT NULL, platform_group_id INT NOT NULL, PRIMARY KEY (platform_id, platform_group_id))');
        $this->addSql('CREATE INDEX IDX_426F123FFE6496F ON AtlasDB_def.dbo.platform_group_platforms (platform_id)');
        $this->addSql('CREATE INDEX IDX_426F1237B24D955 ON AtlasDB_def.dbo.platform_group_platforms (platform_group_id)');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform_group_platforms ADD CONSTRAINT FK_426F123FFE6496F FOREIGN KEY (platform_id) REFERENCES AtlasDB_def.dbo.platform (id)');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform_group_platforms ADD CONSTRAINT FK_426F1237B24D955 FOREIGN KEY (platform_group_id) REFERENCES AtlasDB_def.dbo.platform_group (id)');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform DROP CONSTRAINT FK_3952D0CB7B24D955');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_3952D0CB7B24D955\')
                            ALTER TABLE AtlasDB_def.dbo.platform DROP CONSTRAINT IDX_3952D0CB7B24D955
                        ELSE
                            DROP INDEX IDX_3952D0CB7B24D955 ON AtlasDB_def.dbo.platform');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform DROP COLUMN platform_group_id');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('DROP TABLE process_instance_priority');

        $this->addSql('ALTER TABLE process_instance ADD urgent BIT');
        $this->addSql('ALTER TABLE process_instance ADD priority_changed_at DATETIME2(6)');
        $this->addSql('DROP TABLE AtlasDB_def.dbo.platform_group_platforms');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform ADD platform_group_id INT');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform ADD CONSTRAINT FK_3952D0CB7B24D955 FOREIGN KEY (platform_group_id) REFERENCES AtlasDB_def.dbo.platform_group (id) ON UPDATE NO ACTION ON DELETE SET NULL');
    }
}
