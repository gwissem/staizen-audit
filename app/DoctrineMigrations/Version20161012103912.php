<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161012103912 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE tool_group_translation (id INT IDENTITY NOT NULL, translatable_id INT, name NVARCHAR(255) NOT NULL, locale NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_D22317C62C2AC5D3 ON tool_group_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX tool_group_translation_unique_translation ON tool_group_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE tool_group_translation ADD CONSTRAINT FK_D22317C62C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES tool_group (id) ON DELETE CASCADE'
        );

        $this->addSql(
            "INSERT INTO tool_group_translation (translatable_id, name, locale, created_at, updated_at) SELECT id, name, 'pl', created_at, updated_at FROM tool_group t"
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('CREATE SCHEMA crm');
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('CREATE SCHEMA def');
        $this->addSql('CREATE SCHEMA dok');
        $this->addSql('CREATE SCHEMA hr');
        $this->addSql('CREATE SCHEMA mars');
        $this->addSql('CREATE SCHEMA mbo');
        $this->addSql('CREATE SCHEMA prc');
        $this->addSql('CREATE SCHEMA vin');
        $this->addSql('DROP TABLE tool_group_translation');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN redirect_uri NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE client ALTER COLUMN name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN ssl BIT NOT NULL');
        $this->addSql(
            'ALTER TABLE connected_mailbox ALTER COLUMN connection_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN last_processed DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN last_run DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN document_id INT NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN mime_type NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN file_size INT NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE mail_container ALTER COLUMN mailbox_id INT NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN document_id INT');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_group ADD name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE tool_translation ALTER COLUMN sub_query_description NVARCHAR(255) COLLATE Polish_CI_AS'
        );
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN slug NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql(
            'ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
    }
}
