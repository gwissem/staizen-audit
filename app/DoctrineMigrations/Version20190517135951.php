<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190517135951 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE process_page_time (id INT IDENTITY NOT NULL, process_enter_history_id INT, process_instance_id INT, user_id INT, time INT, host NVARCHAR(255), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_7219C677CB88B103 ON process_page_time (process_enter_history_id)');
        $this->addSql('CREATE INDEX IDX_7219C6773424B10C ON process_page_time (process_instance_id)');
        $this->addSql('CREATE INDEX IDX_7219C677A76ED395 ON process_page_time (user_id)');
        $this->addSql('ALTER TABLE process_page_time ADD CONSTRAINT DF_7219C677_6F949845 DEFAULT 0 FOR time');

        $this->addSql('ALTER TABLE process_page_time ADD CONSTRAINT FK_7219C677CB88B103 FOREIGN KEY (process_enter_history_id) REFERENCES process_enter_history (id)');
        $this->addSql('ALTER TABLE process_page_time ADD CONSTRAINT FK_7219C6773424B10C FOREIGN KEY (process_instance_id) REFERENCES process_instance (id)');
        $this->addSql('ALTER TABLE process_page_time ADD CONSTRAINT FK_7219C677A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE SET NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE process_page_time');

    }
}
