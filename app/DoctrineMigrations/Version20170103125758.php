<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170103125758 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE telephony_conversation (id INT IDENTITY NOT NULL, type NVARCHAR(16) NOT NULL, status NVARCHAR(32) NOT NULL, from_address NVARCHAR(64) NOT NULL, to_address NVARCHAR(64) NOT NULL, conversation_start_time DATETIME2(6), conversation_finish_time DATETIME2(6), created_at DATETIME2(6) NOT NULL, updated_at DATETIME2(6), author INT, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX fk_type_key ON telephony_conversation (type)');
        $this->addSql('CREATE INDEX fk_status_key ON telephony_conversation (status)');
        $this->addSql('CREATE INDEX fk_from_address_key ON telephony_conversation (from_address)');
        $this->addSql('CREATE INDEX fk_to_address_key ON telephony_conversation (to_address)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE telephony_conversation');

    }
}
