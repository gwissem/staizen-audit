<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171129091517 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE service_platform_description (id INT IDENTITY NOT NULL, platform_id INT, service_definition_id INT, description NVARCHAR(MAX) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_3258AB15FFE6496F ON service_platform_description (platform_id)');
        $this->addSql('CREATE INDEX IDX_3258AB153CB2663B ON service_platform_description (service_definition_id)');
        $this->addSql('ALTER TABLE service_platform_description ADD CONSTRAINT FK_3258AB15FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id)');
        $this->addSql('ALTER TABLE service_platform_description ADD CONSTRAINT FK_3258AB153CB2663B FOREIGN KEY (service_definition_id) REFERENCES service_definition (id)');

        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(31, 3, \'Organizacja pojazdu zastępczego jest możliwa w przypadku konieczności holowania do autoryzowanego partnera serwisowego i rozpoczęciu tam diagnozy, maksymalnie na 5 dni roboczych, ale nie dłużej niż zakończenie naprawy. W kraju i za granicą. Klasa pojazdu porównywalna. Podstawienie pojazdu możliwe. Odbiór pojazdu możliwy. Możliwe łączenie ze świadczeniem Taxi. <span class="red">Brak łączenia świadczenia z podróżą i noclegiem.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(31, 6, \'Organizacja Podróży jest możliwa w przypadku konieczności holowania do autoryzowanego partnera serwisowego i rozpoczęciu tam diagnozy do limitu maksymalnie 450 Euro brutto/na osobę. Możliwość zakupu biletu kolejowego pierwszej klasy lub biletu lotniczego klasy ekonomicznej. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą. Możliwe łączenie ze świadczeniem Taxi. <span class="red">Brak łączenia świadczenia z samochodem zastępczym i podróżą.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(31, 4, \'Organizacja Noclegu jest możliwa w przypadku konieczności holowania do autoryzowanego partnera serwisowego i rozpoczęciu tam diagnozy maksymalnie na 5 dni. Przewidziany standard to 3-gwiazdkowy hotel ze śniadaniem. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą. Możliwe łączenie ze świadczeniem Taxi. Dodatkowo możliwość organizacji Taxi do hotelu (nie wliczanej w limit taxi). <span class="red">Brak łączenia świadczenia z samochodem zastępczym i podróżą.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(31, 5, \'Organizacja Taxi możliwa do limitu maksymalnie 80 Euro brutto. Dopuszczalnych jest kilka przejazdów w limicie. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(31, 7, \'Organizacja Transportu możliwa po uprzednim dostarczeniu upoważnienia do odbioru pojazdu z serwisu przez Klienta. Możliwa maksymalnie do limitu 500 Euro brutto. Transport na lawecie lub organizacja biletów lotniczych lub kolejowych. W kraju i za granicą\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(31, 8, \'Jeśli części nie są dostępne na miejscu. W kraju i za granicą. Pokrywany jest koszt przesyłki. Koszt części i koszty celne bądź administracyjne pokrywa Klient.\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(31, 9, \'Tylko za granicą i tylko na koszty naprawy w serwisie. Na wniosek Klienta, gdy Klient nie może pokryć kosztów samodzielnie. Do limitu maksymalnie 1000 Euro bez oprocentowania. Do zwrotu w ciągu 60 dni.\')');

        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(11, 3, \'Organizacja pojazdu zastępczego jest możliwa w przypadku konieczności holowania do autoryzowanego partnera serwisowego i potwierdzeniu, że naprawa będzie dłuższa niż 2 godziny lub w przypadku braku części zamiennych na miejscu, maksymalnie na 5 dni, ale nie dłużej niż zakończenie naprawy. W kraju i za granicą. Klasa pojazdu porównywalna. Podstawienie pojazdu możliwe. Odbiór pojazdu możliwy. Możliwe łączenie ze świadczeniem Taxi. <span class="red">Brak łączenia świadczenia z podróżą i noclegiem.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(11, 6, \'Organizacja Podróży możliwa jest w przypadku konieczności holowania do autoryzowanego partnera serwisowego i potwierdzeniu, że naprawa będzie dłuższa niż 2 godziny lub w przypadku braku części zamiennych na miejscu, do limitu maksymalnie 400 Euro brutto/na osobę. Możliwość zakupu biletu kolejowego pierwszej klasy lub jeśli dystans przekracza 1000 km, wtedy biletu lotniczego klasy ekonomicznej. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą. Możliwe łączenie ze świadczeniem Taxi. <span class="red">Brak łączenia świadczenia z samochodem zastępczym i podróżą.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(11, 4, \'Organizacja Noclegu jest możliwa jest w przypadku konieczności holowania do autoryzowanego partnera serwisowego i potwierdzeniu, że naprawa będzie dłuższa niż 2 godziny lub w przypadku braku części zamiennych na miejscu, maksymalnie na 1 dzień. Przewidziany standard to 3-gwiazdkowy hotel ze śniadaniem. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą. Możliwe łączenie ze świadczeniem Taxi. Dodatkowo możliwość organizacji Taxi do hotelu (nie wliczanej w limit taxi). <span class="red">Brak łączenia świadczenia z samochodem zastępczym i podróżą.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(11, 5, \'Organizacja Taxi możliwa do limitu maksymalnie 80 Euro brutto. Dopuszczalnych jest kilka przejazdów w limicie. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą.\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(11, 7, \'Organizacja Transportu możliwa po uprzednim dostarczeniu przez Klienta upoważnienia do odbioru pojazdu z serwisu. Możliwa maksymalnie do limitu 400 Euro brutto.Transport na lawecie lub organizacja biletów lotniczych lub kolejowych. W kraju i za granicą.\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(11, 8, \'Jeśli części nie są dostępne na miejscu. Tylko za granicą. Pokrywany jest koszt przesyłki. Koszt części i koszty celne bądź administracyjne pokrywa Klient.\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(11, 9, \'Tylko za granicą i tylko na koszty naprawy w serwisie. Na wniosek Klienta, gdy Klient nie może pokryć kosztów samodzielnie. Do limitu maksymalnie 1000 Euro bez oprocentowania. Do zwrotu w ciągu 60 dni.\')');

        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(6, 3, \'Organizacja pojazdu zastępczego jest możliwa w przypadku konieczności holowania do autoryzowanego partnera serwisowego i potwierdzeniu, że naprawa będzie dłuższa niż 4 godziny lub w przypadku braku części zamiennych na miejscu, maksymalnie na 3 dni roboczych, ale nie dłużej niż zakończenie naprawy. W kraju i za granicą. Klasa pojazdu porównywalna. Podstawienie pojazdu możliwe. Odbiór pojazdu możliwy. Możliwe łączenie ze świadczeniem Taxi. <span class="red">Brak łączenia świadczenia z podróżą i noclegiem.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(6, 6, \'Organizacja Podróży możliwa jest w przypadku konieczności holowania do autoryzowanego partnera serwisowego i potwierdzeniu, że naprawa będzie dłuższa niż 4 godziny lub w przypadku braku części zamiennych na miejscu, do limitu maksymalnie 400 Euro brutto/na osobę. Możliwość zakupu biletu kolejowego pierwszej klasy lub jeśli dystans przekracza 1000 km, wtedy biletu lotniczego klasy ekonomicznej. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą. Możliwe łączenie ze świadczeniem Taxi. <span class="red">Brak łączenia świadczenia z samochodem zastępczym i podróżą.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(6, 4, \'Organizacja Noclegu jest możliwa w przypadku konieczności holowania do autoryzowanego partnera serwisowego i potwierdzeniu, że naprawa będzie dłuższa niż 2 godziny lub w przypadku braku części zamiennych na miejscu, maksymalnie na 1 dzień. Przewidziany standard to 3-gwiazdkowy hotel ze śniadaniem. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą. Możliwe łączenie ze świadczeniem Taxi. Dodatkowo możliwość organizacji Taxi do hotelu (nie wliczanej w limit taxi). <span class="red">Brak łączenia świadczenia z samochodem zastępczym i podróżą.</span>\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(6, 5, \'Organizacja Taxi możliwa do limitu maksymalnie 80 Euro brutto. Dopuszczalnych jest kilka przejazdów w limicie. Przysługuje kierowcy i pasażerom pojazdu. W kraju i za granicą.\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(6, 7, \'Organizacja Transportu możliwa po uprzednim dostarczeniu przez Klienta upoważnienia do odbioru pojazdu z serwisu. Możliwa maksymalnie do limitu 400 Euro brutto. Transport na lawecie lub organizacja biletów lotniczych lub kolejowych. W kraju i za granicą.\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(6, 8, \'Jeśli części nie są dostępne na miejscu. Tylko za granicą. Pokrywany jest koszt przesyłki. Koszt części i koszty celne bądź administracyjne pokrywa Klient. Tylko za zgodą KZ.\')');
        $this->addSql('INSERT INTO service_platform_description (platform_id, service_definition_id, description) VALUES(6, 9, \'Tylko za granicą i tylko na koszty naprawy w serwisie. Na wniosek Klienta, gdy Klient nie może pokryć kosztów samodzielnie. Do limitu maksymalnie 1000 Euro bez oprocentowania. Do zwrotu w ciągu 60 dni.\')');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE service_platform_description');
    }
}
