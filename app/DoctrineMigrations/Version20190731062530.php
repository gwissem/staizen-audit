<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190731062530 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'imported_documents')
            BEGIN
                
                CREATE TABLE dbo.imported_documents (
                    id INT IDENTITY(1,1) NOT NULL,
                    name NVARCHAR(255) NOT NULL,
                    created_by_id INT NULL,
                    created_at DATETIME NULL,
                    expire_at DATETIME NULL,
                    document_id INT NOT NULL,
                    company_id INT NULL,
                    deleted_at DATETIME NULL,
                    CONSTRAINT PK__id__imported_documents PRIMARY KEY (id),
                    CONSTRAINT FK__created_by_id__imported_documents FOREIGN KEY (created_by_id) REFERENCES dbo.fos_user(id),
                    CONSTRAINT FK__document_id__imported_documents FOREIGN KEY (document_id) REFERENCES dbo.documents(id),
                    CONSTRAINT FK__company_id__imported_documents FOREIGN KEY (company_id) REFERENCES dbo.company(id)
                )
                CREATE INDEX IDX__created_by_id__imported_documents ON dbo.imported_documents (created_by_id)
                CREATE INDEX IDX__document_id__imported_documents ON dbo.imported_documents (document_id)
                CREATE INDEX IDX__company_id__imported_documents ON dbo.imported_documents(company_id)
            
            END");



    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'imported_documents')
            BEGIN
                
                DROP TABLE dbo.imported_documents
                
            END");

    }
}
