<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160929105656 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('ALTER TABLE mail_container DROP CONSTRAINT FK_BF78F27866EC35CC');
        $this->addSql('ALTER TABLE mail_container DROP CONSTRAINT FK_BF78F27821E4A579');
        $this->addSql('ALTER TABLE mail_container ADD subject NVARCHAR(255)');
        $this->addSql('ALTER TABLE mail_container ADD sender NVARCHAR(255)');
        $this->addSql('ALTER TABLE mail_container ALTER COLUMN mail_file INT');
        $this->addSql('ALTER TABLE mail_container ADD CONSTRAINT FK_BF78F27866EC35CC FOREIGN KEY (mailbox_id) REFERENCES connected_mailbox (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mail_container ADD CONSTRAINT FK_BF78F27821E4A579 FOREIGN KEY (mail_file) REFERENCES files (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE mail_container DROP CONSTRAINT FK_BF78F27866EC35CC');
        $this->addSql('ALTER TABLE mail_container DROP CONSTRAINT FK_BF78F27821E4A579');
        $this->addSql('ALTER TABLE mail_container DROP COLUMN subject');
        $this->addSql('ALTER TABLE mail_container DROP COLUMN sender');
        $this->addSql('ALTER TABLE mail_container ALTER COLUMN mail_file INT NOT NULL');
        $this->addSql('ALTER TABLE mail_container ADD CONSTRAINT FK_BF78F27866EC35CC FOREIGN KEY (mailbox_id) REFERENCES connected_mailbox (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE mail_container ADD CONSTRAINT FK_BF78F27821E4A579 FOREIGN KEY (mail_file) REFERENCES files (id) ON UPDATE NO ACTION ON DELETE NO ACTION');

    }
}
