<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180803100112 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.step_info (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), platform_id INT, text VARCHAR(MAX) NOT NULL, locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_851FAC0673B21E9C ON AtlasDB_def.dbo.step_info (step_id)');
        $this->addSql('CREATE INDEX IDX_851FAC06FFE6496F ON AtlasDB_def.dbo.step_info (platform_id)');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ADD CONSTRAINT FK_851FAC0673B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info ADD CONSTRAINT FK_851FAC06FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE SET NULL');

        $this->addSql('CREATE SYNONYM dbo.step_info for AtlasDB_def.dbo.step_info');
      }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info DROP CONSTRAINT FK_851FAC0673B21E9C');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_info DROP CONSTRAINT FK_851FAC06FFE6496F');

        $this->addSql('DROP TABLE AtlasDB_def.dbo.step_info');
    }
}
