<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180906141712 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_status_dictionary ADD sparx_update NVARCHAR(MAX)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call ADD condition NVARCHAR(255)');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_status_dictionary DROP COLUMN sparx_update');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.api_call DROP COLUMN condition');

    }
}
