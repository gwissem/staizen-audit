<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180129114409 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE log ADD param1 NVARCHAR(255)');
        $this->addSql('ALTER TABLE log ADD param2 NVARCHAR(255)');
        $this->addSql('ALTER TABLE log ADD param3 NVARCHAR(255)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE log DROP COLUMN param1');
        $this->addSql('ALTER TABLE log DROP COLUMN param2');
        $this->addSql('ALTER TABLE log DROP COLUMN param3');

    }
}
