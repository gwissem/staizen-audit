<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180528122436 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_translation ADD forward_variant_info NVARCHAR(255)');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step ADD forward_variant INT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step DROP COLUMN forward_variant');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step_translation DROP COLUMN forward_variant_info');
    }
}
