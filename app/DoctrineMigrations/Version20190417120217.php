<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190417120217 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('CREATE TABLE queued_report (id INT IDENTITY NOT NULL, created_by_id INT, file_id INT, uuid NVARCHAR(255) NOT NULL, status INT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), tool_id INT, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_739441273FF8AF4 ON queued_report (tool_id)');
        $this->addSql('CREATE INDEX IDX_7394412B03A8386 ON queued_report (created_by_id)');
        $this->addSql('CREATE INDEX IDX_739441293CB796C ON queued_report (file_id)');


//        $this->addSql('ALTER TABLE queued_report ADD CONSTRAINT FK_739441273FF8AF4 FOREIGN KEY (tool_id) REFERENCES AtlasDB_tools.dbo.tool (id)');
//        $this->addSql('ALTER TABLE queued_report ADD CONSTRAINT FK_7394412B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)');
//        $this->addSql('ALTER TABLE queued_report ADD CONSTRAINT FK_739441293CB796C FOREIGN KEY (file_id) REFERENCES files (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE queued_report DROP CONSTRAINT FK_739441273FF8AF4');
        $this->addSql('DROP TABLE queued_report');


    }
}
