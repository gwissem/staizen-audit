<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190709081150 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('CREATE TABLE survey_answer (id INT IDENTITY NOT NULL, survey_id INT NOT NULL, checked_button NVARCHAR(255), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE TABLE survey (id INT IDENTITY NOT NULL, uid NVARCHAR(255), rootId INT NOT NULL, phone_number NVARCHAR(255) NOT NULL, state INT NOT NULL,  version int DEFAULT 1 NULL, more_text text NULL, rating INT,type int, recommendation_rating INT, active BIT NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('ALTER TABLE survey ADD CONSTRAINT survey_created_at_default DEFAULT GETDATE() FOR created_at');
        $this->addSql('ALTER TABLE survey ADD CONSTRAINT survey_updated_at_default DEFAULT GETDATE() FOR updated_at');

    }

    /**m
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE IF EXISTS survey_answer');
        $this->addSql('DROP TABLE IF EXISTS survey');
    }
}
