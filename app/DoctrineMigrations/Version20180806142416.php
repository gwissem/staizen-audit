<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180806142416 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE vip ADD company_name VARCHAR(MAX)');
        $this->addSql('ALTER TABLE vip ADD position VARCHAR(MAX)');
        $this->addSql('ALTER TABLE vip ADD type INT');
        $this->addSql('ALTER TABLE vip ADD CONSTRAINT DF_4B076C22_8CDE5729 DEFAULT 1 FOR type');
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');
        $this->addSql('ALTER TABLE vip DROP COLUMN company_name');
        $this->addSql('ALTER TABLE vip DROP COLUMN position');
        $this->addSql('ALTER TABLE vip DROP COLUMN type');
    }
}
