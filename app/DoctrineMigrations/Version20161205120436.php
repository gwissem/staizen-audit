<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205120436 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN confirmation_description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN [description] NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN sub_query_description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN slug NVARCHAR(255)');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN word_template NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_group_translation ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_group_translation ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN tool_id INT');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN url NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN [filters] NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE company ALTER COLUMN full_name NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE company ALTER COLUMN address NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE fos_user ALTER COLUMN cucum_extension INT');
        $this->addSql('ALTER TABLE files ALTER COLUMN mime_type NVARCHAR(255)');
        $this->addSql('ALTER TABLE files ALTER COLUMN file_size INT');
        $this->addSql('ALTER TABLE files ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE files ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE documents ALTER COLUMN name NVARCHAR(255)');
        $this->addSql('ALTER TABLE documents ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE documents ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN ssl BIT');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN last_processed DATETIME2(6)');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN last_run DATETIME2(6)');
        $this->addSql('ALTER TABLE mail_container ALTER COLUMN mailbox_id INT');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN user_id INT');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN redirect_uri NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN expires_at INT');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN scope NVARCHAR(255)');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN user_id INT');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN expires_at INT');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN scope NVARCHAR(255)');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN user_id INT');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN expires_at INT');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN scope NVARCHAR(255)');
        $this->addSql('ALTER TABLE client ALTER COLUMN name NVARCHAR(255)');
        $this->addSql('ALTER TABLE ticket ALTER COLUMN content NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE ticket ALTER COLUMN created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE ticket ALTER COLUMN updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE ticket ALTER COLUMN request_info NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE process_flow ADD graph_points NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE term ALTER COLUMN description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE attribute ALTER COLUMN regex NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE step ALTER COLUMN id NVARCHAR(20) NOT NULL');
        $this->addSql('ALTER TABLE step ALTER COLUMN query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE step ALTER COLUMN interval INT');
        $this->addSql('ALTER TABLE vin_header ALTER COLUMN query NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE step_object_attribute_definition ALTER COLUMN default_value NVARCHAR(MAX) NOT NULL');
        $this->addSql('ALTER TABLE attribute_value ALTER COLUMN value_text NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN output NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN errorOutput NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN exitCode SMALLINT');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN maxRuntime SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN maxRetries SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN runtime SMALLINT');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN memoryUsage INT');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN memoryUsageReal INT');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN originalJob_id BIGINT');
        $this->addSql('ALTER TABLE jms_job_dependencies ALTER COLUMN source_job_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE jms_job_dependencies ALTER COLUMN dest_job_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE jms_job_statistics ALTER COLUMN job_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE jms_job_related_entities ALTER COLUMN job_id BIGINT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE access_token ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE access_token ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE attribute ALTER COLUMN regex NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE attribute_value ALTER COLUMN value_text NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN redirect_uri NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE auth_code ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE client ALTER COLUMN name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE company ALTER COLUMN full_name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE company ALTER COLUMN address NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN ssl BIT NOT NULL');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN procedure_text NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN last_processed DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE connected_mailbox ALTER COLUMN last_run DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE documents ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN mime_type NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN file_size INT NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE files ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE filter_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE fos_user ALTER COLUMN cucum_extension INT');
        $this->addSql('ALTER TABLE jms_job_dependencies ALTER COLUMN source_job_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE jms_job_dependencies ALTER COLUMN dest_job_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE jms_job_related_entities ALTER COLUMN job_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE jms_job_statistics ALTER COLUMN job_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN output NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN errorOutput NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN exitCode SMALLINT');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN maxRuntime SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN maxRetries SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN runtime SMALLINT');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN memoryUsage INT');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN memoryUsageReal INT');
        $this->addSql('ALTER TABLE jms_jobs ALTER COLUMN originalJob_id BIGINT');
        $this->addSql('ALTER TABLE mail_container ALTER COLUMN mailbox_id INT NOT NULL');
        $this->addSql('ALTER TABLE process_flow DROP COLUMN graph_points');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN user_id INT NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN expires_at INT NOT NULL');
        $this->addSql('ALTER TABLE refresh_token ALTER COLUMN scope NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE step ALTER COLUMN id NVARCHAR(20) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE step ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE step ALTER COLUMN interval INT NOT NULL');
        $this->addSql(
            'ALTER TABLE step_object_attribute_definition ALTER COLUMN default_value NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
        $this->addSql('ALTER TABLE style ALTER COLUMN css NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style ALTER COLUMN link_params NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE style_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE term ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE ticket ALTER COLUMN request_info NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE ticket ALTER COLUMN content NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE ticket ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE ticket ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool ALTER COLUMN sub_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN insert_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN update_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN delete_query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool ALTER COLUMN edit_id_column NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_action ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql(
            'ALTER TABLE tool_action_translation ALTER COLUMN confirmation_description NVARCHAR(255) COLLATE Polish_CI_AS'
        );
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_action_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN target_value NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field ALTER COLUMN word_template NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_form_field_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_group_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_group_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_group_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN filters NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_summary_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN translatable_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE tool_translation ALTER COLUMN sub_query_description NVARCHAR(255) COLLATE Polish_CI_AS'
        );
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN slug NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_translation ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN tool_id INT NOT NULL');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN url NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN filters NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN created_at DATETIME2(6) NOT NULL');
        $this->addSql('ALTER TABLE tool_xls_hash ALTER COLUMN updated_at DATETIME2(6) NOT NULL');
        $this->addSql(
            'ALTER TABLE user_filter_set ALTER COLUMN filters_string NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL'
        );
        $this->addSql('ALTER TABLE vin_header ALTER COLUMN query NVARCHAR(255) COLLATE Polish_CI_AS');
    }
}
