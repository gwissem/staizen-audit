<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161201110127 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE jabber_voice_conversation (id INT IDENTITY NOT NULL, type NVARCHAR(16) NOT NULL, status NVARCHAR(32) NOT NULL, id_dialog INT NOT NULL, departament_code NVARCHAR(32) NOT NULL, from_address NVARCHAR(64) NOT NULL, to_address NVARCHAR(64) NOT NULL, conversation_start_time DATETIME2(6) NOT NULL, conversation_finish_time DATETIME2(6), created_at DATETIME2(6) NOT NULL, updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_65B0316A63566D8F ON jabber_voice_conversation (id_dialog) WHERE id_dialog IS NOT NULL');
        $this->addSql('CREATE INDEX fk_type_key ON jabber_voice_conversation (type)');
        $this->addSql('CREATE INDEX fk_status_key ON jabber_voice_conversation (status)');
        $this->addSql('CREATE INDEX fk_from_address_key ON jabber_voice_conversation (from_address)');
        $this->addSql('CREATE INDEX fk_to_address_key ON jabber_voice_conversation (to_address)');
        $this->addSql('CREATE TABLE jabber_voice_conversation_member (id INT IDENTITY NOT NULL, id_conversation INT NOT NULL, role NVARCHAR(64) NOT NULL, is_conversation_master BIT NOT NULL, address NVARCHAR(64) NOT NULL, id_author INT, created_at DATETIME2(6) NOT NULL, updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX fk_id_conversation_key ON jabber_voice_conversation_member (id_conversation)');
        $this->addSql('CREATE INDEX fk_role_key ON jabber_voice_conversation_member (role)');
        $this->addSql('CREATE INDEX fk_address_key ON jabber_voice_conversation_member (address)');
        $this->addSql('CREATE INDEX fk_author_key ON jabber_voice_conversation_member (id_author)');
        $this->addSql('CREATE TABLE jabber_voice_conversation_cucm_server (id INT IDENTITY NOT NULL, address NVARCHAR(255) NOT NULL, priority SMALLINT NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FDD0790D4E6F81 ON jabber_voice_conversation_cucm_server (address) WHERE address IS NOT NULL');
        $this->addSql('CREATE INDEX fk_priority_key ON jabber_voice_conversation_cucm_server (priority)');
        $this->addSql('ALTER TABLE jabber_voice_conversation_member ADD CONSTRAINT FK_7508D57DA94F539B FOREIGN KEY (id_conversation) REFERENCES jabber_voice_conversation (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE company ADD full_name NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE company ADD address NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE company ADD external_account_id INT');
        $this->addSql('ALTER TABLE company ADD type INT');
        $this->addSql('ALTER TABLE company ADD identifier_number INT');
        $this->addSql('ALTER TABLE fos_user ADD cucm_login NVARCHAR(255)');
        $this->addSql('ALTER TABLE fos_user ADD cucm_password NVARCHAR(255)');
        $this->addSql('ALTER TABLE fos_user ADD cucum_extension INT');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE jabber_voice_conversation_member DROP CONSTRAINT FK_7508D57DA94F539B');
        $this->addSql('DROP TABLE jabber_voice_conversation');
        $this->addSql('DROP TABLE jabber_voice_conversation_member');
        $this->addSql('DROP TABLE jabber_voice_conversation_cucm_server');
        $this->addSql('ALTER TABLE company DROP COLUMN full_name');
        $this->addSql('ALTER TABLE company DROP COLUMN address');
        $this->addSql('ALTER TABLE company DROP COLUMN external_account_id');
        $this->addSql('ALTER TABLE company DROP COLUMN type');
        $this->addSql('ALTER TABLE company DROP COLUMN identifier_number');
        $this->addSql('ALTER TABLE fos_user DROP COLUMN cucm_login');
        $this->addSql('ALTER TABLE fos_user DROP COLUMN cucm_password');
        $this->addSql('ALTER TABLE fos_user DROP COLUMN cucum_extension');
    }
}
