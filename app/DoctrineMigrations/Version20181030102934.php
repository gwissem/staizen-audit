<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181030102934 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

 
        $this->addSql('CREATE TABLE AtlasDB_v.dbo.scenario_instance (id INT IDENTITY NOT NULL, user_id INT, root_id INT, last_process_instance_id INT, scenario_id INT NOT NULL, created_at DATETIME2(6) NOT NULL, deleted_at DATETIME2(6), time INT NOT NULL, error_message VARCHAR(MAX), result VARCHAR(MAX), PRIMARY KEY (id))');
        $this->addSql('CREATE SYNONYM dbo.platform_special_conditions for AtlasDB_def.dbo.platform_special_conditions');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        
        $this->addSql('DROP TABLE AtlasDB_v.dbo.scenario_instance');
    }
}
