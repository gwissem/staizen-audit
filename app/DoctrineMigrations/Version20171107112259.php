<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171107112259 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'unique_label_locale\')
                            ALTER TABLE form_control_translation DROP CONSTRAINT unique_label_locale
                        ELSE
                            DROP INDEX unique_label_locale ON form_control_translation');
        $this->addSql('ALTER TABLE form_control DROP COLUMN label');
        $this->addSql('CREATE UNIQUE NONCLUSTERED INDEX unique_label_locale ON form_control_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');

        $this->addSql("UPDATE form_control_translation SET label='Czy kontaktuje się ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` w sprawie wypadku?' WHERE id=50");
        $this->addSql("UPDATE form_control_translation SET label='Zanim przejdziemy do pobierania danych pojazdu z dowodu rejestracyjnego, jestem zobowiązany/a do odczytania ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Panu'' : ''Pani''```  zgody dotyczącej przetwarzania danych osobowych.' WHERE id=53");
        $this->addSql("UPDATE form_control_translation SET label='Brak danych z dowodu rejestracyjnego uniemożliwia weryfikację uprawnień do świadczeń Assistance. Proszę skontaktować się z nami w momencie kiedy będzie ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` w posiadaniu tych danych.' WHERE id=62");
        $this->addSql("UPDATE form_control_translation SET label='Czy jest ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` przy samochodzie?' WHERE id=63");
        $this->addSql("UPDATE form_control_translation SET label='Czy wyraża ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` zgodę na udostępnianie ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pana'' : ''Pani''``` danych osobowych w zakresie (imię, nazwisko, numer telefonu kontaktowego) oraz danych związanych z przypadkiem pomocy drogowej do Volkswagen AG w celu przeprowadzenia jednorazowego badania ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pana'' : ''Pani''``` zadowolenia w zakresie udzielonej ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Panu'' : ''Pani''``` pomocy drogowej związanej z ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pana'' : ''Pani''``` samochodem?' WHERE id=74");
        $this->addSql("UPDATE form_control_translation SET label='Zgodnie z Ustawą o ochronie danych osobowych z dnia 29.08.1997 r. informuje, że ma ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` prawo wglądu do treści swoich danych osobowych oraz ich poprawiania. ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pana'' : ''Pani''``` zgoda na udostępnienie danych jest dobrowolna. Czy zrozumiał ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` treść tej informacji?' WHERE id=75");
        $this->addSql("UPDATE form_control_translation SET label='Ponownie odczytam ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Panu'' : ''Pani''``` treść zgody.' WHERE id=77");
        $this->addSql("UPDATE form_control_translation SET translatable_id=176, label='Czy kontaktuje się ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` w sprawie wypadku?' WHERE id=82");
        $this->addSql("UPDATE form_control_translation SET label='Czy ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` jest kierowcą?' WHERE id=83");
        $this->addSql("UPDATE form_control_translation SET label='Czy jest ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` w stanie odczytać aktualny przebieg samochodu? Jeśli tak, to poproszę o tą informację.' WHERE id=89");
        $this->addSql("UPDATE form_control_translation SET label='Witam. Dzwonię z centrum zgłoszeniowego {@253@}. Otrzymaliśmy prośbę o udzielenie ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Panu'' : ''Pani''``` pomocy. Czy mam przyjemność z ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Panem'' : ''Panią''``` {@80,342,64@} {@80,342,66@}?' WHERE id=102");
        $this->addSql("UPDATE form_control_translation SET label='Czy jest ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` kierowcą pojazdu {@74,73@} o numerzej rejestracyjnym {@74,72@}?' WHERE id=106");
        $this->addSql("UPDATE form_control_translation SET label='Czy wyraża ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` zgodę na prowadzenie korespondencji dotyczącej przedmiotowej szkody drogą elektroniczną na adres e-mail?' WHERE id=168");
        $this->addSql("UPDATE form_control_translation SET label='Czy wyraża ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` zgodę na prowadzenie korespondencji dotyczącej przedmiotowej szkody drogą elektroniczną na adres e-mail?'  WHERE id=170");
        $this->addSql("UPDATE form_control_translation SET label='Czy posiada ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` inne auto, które może użytkowac zamiennie za uszkodzony pojazd, czy będzie ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` potrzebował pojazd zastępczy na czas naprawy pojazdu?'  WHERE id=181");
        $this->addSql("UPDATE form_control_translation SET label='Czy posiada ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` inne auto, które może użytkowac zamiennie za uszkodzony pojazd, czy będzie ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` potrzebował pojazd zastępczy na czas naprawy pojazdu?'  WHERE id=182");
        $this->addSql("UPDATE form_control_translation SET label='Przekażemy ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pana'' : ''Pani''``` kontakt do wypożyczalni preferowanej dla LeasePlan Ubezpieczenia, który skontaktuje się z ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Panem'' : ''Panią''``` i przedstawi szczegóły wynajmu. Czy wyraża ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` na to zgodę?'  WHERE id=183");
        $this->addSql("UPDATE form_control_translation SET label='Przekażemy ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pana'' : ''Pani''``` kontakt do wypożyczalni preferowanej dla LeasePlan Ubezpieczenia, który skontaktuje się z ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Panem'' : ''Panią''``` i przedstawi szczegóły wynajmu. Czy wyraża ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` na to zgodę?'  WHERE id=184");
        $this->addSql("UPDATE form_control_translation SET label='W ciągu 7 dni roboczych w sprawie likwidacji szkody skontaktuje się z ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Panem'' : ''Panią''``` ubezpieczyciel i poinformuje o dalszym procesie likwidacji. Nr ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pana'' : ''Pani''``` zgłoszenia to: - proszę o zapisanie numeru.'  WHERE id=186");
        $this->addSql("UPDATE form_control_translation SET label='Czy chce ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` przejść do procesu dodatkowej weryfikacji uprawnień?'  WHERE id=294");
        $this->addSql("UPDATE form_control_translation SET label='Czy pakiet assistance ```{#gender(80,342,64|418,342,64)#} == 1 ? ''otrzymał Pan'' : ''otrzymała Pani''``` wraz z ubezpieczeniem? Jeśli tak – w jakiej firmie ubezpieczony jest samochód?'  WHERE id=307");
        $this->addSql("UPDATE form_control_translation SET label='Czy ```{#gender(80,342,64|418,342,64)#} == 1 ? ''otrzymał Pan'' : ''otrzymała Pani''``` pakiet assistance do produktów bankowych (np. konta)?'  WHERE id=308");
        $this->addSql("UPDATE form_control_translation SET label='Czy posiada ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` pakiet assistance związany z leasingiem? '  WHERE id=310");
        $this->addSql("UPDATE form_control_translation SET label='Czy pakiet assistance ```{#gender(80,342,64|418,342,64)#} == 1 ? ''otrzymał Pan'' : ''otrzymała Pani''``` wraz z ubezpieczeniem? Jeśli tak – w jakiej firmie ubezpieczony jest samochód?'  WHERE id=360");
        $this->addSql("UPDATE form_control_translation SET label='Czy pakiet assistance ```{#gender(80,342,64|418,342,64)#} == 1 ? ''otrzymał Pan'' : ''otrzymała Pani''``` wraz z'  WHERE id=361");
        $this->addSql("UPDATE form_control_translation SET label='Czy posiada ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` pakiet assistance związany z leasingiem? '  WHERE id=364");
        $this->addSql("UPDATE form_control_translation SET label='Czy ```{#gender(80,342,64|418,342,64)#} == 1 ? ''otrzymał Pan'' : ''otrzymała Pani''``` pakiet assistance do produktów bankowych (np. konta)?'  WHERE id=368");
        $this->addSql("UPDATE form_control_translation SET label='W celu zachowania bezpieczeństwa proszę o włączenie świateł awaryjnych, ubranie kamizelki odblaskowej oraz wystawienie trójkąta ostrzegawczego, proszę też aby ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` oraz pasażerowie oczekiwali na pomoc poza pojazdem, w miejscu bezpiecznym np. za barierkami'  WHERE id=400");
        $this->addSql("UPDATE form_control_translation SET label='Czy mimo to może ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` podać imię i nazwisko oraz numer telefonu do kierowcy? Dane te mogą być przydatne w przypadku organizacji usług na rzecz tej osoby.'  WHERE id=455");
        $this->addSql("UPDATE form_control_translation SET label='Proszę o kontakt pod numer 112 w celu wezwania odpowiednich służb, które udzielą Państwu pomocy na miejscu aby nic nie zagrażało zdrowiu i życiu uczestników kolizji. Po zakończonej rozmowie otrzyma ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` SMS z numerem zgłoszenia i instrukcją jak postępować dalej. '  WHERE id=478");
        $this->addSql("UPDATE form_control_translation SET label='Proszę powiedzieć, jakiego rodzaju pakiet assistance ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` posiada?' WHERE id=486");
        $this->addSql("UPDATE form_control_translation SET label='Czy przeprowadza ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` regularne przeglądy i naprawy samochodu u polskich autoryzowanych partnerów serwisowych marki {@253@}?' WHERE id=487");
        $this->addSql("UPDATE form_control_translation SET label='Czy ```{#gender(80,342,64|418,342,64)#} == 1 ? ''zakupił Pan'' : ''zakupiła Pani''``` samochód jako nowy u polskiego autoryzowanego dealera marki {@253@}?' WHERE id=490");
        $this->addSql("UPDATE form_control_translation SET label='Czy ```{#gender(80,342,64|418,342,64)#} == 1 ? ''mógłby Pan'' : ''mogłaby Pani''``` podać datę zakupu samochodu? Zapisana jest ona w książce gwarancyjnej lub na fakturze.' WHERE id=494");
        $this->addSql("UPDATE form_control_translation SET label='Czy przy zakupie nowego samochodu, ```{#gender(80,342,64|418,342,64)#} == 1 ? ''wykupił Pan'' : ''wykupiła Pani''``` opcję przedłużonej gwarancji wraz z pakietem assistance?' WHERE id=496");
        $this->addSql("UPDATE form_control_translation SET label='Proszę powiedzieć, do kiedy powinien być przeprowadzony następny przegląd? Jeśli nie jest ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` w stanie określić tego dokładnie, proszę podać, co jaki czas jest ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan zobowiązany'' : ''Pani zobowiązana''``` do przeprowadzania przeglądów okresowych.' WHERE id=499");
        $this->addSql("UPDATE form_control_translation SET label='Warunkowo udzielimy podstawowych świadczeń oraz podejmiemy próbę ich zweryfikowania najszybciej jak to możliwe. W przypadku pozytywnej weryfikacji będziemy mogli zaoferować wszystkie dostępne w programie usługi assistance, w przeciwnym przypadku zaś wszelkie udzielone do tej pory oraz w przyszłości świadczenia będą dla ```{#gender(80,342,64|418,342,64)#} == 1 ? ''Pan'' : ''Pani''``` odpłatne.'  WHERE id=525");


        $this->addSql(" UPDATE form_control_translation SET label='Czy mam przyjemność rozmawiać z ```{#gender(81,342,64)#} == 1 ? ''Panem'' : ''Panią''```...' WHERE id=15");
        $this->addSql(" UPDATE form_control_translation SET label='Proszę podać imię i nazwisko oraz nr telefonu kontaktowego do ```{#gender(81,342,64)#} == 1 ? ''Pana'' : ''Pani''```' WHERE id=18");
        $this->addSql(" UPDATE form_control_translation SET label='Czy dzwoni ```{#gender(81,342,64)#} == 1 ? ''Pan'' : ''Pani''``` w istniejącej sprawie?' WHERE id=23");
        $this->addSql(" UPDATE form_control_translation SET label='Proszę o podanie ```{#gender(81,342,64)#} == 1 ? ''Pana'' : ''Pani''``` imienia i nazwiska' WHERE id=188");
        $this->addSql(" UPDATE form_control_translation SET label='Proszę o podanie ```{#gender(81,342,64)#} == 1 ? ''Pana'' : ''Pani''``` numeru telefonu' WHERE id=189");
        $this->addSql(" UPDATE form_control_translation SET label='Czy mam przyjemność rozmawiać z ```{#gender(81,342,64)#} == 1 ? ''Panem'' : ''Panią''``` {@81,342,64@} {@81,342,66@}' WHERE id=190");
        $this->addSql(" UPDATE form_control_translation SET label='Czy dzwoni ```{#gender(81,342,64)#} == 1 ? ''Pan'' : ''Pani''``` w sprawie już istniejącej?', created_at='2017-10-13 13:32:03.513000' WHERE id=194");
        $this->addSql(" UPDATE form_control_translation SET label='Czy dzwoni ```{#gender(81,342,64)#} == 1 ? ''Pan'' : ''Pani''``` w sprawie dotyczącej', created_at='2017-10-13 13:32:03.513000' WHERE id=196");
        $this->addSql(" UPDATE form_control_translation SET label='Proszę o potwierdzenie ```{#gender(81,342,64)#} == 1 ? ''Pana'' : ''Pani''``` imienia i nazwiska', created_at='2017-10-13 13:32:03.513000' WHERE id=331");
        $this->addSql(" UPDATE form_control_translation SET label='Czy możemy się kontaktować z ```{#gender(81,342,64)#} == 1 ? ''Panem'' : ''Panią''``` z numerem z którego ```{#gender(81,342,64)#} == 1 ? ''Pan'' : ''Pani''``` dzwoni?' WHERE id=378");


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE form_control ADD label NVARCHAR(4000) COLLATE Polish_CI_AS');
        $this->addSql('CREATE UNIQUE NONCLUSTERED INDEX unique_label_locale ON form_control_translation (translatable_id, locale, label) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL AND label IS NOT NULL');
        ;
    }
}
