<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205123548 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD29044573B21E9C');
        $this->addSql('ALTER TABLE process_flow ADD next_step_id NVARCHAR(20)');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN description NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN graph_points NVARCHAR(MAX)');
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD290445B13C343E FOREIGN KEY (next_step_id) REFERENCES step (id) ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD29044573B21E9C FOREIGN KEY (step_id) REFERENCES step (id)'
        );
        $this->addSql('CREATE INDEX IDX_BD290445B13C343E ON process_flow (next_step_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD290445B13C343E');
        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD29044573B21E9C');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_BD290445B13C343E\')
                            ALTER TABLE process_flow DROP CONSTRAINT IDX_BD290445B13C343E
                        ELSE
                            DROP INDEX IDX_BD290445B13C343E ON process_flow'
        );
        $this->addSql('ALTER TABLE process_flow DROP COLUMN next_step_id');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN description NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN graph_points NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD29044573B21E9C FOREIGN KEY (step_id) REFERENCES step (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );

    }
}
