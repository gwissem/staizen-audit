<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180705142921 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.user_role_group_task (id INT IDENTITY NOT NULL, platform_id INT, user_role_group_id INT, user_group_id INT, created_by_id INT, updated_by_id INT, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE TABLE AtlasDB_def.dbo.user_role_group (id INT IDENTITY NOT NULL, created_by_id INT, updated_by_id INT, name NVARCHAR(255) NOT NULL, active SMALLINT NOT NULL, deleted_at DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE TABLE AtlasDB_def.dbo.platform_group (id INT IDENTITY NOT NULL, created_by_id INT, updated_by_id INT, name NVARCHAR(255) NOT NULL, active SMALLINT NOT NULL, deleted_at DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_group ADD type SMALLINT');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_group ADD CONSTRAINT DF_8F02BF9D_8CDE5729 DEFAULT 0 FOR type');

        $this->addSql('CREATE INDEX IDX_E6CC86F6FFE6496F ON AtlasDB_def.dbo.user_role_group_task (platform_id)');
        $this->addSql('CREATE INDEX IDX_E6CC86F63A099226 ON AtlasDB_def.dbo.user_role_group_task (user_role_group_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3D7B6A065E237E06 ON AtlasDB_def.dbo.user_role_group (name) WHERE name IS NOT NULL');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_role_group ADD CONSTRAINT DF_3D7B6A06_4B1EFC02 DEFAULT 1 FOR active');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform_group ADD CONSTRAINT DF_5F9C2041_4B1EFC02 DEFAULT 1 FOR active');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_role_group_task ADD CONSTRAINT FK_E6CC86F6FFE6496F FOREIGN KEY (platform_id) REFERENCES AtlasDB_def.dbo.platform (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_role_group_task ADD CONSTRAINT FK_E6CC86F63A099226 FOREIGN KEY (user_role_group_id) REFERENCES AtlasDB_def.dbo.user_role_group (id) ON DELETE SET NULL');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform ADD platform_group_id INT');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform ADD CONSTRAINT FK_3952D0CB7B24D955 FOREIGN KEY (platform_group_id) REFERENCES AtlasDB_def.dbo.platform_group (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_3952D0CB7B24D955 ON AtlasDB_def.dbo.platform (platform_group_id)');

        $this->addSql('ALTER TABLE fos_user ADD role_group_id INT');
        $this->addSql('CREATE INDEX IDX_957A6479D4873F76 ON fos_user (role_group_id)');


        $this->addSql('ALTER TABLE process_instance ADD urgent BIT');
        $this->addSql('ALTER TABLE process_instance ADD priority_changed_at DATETIME2(6)');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step ADD priority SMALLINT');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step ADD priority_boost_interval SMALLINT');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step ADD urgent_priority_boost SMALLINT');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step ADD max_priority SMALLINT');



    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_role_group_task DROP CONSTRAINT FK_E6CC86F61ED93D47');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_role_group_task DROP CONSTRAINT FK_E6CC86F63A099226');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_role_group_task DROP CONSTRAINT FK_E6CC86F6FFE6496F');


        $this->addSql('DROP TABLE AtlasDB_def.dbo.user_role_group_task');
        $this->addSql('DROP TABLE AtlasDB_def.dbo.user_role_group');
        $this->addSql('DROP TABLE AtlasDB_def.dbo.platform_group');

        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_957A6479D4873F76\')
                            ALTER TABLE fos_user DROP CONSTRAINT IDX_957A6479D4873F76
                        ELSE
                            DROP INDEX IDX_957A6479D4873F76 ON fos_user');
        $this->addSql('ALTER TABLE fos_user DROP COLUMN role_group_id');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.platform DROP COLUMN platform_group_id');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step DROP COLUMN priority');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step DROP COLUMN priority_boost_interval');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step DROP COLUMN urgent_priority_boost');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step DROP COLUMN max_priority');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.user_group DROP COLUMN type');


    }
}
