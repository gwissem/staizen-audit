<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190618100023 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE dbo.schedule_telephony_status (id INT IDENTITY NOT NULL, user_id INT, start_date DATETIME2(6) NOT NULL, end_date DATETIME2(6) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_89DFAB98A76ED395 ON schedule_telephony_status (user_id)');
        $this->addSql('ALTER TABLE dbo.schedule_telephony_status ADD label NVARCHAR(20) NULL');
        $this->addSql('ALTER TABLE dbo.schedule_telephony_status ADD CONSTRAINT FK_89DFAB98A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE schedule_telephony_status');

    }
}
