<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180724095117 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.service_editor_translation (id INT IDENTITY NOT NULL, translatable_id INT, name NVARCHAR(500), description VARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_6BEED87D2C2AC5D3 ON AtlasDB_def.dbo.service_editor_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX service_editor_translation_unique_translation ON AtlasDB_def.dbo.service_editor_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');

        $this->addSql('CREATE TABLE AtlasDB_def.dbo.service_editor (id INT IDENTITY NOT NULL, service_definition_id INT, step_id NVARCHAR(20), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_F236F48CED5CA9E6 ON AtlasDB_def.dbo.service_editor (service_definition_id)');
        $this->addSql('CREATE INDEX IDX_F236F48C636669A8 ON AtlasDB_def.dbo.service_editor (step_id)');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_editor_translation ADD CONSTRAINT FK_6BEED87D2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES AtlasDB_def.dbo.service_editor (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_editor ADD CONSTRAINT FK_F236F48CED5CA9E6 FOREIGN KEY (service_definition_id) REFERENCES AtlasDB_def.dbo.service_definition (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_editor ADD CONSTRAINT FK_F236F48C636669A8 FOREIGN KEY (step_id) REFERENCES step (id) ON DELETE SET NULL');

        $this->addSql('Create synonym dbo.service_editor_translation for AtlasDB_def.dbo.service_editor_translation');
        $this->addSql('Create synonym dbo.service_editor for AtlasDB_def.dbo.service_editor');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_editor DROP CONSTRAINT FK_F236F48CED5CA9E6');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.service_editor_translation DROP CONSTRAINT FK_6BEED87D2C2AC5D3');

        $this->addSql('DROP TABLE AtlasDB_def.dbo.service_editor_translation');

        $this->addSql('DROP TABLE AtlasDB_def.dbo.service_editor');


    }
}
