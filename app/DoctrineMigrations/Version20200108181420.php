<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20200108181420 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
          ALTER TABLE dbo.fos_user ADD last_shown_efficiency_popup datetime DEFAULT null  NULL"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("
            ALTER TABLE dbo.fos_user DROP COLUMN last_shown_efficiency_popup"
        );

    }
}
