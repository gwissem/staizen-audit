<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170130091335 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_flow DROP CONSTRAINT FK_BD290445A3FD16B8');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_BD290445A3FD16B8\')
                            ALTER TABLE process_flow DROP CONSTRAINT IDX_BD290445A3FD16B8
                        ELSE
                            DROP INDEX IDX_BD290445A3FD16B8 ON process_flow'
        );
        $this->addSql('ALTER TABLE process_flow DROP COLUMN new_process_id');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN graph_points NVARCHAR(MAX)');
        $this->addSql('ALTER TABLE process_instance ADD active BIT NOT NULL');
        $this->addSql('ALTER TABLE process_instance ADD CONSTRAINT DF_B4C1EF5F_4B1EFC02 DEFAULT \'0\' FOR active');
        $this->addSql('ALTER TABLE process_instance ADD description NVARCHAR(MAX)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE process_flow ADD new_process_id INT');
        $this->addSql('ALTER TABLE process_flow ALTER COLUMN graph_points NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql(
            'ALTER TABLE process_flow ADD CONSTRAINT FK_BD290445A3FD16B8 FOREIGN KEY (new_process_id) REFERENCES process_instance (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_BD290445A3FD16B8 ON process_flow (new_process_id)');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN active');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN description');
    }
}
