<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161027115602 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE ticket ADD created_by_original_id INT');
        $this->addSql(
            'ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3B0B2CEF2 FOREIGN KEY (created_by_original_id) REFERENCES fos_user (id)'
        );
        $this->addSql('CREATE INDEX IDX_97A0ADA3B0B2CEF2 ON ticket (created_by_original_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );


        $this->addSql('ALTER TABLE ticket DROP CONSTRAINT FK_97A0ADA3B0B2CEF2');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_97A0ADA3B0B2CEF2\')
                            ALTER TABLE ticket DROP CONSTRAINT IDX_97A0ADA3B0B2CEF2
                        ELSE
                            DROP INDEX IDX_97A0ADA3B0B2CEF2 ON ticket'
        );
        $this->addSql('ALTER TABLE ticket DROP COLUMN created_by_original_id');
    }
}
