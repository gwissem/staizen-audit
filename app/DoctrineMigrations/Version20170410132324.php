<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170410132324 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE arrowchat');
        $this->addSql('DROP TABLE arrowchat_admin');
        $this->addSql('DROP TABLE arrowchat_applications');
        $this->addSql('DROP TABLE arrowchat_banlist');
        $this->addSql('DROP TABLE arrowchat_chatroom_messages');
        $this->addSql('DROP TABLE arrowchat_chatroom_rooms');
        $this->addSql('DROP TABLE arrowchat_chatroom_users');
        $this->addSql('DROP TABLE arrowchat_config');
        $this->addSql('DROP TABLE arrowchat_graph_log');
        $this->addSql('DROP TABLE arrowchat_notifications');
        $this->addSql('DROP TABLE arrowchat_notifications_markup');
        $this->addSql('DROP TABLE arrowchat_reports');
        $this->addSql('DROP TABLE arrowchat_smilies');
        $this->addSql('DROP TABLE arrowchat_status');
        $this->addSql('DROP TABLE arrowchat_themes');
        $this->addSql('DROP TABLE arrowchat_trayicons');
        $this->addSql('DROP TABLE arrowchat_warnings');

        $this->addSql('ALTER TABLE tool ADD column_for_auto_complete NVARCHAR(100)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE tool DROP COLUMN column_for_auto_complete');

    }
}
