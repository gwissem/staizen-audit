<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191029090047 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("
                IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'refresh_tasks_log')
                BEGIN
                    
                    CREATE TABLE dbo.refresh_tasks_log (
                        id INT IDENTITY(1,1) NOT NULL,
                        filterWord NVARCHAR(1000),
                        createdBy INT,
                        requestDateTime DATETIME,
                        requestTime DECIMAL(15,5),
                        refreshTime DECIMAL(15,5),
                        advancedFilters NVARCHAR(MAX)
                        CONSTRAINT PK__id__refresh_tasks_log PRIMARY KEY (id)
                    )
                        
                END
        ");




    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'refresh_tasks_log')
            BEGIN
                
                DROP TABLE dbo.refresh_tasks_log
                
            END");

    }
}
