<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170220155458 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE program_service_translation (id INT IDENTITY NOT NULL, translatable_id INT, description NVARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_D089E8532C2AC5D3 ON program_service_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX program_service_translation_unique_translation ON program_service_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('CREATE TABLE program_service (id INT IDENTITY NOT NULL, program_id INT, start_step_id NVARCHAR(20), icon NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_6A7762063EB8070A ON program_service (program_id)');
        $this->addSql('CREATE INDEX IDX_6A7762068377424F ON program_service (start_step_id)');
        $this->addSql('CREATE TABLE program_translation (id INT IDENTITY NOT NULL, translatable_id INT, description NVARCHAR(MAX), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_787308072C2AC5D3 ON program_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX program_translation_unique_translation ON program_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL');
        $this->addSql('ALTER TABLE program_service_translation ADD CONSTRAINT FK_D089E8532C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES program_service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program_service ADD CONSTRAINT FK_6A7762063EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program_service ADD CONSTRAINT FK_6A7762068377424F FOREIGN KEY (start_step_id) REFERENCES step (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE program_translation ADD CONSTRAINT FK_787308072C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES vin_program (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE program_services');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE program_service_translation DROP CONSTRAINT FK_D089E8532C2AC5D3');
        $this->addSql('CREATE TABLE program_services (program_id INT NOT NULL, service_id INT NOT NULL, PRIMARY KEY (program_id, service_id))');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_F2071D5C3EB8070A ON program_services (program_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_F2071D5CED5CA9E6 ON program_services (service_id)');
        $this->addSql('ALTER TABLE program_services ADD CONSTRAINT FK_F2071D5C3EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE program_services ADD CONSTRAINT FK_F2071D5CED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('DROP TABLE program_service_translation');
        $this->addSql('DROP TABLE program_service');
        $this->addSql('DROP TABLE program_translation');

    }
}
