<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160923120348 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE connected_mailbox (id INT IDENTITY NOT NULL, name NVARCHAR(255) NOT NULL, host NVARCHAR(255) NOT NULL, port INT NOT NULL, protocol NVARCHAR(10) NOT NULL, ssl BIT, login NVARCHAR(255) NOT NULL, password NVARCHAR(255) NOT NULL, connection_string NVARCHAR(255), procedure_text NVARCHAR(MAX), amount_processed_emails INT NOT NULL, last_processed DATETIME2(6), last_run DATETIME2(6), interval_cron INT NOT NULL, PRIMARY KEY (id))');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE connected_mailbox');
    }
}
