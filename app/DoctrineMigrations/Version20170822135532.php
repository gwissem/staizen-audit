<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170822135532 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE sparx_attributes (id INT IDENTITY NOT NULL, tree_root INT, parent_id INT, title NVARCHAR(64) NOT NULL, lft INT NOT NULL, lvl INT NOT NULL, rgt INT NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_CC22196CA977936C ON sparx_attributes (tree_root)');
        $this->addSql('CREATE INDEX IDX_CC22196C727ACA70 ON sparx_attributes (parent_id)');
        $this->addSql('ALTER TABLE sparx_attributes ADD CONSTRAINT FK_CC22196CA977936C FOREIGN KEY (tree_root) REFERENCES sparx_attributes (id) ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE sparx_attributes ADD CONSTRAINT FK_CC22196C727ACA70 FOREIGN KEY (parent_id) REFERENCES sparx_attributes (id) ON DELETE NO ACTION');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE sparx_attributes DROP CONSTRAINT FK_CC22196CA977936C');
        $this->addSql('ALTER TABLE sparx_attributes DROP CONSTRAINT FK_CC22196C727ACA70');
        $this->addSql('DROP TABLE sparx_attributes');
    }
}
