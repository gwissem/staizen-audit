<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180606101755 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');
        $this->addSql('ALTER TABLE post ADD deleted_at DATETIME2(6)');
        $this->addSql('ALTER TABLE post ADD created_at DATETIME2(6)');
        $this->addSql('ALTER TABLE post ADD updated_at DATETIME2(6)');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D93CB796C FOREIGN KEY (file_id) REFERENCES files (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');
        $this->addSql('ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8D93CB796C');
        $this->addSql('ALTER TABLE post DROP COLUMN deleted_at');
        $this->addSql('ALTER TABLE post DROP COLUMN created_at');
        $this->addSql('ALTER TABLE post DROP COLUMN updated_at');
    }
}
