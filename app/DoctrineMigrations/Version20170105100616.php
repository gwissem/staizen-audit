<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170105100616 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE step_translation (id INT IDENTITY NOT NULL, translatable_id NVARCHAR(20), name NVARCHAR(255) NOT NULL, created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_B5BE5F4F2C2AC5D3 ON step_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX step_translation_unique_translation ON step_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql(
            'CREATE TABLE process_definition_translation (id INT IDENTITY NOT NULL, translatable_id INT, name NVARCHAR(255), created_at DATETIME2(6), updated_at DATETIME2(6), locale NVARCHAR(255) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('CREATE INDEX IDX_790654172C2AC5D3 ON process_definition_translation (translatable_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX process_definition_translation_unique_translation ON process_definition_translation (translatable_id, locale) WHERE translatable_id IS NOT NULL AND locale IS NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE step_translation ADD CONSTRAINT FK_B5BE5F4F2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES step (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE process_definition_translation ADD CONSTRAINT FK_790654172C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES process_definition (id) ON DELETE CASCADE'
        );
        $this->addSql('ALTER TABLE process_definition DROP COLUMN name');
        $this->addSql(
            'CREATE UNIQUE INDEX unique_name_locale ON attribute_translation (locale, name) WHERE locale IS NOT NULL AND name IS NOT NULL'
        );
        $this->addSql('ALTER TABLE step DROP COLUMN name');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('DROP TABLE step_translation');
        $this->addSql('DROP TABLE process_definition_translation');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'unique_name_locale\')
                            ALTER TABLE attribute_translation DROP CONSTRAINT unique_name_locale
                        ELSE
                            DROP INDEX unique_name_locale ON attribute_translation'
        );
        $this->addSql('ALTER TABLE process_definition ADD name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
        $this->addSql('ALTER TABLE step ADD name NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');

    }
}
