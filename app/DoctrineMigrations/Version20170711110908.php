<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170711110908 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE process_instance_flow (id INT IDENTITY NOT NULL, previous_process_instance_id INT, next_process_instance_id INT, created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_2B7E38D4AD7C3F23 ON process_instance_flow (previous_process_instance_id)');
        $this->addSql('CREATE INDEX IDX_2B7E38D4AA275EA4 ON process_instance_flow (next_process_instance_id)');
        $this->addSql('ALTER TABLE process_instance_flow ADD CONSTRAINT FK_2B7E38D4AD7C3F23 FOREIGN KEY (previous_process_instance_id) REFERENCES process_instance (id)');
        $this->addSql('ALTER TABLE process_instance_flow ADD CONSTRAINT FK_2B7E38D4AA275EA4 FOREIGN KEY (next_process_instance_id) REFERENCES process_instance (id)');
        $this->addSql('ALTER TABLE process_instance_flow ADD active BIT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE process_instance_flow');
    }
}
