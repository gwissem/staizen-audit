<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190325121143 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('create table AtlasDB_def.dbo.psa_symptoms
(
  id          int identity
    constraint PK__psa_symptoms__3213E83F6058A572 primary key,
  arc_code    nvarchar(255) not null,
  code_a nvarchar(10) default null,
  code_b nvarchar(10) default null,
  code_c nvarchar(10) default null,
  detail_a nvarchar(400) default null,
  detail_b nvarchar(400) default null,
  detail_c nvarchar(400) default null,
  description nvarchar(400)
)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE AtlasDB_def.dbo.psa_symptoms');
    }
}
