<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181108133619 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE dbo.tool_permission_user (
            id int NOT NULL IDENTITY(1,1),
            tool_id int NOT NULL,
            user_id int NOT NULL,
            can_read bit NOT NULL,
            can_write bit NOT NULL,
            can_batch_write bit NOT NULL,
            created_at datetime2(6),
            updated_at datetime2(6),
            CONSTRAINT PK__tool_per__3213E83F1535F5EF PRIMARY KEY (id)
        )');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('DROP TABLE dbo.tool_permission_user');

    }
}
