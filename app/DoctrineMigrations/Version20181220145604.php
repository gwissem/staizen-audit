<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181220145604 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE rental_price_list_group (id INT IDENTITY NOT NULL, name NVARCHAR(255), nip NVARCHAR(255), [file] NVARCHAR(1000), type INT NOT NULL, platforms NVARCHAR(255), kinds NVARCHAR(255), PRIMARY KEY (id))');


        $this->addSql('ALTER TABLE rental_priority ADD rental_group_id INT');
//        $this->addSql('ALTER TABLE rental_priority DROP COLUMN nip');
//        $this->addSql('ALTER TABLE rental_priority DROP COLUMN name');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('DROP TABLE rental_price_list_group');

//        $this->addSql('ALTER TABLE rental_priority ADD nip NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
//        $this->addSql('ALTER TABLE rental_priority ADD name NVARCHAR(255) COLLATE Polish_CI_AS');
        $this->addSql('ALTER TABLE rental_priority DROP COLUMN rental_group_id');

    }
}
