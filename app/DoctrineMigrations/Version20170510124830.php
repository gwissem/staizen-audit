<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170510124830 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE step_attribute_definition ALTER COLUMN id INT NOT NULL');
        $this->addSql('ALTER TABLE step_attribute_definition ALTER COLUMN updatable INT NOT NULL');
        $this->addSql('ALTER TABLE step_attribute_definition ADD CONSTRAINT PK__step_att__3213E83FAE705088 PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE attribute ALTER COLUMN active BIT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
