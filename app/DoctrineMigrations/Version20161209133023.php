<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161209133023 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE vin_package (id INT IDENTITY NOT NULL, name NVARCHAR(255) NOT NULL, PRIMARY KEY (id))'
        );
        $this->addSql('ALTER TABLE vin_dictionary ADD package_id INT');
        $this->addSql(
            'ALTER TABLE vin_dictionary ADD CONSTRAINT FK_9C7F4A26F44CABFF FOREIGN KEY (package_id) REFERENCES vin_package (id) ON DELETE SET NULL'
        );
        $this->addSql('CREATE INDEX IDX_9C7F4A26F44CABFF ON vin_dictionary (package_id)');
        $this->addSql('ALTER TABLE vin_header ADD start_date DATETIME2(6)');
        $this->addSql('ALTER TABLE vin_header ADD end_date DATETIME2(6)');
        $this->addSql('ALTER TABLE vin_header ALTER COLUMN query NVARCHAR(MAX)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE vin_dictionary DROP CONSTRAINT FK_9C7F4A26F44CABFF');
        $this->addSql('DROP TABLE vin_package');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_9C7F4A26F44CABFF\')
                            ALTER TABLE vin_dictionary DROP CONSTRAINT IDX_9C7F4A26F44CABFF
                        ELSE
                            DROP INDEX IDX_9C7F4A26F44CABFF ON vin_dictionary'
        );
        $this->addSql('ALTER TABLE vin_dictionary DROP COLUMN package_id');
        $this->addSql('ALTER TABLE vin_header DROP COLUMN start_date');
        $this->addSql('ALTER TABLE vin_header DROP COLUMN end_date');
    }
}
