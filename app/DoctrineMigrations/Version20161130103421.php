<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161130103421 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE platform ADD active BIT');
        $this->addSql('ALTER TABLE platform ADD extId INT');
        $this->addSql('CREATE INDEX value_idx ON vin_element (value)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE platform DROP COLUMN active');
        $this->addSql('ALTER TABLE platform DROP COLUMN extId');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'value_idx\')
                            ALTER TABLE vin_element DROP CONSTRAINT value_idx
                        ELSE
                            DROP INDEX value_idx ON vin_element'
        );
    }
}
