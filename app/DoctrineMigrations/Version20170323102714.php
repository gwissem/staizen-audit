<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170323102714 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE jms_jobs ADD discr NVARCHAR(255) DEFAULT \'job\'');
        $this->addSql('ALTER TABLE attribute_value ADD root_process_instance_id INT');
        $this->addSql('ALTER TABLE attribute_value ADD CONSTRAINT FK_FE4FBB824F4667B6 FOREIGN KEY (root_process_instance_id) REFERENCES process_instance (id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB824F4667B6 ON attribute_value (root_process_instance_id)');
        $this->addSql('ALTER TABLE process_instance ADD root_id INT');
        $this->addSql('ALTER TABLE process_instance ADD CONSTRAINT FK_B4C1EF5F79066886 FOREIGN KEY (root_id) REFERENCES process_instance (id)');
        $this->addSql('CREATE INDEX IDX_B4C1EF5F79066886 ON process_instance (root_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE attribute_value DROP CONSTRAINT FK_FE4FBB824F4667B6');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_FE4FBB824F4667B6\')
                            ALTER TABLE attribute_value DROP CONSTRAINT IDX_FE4FBB824F4667B6
                        ELSE
                            DROP INDEX IDX_FE4FBB824F4667B6 ON attribute_value');
        $this->addSql('ALTER TABLE attribute_value DROP COLUMN root_process_instance_id');

        $this->addSql('ALTER TABLE process_instance DROP CONSTRAINT FK_B4C1EF5F79066886');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B4C1EF5F79066886\')
                            ALTER TABLE process_instance DROP CONSTRAINT IDX_B4C1EF5F79066886
                        ELSE
                            DROP INDEX IDX_B4C1EF5F79066886 ON process_instance');
        $this->addSql('ALTER TABLE process_instance DROP COLUMN root_id');
        $this->addSql('ALTER TABLE jms_jobs DROP COLUMN discr');
    }
}
