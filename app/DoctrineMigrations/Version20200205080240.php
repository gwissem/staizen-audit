<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200205080240 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.pushing_template (id INT IDENTITY NOT NULL, name NVARCHAR(100) NOT NULL, PRIMARY KEY (id))');
//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.pushing_definition (id INT IDENTITY NOT NULL, step_id NVARCHAR(20), user_group_id INT, template_id INT, priority INT NOT NULL, PRIMARY KEY (id))');
//        $this->addSql('CREATE INDEX IDX_AFD8BA3973B21E9C ON AtlasDB_def.dbo.pushing_definition (step_id)');
//        $this->addSql('CREATE INDEX IDX_AFD8BA395DA0FB8 ON AtlasDB_def.dbo.pushing_definition (template_id)');
        $this->addSql('CREATE TABLE AtlasDB_v.dbo.user_platform (user_id INT NOT NULL, platform_id INT NOT NULL, PRIMARY KEY (user_id, platform_id))');
        $this->addSql('CREATE INDEX IDX_D9DF34CBA76ED395 ON AtlasDB_v.dbo.user_platform (user_id)');
        $this->addSql('CREATE INDEX IDX_D9DF34CBB45BA360 ON AtlasDB_v.dbo.user_platform (platform_id)');
//        $this->addSql('CREATE TABLE AtlasDB_def.dbo.mail_group (id INT IDENTITY NOT NULL, user_group_id INT NOT NULL, mail NVARCHAR(255), platform_id INT, PRIMARY KEY (id))');
//        $this->addSql('CREATE INDEX IDX_E5DG34CBB76VD487 ON AtlasDB_def.dbo.mail_group (user_group_id)');

//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.pushing_definition ADD CONSTRAINT FK_AFD8BA3973B21E9C FOREIGN KEY (step_id) REFERENCES AtlasDB_def.dbo.step (id)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.pushing_definition ADD CONSTRAINT FK_AFD8BA395DA0FB8 FOREIGN KEY (template_id) REFERENCES AtlasDB_def.dbo.pushing_template (id)');
        $this->addSql('ALTER TABLE AtlasDB_v.dbo.user_platform ADD CONSTRAINT FK_D9DF34CBA76ED395 FOREIGN KEY (user_id) REFERENCES AtlasDB_v.dbo.fos_user (id)');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.mail_group ADD CONSTRAINT FK_E5DG34CBB76VD487 FOREIGN KEY (user_group_id) REFERENCES AtlasDB_def.dbo.user_group (id)');

        $this->addSql("INSERT INTO AtlasDB_v.dbo.config ([key],[value]) VALUES ('pushing_template_id','1')");

        $this->addSql('CREATE SYNONYM dbo.pushing_template for AtlasDB_def.dbo.pushing_template');
        $this->addSql('CREATE SYNONYM dbo.pushing_definition for AtlasDB_def.dbo.pushing_definition');
        $this->addSql('CREATE SYNONYM dbo.mail_group for AtlasDB_def.dbo.mail_group');

//        $this->addSql('ALTER TABLE mail_group DROP CONSTRAINT DF__step__pushing_en__6C44C29B');
//        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step DROP COLUMN pushing_enabled');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pushing_definition DROP CONSTRAINT FK_AFD8BA3973B21E9C');
        $this->addSql('ALTER TABLE pushing_definition DROP CONSTRAINT FK_AFD8BA395DA0FB8');
        $this->addSql('ALTER TABLE user_platform DROP CONSTRAINT FK_D9DF34CBA76ED395');
        $this->addSql('ALTER TABLE mail_group DROP CONSTRAINT FK_E5DG34CBB76VD487');
        $this->addSql('DROP TABLE pushing_definition');
        $this->addSql('DROP TABLE pushing_template');
        $this->addSql('DROP TABLE user_platform');
        $this->addSql('DROP TABLE mail_group');

        $this->addSql('DROP SYNONYM dbo.pushing_template');
        $this->addSql('DROP SYNONYM dbo.pushing_definition');
        $this->addSql('DROP SYNONYM dbo.mail_group');

        $this->addSql('ALTER TABLE AtlasDB_def.dbo.step ADD pushing_enabled bit default 0');
    }
}
