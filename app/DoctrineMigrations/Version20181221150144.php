<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181221150144 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN name');
        $this->addSql('ALTER TABLE rental_price_list DROP COLUMN type');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN group_id INT');
        $this->addSql('ALTER TABLE rental_price_list ADD CONSTRAINT FK_A99AF734FE54D947 FOREIGN KEY (group_id) REFERENCES rental_price_list_group (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_A99AF734FE54D947 ON rental_price_list (group_id)');
        $this->addSql('sp_RENAME \'rental_priority.rental_group_id\', \'price_list_group_id\', \'COLUMN\'');
        $this->addSql('ALTER TABLE rental_priority DROP COLUMN nip');
        $this->addSql('ALTER TABLE rental_priority DROP COLUMN name');//
        $this->addSql('ALTER TABLE rental_priority ADD CONSTRAINT FK_52C0C514E3CB5592 FOREIGN KEY (price_list_group_id) REFERENCES rental_price_list_group (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_52C0C514E3CB5592 ON rental_priority (price_list_group_id)');

        $this->addSql('ALTER TABLE rental_price_list_extra DROP CONSTRAINT FK_A6AC4F20E3CB5592');
        $this->addSql('ALTER TABLE rental_price_list_extra ADD CONSTRAINT FK_A6AC4F20E3CB5592 FOREIGN KEY (price_list_group_id) REFERENCES rental_price_list_group (id) ON DELETE SET NULL');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('ALTER TABLE rental_price_list DROP CONSTRAINT FK_A99AF734FE54D947');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_A99AF734FE54D947\')
                            ALTER TABLE rental_price_list DROP CONSTRAINT IDX_A99AF734FE54D947
                        ELSE
                            DROP INDEX IDX_A99AF734FE54D947 ON rental_price_list');
        $this->addSql('ALTER TABLE rental_price_list ALTER COLUMN group_id INT NOT NULL');

        $this->addSql('ALTER TABLE rental_price_list_extra DROP CONSTRAINT FK_A6AC4F20E3CB5592');

        $this->addSql('ALTER TABLE rental_priority DROP CONSTRAINT FK_52C0C514E3CB5592');
        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_52C0C514E3CB5592\')
                            ALTER TABLE rental_priority DROP CONSTRAINT IDX_52C0C514E3CB5592
                        ELSE
                            DROP INDEX IDX_52C0C514E3CB5592 ON rental_priority');
        $this->addSql('sp_RENAME \'rental_priority.price_list_group_id\', \'rental_group_id\', \'COLUMN\'');
//        $this->addSql('ALTER TABLE rental_priority ADD nip NVARCHAR(255) COLLATE Polish_CI_AS NOT NULL');
//        $this->addSql('ALTER TABLE rental_priority ADD name NVARCHAR(255) COLLATE Polish_CI_AS');



    }
}
