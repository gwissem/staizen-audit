<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191031105102 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("
            IF NOT EXISTS(SELECT 1 FROM AtlasDB_v.sys.columns WHERE Name = 'company_key' AND Object_ID = Object_ID('AtlasDB_v.dbo.company_platform_permissions'))
            BEGIN
                
                ALTER TABLE company_platform_permissions ADD company_key NVARCHAR(30)
                
            END"
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql("
            IF EXISTS(SELECT 1 FROM AtlasDB_v.sys.columns WHERE Name = 'company_key' AND Object_ID = Object_ID('AtlasDB_v.dbo.company_platform_permissions'))
            BEGIN
                
                ALTER TABLE company_platform_permissions DROP COLUMN  company_key
                
            END"
        );

    }
}
