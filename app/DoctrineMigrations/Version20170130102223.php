<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170130102223 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE vin_program DROP COLUMN is_incremental');
        $this->addSql('ALTER TABLE vin_package ADD is_incremental BIT');
        $this->addSql('ALTER TABLE vin_dictionary DROP CONSTRAINT FK_9C7F4A263EB8070A');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_9C7F4A263EB8070A\')
                            ALTER TABLE vin_dictionary DROP CONSTRAINT IDX_9C7F4A263EB8070A
                        ELSE
                            DROP INDEX IDX_9C7F4A263EB8070A ON vin_dictionary'
        );
        $this->addSql('ALTER TABLE vin_dictionary DROP COLUMN program_id');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE vin_package DROP COLUMN is_incremental');
        $this->addSql('ALTER TABLE vin_program ADD is_incremental BIT');
        $this->addSql('ALTER TABLE vin_dictionary ADD program_id INT');
        $this->addSql(
            'ALTER TABLE vin_dictionary ADD CONSTRAINT FK_9C7F4A263EB8070A FOREIGN KEY (program_id) REFERENCES vin_program (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_9C7F4A263EB8070A ON vin_dictionary (program_id)');
    }
}
