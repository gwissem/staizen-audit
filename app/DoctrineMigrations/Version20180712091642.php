<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180712091642 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B91825883952D0CB\')
                            ALTER TABLE process_instance_property DROP CONSTRAINT IDX_B91825883952D0CB
                        ELSE
                            DROP INDEX IDX_B91825883952D0CB ON process_instance_property');
        $this->addSql('sp_RENAME \'process_instance_property.platform\', \'step_id\', \'COLUMN\'');

        $this->addSql('CREATE INDEX IDX_B918258873B21E9C ON process_instance_property (step_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mssql', 'Migration can only be executed safely on \'mssql\'.');


        $this->addSql('IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_B918258873B21E9C\')
                            ALTER TABLE process_instance_property DROP CONSTRAINT IDX_B918258873B21E9C
                        ELSE
                            DROP INDEX IDX_B918258873B21E9C ON process_instance_property');
        $this->addSql('sp_RENAME \'process_instance_property.step_id\', \'platform\', \'COLUMN\'');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_B91825883952D0CB ON process_instance_property (platform)');

    }
}
