<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170118094207 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql('ALTER TABLE step_attribute_definition DROP CONSTRAINT FK_A6E469933C68CD8');
        $this->addSql('DROP TABLE process_attribute_definition');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_A6E469933C68CD8\')
                            ALTER TABLE step_attribute_definition DROP CONSTRAINT IDX_A6E469933C68CD8
                        ELSE
                            DROP INDEX IDX_A6E469933C68CD8 ON step_attribute_definition'
        );
        $this->addSql(
            'sp_RENAME \'step_attribute_definition.process_attribute_definition_id\', \'parent_attribute_id\', \'COLUMN\''
        );
        $this->addSql('ALTER TABLE step_attribute_definition ADD state INT NOT NULL');
        $this->addSql(
            'ALTER TABLE step_attribute_definition ADD CONSTRAINT FK_A6E46993E68873D4 FOREIGN KEY (parent_attribute_id) REFERENCES attribute (id)'
        );
        $this->addSql('CREATE INDEX IDX_A6E46993E68873D4 ON step_attribute_definition (parent_attribute_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mssql',
            'Migration can only be executed safely on \'mssql\'.'
        );

        $this->addSql(
            'CREATE TABLE process_attribute_definition (id INT IDENTITY NOT NULL, process_definition_id INT, attribute_id INT, created_by_id INT, updated_by_id INT, deleted_at DATETIME2(6), created_at DATETIME2(6), updated_at DATETIME2(6), PRIMARY KEY (id))'
        );
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_A79EA0A780AA177E ON process_attribute_definition (process_definition_id)'
        );
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_A79EA0A7B6E62EFA ON process_attribute_definition (attribute_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_A79EA0A7B03A8386 ON process_attribute_definition (created_by_id)');
        $this->addSql('CREATE NONCLUSTERED INDEX IDX_A79EA0A7896DBBDE ON process_attribute_definition (updated_by_id)');
        $this->addSql(
            'ALTER TABLE process_attribute_definition ADD CONSTRAINT FK_A79EA0A780AA177E FOREIGN KEY (process_definition_id) REFERENCES process_definition (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_attribute_definition ADD CONSTRAINT FK_A79EA0A7B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON UPDATE NO ACTION ON DELETE SET NULL'
        );
        $this->addSql(
            'ALTER TABLE process_attribute_definition ADD CONSTRAINT FK_A79EA0A7B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql(
            'ALTER TABLE process_attribute_definition ADD CONSTRAINT FK_A79EA0A7896DBBDE FOREIGN KEY (updated_by_id) REFERENCES fos_user (id) ON UPDATE NO ACTION ON DELETE NO ACTION'
        );
        $this->addSql('ALTER TABLE step_attribute_definition DROP CONSTRAINT FK_A6E46993E68873D4');
        $this->addSql(
            'IF EXISTS (SELECT * FROM sysobjects WHERE name = \'IDX_A6E46993E68873D4\')
                            ALTER TABLE step_attribute_definition DROP CONSTRAINT IDX_A6E46993E68873D4
                        ELSE
                            DROP INDEX IDX_A6E46993E68873D4 ON step_attribute_definition'
        );
        $this->addSql(
            'sp_RENAME \'step_attribute_definition.parent_attribute_id\', \'process_attribute_definition_id\', \'COLUMN\''
        );
        $this->addSql('ALTER TABLE step_attribute_definition DROP COLUMN state');
        $this->addSql(
            'ALTER TABLE step_attribute_definition ADD CONSTRAINT FK_A6E469933C68CD8 FOREIGN KEY (process_attribute_definition_id) REFERENCES process_attribute_definition (id) ON UPDATE NO ACTION ON DELETE CASCADE'
        );
        $this->addSql(
            'CREATE NONCLUSTERED INDEX IDX_A6E469933C68CD8 ON step_attribute_definition (process_attribute_definition_id)'
        );

    }
}
