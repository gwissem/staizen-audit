#!/bin/bash

#   ----------
#   Argumenty:
#   $1 - nazwa katalogu z projektem (/var/www/$1)
#   $2 - nazwa katalogu na dysku zewnętrzym (/mnt/pliki/$2)
#   $3 - katalog który jest przenoszony na zewnętrzny dysk (/var/www/$1/current/$3)
#   $4 - Czy kopiować pliki  - 0/1 (domyślnie 0)

#       Przykład: ./migrate_files.sh atlas_production atlas_production data

if [ -z "$1" ]
  then
    echo "Podaj nazwę katalogu z projektem"
    exit 0;
fi

if [ -z "$2" ]
  then
    echo "Podaj nazwę katalogu na dysku zewnętrznym"
    exit 0;
fi

if [ -z "$3" ]
  then
    echo "Podaj nazwę katalogu, który ma być przeniesiony na dysk zewnętrzny"
    exit 0;
fi

mkdir -p /mnt/pliki/$2/$3
unlink /var/www/$1/current/$3
mv /var/www/$1/shared/$3 /var/www/$1/shared/$3_
ln -s /mnt/pliki/$2/$3 /var/www/$1/shared/$3
ln -s /var/www/$1/shared/$3 /var/www/$1/current/$3

# Kopiuje tylko wtedy gdy jako 4 argument podamy "1"

if [[ -n "$4" ]] && [ $4 = 1 ]
then
    rsync -Pr /var/www/$1/shared/$3_/* /mnt/pliki/$2/$3
fi