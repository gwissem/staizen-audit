#!/bin/bash

#Skrypt który sprawdza status wirtualnych terminali "screen"
#Command: checking_screen with-reset

NAME="$1"

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

WITH_RESET=0

check_process_in_screen() {

    SNAME="$1"

    echo "-----"
	echo -e "Screen name: ${GREEN} $SNAME${NC}"

	IDSCREEN=$(screen -ls | grep $SNAME | awk 'match($1,/[0-9]+/) {print substr($1, RSTART, RLENGTH)}')

	if [ -z "$IDSCREEN" ]; then
	        echo -e "${RED}Nie znaleziono screen'a${NC}"
	else

		#echo "ID: $IDSCREEN"

		PIDD_PROCESS=$(ps -el | grep $(ps -el | grep $IDSCREEN | grep bash | awk '{print $4}') | grep -v bash | awk '{print $4}')

		if [ -z "$PIDD_PROCESS" ]; then
		        #echo -e "${RED}W screen'ie ${GREEN} $SNAME ${RED} nie ma zadnego procesu.${NC}"

		        if [ $WITH_RESET -eq 1 ]; then

					screen -S "$SNAME" -X stuff '^N php current/bin/console gos:websocket:server --profile --env=prod^M'

		        	echo -e "Status: ${RED}IS RESSETING...${NC}"
		        else
		        	echo -e "Status: ${RED}NOT WORK${NC}"
		        fi

		else
			#echo "PIDD: $PIDD_PROCESS"

			RESULT=$(ps u -p $(ps -el | grep $(ps -el | grep $IDSCREEN | grep bash | awk '{print $4}') | grep -v bash | awk '{print $4}') --no-headers)

			echo -e "Status: ${GREEN}WORK${NC}"

			#echo $RESULT
		fi

	fi

}

run_fun() {
	for i in "screen_for_sandbox" "screen_for_master" "pts-1.AtlasWeb"
	do
		check_process_in_screen $i
	done
}

if [ -z "$NAME" ]; then

	run_fun

else

	if [ "$NAME" == "with-reset" ]; then

		WITH_RESET=1
		run_fun

	else

		check_process_in_screen $NAME

	fi

fi