#!/bin/bash

# ./test-reset.sh tests screen_for_tests

GREEN='\033[0;32m'
BLUE='\033[34m'
NC='\033[0m' # No Color

NAMESCREEN=$2
INSTANCE=$1

PATH_REL="/var/www/atlas_"$INSTANCE"/releases/*"

if [ -z "$INSTANCE" ]; then

	echo -e "${BLUE} x Nie podano Instance! ${NC}"

else

	echo -e "${BLUE} --> Cleaning last releases...${NC}"
	ls -1dt $PATH_REL | tail -n +5 | sudo xargs rm -rf
	echo -e "${GREEN} --> Done ✔${NC}"

	echo -e "${BLUE} --> Restarting PHP...${NC}"
	sudo service php7.0-fpm restart
	echo -e "${GREEN} --> Done ✔${NC}"

	echo -e "${BLUE} --> Restarting screen...${NC}"
	screen -S $NAMESCREEN -X stuff '^N php current/bin/console gos:websocket:server --profile --env=prod^M'
	echo -e "${GREEN} --> Done ✔${NC}"

fi