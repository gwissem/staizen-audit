-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: atlas_chat
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cometchat`
--

DROP TABLE IF EXISTS `cometchat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `sent` int(10) unsigned NOT NULL DEFAULT '0',
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `direction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `from` (`from`),
  KEY `direction` (`direction`),
  KEY `read` (`read`),
  KEY `sent` (`sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat`
--

LOCK TABLES `cometchat` WRITE;
/*!40000 ALTER TABLE `cometchat` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_announcements`
--

DROP TABLE IF EXISTS `cometchat_announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_announcements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `announcement` text NOT NULL,
  `time` int(10) unsigned NOT NULL,
  `to` int(10) NOT NULL,
  `recd` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `time` (`time`),
  KEY `to_id` (`to`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_announcements`
--

LOCK TABLES `cometchat_announcements` WRITE;
/*!40000 ALTER TABLE `cometchat_announcements` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_block`
--

DROP TABLE IF EXISTS `cometchat_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_block` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fromid` int(10) unsigned NOT NULL,
  `toid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fromid` (`fromid`),
  KEY `toid` (`toid`),
  KEY `fromid_toid` (`fromid`,`toid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_block`
--

LOCK TABLES `cometchat_block` WRITE;
/*!40000 ALTER TABLE `cometchat_block` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_bots`
--

DROP TABLE IF EXISTS `cometchat_bots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_bots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `keywords` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `apikey` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_bots`
--

LOCK TABLES `cometchat_bots` WRITE;
/*!40000 ALTER TABLE `cometchat_bots` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_bots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_chatroommessages`
--

DROP TABLE IF EXISTS `cometchat_chatroommessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_chatroommessages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `chatroomid` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `sent` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `chatroomid` (`chatroomid`),
  KEY `sent` (`sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_chatroommessages`
--

LOCK TABLES `cometchat_chatroommessages` WRITE;
/*!40000 ALTER TABLE `cometchat_chatroommessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_chatroommessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_chatrooms`
--

DROP TABLE IF EXISTS `cometchat_chatrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_chatrooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lastactivity` int(10) unsigned NOT NULL,
  `createdby` int(10) unsigned NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `vidsession` varchar(512) DEFAULT NULL,
  `invitedusers` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lastactivity` (`lastactivity`),
  KEY `createdby` (`createdby`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_chatrooms`
--

LOCK TABLES `cometchat_chatrooms` WRITE;
/*!40000 ALTER TABLE `cometchat_chatrooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_chatrooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_chatrooms_users`
--

DROP TABLE IF EXISTS `cometchat_chatrooms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_chatrooms_users` (
  `userid` int(10) unsigned NOT NULL,
  `chatroomid` int(10) unsigned NOT NULL,
  `isbanned` int(1) DEFAULT '0',
  PRIMARY KEY (`userid`,`chatroomid`) USING BTREE,
  KEY `chatroomid` (`chatroomid`),
  KEY `userid` (`userid`),
  KEY `userid_chatroomid` (`chatroomid`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_chatrooms_users`
--

LOCK TABLES `cometchat_chatrooms_users` WRITE;
/*!40000 ALTER TABLE `cometchat_chatrooms_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_chatrooms_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_colors`
--

DROP TABLE IF EXISTS `cometchat_colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_colors` (
  `color_key` varchar(100) NOT NULL,
  `color_value` text NOT NULL,
  `color` varchar(50) NOT NULL,
  UNIQUE KEY `color_index` (`color_key`,`color`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_colors`
--

LOCK TABLES `cometchat_colors` WRITE;
/*!40000 ALTER TABLE `cometchat_colors` DISABLE KEYS */;
INSERT INTO `cometchat_colors` VALUES ('color1','a:3:{s:7:\"primary\";s:6:\"56a8e3\";s:9:\"secondary\";s:6:\"3777A7\";s:5:\"hover\";s:6:\"ECF5FB\";}','color1'),('color10','a:3:{s:7:\"primary\";s:6:\"23025E\";s:9:\"secondary\";s:6:\"3D1F84\";s:5:\"hover\";s:6:\"E5D7FF\";}','color10'),('color11','a:3:{s:7:\"primary\";s:6:\"24D4F6\";s:9:\"secondary\";s:6:\"059EBB\";s:5:\"hover\";s:6:\"DBF9FF\";}','color11'),('color12','a:3:{s:7:\"primary\";s:6:\"289D57\";s:9:\"secondary\";s:6:\"09632D\";s:5:\"hover\";s:6:\"DDF9E8\";}','color12'),('color13','a:3:{s:7:\"primary\";s:6:\"D9B197\";s:9:\"secondary\";s:6:\"C38B66\";s:5:\"hover\";s:6:\"FFF1E8\";}','color13'),('color14','a:3:{s:7:\"primary\";s:6:\"FF67AB\";s:9:\"secondary\";s:6:\"D6387E\";s:5:\"hover\";s:6:\"F3DDE7\";}','color14'),('color15','a:3:{s:7:\"primary\";s:6:\"8E24AA\";s:9:\"secondary\";s:6:\"7B1FA2\";s:5:\"hover\";s:6:\"EFE8FD\";}','color15'),('color2','a:3:{s:7:\"primary\";s:6:\"4DC5CE\";s:9:\"secondary\";s:6:\"068690\";s:5:\"hover\";s:6:\"D3EDEF\";}','color2'),('color3','a:3:{s:7:\"primary\";s:6:\"FFC107\";s:9:\"secondary\";s:6:\"FFA000\";s:5:\"hover\";s:6:\"FFF8E2\";}','color3'),('color4','a:3:{s:7:\"primary\";s:6:\"FB4556\";s:9:\"secondary\";s:6:\"BB091A\";s:5:\"hover\";s:6:\"F5C3C8\";}','color4'),('color5','a:3:{s:7:\"primary\";s:6:\"DBA0C3\";s:9:\"secondary\";s:6:\"D87CB3\";s:5:\"hover\";s:6:\"ECD9E5\";}','color5'),('color6','a:3:{s:7:\"primary\";s:6:\"3B5998\";s:9:\"secondary\";s:6:\"213A6D\";s:5:\"hover\";s:6:\"DFEAFF\";}','color6'),('color7','a:3:{s:7:\"primary\";s:6:\"065E52\";s:9:\"secondary\";s:6:\"244C4E\";s:5:\"hover\";s:6:\"AFCCAF\";}','color7'),('color8','a:3:{s:7:\"primary\";s:6:\"FF8A2E\";s:9:\"secondary\";s:6:\"CE610C\";s:5:\"hover\";s:6:\"FDD9BD\";}','color8'),('color9','a:3:{s:7:\"primary\";s:6:\"E99090\";s:9:\"secondary\";s:6:\"B55353\";s:5:\"hover\";s:6:\"FDE8E8\";}','color9'),('color91486388678','a:3:{s:7:\"primary\";s:6:\"3B3F51\";s:9:\"secondary\";s:6:\"292c38\";s:5:\"hover\";s:6:\"3b3f51\";}','color91486388678'),('color91486481477','a:3:{s:7:\"primary\";s:6:\"3B3F51\";s:9:\"secondary\";s:6:\"292c38\";s:5:\"hover\";s:6:\"e9ecf3\";}','color91486481477');
/*!40000 ALTER TABLE `cometchat_colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_guests`
--

DROP TABLE IF EXISTS `cometchat_guests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_guests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000001 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_guests`
--

LOCK TABLES `cometchat_guests` WRITE;
/*!40000 ALTER TABLE `cometchat_guests` DISABLE KEYS */;
INSERT INTO `cometchat_guests` VALUES (10000000,'guest-10000000');
/*!40000 ALTER TABLE `cometchat_guests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_languages`
--

DROP TABLE IF EXISTS `cometchat_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_languages` (
  `lang_key` varchar(255) NOT NULL COMMENT 'Key of a language variable',
  `lang_text` text NOT NULL COMMENT 'Text/value of a language variable',
  `code` varchar(20) NOT NULL COMMENT 'Language code for e.g. en for English',
  `type` varchar(20) NOT NULL COMMENT 'Type of CometChat add on for e.g. module/plugin/extension/function',
  `name` varchar(50) NOT NULL COMMENT 'Name of add on for e.g. announcement,smilies, etc.',
  UNIQUE KEY `lang_index` (`lang_key`,`code`,`type`,`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all CometChat languages';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_languages`
--

LOCK TABLES `cometchat_languages` WRITE;
/*!40000 ALTER TABLE `cometchat_languages` DISABLE KEYS */;
INSERT INTO `cometchat_languages` VALUES ('contacts','Kontakty','pl','core','default'),('rtl','0','en','core','default'),('rtl','0','pl','core','default');
/*!40000 ALTER TABLE `cometchat_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_session`
--

DROP TABLE IF EXISTS `cometchat_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_session` (
  `session_id` char(32) NOT NULL,
  `session_data` text NOT NULL,
  `session_lastaccesstime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_session`
--

LOCK TABLES `cometchat_session` WRITE;
/*!40000 ALTER TABLE `cometchat_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_settings`
--

DROP TABLE IF EXISTS `cometchat_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_settings` (
  `setting_key` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Configuration setting name. It can be PHP constant, variable or array',
  `value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Value of the key.',
  `key_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'States whether the key is: 0 = PHP constant, 1 = atomic variable or 2 = serialized associative array.',
  PRIMARY KEY (`setting_key`),
  KEY `key` (`setting_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Stores all the configuration settings for CometChat';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_settings`
--

LOCK TABLES `cometchat_settings` WRITE;
/*!40000 ALTER TABLE `cometchat_settings` DISABLE KEYS */;
INSERT INTO `cometchat_settings` VALUES ('allowAvatar','1',1),('allowDelete','1',1),('allowGuests','0',1),('allowUsers','0',1),('announcementTime','15000',1),('apikey','da1273d382d60978ba1309fa8f410f1d',1),('armyTime','1',1),('autoPopupChatbox','1',1),('BASE_URL','/cometchat_lib/',0),('beepOnAllMessages','1',1),('blockpluginmode','0',1),('CC_SITE_URL','',0),('chatroomTimeout','604800',1),('color','color91486481477',1),('crguestsMode','1',1),('CROSS_DOMAIN','0',0),('crplugins','a:2:{i:0;s:12:\"filetransfer\";i:1;s:7:\"smilies\";}',2),('desktopNotifications','1',1),('DEV_MODE','0',0),('disableForIE6','0',1),('disableForMobileDevices','1',1),('displayBusyNotification','1',1),('displayOfflineNotification','1',1),('displayOnlineNotification','1',1),('ERROR_LOGGING','1',0),('extensions','a:2:{i:0;s:9:\"mobileapp\";i:1;s:7:\"desktop\";}',2),('extensions_core','a:4:{s:3:\"ads\";s:14:\"Advertisements\";s:9:\"mobileapp\";s:9:\"Mobileapp\";s:7:\"desktop\";s:7:\"Desktop\";s:4:\"bots\";s:4:\"Bots\";}',2),('fixFlash','0',1),('floodControl','0',1),('hideEnterExit','0',1),('hideOffline','1',1),('idleTimeout','300',1),('lang','pl',1),('lastMessages','10',1),('lastseen','0',1),('LATEST_VERSION','',0),('lightboxWindows','1',1),('maxHeartbeat','12000',1),('messageBeep','1',1),('minHeartbeat','3000',1),('modules_core','a:11:{s:13:\"announcements\";a:9:{i:0;s:13:\"announcements\";i:1;s:13:\"Announcements\";i:2;s:31:\"modules/announcements/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"280\";i:5;s:3:\"310\";i:6;s:0:\"\";i:7;s:1:\"1\";i:8;s:0:\"\";}s:16:\"broadcastmessage\";a:9:{i:0;s:16:\"broadcastmessage\";i:1;s:17:\"Broadcast Message\";i:2;s:34:\"modules/broadcastmessage/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"385\";i:5;s:3:\"300\";i:6;s:0:\"\";i:7;s:1:\"1\";i:8;s:0:\"\";}s:9:\"chatrooms\";a:9:{i:0;s:9:\"chatrooms\";i:1;s:9:\"Chatrooms\";i:2;s:27:\"modules/chatrooms/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"600\";i:5;s:3:\"300\";i:6;s:0:\"\";i:7;s:1:\"1\";i:8;s:1:\"1\";}s:8:\"facebook\";a:9:{i:0;s:8:\"facebook\";i:1;s:17:\"Facebook Fan Page\";i:2;s:26:\"modules/facebook/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"500\";i:5;s:3:\"460\";i:6;s:0:\"\";i:7;s:1:\"1\";i:8;s:0:\"\";}s:5:\"games\";a:9:{i:0;s:5:\"games\";i:1;s:19:\"Single Player Games\";i:2;s:23:\"modules/games/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"465\";i:5;s:3:\"300\";i:6;s:0:\"\";i:7;s:1:\"1\";i:8;s:0:\"\";}s:4:\"home\";a:8:{i:0;s:4:\"home\";i:1;s:4:\"Home\";i:2;s:1:\"/\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";}s:17:\"realtimetranslate\";a:9:{i:0;s:17:\"realtimetranslate\";i:1;s:23:\"Translate Conversations\";i:2;s:35:\"modules/realtimetranslate/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"280\";i:5;s:3:\"310\";i:6;s:0:\"\";i:7;s:1:\"1\";i:8;s:0:\"\";}s:11:\"scrolltotop\";a:8:{i:0;s:11:\"scrolltotop\";i:1;s:13:\"Scroll To Top\";i:2;s:40:\"javascript:jqcc.cometchat.scrollToTop();\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:0:\"\";i:6;s:0:\"\";i:7;s:0:\"\";}s:5:\"share\";a:8:{i:0;s:5:\"share\";i:1;s:15:\"Share This Page\";i:2;s:23:\"modules/share/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"350\";i:5;s:2:\"50\";i:6;s:0:\"\";i:7;s:1:\"1\";}s:9:\"translate\";a:9:{i:0;s:9:\"translate\";i:1;s:19:\"Translate This Page\";i:2;s:27:\"modules/translate/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"280\";i:5;s:3:\"310\";i:6;s:0:\"\";i:7;s:1:\"1\";i:8;s:0:\"\";}s:7:\"twitter\";a:8:{i:0;s:7:\"twitter\";i:1;s:7:\"Twitter\";i:2;s:25:\"modules/twitter/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"500\";i:5;s:3:\"300\";i:6;s:0:\"\";i:7;s:1:\"1\";}}',2),('newMessageIndicator','1',1),('notificationTime','5000',1),('plugins','a:3:{i:0;s:7:\"smilies\";i:1;s:17:\"clearconversation\";i:2;s:12:\"filetransfer\";}',2),('plugins_core','a:17:{s:9:\"audiochat\";a:2:{i:0;s:10:\"Audio Chat\";i:1;i:0;}s:6:\"avchat\";a:2:{i:0;s:16:\"Audio/Video Chat\";i:1;i:0;}s:5:\"block\";a:2:{i:0;s:10:\"Block User\";i:1;i:1;}s:9:\"broadcast\";a:2:{i:0;s:21:\"Audio/Video Broadcast\";i:1;i:0;}s:11:\"chathistory\";a:2:{i:0;s:12:\"Chat History\";i:1;i:0;}s:17:\"clearconversation\";a:2:{i:0;s:18:\"Clear Conversation\";i:1;i:0;}s:12:\"filetransfer\";a:2:{i:0;s:11:\"Send a file\";i:1;i:0;}s:9:\"handwrite\";a:2:{i:0;s:19:\"Handwrite a message\";i:1;i:0;}s:6:\"report\";a:2:{i:0;s:19:\"Report Conversation\";i:1;i:1;}s:4:\"save\";a:2:{i:0;s:17:\"Save Conversation\";i:1;i:0;}s:11:\"screenshare\";a:2:{i:0;s:13:\"Screensharing\";i:1;i:0;}s:7:\"smilies\";a:2:{i:0;s:7:\"Smilies\";i:1;i:0;}s:8:\"stickers\";a:2:{i:0;s:8:\"Stickers\";i:1;i:0;}s:5:\"style\";a:2:{i:0;s:15:\"Color your text\";i:1;i:2;}s:13:\"transliterate\";a:2:{i:0;s:13:\"Transliterate\";i:1;i:0;}s:10:\"whiteboard\";a:2:{i:0;s:10:\"Whiteboard\";i:1;i:0;}s:10:\"writeboard\";a:2:{i:0;s:10:\"Writeboard\";i:1;i:0;}}',2),('prependLimit','10',1),('searchDisplayNumber','10',1),('showChatroomUsers','1',1),('showUsername','0',1),('startOffline','0',1),('theme','docked',1),('thumbnailDisplayNumber','40',1),('trayicon','a:1:{s:9:\"chatrooms\";a:9:{i:0;s:9:\"chatrooms\";i:1;s:9:\"Chatrooms\";i:2;s:27:\"modules/chatrooms/index.php\";i:3;s:6:\"_popup\";i:4;s:3:\"600\";i:5;s:3:\"300\";i:6;s:0:\"\";i:7;s:1:\"1\";i:8;s:1:\"1\";}}',2),('typingTimeout','10000',1),('windowFavicon','0',1),('windowTitleNotify','1',1);
/*!40000 ALTER TABLE `cometchat_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_status`
--

DROP TABLE IF EXISTS `cometchat_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_status` (
  `userid` int(10) unsigned NOT NULL,
  `message` text,
  `status` enum('available','away','busy','invisible','offline') DEFAULT NULL,
  `typingto` int(10) unsigned DEFAULT NULL,
  `typingtime` int(10) unsigned DEFAULT NULL,
  `isdevice` int(1) unsigned NOT NULL DEFAULT '0',
  `lastactivity` int(10) unsigned NOT NULL DEFAULT '0',
  `lastseen` int(10) unsigned NOT NULL DEFAULT '0',
  `lastseensetting` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  KEY `typingto` (`typingto`),
  KEY `typingtime` (`typingtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_status`
--

LOCK TABLES `cometchat_status` WRITE;
/*!40000 ALTER TABLE `cometchat_status` DISABLE KEYS */;
INSERT INTO `cometchat_status` VALUES (1,NULL,NULL,NULL,NULL,0,1486551900,0,0);
/*!40000 ALTER TABLE `cometchat_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_users`
--

DROP TABLE IF EXISTS `cometchat_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `displayname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `grp` varchar(25) NOT NULL,
  `friends` text NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_users`
--

LOCK TABLES `cometchat_users` WRITE;
/*!40000 ALTER TABLE `cometchat_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cometchat_videochatsessions`
--

DROP TABLE IF EXISTS `cometchat_videochatsessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cometchat_videochatsessions` (
  `username` varchar(255) NOT NULL,
  `identity` varchar(255) NOT NULL,
  `timestamp` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`username`),
  KEY `username` (`username`),
  KEY `identity` (`identity`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cometchat_videochatsessions`
--

LOCK TABLES `cometchat_videochatsessions` WRITE;
/*!40000 ALTER TABLE `cometchat_videochatsessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `cometchat_videochatsessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-08 12:05:04
