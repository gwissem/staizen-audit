# set :php_bin, "/usr/local/php/702/bin/php"

set :stages,        %w(stable unstable staging pre_staging local tests sandbox custom production production-socket demo_ru)
set :stages,        %w(stable unstable staging pre_staging local tests sandbox custom production production-socket demo_ru docker-test pre-production-docker)
set :default_stage, "staging"
set :stage_dir,     "app/config/deploys"

require 'capistrano/ext/multistage'

set :application,   "atlas"

set :app_path,          "app"
set :web_path,          "web"
set :symfony_console,   "bin/console"
set :log_path,          "var/logs"
set :cache_path,        "var/cache"
set :app_config_path,   "app/config"

set :user, "starter"
set :use_sudo, false
# set :permission_method,   :chown
set :permission_method,   :acl

set :use_set_permissions, true

set :repository,  "git@codebasehq.com:sigmeo/starter24/atlas.git"

set :keep_releases,  5
set :use_composer, true

set :assets_install,      true
set :dump_assetic_assets,   true

set :composer_options,  "--optimize-autoloader --prefer-dist --no-progress"
set :path_gulp, "node /home/starter/www/node_modules/gulp/bin/gulp.js prod"
set :screen_name, 0

task :set_composer_quiet do

    if "#{stage}" == 'tests' || "#{stage}" == 'custom' || "#{stage}" == 'production' || "#{stage}" == 'pre-production-docker'

        set :composer_options,  "--optimize-autoloader --prefer-dist --no-progress --quiet"

    else
        if Capistrano::CLI.ui.agree("Uruchomić composer z parametrem --quite? (Y/N)")

            set :composer_options,  "--optimize-autoloader --prefer-dist --no-progress --quiet"

        end
    end
end

set :shared_files,      ["app/config/parameters.yml"]
set :shared_children,   [log_path, "data", web_path + "/cached_emails", web_path + "/files", web_path + "/cometchat_lib/writable/filetransfer/uploads"]

logger.level = Logger::MAX_LEVEL

set :writable_dirs,       [cache_path, log_path, "data", web_path + "/cached_emails", web_path + "/files",  web_path + "/cometchat_lib/writable/filetransfer/uploads"]
set :webserver_user,      "www-data"

ssh_options[:forward_agent] = true
default_run_options[:pty] = true

set :deploy_via, :remote_cache

#before "symfony:cache:warmup", "symfony:doctrine:migrations:migrate"

task :atlas_set_permissions do
    run "chmod -R 777 #{latest_release}/#{web_path}/cometchat_lib/writable/cache"
end

task :set_public_chat_cache do
    set :use_sudo, true
    run "#{try_sudo} rm -rf #{latest_release}/#{web_path}/cometchat_lib/writable/cache/*"
end

# GULP
task :run_gulp do
    run "cd #{latest_release}; #{path_gulp}"
end


task :translations_extract do

    if "#{stage}" == 'tests' || "#{stage}" == 'custom'

        capifony_pretty_print "--> Update translations"

        run "cd #{latest_release}; #{php_bin} #{symfony_console} translation:extract en --dir=./src/ --dir=./app/ --output-dir=./app/Resources/translations"
        run "cd #{latest_release}; #{php_bin} #{symfony_console} translation:extract pl --dir=./src/ --dir=./app/ --output-dir=./app/Resources/translations"

    else

        if Capistrano::CLI.ui.agree("Zaaktualizować tłumaczenia? (Y/N)")

            capifony_pretty_print "--> Update translations"

            run "cd #{latest_release}; #{php_bin} #{symfony_console} translation:extract en --dir=./src/ --dir=./app/ --output-dir=./app/Resources/translations"
            run "cd #{latest_release}; #{php_bin} #{symfony_console} translation:extract pl --dir=./src/ --dir=./app/ --output-dir=./app/Resources/translations"

        end

    end

    capifony_puts_ok

    run "cd #{latest_release}; #{php_bin} #{symfony_console} cache:clear --env=prod --no-warmup --no-optional-warmers"

end

task :remove_cache do

    capifony_pretty_print "--> CACHE CLEAR"

    run "cd #{latest_release}; #{php_bin} #{symfony_console} cache:clear --env=prod --no-warmup --no-optional-warmers"

    capifony_puts_ok

end

task :clean_last_releases do

    capifony_pretty_print "--> Clean last releases"

    if "#{stage}" == 'docker-test'
        run "ls -1dt /var/www/atlas_#{stage}/releases/* | tail -n +#{keep_releases + 1} |  xargs rm -rf"
    else
        run "ls -1dt /var/www/atlas_#{stage}/releases/* | tail -n +#{keep_releases + 1} | sudo xargs rm -rf"
    end

    capifony_puts_ok

end

before "symfony:cache:warmup" do
   capifony_pretty_print "--> Update assets version"
   run "if [ -f #{shared_path}/app/config/parameters.yml ]; then sed -i.bak 's/\\(assets_version:[[:space:]]*\\)[[:digit:]]*/\\1#{release_name}/' #{shared_path}/app/config/parameters.yml; fi"
   capifony_puts_ok
end

before "symfony:cache:warmup" do
   capifony_pretty_print "--> Clear Doctrine Cache"
   run "cd #{latest_release}; #{php_bin} #{symfony_console} doctrine:cache:clear-metadata --env=prod && #{php_bin} #{symfony_console} doctrine:cache:clear-query --env=prod && #{php_bin} #{symfony_console} doctrine:cache:clear-result --env=prod"
   capifony_puts_ok
end

before "reset_all_services" do
   capifony_pretty_print "--> Clear Doctrine Cache"
   run "cd #{latest_release}; #{php_bin} #{symfony_console} doctrine:cache:clear-metadata --env=prod && #{php_bin} #{symfony_console} doctrine:cache:clear-query --env=prod && #{php_bin} #{symfony_console} doctrine:cache:clear-result --env=prod"
   capifony_puts_ok
end

task :reset_all_services do

#     screen_name = Capistrano::CLI.ui.ask "Which screen reset? (0 == 'no reset')  [#{screen_id}]:"
#     screen_name = screen_id if screen_name.empty?

    if "#{screen_name}" != '0'
        capifony_pretty_print "--> Resetting screen: '#{screen_name}'."

        #run "screen -S '#{screen_name}' -X stuff '^C' && screen -S '#{screen_name}' -X stuff '^P^O'"
        run "screen -S '#{screen_name}' -X stuff '^C' && screen -S '#{screen_name}' -X stuff '^N php current/bin/console gos:websocket:server --profile --env=prod^M'"

        #puts '--> ✔ Done.'.green
        capifony_puts_ok
    else
        #puts '✘ No reset screen'.red
        capifony_pretty_print "--> No reset screen."
        puts '✘'.red
    end

    capifony_pretty_print "--> Resetting PHP-FPM..."

    if "#{stage}" == 'docker-test'
        #run "service php7.0-fpm restart"
    else
        run "sudo service php7.0-fpm restart"
    end

    capifony_puts_ok

    if "#{stage}" == 'tests' || "#{stage}" == 'custom'
        puts "---".blue
        puts "--> Wszystko git, możesz testować ;)".green
        puts "---".blue
    end

    #puts '--> ✔ Done.'.green

end


before :deploy, 'set_composer_quiet'

before "deploy:rollback", "set_public_chat_cache"

after "deploy:finalize_update", "atlas_set_permissions"

after "symfony:cache:warmup", "run_gulp"

#before "symfony:cache:warmup", "translations_extract"
before "symfony:cache:warmup", "remove_cache"

after "deploy", "clean_last_releases"

after "clean_last_releases", "reset_all_services"