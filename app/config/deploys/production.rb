logger.level = Logger::IMPORTANT

set :domain,        '10.10.77.145'
#set :domain2,        '10.10.77.147'

#role  :web,         domain, domain2
 role  :web,         domain
role  :db,          domain
#role  :app,         domain, domain2
 role  :app,         domain

server '10.10.77.145', :app, :web, :db, :primary => true
#server '10.10.77.147', :app, :web, :db

#set :branch, "task-513/deploy_for_new_server"
set :branch, "production"
#set :clear_controllers, false
set :deploy_to, "/var/www/atlas_production"
#set :screen_name, "atlas_production"
set :path_gulp, "nodejs /home/starter/www/node_modules/gulp/bin/gulp.js prod"
#set :interactive_mode, true