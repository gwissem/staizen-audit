logger.level = Logger::IMPORTANT
set :domain,        '10.10.77.160'

role  :web,         domain
role  :db,          domain, :primary => true
role  :app,         domain, :primary => true

server '10.10.77.160', :app, :web, :db, :primary => true

set :interactive_mode, false
set :use_sudo, false
set :repository, "file://" + Dir.pwd
#set :branch, "dev"
set :deploy_to, "/var/www/atlas_tests"
set :deploy_via, :copy
set :scm, :git


task :update_code_echo do

    puts "--> Archiving and sending files to server...".green

end

before 'deploy:update_code', 'update_code_echo'
