logger.level = Logger::IMPORTANT
set :domain,        '10.10.77.160'

role  :web,         domain
role  :db,          domain, :primary => true
role  :app,         domain, :primary => true

server '10.10.77.160', :app, :web, :db, :primary => true

set :branch, "tests"
set :screen_name, "screen_for_tests"

task :set_name_branch do
    branch_name = Capistrano::CLI.ui.ask "Which branch to use? (put branch name): "

    branch_name = branch if branch_name.empty?

    set :branch, branch_name
    puts "--> Using branch: #{branch}.".green

end

# task :set_branch_revision do
#     default_tag = `git tag`.split("\n").last
#     tag = Capistrano::CLI.ui.ask "Tag to deploy (make sure to push the tag first): [#{default_tag}] "
#
#     if tag.empty?
#         default_revision = `git log origin/#{branch} -n 1 --pretty=format:%H`
#         default_short_revision = default_revision[0, 10]
#         revision = Capistrano::CLI.ui.ask "Revision to deploy (on branch #{branch}): [#{default_short_revision}] "
#         revision = default_revision if revision.empty?
#         set :revision, revision
#     end
#
#     tag = branch if tag.empty?
#     set :branch, tag
#     puts "Tag #{branch} revision #{revision}"
# end

before :deploy, 'set_name_branch'

set :interactive_mode, false
set :clear_controllers, false
set :deploy_to, "/var/www/atlas_tests"