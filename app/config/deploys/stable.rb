set :domain,        '10.10.77.160'

role  :web,         domain
role  :db,          domain, :primary => true
role  :app,         domain, :primary => true

server '10.10.77.160', :app, :web, :db, :primary => true

set :branch, "master"
set :deploy_to, "/var/www/atlas_stable"
set :screen_name, "screen_for_master"