#logger.level = Logger::IMPORTANT

set :domain,        '10.10.77.60:222'

role  :web,         domain
#role  :db,          domain
#role  :app,         domain

server '10.10.77.60:222', :app, :web, :db, :primary => true

set :branch, "docker_integrations"
set :deploy_to, "/var/www/atlas_production"
set :path_gulp, "nodejs /home/node_modules/gulp/bin/gulp.js prod"
#set :interactive_mode, true
#set :path_gulp, "node /home/node_modules/gulp/bin/gulp.js prod"


# GULP
task :set_node_modules do
    run "cd #{latest_release}; ln -s /home/node_modules node_modules"
end

before "run_gulp", "set_node_modules"