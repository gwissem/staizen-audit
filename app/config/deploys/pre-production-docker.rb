#   ---------------------
#   pre-production-docker
#   ---------------------

logger.level = Logger::IMPORTANT

set :domain,        '10.10.77.147:222'

role  :web,         domain
#role  :db,          domain
#role  :app,         domain

server '10.10.77.147:222', :app, :web, :db, :primary => true

set :branch, "docker_integrations"
set :deploy_to, "/var/www/atlas_production"
set :mount_path, "#{deploy_to}/mount_disk"
set :path_gulp, "nodejs /home/node_modules/gulp/bin/gulp.js prod"
#set :interactive_mode, true

# GULP
task :set_node_modules do
    run "cd #{latest_release}; ln -s /home/node_modules node_modules"
end

set :mnt_dir_path,   "atlas_production_test"

#Symlink DIRS
task :mnt_share_childs do

    capifony_pretty_print "--> Create symlinks for share dirs..."

    #capifony_pretty_print "------> Create symlink for data"

    run "ln -nfs #{mount_path}/data #{latest_release}/data"
    run "ln -nfs #{mount_path}/web/cached_emails #{latest_release}/web/cached_emails"
    run "ln -nfs #{mount_path}/web/files #{latest_release}/web/cached_emails"
    run "ln -nfs #{mount_path}/web/cometchat_lib/writable/filetransfer/uploads #{latest_release}/web/cometchat_lib/writable/filetransfer/uploads"

    capifony_puts_ok

end

task :clean_last_releases do

    capifony_pretty_print "--> Clean last releases"

    run "ls -1dt /var/www/atlas_production/releases/* | tail -n +#{keep_releases + 1} |  xargs rm -rf"

    capifony_puts_ok

end

task :reset_all_services do

    #capifony_pretty_print "--> Resetting PHP-FPM..."

    #run "service php7.0-fpm restart"

    #capifony_puts_ok

end

before "run_gulp", "set_node_modules"
after "deploy:share_childs", "mnt_share_childs"