logger.level = Logger::IMPORTANT

set :domain,        '10.10.77.60'

role  :web,         domain
#role  :db,          domain
#role  :app,         domain

server '10.10.77.60', :app, :web, :db, :primary => true

set :branch, "dev"
set :deploy_to, "/var/www/atlas_production"
set :path_gulp, "nodejs /home/starter/www/node_modules/gulp/bin/gulp.js prod"
#set :interactive_mode, true