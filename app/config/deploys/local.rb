set :domain,        "127.0.0.1"

role  :web,         domain
role  :db,          domain, :primary => true
role  :app,         domain, :primary => true

server '127.0.0.1', :app, :web, :db, :primary => true

set :webserver_user,      "www-data"
set :user, "dylesiu"
set :branch, "deploy_system"
set :clear_controllers, false
set :deploy_to, "/home/dylesiu/www/sigmeo/atlas_test_deploy/"