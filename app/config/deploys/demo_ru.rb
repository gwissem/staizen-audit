logger.level = Logger::IMPORTANT

set :domain,        '10.10.77.190'

role  :web,         domain
role  :db,          domain
role  :app,         domain

server '10.10.77.190', :app, :web, :db, :primary => true

set :branch, "dev-ru"
set :deploy_to, "/var/www/atlas_production"
#set :path_gulp, "nodejs /home/starter/www/node_modules/gulp/bin/gulp.js prod"