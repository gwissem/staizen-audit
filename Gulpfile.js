var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglifycss = require('gulp-uglifycss'),
    uglifyjs = require('gulp-uglify'),
    runSequence = require('run-sequence'),
    liveReload = require('gulp-livereload'),
    rename = require('gulp-rename'),
    exec = require('child_process').exec;

var config = {
    env: 'dev'
};

var vendors = {
    js: [
        'web/bundles/web/js/vendor/jquery.min.js',
        'web/bundles/web/js/vendor/jquery-ui-custom.min.js',
        'web/bundles/web/js/vendor/js.cookie.min.js',
        'web/bundles/web/js/vendor/moment.min.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap.min.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap-hover-dropdown.min.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap-switch.min.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap-editable.js',
        'web/bundles/web/js/vendor/air-datepicker-master/datepicker.min.js',
        'web/bundles/web/js/vendor/air-datepicker-master/i18n/datepicker.pl.js',
        // 'web/bundles/web/js/vendor/metronic/jquery.slimscroll.min.js',
        'web/bundles/web/js/vendor/metronic/ui-confirmations.min.js',
        'web/bundles/web/js/vendor/colResizable/colResizable.min.js',
        'web/bundles/web/js/vendor/bootstrap/colorpicker/bootstrap-colorpicker.js',
        'web/bundles/web/js/vendor/metronic/jquery.minicolors.min.js',
        'web/bundles/web/js/vendor/metronic/jquery.multi-select.js',
        'web/bundles/web/js/vendor/metronic/datatable.min.js',
        // 'web/bundles/web/js/vendor/metronic/datatables.min.js',
        'web/bundles/web/js/vendor/metronic/nouisliders.min.js',
        'web/bundles/web/js/vendor/metronic/app.min.js',
        'web/bundles/web/js/vendor/metronic/v4-layout.min.js',
        'web/bundles/web/js/vendor/metronic/quick-sidebar.min.js',
        'web/bundles/web/js/vendor/bootstrap-confirmation.min.js',
        'web/bundles/web/js/vendor/toastr/toastr.min.js',
        'web/bundles/web/js/vendor/select2/select2.full.min.js',
        'web/bundles/web/js/vendor/typeahead.jquery.min.js',
        'web/bundles/web/js/vendor/bootstrap/tagsinput/bootstrap-tagsinput.min.js',
        'web/bundles/web/js/vendor/jquery.twbsPagination.min.js',
        'web/bundles/web/js/vendor/jquery.hideseek.min.js',
        'web/bundles/web/js/vendor/jquery.parseurl.js',
        'web/bundles/web/js/vendor/ladda/spin.min.js',
        'web/bundles/web/js/vendor/ladda/ladda.min.js',
        'web/bundles/web/js/vendor/file-uploader/jquery.ui.widget.js',
        'web/bundles/web/js/vendor/file-uploader/jquery.iframe-transport.js',
        'web/bundles/web/js/vendor/file-uploader/jquery.fileupload.js',
        'web/bundles/web/js/vendor/file-uploader/jquery.fileupload-ui.js',
        'web/bundles/web/js/vendor/file-uploader/jquery.fileupload-process.js',
        'web/bundles/web/js/vendor/file-uploader/jquery.fileupload-validate.js',
        'web/bundles/web/js/vendor/trunk8.js',
        'web/bundles/web/js/vendor/clipboard.min.js',
        'web/bundles/web/js/vendor/isMobile.min.js',
        'web/bundles/web/js/vendor/vue.min.js',
        'web/bundles/web/js/vendor/duel.min.js',
        'web/bundles/web/js/vendor/codemirror/codemirror.js',
        'web/bundles/web/js/vendor/codemirror/addon/hint/show-hint.js',
        'web/bundles/web/js/vendor/codemirror/addon/hint/javascript-hint.js',
        'web/bundles/web/js/vendor/codemirror/addon/hint/codeMirrorCustom.js',
        'web/bundles/web/js/vendor/codemirror/mode/sql.js',
        'web/bundles/web/js/vendor/codemirror/addon/fullscreen.js',
        'web/bundles/web/js/vendor/codemirror/addon/search.js',
        'web/bundles/web/js/vendor/codemirror/addon/searchcursor.js',
        'web/bundles/web/js/vendor/codemirror/addon/format/autoformatting.js',
        'web/bundles/web/js/vendor/codemirror/addon/autorefresh.js',
        'web/bundles/web/js/vendor/jquery-slim-scroll/jquery.slimscroll.min.js',
        'web/bundles/web/js/vendor/sweetalert.min.js',
        'web/bundles/web/js/vendor/promise-queue.js'
    ],
    jabber: [
        // 'web/bundles/jabber/js/cisco/ciscobase.js',
        'web/bundles/web/js/vendor/volume-meter.js',
        'web/bundles/jabber/js/cisco/cwic/out/cwic.js',
        // 'web/bundles/jabber/js/cisco/cwic/out_11-8-2/cwic.js',
        // 'web/bundles/jabber/js/cisco/cwic/out_11-8-2/cwic-debug.js',
        'web/bundles/jabber/js/jabber-class.js',
        'web/bundles/jabber/js/jabber-script.js'
    ],
    guest: [
        'web/bundles/fosjsrouting/js/router.js',
        'web/bundles/web/js/vendor/jquery.min.js',
        'web/bundles/web/js/vendor/ladda/spin.min.js',
        'web/bundles/web/js/vendor/ladda/ladda.min.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap.min.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap-hover-dropdown.min.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap-switch.min.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap-editable.js',
        'web/bundles/web/js/vendor/metronic/ui-confirmations.min.js',
        'web/bundles/web/js/vendor/bootstrap-confirmation.min.js',
        'web/bundles/web/js/vendor/toastr/toastr.min.js',
        'web/bundles/web/js/vendor/moment.min.js',
        'web/bundles/web/js/vendor/air-datepicker-master/datepicker.min.js',
        'web/bundles/web/js/vendor/air-datepicker-master/i18n/datepicker.pl.js',
        'web/bundles/web/js/global-scripts.js',
        'web/bundles/web/js/vendor/select2/select2.full.min.js',
        'web/bundles/web/js/vendor/isMobile.min.js',
        'web/bundles/web/js/vendor/jquery.parseurl.js',
        'web/bundles/web/js/table-tool-handle.js'
    ],
    headerAtlas: [
        'web/bundles/fosjsrouting/js/router.js',
        'web/bundles/web/js/vendor/jquery.min.js',
        'web/bundles/web/js/modules/atlas-chrome-extension.js',
        'web/bundles/web/js/vendor/moment.min.js',
        'web/bundles/web/js/vendor/toastr/toastr.min.js',
        'web/bundles/web/js/vendor/js.cookie.min.js',
        'web/bundles/web/js/global-scripts.js',
        'web/bundles/web/js/vendor/bootstrap/bootstrap.min.js',
        'web/bundles/web/js/vendor/metronic/app.min.js',
        'web/bundles/web/js/vendor/metronic/v4-layout.min.js',
        'web/bundles/web/js/vendor/select2/select2.full.min.js',
        'web/bundles/web/js/vendor/metronic/nouisliders.min.js',
        'web/bundles/web/js/vendor/typeahead.jquery.min.js',
        'web/bundles/web/js/vendor/jquery.hideseek.min.js',
        'web/bundles/web/js/vendor/jquery.parseurl.js',
        'web/bundles/web/js/vendor/ladda/spin.min.js',
        'web/bundles/web/js/vendor/ladda/ladda.min.js',
        'web/bundles/web/js/vendor/isMobile.min.js',
        'web/bundles/web/js/vendor/vue.min.js',
        'web/bundles/web/js/vendor/duel.min.js',
        'web/bundles/web/js/vendor/jquery-slim-scroll/jquery.slimscroll.min.js',
        'web/bundles/web/js/vendor/wowJs/wow.min.js',
        'web/bundles/web/js/vendor/html2canvas.min.js',
        'web/bundles/web/js/modules/home-scripts-module.js'

    ],
    dashboardAtlas: [
        'web/bundles/web/js/vendor/wowJs/wow.min.js',
        'web/bundles/web/js/vendor/jexl.min.js',
        'web/bundles/web/js/grid-stack-atlas-component.js',
        'web/bundles/web/js/modules/dashboard-socket-module.js',
        'web/bundles/web/js/modules/email-preview-module.js',
        'web/bundles/web/js/modules/partner-interface-module.js',
        'web/bundles/web/js/modules/email-inbox-editor-module.js',
        'web/bundles/web/js/modules/case-todo-management.js',
        'web/bundles/web/js/modules/case-note-module.js',
        'web/bundles/web/js/modules/widget-panel-edit-attribute-module.js',
        'web/bundles/web/js/modules/public-dashboard-module.js',
        'web/bundles/web/js/modules/gridstack-update-item-module.js',
        'web/bundles/web/js/modules/custom-popup-dashboard-module.js',
        'web/bundles/web/js/modules/atlas-uploader-document-module.js',
        'web/bundles/web/js/modules/dynamical-widget-module.js',
        'web/bundles/web/js/modules/postpone-process-module.js',
        'web/bundles/web/js/modules/case-editor-panel-module.js',
        'web/bundles/web/js/modules/validator-module.js',
        'web/bundles/web/js/tabkey-module.js',
        'web/bundles/web/js/modules/parser-inputs-module.js',
        'web/bundles/web/js/modules/service-summary-tooltip-module.js',
        'web/bundles/web/js/modules/dynamic-edit-attribute-module.js',
        'web/bundles/web/js/modules/module-dynamic-translations-editor.js',
        'web/bundles/web/js/modules/_d-ticket-list-manager.js',
        'web/bundles/web/js/dashboard-scripts.js'

    ],
    gosWebSocket: [
        'web/bundles/goswebsocket/js/vendor/autobahn.min.js',
        'web/bundles/goswebsocket/js/gos_web_socket_client.js'
    ]
};

var scenarioAssets = {
    js: [
        'web/bundles/web/js/vendor/Sortable.js',
        'web/bundles/web/js/modules/scenarios/AttributeValueRow.js',
        'web/bundles/web/js/modules/scenarios/EventRow.js',
        'web/bundles/web/js/modules/scenarios/StepRow.js',
        'web/bundles/web/js/modules/scenarios/ScenarioCreator.js',
        'web/bundles/web/js/modules/scenarios/scenarios-builder.js'
    ]
};

var stylesheets = {
   custom: [
        'web/bundles/web/css/vendor/simple-line-icons/simple-line-icons.min.css',
        'web/bundles/web/css/vendor/font-awesome/css/font-awesome.min.css',
        'web/bundles/web/css/vendor/select2/css/select2.min.css',
        'web/bundles/web/css/vendor/select2/css/select2-bootstrap.min.css',
        'web/bundles/web/css/vendor/bootstrap/bootstrap.min.css',
        'web/bundles/web/css/vendor/bootstrap/bootstrap-switch.min.css',
        'web/bundles/web/css/vendor/bootstrap/bootstrap-editable.css',
        'web/bundles/web/css/vendor/air-datepicker-master/datepicker.min.css',
        'web/bundles/web/css/vendor/jquery-ui.min.css',
        'web/bundles/web/css/vendor/jstree/default/style.min.css',
        'web/bundles/web/css/vendor/metronic/jquery.minicolors.min.css',
        'web/bundles/web/css/vendor/toastr/toastr.min.css',
        'web/bundles/web/css/vendor/bootstrap/colorpicker.css',
        'web/bundles/web/css/vendor/ladda/ladda-themeless.min.css',
        'web/bundles/web/css/vendor/jquery-multi-select/css/multi-select.css',
        'web/bundles/web/css/vendor/metronic/components.min.css',
        'web/bundles/web/css/vendor/metronic/datatables.min.css',
        'web/bundles/web/css/vendor/metronic/nouislider.min.css',
        'web/bundles/web/css/vendor/wowJs/animate.css',
        'web/bundles/web/css/vendor/fullpage/jquery.fullpage.min.css',
        'web/bundles/web/css/vendor/metronic/plugins.min.css',
        'web/bundles/web/css/vendor/metronic/v4-layout.min.css',
        'web/bundles/web/css/vendor/codemirror/codemirror.css',
        'web/bundles/web/css/vendor/codemirror/addon/hint/show-hint.css',
        'web/bundles/web/css/vendor/codemirror/theme/neat.css',
        'web/bundles/web/css/vendor/codemirror/addon/fullscreen.css',
        'web/bundles/web/css/vendor/metronic/themes/default.min.css',
        'web/bundles/web/css/vendor/metronic/blog.min.css'
   ]
};

/** Tu będą JS'y per każdy widok */

var jsViews = {

    mapBundle_Sms: [
        'web/bundles/fosjsrouting/js/router.js',
        'web/bundles/web/js/vendor/jquery.min.js',
        'web/bundles/web/js/vendor/jquery-ui-custom.min.js',
        'web/bundles/web/js/global-scripts.js',
        'web/bundles/web/js/vendor/js.cookie.min.js',
        'web/bundles/web/js/vendor/moment.min.js'
    ]
};

var cssViews = {
  mapBundle_Sms: [
      'web/bundles/map/css/main_map_bundle.scss'
  ]
};

gulp.task('css-views', function () {
    gulp.src(cssViews.mapBundle_Sms)
        .pipe(sass().on('error', sass.logError))
        .pipe((config.env === 'prod') ? uglifycss() : gutil.noop())
        .pipe(gulp.dest('./web/css/'))
        .pipe(liveReload());
});

gulp.task('js-views', function () {
    return gulp.src(jsViews.mapBundle_Sms)
        .pipe(concat('mapBundleSms.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
});

/** end */

gulp.task('css-vendor', function () {
    return gulp.src('./web/bundles/*/css/**/*.css')
        .pipe(concat('vendor.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest('./web/css'))
        .pipe(liveReload());
});


gulp.task('tinymce', function () {
    return gulp.src('./web/bundles/web/css/tinymce-custom.scss')
        .pipe(concat('tinymce-custom.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest('./web/css'))
        .pipe(liveReload());
});

gulp.task('custom-vendors', function () {
    return gulp.src(stylesheets.custom)
        .pipe(concat('custom-vendors.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest('./web/css'))
        .pipe(liveReload());
});

gulp.task('js-vendor', function () {
    return gulp.src(vendors.js)
        .pipe(concat('vendor.js'))
        .pipe(uglifyjs())
        .pipe(gulp.dest('./web/js'));
        // .pipe(liveReload());
});

gulp.task('js-jabber', function () {
    return gulp.src(vendors.jabber)
        .pipe(concat('jabber.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
});

gulp.task('js-guest', function () {
    return gulp.src(vendors.guest)
        .pipe(concat('guest.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
});

gulp.task('js-header-atlas', function () {
    return gulp.src(vendors.headerAtlas)
        .pipe(concat('header-atlas.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
});

gulp.task('js-dashboard', function () {

    return gulp.src(vendors.dashboardAtlas)
        .pipe(concat('dashboard-scripts.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
});

gulp.task('scenario-assets', function () {

    return gulp.src(scenarioAssets.js)
        .pipe(concat('scenario.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
});

gulp.task('js-map', function () {
    return gulp.src(['./web/bundles/map/js/**/*.js', '!./web/bundles/map/**/debug-map-monitor.js'])
        .pipe(concat('atlas-map.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
        // .pipe(liveReload());
});

gulp.task('gos-web-socket', function () {
    return gulp.src(vendors.gosWebSocket)
        .pipe(concat('gos-web-socket.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
        // .pipe(liveReload());
});

gulp.task('fonts', function () {
    return gulp.src(
        [
            'web/bundles/*/**/*.{eot,svg,ttf,woff,woff2}'
        ])
        .pipe(rename({dirname: ''}))
        // .pipe(gulp.dest('./web/fonts'))
        .pipe(gulp.dest('./web/css/fonts'))
        .pipe(liveReload());
});

gulp.task('img', function () {
    return gulp.src(['web/bundles/*/**/*.{jpg,jpeg,png,gif,svg}'])
        .pipe(gulp.dest('./web/images'))
        .pipe(liveReload());
});

gulp.task('js', function () {
    return gulp.src([
        './web/bundles/**/js/**/*.js',
        '!./web/bundles/jabber/**/*.js',
        '!./web/bundles/sparx/**/*.js',
        '!./web/**/home-scripts.js',
        '!./web/**/jstree.min.js',
        '!./web/**/jstree.js',
        '!./web/**/grid-stack-atlas-component.js',
        '!./web/**/process-generator-form.js',
        '!./web/**/process-chart.js',
        '!./web/**/attribute-chart.js',
        '!./web/**/mailbox-script.js',
        '!./web/**/tabkey-module.js',
        '!./web/**/dashboard-scripts.js',
        '!./web/bundles/*/js/vendor/**/*.js',
        '!./web/bundles/jmstranslation/**/*.js',
        '!./web/bundles/nelmioapidoc/**/*.js',
        '!./web/bundles/stfalcontinymce/**/*.js',
        '!./web/bundles/jmsjobqueue/**/*.js',
        '!./web/bundles/map/js/**/*.js',
        '!./web/**/modules/*.js',
        '!./web/**/modules/scenarios/*.js',
        '!./web/bundles/bazingajstranslation/js/**/*.js'
    ])
        .pipe(concat('app.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
        // .pipe(liveReload());
});

gulp.task('js-sparx', function() {
    return gulp.src('src/SparxBundle/Resources/public/js/**/*.js')
        .pipe(concat('sparx.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
});

gulp.task('js-survey', function() {
    return gulp.src('src/SurveyBundle/Resources/public/js/**/*.js')
        .pipe(concat('survey.js'))
        .pipe((config.env === 'prod') ? uglifyjs() : gutil.noop())
        .pipe(gulp.dest('./web/js'));
});

gulp.task('css-survey', function() {
    return gulp.src('src/SurveyBundle/Resources/public/css/**/*.css')
        .pipe(concat('survey.css'))
        .pipe((config.env === 'prod') ? uglifycss() : gutil.noop())
        .pipe(gulp.dest('./web/css/'));
});


gulp.task('sass', function () {
    gulp.src('./web/bundles/web/css/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe((config.env === 'prod') ? uglifycss() : gutil.noop())
        .pipe(gulp.dest('./web/css/'))
        .pipe(liveReload());
});

gulp.task('watch', function () {
    var onChange = function (event) {
        // liveReload.changed();
        console.log('File ' + event.path + ' has been ' + event.type);
    };
    // Starts the serverjs:303 Uncaught TypeError: (intermediate value)(...) is not a function
    liveReload.listen();

    gulp.watch('./src/WebBundle/Resources/public/css/**/*.scss', ['sass'])
        .on('change', onChange);
    gulp.watch('./src/WebBundle/Resources/public/js/**/*.js', ['js', 'js-guest'])
        .on('change', onChange);
    gulp.watch('./src/MapBundle/Resources/public/js/**/*.js', ['js-map'])
        .on('change', onChange);

    gulp.watch('./src/MapBundle/Resources/public/css/**/*.scss', ['css-views'])
        .on('change', onChange);

    gulp.watch('./src/JabberBundle/Resources/public/js/**/*.js', ['js-jabber']);

    gulp.watch(vendors.dashboardAtlas, ['js-dashboard']);

    gulp.watch('src/SparxBundle/Resources/public/js/**/*.js', ['js-sparx']);

    gulp.watch('src/SurveyBundle/Resources/public/js/**/*.js', ['js-survey']);
    gulp.watch('src/SurveyBundle/Resources/public/css/**/*.css', ['css-survey']);

    gulp.watch('web/bundles/web/js/modules/home-scripts-module.js', ['js-header-atlas']);

    gulp.watch(scenarioAssets.js, ['scenario-assets']);

});

var tasksToDo = [
    'fonts',
    'sass',
    'css-views',
    'custom-vendors',
    'tinymce',
    'scenario-assets',
    'js-views',
    'gos-web-socket',
    'js-vendor',
    'js',
    'js-jabber',
    'js-guest',
    'js-header-atlas',
    'js-map',
    'js-dashboard',
    'js-sparx',
    'js-survey',
    'css-survey'];

gulp.task('dev', function () {
    runSequence(tasksToDo, 'watch');
});

gulp.task('prod', function () {
    config.env = 'prod';

    runSequence(tasksToDo);
});

gulp.task('default', function() {

    runSequence('dev');
    // place code for your default task here
});

gulp.task('installAssets', function () {
    exec('php bin/console fos:js-routing:dump', function (err, stdout, stderr) {
        logStdOutAndErr(err, stdout, stderr);
        exec('php bin/console assetic:dump', function (err, stdout, stderr) {
            logStdOutAndErr(err, stdout, stderr);
            exec('php bin/console assets:install --symlink', function (err, stdout, stderr) {
                logStdOutAndErr(err, stdout, stderr);

                runSequence(tasksToDo);

            });
        });
    });
});

// Without this function exec() will not show any output
var logStdOutAndErr = function (err, stdout, stderr) {
    console.log(stdout + stderr);
};