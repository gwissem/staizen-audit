E-mapi 
========
#####Backend

Wszystkie rzeczy związane z e-mapi znajdują się w MapBundle.

Najważniejsze kontroler: src/MapBundle/Controller/DefaultController.php

Ogólne informację
---

Każdy użytkownik, który rozpocznie jaką kolwiek akcję wyszukiwania, rezerwuje dla siebie SESSION ID mapy.
Sesja trwa godzinę i jest ona zapisywana dla tego użytkownika w Redis. 

Niezalogowany użytkownik bądź automat używa sesji anonimowych.
Liczba sesji anonimowych jest dynamicznie poszerzana w razie potrzeby.

Kontakst językowy (nazewnisctwo Państw i miast) jest ustawiony taki, jaki jest ustawiony dla instancji aplikacji (PL/RU)

API
---

####/geocode/{expression}

```php
function searchLocation()
```

Pobieranie lokalizacji po adresie, podając "string"

####/exact_geocode

```php
function searchOneLocation()
```

Pobieranie lokalizacji, podając dokładną tablice danych: 'country', 'city', 'zipCode', 'voivodeship', 'street', 'streetNumber', 'countryId'

####/get_location/{latitude}/{longitude}

```php
function getLocation()
```

Pobieranie lokalizacji po współrzędnych


#### /get-routes-points

```php
function calculateRoutePoints()
```

Obliczanie odległości między punktami

#### /get_map_statistic

Pobranie statystyk o serwerze e-mapi. Ile sesji, jaka wersja mapy

#### /map-center/clean_sessions

Czyści sesje użytkowników, którzy teoretycznie nie są zalogowani

#### /get_object/{name}

```php
function getObject()
```

Pobiera punkty POI. 

TODO
---

Można by przepisać to wszystko na service, żeby nie trzymać w kontrolerze