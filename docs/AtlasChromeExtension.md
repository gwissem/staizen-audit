AtlasChromeExtension
========

W systemie wykorzystywane jest chrome extension dostępne pod tym adresem: 

`https://chrome.google.com/webstore/detail/atlaschromeextension/npefhajgabfnhjiieeifbbnpcekidnam`

Kod tej aplikacji znajduje się na repozytorium Codebase

`git@codebasehq.com:sigmeo/starter24/atlas-ext.git`

Funkcjonalności
---

- Wyświetlanie powiadomienia przy przychodzącym połączeniu telefonicznym (+focus tab)
- monitorowanie ile czasu na jakiej stronie spędzają konsultanci
- robienie screen shoota przy raportowaniu błędu