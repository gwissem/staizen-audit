Viewer
========

Viewer jest narzędziem udostępnianym dla firm zewnętrznych, żeby mogli przeglądać swoje sprawy zakładane w Atlasie.

Cały kod znajduje się w:
```php
src/AppBundle/Controller/CFMCaseViewerController.php
```
<small>(warto to kiedyś przepisać na serwis)</small>

Zarządzanie uprawnieniami - czyli to co ma widzieć dany użytkownik, znajduje się w tabelce `company_platform_permissions`. 
Ustala się, jakie sprawy per platforma ma widzieć konkretna firma.

Dodatkowe procedury, które sterują widocznością elementów Viewera to:

- `A_VIEWER_canExportData` - czy ma możliwość exportu danych z Viewera
- `A_VIEWER_chatIsEnabled` - czy ma możliwość dodawania notatek typu "Chat"
- `P_note_permissions` - jakie typy notatek będą wyświetlane

Główną procedurą, która jest wykorzystywana do wyszukiwania zadań - `[dbo].[p_find_cfm_cases]`. Natomiast po odświeżeniu Viewera na początku zwraca nagłówki tabelki spraw.

W wersji <b>developerskiej</b> wszystko dzieje się w procedurze `[dbo].[p_find_cfm_cases]`.

W wersji <b>produkcyjnej</b> dla celów wydajnościowych wykorzystywana jest hurtownia danych.
W procedurze `[dbp].[p_find_cfm_cases]` jest uruchamiana procedura `[SETTE].[dbo].[p_fast_find_cfm_cases]`, która odpytuje hurtownie.

