WebSocket 
========

Atlasowy WebSocket postawiony jest na projekcie `GeniusesOfSymfony/WebSocketBundle` w wersji 1.8.
Jakby teraz było to robione, to wykorzystawalibyśmy pewnie Node.js, a dokładnie https://socketcluster.io/.
Niestety przepisanie tego wszystkiego na nową aplikację, może zająć sporo czasu. Sobie bym wycenił to na jakieś 10 roboczych dni.

Natomiast, jeżeli chcielibyście pozostać przy PHP to lepszymi opcjami jest 'swoole' lub 'workerman'. 
Tam można budować WebSockety wielowątkowe, jednak są też różne ograniczenia...

*** Trzeba pamiętać, że WebSocket jest jednowątkowy! ***

Przez to, że WebSocket jest jednowątkowy trzeba pamiętać, żeby nie wykorzystywać operacji blokujących I/O. 
Są to między innymi zapytania do bazy danych albo pobieranie kontekstu z jakiś stron (getfile_get_contents).
Dlatego do wielu operacji (jak np inserty do DB) wykorzystałem Redis'a. On też ma operacje blokujące, ale jest extremalnie szybki.

Niestety rozwiązanie to nie jest skalowalne, nawet w najnowszej wersji 2.x. 


Co robi?
-------

Websocket jest odpowiedzialny m.in. za:

// TODO


Pinger (heartBeat)
-------

Każdy zalogowany użytkownik, który połączy się z Socketem, musi to połączenie podtrzymywać, a żeby to robić
to wysyła co 25 sekund 'ping' do WebSocketa. Jeżeli użytkownik nie daje znaku życia przez ponad 60 sekund życia, to serwer zrywa z takim użytkownikiem połączenie.
Wtedy np. wracają do puli zarezerwowane zadania.
 

Wersja 2.x ?
-------

Generalnie od momentu jak zaimplementowaliśmy WebSocket, to jest to nadal rozwijane i powstała już wersja 2.x.

####Dlaczego warto: 

No co nieco się zmieniło i ulepszyło. 
Dodano np natywny Pinger (u nas został zaimplementowany ręcznie, więc też pewnie mało wydajnie)

#### Dlaczego może być ciężko migrować:

W najnowszej wersji już nie ma wsparcia dla ZMQ (Push Provider), dlatego liczyło by się to też z przebudową bibliotek dla PHP i serwera

