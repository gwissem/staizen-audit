Czat
========

W Atlasie wykorzystujemy  CometChat (6.3.3). 
Kod chatu jest zaimplementowany w katalogu `/web/cometchat_lib`.
Baza danych znajduje się w MySQL na serwerze `10.10.77.168`.

Panel Admina
---

Można zarządzać czatem, logując się do panelu pod adresem `/cometchat_lib/admin/`
Dane dostepowe znajdują się w bazie czatu w tabeli `cometchat_settings` pod kluczami `ADMIN_PASS` i `ADMIN_USER`. 
<small>(Bardzo głupi sposób przetrzymywania hasła)</small>


Synchronizacja
---

Codziennie o 23:00 są synchronizowani użytkownicy z tabelki `fos_user` z bazą chatu.
Użytkownicy którzy nie istnieją w bazie czatu, są dodawani.

Za synchronizację odpowiada cron, odpalając Symfony Command `cometchat:migrations:user`. 

