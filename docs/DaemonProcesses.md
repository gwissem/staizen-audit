### Skrypty PHP, które działają w tle

#### - Użytkownicy Online/Offline

Skrypt nasłuchuje na liście redisa, czy trzeba zmienić status Online/Offline użytkownikowi

```bash
php bin/console redis:listener-list:tasks --channel=online_user --env=prod
```

Warto przed pierwszym uruchomieniem, wyczyścić liste jobów 

```bash
php bin/console redis:listener-list:tasks --flush=true --env=prod
```

#### - Monitorowanie wejść/wyjść na kroki

Skrypt nasłuchuje na liście redisa i loguje momenty wejścia i wyjścia użytkownika na krok / z kroku

```bash
php bin/console redis:listener-list:tasks --channel=enter_and_leave_tasks --env=prod
```

#### - Logowanie do ElasticSearch

Skrypt nasłuchuje na liście redisa i loguje otrzymane dane do ElasticSearch. 
Obecnie użwyany np w WebSockecie, żeby monitorować zużycie pamięci i liczbę połączonych użytkowników

```bash
php bin/console redis:listener-list:tasks --channel=atlas_elastica_monitor --env=prod
```

Ten skrypt zbiera informacje m.in. o czasie ładowania kroków i ich nextowania

```bash
php bin/console redis:listener-list:tasks --channel=elastica_log --env=prod
```