Przydatne templatki t-sql 
========

- [Cursor](#Cursor)
- [After Procedure](#After-Procedure)
- [After After Procedure](#After-After-Procedure)
- [Edit Attribute](#Edit-Attribute)
- [Get Attribute](#Get-Attribute)
- [Get Value Text](#Get-Value-Text)
- [Next Step](#Next-Step)
- [ParseString](#ParseString)
- [Comment](#Comment)
- [Nice Log](#Nice-Log)
- [Log if](#Log-if)
- [Get statuses of service](#Get-Statuses-of-Service)
- [Search in procedures](#Search-in-procedures)
- [Send Email](#Send-E-mail)
- [Try Catch](#Try-Catch)

### Cursor
```sql
DECLARE kur cursor LOCAL for
    SELECT *
    FROM x WITH(NOLOCK)
    OPEN kur;
    FETCH NEXT FROM kur INTO @id
    WHILE @@FETCH_STATUS=0
        BEGIN
            -- WHILE BEGIN

                PRINT @id
            
            -- WHILE END
            FETCH NEXT FROM kur INTO @id
        END
    CLOSE kur
DEALLOCATE kur

```

### After Procedure
```sql
CREATE PROCEDURE [dbo].[s_0_0]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT,
    @currentUser INT, 
    @errId INT = 0 OUTPUT
) 
AS
BEGIN
	
	/* ______________________________________________________
	 
	 	TITLE
	 ________________________________________________________*/
	
	PRINT '-------------------------- EXEC [dbo].[s_0_0]'
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
END

```

### After After Procedure
```sql
CREATE PROCEDURE [dbo].[s_0_0_a]
( 
  @nextProcessId INT,
  @currentUser INT = NULL, 
  @token VARCHAR(50) = NULL OUTPUT
) 
AS
BEGIN
	
	/* ______________________________________________________
	 
	 	TITLE
	 ________________________________________________________*/
	
	PRINT '-------------------------- EXEC [dbo].[s_0_0_a]'
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @nextProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
END
```

### Edit Attribute
```sql
EXEC p_attribute_edit
			@attributePath = '',
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@userId = NULL,
			@originalUserId = NULL,
			@valueString = @value,
			@err = @err OUTPUT,
			@message = @message OUTPUT
```

### Get Attribute
```sql
DELETE FROM @values
INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @ = value_int FROM @values
```

### Get Value Text
```sql
[dbo].[valueToText]('111', @groupProcessInstanceId, DEFAULT)
```

### Next Step
```sql
BEGIN TRANSACTION

DECLARE @err INT
DECLARE @message NVARCHAR(255)
DECLARE @processInstanceIds varchar(4000)

EXEC [dbo].[p_process_next]
		@previousProcessId = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT,
		@processInstanceIds = @processInstanceIds OUTPUT

PRINT @err
PRINT @message
PRINT @processInstanceIds

ROLLBACK TRANSACTION
```

### ParseString
```sql
DECLARE @processInstanceId INT = 
DECLARE @simpleText NVARCHAR(MAX) = ''
DECLARE @parsedText NVARCHAR(MAX) = ''

EXEC [dbo].[P_parse_string]
@processInstanceId = @processInstanceId,
@simpleText = @simpleText,
@parsedText = @parsedText OUTPUT

SELECT @parsedText
```

### Comment
```sql
/*	
____________________________________*/
```

### Nice Log
```sql
PRINT '--- @VAL: ' + CAST(ISNULL(@VAL, '') AS NVARCHAR(200))
```

### Log if
```sql
IF SYSTEM_USER = 'user.name'
BEGIN
  PRINT '--- @val: ' + CAST(ISNULL(@val, '') AS NVARCHAR(200))			
END
```

### Get Statuses of Service
```sql
DECLARE @serviceId INT = 1

SELECT ssd.id as Status_Id, ssdt.message as Nazwa, ssd.service as ServiceId, ssd.progress as Progress, ssdt.id as Status_Dictionary_Id
FROM dbo.service_status_dictionary as ssd
LEFT JOIN dbo.service_status_dictionary_translation as ssdt ON ssdt.translatable_id = ssd.id
WHERE ssd.service = @serviceId
AND ssdt.locale = 'pl'
```

### Search in procedures
```sql
SELECT DISTINCT
       o.name AS Object_Name,
       o.type_desc
  FROM sys.sql_modules m
       INNER JOIN
       sys.objects o
         ON m.object_id = o.object_id
WHERE m.definition Like '%Znaleziono partnera%' ESCAPE '\'
```

### Send E-mail
```sql
DECLARE @err INT
DECLARE @message VARCHAR(400)
DECLARE @groupProcessInstanceId INT = 111111
DECLARE @body VARCHAR(MAX)
DECLARE @email VARCHAR(400)
DECLARE @subject VARCHAR(400)
	
SET @body = 'Testowy email.'					
SET @subject = 'Testowy email.'
SET @email = 'email@example.com'

EXECUTE dbo.p_note_new 
	 @groupProcessId = @groupProcessInstanceId
	,@type = 'email'
	,@content = 'Treść notatki'
	,@email = @email
	,@userId = 1
	,@subject = @subject
	,@direction=1
	,@dw = ''
	,@udw = ''
	,@sender = 'atlas_test@starter24.pl'
--	,@additionalAttachments = '{FORM::67555::raport serwisowy}',
--	,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
--	,@additionalAttachments = '{ATTRIBUTE::2992034::tresc atrybutu 2992034::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
	,@emailBody = @body
	,@err=@err OUTPUT
	,@message=@message OUTPUT
```

### Try Catch
```sql
BEGIN TRY  

    BEGIN TRANSACTION
		-- CODE HERE
	ROLLBACK TRANSACTION

END TRY  
BEGIN CATCH  
    SELECT  
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
        
        
        IF @@TRANCOUNT > 0
        			ROLLBACK TRANSACTION --RollBack in case of Error
        			
END CATCH;
```