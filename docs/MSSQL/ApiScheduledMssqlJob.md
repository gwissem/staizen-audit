# ApiScheduledMssqlJob

<h5>(Sposób na odpalanie procedur w wyznaczonym terminie)</h5>

<small>Kiedyś można by zmienić sposób wywoływania tej procedury. Zamiast JSON'a podawać Table-Valued</small>

### - Używanie API w SQL

```sql

/*	Wymagane zmienne
____________________________________*/

DECLARE @procedureName NVARCHAR(255) = 'P_processJobTest'
DECLARE @parameters TABLE(name NVARCHAR(50), type NVARCHAR(20), value NVARCHAR(255) )
DECLARE @jsonText NVARCHAR(MAX)
DECLARE @dateExecute DATETIME

/*
 * Ustawienie kiedy ma się uruchomić procedura
 * W przykładzie - wykona się za 1 min
____________________________________*/

SET @dateExecute = DATEADD(minute, 1, GETDATE());  

/*	
 * Dodawanie parametrów dla procedury
 * 
  ( Robione w formie tabelki, bo łatwiej z tego JSON'a wygenerować )
  
 * Obslugiwane formaty:
 * INT => INT
 * STR => NVARCHAR(255)
 * 
 * ! Parametry są opcjonalne. 
____________________________________*/

INSERT INTO @parameters SELECT 'processInstanceId', 'INT', '123123123'

/*	Tworzenie JSON'a
____________________________________*/

SELECT @jsonText = (SELECT * FROM @parameters FOR JSON AUTO)

/*	Dodanie JOB'a
____________________________________*/

EXEC [dbo].[add_scheduled_job] @dateExecute = @dateExecute, @procedureName = @procedureName, @parameters = @jsonText

```

### - Wyniki

Efekt wykonanych JOB'ów można znaleźć w jms_jobs. Dla ułatwienia queue nazywa się 'mssql_cron_queue'.