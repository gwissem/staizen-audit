API do UCCX 
========

<h5>(Sposoby użycia)</h5>


Wyciąganie statystyk o statusie konsultantów
----

```sql
EXEC [dbo].[UCCX_agent_stats]
```

Zmiana skilli konsultantom
----

<b>Ważne! Procedura ta NIE dodaje/usuwa Skilli tylko je USTAWIA (kasuje i ustawia na nowo)</b>
```sql
DECLARE @skills SkillsTableType
DECLARE @status NVARCHAR(32)

INSERT INTO @skills (id) VALUES (157), (162), (165), (177)

EXEC [dbo].[UCCX_change_skill_of_agent] @agentName = 'janusz.nietypowy', @skills = @skills, @status = @status OUTPUT

SELECT @status
```


Zmiana skilli na kolejce
----

```sql
EXEC [dbo].[P_change_skill_of_queue] @queueId = 123, @skillId = 456
```

Jest też opcja z podawaniem nazw, zamiast ID:

```sql
EXEC [dbo].[P_change_skill_of_queueN] @queueName = 'nazwa_kolejki', @skillName= 'nazwa_skilla'
```