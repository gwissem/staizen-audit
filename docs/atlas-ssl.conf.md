produkcyjny vhost dla Apache2
========

```apacheconfig

<IfModule mod_ssl.c>
	<VirtualHost _default_:443>
		
		ServerName atlas.starter24.pl
		DocumentRoot /var/www/atlas_production/current/web
		#ServerAdmin webmaster@localhost
	
		<FilesMatch "\.php$">
			SetHandler proxy:fcgi://127.0.0.1:9001
		</FilesMatch>

		<Directory /var/www/atlas_production/current/web>
            DirectoryIndex app.php
            AllowOverride All
            Require all granted
            Options FollowSymlinks
            <IfModule mod_rewrite.c>
                    Options -MultiViews
                    RewriteEngine On
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteRule ^(.*)$ app.php [QSA,L]
            </IfModule>
        </Directory>
        
		SetEnv PHP_POOL_NAME "MAIN_POOL"
        
        # Special IF, for Tools which use other PHP-FPM POOL
        <If "%{THE_REQUEST} =~ m#/tools/# && %{THE_REQUEST} !~ m#/tools/embed#">
            SetHandler proxy:fcgi://127.0.0.1:9002
            SetEnv PHP_POOL_NAME "TOOL_POOL"
        </If>
        
        # Special IF, for chat, which use other PHP-FPM POOL (like tools)
        <If "%{THE_REQUEST} =~ m#/cometchat_lib/# && %{THE_REQUEST} !~ m#/images/#">
            SetHandler proxy:fcgi://127.0.0.1:9002
            SetEnv PHP_POOL_NAME "CHAT_POOL"
        </If>
        
        # Special IF, for scenario - DISABLE BUFFER
        <If "%{THE_REQUEST} =~ m#/run-scenario#">
                SetEnv no-gzip 1
                SetEnv output_compression Off
                SetEnv output_buffering 1
        </If>
        
        # FOR DEVELOPMENT
        #RewriteCond %{HTTP:Authorization} ^(.*)
        #RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]

		ErrorLog ${APACHE_LOG_DIR}/atlas-prod-error.log
		CustomLog ${APACHE_LOG_DIR}/atlas-prod-access.log combined

		SSLEngine on
		SSLCertificateFile /etc/ssl/certs/cert.pem
		SSLCertificateKeyFile /etc/ssl/private/private.key
		SSLCertificateChainFile /etc/ssl/certs/chain.crt

		<FilesMatch "\.(cgi|shtml|phtml|php)$">
				SSLOptions +StdEnvVars
		</FilesMatch>
		<Directory /usr/lib/cgi-bin>
				SSLOptions +StdEnvVars
		</Directory>

	</VirtualHost>
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet

```