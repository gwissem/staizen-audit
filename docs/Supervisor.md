Supervisor
========
http://supervisord.org/

Aplikacja, która podtrzymuje przy życiu wiele naszych aplikacji CLI

Żeby sprawdzić czy wszystkie aplikacje są aktywne i od kiedy działają wystarczy komenda:

```bash
sudo supervisorctl status
```

Plik z configiem: 
```bash
/etc/supervisor/supervisord.conf
```

Resetowanie programu supervisora: 

Najpierw wchodzimy w powłokę supervisora:
```shell script
sudo supervisorctl
```

A następnie odpalamy:
```shell script
restart nazwa_programu
```



atlas_queue
---

Program obsługuje JMS JOB (czyli np wysyłanie e-maili)

```smartyconfig
[program:atlas_queue]
command=php /var/www/atlas_production/current/bin/console jms-job-queue:run --env=prod --verbose
process_name=%(program_name)s
numprocs=1
directory=/tmp
autostart=true
autorestart=true
startsecs=5
startretries=10000
user=www-data
redirect_stderr=false
stdout_logfile=/var/www/atlas_production/current/var/logs/jms_job_queue_runner.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/jms_job_queue_runner.error.log
stderr_capture_maxbytes=1MB

```

atlas_socket
---

WebSocket

```smartyconfig
[program:atlas_socket]
command: php current/bin/console gos:websocket:server --env=prod -a atlas.starter24.pl
directory: /var/www/atlas_production
autostart=true
autorestart=true
stdout_logfile=/var/www/atlas_production/current/var/logs/socket_atlas.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/socket_atlas.error.log
stderr_capture_maxbytes=1MB

```

save_task_realtime
---

Skrypt zapisuje do bazy wejścia do i wyjścia z kroków

```smartyconfig
[program:save_task_realtime]
command=php current/bin/console redis:listener-list:tasks --env=prod
process_name=%(program_name)s
numprocs=1
directory=/var/www/atlas_production
autostart=true
autorestart=true
user=www-data
redirect_stderr=false
startretries=30
stdout_logfile=/var/www/atlas_production/current/var/logs/save_task_realtime.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/save_task_realtime.error.log
stderr_capture_maxbytes=1MB

```

ecall_psa_job
---

Nasłuchuje na nowych zdarzeniach e-call PSA. Zakłada sprawę i wysyła powiadomienie do konsultantów.

```smartyconfig
[program:ecall_psa_job]
command=php current/bin/console job:loop get_xml_incoming 5 --env=prod
process_name=%(program_name)s
numprocs=1
directory=/var/www/atlas_production
autostart=true
autorestart=true
user=www-data
redirect_stderr=false
stdout_logfile=/var/www/atlas_production/current/var/logs/ecall_psa_job.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/ecall_psa_job.error.log
stderr_capture_maxbytes=1MB

```

elastica_log  
---

Nasłuchuje na nowych log'ach na liście Redisowej i wysyła do Elastica Search

```smartyconfig
[program:elastica_log]
command=php current/bin/console redis:listener-list:tasks --channel=elastica_log --env=prod
process_name=%(program_name)s
numprocs=1
directory=/var/www/atlas_production
autostart=true
autorestart=true
user=www-data
redirect_stderr=false
startretries=30
stdout_logfile=/var/www/atlas_production/current/var/logs/elastica_log.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/elastice_log.error.log
stderr_capture_maxbytes=1MB

```

jobs_loop 
---

Odświeża jakieś statystyki telefoniczne

```smartyconfig
[program:jobs_loop]
command=php current/bin/console job:loop refresh_telephony_stats 10 --env=prod
process_name=%(program_name)s
numprocs=1
directory=/var/www/atlas_production
autostart=true
autorestart=true
user=www-data
redirect_stderr=false
stdout_logfile=/var/www/atlas_production/current/var/logs/job_loop.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/job_loop.error.log
stderr_capture_maxbytes=1MB

```

mssql_monitor
---

Odczytuje statystyki MSSQL i wysyła do Elastica Search

```smartyconfig
[program:mssql_monitor]
command=php current/bin/console job:loop mssql_monitor 1 --env=prod
process_name=%(program_name)s
numprocs=1
directory=/var/www/atlas_production
autostart=true
autorestart=true
user=www-data
redirect_stderr=false
startretries=30
stdout_logfile=/var/www/atlas_production/current/var/logs/mssql_monitor.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/mssql_monitor.error.log
stderr_capture_maxbytes=1MB

```

online_user
---

Monitoruje Online/Offline użytkowników Atlasa

```smartyconfig
[program:online_user]
command=php current/bin/console redis:listener-list:tasks --channel=online_user --env=prod
process_name=%(program_name)s
numprocs=1
directory=/var/www/atlas_production
autostart=true
autorestart=true
user=www-data
redirect_stderr=false
startretries=30
stdout_logfile=/var/www/atlas_production/current/var/logs/online_user.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/online_user.error.log
stderr_capture_maxbytes=1MB

```

redis_listener_elastica_monitor
---

To samo co 'elastica_log', ale starsza ograniczona wersja 

```smartyconfig
[program:redis_listener_elastica_monitor]
command=php current/bin/console redis:listener-list:tasks --channel=atlas_elastica_monitor --env=prod
process_name=%(program_name)s
numprocs=1
directory=/var/www/atlas_production
autostart=true
autorestart=true
user=www-data
redirect_stderr=false
startretries=30
stdout_logfile=/var/www/atlas_production/current/var/logs/redis_listener_elastica_monitor.out.log
stdout_capture_maxbytes=1MB
stderr_logfile=/var/www/atlas_production/current/var/logs/redis_listener_elastica_monitor.error.log
stderr_capture_maxbytes=1MB

```

telephony_agent_status
---

Zapisuje statusy telefoniczne konsultantów do bazy danych

```smartyconfig
[program:telephony_agent_status]
command=php current/web/machine_central/automatically-status.php -s=1
process_name=%(program_name)s
numprocs=1
directory=/var/www/atlas_production
autostart=true
autorestart=true
user=www-data
redirect_stderr=false
startretries=30
```

# Dodawanie nowego Programu
---

- stworzyć nowy .conf

```bash
sudo supervisorctl reread
sudo supervisorctl add $programName
```