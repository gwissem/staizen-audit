HaProxy
========

###Loadbalancer

W tym projekcie nie do końca balansuje między serwerami, ponieważ mamy tylko 1.

###Zarządzanie ruchem

HaProxy rozdziela ruch między różne serwery w zależności od domeny.

- atlas.starter24.pl (domyślnie)    ---> `(.77.145)` Front_1
- sparx-dev.starter24.pl            ---> `(.77.160)` Server DEV/TEST 
- gdpr.starter24.pl                 ---> `(.77.160)` Server DEV/TEST    -   (Rodo)
- atlasmoskwa.starter24.pl          ---> `(.77.190)` Server Moskwa  
- atlas.ruamc.ru                    ---> `(.77.190)` Server Moskwa

### WebSocket

HaProxy przechwytuje też połączenia WebSocketowe i je podtrzymuje. Dzięki temu mamy połączenie `WSS`. PHP WebSocket obsługuje tylko `WS`.


### Web Monitor

HaProxy posiada monitoring pod adresem `/haproxy_statistic`. Dane logowania znajdują się w konfiguracji HaProxy w sekcji `backend haproxy_out_https`.