Lista użytkowników do utworzenia
========
Posiadamy mechanizm to szybkiego tworzenia kont dla dużej liczby użytkowników.

Skrypt zakłada użytownikowi konto z losowym hasłem i wysyła link do zresetowania hasła

#### Jak używać:

- dane użytkowników znajdują się w tabelce `AtlasDB_v.dbo.user_to_create`
- uzupełniamy tabelkę nowymi danymi (nie uzupełniać tabelki `sent_date`)
- w katalogu z projektem odpalamy komendę:
```bash
php bin/console atlas:create-users --env=prod
```
- jeżeli dla danego użytkownika zostanie stworzone konto i wysłany e-mail to uzupełnia się kolumna `sent_date` przez co użytkownik nie jest następnym razem tworzony
