Ecall PSA
========

Atlas obsługuje zdarzenia, które przychodzą z E-CALL PSA.

W tylu celu mamy skrypt (Symfony Command `job:loop get_xml_incoming` ), który co 5 sekund sprawdza, czy jest nowe zlecenie w tabeli `[SETTE].[StarterOperations_Stage].[dbo].[XmlIncoming]`

Wykorzystywany jest serwis `eCallPSAService`, w którym jest cała logika.

- co 5 sekund za pomocą procedury `[dbo].[p_getXmlIncoming]` sprawdzamy czy istnieje nowe zgłoszenie E-call

- jeżeli sprawa istnieje, to pobieramy jej dane (format XML)

- tworzymy sprawę (proces 1200.001)

- wysyłamy powiadomienie do wszystkich konsultantów, którzy mają uprawnienia do 1200.001 i są online

Problemy
---
Zdarzało się, że czasami wysypywał się skrypt w Supervisor. Wtedy trzeba go zresetować:

```bash
sudo supervisorctl restart ecall_psa_job
```

Jeżeli zdażył by się problem, że np zaczyna MOCNO bombardować Atlasa powiadomieniami, to znaczy, że coś się stało z zachowaniem kolejności nr. spraw Ecall
Wtedy najlepiej zatrzymać usługę:
```bash
sudo supervisorctl stop ecall_psa_job
```
<small>(Generalnie nigdy się to jeszcze nie zdażyło, ale niestety jest taka możliwość, przez to, że część mechanizmu stoi na starym SO)