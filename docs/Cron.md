CRON
========

Ten dokument opisuje jakie skrypty mamy odpalane NA PRODUKCJI za pomocą CRONa.

Żeby zdefiniować lub zedytować obecne joby, na serwerze trzeba użyć komendy `crontab -e`.
Trzeba pamiętać, żeby być zalogowanym na użytkowniku `starter`.

<s>Wysyłanie e-mali</s> 
--------
#### Zostało to zastąpione przez JMS JOBs
    
    */1 * * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console atlas:mailbox:send --limit=20 --env=prod

Zczytywanie e-mali ze skrzynek pocztowych
------------

    */1 * * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console atlas:mailbox:run-cron --env=prod

Czyszczenie sesji w telefonii
----------

    0 */1 * * * /usr/bin/php7.0 /var/www/atlas_production/current/web/machine_central/automatically-status.php -l=1 --env=prod

Zapisywanie edytowalnych notatek
-------
Konsultanci nie zawsze zapisują notatki z rozmów. Ten automat co jakiś czas zapisuje je, zeby już nie można było edytować.

    0 */2 * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console atlas:notes:clear-editable-notes --env=prod >> /var/www/atlas_production/shared/var/logs/cron_clear_editable_notes.txt
    

Aktualizacja uzytkowników czata
-------
Codziennie o 23:00 synchronizowani są użytkownicy  (fos_user ---> atlas_chat)

    0 23 */1 * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console cometchat:migrations:user --env=prod
    
Usuwanie tymczasowych plików
-------
Codziennie o 2:55 czyszczone są tymczasowe pliki w Atlasie (np. załączniki do podglądu e-maila)

    55 2 * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console cleaner:temp-files --env=prod
    
<s>Wysyłanie raportów </s>
-------
<u><b>Na ten moment, żadne raporty nie są wysyłane</u></b>

Codziennie o 12:00 wysyła raporty (np. Lista trwających wynajmów)

    0 12 * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console report:send 1 --env=prod >> /var/www/atlas_production/shared/var/logs/cron_send_reports.txt
    0 12 * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console report:send 2 --env=prod >> /var/www/atlas_production/shared/var/logs/cron_send_reports.txt
    
Czyszczenie logów w ELK
-------

    30 1 * * * /var/www/atlas_production/current/app/Resources/scripts/es-remove-expired-index.sh -e "http://10.10.77.154:9200" -o /var/www/atlas_production/shared/var/logs/es_remove_expired_index.txt
    