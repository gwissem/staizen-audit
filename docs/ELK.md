ELK 
========
#####(ElasticSearch, Logstash, Kibana)


Aplikacja działa na serwerze 10.10.77.154
Została postawiona przy pomocy Dockera w wersji 6.6.0. (obecnie najnowsza to +7.0)

Kibana
---
```
http://10.10.77.154:5601
```

// TODO :
Od początku rozważałem, żeby jakoś zablokować dostęp Kibany dla wszystkich, bo obecnie każdy w firmie może wejść.
Kibana chyba nie ma autoryzacji w wersji darmowej. W internetach jest jakaś opcja, żeby zrobić to wykorzystując PROXY w nginx.


Atlas & ELK
---

W Atlasie wykorzystujemy ELK do logowania różnych zdarzeń.
Wszystko jest logowane za pomocą serwisu:  
```
app.elasticsearch.log
```

#### Wykorzystanie w kontrolerze:

Za pomocą tego log jest od razu wysyłany do ElasticSearch
```php
$esService = $this->getContainer()->get('app.elasticsearch.log');
$esService->log('mssql_monitor', ['foo' => 'bar']);
```

Jednak, żeby nie blokować aplikacji, lepiej wykorzystać kolejkę Redisową.
Specjalny skrypt który jest podtrzymywany przez Supervisora, nasłuchuje na Redisie i wysyła logi do ELK.

Przykład:

```php
$log = [
    'name' => 'case_monitor_get',
    'data' => [
        'case_monitor_get.time.full' => 123
    ]
];

$this->container->get('snc_redis.default')->rpush(
    RedisListListenerService::ELASTICA_LOG,
    [
        json_encode($log)
    ]
);
```

#### Indices

W app/config/config.yml pod nazwą "fos_elastica" zdefiniowane są klucze, jakie można wysłać do ELK.
Są one zmapowane, żeby ELK mógł określić ich typ danych - wtedy łatwiej się rysuje wykresy.

#### Usuwanie starych kluczy

Generalnie w ELK trzymamy logi z ostatnich 30 dni.
Jest utworzony CRON który codziennie czyści logi za pomocą skryptu bash'owego

app/Resources/scripts/es-remove-expired-index.sh