
atlas
=====

####1. Instruction for install and configuration project

> ##### 1.1. Clone Repository


```
git clone https://sigmeo.codebasehq.com/starter24/atlas.git atlas
```

> ##### 1.2 PHP EXTENSIONS and Aplications

#####PHP extension zip
___
#####PHP extension LDAP
___
#####redis-server
___
#####PHP extension zmq

######Nowy sposób:
https://eole-io.github.io/sandstone-doc/install-zmq-php-linux
```
sudo apt-get install php-zmq
```

######Stary sposób:

```
sudo apt-get install php7.0-dev libtool pkg-config build-essential autoconf automake php-mailparse
sudo apt-get install libzmq-dev

wget https://github.com/zeromq/libzmq/releases/download/v4.2.3/zeromq-4.2.3.tar.gz
tar -xvf zeromq-4.2.3.tar.gz
cd zeromq-4.2.3
./autogen.sh
./configure && make check
sudo make install
sudo ldconfig

sudo pecl install zmq-beta
sudo echo "extension=zmq.so" > /etc/php/7.0/apache2/conf.d/20-zmq.ini
sudo echo "extension=zmq.so" > /etc/php/7.0/cli/conf.d/20-zmq.ini

```

> ##### 1.3 Install Vendors

```
composer install
```

> ##### 1.4 NPM install modules

```
npm install gulp gulp-sass gulp-concat gulp-uglifycss gulp-uglify run-sequence gulp-livereload gulp-rename
```
> ##### 1.5 Create dirs in /web and set 777/775 chmod

- /web/<b>files</b>
- /web/<b>cached_emails</b>
- /web/<b>mailbox_attachments</b>


> ##### 1.6 Install translations 

```
php bin/console translation:extract en --dir=./src/ --dir=./app/ --output-dir=./app/Resources/translations

php bin/console translation:extract pl --dir=./src/ --dir=./app/ --output-dir=./app/Resources/translations

```

> ##### 1.7 Connect with MSSQL 

For Ubuntu/Debian:
```
https://github.com/mediamonks/symfony-mssql-bundle/blob/master/Resources/doc/0-requirements-nix.rst

https://www.microsoft.com/en-us/sql-server/developer-get-started/php/ubuntu/
```

> (FOR WINDOWS) Create symlink (MKLINK) for assets on Windows

```
mklink /d jabber C:\xampp\htdocs\sigmeo\atlas\src\JabberBundle\Resources\public

mklink /d web C:\xampp\htdocs\sigmeo\atlas\src\WebBundle\Resources\public
```

> (FOR WINDOWS) Install ODBC Driver 11 for SQL Server (connection PDO don't work)

```
https://www.microsoft.com/en-us/download/details.aspx?id=36434

```

> #### 1.8 CRON for checking e-mail box

```
*/1 * * * * /usr/bin/php5 /var/www/html/bin/console atlas:mailbox:run-cron --env=prod
```

> #### 1.9 WebSocket SSL - config

```
https://github.com/GeniusesOfSymfony/WebSocketBundle/blob/master/Resources/docs/Ssl.md
```

My working config:

```
# Certificate
cert = /etc/apache2/ssl/apache.crt
key = /etc/apache2/ssl/apache.key

# Remove TCP delay for local and remote.
socket = l:TCP_NODELAY=1
socket = r:TCP_NODELAY=1

chroot = /var/run/stunnel4/
pid = /stunnel.pid

#local = atlas.starter24.pl 

[websockets]
accept = 9092
connect = atlas.starter24.pl:9091

```

> #### 1.10 WebSocket - start Server

```
php bin/console gos:websocket:server
```

> #### 1.11 Screen

Create screen with name
```
screen -S atlas_production
```

> #### 1.12 wkhtmltopdf - for generate PDF

Nowy sposób:

```
cd ~

wget https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.xenial_amd64.deb
sudo dpkg -i wkhtmltox_0.12.5-1.xenial_amd64.deb 


```

Stary sposób:

```
cd ~
wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
tar vxf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz 
cp wkhtmltox/bin/wk* /usr/bin/
```

Jakby coś nie śmigało:

```
sudo apt-get update
sudo apt-get install xvfb libfontconfig wkhtmltopdf
```

####2. Extra:
http://phreek.org/blog/2011/12/enable-php-ldap-module-in-xampp

> Connecting by VPN

```
For linux install: network-manager-openconnect-gnome

```

####3. Install Helplines Status API
 
- wejść do katalogu /wev/helplines_status
- odpalić:

```
composer install
```
- zmienić nazwę .env.example na .env
- uzupełnić plik .env o brakujące dane (te same co przy naszym projekcie tylko <b>DB_DATABASE=HelpLinesStatus</b>)

- odaplić migracje (<b>ALE TYLKO PRZY TWORZENIU NOWEGO PROJEKTU / BAZY</b>)

```
php artisan migrate
```

- Dodać now vhost (sub-domena) do katalogu **<b>/atlas/web/helplines_status/public</b>. Przykład z localhosta:

```
<VirtualHost *:80>

	ServerName helplines.starter24.dev
	#ServerAdmin webmaster@localhost

	<IfModule proxy_module>
    	ProxyPassMatch ^/(.*\.php(/.*)?)$ fcgi://127.0.0.1:9001/var/www/html/sigmeo/atlas/web/helplines_status/public/$1
  	</IfModule>

	DocumentRoot /var/www/html/sigmeo/atlas/web/helplines_status/public

	<Directory /var/www/html/sigmeo/atlas/web/helplines_status/public>
        AllowOverride All
        Require all granted
        Options FollowSymlinks
  	</Directory>

	DirectoryIndex index.php index.html

	ErrorLog ${APACHE_LOG_DIR}/atlas-error.log
	CustomLog ${APACHE_LOG_DIR}/atlas-access.log combined


</VirtualHost>
```




####4. Components 

> Datepicker

https://github.com/t1m0n/air-datepicker

> E-Mapa

dokumentacja API: http://www.emapi.pl/dokumentacja_emapi_leaflet/files/EMAPI-js.html

> Jabber

Finesse API : https://developer.cisco.com/media/finesseDevGuide2/
Cisco DEVNET: https://learninglabs.cisco.com/lab/finesse-basic-user-rest-apis-with-xmpp-events/step/1

-- działające:
GUIDE FINESSE: https://developer.cisco.com/docs/finesse/#rest-api-dev-guide
Jabber SDK API: https://developer.cisco.com/site/jabber-websdk/develop-and-test/voice-and-video/api/
JABBER: https://developer.cisco.com/site/jabber-websdk/overview/

Log Files
Windows 
C:\Users\{username}\AppData\Local\Cisco\Unified Communications\Jabber Web SDK\Logs
Mac OS X 
/Users/{username}/Library/Logs/Jabber Web SDK/

> 

#####5. Capifony - deploy

- instalacja Ruby, gem, capifony

```
sudo apt-get install ruby

(bez sudo)
gem install capifony 

```

- Deploy

```

cap {stage_name} deploy

```

- When first deploy (new instace project)

```

cap stable deploy:setup

cap stable atlas_tool:create_shared_dir  // ONLY ONCE

cap stable atlas_tool:create_shared_symlinks  // ONLY for prod-sable and prod-unstable

cap stable atlas_tool:init_permissions

```

- deploy z innego brancha

```bash
cap staging deploy -s b=inny_testowy_branch
```

- zmiana logowania przy deployu

```bash
cap staging deploy -s l=important
```


#####6. Helper

>##### SVG TO PNG

```
http://bl.ocks.org/biovisualize/8187844
```

>#####Usunięcie submodule z .git

If you haven't already run git rm –cached path_to_submodule (no trailing slash) as well as rm -rf path_to_submodule, do that!

Then:

Delete the relevant lines from the .gitmodules file. e.g. delete these:

[submodule "path_to_submodule"]
    path = path_to_submodule
    url = https://github.com/path_to_submodule
Delete the relevant section from .git/config. e.g. delete these:

[submodule "path_to_submodule"]
    url = https://github.com/path_to_submodule
rm -rf .git/modules/path_to_submodule
Then, you can finally:

git submodule add https://github.com/path_to_submodule

### Czytanie błędów w MSSQL

```
SELECT * FROM sys.messages
WHERE text like '%duplicate%' and text like '%key%' and language_id = 1033
```

### CometChat - integration

> php composer install

> Ustawić 777 na web/cometchat_lib/writable

> zmienić config do bazy danych:


- app/config/parameters.yml

```
database_host: 127.0.0.1
database_port: null
database_name: atlas_chat
database_user: root
database_password: 
    
```

> run this commands:

Utworzenie bazy danych do czatu:

```
php bin/console doctrine:database:create --connection chat

```

Import tabel i podstawowych danych:
```
php bin/console cometchat:import:tables
```

Ustawienie BaseUrl

```
mysql -u root -p
use atlas_chat;
UPDATE `atlas_chat`.`cometchat_settings` SET `value`='/cometchat_lib/' WHERE `setting_key`='BASE_URL';
```

Migracja użytkowników z Atlasa:
```
php bin/console cometchat:migrations:user

```

> Wejść do /cometchat_lib/admin -> zakładka Layout -> Update color

>###If install new instance cometchat edit code:

- in cometchat_init.php

```
require_once (dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../vendor/autoload.php');
 
// AFTER session_start()
 
if(isset($_COOKIE) && $_COOKIE['PHPSESSID']) {

    $client = new Predis\Client('redis://localhost/2');
    $value = $client->get('session' . $_COOKIE['PHPSESSID']);

    if($value) {
        session_decode($value);
    }

}

```

- in web/cometchat_lib/integration.php

```

// Change getUserId()

function getUserID() {

    if(isset($_SESSION['_sf2_attributes']) && isset($_SESSION['_sf2_attributes']['_security_main'])) {
        $token = unserialize($_SESSION['_sf2_attributes']['_security_main']);

        if($token && $token->getUser()) {
            return $token->getUser()->getId();
        }
    }

    return 0;

}


```

- web/cometchat_lib/cometchat_receive.php (after line 260)

```
/** Add for ATLAS */

$client = new Predis\Client('redis://localhost');
$ids = $client->smembers(\ChatBundle\Service\ChatUserManager::REDIS_CHAT_USERS);

$idsAsString = '(' . implode(',', $ids) . ')';

$sql = str_replace('order by', ' WHERE userid IN ' . $idsAsString . ' order by', $sql);

/** END */
```