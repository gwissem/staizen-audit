
ALTER PROCEDURE [dbo].[s_1009_081]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)

	SET @variant = 1

	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	SELECT 
		@groupProcessInstanceId = group_process_id, 
		@stepId = step_id,
		@rootId = root_id
	FROM process_instance with(nolock) where id = @previousProcessId
	
	declare @condition1 int
	declare @condition2 int


	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '871', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @condition1 = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '872', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @condition2 = value_int FROM @values
	

	IF ISNULL(@condition1,0) = 0 or ISNULL(@condition2,0) = 0
	BEGIN
		EXEC p_attribute_edit
		@attributePath = '870',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueText = dbo.f_translate('Świadczenia usługi holowania pojazdu poszkodowanego jest możliwe tylko jeżeli sprawcą zdarzenia jest kierowca pojazdu ubezpieczonego oraz niezbędny jest protokół Policyjny lub oświadczenie podpisane przez obydwie strony.',default),
		@err = @err OUTPUT,
		@message = @message OUTPUT

		SET @variant = 2
		RETURN
	END
	else
	begin
		declare @driverId int
		declare @vehicleId int

		if not exists(
		select  id
		from	dbo.attribute_value
		where	group_process_instance_id=@groupProcessInstanceId and attribute_path='80'
		)
		begin

			INSERT INTO dbo.attribute_value
					   (root_process_instance_id
					   ,group_process_instance_id
					   ,attribute_path
					   ,created_at
					   ,updated_at
					   ,created_by_id
					   ,updated_by_id
					   ,attribute_id
						)
				 VALUES
					   (@rootId, @groupProcessInstanceId, '80',getdate(),getdate(),1,1,80)

			set @driverId=SCOPE_IDENTITY()
		
			EXECUTE dbo.P_attribute_refresh @attribute_value_id=@driverId
		end

		
		if not exists(
			select  id
			from	dbo.attribute_value
			where	group_process_instance_id=@groupProcessInstanceId and attribute_path='74'
		)
		begin
			INSERT INTO dbo.attribute_value
					   (root_process_instance_id
					   ,group_process_instance_id
					   ,attribute_path
					   ,created_at
					   ,updated_at
					   ,created_by_id
					   ,updated_by_id
					   ,attribute_id
						)
				 VALUES
					   (@rootId, @groupProcessInstanceId, '74',getdate(),getdate(),1,1,74)

			set @vehicleId=SCOPE_IDENTITY()
		
			EXECUTE dbo.P_attribute_refresh @attribute_value_id=@vehicleId
		end

		EXEC p_attribute_edit
		@attributePath = '212',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 0,
		@err = @err OUTPUT,
		@message = @message OUTPUT


		EXEC p_attribute_edit
		@attributePath = '560',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT

		
		EXEC p_attribute_edit
		@attributePath = '291',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 2,
		@err = @err OUTPUT,
		@message = @message OUTPUT

	end 
END

