ALTER PROCEDURE [dbo].[P_AD_Synchronize]
AS begin
SET NOCOUNT ON

declare @domainUsers as table (
 LoginAccount	varchar(100),
 Name	varchar(100),
 Email	varchar(100),
 Firstname	varchar(100),
 Surname	varchar(100),
 Active tinyint,
 extension varchar(32)
)


insert into @domainUsers
	SELECT	sAMAccountName LoginAccount,
			displayName Name,
			mail Email, 
			givenName Firstname,
			sn Surname,
			case userAccountControl when 512 then 1
									when 514 then 0
									when 66050 then 0
									else 1 end Active,
			isnull(ipPhone,telephoneNumber)

	FROM	OpenQuery(ADSI, 'SELECT displayName,name,sAMAccountName,mail,givenName,sn,userAccountControl,telephoneNumber,ipPhone 
							 FROM ''LDAP://starter.local/DC=starter,DC=local'' 
							 where objectClass = 'dbo.f_translate('User',default)' ')




INSERT INTO [dbo].[fos_user]
           ([company_id]
           ,[summary_tool_id]
           ,[username]
           ,[username_canonical]
           ,[email]
           ,[email_canonical]
           ,[enabled]
           ,[salt]
           ,[password]
           ,[last_login]
           ,[locked]
           ,[expired]
           ,[expires_at]
           ,[confirmation_token]
           ,[password_requested_at]
           ,[roles]
           ,[credentials_expired]
           ,[credentials_expire_at]
           ,[firstname]
           ,[lastname]
           ,[slug]
           ,[created_at]
           ,[updated_at]
		   ,[cucum_extension])
 select		1 company_id
           , null summary_tool_id
           ,d.LoginAccount  username
           ,d.LoginAccount  username_canonical
           ,d.Email Email
           ,d.Email email_canonical
           ,1 enabled
           ,'' salt
           ,'' password
           ,null last_login
           ,0 locked
           ,0 expired
           ,null expires_at
           ,null confirmation_token
           ,null password_requested_at
           ,'a:0:{}' roles
           ,0 credentials_expired
           ,null credentials_expire_at
           ,d.Firstname firstname
           ,d.Surname lastname
           ,null slug
           ,getdate() created_at
           ,getdate() updated_at 
		   ,extension
 from @domainUsers d left join
		dbo.fos_user u on d.LoginAccount=u.username 
WHERE u.username is null and d.Active=1 and d.Email is not null
order by 3

UPDATE u
set			username=d.LoginAccount
           ,username_canonical=d.LoginAccount  
           ,Email=d.Email 
           ,email_canonical=d.Email 
           ,enabled=Active 
           ,firstname=d.Firstname 
           ,lastname=d.Surname 
           ,updated_at=getdate()  
		   ,cucum_extension=extension
 from @domainUsers d inner join
	   dbo.fos_user u on d.LoginAccount=u.username 
--WHERE  d.Email is not null

-------------------------------
--- import użytkowników ICS ---
-------------------------------
declare @companyTable as table(locCode nvarchar(200), locName nvarchar(200), partnerId int, extId int)

insert into @companyTable (locCode, locName, partnerId, extId)
select	avc.value_string locCode,
		avn.value_string locName,
		avc.parent_attribute_value_id locId,
		min(ics.id) extId
from	openquery(teka2, 'SELECT id, name, external_contractor_id extCode FROM StarterTeka.companies ') ics inner join
		dbo.attribute_value avc on avc.attribute_path='595,631' and left(avc.value_string,5)=left(ics.extCode,5) collate Polish_CI_AS inner join
		dbo.attribute_value avn on avn.attribute_path='595,84' and avn.parent_attribute_value_id=avc.parent_attribute_value_id left join				
		dbo.attribute_value avlid on avc.id = avlid.parent_attribute_value_id left join
		dbo.company c on c.external_account_id=ics.id 
GROUP BY	avc.value_string,
			avn.value_string,
			avc.parent_attribute_value_id 
UNION
select	avc.value_string locCode,
		avn.value_string locName,
		avp.id locId,
		min(ics.id) extId
from	openquery(teka2, 'SELECT id, name, external_contractor_id extCode FROM StarterTeka.companies ') ics inner join
		dbo.attribute_value avl on avl.attribute_path='595,597,631' and left(avl.value_string,5)=left(ics.extCode,5) collate Polish_CI_AS inner join
		dbo.attribute_value avloc on avloc.attribute_path='595,597' and avloc.id=avl.parent_attribute_value_id inner join
		dbo.attribute_value avp on avp.attribute_path='595' and avp.id=avloc.parent_attribute_value_id inner join
		dbo.attribute_value avc on avc.attribute_path='595,631' and avc.parent_attribute_value_id=avp.id inner join
		dbo.attribute_value avn on avn.attribute_path='595,84' and avn.parent_attribute_value_id=avp.id left join
		dbo.company c on c.external_account_id=ics.id 
GROUP BY	avc.value_string,
			avn.value_string,
			avp.id


INSERT INTO dbo.company
           (name
           ,slug
           ,created_at
           ,updated_at
           ,full_name
           ,external_account_id
           ,type
		   ,code
		   ,partner_location_id)
select	locName,
		locName,
		getdate(),
		getdate(),
		locName,
		extId,
		1,
		locCode,
		partnerId 
from	@companyTable ct left join
		dbo.company c on c.external_account_id=ct.extId
where	c.external_account_id is null

Update	c
set		slug=name,
		code=t.locCode,
		partner_location_id=t.partnerId,
		updated_at=getdate(),
		full_name=t.locName
from	dbo.company c inner join
		@companyTable t on c.external_account_id=t.extId
where	isnull(c.code,'')<>isnull(t.locCode,'') or
		isnull(c.full_name,'')<>isnull(t.locName,'') or
		isnull(c.partner_location_id,0)<>isnull(t.partnerId,0)


declare @users as table (type nvarchar(50), phone_number nvarchar(50), pass nvarchar(100), name nvarchar(100), companyId int, isAccountant int, disabled int, extId int, atlasId int)

insert into @users
select	u.type,
		u.phone_number,
		u.pass,
		u.name,
		min(c.id),
		u.is_accountant,
		u.disabled,
		u.id,
		fu.id 
from	openquery(teka2, 'SELECT u.id, 
		u.type,
		u.phone_number,
		p.pass,
		u.name,
		c.id companyId,
		u.is_accountant,
		u.disabled,
		c.external_contractor_id extCode
FROM 	StarterTeka.users u inner join 
		StarterTeka.companies c on c.id=u.company_id inner join
		StarterTeka.pass p on p.hash=u.password_digest collate utf8_general_ci
 ') u 
  
 	--inner join dbo.company c on u.extCode=c.code collate Polish_CI_AS Stary join---------------------------------------------------------------------------	
	inner join attribute_value avps ON left(avps.value_string,5) = left(u.extCode,5) collate Polish_CI_AS  AND avps.attribute_path IN ('595,631','595,597,631')
	inner join attribute_value avpos ON avps.root_process_instance_id = avpos.root_process_instance_id AND avpos.attribute_path IN ('595,631','595,597,631')
	join dbo.company c on c.code = avpos.value_string collate Polish_CI_AS 
	---------------------------------------------------------------------------------------------------------------------------------------------------------
	left join
	dbo.fos_user fu on fu.username=u.phone_number collate Polish_CI_AS
	-- inner join (
	
	--select	distinct right(replace(avp.value_string,'x',''),9) phone 
	--from	dbo.attribute_value avp inner join
	--		dbo.attribute_value avt on avp.parent_attribute_value_id=avt.parent_attribute_value_id 
	--where	avp.attribute_path = '595,597,611,642,656' and avt.value_int=1 and avp.value_string is not null
	
	--) ap on right(u.phone_number,9)=right(ap.phone,9) collate Polish_CI_AS
group by 
		u.type,
		u.phone_number,
		u.pass,
		u.name,
		u.is_accountant,
		u.disabled,
		u.id,
		fu.id 
order by u.phone_number

INSERT INTO dbo.fos_user
           (company_id
           ,username
           ,username_canonical
           ,email
           ,email_canonical
           ,enabled
           ,salt
           ,password
           ,expired
           ,expires_at
           ,roles
           ,firstname
           ,lastname
           ,slug
           ,created_at
 ,updated_at
           ,deleted_at
           ,accepted_regulations
		   ,locked
		   ,credentials_expired
           )
 select companyId, phone_number, phone_number, '', '', 1, dbo.f_translate(dbo.f_translate('afersmewdewdewret34',default),default), dbo.f_translate('AAAA',default), 0 expired, null expires_at, 'a:0:{}' roles,
		'' firstname, name lastname, phone_number slug, getdate(), getdate(), null, 0, 0, 0
 from	@users 
 where	atlasId is null



 ----------------------

	declare @uidpass int
	declare @pass nvarchar(100)

	declare kur cursor LOCAL for
		 select fu.id,
				u.pass
		 from	@users u inner join
				dbo.fos_user fu on fu.username=u.phone_number
		 where	company_id<>1 and fu.salt=dbo.f_translate(dbo.f_translate('afersmewdewdewret34',default),default) and enabled=1 --and len(fu.password)<6 

	OPEN kur;
	FETCH NEXT FROM kur INTO @uidpass, @pass;
	WHILE @@FETCH_STATUS=0
		BEGIN
		DECLARE @URL VARCHAR(8000)
		DECLARE @HTTPStatus int
		SET @HTTPStatus = null
		SET @URL = null
		
		SET @URL = [dbo].[f_getDomain]() + '/atlas-change-password/bky3xabxw7sc88wwk4ww84kcgssgock?user='+cast(@uidpass as varchar(20))+'&pass='+@pass
		print @URL
		DECLARE @Response varchar(8000)
		DECLARE @Obj int 
		 
		DECLARE @xmlT as table ( answerXML XML )
		EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
		EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
		EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
		EXEC sp_OAMethod @Obj, send, NULL, ''
		EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
		EXEC sp_OADestroy @Obj
		
		IF @HTTPStatus <> 200 
		BEGIN
			
			EXEC dbo.p_log_automat
			@name = 'ad_synchronize',
			@content = @uidpass,
			@param1 = @HTTPStatus
			
		END 
		
		FETCH NEXT FROM kur INTO @uidpass, @pass;
	END
	CLOSE kur
	DEALLOCATE kur


	
 -------------------------------

UPDATE fu 
set		company_id=u.companyId,
		username=u.phone_number,
		username_canonical=u.phone_number,
        email='',
        email_canonical='',
        enabled=case when u.disabled=1 then 0 else 1 end,
        lastname=u.name,
        slug=u.phone_number,
        updated_at=getdate(),
        deleted_at=case when u.disabled=1 and deleted_at is null then getdate() when u.disabled <> 1 then null else deleted_at end
from	@users u inner join
		dbo.fos_user fu on u.AtlasId=fu.id
where	isnull(company_id,0)<>isnull(u.companyId,0) or 
		isnull(username,'')<>isnull(u.phone_number,'') or 
		isnull(username_canonical,'')<>isnull(u.phone_number,'') or
        isnull(enabled,0)<>isnull(case when u.disabled=1 then 0 else 1 end,0) or
        isnull(lastname,'')<>isnull(u.name,'') or
        isnull(slug,'')<>isnull(u.phone_number,'')

-- włożenie do grup

INSERT INTO dbo.user_user_group (user_id,group_id)
select	id,43
from	@users uics inner join 
		dbo.fos_user u on uics.atlasId=u.id left join 
		dbo.user_user_group g on u.id=g.user_id and g.group_id=43 -- kontraktorzy
where	u.enabled=1 and 
		g.user_id is null
		
		
INSERT INTO dbo.user_user_group (user_id,group_id)
select	u.id,3502
	FROM	OpenQuery(ADSI, 'SELECT displayName,name,sAMAccountName,mail,givenName,sn,userAccountControl,telephoneNumber,ipPhone 
							 FROM ''LDAP://starter.local/DC=starter,DC=local'' 
							 where objectClass = 'dbo.f_translate('User',default)' and description='dbo.f_translate('Viavox',default)' ') ad inner join
							 dbo.fos_user u on u.email=ad.mail left join 
		dbo.user_user_group g on u.id=g.user_id and g.group_id=3502 
where	g.user_id is null
-------------------------------
-------------------------------



declare @userGroups as table (userId int, groupsId varchar(4000))

--;WITH all_groups
--AS
--(
--    select	user_id, 
--			group_id
--	from	dbo.user_user_group
--	--where	user_id=371

--    UNION ALL
	
--    SELECT	ag.user_id,
--			ug.parent_id
--    FROM	all_groups ag inner join
--			dbo.user_group ug on ug.id=ag.group_id
--)
--insert into @userGroups
--SELECT	ag.user_id,
--		dbo.Concatenate(group_id)
--FROM	all_groups ag 
--group by ag.user_id

--update u
--set u.groups_token=ug.groupsId 
--from dbo.fos_user u inner join
--	 @userGroups ug on ug.userId=u.id
--where isnull(u.groups_token,-1)<>isnull(ug.groupsId,-1)


end




