ALTER PROCEDURE [dbo].[s_1009_063]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int = 1,
	@errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @acceptedOrder SMALLINT = 0
	DECLARE @amountRefused SMALLINT
	DECLARE @stepId VARCHAR(32)
	DECLARE @email VARCHAR(400)
	DECLARE @subject VARCHAR(400)
	DECLARE @p1 VARCHAR(400)
	DECLARE @body VARCHAR(MAX)
	DECLARE @partnerLocId INT
	DECLARE @newProcessId INT
	declare @VIN nvarchar(100)
	declare @regNumber nvarchar(100)
	
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	-- Sprawdzenie czy zadanie jest przekładane
	
	IF @variant = 99
	BEGIN
		
		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '767,221', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @amountRefused = value_int FROM @values
		
		SET @amountRefused = ISNULL(@amountRefused, 0)
		
		PRINT 'START @amountRefused: ' + CAST(@amountRefused AS VARCHAR) 
		
		IF @amountRefused = 0
		BEGIN
			-- Pierwsza próba przełożenia, wysłanie e-maila do PZ
			
			PRINT dbo.f_translate('Leci e-mail',default)
			
					
			INSERT  @values EXEC dbo.p_attribute_get2
		 		@attributePath = '767,773,368',
		 		@groupProcessInstanceId = @groupProcessInstanceId
		 	SELECT @email = value_string FROM @values
		 	
		 	delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '73,71', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @VIN = value_string FROM @values
		
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @regNumber = value_string FROM @values
			
--			SET @body=dbo.f_translate('Czy otrzymali Państwo zlecenie o numerze sprawy: ',default) + dbo.f_caseId(@previousProcessId)  + '?
--					<BR><BR><BR>
--					Best regards,<BR>
--					Starter24'
--							
			SET @body='Hello from Poland. Did you get our request for roadside assistance?
					<BR>
					Details:<BR>
					Starter24 Case Number: ' +  dbo.f_caseId(@previousProcessId) + ' <BR>
					' + ISNULL('Plate number: '+@regNumber,'')+' '+ ISNULL('VIN: '+@VIN,'')+'<BR>
					<BR><BR>
					Best regards,<BR>
					Starter24'
					
			SET @subject = 'Did you get our request for roadside assistance? - ' + dbo.f_caseId(@previousProcessId)

			DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('Arc')

			EXECUTE dbo.p_note_new 
				 @groupProcessId = @groupProcessInstanceId
				,@type = dbo.f_translate('email',default)
				,@content = dbo.f_translate('Zapytanie o otrzymaniu zlecenie',default)
				,@email = @email
				,@userId = 1  -- automat
				,@subject = @subject
				,@direction=1
				,@dw = ''
				,@udw = ''
				,@sender = @senderEmail
				,@additionalAttachments = ''
				,@emailBody = @body
				,@err=@err OUTPUT
				,@message=@message OUTPUT
			
		END
		ELSE IF @amountRefused >= 3
		BEGIN
			
			-- Były juz 3 próby zadzwonienia, zadanie do KZ
			
			SET @variant = 2
			RETURN 
			
		END
		
		IF @amountRefused < 3
		BEGIN
			 
			-- Próby dzwonienia lub wysłanie e-maila 
			PRINT 'PRÓBA DZWONIENIA LUB WYSŁANIE E_MAILA'
			
			 SET @amountRefused = @amountRefused + 1
			
			 EXEC p_attribute_edit
   				@attributePath = '767,221',
	   			@groupProcessInstanceId = @groupProcessInstanceId,
	   			@stepId = @stepId,
	   			@valueInt = @amountRefused,
	   			@err = @err OUTPUT,
	   			@message = @message OUTPUT
   			
			 UPDATE dbo.process_instance SET postpone_date = DATEADD(MINUTE, 15, GETDATE()) WHERE id = @previousProcessId
		
		 END
		 
		 PRINT 'END @amountRefused: ' + CAST(@amountRefused AS VARCHAR)
		 
	END
	ELSE
	BEGIN
		
		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '767,769', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @acceptedOrder = value_int FROM @values
		
		IF ISNULL(@acceptedOrder, 0) = 1
		BEGIN
			
			-- ZLECENIE PRZYJĘTE 
			
			DECLARE @etaMinutes INT
			DECLARE @eta DATETIME
		
			INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '767,698',
				@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @etaMinutes = value_int FROM @values
			
			SET @eta = DATEADD(MINUTE, @etaMinutes, GETDATE())
			
			EXEC p_attribute_edit
	   			@attributePath = '767,107',
	   			@groupProcessInstanceId = @groupProcessInstanceId,
	   			@stepId = @stepId,
	   			@valueDate = @eta,
	   			@err = @err OUTPUT,
	   			@message = @message OUTPUT
	   			
   			EXEC p_attribute_edit
	   			@attributePath = '107',
	   			@groupProcessInstanceId = @groupProcessInstanceId,
	   			@stepId = @stepId,
	   			@valueDate = @eta,
	   			@err = @err OUTPUT,
	   			@message = @message OUTPUT
	   		
	   		DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2
		 		@attributePath = '767,773,704',
		 		@groupProcessInstanceId = @groupProcessInstanceId
		 	SELECT @partnerLocId = value_int FROM @values
		 	
		 	EXEC p_attribute_edit
				@attributePath = '741',
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueInt = @partnerLocId,
				@err = @err OUTPUT,
				@message = @message OUTPUT
			
--		 	EXEC p_attribute_edit
--	   			@attributePath = '610',
--	   			@groupProcessInstanceId = @groupProcessInstanceId,
--	   			@stepId = @stepId,
--	   			@valueInt = @partnerLocId,
--	   			@err = @err OUTPUT,
--	   			@message = @message OUTPUT
		 	
	   		-- TODO  : wysłanie sms'a do klienta

      DECLARE @eventType INT
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @eventType = value_int FROM @values

			DECLARE @country nvarchar(100)
			DELETE from @values
			INSERT @values exec p_attribute_get2  @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @country = value_string from @values


      DECLARE  @platformGrupName NVARCHAR(100)
      EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGrupName OUTPUT
      IF isnull(@platformGrupName,'') like dbo.f_translate('PSA',default) and isnull(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) = dbo.f_translate(dbo.f_translate('Polska',default),default)
        BEGIN
          IF @eventType = 2
            BEGIN
              DECLARE @process30minID int
              -- 					monit 30 min
              EXEC p_process_new @stepId = '1009.101',
                                 @groupProcessId = @groupProcessInstanceId,
                                 @rootId = @rootId,
                                 @parentProcessId = @rootId,
                                 @processInstanceId = @process30minID output,
                                 @err = @err OUTPUT,
                                 @message = @message OUTPUT
              -- 					monit 24h
              DECLARE @process24hID int
              -- 					monit 30 min
              EXEC p_process_new @stepId = '1009.102',
                                 @groupProcessId = @groupProcessInstanceId,
                                 @rootId = @rootId,
                                 @parentProcessId = @rootId,
                                 @processInstanceId = @process24hID output,
                                 @err = @err OUTPUT,
                                 @message = @message OUTPUT
            end
        end
	   		-- SKOK DO PANELU ŚWIADCZEŃ
	
			SELECT TOP 1 @newProcessId = p.id
			FROM dbo.process_instance p
			WHERE p.root_id = @rootId
			AND p.step_id = '1011.010'
			AND p.active = 1
			ORDER BY p.id DESC
			
			IF @newProcessId IS NOT NULL
			BEGIN
				
				INSERT INTO dbo.process_instance_flow (previous_process_instance_id, next_process_instance_id, active, variant)
				VALUES (@previousProcessId, @newProcessId, 1, 1)
				
			END
	   			
		END
		ELSE
		BEGIN
			
			-- PZ ODMÓWIŁ
			
			-- Powód odmowy
			
			DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2
		 		@attributePath = '767,773,704',
		 		@groupProcessInstanceId = @groupProcessInstanceId
		 	SELECT @partnerLocId = value_int FROM @values
		 	
		    EXEC p_add_service_refuse_reason
				@groupProcessInstanceId = @groupProcessInstanceId,
				@partnerLocationId = @partnerLocId,
				@reasonId = 101
				
			SET @p1 = dbo.f_caseId(@rootId)
	  		EXEC dbo.p_log_automat
	 		@name = 'service_NHzG_automat',
	  		@content = dbo.f_translate('PZ odmówił.',default),
	  		@param1 = @p1,
	  		@param2 = @partnerLocId
  		
			-- CZYSZCZENIE DANYCH	
			
			EXEC p_attribute_edit
	   			@attributePath = '767,698',
	   			@groupProcessInstanceId = @groupProcessInstanceId,
	   			@stepId = @stepId,
	   			@valueInt = NULL,
	   			@err = @err OUTPUT,
	   			@message = @message OUTPUT
	   			
	   		EXEC p_attribute_edit
	   			@attributePath = '767,769',
	   			@groupProcessInstanceId = @groupProcessInstanceId,
	   			@stepId = @stepId,
	   			@valueInt = NULL,
	   			@err = @err OUTPUT,
	   			@message = @message OUTPUT
			
	   		EXEC p_attribute_edit
	   			@attributePath = '767,221',
	   			@groupProcessInstanceId = @groupProcessInstanceId,
	   			@stepId = @stepId,
	   			@valueInt = 0,
	   			@err = @err OUTPUT,
	   			@message = @message OUTPUT	
	   			
			SET @variant = 3
			
		END
		
	END
	
--	EXEC p_process_jump
--			@previousProcessId = @previousProcessId,
--			@nextStepId = '1011.010',
--			@doNext = 0,
--			@parentId = NULL,
--			@rootId = @rootId,
--			@variant = @variant,
--			@groupId = @rootId,
--		    @err = @errId output,
--		    @message = @message output
	
END