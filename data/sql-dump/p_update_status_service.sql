ALTER PROCEDURE [dbo].[p_update_status_service]
		@processInstanceId INT,
		@variant INT
	--@err INT OUTPUT,`
	--@message NVARCHAR(255) OUTPUT

AS
	BEGIN

		PRINT '------------------ START p_update_status_service'

		-- DECLARATIONS
		DECLARE @rootId INT
		DECLARE @stepId NVARCHAR(64)
		DECLARE @servicesIds NVARCHAR(255)
		DECLARE @groupProcessInstanceId INT
		DECLARE @fixingOrTowing INT
		DECLARE @existServiceStatusUpdate SMALLINT = 0
		DECLARE @ServiceReport int
		DECLARE @serviceGroupId INT
		DECLARE @sparxId NVARCHAR(255)
		DECLARE @platformId int
		DECLARE @addedRelated int
		DECLARE @err TINYINT
		DECLARE @message NVARCHAR(255)
		-- OPERATIONS

		-- deklaracja pomocniczej tabelki
		DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

		SELECT @rootId = root_id, @groupProcessInstanceId = group_process_id, @stepId = step_id
		FROM dbo.process_instance with(nolock) where id = @processInstanceId

		-- sprawdzenie czy na tym kroku trzeba aktualizować status jakieś usługi
		SELECT @existServiceStatusUpdate = count(id) FROM dbo.service_status_update with(nolock)
		WHERE step_id = @stepId

		PRINT Ndbo.f_translate('existServiceStatusUpdate: ',default) + (CAST(@existServiceStatusUpdate AS nvarchar(32)))

		EXEC p_running_services
				@groupProcessInstanceId = @groupProcessInstanceId,
				@servicesIds = @servicesIds OUTPUT --activated services



		-- trzeba zaktualizować jakiś status
		IF @existServiceStatusUpdate > 0
			BEGIN

				DECLARE @serviceId INT
				DECLARE @currentServiceProgress SMALLINT = 0
				DECLARE @statusDictionaryId SMALLINT = 0
				DECLARE @icsStatusId INT
				DECLARE @icsId INT
				DECLARE @lastStatusId INT
				DECLARE @towingStatus INT
				DECLARE @fixingStatus INT
				DECLARE @partnerReason INT
				DECLARE @canDoTowing INT
				DECLARE @code NVARCHAR(10)
				DECLARE @onPlace INT
				DECLARE @transportFixedOrNot int

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '109', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @towingStatus = value_int FROM @values

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '209', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @fixingStatus = value_int FROM @values

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '208', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @onPlace = value_int FROM @values

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '700', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @partnerReason = value_int FROM @values

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '274', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @canDoTowing = value_int FROM @values

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @platformId = value_int FROM @values

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @fixingOrTowing = value_int FROM @values

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '232', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @addedRelated = value_int FROM @values

				set @transportFixedOrNot=null

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '948', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @transportFixedOrNot = value_int FROM @values


				declare @towing2 int

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '291', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @towing2 = value_int FROM @values

				if @towing2=2 set @fixingOrTowing=15

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '609', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @icsId = value_int FROM @values

				--		EXEC dbo.p_get_ics_finish_code @icsId = @icsId, @code = @code OUTPUT
				DECLARE kur cursor LOCAL for
					SELECT [service_id] FROM dbo.service_status_update with(nolock) WHERE step_id = @stepId and (service_id=@fixingOrTowing or service_id not in (1,2,15))
				OPEN kur;
				FETCH NEXT FROM kur INTO @serviceId;
				WHILE @@FETCH_STATUS=0
					BEGIN
						-- WHILE BEGIN
						SET @statusDictionaryId = 0
						SET @serviceGroupId = @groupProcessInstanceId

						SELECT TOP 1 @lastStatusId = status_dictionary_id
						FROM dbo.service_status with(nolock)
						WHERE group_process_id = @groupProcessInstanceId AND serviceId = @serviceId
						order by id desc

						set @ServiceReport=null

						PRINT dbo.f_translate('serviceId: ',default) + (CAST(@serviceId AS nvarchar(32)))

						-- Pobranie progresu i treści statusu

						SELECT TOP 1 @currentServiceProgress = ssd.progress
						FROM dbo.service_status as ss with(nolock)
									 LEFT JOIN dbo.service_status_dictionary as ssd with(nolock) ON ss.status_dictionary_id = ssd.id
						WHERE ss.group_process_id = @groupProcessInstanceId
							AND ss.serviceId = @serviceId
						ORDER BY ss.id DESC

						PRINT dbo.f_translate('currentServiceProgress: ',default) + (CAST(@currentServiceProgress AS nvarchar(32)))

						-- Sprawdzenie czy holowanie czy naprawa
						IF @serviceId = 1 OR @serviceId = 2
							BEGIN
								DELETE FROM @values
								INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
								SELECT @fixingOrTowing = value_int FROM @values
							END

						IF @serviceId IN (1,15)
							BEGIN

								-------------------------------------------------------------------------------
								---- 							HOLOWANIE			 						---
								-------------------------------------------------------------------------------

								--+ 	Organizowane (1)
								--+ 	Kontraktor przyjął zlecenie. Szacowane przybycie o godzinie [godzina] (2)
								--+		Kontraktor na miejscu (3)
								--+		Kontraktor holuje (3)
								--		Holuje na parking (3)
								--		Na parkingu (3)
								--		Drugie holowanie (3)
								--+-	Zakończono holowanie. Pojazd w [nazwa ASO], [miejscowość] (4)
								--		Zakończono usługę holowania (4)
								--		Anulowane bezkosztowo (5)
								--		Pusty wyjazd (5)
								--		Samodzielna organizacja przez Klienta (6)

								IF @stepId = '1009.079' OR (@stepId = '1009.042' AND @fixingOrTowing = @serviceId AND @variant <> '2')
									BEGIN
										SET @statusDictionaryId = 35
									END
								ELSE IF @stepId = '1009.043' AND @fixingOrTowing = @serviceId AND @variant IN (1,2)
									BEGIN
										SET @statusDictionaryId = 35
									END
								ELSE IF (@stepId = '1009.011' OR @stepId = '1009.003') AND @fixingOrTowing = @serviceId
									BEGIN
										IF @variant IN (3,99) AND @stepId = '1009.011'
											BEGIN
												-- Organizowane
												SET @statusDictionaryId = 35
											END
										ELSE IF @stepId = '1009.003' AND @variant = 5
											BEGIN
												-- Ścieżka skrócona
												SET @statusDictionaryId = 43
											END
										ELSE
											BEGIN
												-- Przyjął zlecenie
												SET @statusDictionaryId = 37
											END

									END
								ELSE IF (@stepId = '1009.061' AND @fixingOrTowing = @serviceId AND @variant = 1) OR (@stepId = '1009.034' AND @variant <> 4) OR (@stepId = '1009.047' AND @variant = 1)
									BEGIN

										-- Organizowane
										SET @statusDictionaryId = 35
										PRINT 'NEW STATUS: Organizowane (24)'

									END
								--					ELSE IF @stepId = '1009.063' AND @fixingOrTowing = 1 AND @variant = 1
								--					BEGIN
								--
								--						-- Przyjął zlecenie
								--						SET @statusDictionaryId = 37
								--
								--					END
								ELSE IF (@stepId = '1009.018' OR @stepId = '1009.063') AND @fixingOrTowing = @serviceId
									BEGIN
										-- Kontraktor przyjął zlecenie. Szacowane przybycie o godzinie [godzina]
										SET @statusDictionaryId = 55
										PRINT dbo.f_translate('NEW STATUS: Kontraktor w drodze',default)
									END
								ELSE IF @stepId = '1009.068' AND @variant IN (1,2)
									BEGIN
										-- Zakończono usługę holowania
										SET @statusDictionaryId = 44
									END
								ELSE IF @stepId = '1009.064' AND @fixingOrTowing = @serviceId AND @variant = 1
									BEGIN
										-- PIERWOTNIE BYŁO HOLOWANIE, ale jednak Naprawa na drodze

										-- Kontraktor przyjął
										SET @statusDictionaryId = -1

										-- Dodanie statusu do naprawy, że zakończono
										EXEC [dbo].[p_add_service_status]
												@groupProcessInstanceId = @groupProcessInstanceId,
												@status = 1005,
												@serviceId = 2

										--						INSERT INTO dbo.service_status (group_process_id, status_dictionary_id, serviceId, created_at, updated_at)
										--						VALUES(@groupProcessInstanceId, 1005, 2, GETDATE(), GETDATE());


									END
								ELSE IF @stepId = '1009.064' AND @fixingOrTowing = 2 AND @variant = 2
									BEGIN
										-- PIERWOTNIE BYŁA NAPRAWA na DRODZE, ale jednak Holowanie

										-- Dodanie statusu do naprawy, że jednak anulowano
										--							INSERT INTO dbo.service_status (group_process_id, status_dictionary_id, serviceId, created_at, updated_at)
										--							VALUES(@groupProcessInstanceId, -1, 2, GETDATE(), GETDATE());

										EXEC [dbo].[p_add_service_status]
												@groupProcessInstanceId = @groupProcessInstanceId,
												@status = -1,
										@serviceId = 2

										-- Kontraktor przyjął
										SET @statusDictionaryId = 36

									END
								ELSE IF @stepId IN ('1009.004','1009.051') AND @fixingOrTowing = @serviceId
									BEGIN
										-- Kontraktor na miejscu
										IF ISNULL(@onPlace,0) <> 0
											BEGIN
												SET @statusDictionaryId = 38
												PRINT 'NEW STATUS: Kontraktor na miejscu (38)'
											END
										ELSE
											BEGIN
												SET @statusDictionaryId = 55
												PRINT dbo.f_translate('NEW STATUS: Kontraktor w drodze',default)
											END
									END
								ELSE IF @stepId = '1009.005' AND @fixingOrTowing = @serviceId
									BEGIN
										EXEC dbo.p_get_ics_finish_code @icsId = @icsId, @code = @code OUTPUT
										IF @code IN ('9214685','9214687','9214688','9214689','9214690')
											BEGIN
												-- pusty wyjazd
												SET @statusDictionaryId = -2
											END
										ELSE IF @variant = 2
											BEGIN
												-- holuje
												SET @statusDictionaryId = 39
											END

									END
								--					ELSE IF @stepId = '1009.076' AND @fixingOrTowing = 1
								--					BEGIN
								--						SET @statusDictionaryId = 43
								--					END
								--					ELSE IF @stepId = '1009.036' AND @fixingOrTowing = @serviceId  AND @variant = 0
								--					BEGIN
								--						SET @statusDictionaryId = 43
								--					END
								ELSE IF @stepId = '1009.074' AND ISNULL(@lastStatusId,0) NOT IN (43,44,45) AND @fixingOrTowing = @serviceId AND @variant <> 99
									BEGIN
										SET @statusDictionaryId = 43
									END
								ELSE IF @stepId = '1009.006' AND ISNULL(@lastStatusId,0) NOT IN (44,45) AND @fixingOrTowing = @serviceId
									BEGIN
										-- Zakończono holowanie. Pojazd w [nazwa ASO], [miejscowość]
										SET @statusDictionaryId = 43
										PRINT 'NEW STATUS: Zakończono holowanie. Pojazd w [nazwa ASO], [miejscowość] (43)'
									END
								ELSE IF @stepId = '1009.008' AND ISNULL(@lastStatusId,0) NOT IN (45) AND @fixingOrTowing = @serviceId
									BEGIN

										-- Zakończono usługę holowania
										SET @statusDictionaryId = 44
									END
								ELSE IF @stepId = '1009.055' AND ISNULL(@lastStatusId,0) NOT IN (-1,-2,45)
									BEGIN

										IF @fixingStatus = 0 AND @canDoTowing = 1
											BEGIN
												-- Kontraktor nie naprawił, ale holuje
												SET @statusDictionaryId = 39
											END

										IF @towingStatus = 1 AND @fixingOrTowing = @serviceId
											BEGIN
												-- Rozpoczęto holowanie
												SET @statusDictionaryId = 39
											END
										ELSE IF @towingStatus = 2 AND @fixingOrTowing = @serviceId
											BEGIN
												-- Naprawiono na miejscu, holowanie anulowano bezkosztowo
												SET @statusDictionaryId = -1
											END
										ELSE IF @towingStatus = 0 AND @fixingOrTowing = @serviceId
											BEGIN
												-- Nie może holować
												IF ISNULL(@partnerReason,999) IN (2,3)
													BEGIN
														-- anulowane bezkosztowo (awaria, brak sprzętu)
														SET @statusDictionaryId = -1
													END
												ELSE IF ISNULL(@partnerReason,999) IN (12,50)
													BEGIN
														-- pusty wyjazd (wina klienta)
														SET @statusDictionaryId = -2
													END
											END
									END
								ELSE IF @stepId = '1009.057' AND @fixingOrTowing = @serviceId AND ISNULL(@lastStatusId,0) NOT IN (-1,-2,45) 
									BEGIN
										SET @statusDictionaryId = 39

										exec dbo.s_RS @processInstanceId=@groupProcessInstanceId

									END
								ELSE IF @stepId = '1009.052' AND @fixingOrTowing = @serviceId
									BEGIN
										IF @variant = 2
											BEGIN
												SET @statusDictionaryId = -1
											END
										ELSE IF @variant = 3
											BEGIN
												SET @statusDictionaryId = -2
											END
									END
								ELSE IF @stepId = '1009.059' AND @fixingOrTowing = @serviceId AND @variant < 90
									BEGIN
										SET @statusDictionaryId = -1
									END

							END
						ELSE IF @serviceId = 3
							BEGIN

								-------------------------------------------------------------------------------
								---- 							Auto zastępcze				 						---
								-------------------------------------------------------------------------------

								--+	Czeka na organizację (1)
								--+	Organizowane (1)
								--+  Wypożyczalnia przyjęła (1)
								--+  Pojazd zastępczy w drodze do Klienta Szacowane przybycie za [liczba minut] minut (2)
								--	Pojazd zastępczy dotarł/ odebrany przez Klienta
								--	Wynajem trwa. Doba [liczba – która doba wynajmu], z [liczba – na ile dni wysłano gop] dób ‚ maksymalny okres wynajmu to (liczba) doby
								--	Zakończono wynajem (4)
								--	Samodzielna organizacja przez Klienta

								PRINT '---status!'

								IF @stepId = '1007.037' --  OR (@stepId = '1007.040' AND ISNULL(@variant,99) = 99)
									BEGIN
										PRINT '---czeka na organizacje'
										-- Czeka na organizację (1)
										SET @statusDictionaryId = 90
									END
								ELSE IF (@stepId = '1007.040' AND @variant NOT IN (2,99)) OR (@stepId = '1007.052' AND @variant = 2)
									BEGIN
										-- Organizowane (1)
										SET @statusDictionaryId = 91
									END
								ELSE IF (@stepId = '1007.040' AND @variant = 2) OR (@stepId = '1007.067' AND @variant = 2) OR (@stepId = '1007.046' AND @variant = 2) OR @stepId = '1007.063'
									BEGIN
										-- Anulowano bezkosztowo
										SET @statusDictionaryId = -1
									END
								ELSE IF @stepId = '1007.044'
									BEGIN
										-- Wypożyczalnia przyjęła
										SET @statusDictionaryId = 92

										IF @addedRelated IS NOT NULL
											BEGIN
												SET @statusDictionaryId = 95
											END
									END
								ELSE IF @stepId = '1007.050'
									BEGIN
										-- Pojazd zastępczy w drodze do Klienta Szacowane przybycie za...
										SET @statusDictionaryId = 93
									END
								ELSE IF (@stepId = '1007.051' AND @variant = 1) OR (@stepId = '1007.052' AND @variant = 3)
									BEGIN
										-- Pojazd zastępczy dotarł/ odebrany przez Klienta
										SET @statusDictionaryId = 94
									END
								ELSE IF @stepId = '1007.053' OR @stepId = '1007.068'
									BEGIN
										-- Wynajem trwa. Liczba dni wynajmu: {#rentalSummary(duration)#}, koniec wynajmu: {#rentalSummary(end_date)#}
										SET @statusDictionaryId = 95
									END
								ELSE IF @stepId = '1007.054' AND @variant = 1
									BEGIN
										-- Oczekiwanie na potwierdzenie kosztów
										SET @statusDictionaryId = 1001
									END
								ELSE IF (@stepId = '1007.054' AND @variant = 2) OR (@stepId = '1007.070' AND ISNULL(@variant,99) = 1) OR (@stepId IN ('1007.059', '1007.091') AND ISNULL(@variant,1) <> 99) 
									BEGIN
										-- Zakończono wynajem
										SET @statusDictionaryId = 96
									END
							END
						ELSE IF @serviceId = 5
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "Taxi"',default)

								-------------------------------------------------------------------------------
								---- 							TAXI				 						---
								-------------------------------------------------------------------------------

								--+      Organizowane (1)
								--      Kontraktor przyjął (1)
								--+      Kontraktor jedzie do klienta (2)
								--		Kontraktor na miejscu (3)
								--+		Zakończono (4)
								--      Anulowane bezkosztowo (5)
								--      Odwołano z kosztami (5)
								--		Samodzielna organizacja przez Klienta (6)

								IF @stepId = '1016.002' OR @stepId = '1016.017'
									BEGIN
										-- Organizowane (1)
										SET @statusDictionaryId = 64
										PRINT 'NEW STATUS: Organizowane (64)'
									END
								ELSE  IF @stepId = '1016.020' OR @stepId = '1016.006'
									BEGIN
										-- Kontraktor jedzie do klienta (2)
										SET @statusDictionaryId = 66
										PRINT 'NEW STATUS: Organizowane (66)'
									END
								ELSE  IF @stepId = '1016.007'
									BEGIN
										-- Zakończono (4)
										SET @statusDictionaryId = 68
										PRINT 'NEW STATUS: Organizowane (68)'
									END
								ELSE  IF @stepId = '1016.021'
									BEGIN
										IF @lastStatusId IS NULL
											BEGIN
												SET @statusDictionaryId = 64
											END

										IF @variant = 1
											BEGIN
												SET @statusDictionaryId = 65
											END
										ELSE IF @variant = 2
											BEGIN
												SET @statusDictionaryId = 68
											END
									END

							END
						ELSE IF @serviceId = 4
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "Nocleg"',default)

								-------------------------------------------------------------------------------
								---- 							NOCLEG				 						---
								-------------------------------------------------------------------------------

								--	Organizowane (1)
								--	Zorganizowano (3)
								--	Zakończono (4)
								--+ Samodzielna organizacja przez Klienta (6)

								IF @stepId = '1014.002' OR @stepId = '1014.021'
									BEGIN
										-- Organizowane (1)
										SET @statusDictionaryId = 58
										PRINT 'NEW STATUS: Organizowane (58)'
									END
								ELSE  IF @stepId = '1014.040'
									BEGIN
										-- Zorganizowano (3)
										SET @statusDictionaryId = 59
										PRINT 'NEW STATUS: Zorganizowano (66)'
									END
								ELSE  IF @stepId = '1014.004'
									BEGIN
										-- Zakończono (4)
										SET @statusDictionaryId = 60
										PRINT 'NEW STATUS: Zakończono (68)'
									END
								ELSE  IF (@stepId = '1014.086' or @stepId = '1014.085') and @variant = 2
									BEGIN
										--Samodzielna organizacja przez Klienta (6)
										SET @statusDictionaryId = 63
									END

							END
						ELSE IF @serviceId = 7 and isnull(@transportFixedOrNot,0)=0
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "Tranport"',default)

								-------------------------------------------------------------------------------
								---- 							Tranport				 						---
								-------------------------------------------------------------------------------

								--	Organizowane (1)
								--	Zorganizowano (3)
								--	Zakończono (4)

								IF @stepId = '1018.027'
									BEGIN
										-- Organizowane (1)
										SET @statusDictionaryId = 83
										PRINT 'NEW STATUS: Organizowane (83)'
									END
								ELSE  IF @stepId = '1018.040'
									BEGIN
										-- Zorganizowano (3)
										SET @statusDictionaryId = 84
										PRINT 'NEW STATUS: Zorganizowano (84)'
									END
								ELSE  IF @stepId = '1018.004'
									BEGIN
										-- Zakończono (4)
										SET @statusDictionaryId = 85
										PRINT 'NEW STATUS: Zakończono (85)'
									END
								ELSE IF @stepId = '1015.011' AND EXISTS(SELECT id FROM dbo.process_instance with(nolock) where step_id = '1015.025' and group_process_id = @groupProcessInstanceId)
									BEGIN
										SELECT top 1 @serviceGroupId = group_process_id FROM dbo.process_instance with(nolock) where step_id = '1018.035' and root_id = @rootId
										SET @statusDictionaryId = 83
										PRINT 'NEW STATUS: Zakończono (85)'
									END
								ELSE IF @stepId = '1015.004' AND EXISTS(SELECT id FROM dbo.process_instance with(nolock) where step_id = '1015.025' and group_process_id = @groupProcessInstanceId)
									BEGIN
										SELECT top 1 @serviceGroupId = group_process_id FROM dbo.process_instance with(nolock) where step_id = '1018.035' and root_id = @rootId
										SET @statusDictionaryId = 85
										PRINT 'NEW STATUS: Zakończono (85)'
									END

							END
						ELSE IF @serviceId = 16 and isnull(@transportFixedOrNot,0)=1
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "Tranport"',default)

								-------------------------------------------------------------------------------
								---- 							Tranport nienaprawionego pojazdu			---
								-------------------------------------------------------------------------------

								--	Organizowane (1)
								--	Zorganizowano (3)
								--	Zakończono (4)

								IF @stepId = '1018.014'
									BEGIN
										-- Organizowane (1)
										SET @statusDictionaryId = 83
										PRINT 'NEW STATUS: Organizowane (83)'
									END
								ELSE  IF @stepId = '1018.040'
									BEGIN
										-- Zorganizowano (3)
										SET @statusDictionaryId = 84
										PRINT 'NEW STATUS: Zorganizowano (84)'
									END
								ELSE  IF @stepId = '1018.004'
									BEGIN
										-- Zakończono (4)
										SET @statusDictionaryId = 85
										PRINT 'NEW STATUS: Zakończono (85)'
									END
								ELSE IF @stepId = '1015.011' AND EXISTS(SELECT id FROM dbo.process_instance with(nolock) where step_id = '1015.025' and group_process_id = @groupProcessInstanceId)
									BEGIN
										SELECT top 1 @serviceGroupId = group_process_id FROM dbo.process_instance with(nolock) where step_id = '1018.035' and root_id = @rootId
										SET @statusDictionaryId = 83
										PRINT 'NEW STATUS: Zakończono (85)'
									END
								ELSE IF @stepId = '1015.004' AND EXISTS(SELECT id FROM dbo.process_instance with(nolock) where step_id = '1015.025' and group_process_id = @groupProcessInstanceId)
									BEGIN
										SELECT top 1 @serviceGroupId = group_process_id FROM dbo.process_instance with(nolock) where step_id = '1018.035' and root_id = @rootId
										SET @statusDictionaryId = 85
										PRINT 'NEW STATUS: Zakończono (85)'
									END

							END
						ELSE IF @serviceId = 6
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "Podróż"',default)

								-------------------------------------------------------------------------------
								---- 							Podróż				 						---
								-------------------------------------------------------------------------------

								--	Organizowane (1)
								--	Zorganizowano (3)
								--	Zakończono (4)

								IF @stepId = '1015.014'
									BEGIN
										-- Organizowane (1)
										SET @statusDictionaryId = 72
										PRINT 'NEW STATUS: Organizowane (72)'
									END
								ELSE  IF @stepId = '1015.011'
									BEGIN
										-- Zorganizowano (3)
										SET @statusDictionaryId = 73
										PRINT 'NEW STATUS: Zorganizowano (73)'
									END
								ELSE  IF @stepId = '1015.004'
									BEGIN
										-- Zakończono (4)
										SET @statusDictionaryId = 74
										PRINT 'NEW STATUS: Zakończono (74)'
									END

							END
						ELSE IF @serviceId = 9
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "Kredyt"',default)

								-------------------------------------------------------------------------------
								---- 							Kredyt				 						---
								-------------------------------------------------------------------------------

								--	Organizowane (1)
								--	Zorganizowano (3)
								--	Zakończono (4)

								IF @stepId = '1019.002'
									BEGIN
										-- Organizowane (1)
										SET @statusDictionaryId = 78
										PRINT 'NEW STATUS: Organizowane (78)'
									END
								ELSE  IF @stepId = '1019.060'
									BEGIN
										-- Zorganizowano (3)
										SET @statusDictionaryId = 79
										PRINT 'NEW STATUS: Zorganizowano (79)'
									END
								ELSE  IF @stepId = '1019.080'
									BEGIN
										-- Zakończono (4)
										SET @statusDictionaryId = 80
										PRINT 'NEW STATUS: Zakończono (80)'
									END

							END
						ELSE IF @serviceId = 8
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "CZĘŚCI ZAMIENNE"',default)

								-------------------------------------------------------------------------------
								---- 						CZĘŚCI ZAMIENNE			 						---
								-------------------------------------------------------------------------------

								--		Możliwa do organizacji (1)
								--      Czeka na organizację (1)
								--+      Organizowane (1)
								--      Zorganizowano (3)
								--      Zakończono (4)
								--      Anulowane bezkosztowo (5)
								--      Odwołano z kosztami (5)


								IF @stepId = '1017.009'
									BEGIN
										-- Organizowane (1)
										SET @statusDictionaryId = 50
										PRINT 'NEW STATUS: Organizowane (50)'
									END


							END
						ELSE IF @serviceId = 2
							BEGIN

								-------------------------------------------------------------------------------
								---- 						NAPRAWA NA DRODZE			 					---
								-------------------------------------------------------------------------------

								--+ 		Organizowane (1)
								--+ 		Kontraktor przyjął zlecenie. Szacowane przybycie: {@107@} (2)
								--+			Kontraktor na miejscu (3)
								--+		Kontraktor naprawił (3)
								--		Kontraktor nie naprawił (3)
								--		Zakończono usługę naprawy na drodze (4)
								--		Anulowane bezkosztowo (5)
								--		Pusty wyjazd (5)
								--		Samodzielna organizacja przez Klienta (6)

								IF @stepId = '1009.042' AND @fixingOrTowing = @serviceId AND @variant <> '2'
									BEGIN
										SET @statusDictionaryId = 24
									END
								ELSE IF @stepId = '1009.043' AND @fixingOrTowing = @serviceId AND @variant IN (1,2)
									BEGIN
										SET @statusDictionaryId = 24
									END
								ELSE IF (@stepId = '1009.011' OR @stepId = '1009.003') AND @fixingOrTowing = @serviceId
									BEGIN

										IF @variant = 99 AND @stepId = '1009.011'
											BEGIN
												-- Organizowane
												SET @statusDictionaryId = 24
												PRINT 'NEW STATUS: Organizowane (24)'
											END
										ELSE IF @stepId = '1009.003' AND @variant = 5
											BEGIN
												-- Ścieżka skrócona
												SET @statusDictionaryId = 28
											END
										ELSE
											BEGIN
												-- Przyjął zlecenie
												SET @statusDictionaryId = 26
											END

									END
								ELSE IF @stepId = '1009.008' AND @fixingOrTowing = @serviceId
									BEGIN
										-- Zakończono Usługę naprawy na drodze
										SET @statusDictionaryId = 1005
									END
								ELSE IF (@stepId = '1009.061' AND @fixingOrTowing = @serviceId AND @variant = 1) OR @stepId = '1009.054'
									BEGIN

										-- Organizowane
										SET @statusDictionaryId = 24
										PRINT 'NEW STATUS: Organizowane (24)'

									END
								ELSE IF @stepId = '1009.063' AND @fixingOrTowing = @serviceId AND @variant = 1
									BEGIN

										--Kontraktor w drodze do klienta. Szacowane przybycie: {#estimatedDate({@215@}|{@540@}|{@107@})#}
										SET @statusDictionaryId = 56

									END
								ELSE IF @stepId = '1009.064' AND @variant = 1
									BEGIN

										-- Zakończono usługę naprawy na drodze
										SET @statusDictionaryId = 1005

									END
								ELSE IF @stepId = '1009.064' AND @fixingOrTowing = @serviceId AND @variant = 2
									BEGIN
										-- JEDNAK BYŁO HOLOWANIE
										-- Anulowane bezkosztowo
										SET @statusDictionaryId = -1

									END
								ELSE IF @stepId = '1009.018' AND @fixingOrTowing = @serviceId
									BEGIN
										-- Kontraktor w drodze.
										SET @statusDictionaryId = 56
									END
								ELSE IF @stepId IN ('1009.004','1009.051') AND @fixingOrTowing = @serviceId
									BEGIN
										-- Kontraktor na miejscu
										IF ISNULL(@onPlace,0) <> 0
											BEGIN
												-- Kontraktor na miejscu
												SET @statusDictionaryId = 27
												PRINT 'NEW STATUS: Kontraktor na miejscu (27)'
											END
										ELSE
											BEGIN
												SET @statusDictionaryId = 56
												PRINT dbo.f_translate('NEW STATUS: Kontraktor w drodze',default)
											END
									END
								ELSE IF @stepId = '1009.005' AND @fixingOrTowing = @serviceId
									BEGIN

										IF @variant = 1
											BEGIN
												EXEC dbo.p_get_ics_finish_code @icsId = @icsId, @code = @code OUTPUT
												IF SUBSTRING(ISNULL(@code,''),6,2) = ('6S')
													BEGIN
														-- kontraktor nie naprawił i nie holuje
														SET @statusDictionaryId = 29
													END
												ELSE IF @code IN ('9214685','9214687','9214688','9214689','9214690')
													BEGIN
														-- pusty wyjazd
														SET @statusDictionaryId = -2
													END
												ELSE
													BEGIN
														-- kontraktor naprawił
														SET @statusDictionaryId = 28
													END
											END
										ELSE IF @variant = 2
											BEGIN
												-- kontraktor nie naprawił i holuje
												SET @statusDictionaryId = 29
											END

									END
								ELSE IF @stepId = '1009.076' AND @fixingOrTowing = @serviceId AND ISNULL(@lastStatusId,28) <> 29
									BEGIN
										SET @statusDictionaryId = 30
									END
								--					ELSE IF @stepId = '1009.006' AND ISNULL(@lastStatusId,0) NOT IN (30,31) AND @fixingOrTowing = 2
								--					BEGIN
								--						-- Kontraktor naprawił
								--						SET @statusDictionaryId = 30
								--						PRINT 'NEW STATUS: Kontraktor naprawił (28)'
								--					END
								--					ELSE IF @stepId = '1009.008' AND ISNULL(@lastStatusId,0) NOT IN (31) AND @fixingOrTowing = 2
								--					BEGIN
								--						-- Zakończono usługę naprawy
								--						SET @statusDictionaryId = 30
								--					END
								ELSE IF @stepId = '1009.055' AND ISNULL(@lastStatusId,0) NOT IN (45)
									BEGIN
										IF @towingStatus = 2
											BEGIN
												-- Miał holować a naprawił na miejscu
												--							SET @statusDictionaryId = 28

												--							INSERT INTO dbo.service_status (group_process_id, status_dictionary_id, serviceId, created_at, updated_at)
												--							VALUES(@groupProcessInstanceId, 28, 2, GETDATE(), GETDATE());

												EXEC [dbo].[p_add_service_status]
														@groupProcessInstanceId = @groupProcessInstanceId,
														@status = 28,
														@serviceId = 2

											END

										IF ISNULL(@fixingStatus,-1) = 1 AND @fixingOrTowing = @serviceId
											BEGIN
												-- Kontraktor naprawił
												SET @statusDictionaryId = 28
											END
										ELSE IF ISNULL(@fixingStatus,-1) = 0 AND @fixingOrTowing = @serviceId
											BEGIN
												-- Kontraktor nie naprawił
												SET @statusDictionaryId = 29
											END
									END
								ELSE IF @stepId = '1009.052' AND @fixingOrTowing = @serviceId
									BEGIN
										IF @variant = 2
											BEGIN
												SET @statusDictionaryId = -1
											END
										ELSE IF @variant = 3
											BEGIN
												SET @statusDictionaryId = -2
											END
									END
								ELSE IF @stepId = '1009.059' AND @fixingOrTowing = @serviceId AND @variant < 90
									BEGIN
										SET @statusDictionaryId = -1
									END
							END

						ELSE IF @serviceId = 12
							BEGIN

								-------------------------------------------------------------------------------
								---- 						NAPRAWA WARSZTATOWA     						---
								-------------------------------------------------------------------------------
								IF @stepId IN ('1021.019','1021.015','1021.003','1021.017')
									BEGIN
										DECLARE @finalFixingDate INT
										DECLARE @fixingEndDate DATETIME

										DELETE FROM @values
										INSERT @values EXEC p_attribute_get2 @attributePath = '129,130', @groupProcessInstanceId = @groupProcessInstanceId
										SELECT @fixingEndDate = value_date FROM @values

										DELETE FROM @values
										INSERT @values EXEC p_attribute_get2 @attributePath = '129,798', @groupProcessInstanceId = @groupProcessInstanceId
										SELECT @finalFixingDate = value_int FROM @values

										IF @lastStatusId IS NULL 
										BEGIN
											SET @statusDictionaryId = 1004
										END
										ELSE IF ISNULL(@lastStatusId,999) <> 999
										BEGIN										
											SET @statusDictionaryId = 1004
	
											IF ISNULL(@finalFixingDate,0) <> 1 AND @fixingEndDate IS NOT NULL -- AND @fixingEndDate < GETDATE() (mateuszke nie chciał)
											BEGIN
												SET @statusDictionaryId = 1003
											END
											ELSE IF @fixingEndDate IS NOT NULL 
											BEGIN
												SET @statusDictionaryId = 1002
											END
										END 
									END
								ELSE IF @stepId = '1009.068'
									BEGIN

										DECLARE @monitoringGroupId INT

										SELECT TOP 1 @monitoringGroupId = group_process_id FROM dbo.process_instance with(nolock) WHERE step_id = '1021.017' AND root_id = @rootId

										IF @monitoringGroupId IS NOT NULL
											BEGIN

												SET @serviceGroupId = @monitoringGroupId
												SET @statusDictionaryId = 1002

											END

									END

							END
						ELSE IF @serviceId = 14
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "PARKING"',default)

								-------------------------------------------------------------------------------
								---- 							PARKING				 						---
								-------------------------------------------------------------------------------

								-- W trakcie organizacji (1)
								-- Zorganizowany parking w [lokalizacja parkingu] (3)
								-- Zorganizowany parking
								-- Zakończono (4)

								IF @stepId = '1142.006'
									BEGIN
										-- W trakcie organizacji (1)
										SET @statusDictionaryId = 1006
										PRINT 'NEW STATUS: W trakcie organizacji (1006)'
									END
								ELSE  IF @stepId = '1142.004' AND @variant = 1
									BEGIN
										-- Zorganizowany parking w [lokalizacja parkingu](3)
										SET @statusDictionaryId = 1007
										PRINT 'NEW STATUS: Zorganizowany parking w [lokalizacja parkingu] (1007)'
									END
								ELSE  IF @stepId = '1142.004' AND @variant = 2
									BEGIN
										-- Zorganizowany parking w [lokalizacja parkingu](3)
										SET @statusDictionaryId = 1008
										PRINT 'NEW STATUS: Zakończono (1008)'
									END
								ELSE  IF @stepId = '1142.008'
									BEGIN
										-- Zorganizowany parking w [lokalizacja parkingu](3)
										SET @statusDictionaryId = 1011
										PRINT dbo.f_translate('NEW STATUS: Zorganizowany parking',default)
									END
								ELSE  IF @stepId = '1142.005'
									BEGIN
										-- Zakończono (4)
										SET @statusDictionaryId = 1008
										PRINT 'NEW STATUS: Zakończono (1008)'
									END

							END

						---------
						ELSE IF @serviceId = 17
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "HOME ASSISTANCE"',default)
								-------------------------------------------------------------------------------
								---- 							HOME ASSISTANCE				 				---
								-------------------------------------------------------------------------------

								-- Czeka na organizacje (1)
								-- Wybor serwisu (2)
								-- W trakcie realizacji (3)
								-- Zakonczono (4)
								IF @stepId = '1148.002' OR @stepId = '1148.007'
									BEGIN
										SET @statusDictionaryId = 1012
									END
								ELSE IF @stepId = '1148.005' OR @stepId = '1148.006'
									BEGIN
										SET @statusDictionaryId = 1013
									END
								ELSE IF  @stepId = '1148.008' OR @stepId = '1148.009'
									BEGIN
										SET @statusDictionaryId = 1014
									END
								ELSE IF @stepId = '1148.015'
									BEGIN
										SET @statusDictionaryId = 1015
									END
							END
						---------
						ELSE IF @serviceId = 18
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "MEDICAL"',default)
								-------------------------------------------------------------------------------
								---- 							MEDICAL						 				---
								-------------------------------------------------------------------------------

								-- Czeka na organizacje (1)
								-- Wybor serwisu (2)
								-- W trakcie realizacji (3)
								-- Zakonczono (4)
								IF @stepId = '1152.002' OR @stepId = '1152.003' OR @stepId = '1152.004'
									BEGIN
										SET @statusDictionaryId = 1017
									END
								ELSE IF @stepId = '1152.006' OR @stepId = '1152.005'
									BEGIN
										SET @statusDictionaryId = 1018
									END
								ELSE IF @stepId = '1152.007'
									BEGIN
										SET @statusDictionaryId = 1019
									END

							END

						---------
						ELSE IF @serviceId = 21
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "SCRAPPING"',default)
								-------------------------------------------------------------------------------
								---- 							SCRAPPING						 				---
								-------------------------------------------------------------------------------

								-- Czeka na organizacje (1)
								-- Wybor serwisu (2)
								-- W trakcie realizacji (3)
								-- Zakonczono (4)
								IF @stepId = '1154.002'
									BEGIN
										SET @statusDictionaryId = 1021
									END
								ELSE IF @stepId = '1154.006'
									BEGIN
										SET @statusDictionaryId = 1022
									END
								ELSE IF @stepId = '1154.007'
									BEGIN
										SET @statusDictionaryId = 1023
									END

							END

						---------
						ELSE IF @serviceId = 22
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "MONITORING SZKODY"',default)
								-------------------------------------------------------------------------------
								---- 							MONITORING SZKODY						 				---
								-------------------------------------------------------------------------------


								--1024 Czeka na organizacje (1)
								--1025 W trakcie monitoringu (2)
								--1026 W trakcie monitoringu. Etap: <b>{@671,308@}</b> (3)
								--1027 Zakonczono (4)

								IF @stepId IN ('1155.009', '1155.013')
									BEGIN
										SET @statusDictionaryId = 1025
									END
								ELSE IF @stepId = '1155.008'
									BEGIN

										DECLARE @etapLikwidacji INT

										INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '671,308', @groupProcessInstanceId = @groupProcessInstanceId
										SELECT @etapLikwidacji = value_int FROM @values

										IF @etapLikwidacji IS NOT NULL
											BEGIN

												IF @etapLikwidacji = 9 -- Naprawa zakończona
													BEGIN

														SET @statusDictionaryId = 1027

													END
												ELSE IF ISNULL(@lastStatusId, 0) <> 1026
													BEGIN

														SET @statusDictionaryId = 1026

													END

											END

									END

								--					IF @stepId IN ('1155.009', '1155.010') OR (@stepId = '1155.013' AND @variant = 1)
								--					BEGIN
								--
								--						EXEC [dbo].[p_alphabet_monit_harm] @groupProcessInstanceId = @groupProcessInstanceId, @type = 1
								--
								--						SET @statusDictionaryId = 1025
								--					END

							END

						---------
						ELSE IF @serviceId = 28
							BEGIN

								PRINT dbo.f_translate('UPDATE STATUS FOR: "Door2Door"',default)
								-------------------------------------------------------------------------------
								---- 							Door2Door						 				---
								-------------------------------------------------------------------------------


								--					1028	Czeka na organizację (1)
								--					1029	Pojazd w trakcie holowania (2)
								--					1030	Pojazd w warsztacie (2)
								--					1031	Pojazd transportowany (3)
								--					1032	Zakończono usługę D2D (4)


								IF @stepId = '1191.006'
									BEGIN
										SET @statusDictionaryId = 1028
									END
								ELSE IF @stepId = '1191.010'
									BEGIN
										SET @statusDictionaryId = 1029
									END

							END
						ELSE IF @serviceId in (23,24,25,26,27)
							BEGIN
								declare @serviceIdProc int

								DELETE from @values
								INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '526', @groupProcessInstanceId = @groupProcessInstanceId
								SELECT @serviceIdProc = value_int FROM @values

								if @serviceId=@serviceIdProc
									begin
										PRINT dbo.f_translate('UPDATE STATUS FOR: TRUCK',default)

										-------------------------------------------------------------------------------
										---- 							Truck				 						---
										-------------------------------------------------------------------------------

										IF @stepId in ('1190.002','1190.003','1190.004','1190.005','1190.006')
											BEGIN
												-- Organizowane (1)
												SET @statusDictionaryId = 1021
												PRINT 'NEW STATUS: 1021	W trakcie organizacji'
											END
										ELSE  IF @stepId in ('1190.008','1190.011')
											BEGIN
												-- Zorganizowano (3)
												SET @statusDictionaryId = 1022
												PRINT 'NEW STATUS: 1022	W trakcie realizacji'
											END
										ELSE  IF @stepId = '1190.012'
											BEGIN
												-- Zakończono (4)
												SET @statusDictionaryId = 1023
												PRINT 'NEW STATUS: 1023	Zakończono'
											END
									end
							END
						ELSE IF @serviceId =  29
						BEGIN
							IF @stepId = '1204.003'
							BEGIN
								SET @statusDictionaryId = 1036 --progress 2
							end --naprawa rozpoczęta
							ELSE IF @stepId = '1204.006' AND @variant <> 99
							BEGIN
								-------------------------------------------------------------------------------
								---- 							NPT				 						---
								-------------------------------------------------------------------------------
								DECLARE @resultOfNPT int
								DELETE from @values
								INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '717', @groupProcessInstanceId = @groupProcessInstanceId
								SELECT @resultOfNPT = value_int FROM @values

								IF @resultOfNPT = 1
									BEGIN
										-- 								settujemy na succesfully finished stawiamy taska na po 24h
										-- w trakcie
										SET @statusDictionaryId = 1037 --progress 4
									end
								ELSE
									BEGIN
										-- 								STATUS - zakończona/ nie udało się
										SET @statusDictionaryId = 1038
									END

							end
						END

						IF @statusDictionaryId <> 0
							BEGIN

								-- Zapisanie statusu do tabeli
								IF ISNULL(@lastStatusId, 0) <> @statusDictionaryId
									BEGIN
--										INSERT INTO dbo.service_status (group_process_id, status_dictionary_id, serviceId, created_at, updated_at)
--										VALUES(ISNULL(@serviceGroupId,@groupProcessInstanceId), @statusDictionaryId, @serviceId, GETDATE(), GETDATE());

										SET @serviceGroupId = IIF(@serviceGroupId IS NULL, @groupProcessInstanceId, @serviceGroupId)
										
										EXEC [dbo].[p_add_service_status]
										@groupProcessInstanceId = @serviceGroupId,
										@status = @statusDictionaryId,
										@serviceId = @serviceId
										
										
										/*	Dodatkowe Akcje po ustawieniu statusu
                        ____________________________________*/

--                     JEŚLI HOLOWANIE Z TAXI - ZAMYKAMY TAXI RÓWNIEŻ

                    IF @serviceId = 1 and @statusDictionaryId in (43,44)
                      BEGIN
--                         Znajdź taxi
												DECLARE @taxiServiceId int

												SELECT top 1 @taxiServiceId = group_process_instance_id from attribute_value with (nolock )
												where attribute_path = '152,796' and value_int = @serviceGroupId



												EXEC [dbo].[p_add_service_status]
														@groupProcessInstanceId = @taxiServiceId,
														@status = 68,
														@serviceId = 5

-- 												68 "Zakończono"
                      end

										PRINT 'BEFORE P_after_update_status'

										BEGIN TRY

										EXEC [dbo].[P_after_update_status] @groupProcessInstanceId = @groupProcessInstanceId, @platformId = @platformId, @statusDictionaryId = @statusDictionaryId, @serviceId = @serviceId

										END TRY
										BEGIN CATCH

										EXEC dbo.p_log_automat
												@name = 'after_update_status_error',
												@content = 'Błąd przy odpaleniu P_after_update_status',
												@param1 = @processInstanceId,
												@param2 = @serviceId,
												@param3 = @statusDictionaryId
										-- LOGGER END

										END CATCH;


										/*	____________________________________*/

--										DECLARE @inserted INT
--										SET @inserted = SCOPE_IDENTITY()
--
--										DELETE FROM @values
--										INSERT @values EXEC p_attribute_get2 @attributePath = '592', @groupProcessInstanceId = @groupProcessInstanceId
--										SELECT @sparxId = value_string FROM @values
--
--										IF @sparxId IS NOT NULL AND dbo.f_abroad_case(@groupProcessInstanceId) = 0
--											BEGIN
--												-- API CALL
--												INSERT INTO AtlasDB_v.dbo.jms_jobs
--														(state, queue, priority, createdAt, executeAfter, command, args, maxRuntime, maxRetries, stackTrace, discr)
--												VALUES('pending', 'default', 0, GETDATE(), GETDATE(), dbo.f_translate('api:sparx:service:update',default), '{"serviceStatusId":'+CAST(@inserted AS NVARCHAR(20))+'}', 0, 0, 'N;', 'job');
--											END

										-- LOGGER START
										DECLARE @p1 VARCHAR(255) = dbo.f_caseId(@rootId)
										EXEC dbo.p_log_automat
												@name = 'update_status_service',
												@content = dbo.f_translate('Zaktualizowano status usługi',default),
												@param1 = @p1,   -- numer sprawy
												@param2 = @serviceId, -- id usługi
												@param3 = @statusDictionaryId
										-- LOGGER END


-- 										FORD SURVEYS
										IF @platformId = 2
											BEGIN
												IF isnull(@serviceId,0) = 1 and @statusDictionaryId in (43,44)
													BEGIN
														exec p_make_survey @processInstanceId = @processInstanceId, @type = 3
													end
												ELSE IF isnull(@serviceId,0) = 2 and @statusDictionaryId in(30)
													BEGIN
														EXEC p_make_survey  @processInstanceId = @processInstanceId, @type = 2
													end
												else if isnull(@serviceId,0) = 3  and @statusDictionaryId in (92)
													BEGIN
														EXEC p_make_survey @processInstanceId = @processInstanceId, @type = 4
													end
												ELSE IF isnull(@serviceId,0) = 12 and @statusDictionaryId in (1003)
													BEGIN

														DECLARE @hasServices int

														SELECT   distinct  @hasServices = count(x.group_process_id)
														from (SELECT distinct group_process_id
																	from service_status ss with(nolock)
																				 join service_status_dictionary ssd with (nolock) on ssd.id = ss.status_dictionary_id
																																															 and ssd.progress < 4
																																															 and ssd.progress > 0
																	where ss.group_process_id in
																				(SELECT distinct group_process_id from process_instance with (nolock) where root_id = @rootId))x

												IF isnull(@hasServices,0)>0
														BEGIN
															EXEC p_make_survey @processInstanceId= @processInstanceId, @type = 5
														end


													end
											end


										If @platformId =53
											BEGIN

												-- 						BWB po 16  / w święta - Alphabet - monit o zorganizowanych usługach

												declare @bwbState int

												DELETE from @values
												INSERT @values EXEC p_attribute_get2 @attributePath =  '573', @groupProcessInstanceId = @groupProcessInstanceId
												SELECT @bwbState = value_int from @values
												PRINT '@bwbState'
												PRINT @bwbState
												iF ISNULL(@bwbState,0) = -1
													begin
														DECLARE @sendedNotesList TABLE(value_string nvarchar(100))
														INSERT into @sendedNotesList SELECT value_string from attribute_value where root_process_instance_id = @rootId and attribute_path  = '406,226,197'
														DECLARE @serviceProgressGroup int
														DECLARE @serviceProgress int
														DECLARE @subtype nvarchar(20)
														SET @subtype =dbo.f_translate('bwb',default)+cast(@serviceId as nvarchar(3))
														DECLARE @alreadySended nvarchar(100)
														SELECT @alreadySended  = value_string FROM @sendedNotesList where value_string =  @subtype

														PRINT @serviceId

														if @serviceId in (1,2,3) and @alreadySended is null
															BEGIN
																SET @serviceProgressGroup = [dbo].[f_service_top_progress_group](@rootId, @serviceId)
																EXEC dbo.p_get_service_progress @groupProcessInstanceId = @serviceProgressGroup, @progress = @serviceProgress OUTPUT
																print dbo.f_translate('bwb in ',default)
																PRINT @serviceProgress
																PRINT @serviceProgressGroup
																IF @serviceProgress = 2
																	BEGIN
																		EXEC p_alphabet_monit_bwb @serviceId = @serviceId, @rootId = @rootId
																	END
															end



													end


												-- 								BRS - monity




												DECLARE @CFMServiceType int

												DELETE FROM @values
												INSERT @values EXEC p_attribute_get2 @attributePath = '143', @groupProcessInstanceId = @groupProcessInstanceId
												SELECT @CFMServiceType = value_int FROM @values


												IF (@CFMServiceType in (2,3) and @serviceId in (1,3))
													BEGIN
														print '@statusDictionaryId'
														print @statusDictionaryId
														if (@serviceId = 1 and @statusDictionaryId in (36,37)) -- jeśli zorganizowano holowanie
															BEGIN
																EXEC p_alphabet_monit_harm @groupProcessInstanceId = @groupProcessInstanceId, @type = 2
															END
														ELSE if (@serviceId = 3)
															BEGIN
																DECLARE @existTowing bit = 0
																DECLARE @towingProgress  int =0
																DECLARE @serviceFromTowingGroupId INT
																SET @serviceFromTowingGroupId = [dbo].[f_service_top_progress_group](@rootId, 1)
																EXEC dbo.p_get_service_progress @groupProcessInstanceId = @serviceFromTowingGroupId, @progress = @towingProgress OUTPUT



																PRINT '@towingProgress'
																PRINT @towingProgress
																PRINT '@MWProgress'


																IF isnull(@towingProgress,0) in (1,2,3) AND @statusDictionaryId =93
																	BEGIN
																		SET @existTowing =1
																	END

																IF @existTowing = 1
																	BEGIN
																		EXEC p_alphabet_monit_harm @groupProcessInstanceId = @serviceFromTowingGroupId, @type = 3
																	end
															end
													END


											end

										EXEC p_send_abroad_case_info @groupProcessId =  @groupProcessInstanceId, @serviceId =@serviceId




										DECLARE @canSendRs INT

										EXEC @canSendRs = [dbo].[p_canSendRs]
												@statusDictionaryId = @statusDictionaryId,
												@serviceId = @serviceId,
												@groupProcessInstanceId = @groupProcessInstanceId

										PRINT '--@canSendRs'
										PRINT @canSendRs

										IF @canSendRs = 1
											begin
												DECLARE @processInstanceIdRS int
												DECLARE @processInstanceIds nvarchar(100)

												declare @RSGroupId int
												declare @Rsid int
												declare @RsVariant int

												-- 								EXEC dbo.p_log_automat
												-- 										@name = 'update_status_service_logford',
												-- 										@content = dbo.f_translate('TEXT',default),
												-- 										@param1 = @program,   -- numer sprawy
												-- 										@param2 = @platformId, -- id usługi
												-- 										@param3 = @serviceId
												declare @accidentDate DATETIME
												declare @accidentReportedDate DATETIME
												DELETE FROM @values
												INSERT @values EXEC p_attribute_get2 @attributePath = '101,104', @groupProcessInstanceId = @groupProcessInstanceId
												SELECT @accidentDate = value_date FROM @values

												DELETE FROM @values
												INSERT @values EXEC p_attribute_get2 @attributePath = '883,884', @groupProcessInstanceId = @groupProcessInstanceId
												SELECT @accidentReportedDate = value_date FROM @values

												IF isnull(@accidentDate,'') = ''
													BEGIN
														SELECT  TOP 1 @accidentDate =  created_at  from dbo.process_instance where group_process_id = @groupProcessInstanceId order by id asc
														SET @accidentDate = DATEADD(minute ,-5, @accidentDate)
														EXEC p_attribute_edit
																@attributePath = '101,104',
																@groupProcessInstanceId = @groupProcessInstanceId,
																@stepId = 'xxx',
																@valueDate = @accidentDate,
																@err = @err OUTPUT,
																@message = @message OUTPUT

													END

												set @RsVariant=1

												select	top 1 @RSGroupId=group_process_id
												from	dbo.process_instance  with(nolock)
												where	root_id=@rootId and
															 step_id like '1021.%'
												order by id

												select	top 1 @Rsid=group_process_id
												from	dbo.process_instance  with(nolock)
												where	root_id=@rootId and
															 step_id like '1021.003' and
															 active=1
												order by id

												declare @serviceMonitoring int

												select	top 1 @serviceMonitoring=group_process_id
												from	dbo.process_instance  with(nolock)
												where	root_id=@rootId and
															 step_id like '1021.015'
												order by id

												if @Rsid is not null
													begin
														if @serviceMonitoring is not null
															begin
																set @RsVariant=2
															end
														else
															begin
																set @RsVariant=null
															end

														EXECUTE dbo.p_process_next
																@previousProcessId=@Rsid
																,@userId=1
																,@variant=2
																,@err=@err OUTPUT
																,@message=@message OUTPUT
																,@processInstanceIds=@processInstanceIds OUTPUT
													end

												PRINT dbo.f_translate('RS START',default)
												PRINT @rootId
												PRINT @RSGroupId

												EXECUTE dbo.p_process_new
														@stepId='1021.070'
														,@userId=1
														,@parentProcessId=@rootId
														,@groupProcessId=@RSGroupId
														,@rootId=@rootId
														,@err=@err OUTPUT
														,@message=@message OUTPUT
														,@processInstanceId=@processInstanceIdRS OUTPUT

												EXECUTE dbo.p_process_next
														@previousProcessId=@processInstanceIdRS
														,@userId=1
														,@variant=@RsVariant
														,@err=@err OUTPUT
														,@message=@message OUTPUT
														,@processInstanceIds=@processInstanceIds OUTPUT

											end
									END
							END
						-- WHILE END
						FETCH NEXT FROM kur INTO @serviceId;
					END
				CLOSE kur
				DEALLOCATE kur

			END

		PRINT '------------------ END p_update_status_service'

	END