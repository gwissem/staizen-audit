

ALTER PROCEDURE [dbo].[s_1011_080]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @content NVARCHAR(MAX)
	DECLARE @phoneNumber NVARCHAR(255)
	DECLARE @attrTable Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @platformId INT
	DECLARE @platformPhone NVARCHAR(100)
	DECLARE @platformName NVARCHAR(100)
	DECLARE @emergency INT
	DECLARE @caseId VARCHAR(8)
	DECLARE @programIds NVARCHAR(255)
	
	SELECT @groupProcessInstanceId = group_process_id FROM process_instance with(nolock) where id = @previousProcessId
	
 	DELETE FROM @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '253',
		   @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @attrTable
		
	set @variant=1
	
	DECLARE @diagnosisSummary NVARCHAR(10)
	SELECT @diagnosisSummary = dbo.f_diagnosis_summary(@groupProcessInstanceId)
		
	DECLARE @platformGroup nvarchar(255)
	EXEC dbo.p_platform_group_name @groupProcessInstanceId= @groupProcessInstanceId, @name =@platformGroup OUTPUT
	
	IF dbo.f_diagnosis_code(@groupProcessInstanceId) <> '1464391'
	AND ISNULL(@diagnosisSummary,'') not like '%\[H\]%dbo.f_translate(' escape ',default)\'  
	AND @platformId = 25
	BEGIN
		SET @variant = 2	
	END
	
	IF @platformId = 25 AND left(dbo.f_diagnosis_code(@groupProcessInstanceId),3) in ('300','309')
	BEGIN
		declare @fleet nvarchar(100)
		exec dbo.p_get_vin_headers_by_rootId @root_id=@groupProcessInstanceId, @columnName=dbo.f_translate('wlasciciel',default), @output=@fleet OUTPUT
		if @fleet like '%APTEKA GEMINI%'
		begin
			SET @variant = 1
		end
	end
END




