ALTER PROCEDURE [dbo].[s_1019_003]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err TINYINT
	DECLARE @message NVARCHAR(255)
	DECLARE @caseId NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	
	SELECT @groupProcessInstanceId = group_process_id FROM process_instance where id = @previousProcessId
	DECLARE @isSupervisorAgree int
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '123', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isSupervisorAgree = value_int FROM @values
	
	if @isSupervisorAgree=1
	begin
		SET @variant = 1

		declare @partnerEmail nvarchar(200)
		declare @body nvarchar(4000)
		declare @firstname nvarchar(100)
		declare @lastname nvarchar(100)
		declare @VIN nvarchar(100)
		declare @regNumber nvarchar(100)
		
		declare @partnerLocId int
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerLocId = value_int FROM @values

--		SELECT	@partnerEmail=avContactEmail.value_string
--		from dbo.attribute_value avLocation with(nolock) inner join
--			 dbo.attribute_value avPartnerLocStatus with(nolock) on avPartnerLocStatus.parent_attribute_value_id=avLocation.id and avPartnerLocStatus.attribute_path='595,597,676' and avPartnerLocStatus.value_int=3 inner join
--			 dbo.attribute_value avContact with(nolock) on avContact.parent_attribute_value_id=avLocation.id and avContact.attribute_path='595,597,642' inner join
--			 dbo.attribute_value avContactType with(nolock) on avContactType.parent_attribute_value_id=avContact.id and avContactType.attribute_path='595,597,642,643' /*and avContactType.value_int=4*/ inner join
--			 dbo.attribute_value avContactEmail with(nolock) on avContactEmail.parent_attribute_value_id=avContact.id and avContactEmail.attribute_path='595,597,642,656'  and avContactEmail.value_string is not null 
--		where avLocation.attribute_path='595,597' and avLocation.id=@partnerLocId

		SELECT @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId,@partnerLocId, null, 4)		

		declare @extCaseId nvarchar(100)
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '711', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @extCaseId = value_string FROM @values
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '73,71', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @VIN = value_string FROM @values

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @regNumber = value_string FROM @values
		


		set @caseId=dbo.f_caseId(@groupProcessInstanceId)

		declare @subject nvarchar(100)

		set @subject=''+isnull(@extCaseId,'')+' '+isnull(@caseId,'')
		set @body='Hello from Poland,<BR><BR>
					Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR><BR>'+
					isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR><BR>
					Please provide us information about information listed below:<BR>
					<BR>
					Workshop:<BR> 
					Adress of the workshop<BR>
					Phone number:<BR>
					E-mail: <BR>
					Cost of the repair (total):<BR>
					IBAN account number:<BR>
					SWIFT number:<BR>
					<BR>
					Best regards,<BR>
					Starter24'
					
		DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')

		EXECUTE dbo.p_note_new 
		   @sender=@senderEmail
		  ,@groupProcessId=@groupProcessInstanceId
		  ,@type=dbo.f_translate('email',default)
		  ,@content=@body
		  ,@phoneNumber=null
		  ,@email=@partnerEmail
		  ,@userId=@currentUser
		  ,@subType=null
		  ,@attachment=null
		  ,@subject=@subject
		  ,@direction=1
		  ,@addInfo=null
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT
		
	end
	ELSE
	BEGIN
		SET @variant = 2
	END
END


