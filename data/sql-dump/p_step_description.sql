ALTER PROCEDURE [dbo].[p_step_description] @instanceId int, @isParagraph int=0
AS
BEGIN
	DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))
	DECLARE @output NVARCHAR(MAX) = ''
	DECLARE @platformId int
	DECLARE @programId int
	declare @groupProcessInstanceId int 
	declare @stepId nvarchar(255)
	declare @rootId int

	select	@groupProcessInstanceId = group_process_id, 
			@stepId = step_id,
			@rootId = root_id
	FROM dbo.process_instance where id = @instanceId
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
    
	if @isParagraph=1
	begin
		if @stepId='1152.002'
		begin
			declare @medicalService int

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '990', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @medicalService = value_int FROM @values
		
			if @medicalService in (10,11)
			begin
				set @output=dbo.f_translate('Telefoniczna rozmowa z lekarzem uprawnionym Centrum Zgłoszeniowego, który w miarę posiadanej wiedzy specjalistycznej oraz istniejących możliwości, udziela Ubezpieczonemu ustnej informacji, co do dalszego postępowania. Informacje te mają char',default)
			end
			else if @medicalService in(20,21)
			begin
				set @output='a) Wizyta lekarza – zorganizowanie jednorazowej wizyty lekarza pierwszego kontaktu w miejscu zamieszkania pacjenta (w sytuacjach braku bezpośredniego zagrożenia zdrowia i życia wymagającego natychmiastowej interwencji Pogotowia Ratunkowego)<br><br>b) Wizyta pielęgniarki – zorganizowanie wizyty pielęgniarki w miejscu zamieszkania w miejscu zamieszkania pacjenta'
			end
			else if @medicalService=30
			begin
				set @output='Transport medyczny Ubezpieczonego do kraju zamieszkania lub przewiezienie pacjenta ze szpitala: na lotnisko; do hotelu; pod prywatny adres; do innego szpitala (np. ze wzgl. na niski standard usług). Transport Ubezpieczonego odbywa się dostosowanym do jego stanu zdrowia środkiem transportu. O celowości, terminie, sposobie i możliwości transportu Ubezpieczonego decyduje lekarz Centrum Zgłoszeniowego, po konsultacji z lekarzem prowadzącym leczenie Ubezpieczonego.'
			end
			else if @medicalService=40
			begin
				set @output=dbo.f_translate('Transport medyczny Ubezpieczonego do kraju zamieszkania lub Przewiezienie pacjenta do innego szpitala',default)
			end
			else if @medicalService=50
			begin
				set @output=dbo.f_translate('Jeżeli Ubezpieczony w czasie podróży uległ nieszczęśliwemu wypadkowi lub nagle zachorował, Centrum Zgłoszeniowe, po konsultacji z Ubezpieczonym, zapewnia wymaganą stanem jego zdrowia opiekę lekarską i pokrywa jej koszty.',default)
			end
			else if @medicalService=60
			begin
				set @output=dbo.f_translate('Organizacja potrzebnych leków oraz dostarczenie ich do pacjenta. Wysyłka drugiej pary okularów w przypadku ich uszkodzenia.',default)
			end
			else if @medicalService=70
			begin
				set @output=dbo.f_translate('Jeżeli Ubezpieczony zmarł podczas podróży, Centrum Zgłoszeniowe organizuje pogrzeb lub kremację.',default)
			end
			else if @medicalService=80
			begin
				set @output=dbo.f_translate('Jeżeli Ubezpieczony zmarł podczas pobytu za granicą Centrum Zgłoszeniowe organizuje transport zwłok do miejsca pochówku na terenie kraju zamieszkania Ubezpieczonego.',default)
			end
			else set @output=''

			set @output=@output+'<br><br>Obszar działania:<br>Polska oraz Holandia; Niemcy; Belgia; Szwajcaria; Hiszpania; Austria; Włochy; Szwecja + EUROPA w ramach sieci ARC'

			set @output='<font style='dbo.f_translate('color:red;',default)'>'+@output+'</font>'
			
			set @output=isnull(nullif(@output,''),'&nbsp;')
			
		end
		else
		if @stepId='1152.003' or @stepId='1152.004'
		begin
		
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '990', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @medicalService = value_int FROM @values
		
			declare @abroad int
			select @abroad=dbo.f_abroad_case(@rootId)

			if @abroad=0
			begin
				if @medicalService in(10,11)
				begin
					set @output='<p>Ustalic jak najwięcej szczegółów dotyczących stanu zdrowia chorego:<br>
	a) od jak dawna ma objawy choroby<br>
	b) czy przyjmuje jakieś leki?<br>
	c) jakie są dokładne objawy choroby<br>
	d) czy kontaktował się z lekarzem rodzinnym lub pogotowiem, jeżeli nie to dlaczego?<br>
	
	POLSKA:<br>
	Starter24 kontaktuje sie z naszym partnerem firmą NZOZ MEDICAL TRANSPORT, która dysponuje specjalistami i na nasze zlecenie przeprowadza konsultację medyczną. <br>
	Wyniki Konsultacji sa niezwlocznie przekazywane Konsultantowi wraz z informacja jak nalezy dalej postepować (zalecenia).<br>
	Awaryjne postepowanie 1: Starter24 kontaktuje sie firma Medikor, z prośba o przeprowadzenie rozmowy z pacjentem i inforamcję zwrotną na temat jego stanu zdrowia.<br>
	W razie braku możliwości kontaktu z lekarzem należy przekazać pacjentowi żeby: NIEZWŁOCZNIE UDAŁ SIE DO LEKARZA RODZINNEGO lub NA POGOTOWIE GDZIE ZOSTANIE UDZIELONA MU POMOC!</p>'
				end
				else if @medicalService in(20,21)
				begin
					set @output='<p>Wizytę lekarza w sytuacjach braku bezpośredniego zagrożenia zdrowia i życia wymagającego natychmiastowej interwencji Pogotowia Ratunkowego poprzedza Konsultacja Medyczna w celu natychmiastowego stwierdzenia czy zgłaszana sytuacja nie zagraża życiu pacjenta.<br>
	POLSKA:
	Starter24 kontaktuje sie z naszym partnerem firmą NZOZ MEDICAL TRANSPORT, która dysponuje specjalistami i na nasze zlecenie organizuje wizytę lekarza, która poprzedzona jest telefoniczną konsultację medyczną.<br>
	Wyniki Konsultacji są niezwlocznie przekazywane Konsultantowi wraz z informacja jak nalezy dalej postepować (zalecenia) oraz informacją czy wizyta lekarska będzie organizowana. Jeżeli wizyta będzie organizowana to uzgadniamy w jaki sposób i kiedy.
	Awaryjne postepowanie 1: Starter24 kontaktuje sie firma Medikor, z prośba o przeprowadzenie rozmowy z pacjentem i inforamcję zwrotną na temat jego stanu zdrowia. W razie możliwości zorganizowanie wizyty lekarskiej poprzez lekarza firmy Medicor.
	W razie braku możliwości kontaktu z lekarzem należy przekazać pacjentowi żeby: NIEZWŁOCZNIE UDAŁ SIE DO LEKARZA RODZINNEGO lub NA POGOTOWIE GDZIE ZOSTANIE UDZIELONA MU POMOC!
	Jak organizaować:<br>
	1) Zlecenie przekazujemy do NZOZ MEDICAL TRANSPORT lub firmy MEDICOR. Umowa obejmuje organizację takich usług.
	2) Lekarzy mogących pojechać na wizytę i zbadać pacjenta szukamy na stronie: www.medmemo.pl</p>'
				end
				else if @medicalService=30
				begin
					set @output='<p>a)samochód osobowy – zlecenie przekazujemy Kontraktorowi Starter24<br>
	b) karetka – zlecenie dla współpracującej z nami firmy NZOZ MEDICAL TRANSPORT w dalszej kolejności dla firmy Medikor.<br>
	UWAGA: CO NALEŻY USTALIĆ PRZED ZLECENIEM TRANSPORTU I MIEĆ TO NA PIŚMIE:<br>
	- jakie leki przyjmuje<br>
	- czy stan pacjenta jest "stabilny"<br>
	- jakie jest rozpoznanie choroby ,diagnoza<br>
	- ile waży i ile ma wzrostu<br>
	- w jakiej pozycji ma być transportowany<br>
	- czy transport ma się odbywać w asyście lekarza czy tylko sanitariuszy?<br>
	- gdzie i o której godzinie ma być dostarczony pacjent, jeżeli na samolot to należy podać numer lotu i upewnić się czy lot jest rejsowy czy specjalny medyczny dla tego pacjenta</p>'
				end
				else if @medicalService=40
				begin
					set @output='<p>Mamy możliwość rezerwacji biletu lotniczego oraz przygotowanie samolotu do przewozu pacjenta nie wymagającego specjalnego sprzętu ani warunków przewozu.
	Organizujemy: asystę pielęgniarską; wózek inwalidzki na lotnisku; windę pokładową dla pacjenta na noszach. Nie organizujemy odpowiedniego wyposażenie samolotu dla ciążko chorych pacjentów (tlen; prąd 12 volt itd., rezerwacja kilku miejsc w samolocie, transport w pozycji leżącej)
	<br>
	kontakt: Lotnicze Pogotowie Ratunkowe<br>
	tel: 022 331 49 99<br>
	fax: 022 835 19 19</p>'
				end
				else if @medicalService=50
				begin
					set @output='<p>Pokrywanie kosztów: <br>
	a) konsultacji lekarskich <br>
	b) badań lekarskich, zabiegów, lekarstw<br>
	c) pobytu w szpitalu<br>
	d) leczenia dentystycznego<br>
	Pokrywanie kosztów ogranicza się do przekazania europejskiego formularza E111 do szpitala. W przypadku braku zgody na akceptację europejskiego ubezpieczenia przekazywana jest gwarancja płatności. Nie mamy możliwości dokonania wyboru szpitala, który najlepiej odpowiada stanowi zdrowia Ubezpieczonego czy rezerwacji  łóżka w szpitalu. Nie mamy możliwości rezerwacji komory dekompresacyjnej.</p>'
				end
				else if @medicalService=60
				begin
					set @output='<p>Konieczność kontaktu z lekarzem i wypisanie recepty (leki rzadkie; drogie; o krótkim terminie ważności; konieczność natychmiastowego zażycia). Umowy z firmami medycznymi (Falck, Medikor, Swissmed). Następnie kontakt z kurierem TNT.</p>'
				end
				else if @medicalService=70
				begin
					set @output='<p>Przekazanie zlecenia firmie pogrzebowej, która organizuje: kremacje, trumne, urne, transport, zalatwia formalnosci, ceremonie, miejsce na cmentarzu itp.<br>
	Kontakt: S.O.S. Agencja Funeralna Piotr Godlewski<br>
	Warszawa ul. Ks.T. Boguckiego 3A/63<br>
	tel: 022 839 92 38<br>
	fax: 022 869 00 29<br>
	W przypadku ceremonii i pogrzebu na cmentarzu katolickim rodzina sama zalatwia sprawy zwiazane z organizacja nabożeństwa.</p>'
				end
				else if @medicalService=80
				begin
					set @output='<p>Swiadczenie "transport zwłok" obejmuje: <br>
	a) przygotowanie niezbędnych dokumentów<br>
	b) przygotowanie ciała i trumny do transportu<br>
	c) dostarczenie na lotnisko<br>
	opakowania trumny (konieczne przy transporcie lotniczym)<br>
	d) zorganizowanie podróży dla rodziny zmarłego<br>
	e) przesyłka rzeczy i bagażu zmarłego<br>
	Kontakt: S.O.S. Agencja Funeralna Piotr Godlewski.<br>
	Warszawa ul. Ks.T. Boguckiego 3A/63<br>
	tel: 022 839 92 38<br>
	fax: 022 869 00 29</p>'
				end
				else set @output=''
			end
			else
			begin
				if @medicalService=10
				begin
					set @output='<p>Starter24 kontaktuje się z właściwym partnerem sieci ARC, który przekazuje nam wyniki konsultacji medycznej. Następnie kontaktujemy się z zainteresowanymi stronami przekazując im wiadomości.
	<br>
	ARC NETWORK: Starter24 kontaktuje się z naszym partnerem firmą NZOZ MEDICAL TRANSPORT, której lekarz zadzwoni do  pacjenta lub szpitala oraz przeprowadza rozpoznanie medyczne z lekarzem prowadzącym pacjenta (stan chorego, podane leki, przeprowadzone badania itp.) Następnie Starter24 przekazujuje te informacje do  właściwego klubu, rodziny lub zainteresowanego.</p>'
				end
				else if @medicalService=20
				begin
					set @output='<p>Organizacja przez sieć ARC Medical Network</p>'
				end
				else if @medicalService=30
				begin
					set @output='<p>Organizacja na zasadach podobnych do krajowych po wcześniejszym kontakcie z właściwym partnerem.</p>'
				end
				else if @medicalService=40
				begin
					set @output='<p>Na terenie Austrii, Szwajcarii oraz Niemiec mamy możliwość oferowania pełnej gamy transportu lotniczego. Kluby z ww krajów posaidają bowiem własne samoloty.</p>'
				end
				else if @medicalService=50
				begin
					set @output='<p>Na terenie Austrii, Szwajcarii oraz Niemiec mamy możliwość oferowania pełnej gamy pokrywania kosztów leczenia łącznie z organizacją szpitala odpowiedniego do stanu zdrowia pacjenta.</p>'
				end
				else if @medicalService=60
				begin
					set @output='<p>Kontakt z właściwym parnerem.</p>'
				end
				else if @medicalService=70
				begin
					set @output='<p>Kontakt z właściwym parnerem.</p>'
				end
				else if @medicalService=80
				begin
					set @output='<p>Kontakt z właściwym parnerem.</p>'
				end
				else set @output=''
			end

			set @output='<font style='dbo.f_translate('color:red;',default)'>'+@output+'</font>'
		end
		ELSE IF @stepId = '1011.073' OR @stepId = '1011.074'
		BEGIN
			
			-- Informacja od koordynatora / Reklamacja
			
			DECLARE @noteId INT
			DECLARE @coordynatorId INT
			DECLARE @coordynatorName NVARCHAR(255)
			DECLARE @noteText NVARCHAR(MAX)
			
			--  Jest wyciagana treść notatki i wyświetlana na kroku.
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '167,228', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @noteId = value_int FROM @values
						
			SELECT @noteText = value_text, @coordynatorId = updated_by_id FROM AtlasDB_v.dbo.attribute_value WHERE parent_attribute_value_id = @noteId and attribute_path = '406,226,227'
			
			SET @coordynatorName = (SELECT CONCAT(firstname, ' ', lastname) FROM dbo.fos_user WHERE id = @coordynatorId)
			
			SET @output = '<i>Koordynator: </i> <b>' + ISNULL(@coordynatorName, '-') + '</b><br><br>'
			SET @output = @output + '<i>Treść: </i> <br>' + ISNULL(@noteText, '')
			
			
		END
		
		if @stepId like '1011.017'
		begin
			set @output='&nbsp;'
			DECLARE @platformGroup nvarchar(255)
			EXEC dbo.p_platform_group_name @groupProcessInstanceId=@groupProcessInstanceId, @name=@platformGroup OUTPUT
		
			if @platformGroup=dbo.f_translate('CFM',default)
			begin
				select top 1 @output=st.description 
				from	dbo.step_translation st inner join
						dbo.process_instance pin on pin.step_id=st.translatable_id 
				where	locale='pl' and 
						st.name=dbo.f_translate('Komunikat',default) and 
						pin.root_id=@rootId 	
				order by 	pin.id desc
			end
		end

		if @stepId= '1011.075'
		begin
			declare @code nvarchar(100)
			set		@code=dbo.f_diagnosis_code(@groupProcessInstanceId)
	
			SELECT @output = remarks
			FROM dbo.rsa_close_event 
			where @code LIKE arc_mask 
			AND description = dbo.f_translate('8-18 Kontakt z BLP',default)
			AND platform_id=@platformId
			AND (program_id=@programId or program_id is null)

		end
	end
--	set @output=dbo.f_get_platform_key('transport.question_limit',@platformId,@programId)
--	
--	PRINT @output
--		
--	if isnull(@output,'')=''
--	BEGIN
--		set @output= 'Możemy podjąć próbę organizacji biletu kolejowego celem odbioru samochodu po naprawie. <br>Czy życzy sobie Pan / Pani organizacji takiej usługi?'
--	END
--		

--	set @output=isnull(nullif(@output,''),'&nbsp;')
	IF isnull(@output,'') = ''
		BEGIN
			SET @output='&nbsp;'
		end
	SELECT @output 

end
