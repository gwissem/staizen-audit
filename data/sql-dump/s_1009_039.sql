
ALTER PROCEDURE [dbo].[s_1009_039]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @eta DATETIME
	DECLARE @smsText NVARCHAR(4000)
	DECLARE @locationString NVARCHAR(400)
	DECLARE @content NVARCHAR(4000)
	DECLARE @checkDate DATETIME
	DECLARE @estimated INT
	DECLARE @makeModel NVARCHAR(255)
	DECLARE @vin NVARCHAR(255)
	DECLARE @regNumber NVARCHAR(255)
	DECLARE @distance DECIMAL(18,2)
	DECLARE @executorPhone NVARCHAR(255)
	DECLARE @postponeCount INT
	DECLARE @postponeLimit INT
	DECLARE @groupProcessInstanceId INT
	DECLARE @latStart DECIMAL(18,5)
	DECLARE @longStart DECIMAL(18,5)
	DECLARE @latEnd DECIMAL(18,5)
	DECLARE @longEnd DECIMAL(18,5)
	DECLARE @partnerId INT
	
	
	SELECT @groupProcessInstanceId = p.group_process_id, 
	@postponeCount = p.postpone_count,
	@postponeLimit = s.postpone_count
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s on s.id = p.step_id
	WHERE p.id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '691', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @executorPhone = right(value_string,9) FROM @values
		
	SET @variant = 99
	
	IF @postponeCount = 0
	BEGIN
		DECLARE @minEstimated INT
		SET @minEstimated = 20
		
		EXEC p_location_string
		@attributePath = '101,85',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@locationString = @locationString OUTPUT

		SELECT @locationString = @locationString Collate SQL_Latin1_General_CP1253_CI_AI
	
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerId = value_int FROM @values
	
		EXEC [dbo].[p_sms_case_info]
		@groupProcessInstanceId = @groupProcessInstanceId,
	    @addEtaChangeInfo = 1,
	    @details = 1,
	    @content = @content OUTPUT
		
		EXEC p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @executorPhone,
		@userId = 1,
		@originalUserId = 1,
		@addInfo = 0,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
	END 
	ELSE IF @postponeCount >= @postponeLimit
	BEGIN
		SET @variant = 1
	END 
	ELSE
	BEGIN
		
		DECLARE @minutes BIGINT
		SELECT @checkDate = ISNULL(updated_at, created_at) FROM dbo.process_instance with(nolock) WHERE id = @previousProcessId
		SELECT TOP 1 @smsText = sms_text FROM sms_inbox with(nolock) WHERE numer_telefonu = '48'+right(@executorPhone,9) and data_otrzymania >= @checkDate AND ISNUMERIC(sms_text) = 1
		SET @minutes = try_parse(@smsText AS BIGINT)
		IF ISNULL(@minutes,0) > 0 AND ISNULL(@minutes,10000) < 10000  
		BEGIN
		
			SET @eta = DATEADD(MINUTE, @minutes , GETDATE())
			
			EXEC p_attribute_edit
			@attributePath = '107', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueDate = @eta,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @smsText,
			@phoneNumber = @executorPhone,
			@direction = 2,
			@userId = 1,
			@originalUserId = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			--IF @minutes > 90
			--BEGIN
			--	SET @variant = 3				
			--END 
			--ELSE
			--BEGIN
				print dbo.f_translate('Ala ma kota',default)

				SET @content = dbo.f_translate('Dokonalismy zmiany szacunkowego czasu dojazdu na: ',default)+dbo.FN_VDateHour(@eta)+dbo.f_translate('. Skontaktujemy sie telefonicznie, aby potwierdzic dojazd na miejsce.',default)
		
				EXEC p_note_new
				@groupProcessId = @groupProcessInstanceId,
				@type = dbo.f_translate('sms',default),
				@content = @content,
				@phoneNumber = @executorPhone,
				@userId = 1,
				@originalUserId = 1,
				@addInfo = 0,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				SET @variant = 1
			--END 
			
			
			--if dbo.f_isStepVariantInProcess(@groupProcessInstanceId,'1009.041',null)=1
			--begin
			--	SET @variant = 2
			--end
		END
	END

	UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(second, 15, GETDATE()), updated_at = GETDATE() WHERE id = @previousProcessId
	
END

