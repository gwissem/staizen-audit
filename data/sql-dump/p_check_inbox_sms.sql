ALTER PROCEDURE  p_check_inbox_sms
AS
  BEGIN
    DECLARE @smsInInbox TABLE(id                        int
    ,                         value                     NVARCHAR(MAX)
    ,                         number                    nvarchar(20)
    ,                         data                      NVARCHAR(1000)
    ,                         group_process_instance_id int)

    INSERT INTO @smsInInbox
    SELECT sms_inbox.id, sms_text, av.value_string, data_otrzymania, group_process_instance_id
    from sms_inbox with (nolock)
           left join sms_inbox_added sms_added with (nolock) on sms_added.id = sms_inbox.id
           left join attribute_value av with (nolock) on av.id = (select top 1 temp.id
                                                                  from attribute_value temp with (nolock)
                                                                  where temp.attribute_path = '197'
                                                                    and right(temp.value_string, 9) = right(sms_inbox.numer_telefonu, 9)
                                                                  order by isnull(temp.updated_at, temp.created_at) desc)
    where data_otrzymania > DATEADD(minute ,-10,GETDATE())
      and sms_added.id is null
      and group_process_instance_id is not null


    DECLARE @smsInboxID int
    DECLARE @phoneNumber nvarchar(20)
    DECLARE @text nvarchar(MAX)
    DECLARE @dateText nvarchar(30)
    DECLARE @groupSMSId int

    DECLARE db_cursor CURSOR FOR
      SELECT id, value, number, data, group_process_instance_id FROM @smsInInbox

    OPEN db_cursor
    FETCH NEXT FROM db_cursor
    INTO @smsInboxID,
      @text,
      @phoneNumber,
      @dateText,
      @groupSMSId

    WHILE @@FETCH_STATUS = 0
      BEGIN
        DECLARE @msg NVARCHAR(2000)
        declare  @err int


        SET @text = '' + isnull(@text, '')

        EXEC p_note_new @groupProcessId = @groupSMSId
        , @type = dbo.f_translate('sms',default)
        , @content = @text
        , @phoneNumber = @phoneNumber
        , @email = null
        , @subject = null
        , @direction = 2
        , @err = @err OUTPUT
        , @message = @msg OUTPUT
        ,
                        -- FOR EMAIL
                        @dw = ''
        , @udw = ''
        , @sender = ''
        , @additionalAttachments = ''
        , @emailBody = ''

        /**
        p_note_new <- Nie zwraca id notatki nowo utworzonej, trzeba by robić update po groupid, attribute_path = '406'
        */

        DECLARE @rootId int

        SELECT top 1 @rootID = root_id from process_instance with (nolock) where process_instance.id = @groupSMSId

        INSERT INTO sms_inbox_added (id, state, rootId) select @smsInboxID, 1, @rootID


        FETCH NEXT FROM db_cursor
        INTO @smsInboxID,
          @text,
          @phoneNumber,
          @dateText,
          @groupSMSId
      END

    CLOSE db_cursor
    DEALLOCATE db_cursor




  end