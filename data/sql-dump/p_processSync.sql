ALTER PROCEDURE [dbo].[p_processSync]
@processId int,
@databaseSrc varchar(50),
@databaseTrg varchar(50)
AS

declare @id varchar(20)

set @id=null
print '-- dbo.process_definition'
declare kur cursor LOCAL for			
		select id from dbo.process_definition where id=@processId
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.process_definition',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur

set @id=null
print '-- dbo.process_definition'
declare kur cursor LOCAL for			
		select id from dbo.process_definition_translation where translatable_id=@processId 
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.process_definition_translation',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur

set @id=null
print '-- dbo.step'
declare kur cursor LOCAL for			
		select id from dbo.step where id like cast(@processId as varchar(10))+'%'
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table=dbo.f_translate('dbo.step',default),
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur

set @id=null
print '-- dbo.step_translation'
declare kur cursor LOCAL for			
		select id from dbo.step_translation where translatable_id like cast(@processId as varchar(10))+'%'
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.step_translation',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur


set @id=null
print '-- dbo.step_attribute_definition'
declare kur cursor LOCAL for			
		select id from dbo.step_attribute_definition where step_id like cast(@processId as varchar(10))+'%'
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.step_attribute_definition',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur

--set @id=null
--print 'dbo.step_permissions'
--declare kur cursor LOCAL for			
--		select step_id from dbo.step_permissions where step_id like cast(@processId as varchar(10))+'%'
--OPEN kur;
--FETCH NEXT FROM kur INTO @id;
--WHILE @@FETCH_STATUS=0
--BEGIN
--	exec dbo.p_recordSync
--			@databaseSrc=@databaseSrc,
--			@databaseTrg=@databaseTrg,
--			@table='dbo.step_permissions',
--			@id=@id
--	FETCH NEXT FROM kur INTO @id;
--END
--CLOSE kur
--DEALLOCATE kur

set @id=null
print '-- dbo.process_flow'
declare kur cursor LOCAL for			
		select id from dbo.process_flow where step_id like cast(@processId as varchar(10))+'%'
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.process_flow',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur


set @id=null
print '-- dbo.process_flow_translation'
declare kur cursor LOCAL for			
		select id from dbo.process_flow_translation where translatable_id in (
			select id from dbo.process_flow where step_id like cast(@processId as varchar(10))+'%'
		)
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.process_flow_translation',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur


set @id=null
print '-- dbo.form_template'
declare kur cursor LOCAL for			
		select id from dbo.form_template where id in (
		 select form_template_id from dbo.step_form_template where step_id like cast(@processId as varchar(10))+'%'
			)
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.form_template',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur

set @id=null
print '-- dbo.step_form_template'
declare kur cursor LOCAL for			
		select form_template_id from dbo.step_form_template where step_id like cast(@processId as varchar(10))+'%'

OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.step_form_template',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur

set @id=null
print '-- dbo.form_control'
declare kur cursor LOCAL for			
		select id from dbo.form_control where form_template in (
			select id from dbo.form_template where id in (
				select form_template_id from dbo.step_form_template where step_id like cast(@processId as varchar(10))+'%'
		))
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.form_control',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur

set @id=null
print '-- dbo.form_control_translation'
declare kur cursor LOCAL for			
		select id from dbo.form_control_translation where translatable_id in (
			select id from dbo.form_control where form_template in (
				select id from dbo.form_template where id in (
					select form_template_id from dbo.step_form_template where step_id like cast(@processId as varchar(10))+'%'
			)))
OPEN kur;
FETCH NEXT FROM kur INTO @id;
WHILE @@FETCH_STATUS=0
BEGIN
	exec dbo.p_recordSync
			@databaseSrc=@databaseSrc,
			@databaseTrg=@databaseTrg,
			@table='dbo.form_control_translation',
			@id=@id
	FETCH NEXT FROM kur INTO @id;
END
CLOSE kur
DEALLOCATE kur

print 'GO'

--attribute
--attribute_translation
--attribute_attributes
--attribute_condition
--attribute_form_template

declare @SQLP nvarchar(max)

declare kur cursor LOCAL for			
		SELECT dbo.f_translate('ALTER ',default) + RIGHT(r.ROUTINE_DEFINITION, (LEN(r.ROUTINE_DEFINITION) - CHARINDEX(dbo.f_translate('PROCEDURE',default), r.ROUTINE_DEFINITION, 0)+2)) 
		FROM	INFORMATION_SCHEMA.ROUTINES r 
		WHERE	r.ROUTINE_TYPE = dbo.f_translate('PROCEDURE',default) and ROUTINE_NAME like '%s_'+cast(@processId as varchar(20))+'_%'
OPEN kur;
FETCH NEXT FROM kur INTO @SQLP;
WHILE @@FETCH_STATUS=0
BEGIN
	print '------------------------------------'
	print @SQLP
	print 'GO'
	print '------------------------------------'
	FETCH NEXT FROM kur INTO @SQLP;
END
CLOSE kur
DEALLOCATE kur


print '-- Finish'
