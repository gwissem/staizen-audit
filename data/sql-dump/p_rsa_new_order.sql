ALTER PROCEDURE [dbo].[p_rsa_new_order]
@groupProcessInstanceId INT,
@partnerPhone NVARCHAR(20),
@iteration INT = 1,
@alreadyConfirmed INT = 0
AS
BEGIN
    
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @caseId NVARCHAR(255)
	DECLARE @rootId INT
	DECLARE @eventName NVARCHAR(255)
 	DECLARE @postalCode NVARCHAR(255)
 	DECLARE @city NVARCHAR(255)
 	DECLARE @streetName NVARCHAR(255)
 	DECLARE @streetNumber NVARCHAR(255)
 	DECLARE @longitude DECIMAL(18,4)
 	DECLARE @latitude DECIMAL(18,4)
 	DECLARE @executorLongitude DECIMAL(18,4)
 	DECLARE @executorLatitude DECIMAL(18,4)
	DECLARE @executorLongitudeText NVARCHAR(255)
 	DECLARE @executorLatitudeText NVARCHAR(255)
 	DECLARE @dmc INT
 	DECLARE @programName NVARCHAR(255)
 	DECLARE @programId NVARCHAR(255)
 	DECLARE @eventType NVARCHAR(255)
 	DECLARE @makeModel NVARCHAR(255)
 	DECLARE @vin NVARCHAR(20)
 	DECLARE @regNumber NVARCHAR(12)
 	DECLARE @driverFirstname NVARCHAR(100)
 	DECLARE @driverLastname NVARCHAR(100)
 	DECLARE @driverName NVARCHAR(255)
 	DECLARE @driverPhone NVARCHAR(255)
 	DECLARE @mileage INT
 	DECLARE @partnerRegNumber NVARCHAR(255)
 	DECLARE @serviceId INT
 	DECLARE @serviceName NVARCHAR(255)
 	DECLARE @partnerName NVARCHAR(255)
 	DECLARE @partnerShortName NVARCHAR(255)
 	DECLARE @platformId INT
 	DECLARE @platformName NVARCHAR(255)
 	DECLARE @diagnosisDescription NVARCHAR(4000)
 	DECLARE @additionalInfoForContractor NVARCHAR(4000)
 	DECLARE @partnerServiceLongitude DECIMAL(18,4)
 	DECLARE @partnerServiceLatitude DECIMAL(18,4)
 	DECLARE @partnerServiceLocation NVARCHAR(4000)
 	DECLARE @customDate DATETIME
 	DECLARE @isIcs INT
 	DECLARE @locationShort NVARCHAR(MAX)
 	DECLARE @road NVARCHAR(255)
 	DECLARE @partnerId INT
 	DECLARE @towingDestination NVARCHAR(4000)
 	DECLARE @additionalInfo NVARCHAR(1000)
 	DECLARE @rsaCount INT
 	DECLARE @sufix NVARCHAR(10)
 	DECLARE @fixingOrTowing INT
 	DECLARE @peopleToTransport INT
 	DECLARE @vehicleLength DECIMAL(10,2)
 	DECLARE @vehicleHeight DECIMAL(10,2)
 	DECLARE @vehicleWidth DECIMAL(10,2)
	DECLARE @location NVARCHAR(2000)
 	DECLARE @diagnosisSummary NVARCHAR(1000)
 	DECLARE @towingProblem NVARCHAR(255)
 	DECLARE @terminatedEtaCount INT
 	DECLARE @diagnosisCode NVARCHAR(20)
 	DECLARE @locationNotes NVARCHAR(4000)
 	DECLARE @towingProblemDesc NVARCHAR(4000)
 	DECLARE @towingProblemTireMissing INT
 	DECLARE @towingProblemId INT
 	DECLARE @diagnosisMoreInfo NVARCHAR(4000)
 	DECLARE @fddsMoreInfo NVARCHAR(4000)
 	DECLARE @towingDestinationCity NVARCHAR(50)
 	DECLARE @towingProblemText NVARCHAR(200)
 	DECLARE @reasonsCount INT
	DECLARE @isCargo INT
	DECLARE @cargoType  NVARCHAR(200)
	DECLARE @cargoWeight INT
	DECLARE @cargoTowingText  NVARCHAR(500)
	DECLARE @towingDestinationZipCode nvarchar(50)
	DECLARE @towingPlace nvarchar(400)
	-- 	TRAILER
	DECLARE @isTrailer INT
	DECLARE @trailerTowingText NVARCHAR(4000)
	DECLARE @trailerDMC INT
	DECLARE @isTrailerCargo INT
	DECLARE @trailerCargoWeight DECIMAL(10,2)
	DECLARE @trailerCargoType NVARCHAR(500)
	DECLARE @trailerLength DECIMAL(10,2)
	DECLARE @trailerWidth DECIMAL(10,2)
	DECLARE @trailerHeight DECIMAL(10,2)
	DECLARE @trailerAdditionalText NVARCHAR(2000)
	DECLARE @trailerTowingSameDestination INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @mute int 
 	
	INSERT @values EXEC p_attribute_get2 @attributePath = '860', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @mute = value_int FROM @values
	
	IF @mute = 1
	BEGIN
		RETURN
	END 
	
	-- TRAILER END
	-- 	TRAILER MOCKED VALUES
	SET @isCargo  = 0 -- default none

	SET @trailerTowingText =''
	SET @trailerAdditionalText =''
	-- 	TRAILER MOCKED END
--  Cargo setup
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '74,430',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @cargoType = value_string FROM @values
	If isnull(@cargoType,'') <> ''
	BEGIN
		SET @isCargo = 1
	END
-- END CARGO SETUP

-- 	TRAILER
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '868,74,76',  --Trailer DMC
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @trailerDMC = value_int FROM @values

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '868,74,430', -- trailerCargo type
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @trailerCargoType = value_string FROM @values
	if isnull(@trailerCargoType,'') <> ''
		BEGIN
			SET @isTrailerCargo  = 1
		END
	ELSE
		BEGIN
			SET @isTrailerCargo = 0
		end

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '868,74,261',  --Trailer Length
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @trailerLength = value_decimal FROM @values

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '868,74,262',  --Trailer Width
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @trailerWidth = value_decimal FROM @values

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '868,74,263',  --Trailer height
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @trailerHeight = value_decimal FROM @values


	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '291', -- czy holowawnie przyczepy
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isTrailer = value_int FROM @values
	IF isnull(@isTrailer,0) <> 1
		BEGIN
			SET @isTrailer = 0
	END

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '868,74,429',  --Trailer DMC
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @trailerCargoWeight = value_int FROM @values

	-- /Trailer Setting up values
 	SET @towingProblemText = ''
 	SET @cargoTowingText = ''

	SELECT @rootId = root_id FROM process_instance where group_process_id = @groupProcessInstanceId
	SET @rsaCount = (SELECT TOP 1 ROW_NUMBER() OVER (ORDER BY group_process_id) rowNumber from  process_instance where root_id = @rootId and step_id like '1009.%'  group by group_process_id order by rowNumber desc)
	SELECT @terminatedEtaCount = COUNT(*) FROM dbo.process_instance where root_id = @rootId AND step_id = '1009.049' AND choice_variant = 3
	SELECT @reasonsCount = COUNT(*) FROM dbo.attribute_value where group_process_instance_id = @groupProcessInstanceId and attribute_path = '701,702,703' and value_int IS NOT NULL 
	
	SET @rsaCount = isnull(@rsaCount,1) + ISNULL(@terminatedEtaCount,0) + ISNULL(@reasonsCount,0) + rand()
	SELECT @sufix = CAST(@rsaCount as NVARCHAR(5)) + '-' + CAST(@iteration as NVARCHAR(5)) + '-' + CAST(CAST(20*RAND() AS INT) AS NVARCHAR(5))
	SELECT @caseId = dbo.f_caseId(@groupProcessInstanceId) +'-'+@sufix
	
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '610',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	SELECT TOP 1 @isIcs =  value_int FROM dbo.attribute_value WHERE parent_attribute_value_id = (
		SELECT parent_attribute_value_id FROM dbo.attribute_value where id = @partnerId 
	) and attribute_path = '595,634'

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	SELECT @platformName = name from dbo.platform WHERE id = @platformId
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @customDate = value_date FROM @values
	
	declare @eventTypeId INT 
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventTypeId = value_int FROM @values
	SELECT @eventType = textD FROM dbo.dictionary WHERE value = @eventTypeId AND typeD = 'eventType'
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,89', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @postalCode = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,87', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @city = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,509', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @road = value_string FROM @values
	
	DECLARE @locationString NVARCHAR(1000)
	DECLARE @fullLocationString NVARCHAR(1000)
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,94', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @streetName = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,95', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @streetNumber = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,63', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @locationNotes = value_text FROM @values
	
	SET @locationString = ISNULL(@streetName,'') + ' ' + ISNULL(@streetNumber,'')
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @longitude = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @latitude = value_string FROM @values
	
	SELECT @executorLongitudeText = av.value_string FROM dbo.attribute_value av 
	INNER JOIN dbo.attribute_value av2 ON av.parent_attribute_value_id = av2.id
	WHERE av2.parent_attribute_value_id = @partnerId AND av.attribute_path = '595,597,85,93'
	
	SELECT @executorLatitudeText = av.value_string FROM dbo.attribute_value av 
	INNER JOIN dbo.attribute_value av2 ON av.parent_attribute_value_id = av2.id
	WHERE av2.parent_attribute_value_id = @partnerId AND av.attribute_path = '595,597,85,92'
	
	PRINT '--lat'
	PRINT @executorLatitudeText
	PRINT '--long'
	PRINT @executorLongitudeText
	
	SELECT @executorLatitude = CAST(@executorLatitudeText AS DECIMAL(18,4))
	SELECT @executorLongitude = CAST(@executorLongitudeText AS DECIMAL(18,4))
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,76', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @dmc = value_int FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT  @makeModel = textD FROM dbo.dictionary WHERE value IN (SELECT value_int FROM @values) AND typeD = 'makeModel'
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT  @programName = name FROM dbo.vin_program WHERE id IN (SELECT value_string FROM @values)
	SELECT  @programId= value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,64', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT  @driverFirstname = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,66', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT  @driverLastname = value_string FROM @values
	
	SET @driverName = ISNULL(@driverFirstname,'') + ' ' + ISNULL(@driverLastname,'')
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT  @driverPhone = value_string FROM @values
	
	if left(@driverPhone,2)='00' set @driverPhone = substring(@driverPhone,3,20)
	if left(@driverPhone,1)='0' set @driverPhone = substring(@driverPhone,2,20)

	if LEN(@driverPhone) > 9
	BEGIN
		SET @driverPhone = '+'+@driverPhone
	END 
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vin = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @mileage = value_int FROM @values
	
	
	--delete from @values
	--INSERT @values EXEC p_attribute_get2 @attributePath = '595,614', @groupProcessInstanceId = @groupProcessInstanceId
	--SELECT @partnerShortName = value_string FROM @values
	
	--delete from @values
	--INSERT @values EXEC p_attribute_get2 @attributePath = '595,84', @groupProcessInstanceId = @groupProcessInstanceId
	--SELECT @partnerName = value_string FROM @values
	
	--SELECT TOP 1 @partnerShortName =  value_string FROM dbo.attribute_value WHERE parent_attribute_value_id = @partnerId and attribute_path = '595,597,84'
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerServiceLatitude = try_parse(value_string as decimal(18,6)) FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerServiceLongitude = try_parse(value_string as decimal(18,6)) FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '428', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @peopleToTransport = value_int FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,263', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleHeight = value_decimal FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,261', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleLength = value_decimal FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,262', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleWidth = value_decimal FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,429', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @cargoWeight = value_int FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '417', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @diagnosisSummary = value_text FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '551', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towingProblemId = value_int FROM @values
	SELECT @towingProblem = textD FROM dbo.dictionary WHERE value = @towingProblemId and typeD = 'towingProblems'
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '552', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT  @towingProblemDesc = value_text FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '553', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT  @towingProblemTireMissing = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '417', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @diagnosisMoreInfo = value_text FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @serviceId = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '994', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fddsMoreInfo = value_text FROM @values
	PRINT '-----test-1'
	
	exec [dbo].[p_towing_destination] 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@partnerId = @serviceId,
	@attributePath = '211,85',
	@output = @towingPlace output
	
	PRINT '-----test-2'
	
	SELECT @diagnosisDescription = dbo.f_diagnosis_description(@rootId,'pl')

	SET @additionalInfoForContractor = isnull(dbo.f_get_platform_key ('program.contractor_info_all',@platformId, @programId),'')

	IF @eventTypeId = 1
	BEGIN
		SET @additionalInfoForContractor = @additionalInfoForContractor +  isnull(dbo.f_get_platform_key ('program.contractor_info_harm',@platformId, @programId),'')
	END

	-- 	SETTING UP VARIABLES END

	SELECT @additionalInfo = dbo.f_translate('Zlecenie ',default)+dbo.f_conditionalText(@fixingOrTowing,dbo.f_translate('holowania',default),dbo.f_translate('naprawy na drodze',default))+' / ' + 
			dbo.f_translate('Klient: ',default) + @driverName
-- 	HOLOWANIE
	IF @fixingOrTowing = 1 -- 1 - holowanie  0/2 - naprawa (prosta / złożna)
	BEGIN
		IF ISNULL(@peopleToTransport,0) > 0
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Liczba osób do transportu wraz z holowanym pojazdem: ',default) + 
			CAST(@peopleToTransport AS NVARCHAR(10))
			if isnull(@peopleToTransport,0) >2 and @platformId in (6,11,31)
				BEGIN
					SET @additionalInfo = @additionalInfo +
																			 dbo.f_translate(' / Maksymalny koszt taksówki to 80 EUR. Powyżej podanego limitu klient dopłaca wg stawek ad-hoc 3 zl brutto/km transportu osób dodatkowym samochodem osobowym wraz z powrotem.',default)
				end
			
		END 
		IF ISNULL(@diagnosisDescription,'') <> ''
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Opis usterki: ',default) +
			@diagnosisDescription
		END
		IF ISNULL(@diagnosisSummary,'') <> ''
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Dodatkowe informacje: ',default) +
			@diagnosisSummary 
		END
		IF ISNULL(@locationNotes,'') <> ''
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Lokalizacja - uwagi: ',default) + 
			@locationNotes 
		END
		IF ISNULL(@vehicleLength,0) > 0
		BEGIN
			SET @additionalInfo = @additionalInfo +
			' / Długość pojazdu (m): ' + 
			CAST(@vehicleLength AS NVARCHAR(10))
		END
		IF ISNULL(@vehicleHeight,0) > 0
		BEGIN
			SET @additionalInfo = @additionalInfo +
			' / Wysokość pojazdu (m): ' +
			CAST(@vehicleHeight AS NVARCHAR(10)) 
		END

		IF ISNULL(@towingProblemDesc,'') <> '' AND @towingProblemId IN (98,99)
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Możliwe trudności przy załadunku: ',default) +
			@towingProblemDesc
			
			SET @towingProblemText = dbo.f_translate('Możliwe trudności przy załadunku: ',default) +@towingProblemDesc +'. '
		END
		ELSE
		IF ISNULL(@towingProblemId,0) <> 0
		BEGIN
			
			IF ISNULL(@towingProblemTireMissing,0) <> 0 AND @towingProblemId = 7
			BEGIN
				SET @towingProblem = @towingProblem + '(' + CAST(@towingProblemTireMissing AS NVARCHAR(10)) + ')'
			END
			
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Możliwe trudności przy załadunku: ',default) +
			@towingProblem
			
			SET @towingProblemText = dbo.f_translate('Możliwe trudności przy załadunku: ',default) +@towingProblem+'. '
		END
		
		IF isnull(@isCargo,0) >0
			BEGIN
			SET @cargoTowingText = dbo.f_translate('/ Ładunek w pojeździe: ',default)	 + isnull(@cargoType,'')
				IF isnull(@cargoWeight,0) > 0
					BEGIN
						SET @cargoTowingText = @cargoTowingText +
																	' / Waga ładunku (kg): ' +
																	 isnull(CAST(@cargoWeight AS NVARCHAR(10)), '')
				END
				SET @additionalInfo = @additionalInfo + @cargoTowingText
		END

		IF isnull(@isTrailer,0 ) >0
			BEGIN

				SET @trailerTowingText = dbo.f_translate(' / Przyczepa: / DMC:',default) +	isnull(CAST(@trailerDMC AS NVARCHAR(10)), '')

				IF isnull(@isTrailerCargo, 0 ) > 0
				BEGIN
						SET @trailerTowingText = @trailerTowingText +dbo.f_translate(', ładunek:',default)+isnull(CAST(@trailerCargoWeight AS NVARCHAR(10)), '') + 'kg '+isnull(@trailerCargoType,'');
				END

				SET @trailerTowingText = @trailerTowingText +', dł: '+	isnull(CAST(@trailerLength AS NVARCHAR(10)), '') + dbo.f_translate(', szer: ',default) +	isnull(CAST(@trailerWidth AS NVARCHAR(10)), '')  +dbo.f_translate(', wys: ',default)+isnull(CAST(@trailerHeight AS NVARCHAR(10)), '')
				IF isnull(@trailerAdditionalText,'') <> ''
				BEGIN
						SET @trailerTowingText = @trailerTowingText+', '+@trailerAdditionalText
				END
				
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '868,869', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @trailerTowingSameDestination = value_int FROM @values
			

				IF isnull(@trailerTowingSameDestination,1) = 0
				BEGIN
					
					EXEC p_location_string
					@attributePath = '868,211,85',
					@groupProcessInstanceId = @groupProcessInstanceId,
					@type = 1,
					@locationString = @location OUTPUT
					
					SET @trailerTowingText = @trailerTowingText +dbo.f_translate(', docelowe miejsce holowania: ',default)+@location+'.'
				END 
				ELSE
				BEGIN
					SET @trailerTowingText = @trailerTowingText +dbo.f_translate(', docelowe miejsce holowania: takie same jak poszkodowanego pojazdu.',default)
				END 

		END
		SET @additionalInfo =@additionalInfo + @trailerTowingText
		IF isnull(@additionalInfoForContractor,'') <> ''
		BEGIN
			SET @additionalInfo = @additionalInfo + dbo.f_translate('/ Dodatkowe informacje:  ',default)+ @additionalInfoForContractor
		END
		
		if @platformId=5
		begin
			declare @km int
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '295,225', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @km = value_decimal FROM @values

			SET @additionalInfo = @additionalInfo + dbo.f_translate('Maksymalnie można transportować pojazd na dystansie ',default)+isnull(cast(@km as nvarchar(20)),'')+dbo.f_translate(' km. Zgodnie z umową w skład powyższych kilometrów wchodzą: ',default)
			
			if @peopleToTransport>2
			begin
				SET @additionalInfo = @additionalInfo + dbo.f_translate('dojazd na miejsce zdarzenia, holowanie i transport powyżej 2 osób w kabinie. ',default)
			end
			else
			begin
				SET @additionalInfo = @additionalInfo + dbo.f_translate('dojazd na miejsce zdarzenia i holowanie. ',default)
			end
			
			SET @additionalInfo = @additionalInfo + dbo.f_translate('Powyżej tej liczby kilometrów klient dopłaca wg stawek ad-hoc.',default)	
		end

	END
	ELSE
	BEGIN -- 		NAPRAWA
		IF ISNULL(@diagnosisDescription,'') <> ''
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Opis usterki: ',default) +
			@diagnosisDescription
		END
		IF ISNULL(@diagnosisSummary,'') <> ''
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Dodatkowe informacje: ',default) +
			@diagnosisSummary 
		END
		IF ISNULL(@locationNotes,'') <> ''
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Lokalizacja - uwagi:',default) + 
			@locationNotes 
		END
		
		IF ISNULL(@towingProblemDesc,'') <> '' AND @towingProblemId IN (98,99)
		BEGIN
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Możliwe trudności przy załadunku: ',default) +
			@towingProblemDesc
			
--			SET @towingProblemText = dbo.f_translate('Możliwe trudności przy załadunku: ',default) +@towingProblemDesc +'. '
		END
		ELSE
		IF ISNULL(@towingProblemId,0) <> 0
		BEGIN
			
			IF ISNULL(@towingProblemTireMissing,0) <> 0 AND @towingProblemId = 7
			BEGIN
				SET @towingProblem = @towingProblem + '(' + CAST(@towingProblemTireMissing AS NVARCHAR(10)) + ')'
			END
			
			SET @additionalInfo = @additionalInfo +
			dbo.f_translate(' / Możliwe trudności przy załadunku: ',default) +
			@towingProblem
			
--			SET @towingProblemText = dbo.f_translate('Możliwe trudności przy załadunku: ',default) +@towingProblem+'. '
		END
		
		IF isnull(@additionalInfoForContractor,'')<> ''
		BEGIN
			SET @additionalInfo = @additionalInfo + dbo.f_translate('/ Dodatkowe informacje: ',default)+ @additionalInfoForContractor
		END

		if @platformId=5
		begin
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '295,225', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @km = value_decimal FROM @values

			SET @additionalInfo = @additionalInfo + dbo.f_translate('W przypadku potwncjalnego holowania maksymalnie można transportować pojazd na dystansie ',default)+isnull(cast(@km as nvarchar(20)),'')+dbo.f_translate(' km. Zgodnie z umową w skład powyższych kilometrów wchodzą: ',default)
			
			if @peopleToTransport>2
			begin
				SET @additionalInfo = @additionalInfo + dbo.f_translate('dojazd na miejsce zdarzenia, holowanie i transport powyżej 2 osób w kabinie. ',default)
			end
			else
			begin
				SET @additionalInfo = @additionalInfo + dbo.f_translate('dojazd na miejsce zdarzenia i holowanie. ',default)
			end
			
			SET @additionalInfo = @additionalInfo + dbo.f_translate('Powyżej tej liczby kilometrów klient dopłaca wg stawek ad-hoc.',default)	
		end

	END -- naprawa


-- 	Dodałem info z diagnozy i fdds ~Helis
	IF isnull(@diagnosisMoreInfo,'') <> '' Or isnull(@fddsMoreInfo,'')<> ''
		BEGIN
			SET @additionalInfo = @additionalInfo + isnull(@diagnosisMoreInfo+' / ','') + isnull(@fddsMoreInfo,'')+'. '
	END

	PRINT '-----test-3'
	
	DECLARE @aboveLimit INT
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '562', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @aboveLimit = value_int FROM @values
	
	IF @aboveLimit = 1
	BEGIN
		DECLARE @programDistance INT
		declare @arcCode NVARCHAR(10)
		SELECT @arcCode = dbo.f_diagnosis_code(@groupProcessInstanceId)
		SELECT @programDistance = dbo.f_program_towing_distance(@programId, @arcCode)
		
		IF @programDistance > 0
		BEGIN			
			SET @additionalInfo = @additionalInfo + dbo.f_translate(' / UWAGA: Powyżej limitu ',default)+CAST(@programDistance AS NVARCHAR(10))+dbo.f_translate(' km, Klient pokrywa koszty dalszego holowania.',default)						 
		END 		
	END
	
	SET @additionalInfo = @additionalInfo + '/ Pamiętaj! Naciśnij PRZYJMIJ i podaj czas dojazdu jeżeli chcesz przyjąć zlecenie.'
	DECLARE @uid NVARCHAR(40)
	SET @uid = NEWID()

	IF @isIcs = 1
	BEGIN
		-- nullowanie icsId		
		EXEC p_attribute_edit
		@attributePath = '609', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = null,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		declare @extId int
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '695', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @extId = value_int FROM @values

		if @extId is null
		begin
			set @extId=@groupProcessInstanceId
		end

	declare @tabela as table ( 
		  nrSprawy varchar(35)
		, status          int NULL
		, typKontaktu     int
		, podusluga       varchar(300)
		, nazwaZdarzenia  varchar(300)
		, kod             varchar(10)
		, miejscowosc     varchar(50)
		, ulicaNr         varchar(100)
		, koordX          decimal(18,4)
		, koordY          decimal(18,4)
		, lokalizacja     varchar(300)
		, DMC             int
		, program         varchar(300)
		, awariaWypadek   varchar(15)
		, marka           varchar(50)
		, model           varchar(50)
		, VIN             varchar(20)
		, nrRej           varchar(20)
		, kierowca        varchar(50)
		, kierowcaTel     varchar(20)
		, TrescSMS        varchar(4000)
		, nrTelSMS        varchar(20)
		, sprawa_id       int
		, przebieg        int
		, nrRejHolownika  varchar(20)
		, uslugaId        int
		, nazwaUslugi     varchar(100)
		, dataPrzekazania datetime
		, KodStarter      varchar(50)
		, nazwaKontraktora varchar(200)
		, opiekun         varchar(100)
    , telefonKontr    varchar(200)
    , uid             varchar(40)
    , platformaId     int
		, platforma			varchar(200)
    , opisDiagnozyPl  varchar(300)
    , WkoordX          decimal(18,4)
    , WkoordY          decimal(18,4)
    , warsztatDocelowy varchar(300)
    , preEta datetime
    , info varchar(1000)
    , termin datetime
		, BkoordX decimal(18,4)
		, BkoordY decimal(18,4)
	)


	declare @serviceType nvarchar(100)

	set @serviceType=dbo.f_translate('naprawa',default)
    if @fixingOrTowing=1
	begin
		set @serviceType=dbo.f_translate('holowanie',default)
	end

	insert into @tabela
 	values (
	 	@caseId,
	 	NULL,
	 	NULL,
	 	@serviceType,
	 	@eventName,
	 	@postalCode,
	 	@city,
	 	@locationString,
	 	@latitude,
	 	@longitude,
	 	@locationString,
	 	@dmc,
	 	@programName,
	 	@eventType,
	 	@makeModel,
	 	@makeModel,
	 	@vin,
	 	@regNumber,
	 	@driverName,
	 	@driverPhone,
	 	NULL,
	 	@partnerPhone,
	 	@rootId,
	 	@mileage,
	 	@partnerRegNumber,
	 	@extId,
	 	@serviceName,
	 	GETDATE(),
	 	'',
	 	'',
	 	NULL,
	 	@partnerPhone,
	 	@uid,
	 	@platformId,
	 	@platformName,
	 	@diagnosisDescription,
	 	@partnerServiceLatitude,
	 	@partnerServiceLongitude,
	 	@towingPlace,
	 	@customDate,
	 	@additionalInfo,
	 	@customDate,
		@executorLatitude,
		@executorLongitude
	)
   
    -- Teka2 APP
    INSERT OPENQUERY (Teka2, 'SELECT number, contact_type,  service,  name,  postal_code, city, street_and_number, longitude, latitude, localisation, dmc, program, crash_accident, brand, model, vin,
                              registration_number, client_name, responsible_user_phone,client_phone_number, company_id, status, created_at, updated_at, towing_destination, pre_eta, additional_info, order_to_date, eta
                    		  FROM   StarterTeka.orders')
    select nrSprawy        
    , typKontaktu     
    , podusluga       
    , nazwaZdarzenia  
    , kod              
    , miejscowosc     
    , ulicaNr         
    , koordX          
    , koordY          
    , lokalizacja     
    , DMC             
    , program         
    , awariaWypadek   
    , marka           
    , model      
    , VIN            
    , nrRej           
    , kierowca        
    , right(nrTelSMS,9) nrTelSMS
    , kierowcaTel     
    , 1 KodStarter
    , status          
    , getdate()       
    , getdate()     
    , warsztatDocelowy     
    , preEta
    , info
    , termin
    , termin
 from @tabela
    where nrTelSMS is not null

    DECLARE @icsId INT
	
    
	select top 1 @icsId = id from openquery(teka2,'select id,number  FROM   StarterTeka.orders where number like ''A%'' order by id desc limit 20') a
	where number= @caseId
	order by id desc

	
	-- domyślne kody zamknięć
	SET @diagnosisCode = dbo.f_diagnosis_code(@groupProcessInstanceId)
	INSERT OPENQUERY (Teka2, 'SELECT order_id, code, mask, created_at, updated_at FROM StarterTeka.order_predefined_teka_codes')
	SELECT @icsId, @diagnosisCode, 0, GETDATE(), GETDATE()
	INSERT OPENQUERY (Teka2, 'SELECT order_id, code, mask, created_at, updated_at FROM StarterTeka.order_predefined_teka_codes')
	SELECT @icsId, @diagnosisCode, 1, GETDATE(), GETDATE()
	INSERT OPENQUERY (Teka2, 'SELECT order_id, code, mask, created_at, updated_at FROM StarterTeka.order_predefined_teka_codes')
	SELECT @icsId, @diagnosisCode, 2, GETDATE(), GETDATE()
	INSERT OPENQUERY (Teka2, 'SELECT order_id, code, mask, created_at, updated_at FROM StarterTeka.order_predefined_teka_codes')
	SELECT @icsId, @diagnosisCode, 3, GETDATE(), GETDATE()
	
	EXEC p_attribute_edit
	@attributePath = '609', 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@valueInt = @icsId,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	EXEC p_attribute_edit
	@attributePath = '616', 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@valueString = @uid,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
 --   INSERT OPENQUERY (Teka2, 'SELECT          order_id,code,mask,created_at,updated_at
 --   FROM    StarterTeka.order_predefined_teka_codes')
 --   select distinct t2.id,tk.kod,maska,getdate(),getdate()
 --   from  OPENQUERY (Teka2, 'SELECT id,number
 --   FROM   StarterTeka.orders o left join StarterTeka.order_predefined_teka_codes p on o.id=p.order_id
	--where p.code is null and o.created_at>now()- interval 1 day') t2 inner join @tabela t on t.nrSprawy=t2.number COLLATE Polish_CI_AS inner join
 --   dbo.Teka_kody tk with(nolock) on (t.opisDiagnozyPl=tk.opis) 

	PRINT '-----test-5'
	
    -- Teka server
    INSERT OPENQUERY (Teka2, 'SELECT nrSprawy, status, typKontaktu, podusluga, nazwaZdarzenia, kod, miejscowosc, ulicaNr, koordX, koordY, lokalizacja, DMC,
    program, awariaWypadek, marka, model, VIN, nrRej, kierowca, kierowcaTel,
    TrescSMS, nrTelSMS,
    sprawa_id, przebieg, nrRejHolownika, uslugaId, nazwaUslugi, dataPrzekazania, kodStarter, telefonKontr, uid, platformaId, platforma, WkoordX, WkoordY, BkoordX, BkoordY
    FROM                  Sprawy')
    select nrSprawy
    , status
    , typKontaktu
    , podusluga
    , nazwaZdarzenia
    , kod
    , miejscowosc
    , ulicaNr
    , koordX
    , koordY
    , lokalizacja+ isnull(' '+miejscowosc,'') lokalizacja
    , DMC
    , program
    , awariaWypadek
    , marka
    , model
    , VIN
    , nrRej
    , kierowca
    , kierowcaTel
    , TrescSMS
    , nrTelSMS
    , sprawa_id
    , przebieg
    , nrRejHolownika
    , uslugaId
    , nazwaUslugi
    , dataPrzekazania
    , KodStarter
    , telefonKontr
    , uid
    , platformaId
    , platforma
    , WkoordX
    , WkoordY
	, BkoordX
	, BkoordY
    from @tabela

	END
	ELSE
	BEGIN
			
		-- zlecenie sms
		IF ISNULL(@alreadyConfirmed,0) = 0
		BEGIN
			DECLARE @content NVARCHAR(MAX)
			
			EXEC [dbo].[p_sms_case_info]
			@groupProcessInstanceId = @groupProcessInstanceId,
		    @content = @content OUTPUT
			
			EXEC p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @partnerPhone,
			@userId = 1,
			@originalUserId = 1,
			@addInfo = 0,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
		END
		
		EXEC p_attribute_edit
		@attributePath = '609', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = -1,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
	END 
END
