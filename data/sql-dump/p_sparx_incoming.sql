
ALTER PROCEDURE [dbo].[p_sparx_incoming]
@sparx_id int,
@processInstanceId int output
AS
begin

	DECLARE @json NVARCHAR(MAX)
	declare @eventType nvarchar(100)
	declare @groupProcessInstanceId int
	DECLARE @err int
	DECLARE @message nvarchar(255)
	DECLARE @processInstanceIds varchar(4000)
	
	
	SELECT	@json=json, @eventType = event_type, @groupProcessInstanceId = group_process_id
	FROM	dbo.sparx_incoming
	where	id=@sparx_id

	if @eventType = dbo.f_translate('cancel',default) and not exists (select id from dbo.process_instance where group_process_id = @groupProcessInstanceId and step_id = '1011.053' and active = 1)
	BEGIN
		-- create cancel services info step
		EXECUTE dbo.p_process_new
		   @stepId='1011.053'
		  ,@userId=1
		  ,@groupProcessId = @groupProcessInstanceId
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT
		  ,@processInstanceId=@processInstanceId OUTPUT
		  
	    DECLARE @servicesIds NVARCHAR(255)

		EXEC [dbo].[p_running_services]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@servicesIds = @servicesIds OUTPUT
		
		IF ISNULL(@servicesIds,'') = ''
		BEGIN	
		   EXEC [dbo].[p_process_next]
	       @previousProcessId = @processInstanceId,
	       @err = @err OUTPUT,
	       @message = @message OUTPUT,
	       @processInstanceIds = @processInstanceIds OUTPUT
		END 
	    
		return 
		  
	END 
	
	declare @sql nvarchar(max)
	declare @id int
	declare @json_key nvarchar(200)
	declare @json_type nvarchar(100)
	declare @attribute_path nvarchar(500)

	-- create process

	-- send back ref case id
	EXECUTE dbo.p_process_new
	   @stepId='1011.50'
	  ,@userId=1
	  ,@err=@err OUTPUT
	  ,@message=@message OUTPUT
	  ,@processInstanceId=@processInstanceId OUTPUT

	  EXECUTE dbo.p_form_controls 
	   @instance_id=@processInstanceId
	  ,@returnResults=0
	  ,@returnWithNote=0
	  
 	EXEC dbo.p_note_new
	@groupProcessId = @processInstanceId,
	@type = dbo.f_translate('system',default),
	@content = @json,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	

 	EXEC [dbo].[p_process_next]
     @previousProcessId = @processInstanceId,
     @err = @err OUTPUT,
     @message = @message OUTPUT,
     @processInstanceIds = @processInstanceIds OUTPUT  
	  
    SET @processInstanceId = CAST(@processInstanceIds AS INT)
    
--	EXECUTE dbo.p_process_new
--	   @stepId='1011.51'
--	  ,@userId=1
--	  ,@err=@err OUTPUT
--	  ,@message=@message OUTPUT
--	  ,@processInstanceId=@processInstanceId OUTPUT
--
 
	EXECUTE dbo.p_form_controls 
	   @instance_id=@processInstanceId
	  ,@returnResults=0
	  ,@returnWithNote=0

	

	select @groupProcessInstanceId=group_process_id
	from dbo.process_instance 
	where id=@processInstanceId

	declare @result as table(keyX nvarchar(100),valueX nvarchar(300))
		
	declare @attributeValueId int
	set  @attributeValueId=null

	EXECUTE dbo.p_attribute_edit 
		@attributePath='713'
		,@groupProcessInstanceId=@groupProcessInstanceId
		,@stepId='xxx'
		,@userId=1
		,@attributeValueId=@attributeValueId OUTPUT
		,@valueint=0
		,@err=@err OUTPUT
		,@message=@message OUTPUT
	
	EXEC p_attribute_edit
		@attributePath = '101,102' , 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 3,
		@err = @err OUTPUT,
		@message = @message OUTPUT	

	declare kur cursor LOCAL for
			select	id,
					json_key,
					json_type,
					attribute_path
			from    dbo.sparx_json_definition
			where   type=dbo.f_translate('incomingNewCase',default) and 
					attribute_path is not null
		OPEN kur;
		FETCH NEXT FROM kur INTO @id, @json_key, @json_type, @attribute_path;
		WHILE @@FETCH_STATUS=0
		BEGIN
			set  @attributeValueId=null

			declare @valueX nvarchar(300)
			set @valueX=null

			delete from @result

			set @sql=dbo.f_translate('SELECT ',default)''+@json_key+''',valueX FROM OPENJSON (@json) WITH(valueX '+@json_type+' ''$.'+@json_key+''' )' 
		


	--	print @sql
			insert into @result
			EXECUTE sp_executesql @sql, N'@json NVARCHAR(MAX)',@json=@json 

			select @valueX=valueX
			from @result

			if @attribute_path='80,342,408,197'
			begin
				set @valueX=replace(@valueX,'+','00')
			end
			else if @attribute_path = '491'
			BEGIN
				IF @valueX = dbo.f_translate('Breakdown',default)
				BEGIN
					SET @valueX = '2'
				END 
				ELSE IF @valueX = dbo.f_translate('Accident',default)
				BEGIN
					set @valueX = '1'
				END 
			END 

			declare @type int
			set @type=null

			select	@type=type
			from	dbo.attribute 
			where id=(
				select top 1 data from dbo.f_split (@attribute_path,',') order by id desc
			)

			declare @valueInt int
			declare @valueDate datetime
			declare @valueString nvarchar(200)
			declare @valueText nvarchar(500)
			declare @valueDecimal decimal(18,6)

			set @valueInt=null
			set @valueDate=null
			set @valueString=null
			set @valueText=null
			set @valueDecimal=null

			if @type=1 -- 1 string
			begin
				set @valueString=@valueX
			end
		
			if @type in (2,6) -- 2	integer -- 6	boolean
			begin
				set @valueInt=try_parse(@valueX as int)
			end
		
			if @type=3 -- 3	datetime
			begin
				set @valueDate=try_parse(@valueX as datetime)
			end
		
			if @type=4 -- 4	text
			begin
				set @valueText=@valueX
			end
		
			if @type=5 -- 5	decimal
			begin
				set @valueDecimal=try_parse(@valueX as decimal(18,6))
			end
		
			--select * from dbo.dictionary where typeD='attributeType'
	
			EXECUTE dbo.p_attribute_edit 
			   @attributePath=@attribute_path
			  ,@groupProcessInstanceId=@groupProcessInstanceId
			  ,@stepId='xxx'
			  ,@userId=1
			  ,@attributeValueId=@attributeValueId OUTPUT
			  ,@valueInt=@valueInt
			  ,@valueDate=@valueDate
			  ,@valueString=@valueString
			  ,@valueText=@valueText
			  ,@valueDecimal=@valueDecimal
			  ,@err=@err OUTPUT
			  ,@message=@message OUTPUT
	
			FETCH NEXT FROM kur INTO @id, @json_key, @json_type, @attribute_path;
		END
		CLOSE kur
		DEALLOCATE kur

		DECLARE @IntVariable int;  
	DECLARE @SQLString nvarchar(500);  
	DECLARE @ParmDefinition nvarchar(500);  

	DECLARE @make NVARCHAR(100)
	DECLARE @model NVARCHAR(100)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,766', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @make = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,576', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @model = value_string FROM @values
	
	DECLARE @makeModel NVARCHAR(300)
	SELECT @makeModel = @make + ' ' + @model
	
	IF ISNULL(@makeModel,'') <> ''
	BEGIN		
		DECLARE @makeModelId INT
		SELECT @makeModelId = dbo.f_makeModelStandard(@makeModel,1)
		
		EXEC p_attribute_edit
		@attributePath = '74,73' , 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = @makeModelId,
		@err = @err OUTPUT,
		@message = @message OUTPUT		
	END 
	
	IF @eventType = dbo.f_translate('create',default)
	BEGIN
		UPDATE dbo.sparx_incoming set group_process_id = @groupProcessInstanceId where id = @sparx_id
	END 
end

