
ALTER PROCEDURE [dls].[P_SendEmail]
@To varchar(1000),
@CC varchar(1000),
@BCC varchar(1000),
@subject nvarchar(100),
@Body1 nvarchar(max),
@Body2 nvarchar(max),
@Body3 nvarchar(max),
@Body4 nvarchar(max),
@type int,
@attachment1 varchar(200),
@attachment2 int
AS

SET NOCOUNT ON

	declare @file_attachments nvarchar(4000)
	set @file_attachments=''
	declare @attachmentId int

	declare kur cursor LOCAL for

		select data
		from dbo.f_split(@attachment1,',')
		UNION
		Select @attachment2

	OPEN kur;
	FETCH NEXT FROM kur INTO @attachmentId;
	WHILE @@FETCH_STATUS=0
	BEGIN
		
		select	@file_attachments=@file_attachments+'\\10.10.77.145\udzial'+f.path+';'
		from	dbo.files f
		where	f.document_id=@attachmentId

		--declare @cmdstring varchar(1000)

		--set @cmdstring = 'copy\\atlas.starter24.pl\udzial\59\35\59354416262da.pdf F:\Temp\'
		--exec master..xp_cmdshell @cmdstring 

		FETCH NEXT FROM kur INTO @attachmentId;
	END
	CLOSE kur
	DEALLOCATE kur

	if len(@file_attachments)>1 
	begin
		set @file_attachments=left(@file_attachments,len(@file_attachments)-1)
	end

	set @file_attachments=replace(@file_attachments,'/','\')
	--set @file_attachments='C:\clu\log\elxflash.log'

	declare @body nvarchar(max)
	if @type=1 set @body=@Body1
	if @type=2 set @body=@Body2
	if @type=3 set @body=@Body3
	if @type=4 set @body=@Body4

	set @body=replace(@body,',',', ')

	set @bcc=isnull(@bcc,'')+';szkodyarchiwum@starter24.pl;szkody@starter24.pl'

	select @file_attachments
	EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('Regresy',default)
			,                            @recipients  = @to
			,                            @copy_recipients  = @cc
			,                            @blind_copy_recipients  = @bcc
			,                            @subject     = @subject
			,                            @body        = @body
			,	                         @body_format = dbo.f_translate('HTML',default)	
			,							 @file_attachments=@file_attachments
