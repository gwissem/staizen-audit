ALTER FUNCTION [dbo].[f_mapReverseGeocoding](@lat decimal(9,6),@lng decimal(9,6))
RETURNS varchar(3000) AS  
BEGIN 
	DECLARE @URL VARCHAR(8000) 
	SET @URL = 'https://maps.googleapis.com/maps/api/distancematrix/xml?latlng='+@lat+','+@lng+'&key=AIzaSyDMUZXCjq_3Y6ZHHLhapOrU4LcpwjZVPpY'

	DECLARE @Response varchar(8000)
	DECLARE @XML xml
	DECLARE @Obj int 
	DECLARE @Result int 
	DECLARE @HTTPStatus int 
	
	EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
	EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
	EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
	EXEC sp_OAMethod @Obj, send, NULL, ''
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseXML.xml',default), @Response OUT 

	declare @responseXML XML
	set @responseXML=cast(@response as XML)
	set @result=@XML.query('/GeocodeResponse/result/formatted_address').value('.','nvarchar(300)')

	return @result
END

