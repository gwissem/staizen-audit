ALTER PROCEDURE [dbo].[s_1202_002]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, 
    @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @processInstanceIds varchar(4000)
	DECLARE @rootId INT
	DECLARE @decision INT 
	DECLARE @customContent NVARCHAR(max)
	DECLARE @programId int 
	DECLARE @vin nvarchar(255)
	DECLARE @subject nvarchar(255)
	DECLARE @body nvarchar(max)
	DECLARE @email nvarchar(255)
	DECLARE @senderEmail nvarchar(255)
	DECLARE @dw nvarchar(255)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance with(nolock)
	WHERE id = @previousProcessId 
	
	delete from @values	
	insert into @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	delete from @values	
	insert into @values EXEC dbo.p_attribute_get2 @attributePath = '59,60', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @decision = value_int FROM @values
	
	IF @decision = 1 AND @programId IN ('590','591')
	BEGIN
		
		DECLARE @servicesIds nvarchar(255)
		DECLARE @relatedService nvarchar(100)
		
		EXEC [dbo].[p_running_services]
		@groupProcessInstanceId = @rootId,
		@servicesIds = @servicesIds OUTPUT
		
		SET @relatedService = ''
		IF @servicesIds LIKE '%,3,%'
		BEGIN
			SET @relatedService = dbo.f_translate('pojazdu zastępczego',default)	
		END 
		IF @servicesIds LIKE '%,4,%'
		BEGIN
			SET @relatedService = isnull(' i '+@relatedService,'')+dbo.f_translate('noclegu',default)	
		END 
		IF @servicesIds LIKE '%,6,%'
		BEGIN
			SET @relatedService = isnull(' i '+@relatedService,'')+dbo.f_translate('podróży',default)
		END 
		
		SELECT TOP 1 @vin = value_string from dbo.attribute_value where group_process_instance_id = @rootId and attribute_path = '74,71'
		SET @subject = dbo.f_caseId(@rootId)+ISNULL(', '+@vin,'')+dbo.f_translate(', Powiadomienie o konieczności połączenia świadczeń',default)
		
		SET @body = 'Szanowni Państwo,<br>Użytkownik przedmiotowego pojazdu zgłosił prośbę o połączenie świadczeń '+ISNULL(@relatedService,'')+'. Prosimy o zwrotne potwierdzenie zgody na zorganizowanie obu usług w sprawie.<br><br>
					Nr sprawy: {#caseid#}<br>
					Nr VIN: {@74,71@}<br>
					Marka i model pojazdu: {@74,73@}<br>
					Nazwa serwisu: {#workshopName()#}<br>'		
		
		delete from @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '59,737', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @body = @body+isnull('Uzasadnienie: '+value_text,'') FROM @values
		
		SET @body = @body + '<br><br>Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres kz@starter24.pl'
		
		EXEC [dbo].[P_get_body_email]
			@contentEmail = @body,
			@title = @subject,
			@previousProcessId = @groupProcessInstanceId,
			@body = @body OUTPUT
		
					
		SELECT @email = dbo.f_getRealEmailOrTest('lukasz.chorylo@opel.com')
		SELECT @dw =  dbo.f_getRealEmailOrTest('waldemar.szajkowski@opel.com,')+', '+dbo.f_getRealEmailOrTest('kz@starter24.pl')
		SELECT @senderEmail = dbo.f_getRealEmailOrTest('callcenter@starter24.pl')
  		
		EXECUTE dbo.p_note_new
        @sender = @senderEmail
        , @groupProcessId = @groupProcessInstanceId
        , @type = dbo.f_translate('email',default)
        , @content = dbo.f_translate('Powiadomienie o łączeniu świadczeń',default)        
        , @email = @email		
        , @dw = @dw
        , @userId = 1
        , @direction = 1
        , @subject = @subject        
        , @emailBody = @body
        , @err = @err OUTPUT
        , @message = @message OUTPUT
		
        EXEC [dbo].[p_jump_to_service_panel]
		@previousProcessId = @previousProcessId,
		@variant = 1
		
	END 
	ELSE
	BEGIN
		EXEC [dbo].[p_add_service_status]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@status = -1,
		@serviceId = 3
		
		EXEC [dbo].[p_jump_to_service_panel]
		@previousProcessId = @previousProcessId
		
		
	END 
	
	

END


