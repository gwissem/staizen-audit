ALTER PROCEDURE [dbo].[s_1018_036]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @processInstanceId INT
	DECLARE @czyPrzyjalZlecenie TINYINT
	DECLARE @partnerPhone NVARCHAR(20)
	DECLARE @partnerId INT
	DECLARE @rootId INT


	-- Pobranie GroupProcessInstanceId --
	SELECT	@groupProcessInstanceId = group_process_id,
		@rootId = root_id,
			@stepId = step_id
	FROM process_instance 
	WHERE id = @previousProcessId 
	
	-- Stworzenie pomocniczej tabelki
	DECLARE @Values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '670', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @czyPrzyjalZlecenie = value_int FROM @values
	
	-- Może wejść z @variant = 2, a to oznacza, że nie odebrał
		
	IF ISNULL(@czyPrzyjalZlecenie,0) = 1 AND isnull(@variant,1) = 1
	BEGIN
		-- Kontraktor przyjął zlecenie 
		
		DECLARE @token NVARCHAR(36)
		DECLARE @urlForm NVARCHAR(100)
		DECLARE @haveICS TINYINT
		DECLARE @fixingOrTowing INT
		DECLARE @onTime TINYINT
		DECLARE @content NVARCHAR(MAX)
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '691', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerPhone = value_string FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerId = value_int FROM @values
		
		declare @distance decimal(18,2)
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '647', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @distance = value_decimal FROM @values
		
		declare @dmc int
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,76', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @dmc = value_int FROM @values
		
		declare @platformId int
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @platformId = value_int FROM @values
		
		declare @transportType int
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '62,867', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @transportType = value_int FROM @values
		

		set @variant=1

		-- cennik
		if @transportType=10
		begin
			declare @price int

			if @distance<45 
			begin
				if @dmc<=2599
				begin
					set @price=120
				end
				else if @dmc<=3399
				begin
					set @price=155
				end
				else if @dmc<=5699
				begin
					set @price=200
				end
				else
				begin
					set @price=120
				end
			end
			else
			begin
				if @dmc<=2599
				begin
					set @price=120+4*(@distance-45.00)
				end
				else if @dmc<=3399
				begin
					set @price=155+5*(@distance-45.00)
				end
				else if @dmc<=5699
				begin
					set @price=200+6*(@distance-45.00)
				end
				else 
				begin
					set @price=120+4*(@distance-45.00)
				end
			end
			--select @price



			EXEC p_attribute_edit
				@attributePath = '62,319', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueInt = @price,
				@err = @err OUTPUT,
				@message = @message OUTPUT
		
		end

		declare @priceLimit int

		if @platformId = 31
			begin
				set @priceLimit = dbo.FN_Exchange(500, dbo.f_translate('EUR',default), getdate())
			end
		else if @platformId in (6, 11)
			begin
				set @priceLimit = dbo.FN_Exchange(400, dbo.f_translate('EUR',default), getdate())
			end
		else if @platformId = 32
			begin
				set @priceLimit = 3500
			end
		else
			begin
				set @priceLimit = 9999
			end

		--	select * from dbo.platform where name like '%kia'

		EXEC p_attribute_edit
				@attributePath = '62,249',
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueInt = @priceLimit,
				@err = @err OUTPUT,
				@message = @message OUTPUT

		DECLARE @serviceId int

	SELECT top 1 @serviceId = serviceId from service_status with (nolock )
	where group_process_id = @groupProcessInstanceId


		IF isnull(@serviceId,0) = 7
			BEGIN

				DECLARE @platformName nvarchar(500)

				SELECT @platformName = name from platform with (nolock) where id = @platformId

				DECLARE @programId int
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @programId = cast(value_string as int) FROM @values


				DECLARE @programName nvarchar(500)
				SELECT @programName = name FROM vin_program with (nolock) where id = @programId
				DECLARE @startLocation nvarchar(500)

				DECLARE @endLocation nvarchar(500)

				EXEC p_location_string @attributePath = '62,591,85',
															 @groupProcessInstanceId = @groupProcessInstanceId,
															 @locationString = @endLocation OUTPUT

				DECLARE @ServicePartnerId int

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @rootId
				SELECT @ServicePartnerId = value_int FROM @values

				SELECT @startLocation = [dbo].[f_partnerAdress](@ServicePartnerId)

				DECLARE @smsContent NVARCHAR(2000)
				= dbo.f_translate('Tu Starter24. Pełne informacje do zlecenia transportu naprawionego pojazdu nr ',default) +
					isnull(dbo.f_caseId(@groupProcessInstanceId), '') + '.' +
					dbo.f_translate(' Platforma/program ',default) + isnull(@platformName, '') + ' / ' + isnull(@programName, '') + '.' +
					dbo.f_translate(' Transport z ',default) + isnull(@startLocation, '-') + ' do ' + isnull(@endLocation, '-') + '.'

				EXEC dbo.p_note_new
						@groupProcessId = @groupProcessInstanceId
						, @type = dbo.f_translate('sms',default)
						, @content = @smsContent
						, @phoneNumber = @partnerPhone
						, @userId = 1  -- automat
						, @direction = 1
						, @err = @err OUTPUT
						, @message = @message OUTPUT

			end

	END 
	ELSE
	BEGIN
		
		-- ZAPISANIE POWODU ODMOWY
		
		DECLARE @reasonId INT
		DECLARE @reasonMultiId INT
	    DECLARE @reasonPartnerId INT
	    DECLARE @reasonExists SMALLINT
	    DECLARE @parentReasonId INT
	    
		IF @variant = 2 
		BEGIN
			-- Gdy kontraktor nie odbierze
			SET @reasonId = 0	
		END
		ELSE
		BEGIN
			
			DELETE FROM @values
		    INSERT @values EXEC p_attribute_get2 @attributePath = '700', @groupProcessInstanceId = @groupProcessInstanceId
		    SELECT @reasonId = value_int FROM @values
			
		END 
	    
	    DELETE FROM @values
	    INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
	    SELECT @reasonPartnerId = value_int FROM @values
	    
	    EXEC p_add_service_refuse_reason
		@groupProcessInstanceId = @groupProcessInstanceId,
		@partnerLocationId = @reasonPartnerId,
		@reasonId = @reasonId
	    
			
		EXEC p_attribute_edit
			@attributePath = '670', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
		EXEC p_attribute_edit
			@attributePath = '610', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
		EXEC p_attribute_edit
			@attributePath = '691', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueString = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
		EXEC p_attribute_edit
			@attributePath = '647', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueDecimal = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
			
		-- ZRESETOWANIE POWODU 		
		EXEC p_attribute_edit
			@attributePath = '700', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
				
		SET @variant = 2
		
	END 
	
	PRINT '------------------------------- END ---------------------------------'
END