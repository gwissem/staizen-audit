ALTER PROCEDURE [dbo].[p_OCLP_updateServices]
AS

SET NOCOUNT ON
declare @table as table(

id int,
email nvarchar(1000),
shortName nvarchar(200),
code nvarchar(200),
address nvarchar(100),
city nvarchar(100),
latitude decimal(18,6),
longitude decimal(18,6),
soGroup int
)

insert into @table
 select * from openquery(sette,'SELECT
k.Id
,StarterOperations_Stage.dbo.KontraktorLokalizacjaMaile(kl.Id) email
,k.KrotkaNazwa
,k.kodstarter
,l.miejscowosc+'', ''+isnull(l.ulica,'''')+'' ''+isnull(numerposesji,'''') adres
,l.miejscowosc
,l.Szerokosc
,l.Dlugosc
,ktg.grupakontraktorow_id
from StarterOperations_Stage.dbo.Kontraktor k WITH(NOLOCK)
join StarterOperations_Stage.dbo.KontraktorToGrupa ktg WITH(NOLOCK) on ktg.Kontraktor_id = k.Id and k.Archiwalny = 0 and
ktg.Archiwalny = 0 and ktg.GrupaKontraktorow_id in (282,200,199) 
INNER JOIN StarterOperations_Stage.dbo.KontraktorLokalizacja kl WITH (NOLOCK) ON kl.Kontraktor_id = k.id and kl.LokalizacjaTyp=0
INNER JOIN StarterOperations_Stage.dbo.lokalizacja l WITH (NOLOCK) ON l.id = kl.lokalizacja_id and  l.archiwalny=0

UNION

 
select
k.Id
,StarterOperations_Stage.[dbo].[KontraktorLokalizacjaMaile](kl.Id) email
,k.KrotkaNazwa
,k.kodstarter
,l.miejscowosc+'', ''+isnull(l.ulica,'''')+'' ''+isnull(numerposesji,'''') adres
,l.miejscowosc
,l.Szerokosc
,l.Dlugosc
,999 
from  StarterOperations_Stage.dbo.Kontraktor k
INNER JOIN StarterOperations_Stage.dbo.KontraktorLokalizacja kl WITH (NOLOCK) ON kl.Kontraktor_id = k.id and kl.LokalizacjaTyp=0
INNER JOIN StarterOperations_Stage.dbo.lokalizacja l WITH (NOLOCK) ON l.id = kl.lokalizacja_id and  l.archiwalny=0
             where  left(KodStarter,2)
             in (''MW'') and
                       left(KrotkaNazwa,5) in (''99 Re'','dbo.f_translate('Avis',default)','dbo.f_translate('Renti',default)','dbo.f_translate('Panek',default)')
 ') so


INSERT INTO dbo.services
           ([id]
           ,[email]
           ,[shortName]
           ,[code]
		   ,[address]
           ,[city]
           ,[latitude]
           ,[longitude]
           ,[type]
           ,[priority])
select  so.id,
		so.email,
		so.shortName + case when so.soGroup=282 then dbo.f_translate(' MIF',default) when so.soGroup in (200,199) then dbo.f_translate('BRS',default) else '' end shortName,
		so.code,
		so.address,
		so.city,
		so.latitude,
		so.longitude,
		case 
		when so.soGroup=999 then 1
		else 2 end type,
		case when so.soGroup=999 then
			case 
				when so.shortname like '99Rent%' then 1
				when so.shortname like 'Rentis%' then 2
				when so.shortname like 'Avis%' then 3
				when so.shortname like 'Panek%' then 4
			else null end 
		else null end

from	@table so left join 
		dbo.services s on s.id=so.id
WHERE	s.id is null  
		
UPDATE s
SET  s.email=case 
				when so.shortname like '99 Rent%' then 'assistance@99rent.pl'
				else so.email end,
		s.shortName=so.shortName + case when so.soGroup=282 then dbo.f_translate(' MIF',default) when so.soGroup in (200,199) then dbo.f_translate('BRS',default) else '' end ,
		s.code=so.code,
		s.address=so.address,
		s.city=so.city,
		s.latitude=so.latitude,
		s.longitude=so.longitude,
		s.type=case 
		when so.soGroup=999 then 1
		else 2 end,
		s.priority=case when so.soGroup=999 then
			case 
				when so.shortname like '99Rent%' then 1
				when so.shortname like 'Rentis%' then 2
				when so.shortname like 'Avis%' then 3
				when so.shortname like 'Panek%' then 4
			else null end 
		else null end
from	@table so inner join 
		dbo.services s on s.id=so.id
		


UPDATE dbo.services
set type=-1
where id not in (

select id from @table

) 
