ALTER PROCEDURE [dbo].[p_current_user_in_process]
as
/* 2018-04-20 Maciej Grzelak
	procedura zwraca tabele aktualnych uzytkownikow w sprawach (dbo.user_current_process)

*/
begin
declare @URL varchar(8000)
set @URL = dbo.f_getDomain() + dbo.f_translate('/api-task/active/all.string',default)

delete from dbo.user_current_process


DECLARE @Response varchar(8000)
DECLARE @Obj int 
DECLARE @HTTPStatus int 
DECLARE @Text as table ( answer nvarchar(4000) )

EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
EXEC sp_OAMethod @Obj, send, NULL, ''
EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 

INSERT @Text
EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseText',default)
EXEC sp_OADestroy @Obj


declare @string varchar(8000)
set @string = (select answer from @Text)
declare @s varchar(1000)
declare @Tab1 table (value nvarchar(1000))

insert into @Tab1
select data from dbo.f_split(@string,'&')

 insert into dbo.user_current_process
 Select
	main.process_instance_id
	,main.userId
	,dateadd(HOUR,isnull(DATEDIFF(HOUR,GETUTCDATE(),getdate()),0),dateadd(s,(isnull(cast(main.timestamps as bigint),0)),'1970-01-01')) as data_wejscia
	,main.root_id
	,getdate() as log_date
	from
		(Select Substring(value,0,CHARINDEX('|',value)) as process_instance_id
		,Substring((right(value,len(value) - len(Substring(value,0,CHARINDEX('|',value)))-1 )),0,CHARINDEX('|',(right(value,len(value) - len(Substring(value,0,CHARINDEX('|',value)))-1 )))) userId
		,REVERSE(SUBSTRING(reverse(left(value,len(value)-len(REVERSE(SUBSTRING(reverse(value),0,CHARINDEX ('|',REVERSE(value)))))-1)),0,CHARINDEX ('|',REVERSE(left(value,len(value)-len(REVERSE(SUBSTRING(reverse(value),0,CHARINDEX ('|',REVERSE(value)))))-1))))) as timestamps
		,REVERSE(SUBSTRING(reverse(value),0,CHARINDEX ('|',REVERSE(value)))) as root_id
		from @Tab1
	)main
 
 return
end


