ALTER PROCEDURE [dbo].[s_1012_024]
  (
    @previousProcessId INT,
    @variant           TINYINT OUTPUT, @currentUser int, @errId int = 0 output
  )
AS
  BEGIN

    DECLARE @err INT
    DECLARE @userId INT
    DECLARE @originalUserId INT
    DECLARE @message NVARCHAR(255)
    DECLARE @newProcessInstanceId INT
    DECLARE @groupProcessInstanceId INT
    DECLARE @processInstanceIds varchar(4000)
    declare @doNext int = 1

    SELECT @groupProcessInstanceId = group_process_id, @userId = created_by_id, @originalUserId = created_by_original_id
    FROM process_instance with(nolock)
    WHERE id = @previousProcessId








    DECLARE @applicationName NVARCHAR(30)
    DECLARE @applicationType int


    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))
    INSERT @values EXEC p_attribute_get2 @attributePath = '1084', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @applicationType = value_int FROM @values

    SELECT @applicationName = IIF(@applicationType = 1 , dbo.f_translate('eDealer',default), dbo.f_translate('eBrooker',default))


    DECLARE @shouldSendMail int
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1085', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @shouldSendMail = value_int from @values

    DECLARE @clientFullName NVARCHAR(200)

    DECLARE @firstName nvarchar(100)
    DECLARE @lastName nvarchar(100)

    DELETE @values
    INSERT @values EXEC p_attribute_get2  @attributePath = '80,342,64', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @firstName = value_string from @values


    DELETE @values
    INSERT @values EXEC p_attribute_get2  @attributePath = '80,342,66', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @lastName = value_string from @values

if isnull(@firstName,'') = ''
  BEGIN
    DELETE @values
    INSERT @values EXEC p_attribute_get2  @attributePath = '81,342,64', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @firstName = value_string from @values


    DELETE @values
    INSERT @values EXEC p_attribute_get2  @attributePath = '81,342,66', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @lastName = value_string from @values
  end
    if isnull(@firstName,'') = ''
      BEGIN
        DELETE @values
        INSERT @values EXEC p_attribute_get2  @attributePath = '418,342,64', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @firstName = value_string from @values


        DELETE @values
        INSERT @values EXEC p_attribute_get2  @attributePath = '418,342,66', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @lastName = value_string from @values
      end

    SELECT @clientFullName = isnull(@firstName,'')+ ' ' + isnull(@lastName,'')

    IF isnull(@shouldSendMail, 0) = 1
      BEGIN
        DECLARE @content nvarchar(max)
        DECLARE @mailAddress nvarchar(300)


        SELECT @mailAddress = dbo.f_getRealEmailOrTest('krzysztof.leszczynski@alphabet.pl')
        
        SET @content = 'Szanowni Państwo, <br><br>

Skontaktował się z nami użytkownik aplikacji ' + isnull(@applicationName, '') + dbo.f_translate(' - Pan/Pani ',default) +
                       isnull(@clientFullName, '') + dbo.f_translate(' z prośbą o odblokowanie dostępu. ',default)

DECLARE @senderMail nvarchar(100)
        SELECT @senderMail = dbo.f_getEmail('cfm')

        DECLARE @subject nvarchar(200)
        SELECT @subject = dbo.f_translate('Problem z logowaniem do aplikacji eDealer/eBroker. Numer zgłoszenia ',default) + isnull(dbo.f_caseId(@groupProcessInstanceId),'')+'.'



        EXEC p_note_new
            @groupProcessId = @groupProcessInstanceId,
            @type = dbo.f_translate('email',default),
            @content = @content,
            @phoneNumber = NULL,
            @email = @mailAddress,
            @subject = @subject,
            @direction = 1,
            @emailRegards = 1,
            @err = @err OUTPUT,
            @message = @message OUTPUT,
            -- FOR EMAIL
            @dw = '',
            @udw = '',
            @sender = @senderMail,
            @additionalAttachments = '',
            @emailBody = @content
      end


    SET @variant = 1
  END