
ALTER PROCEDURE [dbo].[p_entity_export] @rootId INT
AS
BEGIN

BEGIN TRANSACTION
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED --> OD 2019-07-29 9:27 ZAMIAST WITH(NOLOCK)
SET NOCOUNT ON

DECLARE @groupProcessId INT
DECLARE @serviceId INT
DECLARE @entityId INT
DECLARE @manufacturerQuantity DECIMAL(20,4)
DECLARE @insurancerQuantity DECIMAL(20,4)
DECLARE @partnerQuantity DECIMAL(20,4)
DECLARE @dealerQuantity DECIMAL(20,4)
DECLARE @clientQuantity DECIMAL(20,4)
DECLARE @partnerPath NVARCHAR(255)
DECLARE @country NVARCHAR(255) = (SELECT TOP 1 value_string FROM attribute_value WITH(NOLOCK) WHERE root_process_instance_id=@rootId AND attribute_path='101,85,86' AND value_string IS NOT NULL)
DECLARE @countryCode NVARCHAR(3) = (SELECT TOP 1 argument1 FROM dbo.dictionary WITH(NOLOCK) WHERE typeD='country' AND textD=@country)
DECLARE @partnerId INT
DECLARE @platformName NVARCHAR(255)
DECLARE @zzId INT
DECLARE @gidTypeZZ SMALLINT
DECLARE @gidTypeZS SMALLINT
DECLARE @finishedAt DATETIME
DECLARE @caseId NVARCHAR(255) = 'A' + RIGHT('00000000' + CAST(@rootId AS NVARCHAR(10)), 8)
DECLARE @partnerStarterCode NVARCHAR(255)
DECLARE @platformId INT
DECLARE @programId INT
DECLARE @programName NVARCHAR(255)
DECLARE @type INT
DECLARE @headerType NVARCHAR(2)
DECLARE @serieZS varchar(5)
DECLARE @serieZZ varchar(5)
DECLARE @description NVARCHAR(255)
DECLARE @descriptionTmp NVARCHAR(255)
DECLARE @starterCode NVARCHAR(255)
DECLARE @quantity DECIMAL(20,4)
DECLARE @headerZS INT
DECLARE @headerZZ INT
DECLARE @vat INT
DECLARE @nettoBrutto VARCHAR(1)
DECLARE @discount DECIMAL(20,4)
DECLARE @vatGroup NVARCHAR(2)
DECLARE @twrCode NVARCHAR(100)
DECLARE @value DECIMAL(20,4)
DECLARE @currency nvarchar(3)
DECLARE @casePayment bit = 0
DECLARE @entityValueId int
DECLARE @akronimZSADH nvarchar(50)
DECLARE @additTownigADH int = 0
DECLARE @quantityZ DECIMAL(20,4)
DECLARE @onlyEmptyService int = 0
DECLARE @akronimTarget nvarchar(50)
DECLARE @quantityXL int
DECLARE @descrTable as table (idHeader int, descr nvarchar(100)) 
DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
DECLARE @caseSource INT = (SELECT TOP 1 value_int FROM attribute_value WITH(NOLOCK) WHERE root_process_instance_id=@rootId AND attribute_path='101,102' AND value_int IS NOT NULL) --> Źródło zgłoszenia
DECLARE @policyStartDate DATE = (SELECT TOP 1 value_date FROM attribute_value WITH(NOLOCK)WHERE root_process_instance_id=@rootId AND attribute_path='981,67' AND value_date IS NOT NULL) --> Data początku polisy, polisa od

DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
SELECT @platformId = value_int FROM @values	
SELECT @platformName = name FROM dbo.platform with(nolock) WHERE id = @platformId

print '#######################################################'
print dbo.f_translate('Platforma: ',default) + @platformName

/* Wyjątek CFM oraz VGP oraz Naprawa na drodze, przenoszenie po zakończeniu.
	25	LEASEPLAN
	48	Bussiness Lease
	53	ALPHABET
	43	ALD
	78  Carefleet */
IF @platformId IN (25,48,53,43,78) --> CFM
BEGIN
	DECLARE @previousProcessId INT = (SELECT TOP 1 id FROM process_instance WHERE root_id=@rootId)

	DECLARE @activeServices Table (
		id INT,
		group_process_id INT,
		serviceId INT,
		status_dictionary_id int,
		status_label NVARCHAR(255),
		status_progress INT,
		icon NVARCHAR(MAX),
		service_name NVARCHAR(255)
	)
	INSERT @activeServices EXEC AtlasDB_v.[dbo].[p_get_status_active_services] @processInstanceId=@previousProcessId,@onlyActive=1,@showAll=1
	DECLARE @activeServicesCount INT = ISNULL((SELECT COUNT(1) FROM @activeServices),0) --> Liczba aktywnych usług

	DECLARE @allServices Table (
		id INT,
		group_process_id INT,
		serviceId INT,
		status_dictionary_id int,
		status_label NVARCHAR(255),
		status_progress INT,
		icon NVARCHAR(MAX),
		service_name NVARCHAR(255)
	)
	INSERT @allServices EXEC AtlasDB_v.[dbo].[p_get_status_active_services] @processInstanceId=@previousProcessId,@onlyActive=0,@showAll=1
	DECLARE @allServicesCount INT = ISNULL((SELECT COUNT(1) FROM @allServices),0) --> Liczba wszystkich usług
END

delete from @values
insert into @values 
EXEC	dbo.p_attribute_get2
		@attributePath = '74,73',
		@groupProcessInstanceId = @rootId
declare @customerCarId int = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)

--SELECT @description = ISNULL(@programName,'')
--SELECT @caseId = dbo.f_caseId(@rootId)
DECLARE @caseDate DATETIME = (select top 1 created_at from dbo.process_instance with(nolock)  where root_id=@rootId order by id)

/******************************/

declare @name nvarchar(100)

SET @partnerPath = '610'
IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) <> dbo.f_translate(dbo.f_translate('Polska',default),default)
BEGIN
	SET @partnerPath = '741'
END 

-- ============================================================= --
--		    CASE FLAGS • Flagi do zamówień dla sprawy			 --
-- ============================================================= --
DECLARE @AllTowingKmInOneOrder BIT = 1 -- Wszystkie kilometry holowania na jednym zamówieniu dla programów ARC,AA,TCB,ANWB.
DECLARE @IsMedicalInCase BIT = 0 -- Czy w sprawie znajduje się usługa medical, jeśli tak to będzie opłata za sprawę.
DECLARE @IsAdviceInCase BIT = 0 -- Czy w sprawie znajduje się porada.
DECLARE @KIAonceNHpriceZS BIT = 1 -- Dla programów KIA jest pobierany jeden ryczałt za usługi NH.
DECLARE @POCZTOEonceNHpriceZS BIT = 1 -- Dla programów TUW POCZTOWY jest pobierany raz ryczałt za usługi NH.
DECLARE @isTransport BIT = 0 -- Dla ALD, Czy w sprawie istnieje transport, jeśli tak to opłata za sprawę.
DECLARE @isTowing BIT = 0 -- Dla BL opłata za sprawę, gdy jest holowanie (pojazd wymagający holowania).
DECLARE @isTowingPSA BIT = 0 --> Dla PSA jest inny ryczały dla drugiego holowania.
DECLARE @isRepairByPhone BIT = 0 --> Dla PSA jest inny ryczały dla drugiego holowania.
DECLARE @isAdHocPSA BIT = 0 --> Dla PSA, ustawienie typu opłaty za sprawę, gdzy są programy AD hoc.
DECLARE @statusBWB INT = ( --> Status braku w bazie. 1) Jest w bazie, 0 i -1 brak w bazie.
	SELECT TOP 1 value_int FROM attribute_value WITH(NOLOCK) WHERE attribute_path='573' AND root_process_instance_id=@rootId AND value_int IS NOT NULL)
DECLARE @isAlphabetAdviceOnce BIT = 0 --> Porada dla Alphabet, tylko jeśli występuje bez innych usług.
DECLARE @isLeaseplanAdviceOnce BIT = 0 --> Porada dla Leaseplan, tylko jeśli występuje bez innych usług.
DECLARE @caseMainProgram INT = (SELECT TOP 1 value_string FROM attribute_value with(nolock) WHERE root_process_instance_id=@rootId AND attribute_path='202' AND value_string IS NOT NULL) --> Program główny dla sprawy

DECLARE @entityTempTable TABLE (
	entityValueId int,
	service_group_process_id int,
	serviceId int, 
	entityId int, 
	manufacturerQuantity decimal(10,2), 
	insurancerQuantity decimal(10,2),
	partnerQuantity decimal(10,2),
	dealerQuantity decimal(10,2),
	clientQuantity decimal(10,2),
	name  nvarchar(100),
	programId int
)

insert into @entityTempTable
select	ave.id entityValueId,
		avs.value_int service_group_process_id,
		avsn.value_int serviceId,
		ave.value_int entityId,
		avpr.value_int manufacturerQuantity,
		avu.value_int insurancerQuantity,
		avpz.value_int partnerQuantity,
		avd.value_int dealerQuantity,
		avkl.value_int clientQuantity,
		e.entity,
		avp.value_int   
from	dbo.attribute_value avs with(nolock) inner join
		dbo.attribute_value avsn with(nolock) on avsn.parent_attribute_value_id=avs.id and avsn.attribute_path='820,821,805' and avsn.root_process_instance_id=@rootId inner join
		dbo.attribute_value avp with(nolock) on avp.parent_attribute_value_id=avs.id and avp.attribute_path='820,821,830' and avp.root_process_instance_id=@rootId inner join
		dbo.attribute_value ave with(nolock) on ave.parent_attribute_value_id=avs.id and ave.attribute_path='820,821,822' and ave.root_process_instance_id=@rootId inner join
		dbo.attribute_value avpr with(nolock) on avpr.parent_attribute_value_id=ave.id and avpr.attribute_path='820,821,822,824' and avpr.root_process_instance_id=@rootId inner join
		dbo.attribute_value avu with(nolock) on avu.parent_attribute_value_id=ave.id and avu.attribute_path='820,821,822,825' and avu.root_process_instance_id=@rootId inner join
		dbo.attribute_value avpz with(nolock) on avpz.parent_attribute_value_id=ave.id and avpz.attribute_path='820,821,822,826' and avpz.root_process_instance_id=@rootId inner join
		dbo.attribute_value avd with(nolock) on avd.parent_attribute_value_id=ave.id and avd.attribute_path='820,821,822,827' and avd.root_process_instance_id=@rootId inner join
		dbo.attribute_value avkl with(nolock) on avkl.parent_attribute_value_id=ave.id and avkl.attribute_path='820,821,822,828' and avkl.root_process_instance_id=@rootId inner join
		dbo.attribute_value avq with(nolock) on avq.parent_attribute_value_id=ave.id and avq.attribute_path='820,821,822,221' and avq.root_process_instance_id=@rootId left join		
		dbo.attribute_value avcdn with(nolock) on avcdn.parent_attribute_value_id=ave.id and avcdn.attribute_path='820,821,822,861' and avcdn.root_process_instance_id=@rootId inner join		
		dbo.entities e with(nolock) on e.id=ave.value_int 
where	avs.attribute_path='820,821' and 
		avs.value_int is not null and
		ISNULL(avcdn.value_int,1) = 1 and
		avs.root_process_instance_id=@rootId

declare kurZ cursor LOCAL for
select * from(
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity*0.5 quantity, 1 type, name, manufacturerQuantity  quantityXL, programId
		from @entityTempTable 
		where programId in (418,420) and entityId IN(10,1,21,29,43,19,23,14,22,16,63)
		UNION
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity*0.5 quantity, 2 type, name , manufacturerQuantity quantityXL, programId
		from @entityTempTable 
		where programId in (418,420) and entityId IN(10,1,21,29,43,19,23,14,22,16,63)
		UNION ALL
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity*0.7 quantity, 1 type, name , manufacturerQuantity quantityXL, programId
		from @entityTempTable 
		where programId in (415) and entityId IN(10,1,21,29,43,19,23,14,22,16,63)
		UNION
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity*0.3 quantity, 2 type, name , manufacturerQuantity quantityXL, programId
		from @entityTempTable 
		where programId in (415) and entityId IN(10,1,21,29,43,19,23,14,22,16,63)	
		union all
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity quantity, 1 type, name , manufacturerQuantity quantityXL, programId
		from @entityTempTable 
		WHERE not(entityId IN(10,1,21,29,43,19,23,14,22,16,63) and programId in (415,418,420))
		union all
		select entityValueId, service_group_process_id, serviceId, entityId, insurancerQuantity quantity, 2 type, name , insurancerQuantity quantityXL, programId from @entityTempTable
		union all 
		select entityValueId, service_group_process_id, serviceId, entityId, partnerQuantity quantity, 3 type, name , partnerQuantity quantityXL, programId from @entityTempTable
		union all 
		select entityValueId, service_group_process_id, serviceId, entityId, dealerQuantity quantity, 4 type, name , dealerQuantity quantityXL, programId from @entityTempTable
		union all
		select entityValueId, service_group_process_id, serviceId, entityId, clientQuantity quantity, 5 type, name , clientQuantity quantityXL, programId from @entityTempTable
	) a where isnull(a.quantity,0.0) <> 0.0 
OPEN kurZ;
FETCH NEXT FROM kurZ INTO @entityValueId, @groupProcessId, @serviceId, @entityId, @quantity, @type, @name, @quantityXL,@programId;
WHILE @@FETCH_STATUS=0
BEGIN
	print '#######################################################'
	print @name  + dbo.f_translate(' entity: ',default) + CAST(@entityId AS VARCHAR(20))

	/* Flagi opcji. */
	declare @skipStarterCodeBL bit = 0
	declare @atrybutLeaseplan nvarchar(50)

	set @onlyEmptyService=0
	SET @headerZZ = NULL
	SET @headerZS = NULL
	declare @AkronimZS varchar(50)
	declare @czyASO int
	set @akronimTarget=''
	set @partnerId=0

	SELECT @programName = name FROM dbo.vin_program with(nolock) WHERE id = @programId
    set @description = ISNULL(@programName,'')

	-- Jeśli jest odwrotny GroupId to ustawia zmienną @reverseGroupId. Wykorzystywane dla rozdzielania usługi MW po programach.
	declare @reverseGroupId int = 0 -- standardowo brak
	if @groupProcessId < 0
	begin
		set @reverseGroupId = @groupProcessId
		set @groupProcessId = @groupProcessId * -1
		set @description = @description + dbo.f_translate('. Usługa łączona',default)
	end

	if @activeServicesCount > 0
	begin
		set @description = @description + ' » Sprawa niezakończona'
	end

	-- Sprawdzenie statusu pusty wyjazd, jeśli tak(1), jesli nie (0)
	declare @arcCode3Id int = (
		SELECT TOP 1 value_int FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path='638,722' AND value_int IS NOT NULL
	)
	declare @emptyService int = 0
	declare @serviceStatus int = (	SELECT TOP 1 status_dictionary_id
									FROM dbo.service_status with(nolock)
									WHERE group_process_id=@groupProcessId AND @entityId IN (1,10)
									ORDER BY id DESC)
	if @serviceStatus = -2 or @arcCode3Id=170 --> 85 Pusty wyjazd (ARC3)
	begin
		set @emptyService=1
		print 'Pusty wyjazd!'
	end

	-- Naprawa nieudana
	declare @noFix int
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '209', @groupProcessInstanceId = @groupProcessId
	DECLARE @isPartnerFixed BIT = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)

	/*	120	6F naprawa na miejscu niemożliwa - podholowano do serwisu
		140	97 holowanie (ogólnie)
		150	6A naprawa na miejscu niemożliwa - dalsze usługi nie są konieczne
		160	2G brak narzędzi do naprawy na miejscu - holowanie do serwisu
		180	6G naprawa na miejscu niemożliwa - holowanie do serwisu
		190	6D naprawa na miejscu niemożliwa- holowanie do miejsca wskazanego przez klienta
		200	6S - naprawa niemożliwa. Konieczne holowanie innym pojazdem							*/
	if ((@arcCode3Id in (120,140,150,160,180,190,200) AND @entityId=10) OR (@arcCode3Id IS NULL AND @isPartnerFixed=0 AND @entityId=10)) AND @emptyService=0 --(@arcCode3Id in (150,200) AND @entityId = 1) OR Skasowane ze względu na to że kontraktorzy zamykają holowanie kodami z naprawy.
	begin
		set @noFix=1
		print 'Naprawa nieudana!'
	end
	else
	begin
		set @noFix=0
	end 

	--> Pobieranie partnera dla usługi (Akronim ZZ)
	IF EXISTS(SELECT TOP 1 1 FROM attribute_value with(nolock) WHERE attribute_path = '610' AND group_process_instance_id = @groupProcessId AND value_int IS NOT NULL) AND @country = dbo.f_translate(dbo.f_translate('Polska',default),default)
	BEGIN --> Partner polski.
		SET @partnerId = (SELECT TOP 1 value_int FROM attribute_value with(nolock) WHERE group_process_instance_id=@groupProcessId AND attribute_path IN ('610') AND value_int IS NOT NULL)
	END
	ELSE IF EXISTS(SELECT TOP 1 1 FROM attribute_value WHERE attribute_path = '767,773,704' AND group_process_instance_id = @groupProcessId AND value_int IS NOT NULL) AND @country <> dbo.f_translate(dbo.f_translate('Polska',default),default)
	BEGIN --> Partner zagraniczny.
		SET @partnerId = (SELECT TOP 1 value_int FROM attribute_value with(nolock) WHERE group_process_instance_id=@groupProcessId AND attribute_path IN ('767,773,704') AND value_int IS NOT NULL)
	END
	ELSE  --> Nie udało się dopasować partnera, wybór z dowolnego wypełnionego atrybutu.
	BEGIN
		SET @partnerId = (SELECT TOP 1 value_int FROM attribute_value with(nolock) WHERE group_process_instance_id=@groupProcessId AND attribute_path IN ('610','767,773,704') AND value_int IS NOT NULL)
	END

	IF ISNULL(@partnerId,0)=0 AND @entityId=29 --> MW Pobieranie partnera z pola Wypożyczalnia organizująca wynajem
	BEGIN
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessId
		SELECT @partnerId = value_int FROM @values		
	END

	DECLARE @platformFromProgram INT = (SELECT TOP 1 platform_id FROM dbo.vin_program with(nolock) WHERE id=@programId AND platform_id IS NOT NULL)
	DECLARE @partnerKinds NVARCHAR(255) = NULL
	DECLARE @partnerCode NVARCHAR(255) = NULL
	DECLARE @isBRS BIT = 0
	/*	48 Business Lease
		53 ALPHABET			
		25 Leaseplan
		78 Carefleet   */
	IF @platformFromProgram IN (48,53,25,78) ---- Maria
	BEGIN
		SELECT @partnerKinds=av_p.value_text,@partnerCode=av_ks.value_string FROM attribute_value av WITH(NOLOCK)
		JOIN attribute_value av_p WITH(NOLOCK) ON av.id=av_p.parent_attribute_value_id AND av_p.attribute_path = '595,597,596' 
		JOIN attribute_value av_ks WITH(NOLOCK) ON av_ks.group_process_instance_id=av_p.group_process_instance_id AND av_ks.attribute_path = '595,631' 
		WHERE av.id=@partnerId

		/*	85	BRS Priorytet 1
			94	BRS Priorytet 1A
			95	BRS Zawieszone
			98	BRS LUM
			99	BRS Poszerzony obszar obsługi
			100	BRS Serwis preferowany
			101	BRS COMPET	*/
		IF EXISTS( SELECT  d.textD FROM f_split(@partnerKinds,',') fs --> Serwisy preferowane
		JOIN dictionary d WITH(NOLOCK) ON d.value=fs.data AND d.typeD = 'PartnerLocationType' WHERE ( d.textD LIKE '%BRS%'))
		BEGIN
			SET @isBRS = 1
			PRINT dbo.f_translate('Partner BRS',default)
		END
	END

	IF (@type = 4)
	BEGIN
		SET @description = @description + ' (rozliczenie z Dealerem)'
	END

	SELECT @partnerStarterCode = av.value_string 
	FROM	dbo.attribute_value av with(nolock) inner join 
			dbo.attribute_value location with(nolock) ON location.parent_attribute_value_id = av.parent_attribute_value_id  
	where	av.attribute_path = '595,631'
		and location.id = @partnerId

	if @partnerId=0 set @partnerStarterCode='111111'

	print '@partnerStarterCode: ' + @partnerStarterCode

	-------------------------------------------------------
	------------- service Partner Code
	------------- kod partnera do którego holujemy (serwis)
	-------------------------------------------------------
	declare @servicePartnerId int = null
	declare @towingGroupProcessId int = null
	declare @servicePartnerStarterCode nvarchar(50) = null

	if exists ( --> Aktualna grupa to proces NH
		select  top 1 1 from	dbo.process_instance with(nolock) 
		where	group_process_id=@groupProcessId and step_id like '1009%'
	)
	begin
		set @towingGroupProcessId=@groupProcessId
	end
	else 
	begin --> Aktualna grupa nie posiada NH, szuka pierwszego procesu NH dla sprawy
		select  top 1 @towingGroupProcessId=group_process_id from dbo.process_instance with(nolock) 
		where	root_id=@rootId and step_id like '1009%' order by id desc
	end

	-- Dealer Call zawsze na Serwis
	DECLARE @IsDealerCall BIT = ISNULL((SELECT TOP 1 value_int FROM attribute_value with(nolock) WHERE root_process_instance_id = @rootId AND attribute_path='802' AND group_process_instance_id=@groupProcessId ORDER BY id DESC),0)
	IF (@IsDealerCall = 1 AND @towingGroupProcessId IS NULL)
	BEGIN
		SET @towingGroupProcessId = @groupProcessId
	END

	print @country
	if isnull(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) = dbo.f_translate(dbo.f_translate('Polska',default),default)
	begin
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towingGroupProcessId
		SELECT TOP 1 @servicePartnerId = value_int FROM @values WHERE value_int IS NOT NULL
	end
	else
	begin
		print @country
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '767,773,704', @groupProcessInstanceId = @towingGroupProcessId
		SELECT TOP 1 @servicePartnerId = value_int FROM @values WHERE value_int IS NOT NULL
	end

	SELECT @servicePartnerStarterCode = av.value_string
	FROM	dbo.attribute_value av with(nolock) inner join 
			dbo.attribute_value location with(nolock) ON location.parent_attribute_value_id = av.parent_attribute_value_id  
	where	av.attribute_path = '595,631'
		and location.id = @servicePartnerId
	-------------------------------------------------------
	if exists(
		SELECT av.value_string 
		FROM dbo.attribute_value av with(nolock)
		inner join dbo.attribute_value location with(nolock) ON location.id = av.parent_attribute_value_id cross apply dbo.f_split (av.value_string,',') s
		where av.attribute_path = '595,597,596'
		and location.id = @partnerId and s.data in (2,3,4,5,7,8,9,10,11,47) 
	) 
	begin
		set @czyASO=1
	end
	else
	begin
		set @czyASO=0
	end

	declare @towing int = null
	--/*1-hol, 2-naprawa, null*/
	SELECT TOP 1 @towing=value_int FROM attribute_value WITH(NOLOCK) WHERE attribute_path='560' AND group_process_instance_id=@groupProcessId AND value_int IS NOT NULL

	-- Usługi auta zastępczego.
	DECLARE @AutoReplacement BIT = 0
	IF (@entityId IN (29,30,31,32,33,34,35,36,37,38,39,40,41,42))
	BEGIN
		SET @AutoReplacement = 1
	END

	---------------Kontrahent Docelowy ---------------------------------------
	declare @Target varchar(20)
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towingGroupProcessId
	SELECT TOP 1 @servicePartnerId = value_int FROM @values WHERE value_int IS NOT NULL

	if @servicePartnerId is not null
	begin
		SELECT  @Target = avKod.value_string 
		FROM	dbo.attribute_value avL with(nolock) inner join 
				dbo.attribute_value avKod with(nolock) ON avKod.parent_attribute_value_id = avL.id and avKod.attribute_path='595,597,631'  
		WHERE	avL.id = @servicePartnerId and avL.attribute_path='595,597'
	end

	-------------------------------------------------------------------
	-- Tutaj wpisać wszelkie zmienne i dane apropos nagłówka sprawy
	-------------------------------------------------------------------
	SELECT @gidTypeZZ = 1152
	SELECT @serieZZ = ''

	SELECT @gidTypeZS = 1280
	SELECT @serieZS = ''

	-- Usługa kredytu tak samo jak klient płaci, nie ma się dzielić itd...
	if @entityId=18
	begin
		set @type=5
	end

	-- ============================================================= --
	--						Flagi do zamówień --
	-- ============================================================= --
	declare @XL_ExpoNormZS int = 0	
	declare @XL_ExpoNormZZ int = 0
	declare @XL_FlagaNBZS varchar(1) = 'N'
	declare @XL_FlagaNBZZ varchar(1) = 'N'
	declare @isTrialPrice bit = 0 --> Cenniki testowe dla wybranych kontraktorów na ZZ
	declare @isTireRepair bit = 0 --> Kontraktorzy z możliwością naprawy opon
	declare @XL_FlagaSagai int = 0 --> PSA » Sterowanie importem do Sagai

	-- ============================================================== --
	--  Konfiguracja nagłówków i serii dla poszczególnych programów.  --
	-- ============================================================== --

	IF		@programId IN (402,414,415,416,417,418,419,420,421,424,425,426) --> VGP	BEZ SEAT IF @platformId IN (6,11,31) -- VGP
	BEGIN
		print dbo.f_translate('VGP',default)
		if (@programId in (414,415,427,426) AND @type in (1,4,5)) -- OR Audi ARC
		begin
			SET @serieZS = 'AU'
			set @AkronimZS='103600'

			if (@towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1 or @entityId in (21,63)) and (@entityId not in (30,31)) -- podstawienie i odbir
			begin
				set @AkronimZS=@servicePartnerStarterCode
				set @akronimTarget=@Target
			end
		end

		if @programId in (414,415) AND @type=2
		begin
			SET @serieZS = dbo.f_translate('AUB',default)
			set @AkronimZS='106000'
		end

		if (@programId in (416,402,421,427,425) AND @type in (1,4,5)) -- OR Skoda ARC
		begin
			SET @serieZS = dbo.f_translate('SKO',default)

			set @AkronimZS='103600'		
			if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1 or @entityId in (21,63)
			begin
				set @AkronimZS=@servicePartnerStarterCode
				set @akronimTarget=@Target
			end 
		end

		if @programId in (416,402,421,427) AND @type=2
		begin
			SET @serieZS = dbo.f_translate('SKOB',default)
			set @AkronimZS='106000'
		end

		if (@programId in (417,418,419,420,427,424) AND @type in (1,4,5)) -- OR Volkswagen ARC
		begin
			SET @serieZS = 'VW'

			set @AkronimZS='103600'
			if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1 or @entityId in (21,63)
			begin
				set @AkronimZS=@servicePartnerStarterCode
				set @akronimTarget=@Target
			end
		end
	
		if @programId in (417,418,419,420) AND @type=2
		begin
			SET @serieZS = dbo.f_translate('VWB',default)
			set @AkronimZS='106000'
			print 'AA'
		end

		--> Unacsa od 2019-07-01
		declare @unacsaCase bit = 0
		if @AkronimZS='106000' and @type=2 and @caseDate>'2019-07-01' and @policyStartDate>'2019-07-01' and @IsDealerCall=0
		begin
			set @AkronimZS='102593'
			set @serieZS = case when @platformFromProgram= 6 then dbo.f_translate('SKOU',default)    --> Skoda Assistance
								when @platformFromProgram=10 then dbo.f_translate('SEAU',default)    --> Seat Assistance
								when @platformFromProgram=11 then dbo.f_translate('VWU',default)     --> Volkswagen Assistance
								when @platformFromProgram=31 then dbo.f_translate('AUU',default) end --> Audi Assistance
			set @unacsaCase=1
			print 'BB'
		end

		if @type = 3
		begin
			SET @serieZS = dbo.f_translate('SARC',default)
			set @AkronimZS='111111'
		end

		if @type = 5
		begin
			SET @description = @description + dbo.f_translate(' Rozliczenie z KL',default)
			set @AkronimZS='103600'
		end

		if @programId in (424,425,426) and (isnull(@serieZS,'')='' or isnull(@serieZS,'')='')
		begin
			SET @serieZS = dbo.f_translate('SARC',default)
		end

		IF @statusBWB IN (0,-1)
		BEGIN
			SET @description = @description + dbo.f_translate('. Brak w bazie',default)
		END
	END
	ELSE IF @programId IN (429,430,431,432,433,434,435,436,437,438,439,440,441) OR @platformId=60--> Ford, Ford Vignale	ELSE IF @platformId IN (2,60) -- FORD,Ford Vignale
	BEGIN
		set @serieZS = dbo.f_translate('FORD',default) -- Seria uniwersalna w ramach platformy
		set @type = 1 -- Jeśli FORD, ustaw rozliczenie na Producenta

		/*	429	Ford Euroservice
			430	Ford Assistance 12
			431	Ford Protect
			432	Ford Inne Marki Standard
			433	Ford Inne Marki Exclusive
			434	Ford Inne Marki Plus
			435	Ford Ubezpieczenia Standard
			436	Ford Ubezpieczenia Plus
			437	Ford Ubezpieczenia Exclusive
			438	Ford Mini Car Assistance
			439	Ford Assistance Kolizyjny	*/
		if @programId in (429,430,431,432,433,434,435,436,437,438)
		begin
			set @XL_FlagaNBZS = 'B'
			set @AkronimZS='100009'
			--set @type = 2 -- Jeśli FORD, ustaw rozliczenie na Ubezpieczyciel
		end

		if @programId = 439 -- Ford Assistance Kolizyjny
		begin
			set @AkronimZS='100020'
		end

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1
		begin
			set @akronimTarget=@AkronimZS--@Target
		end

		if @programId = 441 or @platformId = 60 -- Ford Vignale 60
		begin
			set @serieZS = dbo.f_translate('FORV',default)
			set @AkronimZS='100268' -- ARC EUROPE
			set @XL_ExpoNormZS = 6
			set @XL_ExpoNormZZ = 6
		end

		--if @programId in (429,430) and GETDATE()>='2019-05-01'
		--begin
		--	print dbo.f_translate('Od maja import FE I FA12 wstrzymany',default)
		--	set @XL_FlagaSagai=3
		--end
	END
	ELSE IF	@programId IN (368,461,462,463,464) --> VGP	BEZ SEAT IF @platformId IN (6,11,31) -- VGP	ELSE IF @platformId = 10 -- Seat Assistance
	BEGIN
		print dbo.f_translate('SEAT',default)
		set @serieZS = dbo.f_translate('SEAT',default)

		/*	461	SEAT Assistance
			462	SEAT Przedłużone Assistance				*/
		if (@programId in (461,462)) -- 
		begin
			set @type = 1 -- Producent
			set @AkronimZS='103600'
		end

		/*	368	SEAT Gwarancja Mobilności od 1.7.2015
			463	Seat Gwarancja Mobilności				*/
		if (@programId in (368,463)) -- 
		begin
			set @type = 2 -- Ubezpieczyciel
			set @AkronimZS='106000'
		end

		/*	464	ARC SEAT  */
		if (@programId in (464))
		begin
			set @type = 3 -- Partner Zagraniczny
			SET @serieZS = dbo.f_translate('SARC',default)
			set @AkronimZS='111111'
		end

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1  or @entityId = 21
		begin
			--set @AkronimZS=@servicePartnerStarterCode
			set @akronimTarget=@AkronimZS--@Target
		end

		IF @statusBWB IN (0,-1)
		BEGIN
			SET @description = @description + dbo.f_translate('. Brak w bazie',default)
		END
	END
	ELSE IF @platformFromProgram = 76 --> Cupra Assistance
	BEGIN
		print dbo.f_translate('Cupra Assistance',default)
		set @serieZS = dbo.f_translate('CUP',default)

		set @type = 1 -- Producent
		set @AkronimZS='103600'

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1  or @entityId = 21
		begin
			--set @AkronimZS=@servicePartnerStarterCode
			set @akronimTarget=@AkronimZS--@Target
		end

		IF @statusBWB IN (0,-1)
		BEGIN
			SET @description = @description + dbo.f_translate('. Brak w bazie',default)
		END
	END
	ELSE IF @programId IN (447,448,449) --> KIA	ELSE IF @platformId = 32 -- KIA
	BEGIN
		print dbo.f_translate('KIA',default)
		set @serieZS = dbo.f_translate('KIA',default)
		set @AkronimZS='102900'

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1
		begin
			set @akronimTarget=@AkronimZS--@Target
		end
	END
	ELSE IF @programId IN (475,476,477,478,479,480,481,482,483) --> TUW TUZ	ELSE IF @platformId = 35 --	TUW TUZ
	BEGIN
		print dbo.f_translate('TUW TUZ',default)
		set @AkronimZS='103200'
		set @serieZS = dbo.f_translate('TUZ',default)
		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1
		begin
			set @akronimTarget=@AkronimZS--@Target
		end
	END
	ELSE IF @programId IN (484,485,486) -->	TUW TUZ Home Assistance	ELSE IF @platformId = 36 --	TUW TUZ Home Assistance
	BEGIN
		print dbo.f_translate('TUW TUZ',default)
		set @AkronimZS='103200'
		set @serieZS = 'HA'
		set @XL_FlagaNBZZ = 'B'
	END
	ELSE IF @programId IN (469,470,471,472,473,474) -->	TUW Pocztowe	ELSE IF @platformId = 42 --	TUW Pocztowe
	BEGIN
		print dbo.f_translate('TUW Pocztowe',default)
		set @AkronimZS='103903'
		set @serieZS = dbo.f_translate('POCZ',default)
		set @XL_FlagaNBZS = 'B'

		/*	469	42	TUW Assistance (Pocztowe OC)
			470	42	Moto Assistance Standard (TUW Pocztowe)
			471	42	Moto Assistance Komfort (TUW Pocztowe)*/
		if @programId in (469,470,471)
		begin
			set @XL_FlagaNBZZ = 'N'
		end

		/*	472	42	TUW Pocztowe Bezpieczny Dom
			473	42	TUW Pocztowe ubezpieczenie kredytów i pożyczek hipotecznych
			474	42	TUW Pocztowe ubezpieczenie ROR */
		if @programId in (472,473,474)
		begin
			set @XL_FlagaNBZZ = 'B'
		end
	END
	ELSE IF @programId IN (443,444,445,446,494,495,496,497) -->	Concordia Assistance	ELSE IF @platformId = 18 --	Concordia Assistance
	BEGIN
		print dbo.f_translate('Concordia',default)
		set @AkronimZS='101800'
		set @serieZS = dbo.f_translate('CON',default)
		set @XL_FlagaNBZS = 'B'
	END
	ELSE IF @programId IN (458,459,460) -->	Orix Standard,VIP,Exclusive
	BEGIN
		print dbo.f_translate('Orix',default)
		set @AkronimZS='101800'
		set @serieZS = dbo.f_translate('ORIX',default)
		set @XL_FlagaNBZS = 'B'
	END
	ELSE IF @programId IN (468) --> Comfort ERV
	BEGIN
		print dbo.f_translate('ERV',default)
		set @AkronimZS='104800'
		set @serieZS = dbo.f_translate('ERV',default)
		set @XL_FlagaNBZS = 'B'
	END
	ELSE IF @programId IN (492,493) --> Centra Assistance, Exide Assistance
	BEGIN
		print dbo.f_translate('CENTRA',default)
		set @AkronimZS='100006'
		set @serieZS = dbo.f_translate('CEN',default)
	END
	--» Od 06.12.2018r.
	ELSE IF @programId IN (498) --> AA (członkowie klubu brytyjskiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = 'AA'
		set @AkronimZS='101612'
	END
	ELSE IF @programId IN (499) --> ANWB (członkowie klubu holenderskiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('ANWB',default)
		set @AkronimZS='106813'
	END
	ELSE IF @programId IN (507,501,502,488,489,487) --> ARC,RACE (członkowie klubu hiszpańskiego),Rosqvist (członkowie klubu fińskiego)
	BEGIN/*	489	Volvo Over Seas delivery
			488	Volvo Global Special Sales
			487	Volvo Tourist & Diplomat Sales*/
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('SARC',default)
		set @AkronimZS='111111'
	END
	ELSE IF @programId IN (505) --> Husqvarna Assistance (Sylwia)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('HUSQ',default)
		set @AkronimZS='111111'
	END
	ELSE IF @programId IN (450) --> KTM Assistance (Sylwia)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('KTM',default)
		set @AkronimZS='100268'
	END
	ELSE IF @programId IN (451) --> Mercedes Wypadek (Łukasz)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 6
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('MERW',default)
		set @AkronimZS='100268'
	END
	ELSE IF @programId IN (500) --> OAMTC (członkowie klubu austriackiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('OMTC',default)
		set @AkronimZS='100044'
	END
	ELSE IF @programId IN (465) --> Smart Assistance (Sylwia)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('SMA',default)
		set @AkronimZS='100574'
	END
	ELSE IF @programId IN (503) --> TCB (członkowie klubu belgijskiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('TCB',default)
		set @AkronimZS='100439'
	END
	ELSE IF @programId IN (504) --> TCS (członkowie klubu szwajcarskiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('TCS',default)
		set @AkronimZS='100267'
	END
	--» CFM Od 1,2,3? 2019r. 
	ELSE IF @programId IN (508,509,510,511,512,513,514,515,516,517,518,519,520,521) OR @platformFromProgram = 53 --> 53 Alphabet
	BEGIN
		PRINT dbo.f_translate('Alphabet',default)
		set @serieZS = dbo.f_translate('ALF',default)
		set @AkronimZS='105000'
	END
	ELSE IF @programId IN (551,552,553) OR @platformFromProgram = 48--> 48 Bussiness Lease
	BEGIN
		PRINT dbo.f_translate('Bussiness Lease',default)
		set @serieZS = 'BL'
		set @AkronimZS='104000'
		/* Jeśli docelowy kod starter lokalizacji będzie Motoflota to zmienić Arkonim. */
		--if @serviceId = 5 AND EXISTS ( --> Auto zastępcze
		--	SELECT  d.textD FROM f_split(@partnerKinds,',') fs
		--	JOIN dictionary d WITH(NOLOCK) ON d.value=fs.data AND d.typeD = 'PartnerLocationType' WHERE d.textD LIKE '%ASO%'
		--) AND @programId IN (SELECT id FROM vin_program WHERE platform_id=48) --> Program nalezy do BL 
		--begin
		--	print dbo.f_translate('Wynajem rozliczy: BL',default)
		--end
		--else if @serviceId = 5
		--begin
		--	set @AkronimZS='107176' --> Wszystkie koszty związane z obsługa wynajmów powinny iść na Motoflotę. Ale nie jeśli wykonuje ASO!
		--	print dbo.f_translate('Wynajem rozliczy: Motoflota',default)
		--end
	END
	ELSE IF @platformFromProgram = 25 --> 25 LEASEPLAN
	BEGIN
		PRINT dbo.f_translate('LEASEPLAN',default)
		set @serieZS = dbo.f_translate('LEAS',default)
		set @AkronimZS='102800'

		set @atrybutLeaseplan = (select top 1 value_string from attribute_value with(nolock) where root_process_instance_id=@rootId and attribute_path='440' and value_string is not null)
		if @programId = 575 --> LeasePlan Extra (dodatkowa autoryzacja LeasePlan)
		begin
			set @description = @description + ' » LP-Passon'
		end
		else if isnull(@atrybutLeaseplan,'') <> ''
		begin
			set @description = @description + ' » ' + @atrybutLeaseplan
		end

		if (select top 1 value_string from attribute_value with(nolock) where root_process_instance_id=@rootId and attribute_path='981,438' and value_string is not null) like '%tikkurila%'
			and isnull((select top 1 value_int from attribute_value with(nolock) where root_process_instance_id=@rootId and attribute_path='1067' and value_int is not null),0)>0
		begin
			set @description = @description + ' » ' + dbo.f_translate('Tikkurila',default)
		end


		if @programId in (576,578) --> LeasePlan Strata, LeasePlan Serwis Strata
		begin
			set @description = @programName
		end
	END
	ELSE IF @platformFromProgram = 43 --> 43 ALD
	BEGIN
		PRINT dbo.f_translate('ALD',default)
		set @serieZS = dbo.f_translate('ALD',default)
		set @AkronimZS='102725'
	END
	ELSE IF @platformFromProgram IN (8,13,16,20,21,22,50,64,68,73,74) --> TRUCK
	BEGIN
		PRINT dbo.f_translate('TRUCK',default)
		/*	8	DKV
			16	UTA Assistance    
			20	Euroshell Breakdown Service
			21	Shell Autoryzacje
			22	ESSO Assistance
			50	Shell Alarmowy
			64	Webasto Assistance
			68	KOGEL Assistance
			73	IDS
			74	Otokar Assistance	*/
		set @XL_ExpoNormZS = 6
		set @XL_ExpoNormZZ = 0
		set @serieZS = 'TR'
		set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
		-- holowanie, naprawa na drodze, naprawa warsztatowa oraz dowóz gotówki.
		set @AkronimZS='111111'

		if @programId = 522 --> DKV
		begin
			set @AkronimZS='101756'
			set @currency = dbo.f_translate(dbo.f_translate('PLN',default),default)
			set @XL_ExpoNormZS = 0
		end

		if @programId in (526,525) --> UTA Assistance, Euroshell Breakdown Service
		begin
			set @AkronimZS='100390'
		end
	END
	ELSE IF @platformFromProgram IN (5) --> ADAC
	BEGIN
		PRINT dbo.f_translate('ADAC',default)
		set @serieZS = dbo.f_translate('ADAC',default)
		set @AkronimZS='100264'
	END
	--» PSA (Opel, Citroen, Peugeot, DC, Motorhomes) od 04.2019
	ELSE IF @platformFromProgram IN (	14, --> Opel
								79, --> Citroen
								80, --> Peugeot
								81, --> DS
								82) --> Motorhomes
	BEGIN
		print dbo.f_translate('PSA',default)

		if @platformFromProgram in (79,80,81,82)
		begin
			set @XL_FlagaSagai = 3
		end

		set @serieZS = case when @platformFromProgram = 14 then 'GM'	--> Opel
							when @platformFromProgram = 79 then dbo.f_translate('CIT',default)	--> Citroen
							when @platformFromProgram = 80 then dbo.f_translate('PEU',default)	--> Peugeot
							when @platformFromProgram = 81 then 'DS'	--> DS
							when @platformFromProgram = 82 then 'MH' end--> Motorhomes

		set @AkronimZS = case	when @platformFromProgram = 14 then '102200'	--> Opel
								when @platformFromProgram = 79 then '104472'	--> Citroen
								when @platformFromProgram = 80 then '103641'	--> Peugeot
								when @platformFromProgram = 81 then '104472'	--> DS
								when @platformFromProgram = 82 then '102200' end--> Motorhomes
		/*	1	0201-Holowanie
			10	0101-Naprawa na drodze
			29	0401-Wynajem auta zastępczego
			19	20602-Lot
			21	0701-Odwiezienie osobówką
			23	10601-Pociąg
			26	1901-Porada
			43	0501-Nocleg
			63	Transport						*/
		if @entityId not in (1,10,29,19,21,23,26,43,63)
		begin
			print 'PSA » Usługa dodatkowa'
			set @AkronimZS='111111' 
			set @XL_FlagaSagai = 0
		end

		--> AD HOC
		if exists(select top 1 1 from dbo.vin_program with(nolock) where platform_id IN (14,79,80,81,82) and name like '%Ad hoc%' and id=@programId order by platform_id)
		begin
			set @type = 10 --> AD HOC
			set @isAdHocPSA = 1
		end

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1
		begin
			set @akronimTarget=@AkronimZS--@Target
		end
	END
	--> CFM: Carefleet od 11.06.2019
	ELSE IF @platformFromProgram = 78 --> Carefleet
	BEGIN
		PRINT dbo.f_translate('Carefleet',default)
		SET @serieZS = CASE WHEN @programName LIKE 'IPA%dbo.f_translate(' THEN ',default)CARAdbo.f_translate(dbo.f_translate(' ELSE ',default),default)CAR' END

		SET @AkronimZS = CASE WHEN @programName LIKE 'IPA%dbo.f_translate(' THEN ',default)107500dbo.f_translate(dbo.f_translate(' ELSE ',default),default)107400' END

		IF @serieZS = dbo.f_translate('CAR',default) AND @description NOT LIKE 'CF %'
		BEGIN
			SET @descriptionTmp = @description
			SET @description = 'CF ' + @descriptionTmp
		END
	END

	--- Uniwersalne parametry dla wszystkich platform
	if @programId=428 -- Strata
	begin
		SET @serieZS = dbo.f_translate('STRA',default)
		set @partnerId=-1
		set @AkronimZS='111111'
	end

	SET @additTownigADH = 0
	if @programId=423 -- Usługi płatne
	begin
		SET @description = ISNULL(@programName,'') + dbo.f_translate(' Ad-hoc',default)
		SET @serieZS = dbo.f_translate('ADH',default)
		set @partnerId=-21
		set @AkronimZS=@partnerStarterCode
		--set @AkronimZS='111111'
		set @akronimZSADH=@partnerStarterCode
		--set @akronimZSADH='111111'
		set @type=10
		set @isAdHocPSA = 1
		--> W przypadku AD-HOC kiedy jest ADH oraz inne holowanie z pokryciem różnym niż usługi płatne
		IF EXISTS(SELECT TOP 1 1 FROM attribute_value av WITH(NOLOCK) JOIN attribute_value av_p WITH(NOLOCK) 
			ON av.group_process_instance_id=av_p.group_process_instance_id AND av_p.attribute_path='202' AND av_p.value_string <> '423'
			WHERE av.root_process_instance_id=@rootId AND av.attribute_path='560' and av.value_int=1)
		BEGIN
			IF @programId = 423 --> Usługi Płatne
			BEGIN
				SET @additTownigADH = 1
			END
		END
	end

	if @entityId=18	-- Usługa kredytu zawsze na akronim jednorazowy
	begin
		set @AkronimZS='111111'
	end

	--Jeśli Dealer's Calls to na serwis
	if @IsDealerCall=1
	begin
		-- Najlepiej zmienić na @Target ale wtedy nie importuje się jak trzeba i jest kontrahent,miasto 02
		set @partnerStarterCode=@servicePartnerStarterCode--@Target
		print '-- Partner: ' + @servicePartnerStarterCode

		IF @platformFromProgram IN (6,11,31) --> UNACSA.VGP, FW: 20190718_Programy_Serie FS_UNACSA.xlsx
		BEGIN
			IF @policyStartDate IS NOT NULL
			BEGIN
				PRINT 'CC'
				PRINT '@policyStartDate: ' + CAST(@policyStartDate AS nvarchar(255)) --> @policyStartDate: 2018-01-02
				SET @AkronimZS = CASE	WHEN @policyStartDate > '2019-06-30' AND @caseDate BETWEEN '2019-07-01' AND '2019-07-31dbo.f_translate(' THEN ',default)111111' --> Starter
										WHEN @policyStartDate > '2019-06-30' AND @caseDate > '2019-07-31dbo.f_translate(' THEN ',default)103600'
										ELSE '103600'
										END --> VGP
			END
		END
	end

	--if @entityId = 21 and isnull(@partnerStarterCode,'111111')='111111' --> Jeśli brak kontraktora taxi to serwis z holowania
	--begin
	--	set @partnerStarterCode = @servicePartnerStarterCode--@servicePartnerStarterCode
	--end

	/*	Organizowanie usługi N/H na GOP. Wtedy możemy oczekiwać wartości w atrybucie '638,957' [value_int] która mówi o kwocie netto za usługę. 
		Kontraktor nie jest zobligowany, aby podawać ilości przejechanych km dojazd/hol/powrót bo na gopa interesuje nas tylko kwota która została wynegocjowana. */
	DECLARE @isGopNH BIT = 0
	if @entityId in (1,10) and exists (SELECT 1 FROM attribute_value WITH(NOLOCK) WHERE attribute_path='1005,992' AND value_int=1 AND group_process_instance_id=@groupProcessId)
	begin
		set @isGopNH = 1
		SET @description = @description + ' » Usługi NH na GOP' 
	end

	if (@partnerStarterCode in ('36015', --> F.H.U. Holmax Andrzej Hoć
								'36017', --> Tomala
								'36024', --> P.H.U. Jan Wengrzyn
								'36029', --> Auto-Hilfe Pomoc Drogowa
								'36034', --> Regmar Auto-Serwis
								'36042', --> Transport Ciężarowy Janusz Rapp
								'36047') --> Ratownictwo Drogowe Pomoc Drogowa Jerzy Kijański
								and @caseDate >= '2019-03-01')
	or (@partnerStarterCode in ('36031', --> Auto-Art. Joanna Wolniczek
								'36039') --> Auto Crew ASO Anklewicz
								and @caseDate >= '2019-05-01')
	or (@partnerStarterCode in ('31008', --> Transport Pojazdow Na Samochodzie Przewoz Ladunkow
								'32049', --> Zakład Handlowo - Usługowy Maciej Nogalski
								'33022', --> Pomoc Drogowa Aleksander Jawor
								'33051', --> AUTO ALERT
								'35057', --> AL.-KU SERWIS
								'35113', --> ESTDAN Autoparts Daniel Paciorkowski
								'35116', --> MK PUNKT MARCIN MAZUREK
								'36009', --> Pomoc Drogowa A.Szeredi K.Szeredi
								'36011', --> Pomoc Drogowa Fiałkowski
								'36027', --> Blacharstwo i Lakiernictwo Pojazdowe Pomoc Drogowa
								'36032', --> Angelika Import-Export Auto Handel
								'36033', --> Auto-As
								'36035', --> Aera Transport Drogowy Usługi Transportowe
								'36037', --> Pomoc Drogowa Parking Strzeżony
								'36046', --> Całodobowa Pomoc Drogowa Piotr Kotara
								'36049', --> PPHU Moto-Trans-Bud
								'36052', --> Auto Pomoc - Sasin Sp.J.
								'36057', --> PLUCIŃSKI AUTOSERWIS
								'74013', --> P.H.U. Auto-Darex
								'11030', --> Pomoc Drogowa S.O.S. Assistance Marcin Kowalczyk
								'12004', --> Auto Service Warowny Sp.J.
								'12006', --> Auto - Części, Warsztat Mechaniczny Blacharsko Lakierniczy, Pomoc Drogowa Iwona Banaszek
								'13006', --> Pomoc Drogowa Parking Strzezony
								'13009', --> Pomoc Drogowa Kazimierz Jarząbek
								'32073', --> V6 Spółka Cywilna Przemysław Woźniak Sławomir Kowalski     
								'32082', --> DAHLKE Marek Dahlke
								'71001', --> P.H.U.  Moto-Rot  Sp. Z O.O.
								'71028', --> Auto Service Andrzej Wojtan
								'71029', --> F.U.H. Koper Andzej Koperski
								'71057', --> Moto – Komplex Sp. z o.o.
								'71080', --> Auto - Komis
								'71100', --> Z.U. AUTOHOL Marek Wadowski
								'71129', --> BER - GAZ Tomasz Berdys
								'71130', --> BER - GAZ Tomasz Berdys o/Annopol
								'71132', --> Auto Hol Serwis Jaroslaw Gryta
								'71135', --> PUH Import - Export Hurt - Detal Edward Jasiak
								'71146', --> Top - Car Marek Fedio
								'71147', --> Usługi Blacharsko - Lakiernicze "FIAT" Krzysztof Kamiński
								'71152', --> MARGRAB Marek Grabowski
								'71157', --> Auto - Serwis Artur Mroczkowski
								'71164', --> Auto - Transport Tomasz Rozwadowski
								'71183', --> Przedsiębiorstwo Wielobranżowe "GRANICA" Witold Potocki
								'71213', --> CARMAX Dariusz Nowakowski     
								'71214', --> FUH "Auto - Max" Jarosław Gryczka     
								'71220', --> PUH "HRUDEŃ" Mariusz Hrudeń     
								'71223', --> SDK Sp. z o.o.     
								'71224', --> P.W. BROS Sławomir Bielak     
								'71225', --> Autoholowanie Sprint - Hol Michał Zaręba     
								'71234', --> Auto Trans Dominik Czarnowski
								'71238', --> TMS TOMASZ SZOSTAK
								'71251', --> Pawłowski Piotr PP Transport
								'71255') --> FHU AMMAR MILENA MICHALSKAROZNER
								and @caseDate >= '2019-07-01')
	or (@partnerStarterCode in ('31002', --> Ryszard Krzysztof Pietrzak Auto - HolowanieOlchowo
								'31010', --> Trans-Hol Adam MolisKołobrzeg
								'31020', --> Krall Tourist-TransRewal
								'31034', --> DANPOL Andrzej DaniłowiczPołczyn-Zdrój
								'31035', --> Firma Trans-Pol S.C. Zuzanna Stolarczyk-Waszczuk, Artur WaszczukGryfice
								'31043', --> "IDEAL AUTO" Piotr KucharskiChoszczno
								'31047', --> Auto Service ZmitrowiczŚwinoujście
								'31050', --> AUTO - HOL Henryk Trojanowski    Kołobrzeg     
								'31052', --> CAR SERVICE Pomoc Drogowa Ewa DankiewiczŚwinoujście
								'31059', --> Przedsiębiorstwo Usługowo HandloweBobolice
								'31062', --> Usługi Transportowe Pomoc Drogowa "ADACH" Adam HukowskiKołobrzeg
								'31064', --> F.H.U. AUTO-HOL Marcin PiorowiczSzczecinek
								'31067', --> Damian Kalisiak Firma Handlowo - UsługowaWolin
								'31068', --> JAMKO Andrzej DrewnikowskiKoszalin
								'31070', --> CAR-MEDICAL MICHAŁ NIEZBRZYCKISzczecin
								'31072', --> POMOC DROGOWA "BORYS" PIOTR BORYSIUKMirosławiec
								'31073', --> Firma Trans-Pol S.C. Zuzanna Stolarczyk-Waszczuk, Artur WaszczukGoleniów
								'31078', --> PRZEDSIĘBIORSTWO HANDLOWO­ USŁUGOWE FREUDENREICH MACIEJBobolice
								'33028', --> JANKOWSKI Auto-Handel - Mariusz JankowskiChodów
								'33036', --> JANKOWSKI Auto – Handel – Mariusz JankowskiNowy Ciechocinek
								'33041', --> JANKOWSKI Auto – Handel – Mariusz JankowskiWłocławek
								'34029', --> P.H.U. " KONSERWATOR" Przemysław KotzbachIłowa
								'34031', --> Zakład Usługowo - Handlowy Maciej RybczyńskiGubin
								'34032', --> Zakład Mechaniki Pojazdowej POMOC DROGOWA USŁUGI TRANSORTOWE Leszek BiałekLubniewice
								'34033', --> P.H.U. "AL.-TOR-POL" Torzyński KrzysztofMiędzyrzecz
								'34035', --> "JANKOWSKI" Auto-Handel Mariusz JankowskiLutol Suchy St. kolejowa
								'34044', --> Pomoc Drogowa Mirosław KołodziejczykŚwiebodzin
								'34047', --> Firma Handlowo - Usługowa Arkadiusz PawluczenkoSulęcin
								'34049', --> Auto Serwis "TYMEX" Marlena AdamczukMiędzyrzecz
								'36030', --> Pomoc Drogowa Dariusz PawlakWrocław
								'36055', --> Alex-HolPsary
								'42008', --> P.P.U.H. TytanKędzierzyn-Koźle
								'43014', --> AUTO-POMOC-FALKO Edyta KwaśniewskaSuchedniów
								'73012', --> F.H.U. Auto - PolMiędzyzdroje
								'12002', --> Sabat Sp. Z O.O.Lublin
								'46039', --> GP Mobile Gołąb Spółka JawnaGnojnik
								'46047', --> AUTOSERWIS s.c. E.J. PapieżBiały Dunajec
								'46048', --> FIRMA USŁUGOWO TRANSPORTOWA MISINIECSzaflary
								'46046', --> ZPHU Auto-TokSzczawnica
								'74002', --> F H U  AUTOHOLOWANIE  MIROSŁAW  RUTKOWSKIRabka-Zdrój
								'74008', --> F.U.T. Misiniec Zdzisław MisiniecZakopane
								'71195', --> "KACPER" Usługi Transportowe Krzysztof PoddenekLeszno
								'71134', --> Mechanika Pojazdowa Blacharstwo - Lakiernictwo AutoHolowanie Mirosław KlimekWieczfnia Kościelna
								'71193', --> CENTRUM OBSŁUGI POJAZDÓW FALKOJedlińsk
								'71207', --> KODI - AUTO Konrad Rymarz     Wiśniewo
								'71227', --> Przedsiębiorstwo Handlowo - Usługowe "ROBEX" Robert StaworkoOjrzeń
								'71252', --> M.K. CAR-SERVICE Michał KołodziejskiMaków Mazowiecki
								'71221', --> Centrum Obsługi Pojazdów "FALKO" Paweł FalińskiRadom
								'71229', --> Pomoc Drogowa Mateusz KarpińskiWyszków
								'14006', --> Polmozbyt Plus Sp. Z O.O.Białystok
								'71161', --> Raf - Hol Rafal TylmanBiałystok
								'71168', --> BIACOMEX S.ABiałystok
								'14001', --> Auto Naprawa Pomoc DrogowaBielsk Podlaski
								'14007', --> PRZEDSIĘBIORSTWO PRODUKCYJNO HANDLOWE "DEMEX" JERZY DEMIAŃCZUK Augustów
								'14008', --> AUTO KRIS KRZYSZTOF ŻARSKIBielsk Podlaski
								'71049', --> Andrzej Serwis F U HSiemiatycze
								'71087', --> Pomoc DrogowaŁomża
								'71099', --> PHU EXPERT S.C.Zambrów
								'71187', --> R&CARS Robert ZaorskiSuwałki
								'71205', --> P.H.U. Mechanika Pojazdowa Blacharstwo Sklep Motoryzacyjny Auto Holowanie Jerzy Wyszomirski     Mystki-Rzym
								'71243', --> AUTO PLUS Jolanta PłoszkiewiczGrajewo
								'32011', --> Pomoc Drogowa - Parking Strzezony  NaprawaChojnice
								'32076', --> Paweł Nowakowski Przedsiębiorstwo Handlowo - Usługowe "MARIUSZ"Trąbki Małe
								'32084', --> BOBROWSKI SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄGdańsk
								'32015', --> Przedsiębiorstwo Handlowo-Usługowe BUNIKSLębork
								'32017', --> Zaklad Uslugowy  Mechanika PojazdowaBrusy
								'32021', --> Pomoc Drogowa Stanislaw SzewczykKościerzyna
								'32026', --> Przedsiebiorstwo Handlowo-Usługowe Auto-WłodarskiMiastko
								'32030', --> Pomoc Drogowa Mechanika-Diagnostyka-Blacharstwo Roman SzutaGrzybno
								'32033', --> P.H.U. TOMASZ MASIAKCzłuchów
								'32047', --> Zakład Usługowy Mechanika Pojazdowa Wojciech SzopińskiCzersk
								'32058', --> Michał Pobłocki, Auto - WsparcieGdynia
								'32072', --> Auto Centrum Kohnke     Łebcz
								'32080', --> TECHSERWIS Mariusz JankowskiMalbork
								'32081', --> SZOPIŃSKI Krzysztof SzopińskiDziemiany
								'32086', --> ARKADIUSZ KOBIELA Przedsiębiorstwo Handlowo Usługowe KOBIELASierakowice
								'71249', --> Przedsiębiorstwo Handlowo Usługowe KAM-TRANS Kamila HoebellMalbork
								'32077', --> P.P.H.U.T. "FART" Sebastian KaczmarkiewiczReda
								'32078', --> KAM-CAR Kamil UlkowskiGdynia
								'32083', --> KS MOBIL Krzysztof SzrederRumia    
								'32035', --> Firma ADKONIS - Oliver AdkonisSłupsk
								'32046', --> Usługi Transportowe i Parkowanie Pojazdów Dariusz KowalczykSłupsk
								'32008', --> Auto Naprawa  Pomoc DrogowaStarogard Gdański
								'44053', --> HOLSERWIS Michał PiedoZawiercie
								'71248', --> BOLEK ANDRZEJ P.P.U.H. BOL-BUDJaworzno
								'44030', --> Auto-Hol Dariusz JaskółkaRybnik
								'71055') --> Phu RisLidzbark Warmiński
								and @caseDate >= '2019-08-01')
	or (@partnerStarterCode in ('31065', --> AUTO - TRANS Lewczuk Andrzej
								'31071', --> POMOC DROGOWA - ASSISTANCE CZESŁAW ZAULICZNY
								'31077', --> MAT-CAR Mateusz Bryś
								'35014', --> Mechanika Pojazdowa - Pomoc Drogowa Pawel Straburzynski
								'36040', --> Auto Naprawa Grzegorz Turek
								'36041', --> Lacart Sławomir Gębski
								'36050') --> Peters Piotr Sierakowski
								and @caseDate >= '2019-09-01')
	begin
		set @isTrialPrice = 1
		print dbo.f_translate('Cenniki testowe dla wybranych kontraktorów',default)
		SET @description = @description + ' » Cennik testowy'
	end

	if @partnerStarterCode in	(	'35114', --> Opony u klienta Maciej Działowski
									'35117', --> Opony u Klienta Monika Czerwińska
									'36058', --> MK SERWIS MAREK KUBICKI
									'71256', --> PHU MARCIN ZIEMKIEWICZ
									'71257', --> MOBILE GUM MOBILNY SERWIS OPON DZIECIELSKI K.
									'71258') --> WULKANIZACJA MOBILNA MASTER-GUM MARCIN MAJCHRZYCKI
	begin
		set @isTireRepair = 1
		print dbo.f_translate('Naprawa opon',default)
		SET @description = @description + ' » Naprawa opon'
	end

	/************Nowa data @finishedAt dla ZZ jako data zakonczenia usługi, wyliczana indywidualnie**************/
	declare @prcId varchar(20)

	select top 1 @prcId=left(step_id,4)
	from dbo.process_instance
	where group_process_id=@groupProcessId

	if @prcId='1009' -- Data -> Holowanie lub naprawa
	begin
		/*Naprawa na drodze*/
	    if (select top 1 st.[serviceId] from [dbo].[service_status] st with(nolock) where st.group_process_id=@groupProcessId order by st.updated_at desc)=2
		begin
			SELECT TOP 1 @finishedAt=value_date FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path='638,130' AND value_date IS NOT NULL
		end
		else /*Holowanie*/
		begin 
			SELECT TOP 1 @finishedAt=value_date FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path='638,268' AND value_date IS NOT NULL
		end
	end
	else if @prcId='1007' -- Data -> Auto zastępcze
	begin
		begin 
			DECLARE @values5 Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

			insert into @values5	
			EXEC	dbo.p_attribute_get2
				@attributePath = '812,853', --> Data podana przez Kontraktora
				@groupProcessInstanceId =@groupProcessId

			select @finishedAt=value_date   from @values5 

			IF @finishedAt IS NULL --> Wyliczanie daty
			BEGIN
				DECLARE @rentalStartDate DATETIME = (
					SELECT TOP 1 value_date FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path='540' AND value_date IS NOT NULL
				)
				SET @finishedAt = DATEADD(day, @quantity, @rentalStartDate)
			END
		end
	end	 
	else if @prcId='1018' -- Data -> Transport pojazdu
	begin
		
		begin 
			DECLARE @values6 Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

			insert into @values6	
			EXEC	dbo.p_attribute_get2
				@attributePath = '62,318',
				@groupProcessInstanceId =@groupProcessId

			select @finishedAt=value_date   from @values6 
		end 
	end
	else if @prcId='1021' -- Data -> Dealer Call's
	begin
		select top 1 @finishedAt=created_at from dbo.process_instance with(nolock) where root_id=@rootId order by id
	end
	else -- Data -> pozostałe przypadki
	begin	
		select @finishedAt=(select	top 1 ss.created_at  
		from	dbo.service_status ss with(nolock) inner join 
			dbo.service_status_dictionary ssd with(nolock) on ss.status_dictionary_id=ssd.id and ssd.progress=4
		where	ss.group_process_id=@groupProcessId
		order by ss.id )		
	end

	/*******************************/
	-- 2018-06-20 Poprawnienie pobierania daty oraz interpretowania dodatku nocnego (KR) 2H
	declare @startedAt datetime	
	if @entityId in (1,10)
	begin
		set @startedAt = (SELECT TOP 1 date_enter FROM process_instance with(nolock) WHERE group_process_id = @groupProcessId AND root_id = @rootId AND step_id IN ('1009.018','1009.071') order by date_enter asc)
	end

	if @startedAt is null
	begin
		set @startedAt = (SELECT TOP 1 date_enter FROM process_instance with(nolock) WHERE group_process_id = @groupProcessId AND root_id = @rootId order by date_enter asc)
	end

	declare @startHour int = datepart(HH, @startedAt)
	declare @night bit -- Dodatek nocny za zlecenie prac od 22 do 6

	if @startHour>=22 or @startHour<6
	begin
		set @night=1
		print dbo.f_translate('Dodatek nocny: ',default) + CAST(@night AS NVARCHAR(1))
	end
	else
	begin
		set @night=0
	end

	IF @programId IN (402,414,415,416,417,418,419,420,421,424,425,426)--VGP bez Seat'a
	BEGIN
		SELECT TOP 1 @headerZZ = id FROM sync.ZamNag with(nolock) WHERE partnerLocId = @partnerId AND type = @type and rootId=@rootId and XL_typ='ZZ' and AUD_WynikAPI is null
	END
	ELSE IF @platformFromProgram IN (79,80)
	BEGIN
		select TOP 1 @headerZZ=zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
		where zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZZ' and zn.AUD_WynikAPI is null and zn.type = @type AND zn.XL_KntAkronim = @partnerStarterCode AND groupProcessId=@groupProcessId AND zn.XL_FlagaSagai=@XL_FlagaSagai
	END
	ELSE -- Pozostałe platformy
	BEGIN	
		select TOP 1 @headerZZ=zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
		where zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZZ' and zn.AUD_WynikAPI is null and zn.type = @type AND zn.XL_KntAkronim = @partnerStarterCode
	END

	-- Wykonywał pojazd (1 -> Laweta, 2 -> Patrolówka)----------------------------------------------------------
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,273', @groupProcessInstanceId = @groupProcessId
	declare @towVehicleType int = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)
	------------------------------------------------------------------------------------------------------------ 

	IF @entityId IN (1,10) AND (@emptyService = 1 OR (@noFix = 1 and @towVehicleType=2)) OR (@platformFromProgram=78 AND @noFix=1) OR (@programId=423 AND (@noFix=1 OR @emptyService=1)) --> Naprawa nieudana dla lewety Carefleet
	begin
		set @onlyEmptyService=1
		if @emptyService = 1
		begin
			print '> Pusty wyjazd'
			set @description = @description + dbo.f_translate(' Pusty wyjazd',default)
		end
		else
		begin
			print '> Naprawa nieudana'
			set @description = @description + dbo.f_translate(' Naprawa nieudana',default)
		end

		if @programId NOT IN (483,482,481,480,479,478,477,476,475,497,496,495,494,446,445,444,443) AND @platformFromProgram not in (18,35,   79,80,81,82) OR @programId=423 --@platformId NOT IN (18,35) /* Bez Concordia i TUW TUZ, PSA rozliczamy pusty wyjazd i naprawę nieudaną. */
		begin
			set @AkronimZS = '111111'
			set @headerZZ = NULL
		end
		else
		begin
			if @noFix = 1 and @towVehicleType = 2 -- naprawa nieudana i patrolówka
			begin
				set @AkronimZS = '111111'
				set @headerZZ = NULL

				IF @platformFromProgram IN (79,80,81,82) --> PSA, importujemy od razu naprawy nieudane, ponieważ nie idą przez Sagai
				BEGIN
					set @XL_FlagaSagai = 0
				END
			end
		end

		if @platformFromProgram = 25 and @emptyService = 1 --> Pusty wyjazd dla Leaseplan na akronim LP
		begin
			set @AkronimZS = '102800'
		end

		if @platformFromProgram = 48 and @emptyService = 1 --> Pusty wyjazd dla Business Lease na akronim BL
		begin
			set @AkronimZS = '104000'
		end

		if @platformFromProgram = 53 and @emptyService = 1 --> Pusty wyjazd dla Alphabet na akronim ALF
		begin
			set @AkronimZS = '105000'
		end

		if @platformFromProgram = 5 and ((@noFix = 1 and @towVehicleType=2) or @emptyService = 1) --> ADAC
		begin
			set @AkronimZS = '100264'
		end

		if @programId = 500 and @noFix = 1 and @towVehicleType=2 --> OAMTC
		begin
			set @AkronimZS = '100044'
		end

		if @platformFromProgram = 78 --> Carefleet
		begin
			SET @AkronimZS = CASE WHEN @programName LIKE 'IPA%dbo.f_translate(' THEN ',default)107500dbo.f_translate(dbo.f_translate(' ELSE ',default),default)107400' END
		end
	end

	PRINT '@AkronimZS = ' + @AkronimZS

	IF NOT (@noFix = 1 and @towVehicleType = 1) OR @platformFromProgram in (5,78) --> ADAC
	BEGIN
		PRINT '<*> Tworzenie nagłówków:'
		IF @headerZZ IS NULL
		BEGIN
			print dbo.f_translate(' - Dodaję nowy nagłówek ZZ.',default)
			INSERT INTO sync.ZamNag (
				AUD_GidTyp, -- 960
				XL_DataRealizacji, -- data statusu progres 4
				XL_DokumentObcy, -- caseId
				XL_KntAkronim,--XL_NabywcaDocelowyKntAkronim, -- kod starter
				XL_Opis, -- ProgramName
				XL_Typ, -- ZS/ZZ
				XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
				rootId, -- rootId
				type,
				partnerLocId,
				XL_DataWystawienia,
				XL_TargetKntAkronim,
				XL_ExpoNorm,
				XL_FlagaNB,
				XL_FlagaSagai
			)
			SELECT 
			@gidTypeZZ, 
			getdate(),--@finishedAt, 
			@caseId, 
			@partnerStarterCode, 
			@description, 
			'ZZ',
			@serieZZ,	
			@rootId,
			@type,
			@partnerId,
			isnull(@finishedAt,getdate()),
			'',
			@XL_ExpoNormZZ,
			@XL_FlagaNBZZ,
			@XL_FlagaSagai

			SET @headerZZ = SCOPE_IDENTITY()
			print '@headerZZ='+cast(isnull(@headerZZ,-999) as varchar(20))
		
			set @headerZS=null

			IF @programId IN (402,414,415,416,417,418,419,420,421,424,425,426)--VGP bez Seat'a
			BEGIN
				SELECT @headerZS = id FROM sync.ZamNag with(nolock) WHERE XL_KntAkronim=@AkronimZS and  rootId=@rootId and XL_typ='ZS' and AUD_WynikAPI is null
			END
			ELSE IF @platformFromProgram IN (79,80,81,82) --> Citroen, Peugeot » Każda grupa usług posiada osobny ZS.
			BEGIN
				select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
				where zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null and XL_KntAkronim=@AkronimZS and groupProcessId=@groupProcessId AND zn.XL_FlagaSagai=@XL_FlagaSagai
			END
			ELSE -- Pozostałe platformy
			BEGIN	
				select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
				where zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null and XL_KntAkronim=@AkronimZS
			END

			IF @platformFromProgram = 25 --> 25 LEASEPLAN
			BEGIN
				IF @entityId = 12 AND @atrybutLeaseplan like '%EIIE%' --> 9901-Inne
				BEGIN
					set @headerZS = null
					set @description = @programName + ' » LP'
				END
				ELSE
				BEGIN
					select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
					where zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null and XL_Opis not like '%LP%' and XL_KntAkronim=@AkronimZS
				END
			END

			if @headerZS is null
			begin
				print dbo.f_translate(' - Dodaję nagłówek ZS',default)
				INSERT INTO sync.ZamNag (
					ParentZsId,
					AUD_GidTyp, -- 960
					XL_DataRealizacji, -- data statusu progres 4
					XL_DokumentObcy, -- caseId
					XL_KntAkronim,
					XL_Opis, -- Program name
					XL_Typ, -- ZS/ZZ
					XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
					rootId,--SO_SprawaId, -- rootId
					type,
					partnerLocId,
					XL_DataWystawienia,
					XL_TargetKntAkronim,
					XL_ExpoNorm,
					XL_FlagaNB,
					XL_FlagaSagai
				)
				SELECT 
				null,
				@gidTypeZS, 
				getdate(),--@finishedAt, 
				@caseId,  
				@AkronimZS, 
				@description, 
				'ZS',
				@serieZS,	
				@rootId,
				@type,
				@partnerId,
				@caseDate,
				@akronimTarget,
				@XL_ExpoNormZS,
				@XL_FlagaNBZS,
				@XL_FlagaSagai

				SET @headerZS = SCOPE_IDENTITY()
			end
		
			print '@headerZS='+cast(isnull(@headerZS,-999) as varchar(20))
		
			UPDATE sync.ZamNag set ParentZsId=@headerZS where id=@headerZZ

		END
		ELSE -- Jesli Nagłówek ZZ już istnieje nagłówek dla kontrahenta to dopisuje pozycje zamówienia do istniejącego nagłówka
		BEGIN
			--SELECT @headerZS = id FROM sync.ZamNag WHERE XL_KntAkronim=@AkronimZS and  rootId=@rootId and XL_typ='ZS' and AUD_WynikAPI is null

			select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
			where XL_KntAkronim=@AkronimZS AND zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null

			IF @platformFromProgram = 25 --> 25 LEASEPLAN
			BEGIN
				IF @entityId = 12 AND @atrybutLeaseplan like '%EIIE%' --> 9901-Inne
				BEGIN
					set @headerZS = null
					set @description = @programName + ' » LP'
				END
				ELSE
				BEGIN
					select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
					where XL_KntAkronim=@AkronimZS AND zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null and XL_Opis not like '%LP%'
				END
			END
			ELSE IF @platformFromProgram IN (79,80,81,82) --> Citroen, Peugeot » Każda grupa usług posiada osobny ZS.
			BEGIN
				SET @headerZS = NULL
				select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
				where XL_KntAkronim=@AkronimZS AND zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null and groupProcessId=@groupProcessId AND zn.XL_FlagaSagai=@XL_FlagaSagai
				
				PRINT '@headerZS' + ISNULL(CAST ( @headerZS AS NVARCHAR(20)),dbo.f_translate('NULL',default))
				PRINT '@groupProcessId' + ISNULL(CAST ( @groupProcessId AS NVARCHAR(20)),dbo.f_translate('NULL',default))
				PRINT '@AkronimZS' + ISNULL(CAST ( @AkronimZS AS NVARCHAR(20)),dbo.f_translate('NULL',default))
				PRINT '@programId' + ISNULL(CAST ( @programId AS NVARCHAR(20)),dbo.f_translate('NULL',default))
			END

			-- KR
			if @headerZS is null --and @type = 5
			begin
				print dbo.f_translate(' - Dodaję nagłówek ZS',default)
				INSERT INTO sync.ZamNag (
					ParentZsId,
					AUD_GidTyp, -- 960
					XL_DataRealizacji, -- data statusu progres 4
					XL_DokumentObcy, -- caseId
					XL_KntAkronim,
					XL_Opis, -- Program name
					XL_Typ, -- ZS/ZZ
					XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
					rootId,--SO_SprawaId, -- rootId
					type,
					partnerLocId,
					XL_DataWystawienia,
					XL_TargetKntAkronim,
					XL_ExpoNorm,
					XL_FlagaNB,
					XL_FlagaSagai
				)
				SELECT 
				null,
				@gidTypeZS, 
				getdate(),--@finishedAt, 
				@caseId,  
				@AkronimZS, 
				@description, 
				'ZS',
				@serieZS,	
				@rootId,
				@type,
				@partnerId,
				@caseDate,
				@akronimTarget,
				@XL_ExpoNormZS,
				@XL_FlagaNBZS,
				@XL_FlagaSagai

				SET @headerZS = SCOPE_IDENTITY()
			end
			-- /KR

			--SELECT dbo.f_translate('jest',default), @headerZS headerZS, @AkronimZS AkronimZS, @entityId entityId
		END 
	END

	IF @platformFromProgram = 48 --> Business Lease
	BEGIN
		IF EXISTS (SELECT data FROM f_split(@partnerKinds,',') WHERE data = '188') --> BL blach.-lak.
		BEGIN
			SET @skipStarterCodeBL=1
		END
	END

	print dbo.f_translate('Dodaję ',default)+@name + dbo.f_translate(' - ZZ typ:',default)+cast(@type as varchar(10))+dbo.f_translate(' headerZZ:',default)+cast(isnull(@headerZZ,-999) as varchar(100)) 
	--- element ZZ
	----------------------------------------------------------
	-- tutaj wpisać wszelkie zmienne i dane apropos entity ZZ

	declare @towingKm DECIMAL(20,4) = 0	-- Kilometry holowania
	declare @arrivalKm DECIMAL(20,4) = 0-- Kilometry dojazdu
	declare @backKm DECIMAL(20,4) = 0	-- Kilometry powrotu

	if @entityId in (1,8,10) -- Naprawa lub holowanie, Liczba osób transp. w lawecie powyżej 2 » Pobieranie liczby kilometrów.
	begin
		SET	@towingKm = (SELECT TOP 1 value_decimal FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path IN ('638,225','767,782') AND value_decimal IS NOT NULL)
		SET	@towingKm = ROUND(ISNULL(@towingKm,0),0) 	-- Zaokrąglanie kilometrów

		SET	@arrivalKm = (SELECT TOP 1 value_decimal FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path ='638,217' AND value_decimal IS NOT NULL)
		SET	@arrivalKm = ROUND(ISNULL(@arrivalKm,0),0) 	-- Zaokrąglanie kilometrów
		
		SET	@backKm = (SELECT TOP 1 value_decimal FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path ='638,218' AND value_decimal IS NOT NULL)
		SET	@backKm = ROUND(ISNULL(@backKm,0),0) 	-- Zaokrąglanie kilometrów
	end

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,76', @groupProcessInstanceId = @groupProcessId
	DECLARE @dmc INT = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)
	IF @dmc IS NULL
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,76', @groupProcessInstanceId = @groupProcessId
		SET @dmc = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)
	END

	PRINT '@dmc:' + ISNULL(CAST(@dmc AS NVARCHAR(40)),dbo.f_translate('NULL',default))


	declare @markaModelAutaWyp int = null
	declare @class int = null
	declare @summaryQuantity int = null

	set @value=0.0

	BEGIN -- ZZ
		set @currency=dbo.f_translate(dbo.f_translate('PLN',default),default)

		if @entityId=1 -- holowanie 
		begin
			set @twrCode='02'	
			set @isTowing = 1 --> Flaga dla BL, potrzeba do opłaty za sprawę.

			if @towVehicleType=2 -- pojazd patrolowy
			----	
			begin
				if @emptyService=1 or @noFix=1
				begin 
					set @onlyEmptyService=1
					if @type IN (1,2,4,5) 
					begin					
						set @type=4 
						--set @quantity=1
					end
					--else
					--begin
					--	set @quantity=null
					--end
					set @value=70.0
					---------------------------------------------------------
					--if @emptyService = 1
					--begin
					--	insert into @descrTable
					--	select @headerZZ,dbo.f_translate('Pusty wyjazd',default)

					--	insert into @descrTable
					--	select @headerZS,dbo.f_translate('Pusty wyjazd',default)
					--end
					--else if @noFix=1
					--begin
					--	insert into @descrTable
					--	select @headerZZ,dbo.f_translate('Naprawa nieudana',default)

					--	insert into @descrTable
					--	select @headerZS,dbo.f_translate('Naprawa nieudana',default)
					--end
				end
				else
				begin
					if @arrivalKm+@towingKm<=30
					begin
						if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
						begin
							set @value=(@arrivalKm+@towingKm)*3
						end
						else
						begin
							set @value=160.0
						end
					end
					else
					begin
						if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
						begin
							set @value=(@arrivalKm+@towingKm)*3
						end
						else
						begin
							set @value=160.0+(@arrivalKm+@towingKm-30.0)*3
						end
					end

					if @night=1
					begin
						set @value=@value+30
					end
				end
			end
			else -- laweta
			begin
				print '@noFix: ' + CAST(@noFix AS nvarchar(5))
				print '@emptyService: ' + CAST(@emptyService AS nvarchar(5))
				if @emptyService=1 or @noFix=1
				begin
					set @onlyEmptyService=1
					if @type IN (1,2,4,5) 
					begin
						set @type=4 
					end

					if @emptyService = 1 or (@platformFromProgram = 78 and @noFix=1)
					begin
						set @value=70.0
					end
					else
					begin
						set @value=null
					end

					--insert into @descrTable
					--select @headerZZ,dbo.f_translate('Pusty wyjazd',default)
				
					--insert into @descrTable
					--select @headerZS,dbo.f_translate('Pusty wyjazd',default)
				end
				else
				begin
					declare @basePrice decimal(18,2)
					declare @kmPrice decimal(18,2)

					if @dmc<=2599
					begin
						set @basePrice=120.0
						set @kmPrice=4.0
						--select @arrivalKm, @towingKm arrivalKmtowingKm
					end		
					else if @dmc<=3399
					begin
						set @basePrice=155.00
						set @kmPrice=5.0
					end		
					else if @dmc<=5699
					begin
						set @basePrice=200.00
						set @kmPrice=6.0
					end		
					else
					begin
						set @basePrice=0.00
						set @kmPrice=0.0
						print '!!! DMC wykroczyło poza skalę!'
					end

					if @arrivalKm+@towingKm<=45
					begin
						if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
						begin
							set @value=(@arrivalKm+@towingKm)*@kmPrice
						end
						else
						begin
							set @value=@basePrice
						end
					end
					else
					begin
						if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
						begin
							set @value=(@arrivalKm+@towingKm)*@kmPrice
						end
						else
						begin
							set @value=@basePrice+(@arrivalKm+@towingKm-45.0)*@kmPrice
						end
					end

					if @night=1
					begin
						set @value=@value+30
					end
				end
			end

			if @platformFromProgram=78 and @isBRS=1 and @noFix=0 and @emptyService=0--> BRS Carefleet Hol
			begin
				if @towingKm <= 30
				begin
					set @value=150.00
				end
				else
				begin
					set @value=200.00
				end
			end

			if @isTrialPrice = 1 --> Cennik Testowy dla wybranych kontraktorów
			begin
				print dbo.f_translate('Cennik testowy.',default)

				if @towVehicleType=2 -- pojazd patrolowy
				----	
				begin
					/*
					if @emptyService=1 or @noFix=1
					begin 
						set @onlyEmptyService=1
						if @type IN (1,2,4,5) 
						begin					
							set @type=4 
							--set @quantity=1
						end
						--else
						--begin
						--	set @quantity=null
						--end
						set @value=70.0
						---------------------------------------------------------
						--if @emptyService = 1
						--begin
						--	insert into @descrTable
						--	select @headerZZ,dbo.f_translate('Pusty wyjazd',default)

						--	insert into @descrTable
						--	select @headerZS,dbo.f_translate('Pusty wyjazd',default)
						--end
						--else if @noFix=1
						--begin
						--	insert into @descrTable
						--	select @headerZZ,dbo.f_translate('Naprawa nieudana',default)

						--	insert into @descrTable
						--	select @headerZS,dbo.f_translate('Naprawa nieudana',default)
						--end
					end
					else
					begin
						if @arrivalKm+@towingKm<=30
						begin
							if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
							begin
								set @value=(@arrivalKm+@towingKm)*3
							end
							else
							begin
								set @value=160.0
							end
						end
						else
						begin
							if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
							begin
								set @value=(@arrivalKm+@towingKm)*3
							end
							else
							begin
								set @value=160.0+(@arrivalKm+@towingKm-30.0)*3
							end
						end

						if @night=1
						begin
							set @value=@value+30
						end
					end
					*/

					set @value = 0.00 --> Nie ma cennika dla pojazdu patrolowego
					print dbo.f_translate('Nie ma cennika dla pojazdu patrolowego',default)

				end
				else -- laweta
				begin
					print '@noFix: ' + CAST(@noFix AS nvarchar(5))
					print '@emptyService: ' + CAST(@emptyService AS nvarchar(5))
					if @emptyService=1 or @noFix=1
					begin
						set @onlyEmptyService=1
						if @type IN (1,2,4,5) 
						begin
							set @type=4 
						end

						if @emptyService = 1
						begin
							set @value=70.0
						end
						else
						begin
							set @value=null
						end

						--insert into @descrTable
						--select @headerZZ,dbo.f_translate('Pusty wyjazd',default)
				
						--insert into @descrTable
						--select @headerZS,dbo.f_translate('Pusty wyjazd',default)
					end
					else
					begin
						if @emptyService=1 or @noFix=1
						begin
							set @onlyEmptyService=1
							if @type IN (1,2,4,5) 
							begin
								set @type=4 
							end

							if @emptyService = 1
							begin
								set @value=70.0
							end
							else
							begin
								set @value=null
							end
						end
						else
						begin
							if @arrivalKm+@towingKm<=35
							begin
								if @dmc<=2599
								begin
									set @value=115.0				
								end		
								else if @dmc<=3399
								begin
									set @value=130.00
								end		
								else if @dmc<=5699
								begin
									set @value=170.00
								end
								else
								begin
									set @value=0.00
									print '!!! DMC wykroczyło poza skalę!'
								end
							end
							else --> Dojazd-Powrót powyżej 35km
							begin
								if @dmc<=2599
								begin
									if (@arrivalKm+@towingKm+@backKm)*2 < 115
									begin
										set @value=115.0			
									end
									else
									begin
										set @value = (@towingKm+@arrivalKm+@backKm)*2
									end			
								end		
								else if @dmc<=3399
								begin
									if (@arrivalKm+@towingKm+@backKm)*2 < 130
									begin
										set @value=130.0			
									end
									else
									begin
										set @value = (@towingKm+@arrivalKm+@backKm)*2.5
									end			
								end		
								else if @dmc<=5699
								begin
									if (@arrivalKm+@towingKm+@backKm)*2 < 170
									begin
										set @value=170.0			
									end
									else
									begin
										set @value = (@towingKm+@arrivalKm+@backKm)*3
									end			
								end	
								else
								begin
									set @value=0.00
									print '!!! DMC wykroczyło poza skalę!'
								end
							end
						end
					end
				end
			end

			if @isTireRepair = 1
			begin
				set @value=80
			end
		end 

		if @entityId=2 -- holowanie przyczepy
		begin
			print dbo.f_translate('na razie nie ma',default)
		end 

		if @entityId=3 -- roboczogodziny
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			set @value=20.00 --*@value
			--select @value
		end 

		if @entityId=4 -- dokumentacja
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			IF @platformFromProgram IN (25,48) --> 25 Leaseplan, 48 Bussiness Lease
			BEGIN
				if dbo.f_rsa_photos(@groupProcessId)=1
				begin
					set @value=20.0 --> Zdjęcia tylko w LP
				end
				else --> Brak zgody wynikająca z wytycznych.
				begin
					set @value=null
					print dbo.f_translate('brak zgody na zdjęcia',default)
				end
			END
			ELSE
			BEGIN
				set @value=NULL
			END
		end 

		if @entityId=5 -- koszty autostrady
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
			print dbo.f_translate('koszty wpisane w raport nh',default)
		
			declare @structureId int
			declare @TypKosztu int 

			DECLARE kur cursor LOCAL for
				select  id 
				from      dbo.attribute_value with(nolock) 
				where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

			OPEN kur;
			FETCH NEXT FROM kur INTO @structureId;
			WHILE @@FETCH_STATUS=0
			BEGIN 
				delete from @values     
				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '638,219,220', --Typ
						@groupProcessInstanceId = @groupProcessId,
						@parentAttributeId=@structureId
                               
					SELECT @TypKosztu=value_int FROM @values

					if @TypKosztu=1 /*autostrada*/
					begin
						delete from @values     
						INSERT  @values EXEC dbo.p_attribute_get2
								@attributePath = '638,219,222', /*wartość*/
								@groupProcessInstanceId = @groupProcessId,
								@parentAttributeId=@structureId

						SELECT @value=value_decimal FROM @values
									
					end
				FETCH NEXT FROM kur INTO @structureId;
			END
			CLOSE kur
			DEALLOCATE kur
		end 

		if @entityId=6 -- wyciąganie z poza drogi
		begin
			print dbo.f_translate('koszt ',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
			set @value=0.0
		end 

		if @entityId=7 -- DO UZUPEŁNIENIA!!! 1301-Części zamienne
		begin
			set @twrCode='01'
			set @value=0.0
		end

		if @entityId=8 -- transport w holowniku
		begin
			set @twrCode='02'
		
			print dbo.f_translate('wg cennika',default)
			declare @persons int
			set @persons=0

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,644,154', @groupProcessInstanceId = @groupProcessId
			SELECT @persons = value_int FROM @values	
		
			if @persons>2
			begin
				set @value=@towingKm*1.5
			end
		end 

		if @entityId=9 -- viatoll
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			DECLARE kur cursor LOCAL for
				select  id 
				from      dbo.attribute_value with(nolock) 
				where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

			OPEN kur;
			FETCH NEXT FROM kur INTO @structureId;
			WHILE @@FETCH_STATUS=0
			BEGIN 
				delete from @values     
				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '638,219,220', --Typ
						@groupProcessInstanceId = @groupProcessId,
						@parentAttributeId=@structureId
                               
					SELECT @TypKosztu=value_int FROM @values

					if @TypKosztu=2 /*viatoll*/
					begin
						delete from @values     
						INSERT  @values EXEC dbo.p_attribute_get2
								@attributePath = '638,219,222', /*wartość*/
								@groupProcessInstanceId = @groupProcessId,
								@parentAttributeId=@structureId

						SELECT @value=value_decimal FROM @values
									
					end
				FETCH NEXT FROM kur INTO @structureId;
			END
			CLOSE kur
			DEALLOCATE kur
		end 

		if @entityId=10 -- naprawa na drodze
		begin
			print dbo.f_translate('wg cennika',default)
			set @twrCode='01'

			if @towVehicleType=2 -- pojazd patrolowy
			begin
				if @emptyService=1 or @noFix=1
				begin
					set @onlyEmptyService=1
					if @type IN (1,2,4,5) 
					begin
						set @type=4	
					end
					set @value=70.0

					print dbo.f_translate('Pusty wyjazd lub naprawa nieudana. Value: ',default) + ISNULL(CAST(@value AS NVARCHAR(30)),dbo.f_translate('NULL',default))
				end
				else
				begin
					if @arrivalKm<=30
					begin
						set @value=160.0
					end
					else
					begin
						set @value=160.0+(@arrivalKm-30.0)*3
					end

					if @night=1
					begin
						set @value=@value+30
					end
				end
			end
			else -- laweta
			begin	
				if @emptyService=1 or @noFix=1
				begin
					set @onlyEmptyService=1
					if @type IN (1,2,4,5) 
					begin
						set @type=4 
					end

					if @emptyService = 1 or (@platformFromProgram = 78 and @noFix=1)
					begin
						print dbo.f_translate('Uwzględnia',default)
						set @value=70.0
					end
					else
					begin
						set @value=null
					end
				end
				else
				begin
					if @dmc<=2599
					begin
						set @basePrice=150.0
						set @kmPrice=4.0					
					end		
					else if @dmc<=3399
					begin
						set @basePrice=190.00
						set @kmPrice=5.0
					end		
					else if @dmc<=5699
					begin
						set @basePrice=240.00
						set @kmPrice=6.0
					end		
					else
					begin
						set @basePrice=0.00
						set @kmPrice=0.0
						print '!!! DMC wykroczyło poza skalę!'
					end

					if @arrivalKm<=45
					begin
						set @value=@basePrice				
					end
					else
					begin
						set @value=@basePrice+(@arrivalKm-45.0)*@kmPrice
					end

					if @night=1
					begin
						set @value=@value+30
					end
				end
			end		
		
			if @isTrialPrice = 1 --> Cennik Testowy dla wybranych kontraktorów
			begin
				print dbo.f_translate('Cennik testowy.',default)

				if @towVehicleType=2 -- pojazd patrolowy
				begin
					/*
					if @emptyService=1 or @noFix=1
					begin
						set @onlyEmptyService=1
						if @type IN (1,2,4,5) 
						begin
							set @type=4	
						end
						set @value=70.0

						print dbo.f_translate('Pusty wyjazd lub naprawa nieudana. Value: ',default) + ISNULL(CAST(@value AS NVARCHAR(30)),dbo.f_translate('NULL',default))
					end
					else
					begin
						if @arrivalKm<=30
						begin
							set @value=160.0
						end
						else
						begin
							set @value=160.0+(@arrivalKm-30.0)*3
						end

						if @night=1
						begin
							set @value=@value+30
						end
					end
					*/
					set @value = 0.00 --> Nie ma cennika dla pojazdu patrolowego
					print dbo.f_translate('Nie ma cennika dla pojazdu patrolowego',default)
				end
				else -- laweta
				begin	
					if @emptyService=1 or @noFix=1
					begin
						set @onlyEmptyService=1
						if @type IN (1,2,4,5) 
						begin
							set @type=4 
						end

						if @emptyService = 1
						begin
							set @value=70.0
						end
						else
						begin
							set @value=null
						end
					end
					else
					begin
						if @arrivalKm+@backKm<=35
						begin
							if @dmc<=2599
							begin
								set @value=145.0				
							end		
							else if @dmc<=3399
							begin
								set @value=160.00
							end		
							else if @dmc<=5699
							begin
								set @value=200.00
							end
							else
							begin
								set @value=0.00
								print '!!! DMC wykroczyło poza skalę!'
							end
						end
						else --> Dojazd-Powrót powyżej 35km
						begin
							if @dmc<=2599
							begin
								if (@arrivalKm+@backKm)*2 < 145
								begin
									set @value=145.0			
								end
								else
								begin
									set @value = (@arrivalKm+@backKm)*2
								end			
							end		
							else if @dmc<=3399
							begin
								if (@arrivalKm+@backKm)*2 < 160
								begin
									set @value=160.0			
								end
								else
								begin
									set @value = (@arrivalKm+@backKm)*2.5
								end			
							end		
							else if @dmc<=5699
							begin
								if (@arrivalKm+@backKm)*2 < 145
								begin
									set @value=145.0			
								end
								else
								begin
									set @value = (@arrivalKm+@backKm)*2
								end			
							end	
							else
							begin
								set @value=0.00
								print '!!! DMC wykroczyło poza skalę!'
							end
						end

						if @night=1
						begin
							set @value = @value + 30
						end

						set @value = @value + 30 --> Dodatek za skuteczną naprawę.
					end
				end		
			end

			if @isTireRepair = 1
			begin
				set @value=80
			end
		end 

		if @entityId=11 -- taxi
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			declare @km int

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,154', @groupProcessInstanceId = @groupProcessId
			SELECT @persons = value_int FROM @values	

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,647', @groupProcessInstanceId = @groupProcessId
			SELECT @km = value_int FROM @values	

	
			set @value=@km*1.5

			if @night=1
			begin
				set @value=@value+30
			end
		end 

		if @entityId=12 -- Nowość --> 9901-Inne
		begin
			DECLARE @serviceStatusId INT = NULL

			DELETE FROM @values
			INSERT	@values EXEC dbo.p_attribute_get2 
					@attributePath = '560', 
					@groupProcessInstanceId = @groupProcessId
			declare @NH int = (Select top 1 value_int from @values where value_int is not null)

			IF @NH IS NOT NULL -- Naprawa czy Holowanie
			BEGIN
				SET @serviceStatusId = (select top 1 serviceId from service_status WITH (NOLOCK) where group_process_id = @groupProcessId and serviceId = @NH order by updated_at DESC)
			END
			ELSE
			BEGIN
				SET @serviceStatusId = (select top 1 serviceId from service_status WITH (NOLOCK) where group_process_id = @groupProcessId order by updated_at DESC)
			END

			SET @twrCode = case when @serviceStatusId = 1  then '02' -- Holowanie
								when @serviceStatusId = 2  then '01' -- Naprawa
								when @serviceStatusId = 3  then '04' -- Auto zastępcze
								when @serviceStatusId = 4  then '05' -- Nocleg
								when @serviceStatusId = 5  then '07' -- Taxi
								when @serviceStatusId = 6  then '06' -- Podróż
								when @serviceStatusId = 7  then '10' -- Transport
								when @serviceStatusId = 8  then '04' -- Części zamienne
								when @serviceStatusId = 9  then '17' -- Kredyt
								when @serviceStatusId = 10 then '19' -- Przekazywanie informacji
								--when @serviceStatusId = 11 then '' -- 
								when @serviceStatusId = 12 then '0' + ISNULL(CAST(@NH AS nvarchar(2)),'0') -- Monit. napr. warszt.
								when @serviceStatusId = 13 then '00' -- Odwołanie usługi
								when @serviceStatusId = 14 then '0' + ISNULL(CAST(@NH AS nvarchar(2)),'0') -- Parking
								when @serviceStatusId = 15 then '02'  -- Holowanie poszkodowanego
								when @serviceStatusId = 16 then '11' -- Trans. nienapr. pojazdu
								when @serviceStatusId = 17 then '24' -- Home Assistance
								when @serviceStatusId = 18 then '16' -- Medical
								when @serviceStatusId = 19 then '00' -- Porada ALPHABET - Lokalizacja serwisu
								when @serviceStatusId = 20 then '00' -- Porada ALPHABET - udzielenie informacji
								when @serviceStatusId = 21 then '00' -- Złomowanie
							else '00' end

			EXEC [dbo].[p_f_other_costs] @groupId=@groupProcessId,@value = @value OUTPUT

			-- Jeśli naprawa lub holowanie i są dodatkowe koszty to dokładamy je do pozycji inne.
			IF (SELECT DISTINCT TOP 1 LEFT(step_id,4) FROM [dbo].[process_instance] with(nolock) WHERE group_process_id = @groupProcessId) = 1009 AND @value >= 70
			BEGIN
				SET @value = @value - 70
			END
		end 

		if @entityId=14 -- dodatkowe koszty
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			DECLARE @exist INT
			IF (SELECT COUNT(1) FROM [dbo].[process_instance] with(nolock) WHERE step_id LIKE '1018%' AND group_process_id=@groupProcessId) > 0 
			BEGIN --> Transport
				SET @exist=1
				DELETE FROM @values
				INSERT @values EXEC dbo.p_attribute_get2
							@attributePath = '62,319',
							@groupProcessInstanceId =@groupProcessId
				SET @value = (SELECT value_int from @values)

				DECLARE @CostType VARCHAR(20)
				DELETE FROM @values
				INSERT @values EXEC dbo.p_attribute_get2
							@attributePath = '62,747',
							@groupProcessInstanceId =@groupProcessId
				select @CostType=value_string from @values

				IF (@CostType = dbo.f_translate('brutto',default))
				BEGIN
					SET @value = @value / 1.23
				END
				ELSE
				BEGIN
					SET @value = @value
				END
			END
			ELSE
			BEGIN --> Holowanie lub Naprawa
				EXEC [dbo].[p_f_towing_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=6,@value=@value OUTPUT
			END
		end 

		if @entityId=16 -- dźwig
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			if @dmc<=2599
			begin
				set @value=150.0		
			end		
			else if @dmc<=3399
			begin
				set @value=170.0
			end		
			else if @dmc<=5699
			begin
				set @value=170.0
			end		
		end 

		if @entityId=17 -- NpT
		begin
			set @twrCode='01'
			set @value=0
		end 

		if @entityId=18 -- Kredyt
		begin
			set @twrCode='17'
		
			set @value=NULL -- KR Zawsze jest brak ZZ dla kredytu
		end 

		if @entityId=19 -- lot
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='06'
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '147,746', @groupProcessInstanceId = @groupProcessId
			SELECT @value = value_decimal FROM @values			
		end 

		IF @entityId=21 -- Taxi
		BEGIN
			PRINT dbo.f_translate('koszt taxi wpisany',default)
			SET @twrCode='07'
			SET @value=0

			SET @value = (
				SELECT TOP 1 value_int FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId 
				AND attribute_path = ('152,319') AND value_int IS NOT NULL
			)

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '152,556', @groupProcessInstanceId = @groupProcessId
			SELECT @currency = value_string FROM @values	

			IF @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			BEGIN
				SET @value = dbo.FN_Exchange(@value,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate()) 
			END

			IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) <> dbo.f_translate(dbo.f_translate('Polska',default),default)
			BEGIN
				SET @value = 0
			END 
			ELSE
			BEGIN
				IF NOT EXISTS(SELECT 1 FROM process_instance WITH(NOLOCK) WHERE step_id = '1016.021' AND group_process_id=@groupProcessId)
				BEGIN
					PRINT dbo.f_translate('Proces Taxi odpalony z kafelka',default)
					IF ISNULL(@value,0)=0
					BEGIN -- Jeżeli koszty rzeczywiste są nie wpisane to zostaną wyliczone z kilometrów.
						DELETE FROM @values
						INSERT @values EXEC p_attribute_get2 @attributePath = '152,647', @groupProcessInstanceId = @groupProcessId
						DECLARE @taxiDistance INT = (SELECT value_int FROM @values)

						SET @value=50			
						IF @taxiDistance>20.0
						BEGIN
							SET @value=@value+(@taxiDistance-20.0)*3.0
						END
					END
				END
				SET @value = ISNULL(@value,0)
			END

			-- Sprawdzenie czy netto/brutto (20180816)
			DELETE FROM @values -- Zamiana z brutto na netto
			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '152,747', -- • Brutto/netto
					@groupProcessInstanceId = @groupProcessId                              
			SELECT @CostType=value_string FROM @values
			IF ((@CostType = dbo.f_translate('brutto',default)) AND ISNULL(@value,0)>0)
			BEGIN
				SET @value = @value /1.23 -- Zamiana kosztu brutto na netto
			END

			declare	@linkedService int = (select top 1 value_int from attribute_value with(nolock) where group_process_instance_id=@groupProcessId and attribute_path='856' and value_int is not null)		

			if @night=1 and @linkedService is null
			begin
				set @value=@value+30
			end
		END 

		if @entityId=22 -- części zamienne
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='13'
			set @value=0
		end 

		if @entityId=23 -- pociąg
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='06'
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '147,746', @groupProcessInstanceId = @groupProcessId
			SELECT @value = value_decimal FROM @values	
		end 

		if @entityId=25 -- parking
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
			EXEC	[dbo].[p_f_towing_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=5,@value=@value OUTPUT
			IF ISNULL(@value,0)=0 --> Parking z kafelka
			BEGIN
				SET @value=(	SELECT TOP 1 value_decimal FROM attribute_value WITH(NOLOCK) WHERE 
								group_process_instance_id=@groupProcessId AND attribute_path='862,746' AND value_decimal IS NOT NULL)
			END
		end 

		if @entityId=26 -- porada
		begin
			set @twrCode='19'
			set @value=null
			set @IsAdviceInCase = 1
		end 

		if @entityId=27 -- 1951-Formularz Ford Vignale
		begin
			set @twrCode='19'
			set @value=null
		end 

		if @entityId=29 /*wynajem auta. Jeśli wynajmuje auto serwis naprawiający to sam rozlicza kwoty wynajmu z producentem. */
		begin
			set @twrCode='04'
			declare @IleDni int
			EXEC [p_f_replacement_days] @groupProcessInstanceId=@groupProcessId,@quantity=@IleDni OUTPUT,@value=@value OUTPUT
			SET @value = (@value /  @IleDni)

			/*
			IF @platformFromProgram=78 and @isBRS=1 --> BRS Carefleet MW
			begin
				-----------------------
				SET @markaModelAutaWyp = (SELECT TOP 1 value_int FROM attribute_value WITH(NOLOCK)WHERE 
					group_process_instance_id=@groupProcessId AND attribute_path='764,73' AND value_int IS NOT NULL)

				SET @class = (SELECT TOP 1 argument3 FROm dbo.dictionary WITH(NOLOCK) WHERE typeD='makeModel' and value=@markaModelAutaWyp)

				if @partnerId is null
				begin
					print '@partnerId: ' +  isnull(cast(@partnerId as nvarchar(20)),dbo.f_translate('null',default))
					set @summaryQuantity = @Quantity
				end
				else
				begin
					set @summaryQuantity = (
						select SUM(av_quantity.value_int) as dbo.f_translate('Quantity',default)
						from   attribute_value av with(nolock) 
						join   attribute_value av_quantity with(nolock) on av_quantity.parent_attribute_value_id = av.id 
								and av_quantity.attribute_path='820,821,822,221'
						join   attribute_value av_groupId with(nolock) on av_groupId.id=av.parent_attribute_value_id
						join   attribute_value av_partner with(nolock) on av_partner.group_process_instance_id = av_groupId.value_int 
								and av_partner.attribute_path='764,742'
						join   attribute_value av_makeModel with(nolock) on av_makeModel.group_process_instance_id = av_groupId.value_int 
								and av_makeModel.attribute_path='764,73'
						join   dictionary d_class with(nolock) on d_class.value=av_makeModel.value_int and d_class.typeD='makeModel'
						where  av.attribute_path='820,821,822' and av.value_int=29 and av.root_process_instance_id=@rootId
								and av_partner.value_int=@partnerId and d_class.argument3=@class
					)
				end

				SET @value = CASE 
					WHEN @class IN (1,3) THEN CASE --> 'A' i 'B'	
						when @summaryQuantity <= 7 then 90
						when @summaryQuantity > 7 then 90 
						else null end
					WHEN @class IN (7) THEN CASE --> 'C'
						when @summaryQuantity <= 7 then 110
						when @summaryQuantity > 7 then 110
						else null end
					WHEN @class IN (11) THEN CASE --> 'D'
						when @summaryQuantity <= 7 then 150
						when @summaryQuantity > 7 then 150
						else null end
					WHEN @class IN (24) THEN CASE--> 'N'
						when @summaryQuantity <= 7 then 180
						when @summaryQuantity > 7 then 180
						else null end	
					ELSE 0.00 END --> KOSZT RZECZYWISTY
				
				print dbo.f_translate('KLASA WYNAJMOWANEGO AUTA: ',default) + CAST(@class AS NVARCHAR(10))
				------------------------
			end
			*/

			print dbo.f_translate('Wartość wynajmu całkowita: ',default) + cast (@value as varchar(20))
		end 

		if @entityId=30 -- podstawienie
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			-- Pobieranie wartości, Krzysztof
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,790,240,222', @groupProcessInstanceId = @groupProcessId
			SELECT @value = value_decimal FROM @values	
			--

		end 

		if @entityId=31 -- odbiór
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			-- Pobieranie wartości, Krzysztof
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,787,240,222', @groupProcessInstanceId = @groupProcessId
			SELECT @value = value_decimal FROM @values	
			--
		end 

		if @entityId=32 -- wynajm poza godzinami
		begin
			print dbo.f_translate('koszt wpisany / 20 pln',default)
			set @twrCode='04'

			-- Podstawienie poza godzinami pracy wypożyczalni
			declare @substitutionValue decimal(20,4) = 0
			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=1,@value=@substitutionValue output
			set @substitutionValue = isnull(@substitutionValue,0)

			-- Zwrot poza godzinami pracy wypożyczalni
			declare @returnValue decimal(20,4) = 0
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=2,@value=@returnValue output		
			set @returnValue = isnull(@returnValue,0)

			if (@substitutionValue != 0 AND @returnValue = 0) -- podstawienie
			begin
				set @value = @substitutionValue
			end

			else if (@substitutionValue = 0 AND @returnValue != 0) -- zwrot
			begin
				set @value = @returnValue
			end

			else if (@substitutionValue != 0 AND @returnValue != 0) -- podstawienie i zwrot
			begin
				set @value = @substitutionValue + @returnValue
			end
		end

		if @entityId=34 -- dodatkowe ubezp
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=4,@value=@value OUTPUT
		end 

		if @entityId=35 -- hak holowniczy
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=5,@value=@value OUTPUT
		end 

		if @entityId=36 -- fotelik
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=6,@value=@value OUTPUT
		end 

		if @entityId=37 -- bagażnik sam.
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=7,@value=@value OUTPUT
		end 

		if @entityId=38 -- wyjazd za granicę
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=3,@value=@value OUTPUT
		end 

		if @entityId=40 --> 0412-Dotankowanie
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=10,@value=@value output
		end 

		if @entityId=41 --> 0413-Uszkodzenie
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=9,@value=@value output
		end 

		if @entityId=42 --> 0414-Mycie
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=11,@value=@value output
		end 

		if @entityId=43 -- nocleg
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='05'

			EXEC [dbo].[p_f_accommodation] @groupProcessInstanceId=@groupProcessId,@value=@value OUTPUT
			SET @value = @value / @quantity
		end 

		if @entityId=44 -- śniadanie
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='05'

			EXEC [dbo].[p_f_breakfast] @groupProcessInstanceId=@groupProcessId,@value=@value OUTPUT
		end 

		if @entityId=63 -- transport MP  to jest netto ZZ
		begin
			SET @serviceStatusId = (select top 1 serviceId from service_status WITH (NOLOCK) where group_process_id = @groupProcessId order by updated_at DESC)
			SET @twrCode = case when @serviceStatusId = 7  then '10' -- Transport
								when @serviceStatusId = 16 then '11' -- Trans. nienapr. pojazdu
							else '10' end
			SET @isTransport = 1
			
			SELECT @value=value_int FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id =@groupProcessId AND attribute_path = '62,319'
			
			-- Sprawdzenie czy netto/brutto (20180816)
			IF (SELECT TOP 1 value_string FROM attribute_value WITH(NOLOCK) WHERE attribute_path='62,747' AND 
				group_process_instance_id=@groupProcessId AND value_string IS NOT NULL)=dbo.f_translate('brutto',default) AND ISNULL(@value,0)>0
			BEGIN
				SET @value = @value /1.23 -- Zamiana kosztu brutto na netto
			END
		end 

		if @entityId=64 -- Home Assistance
		begin
			set @twrCode='24'
			exec [dbo].[p_f_homeAssistance] @groupId=@groupProcessId,@value=@value output
		end 

		if @entityId=65 -- Medical
		begin
			set @twrCode='16'
			set @value=isnull((select top 1 value_decimal from attribute_value with(nolock) where group_process_instance_id=@groupProcessId 
						and attribute_path='991,746' and value_decimal is not null),0.00)
		end 

		if @entityId in (66,69,71,73) --> Usługi Truck
		begin
			set @twrCode = case when @entityId=66 then '31' --> 3101-Naprawa na drodze Truck
								when @entityId=69 then '32' --> Holowanie Truck
								when @entityId=71 then '33' --> Dowóz gotówki na inne potrzeby
								when @entityId=73 then '34' --> Naprawa mechaniczna Truck
								else '00' end
			set @value = (select top 1 value_decimal from attribute_value with(nolock) where group_process_instance_id=@groupProcessId and attribute_path='1028,1029' and value_decimal is not null)
			set @value = isnull(@value,0.00)

			IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))<>dbo.f_translate(dbo.f_translate('Polska',default),default) AND @entityId<>71 --> Za granicą tylko importuje się dowóz gotówki
			BEGIN
				set @value = NULL
			END
		end 

		if @entityId=75 --> Naprawa Warsztatowa SARC
		begin
			set @twrCode='NW'
			set @value = NULL
		end

		if @entityId=76 --> 1401-złomowanie
		begin
			set @twrCode='14'
			set @value = (select top 1 value_decimal from attribute_value WITH(NOLOCK) where group_process_instance_id=@groupProcessId and attribute_path = '953,219,222' and value_decimal is not null)
		end

		if @entityId in (1,10) and @isGopNH=1 --> Organizowanie usługi N/H na GOP.
		begin
			set @value=(select top 1 value_int from attribute_value with(nolock) where group_process_instance_id=@groupProcessId 
						and attribute_path in ('638,957','1005,957') and value_int is not null)

			--if @XL_FlagaNBZZ = 'B'
			--begin
			--??????????????????????????
			--end
		end

		/*	415	Audi Gwarancja Mobilności
			418	Volkswagen osobowe Gwarancja Mobilności
			420	Volkswagen użytkowe Gwarancja Mobilności
			421	Skoda Gwarancja Mobilności	
			Aso rozlicza swoje koszty z producentem - bez udziału Starter24		
		*/
		if (@type = 1 AND @czyASO = 1 AND @programId IN (415,418,420,421) AND @twrCode IN ('02','04')) 
		or @skipStarterCodeBL=1 
		or (@platformFromProgram = 5 and @twrCode='04') --> ADAC nie przenosimy wynajmów
		BEGIN
			set @value=NULL

			print 'Pomija wstawienie ZamElem(ZZ). (if (@type = 1 AND @czyASO = 1 AND @programId IN (415,418,420,421) AND @twrCode IN (''02'',''04'')) or @skipStarterCodeBL=1 ALBO MW DLA ADAC'
			print '@czyASO: ' + ISNULL(CAST(@czyASO AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@type: ' + ISNULL(CAST(@type AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@quantity: ' + ISNULL(CAST(@quantity AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@skipStarterCodeBL: ' + ISNULL(CAST(@skipStarterCodeBL AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@platformFromProgram: ' + ISNULL(CAST(@platformFromProgram AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@twrCode: ' + ISNULL(CAST(@twrCode AS NVARCHAR(50)),dbo.f_translate('NULL',default))
		END

		IF @programId IS NULL
		BEGIN
			set @value=NULL
			print 'BRAK PROGRAMU! nie importuje'
		END

		/* Waluty */
		IF @twrCode = '04' --> Wynajem Auta Zastępczego
		BEGIN
			SET @currency = ISNULL((SELECT TOP 1 value_string FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path='812,789,556'),dbo.f_translate(dbo.f_translate('PLN',default),default))
		END

	END

	print '@value: ' + ISNULL(CAST(@value AS NVARCHAR(30)),dbo.f_translate('NULL',default)) + ' @quantity: ' +  ISNULL(CAST(@quantity AS NVARCHAR(30)),dbo.f_translate('NULL',default))

	set @nettoBrutto='N'
	set @vatGroup='23'
	set @value=@value*@quantity 
	
	print dbo.f_translate('TwrCode - ',default)+cast(isnull(@twrCode,'---') as varchar(20))+' entityId='+cast(@entityId as varchar(20))+' value='+isnull(cast(@value as varchar(20)),dbo.f_translate('NULL',default))
	----------------------------------------------------------
	set @quantityZ=ceiling(@quantity)

	if @entityId<>17 /*wykluczenie NpT*/
	begin
		Print dbo.f_translate('ZZ: Koszt value przed dodaniem do elem: ',default) + ISNULL(CAST(@value AS NVARCHAR(20)),dbo.f_translate('NULL',default))

		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			groupProcessId,
			programId
		)	
		SELECT  
		@headerZZ,
		@quantityXL,    ---@quantityZ MP
		@twrCode,
		@value,
		isnull(@currency,dbo.f_translate(dbo.f_translate('PLN',default),default)),
		@entityValueId, 
		@entityId,
		@groupProcessId,--CASE WHEN (@reverseGroupId < 0) THEN @reverseGroupId ELSE @groupProcessId END,
		@programId
	end
	print @value

	set @currency = dbo.f_translate(dbo.f_translate('PLN',default),default)

	print '-----------------------------------------'

	------------------------------------------------------------
	-- Tutaj wpisać wszelkie zmienne i dane apropos entity ZS --
	------------------------------------------------------------

	declare @valueZS decimal(18,2)

	set @valueZS=0.0
	set @quantityZ=@quantity

	IF		@programId IN (368,402,414,415,416,417,418,419,420,421,424,425,426,461,462,463,464) or @platformFromProgram = 76 --> VGP or Cupra Assistance
	BEGIN
		IF @type IN (1,3,4,5) --> Producent, Dealer, Klient, PZ
		BEGIN
			if @entityId=1 -- 0201-Holowanie
			begin
				if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
				begin
					if @towingKm<=15 
					begin
						set @valueZS=174.74
					end
					else
					begin
						set @valueZS=174.74+(@towingKm-15.0)*4.62
					end	
				end
				else
				begin
					set @value=0.0
				end
			end

			if @entityId=3 -- 0205-Pomoc/Oczekiwanie
			begin
				set @valueZS=20.0
			end

			if @entityId=10 -- 0101-Naprawa na drodze
			begin
				if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
				begin
					set @valueZS=203.34
				end
				else
				begin
					set @valueZS=0.0
				end
			end

			if @entityId=16 -- 0203-Dźwig
			begin
				if @dmc<=2599
				begin
					set @valueZS=150.0		
				end		
				else if @dmc<=3399
				begin
					set @valueZS=170.0
				end		
				else if @dmc<=5699
				begin
					set @valueZS=170.0
				end
			end

			if @entityId=17 /*Naprawa przez telefon*/
			begin
				set @isRepairByPhone = 1				
				set @valueZS=110.74
			end

			if @entityId=18 -- Kredyt
			begin
				set @twrCode='17'
		
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '556', @groupProcessInstanceId = @groupProcessId
				SELECT @currency = value_string FROM @values	

				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '732', @groupProcessInstanceId = @groupProcessId
				SELECT @valueZS = value_decimal FROM @values	

				--if @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
				--begin
				--	set @valueZS = dbo.FN_Exchange(@valueZS,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate()) 
				--end
			end 

			if @entityId=21 -- taxi
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='07'
				set @valueZS=0.0 -- Koszty rzeczywiste, przepisane z ZZ
				--delete from @values
				--INSERT @values EXEC p_attribute_get2 @attributePath = '152,319', @groupProcessInstanceId = @groupProcessId
				--SELECT @valueZS = value_decimal FROM @values	
			end 

			if @entityId=25 -- parking
			begin
				print dbo.f_translate('koszty wpisane parking',default)
				if @towing=2
				begin
					set @twrCode='01'
				end
				else
				begin
					set @twrCode='02'
				end

				DECLARE kur cursor LOCAL for
					select  id 
					from      dbo.attribute_value WITH(NOLOCK) 
					where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

				OPEN kur;
				FETCH NEXT FROM kur INTO @structureId;
				WHILE @@FETCH_STATUS=0
				BEGIN 
					delete from @values     
					INSERT  @values EXEC dbo.p_attribute_get2
							@attributePath = '638,219,220', --Typ
							@groupProcessInstanceId = @groupProcessId,
							@parentAttributeId=@structureId
                               
						SELECT @TypKosztu=value_int FROM @values

						if @TypKosztu=5 /*parking*/
						begin
							delete from @values     
							INSERT  @values EXEC dbo.p_attribute_get2
									@attributePath = '638,219,222', /*wartość*/
									@groupProcessInstanceId = @groupProcessId,
									@parentAttributeId=@structureId

							SELECT @valueZS=value_decimal FROM @values
									
						end
					FETCH NEXT FROM kur INTO @structureId;
				END
				CLOSE kur
				DEALLOCATE kur
			end 

			if @entityId=29 -- Wynajem pojazdu zastępczego
			begin
				set @twrCode='04'
				EXEC [p_f_replacement_days] @groupProcessInstanceId=@groupProcessId,@quantity=@IleDni OUTPUT,@value=@valueZS OUTPUT
				print dbo.f_translate('ZS: reszta Wartość wynajmu całkowita: ',default) + cast (@value as varchar(20))
				set @valueZS=(@valueZS/@IleDni)
			end

			if @entityId=30 -- podstawienie
			begin
				--print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				-- Pobieranie wartości, Krzysztof
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,790,240,222', @groupProcessInstanceId = @groupProcessId
				SELECT @valueZS = value_decimal FROM @values	
				--
				--print dbo.f_translate('TYP: ',default) + CAST(@type AS VARCHAR(10)) + dbo.f_translate(' ValZS: ',default) + CAST(@valueZS AS VARCHAR(10))
			end 

			if @entityId=31 -- odbiór
			begin
				--print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				-- Pobieranie wartości, Krzysztof
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,787,240,222', @groupProcessInstanceId = @groupProcessId
				SELECT @valueZS = value_decimal FROM @values	
				--

			end 

			if @entityId=32 -- wynajm poza godzinami
			begin
				set @twrCode='04'

				-- Podstawienie poza godzinami pracy wypożyczalni
				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=1,@value=@substitutionValue output
				set @substitutionValue = isnull(@substitutionValue,0)

				-- Zwrot poza godzinami pracy wypożyczalni
				exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=2,@value=@returnValue output		
				set @returnValue = isnull(@returnValue,0)

				if (@substitutionValue != 0 AND @returnValue = 0) -- podstawienie
				begin
					set @valueZS = @substitutionValue
				end

				else if (@substitutionValue = 0 AND @returnValue != 0) -- zwrot
				begin
					set @valueZS = @returnValue
				end

				else if (@substitutionValue != 0 AND @returnValue != 0) -- podstawienie i zwrot
				begin
					set @valueZS = @substitutionValue + @returnValue
				end
			end 

			if @entityId=34 -- dodatkowe ubezp
			begin
				--print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=4,@value=@valueZS OUTPUT
			end 

			if @entityId=35 -- hak holowniczy
			begin
				--print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=5,@value=@valueZS OUTPUT
			end 

			if @entityId=36 -- fotelik
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=6,@value=@valueZS OUTPUT
			end 

			if @entityId=37 -- bagażnik sam.
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=7,@value=@valueZS OUTPUT
			end 

			if @entityId=38 -- wyjazd za granicę
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=3,@value=@valueZS OUTPUT
			end 

			if @entityId=63 -- Transport pojazdu Producent
			begin		
				set @valueZS=(SELECT value_int FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id =@groupProcessId AND attribute_path = '62,319')			
			end 

			/*	415	Audi Gwarancja Mobilności
				418	Volkswagen osobowe Gwarancja Mobilności
				420	Volkswagen użytkowe Gwarancja Mobilności
				421	Skoda Gwarancja Mobilności	
				Aso rozlicza swoje koszty z producentem - bez udziału Starter24	*/
			if (@type = 1 AND @czyASO = 1 AND @programId IN (415,418,420,421) AND @twrCode IN ('02','04')) 
			BEGIN
				print dbo.f_translate('Pomija ZS',default)
				set @valueZS=NULL
			END
		END

		IF @type = 2 AND @unacsaCase=0 --> Ubezpieczyciel Compensa
		BEGIN
			if @entityId=1 -- 0201-Holowanie
			begin
				if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
				begin
					set @valueZS=175.68/1.23
					if @towingKm>25.0
					begin
						set @valueZS=@valueZS+(@towingKm-25.0)*5.86/1.23
					end
				end	
				else
				begin -- brak km holowania za granicą (+113.98 jeśli jest 15-30km, powyżej 30km * 1.26 eur/KM)
					set @valueZS=dbo.FN_Exchange(75.70,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate())/1.23		
				end
			end

			if @entityId=3 -- 0205-Pomoc/Oczekiwanie
			begin
				set @valueZS=20.0
			end

			if @entityId=10 -- 0101-Naprawa na drodze
			begin
				if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
				begin
					set @valueZS=196.42/1.23
					if (@arrivalKm+@backKm)>25.0
					begin
						set @valueZS=@valueZS+(@arrivalKm+@backKm-25.0)*1.83/1.23
					end
				end
				else
				begin
					set @valueZS=dbo.FN_Exchange(60.51,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate())/1.23
				end
			end

			if @entityId=12 -- Nowość --> 9901-Inne
			begin
				EXEC [dbo].[p_f_other_costs] @groupId=@groupProcessId,@value = @valueZS OUTPUT

				-- Jeśli naprawa lub holowanie i są dodatkowe koszty to dokładamy je do pozycji inne.
				IF (SELECT DISTINCT TOP 1 LEFT(step_id,4) FROM [dbo].[process_instance] WITH(NOLOCK) WHERE group_process_id = @groupProcessId) = 1009 AND @value >= 70
				BEGIN
					SET @valueZS = @valueZS - 70
				END
			end 

			if @entityId=16 -- 0203-Dźwig
			begin
				if @dmc<=2599
				begin
					set @valueZS=150.0		
				end		
				else if @dmc<=3399
				begin
					set @valueZS=170.0
				end		
				else if @dmc<=5699
				begin
					set @valueZS=170.0
				end
			end
		
			if @entityId=21 -- taxi
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='07'
				set @valueZS=0.0 -- Koszty rzeczywiste, przepisane z ZZ
				--delete from @values
				--INSERT @values EXEC p_attribute_get2 @attributePath = '152,319', @groupProcessInstanceId = @groupProcessId
				--SELECT @valueZS = value_decimal FROM @values	
			end 

			if @entityId=25 -- parking
			begin
				print dbo.f_translate('koszty wpisane parking',default)
				if @towing=2
				begin
					set @twrCode='01'
				end
				else
				begin
					set @twrCode='02'
				end

				DECLARE kur cursor LOCAL for
					select  id 
					from      dbo.attribute_value WITH(NOLOCK) 
					where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

				OPEN kur;
				FETCH NEXT FROM kur INTO @structureId;
				WHILE @@FETCH_STATUS=0
				BEGIN 
					delete from @values     
					INSERT  @values EXEC dbo.p_attribute_get2
							@attributePath = '638,219,220', --Typ
							@groupProcessInstanceId = @groupProcessId,
							@parentAttributeId=@structureId
                               
						SELECT @TypKosztu=value_int FROM @values

						if @TypKosztu=5 /*parking*/
						begin
							delete from @values     
							INSERT  @values EXEC dbo.p_attribute_get2
									@attributePath = '638,219,222', /*wartość*/
									@groupProcessInstanceId = @groupProcessId,
									@parentAttributeId=@structureId

							SELECT @valueZS=value_decimal FROM @values
									
						end
					FETCH NEXT FROM kur INTO @structureId;
				END
				CLOSE kur
				DEALLOCATE kur
			end 

			if @entityId=29 -- Wynajem pojazdu zastępczego wg cennika Compensy dla nie ASO: tj. nie z wypozyczlni typu panek...
			begin
				set @twrCode='04'
				/*	415	Audi Gwarancja Mobilności
					418	Volkswagen osobowe Gwarancja Mobilności
					420	Volkswagen użytkowe Gwarancja Mobilności
					421	Skoda Gwarancja Mobilności	*/
				if ( (@czyASO=0) or (@programId in (415,418,420,421)) )
				begin				
					declare @dni int

					delete from @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
					SELECT @markaModelAutaWyp = value_int FROM @values				

					select	@class=argument3
					from	dbo.dictionary 
					where	typeD='makeModel' and 
							value=@markaModelAutaWyp

					-- SELECT DO WYJĄTKÓW BENEFIA
					declare @klasaBenefia int = (SELECT [classBenefia] 
												FROM [dbo].[classException] WITH(NOLOCK)
												WHERE makeModelId = @markaModelAutaWyp)

					if (@czyASO=0 AND ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) IN (dbo.f_translate(dbo.f_translate('Polska',default),default),'PL',dbo.f_translate('Poland',default)))
					begin 
						set @valueZS=0.01
						IF @klasaBenefia IS NOT NULL -- Cennik Benefia, wg. wykluczeń
						BEGIN
							if @klasaBenefia in (1)
							begin
								set @valueZS=107.57
							end

							if @klasaBenefia in (2)
							begin
								set @valueZS=120.15
							end

							if @klasaBenefia in (3)
							begin
								set @valueZS=132.84
							end

							if @klasaBenefia in (4)
							begin
								set @valueZS=148.86
							end

							if @klasaBenefia in (5)
							begin
								set @valueZS=197.82
							end

							if @klasaBenefia in (6)
							begin
								set @valueZS=214.01
							end

							if @klasaBenefia in (7)
							begin
								set @valueZS=169.89
							end

							if @klasaBenefia in (8)
							begin
								set @valueZS=261.28
							end

							if @klasaBenefia in (9)
							begin
								set @valueZS=171.62
							end

							if @klasaBenefia in (10)
							begin
								set @valueZS=214.05
							end

							if @klasaBenefia in (11)
							begin
								set @valueZS=334.50
							end

							if @klasaBenefia in (12)
							begin
								set @valueZS=230.71
							end

							if @klasaBenefia in (13)
							begin
								set @valueZS=283.00
							end

							if @klasaBenefia in (14)
							begin
								set @valueZS=290.99
							end

							if @klasaBenefia in (15)
							begin
								set @valueZS=353.62
							end

							if @klasaBenefia in (16)
							begin
								set @valueZS=487.45
							end

							if @klasaBenefia in (17)
							begin
								set @valueZS=535.12
							end

							if @klasaBenefia in (18)
							begin
								set @valueZS=319.45
							end

							if @klasaBenefia in (19)
							begin
								set @valueZS=351.24
							end

							if @klasaBenefia in (20)
							begin
								set @valueZS=373.80
							end
							if @klasaBenefia in (21)
							begin
								set @valueZS=162.60
							end
						END
						ELSE -- Cennik Starter24
						BEGIN
						
							if @class in (1,2)
							begin
								set @valueZS=107.57
							end

							if @class in (3)
							begin
								set @valueZS=120.15
							end

							if @class in (4)
							begin
								set @valueZS=171.62
							end

							--if @class in (5)
							--begin
							--	set @valueZS=132.84
							--end
				
							if @class in (5)
							begin
								set @valueZS=230.71
							end

							if @class in (6)
							begin
								set @valueZS=319.45
							end

							if @class in (7)
							begin
								set @valueZS=148.86
							end
					
							if @class in (9)
							begin
								set @valueZS=283.00
							end

							if @class in (10)
							begin
								set @valueZS=214.05
							end

							if @class in (11)
							begin
								set @valueZS=197.82
							end

							if @class in (13)
							begin
								set @valueZS=214.01
							end

							if @class in (15)
							begin
								set @valueZS=351.24
							end

							if @class in (16)
							begin
								set @valueZS=290.99
							end

							if @class in (18)
							begin
								set @valueZS=373.80
							end

							if @class in (19)
							begin
								set @valueZS=353.62
							end

							if @class in (21)
							begin
								set @valueZS=535.12
							end

							if @class in (22)
							begin
								set @valueZS=487.45
							end

							if @class in (23)
							begin
								set @valueZS=169.89
							end

							if @class in (24)
							begin
								set @valueZS=261.28
							end

							if @class in (25)
							begin
								set @valueZS=334.50
							end
						end
					end
					else -- Aso 1
					begin
						set @valueZS=0.00
					end
			
					if(@IsDealerCall = 1)
					begin
						EXEC [p_f_replacement_days] @groupProcessInstanceId=@groupProcessId,@quantity=@dni OUTPUT,@value = @valueZS OUTPUT
					end
					else
					begin			
						EXEC [p_f_replacement_days] @groupProcessInstanceId=@groupProcessId,@quantity=@dni OUTPUT
						set @valueZS=@valueZS*@dni--@quantityZ=@dni
					end
					print '-------'
					print '------- ZS:U Wartość wynajmu całkowita: ' + cast (@value as varchar(20))
					print '-------'
					---------------------------------------					
				end			
			end

			if (@entityId IN (30,31)) -- 0402-Podstawienie, 0403-Odbiór od klienta, 0404-Wynajem poza godzinami --> Przepisz wartość z ZZ
			begin
				SET @valueZS = @value
				print '@valueZS: ' + CAST(@valueZS AS VARCHAR(30)) + ' @value:' + CAST(@value AS VARCHAR(30))
			end

			if @entityId=32
			begin
				-- Podstawienie poza godzinami pracy wypożyczalni
				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=1,@value=@substitutionValue output
				set @substitutionValue = isnull(@substitutionValue,0)

				-- Zwrot poza godzinami pracy wypożyczalni
				exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=2,@value=@returnValue output		
				set @returnValue = isnull(@returnValue,0)

				if (@substitutionValue != 0 AND @returnValue = 0) -- podstawienie
				begin
					set @valueZS = @substitutionValue
				end

				else if (@substitutionValue = 0 AND @returnValue != 0) -- zwrot
				begin
					set @valueZS = @returnValue
				end

				else if (@substitutionValue != 0 AND @returnValue != 0) -- podstawienie i zwrot
				begin
					set @valueZS = @substitutionValue + @returnValue
				end
			end

			if @entityId=63 -- MP Transport pojazdu Compensa
			begin
				set @twrCode='10'
				SELECT @valueZS=value_int FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id =@groupProcessId AND attribute_path = '62,319'		
			end 
		END

		IF @type = 2 AND @unacsaCase=1 --> Ubezpieczyciel Unacsa
		BEGIN
			--IF NOT EXISTS --> Usługi podlegające rycałtowi
			--BEGIN
			--	SET @valueZS = CASE WHEN @programId=416	THEN 286 --> Skoda Nowe
			--						WHEN @programId=402	THEN 311 --> Skoda Przedł.Assist.
			--						WHEN @programId=421	THEN 311 --> Skoda Gwarancja Mobilności
			--						WHEN @programId=368	THEN 367 --> SEAT Gwarancja Mobilności od 1.7.2015
			--						WHEN @programId=461	THEN 367 --> SEAT Assistance
			--						WHEN @programId=462	THEN 344 --> SEAT Przedłużone Assistance
			--						WHEN @programId=463	THEN 367 --> Seat Gwarancja Mobilności
			--						WHEN @programId=417	THEN 363 --> VW osobowe Nowe
			--						WHEN @programId=418	THEN 229 --> VW osobowe Gwarancja Mobilności
			--						WHEN @programId=419	THEN 597 --> VW użytkowe Nowe
			--						WHEN @programId=420	THEN 384 --> VW użytkowe Gwarancja Mobilności
			--						WHEN @programId=414	THEN 953 --> Audi Nowe
			--						WHEN @programId=415	THEN 492 --> Audi Gwarancja Mobilności
			--						ELSE 0.00 END
			--END
			--ELSE
			--BEGIN
				SET @valueZS=0.00 --> Usługi dodatkowe
			--END
		END
	END
	ELSE IF @programId IN (429,430,431,432,433,434,435,436,437,438,439,440,441) OR @platformId = 60--> Ford, Ford Vignale
	BEGIN
		IF @entityId=1 -- 0201-Holowanie -- DOPISAC pusty wyjazd = 0PLN i naprawa nieudana na kontrahenta z 111111
		BEGIN
			set @twrCode='02'

			if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				if @programId = 430 -- Ford Assistance 12 Polska
				begin
					if @dmc<3500
					begin
						set @basePrice=178.40
						set @kmPrice=6.80
					end		
					else if 3500<=@dmc AND @dmc<=7500
					begin
						set @basePrice=756.50
						set @kmPrice=24.60
					end
					else -- powyżej DMC 7500
					begin
						set @basePrice=0.00
						set @kmPrice=0.00
					end

					if @towingKm<=25
					begin
						set @valueZS=@basePrice
					end
					else
					begin
						set @valueZS=@basePrice+(@towingKm-25.0)*@kmPrice
					end
				end

				if @programId IN (429,435,436,437,438) -- Ford Euroservice,Ford Ubezpieczenia,Ford Mini Car Assistance, Polska
				begin
					if @dmc<3500
					begin
						set @basePrice=228.80
						set @kmPrice=8.00
					end		
					else if 3500<=@dmc AND @dmc<=7500
					begin
						set @basePrice=756.50
						set @kmPrice=24.60
					end
					else -- powyżej DMC 7500
					begin
						set @basePrice=0.00
						set @kmPrice=0.00
					end

					if @towingKm<=25
					begin
						set @valueZS=@basePrice
					end
					else
					begin
						set @valueZS=@basePrice+(@towingKm-25.0)*@kmPrice
					end
				end

				if @programId IN (431,432,433,434) -- Ford Protect,Ford Inne Marki Wszystkie, Polska
				begin
					if @dmc<3500
					begin
						set @basePrice=186.00
						set @kmPrice=6.50
					end		
					else if 3500<=@dmc AND @dmc<=7500
					begin
						set @basePrice=615.00
						set @kmPrice=20.00
					end
					else -- powyżej DMC 7500
					begin
						set @basePrice=0.00
						set @kmPrice=0.00
					end

					if @towingKm<=25
					begin
						set @valueZS=@basePrice
					end
					else
					begin
						set @valueZS=@basePrice+(@towingKm-25.0)*@kmPrice
					end
				end

			end
			else -- Zagranica
			begin
				if @programId = 430 -- Ford Assistance 12, Zagranica
				begin
					if @dmc<=3500
					begin
						if @towingKm <= 15
						begin
							set @valueZS=393.60
						end
						else if 15 < @towingKm and @towingKm <= 30
						begin
							set @valueZS=590.40
						end
						else if 30 < @towingKm
						begin
							set @kmPrice=6.80
							set @valueZS=590.40+(@towingKm-30.0)*@kmPrice
						end
					end		
					else
					begin
						set @valueZS = 0.00
					end
				end

				if @programId IN (429,435,436,437,438) -- Ford Euroservice,Ford Ubezpieczenia,Ford Mini Car Assistance, Zagranica
				begin
					if @dmc<=3500
					begin
						if @towingKm <= 15
						begin
							set @valueZS=436.70
						end
						else if 15 < @towingKm and @towingKm <= 30
						begin
							set @valueZS=651.90
						end
						else if 30 < @towingKm
						begin
							set @kmPrice=7.50
							set @valueZS=651.90+(@towingKm-30.0)*@kmPrice
						end
					end		
					else
					begin
						set @valueZS = 0.00
					end
				end

				if @programId IN (431,432,433,434) -- Ford Protect,Ford Inne Marki Wszystkie, Zagranica
				begin
					if @dmc<=3500
					begin
						if @towingKm <= 15
						begin
							set @valueZS=355.00
						end
						else if 15 < @towingKm and @towingKm <= 30
						begin
							set @valueZS=530.00
						end
						else if 30 < @towingKm
						begin
							set @kmPrice=6.10
							set @valueZS=530.00+(@towingKm-30.0)*@kmPrice
						end
					end		
					else
					begin
						set @valueZS = 0.00
					end
				end

			end
			
			if @noFix = 1
			begin
				print dbo.f_translate('No FIX',default)
			end
		end

		if @entityId=3 -- 0205-Pomoc/Oczekiwanie
		begin
			set @valueZS=20.0
		end

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @twrCode='01'

			if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				if @programId = 430	-- Ford Assistance 12 Polska
				begin
					set @valueZS=203.00
				end

				if @programId IN (429,435,436,437,438) -- Ford Euroservice,Ford Ubezpieczenia,Ford Mini Car Assistance, Polska
				begin
					set @valueZS=252.20
				end

				if @programId IN (431,432,433,434) -- Ford Protect,Ford Inne Marki Wszystkie, Polska
				begin
					set @valueZS=205.00
				end

			end
			else -- Zagranica
			begin
				if @programId = 430	-- Ford Assistance 12, Zagranica
				begin
					set @valueZS=319.80
				end

				if @programId IN (429,435,436,437,438) -- Ford Euroservice,Ford Ubezpieczenia,Ford Mini Car Assistance, Zagranica
				begin
					if @dmc<3500
					begin
						set @valueZS=350.60
					end		
					else
					begin
						set @valueZS=0.00
					end
				end

				if @programId IN (431,432,433,434) -- Ford Protect,Ford Inne Marki Wszystkie, Zagranica
				begin
					if @dmc<3500
					begin
						set @valueZS=285.00
					end		
					else
					begin
						set @valueZS=0.00
					end
				end
			end
		end

		if @entityId=16 -- 0203-Dźwig
		begin
			if @dmc<=2599
			begin
				set @valueZS=150.0		
			end		
			else if @dmc<=3399
			begin
				set @valueZS=170.0
			end		
			else if @dmc<=5699
			begin
				set @valueZS=170.0
			end
		end

		if @entityId=17 /*Naprawa przez telefon*/
		begin
			set @valueZS=110.74
		end

		if @entityId=18 -- Kredyt
		begin
			set @twrCode='17'
		
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '556', @groupProcessInstanceId = @groupProcessId
			SELECT @currency = value_string FROM @values	

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '732', @groupProcessInstanceId = @groupProcessId
			SELECT @valueZS = value_decimal FROM @values	

			--if @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			--begin
			--	set @valueZS = dbo.FN_Exchange(@valueZS,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate()) 
			--end
		end 

		if @entityId=21 -- taxi
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='07'
			set @valueZS=0.0 -- Koszty rzeczywiste, przepisane z ZZ
			--delete from @values
			--INSERT @values EXEC p_attribute_get2 @attributePath = '152,319', @groupProcessInstanceId = @groupProcessId
			--SELECT @valueZS = value_decimal FROM @values	
		end 

		if @entityId=25 -- parking
		begin
			print dbo.f_translate('koszty wpisane parking',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			DECLARE kur cursor LOCAL for
				select  id 
				from      dbo.attribute_value WITH(NOLOCK) 
				where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

			OPEN kur;
			FETCH NEXT FROM kur INTO @structureId;
			WHILE @@FETCH_STATUS=0
			BEGIN 
				delete from @values     
				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '638,219,220', --Typ
						@groupProcessInstanceId = @groupProcessId,
						@parentAttributeId=@structureId
                               
					SELECT @TypKosztu=value_int FROM @values

					if @TypKosztu=5 /*parking*/
					begin
						delete from @values     
						INSERT  @values EXEC dbo.p_attribute_get2
								@attributePath = '638,219,222', /*wartość*/
								@groupProcessInstanceId = @groupProcessId,
								@parentAttributeId=@structureId

						SELECT @valueZS=value_decimal FROM @values
									
					end
				FETCH NEXT FROM kur INTO @structureId;
			END
			CLOSE kur
			DEALLOCATE kur
		end 

		--if @entityId=26 and @programId = 441-- @platformId = 60 -- 1901-Porada
		--begin
		--	set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
		--	set @twrCode='19'
		--	set @valueZS=3.88
		--	set @programId = 441
		--end 

		if @entityId=27 and @platformId = 60-- @platformId = 60 -- 1951-Formularz Ford Vignale
		begin
			SET @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			SET @twrCode='19'
			SET @programId = 441

			SET @valueZS = CASE WHEN (	SELECT TOP 1 ISNULL(value_int,0) FROM attribute_value WITH(NOLOCK) --> Czy wysłałeś plik Vignale: TAK
										WHERE group_process_instance_id=@groupProcessId AND attribute_path='928' AND value_int IS NOT NULL) = 1
							THEN 5.82 ELSE 3.88 END  --> Czy wysłałeś plik Vignale: NIE, zwykła porada
		end 

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'
			set @valueZS = 0.00 -- 

			-- SELECT DO WYJĄTKÓW FORD
			declare @klasaFord int = (SELECT [classFord] FROM [dbo].[classException] WITH(NOLOCK) WHERE makeModelId = @customerCarId)
		
			if @programId IN (429,430) -- Ford Euroservice, Ford Assistance 12
			begin
				set @valueZS = 0.01 -- Brakuje klasy wynajmu Ford.
				if @klasaFord = 1 -- Klasa A
				begin
					set @valueZS = 100.00
				end

				if @klasaFord = 2 -- Klasa B
				begin
					set @valueZS = 112.00
				end

				if @klasaFord = 3 -- Klasa C
				begin
					set @valueZS = 141.00
				end

				if @klasaFord = 4 -- Klasa D/SUV/VAN
				begin
					set @valueZS = 186.00
				end

				if @klasaFord = 5 -- Klasa Sport
				begin
					set @valueZS = 236.00
				end

				if @klasaFord = 6 AND @dmc <= 2500 -- Klasa - Mały dostawczy / Pick-up (do 2,499 tony DMC)
				begin
					set @valueZS = 184.00
				end

				if @klasaFord = 7 AND 2500 < @dmc AND @dmc <= 7500 -- Klasa - Duży dostawczy (od 2,500 do 7,500 tony DMC)
				begin
					set @valueZS = 226.00
				end

				/* Modele aut, które występują w różnych DMC. */
				if @customerCarId IN (611,612,613,614,608,609,610,599) AND @dmc <= 2500
				begin
					set @valueZS = 184.00 -- Klasa - Mały dostawczy / Pick-up (do 2,499 tony DMC)
				end

				if @customerCarId IN (611,612,613,614,608,609,610,599) AND 2500 < @dmc AND @dmc <= 7500
				begin
					set @valueZS = 226.00 -- Klasa - Duży dostawczy (od 2,500 do 7,500 tony DMC)
				end
			end

			if @programId IN (435,436,437) -- Ford Ubezpieczenia *
			begin
				set @valueZS = 0.01 -- Brakuje klasy wynajmu Ford.

				if @klasaFord = 1 -- Klasa A
				begin
					set @valueZS = 101.00
				end

				if @klasaFord = 2 -- Klasa B
				begin
					set @valueZS = 107.00
				end

				if @klasaFord = 3 -- Klasa C
				begin
					set @valueZS = 135.00
				end

				if @klasaFord = 4 -- Klasa D/SUV/VAN
				begin
					set @valueZS = 167.00
				end

				if @klasaFord = 5 -- Klasa Sport
				begin
					set @valueZS = 148.00
				end

				if @klasaFord = 6 AND @dmc <= 2500 -- Klasa - Mały dostawczy / Pick-up (do 2,499 tony DMC)
				begin
					set @valueZS = 186.00
				end

				if @klasaFord = 7 AND 2500 < @dmc AND @dmc <= 7500 -- Klasa - Duży dostawczy (od 2,500 do 7,500 tony DMC)
				begin
					set @valueZS = 224.00
				end

				/* Modele aut, które występują w różnych DMC. */
				if @customerCarId IN (611,612,613,614,608,609,610,599) AND @dmc <= 2500
				begin
					set @valueZS = 186.00 -- Klasa - Mały dostawczy / Pick-up (do 2,499 tony DMC)
				end

				if @customerCarId IN (611,612,613,614,608,609,610,599) AND 2500 < @dmc AND @dmc <= 7500
				begin
					set @valueZS = 224.00 -- Klasa - Duży dostawczy (od 2,500 do 7,500 tony DMC)
				end
			end
		end

		if @entityId=30 -- podstawienie
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			-- Pobieranie wartości, Krzysztof
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,790,240,222', @groupProcessInstanceId = @groupProcessId
			SELECT @valueZS = value_decimal FROM @values	
			--
			--print dbo.f_translate('TYP: ',default) + CAST(@type AS VARCHAR(10)) + dbo.f_translate(' ValZS: ',default) + CAST(@valueZS AS VARCHAR(10))
		end 

		if @entityId=31 -- odbiór
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			-- Pobieranie wartości, Krzysztof
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,787,240,222', @groupProcessInstanceId = @groupProcessId
			SELECT @valueZS = value_decimal FROM @values	
			--

		end 

		if @entityId=32 -- wynajm poza godzinami
		begin
			set @twrCode='04'

			-- Podstawienie poza godzinami pracy wypożyczalni
			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=1,@value=@substitutionValue output
			set @substitutionValue = isnull(@substitutionValue,0)

			-- Zwrot poza godzinami pracy wypożyczalni
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=2,@value=@returnValue output		
			set @returnValue = isnull(@returnValue,0)

			if (@substitutionValue != 0 AND @returnValue = 0) -- podstawienie
			begin
				set @valueZS = @substitutionValue
			end

			else if (@substitutionValue = 0 AND @returnValue != 0) -- zwrot
			begin
				set @valueZS = @returnValue
			end

			else if (@substitutionValue != 0 AND @returnValue != 0) -- podstawienie i zwrot
			begin
				set @valueZS = @substitutionValue + @returnValue
			end
		end 

		if @entityId=34 -- dodatkowe ubezp
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=4,@value=@valueZS OUTPUT
		end 

		if @entityId=35 -- hak holowniczy
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=5,@value=@valueZS OUTPUT
		end 

		if @entityId=36 -- fotelik
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=6,@value=@valueZS OUTPUT
		end 

		if @entityId=37 -- bagażnik sam.
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=7,@value=@valueZS OUTPUT
		end 

		if @entityId=38 -- wyjazd za granicę
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=3,@value=@valueZS OUTPUT
		end 

		if @entityId=63 -- Transport pojazdu Producent
		begin			
			set @valueZS=(SELECT value_int FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id =@groupProcessId AND attribute_path = '62,319')			
		end 

		/*	415	Audi Gwarancja Mobilności
			418	Volkswagen osobowe Gwarancja Mobilności
			420	Volkswagen użytkowe Gwarancja Mobilności
			421	Skoda Gwarancja Mobilności	
			Aso rozlicza swoje koszty z producentem - bez udziału Starter24		
		*/
		if (@type = 1 AND @czyASO = 1 AND @programId IN (415,418,420,421) AND @twrCode IN ('02','04')) 
		BEGIN
			print dbo.f_translate('Pomija ZS',default)
			set @valueZS=NULL
		END

		if @programId = 439	-- Ford Assistance Kolizyjny
		begin
			set @AkronimZS = '100020' -- Ford Polska
			set @valueZS=0.00
		end

		/*	26 1901-Porada
			27 1951-Formularz Ford Vignale */
		if @entityId NOT IN (1,10,29) AND (@entityId NOT IN (28,29) AND @programId <> 441)-- @platformId != 60) -- Pozostałe usługi rozliczane wg kosztów rzeczywistych
		begin
			set @valueZS=0.00
		end
	END
	ELSE IF @programId IN (447,448,449) --> KIA
	BEGIN
		set @valueZS = 0 -- Pozostałe przypadki

		if @programId = 448 -- KIA Assistance
		begin
			if @entityId in (1,10) and @KIAonceNHpriceZS = 1
			begin
				set @valueZS = 333.21
				set @KIAonceNHpriceZS = 0 --> Ryczałt pobrany, kolejny nie zostanie naliczony
			end
		end

		if @programId = 449 -- KIA Assistance FLOTA 
		begin
			if @entityId in (1,10)
			begin
				set @valueZS = 463.64
			end
		end

		if @programId = 447 -- KIA Służby Państwowe
		begin
			if @entityId in (1,10)
			begin
				set @valueZS = 224.18
			end
		end
	END
/*	ELSE IF @programId IN (442,454,455,456,457,590,591,592) OR @platformFromProgram=14 --> OPEL W REGUŁACH OGÓLNYCH DLA PSA
	BEGIN
		set @valueZS = 0 -- Pozostałe przypadki

		if @entityId in (10) -- 0101-Naprawa na drodze
		begin
			set @valueZS = 345.06
		end

		if @entityId in (17) -- 00N01-Naprawa przez telefon
		begin
			set @valueZS = 36.43
		end

		if @entityId in (1) -- 0201-Holowanie
		begin
			set @valueZS = 373.99
		end

		if @entityId in (21) -- 0701-Odwiezienie osobówką
		begin
			set @valueZS = 145.00
		end

		if @entityId in (19,23) -- 0602-Lot , 0601-Pociąg
		begin
			set @valueZS = 637.00
		end

		if @entityId in (29) -- 0401-Wynajem auta zastępczego
		begin
			set @valueZS = 524.02
		end

		if @entityId in (43) -- 0501-Nocleg
		begin
			set @valueZS = 352.00
		end

		if @entityId in (63) -- Transport
		begin
			set @valueZS = 2012.00
		end
	END	*/
	ELSE IF @programId IN (475,476,477,478,479,480,481,482,483) --> TUW TUZ
	BEGIN
		set @valueZS = 0.00 -- Pozostałe przypadki

		if @entityId=1 -- 0201-Holowanie
		begin
			if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				if @towingKm<=50 -- Ryczałt do 50km
				begin
					set @valueZS=120.00
				end
				else if @towingKm<=100 -- Ryczałt do 100km
				begin
					set @valueZS=200.00
				end
				else if @towingKm<=200 -- Ryczałt do 200km
				begin
					set @valueZS=400.00 
				end	
				else -- Ryczałt powyżej 200km
				begin
					set @valueZS=400.00 + (@towingKm - 200)*2.00
				end
				
				set @valueZS = @valueZS + (2.60 * (@arrivalKm + @backKm)) -- + Ryczałt za kilometry dojazdu i powrotu
			end
			else
			begin
				set @value=0.0
			end
		end

		if @entityId=3 -- 0205-Pomoc/Oczekiwanie
		begin
			set @valueZS=20.00
		end

		if @entityId=8 -- 0210-Transport pow 2 osób w holowniku
		begin
			set @valueZS=1.60 * @towingKm
		end

		if @entityId=10 -- 0101-Naprawa na drodze
		begin
			if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				set @valueZS=190.00

				if @towingKm > (@arrivalKm + @backKm)
				begin
					set @valueZS = @valueZS + ((@arrivalKm + @backKm - 50) * 1.60)
				end
			end
			else
			begin
				set @valueZS=0.0
			end
		end

		if @entityId=16 -- 0203-Dźwig
		begin
			set @valueZS=150.00
		end

		if @entityId=17 -- 00N01-Naprawa przez telefon
		begin
			set @valueZS=80.00
		end	

		if @entityId=21 -- 0701-Odwiezienie osobówką
		begin
			delete from @values
			insert @values exec p_attribute_get2 @attributePath = '152,647', @groupProcessInstanceId = @groupProcessId
			set @taxiDistance = (select value_int from @values)
			set @valueZS=50			
			if @taxiDistance>20.0
			begin
				set @valueZS=@valueZS+(@taxiDistance-20.0)*3.0
			end
		end	

		if @entityId=25 and @dmc < 3500 -- 2201-Parking
		begin
			set @valueZS=10.00
		end

		if @entityId=26 -- 1901-Porada
		begin
			set @valueZS=26.25  /*TUW TUZ cennik od 01.10.2018*/
		end

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			SET @valueZS = case when @class = 1  then 70.00  -- 'A'
								when @class = 2  then 80.00  -- 'B'
								when @class = 7  then 90.00  -- 'C'
								when @class = 5  then 130.00 -- dbo.f_translate('B Premium',default)
								when @class = 11 then 130.00 -- 'D'
								when @class = 23 then 110.00 -- 'M'
								when @class = 9  then 180.00 -- dbo.f_translate('C Premium',default)
								when @class = 16 then 350.00 -- dbo.f_translate('D Premium',default)
								when @class = 21 then 400.00 -- 'F'
								when @class = 24 then 200.00 -- 'N'
								when @class = 17 then 210.00 -- 'R'
								when @class IN (4,6,10,15,18,25) then 0.00 -- dbo.f_translate('SUV',default)
							else 0.01 end
		end
	END
	ELSE IF @programId IN (484,485,486) -->	TUW TUZ Home Assistance
	BEGIN
		set @valueZS = 0 -- Pozostałe przypadki

		if @entityId=26 -- 1901-Porada
		begin
			set @twrCode='19'
			set @valueZS=26.25 /*zmiana od 01.10.2018*/
		end

		if @entityId=64 -- Home Assistance
		begin
			/*	0	Ślusarz
				1	Elektryk
				2	Hydraulik
				3	Murarz
				4	Malarz
				5	Szklarz
				6	Dekarz
				7	Specjalista od systemów alarmowych
				8	Technik urządzeń grzewczych
				9	Technik urządzeń AGD
				10	Technik urządzeń RTV
				11	Opieka domowa po hospitalizacji
				12	Transport i dozór
				13	Opieka nad zwierzętami
				14	Organizacja wizyty u lekarza specjalisty
				15	Domowa wizyta lekarza pierwszego kontaktu
				16	Domowa wizyta pielęgniarki
				17	Dostawa leków lub sprzętu rehabilitacyjnego
				18	Konsultacja medyczna
				19	Raport medyczny
				20	Opieka psychologiczna
				21	Rehabilitacja
				22	Wizyta pielęgniarki u dziecka
				23	Wizyta pediatry
				24	Powtórna opinia medyczna
				25	Całodobowa opieka pielęgniarki w szpitalu
				26	Wizyta u chorego
				27	Transport medyczny inny niz samolot
				28	Inne */
			declare @haService int -- Numer usługi Home Assistance
			delete from @values
			insert @values exec p_attribute_get2 @attributePath = '954', @groupProcessInstanceId = @groupProcessId
			select @haService = value_int FROM @values	

			set @valueZS = case when @haService IN (0,1,2,3,4,5,6,7,8)  then 245.00  -- Ślusarz,Elektryk,Hydraulik,Murarz,Malarz,Szklarz,Dekarz,Specjalista od systemów alarmowych,Technik urządzeń grzewczych
								when @haService IN (9,10)  then 245.00  -- Technik urządzeń AGD i RTV
								when @haService IN (11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27)  then 122.00  -- Pakiet Assistance Medyczny
							else 0.00 end
		end
	END
	ELSE IF @programId IN (469,470,471,472,473,474) -->	TUW Pocztowe
	BEGIN
		set @valueZS = 0.00

		/*	469	42	TUW Assistance (Pocztowe OC)
			470	42	Moto Assistance Standard (TUW Pocztowe)
			471	42	Moto Assistance Komfort (TUW Pocztowe)*/
		if @programId in (469,470,471)
		begin

			if @entityId in (1,10) and @POCZTOEonceNHpriceZS = 1
			begin		
				set @valueZS = case when @programId = 469 then 364.53  -- TUW Assistance (Pocztowe OC)
									when @programId = 470 then 636.42  -- Moto Assistance Standard (TUW Pocztowe)
									when @programId = 471 then 878.10  -- Moto Assistance Komfort (TUW Pocztowe)
								else 0.00 end
				set @POCZTOEonceNHpriceZS = 0
			end
		end

		/*	472	42	TUW Pocztowe Bezpieczny Dom
			473	42	TUW Pocztowe ubezpieczenie kredytów i pożyczek hipotecznych
			474	42	TUW Pocztowe ubezpieczenie ROR */
		if @programId in (472,473,474)
		begin
			print 'T'
			--set @XL_FlagaNBZZ = 'B'
		end
	END
	ELSE IF @programId IN (443,444,445,446,494,495,496,497) -->	Concordia Assistance
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			if isnull(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) = dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				if (@arrivalKm+@backKm<=50) -- ponizej 50 km
				begin
					set @valueZS=246.75
				end
				else 
				begin 
			 		set @valueZS=246.75+(2.10*(@arrivalKm+@backKm-50)) -- 2.10 pln brutto za km powyzej 50 km
				end
			end
		end

		if @entityId=1 -- 0201-Holowanie
		begin
			if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				if (@dmc < 2500)
				begin
					set @valueZS = 206.85			
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 6.56 * (@towingKm - 25)
					end
				end
				else if (@dmc between 2500 and 3499)
				begin
					set @valueZS = 252.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin								
						set @valueZS = @valueZS + 8.51 * (@towingKm - 25)
					end
				end
				else if (@dmc between 3500 and 7500)
				begin
					set @valueZS = 756.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 24.15 * (@towingKm - 25)
					end
				end
			end
		end

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary WITH(NOLOCK) 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			SET @valueZS = case when @class = 1  then 95.00  -- 'A'
								when @class = 2  then 100.00 -- 'B'
								when @class = 7  then 116.00 -- 'C'
								when @class = 11 then 163.00 -- 'D'
								when @class = 16 then 257.00 -- dbo.f_translate('D Premium',default)
								when @class = 19 then 336.00 -- 'E'
								when @class = 21 then 578.00 -- 'F'
								when @class in (4,6,10,15,18,25) then 221.00 -- dbo.f_translate('SUV',default)
								when @class = 13 then 236.00 -- dbo.f_translate('VAN',default)
								when @class = 23 then 173.00 -- 'M'
								when @class = 24 then 221.00 -- 'N'
								when @class = 17 then 221.00 -- 'R'
							else 0.01 end
		end

		if @entityId=16 -- 0203-Dźwig
		begin
			if @dmc < 2499
			begin
				set @valueZS=194.25
			end
			else if @dmc > 2500
			begin
				set @valueZS=220.50
			end
		end

		if @entityId=26 -- 1901-Porada
		begin
			set @valueZS=21.00
		end
	END
	ELSE IF @programId IN (458,459,460) -->	Orix Standard,VIP,Exclusive
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			if isnull(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) = dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				if (@arrivalKm+@backKm<=50) -- ponizej 50 km
				begin
					set @valueZS=235.00
				end
				else 
				begin 
			 		set @valueZS=235.00+(2.00*(@arrivalKm+@backKm-50)) -- 2.10 pln brutto za km powyzej 50 km
				end
			end
		end

		if @entityId=1 -- 0201-Holowanie
		begin
			if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				if (@dmc < 2500)
				begin
					set @valueZS = 197.00			
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 6.25 * (@towingKm - 25)
					end
				end
				else if (@dmc between 2500 and 3499)
				begin
					set @valueZS = 240.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin								
						set @valueZS = @valueZS + 7.13 * (@towingKm - 25)
					end
				end
				else if (@dmc between 3500 and 7500)
				begin
					set @valueZS = 720.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 23.00 * (@towingKm - 25)
					end
				end
				else if (@dmc > 7500)
				begin
					set @valueZS = 585.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 18.90 * (@towingKm - 25)
					end
				end
			end
		end

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary WITH(NOLOCK) 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			SET @valueZS = case when @class = 1  then 123.00  -- 'A'
								when @class = 2  then 130.00 -- 'B'
								when @class = 7  then 141.00 -- 'C'
								when @class = 11 then 178.00 -- 'D'
								when @class = 8  then 178.00 -- dbo.f_translate('DOSTAWCZY MAŁY',default)
								when @class = 14 then 220.00 -- dbo.f_translate('DOSTAWCZY duży',default)
								when @class = 19 then 245.00 -- 'E'
								when @class in (4,6,10,15,18,25) then 245.00 -- dbo.f_translate('SUV',default)
							else 0.01 end
		end

		if @entityId=26 -- 1901-Porada
		begin
			set @valueZS=12.00
		end
	END
	ELSE IF @programId IN (468) --> Comfort ERV
	BEGIN
		set @valueZS = 0.00

		if @entityId=1 -- 0201-Holowanie
		begin
			set @valueZS = 1333.00
		end
	END
	ELSE IF @programId IN (492,493) --> Centra Assistance, Exide Assistance
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS=170.00
		end
	END
	--» Od 06.12.2018r.
	ELSE IF @programId IN (487,488,489) --> Volvo
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (505) --> Husqvarna Assistance (Sylwia)
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (450) --> KTM Assistance (Sylwia)
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (451) --> Mercedes Wypadek (Łukasz)
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (500) --> OAMTC (członkowie klubu austriackiego)
	BEGIN
		set @currency = dbo.f_translate(dbo.f_translate('PLN',default),default)
		set @valueZS = 0.00 -- Koszty rzeczywiste

		if @entityId=1 -- 0201-Holowanie
		begin					
			if (@towingKm+@arrivalKm+@backKm) <= 25
			begin					
				set @valueZS = 245.51 -- Ryczałt 1
			end
			else if (@towingKm+@arrivalKm+@backKm) <= 50
			begin
				set @valueZS = 271.12 -- Ryczałt 2
			end
			else -- Kilometry ponad ryczałt (50km)
			begin
				set @valueZS = 271.12 + 3.01 * ((@towingKm+@arrivalKm+@backKm) - 50)
			end
		end

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS = 199.32
			if (@arrivalKm+@backKm) > 25
			begin -- Kilometry ponad ryczałt (25km)
				set @valueZS = @valueZS + 2.10 * ((@arrivalKm+@backKm) - 25)
			end		
		end

		if @entityId=75 --> Naprawa Warsztatowa SARC
		begin
			set @valueZS=21.00
			set @currency=dbo.f_translate(dbo.f_translate('EUR',default),default)
		end
	END
	ELSE IF @programId IN (465) --> Smart Assistance (Sylwia)
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (507,498,499,501,502,503,504) --> ARC, AA, ANWB, RACE, Rosqvist, TCB, TCS
	BEGIN	/*	507	27	ARC
				498	27	AA (członkowie klubu brytyjskiego)
				499	27	ANWB (członkowie klubu holenderskiego)
				501	27	RACE (członkowie klubu hiszpańskiego)
				502	27	Rosqvist (członkowie klubu fińskiego)
				503	27	TCB (członkowie klubu belgijskiego)
				504	27	TCS (członkowie klubu szwajcarskiego)	*/	
		set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
		set @valueZS = 0.00 -- Koszty rzeczywiste

		if @entityId=1 -- 0201-Holowanie
		begin
			if @AllTowingKmInOneOrder = 1
			begin			
				declare @totalKM decimal(20,4) = 0 -- Suma wszystkich kilometów w sprawie dla usługi holowania.
				declare @totalKMtmp decimal(20,4) = 0

				declare totalKM cursor for
				select value_decimal from attribute_value WITH(NOLOCK) where attribute_path='638,225' and root_process_instance_id=@rootId and value_decimal is not null
				open totalKM
				fetch next from totalKM into @totalKMtmp
				while @@FETCH_STATUS = 0
				begin
					set @totalKM = @totalKM + isnull(@totalKMtmp,0.00)
					fetch next from totalKM into @totalKMtmp
				end
				close totalKM;
				deallocate totalKM;

				set @totalKM = ROUND(ISNULL(@totalKM,0),0) 	-- Zaokrąglanie kilometrów

				if @dmc < 3500
				begin
					set @valueZS = 125.50
				end
				else
				begin -- Dodatkowy ryczałt za DMC powyżej 3,5t
					set @valueZS = 125.50 + 32.11
				end		
						
				if @totalKM > 30 -- Dolicz każdy kilometr ponad zakres bazowy
				begin					
					set @valueZS = @valueZS + 2.40 * (@totalKM - 30)
				end

				set @AllTowingKmInOneOrder = 0
			end
			else
			begin
				set @valueZS = 0
			end
		end

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS = 125.50
			if @dmc > 3500
			begin -- Dodatkowy ryczałt za DMC powyżej 3,5t
				set @valueZS = @valueZS + 32.11
			end		
		end

		if @entityId=65 -- Medical
		begin
			set @IsMedicalInCase = 1
		end

		if @entityId=75 --> Naprawa Warsztatowa SARC
		begin
			set @valueZS=21.00
			set @currency=dbo.f_translate(dbo.f_translate('EUR',default),default)
		end
	END
	--» CFM Od 1,2,3? 2019r.
	ELSE IF @programId IN (508,509,510,511,512,513,514,515,516,517,518,519,520,521) OR @platformFromProgram = 53 --> Alphabet
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS = 192.78 --> Ryczałt do 50 km
			if (@arrivalKm+@backKm) > 50
			begin -- Kilometry ponad ryczałt (50km)
				set @valueZS = @valueZS + 1.80 * ((@arrivalKm+@backKm) - 50)
			end	
		end

		if @entityId=1 -- 0201-Holowanie
		begin		
			if @dmc < 2200
			begin
				set @valueZS=163.20
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 5.90 * (@towingKm - 25)
				end
			end
			else if @dmc >= 2200 and @dmc < 3499
			begin
				set @valueZS=198.90
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 6.70 * (@towingKm - 25)
				end
			end
			if @dmc >= 3499
			begin
				set @valueZS=596.70
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 19.30 * (@towingKm - 25)
				end
			end
		end

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary WITH(NOLOCK) 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			if @partnerId is null
			begin
				print '@partnerId: ' +  isnull(cast(@partnerId as nvarchar(20)),dbo.f_translate('null',default))
				set @summaryQuantity = @Quantity
			end
			else
			begin
				set @summaryQuantity = (
					select SUM(av_quantity.value_int) as dbo.f_translate('Quantity',default)
					from   attribute_value av with(nolock) 
					join   attribute_value av_quantity with(nolock) on av_quantity.parent_attribute_value_id = av.id 
							and av_quantity.attribute_path='820,821,822,221'
					join   attribute_value av_groupId with(nolock) on av_groupId.id=av.parent_attribute_value_id
					join   attribute_value av_partner with(nolock) on av_partner.group_process_instance_id = av_groupId.value_int 
							and av_partner.attribute_path='764,742'
					join   attribute_value av_makeModel with(nolock) on av_makeModel.group_process_instance_id = av_groupId.value_int 
							and av_makeModel.attribute_path='764,73'
					join   dictionary d_class with(nolock) on d_class.value=av_makeModel.value_int and d_class.typeD='makeModel'
					where  av.attribute_path='820,821,822' and av.value_int=29 and av.root_process_instance_id=@rootId
							and av_partner.value_int=@partnerId and d_class.argument3=@class
				)
			end

			-->	Wypo.Prefer.	Jupol ,Express,99Rent,Rentis, Anca ,Carsson,Kangoor
			IF @partnerCode IN ('100414','100040' ,'102562','103700','106090','106088' ,'105820')
			OR EXISTS( --> Serwisy preferowane
				SELECT  d.textD FROM f_split(@partnerKinds,',') fs
				JOIN dictionary d WITH(NOLOCK) ON d.value=fs.data AND d.typeD = 'PartnerLocationType'
				WHERE ( d.textD LIKE '%BRS%' OR
						d.textD LIKE '%Warsztaty naprawy szyb%' OR --'%ALPHABET SZYBY%' OR
						d.textD LIKE '%Warsztat wulkanizacyjny%' OR --'%ALPHABET OPONY%' OR
						d.textD LIKE '%Alphabet ASO Preferowane%' OR
						d.textD LIKE '%ALPHABET ASN%' OR
						d.textD LIKE '%ALPHABET S-PLUS%' OR
						d.textD LIKE '%ASO%' )
			)
			OR @partnerId IN ( --> Jeśli serwis który jest docelowym miejscem holowania
				SELECT DISTINCT av.value_int FROM process_instance pin WITH(NOLOCK)
				JOIN attribute_value av WITH(NOLOCK) ON av.group_process_instance_id=pin.group_process_id AND av.attribute_path='522'
				WHERE pin.root_id=@rootId AND step_id LIKE '1021.%'
			)
			BEGIN
				PRINT dbo.f_translate('Partner preferowany. Koszt 0.00zł',default)
				SET @valueZS = 0.00 --> Partner preferowany
			END
			ELSE
			BEGIN --> Serwisy niepreferowane
				SET @valueZS = CASE 
				WHEN @class IN (3) THEN CASE --> 'B'			
					when @summaryQuantity <= 5 then 87
					when @summaryQuantity <= 14 then 71
					when @summaryQuantity > 14 then 65 
					else null end
				WHEN @class IN (7) THEN CASE --> 'C'
					when @summaryQuantity <= 5 then 102
					when @summaryQuantity <= 14 then 96
					when @summaryQuantity > 14 then 85
					else null end
				WHEN @class IN (11) THEN CASE --> 'D'
					when @summaryQuantity <= 5 then 146
					when @summaryQuantity <= 14 then 137
					when @summaryQuantity > 14 then 125
					else null end
				WHEN @class IN (16,19) THEN CASE --> 'E'
					when @summaryQuantity <= 5 then 260
					when @summaryQuantity <= 14 then 250
					when @summaryQuantity > 14 then 240
					else null end
				WHEN @class IN (8,23) THEN CASE --> 'M' 
					when @summaryQuantity <= 5 then 122
					when @summaryQuantity <= 14 then 110
					when @summaryQuantity > 14 then 100
					else null end
				WHEN @class IN (14,17,24,26) THEN CASE--> dbo.f_translate('N i R',default)
					when @summaryQuantity <= 5 then 215
					when @summaryQuantity <= 14 then 210
					when @summaryQuantity > 14 then 180
					else null end	
				ELSE 0.00 END --> KOSZT RZECZYWISTY
			END
			print dbo.f_translate('KLASA WYNAJMOWANEGO AUTA: ',default) + CAST(@class AS NVARCHAR(10))

			IF @value = 0.00 --> Jeśli wartość na ZZ jest na 0.00 to przypisz 0.00 na ZS też.
			BEGIN
				SET @valueZS=@value
			END
		end

		if @entityId=26 --> 1901-Porada jest udzielana bezpłatnie
		begin
			set @valueZS = null
			--if not exists (select top 1 1 from [sync].[ZamElem] ze WITH(NOLOCK) join [sync].[ZamNag] zn WITH(NOLOCK) on ze.AUD_IdDok=zn.Id where zn.rootId = @rootId and isnull(ze.entityDefId,0) not in (0,26)) 
			--> Jeśli nie istnieją inne usługi niż Auto Zastępcze

			if @allServicesCount > 0 --> 1901-Porada jest udzielana bezpłatnie, ale tylko jeśli w sprawie nie było usług. Jeśli były usługi porada jest usuwana.
			begin
				set @valueZS = null
			end
			else
			begin
				set @valueZS = 0.00
				set @isAlphabetAdviceOnce = 1
			end
		end
	END
	ELSE IF @programId IN (0) OR @platformFromProgram = 48 --> 48 Bussiness Lease
	BEGIN
		set @valueZS = 0.00

		if @entityId=1 -- 0201-Holowanie
		begin		
			if @dmc < 2200
			begin
				set @valueZS=174.60
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 6.30 * (@towingKm - 25)
				end
			end
			else if @dmc >= 2200 and @dmc < 3499
			begin
				set @valueZS=212.90
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 7.20 * (@towingKm - 25)
				end
			end
			if @dmc >= 3499
			begin
				set @valueZS=638.50
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 19.70 * (@towingKm - 25)
				end
			end
		end

		if @entityId=4 -- dokumentacja
		begin
			if dbo.f_rsa_photos(@groupProcessId)=1

			begin
				set @valueZS=20.0 --> Zdjęcia tylko w LP
			end
			else --> Brak zgody wynikająca z wytycznych.
			begin
				print dbo.f_translate('brak zgody na zdjęcia',default)
				set @valueZS=null
			end
		end 

		if @entityId=10 --> 0101-Naprawa na drodze
		begin	
			set @valueZS = 206.20 --> Ryczałt do 50 km
			if (@arrivalKm+@backKm) > 50
			begin -- Kilometry ponad ryczałt (50km)
				set @valueZS = @valueZS + 1.90 * ((@arrivalKm+@backKm) - 50)
			end	
		end

		if @entityId=17 --> Naprawa przez telefon
		begin			
			set @valueZS=13.70
		end

		if @entityId=26 --> 1901-Porada jest udzielana płatnie, ale tylko jeśli jest sama porada
		begin
			set @valueZS = 13.70
			if @allServicesCount > 0 
			--> Jeśli nie istnieją inne usługi niż Auto Zastępcze
			begin
				set @valueZS = null
			end
			else if EXISTS(SELECT TOP 1 1 FROM process_instance WITH(NOLOCK) WHERE root_id=@rootId AND step_id LIKE '1155.%')
			begin
				set @valueZS = 0.00
			end
		end
	END
	ELSE IF @platformFromProgram = 25 --> 25 LEASEPLAN
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			if isnull(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				set @valueZS = 187.00 --> Ryczałt do 50 km
				if (@arrivalKm+@backKm) > 50
				begin -- Kilometry ponad ryczałt (50km)
					set @valueZS = @valueZS + 1.70 * ((@arrivalKm+@backKm) - 50)
				end	
			end
			else --> Zagranica
			begin
				set @valueZS = 161.00
				set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			end
		end

		if @entityId=1 -- 0201-Holowanie
		begin	
			if isnull(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				if @dmc < 2200
				begin
					set @valueZS=157.00
					if @towingKm > 25
					begin
						set @valueZS = @valueZS + 5.70 * (@towingKm - 25)
					end
				end
				else if @dmc >= 2200 and @dmc < 3499
				begin
					set @valueZS=192.00
					if @towingKm > 25
					begin
						set @valueZS = @valueZS + 6.50 * (@towingKm - 25)
					end
				end
				else if @dmc >= 3499
				begin
					set @valueZS=574.00
					if @towingKm > 25
					begin
						set @valueZS = @valueZS + 18.50 * (@towingKm - 25)
					end
				end
			end
			else --> Zagranica
			begin
				set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
				if @dmc < 3500
				begin
					set @valueZS=161.00
					if @towingKm > 30
					begin
						set @valueZS = @valueZS + 2.70 * (@towingKm - 30)
					end
				end
			end
		end

		if @entityId=4 -- dokumentacja
		begin
			if dbo.f_rsa_photos(@groupProcessId)=1
			begin
				set @valueZS=20.0 --> Zdjęcia tylko w LP
			end
			else --> Brak zgody wynikająca z wytycznych.
			begin
				set @valueZS=null
				print dbo.f_translate('brak zgody na zdjęcia',default)
			end
		end 

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			if @partnerId is null
			begin
				print '@partnerId: ' +  isnull(cast(@partnerId as nvarchar(20)),dbo.f_translate('null',default))
				set @summaryQuantity = @Quantity
			end
			else
			begin
				set @summaryQuantity = (
					select SUM(av_quantity.value_int) as dbo.f_translate('Quantity',default)
					from   attribute_value av with(nolock) 
					 join   attribute_value av_quantity with(nolock) on av_quantity.parent_attribute_value_id = av.id 
							and av_quantity.attribute_path='820,821,822,221'
					 join   attribute_value av_groupId with(nolock) on av_groupId.id=av.parent_attribute_value_id
					 -- join   attribute_value av_partner with(nolock) on av_partner.group_process_instance_id = av_groupId.value_int 
					 --		and av_partner.attribute_path='764,742'
					 join   attribute_value av_partner with(nolock) on av_partner.id=(
						select top 1 id from attribute_value with(nolock) where group_process_instance_id = av_groupId.value_int 
						and attribute_path in ('610','764,742') and value_int is not null
					 )						
					 join   attribute_value av_makeModel with(nolock) on av_makeModel.group_process_instance_id = av_groupId.value_int 
							and av_makeModel.attribute_path='764,73'
					 join   dictionary d_class with(nolock) on d_class.value=av_makeModel.value_int and d_class.typeD='makeModel'
					where  av.attribute_path='820,821,822' and av.value_int=29 and av.root_process_instance_id=@rootId
							and 
							av_partner.value_int=@partnerId and d_class.argument3=@class

				)
			end
	
			-->	Wypo.Prefer.
			IF @partnerCode IN (
			'100414', --> Jupol 
			'103700', --> Rentis
			'102562', --> 99Rent
			'101600', --> Partner
			'102800') --> LEASEPLAN FLEET MANAGEMENT (POLSKA) SP. Z O.O. (Flexplan)
			BEGIN

				PRINT dbo.f_translate('Partner preferowany. Koszt 0.00zł',default)
				SET @valueZS = 0.00 --> Partner preferowany
			END
			ELSE
			BEGIN --> Serwisy niepreferowane
					
				SET @valueZS = CASE 
					WHEN @class IN (3) THEN CASE --> 'B'			
						when @summaryQuantity <= 3 then 90
						when @summaryQuantity <= 14 then 85
						when @summaryQuantity > 14 then 80 
						else null end
					WHEN @class IN (7) THEN CASE --> 'C'
						when @summaryQuantity <= 3 then 115
						when @summaryQuantity <= 14 then 105
						when @summaryQuantity > 14 then 100
						else null end
					WHEN @class IN (11) THEN CASE --> 'D'
						when @summaryQuantity <= 3 then 150
						when @summaryQuantity <= 14 then 140
						when @summaryQuantity > 14 then 130
						else null end
					WHEN @class IN (16,19) THEN CASE --> 'E'
						when @summaryQuantity <= 3 then 240
						when @summaryQuantity <= 14 then 220
						when @summaryQuantity > 14 then 210
						else null end
					WHEN @class IN (8,23) THEN CASE --> 'M' 
						when @summaryQuantity <= 3 then 135
						when @summaryQuantity <= 14 then 120
						when @summaryQuantity > 14 then 110
						else null end
					WHEN @class IN (14,17,24,26) THEN CASE--> dbo.f_translate('N i R',default)
						when @summaryQuantity <= 3 then 220
						when @summaryQuantity <= 14 then 210
						when @summaryQuantity > 14 then 200
						else null end	
					ELSE 0.00 END --> KOSZT RZECZYWISTY
			END

			print dbo.f_translate('KLASA WYNAJMOWANEGO AUTA: ',default) + CAST(@class AS NVARCHAR(10))
			print '@summaryQuantity: ' + ISNULL(CAST(@summaryQuantity AS NVARCHAR(10)),dbo.f_translate('NULL',default))
			print '@partnerId: ' + ISNULL(CAST(@partnerId AS NVARCHAR(10)),dbo.f_translate('NULL',default))
		end

		if @entityId=26 AND @allServicesCount > 0 --> 1901-Porada jest udzielana bezpłatnie, ale tylko jeśli w sprawie nie było usług. Jeśli były usługi porada jest usuwana.
		begin
			set @valueZS = null
		end
	END
	ELSE IF @programId IN (0) OR @platformFromProgram = 43 --> 43 ALD
	BEGIN
		set @valueZS = 0.00 -- Pozostałe usługi z kosztami rzeczywistymi

		if @entityId=10 --> 0101-Naprawa na drodze
		begin	
			set @valueZS = 172.07 --> Ryczałt do 50 km
			if (@arrivalKm+@backKm) > 50
			begin -- Kilometry ponad ryczałt (50km)
				set @valueZS = @valueZS + 2.15 * ((@arrivalKm+@backKm) - 50)
			end	
		end

		if @entityId=1 --> 0201-Holowanie
		begin		
			if @dmc < 2500
			begin
				set @valueZS=147.90
				if @towingKm > 20
				begin
					set @valueZS = @valueZS + 4.70 * (@towingKm - 20)
				end
			end
			else
			begin
				set @valueZS=168.30
				if @towingKm > 20
				begin
					set @valueZS = @valueZS + 6.12 * (@towingKm - 20)
				end
			end
		end

		if @entityId=26 AND @allServicesCount > 0 --> 1901-Porada jest udzielana bezpłatnie, ale tylko jeśli w sprawie nie było usług. Jeśli były usługi porada jest usuwana.
		begin
			set @valueZS = null
		end

		if isnull(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) <> dbo.f_translate(dbo.f_translate('Polska',default),default) --> Zdarzenia zagraniczne z kosztami rzeczywistymi
		begin
			set @valueZS = 0.00
		end
	END
	--» TRUCK
	ELSE IF @platformFromProgram IN (8,13,16,20,21,22,50,64,68,73,74) --> TRUCK
	BEGIN
		/*	8	DKV
		13  Starter Truck Service
		16	UTA Assistance    
		20	Euroshell Breakdown Service
		21	Shell Autoryzacje
		22	ESSO Assistance
		50	Shell Alarmowy
		64	Webasto Assistance
		68	KOGEL Assistance
		73	IDS
		74	Otokar Assistance	*/
		SET @valueZS = 0.00 --> Koszty rzeczywiste
		DECLARE @twrCodeTruck NVARCHAR(10) = NULL
		--SET @AkronimZSTruck = @AkronimZS
		IF @entityId IN (66,69) --> 3101-Naprawa na drodze Truck, Holowanie Truck
		BEGIN
			SET @twrCodeTruck = 'TR12'
		END

		IF @entityId IN (73) --> Naprawa mechaniczna Truck
		BEGIN

			SET @twrCodeTruck = CASE WHEN @partnerStarterCode = '103243dbo.f_translate(' THEN ',default)TR10'
									WHEN @partnerStarterCode = '101751dbo.f_translate(' THEN ',default)SIGNELLA'
									WHEN @partnerStarterCode = '101403dbo.f_translate(' THEN ',default)TR10'
									WHEN @partnerStarterCode = '102719dbo.f_translate(' THEN ',default)TR10'
									WHEN @partnerStarterCode = '103006dbo.f_translate(' THEN ',default)BIASCAN'
									WHEN @partnerStarterCode = '100860dbo.f_translate(' THEN ',default)TR10'
									WHEN @partnerStarterCode = '103974dbo.f_translate(' THEN ',default)TR11' 
								ELSE 'TR11' END

			
		END

		IF @entityId NOT IN (71,12) AND ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))=dbo.f_translate(dbo.f_translate('Polska',default),default) --> Poza Dowóz gotówki na inne potrzeby oraz Koszty Inne
		BEGIN

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @towingGroupProcessId
			SELECT @servicePartnerId = value_int FROM @values
	
			if @servicePartnerId is null
			begin
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @towingGroupProcessId
				SELECT @servicePartnerId = value_int FROM @values
			end

			if @servicePartnerId is not null
			begin
				SELECT  @akronimTarget = avKod.value_string 
				FROM	dbo.attribute_value avL WITH(NOLOCK) inner join 
						dbo.attribute_value avKod WITH(NOLOCK) ON avKod.parent_attribute_value_id = avL.id and avKod.attribute_path='595,597,631'  
				WHERE	avL.id = @servicePartnerId and avL.attribute_path='595,597'
			end

			SET @akronimTarget = CASE --WHEN @partnerStarterCode = '103243dbo.f_translate(' THEN ',default)ASO 032'
									--WHEN @partnerStarterCode = '101751dbo.f_translate(' THEN ',default)ASO 032'
									--WHEN @partnerStarterCode = '101403dbo.f_translate(' THEN ',default)ASO 032'
									--WHEN @partnerStarterCode = '102719dbo.f_translate(' THEN ',default)ASO 032'
									WHEN @partnerStarterCode = '103006dbo.f_translate(' THEN ',default)ASO 032'
									--WHEN @partnerStarterCode = '100860dbo.f_translate(' THEN ',default)ASO 032'
									--WHEN @partnerStarterCode = '103974dbo.f_translate(' THEN ',default)ASO 032' 
								END

			IF isnull(@akronimTarget,'')=''
			begin
				set @akronimTarget=@AkronimZS 
			end

			IF @twrCodeTruck IS NOT NULL --> Dodawanie nowego zamówienia, tylko jeśli jest wypełniony TWR KOD
			BEGIN
				INSERT INTO sync.ZamNag (
								ParentZsId,
								AUD_GidTyp, -- 960
								XL_DataRealizacji, -- data statusu progres 4
								XL_DokumentObcy, -- caseId
								XL_KntAkronim,
								XL_Opis, -- platforma + dbo.f_translate('atlas',default)
								XL_Typ, -- ZS/ZZ
								XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
								rootId,--SO_SprawaId, -- rootId
								type,
								partnerLocId,
								XL_DataWystawienia,
								XL_TargetKntAkronim,
								XL_FlagaNB
							)
							SELECT 
							null,
							1280, 
							getdate(),--@finishedAt, 
							@caseId,  
							@partnerStarterCode, --Akronim VGP
							@description, -- Description (opis)
							'ZS',
							@serieZS,	
							@rootId,
							1, -- Type (Producent)
							null,--@partnerId, -- Czy to musi być?
							@caseDate, --> Numer sprawy A0XXXXXX
							@akronimTarget, /*Docelowy*/
							'N' --> Netto

				INSERT INTO sync.ZamElem(
					AUD_IdDok, -- id nagłówka
					XL_Ilosc, -- zmienna
					XL_TwrKod, -- zmienna 
					XL_Wartosc, -- zmienna 
					XL_Waluta, -- zmienna 
					entityValueId, -- entityId
					entityDefId,
					programId,
					groupProcessId
				)	
				SELECT  
				scope_identity(),
				1,
				@twrCodeTruck,
				0.00,
				isnull(@currency,dbo.f_translate(dbo.f_translate('PLN',default),default)),
				null,
				null,
				@programId --> Alphabet wsparcie kierowcy
				,@groupProcessId
			END
		END


		--IF @entityId = 66 --> 3101-Naprawa na drodze Truck
		--BEGIN
			
		--END

		--SET @valueZS =	CASE --> ZS - Koszty rzeczywiste 0.00
		--	WHEN @entityId = 66 THEN 
		--	0.00 
		--	--WHEN @entityId = 67 THEN 0.00 --> 3102-Diagnoza na drodze Truck
		--	--WHEN @entityId = 68 THEN 0.00 --> 3103-Serwis oponiarski na drodze Truck
		--	WHEN @entityId = 69 THEN 0.00 --> Holowanie Truck
		--	--WHEN @entityId = 70 THEN 0.00 --> Wyciąganie spoza drogi dla pojazdów ciężarowych
		--	WHEN @entityId = 71 THEN 0.00 --> Dowóz gotówki na inne potrzeby
		--	--WHEN @entityId = 72 THEN 0.00 --> Dowóz gotówki na opłacenie kary lub mandatu
		--	WHEN @entityId = 73 THEN 0.00 --> Naprawa mechaniczna Truck



		--ELSE 0.00 END

		IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))<>dbo.f_translate(dbo.f_translate('Polska',default),default) AND @entityId <>71 --> Sprawy za granica powinny sie przenosić tylko z dowozem gotówki
		BEGIN
			PRINT dbo.f_translate('Sprawy za granica powinny sie przenosić tylko z dowozem gotówki',default)
			SET @valueZS = NULL
		END

		--set @valueZS = NULL -- IMPORT WYŁĄCZONY
	END
	ELSE IF @platformFromProgram IN (5) --> ADAC
	BEGIN
		set @valueZS = 0.00 --> Koszty rzeczywiste

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS = 199.32 --> Ryczałt do 50 km
			if (@arrivalKm+@backKm) > 25
			begin -- Kilometry ponad ryczałt (50km)
				set @valueZS = @valueZS + 2.10 * ((@arrivalKm+@backKm) - 25)
			end
		end

		if @entityId=1 -- 0201-Holowanie
		begin
			set @valueZS=245.41
			if (@towingKm+@backKm+@arrivalKm) > 25 and (@towingKm+@backKm+@arrivalKm) <= 50
			begin
				set @valueZS=271.12
			end
			else if (@towingKm+@backKm+@arrivalKm) > 50
			begin
				set @valueZS = 271.12 + 3.01 * ((@towingKm+@backKm+@arrivalKm) - 50)
			end
		end

		if @twrCode='04' --> Wynajem pojazdu zastępczego nie przenosi się.
		begin
			set @valueZS=null
		end
	END
	--» PSA od 04.2019
	ELSE IF @platformFromProgram IN (	
		14, --> Opel
		79, --> Citroen
		80, --> Peugeot
		81, --> DS
		82) --> Motor homes
	BEGIN
		SET @valueZS = 0 -- Pozostałe przypadki

		IF @entityId = 1 AND @platformFromProgram IN (14,79,80) --> Holowanie
		BEGIN
			IF @isTowingPSA = 0
			BEGIN
				SET @basePrice = 203.70 --> Ryczał
			END
			ELSE
			BEGIN
				SET @basePrice = 242.50 --> Ryczał drugie holowanie
			END

			IF @towingKm <= 30
			BEGIN
				SET @valueZS = @basePrice
			END
			ELSE
			BEGIN
				SET @valueZS = @basePrice + (@towingKm-30)*7.45
			END

			SET @isTowingPSA = 1 --> Ustawienie następnego holowania
		END
		ELSE IF @entityId = 1 AND @platformFromProgram IN (81,82) --> Holowanie Premium (DS oraz Motor homes)
		BEGIN
			IF @isTowingPSA = 0
			BEGIN
				SET @basePrice = 265.83 --> Ryczał
			END
			ELSE
			BEGIN
				SET @basePrice = 242.50 --> Ryczał drugie holowanie
			END

			IF @arrivalKm <= 50
			BEGIN
				SET @valueZS = @basePrice
			END
			ELSE
			BEGIN
				SET @valueZS = @basePrice + (@arrivalKm-50)*7.45
			END

			IF @emptyService = 0 AND @noFix = 0 --> Skuteczne holowanie
			BEGIN
				SET @isTowingPSA = 1 --> Ustawienie następnego holowania
			END
		END

		IF @entityId = 10 AND @platformFromProgram IN (14,79,80) --> Naprawa na drodze
		BEGIN
			IF @arrivalKm <= 30
			BEGIN
				SET @valueZS = 186.24
			END
			ELSE
			BEGIN
				SET @valueZS = 186.24 + (@arrivalKm-30)*7.45
			END
		END
		ELSE IF @entityId = 10 AND @platformFromProgram IN (81,82) --> Naprawa na drodze Premium (DS oraz Motor homes)
		BEGIN
			IF @arrivalKm <= 50
			BEGIN
				SET @valueZS = 222.60
			END
			ELSE
			BEGIN
				SET @valueZS = 222.60 + (@arrivalKm-50)*7.45
			END
		END

		--IF @entityId = 17 --> 00N01-Naprawa przez telefon
		--BEGIN
		--	SET @valueZS = 113.49
		--END

		IF @entityId = 26 --> 1901-Porada
		BEGIN
			SET @valueZS = 20.86
		END

		IF @entityId = 29 --> Auto zastępcze
		BEGIN
			IF @customerCarId IN (
				2651, -->	Peugeot ION
				1280, -->	Peugeot 108
				1289, -->	Peugeot 208
				1281, -->	Peugeot 2008
				1291, -->	Peugeot 301
				1297, -->	Peugeot 308
					  -->	Peugeot 308SW
				2653, -->	Peugeot 408
				2638, -->	Peugeot Bipper VP
				2639, -->	Peugeot Partner VP
				2640, -->	Peugeot Rifter

				2643, -->	Citroen C-ZERO
				2645, -->	Citroen E-MEHARI
				332,  -->	Citroen C1
				334,  -->	Citroen C3
				331,  -->	Citroën C-Elysée
				2644, -->	Citroën C3 Aircross
				337,  -->	Citroen C4
				339,  -->	Citroen C4-Cactus
				2642, -->	Citroen Berlingo VP

				1838, -->	Opel Karl
				1238, -->	Opel Adam
				1250, -->	Opel Corsa
				1836, -->	Opel Crossland X
				1258, -->	Opel Mokka
				1244, -->	Opel Astra
				2649, -->	Opel Astra Tourer

				2625) -->	DS 3
			BEGIN
				SET @valueZS = 110.58
			END
			ELSE IF @customerCarId IN (
				1290, -->	Peugeot 3008
				1302, -->	Peugeot 4008
				1307, -->	Peugeot 5008
				1310, -->	Peugeot 508
				2641, -->	Peugeot 508SW
				1322, -->	Peugeot Traveller
				2616, -->	Peugeot Expert 9 os.

				341,  -->	Citroen C4-Picasso
				340,  -->	Citroen C4-Grand-Picasso
				342,  -->	Citroen C5
				2646, -->	Citroen C5 Aircross
				358,  -->	Citroen Spacetourer

				1837, -->	Opel Grandland X
				1274, -->	Opel Zafira
				1254, -->	Opel Insignia
				2650, -->	Opel Insignia Tourer
				2615, -->	Opel Vivaro Combi

				2626, -->	DS 4
				2627, -->	DS 5
				2629) -->	DS 7 Crossback
			BEGIN
				SET @valueZS = 179.45
			END
			ELSE IF @customerCarId IN (
				1316, -->	Peugeot Bipper
				1320, -->	Peugeot Partner VU
				1318, -->	Peugeot Expert
				1317, -->	Peugeot Boxer

				354,  -->	Citroen Nemo
				328,  -->	Citroen Berlingo VU
				353,  -->	Citroen Jumpy
				2661,  -->	Citroen Jumper
				
				1248, -->	Opel Combo
				1273, -->	Opel Vivaro
				1261  -->	Opel Movano
			)
			BEGIN
				SET @valueZS = 223.10
			END
			ELSE
			BEGIN
				SET @valueZS = 0.00
				PRINT 'MW : Brak pojazdu ?: ' + ISNULL(CAST(@customerCarId AS NVARCHAR(10)),dbo.f_translate('NULL',default))
			END
		END

		--IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))<>dbo.f_translate(dbo.f_translate('Polska',default),default) --> Za granica
		--BEGIN
		--	SET @valueZS = 0.00 --> Koszty rzeczywiste
		--END
	END
	--> CFM: Carefleet od 11.06.2019
	ELSE IF @platformFromProgram = 78 --> Carefleet
	BEGIN
		SET @valueZS = 0.00

		--> Usługi wspólne:	
		if @entityId=17 --> Naprawa przez telefon
		begin			
			set @valueZS=100.00 
		end

		if @twrCode='04'
		begin
			declare @prefferedPartner bit = case when 
			-->	Wypo.Prefer.  	  Panek  ,Express , Rentis , Abacus ,Kaizen Fleet, Carnet ,AVIS-Jupol
				@partnerCode IN ('101600','100040','103700','105798','107191'    ,'103493', '100414' ) then 1 else 0 end
		end

		if @entityId=29 --> Wynajem pojazdu zastępczego
		begin
			SET @markaModelAutaWyp = (SELECT TOP 1 value_int FROM attribute_value WITH(NOLOCK)WHERE 
				group_process_instance_id=@groupProcessId AND attribute_path='764,73' AND value_int IS NOT NULL)

			SET @class = (SELECT TOP 1 argument3 FROm dbo.dictionary WITH(NOLOCK) WHERE typeD='makeModel' and value=@markaModelAutaWyp)

			if @partnerId is null
			begin
				print '@partnerId: ' +  isnull(cast(@partnerId as nvarchar(20)),dbo.f_translate('null',default))
				set @summaryQuantity = @Quantity
			end
			else
			begin
				set @summaryQuantity = (
					select SUM(av_quantity.value_int) as dbo.f_translate('Quantity',default)
					from   attribute_value av with(nolock) 
					join   attribute_value av_quantity with(nolock) on av_quantity.parent_attribute_value_id = av.id 
							and av_quantity.attribute_path='820,821,822,221'
					join   attribute_value av_groupId with(nolock) on av_groupId.id=av.parent_attribute_value_id
					join   attribute_value av_partner with(nolock) on av_partner.group_process_instance_id = av_groupId.value_int 
							and av_partner.attribute_path='764,742'
					join   attribute_value av_makeModel with(nolock) on av_makeModel.group_process_instance_id = av_groupId.value_int 
							and av_makeModel.attribute_path='764,73'
					join   dictionary d_class with(nolock) on d_class.value=av_makeModel.value_int and d_class.typeD='makeModel'
					where  av.attribute_path='820,821,822' and av.value_int=29 and av.root_process_instance_id=@rootId
							and av_partner.value_int=@partnerId and d_class.argument3=@class
				)
			end
			PRINT '@partnerCode' + isnull(@partnerCode,dbo.f_translate('NUllos',default))
			-->	Wypo.Prefer.	 Panek  ,Express , Rentis , Abacus ,Kaizen Fleet, Carnet ,AVIS-Jupol
			IF @partnerCode IN ('101600','100040','103700','105798','107191'    ,'103493', '100414' )
			--OR EXISTS( --> Serwisy preferowane
			--	SELECT  d.textD FROM f_split(@partnerKinds,',') fs
			--	JOIN dictionary d WITH(NOLOCK) ON d.value=fs.data AND d.typeD = 'PartnerLocationType'
			--	WHERE ( d.textD LIKE '%BRS%' OR
			--			d.textD LIKE '%Warsztaty naprawy szyb%' OR --'%ALPHABET SZYBY%' OR
			--			d.textD LIKE '%Warsztat wulkanizacyjny%' OR --'%ALPHABET OPONY%' OR
			--			d.textD LIKE '%Alphabet ASO Preferowane%' OR
			--			d.textD LIKE '%ALPHABET ASN%' OR
			--			d.textD LIKE '%ALPHABET S-PLUS%' OR
			--			d.textD LIKE '%ASO%' )
			--)
			BEGIN
				PRINT dbo.f_translate('Partner preferowany. Koszt 0.00zł',default)
				SET @valueZS = 0.00 --> Partner preferowany
			END
			ELSE
			BEGIN --> Serwisy niepreferowane
				IF @isBRS = 1
				BEGIN
					PRINT dbo.f_translate('WYPOŻYCZALNIA BRS',default)
					SET @valueZS = @value / @quantity/*CASE 
					WHEN @class IN (1,3) THEN CASE --> 'A' i 'B'	
						when @summaryQuantity <= 7 then 90
						when @summaryQuantity > 7 then 90 
						else null end
					WHEN @class IN (7) THEN CASE --> 'C'
						when @summaryQuantity <= 7 then 110
						when @summaryQuantity > 7 then 110
						else null end
					WHEN @class IN (11) THEN CASE --> 'D'
						when @summaryQuantity <= 7 then 150
						when @summaryQuantity > 7 then 150
						else null end
					WHEN @class IN (24) THEN CASE--> 'N'
						when @summaryQuantity <= 7 then 180
						when @summaryQuantity > 7 then 180
						else null end	
					ELSE 0.00 END --> KOSZT RZECZYWISTY*/
				END
				ELSE
				BEGIN
					SET @valueZS = CASE 
					WHEN @class IN (3) THEN CASE --> 'B'			
						when @summaryQuantity <= 3 then 90
						when @summaryQuantity <= 14 then 85
						when @summaryQuantity > 14 then 80 
						else null end
					WHEN @class IN (7) THEN CASE --> 'C'
						when @summaryQuantity <= 3 then 115
						when @summaryQuantity <= 14 then 105
						when @summaryQuantity > 14 then 100
						else null end
					WHEN @class IN (11) THEN CASE --> 'D'
						when @summaryQuantity <= 3 then 150
						when @summaryQuantity <= 14 then 140
						when @summaryQuantity > 14 then 130
						else null end
					WHEN @class IN (16,19) THEN CASE --> 'E'
						when @summaryQuantity <= 3 then 260
						when @summaryQuantity <= 14 then 250
						when @summaryQuantity > 14 then 240
						else null end
					WHEN @class IN (8,23) THEN CASE --> 'M' 
						when @summaryQuantity <= 3 then 135
						when @summaryQuantity <= 14 then 120
						when @summaryQuantity > 14 then 110
						else null end
					WHEN @class IN (24) THEN CASE--> 'N' bez 14,17,26
						when @summaryQuantity <= 3 then 220
						when @summaryQuantity <= 14 then 210
						when @summaryQuantity > 14 then 200
						else null end	
					ELSE 0.00 END --> KOSZT RZECZYWISTY
				END
			END
			print dbo.f_translate('KLASA WYNAJMOWANEGO AUTA: ',default) + CAST(@class AS NVARCHAR(10))
		end

		if @entityId in (30,31)	AND @isBRS=0 -->	0402-Podstawienie, 0403-Odbiór od klienta
		begin
			set @valueZS=30.00
			if @prefferedPartner = 1
			begin
				set @valueZS=@value
			end
		end

		IF @serieZS = dbo.f_translate('CAR',default)
		BEGIN
			if @entityId=1 -- 0201-Holowanie
			begin	
				if @dmc < 3500
				begin
					set @valueZS=210.00
					if @towingKm > 50
					begin
						set @valueZS = @valueZS + 5.00 * (@towingKm - 50)
					end
				end
				else if @dmc >= 3500 and @dmc < 7500
				begin
					set @valueZS=350.00
					if @towingKm > 50
					begin
						set @valueZS = @valueZS + 10.00 * (@towingKm - 50)
					end
				end
				else --> Brak lub za duże DMC
				begin
					set @valueZS=0.00
					print 'Brak lub za duże DMC!'
				end
			end

			if @entityId=10 -- 0101-Naprawa na drodze
			begin	
				set @valueZS = 198.00
			end
		END
		ELSE IF @serieZS = dbo.f_translate('CARA',default) --> AXA = IPA 
		BEGIN
			if @entityId=1 -- 0201-Holowanie
			begin	
				if @dmc < 3500
				begin
					set @valueZS=200.00
					if @towingKm > 50
					begin
						set @valueZS = @valueZS + 5.00 * (@towingKm - 50)
					end
				end
				else if @dmc >= 3500 and @dmc < 7500
				begin
					set @valueZS=350.00
					if @towingKm > 50
					begin
						set @valueZS = @valueZS + 10.00 * (@towingKm - 50)
					end
				end
				else --> Brak lub za duże DMC
				begin
					set @valueZS=0.00
					print 'Brak lub za duże DMC!'
				end
			end

			if @entityId=10 -- 0101-Naprawa na drodze
			begin	
				set @valueZS = 180.00
			end

			if @entityId=17 --> Naprawa przez telefon
			begin			
				set @valueZS=100.00 
			end
		END
		ELSE
		BEGIN
			SET @valueZS = NULL --> Brak serii nie importujemy.
		END

		IF @entityId=1 and @isBRS=1 and @noFix=0 and @emptyService=0--> BRS Carefleet Hol
		BEGIN
			PRINT dbo.f_translate('HOLOWANIE BRS',default)
			IF @towingKm <= 30
			BEGIN
				SET @valueZS=150.00
			END
			ELSE
			BEGIN
				SET @valueZS=200.00
			END
		END

		IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))<>dbo.f_translate(dbo.f_translate('Polska',default),default)
		BEGIN
			SET @valueZS = 0.00 --> Zagranica koszty rzeczywiste
		END
	END

	IF @type = 10 --> Cennik ADH
	BEGIN
		print '@towingKm:' + cast(@towingKm as nvarchar(20))
		set @valueZS=0.0

		if @entityId = 10
		begin
			if @arcCode3Id = 80 or @isTireRepair = 0
			begin
				set @valueZS=160.00/1.23
			end
			else
			begin
				set @valueZS=240.00/1.23
			end
		end

		if @entityId = 1
		begin
			print '@dmc:' + cast(@dmc as nvarchar(20))
			if @dmc<=2190 -- 2820
			begin	
				if @additTownigADH=1
				begin			
					set @valueZS=@towingKm*(5.0/1.23)
				end
				else
				begin --> Skuteczna naprawa
					set @valueZS=240.00/1.23
					if @towingKm>25
					begin
						set @valueZS=@valueZS+(@towingKm-25)*(5.0/1.23)
					end
				end
			end 
			else if @dmc<=3390 -- 2820
			begin
				if @additTownigADH=1 
				begin
					set @valueZS=@towingKm*(7.0/1.23)
				end
				else
				begin
					set @valueZS=280.00/1.23
					if @towingKm>25
					begin
						set @valueZS=@valueZS+(@towingKm-25)*(7.0/1.23)
					end
				end
				print dbo.f_translate('ADH Test',default)
			end
			else
			begin
				if @additTownigADH=1 
				begin
					set @valueZS=@towingKm*(8.80/1.23)
				end
				else
				begin
					set @valueZS=380.00/1.23
					if @towingKm>25
					begin
						set @valueZS=@valueZS+(@towingKm-25)*(8.80/1.23)
					end
				end
			end
		end

		if @entityId=3 -- roboczogodziny
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			set @valueZS=80.00/1.23
		end 
	
		if @entityId=16 -- dźwig
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			if @dmc<=2599
			begin
				set @valueZS=150.0		
			end		
			else if @dmc<=3399
			begin
				set @valueZS=170.0
			end		
			else if @dmc<=5699
			begin
				set @valueZS=170.0
			end
		end 
	
		if @emptyService=1
		begin
			if @type in (1,10) 
			begin
				set @type=4 
				set @quantity=1
			end
			else
			begin
				set @quantity=null
			end
		end

		if @noFix=1 --> I nie występuje w sprawie udane holowanie
		begin
			set @valueZS=50.0/1.23
		end

		if @rootId=05717549 and @noFix=1 
		begin
			set @valueZS=0.0
		end

		if @entityId=11 -- taxi w osobnym pojeździe w usłudze holowania (nieaktywny)
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,154', @groupProcessInstanceId = @groupProcessId
			SELECT @persons = value_int FROM @values	

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,647', @groupProcessInstanceId = @groupProcessId
			SELECT @km = value_int FROM @values	
	
			set @valueZS=@km*3.0/1.23
		end 

		print '@valueZS ADH: ' + ISNULL(CAST(@valueZS AS NVARCHAR(50)),dbo.f_translate('NULL',default))


	END

	-->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->> o <<-- u -->>

	if @dni>0 and @entityId=29
		begin 
			set @valueZS=@valueZS/@dni*@quantityZ 
			print dbo.f_translate('dnidd',default)
		end
	else 
		begin
		set @valueZS=@valueZS*@quantityZ 
		end

	-- ROZLICZENIE DLA PUSTEGO WYJAZDU I NAPRAWY NIEUDANEJ.
	if @entityId IN (1,10) AND (@noFix = 1 OR @emptyService = 1)  AND @type<>10
	begin
		if @programId in (483,482,481,480,479,478,477,476,475,497,496,495,494,446,445,444,443)
		--@platformId in (35,18)-- Dla programów z listy, rozliczamy pusty wyjazd i naprawę nieudaną na ZS.
		or @programId in (507,498,499,501,502,503,504) -- ARC, AA, ANWB, RACE, Rosqvist, TCB, TCS (Brak pustych wyjazdów)
		or @programId in (500) -- OAMTC (członkowie klubu austriackiego)
		or @platformFromProgram = 5 --> ADAC
		or @platformFromProgram in (79,80,81,82) --> PSA (Pusty Wyjazd)
		or @platformFromProgram = 78 --> Carefleet rozliczy (Pusty Wyjazd) oraz naprawę nieudaną
		begin
			set @valueZS=0.00

			if @emptyService = 1 and @programId in (475,476,477,478,479,480,481,482,483)--@platformId = 35 -- TUW TUZ		
			begin
				set @valueZS=70
			end

			if @emptyService = 1 and @programId in (443,444,445,446,494,495,496,497)--@platformId = 18  -- Concordia Assistance			
			begin
				set @valueZS=90.41
			end

			if @programId in (500) -- OAMTC (członkowie klubu austriackiego)
			begin
				set @valueZS = 83.59 -- Ryczałt do 25km
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 2.10 * (@towingKm - 25)
				end
			end

			if @platformFromProgram = 5 --> Pusty Wyjazd i Naprawa Nieudana - ADAC
			begin
				set @valueZS = 83.59 -- Ryczałt do 25km
				if (@towingKm+@backKm+@arrivalKm) > 25
				begin
					set @valueZS = @valueZS + 2.10 * ((@towingKm+@backKm+@arrivalKm) - 25)
				end
			end

			if @emptyService = 1 and @platformFromProgram in (79,80,81,82) --> PSA (Pusty Wyjazd)
			begin
				set @valueZS=97.00
			end

			if @platformFromProgram = 78 --> Carefleet rozliczy (Pusty Wyjazd) oraz naprawę nieudaną
			begin
				set @valueZS=70.00
			end
		end
		else
		begin
			if @emptyService=1 
			begin
				set @valueZS=0
			end
			else if @towVehicleType=2 and @noFix=1 -- pojazd patrolowy, i naprawa nieudana (Płacimy, ZZ=70,ZS=0)
			begin
				set @valueZS=0
			end
			else
			begin
				set @valueZS=null
			end
		end
	end

	if @entityId in (1,10) and @isGopNH=1 --> Organizowanie usługi N/H na GOP.
	begin
		set @valueZS=0.00
		--(select top 1 value_int from attribute_value with(nolock) where group_process_instance_id=@groupProcessId 
					--and attribute_path='638,957' and value_int is not null)
	end
	
	--DEBUG
	if @entityId = 29
	begin
		print '@dni: ' + ISNULL(CAST(@dni AS NVARCHAR(50)),dbo.f_translate('NULL',default))	
	end
	print '@valueZS: ' + ISNULL(CAST(@valueZS AS NVARCHAR(50)),dbo.f_translate('NULL',default))
	print '@quantityZ: ' + ISNULL(CAST(@quantityZ AS NVARCHAR(50)),dbo.f_translate('NULL',default))

	if ((@czyASO=0 or @entityId in (10,18,19,21,22,23,25,26,43,44,63)) or @type<>1 or @programId=428 or 
		(select platform_id from vin_program where id=@programId) in (10,43,48,25,53,78) --> Dla programów z platformy ALD,BL i LP i ALPHABET CAREFLEET ma sie przenosić mimo że ASO
	) and @quantity>0 and @skipStarterCodeBL=0 AND @programId IS NOT NULL
	begin 
		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			groupProcessId,
			programId
		)	
		SELECT  
		@headerZS,
		@quantityXL,    ---@quantityZ MP
		@twrCode,
		@valueZS,
		isnull(@currency,dbo.f_translate(dbo.f_translate('PLN',default),default)),
		@entityValueId,
		@entityId,
		@groupProcessId,--CASE WHEN (@reverseGroupId < 0) THEN @reverseGroupId ELSE @groupProcessId END,
		@programId
					
		print @valueZS
	end
	else
	begin
		print 'Pomija wstawienie ZamElem(ZS). ((@czyASO=0 or @entityId in (10,18,19,21,22,23,25,26,43,44,63)) or @type<>1 or @programId=428) and @quantity>0'
		print '@czyASO: ' + ISNULL(CAST(@czyASO AS NVARCHAR(50)),dbo.f_translate('NULL',default))
		print '@type: ' + ISNULL(CAST(@type AS NVARCHAR(50)),dbo.f_translate('NULL',default))
		print '@quantity: ' + ISNULL(CAST(@quantity AS NVARCHAR(50)),dbo.f_translate('NULL',default))
		print '@skipStarterCodeBL: ' + ISNULL(CAST(@skipStarterCodeBL AS NVARCHAR(50)),dbo.f_translate('NULL',default))	

		IF @programId IS NULL
		BEGIN
			set @valueZS=NULL
			print 'BRAK PROGRAMU! Nie importuje'
		END

	end
	
	FETCH NEXT FROM kurZ INTO @entityValueId, @groupProcessId, @serviceId, @entityId, @quantity, @type, @name, @quantityXL,@programId;
END
CLOSE kurZ
DEALLOCATE kurZ

UPDATE zn
set zn.XL_Opis=isnull(XL_opis,'')+isnull(' '+dt.descr,'')
from sync.ZamNag zn WITH(NOLOCK) inner join
	@descrTable dt on dt.idHeader=zn.Id

declare @znsId int
declare @znzId int

select	@znsId=zns.Id,
		@znzId=znz.id
from	sync.ZamNag zns WITH(NOLOCK) inner join
		sync.ZamNag znz WITH(NOLOCK) on zns.id=znz.ParentZsId 
where	zns.rootId=@rootId and
		znz.rootId=@rootId and 
		zns.XL_ZamSeria=dbo.f_translate('ADH',default)

if @znsId is not null
begin  
	declare @sumZZ decimal(18,2)
	declare @sumZS decimal(18,2)

	select	@sumZS=sum(XL_Wartosc)
	from	sync.ZamElem ze WITH(NOLOCK) 
	where	ze.AUD_IdDok=@znsId and entityDefId not in (9,5) /*bez autostradan viatol pusty przejazd i nieudana naprawa jesli hol ktos inny*/

	select	@sumZZ=sum(XL_Wartosc)
	from	sync.ZamElem ze WITH(NOLOCK) 
	where	ze.AUD_IdDok=@znzId and entityDefId not in (9,5) /*bez autostradan viatol pusty przejazd i nieudana naprawa jesli hol ktos inny*/

	delete from sync.ZamElem where AUD_IdDok=@znsId and entityDefId not in (9,5)
	delete from sync.ZamElem where AUD_IdDok=@znzId and entityDefId not in (9,5)

	declare @adgGroupId int = (select top 1 group_process_instance_id from attribute_value WITH(NOLOCK) where attribute_path='202' and value_string='423' and root_process_instance_id=@rootId order by id)

	declare @roznica decimal(18,2)=@sumZS-@sumZZ

	print '@sumZS :' + ISNULL(CAST(@sumZS AS NVARCHAR(20)),dbo.f_translate('NULL',default)) + ' - ' + '@sumZZ :' + ISNULL(CAST(@sumZZ AS NVARCHAR(20)),dbo.f_translate('NULL',default)) + ' = ' + '@roznica :' + ISNULL(CAST(@roznica AS NVARCHAR(20)),dbo.f_translate('NULL',default))

	if @roznica<0
	begin
		INSERT INTO sync.ZamNag (
			ParentZsId,
			AUD_GidTyp, -- 960
			XL_DataRealizacji, -- data statusu progres 4
			XL_DokumentObcy, -- caseId
			XL_KntAkronim,
			XL_Opis, -- program name
			XL_Typ, -- ZS/ZZ
			XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
			rootId,--SO_SprawaId, -- rootId
			type,
			partnerLocId,
			XL_DataWystawienia
		)
		SELECT 
		null,
		@gidTypeZS, 
		getdate(),--@finishedAt, 
		@caseId,  
		@akronimZSADH, 
		@description, 
		'ZZ',
		'',	
		@rootId,
		@type,
		@partnerId,
		@caseDate

		declare @headerOplataADH2 int
		set @headerOplataADH2=scope_identity()

		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			groupProcessId,
			programId
		)	
		SELECT  
		@headerOplataADH2,
		1,
		dbo.f_translate('KON1',default), -- ADH Prowizja nazwa było -1
		@roznica*-1,
		dbo.f_translate(dbo.f_translate('PLN',default),default),
		null,
		null,
		@adgGroupId,
		423

		set @roznica=0
	end
	-------------------

	if @roznica=0 -- W przypadku, gdy dla programu Ad-hoc mamy obciążenie po stronie ZZ, proszę aby w każdym takim przypadku na ZS był kontrahent 111111.
	begin
		set @akronimZSADH = '111111' 
	end 

	INSERT INTO sync.ZamNag (
			ParentZsId,
			AUD_GidTyp, -- 960
			XL_DataRealizacji, -- data statusu progres 4
			XL_DokumentObcy, -- caseId
			XL_KntAkronim,
			XL_Opis, -- platforma + dbo.f_translate('atlas',default)
			XL_Typ, -- ZS/ZZ
			XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
			rootId,--SO_SprawaId, -- rootId
			type,
			partnerLocId,
			XL_DataWystawienia
		)
		SELECT 
		null,
		@gidTypeZS, 
		getdate(), --@finishedAt, 
		@caseId,  
		@akronimZSADH, 
		@description, 
		'ZS',
		dbo.f_translate('ADH',default),	
		@rootId,
		@type,
		@partnerId,
		@caseDate

		declare @headerOplataADH int
		set @headerOplataADH=scope_identity()
		set @description=dbo.f_translate('ADHOC ',default)+ISNULL(@programName,'')
		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			groupProcessId,
			programId
		)	
		SELECT  
		@headerOplataADH,
		1,
		dbo.f_translate('KON1',default), -- ADH Prowizja nazwa było -1
		@roznica,
		dbo.f_translate(dbo.f_translate('PLN',default),default),
		null,
		null,
		@adgGroupId,
		423
end

--> ========================================================================================== <--
-- Opłata za sprawę / Opłata za sprawę / Opłata za sprawę / Opłata za sprawę / Opłata za sprawę / 
--> ========================================================================================== <--
set @valueZS = 0.01
declare @headerOplata int
declare @casePaymentType int

DECLARE	@entityDefId INT
DECLARE	@wasNH BIT

-- ====================================================== --
-- \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CF --

/*	1	Wypadek/Szkoda
	2	Awaria
	3	Sprawa nie związana z pojazdem
	5	Szkoda jezdna
	6	Kradzież
	7	Błąd pilotażu
	8	Szkoda całkowita
	9	Porada	*/
DELETE FROM @values --> Rodzaj zdarzenia
INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @rootId
DECLARE @eventType INT = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)

/*	85	BRS Priorytet 1
	94	BRS Priorytet 1A
	95	BRS Zawieszone
	98	BRS LUM
	99	BRS Poszerzony obszar obsługi
	100	BRS Serwis preferowany
	101	BRS COMPET	*/
SET @isBRS = 0
IF EXISTS (
	SELECT av_p.value_string,av.* FROM attribute_value av WITH(NOLOCK) 
	JOIN attribute_value av_p WITH(NOLOCK) ON av.value_int=av_p.parent_attribute_value_id AND av_p.attribute_path = '595,597,596' 
	AND EXISTS ( SELECT * FROM dbo.f_split (av_p.value_string,',') WHERE data in (85,94,95,98,99,100,101))
	WHERE av.root_process_instance_id=@rootId AND av.attribute_path='610' AND av.value_int IS NOT NULL
	AND (SELECT TOP 1 status_dictionary_id FROM service_status WITH(NOLOCK) WHERE group_process_id=av.group_process_instance_id ORDER BY id DESC)<>-1
)BEGIN
	SET @isBRS = 1
END

DECLARE @onlyAlphabetPrograms BIT = 1 --> W sprawie istnieją tylko programy Alphabet
IF EXISTS(
	SELECT av.value_string FROM attribute_value av WITH(NOLOCK) 
	WHERE av.root_process_instance_id=@rootId AND av.attribute_path='202' AND av.value_string IS NOT NULL
	AND av.value_string NOT IN (select id from dbo.vin_program where platform_id=53 union all select 428) --> Alphabet + Strata
)
BEGIN
	SET @onlyAlphabetPrograms = 0 --> Mix programów
END

DECLARE @LP_Programs BIT = CASE WHEN EXISTS (
	SELECT av.value_string FROM attribute_value av WITH(NOLOCK) 
	WHERE av.root_process_instance_id=@rootId AND av.attribute_path='202' AND av.value_string IS NOT NULL
	AND av.value_string IN (select id from dbo.vin_program where platform_id=25) --> Leaseplan
	AND NOT EXISTS (SELECT TOP 1 1 FROM process_instance pin WITH(NOLOCK) WHERE pin.group_process_id=av.group_process_instance_id AND LEFT(pin.step_id,4) IN ('1088','1011','1022'))
) THEN 1 ELSE 0 END

DECLARE @LP_ProducerPrograms BIT = CASE WHEN EXISTS (
	SELECT av.value_string FROM attribute_value av WITH(NOLOCK) 
	WHERE av.root_process_instance_id=@rootId AND av.attribute_path='202' AND av.value_string IS NOT NULL
	AND av.value_string NOT IN (select id from dbo.vin_program where platform_id=25 union select 423) --> Leaseplan + ADH
	AND NOT EXISTS (SELECT TOP 1 1 FROM process_instance pin WITH(NOLOCK) WHERE pin.group_process_id=av.group_process_instance_id AND LEFT(pin.step_id,4) IN ('1088','1011','1022'))
	AND (SELECT TOP 1 status_dictionary_id FROM service_status ss WITH(NOLOCK) WHERE ss.group_process_id=av.group_process_instance_id ORDER BY id DESC)<>-1 --> Bez anulowanych bezkosztowo
) THEN 1 ELSE 0 END

--> Czy w sprawie jest Monitoring Szkody?
DECLARE @wasDetrimentMonitoring BIT = 0 
IF EXISTS(SELECT TOP 1 1 FROM process_instance WITH(NOLOCK) WHERE root_id=@rootId AND step_id LIKE '1155.%')
BEGIN
	SET @wasDetrimentMonitoring = 1
END
-- \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CF --
-- ====================================================== --

DECLARE @tmpPlatformID INT
DECLARE @PROGRAMS AS TABLE(
	platformId INT,
	programId INT,
	entityDefId INT,
	entityValueId INT,
	wasNH BIT
)

INSERT  INTO @PROGRAMS 
	SELECT DISTINCT vp.platform_id as platformId,ze.programId,ze.entityDefId,ze.entityValueId,
	(CASE WHEN vp.platform_id=2 AND ze.entityDefId IN (1,10) THEN 1 ELSE 0 END) wasNH

	FROM [sync].[ZamElem] ze WITH(NOLOCK) 
	LEFT JOIN [sync].[ZamNag] zn WITH(NOLOCK) on ze.AUD_IdDok=zn.Id AND zn.AUD_PrzeniesionoDoErpXl IS NULL
	LEFT JOIN dbo.vin_program vp WITH(NOLOCK) ON vp.id=ze.programId
	WHERE zn.rootId = @rootId
	AND ze.XL_TwrKod <> '-1'
	AND (
	
	ze.programId IN (select distinct data from f_split((select CONCAT(programId,',') from dbo.casePayment with(nolock) FOR XML PATH('')),','))

	OR 
	
	vp.platform_id IN (select distinct data from f_split((select CONCAT(platformId,',') from dbo.casePayment with(nolock) FOR XML PATH('')),','))
	)

	--> Wykluczenie: VGP (Tylko usługi główne. Nie generuje się dla pustych wyjazdów i napraw nieudanych)
	AND (CASE WHEN vp.platform_id IN (6,11,31) AND ( --> Poniżej kryteria dla których ma niewystępować opłata za sprawę.
				zn.XL_Opis LIKE '%Pusty wyjazd%' 
			OR zn.XL_Opis LIKE '%Naprawa nieudana%'
			OR ze.entityDefId NOT IN (10,1,21,29,43,19,23,14,22,63) --> Tylko usługi główne X
			OR (SELECT ISNULL(av_P.value_int,0) FROM attribute_value av_P WITH(NOLOCK) WHERE av_P.parent_attribute_value_id=ze.entityValueId AND attribute_path='820,821,822,824')=0 --> Rozliczenie na producenta i brak unaksy
		) AND @unacsaCase=0
	THEN 0 ELSE 1 END) = 1

	--> Wykluczenie: Ford (Brak opłaty dla programów z pustym wyjazdem i naprawą nieudaną)
	AND (CASE WHEN vp.platform_id = (2) AND ( --> Poniżej kryteria dla których ma niewystępować opłata za sprawę.
				zn.XL_Opis LIKE '%Pusty wyjazd%' 
				OR zn.XL_Opis LIKE '%Naprawa nieudana%'
		)
	THEN 0 ELSE 1 END) = 1

	--> Wykluczenie: ARC, AA, ANWB, RACE, Rosqvist, TCB, TCS (Opłata za sprawę występuje tylko jeśli jest usługa MEDICAL)
	AND (CASE WHEN @programId IN (507,498,499,501,502,503,504) AND @IsMedicalInCase = 0 THEN 0 ELSE 1 END) = 1

	--> Wykluczenie: ALD tylko jeśli jest transport.
	AND (CASE WHEN @platformId = 43 AND @isTransport = 0 THEN 0 ELSE 1 END) = 1

DECLARE casePayment CURSOR
FOR 
SELECT sub.platformId,programId,wasNH FROM (SELECT ROW_NUMBER() OVER(PARTITION BY platformId ORDER BY platformId,wasNH DESC) AS RowN, * FROM @PROGRAMS) sub WHERE sub.RowN=1
OPEN casePayment
FETCH NEXT FROM casePayment INTO @platformId,@programId,@wasNH

WHILE @@FETCH_STATUS = 0
BEGIN
	-- //  
	SET @serieZS = NULL
	SET @XL_FlagaNBZS = NULL
	DECLARE @skipCasePayment BIT = 0 --> Mechanizm sprawdzający czy dla sprawy już istnieje opłata za sprawę. Jesli tak to pomija dodawanie.
	----------------------------------
	DECLARE @generateZS BIT = 0
	SET @casePaymentType = 1
	SET @programName = (select top 1 name from dbo.vin_program with(nolock) where id=@programId)

	--> Pobieranie zagłówna, do którego ma zostać podłączona opłata za sprawę.
	SET	@headerOplata = (	select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
							where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim<>'111111')

	IF @platformId = 2 AND @programId = 436	--> Ford Ubezpieczenia Plus, opłata nie ma być z ZS w którym jest pusty wyjazd lub naprawa nieudana
	BEGIN
		SET	@headerOplata = (	select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
							where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	
							and zn.XL_Opis not like '%Pusty%' and zn.XL_Opis not like '%Nieudana%' and XL_KntAkronim <> '111111')
	END

	IF @platformId = 2 AND @wasNH <> 1 --> Ford
	BEGIN
		SET @AkronimZS = '100009'
		SET @casePaymentType = 2
	END

	IF @platformId IN (6,11,31) --> VGP
	BEGIN
		SET @AkronimZS='103600'
		-------------------------
		if @programId in (414,415,427,426) -- Audi i ARC
		begin
			SET @serieZS = 'AU'
		end

		if @programId in (416,402,421,427,425) -- Skoda i ARC
		begin
			SET @serieZS = dbo.f_translate('SKO',default)
		end

		if @programId in (417,418,419,420,427,424) -- Volkswagen i ARC
		begin
			SET @serieZS = 'VW'
		end
		
		SET @generateZS = 1
		/*
		IF @unacsaCase=1
		BEGIN
			SET @casePaymentType=2
		END
		ELSE
		BEGIN
			SET @casePaymentType=1
		END
		*/
	END

	IF @platformId IN (53) --> ALPHABET
	BEGIN
		set @AkronimZS='105000'

		declare @isAlphabetPrograms bit = 0
		IF EXISTS (
			SELECT TOP 1 zn.id FROM [SYNC].[ZAMELEM] ZE with(nolock) LEFT JOIN [SYNC].[ZAMNAG] ZN with(nolock) ON ZE.AUD_IDDOK=ZN.ID 
			WHERE ze.programId IN (select id from dbo.vin_program with(nolock) where platform_id IN (53)) 
			AND rootId=@rootId AND XL_Typ='ZS' AND ze.programId IS NOT NULL AND XL_TwrKod <> '-1' AND XL_Wartosc IS NOT NULL
		)
		BEGIN
			SET @isAlphabetPrograms = 1
		END
		/*	1	Wypadek/Szkoda
			2	Awaria
			3	Sprawa nie związana z pojazdem
			5	Szkoda jezdna
			6	Kradzież
			7	Błąd pilotażu
			8	Szkoda całkowita
			9	Porada	*/
		set @casePaymentType = case 
			when @eventType in (1,2,3,6,7,8) AND @isBRS=0 AND @onlyAlphabetPrograms=1 AND @isAlphabetAdviceOnce=0 then 1
			when @eventType in (1,2,3,6,7,8) AND @isBRS=1 AND @onlyAlphabetPrograms=1 AND @isAlphabetAdviceOnce=0 then 2
			when @isAlphabetAdviceOnce=1 AND @wasDetrimentMonitoring=0 then 3
			when @wasDetrimentMonitoring=1 then 4 /*@eventType =  5 AND*/ 
			when @onlyAlphabetPrograms=0 and @isAlphabetPrograms=1 then 5 --> Mix programów, istnieją producenckie i istnieją alphabetowe
		else null end

		if @casePaymentType is null --> Opłata za sprawę nie zakwalifikowała się do żadnego typu, pomijanie.
		begin
			print dbo.f_translate('Opłata za sprawę nie zakwalifikowała się do żadnego typu, pomijanie.',default)
			set @skipCasePayment=1
		end

		--> Szuka nagłówka do programu głównego
		SET	@headerOplata = (select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
							where zn.rootId = @rootId and ze.programId = @caseMainProgram and zn.XL_Typ = 'ZS' and XL_KntAkronim=@AkronimZS and zn.AUD_PrzeniesionoDoErpXl is null
							and XL_KntAkronim <> '111111')
		
		--> Szuka nagłówka do pierwszej usługi Alphabet
		IF @headerOplata IS NULL
		BEGIN
			SET	@headerOplata = (
				SELECT TOP 1 zn.id FROM [SYNC].[ZAMELEM] ZE with(nolock) LEFT JOIN [SYNC].[ZAMNAG] ZN with(nolock) ON ZE.AUD_IDDOK=ZN.ID 
				WHERE ze.programId IN (select id from dbo.vin_program with(nolock) where platform_id IN (53)) 
				AND rootId=@rootId AND XL_Typ='ZS' AND ze.programId IS NOT NULL and XL_KntAkronim <> '111111' ORDER BY ze.groupProcessId ASC
			)	
		END
	END

	IF @platformId IN (58) --> ALPHABET WSPARCIE KIEROWCY, DODAJE NA KOŃCU
	BEGIN
		set @AkronimZS='105000'
		set @casePaymentType = 3
	END

	IF @platformId IN (43) --> ALD
	BEGIN
		set @AkronimZS='102725'
		set @casePaymentType = case when @isTransport=1 then 1 else null end

		SET	@headerOplata = (select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
							where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim=@AkronimZS
							and ze.entityDefId=63 and XL_KntAkronim <> '111111')
		
		IF @headerOplata IS NULL
		BEGIN
			SET	@headerOplata = (select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
							where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim=@AkronimZS and XL_KntAkronim <> '111111')
		END

		IF @isTransport=0 --> Transport jest wymagany by została zaliczona opłata za sprawę.
		BEGIN
			SET @skipCasePayment = 1
			PRINT 'ALD » Brak transportu.'
		END

	END
	/*	1	Wypadek/Szkoda
		2	Awaria
		3	Sprawa nie związana z pojazdem
		5	Szkoda jezdna
		6	Kradzież
		7	Błąd pilotażu
		8	Szkoda całkowita
		9	Porada	*/
	IF @platformId IN (48) --> BL
	BEGIN
		set @AkronimZS='104000'

		set @casePaymentType = case 
		when @eventType in (2,7) then 1 --> 106,80zł Awaria lub błąd pilotażu
		when @eventType in (1,3,6,8) and @isTowing=1 then 3 --> 80,40zł Wypadek z holowaniem
		when @eventType in (1,5) then 2 --> 37,60 zł Wypadek, Szkoda jezdna
		else null end

		--> Pobieranie nagłówka, do którego ma zostać podłączona opłata za sprawę.
		SET	@headerOplata = (	select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
								where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim=@AkronimZS and XL_KntAkronim <> '111111')
		IF ISNULL(@headerOplata,0)=0
		BEGIN
			SET @generateZS = 1
			SET @serieZS='BL'
		END

		if not exists (select top 1 1 from [sync].[ZamElem] ze with(nolock) join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id where zn.rootId = @rootId and isnull(ze.entityDefId,0) not in (0,26)) AND @wasDetrimentMonitoring=0
		--> Jeśli nie istnieją inne usługi niż Auto Zastępcze
		begin
			set @skipCasePayment=1
		end
	END

	IF @platformId IN (25) --> Leaseplan
	BEGIN
		set @AkronimZS='102800'
		set @casePaymentType = case 
			when @LP_Programs = 1 AND @LP_ProducerPrograms=0 AND @allServicesCount > 0 then 1 --> Programy Leaseplan
			when @LP_Programs = 1 AND @LP_ProducerPrograms=1 AND @allServicesCount > 0 then 2 --> Mix programów
		else null end

		if @casePaymentType is null --> Opłata za sprawę nie zakwalifikowała się do żadnego typu, pomijanie.
		begin
			print dbo.f_translate('Opłata za sprawę nie zakwalifikowała się do żadnego typu, pomijanie.',default)
			set @skipCasePayment=1
		end

		--> Pobieranie nagłówka, do którego ma zostać podłączona opłata za sprawę.
		SET	@headerOplata = (	
			select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
			where zn.rootId = @rootId and ze.programId in (select id from vin_program with(nolock) where platform_id=@platformId) and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null and XL_KntAkronim=@AkronimZS and XL_KntAkronim <> '111111')
		IF ISNULL(@headerOplata,0)=0
		BEGIN
			SET @generateZS = 1
			SET @serieZS='LP'
		END

		declare @casePaymentHeaderLP int = (
			(select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
			where zn.rootId = @rootId and ze.programId in (select id from vin_program with(nolock) where platform_id=@platformId) and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null and XL_Opis like '%EIIE%' and XL_KntAkronim=@AkronimZS and XL_KntAkronim <> '111111')
		)

		if isnull(@casePaymentHeaderLP,0) <> 0 --> Opłata za sprawę w pierwszej kolejności powinna się przypisywać do zleceń EIIE 
		begin
			set @headerOplata = @casePaymentHeaderLP
		end
	END

	IF @platformId IN (8,13,16,20,21,22,50,64,68,73,74) --> TRUCK
	BEGIN
		SET @casePaymentType = 1
		SET	@headerOplata = (	select TOP 1 zn.id from [sync].[ZamElem] ze with(nolock) left join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id
								where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim=@AkronimZS and XL_TwrKod in ('31','32','33','34') and XL_KntAkronim <> '111111')

		IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))<>dbo.f_translate(dbo.f_translate('Polska',default),default) --> Dla spraw zagranicznych wartość typu jest na minusie.
		BEGIN -- sprawy za granica powinny sie przenosić tylko z dowozem gotówki
			SET @valueZS = NULL
			SET @skipCasePayment = 1
		END

			--SET @valueZS = NULL
			--SET @skipCasePayment = 1
	END

	IF @platformId IN (14,79,80,81,82) --> PSA: Opel, Citroen, Peugeot, DS, Motorhome
	BEGIN

		set @AkronimZS = case	when @platformFromProgram = 14 then '102200'	--> Opel
								when @platformFromProgram = 79 then '104472'	--> Citroen
								when @platformFromProgram = 80 then '103641'	--> Peugeot
								when @platformFromProgram = 81 then '102200'	--> DS
								when @platformFromProgram = 82 then '102200' end--> Motorhomes


		set @serieZS = case when @platformFromProgram = 14 then 'GM'	--> Opel
							when @platformFromProgram = 79 then dbo.f_translate('CIT',default)	--> Citroen
							when @platformFromProgram = 80 then dbo.f_translate('PEU',default)	--> Peugeot
							when @platformFromProgram = 81 then 'DS'	--> DS
							when @platformFromProgram = 82 then 'MH' end--> Motorhomes

		--SET	@headerOplata = (	select TOP 1 zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
		--					where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	
		--					and XL_KntAkronim=@AkronimZS)

		--> Opłata za sprawę na osobnym ZSie
		SET @headerOplata = NULL
		SET @generateZS = 1
		
		IF @platformId <> 14
		BEGIN
			SET @XL_FlagaSagai = 3
		END

		set @casePaymentType = case
			when @caseSource IN (3,4) then 1 --> 3:Sparx + 4:Aplikacja mobilna(SPARX SMART RSA)
			when @statusBWB IN (0,-1) then 2 --> Sprawy na żądanie PSA, brak definicji
			when @isRepairByPhone = 1 then 3 --> Naprawa przez telefon
			when @isAdHocPSA = 1 then 4 --> Obsługa spraw bez uprawnień, AD hoc
			when @platformId IN (14,79,80) then 5 --> Opel, Peugeot, Citroen
			when @platformId IN (81,82) then 6 --> DS, Motorhome
		else null end
	END

	IF @programId = 451 --> Mercedes Wypadek
	BEGIN
		if not exists (select top 1 1 from [sync].[ZamElem] ze with(nolock) join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id where zn.rootId = @rootId and isnull(ze.entityDefId,0) not in (0,29)) 
		--> Jeśli nie istnieją inne usługi niż Auto Zastępcze
		begin
			set @casePaymentType = 1 --> Tylko Auto Zastępcze
		end
		--> Jeśli nie istnieją inne usługi niż holowanie o statusie: Pusty Wyjazd
		else if not exists (select top 1 1 from [sync].[ZamElem] ze with(nolock) join [sync].[ZamNag] zn with(nolock) on ze.AUD_IdDok=zn.Id where zn.rootId = @rootId and zn.XL_Opis not like '%Pusty%')
		begin
			set @casePaymentType = 2 --> Tylko Pusty Wyjazd
		end		
		else
		begin
			set @casePaymentType = 3 --> Pozostałe przypadki
		end
	END

	IF @platformFromProgram = 78 AND ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))=dbo.f_translate(dbo.f_translate('Polska',default),default) --> Carefleet
	BEGIN
		SET @skipCasePayment = 1 
	END

	IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))<>dbo.f_translate(dbo.f_translate('Polska',default),default) AND @platformId IN (2,35,78)--> FORD,TUZ,Carefleet Dla spraw zagranicznych wartość typu jest na minusie.
	BEGIN -- Zagranica
		SET @casePaymentType = @casePaymentType * -1
	END

	SET @currency = ISNULL(@currency,dbo.f_translate(dbo.f_translate('PLN',default),default))

	--if @rootId=03241774
	--begin
	--	SELECT @casePaymentType,@platformId,@programId
	--	SELECT TOP 1 * FROM [AtlasDB_def].[dbo].[casePayment] cp
	--	WHERE ((programId IS NOT NULL AND programId LIKE '%'+CAST(@programId AS nvarchar(10))+'%') OR @platformId IN 
	--	(select distinct data from f_split((select CONCAT(cpp.platformId,',') from AtlasDB_def.dbo.casePayment cpp WHERE cpp.id=cp.id FOR XML PATH('')),',')
	--	)) and cp.type = @casePaymentType
	--end

	PRINT '@casePaymentType: ' + ISNULL(CAST(@casePaymentType AS NVARCHAR(10)),dbo.f_translate('NULL',default))

	SELECT TOP 1 @valueZS=value,@currency=currency FROM [dbo].[casePayment] cp with(nolock)
	WHERE ((programId IS NOT NULL AND programId LIKE '%'+CAST(@programId AS nvarchar(10))+'%') OR @platformId IN 
	(select distinct data from f_split((select CONCAT(cpp.platformId,',') from dbo.casePayment cpp with(nolock) WHERE cpp.id=cp.id FOR XML PATH('')),',')
	)) and cp.type = @casePaymentType

	------------
	DECLARE @ZeValueZS DECIMAL(18,2) = ISNULL((SELECT sum(XL_Wartosc) FROM [sync].[ZamElem] ze with(nolock) LEFT JOIN [sync].[ZamNag] zn with(nolock) ON ze.AUD_IdDok=zn.Id 
				WHERE zn.rootId=@rootId AND ze.XL_TwrKod='-1' AND ze.programId IN (SELECT Id FROM vin_program with(nolock) WHERE platform_id=@platformId)),0.00)

	IF @ZeValueZS < @valueZS AND ISNULL(@skipCasePayment,0)=0
	BEGIN
		PRINT dbo.f_translate('Dodawanie opłaty za sprawe',default)
		IF @ZeValueZS < @valueZS AND @ZeValueZS<>0.00
		BEGIN
			PRINT dbo.f_translate('Dopisywanie pozostałej opłaty za sprawę',default)
			SET @valueZS = @valueZS - @ZeValueZS
			PRINT @platformId
			IF @platformId IN (8,13,16,20,21,22,50,64,68,73,74) --> TRUCK
			BEGIN
				SET @valueZS = 0.00
				IF EXISTS (SELECT TOP 1 zn.XL_ZamSeria FROM [sync].[ZamElem] ze with(nolock) LEFT JOIN [sync].[ZamNag] zn with(nolock) ON ze.AUD_IdDok=zn.Id WHERE  zn.rootId=@rootId and XL_TwrKod = '-1')
				BEGIN
					SET @skipCasePayment = 0
				END

				IF ISNULL(@country,dbo.f_translate(dbo.f_translate('Polska',default),default))<>dbo.f_translate(dbo.f_translate('Polska',default),default) --> Dla spraw zagranicznych wartość typu jest na minusie.
				BEGIN -- sprawy za granica powinny sie przenosić tylko z dowozem gotówki bez opłaty za sprawę
					SET @valueZS = NULL
					SET @skipCasePayment = 1
				END
			END

			SET @serieZS = (
				SELECT TOP 1 zn.XL_ZamSeria FROM [sync].[ZamElem] ze with(nolock) LEFT JOIN [sync].[ZamNag] zn with(nolock) ON ze.AUD_IdDok=zn.Id 
				WHERE zn.rootId=@rootId AND ze.XL_TwrKod='-1' AND ze.programId = @programId AND zn.XL_ZamSeria IS NOT NULL ORDER BY XL_ZamSeria DESC
			)

			SET @XL_FlagaNBZS = (
				SELECT TOP 1 zn.XL_FlagaNB FROM [sync].[ZamElem] ze with(nolock) LEFT JOIN [sync].[ZamNag] zn with(nolock) ON ze.AUD_IdDok=zn.Id 
				WHERE zn.rootId=@rootId AND ze.XL_TwrKod='-1' AND ze.programId = @programId AND zn.XL_FlagaNB IS NOT NULL ORDER BY XL_FlagaNB DESC
			)

			SET @generateZS = 1
		END
	END
	ELSE
	BEGIN
		PRINT dbo.f_translate('Pomiń opłatę za sprawę: ',default) + ISNULL(CAST(@valueZS AS NVARCHAR(20)),dbo.f_translate('NULL',default)) + dbo.f_translate('. Program: ',default) + @programName
		SET @skipCasePayment = 1
	END
	------------

	IF @skipCasePayment = 0
	BEGIN
		IF @generateZS = 1
		BEGIN
			SET @headerOplata = NULL --> Dla VGP opłata za sprawę generuje się na osobnym nagłówku.

			print dbo.f_translate('Dodaje nagłówek ZS dla opłaty za sprawę',default)
			INSERT INTO sync.ZamNag (
				ParentZsId,
				AUD_GidTyp, -- 960
				XL_DataRealizacji, -- data statusu progres 4
				XL_DokumentObcy, -- caseId
				XL_KntAkronim,
				XL_Opis, -- platforma + dbo.f_translate('atlas',default)
				XL_Typ, -- ZS/ZZ
				XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
				rootId,--SO_SprawaId, -- rootId
				type,
				partnerLocId,
				XL_DataWystawienia,
				XL_TargetKntAkronim,
				XL_FlagaNB,
				XL_FlagaSagai
			)
			SELECT 
			null,
			@gidTypeZS, 
			getdate(),--@finishedAt, 
			@caseId,  
			@AkronimZS, --Akronim
			@programName, -- Description (opis)
			'ZS',
			@serieZS,	
			@rootId,
			1, -- Type (Producent)
			null,--@partnerId, -- Czy to musi być?
			@caseDate,
			'', /*Docelowy*/
			isnull(@XL_FlagaNBZS,'N'),
			@XL_FlagaSagai
		END

		IF @platformId = 8 OR @programId = 522 --> DKV
		BEGIN
			PRINT dbo.f_translate('DKV',default)
			SET @valueZS = 0.00
		END

		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			programId
		)	
		SELECT  
		ISNULL(@headerOplata,scope_identity()),
		1,
		'-1',
		@valueZS,
		isnull(@currency,dbo.f_translate(dbo.f_translate('PLN',default),default)),
		null,
		null,
		@programId

		PRINT dbo.f_translate('Opłata za sprawę: ',default) + ISNULL(CAST(@valueZS AS NVARCHAR(20)),dbo.f_translate('NULL',default)) + dbo.f_translate('. Program: ',default) + @programName
	END

	FETCH NEXT FROM casePayment INTO @platformId,@programId,@wasNH
END
CLOSE casePayment;
DEALLOCATE casePayment;

-- ==================================================== --
-- Dodanie opłaty za sprawę Alphabet Wspracie Kierowcy. --
-- ==================================================== --
--IF @platformId = 58	AND NOT EXISTS(SELECT 1 FROM SYNC.ZamNag WITH(NOLOCK) WHERE rootId=@rootId) AND NOT EXISTS (SELECT root_id FROM process_instance WHERE step_id='1011.022' AND root_id=@rootId)
--BEGIN
--	PRINT dbo.f_translate('Dodaje opłatę za sprawę Alphabet Wspracie Kierowcy',default)
--	SET @valueZS = (SELECT TOP 1 value FROM AtlasDB_def.dbo.casePayment WITH(NOLOCK) WHERE type=3 AND platformId=58)
--	SET @currency = (SELECT TOP 1 currency FROM AtlasDB_def.dbo.casePayment WITH(NOLOCK) WHERE type=3 AND platformId=58)
--	SET @serieZS = dbo.f_translate('ALF',default)
--	SET @AkronimZS='105000'

--	INSERT INTO sync.ZamNag (
--					ParentZsId,
--					AUD_GidTyp, -- 960
--					XL_DataRealizacji, -- data statusu progres 4
--					XL_DokumentObcy, -- caseId
--					XL_KntAkronim,
--					XL_Opis, -- platforma + dbo.f_translate('atlas',default)
--					XL_Typ, -- ZS/ZZ
--					XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
--					rootId,--SO_SprawaId, -- rootId
--					type,
--					partnerLocId,
--					XL_DataWystawienia,
--					XL_TargetKntAkronim,
--					XL_FlagaNB
--				)
--				SELECT 
--				null,
--				1280, 
--				getdate(),--@finishedAt, 
--				@caseId,  
--				@AkronimZS,
--				dbo.f_translate('Alphabet wsparcie kierowcy',default), -- Description (opis)
--				'ZS',
--				@serieZS,	
--				@rootId,
--				1, -- Type (Producent)
--				null,--@partnerId, -- Czy to musi być?
--				@caseDate, --> Numer sprawy A0XXXXXX
--				'', /*Docelowy*/
--				'N' --> Netto

--	INSERT INTO sync.ZamElem(
--		AUD_IdDok, -- id nagłówka
--		XL_Ilosc, -- zmienna
--		XL_TwrKod, -- zmienna 
--		XL_Wartosc, -- zmienna 
--		XL_Waluta, -- zmienna 
--		entityValueId, -- entityId
--		entityDefId,
--		programId
--	)	
--	SELECT  
--	scope_identity(),
--	1,
--	'-1',
--	@valueZS,
--	@currency,
--	null,
--	null,
--	506 --> Alphabet wsparcie kierowcy

--	IF NOT EXISTS (SELECT TOP 1 value_text FROM attribute_value WITH(NOLOCK) WHERE attribute_path='406,226,227' AND root_process_instance_id=@rootId AND value_text=dbo.f_translate(dbo.f_translate('Przeniesiono do CDNa.',default),default))
--	BEGIN
--		DECLARE @err TINYINT
--		DECLARE @message NVARCHAR(255)
--		EXECUTE dbo.p_note_new -- Dodawanie notatki
--				@groupProcessId = @rootId
--				,@type = dbo.f_translate('text',default)
--				,@content = dbo.f_translate(dbo.f_translate('Przeniesiono do CDNa.',default),default)
--				,@userId = 1  -- automat
--				,@err = @err
--				,@message = @message
--	END
--END

--====================================--
--        Kasowanie zamówień          --
--====================================--
if @rootId<>04188999
begin
	-- 1. Kasuje Nag, które nie mają Elem
	delete from sync.ZamNag where ParentZsId in (
		select zn.id from sync.ZamNag zn with(nolock) 
			left join sync.ZamElem ze with(nolock) on ze.AUD_IdDok=zn.id
		where ze.id is null and zn.rootId=@rootId
	) 

	-- 2. Kasuje Elem z wartością NULL
	delete from sync.zamElem where XL_Wartosc is null and AUD_IdDok in(select zn.id from sync.ZamNag zn with(nolock) where zn.rootId=@rootId) 

	-- 3. Kasuje Nag ja w pkt 1. ??
	delete from sync.ZamNag where rootId=@rootId and id in(
		select zn.id from sync.ZamNag zn with(nolock) 
			left join sync.ZamElem ze with(nolock) on ze.AUD_IdDok=zn.id
		where ze.id is null and zn.rootId=@rootId
	) and AUD_WynikAPI is null

end
---- 5. Kasowanie ZZ jeśli nie posiada ZS
--DELETE FROM sync.ZamElem where Id IN (
--	select ze.Id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
--	where zn.rootId = @rootId and zn.XL_Typ = 'ZZ' and AUD_WynikAPI is null
--	and not exists(select * from sync.ZamNag zn2 
--		left join [sync].[ZamElem] ze2 on ze2.AUD_IdDok=zn2.Id
--		where zn2.XL_Typ = 'ZS' and zn2.rootId = @rootId and ze2.entityValueId = ze.entityValueId)
--) AND AUD_GidLp is null

COMMIT TRANSACTION

END
