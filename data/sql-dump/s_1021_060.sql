ALTER PROCEDURE [dbo].[s_1021_060]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err TINYINT
	DECLARE @message NVARCHAR(255)
	DECLARE @caseId NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	
	SELECT @groupProcessInstanceId = group_process_id FROM process_instance where id = @previousProcessId
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	declare @partnerEmail nvarchar(200)
	declare @vin varchar(50)
	declare @regNumber varchar(20)
	declare @makeModel nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = textD from dbo.dictionary where value =(select top 1 value_int FROM @values) and typeD='makeModel'


	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '129,800,368', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerEmail = value_string FROM @values
		
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '73,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VIN = value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
	
	declare @DC int
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '802', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @DC = value_int FROM @values
		
	declare @isOK int
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '129,808,801', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isOK = value_int FROM @values

	declare @rejectedAutomaticly int
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1070', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @rejectedAutomaticly = value_int FROM @values
	
	set @caseId=dbo.f_caseId(@groupProcessInstanceId)

	declare @subject nvarchar(300)
	declare @body nvarchar(4000)
	declare @gid varchar(50)
	declare @platformId int

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values

	select top 1 @gid=token 
	from	dbo.process_instance
	where	group_process_id=@groupProcessInstanceId and
			token is not null
	order by id desc
	
	declare @platform varchar(100)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platform = name from dbo.platform where id =(select top 1 value_int FROM @values)

	DECLARE @urlProcess VARCHAR(300) = [dbo].[f_hashTaskUrl](@gid)

	if @isOK=1
	begin
-- 		set @subject=dbo.f_translate('Formularz raportu serwisowego ',default)+isnull(@platform,'')+dbo.f_translate(' do sprawy ',default)+isnull(@caseId,'')+' ('+isnull(@makeModel,'')+' / '+isnull(@vin,'')+' / '+isnull(@regNumber,'')+') - Akceptacja'
		set @subject=isnull(@makeModel,'')+' / '+isnull(@regNumber,'') +' / '+isnull(@vin,'')+dbo.f_translate(' / Sprawa nr ',default)+isnull(@caseId,'') + dbo.f_translate(' / Formularz raportu serwisowego ',default)+isnull(@platform,'') +dbo.f_translate(' - Akceptacja',default)
			IF @platformId =2
				BEGIN
		set @body='Szanowni Państwo,<BR>
					Wniosek o zorganizowanie świadczeń dodatkowych dotyczący sprawy nr '+isnull(@caseId,'')+' został zaakceptowany.<BR>
					Aby uaktualnić dane raportu kliknij 
				<A href="'+@urlProcess+'">tutaj</A><BR><BR>' +
							dbo.f_translate('W załączeniu przesyłamy kopię wniosku z naszą akceptacją',default)
			END
		ELSE
		BEGIN
			set @body='Szanowni Państwo,<BR>
					Wniosek Dealer’s Call nr '+isnull(@caseId,'')+' został zaakceptowany.<BR>
					Aby uaktualnić dane raportu kliknij
				<A href="'+@urlProcess+'">tutaj</A>'
		end
	end
	else
	begin
		set @subject=isnull(@makeModel,'')+' / '+isnull(@regNumber,'') +' / '+isnull(@vin,'')+dbo.f_translate(' / Sprawa nr ',default)+isnull(@caseId,'') + dbo.f_translate(' / Formularz raportu serwisowego ',default)+isnull(@platform,'') +dbo.f_translate(' - Odrzucenie',default)
-- 		set @subject=dbo.f_translate('Formularz raportu serwisowego ',default)+isnull(@platform,'')+dbo.f_translate(' do sprawy ',default)+isnull(@caseId,'')+' ('+isnull(@makeModel,'')+' / '+isnull(@vin,'')+' / '+isnull(@regNumber,'')+') - Odrzucenie'
		IF @platformId =2
			BEGIN
		set @body='Szanowni Państwo,<BR>
					Wniosek o zorganizowanie świadczeń dodatkowych dotyczący sprawy nr '+isnull(@caseId,'')+' został odrzucony.  <BR>
					Aby uaktualnić dane raportu kliknij <A href="'+@urlProcess+'">tutaj</A>. <BR><BR>
					W załączeniu przesyłamy kopię wniosku z naszą odmową i uzasadnieniem. '
		END
		ELSE
		BEGIN

			set @body='Szanowni Państwo,<BR>
					Wniosek Dealer’s Call nr '+isnull(@caseId,'')+' został odrzucony.  <BR>'
				if isnull(@rejectedAutomaticly,0 ) <> 1
					BEGIN
						SET @body = @body + '	Aby uaktualnić dane raportu kliknij <A href="'+@urlProcess+'">tutaj</A>. <BR>'
					end
						SET @body = @body + '<BR> W załączeniu przesyłamy kopię wniosku z naszą odmową i uzasadnieniem. '
		end
	end

  
	DECLARE @idRS INT
	
	SELECT top 1 @idRS = id 
	FROM	process_instance 
	where	group_process_id = @groupProcessInstanceId and 
			step_id='1021.003'
	order by id desc

	declare @RSV varchar(200)
	set @RSV='{FORM::'+cast(@idRS as varchar(100))+'::raport serwisowy}'

	set @variant=2

	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rs')
	
		EXECUTE dbo.p_note_new 
		   @sender = @senderEmail 
		  ,@groupProcessId=@groupProcessInstanceId
		  ,@type=dbo.f_translate('email',default)
		  ,@content=@body
		  ,@phoneNumber=null
		  ,@email=@partnerEmail
		  ,@userId=@currentUser
		  ,@subType=null
		  ,@attachment=null
		  ,@subject=@subject
		  ,@direction=1
		  ,@addInfo=null
		  ,@emailBody=@body
		  ,@emailRegards=1
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT
		  ,@additionalAttachments=@RSV


	IF isnull(@rejectedAutomaticly, 0 ) = 1
		BEGIN
			SET @variant = 0
		end
end