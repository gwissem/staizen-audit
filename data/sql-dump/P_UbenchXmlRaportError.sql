ALTER PROCEDURE [dbo].[P_UbenchXmlRaportError]
AS
BEGIN
	
	/*	Procedura sprwadza co jakis czas, czy wystąpił błąd w wysłaniu XML'a do Ubencha 
 	____________________________________*/
	
	DECLARE @lastId INT
	DECLARE @xmlTable TABLE (id INT, sentDate DATETIME, request XML, response XML)
	DECLARE @amount INT 
	
	SELECT @lastId = value FROM dbo.config WHERE [key] = 'ubench_xml_last_id'
	
	INSERT INTO @xmlTable
	SELECT
	Id as id,
	DataWyslania as sentDate,
	REPLACE(Request, 'encoding="UTF-8"', 'encoding="utf-16"') as request,
	REPLACE(Response, 'encoding="UTF-8"', 'encoding="utf-16"') as response
	FROM Sette.StarterOperations_Stage.dbo.UBenchXmlOutgoing WITH(NOLOCK)
	WHERE Response IS NOT NULl
	AND Id > @lastId
	ORDER BY Id ASC
	
	SELECT @amount = count(id) FROM @xmlTable
	
	PRINT '--- @Nowych spraw: ' + CAST(ISNULL(@amount, '') AS NVARCHAR(200))
	
	IF @amount > 0
	BEGIN
		
		/*	Kursor bo błędnych XMLach
  		____________________________________*/
		
		DECLARE @err INT
        DECLARE @message VARCHAR(400)
        DECLARE @groupProcessInstanceId INT = 38763
        DECLARE @body VARCHAR(MAX)
        DECLARE @email VARCHAR(400) = [dbo].[f_getRealEmailOrTest]('szkody@alphabet.pl')
        DECLARE @subject VARCHAR(400) 

		DECLARE @caseNumber NVARCHAR(100)
		DECLARE @rootId INT
		DECLARE @responseCode INT 
		DECLARE @responseDescription VARCHAR(500) 
		DECLARE @plateNumber NVARCHAR(100)
		
		DECLARE kur cursor LOCAL for
	  		SELECT * FROM (
	  			SELECT
		--		sentDate as sentDate,
				request.value('(/b2b/filenumber)[1]', 'varchar(100)') as caseNumber,
				request.value('(/b2b/car/plate)[1]', 'varchar(100)') as plateNumber,
				response.value('(/ubreply/value)[1]', 'int') as responseCode,
				response.value('(/ubreply/description)[1]', 'varchar(500)') as responseDescription
				FROM @xmlTable
	  		) as a WHERE responseCode <> 1
  		OPEN kur;
  		FETCH NEXT FROM kur INTO @caseNumber, @plateNumber, @responseCode, @responseDescription
  		WHILE @@FETCH_STATUS=0
  				BEGIN
  
--	  				SET @caseNumber = 'A00816815'
	  				
	  			 	SET @rootId = CAST(REPLACE(@caseNumber, 'A', '') as INT)
	  			 	SET @subject = @caseNumber + ' / ' + @plateNumber + dbo.f_translate(' - błąd w wysyłce XML',default)
	  			 	SET @body = @subject + dbo.f_translate('. Kod błędu: ',default) + CAST(ISNULL(@responseCode, '') AS NVARCHAR(200)) + dbo.f_translate(', opis błędu: ',default) + CAST(ISNULL(@responseDescription, '') AS NVARCHAR(200))
	  			 	
	  			 	/*	Wysłania e-maila do szkody@alphabet.pl
        			____________________________________*/
	  			 	
	  			 	BEGIN TRY  
        
		  			 	IF ISNULL(@rootId, 0) <> 0
		  			 	BEGIN
			  			 	
			  			 	EXECUTE dbo.p_note_new 
					       	 @groupProcessId = @rootId
					       	,@type = dbo.f_translate('email',default)
					       	,@content = dbo.f_translate('Napotkano błąd przy wysłaniu XMLa do systemu Ubench.',default)
					       	,@email = @email
					       	,@userId = 1  -- automat
					       	,@subject = @subject
					       	,@direction=1
					       	,@dw = ''
					       	,@udw = 'dylesiu@gmail.com'  -- na razie dla testów też do mnie wysyła.
					       	,@sender = 'atlas-noreply@starter24.pl'
					       	,@emailBody = @body
					       	,@emailRegards = 1
					       	,@err=@err OUTPUT
					       	,@message=@message OUTPUT
	       	
		  					PRINT '--- Błąd w @caseNumber: ' + CAST(ISNULL(@caseNumber, '') AS NVARCHAR(200))
			  			 	
		  			 	END
		        
			        END TRY  
			        BEGIN CATCH  
			        
			        	IF SYSTEM_USER = dbo.f_translate('michal.dylewski',default)
			        	BEGIN
				        	 SELECT  
				                  ERROR_NUMBER() AS ErrorNumber  
				                  ,ERROR_SEVERITY() AS ErrorSeverity  
				                  ,ERROR_STATE() AS ErrorState  
				                  ,ERROR_PROCEDURE() AS ErrorProcedure  
				                  ,ERROR_LINE() AS ErrorLine  
				                  ,ERROR_MESSAGE() AS ErrorMessage;  
                 
			        	END
			        	
			            PRINT '--- CATCH --- '
			                			
			        END CATCH;
    
	  				
  					FETCH NEXT FROM kur INTO @caseNumber, @plateNumber, @responseCode, @responseDescription
  				END
  		CLOSE kur
  		DEALLOCATE kur
		
		
		/*	Aktualizacja ostatniego ID
  		____________________________________*/
		
		SELECT TOP 1 @lastId = id FROM @xmlTable ORDER BY id DESC
		
		UPDATE dbo.config SET value = @lastId, updated_at = GETDATE() WHERE [key] = 'ubench_xml_last_id'
		
	END
	
END