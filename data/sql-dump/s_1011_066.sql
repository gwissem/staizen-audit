ALTER PROCEDURE [dbo].[s_1011_066]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, 
    @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	declare @tips nvarchar(max)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '994', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @tips = value_text FROM @values
	
	IF isnull(@tips,'') <> ''
	BEGIN
		EXEC [dbo].[p_note_new]
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('text',default),
		@content = @tips,
		@userId = @currentUser,
		@originalUserId = @currentUser,
		@special = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	END 
	
END


