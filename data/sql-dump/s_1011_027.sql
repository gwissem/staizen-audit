ALTER PROCEDURE [dbo].[s_1011_027]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessId INT
	DECLARE @content NVARCHAR(MAX)
	DECLARE @driverPhoneNumber NVARCHAR(255)
	DECLARE @requestorPhoneNumber NVARCHAR(255)
	DECLARE @attrTable Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @platformId INT
	DECLARE @platformPhone NVARCHAR(100)
	DECLARE @platformName NVARCHAR(100)
	
	SELECT @groupProcessId = group_process_id FROM process_instance with(nolock) where id = @previousProcessId
	
	INSERT  @attrTable EXEC dbo.p_attribute_get2
			@attributePath = '80,342,408,197',
			@groupProcessInstanceId = @groupProcessId
	SELECT @driverPhoneNumber = value_string FROM @attrTable
	
	DELETE FROM @attrTable
	INSERT  @attrTable EXEC dbo.p_attribute_get2
			@attributePath = '418,342,408,197',
			@groupProcessInstanceId = @groupProcessId
	SELECT @requestorPhoneNumber = value_string FROM @attrTable
	
	DELETE FROM @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '253',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @platformId = value_int FROM @attrTable
	
	SELECT @platformPhone = number from platform_phone with(nolock) where platform_id = @platformId
	SELECT @platformName = name FROM platform with(nolock) where id = @platformId
	
	SET @content = dbo.f_translate('Pod podanym numerem telefonu ',default)+ @driverPhoneNumber +dbo.f_translate(' nie udalo nam sie nawiazac kontaktu z kierowca. Prosimy o bezposredni kontakt kierowcy z nasza infolinia.',default)
	
	EXEC p_note_new
	@groupProcessId = @groupProcessId,
	@type = dbo.f_translate('sms',default),
	@content = @content,
	@phoneNumber = @requestorPhoneNumber,
	@userId = 1,
	@originalUserId = 1,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	set @variant=1
END

