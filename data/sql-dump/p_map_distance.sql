ALTER PROCEDURE [dbo].[p_map_distance]
@longitude1 DECIMAL(12,8),
@latitude1 DECIMAL(12,8),
@longitude2 DECIMAL(12,8),
@latitude2 DECIMAL(12,8),
@roundResult INT = 1,
@distance DECIMAL(15,5) OUTPUT
as
begin

	DECLARE @URL VARCHAR(8000) 
	SET @URL = dbo.f_getDomain() + '/get-routes-points?data=0:'+cast(@longitude1 as varchar(20))+':'+cast(@latitude1 as varchar(20))+'|1:'+cast(@longitude2 as varchar(20))+':'+cast(@latitude2 as varchar(20))

	PRINT '@URL'
	PRINT @URL
	
	DECLARE @Response varchar(8000)
	DECLARE @Obj int 
	DECLARE @HTTPStatus int 
	DECLARE @Text as table ( answer varchar(4000) )
 
	EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
	EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
	EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
	EXEC sp_OAMethod @Obj, send, NULL, ''
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
	
	INSERT @Text
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseText',default)
	EXEC sp_OADestroy @Obj
	
	SELECT @distance = CAST(REPLACE(answer,'1:','') AS DECIMAL(15,5)) FROM @Text
	
	IF @roundResult =1 
	BEGIN 
		SELECT @distance = ROUND(@distance,0)
	END 
end 

