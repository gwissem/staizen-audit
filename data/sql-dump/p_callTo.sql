
ALTER PROCEDURE [dbo].[p_callTo]
@phoneNumber varchar(20),
@recordNumber int=1,
@err int output,
@description nvarchar(300) output
AS
begin
	SET NOCOUNT ON
	
	DECLARE @URL VARCHAR(8000) 

	if left(@phoneNumber,1)<>'0'
	begin
		set @phoneNumber='0'+right(@phoneNumber,9)
	end 
	if @recordNumber is null set @recordNumber=1

	SET @URL = 'http://10.10.77.101:9080/httpoutbound?TelephoneNumber='+@phoneNumber+'&PromptNumber='+cast(@recordNumber as varchar(20))

	DECLARE @Response varchar(8000)
	DECLARE @Obj int 
	DECLARE @HTTPStatus int 
	DECLARE @xmlT as table ( answerXML XML )
 

	EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
	EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
	EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
	EXEC sp_OAMethod @Obj, send, NULL, ''
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
	
	declare @responseText nvarchar(1000)

	EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseText',default), @responseText OUT;
	EXEC sp_OADestroy @Obj

	set @description=@responseText
	if   @responseText like 'Successful%'
	begin
		set @err=0
	end
	else if @responseText like '%Unsuccessful%'
	begin
		set @err=20
	end
	else begin
		set @err=10
	end

	--Results:
	-- Successful - Odsluchana pierwsza zapowiedz
	-- Busy
	-- No Answer
	-- Successful - Odsluchany wszystkie zapowiedzi

END
