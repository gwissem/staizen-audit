ALTER PROCEDURE [dbo].[p_kia_bwb_veryfcation_email]
  (
    @previousProcessId INT,
    @variant TINYINT OUTPUT,
    @currentUser int = 1,
    @errId int=0 output
  )
AS
  BEGIN

    DECLARE @err INT
    DECLARE @message VARCHAR(400)
    DECLARE @groupProcessInstanceId INT
    DECLARE @rootId INT
    DECLARE @stepId VARCHAR(32)
    DECLARE @body NVARCHAR(MAX)
    DECLARE @mailSubject NVARCHAR(255)

    DECLARE @regNumber NVARCHAR(30)
    DECLARE @vinNumber NVARCHAR(30)
    DECLARE @makeModel NVARCHAR(100)
    DECLARE @partnerName NVARCHAR(255)
    DECLARE @firstRegDate DATETIME
    DECLARE @partnerLocationId INT
    DECLARE @assistanceFrom DATETIME
    DECLARE @mileageLimit INT
    DECLARE @policeNumber NVARCHAR(50)
    DECLARE @mailTo NVARCHAR(100) = 'aleksander.kowalewski@kia.com.pl'
    DECLARE @firstName nvarchar(100)
    DECLARE @lastName nvarchar(100)
    DECLARE @clientPhone nvarchar(50)


    SELECT
           @groupProcessInstanceId = group_process_id,
           @stepId = step_id,
           @rootId = root_id
    FROM process_instance  WITH(NOLOCK)
    WHERE id = @previousProcessId

    EXEC p_form_controls @instance_id = @previousProcessId, @returnResults = 0

    DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @regNumber = value_string FROM @values
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @vinNumber = value_string FROM @values
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT TOP 1 @makeModel = isnull(d.textD,'') FROM @values v
                                                        join dbo.dictionary d WITH(NOLOCK) ON d.value = v.value_int and d.typeD = 'MakeModel' and d.active = 1
    DELETE FROM @values


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,233', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @firstRegDate = value_date FROM @values
    DELETE FROM @values

    declare @firstRegDate_string nvarchar(20)

    SET @firstRegDate_string = try_convert(varchar(10),@firstRegDate,120)  ---

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerLocationId = value_int FROM @values
    DELETE FROM @values

    Select top 1 @partnerName = isnull(avPartnerLocationName.value_string,'') from dbo.attribute_value avPartner with(nolock)
                                                                                     join dbo.attribute_value avPartnerLocationName with(nolock) on avPartnerLocationName.parent_attribute_value_id = avPartner.id and avPartnerLocationName.attribute_path ='595,597,84' and avPartnerLocationName.value_string is not null
    where avPartner.attribute_path = '595,597'
      and avPartner.id = @partnerLocationId

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '981,68', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @assistanceFrom = value_date FROM @values
    DELETE FROM @values


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,64', @groupProcessInstanceId = @rootId
    SELECT @firstName = value_string FROM @values
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,66', @groupProcessInstanceId = @rootId
    SELECT @lastName = value_string FROM @values
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,408,197', @groupProcessInstanceId = @rootId
    SELECT @clientPhone = value_string FROM @values
    DELETE FROM @values

    declare @clientContact nvarchar(255)
    set @clientContact= isnull(@firstName,'') + ' ' +  isnull(@lastName,'') + dbo.f_translate(' tel: ',default) + isnull(@clientPhone,'')

    declare @assistanceFrom_string nvarchar(20)

    set @assistanceFrom_string = try_convert(varchar(10),@assistanceFrom,120)

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '934', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @mileageLimit = value_int FROM @values
    DELETE FROM @values

    declare @mileageLimit_string nvarchar(20)
    set @mileageLimit_string = TRY_CAST(@mileageLimit as nvarchar(20))

    declare @dateString nvarchar(20)
    set @dateString = try_convert(nvarchar(16),getdate(),120)

    SET @mailSubject = dbo.f_translate('Prośba o potwierdzenie uprawnień KIA Assistance, ',default) + isnull(@makeModel,'') + dbo.f_translate(', VIN: ',default) + isnull(@vinNumber,'') +dbo.f_translate(', numer rejestrcyjny: ',default) + isnull(@regNumber,'') + dbo.f_translate('. Sprawa numer: ',default) + isnull([dbo].[f_caseId](@rootId),'')


    SET @body = dbo.f_translate('Szanowni Państwo,',default)
                +'</br></br>'
                +dbo.f_translate('Nie udało nam się potwierdzić uprawnień do assistance dla pojazdu, który zgodnie z twierdzeniem Klienta, powinien być objęty polskim programem Kia Assistance, zarówno w przekazanej nam bazie uprawnionych pojazdów, jak i w programie Autoteam',default)
                +dbo.f_translate('Marka i model: ',default) + isnull(@makeModel,'')
                +'</br>Numer rejestracyjny: ' + isnull(@regNumber,'')
                +'</br>VIN: ' + isnull(@vinNumber,'')
                +'</br>Data pierwszej rejestracji: ' + isnull(@firstRegDate_string,'')
                +'</br>Dealer, w którym zakupiono pojazd wg deklaracji Klienta: ' + isnull(@partnerName,'')
                +'</br>Klient: ' + @clientContact
                +'</br></br></br>'
                +dbo.f_translate('Z poważaniem,',default)
                +'</br>Zespół Kia Assistance (Starter24)'
                +'</br>tel.: +48 61 83 19 929'
                +'</br>ul. Józefa Kraszewskiego 30, 60-519 Poznań'
                +'</br>Treść tego dokumentu jest poufna i prawnie chroniona. Odbiorcą może być jedynie jej adresat z wyłączeniem dostępu osób trzecich. Jeżeli nie jest Pani/Pan adresatem niniejszej wiadomości, jej rozpowszechnianie, kopiowanie lub inne działanie o podobnym charakterze jest prawnie zabronione. W razie otrzymania tej wiadomości jako niezamierzony odbiorca, proszę o poinformowanie o tym fakcie nadawcy, a następnie usunięcie wiadomości ze swojego systemu.
							  STARTER Sp. z o.o., ul. Józefa Kraszewskiego 30, 60-519 Poznań, wpisana przez Sąd Rejonowy w Poznaniu, VIII Wydział Gospodarczy Krajowego Rejestru Sądowego, KRS: 0000056095; kapitał zakładowy: 12 500 000,00 zł; NIP: 525-21-83-310; REGON: 016387736
							  Niniejsza wiadomość oraz wszystkie załączone do niej pliki przeznaczone są do wyłącznego użytku zamierzonego adresata i mogą zawierać chronione lub poufne informacje. Przeglądanie, wykorzystywanie, ujawnianie lub dystrybuowanie przez osoby do tego nieupoważnione jest zabronione. Jeśli nie jest Pan/Pani wymienionym adresatem niniejszej wiadomości, prosimy o niezwłoczny kontakt z nadawcą i usunięcie oryginalnej wiadomości oraz zniszczenie wszystkich jej kopii.
							  The information in this email is confidential and may be legally privileged. It is intended solely for the addressee. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited and may be unlawful. If you received this email as the unintended recipient, please inform the sender and delete this message.'
                +'</br></br>'

    DECLARE @sendMail nvarchar(100)
    SET @sendMail = dbo.f_getRealEmailOrTest ('callcenter@starter24.pl')

    EXECUTE dbo.p_note_new
        @groupProcessId = @groupProcessInstanceId
        ,@type = dbo.f_translate('email',default)
        ,@content = dbo.f_translate('KIA',default)
        ,@email = @mailTo  --'aleksander.kowalewski@kia.com.pl'
        ,@userId = 1  -- automat
        ,@subject = @mailSubject
        ,@direction=1
        ,@dw = ''
        ,@udw = ''
        ,@sender = @sendMail
        ,@emailBody = @body
        -- ,@emailRegards = 2
        ,@err=@err OUTPUT
        ,@message=@message OUTPUT

  END