ALTER PROCEDURE [dbo].[p_process_next]
@previousProcessId INT,
@userId INT = NULL,
@originalUserId INT = NULL,
@skipTransaction TINYINT = 0,
@skipStepProcedure TINYINT = 0,
@skipErrors TINYINT = 0,
@variant SMALLINT = 1,
@err INT OUTPUT,
@message NVARCHAR(255) OUTPUT,
@processInstanceIds varchar(4000) = null OUTPUT,
@token varchar(50) = null output
AS
BEGIN
	SET NOCOUNT ON
	
	set @skipTransaction=1
	DECLARE @companyId INT
	DECLARE @stepId NVARCHAR(20)
	DECLARE @currentStepId NVARCHAR(20)
	DECLARE @parentId INT
	DECLARE @nextProcessId INT
	DECLARE @processInstanceId int
	DECLARE @groupProcessInstanceId INT
	DECLARE @programId NVARCHAR(255)
	DECLARE @flowVariant INT
	DECLARE @flowIsTechnical INT
	DECLARE @flowStepId NVARCHAR(255)
	DECLARE @recMsg NVARCHAR(255)
	DECLARE @recProcInsIds varchar(4000)
	DECLARE @recErr INT
	DECLARE @rootId INT
	DECLARE @workingUserId INT
	DECLARE @postponeDate DATETIME
	DECLARE @currentDate DATETIME
	DECLARE @apiCall INT
	DECLARE @overrideVariant INT
	DECLARE @caseSource INT
	
	declare @startTime datetime=getdate()
	declare @ts int
	set @ts=datediff(ms,getdate(),@startTime)
	print '01 - '+cast(@ts as varchar(20))
	set @startTime=getdate()

	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DECLARE @procedureExists TINYINT
	
	
	SET @err = 0
	SET @message = ''

	SELECT @postponeDate = postpone_date, @workingUserId = user_id, @stepId = step_id, @parentId = parent_id, @rootId = root_id, @groupProcessInstanceId = group_process_id FROM dbo.process_instance with(nolock) WHERE active = 1 AND id = @previousProcessId
	SET @currentStepId = @stepId
	
	CREATE TABLE #availableServices (id INT, name NVARCHAR(255), active TINYINT, message NVARCHAR(MAX), description NVARCHAR(MAX), icon NVARCHAR(MAX), variant INT, start_step_id NVARCHAR(255), pos INT, programId INT)
	
	-- PRZERWANIE EW. PĘTLI
	
	DECLARE @isLoop INT = 0
	SELECT @isLoop = dbo.f_is_loop(@groupProcessInstanceId, @stepId)
	IF @isLoop = 1
	BEGIN
		
		DECLARE @content NVARCHAR(4000)
		SET @content = dbo.f_translate('Przerwalem petle przejsc w sprawie ',default)+dbo.f_caseid(@previousProcessId)+dbo.f_translate(': groupProcessInstanceId: ',default)+ CAST(@groupProcessInstanceId AS NVARCHAR(255))+ dbo.f_translate(', stepId: ',default)+ @stepId + ' !'
		
  		EXEC dbo.p_note_new
  		@groupProcessId = @groupProcessInstanceId,
  		@type = dbo.f_translate('sms',default),
  		@content = @content,
  		@phoneNumber = '502317472',
  		@err = @err OUTPUT,
  		@message = @message OUTPUT
  		
  		EXEC dbo.p_note_new
  		@groupProcessId = @groupProcessInstanceId,
  		@type = dbo.f_translate('sms',default),
  		@content = @content,
  		@phoneNumber = '600222882',
  		@err = @err OUTPUT,
  		@message = @message OUTPUT
  		
  		UPDATE dbo.process_instance SET active = -1 where id = @previousProcessId
		SET @processInstanceIds = '999'
		RETURN
	END 
	
	SET @currentDate = GETDATE()
	IF ISNULL(@workingUserId,0) = 1 AND ISNULL(@postponeDate,@currentDate) > @currentDate
	BEGIN
		RETURN 
	END 
	
	BEGIN TRY			
		if @skipTransaction=0 BEGIN TRANSACTION
		IF(@stepId IS NOT NULL)
		BEGIN
			DECLARE @stepFunctionName NVARCHAR(20) 
			SET @stepFunctionName = 's_'+REPLACE(@stepId, '.', '_')
			DECLARE @stepAfterFunctionName NVARCHAR(30) 
			SET @stepAfterFunctionName = 's_'+REPLACE(@stepId, '.', '_')+'_a'
	
			DECLARE @newErr INT
			DECLARE @newMessage NVARCHAR(255)
			DECLARE @nextStepId NVARCHAR(20) = @stepId
			DECLARE @transactionName NVARCHAR(32) = dbo.f_translate('NewProcessActivation',default)
			DECLARE @accessDenied INT
			DECLARE @extraCostsMessage NVARCHAR(4000)
			DECLARE @acceptedExtraCosts INT

			SELECT @procedureExists = dbo.fn_if_procedure_exists(@stepFunctionName)

			DECLARE @errId int
			set @errId=0
			set @accessDenied = 0

			DECLARE @forceVariant TINYINT
			SET @forceVariant = @variant

			set @ts=datediff(ms,getdate(),@startTime)
			print '02 - '+cast(@ts as varchar(20))
			set @startTime=getdate()

			
			IF(@procedureExists <> 0 AND @skipStepProcedure = 0)
			BEGIN
				PRINT '------------------ EXEC AFTER PROCEDURE ' + CAST(@stepFunctionName AS VARCHAR) + ' --------------------'
				EXEC @stepFunctionName @previousProcessId = @previousProcessId, @currentUser = @userId, @variant = @variant OUTPUT, @errId = @errId output
				PRINT dbo.f_translate('Step variant after AFTER PROCEDURE:',default)
				PRINT @variant
				PRINT '--------------------------- END ' + CAST(@stepFunctionName AS VARCHAR) + ' ----------------------------'
			END
		
			-- API CALL 
			DECLARE @apiCallType INT
			DECLARE @doApiCall INT = 1
			DECLARE @platformId INT 
			DECLARE @useSparx INT 
			
			SELECT @apiCallType = type_id FROM dbo.api_call WITH(NOLOCK) WHERE step_id = @stepId AND isnull(variant,@variant) = @variant
			
			IF @apiCallType IS NOT NULL
			BEGIN				
				
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
				SELECT @platformId = value_int FROM @values
				
				SELECT @useSparx = IIF(@platformId IS NULL, 1, [dbo].[f_get_platform_key]('use_sparx', @platformId, default))
				
				IF @apiCallType IN (1,2) AND ISNULL(@useSparx,0) = 0
				BEGIN
					SET @doApiCall = 0
				END 
				ELSE IF @apiCallType = 1
				BEGIN
					
					DECLARE @sparxId NVARCHAR(255)
					
					DELETE FROM @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '592', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @sparxId = value_string FROM @values
					
					IF @sparxId IS NULL 
					BEGIN
						SET @doApiCall = 0
					END 
				END 
				
				IF @doApiCall = 1
				BEGIN
					INSERT INTO AtlasDB_v.dbo.jms_jobs
					(state, queue, priority, createdAt, executeAfter, command, args, maxRuntime, maxRetries, stackTrace, discr)
					VALUES('pending', 'default', 0, GETDATE(), GETDATE(), dbo.f_translate('api:call',default), '{"instanceId":'+CAST(@previousProcessId AS NVARCHAR(20))+'}', 0, 0, 'N;', 'job');	
				END 				
			END 
			
			set @ts=datediff(ms,getdate(),@startTime)
			print '02a - '+cast(@ts as varchar(20))
			set @startTime=getdate()


			-- START Procedura do aktualizacji statusów w usłudze 
			EXEC dbo.p_update_status_service @processInstanceId = @previousProcessId, @variant = @variant
			-- END --

			set @ts=datediff(ms,getdate(),@startTime)
			print '03 - '+cast(@ts as varchar(20))
			set @startTime=getdate()


			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '533', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @acceptedExtraCosts = value_int FROM @values
	
			--IF @variant NOT IN (98,97)
		
			DECLARE @cancel INT
			DECLARE @tempVariant INT
			SELECT @cancel = dbo.f_isStepVariantInProcess(@rootId, '1011.022', DEFAULT)
			
			IF ISNULL(@acceptedExtraCosts, 1) <> 0 AND @cancel = 0
			BEGIN
				
				if (left(@stepId,4) = '1011' OR @stepId IN ('1147.006')) AND @stepId NOT IN ('1011.51','1011.072', '1011.053', '1011.001', '1011.006', '1011.002', '1011.047', '1011.007', '1011.016', '1011.022', '1011.012', '1011.068', '1011.056', '1011.023')
				AND NOT (@stepId = '1011.96' and @variant = 99) 
				begin
					EXEC p_check_available_programs @processInstanceId = @previousProcessId, @overrideVariant = @overrideVariant OUTPUT 
					
					IF @overrideVariant IS NOT NULL
					BEGIN						
						SET @tempVariant = @overrideVariant
					END 
					ELSE
					BEGIN
						EXEC p_check_available_services @processInstanceId = @previousProcessId, @init = 0						
					END 					
				end

				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @programId = value_string FROM @values

				IF (NOT EXISTS(SELECT id FROM #availableServices WHERE active = 1 and id != 13) AND EXISTS (SELECT id FROM #availableServices) ) OR @tempVariant IS NOT NULL
				BEGIN					
					SELECT TOP 1 @extraCostsMessage = message, @tempVariant = variant FROM #availableServices WHERE variant is not null	order by variant desc
				
					PRINT '-----programId in NEXT'
					PRINT @programId
					
					IF @tempVariant = 96 AND ISNULL(@programId,'') not like '%423%' OR @tempVariant IN (91,93,95,97)
					BEGIN
						SET @variant = @tempVariant
					END
				END
				
			END
		
			set @ts=datediff(ms,getdate(),@startTime)
			print '04 - '+cast(@ts as varchar(20))
			set @startTime=getdate()


			if @errId<>0 AND @skipErrors = 0
			begin
			
			
				SELECT @message = message, @variant = variant, @err = error_id FROM dbo.error_message WITH(NOLOCK) WHERE ISNULL(step_id,@stepId) = @stepId AND error_id = @errId
				if @skipTransaction=0 COMMIT TRANSACTION
				RETURN
	--			set @variant=99
	--
	--			select @message=dbo.f_translate('Wystąpił krytyczny i niepodziewany błąd danych... Strzeż się...',default)
			end

			IF @variant = 0
			BEGIN

				IF NOT EXISTS(select id from dbo.process_instance_flow where previous_process_instance_id = @previousProcessId and active = 1)
				BEGIN
					
					SET @processInstanceIds='-999'					
					SET @message = dbo.f_translate('Proces został zakończony.',default)
					
				END
				ELSE
				BEGIN
					SELECT @processInstanceIds = dbo.concatenate(next_process_instance_id) FROM dbo.process_instance_flow where previous_process_instance_id = @previousProcessId and active = 1 
				END
				
				UPDATE dbo.process_instance SET active = 0, choice_variant= @variant, updated_at = getdate(), date_leave = getdate() WHERE id = @previousProcessId
				
				if @skipTransaction=0 COMMIT TRANSACTION
				RETURN
			END 
			ELSE if @variant=99 -- odświeżanie
			begin
				
				SELECT @processInstanceIds = dbo.Concatenate(distinct next_process_instance_id), @flowVariant = dbo.Concatenate(distinct variant)
				FROM dbo.process_instance_flow with(nolock)
				WHERE previous_process_instance_id = @previousProcessId and active = 1
	
				UPDATE dbo.process_instance_flow SET active = 0 WHERE previous_process_instance_id = @previousProcessId and active = 1		
				
				IF @processInstanceIds IS NULL
				BEGIN
					SET @processInstanceIds=@previousProcessId				
				END
				
	--			if @stepId='1134.010'
	--			begin
				SELECT @procedureExists = dbo.fn_if_procedure_exists(@stepAfterFunctionName)
				IF(@procedureExists <> 0 AND @skipStepProcedure = 0)
				BEGIN
					EXEC @stepAfterFunctionName @nextProcessId = @previousProcessId, @currentUser = @userId, @token = @token output
				END
	--			end
				if @skipTransaction=0 COMMIT TRANSACTION
				return
			end
			else if @variant=98 -- anulowanie usługi
			begin
				DECLARE @processDefinitionId INT
				DECLARE @cancelStep NVARCHAR(20)

--				SELECT top 1 @processDefinitionId = data from dbo.f_split(@stepId,'.') order by id
				SELECT @cancelStep = cancel_step_id FROM process_definition WITH(NOLOCK) where id = 1011
				IF @cancelStep IS NULL
				BEGIN
	--				SET @cancelStep = CAST(@processDefinitionId AS NVARCHAR(10))+'.999'
					SET @processInstanceIds='-999'
				END
				ELSE
				BEGIN			
					EXEC dbo.p_process_new @stepId = @cancelStep, @userId = @userId, @originalUserId = @originalUserId, @previousProcessId = @previousProcessId, @err = @err, @message = @message, @parentProcessId = @parentId, @processInstanceId = @processInstanceId OUT
					SET @processInstanceIds=@processInstanceId
				END

				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1
				
				insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)  
				select 'user_action', 'Anulowanie sprawy', @rootId, 'delete_case', @userId, GETDATE(), GETDATE()
				
				EXEC [dbo].[p_attribute_edit]
			      @attributePath = '734', 
			      @groupProcessInstanceId = @rootId,
			      @stepId = 'xxx',
			      @userId = 1,
			      @originalUserId = 1,
			      @valueInt = -1,
			      @err = @err OUTPUT,
			      @message = @message OUTPUT
				
				if @skipTransaction=0 COMMIT TRANSACTION
				return
			end
			else if @variant = 97 -- stop świadczeń 
			begin
				IF @stepId = '1011.022'
				BEGIN
					SET @processInstanceIds='-999'
				END 
				ELSE
				BEGIN
				
					DECLARE @prevToken NVARCHAR(40)
					
					EXEC dbo.p_process_new @stepId = '1011.022', @userId = @userId, @originalUserId = @originalUserId, @previousProcessId = @previousProcessId, @err = @err, @message = @message, @parentProcessId = @parentId, @processInstanceId = @processInstanceId OUT
					SET @processInstanceIds=@processInstanceId
	
					DELETE FROM @values
				    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '102', @groupProcessInstanceId = @rootId
				    SELECT @caseSource = value_int FROM @values
					
				    /*	Jeżeli jest to mobilne przyjęcie sprawy, to kopiujemy token. Klient widzi komunikat na stop świadczeń
        			____________________________________*/
					IF @caseSource = 5
					BEGIN
						
						SELECT @prevToken = token FROM dbo.process_instance WITH(NOLOCK) WHERE id = @previousProcessId
					    
					   	IF ISNULL(@prevToken, '') <> ''
					   	BEGIN
						   	
						   	SET @token = @prevToken
						   	UPDATE dbo.process_instance SET token = @prevToken WHERE id = @processInstanceId
						   	
					   	END
						
					END
					
				END 
				
				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1
				if @skipTransaction=0 COMMIT TRANSACTION
				return
				
			end
			else if @variant = 96 -- usługi płatne
			begin			
				EXEC dbo.p_process_new @stepId = '1011.96', @userId = @userId, @originalUserId = @originalUserId, @previousProcessId = @previousProcessId, @err = @err, @message = @message, @parentProcessId = @parentId, @processInstanceId = @processInstanceId OUT
				SET @processInstanceIds=@processInstanceId

				DELETE FROM @values
			    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '102', @groupProcessInstanceId = @rootId
			    SELECT @caseSource = value_int FROM @values
			    
				/*	Jeżeli jest to mobilne przyjęcie sprawy, to kopiujemy token. Klient widzi komunikat na stop świadczeń
        			____________________________________*/
					IF @caseSource = 5
					BEGIN
						
						SELECT @prevToken = token FROM dbo.process_instance WITH(NOLOCK) WHERE id = @previousProcessId
					    
					   	IF ISNULL(@prevToken, '') <> ''
					   	BEGIN
						   	
						   	SET @token = @prevToken
						   	UPDATE dbo.process_instance SET token = @prevToken WHERE id = @processInstanceId
						   	
					   	END
						
					END
					
				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1
				if @skipTransaction=0 COMMIT TRANSACTION
				return
			end
			else if @variant = 95 -- proces czeka
			begin

				EXEC dbo.p_process_new @stepId = '1011.95', @userId = @userId, @originalUserId = @originalUserId, @previousProcessId = @previousProcessId, @err = @err, @message = @message, @parentProcessId = @parentId, @processInstanceId = @processInstanceId OUT
				SET @processInstanceIds=@processInstanceId

				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1
				if @skipTransaction=0 COMMIT TRANSACTION
				return
			end
			else if @variant = 94 -- wybór świadczeń
			begin

				EXEC dbo.p_process_new @stepId = '1011.010', @userId = @userId, @originalUserId = @originalUserId, @previousProcessId = @previousProcessId, @err = @err, @message = @message, @parentProcessId = @parentId, @processInstanceId = @processInstanceId OUT
				SET @processInstanceIds=@processInstanceId

				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1
				if @skipTransaction=0 COMMIT TRANSACTION
				return
			end
			else if @variant = 93 -- potwierdzenie zgłoszenia na policję
			begin

				IF NOT EXISTS(SELECT id FROM dbo.process_instance WHERE step_id = '1011.055' AND active = 1 AND root_id = @rootId )
				BEGIN
					
					EXEC dbo.p_process_new @stepId = '1011.055', @userId = @userId, @originalUserId = @originalUserId, @groupProcessId = @rootId, @rootId = @rootId, @err = @err, @message = @message, @parentProcessId = null, @processInstanceId = @processInstanceId OUT
					SET @processInstanceIds=@processInstanceId
				
					UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1
					UPDATE dbo.process_instance SET previous_process_instance_id = @previousProcessId where id = @processInstanceId
					
					if @skipTransaction=0 COMMIT TRANSACTION
					return
				
				END
				
			end
			else if @variant = 92 -- anuluj bieżący krok
			begin
				
				declare @cancelledService int 
				
				select @cancelledService = ss.serviceId 
				from dbo.service_status ss WITH(NOLOCK)
				inner join dbo.process_instance pin WITH(NOLOCK) on pin.group_process_id = ss.group_process_id
				where pin.group_process_id = @groupProcessInstanceId
				
				if @cancelledService is not null
				BEGIN
					exec [dbo].[p_add_service_status]
					@groupProcessInstanceId = @groupProcessInstanceId,
					@status = -1,
					@serviceId = @cancelledService	
				END 										
				
				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId
				
				insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)  
				select 'user_action', 'Anulowanie zadania na kroku '+@stepId, @rootId, 'delete_step', @userId, GETDATE(), GETDATE() 				
				
				if @skipTransaction=0 COMMIT TRANSACTION
				return
			end
			else if @variant = 91 -- porada
			begin
				
				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId
				
				EXEC dbo.p_process_new @stepId = '1011.068', @userId = @userId, @originalUserId = @originalUserId, @previousProcessId = @previousProcessId, @err = @err, @message = @message, @parentProcessId = @parentId, @processInstanceId = @processInstanceId OUT
				SET @processInstanceIds=@processInstanceId 				
				
				if @skipTransaction=0 COMMIT TRANSACTION
				return
			end
			else if @variant = 90 -- wybór świadczeń
			begin

				EXEC dbo.p_process_new @stepId = '1133.002', @userId = @userId, @originalUserId = @originalUserId, @previousProcessId = @previousProcessId, @err = @err, @message = @message, @parentProcessId = @rootId, @processInstanceId = @processInstanceId OUT
				SET @processInstanceIds=@processInstanceId

				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1
				if @skipTransaction=0 COMMIT TRANSACTION
				return
			end
		
			IF @forceVariant > 1 AND @forceVariant <> 99
			BEGIN
				SET @variant = @forceVariant
			END

			-- check if there is process instance flow defined

			SELECT @processInstanceIds = dbo.Concatenate(distinct next_process_instance_id), @flowVariant = dbo.Concatenate(distinct variant)
			FROM dbo.process_instance_flow with(nolock)
			WHERE previous_process_instance_id = @previousProcessId and active = 1

 
			
			--SELECT top 1 @processInstanceIds = next_process_instance_id, @flowVariant = variant
			--FROM dbo.process_instance_flow
			--WHERE previous_process_instance_id = @previousProcessId and active = 1
			--order by id desc
		
			--set @processInstanceIds=isnull(@processInstanceIds,'')
			--set @flowVariant=isnull(@flowVariant,@variant)


			UPDATE dbo.process_instance_flow SET active = 0 WHERE previous_process_instance_id = @previousProcessId and active = 1
		
			set @ts=datediff(ms,getdate(),@startTime)
			print '05 - '+cast(@ts as varchar(20))
			set @startTime=getdate()


			IF @processInstanceIds IS NOT NULL
			BEGIN
				SET @nextProcessId = CAST(@processInstanceIds AS INT)

				SELECT @flowIsTechnical = s.is_technical, @flowStepId = s.id
				from step s with(nolock)
				inner join process_instance p with(nolock) on s.id = p.step_id
				where p.id = @nextProcessId

				IF @flowIsTechnical = 1
				BEGIN
					set @processInstanceIds=''

--					DEBUG::::::::::::::::::: IF @rootId = 450158
--					BEGIN 				
--						SELECT @processInstanceIds
--					END 
					
					EXEC [dbo].[p_process_next]
					@previousProcessId = @nextProcessId,
					@skipTransaction = 1,
					@userId = @userId,
					@originalUserId = @originalUserId,
					@err = @recErr OUTPUT,
					@message = @recMsg OUTPUT,
					@processInstanceIds = @recProcInsIds OUTPUT

					--DEBUG::::::::::::::::::: 
					--IF @rootId = 450970
					--BEGIN
						
					--	insert into log(name,content, created_at, param1, param2, param3)
					--	SELECT dbo.f_translate('dupa2',default), @recProcInsIds	, getdate(), @recErr, @recMsg,@nextProcessId
					--END 
					
					SET @processInstanceIds=@processInstanceIds+CAST(@recProcInsIds AS VARCHAR(15))+','
					IF ISNULL(@flowStepId,'') <> '1011.010'
					BEGIN					
						UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1				
					END 
				END

			END
			
			set @ts=datediff(ms,getdate(),@startTime)
			print '06 - '+cast(@ts as varchar(20))
			set @startTime=getdate()


			PRINT dbo.f_translate('flowVariant: ',default)
			PRINT @flowVariant
			PRINT dbo.f_translate('processInstanceIds from flow:',default) + @processInstanceIds
			-- if not, define next instances by variant
			IF @processInstanceIds IS NULL OR @flowVariant IS NOT NULL
			BEGIN

				IF @flowVariant IS NOT NULL
				BEGIN
					SET @variant = CAST(@flowVariant AS INT)
					SET @processInstanceIds = @processInstanceIds+','
				END
				ELSE
				BEGIN
					set @processInstanceIds=''
				END
	--


				/* GET NEXT STEP ID */

				set @ts=datediff(ms,getdate(),@startTime)
			print '07 - '+cast(@ts as varchar(20))
			set @startTime=getdate()

			
				declare kur cursor LOCAL for
					SELECT next_step_id FROM dbo.process_flow with(nolock) WHERE step_id = @stepId AND variant = @variant AND deleted_at IS NULL
					PRINT dbo.f_translate('step and flow:',default)
					PRINT @stepId
					PRINT @variant
				OPEN kur;
				FETCH NEXT FROM kur INTO @nextStepId;
				WHILE @@FETCH_STATUS=0
				BEGIN

					PRINT dbo.f_translate('processInstanceIds before iter:',default) + @processInstanceIds

	--				/* DEACTIVE CURRENT PROCESS AND CREATE NEW FOR NEXT STEP */
	--
	--
	--				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), choice_variant=@variant  WHERE id = @previousProcessId and active=1

					/* CHECK IF IT IS PROCESS dbo.f_translate('STOP',default) STEP */
					IF CHARINDEX('.999',@nextStepId) <> 0
						SET @processInstanceId = -999
					ELSE
					BEGIN
				
						EXEC dbo.p_process_new @stepId = @nextStepId, @userId = @userId, @originalUserId = @originalUserId, @previousProcessId = @previousProcessId, @err = @err, @message = @message, @parentProcessId = @parentId, @processInstanceId = @processInstanceId OUT
					
						-- step after function
						SELECT @procedureExists = dbo.fn_if_procedure_exists(@stepAfterFunctionName)
						IF(@procedureExists <> 0 AND @skipStepProcedure = 0)
						BEGIN
							EXEC @stepAfterFunctionName @nextProcessId = @processInstanceId, @currentUser = @userId, @token = @token OUTPUT
						END
						
      
						--- CHANGE PRIORITY
						IF @stepId LIKE '1018.%'
						BEGIN
							declare @isBrokenCarTransport int 
							
							DELETE FROM @values
							INSERT @values EXEC p_attribute_get2 @attributePath = '948', @groupProcessInstanceId = @groupProcessInstanceId
							SELECT @isBrokenCarTransport = value_int FROM @values
							
							IF @isBrokenCarTransport = 1
							BEGIN
								EXEC dbo.p_change_priority @priority = 1, @type = dbo.f_translate('tnnp',default), @groupProcessInstanceId = @groupProcessInstanceId	
							END 							
						END 
					END

					DECLARE @isTechnical bit
					SELECT @isTechnical = is_technical FROM dbo.step WITH(NOLOCK) WHERE id = @nextStepId

					IF @isTechnical = 1
					BEGIN
				
						EXEC [dbo].[p_process_next]
						@previousProcessId = @processInstanceId,
						@skipTransaction = 1,
						@userId = @userId,
						@originalUserId = @originalUserId,
						@err = @recErr OUTPUT,
						@message = @recMsg OUTPUT,
						@processInstanceIds = @recProcInsIds OUTPUT

						SET @processInstanceIds=@processInstanceIds+CAST(@recProcInsIds AS VARCHAR(15))+','
					END
					ELSE
					BEGIN
						SET @processInstanceIds=@processInstanceIds+CAST(@processInstanceId AS VARCHAR(15))+','
					END

					PRINT dbo.f_translate('processInstanceIds after iter:',default) + @processInstanceIds

					FETCH NEXT FROM kur INTO @nextStepId;
				END
				CLOSE kur
				DEALLOCATE kur
	--			IF @skipTransaction = 0 COMMIT TRANSACTION @transactionName

				SET @processInstanceIds=left(@processInstanceIds,len(@processInstanceIds)-1)
			END

			PRINT dbo.f_translate('processInstanceIds after loop:',default) + @processInstanceIds
			
			/* DEACTIVE CURRENT PROCESS */
			IF @stepId = '1011.98'
			BEGIN
				UPDATE dbo.process_instance SET active = 999, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId
			END 
			ELSE IF @stepId <> '1011.010'
			BEGIN
				UPDATE dbo.process_instance SET active = 0, updated_at=getdate(), date_leave=getdate(), updated_by_id=@userId, choice_variant=@variant  WHERE id = @previousProcessId and active=1
			END
			ELSE IF @stepId = '1011.010' OR @flowStepId = '1011.010'
			BEGIN
				UPDATE dbo.process_instance SET active = 1 where id = @previousProcessId and active = 0
			END 
			
			IF @currentStepId = '1011.022'
			BEGIN
				
				DELETE FROM @values
			    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '102', @groupProcessInstanceId = @rootId
			    SELECT @caseSource = value_int FROM @values
				
			    /*	Jeżeli jest to mobilne przyjęcie sprawy, to zmieniamy komunikat
    			____________________________________*/
				IF @caseSource = 5
				BEGIN
					SET @message = dbo.f_translate('Dziękujemy za skorzystanie z naszej aplikacji.',default)
				END
					
			END
			
		END
		ELSE BEGIN
			SET @err = 1
			SET @message = dbo.f_translate('ACTIVE PREVIOUS PROCESS NOT FOUND ',default)+ isnull(CAST(@previousProcessId as VARCHAR(15)),'')+ ' ' + isnull(CAST(@stepId as VARCHAR(15)),'')
		END
		set @ts=datediff(ms,getdate(),@startTime)
		print '08 - '+cast(@ts as varchar(20))
		set @startTime=getdate()

		
		if @skipTransaction=0 COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SET @err = 1
		SET @message = ERROR_MESSAGE()
		--select @err err, @message mesage, dbo.f_translate('aaa',default)
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	END CATCH

END