ALTER PROCEDURE [dbo].[s_1021_002_a]
( 
	@nextProcessId INT,
	@currentUser INT = NULL,
	@token VARCHAR(50) = NULL OUTPUT
) 
AS
BEGIN
	declare @groupProcessId int
	DECLARE @stepId VARCHAR(100)
	declare @platformSettedId int
	declare @partnerLocId int
	DECLARE @urlMessage NVARCHAR(MAX)
	DECLARE @hashedUrl nvarchar(300)
	DECLARE @partnerMail nvarchar(100)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	declare @message nvarchar
	declare @err int
	
	select	@groupProcessId=group_process_id, @stepId = step_id
	from	dbo.process_instance
	where	id=@nextProcessId
	
	select top 1 @token=token
	from	dbo.process_instance
	where	group_process_id=@groupProcessId and
			token is not null
	order by id
	
	-- Nie wykonuj tej proceudry, jeżeli wróciło z powrotem do 1021.002
	IF @stepId = '1021.002'
	BEGIN
		RETURN
	END
	
	UPDATE	dbo.process_instance 
	SET		token=@token
	WHERE	id = @nextProcessId

	print @groupProcessId
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessId
	SELECT @platformSettedId = value_int FROM @values



		if(isnull(@platformSettedId,0) = 2) -- FORD
			BEGIN

				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessId
				SELECT @partnerLocId = value_int FROM @values
				PRINT @partnerLocId;

				DECLARE @processID int

				EXEC p_process_new
						@stepId = '1021.003',
				@previousProcessId = NULL,
				@uniqueDefInProcess = 0,
				@skipTransaction = 0,
				@parentProcessId = NULL,
				@rootId = @groupProcessId,
				@groupProcessId =@groupProcessId,
				@err = @err output ,
				@message= @message output,
				@processInstanceId=@processID OUTPUT

				EXEC p_attribute_edit
						@attributePath = '522',
						@groupProcessInstanceId = @groupProcessId,
						@stepId =  dbo.f_translate('XXX',default),
						@valueInt = @partnerLocId,
						@err = @err OUTPUT,
						@message= @message OUTPUT

				DECLARE  @newToken VARCHAR(50)
				set @newToken=newid()

				UPDATE	dbo.process_instance
				SET		token=@newToken,
							 active = 1
				where
						parent_id IS NULL and
						step_id = '1021.003' and
						id = @processID

				-- Zmiana (2018.10.01), e-mail jet wysyłany na adres, który poda osoba uzupełniająca dane serwisu
				INSERT @values EXEC p_attribute_get2 @attributePath = '129,800,368', @groupProcessInstanceId = @groupProcessId
				SELECT @partnerMail = value_string FROM @values
--				SELECT @partnerMail = dbo.f_partner_contact(@groupProcessInstanceId,@groupProcessInstanceId,@partnerLocId,1,8)
				
				SELECT @hashedUrl = dbo.f_hashTaskUrl(
															@newToken
						)
				SET @hashedUrl = '<a href ="'+@hashedUrl+'" />'+@hashedUrl+'</a>'

				SET @urlMessage = '<HTML>' +
													'<h4>Szanowni Państwo,</h4><br>
Zgodnie z Państwa dyspozycją przesyłamy link aktywacyjny do złożenia wniosku o zorganizowanie dodatkowych świadczeń assistance w imieniu Autoryzowanego Dealera Ford, u którego przebiega naprawa uprawnionego do świadczeń assistance pojazdu. Link jest aktywny 4h od momentu przesłania go do Państwa. Prosimy kliknąć w poniższy link i postępować zgodnie z dalszymi instrukcjami.<br>'
													+ @hashedUrl +
													'</HTML>';
        DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rs')
				EXEC p_note_new
						@groupProcessId =@groupProcessId,
						@type =dbo.f_translate('email',default),
						@content = dbo.f_translate('Wysłano adres serwisowy na adres email serwisu',default),
						@phoneNumber = NULL,
						@email = @partnerMail,
						@subject = dbo.f_translate('Ford Pomoc - Starter24 - linkt aktywacyjny do złożenia wniosku',default),
						@direction = 1,
						@emailRegards = 2,
						@err = @err OUTPUT,
						@message = @message OUTPUT,
						-- FOR EMAIL
						@dw = '',
						@udw = '',
						@sender = @senderEmail,
						@additionalAttachments = '',
						@emailBody  = @urlMessage
		END
	END
