ALTER PROCEDURE [dbo].[p_CF_rental_end_notice]
AS
BEGIN
	
	DECLARE @rootId int 
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
    DECLARE @body nvarchar(max)
    DECLARE @vin nvarchar(100)
    DECLARE @subject nvarchar(1000)
    DECLARE @email nvarchar(255)
    DECLARE @senderEmail nvarchar(255)
    DECLARE @dw nvarchar(255)
    DECLARE @groupProcessInstanceId int 
    declare @noteName nvarchar(500)
    declare @dateFrom datetime 
	declare @dateTo datetime 
	declare @automatId int 
	declare @endDate datetime 
	declare @variant int 
	declare @currentUser int = 1
	declare @errId int 
	declare @regNumber nvarchar(50)
	declare @workshopDiagnosis nvarchar(4000)
	declare @eventType int 
	DECLARE @caseViewURL NVARCHAR(100)
    DECLARE @cost DECIMAL(10,2)
    DECLARE @rentalDuration INT 
			    
	declare kur cursor LOCAL for
	 	select pin.id, dateadd(day,duration.value_int,startDate.value_date), pin.group_process_id, pin.root_id
	 	from dbo.attribute_value duration with(nolock)
		inner join dbo.attribute_value startDate with(nolock) on startDate.group_process_instance_id = duration.group_process_instance_id and startDate.attribute_path = '540' 
		inner join dbo.process_instance pin with(nolock) on pin.group_process_id = duration.group_process_instance_id and pin.step_id = '1007.053' and active = 1
		inner join dbo.attribute_value platform with(nolock) on platform.group_process_instance_id = pin.group_process_id and platform.attribute_path = '253' and platform.value_int = 78
		left join dbo.attribute_value note with(nolock) on note.root_process_instance_id = duration.root_process_instance_id 
		and note.value_string = dbo.f_translate('rental-end-notice-',default)+dbo.FN_VDate(dateadd(day,duration.value_int,startDate.value_date) ) and note.attribute_path = '406,226,197'
		where duration.attribute_path = '789,786,240,153'
		and note.id is null
		
	OPEN kur;
	FETCH NEXT FROM kur INTO @automatId, @endDate, @groupProcessInstanceId, @rootId;
	WHILE @@FETCH_STATUS=0
	BEGIN
--	
--		 IF SYSTEM_USER = dbo.f_translate('andrzej.dziekonski',default) AND @rootId = 791038
--		   BEGIN
--		   	 SELECT @variant, 
--		   END
--		   
--		   
		IF [dbo].[f_find_first_working_day_before](@endDate,'08:00') <= GETDATE()
		BEGIN
			
			EXEC [dbo].[s_1007_053]
			@previousProcessId = @automatId,
			@checkDate = @endDate,
		    @variant = @variant OUTPUT, 
		    @currentUser = @currentUser, @errId = @errId output
		    
		   
		    IF @variant = 1
		    BEGIN
			 
			    SET @caseViewURL = null 
			    set @cost = null 
			    set @rentalDuration = null 
			    
			    SET @noteName = dbo.f_translate('rental-end-notice-',default)+dbo.FN_VDate(@endDate)
		        SET @caseViewURL = [dbo].[f_getDomain]() + '/cfm-case-viewer/index?case-number=' + cast(isnull(@rootId, '') as nvarchar(20))
				EXEC [dbo].[p_rental_start_and_duration] @groupProcessInstanceId = @groupProcessInstanceId, @rentalDuration = @rentalDuration OUTPUT, @carCost = @cost OUTPUT
			    
			    SELECT TOP 1 @regNumber = value_string from dbo.attribute_value with(nolock) where group_process_instance_id = @rootId and attribute_path = '74,72'
			    SELECT TOP 1 @workshopDiagnosis = value_text from dbo.attribute_value with(nolock) where root_process_instance_id = @rootId and attribute_path = '129,128'
			    SELECT TOP 1 @eventType = value_int from dbo.attribute_value with(nolock) where root_process_instance_id = @rootId and attribute_path = '491'
			    
			    
			    
				SET @subject = dbo.f_caseId(@rootId)+ISNULL(' / '+@regNumber,'')+dbo.f_translate(' / Powiadomienie o kończącym się wynajmie',default)
				
				SET @body = 'Szanowni Państwo,<br>
					Dotyczy pojazdu: {@74,73@} / {@74,72@} / {@74,71@}
					Kierowca: {@80,342,64@} {@80,342,66@}<br>
					Zdarzenie: {#diagnosisDescription()#}<br>
					Lokalizacja zdarzenia: {#displayLocalization()#}<br>
					Warsztat naprawiający: {#workshopName()#}<br>
					Diagnoza warsztatu: '+isnull(@workshopDiagnosis,'')+'<br>
					Przewidywany czas naprawy: {@286@}<br>
					Wynajem pojazdu zastępczego: {@764,73@}<br>
					Z firmy: {#partnerName(764,742)#}<br>
					Czas i koszt: '+CAST(@rentalDuration AS NVARCHAR(20))+dbo.f_translate(' doba/y, ',default)+CAST(CAST(@cost/@rentalDuration AS DECIMAL(10,2)) AS NVARCHAR(100))+' zł netto za dobę<br><br> 					
					
					Niniejszym informujemy o kończącym się '+dbo.FN_VDateHour(@endDate)+' wynajmie pojazdu zastępczego. Powodem zakończenia jest osiągnięcie limitu dni dla wynajmu<br><br>
					Wiadomość generowana automatycznie.<br>
				    Podgląd sprawy i opcja autoryzacji przedłużenia z pokrycia EXTRA są możliwe na platformie zleceń ATLAS Viewer. <br />
				    Link do sprawy: <a target="_blank" href="' + isnull(@caseViewURL, '#') + '">' + isnull(@caseViewURL, '') + '</a>'
					
					EXEC [dbo].[P_get_body_email]
						@contentEmail = @body,
						@title = @subject,
						@previousProcessId = @groupProcessInstanceId,
						@body = @body OUTPUT
					
					SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'CAREFLEET')
					
					SET @body = REPLACE(@body,'__EMAIL__', 'cfm@starter24.pl')
				
					IF @eventType = 2
					BEGIN
						SELECT @email = dbo.f_getRealEmailOrTest('cok@carefleet.com.pl')
					END 
					ELSE
					BEGIN
						SELECT @email = dbo.f_getRealEmailOrTest('szkody@carefleet.com.pl')
					END 
					
					SELECT @senderEmail = dbo.f_getRealEmailOrTest('callcenter@starter24.pl')
			  		
					EXECUTE dbo.p_note_new
			        @sender = @senderEmail
			        , @groupProcessId = @rootId
			        , @type = dbo.f_translate('email',default)
			        , @content = dbo.f_translate('Powiadomienie o kończącym się wynajmie',default)        
			        , @email = @email		
			        , @dw = @dw
			        , @userId = 1
			        , @direction = 1
			        , @subType = @noteName
			        , @subject = @subject        
			        , @emailBody = @body
			        , @err = @err OUTPUT
			        , @message = @message OUTPUT
		    END 
		END 
		
		    
	    
		
	FETCH NEXT FROM kur INTO @automatId, @endDate, @groupProcessInstanceId, @rootId;
	END
	CLOSE kur
	DEALLOCATE kur
	
END