ALTER PROCEDURE [dbo].[s_1156_006]
  (
    @previousProcessId INT,
    @variant           TINYINT OUTPUT, @currentUser int,
    @errId             int = 0 output
  )
AS
  BEGIN
    DECLARE @userId INT
    DECLARE @originalUserId INT
    DECLARE @rootId INT
    DECLARE @err INT
    DECLARE @message NVARCHAR(255)
    DECLARE @newProcessId INT
    DECLARE @nextProcessInstances varchar(4000)
    DECLARE @groupProcessInstanceId INT
    DECLARE @conditionalAssistance INT


    SELECT @groupProcessInstanceId = group_process_id,
           @userId = created_by_id,
           @rootId = root_id,
           @originalUserId = created_by_original_id
    FROM process_instance with(nolock)
    WHERE id = @previousProcessId


    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))
    INSERT @values EXEC p_attribute_get2 @attributePath = '1002', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @conditionalAssistance = value_int FROM @values


    IF isnull(@conditionalAssistance, 0) = 0
      BEGIN
        exec  p_process_jump
            @previousProcessId = @previousProcessId,
            @nextStepId = '1012.004',
            @doNext = 0,
            @parentId = NULL,
            @rootId = @rootId,
            @deactivateCurrent =1,
            @variant = @variant,
            @groupId = @rootId,
            @err = @errId output,
            @message = @message output

        SET @variant =99;

        return
      end

    DECLARE @companyUsingCar nvarchar(500)
    DECLARE @regNumber nvarchar(20)

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId

    SELECT @regNumber = value_string from @values

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1000', @groupProcessInstanceId = @groupProcessInstanceId

    DELETE FROM @values

    DECLARE @platformId int
    DECLARE @platformName NVARCHAR(100)
    INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int from @values
    SELECT @platformName = name from AtlasDB_def.dbo.platform where id = @platformId


    SELECT @companyUsingCar = value_string from @values
    DECLARE @mailTo nvarchar(500)
    DECLARE @valueMailConfig NVARCHAR(255)
    DECLARE @descriptionMailConfig NVARCHAR(255)
    EXEC dbo.p_get_business_config
        @key = 'bwb.request_veryfication_email',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @value = @valueMailConfig OUTPUT,
        @description = @descriptionMailConfig OUTPUT

    IF @platformId = 53
      BEGIN
        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('serwis@alphabet.pl') + ', ' +
                         [dbo].[f_getRealEmailOrTest]('szkody@alphabet.pl')
      end
    ELSE IF @platformId = 25
      BEGIN
        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('serwisyfa@leaseplan.pl')
      end
    ELSE IF @platformId = 48
      BEGIN

        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('serwis@businesslease.pl') + ', ' +
                         [dbo].[f_getRealEmailOrTest]('szkody@businesslease.pl')
      end
    ELSE IF @platformId = 43
      BEGIN

        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('assistance.pl@aldautomotive.com') + ', ' +
                         [dbo].[f_getRealEmailOrTest]('adrianna.adamowicz@aldautomotive.com')
      end


    DECLARE @dwd nvarchar(200) =''

    IF @platformId = 48
      BEGIN
        SELECT @dwd =dbo.f_getRealEmailOrTest('assistance@busienesslease.pl')
    end

    IF @valueMailConfig <> ''
      BEGIN
        SET @mailTo = dbo.f_getRealEmailOrTest(@valueMailConfig)
      END

    DECLARE @subject nvarchar(500)
    DECLARE @body nvarchar(MAX)


    SET @subject = isnull(@regNumber, '') + dbo.f_translate('  BRAK W BAZIE PROŚBA O POTWIERDZENIE UPRAWNIEŃ',default)

    SET @body = 'Szanowni Państwo <BR>' +
                dbo.f_translate('skontaktował się z nami użytkownik pojazdu ',default) + isnull(@regNumber, '') +
                '. Prosi o wezwanie pomocy assistance. <BR>' +
                dbo.f_translate('Pojazd nie widnieje w bazie uprawnień. Użytkownik jest z firmy ',default) + isnull(@companyUsingCar, '') +
                '. <BR>' +
                'Prosimy o potwierdzenie możliwości organizacji pomocy oraz przekazanie danych pojazdy (w tym VIN). '
    DECLARE @content NVARCHAR(100) = 'Wysłano zapytanie o BWB (poza godzinami) do '

    SET @content = @content + @platformName

 IF @platformId <> 78
   BEGIN
    EXEC p_note_new
        @groupProcessId = @groupProcessInstanceId,
        @type = dbo.f_translate('email',default),
        @content = @content,
        @phoneNumber = NULL,

        @email = @mailTo,
        @subject = @subject,
        @direction = 1,
        @emailRegards = 1,
        @err = @err OUTPUT,
        @message = @message OUTPUT,
        -- FOR EMAIL
        @dw = @dwd,
        @udw = '',
        @sender = 'cfm@starter24.pl',
        @additionalAttachments = '',
        @emailBody = @body

    -- confirm step
    EXEC [dbo].[p_process_new]
        @stepId = '1156.007',
        @userId = 1,
        @originalUserId = 1,
        @skipTransaction = 1,
        @parentProcessId = @rootId,
        @rootId = @rootId,
        @groupProcessId = @groupProcessInstanceId,
        @err = @err OUTPUT,
        @message = @message OUTPUT,
        @processInstanceId = @newProcessId OUTPUT

END
    DECLARE @programId nvarchar(4) = ''
    IF @platformId = 53
      BEGIN
        SET @programID = '524'  -- alphabet
      end
    ELSE IF @platformId = 25
      BEGIN
        SET @programID = '587' -- leaseplan
      end
    ELSE IF @platformId = 43
      BEGIN
        SET @programID = '588' --  ald
      end
    ELSE IF @platformId = 48
      BEGIN
        SET @programID = '589' -- BL
      end
    ELSE IF @platformId = 78
      BEGIN
          SET @programId = '718' -- CF
      end
    -- SETUP THE BWB NOT IN DB PROGRAM
    EXEC p_attribute_edit
        @attributePath = '202',
        @groupProcessInstanceId = @rootId,
        @stepId = 'xxx',
        @valueString = @programId,
        @err = @err OUTPUT,
        @message = @message OUTPUT


    -- jump to normal process
    exec [dbo].[p_process_jump]
        @previousProcessId = @previousProcessId,
        @nextStepId = '1011.009',
        @doNext = 1,
        @deactivateCurrent = 1,
        @rootId = @rootid,
        @groupId = @rootid,
        @err = @err output,
        @message = @message output


    IF @platformId = 78
      BEGIN
        IF (NOT (((dbo.FN_IsHoliday (getdate()) = 1) or NOT (CAST(getdate() as time) between '08:00:00' and '19:00:00'))  -- nie workday,
           AND  NOT (DATEPART(dw, getdate())=7 AND (CAST(getdate() as time) between '09:00:00' and '13:00:00')) ))
        BEGIN
          DECLARE  @postponeDate DATETIME
          IF DATEPART(dw,getdate()) in (6,7) --piątek / sobota
          BEGIN
            IF DATEPART(dw,GETDATE()) = 6
              BEGIN
--                 Wysyłanie w sobotę
                SET @postponeDate = DATEADD(day,1,getdate())
                SET @postponeDate =  DATEADD(hour,  -1*DATEPART(hour,       @postponeDate) + 9, @postponeDate)
              end
            ELSE
            BEGIN
              SET @postponeDate = DATEADD(day,2,getdate())
--               Wysyłanie w poniedziałek
              SET @postponeDate =  DATEADD(hour,  -1*DATEPART(hour,       @postponeDate) + 8, @postponeDate)
            end
          end
          ELSE
          BEGIN
-- Wysyłanie następnego dnia rano
            SET @postponeDate = DATEADD(day,1,getdate())
            SET @postponeDate =  DATEADD(hour,  -1*DATEPART(hour,       @postponeDate) + 8, @postponeDate)
          end

          SET @postponeDate = DATEADD(minute,    -1*DATEPART(minute,     @postponeDate), @postponeDate)

          DECLARE @newProcessIdCF int

          EXEC [dbo].[p_process_new]
              @stepId = '1156.010',
              @userId = 1,
              @originalUserId = 1,
              @skipTransaction = 1,
              @parentProcessId = @rootId,
              @rootId = @rootId,
              @groupProcessId = @groupProcessInstanceId,
              @err = @err OUTPUT,
              @message = @message OUTPUT,
              @processInstanceId = @newProcessIdCF OUTPUT

          UPDATE  process_instance
          set postpone_date = @postponeDate
          where id  = @newProcessIdCF

          EXEC [dbo].[p_process_new]
              @stepId = '1156.011',
              @userId = 1,
              @originalUserId = 1,
              @skipTransaction = 1,
              @parentProcessId = @rootId,
              @rootId = @rootId,
              @groupProcessId = @groupProcessInstanceId,
              @err = @err OUTPUT,
              @message = @message OUTPUT,
              @processInstanceId = @newProcessIdCF OUTPUT

          UPDATE  process_instance
          set postpone_date = @postponeDate
          where id  = @newProcessIdCF

        end
      end
  END