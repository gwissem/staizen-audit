ALTER PROCEDURE [dbo].[p_try_create_step_stolen_car]
( 
	@previousProcessId INT,
    @currentUser INT = NULL
) 
AS
BEGIN
	
	/* ______________________________________________________
	 
	 	Jeżeli wszystkie warunki spełnione, to tworzy krok, gdzie zgłaszamy, że pojazd został skradziony 
	 ________________________________________________________*/
	
	PRINT '-------------------------- EXEC [dbo].[p_create_step_stolen_car]'
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @rootId INT
	DECLARE @platformId INT
	DECLARE @stepId NVARCHAR(20)
	DECLARE @groupProcessInstanceId INT
	DECLARE @vinHeaders NVARCHAR(500)
	DECLARE @gpsService NVARCHAR(200)
	DECLARE @eventType INT
	DECLARE @newProcessInstanceId INT 
	DECLARE @platformGroup nvarchar(255)
		
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, 
			@stepId = step_id,
			@rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	/*	Tylko dla CFM
 	____________________________________*/
	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId= @groupProcessInstanceId, @name = @platformGroup OUTPUT
		
	IF @platformGroup <> dbo.f_translate('CFM',default)
	BEGIN
		RETURN
	END
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	DECLARE @vinHeadersResult Table (description NVARCHAR(500), value NVARCHAR(500), header_id INT, orderBy INT)	
	
	INSERT @vinHeadersResult EXEC [dbo].[p_get_vin_headers_by_rootId] @root_id = @rootId

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values

	-- Jeżeli pojazd posiada wbudowany GPS lub LeasePlan
	IF (SELECT count(*) FROM @vinHeadersResult WHERE description = dbo.f_translate('GPS',default) AND value = dbo.f_translate('TAK',default)) > 0 OR @platformId = 25
	BEGIN
  		
		PRINT dbo.f_translate('JEST GPS LUB PLATFORMA LP',default)
		
		/*	Dla LeasePlan
  		____________________________________*/
		
		IF @platformId = 25
		BEGIN
			
			-- Pobranie usługodawcy GPS (jeżeli nie istnieje albo inny niż "gannet", "keratronik", "wyślij info do: uslugi@alphabet.pl" to nic nie robimy
	  		SELECT @gpsService = value FROM @vinHeadersResult WHERE (description = dbo.f_translate('LojackKeratronik',default)) AND value IS NOT NULL
	  		
	  		PRINT '@gpsService'
	  		PRINT @gpsService
	  		
	  		IF ISNULL(@gpsService, '') <> ''
	  		BEGIN
		  		
		  		EXEC p_attribute_edit
      			@attributePath = '306',
      			@groupProcessInstanceId = @groupProcessInstanceId,
      			@stepId = 'xxx',
      			@valueString = @gpsService,
      			@err = @err OUTPUT,
      			@message = @message OUTPUT
		  		
      			EXEC [dbo].[p_process_new] 
				@stepId = '1011.077', 
				@userId = @currentUser, 
				@originalUserId = @currentUser, 
				@previousProcessId = @previousProcessId,
				@rootId = @rootId,
				@groupProcessId = @groupProcessInstanceId,
				@err = @err OUTPUT, 
				@message = @message OUTPUT, 
				@processInstanceId = @newProcessInstanceId OUTPUT
				
	  		END
			
		END
		ELSE
		BEGIN
			
			-- Pobranie usługodawcy GPS (jeżeli nie istnieje albo inny niż "gannet", "keratronik", "wyślij info do: uslugi@alphabet.pl" to nic nie robimy
	  		SELECT @gpsService = value FROM @vinHeadersResult WHERE (description = dbo.f_translate('Uslugodawca',default) OR description = dbo.f_translate('Lojack Keratronik',default)) AND value IS NOT NULL
	  		
	  		PRINT '@gpsService'
	  		PRINT @gpsService
	  		
	  		IF ISNULL(@gpsService, '') <> ''
	  		BEGIN
		  		
		  		IF @gpsService LIKE '%gannet%' OR @gpsService LIKE '%keratronik%' OR @gpsService LIKE '%LOJACK;%'
		  		BEGIN
			  		
			  		DECLARE @numberToGPSService NVARCHAR(100)
			  		DECLARE @nameOfGPSService NVARCHAR(100)
			  		
			  		-- Stworzenie zadania, żeby zadzwonic do serwisu i powiadomić o kradzierzy 
			  		
			  		EXEC [dbo].[p_process_new] 
					@stepId = '1155.006', 
					@userId = @currentUser, 
					@originalUserId = @currentUser, 
					@previousProcessId = @previousProcessId,
	--					@parentProcessId INT = NULL, 
					@rootId = @rootId,
					@groupProcessId = @groupProcessInstanceId,
					@err = @err OUTPUT, 
					@message = @message OUTPUT, 
					@processInstanceId = @newProcessInstanceId OUTPUT
	
					-- Postpone Date!
					UPDATE dbo.process_instance SET postpone_date = DATEADD(minute, 1, GETDATE()) WHERE id = @newProcessInstanceId
			  	
					-- Ustawienie telefonu i nazwy serwisu od GPS
					IF @gpsService LIKE '%gannet%' 
					BEGIN
						SET @numberToGPSService = '228895520' -- +48 22 889 55 20 
						SET @nameOfGPSService = 'Gannet, +48 22 889 55 20'
					END
					ELSE IF @gpsService LIKE '%keratronik%'
					BEGIN
						SET @numberToGPSService = '223801522' -- +48 22 380 15 22 
						SET @nameOfGPSService = 'Keratronic, +48 22 380 15 22'
					END
					ELSE IF @gpsService LIKE '%LOJACK;%'
					BEGIN
						SET @numberToGPSService = '02219654'
						SET @nameOfGPSService = dbo.f_translate('Lojack Keratronik, 02219654',default)
					END
					
					EXEC p_attribute_edit
	     			@attributePath = '116,197',
	     			@groupProcessInstanceId = @groupProcessInstanceId,
	     			@stepId = 'xxx',
	     			@valueString = @numberToGPSService,
	     			@err = @err OUTPUT,
	     			@message = @message OUTPUT
					
	     			EXEC p_attribute_edit
	     			@attributePath = '116,84',
	     			@groupProcessInstanceId = @groupProcessInstanceId,
	     			@stepId = 'xxx',
	     			@valueString = @nameOfGPSService,
	     			@err = @err OUTPUT,
	     			@message = @message OUTPUT
					
		  		END
		  		ELSE IF @gpsService LIKE '%uslugi@alphabet.pl%'
		  		BEGIN
			  		
			  		-- Wysłanie e-maila do Alphabetu 
			  		
			       	DECLARE @body VARCHAR(MAX)
			       	DECLARE @email VARCHAR(400)
			       	DECLARE @subject VARCHAR(400)
			        DECLARE @caseNumber NVARCHAR(20)
			        DECLARE @regNumber NVARCHAR(100)
			 		DECLARE @sender NVARCHAR(100)
			 	
			        INSERT  @values EXEC dbo.p_attribute_get2
	          		@attributePath = '74,72',
	          		@groupProcessInstanceId = @groupProcessInstanceId
	          		SELECT @regNumber = value_string FROM @values
	          		
	      			SET @caseNumber = [dbo].[f_caseId](@rootId)
			       	SET @body = '<p>Szanowni Państwo, <br>pojazd o numerze rejestracyjnym <b>' + ISNULL(@regNumber, '') + '</b>został skradziony.</p>'					
			       	SET @subject = @caseNumber + ' / ' + ISNULL(@regNumber, '') + dbo.f_translate('  / Kradzież',default)
			       	SET @email = [dbo].[f_getRealEmailOrTest]('uslugi@alphabet.pl')
			       	SET @sender = [dbo].[f_getEmail]('cfm')
			       
			       	EXECUTE dbo.p_note_new 
			       	 @groupProcessId = @groupProcessInstanceId
			       	,@type = dbo.f_translate('email',default)
			       	,@content = @body
			       	,@email = @email
			       	,@userId = 1  -- automat
			       	,@subject = @subject
			       	,@direction=1
			       	,@emailRegards=1
			       	,@sender = @sender
			       	,@emailBody = @body
			       	,@err=@err OUTPUT
			       	,@message=@message OUTPUT
			  		
		  		END
		  
	  		END
			
			
		END
  		
	END
	
END
