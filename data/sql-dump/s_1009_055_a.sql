ALTER PROCEDURE [dbo].[s_1009_055_a]
( 
	@nextProcessId INT,
	@currentUser INT = NULL, @token VARCHAR(50) = NULL OUTPUT
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @towingStatus INT
	DECLARE @fixingStatus INT
	DECLARE @reasonPartnerId INT
	DECLARE @reasonId INT
	DECLARE @executorWillDoTowing INT
	DECLARE @fixingOrTowing INT
	DECLARE @rootId INT
	DECLARE @companyId INT
	DECLARE @partnerPhone NVARCHAR(100)
	DECLARE @createdAt DATETIME 
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id, @createdAt = created_at
	FROM dbo.process_instance p with(nolock)
	WHERE id = @nextProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2
	@attributePath = '560',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2
	@attributePath = '274',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @executorWillDoTowing = value_int FROM @values

	DELETE FROM @values 
	INSERT @values EXEC dbo.p_attribute_get2
	@attributePath = '109',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towingStatus = value_int FROM @values
	
	DELETE FROM @values 
	INSERT @values EXEC dbo.p_attribute_get2
	@attributePath = '691',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerPhone = value_string FROM @values
	
    IF ISNULL(@executorWillDoTowing,-1) = 1 AND @fixingOrTowing = 2
	BEGIN
		
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '723', 
		@groupProcessInstanceId = @rootId,
		@stepId = 'xxx',
		@userId = 1,
		@originalUserId = 1,
		@valueInt = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		-- Zmiana naprawy na holowanie 		
		EXEC p_attribute_edit
		@attributePath = '560', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		IF DATEDIFF(HOUR, @createdAt, getdate()) < 8
		BEGIN
			DECLARE @content NVARCHAR(4000)
		
			EXEC [dbo].[p_sms_case_info]
			@groupProcessInstanceId = @groupProcessInstanceId,
			@addEtaChangeInfo = 0,
			@details = 1,
			@content = @content OUTPUT
				
			EXEC p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @partnerPhone,
			@addInfo = 0,
			@err = @err OUTPUT,
			@message = @message OUTPUT
				
		END 
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '855', 
	    @groupProcessInstanceId = @rootId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = 1,
	   	@err = @err OUTPUT,
	    @message = @message OUTPUT
	    
		exec dbo.s_RS @processInstanceId=@nextProcessId
		
--		UPDATE dbo.process_instance SET postpone_date = DATEADD(MINUTE, 30, GETDATE()) where id = @nextProcessId
		
	END 
	ELSE IF @fixingOrTowing = 1 AND ISNULL(@towingStatus,-1) = 2
	BEGIN
		
		EXEC p_attribute_edit
		@attributePath = '560', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 2,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '723', 
		@groupProcessInstanceId = @rootId,
		@stepId = 'xxx',
		@userId = 1,
		@originalUserId = 1,
		@valueInt = 2,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '855', 
	    @groupProcessInstanceId = @rootId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = 1,
	   	@err = @err OUTPUT,
	    @message = @message OUTPUT
	    
		exec dbo.s_RS @processInstanceId=@nextProcessId
		
	END
		
	-- MATCH PARTNER WITH ATLAS COMPANY ID
	-- set 2 for sigmeo test use case
-- 	SET @companyId = 2
	
-- 	UPDATE dbo.process_instance SET company_id = @companyId WHERE id = @nextProcessId
	
END