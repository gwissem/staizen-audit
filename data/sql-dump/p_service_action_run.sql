ALTER PROCEDURE [dbo].[p_service_action_run]
    @stepId NVARCHAR(255),
    @instanceId INT,
    @actionId INT,
    @userId INT,
    @err INT OUTPUT,
    @message NVARCHAR(255) OUTPUT,
    @processInstanceId INT OUTPUT,
    @callback nvarchar(200) OUTPUT
AS
  BEGIN
	
	  SET NOCOUNT ON 
    --------------------------------------------------------------
    -- PROCEDURA KTÓRA WYKONUJE AKCJE Z PANELU ŚWIADCZEŃ
    --
    -- Zwraca (OUTPUT) MESSAGE i ID kroku do którego ma być wskoczyć user
    --
    -- Akcje (@actionId):
    --
    -- 20: Zmiana miejsca odholowania
    --
    --------------------------------------------------------------
    SET @callback = dbo.f_translate('getTask',default)

    DECLARE @logName NVARCHAR(500)
    DECLARE @groupProcessInstanceId int
    DECLARE @processInstanceIds nvarchar(255)
    DECLARE @rootId INT
    declare @parentProcessId int
    DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

    SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id, @parentProcessId = parent_id FROM dbo.process_instance where id = @instanceId

    EXEC [dbo].[p_attribute_edit]
        @attributePath = '858',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @stepId = 'xxx',
        @userId = @userId,
        @originalUserId = @userId,
        @valueInt = @actionId,
        @err = @err OUTPUT,
        @message = @message OUTPUT

    IF @actionId = -1
      BEGIN
        select top 1 @processInstanceId =  id FROM dbo.process_instance where step_id = '1011.010' and root_id = @rootId
        SET @message = 'Cofnięto anulowanie usługi ('+CAST(@instanceId AS NVARCHAR(255))+')'

        DELETE FROM dbo.service_status where id = (
                                                  SELECT TOP 1 ss.id FROM dbo.service_status ss
                                                                            INNER JOIN dbo.service_status_dictionary ssd ON ss.status_dictionary_id = ssd.id
                                                  WHERE ss.group_process_id = @instanceId
                                                    AND ssd.progress = 5 order by ss.id desc
                                                  )
        UPDATE dbo.process_instance set active = 1 where id = (
                                                              SELECT TOP 1 p.id FROM dbo.process_instance p
                                                                                       INNER JOIN dbo.step s
                                                                                         ON p.step_id = s.id
                                                              where p.group_process_id = @instanceId
                                                                and p.active = 0
                                                                and s.is_technical = 0
                                                              ORDER BY p.id desc
                                                              )

        select top 1 @logName = pdt.name
        from dbo.process_definition_translation pdt
               inner join dbo.process_instance p on pdt.translatable_id = LEFT(p.step_id,4) and pdt.locale = 'pl'
        where p.id = @instanceId

        insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
        select 'user_action', 'Anulowanie statusu 'dbo.f_translate('anulowana',default)' usługi '+isnull(@logName,'')+' '+CAST(@instanceId AS NVARCHAR(20)), @rootId, 'cancel_service_status_cancelled', @userId, GETDATE(), GETDATE()

      END
    ELSE IF @actionId = 20
      BEGIN
        -- ZMIANA MIEJSCA ODHOLOWANIA
        -- Utworzenie kroku (1141.001), gdzie jest zmiana ASO

        EXEC [dbo].[p_process_new]
            @stepId = '1141.001',
            @userId = @userId,
            @originalUserId = @userId,
            @previousProcessId = NULL,
            @parentProcessId = @rootId,
            @rootId = @rootId,
            @groupProcessId = @groupProcessInstanceId,
      @err = @err OUTPUT,
            @message = @message OUTPUT,
            @processInstanceId = @processInstanceId OUTPUT

        insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
        select 'user_action', 'Zmiana miejsca holowania '+CAST(@groupProcessInstanceId AS NVARCHAR(20)), @rootId, 'towing_destination_change', @userId, GETDATE(), GETDATE()

      END
    ELSE IF @actionId = 30
      BEGIN
        -- ODWOŁANIE KONTRAKTORA
        -- Utworzenie kroku (1009.059), gdzie odwołujemy kontraktora

        EXEC [dbo].[p_process_new]
            @stepId = '1009.059',
            @userId = @userId,
            @originalUserId = @userId,
            @previousProcessId = NULL,
            @parentProcessId = @rootId,
            @rootId = @rootId,
            @groupProcessId = @groupProcessInstanceId,
            @err = @err OUTPUT,
            @message = @message OUTPUT,
            @processInstanceId = @processInstanceId OUTPUT

        insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
        select 'user_action', 'Odwołanie kontraktora '+CAST(@groupProcessInstanceId AS NVARCHAR(20)), @rootId, 'rsa_partner_change', @userId, GETDATE(), GETDATE()
      END 
      ELSE IF @actionId = 121
	BEGIN
	   
	    -- Stworzenie zadania na przyjęcie szkody 
	    
	    EXEC [dbo].[p_process_new]
            @stepId = '1011.070',
            @userId = @userId,
            @originalUserId = @userId,
            @previousProcessId = NULL,
            @rootId = @rootId,
            @groupProcessId = @groupProcessInstanceId,
  			@err = @err OUTPUT,
            @message = @message OUTPUT,
            @processInstanceId = @processInstanceId OUTPUT

        /*	Ustawienie uprawnień  Assistance / Przyjęcia szkody 
        ____________________________________*/
        
        DECLARE @permissionsCFM INT
        
        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '143', @groupProcessInstanceId = @rootId
        SELECT @permissionsCFM = value_int FROM @values
        
        IF @permissionsCFM IN (0,1,4)
        BEGIN
            
            EXEC p_attribute_edit
     			@attributePath = '143',
     			@groupProcessInstanceId = @rootId,
     			@stepId = 'xxx',
     			@userId = 1,
     			@originalUserId = 1,
     			@valueInt = 3,
     			@err = @err OUTPUT,
     			@message = @message OUTPUT
            
        END
            
        EXEC [dbo].[p_process_next]
        		@previousProcessId = @processInstanceId,
        		@err = @err OUTPUT,
        		@message = @message OUTPUT,
        		@processInstanceIds = @processInstanceIds OUTPUT
        
        SET @processInstanceId = CAST(@processInstanceIds AS INT)
       
        insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
        select 'user_action', 'Stworzenie formularza na przyjęcie szkody '+CAST(@groupProcessInstanceId AS NVARCHAR(20)), @rootId, 'create_claim_form', @userId, GETDATE(), GETDATE()

        
    END
    ELSE IF @actionId = 170
      BEGIN
        EXEC [dbo].[p_process_new]
            @stepId = '1148.013',
            @userId = @userId,
            @originalUserId = @userId,
            @previousProcessId = NULL,
            @parentProcessId = @rootId,
            @rootId = @rootId,
            @groupProcessId = @groupProcessInstanceId,
            @err = @err OUTPUT,
            @message = @message OUTPUT,
            @processInstanceId = @processInstanceId OUTPUT

      end
    IF @stepId = '1007.053'
      BEGIN

        EXEC [dbo].[p_process_next]
            @previousProcessId = @instanceId,
            @userId = @userId,
            @originalUserId = @userId,
            @err = @err OUTPUT,
            @message = @message OUTPUT,
            @processInstanceIds = @processInstanceIds OUTPUT

        SET @processInstanceId = @processInstanceIds


        if @actionId = 1
          BEGIN
            SET @message = dbo.f_translate('Zakończanie wynajmu. Uzupełnij koszty z jakimi wysłać GOP.',default)

            insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
            select 'user_action', 'Zakończenie wynajmu '+CAST(@groupProcessInstanceId AS NVARCHAR(20)), @rootId, 'ending_rental', @userId, GETDATE(), GETDATE()

          END
        ELSE if @actionId = 2
          BEGIN
            EXEC [dbo].[p_attribute_edit]
                @attributePath = '858',
                @groupProcessInstanceId = @groupProcessInstanceId,
                @stepId = 'xxx',
                @userId = 1,
                @originalUserId = 1,
                @valueInt = NULL,
                @err = @err OUTPUT,
                @message = @message OUTPUT

            SET @message = dbo.f_translate('Wynajem został pozytywnie przedłużony.',default)
            select top 1 @processInstanceId =  id FROM dbo.process_instance where step_id = '1011.010' and root_id = @rootId

            insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
            select 'user_action', 'Przedłużenie wynajmu '+CAST(@groupProcessInstanceId AS NVARCHAR(20)), @rootId, 'extend_rental', @userId, GETDATE(), GETDATE()

          END

      END
    ELSE if @stepId = '1007.057'
      BEGIN

        EXEC [dbo].[p_process_next]
            @previousProcessId = @instanceId,
            @userId = @userId,
            @originalUserId = @userId,
            @err = @err OUTPUT,
            @message = @message OUTPUT,
            @processInstanceIds = @processInstanceIds OUTPUT

        SET @message = dbo.f_translate('Wynajem pojazdu zastępczego został wznowiony.',default)
        select top 1 @processInstanceId =  id FROM dbo.process_instance where step_id = '1011.010' and root_id = @rootId

        insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
        select 'user_action', 'Ponowne wznowienie wynajmu '+CAST(@groupProcessInstanceId AS NVARCHAR(20)), @rootId, 'reopen_rental', @userId, GETDATE(), GETDATE()

      END
    ELSE if @stepId IN ('1009.015','1007.060') and @actionId = 4
      BEGIN

        DELETE FROM dbo.service_status where id IN (
                                                   SELECT ss.id FROM dbo.service_status ss
                                                                       INNER JOIN dbo.service_status_dictionary ssd ON ss.status_dictionary_id = ssd.id
                                                   WHERE ss.group_process_id = @groupProcessInstanceId
                                                     AND ssd.progress = 4
                                                   )
        UPDATE dbo.process_instance set active = 1 where id = @instanceId
        SET @processInstanceId = @instanceId
        SET @message = dbo.f_translate('Aktywowano ponownie krok weryfikacji zamknięcia usługi.',default)

        IF @stepId = '1009.015'
          BEGIN
            SET @logName = dbo.f_translate('Ponowna weryfikacja zamknięcia NH ',default)+CAST(@groupProcessInstanceId AS NVARCHAR(20))
          END
        ELSE
          BEGIN
            SET @logName = dbo.f_translate('Ponowna weryfikacja zamknięcia MW ',default)+CAST(@groupProcessInstanceId AS NVARCHAR(20))
          END

        insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
        select 'user_action', @logName , @rootId, 'reopen_service_cost_verify_step', @userId, GETDATE(), GETDATE()

      END



    if @actionId = 500
      BEGIN
        PRINT '0000'
        DECLARE @editorStep NVARCHAR(255) = LEFT(@stepId, 4)+'.500'
        DECLARE @serviceProgram NVARCHAR(200)
        SELECT @serviceProgram = id from dbo.attribute_value where attribute_path = '202' and group_process_instance_id = @groupProcessInstanceId
        IF @serviceProgram IS NULL
          BEGIN
            DECLARE @programId NVARCHAR(200)

            DELETE FROM @values
            INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
            SELECT @programId = value_string FROM @values

            INSERT INTO dbo.attribute_value (root_process_instance_id, group_process_instance_id, attribute_path, value_string, attribute_id)
            SELECT @rootId, @groupProcessInstanceId, '202', @programId, 202
          END


        SELECT top 1 @processInstanceId = id FROM dbo.process_instance where group_process_id = @groupProcessInstanceId and step_id = @editorStep
        IF @processInstanceId IS NULL
          BEGIN            
            SET @parentProcessId = IIF(@groupProcessInstanceId = @rootId, NULL, @rootId)

            PRINT @parentProcessId

            EXEC [dbo].[p_process_new]
                @stepId = @editorStep,
                @userId = @userId,
                @originalUserId = @userId,
                @parentProcessId = @parentProcessId,
                @rootId = @rootId,
                @groupProcessId = @groupProcessInstanceId,
                @err = @err OUTPUT,
                @message = @message OUTPUT,
                @processInstanceId = @processInstanceId OUTPUT

            update dbo.process_instance set active = 0 where id = @processInstanceId

          END

        PRINT dbo.f_translate('aaaaa',default)

        SET @logName = dbo.f_translate('Otworzono panel edycji usługi - ',default)+ @editorStep
        insert into dbo.log (name, content, param1, param2, param3, created_at, updated_at)
        select 'user_action', @logName , @rootId, 'service_editor_opened', @userId, GETDATE(), GETDATE()

        SET @callback = dbo.f_translate('serviceEditor',default)


      END
      
      
      
  END