
ALTER PROCEDURE [dbo].[s_1009_050]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @userId INT
	DECLARE @originalUserId INT
	DECLARE @message NVARCHAR(255)
	DECLARE @newProcessInstanceId INT
	DECLARE @groupProcessInstanceId INT 
	DECLARE @stepId NVARCHAR(20)
	DECLARE @executorPhone NVARCHAR(20)
	DECLARE @rootId INT	
	DECLARE @parentId INT
	DECLARE @icsId INT
	DECLARE @eta DATETIME
	DECLARE @sql NVARCHAR(4000)
	DECLARE @fixingOrTowing INT
	DECLARE @content NVARCHAR(4000)
	DECLARE @partnerId INT
	DECLARE @postponeCount INT
	DECLARE @postponeLimit INT
	DECLARE @postponeDate DATETIME
	DECLARE @checkDate DATETIME
	DECLARE @newEta DATETIME
	DECLARE @minutes BIGINT
	
	SET @postponeDate = DATEADD(SECOND,30,GETDATE())
	SET @variant = 99
	
	SELECT @groupProcessInstanceId = p.group_process_id, 
	@checkDate = p.created_at,
	@rootId = p.root_id,
	@parentId = p.parent_id,
	@postponeCount = p.postpone_count,
	@postponeLimit = s.postpone_count
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s with(nolock) on s.id = p.step_id
	LEFT JOIN process_instance pp with(nolock) on pp.id = p.previous_process_instance_id
	WHERE p.id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))		
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '609',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '691',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @executorPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	
		
	IF @icsId > 0
	BEGIN
		SET @sql = 'select * from OPENQUERY(teka2, ''select s.id, IFNULL(o.eta,o.pre_eta), o.responsible_user_phone, s.status_id, o.id order_id FROM StarterTeka.orders o 
				 left join StarterTeka.order_statuses s ON o.id = s.order_id where o.id = '''''+CAST(@icsId AS NVARCHAR(10))+'''''   '') a'
	
		declare @statusInfo as table (
			id int, 
			eta DATETIME,
			executorPhone NVARCHAR(100),
			status_id INT, 
			order_id INT
		)
	
		insert into @statusInfo
		exec (@sql)
		
		SELECT top 1 @newEta = eta from @statusInfo
		SET @minutes = DATEDIFF(MINUTE, GETDATE(), @newEta )
	END
	ELSE
	BEGIN
		DECLARE @smsText NVARCHAR(4000)
		SELECT TOP 1 @smsText = sms_text FROM sms_inbox WHERE numer_telefonu = '48'+right(@executorPhone,9) and data_otrzymania >= @checkDate AND ISNUMERIC(sms_text) = 1
		SET @minutes = CAST(@smsText AS BIGINT)
	END 
	
	
	/** zbyt duża eta */
	IF @minutes > 90
	BEGIN
		
		IF @postponeCount >= @postponeLimit
		BEGIN
		
			DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '610',
					@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @partnerId = value_int FROM @values
			
			EXEC p_add_service_refuse_reason
			@groupProcessInstanceId = @groupProcessInstanceId,
			@partnerLocationId = @partnerId,
			@reasonId = -1
			
			IF @icsId > 0
			BEGIN
				
				SET @sql = 'insert into StarterTeka.order_statuses (order_id, status_id,  submitted_at,  created_at,  updated_at, reason) values ('+
			    CAST(@icsId as NVARCHAR(20))+', 10, '''+CONVERT(VARCHAR, GETDATE(),20)+''', '''+CONVERT(VARCHAR, GETDATE(),20)+''', '''+CONVERT(VARCHAR, GETDATE(),20)+''', ''Brak reakcji na prośbę o zmianę wysokiego czasu dojazdu w ciągu 5 minut'')'
				EXEC (@sql) at Teka2
		
				SET @sql = 'update StarterTeka.orders set active = 0 where id = '+CAST(@icsId as NVARCHAR(20))
				EXEC (@sql) at Teka2
				
			END 
			-- zerowanie bieżącego partnera i typu zlecenia sprawy
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '610', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '609', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '691', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueString = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
				
			SET @content = dbo.f_translate('Tu Starter24. Propozycja zlecenia na ',default)+dbo.f_conditionalText(@fixingOrTowing,dbo.f_translate('holowanie',default),dbo.f_translate('naprawę',default))+dbo.f_translate(' dla sprawy ',default)+dbo.f_caseId(@previousProcessId)+dbo.f_translate(' w ICS jest juz nieaktualna ze względu na brak zmiany zbyt wysokiego ETA w ciągu 5 minut.',default)
					
			EXEC p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @executorPhone,
			@userId = 1,
			@originalUserId = 1,
			@addInfo = 0,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			SET @variant = 2
			
		END
	END
	ELSE
	BEGIN
		
		EXEC [dbo].[p_attribute_edit]
			@attributePath = '107', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueDate = @newEta,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
		SET @variant = 1
	END 

	
	UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = @postponeDate WHERE id = @previousProcessId
	
END

