ALTER PROCEDURE [dbo].[s_1007_040]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @fixingCarEndDate DATETIME
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @postponeCount DATETIME
	DECLARE @postponeLimit INT
	DECLARE @supervisorDecision INT
	DECLARE @programRuleDate DATETIME
	DECLARE @platformId INT
	DECLARE @partnerWorkshopId INT
	DECLARE @opened INT
	DECLARE @checkDate DATETIME
	DECLARE @towingFixingGroup INT
	DECLARE @nextProcessInstanceId INT
	DECLARE @processInstanceIds NVARCHAR(500)
	DECLARE @serviceReportGroupId INT
	DECLARE @c NVARCHAR(300)
	DECLARE @p1 NVARCHAR(100)
	DECLARE @eventLocationCountry NVARCHAR(200)
	DECLARE @replacementCarPartner INT
	DECLARE @eta DATETIME
	DECLARE @allow int
	DECLARE @programId NVARCHAR(10)
	
	SET @variant = 99
	
	SELECT @groupProcessInstanceId = p.group_process_id, @rootId = p.root_id, @postponeCount = p.postpone_count, @postponeLimit = s.postpone_count	
	FROM process_instance p WITH(NOLOCK)
	INNER JOIN step s with(nolock) ON s.id = p.step_id
	where p.id = @previousProcessId
	
	SELECT TOP 1 @serviceReportGroupId = group_process_id, @rootId = root_id FROM dbo.process_instance with(nolock) WHERE active = 1 AND step_id LIKE '1021.%' AND root_id = @rootId
	
	IF @postponeCount = 0
	BEGIN
		EXEC p_form_controls @instance_id = @previousProcessId, @returnResults = 0
	END 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventLocationCountry = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '283,285', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	
	IF [dbo].[f_abroad_case](@previousProcessId) = 1
	BEGIN		
		DECLARE @rsaPartner INT
		DECLARE @rsaPartnerId INT
		-- TODO: GET RSA PARTNER
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '837,773', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @replacementCarPartner = id FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @rsaPartnerId = value_int FROM @values
		
		IF @rsaPartnerId IS NOT NULL
		BEGIN
	
			IF @eta IS NULL
			BEGIN
				SET @eta = DATEADD(HOUR, 2, GETDATE())
				
				EXEC [dbo].[p_attribute_edit]
			     @attributePath = '540', 
			     @groupProcessInstanceId = @groupProcessInstanceId,
			     @stepId = 'xxx',
			     @userId = 1,
			     @originalUserId = 1,
			     @valueDate = @eta,
			     @err = @err OUTPUT,
			     @message = @message OUTPUT
				
			END 
			
			SELECT @rsaPartner = partner.id
			FROM dbo.attribute_value partner with(nolock)
			INNER JOIN dbo.attribute_value partnerLocation with(nolock) ON partnerLocation.parent_attribute_value_id = partner.id AND partnerLocation.attribute_path = '767,773,704'
			WHERE partner.root_process_instance_id = @rootId 
			AND partner.attribute_path = '767,773'
			AND partnerLocation.value_int IS NOT NULL
			
			EXEC [dbo].[P_attribute_copy_structure]
			@source_attribute_value_id = @rsaPartner,
			@target_attribute_value_id = @replacementCarPartner,
			@process_instance_id = @previousProcessId
	
			SET @variant = 3
		END 
		ELSE
		BEGIN
			SET @variant = 99
			UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(MINUTE, 1, GETDATE()) where id = @previousProcessId
		END 
	END 
	ELSE
	BEGIN
		
--		INSERT @values EXEC p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @rootId
--		SELECT @fixingCarEndDate = value_date FROM @values
			
		exec [dbo].[p_fixing_end_date] 
		@rootId = @rootId, 
		@fixingEndDate = @fixingCarEndDate output
		
		PRINT '@fixingCarEndDate'
		PRINT @fixingCarEndDate
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '740,123', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @supervisorDecision = value_int FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @platformId = value_int FROM @values
		
		SET @programRuleDate = GETDATE()
	
		DECLARE @hours INT
		SELECT @hours = [dbo].[f_get_platform_key]('replacement_car.minimum_fix_time', @platformId, @programId)
		
		IF ISNULL(@hours,'') <> ''
		BEGIN
			SET @programRuleDate = DATEADD(HOUR, @hours, GETDATE())	
		END 
		
		
		IF @platformId = 2 AND @programId = '429' AND ISNULL(@fixingCarEndDate, '') <> '' 
		BEGIN			
			SET @allow = 1		 
		END 
		ELSE IF @platformId = 53
		BEGIN
			SET @programRuleDate = DATEADD(second,-1,datediff(dd,0,GETDATE())+1)
		END 
		
		SELECT @checkDate = GETDATE() -- DATEADD(HOUR,2,GETDATE()) było wcześniej przed wdrożeniem PSA
		
		EXEC [dbo].[p_is_partner_workshop_opened]
		@groupProcessInstanceId = @rootId,
		@checkDate = @checkDate,
		@opened = @opened OUTPUT
		
		PRINT '---opened'
		PRINT @opened
		
		IF @opened = 0 OR dbo.f_diagnosis_code(@groupProcessInstanceId) = '1464391'
		BEGIN
			SET @variant = 1
		END 
		ELSE
		BEGIN		
			declare @monitId int			
			select top 1 @monitId = id from dbo.process_instance with(nolock) WHERE root_id = @rootId and step_id = '1021.015' and active = 1 
			
			IF @postponeCount = 0 AND @fixingCarEndDate IS NULL AND @serviceReportGroupId IS NOT NULL AND @monitId IS NULL
			BEGIN
			   
			   EXEC [dbo].[p_process_new] 
				@stepId = '1021.015', 
				@userId = 1, 
				@originalUserId = 1,
				@parentProcessId = @rootId, 
				@rootId = @rootId,
				@groupProcessId = @serviceReportGroupId,
				@err = @err OUTPUT, 
				@message = @message OUTPUT, 
				@processInstanceId = @nextProcessInstanceId OUTPUT
		
		       SET @c = dbo.f_translate('Brak daty potencjalnej daty zakończenia naprawy. Wygenerowanie zadania 1021.015 dla konsultanta ',default)
		       
			END 

			DECLARE @repairStatusTemporary int
			DELETE FROM @values

			INSERT @values EXEC p_attribute_get2 @attributePath = '129,798', @groupProcessInstanceId = @serviceReportGroupId
			SELECT @repairStatusTemporary = isnull(value_int,0) FROM @values

			IF ISNULL(@allow,0) = 1
			BEGIN
				SET @variant = 1
				SET @c = dbo.f_translate('Uzupełniona data naprawy warsztatowej pozwala na wynajem auta zastępczego. Wygenerowanie zadania 1007.003 dla konsultanta',default)
			END 
			ELSE
			BEGIN				
				IF @fixingCarEndDate IS NULL AND @supervisorDecision IS NULL 
				BEGIN
					-- nasłuchiwanie na odp. z serwisu, lub decyzję KZ
					SET @variant = 99
					UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(MINUTE, 1, GETDATE()) where id = @previousProcessId
				END 
				ELSE IF @fixingCarEndDate > @programRuleDate OR @supervisorDecision = 1
				BEGIN
					-- start 
					SET @variant = 1
					SET @c = dbo.f_translate('Uzupełniona data naprawy warsztatowej pozwala na wynajem auta zastępczego. Wygenerowanie zadania 1007.003 dla konsultanta',default)
				END 
				ELSE IF @supervisorDecision = 0 AND @fixingCarEndDate IS NULL
				BEGIN
					-- stop, odblokowanie będzie możliwe tylko manualnie
					SET @variant = 99
					UPDATE dbo.process_instance SET user_id = 1 where id = @previousProcessId
				END 
				ELSE IF @fixingCarEndDate < @programRuleDate and @repairStatusTemporary = 0
				BEGIN
					SET @variant = 2
					SET @c = dbo.f_translate('Uzupełniona data naprawy warsztatowej nie pozwala na wynajem auta zastępczego. Wygenerowanie zadania 1007.063 dla konsultanta',default)
					
					EXEC [dbo].[p_attribute_edit]
				       @attributePath = '870', 
				       @groupProcessInstanceId = @groupProcessInstanceId,
				       @stepId = 'xxx',
				       @userId = 1,
				       @originalUserId = 1,
				       @valueText = dbo.f_translate('Otrzymaliśmy informację, że naprawa warsztatowa potrwa krócej niż minimalny czas potrzebny do rozpoczęcia organizacji auta zastępczego.',default),
				       @err = @err OUTPUT,
				       @message = @message OUTPUT
				END 
				
			END 
		END		
		
		PRINT '----c'
		PRINT @c
		
		IF @c IS NOT NULL
		BEGIN
			-- LOGGER START
			SET @p1 = dbo.f_caseId(@groupProcessInstanceId)
			EXEC dbo.p_log_automat
			@name = 'rental_automat',
			@content = @c,
			@param1 = @p1
			-- LOGGER END
		END
		
	END 
	
	EXEC dbo.p_change_priority @priority = 1, @type = 'rental_fixing_date', @groupProcessInstanceId = @groupProcessInstanceId
	
END