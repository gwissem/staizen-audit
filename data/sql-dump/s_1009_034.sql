ALTER PROCEDURE [dbo].[s_1009_034]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @userId INT
	DECLARE @originalUserId INT
	DECLARE @message NVARCHAR(255)
	DECLARE @newProcessInstanceId INT
	DECLARE @groupProcessInstanceId INT
	DECLARE @processInstanceIds varchar(4000)
	DECLARE @country NVARCHAR(50)
	DECLARE @isAccident INT
	DECLARE @programId NVARCHAR(50)
	DECLARE @parkingNeeded INT
	DECLARE @rootId INT
	DECLARE @partnerCount INT
	DECLARE @partnerId INT
	DECLARE @programIds NVARCHAR(255)
	declare @distance INT
	DECLARE @platformName NVARCHAR(100)
	DECLARE @eventType INT
	DECLARE @vehicleOwner nvarchar(255)
	DECLARE @cargoWeight int 
	DECLARE @rootPlatformId int 
	
	CREATE TABLE #partnerTempTable (partnerId INT, partnerName NVARCHAR(300), priority INT, distance DECIMAL(18,2), phoneNumber NVARCHAR(100), reasonForRefusing NVARCHAR(100), partnerServiceId INT DEFAULT NULL)
	
	SELECT @groupProcessInstanceId = group_process_id, 
	@rootId = root_id,
	@userId = created_by_id, 
	@originalUserId = created_by_original_id 
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '725', 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@valueInt = 1,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '560', 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@valueInt = 1,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	DECLARE @platformId INT
	DECLARE @postFactum INT
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	SELECT @rootPlatformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '204', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programIds = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '859', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @postFactum = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '981,438', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleOwner = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,429', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @cargoWeight = value_int FROM @values
	
	IF ISNULL(@postFactum,0) = 1 and @platformId<>5
	BEGIN
		SET @variant = 1
		RETURN
	END 

	IF ISNULL(@postFactum,0) = 1 and @platformId=5
	BEGIN
		SET @variant = 5
		RETURN
	END 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isAccident = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @country = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	 
	DELETE FROM @values
	 INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	 SELECT @eventType = value_int FROM @values
	 
	EXEC [dbo].[p_platform_group_name]
	@groupProcessInstanceId = @groupProcessInstanceId,
	@name = @platformName OUTPUT

	delete from @values	
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '697',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @parkingNeeded = value_int FROM @values
	
	
	IF ISNULL(@parkingNeeded,0) = 0 AND EXISTS(
		SELECT a.groupId, a.rootId, a.rowN
		FROM (
			SELECT	ss.group_process_id groupId,
					p.root_id rootId,
					ss.status_dictionary_id, 
					row_number() over (partition by ss.serviceId order by ss.id desc) rowN, ssd.progress  
			FROM dbo.process_instance p with(nolock)
			left join dbo.service_status as ss with(nolock) on p.group_process_id = ss.group_process_id
			inner JOIN dbo.service_status_dictionary as ssd with(nolock) ON ssd.id = ss.status_dictionary_id
			where 
			p.root_id = @rootId  
			and ss.serviceId = 1	
			and p.group_process_id <> @groupProcessInstanceId	
		) a where a.rowN = 1 and (ISNULL(a.progress,0) IN (1,2,3,4))
	)
	BEGIN
		SET @variant = 4
		RETURN
	END
	
	DECLARE @transportedPeopleWithTowing INT
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '428', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @transportedPeopleWithTowing = value_int FROM @values

	IF @transportedPeopleWithTowing > 2
	BEGIN
		
		/*	Odpalenie Taxi, bo są więcej niż 2 osoby do transportu
  		____________________________________*/
		EXEC [dbo].[P_run_service_taxi] @rootId = @rootId, @parentGroupProcessInstanceId = @groupProcessInstanceId
			
		/*	Powyżej nowa funkcja, jak będzie wszystko ok, to to na dole można usunąć. 19.07.2019
   		____________________________________*/
			
--		set @newProcessInstanceId=null
--		print dbo.f_translate('TAXI odpalam',default)
--		EXEC [dbo].[p_process_new] 
--		@stepId = '1016.021', 
--		@userId = 1, 
--		@originalUserId = 1, 
--		@previousProcessId = NULL, 		
--		@parentProcessId = @rootId, 
--		@rootId = @rootId,
--		@groupProcessId = NULL,
--		@err = @err OUTPUT, 
--		@message = @message OUTPUT, 
--		@processInstanceId = @newProcessInstanceId OUTPUT
--		
--		EXEC [dbo].[p_attribute_edit]
--	    @attributePath = '856', 
--	    @groupProcessInstanceId = @newProcessInstanceId,
--	    @stepId = 'xxx',
--	    @userId = 1,
--	    @originalUserId = 1,
--	    @valueInt = @groupProcessInstanceId,
--	    @err = @err OUTPUT,
--	    @message = @message OUTPUT
--		
--
--		EXEC p_attribute_edit
--			@attributePath = '152,796', 
--			@groupProcessInstanceId = @newProcessInstanceId,
--			@stepId = 'xxx',
--			@valueInt = @groupProcessInstanceId,
--			@err = @err OUTPUT,
--			@message = @message OUTPUT
--
--		declare @variantTemp int
--
--		declare @processInstanceIds2 varchar(500)
--
--		EXECUTE dbo.p_process_next
--		   @previousProcessId=@newProcessInstanceId
--		  ,@userId=1
--		  ,@originalUserId=1
--		  ,@skipTransaction=1
--		  ,@err=@err OUTPUT
--		  ,@message=@message OUTPUT
--		  ,@processInstanceIds=@processInstanceIds2 OUTPUT

	END 

	DECLARE @processPath int	
	DECLARE @sendContractorFromStarterAbroad int
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '115', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @processPath = value_int FROM @values




	DECLARE @useLocalContractor int
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '206', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @useLocalContractor = value_int FROM @values
	
	DECLARE  @platformGrupName NVARCHAR(100)
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGrupName OUTPUT



	PRINT '@useLocalContractor'
	PRINT @useLocalContractor
	PRINT '@processPath'
	PRINT @processPath
	PRINT '@platformGrupName'
	PRINT @platformGrupName

	IF dbo.f_abroad_case(@groupProcessInstanceId) = 1 AND @useLocalContractor is null 
	AND ((@platformGrupName  = dbo.f_translate('CFM',default) AND isnull(@processPath,1) = 2) OR @platformId = 78) 
	BEGIN
		SET @variant = 9
		RETURN
	end



	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1003', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @sendContractorFromStarterAbroad = value_int FROM @values
	

	IF (isnull(@processPath,1) = 2 or @platformId=5) AND dbo.f_abroad_case(@groupProcessInstanceId) = 1 AND @sendContractorFromStarterAbroad IS NULL  and isnull(@useLocalContractor,0) = 0
	BEGIN
		if @platformId=5
		begin
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '1003', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		end
		else
		begin
			SET @variant = 6
			RETURN
		end

	end
	ELSE IF isnull(@processPath,1) = 2 AND NOT EXISTS (SELECT id from dbo.attribute_value where group_process_instance_id = @groupProcessInstanceId and attribute_id = 101)
	BEGIN
		DECLARE @insertedAttributeValue INT
		SET @variant = 7
		
		INSERT INTO dbo.attribute_value (root_process_instance_id, group_process_instance_id, attribute_path, created_at, updated_at, created_by_original_id, created_by_id, attribute_id)
   		SELECT @rootId, @groupProcessInstanceId, '101', getdate(), getdate(), 1, 1, 101

   		SET @insertedAttributeValue = SCOPE_IDENTITY()
   		
   		EXEC [dbo].[P_attribute_refresh] @attribute_value_id = @insertedAttributeValue
   		
		RETURN
	END 
	
	
	/*	Dla CFM Viewer - LeasePlan 
     * jeżeli w sprawie było Holowanie i w ciągu 12h nie pojawiła się żadna inna usługa (lub jeżeli pojawiła się jako anulowana, odwołana bezkosztowo)
     * z typem zdarzenia: Awaria, Awaria poza Aso, Opony
    	____________________________________*/
	
	IF @rootPlatformId = 25 AND @eventType = 2
	BEGIN
		
		DECLARE @parameters TABLE(name NVARCHAR(50), type NVARCHAR(20), value NVARCHAR(255) )
		DECLARE @jsonText NVARCHAR(MAX)
		DECLARE @dateExecute DATETIME = DATEADD(hour, 12, GETDATE());  
		
		INSERT INTO @parameters SELECT dbo.f_translate('processInstanceId',default), dbo.f_translate('INT',default), @previousProcessId
		INSERT INTO @parameters SELECT dbo.f_translate('serviceId',default), dbo.f_translate('INT',default), '1'
		
		SELECT @jsonText = (SELECT * FROM @parameters FOR JSON AUTO)
		
		EXEC [dbo].[add_scheduled_job] @dateExecute = @dateExecute, @procedureName = 'P_checkCaseForCloseInViewer', @parameters = @jsonText
		
	END
	
	
	IF isnull(@processPath,1) = 1 AND @country <> dbo.f_translate('Polska',default) and @platformId<>5
	BEGIN
		/* przypisanie PZ */
		SET @variant = 3
		
		EXEC [dbo].[p_programs_max_towing_distance] @groupProcessInstanceId = @groupProcessInstanceId, @distance = @distance OUTPUT
		IF @distance > 0 
		BEGIN
			/* wybranie z mapy */
			SET @variant = 5			
		END 
		
		/*	Przypadek, gdy zdarzenie jest za granicą i idziemy wariantem 3 i nie ma wybranego serwisu
		 *  Żeby z automatu nie przypisywało jakiegoś partnera na formularzu (gdy nie ma wartości to select bierze 1 opcje z brzegu)
	 		____________________________________*/
		
		IF @variant = 3 
		BEGIN
			
			EXEC [dbo].[p_attribute_edit]
		    @attributePath = '522', 
		    @groupProcessInstanceId = @groupProcessInstanceId,
		    @stepId = 'xxx',
		    @userId = 1,
		    @originalUserId = 1,
		    @valueInt = -1,
		    @err = @err OUTPUT,
		    @message = @message OUTPUT
		    
		END
		
		/*____________________________________*/
		
		RETURN
	END

	IF @rootPlatformId = 25 AND @vehicleOwner = dbo.f_translate('BAT TRADING',default) AND ISNULL(@cargoWeight,0) > 0
	BEGIN
		SET @variant = 8
		RETURN
	END




	-- Konsultacja z flotą LEASE PLAN/BL
	DECLARE @consult int = 0
	EXEC dbo.p_lp_consult_workshop_routing @groupProcessInstanceId = @groupProcessInstanceId, @consult = @consult output
	IF @consult = 1 OR (@platformId = 48 AND @eventType = 1)
	BEGIN
		declare @jump int
		set @jump=1

		if @platformId=48
		begin
			declare @fleet nvarchar(100)
			exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('Leasingobiorca',default), @output=@fleet OUTPUT

			if @fleet like '%kaufland%' set @jump=0
		end

		if @jump=1
		begin
			EXEC [dbo].[p_process_jump]
			@previousProcessId = @previousProcessId,
			@nextStepId = '1009.031',		
			@parentId = @rootId,
			@rootId = @rootId,
			@groupId = @groupProcessInstanceId,
			@err = @err output,
			@message = @message output,
			@newProcessInstanceId = @newProcessInstanceId output
	    
			
			EXEC [dbo].[p_attribute_edit]
		    @attributePath = '522', 
		    @groupProcessInstanceId = @groupProcessInstanceId,
		    @stepId = 'xxx',
		    @userId = 1,
		    @originalUserId = 1,
		    @valueInt = NULL,
		    @err = @err OUTPUT,
		    @message = @message OUTPUT
		    
			return
		end
	END 




	IF isnull(@platformGrupName,'') like dbo.f_translate('PSA',default)
		BEGIN
			EXEC p_send_email_to_PSA_ASO_towing @previousProcessId =  @groupProcessInstanceId
		end


	IF ISNULL(@programId,'') LIKE '%423%'
	BEGIN
		SET @variant = 5
		RETURN
	END
	ELSE
	BEGIN
		
		IF isnull(dbo.f_get_platform_key('towing_to_partner',@platformId, @programId),1) = 1
		BEGIN
			
			EXEC [dbo].[p_find_services]
			@groupProcessInstanceId = @groupProcessInstanceId

			SELECT @partnerCount = COUNT(*) FROM #partnerTempTable where partnerId IS NOT NULL
			SELECT TOP 1 @partnerId = partnerId FROM #partnerTempTable where partnerId IS NOT NULL
			
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '522', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = @partnerId,
			@err = @err OUTPUT,
			@message = @message OUTPUT		
			
		END 
		
		-- wypadek + Polska + VGP
		IF ISNULL(@isAccident,0) = 1 AND @country = dbo.f_translate('Polska',default) AND @platformId IN (6,11,31)
		BEGIN
			SET @variant = 2
		END 
		ELSE 
		BEGIN
			/* przypisanie najbliższego w promieniu */
			SET @variant = 3
			
			DECLARE @arcCode NVARCHAR(10)
--			SELECT @arcCode = dbo.f_diagnosis_code(@rootId)
			EXEC [dbo].[p_programs_max_towing_distance] @groupProcessInstanceId = @groupProcessInstanceId, @distance = @distance OUTPUT
			
			IF @distance > 0 
			BEGIN
				/* wybranie z mapy */
				SET @variant = 5
			END 
			ELSE IF @partnerCount > 1 OR (@eventType NOT IN (1,5) AND @platformName = dbo.f_translate('CFM',default))
			BEGIN
				/* wybór z listy partnerów */
				SET @variant = 1				
			END 
			
			IF @variant = 3 AND @partnerId is null AND isnull(dbo.f_get_platform_key('towing_to_partner',@platformId, @programId),0) = 0
			BEGIN
				EXEC [dbo].[p_find_services]
				@groupProcessInstanceId = @groupProcessInstanceId
				
				SELECT @partnerCount = COUNT(*) FROM #partnerTempTable where partnerId IS NOT NULL
				SELECT TOP 1 @partnerId = partnerId FROM #partnerTempTable where partnerId IS NOT NULL
				
				
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '522', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueInt = @partnerId,
				@err = @err OUTPUT,
				@message = @message OUTPUT	
			END 
		END 
	END	
	
	DROP TABLE #partnerTempTable

	IF @platformId = 78 AND @partnerCount > 1 AND @eventType = 1 
	BEGIN
		EXEC [dbo].[p_process_jump]
		@previousProcessId = @previousProcessId,
		@nextStepId = '1009.031',		
		@parentId = @rootId,
		@rootId = @rootId,
		@groupId = @groupProcessInstanceId,
		@err = @err output,
		@message = @message output,
		@newProcessInstanceId = @newProcessInstanceId output
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '522', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = NULL,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	END 
	
END