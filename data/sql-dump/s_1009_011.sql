ALTER PROCEDURE [dbo].[s_1009_011]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @err INT
	DECLARE @userId INT
	DECLARE @originalUserId INT
	DECLARE @message NVARCHAR(255)
	DECLARE @newProcessInstanceId INT
	DECLARE @groupProcessInstanceId INT 
	DECLARE @stepId NVARCHAR(20)
	DECLARE @postponeCount INT
	DECLARE @partnerId INT
	DECLARE @rootId INT
	DECLARE @parentId INT
	DECLARE @processInstanceId INT
	DECLARE @postponeLimit INT
	DECLARE @icsId INT
	DECLARE @isConfirmed INT
	DECLARE @status INT
	DECLARE @executorPhone NVARCHAR(255)
	DECLARE @eta DATETIME
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @locationString NVARCHAR(400)
	DECLARE @content NVARCHAR(4000)
	DECLARE @postponeDate DATETIME
	DECLARE @c NVARCHAR(MAX)
	DECLARE @p1 VARCHAR(255)
	DECLARE @p3 VARCHAR(255)
	DECLARE @sqlQuery NVARCHAR(3000)
	DECLARE @previousStepId NVARCHAR(255)
	DECLARE @customerPhone NVARCHAR(255)
	DECLARE @fixingOrTowing INT
	DECLARE @reason NVARCHAR(255)
	DECLARE @reasonId INT
	DECLARE @postponeCountFixed INT
	DECLARE @previousPartnerId INT
	DECLARE @partnerAttributeId INT
	DECLARE @recordNumber INT
	DECLARE @startDate DATETIME
	DECLARE @postponeInterval INT
	DECLARE @createdAt DATETIME
	DECLARE @onDate INT
	DECLARE @platformId INT
	DECLARE @maxRequests INT 
	DECLARE @reasonSet INT = 0
	
	PRINT '-----test-0'
	SET @isConfirmed = 0
	SET @startDate = GETDATE()
	SET @postponeInterval = 15
	
	SELECT @groupProcessInstanceId = p.group_process_id, 
	@previousStepId = pp.step_id,
	@postponeCount = p.postpone_count,
	@postponeDate = p.postpone_date,
	@postponeLimit = s.postpone_count,
	@rootId = p.root_id,
	@parentId = p.parent_id,
	@createdAt = p.created_at
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s with(nolock) on s.id = p.step_id
	LEFT JOIN process_instance pp with(nolock) on pp.id = p.previous_process_instance_id
	WHERE p.id = @previousProcessId 
	
	IF @postponeCount > 2 
	BEGIN
		SET @postponeInterval = 25
	END 
	
	SET @postponeCountFixed = @postponeCount + 1

	-- LOGGER START 
	SET @c = dbo.f_translate('Start iteracji ',default)+cast(@postponeCountFixed as nvarchar(10))
	SET @p1 = dbo.f_caseId(@rootId)
	EXEC dbo.p_log_automat
	@name = 'serviceNH_automat',
	@content = @c,
	@param1 = @p1
	-- LOGGER END
	
	
	-- zablokowanie instancji na czas jej wykonywania
	UPDATE dbo.process_instance SET user_id = 1 WHERE id = @previousProcessId
	
	SET @variant = 99
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
		
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @customerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '215', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @onDate = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	SELECT @platformId = value_int FROM @values
	
	SELECT @maxRequests = [dbo].[f_get_platform_key]('rsa.max_patrols_request', @platformId, default)

	SET @postponeLimit = 8 * ISNULL(@maxRequests,3)
	
	-- dev purposes
	IF @customerPhone = '1111111111'
	BEGIN
		SET @variant = 3
		RETURN
	END 
--	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '609',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values
	
	print '3'
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '610',
		@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @previousPartnerId = value_int, @partnerAttributeId = id FROM @values
	
	
	
	-- utwórz powody odmów
	IF @postponeCount = 0 
	BEGIN
		EXEC [dbo].[p_form_controls]
		@instance_id = @previousProcessId,
		@returnResults = 0
	END 
	
	print '4'
	IF @icsId > 0
	BEGIN
		
		
	
		SET @sqlQuery = 'select * from OPENQUERY(teka2, ''select s.id, IFNULL(o.eta,o.pre_eta), o.responsible_user_phone, s.status_id, o.id order_id, s.reason FROM StarterTeka.orders o 
				 left join StarterTeka.order_statuses s ON o.id = s.order_id where o.id = '''''+CAST(@icsId AS NVARCHAR(10))+'''''   '') a'
	
		declare @statusInfo as table (
			id int, 
			eta DATETIME,
			executorPhone NVARCHAR(100),
			status_id INT, 
			order_id INT,
			reason NVARCHAR(255)
		)
	
		insert into @statusInfo
		exec (@sqlQuery)
		
		SELECT top 1 @status = status_id, @eta = eta, @executorPhone = RIGHT(LTRIM(RTRIM(executorPhone)),9), @reason = reason FROM @statusInfo ORDER by id desc 
		
	END
	
	
	--SELECT @postponeCount, @postponeLimit
	
	-- nie udało się znaleźć kontraktora
	IF @postponeCount > @postponeLimit
	BEGIN
		
		
		-- LOGGER START 
		SET @p1 = dbo.f_caseId(@rootId)
  		EXEC dbo.p_log_automat
 		@name = 'serviceNH_automat',
  		@content = 'Przekazanie na wyszukiwanie ręczne (Nie udało się znaleźć kontraktora - przekroczona liczba prób).',
  		@param1 =@p1
		-- LOGGER END
		
		-- INSERT INTO dbo.[log] (name, content) VALUES ('s_1009_011__'+CAST(@groupProcessInstanceId AS NVARCHAR(20)), dbo.f_translate('Iteracja: ',default)+CAST(@postponeCount AS NVARCHAR(5))+dbo.f_translate('. Usuwam sprawę, przekierowuję na wyszukiwanie ręczne',default))
		
		PRINT @status
		
		SET @variant = 3
		IF @icsId > 0 AND @status IS NULL
		BEGIN
--			SET @sql = 'delete from StarterTeka.orders where id='+CAST(@icsId AS NVARCHAR(20))
--			exec (@sql) at Teka2

			INSERT OPENQUERY (Teka2, 'SELECT order_id, status_id, submitted_at, created_at, updated_at, reason
                    		  FROM   StarterTeka.order_statuses')
		    select @icsId        
		    , 8     
		    , getdate()       
		    , getdate()  
		    , getdate()        
		    , dbo.f_translate('Brak reakcji na automatyczną dystrybucję',default)     
	
		   
			
		END
		
		/** SO - nie znaleziono kontraktora automatycznie **/
		IF ISNULL(@previousStepId,'1009.043') = '1009.041'
		BEGIN
			SET @variant = 15
		END 
	END 
	ELSE
	BEGIN
		
		DECLARE @partnerPhone NVARCHAR(100)
		DECLARE @partnerName NVARCHAR(400)
		
		IF @icsId > 0
		BEGIN
		
			IF ISNULL(@status,1) = 1
			BEGIN
				SET @isConfirmed = 0
				SET @executorPhone = '0'+@executorPhone
				
				PRINT dbo.f_translate('postpone',default)
				PRINT @postponeCount
				PRINT dbo.f_translate('phone',default)
				PRINT @executorPhone
				
				IF @postponeCount % 8 = 2
				BEGIN
					SELECT @recordNumber = dbo.f_conditionalText(@fixingOrTowing, 2, 1)
					
					INSERT INTO dbo.phone_call_queue (process_instance_id, record_number, phone_number, active, created_at )
					SELECT @previousProcessId, @recordNumber, @executorPhone, 1, GETDATE()
				END 
			END 
			ELSE IF ISNULL(@status,1) NOT IN (8,10,13)
			BEGIN
				SET @variant = 1
				SET @isConfirmed = 1
				
				EXEC p_attribute_edit
				@attributePath = '107', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueDate = @eta,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				/** zbyt duża eta */
				IF DATEDIFF(MINUTE, GETDATE(), @eta ) > 90 and isnull(@onDate,0) = 0
				BEGIN
					SET @variant = 4
				END
				
				/** SO - znaleziono kontraktora **/
				IF ISNULL(@previousStepId,'1009.043') = '1009.041'
				BEGIN
					SET @variant = 15
				END 
				
			END 
			ELSE IF ISNULL(@status,1) IN (8,10,13) AND (@postponeCount % 8 <> 0)
			BEGIN
				SELECT @reasonId = value FROM dbo.dictionary WHERE typeD = 'partnerReason' and textD = @reason and active = 1
				
				IF @reasonId IS NOT NULL
				BEGIN
					EXEC p_add_service_refuse_reason
					@groupProcessInstanceId = @groupProcessInstanceId,
					@partnerLocationId = @previousPartnerId,
					@reasonId = @reasonId
				
					SET @postponeCountFixed = @postponeCount + (8 - (@postponeCount % 8))
					SET @reasonSet = 1
					
					-- LOGGER START 
			  		SET @c = dbo.f_translate('Odmowa przyjęcia zlecenia za pomocą aplikacji',default)
					SET @p1 = dbo.f_caseId(@rootId)
			  		EXEC dbo.p_log_automat
			  		@name = 'serviceNH_automat',
			  		@content = @c,
			  		@param1 = @p1,
			  		@param2 = @previousPartnerId
					-- LOGGER END
					
			  		UPDATE dbo.phone_call_queue set active = 0 where process_instance_id = @previousProcessId and active = 1
			  		
				END 
			END 
			
		END 
		ELSE IF @icsId = -1
		BEGIN
			DECLARE @smsId INT
			DECLARE @previousIterDate DATETIME
			
			INSERT @values EXEC dbo.p_attribute_get2
			@attributePath = '691',
			@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @executorPhone = right(LTRIM(RTRIM(value_string)),9) FROM @values
		
			SELECT @previousIterDate = ISNULL(@createdAt,DATEADD(MINUTE,-5,GETDATE()))
			
			SELECT TOP 1 @smsId = id FROM sms_inbox WHERE numer_telefonu = '48'+right(@executorPhone,9) and data_otrzymania >= @previousIterDate AND RTRIM(LTRIM(sms_text)) like dbo.f_translate('tak',default) order by id desc
			
			IF @smsId IS NOT NULL
			BEGIN
				SET @variant = 2
				SET @isConfirmed = 1	
				
				RETURN 
				
				/** SO - znaleziono kontraktora **/
				--IF ISNULL(@previousStepId,'1009.043') = '1009.041'
				--BEGIN
				--	SET @variant = 15
				--END 
			END 
			ELSE
			BEGIN
				
				SELECT TOP 1 @smsId = id FROM sms_inbox with(nolock) WHERE numer_telefonu = '48'+right(@executorPhone,9) and data_otrzymania >= @previousIterDate AND RTRIM(LTRIM(sms_text)) like dbo.f_translate('nie',default)  order by id desc
				
				PRINT '--------------------[debug-04-03] smsId NIE'
				PRINT @smsId
				
				IF @smsId IS NOT NULL
				BEGIN
					EXEC p_add_service_refuse_reason
					@groupProcessInstanceId = @groupProcessInstanceId,
					@partnerLocationId = @previousPartnerId,
					@reasonId = 1
				
					
					SET @postponeCount = @postponeCount + (8 - (@postponeCount % 8))
					SET @postponeCountFixed = @postponeCount + 1
					
					PRINT '--------------------[debug-04-03] nowy postponeCount'
					PRINT @postponeCountFixed
					
					SET @reasonSet = 1
					
					-- LOGGER START 
			  		SET @c = dbo.f_translate('Odmowa przyjęcia zlecenia za pomocą sms',default)
					SET @p1 = dbo.f_caseId(@rootId)
			  		EXEC dbo.p_log_automat
			  		@name = 'serviceNH_automat',
			  		@content = @c,
			  		@param1 = @p1,
			  		@param2 = @previousPartnerId
					-- LOGGER END
					
			  		UPDATE dbo.phone_call_queue set active = 0 where process_instance_id = @previousProcessId and active = 1
			  		
				END
				ELSE IF @postponeCount % 8 = 2
				BEGIN
					SELECT @recordNumber = dbo.f_conditionalText(@fixingOrTowing, 5, 4)
					
					PRINT dbo.f_translate('postpone',default)
				PRINT @postponeCount
				PRINT dbo.f_translate('phone',default)
				PRINT @executorPhone
					if isnull(@executorPhone,'')<>''
					begin
						INSERT INTO dbo.phone_call_queue (process_instance_id, record_number, phone_number, active, created_at )
						SELECT @previousProcessId, @recordNumber, @executorPhone, 1, GETDATE()
					end
--					EXEC dbo.p_callTo
--					@phoneNumber = @executorPhone,
--					@recordNumber = @recordNumber,
--					@err = @err output,
--					@description = @message output

					
				END 
			END
			
		END 
		
		
		-- znajdź partnera i wystaw nowe zlecenie w ICS/sms
		IF @icsId IS NULL OR @isConfirmed = 0
		BEGIN
			DECLARE @iteration INT
			SET @iteration = @postponeCount + 1
			
			IF @postponeCount % 8 = 0
			BEGIN
				
				IF @icsId > 0 AND @status IS NULL
				BEGIN
					
					
--					SET @sql = 'delete from StarterTeka.orders where id='+CAST(@icsId AS NVARCHAR(20))
--					exec (@sql) at Teka2
					
					INSERT OPENQUERY (Teka2, 'SELECT order_id, status_id, submitted_at, created_at, updated_at, reason
            		  FROM   StarterTeka.order_statuses')
				    select @icsId        
				    , 8     
				    , getdate()       
				    , getdate()  
				    , getdate()        
				    , dbo.f_translate('Brak reakcji na automatyczną dystrybucję',default)     
			
				    -- LOGGER START 
			  		SET @c = dbo.f_translate('Brak reakcji na automatyczną dystrybucję',default)
					SET @p1 = dbo.f_caseId(@rootId)
			  		EXEC dbo.p_log_automat
			  		@name = 'serviceNH_automat',
			  		@content = @c,
			  		@param1 = @p1,
			  		@param2 = @previousPartnerId
					-- LOGGER END
				    
					SET @content = dbo.f_translate('Tu Starter24. Propozycja zlecenia na ',default)+dbo.f_conditionalText(@fixingOrTowing,dbo.f_translate('holowanie',default),dbo.f_translate('naprawę',default))+dbo.f_translate(' dla sprawy ',default)+dbo.f_caseId(@previousProcessId)+dbo.f_translate(' w ICS jest juz nieaktualna ze względu na brak reakcji w ciągu 2 minut.',default)
					
					EXEC p_note_new
					@groupProcessId = @groupProcessInstanceId,
					@type = dbo.f_translate('sms',default),
					@content = @content,
					@phoneNumber = @executorPhone,
					@userId = 1,
					@originalUserId = 1,
					@addInfo = 0,
					@err = @err OUTPUT,
					@message = @message OUTPUT
					
				END 
				ELSE IF @icsId = -1
				BEGIN
					SET @content = dbo.f_translate('Zlecenie ',default)+dbo.f_caseId(@previousProcessId)+dbo.f_translate(' jest juz nieaktywne. Pozdrawiamy, zespol Starter24.',default)
					
					EXEC p_note_new
					@groupProcessId = @groupProcessInstanceId,
					@type = dbo.f_translate('sms',default),
					@content = @content,
					@phoneNumber = @executorPhone,
					@userId = 1,
					@originalUserId = 1,
					@addInfo = 0,
					@err = @err OUTPUT,
					@message = @message OUTPUT
					
					EXEC p_attribute_edit
					@attributePath = '609', 
					@groupProcessInstanceId = @groupProcessInstanceId,
					@stepId = 'xxx',
					@valueInt = NULL,
					@err = @err OUTPUT,
					@message = @message OUTPUT
					
					 -- LOGGER START 
			  		SET @c = dbo.f_translate('Brak reakcji na automatyczną dystrybucję',default)
					SET @p1 = dbo.f_caseId(@rootId)
			  		EXEC dbo.p_log_automat
			  		@name = 'serviceNH_automat',
			  		@content = @c,
			  		@param1 = @p1,
			  		@param2 = @previousPartnerId
					-- LOGGER END
					
				END 


				IF @status IS NULL AND ISNULL(@reasonSet,0) = 0
				BEGIN
					EXEC p_add_service_refuse_reason
					@groupProcessInstanceId = @groupProcessInstanceId,
					@partnerLocationId = @previousPartnerId,
					@reasonId = -2
				END 
				
				
				-- zerowanie bieżącego partnera i typu zlecenia sprawy
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '610', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueInt = NULL,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '609', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueInt = NULL,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '691', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueString = NULL,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				
					
					
				-- start propozycja nowego zlecenia
				IF @postponeCount < @postponeLimit
				BEGIN
				
					
					-- znajdź i zapisz partnera
					CREATE TABLE #partnerTempTable (partnerId INT, partnerName NVARCHAR(300), priority INT, distance DECIMAL(18,2), phoneNumber NVARCHAR(100), reasonForRefusing NVARCHAR(100), partnerServiceId INT)
					EXEC p_find_partner @groupProcessInstanceId = @groupProcessInstanceId, @resultType = 1
		
					DECLARE @previousPartners NVARCHAR(1000)
	
					SELECT TOP 1 @partnerId = partnerId, @partnerPhone = LTRIM(RTRIM(phoneNumber)), @partnerName = partnerName FROM #partnerTempTable
					WHERE partnerId NOT IN (
					SELECT value_int
						FROM dbo.attribute_value_history with(nolock)
						WHERE attribute_value_id = @partnerAttributeId AND value_int IS NOT NULL
					) and isnull(phoneNumber,'') <> ''
					
					DROP TABLE #partnerTempTable
					
					
					IF @partnerId IS NOT NULL
					BEGIN
					
						PRINT '--------------------[debug-04-03] nowy partnerId'
						PRINT @partnerId
						
						--INSERT INTO dbo.[log] (name, content) VALUES ('s_1009_011__'+CAST(@groupProcessInstanceId AS NVARCHAR(20)), dbo.f_translate('Iteracja: ',default)+CAST(@postponeCount AS NVARCHAR(5))+dbo.f_translate('. Znaleziono partnera: ',default)+CAST(@partnerId AS NVARCHAR(10))+dbo.f_translate(', nr tel: ',default)+@partnerPhone)
						
						EXEC [dbo].[p_attribute_edit]
						@attributePath = '610', 
						@groupProcessInstanceId = @groupProcessInstanceId,
						@stepId = 'xxx',
						@valueInt = @partnerId,
						@err = @err OUTPUT,
						@message = @message OUTPUT
				
						EXEC [dbo].[p_attribute_edit]
						@attributePath = '691', 
						@groupProcessInstanceId = @groupProcessInstanceId,
						@stepId = 'xxx',
						@valueString = @partnerPhone,
						@err = @err OUTPUT,
						@message = @message OUTPUT
			
						PRINT '--------------------[debug-04-03] nowy phone'
						PRINT @partnerPhone
						
						-- wyślij zlecenie
						EXEC [dbo].[p_rsa_new_order]
						@groupProcessInstanceId = @groupProcessInstanceId,
						@partnerPhone = @partnerPhone,
						@iteration = @iteration
			
						
						-- LOGGER START 
				  		SET @c = 'Znaleziono partnera ('+CAST(@partnerId AS NVARCHAR(10))+'), nr tel: ' + @partnerPhone
						SET @p1 = dbo.f_caseId(@rootId)
				  		EXEC dbo.p_log_automat
				  		@name = 'serviceNH_automat',
				  		@content = @c,
				  		@param1 = @p1,
				  		@param2 = @partnerName
						-- LOGGER END
						
					END
					ELSE
					BEGIN
						-- LOGGER START 
						SET @p1 = dbo.f_caseId(@rootId)
				  		EXEC dbo.p_log_automat
				  		@name = 'serviceNH_automat',
				  		@content = 'Przekazanie na wyszukiwanie ręczne (Nie udało się znaleźć kontraktora - 1 próba).',
				  		@param1 =@p1
						-- LOGGER END
						SET @variant = 3
					END 
				
				END 
				-- end propozycja nowego zlecenia
			END 
			
		END 
		
	END 
	
	SET @postponeDate = DATEADD(SECOND, @postponeInterval, ISNULL(@postponeDate,GETDATE()))	
	UPDATE dbo.process_instance SET user_id = NULL, postpone_count = @postponeCountFixed, postpone_date = @postponeDate, updated_at = GETDATE() WHERE id = @previousProcessId
			
	IF @isConfirmed = 1
	BEGIN
		
		-- LOGGER START 
		SET @p1 = dbo.f_caseId(@rootId)
		SET @p3 = convert(nvarchar(255), @eta, 120)
		EXEC dbo.p_log_automat
		@name = 'serviceNH_automat',
		@content = dbo.f_translate('Przyjęcie zgłoszenia przez kontraktora.',default),
		@param1 = @p1,
		@param2 = @executorPhone
		-- LOGGER END
	
		UPDATE dbo.phone_call_queue set active = 0 where process_instance_id = @previousProcessId and active = 1
		
	END 

	-- LOGGER START 
	SET @c = dbo.f_translate('Koniec iteracji ',default)+cast(@postponeCountFixed as nvarchar(10))
	SET @p1 = dbo.f_caseId(@rootId)
	EXEC dbo.p_log_automat
	@name = 'serviceNH_automat',
	@content = @c,
	@param1 = @p1
	-- LOGGER END

END




