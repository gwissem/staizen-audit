ALTER PROCEDURE [dbo].[p_check_available_services]
@processInstanceId INT,
@init INT = 1,
@locale NVARCHAR(10) = 'pl'
AS
BEGIN

	SET NOCOUNT ON

	PRINT '------------------ START p_available_services'

	DECLARE @platformGroup NVARCHAR(255)
	DECLARE @allow INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(MAX)
	DECLARE @weightLimit INT
	DECLARE @platformId INT
	DECLARE @platformName NVARCHAR(255)
	DECLARE @isHighway INT
	DECLARE @notSupportedCountry INT
	DECLARE @notSupportedDiagnosis INT
	DECLARE @accessDeniedConditions INT
	DECLARE @productionDate DATETIME
	DECLARE @otherCaseLastMonth INT
	DECLARE @lastCaseFixed INT
	DECLARE @eventLocationCountry NVARCHAR(255)
	DECLARE @vehicleAtService INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @variant INT
	DECLARE @mainProcessId INT
	DECLARE @policeConfirmation INT
	DECLARE @programId INT
	DECLARE @servicesIds NVARCHAR(255)
	DECLARE @isDealerCall INT
	DECLARE @eventType INT
	DECLARE @programNotFound INT
	DECLARE @towingError INT
	DECLARE @fixError INT
	DECLARE @taxiError INT
	DECLARE @partsError INT
	DECLARE @replacementCarError INT
	DECLARE @hotelError INT
	DECLARE @tripError INT
	DECLARE @HAError int
	DECLARE @transportError INT
	DECLARE @transport2Error INT
	DECLARE @loanError INT
	DECLARE @medicalLawAdviceError INT
	DECLARE @cancel INT
	DECLARE @caseFromPZ SMALLINT
	DECLARE @groupProcessInstanceId INT
	DECLARE @caseSource INT
	DECLARE @programIds NVARCHAR(255)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @serviceOpened INT = 0
	DECLARE @headerName NVARCHAR(255)
	DECLARE @rootPlatformId int
	DECLARE @cargoWeight INT
	DECLARE @vehicleOwner nvarchar(255)
	Declare @prefferedService INT

--  CZ stwierdziło że potwierdzenie nawet gdy serwis jest otwarty potrafi zając dużo czasu więc nie możemy blokować świadczeń
--	EXEC [dbo].[p_is_partner_workshop_opened]
--	@groupProcessInstanceId = @processInstanceId,
--	@opened = @serviceOpened OUTPUT

	SELECT @mainProcessId = root_id, @groupProcessInstanceId = group_process_id, @stepId = step_id FROM process_instance with(nolock) WHERE id = @processInstanceId
	-- TODO: DEALERCALL
	SELECT @isDealerCall = 0

	DECLARE @arcCode NVARCHAR(20)
	SELECT @arcCode = dbo.f_diagnosis_code(@mainProcessId)

	EXEC p_running_services
	@groupProcessInstanceId = @mainProcessId,
	@servicesIds = @servicesIds OUTPUT --activated services

	PRINT '@servicesIds'
	PRINT @servicesIds

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventLocationCountry = value_string FROM @values

 	DECLARE @allAvailableInAppServices TABLE ( id INT, alias nvarchar(100))

 	INSERT INTO @allAvailableInAppServices (id, alias)  --Taking all services with setted alias and active
 	SELECT sd.id , sd.alias
	FROM AtlasDB_def.dbo.service_definition sd
 	INNER JOIN AtlasDB_def.dbo.service_definition_translation trans ON trans.translatable_id = sd.id
 	WHERE sd.alias IS NOT NULL
	AND sd.active = 1 and isnull(sd.type,1) = 1
	GROUP BY sd.id, sd.alias
	--		SELECT @programNotFound = dbo.f_isStepVariantInProcess(@groupProcessInstanceId, '1011.038', default)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '573', @groupProcessInstanceId = @mainProcessId
	SELECT @programNotFound = value_int FROM @values

	DECLARE @diagnosisSummary NVARCHAR(255)
	SELECT @diagnosisSummary = dbo.f_diagnosis_summary(@groupProcessInstanceId)

	PRINT '---diagnosis'
	PRINT @diagnosisSummary

	CREATE TABLE #tempAvailableServices (id INT, name NVARCHAR(255), active TINYINT, message NVARCHAR(MAX), description NVARCHAR(MAX), icon NVARCHAR(MAX), variant INT, start_step_id NVARCHAR(255), pos INT, programId int)

	IF @init = 1
	BEGIN
		CREATE TABLE #availableServices (id INT, name NVARCHAR(255), active TINYINT, message NVARCHAR(MAX), description NVARCHAR(MAX), icon NVARCHAR(MAX), variant INT, start_step_id NVARCHAR(255), pos INT, programId int)
	END

	DECLARE @rootProgramId int

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = ISNULL(value_string,'') FROM @values

	SET @rootProgramId = @programId
	SELECT @rootPlatformId = platform_id from dbo.vin_program where id = @rootProgramId

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '204', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programIds = value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventType = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '542', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @policeConfirmation = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,102', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @caseSource = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '538', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @accessDeniedConditions = value_int FROM @values

	DECLARE @tempAlias nvarchar(100)
	DECLARE @tempID int
	DECLARE @tempDescriptionEnabled NVARCHAR(255)
	DECLARE @tempDescriptionDisabled NVARCHAR(255)
	DECLARE @string_disabledWhenActive nvarchar(250)
	DECLARE @string_enabledToActive nvarchar(250)
	DECLARE @string_excludedEventType nvarchar(250)
	DECLARE @string_excludedDescription nvarchar(250)
	DECLARE @abroadText nvarchar(40) = ''
	DECLARE @HOMELAND_LOCATION_NAME nvarchar(20) = dbo.f_translate('POLSKA',default) -- @todo: change this to setting!!!

	DECLARE @currentServicesList Table(data int)
	DECLARE @disableWhenServices Table(data int)
	DECLARE @enableWhenServices Table(data int)
	DECLARE @commonRows INT

	DECLARE @placeSafety INT
	DECLARE @fixingOrTowing INT
	DECLARE @overrideTowingFixing INT
	DECLARE @programRuleDate DATETIME
	DECLARE @fixingEndDate DATETIME

	DECLARE @tempValue NVARCHAR(255)
	DECLARE @tempDescription NVARCHAR(MAX)
	DECLARE @taxiProcessesCount INT
	DECLARE @taxiSelfOrganisation INT
	DECLARE @partsCostsAccepted INT
	DECLARE @programFromPZ SMALLINT
	declare @cnt int
	declare @isProducer int

	declare kur cursor LOCAL for

		SELECT split.[data] from dbo.f_split(ISNULL(@programIds, @programId),',') split
		inner join dbo.vin_program program with(nolock) on split.[data] = program.id
		order by program.is_producer desc, program.breakdown_priority desc

	OPEN kur;
	FETCH NEXT FROM kur INTO @programId;
	WHILE @@FETCH_STATUS=0
	BEGIN

		DELETE FROM #tempAvailableServices
		SET @isProducer = 0
		SET @cancel = 0
		SET @err = 0
		SET @taxiError = 0
		SET @fixError = 0
		SET @towingError = 0
		SET @replacementCarError = 0
		SET @partsError = 0
		SET @hotelError = 0
		SET @tripError = 0
		SET @transportError = 0
		SET @transport2Error = 0
		SET @HAError = 0
		SET @loanError = 0
		SET @medicalLawAdviceError = 0
		SET @string_excludedEventType = null
		SET @caseFromPZ = null

		SET @placeSafety = null
		SET @fixingOrTowing = null
		SET @overrideTowingFixing = null
		set @programRuleDate = null
		set @fixingEndDate = null

		set @tempValue = null
		set @tempDescription = null
		set @taxiProcessesCount = null
		set @taxiSelfOrganisation = null
		set @partsCostsAccepted = null
		set @programFromPZ = null

		SELECT @platformId = platform.id, @platformName = platform.name, @headerName = platform.name+ ': '+program.name, @isProducer = program.is_producer
		FROM dbo.platform platform with(nolock)
		INNER JOIN dbo.vin_program program with(nolock) on program.platform_id = platform.id
		WHERE program.id = @programId

		EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup OUTPUT

		-- NOWE ZAPYTANIE  -> SERWISY PER PLATFORMA
		INSERT INTO #tempAvailableServices (id, name, active, message, description, icon, variant, start_step_id, pos, programId)
		SELECT sd.id, sdt.name, 1, null, sdp.description, sd.icon, NULL, sd.start_step_id, sdp.[position], @rootProgramId
		FROM dbo.service_definition_program sdp with(nolock)
		INNER JOIN service_definition sd with(nolock) ON sd.id = sdp.service_definition_id
		INNER JOIN service_definition_translation sdt with(nolock) ON sd.id = sdt.translatable_id
		AND sdp.program_id = @programId
		WHERE sd.active = 1
		AND sdt.locale = @locale
		ORDER BY sdp.position



		UPDATE #tempAvailableServices SET active = 0 WHERE id IN (
			SELECT id FROM dbo.service_definition with(nolock) WHERE start_step_id IS NULL
		)

		-- Zweryfikowany negatywnie brak w bazie
		IF @programNotFound = 0
		BEGIN

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z ',default)+@platformName+dbo.f_translate(' Assistance zostały negatywnie zweryfikowane, zablokowane zostały wszystkie świadczenia.',default)
			WHERE id <> 13

			IF @init = 1
			BEGIN
				SELECT * FROM #tempAvailableServices ORDER BY pos
			END

			RETURN
		END

		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2
			   @attributePath = '716',
			   @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @caseFromPZ = value_int FROM @values

		IF ISNULL(@caseFromPZ, 0) = 1
		BEGIN

			PRINT dbo.f_translate('Zlecenie od PZ',default)

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2
				   @attributePath = '713',
				   @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @programFromPZ = value_int FROM @values

			-- @programFromPZ  { 1: Ad-hoc, 0: ARC }

			IF @programFromPZ = 0 AND ISNULL(@caseSource,0) < 3
			BEGIN

				-- DLA ARC uproszczeone "i" na kafelkach
				UPDATE #tempAvailableServices
				SET
				active = 1,
				description = dbo.f_translate('Pomoc organizowana na podstawie zlecenia otrzymanego od Partnera zagranicznego.',default)

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Ta usługa jest niedostępna w zleceniu od PZ',default)
				WHERE id IN (9, 8, 6)

				IF @init = 1
				BEGIN

					SELECT * FROM #tempAvailableServices ORDER BY pos

				END
				PRINT '------------------ END p_available_services'
				RETURN

			END

		END

		-- 	WHILE ITERATOR
		set @cnt = 1
		DECLARE @max int = (select count(*) from @allAvailableInAppServices);
		-- 	WHILE USED VARIABLES:

		-- 	WHILE USED VARIABLES END
		WHILE @cnt <= @max
			BEGIN
				DELETE FROM  @currentServicesList
				DELETE FROM  @disableWhenServices
				DELETE FROM  @enableWhenServices

				SET @commonRows = 0
				SET @tempDescriptionEnabled  = ''
				SET @tempDescriptionDisabled  = ''
				SET @abroadText=''
				SET  @string_enabledToActive =''
				SET @string_disabledWhenActive =''
				--       Select ROW
				SELECT @tempAlias =  alias, @tempID = id
				FROM (SELECT id, alias, ROW_NUMBER() OVER(ORDER BY (select 1)) as RowId
						FROM @allAvailableInAppServices
				 	 ) T1
				WHERE t1.RowId = @cnt

				IF ISNULL(@eventLocationCountry, '') <> '' AND @eventLocationCountry != @HOMELAND_LOCATION_NAME
					BEGIN
						SET @abroadText = '_abroad'
					end

				SELECT @string_enabledToActive =  [value], @tempDescriptionEnabled = description
				from dbo.f_get_platform_key_with_description(@tempAlias+'.enabled_when_active' +isnull(@abroadText,''),
				@platformId,
				@programId
				)

				SELECT @string_disabledWhenActive = [value], @tempDescriptionDisabled = description
				from dbo.f_get_platform_key_with_description(@tempAlias+'.disabled_when_active'+isnull(@abroadText,''),
				@platformId,
				@programId
				)


				INSERT INTO @currentServicesList SELECT cast(data as int) from f_split(@servicesIds, ',')
				INSERT INTO @enableWhenServices SELECT cast(data as int) from f_split(@string_enabledToActive, ',')
				INSERT INTO @disableWhenServices SELECT cast(data as int) from f_split(@string_disabledWhenActive, ',')

				SELECT @commonRows = count(*)
				FROM @currentServicesList currentServices
				INNER JOIN @enableWhenServices enabledConfig ON enabledConfig.data = currentServices.data

				IF @commonRows = 0 AND ISNULL(@string_enabledToActive,'') <> ''
				BEGIN
					UPDATE #tempAvailableServices
					SET
					active = 0,
					message = isnull(@tempDescriptionEnabled,'')
					where id = @tempID
				END

				SET @commonRows = 0

				SELECT @commonRows = count(*)
				FROM @currentServicesList currentServices
				INNER JOIN @disableWhenServices disabledConfig ON disabledConfig.data = currentServices.data

				IF @commonRows > 0 AND ISNULL(@string_disabledWhenActive,'') <> ''
				BEGIN
					UPDATE #tempAvailableServices
					SET
					active = 0,
					message = isnull(@tempDescriptionDisabled,'')
					where id = @tempID
				END

				set @cnt = @cnt+1
			end

		-------------------------------------------------------
		-- OOLICZNOŚCI WYKLUCZAJĄCE - TYLKO USŁUGA PODSTAWOWA
		-------------------------------------------------------
		if @accessDeniedConditions>0
		begin
			declare @basicServiceOnly int
			set @basicServiceOnly=0

			select @basicServiceOnly=psc.basicService
			from dbo.platform_special_conditions psc with(nolock)
			where psc.accessDeniedConditionId=@accessDeniedConditions and dbo.f_exists_in_split(platformIds,@platformId)=1

			if @basicServiceOnly=1
			begin
				DECLARE @accessDeniedConditionsText NVARCHAR(255)
				set @accessDeniedConditionsText = NULL

				SELECT @accessDeniedConditionsText = d.textD
				FROM dbo.dictionary d with(nolock)
				where typeD = 'accessDeniedConditions'
				and d.value = @accessDeniedConditions
				and dbo.f_exists_in_split(ISNULL(NULLIF(d.argument1,''),@platformId),@platformId) = 1


				-- tylko świadczenia naprawy i holowania
				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @accessDeniedConditionsText
				WHERE id > 2

				SET @taxiError = 1
				SET @partsError = 1
				SET @replacementCarError = 1
				SET @hotelError = 1
				SET @tripError = 1
				SET @transportError = 1
				SET @transport2Error = 1
				SET @loanError = 1


			end
		end
		-------------------------------------------------------
		-- ZGODA NA USŁUGĘ PŁATNĄ
		-------------------------------------------------------


		-- Wskazanie diagnozy


		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '723', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @overrideTowingFixing = value_int FROM @values

		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	 	SELECT @fixingOrTowing = value_int FROM @values

	 	IF @overrideTowingFixing IS NOT NULL
	 	BEGIN
	 	 	SET @fixingOrTowing = @overrideTowingFixing
	 	END

	 	IF EXISTS(
		SELECT 1 FROM dbo.rsa_close_event rce
			where @arcCode LIKE arc_mask
			and description = dbo.f_translate('Przysługuje tylko świadczenie naprawy/holowania',default)
			AND platform_id=@platformId
			AND isnull(program_id,@programId)=@programId
		)
		BEGIN
			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = 'Dla danych okoliczności zdarzenia (diagnoza), w ramach '+isnull(@platformName,'')+dbo.f_translate(' Assistance przysługuje tylko świadczenie naprawy/holowania',default)
			WHERE id > 2 and id not in (12,13)

			SET @taxiError = 1
			SET @replacementCarError = 1
			SET @partsError = 1
			SET @hotelError = 1
			SET @tripError = 1
			SET @transportError = 1
			SET @transport2Error = 1
			SET @loanError = 1
			SET @medicalLawAdviceError = 1
		END

		IF @overrideTowingFixing IS NOT NULL
		BEGIN
			IF @overrideTowingFixing = 1
			BEGIN
				SET @fixError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Nieudana naprawa na drodze, lub brak możliwości znalezienia kontraktora na holowanie.',default)
				WHERE id = 2
			END
			ELSE IF @overrideTowingFixing = 2
			BEGIN
				SET @towingError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Brak możliwości znalezienia kontraktora na naprawę na drodzę.',default)
				WHERE id = 1
			END
		END
		ELSE
		BEGIN

			IF ISNULL(@diagnosisSummary,'') like '%\[H]%dbo.f_translate(' ESCAPE ',default)\'
			BEGIN
				SET @fixError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Diagnoza wskazuje na holowanie',default)
				WHERE id = 2
			END
			ELSE IF ISNULL(@diagnosisSummary,'') like '%[N]%' or ISNULL(@diagnosisSummary,'') like '%[PN]%'
			BEGIN
				SET @towingError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Diagnoza wskazuje na naprawę na drodzę',default)
				WHERE id = 1
			END

		END

		IF @programId like '%423%' and @platformGroup<>dbo.f_translate('CFM',default)
		begin
		-- tylko świadczenia naprawy i holowania
			UPDATE #tempAvailableServices
			SET
					active = 0,
					message = dbo.f_translate('Świadczenie nie jest dostępne w ramach programu ad-hoc',default)
			WHERE id > 2

			SET @taxiError = 1
			SET @partsError = 1
			SET @replacementCarError = 1
			SET @hotelError = 1
			SET @tripError = 1
			SET @transportError = 1
			SET @transport2Error = 1
			SET @loanError = 1
		end

		-- pojazd skradziony
		IF @arcCode = '1464391'
		BEGIN
			SET @towingError = 1
			SET @fixError = 1
			SET @partsError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Pojazd został skradziony',default)
			WHERE id IN (1,2,8,12)
		END


		If @platformId=5
		begin
			declare @costLimit int

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '295,298', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @costLimit = value_int FROM @values

			UPDATE #tempAvailableServices
			SET
			active = 1,
			description = isnull('<p style='dbo.f_translate('color:red;',default)'>Limit kosztów: '+cast(@costLimit as varchar(100))+' EUR</p>','') + isnull(description,'')
			WHERE id = 2

			set @costLimit=null

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '295,296', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @costLimit = value_int FROM @values

			UPDATE #tempAvailableServices
			SET
			active = 1,
			description = isnull('<p style='dbo.f_translate('color:red;',default)'>Limit kosztów: '+cast(@costLimit as varchar(100))+' EUR</p>','') + isnull(description,'')
			WHERE id = 1

			set @costLimit=null

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '295,300', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @costLimit = value_int FROM @values

			UPDATE #tempAvailableServices
			SET
			active = 1,
			description = isnull('<p style='dbo.f_translate('color:red;',default)'>Limit kosztów: '+cast(@costLimit as varchar(100))+' EUR</p>','') + isnull(description,'')
			WHERE id = 3

			set @costLimit=null

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '295,299', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @costLimit = value_int FROM @values

			UPDATE #tempAvailableServices
			SET
			active = 1,
			description = isnull('<p style='dbo.f_translate('color:red;',default)'>Limit kosztów: '+cast(@costLimit as varchar(100))+' EUR</p>','') + isnull(description,'')
			WHERE id = 5

			set @costLimit=null

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '295,304', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @costLimit = value_int FROM @values

			UPDATE #tempAvailableServices
			SET
			active = 1,
			description = isnull('<p style='dbo.f_translate('color:red;',default)'>Limit kosztów: '+cast(@costLimit as varchar(100))+' EUR</p>','') + isnull(description,'')
			WHERE id = 4
		end

		-------------------------------------------------------
		-- NAPRAWA
		-------------------------------------------------------

		-- Preferowane świadczenie
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '539', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @prefferedService = value_int FROM @values

		IF @prefferedService = 2
		BEGIN
			SET @fixError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Klient preferuje holowanie.',default)
			WHERE id = 2

			UPDATE #tempAvailableServices
			SET
			active = 1,
			message = ''
			WHERE id = 1

		END
		ELSE IF @prefferedService = 1
		BEGIN

			SET @towingError = 1

			UPDATE #tempAvailableServices
			SET
			active = 1,
			message = ''
			WHERE id = 2 and dbo.f_exists_in_split(@servicesIds,'2') = 0

	--			UPDATE #availableServices
	--			SET
	--			active = 0,
	--			message = dbo.f_translate('Klient preferuje naprawę na miejscu.',default)
	--			WHERE id = 1

			-- Problem był w AUDI, gdy klient preferował naprawe na drodze, a ona sie odbyła a potem ropoczęto holowanie - Wszystko było nieaktywne - nie można odpalić HOL-PARK-HOL
			IF dbo.f_exists_in_split(@servicesIds,'1') = 0
			BEGIN

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Klient preferuje naprawę na miejscu.',default)
				WHERE id = 1

			END
			ELSE
			BEGIN

				UPDATE #tempAvailableServices
				SET
				active = 1,
				message = ''
				WHERE id = 1

			END

		END

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '414', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @placeSafety = value_int FROM @values

		-- istnieje już naprawa
		IF dbo.f_exists_in_split(@servicesIds,'2') = 1 AND @fixError = 0
		BEGIN
			SET @fixError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowana naprawa na drodze. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 2
		END

		-- Pojazd w niebezpiecznym miejscu
		IF @placeSafety IN (1,2,3) and @fixError = 0
		BEGIN
			IF @fixError = 0
			BEGIN
				SET @fixError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Ze względu na miejsce, w którym znajduje się pojazd, najpierw przetransportujemy go do serwisu lub w bezpieczną lokalizację, gdzie będzie kontynuowana naprawa. ',default)
				WHERE id = 2

				UPDATE #tempAvailableServices
				SET
				active = 1,
				message = ''
				WHERE id = 1

			END

	--			SET @fixingOrTowing = 1
			SET @diagnosisSummary = '[H]'

		END

		-------------------------------------------------------
		-- AUTO ZASTĘPCZE
		-------------------------------------------------------

	--	DELETE FROM @values
	--	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @groupProcessInstanceId
	-- 	SELECT @fixingEndDate = value_date FROM @values
	--
	 	EXEC [dbo].[p_fixing_end_date]
		@rootId = @processInstanceId,
		@fixingEndDate = @fixingEndDate output


		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('replacement_car.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @replacementCarError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 3
		END

		-- istnieje już auto zastępcze
		IF dbo.f_exists_in_split(@servicesIds,'3') = 1
		BEGIN
			SET @replacementCarError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowane auto zastępcze. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 3
		END

		-- Brak w bazie + zamknięty serwis macierzysty
	--	IF @programNotFound = -1 AND @serviceOpened = 1 AND @replacementCarError = 0
	--	BEGIN
	--
	--		SET @replacementCarError = 1
	--
	--		UPDATE #tempAvailableServices
	--		SET
	--		active = 0,
	--		message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z Assistance nie widnieją w systemie skontaktujemy się z odpowiednim Dealerem/ Partnerem Serwisowym i potwierdzimy uprawnienia. Po potwierdzeniu uprawnień otrzyma Pan/Pani wiadomość z SMS',default)
	--		WHERE id = 3
	--	END

		-- Kradzież
		IF @arcCode = '1464391'
		BEGIN
			set @err = 0
			set @variant = null
			IF @servicesIds = ''
			BEGIN
				UPDATE #tempAvailableServices
				SET
				active = 1,
				message = ''
				where id IN (3,4,6)
			END

			UPDATE #tempAvailableServices
			SET
			active = 1,
			message = ''
			where id IN (5,7)

		END

		if @replacementCarError=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('replacement_car.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @replacementCarError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 3
			end
		end

		IF @replacementCarError = 0
		BEGIN

			set @allow = null
			set @message = null

			EXEC [dbo].[p_service_allow_extend]
			@groupProcessInstanceId = @groupProcessInstanceId,
			@serviceId = 3,
			@useRoot = 1,
			@allow = @allow OUTPUT,
			@message = @message OUTPUT

			if @allow = 0
			BEGIN
				SET @replacementCarError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @message
				where id = 3
			END

		END

		IF @replacementCarError = 0
		BEGIN
			declare @translationDays int
			declare @limitInstructions nvarchar(255)

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '981,289', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @limitInstructions = value_string FROM @values

			SELECT top 1 @translationDays = days FROM dbo.replacement_car_limit_translation where program_id = @programId and event_type = @eventType and info = isnull(@limitInstructions,'nd')

			IF isnull(@translationDays,-1) = 0
			BEGIN
				SET @replacementCarError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Świadczenie auta zastępczego nie jest dostępne dla tego rodzaju zdarzenia.',default)
				where id = 3
			END
		END
		-------------------------------------------------------
		-- NOCLEG
		-------------------------------------------------------

		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('hotel.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @hotelError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+isnull(@platformName,'')+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 4
		END


		IF dbo.f_exists_in_split(@servicesIds,'4') = 1
		BEGIN
			SET @hotelError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowany nocleg. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 4
		END

		-- Brak w bazie + otwarty serwis macierzysty
		IF @programNotFound = -1 AND @hotelError = 0  AND @serviceOpened = 1
		BEGIN
			SET @hotelError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z ',default)+isnull(@platformName,'')+dbo.f_translate(' Assistance nie widnieją w systemie skontaktujemy się z odpowiednim Dealerem/Partnerem Serwisowym i potwierdzimy uprawnienia. Po potwierdzeniu uprawnień otrzyma Pan/Pani wiadomość z SMS z numerem sprawy i prośbą o ponowny kontakt.',default)
			WHERE id = 4
		END

		if @hotelError=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('hotel.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @hotelError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 4
			end
		end

		IF @hotelError = 0
		BEGIN

			set @allow = null
			set @message = null

			EXEC [dbo].[p_service_allow_extend]
			@groupProcessInstanceId = @groupProcessInstanceId,
			@serviceId = 4,
			@useRoot = 1,
			@allow = @allow OUTPUT,
			@message = @message OUTPUT

			if @allow = 0
			BEGIN
				SET @hotelError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @message
				where id = 4
			END

		END

		-------------------------------------------------------
		-- PODRÓŻ
		-------------------------------------------------------

		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('travel.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @tripError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 6
		END

		-- istnieje już podróż
		IF dbo.f_exists_in_split(@servicesIds,'6') = 1
		BEGIN
			SET @tripError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowany nocleg. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 6
		END

		if @tripError=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('travel.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @tripError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 6
			end
		end

		-------------------------------------------------------
		-- TRANSPORT
		-------------------------------------------------------

		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('transport.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @transportError = 1

			EXEC [dbo].[p_get_business_config]
			@key = 'transport.event_place_disabled_countries',
			@platformId = @platformId,
			@programId = @programId,
			@value = @tempValue OUTPUT,
			@description = @tempDescription OUTPUT

			IF ISNULL(@tempDescription, '') = ''
			BEGIN
				SET @tempDescription = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			END

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = @tempDescription
			where id = 7
		END

		-- Brak w bazie + otwarty serwis macierzysty
		IF @programNotFound = -1 AND @transportError = 0
		BEGIN
			SET @hotelError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z ',default)+@platformName+dbo.f_translate(' Assistance nie widnieją w systemie skontaktujemy się z odpowiednim Dealerem/Partnerem Serwisowym i potwierdzimy uprawnienia. Po potwierdzeniu uprawnień otrzyma Pan/Pani wiadomość z SMS z numerem sprawy i prośbą o ponowny kontakt.',default)
			where id = 7
		END

	-- 		-- istnieje już transport
		IF dbo.f_exists_in_split(@servicesIds,'7') = 1 AND @transportError = 0
		BEGIN
			SET @transportError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowany transport. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 7
		END

		if @transportError=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('transport.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @transportError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 7
			end
		end

		-------------------------------------------------------
		-- TRANSPORT NIENAPRAWIONEGO POJAZDU
		-------------------------------------------------------

		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('transportunfixed.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @transport2Error = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 16
		END

	 	-- istnieje już transport nienaprawionego pozjazdu
		IF dbo.f_exists_in_split(@servicesIds,'16') = 1 AND @transport2Error = 0
		BEGIN
			SET @transport2Error = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowany transport. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 16
		END

		if @transport2Error=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('transportunfixed.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @transport2Error = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 16
			end
		end

	-- 		-------------------------------------------------------
	-- 		-- KREDYT
	-- 		-------------------------------------------------------
	--
	-- 		-- istnieje już kredyt
		IF dbo.f_exists_in_split(@servicesIds,'9') = 1
		BEGIN
			SET @fixError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowany kredyt. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 9
		END

		-- Zdarzenie w Polsce
		IF @eventLocationCountry = dbo.f_translate('Polska',default) AND @loanError = 0
		BEGIN
			SET @loanError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Nie mamy możliwości zorganizowania usługi, jeśli pojazd znajduje się na terenie Polski. Jest to niezgodne z Ogólnymi Warunkami Programu',default)
			WHERE id = 9
		END

		if @loanError=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('credit.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @loanError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 9
			end
		end
		-------------------------------------------------------
		-- POMOC PRAWNA I MEDYCZNA
		-------------------------------------------------------

		-- Tylko volksvagen
		IF @platformId <> 11 AND @medicalLawAdviceError = 0
		BEGIN
			SET @medicalLawAdviceError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Świadczenie dostępne tylko dla platformy VW',default)
			WHERE id = 11
		END

		-- Tylko za granicą
		IF @eventLocationCountry = dbo.f_translate('Polska',default) AND @medicalLawAdviceError = 0
		BEGIN
			SET @medicalLawAdviceError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Świadczenie przysługuje tylko jeśli lokalizacja zdarzenia jest za granicą.',default)
			WHERE id = 11
		END

		if @medicalLawAdviceError=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('information.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @medicalLawAdviceError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 11
			end
		end
		-------------------------------------------------------
		-- TAXI
		-------------------------------------------------------

		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('taxi.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @taxiError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 5
		END

		-- Brak w bazie + zamknięty serwis macierzysty
		IF @programNotFound = -1 AND @taxiError = 0 AND @serviceOpened = 1
		BEGIN

			-- organizowano już taxi

			SELECT @taxiProcessesCount = COUNT(*)
			FROM process_instance  with(nolock)
			WHERE step_id LIKE '1016.%'
			AND active = 1
			AND parent_id = @mainProcessId


			IF @taxiProcessesCount > 0
			BEGIN
				SET @taxiError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z Assistance nie widnieją w systemie skontaktujemy się z odpowiednim Dealerem/Partnerem Serwisowym i potwierdzimy uprawnienia. Po potwierdzeniu uprawnień otrzyma Pan wiadomość z SMS z num',default)
				WHERE id = 5
			END
		END

		-- Zdarzenie za granicą
		IF @eventLocationCountry <> dbo.f_translate('Polska',default) AND @taxiError = 0
		BEGIN
			-- klient sam organizuje i będzie ubiegał się o zwrot

			IF @taxiSelfOrganisation = 1
			BEGIN
				SET @taxiError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Klient będzie organizował samodzielnie z opcją ubiegania się o zwrot kosztów',default)
				WHERE id = 5
			END
		END

		if @taxiError=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('taxi.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @taxiError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 5
			end
		end


	--	IF ISNULL(@prefferedService,1) <> 2 AND @diagnosisSummary IN ('[N]','[PN]') AND dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @taxiError = 0
	--	BEGIN
	--		SET @taxiError = 1
	--
	--		UPDATE #tempAvailableServices
	--		SET
	--		active = 0,
	--		message = dbo.f_translate('Świadczenie TAXI nie przysługuje w przypadku naprawy na drodze.',default)
	--		WHERE id = 5
	--	END


		-------------------------------------------------------
		-- CZĘŚCI ZAMIENNE
		-------------------------------------------------------

		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('parts.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @partsError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 8
		END

	-- 		-- istnieje już cz
		IF dbo.f_exists_in_split(@servicesIds,'8') = 1 AND @partsError = 0
		BEGIN
			SET @partsError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowany kredyt. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 8
		END

		-- Lokalizacja w Polsce i nie AUDI i usługa Holowania

		IF @partsError = 0
		BEGIN

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '559', @groupProcessInstanceId = @mainProcessId, @rootProcessInstanceId = @mainProcessId
			SELECT @partsCostsAccepted = MAX(value_int) FROM @values

			IF @partsCostsAccepted = 0
			BEGIN
				set @partsError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Organizacja Części zamiennych jest możliwa tylko jeżeli zgodzi się Pan/Pani na pokrycie ich kosztów. W związku z brakiem zgody nie mamy możliwości zorganizowania tej usługi dla Pana/Pani',default)
				WHERE id = 8
			END

		END

		if @partsError=0
		begin
			set @string_excludedEventType=null
			set @string_excludedDescription=null

			SELECT @string_excludedEventType = [value], @string_excludedDescription = description
			from dbo.f_get_platform_key_with_description('replacement_parts.excluded_event_type',@platformId,@programId)

			if exists(
				select 1
				from dbo.f_split(@string_excludedEventType,',')
				where data=@eventType
			)
			begin
				SET @partsError = 1

				UPDATE #tempAvailableServices
				SET
				active = 0,
				message = @string_excludedDescription
				WHERE id = 8
			end
		end

		IF @platformId in (6,11) AND @eventLocationCountry = dbo.f_translate('Polska',default) and @partsError = 0
		BEGIN
			set @partsError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Organizacja części zamiennych jest możliwa tylko jeżeli zdarzenie ma miejsce poza granicami Polski. W związku z tym nie mamy możliwości zorganizowania tej usługi dla Pana/Pani.',default)
			WHERE id = 8
		END
		-------------------------------------------------------
		-- HOLOWANIE POSZKODOWANEGO
		-------------------------------------------------------

		IF @eventType <> 1
		BEGIN
	--			SET @victimCarError = 1
			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('Nie mamy możliwości zorganizowania holowania poszkodowanego pojazdu jeśli nie brał udziału w wypadku.',default)
			WHERE id = 15
		END


		IF NOT EXISTS (SELECT id FROM #availableServices)
		BEGIN
			INSERT INTO #availableServices
			SELECT * FROM #tempAvailableServices
		END


		-- ===============================
		-- Dodatkowe warunki dla platform VGP
		-- ===============================

		IF @platformId IN (6,11,31)
		BEGIN
			EXEC [dbo].[p_check_available_services_vgp]
			@processInstanceId = @processInstanceId,
			@rootId = @mainProcessId,
			@stepId = @stepId,
			@servicesIds = @servicesIds,
			@arcCode = @arcCode,
			@fixingEndDate = @fixingEndDate,
			@eventType = @eventType,
			@init = 0,
			@programId = @programId

		END
		ELSE IF @platformId = 2
		BEGIN

			-- ===============================
			-- Dodatkowe warunki dla platform Ford
			-- ===============================

			EXEC [dbo].[p_check_available_services_ford]
			@processInstanceId = @processInstanceId,
			@rootId = @mainProcessId,
			@stepId = @stepId,
			@servicesIds = @servicesIds,
			@arcCode = @arcCode,
			@fixingEndDate = @fixingEndDate,
			@eventType = @eventType,
			@init = 0,
			@programId = @programId

		END
		ELSE IF @platformId IN (14)--(32,10,14)
		BEGIN

			EXEC dbo.p_check_available_services_opel_kia_seat
			@processInstanceId = @processInstanceId,
			@rootId = @mainProcessId,
			@stepId = @stepId,
			@servicesIds = @servicesIds,
			@arcCode = @arcCode,
			@fixingEndDate = @fixingEndDate,
			@eventType = @eventType,
			@init = 0,
			@programId = @programId

		END
		ELSE IF @platformGroup = dbo.f_translate('CFM',default)
		BEGIN

			-- ===============================
			-- Dodatkowe warunki dla platform CFM
			-- ===============================

			EXEC dbo.p_check_available_services_cfm
			@processInstanceId = @processInstanceId,
			@rootId = @mainProcessId,
			@stepId = @stepId,
			@servicesIds = @servicesIds,
			@arcCode = @arcCode,
			@fixingEndDate = @fixingEndDate,
			@eventType = @eventType,
			@init = 0,
			@programId = @programId

		END
		ELSE IF @platformGroup = dbo.f_translate('PSA',default)
		BEGIN

			-- ===============================
			-- Dodatkowe warunki dla platform PSA
			-- ===============================

			EXEC dbo.p_check_available_services_psa
			@processInstanceId = @processInstanceId,
			@rootId = @mainProcessId,
			@stepId = @stepId,
			@servicesIds = @servicesIds,
			@arcCode = @arcCode,
			@fixingEndDate = @fixingEndDate,
			@eventType = @eventType,
			@init = 0,
			@programId = @programId

		END


		if exists (
			select id from dbo.process_instance with(nolock) where step_id in ('1021.018','1021.017','1021.015') and active=1 and root_id=@mainProcessId
		)
		UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Monitoring usługi warsztatowej jest aktywny.',default)
				WHERE id = 12

		if exists(
			select	pin.group_process_id
			from	dbo.process_instance pin with(nolock) inner join
					dbo.service_status ss with(nolock) on ss.group_process_id = pin.group_process_id inner join
					dbo.service_status_dictionary ssd with(nolock) on ssd.id = ss.status_dictionary_id
			where	root_id=@mainProcessId and ss.serviceId=12 and ssd.progress = 4
		)
		UPDATE #tempAvailableServices
		SET
		active = 0,
		message = dbo.f_translate('Naprawa warsztatowa zakończona',default)
		WHERE id = 12


		-- anulowanie świadczenia
		UPDATE #tempAvailableServices SET active = 0, variant = NULL where id = 13

		if exists(
			select	pin.group_process_id
			from	dbo.process_instance pin with(nolock) inner join
					dbo.service_definition sd with(nolock) on left(sd.start_step_id,4)=left(step_Id,4) inner join
					dbo.service_definition_translation st with(nolock) on st.translatable_id=sd.id and st.locale='pl' inner join
					(

						select	ss.group_process_id, ss.serviceId,max(ssd.progress) progress
						from	dbo.service_status ss with(nolock) inner join
								dbo.service_status_dictionary ssd with(nolock) on ssd.id=ss.status_dictionary_id
						group by ss.group_process_id, ss.serviceId

					) ssd1 on ssd1.group_process_id=pin.group_process_id and ssd1.serviceId=sd.id and ssd1.progress in (1,2,3,4)
			where	root_id=@mainProcessId and sd.id<>13

		)
		begin
			UPDATE #tempAvailableServices SET active = 1, variant = NULL where id = 13
		end

		-- ===============================
		-- home assistance wykluczenie zagranicy dla tuw tuz home assistance i tuw pocztowy
		-- ===============================
		print dbo.f_translate('HOME ASSISTANCE',default)

		IF @platformId in (36,42) AND ISNULL(@eventLocationCountry, '') <> dbo.f_translate('Polska',default) AND @err = 0
		BEGIN
			set @HAError=1

			UPDATE #tempAvailableServices
				SET
				active = 0,
				message = dbo.f_translate('Pomoc przysługuje tylko na terenie Polski.',default)
				WHERE id = 17
		END
		-------------------------------------------

		-- istnieje już podróż
		IF @HAError=0 and dbo.f_exists_in_split(@servicesIds,'17') = 1
		BEGIN
			SET @HAError = 1

			UPDATE #tempAvailableServices
			SET
			active = 0,
			message = dbo.f_translate('W systemie jest już zorganizowana usługa Home Assistance. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
			WHERE id = 17
		END

		UPDATE #availableServices
		SET description = isnull(#availableServices.description+'<br>','') + ISNULL('<b class="program-header">'+@headerName+' </b><br>' + #tempAvailableServices.description,dbo.f_translate('Brak opisu',default)),
			message = CASE WHEN isnull(#tempAvailableServices.active,0) = 0 THEN isnull(#availableServices.message+'<br>','') + ISNULL('<b class="program-header">'+@headerName+'</b><br>' + #tempAvailableServices.message,dbo.f_translate('Brak opisu',default)) ELSE isnull(#availableServices.message+'<br>','') END,
			active = CASE WHEN #tempAvailableServices.active = 1 THEN 1 ELSE 0 /*#availableServices.active*/ END
		FROM #availableServices
		INNER JOIN #tempAvailableServices on #availableServices.id = #tempAvailableServices.id

		INSERT INTO #availableServices (id, name, active, message, description, icon, variant, start_step_id, pos, programId)
		SELECT id, name, active, CASE WHEN #tempAvailableServices.active = 0 THEN '<b class="program-header">'+@headerName+'</b><br>' + isnull(message,dbo.f_translate('Brak opisu',default)) ELSE null END, '<b class="program-header">'+@headerName+'</b><br>' + isnull(description,dbo.f_translate('Brak opisu',default)), icon, variant, start_step_id, pos, programId FROM #tempAvailableServices WHERE NOT EXISTS (
			SELECT id FROM #availableServices where #availableServices.id = #tempAvailableServices.id
		)
		
		IF @isProducer = 1 AND @platformGroup = dbo.f_translate('CFM',default) AND @eventType = 2
		BEGIN

			update #availableServices set programId = @programId where active = 1 and id < 12
			
		END

		FETCH NEXT FROM kur INTO @programId;
 	END
 	CLOSE kur
 	DEALLOCATE kur


 	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '981,438', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleOwner = value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,429', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @cargoWeight = value_int FROM @values

 	-- opony BL / BAT TRADING z pakietu CFM
 	if ((@arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%') AND @platformGroup = dbo.f_translate('CFM',default) AND @platformId <> 78) /* BYŁO: @rootPlatformId in (43,48))*/
 	OR (ISNULL(@cargoWeight,0) > 0 AND @vehicleOwner = dbo.f_translate('BAT TRADING',default))
 	BEGIN
	 	UPDATE #availableServices set programId = @rootProgramId where programId IS NOT NULL
 	END

 	UPDATE #availableServices set message = '' where active = 1

 	IF EXISTS(select id from #availableServices where start_step_id = 1204.005)
 	BEGIN
		declare @helpdeskProgress INT 
	 	
		select @helpdeskProgress = ssd.progress
	 	from dbo.service_status_dictionary ssd with(nolock)
	 	inner join dbo.service_status ss with(nolock) on ss.status_dictionary_id = ssd.id
	 	inner join dbo.process_instance pin with(nolock) on ss.group_process_id = pin.id
	 	inner join dbo.process_instance root with(nolock) on root.id = pin.root_id
	 	where root.id = @mainProcessId
	 	
	 	if ISNULL(@helpdeskProgress,0) < 2
	 	BEGIN
		 	DELETE FROM #availableServices where start_step_id = 1204.005
	 	END
	 	
	END
 	
 	
	IF @init = 1
	BEGIN
		SELECT * FROM #availableServices ORDER BY pos
	END

	DROP TABLE #tempAvailableServices

	PRINT '------------------ END p_available_services'

END