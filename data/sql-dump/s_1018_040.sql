ALTER PROCEDURE [dbo].[s_1018_040]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessId INT
	DECLARE @content NVARCHAR(MAX)
	DECLARE @driverPhoneNumber NVARCHAR(255)
	DECLARE @attrTable Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @platformId INT
	DECLARE @platformPhone NVARCHAR(100)
	DECLARE @platformName NVARCHAR(100)
	declare @rootId int
	declare @attachment nvarchar(200)

	SELECT @groupProcessId = group_process_id, @rootId=root_id FROM process_instance where id = @previousProcessId
	
	INSERT  @attrTable EXEC dbo.p_attribute_get2
			@attributePath = '80,342,408,197',
			@groupProcessInstanceId = @groupProcessId
	SELECT @driverPhoneNumber = value_string FROM @attrTable
	
	DELETE FROM @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '253',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @platformId = value_int FROM @attrTable
	
	SELECT @platformPhone = number from platform_phone where platform_id = @platformId
	SELECT @platformName = name FROM platform where id = @platformId
	
	SET @content = dbo.f_translate('Informujemy, że organizacja transportu pojazdu jest w toku, skontaktuje się z Państwem nasz przewoźnik',default)
	
	EXEC p_note_new
	@groupProcessId = @groupProcessId,
	@type = dbo.f_translate('sms',default),
	@content = @content,
	@phoneNumber = @driverPhoneNumber,
	@userId = 1,
	@originalUserId = 1,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	declare @driverEmail nvarchar(100)
	declare @partnerEmail nvarchar(200)
	declare @body nvarchar(4000)
	declare @firstname nvarchar(100)
	declare @lastname nvarchar(100)
	declare @VIN nvarchar(100)
	declare @regNumber nvarchar(100)
	declare @country nvarchar(100)
	
	DECLARE @name nvarchar(255)

	EXECUTE dbo.p_platform_group_name
	   @groupProcessInstanceId=@groupProcessId
	  ,@name=@name OUTPUT
	

	declare @partnerLocId int
	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessId
	SELECT @partnerLocId = value_int FROM @attrTable

	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessId
	SELECT @VIN = value_string FROM @attrTable

	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessId
	SELECT @regNumber = value_string FROM @attrTable
		
	declare @caseId nvarchar(100)

	set @caseId=dbo.f_caseId(@groupProcessId)

	declare @subject nvarchar(100)

	-- Realizacja - email, Holowanie
	SELECT	@partnerEmail=dbo.f_partner_contact(@groupProcessId,@partnerLocId,3,4)
	
	declare @partnerServiceId int
	declare @group1021 int
	declare @makeModel int
	declare @makeModelV nvarchar(100)

	select top 1 @group1021=group_process_id 
	from dbo.process_instance where root_id=@rootId and step_id like '1021.%'

	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @group1021
	SELECT @partnerServiceId = value_int FROM @attrTable

	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessId
	SELECT @makeModelV = textD from dbo.dictionary where value = (select top 1 value_int FROM @attrTable) and  typeD like 'makeModel'

	declare @firstNameP nvarchar(100)
	declare @lastnameP nvarchar(100)
		
	declare @IDnumber nvarchar(100)

	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '62,778,64', @groupProcessInstanceId = @groupProcessId
	SELECT @firstNameP = value_string FROM @attrTable

	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '62,778,66', @groupProcessInstanceId = @groupProcessId
	SELECT @lastnameP = value_string FROM @attrTable

	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '62,778,779', @groupProcessInstanceId = @groupProcessId
	SELECT @IDnumber = value_string FROM @attrTable

	DECLARE @senderEmail VARCHAR(200)

	delete from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessId
	SELECT @country = value_string FROM @attrTable
	
	if @name=dbo.f_translate('CFM',default)
	begin
		--select * from dbo.platform where id=25
		
		if @platformId=25
		begin
			set @body='Szanowni Państwo,<br><br>w związku z organizacją transportu przesyłamy upoważnienie niezbędne do odbioru pojazdu '+isnull(@makeModelV,'')+' '+isnull(@regNumber,'')+' przez współpracująca z nami pomoc drogową.<br><br>'
			
			IF @country = dbo.f_translate(dbo.f_translate('Polska',default),default)
	   		BEGIN
		   		SET @attachment = '[upowaznienie_tnp_pl]'
	   		END
	   		ELSE
	   		BEGIN
		   		SET @attachment = '[upowaznienie_tnp_en]'
	   		END
	   		
		end
		else
		begin
			set @body='Szanowni Państwo,<br><br>w związku z organizacją transportu przesyłamy upoważnienie niezbędne do odbioru pojazdu '+isnull(@makeModelV,'')+' '+isnull(@regNumber,'')+' przez współpracująca z nami pomoc drogową.<br><br>
						Osoba upoważniona do odbioru pojazdu to Pan/i '+isnull(@firstNameP,'')+' '+isnull(@lastnameP,'')+dbo.f_translate(' o numberze dowodu ',default)+isnull(@IDnumber,'')
		end
		set @subject=isnull(@caseId,'')+' / '+isnull(@regNumber,'')

		set @senderEmail = [dbo].[f_getEmail]('cfm')
		declare @email nvarchar(200)

		delete from @attrTable
		INSERT @attrTable EXEC p_attribute_get2 @attributePath = '368', @groupProcessInstanceId = @groupProcessId
		SELECT @email = value_string FROM @attrTable



   		IF @platformId = 25
				BEGIN
					declare @contractorMail NVARCHAR(200)
					declare @contractorId int
					delete from @attrTable
					INSERT @attrTable EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessId
					SELECT @contractorId = value_int FROM @attrTable

					SELECT	@contractorMail=dbo.f_partner_contact(@groupProcessId,@contractorId,3,4)

					IF isnull(@email,'') <>''
					BEGIN
						SET @email = @email + ', '+ isnull(@contractorMail,'')
					END
					ELSE
					BEGIN
						SET @email = isnull(@contractorMail,'')
					end
				end



		EXECUTE dbo.p_note_new 
			   @sender=@senderEmail
			  ,@groupProcessId=@groupProcessId
			  ,@type=dbo.f_translate('email',default)
			  ,@content=@subject
			  ,@emailBody=@body
			  ,@phoneNumber=null
			  ,@email=@email
			  ,@userId=@currentUser
			  ,@subType=null
			  ,@subject=@subject
			  ,@emailRegards=1
			  ,@direction=1
			  ,@addInfo=null
			  ,@additionalAttachments = @attachment
			  ,@err=@err OUTPUT
			  ,@message=@message OUTPUT
	end
	else
	begin 

		declare @partnerEmailRs nvarchar(200)

		-- Email RS, Naprawa Warsztatowa
		SELECT	@partnerEmailRs=dbo.f_partner_contact(@groupProcessId,@partnerServiceId,1,8)
	
		declare @partnerEmailSum nvarchar(300)
		set @partnerEmailSum=isnull(@partnerEmail,'')+isnull(','+@partnerEmailRs,'')

		
	
		declare @bodyPartner nvarchar(max)

		SELECT @bodyPartner=body
		FROM   dls.templates
		where	id=5

		declare @partnerId int

		delete from @attrTable
		INSERT @attrTable EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessId
		SELECT @partnerId = value_int FROM @attrTable
		-----------

			declare @partnerLocation nvarchar(1000)
		
			if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
			begin
				set @partnerLocation=[dbo].[f_partnerAdress](@partnerId)
				print dbo.f_translate(dbo.f_translate('Polska',default),default)
				declare @partnerName nvarchar(100)

				select @partnerName=value_string 
				from	dbo.attribute_value 
				where	parent_attribute_value_id=@partnerId and 
						attribute_path ='595,597,84'

				set @partnerLocation=isnull(@partnerName,'')+' '+@partnerLocation

			end
			else
			begin
				declare @towGroupProcessId int
				set @towGroupProcessId=dbo.f_service_top_progress_group(@rootId,1)
			
				declare @serviceName nvarchar(100)
				declare @serviceAddress nvarchar(100)
			
				delete from @attrTable
				INSERT @attrTable EXEC p_attribute_get2 @attributePath = '767,781,84', @groupProcessInstanceId = @towGroupProcessId
				SELECT @serviceName = value_string FROM @attrTable

				delete from @attrTable
				INSERT @attrTable EXEC p_attribute_get2 @attributePath = '767,781,687', @groupProcessInstanceId = @towGroupProcessId
				SELECT @serviceAddress = value_string FROM @attrTable

				set @partnerLocation=isnull(@serviceName,'')+isnull(@serviceAddress,'')
			end

		-----------
		declare @locationString nvarchar(1000)
		EXECUTE dbo.p_location_string 
		   @attributePath='62,591,85'
		  ,@groupProcessInstanceId=@groupProcessId
		  ,@locationString=@locationString OUTPUT

		declare @userName nvarchar(100)
		declare @lineNumber nvarchar(100)

		select @lineNumber=official_line_number
		from dbo.platform
		where id=@platformId

		 select @userName=firstName+' '+lastname
		 from dbo.fos_user
		 where id=@currentUser
		set @bodyPartner=REPLACE(
						 REPLACE(
						 REPLACE(
						 REPLACE(
						 REPLACE(
						 REPLACE(
						 REPLACE(
						 REPLACE(
						 REPLACE(
						 REPLACE(
						 REPLACE(
						  @bodyPartner,
						 '{@74,71@}',isnull(@VIN,'..........................')),
						 '{@74,72@}',isnull(@regNumber,'....................')),
						 '{@62,778,64@}',isnull(@firstNameP,'.....................')),
						 '{@62,778,66@}',isnull(@lastnameP,'............................')),
						 '{@74,73@}',isnull(@makeModelV,'.................................')),
						 '{@62,591,85@}',isnull(@locationString,'.....................................')),
						 '{@62,778,779@}',isnull(@IDnumber,'.....................................')),
						 '{@PS@}',isnull(@partnerLocation,'..................................')),
						 '{@Date@}',isnull(dbo.fn_vdate(getdate()),'..................................')),
						 '{@username@}',isnull(@userName,'..................................')),
						 '{@lineNumber@}',isnull(@lineNumber,'600 222 222'))

		set @subject=dbo.f_translate('Upoważnienie do odbioru auta ',default)+isnull(@caseId,'')

		declare @attributeValueId int

		EXEC [dbo].[p_attribute_edit]
   		@attributePath = '959', 
   		@groupProcessInstanceId = @groupProcessId,
   		@stepId = 'xxx',
   		@userId = 1,
   		@originalUserId = 1,
   		@valueText = @bodyPartner,
   		@err = @err OUTPUT,
   		@message = @message OUTPUT,
   		@attributeValueId = @attributeValueId OUTPUT
	
		declare @fileName nvarchar(100)
		set @fileName=@caseId +' '+@userName 

   		SET @attachment = '{ATTRIBUTE::'+CAST(@attributeValueId AS NVARCHAR(20))+'::'+@fileName+'::true}'
   		
		set @senderEmail = [dbo].[f_getEmail]('callcenter')

		EXECUTE dbo.p_note_new 
			   @sender=@senderEmail
			  ,@groupProcessId=@groupProcessId
			  ,@type=dbo.f_translate('email',default)
			  ,@content=@subject
			  ,@emailBody=@bodyPartner
			  ,@phoneNumber=null
			  ,@email=@partnerEmailSum
			  ,@userId=@currentUser
			  ,@subType=null
			  ,@subject=@subject
			  ,@direction=1
			  ,@addInfo=null
			  ,@additionalAttachments = @attachment
			  ,@err=@err OUTPUT
			  ,@message=@message OUTPUT
	end

DECLARE @invoiceByMillage TINYINT

	DELETE from @attrTable
	INSERT @attrTable EXEC p_attribute_get2 @attributePath = '1078', @groupProcessInstanceId = @groupProcessId
	SELECT @invoiceByMillage = value_int FROM @attrTable


	IF isnull(@invoiceByMillage,0) = 0
		BEGIN
			EXECUTE  dbo.p_send_gop
				@groupProcessInstanceId=@groupProcessId
				,@email=@partnerEmail
				,@final=0
				,@userName=@userName
		END
	

	set @variant=1



	--[NR SPRAWY] Transport pojazdu[ marka;model;nr rejestracyjny] z [adres poczatkowy] do [adres koncowy] umowione na [data i godzina realizacji uslugi]
END