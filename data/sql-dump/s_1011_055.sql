ALTER PROCEDURE [dbo].[s_1011_055]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @policeConfirmation INT
	DECLARE @rootId INT
	DECLARE @previousStepId NVARCHAR(255)
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @postponeCount INT
	DECLARE @callerPhone NVARCHAR(255)
	DECLARE @content NVARCHAR(4000)
	
	SELECT @groupProcessInstanceId = p.group_process_id, @rootId = p.root_id,  @previousStepId = pp.step_id, @postponeCount = p.postpone_count	 
	FROM process_instance p with(nolock) 
	INNER JOIN dbo.process_instance pp with(nolock) ON pp.id = p.previous_process_instance_id 
	WHERE p.id = @previousProcessId 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '542', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @policeConfirmation = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	PRINT '------police'
	PRINT @policeConfirmation
	PRINT '-----postponeCount'
	PRINT @postponeCount

	DECLARE @platformGroup nvarchar(255)

	EXEC p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup OUTPUT


	IF @policeConfirmation = 1 
	BEGIN	
		
		PRINT '---prevStepId'
		PRINT @previousStepId
		
		EXEC [dbo].[p_process_jump]
		@previousProcessId = @previousProcessId,
		@nextStepId = @previousStepId,
--		@doNext = 1,		
		@rootId = @rootId,
		@groupId = @rootId,
	    @err = @err output,
	    @message = @message output
	    
	END 
	ELSE IF @platformGroup = dbo.f_translate('CFM',default)
		BEGIN
			PRINT '---cfm'
-- 			exec p_attribute_edit
-- 					@attributePath = '542',
-- 					@groupProcessInstanceId = @groupProcessInstanceId,
-- 					@stepId = dbo.f_translate('xxx',default),
-- 					@valueInt = 1,
-- 					@err = @err OUTPUT,
-- 					@message = @message OUTPUT

			EXEC [dbo].[p_process_jump]
					@previousProcessId = @previousProcessId,
					@nextStepId = @previousStepId,
					--		@doNext = 1,		
					@rootId = @rootId,
					@groupId = @rootId,
					@err = @err output,
					@message = @message output
			return 
		end
	ELSE
	BEGIN
		SET @variant = 99
		IF @postponeCount = 0
		BEGIN

			PRINT '------variant99'
			
			DECLARE @email_ VARCHAR(200) = [dbo].[f_getEmail]('callcenter')

			SET @content = 'Prosimy o przesłanie kopii zawiadomienia zgłoszenia zdarzeni a na Policję (skan lub zdjęcie) na adres '+@email_+dbo.f_translate(', wpisując w temat maila nasz numer zgłoszenia: ',default)+dbo.f_caseId(@groupProcessInstanceId)
			PRINT '---------content'
			PRINT @content
			EXEC dbo.p_note_new
	   		@groupProcessId = @groupProcessInstanceId,
	   		@type = dbo.f_translate('sms',default),
	   		@content = @content,
	   		@phoneNumber = @callerPhone,
	   		@err = @err OUTPUT,
	   		@message = @message OUTPUT	 
		END
		
		UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(hour, 1, getdate()) where id = @previousProcessId
		
	END
	
END
