

ALTER PROCEDURE [dbo].[s_1018_032]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT 
	DECLARE @stepId NVARCHAR(20)
	DECLARE @partnerId INT
	DECLARE @rootId INT
	DECLARE @parentId INT
	DECLARE @processInstanceId INT
	DECLARE @icsId INT
	DECLARE @status INT
	DECLARE @executorPhone NVARCHAR(255)
	DECLARE @uid NVARCHAR(50)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @platformId INT
	DECLARE @eta DATETIME
	DECLARE @fixingOrTowing INT
	DECLARE @platformName NVARCHAR(200)
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @p1 VARCHAR(255)
	DECLARE @p3 VARCHAR(255)
	DECLARE @etaText NVARCHAR(300)
	DECLARE @isParking INT
	DECLARE @onTime TINYINT
	
	SELECT @groupProcessInstanceId = p.group_process_id, 
	@rootId = p.root_id,
	@parentId = p.parent_id
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s on s.id = p.step_id
	WHERE p.id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
		
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '253',
		@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '80,342,408,197',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DELETE FROM @values

	declare @repairDate datetime

--	INSERT @values EXEC p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @groupProcessInstanceId
--	SELECT @repairDate = value_date FROM @values
--	DELETE FROM @values
--		
	exec [dbo].[p_fixing_end_date] 
	@rootId = @rootId, 
	@fixingEndDate = @repairDate output

	
	DECLARE @name nvarchar(255)

	EXECUTE dbo.p_platform_group_name
	   @groupProcessInstanceId=@groupProcessInstanceId
	  ,@name=@name OUTPUT
	
	if @name=dbo.f_translate('CFM',default)
	begin
		declare @isOK int
		declare @date datetime

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '280', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @isOK = value_int FROM @values
		
		if @isOK=1
		begin
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '130', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @date = value_date FROM @values

			declare @1021Id int
					
			select @1021Id=group_process_id
			from dbo.process_instance 
			where step_id like '1021.%' and root_id=@rootId

			EXEC p_attribute_edit
				@attributePath = '129,130', 
				@groupProcessInstanceId = @1021Id,
				@stepId = 'xxx',
				@valueDate = @date,
				@err = @err OUTPUT,
				@message = @message OUTPUT

			EXEC p_attribute_edit
				@attributePath = '129,798', 
				@groupProcessInstanceId = @1021Id,
				@stepId = 'xxx',
				@valueInt = 0,
				@err = @err OUTPUT,
				@message = @message OUTPUT
		end

	end 
	else
	begin
		if @repairDate is null --isnull(@repairDate,getdate()+10)>getdate()
		begin
			set @content=dbo.f_translate('Informujemy, że nie mamy jeszcze potwierdzonej daty zakończenia naprawy pojazdu w związku z czym przystąpimy do organizacji odbioru samochodu w momencie kiedy potwierdzimy orientacyjną datę zakończenia naprawy warsztatowej',default)
			EXEC dbo.p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @callerPhone,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		end
	end
	
	SET @variant = 1
	
END


