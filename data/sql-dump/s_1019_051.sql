ALTER PROCEDURE [dbo].[s_1019_051]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err TINYINT
	DECLARE @message NVARCHAR(255)
	DECLARE @caseId INT
	DECLARE @groupProcessInstanceId INT
	
	SELECT @groupProcessInstanceId = group_process_id FROM process_instance where id = @previousProcessId
	DECLARE @isSupervisorAgree int
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	declare @driverEmail nvarchar(200)
	declare @body nvarchar(4000)
	declare @firstname nvarchar(100)
	declare @lastname nvarchar(100)
	declare @VIN nvarchar(100)
	declare @regNumber nvarchar(100)
	declare @partnerLocId int
	declare @platformId int

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,64', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @firstname = value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,66', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @lastname = value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VIN = value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,368', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @driverEmail = value_string FROM @values


	declare @subject nvarchar(100)

	DECLARE @domain_ VARCHAR(100) = [dbo].[f_getDomain]()
	
	set @subject=dbo.f_translate('Umowa pożyczki ',default)+isnull(@VIN,'')+' '+isnull(@regNumber,'')
	set @body='Szanowni Państwo<BR>
				uprzejmie prosimy o wypełnienie załączonej umowy pożyczki. <BR>
				Wypełniony dokument prosimy odesłać na adres email: '+@domain_+' lub numer faksu +48 61 83 19 850.<BR> 
				Dodatkowo prosimy o załączenie kopii dowodu osobistego.<BR><BR>
				Pozdrawiamy<BR>
				Zespół Starter24'

	declare @agreement nvarchar(100)
	if @platformId=31
	begin
		set @agreement='[pozyczka_audi_wzor_umowy]'
	end
	if @platformId=11
	begin
		set @agreement='[pozyczka_vw_wzor_umowy]'
	end

	
	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')

	EXECUTE dbo.p_note_new 
		 @sender=@senderEmail
		,@groupProcessId=@groupProcessInstanceId
		,@type=dbo.f_translate('email',default)
		,@content=@body
		,@phoneNumber=null
		,@email=@driverEmail
		,@userId=@currentUser
		,@subType=null
		,@attachment=null
		,@subject=@subject
		,@direction=1
		,@addInfo=null
		,@additionalAttachments=@agreement
		,@err=@err OUTPUT
		,@message=@message OUTPUT
END

