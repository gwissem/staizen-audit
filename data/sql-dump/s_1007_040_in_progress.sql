ALTER PROCEDURE [dbo].[s_1007_040_in_progress]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @fixingCarEndDate DATETIME
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @postponeCount DATETIME
	DECLARE @postponeLimit INT
	DECLARE @supervisorDecision INT
	DECLARE @programRuleDate DATETIME
	DECLARE @platformId INT
	DECLARE @partnerWorkshopId INT
	DECLARE @opened INT
	DECLARE @checkDate DATETIME
	DECLARE @towingFixingGroup INT
	DECLARE @nextProcessInstanceId INT
	DECLARE @processInstanceIds NVARCHAR(500)
	DECLARE @serviceReportGroupId INT
	DECLARE @c NVARCHAR(300)
	DECLARE @p1 NVARCHAR(100)
	DECLARE @eventLocationCountry NVARCHAR(200)
	DECLARE @replacementCarPartner INT
	DECLARE @eta DATETIME
	DECLARE @fixingPartnerId INT
	DECLARE @regNumber NVARCHAR(255)
	DECLARE @vin NVARCHAR(255)
	DECLARE @content NVARCHAR(4000)
	DECLARE @smsContent NVARCHAR(4000)
	DECLARE @subject NVARCHAR(1000)
	DECLARE @partnerPhone NVARCHAR(255)
	DECLARE @partnerEmail NVARCHAR(255)
	DECLARE @callerPhone NVARCHAR(255)
	DECLARE @hashUrl NVARCHAR(255)
	DECLARE @hash NVARCHAR(255)
	
	SET @variant = 99
	
	SELECT @groupProcessInstanceId = p.group_process_id, @rootId = p.root_id, @postponeCount = p.postpone_count, @postponeLimit = s.postpone_count	
	FROM process_instance p WITH(NOLOCK)
	INNER JOIN step s with(nolock) ON s.id = p.step_id
	where p.id = @previousProcessId
	
	SELECT TOP 1 @serviceReportGroupId = group_process_id, @rootId = root_id FROM dbo.process_instance with(nolock) WHERE active = 1 AND step_id LIKE '1021.%' AND root_id = @rootId
	
	IF @postponeCount = 0
	BEGIN
		EXEC p_form_controls @instance_id = @previousProcessId, @returnResults = 0
	END 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventLocationCountry = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '283,285', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
	
	
	
	IF ISNULL(@eventLocationCountry,'') <> dbo.f_translate('Polska',default)
	BEGIN		
		DECLARE @rsaPartner INT
		DECLARE @rsaPartnerId INT
		-- TODO: GET RSA PARTNER
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '837,773', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @replacementCarPartner = id FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @rsaPartnerId = value_int FROM @values
		
		IF @rsaPartnerId IS NOT NULL
		BEGIN
	
			IF @eta IS NULL
			BEGIN
				SET @eta = DATEADD(HOUR, 2, GETDATE())
				
				EXEC [dbo].[p_attribute_edit]
			     @attributePath = '540', 
			     @groupProcessInstanceId = @groupProcessInstanceId,
			     @stepId = 'xxx',
			     @userId = 1,
			     @originalUserId = 1,
			     @valueDate = @eta,
			     @err = @err OUTPUT,
			     @message = @message OUTPUT
				
			END 
			
			SELECT @rsaPartner = partner.id
			FROM dbo.attribute_value partner with(nolock)
			INNER JOIN dbo.attribute_value partnerLocation with(nolock) ON partnerLocation.parent_attribute_value_id = partner.id AND partnerLocation.attribute_path = '767,773,704'
			WHERE partner.root_process_instance_id = @rootId 
			AND partner.attribute_path = '767,773'
			AND partnerLocation.value_int IS NOT NULL
			
			EXEC [dbo].[P_attribute_copy_structure]
			@source_attribute_value_id = @rsaPartner,
			@target_attribute_value_id = @replacementCarPartner,
			@process_instance_id = @previousProcessId
	
			SET @variant = 3
		END 
		ELSE
		BEGIN
			SET @variant = 99
			UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(MINUTE, 1, GETDATE()) where id = @previousProcessId
		END 
	END 
	ELSE
	BEGIN
		
		INSERT @values EXEC p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @rootId
		SELECT @fixingCarEndDate = value_date FROM @values
			
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '740,123', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @supervisorDecision = value_int FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @platformId = value_int FROM @values
		
		IF @platformId = 31
		BEGIN
			SET @programRuleDate = GETDATE()
		END 
		ELSE IF @platformId = 11
		BEGIN
			SET @programRuleDate = DATEADD(HOUR, 2, GETDATE())
		END 
		ELSE IF @platformId = 6
		BEGIN
			SET @programRuleDate = DATEADD(HOUR, 4 , GETDATE())
		END 
		
		SELECT TOP 1 @towingFixingGroup = group_process_id FROM dbo.process_instance with(nolock) WHERE root_id = @rootId AND step_id LIKE '1009.%' order by id desc
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towingFixingGroup
		SELECT @fixingPartnerId = value_int FROM @values
		
		SELECT @checkDate = DATEADD(HOUR,2,GETDATE())
		
		EXEC [dbo].[p_is_partner_workshop_opened]
		@groupProcessInstanceId = @towingFixingGroup,
		@checkDate = @checkDate,
		@opened = @opened OUTPUT
		
		IF @opened = 0 OR dbo.f_diagnosis_code(@groupProcessInstanceId) = '1464391'
		BEGIN
			SET @variant = 1
		END 
		ELSE
		BEGIN		
			declare @monitId int			
			select top 1 @monitId = id from dbo.process_instance with(nolock) WHERE root_id = @rootId and step_id = '1021.015' and active = 1 
			
			IF @postponeCount = 0 AND @fixingCarEndDate IS NULL AND @serviceReportGroupId IS NOT NULL AND @monitId IS NULL
			BEGIN
			   
			   EXEC [dbo].[p_process_new] 
				@stepId = '1021.015', 
				@userId = 1, 
				@originalUserId = 1,
				@parentProcessId = @rootId, 
				@rootId = @rootId,
				@groupProcessId = @serviceReportGroupId,
				@err = @err OUTPUT, 
				@message = @message OUTPUT, 
				@processInstanceId = @nextProcessInstanceId OUTPUT
		
		       SET @c = dbo.f_translate('Brak daty potencjalnej daty zakończenia naprawy. Wygenerowanie zadania 1021.015 dla konsultanta ',default)
		       
			END 
			
			IF @fixingCarEndDate IS NULL AND @supervisorDecision IS NULL
			BEGIN
				-- nasłuchiwanie na odp. z serwisu, lub decyzję KZ
				SET @variant = 99
				
				IF @postponeCount IN (0,6,12)
				BEGIN
					
					select @partnerEmail=dbo.f_partner_contact(@groupProcessInstanceId,@fixingPartnerId,1,8)
					select @partnerPhone=dbo.f_partner_contact_mobile(@fixingPartnerId,1,2)
					
					SELECT TOP 1 @hash = token from dbo.process_instance with(nolock) WHERE root_id = @rootId and step_id like '1021.%' and token is not null
					SELECT @hashUrl = dbo.f_hashTaskUrl(@hash)
					
					DELETE FROM @values
					INSERT @values EXEC value_string @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @regNumber = value_string FROM @values
					
					DELETE FROM @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @vin = value_string FROM @values
					
					DELETE FROM @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @callerPhone = value_string FROM @values
					
					IF @postponeCount = 0 
					BEGIN
						-- info do klienta
						SET @smsContent = dbo.f_translate('Uprzejmie informujemy, że aktualnie oczekujemy na informację z warsztatu dotyczącą diagnozy pojazdu. Po jej uzyskaniu, jeśli spełnione zostaną warunki assistance, przystąpimy do organizacji samochodu zastępczego. W przypadku pytań dotyczą',default)+@partnerPhone
						
						EXEC dbo.p_note_new
						@groupProcessId = @groupProcessInstanceId,
						@type = dbo.f_translate('sms',default),
						@content = @smsContent,
						@phoneNumber = @callerPhone,
						@err = @err OUTPUT,
						@message = @message OUTPUT
						
						SET @subject = dbo.f_translate('Prośba o pilną aktualizację naprawy warsztatowej dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
						SET @content = 'Szanowni Państwo!<br/>Prosimy o pilną aktualizację naprawy warsztatowej dotyczącej przetransportowanego do Państwa samochodu o numerze rej. '+ISNULL(@regNumber,'')+dbo.f_translate(' oraz VIN ',default)+ISNULL(@vin,'')+dbo.f_translate(', w związku z prośbą Klienta o wynajęcie samochodu zastępczego. Odnośnik do formularza: ',default)+@hashUrl
						
					END 
					ELSE IF @postponeCount = 6
					BEGIN
						-- monit do ASO 1
						SET @subject = dbo.f_translate('Ponowna prośba o pilną aktualizację naprawy warsztatowej dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
						SET @content = 'Szanowni Państwo!<br/>Prosimy o pilną aktualizację naprawy warsztatowej dotyczącej przetransportowanego do Państwa samochodu o numerze rej. '+ISNULL(@regNumber,'')+dbo.f_translate(' oraz VIN ',default)+ISNULL(@vin,'')+dbo.f_translate(', w związku z prośbą Klienta o wynajęcie samochodu zastępczego. Odnośnik do formularza: ',default)+@hashUrl												
					END 
					ELSE IF @postponeCount = 12
					BEGIN
						-- monit do ASO 2
						SET @subject = dbo.f_translate('Ponowna prośba o pilną aktualizację naprawy warsztatowej dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
						SET @content = 'Szanowni Państwo!<br/>Prosimy o pilną aktualizację naprawy warsztatowej dotyczącej przetransportowanego do Państwa samochodu o numerze rej. '+ISNULL(@regNumber,'')+dbo.f_translate(' oraz VIN ',default)+ISNULL(@vin,'')+dbo.f_translate(', w związku z prośbą Klienta o wynajęcie samochodu zastępczego. Odnośnik do formularza: ',default)+@hashUrl						
						IF @partnerPhone is not NULL
					    BEGIN
						    SET @smsContent = 'Szanowni Państwo! Prosimy o pilną aktualizację naprawy warsztatowej dotyczącej przetransportowanego do Państwa samochodu o numerze rej. '+ISNULL(@regNumber,'')+dbo.f_translate(' oraz VIN ',default)+ISNULL(@vin,'')+dbo.f_translate(', w związku z prośbą Klienta o wynajęcie samochodu zastępczego. Odnośnik do formularza: ',default)+@hashUrl
						    
						    EXEC dbo.p_note_new
							@groupProcessId = @groupProcessInstanceId,
							@type = dbo.f_translate('sms',default),
							@content = @smsContent,
							@phoneNumber = @partnerPhone,
							@err = @err OUTPUT,
							@message = @message OUTPUT
					    END 
					END 
					
					IF @content is not null
					BEGIN						
					
						DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rentals')
						
						EXECUTE dbo.p_note_new 
					     @groupProcessId = @groupProcessInstanceId
					    ,@type = dbo.f_translate('email',default)
					    ,@content = @content
					    ,@email = @partnerEmail
					    ,@userId = 1  -- automat
					    ,@subject = @subject
					    ,@direction=1
					    ,@dw = ''
					    ,@udw = ''
					    ,@sender = @senderEmail
					    ,@emailBody = @content
					    ,@emailRegards = 1
					    ,@err=@err OUTPUT
					    ,@message=@message OUTPUT					    
					END 
				    
				END
				
				UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(MINUTE, 5, GETDATE()) where id = @previousProcessId
				
			END 
			ELSE IF @fixingCarEndDate > @programRuleDate OR @supervisorDecision = 1
			BEGIN
				-- start 
				SET @variant = 1
				SET @c = dbo.f_translate('Uzupełniona data naprawy warsztatowej pozwala na wynajem auta zastępczego. Wygenerowanie zadania 1007.003 dla konsultanta',default)
			END 
			ELSE IF @supervisorDecision = 0 AND @fixingCarEndDate IS NULL
			BEGIN
				-- stop, odblokowanie będzie możliwe tylko manualnie
				SET @variant = 99
				UPDATE dbo.process_instance SET user_id = 1 where id = @previousProcessId
			END 
			ELSE IF @fixingCarEndDate < @programRuleDate
			BEGIN
				SET @variant = 2
				SET @c = dbo.f_translate('Uzupełniona data naprawy warsztatowej nie pozwala na wynajem auta zastępczego. Wygenerowanie zadania 1007.063 dla konsultanta',default)
			END 
		END		
		
		IF @c IS NOT NULL
		BEGIN
			-- LOGGER START
			SET @p1 = dbo.f_caseId(@groupProcessInstanceId)
			EXEC dbo.p_log_automat
			@name = 'rental_automat',
			@content = @c,
			@param1 = @p1
			-- LOGGER END
		END
		
	END 
	
	EXEC dbo.p_change_priority @priority = 1, @type = 'rental_fixing_date', @groupProcessInstanceId = @groupProcessInstanceId
	
END


