ALTER PROCEDURE [dbo].[s_1009_036]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT 
	DECLARE @stepId NVARCHAR(20)
	DECLARE @partnerId INT
	DECLARE @rootId INT
	DECLARE @parentId INT
	DECLARE @processInstanceId INT
	DECLARE @icsId INT
	DECLARE @status INT
	DECLARE @executorPhone NVARCHAR(255)
	DECLARE @uid NVARCHAR(50)
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @platformId INT
	DECLARE @platformName NVARCHAR(100)
	DECLARE @startSmsSend INT
	DECLARE @processInstanceIds NVARCHAR(200)
	DECLARE @nextProcessInstanceId INT
	DECLARE @statusDate DATETIME
	DECLARE @reportId INT
	DECLARE @vehicleReportId INT
	DECLARE @closeReportId INT
	DECLARE @makeModel INT
	DECLARE @reportBrand NVARCHAR(255)
	DECLARE @reportModel NVARCHAR(255)
	DECLARE @reportComment NVARCHAR(4000)
	DECLARE @reportVin NVARCHAR(255)
	DECLARE @reportRegNumber NVARCHAR(255)
	DECLARE @reportMileage NVARCHAR(255)
	DECLARE @reportDmc INT
	DECLARE @code NVARCHAR(50)
	DECLARE @sqlQuery NVARCHAR(MAX)
	DECLARE @customDate DATETIME
	DECLARE @eta DATETIME
	DECLARE @reason NVARCHAR(255)
	DECLARE @executorId INT
	DECLARE @reasonId INT
	DECLARE @postponeCount INT
	DECLARE @fixingOrTowing INT
	
	SET @variant = 99
	
	IF OBJECT_ID('tempdb..#tempNoteTable') IS NOT NULL
	BEGIN
		DROP TABLE #tempNoteTable
	END
	
	CREATE TABLE #tempNoteTable (
		id INT,
		parent_attribute_value_id INT,
		attribute_path NVARCHAR(50),
		value_int INT,
		value_string NVARCHAR(255), 
		value_text NVARCHAR(MAX), 
		value_date DATETIME, 
		value_decimal DECIMAL(18,5),
		created_at DATETIME,
		created_by_id INT,
		created_by_original_id INT,
		updated_at DATETIME,
		updated_by_id INT,
		updated_by_original_id INT
	)
				
	SELECT @groupProcessInstanceId = p.group_process_id, 
	@postponeCount = p.postpone_count,
	@rootId = p.root_id,
	@parentId = p.parent_id
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s with(nolock) on s.id = p.step_id
	WHERE p.id = @previousProcessId 
	
	IF @postponeCount = 0
	BEGIN
		EXEC [dbo].[p_form_controls]
		@instance_id = @previousProcessId,
		@returnResults = 0
	END 
	
	-- zablokowanie instancji na czas jej wykonywania
	UPDATE dbo.process_instance SET user_id = 1 WHERE id = @previousProcessId

	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '610',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @executorId = value_int FROM @values
	
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '560',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	
	DELETE FROM @values	
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '609',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '253',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	SELECT @platformName = name FROM dbo.platform with(nolock) WHERE id = @platformId
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '80,342,408,197',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '616',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @uid = value_string FROM @values

	
	SET @sqlQuery = 'select * from OPENQUERY(teka2, ''select s.id, IFNULL(o.eta,o.pre_eta), s.status_id, o.id order_id, s.created_at, s.reason FROM StarterTeka.order_statuses s 
					 inner join StarterTeka.orders o ON o.id = s.order_id where o.id = '''''+CAST(@icsId AS NVARCHAR(10))+'''''   '') a'
		
	declare @statuses as table (
		id int, 
		eta DATETIME,
		status_id INT, 
		order_id INT, 
		created_at DATETIME,
		reason NVARCHAR(255)
	)

	delete from @statuses
	insert into @statuses
	exec (@sqlQuery)
	
	SELECT top 1 @status = status_id, @eta = eta, @statusDate = created_at, @reason = reason FROM @statuses ORDER by id desc 
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
	@attributePath = '638',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @closeReportId = id FROM @values

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
	@attributePath = '638,223',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@parentAttributeId = @closeReportId
	SELECT @vehicleReportId = id FROM @values
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '406,226,615',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @startSmsSend = count(id) FROM @values WHERE value_string = dbo.f_translate('partnerStarted',default)
	
	
	-- kontraktor anulował 
	IF @status = 10 
	BEGIN
		
		DECLARE @cancelId INT
		SELECT @cancelId = id FROM dbo.process_instance with(nolock) WHERE step_id = '1009.052' AND group_process_id = @groupProcessInstanceId and active = 1
		
		IF @cancelId IS NULL
		BEGIN
			EXEC [dbo].[p_process_new] 
			@stepId = '1009.052', 
			@userId = 1, 
			@originalUserId = 1, 
			@previousProcessId = @previousProcessId, 
			@parentProcessId = @rootId, 
			@rootId = @rootId,
			@groupProcessId = @groupProcessInstanceId,
			@err = @err OUTPUT, 
			@message = @message OUTPUT, 
			@processInstanceId = @nextProcessInstanceId OUTPUT
			
--			SELECT @reasonId = value FROM dictionary where typeD = 'partnerReason' AND textD = @reason 
			
			EXEC p_add_service_refuse_reason
			@groupProcessInstanceId = @groupProcessInstanceId,
			@partnerLocationId = @executorId,
			@reason = @reason
		END
	END 
	-- wystartowanie kontraktora
	IF @status = 4 AND @startSmsSend = 0
	BEGIN
	
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '107', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@userId = 1,
		@originalUserId = 1,
		@valueDate = @eta,
		@err = @err OUTPUT,
		@message = @message OUTPUT
				
		DECLARE @content NVARCHAR(MAX)
		SET @content = 'Mozesz sprawdzic, gdzie aktualnie znajduje sie Twoja pomoc drogowa. Aby przejsc do mapy kliknij w link: http://teka.starter24.pl/Sprawa.php?uid='+@uid
	
		EXEC p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @callerPhone,
		@subType = dbo.f_translate('partnerStarted',default),
		@err = @err OUTPUT,
		@message = @message OUTPUT
			
	END 
	
	DECLARE @activeInstanceId INT
	DECLARE @activeStepId NVARCHAR(20)
	SELECT @activeInstanceId = id, @activeStepId = step_id 
	FROM process_instance 
	WHERE group_process_id = @groupProcessInstanceId 
	AND active = 1 
	AND step_id IN ('1009.004', '1009.005', '1009.006', '1009.051', '1009.049')
	
	
	-- przedłuż o eta
	IF @activeStepId = '1009.004'
	BEGIN
	
		DECLARE @postponeDate DATETIME
		
		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '540',
		@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @customDate = value_date FROM @values
	
		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '107',
		@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @eta = value_date FROM @values
	
		SET @postponeDate = ISNULL(@customDate,@eta)
		IF @postponeDate IS NOT NULL
		BEGIN
			DECLARE @contractorReportCarId INT
			DECLARE @carId INT
			
			IF @vehicleReportId IS NULL
			BEGIN
				DECLARE @makeModelId INT
				DECLARE @vin NVARCHAR(50)
				DECLARE @regNumber NVARCHAR(50)
				
				EXEC [dbo].[p_form_controls]
				@instance_id = @activeInstanceId,
				@returnResults = 0

				SELECT @vehicleReportId = id FROM dbo.attribute_value with(nolock) where attribute_path = '638,223' and group_process_instance_id = @groupProcessInstanceId
				
				DELETE FROM @values
				INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @vin = value_string FROM @values
	
				DELETE FROM @values
				INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @regNumber = value_string FROM @values
				
				DELETE FROM @values
				INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @makeModelId = value_int FROM @values
				
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '638,223,71', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = @activeStepId,
				@parentAttributeValueId = @vehicleReportId,
				@userId = 1,
				@originalUserId = 1,
				@valueString = @vin,
				@err = @err OUTPUT,
				@message = @message OUTPUT
	
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '638,223,72', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = @activeStepId,
				@parentAttributeValueId = @vehicleReportId,
				@userId = 1,
				@originalUserId = 1,
				@valueString = @regNumber,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '638,223,73', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = @activeStepId,
				@parentAttributeValueId = @vehicleReportId,
				@userId = 1,
				@originalUserId = 1,
				@valueInt = @makeModelId,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				
				
			END 
			
			UPDATE dbo.process_instance SET postpone_date = DATEADD(MINUTE, 12, @postponeDate) where id = @activeInstanceId AND postpone_count = 0
			
		END 
		
	END 
	
	-- kontraktor na miejscu
	IF @status IN (5,6,7,9)
	BEGIN
		
		IF @activeStepId = '1009.004' 
		BEGIN
			
			EXEC [dbo].[p_form_controls]
			@instance_id = @activeInstanceId,
			@returnResults = 0
			
			DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '638',
			@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @closeReportId = id FROM @values
		
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '208', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @activeStepId,
			@userId = 1,
			@originalUserId = 1,
			@valueInt = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '638,214', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @activeStepId,
			@parentAttributeValueId = @closeReportId,
			@userId = 1,
			@originalUserId = 1,
			@valueDate = @statusDate,
			@err = @err OUTPUT,
			@message = @message OUTPUT
				
			DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '638,223',
			@groupProcessInstanceId = @groupProcessInstanceId,
			@parentAttributeId = @closeReportId
			SELECT @vehicleReportId = id FROM @values
			
			DECLARE @onPlaceVariant INT
			
			SET @onPlaceVariant = 1 
			IF @status = 5
			BEGIN
				SET @onPlaceVariant = 2
			END 
			
			
			UPDATE dbo.process_instance set active = 0 where group_process_id = @groupProcessInstanceId and step_id = '1009.051' and active = 1
			
			EXEC [dbo].[p_process_next]
			@previousProcessId = @activeInstanceId,
			@userId = 1,
			@originalUserId = 1,
			@err = @err OUTPUT,
			@variant = @onPlaceVariant,
			@message = @message OUTPUT,
			@processInstanceIds = @processInstanceIds OUTPUT
			
			SET @nextProcessInstanceId = CAST(@processInstanceIds as INT)	
			
		END 
		ELSE IF @activeStepId = '1009.051'
		BEGIN
		
			EXEC [dbo].[p_process_next]
			@previousProcessId = @activeInstanceId,
			@userId = 1,
			@originalUserId = 1,
			@err = @err OUTPUT,
			@variant = 1,
			@message = @message OUTPUT,
			@processInstanceIds = @processInstanceIds OUTPUT
			
			SET @nextProcessInstanceId = CAST(@processInstanceIds as INT)
		END 
		ELSE IF @activeStepId = '1009.049'
		BEGIN
			
			EXEC [dbo].[p_process_next]
			@previousProcessId = @activeInstanceId,
			@userId = 1,
			@originalUserId = 1,
			@err = @err OUTPUT,
			@variant = 4,
			@message = @message OUTPUT,
			@processInstanceIds = @processInstanceIds OUTPUT
			
			SET @nextProcessInstanceId = CAST(@processInstanceIds as INT)
		END 
		ELSE IF @activeStepId = '1009.005' AND @fixingOrTowing = 2 and @status = 6	
		BEGIN
	
			EXEC [dbo].[p_process_next]
			@previousProcessId = @activeInstanceId,
			@userId = 1,
			@originalUserId = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT,
			@processInstanceIds = @processInstanceIds OUTPUT
			
			SET @nextProcessInstanceId = CAST(@processInstanceIds as INT)
			
		END 
		
	END 
	
	IF @status IN (5,7) -- or @rootId=45512
	BEGIN
		SET @sqlQuery = 'select * from OPENQUERY(teka2, ''select id, order_id, brand, model, registration_number, mileage, dmc, vin, comment FROM StarterTeka.reports where order_id = '''''+CAST(@icsId AS NVARCHAR(10))+'''''   '') a'
		declare @reports as table (
			id int, 
			order_id INT, 
			brand nvarchar(255),
			model nvarchar(255),
			registration_number nvarchar(255),
			mileage int,
			dmc int,
			vin nvarchar(255),
			comment nvarchar(max)
		)
	
		insert into @reports
		exec (@sqlQuery)
		
		select @reportId = MAX(id), @reportDmc = MAX(dmc), @reportMileage = MAX(mileage), @reportVin = MAX(vin), @reportRegNumber = MAX(registration_number), @reportComment = MAX(comment), @reportBrand = MAX(brand), @reportModel = MAX(model)
		FROM @reports
		
		IF @reportId IS NOT NULL
		BEGIN
			
			DECLARE @reportMakeModel NVARCHAR(255)
			SELECT @reportMakeModel = @reportBrand + ' ' + @reportModel
			
			select @reportMakeModel=dbo.GROUP_CONCAT_D(data,' ') from (
			select id,data from dbo.f_split(@reportBrand,' ')
			union
			select id,data from dbo.f_split(@reportModel,' ')
			
			) a 

			--select @reportMakeModel
			SELECT @makeModel = dbo.f_makeModelStandard(@reportMakeModel, 1)
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '638,223,71', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @activeStepId,
			@parentAttributeValueId = @vehicleReportId,
			@userId = 1,
			@originalUserId = 1,
			@valueString = @reportVin,
			@err = @err OUTPUT,
			@message = @message OUTPUT

			EXEC [dbo].[p_attribute_edit]
			@attributePath = '638,223,72', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @activeStepId,
			@parentAttributeValueId = @vehicleReportId,
			@userId = 1,
			@originalUserId = 1,
			@valueString = @reportRegNumber,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '638,223,73', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @activeStepId,
			@parentAttributeValueId = @vehicleReportId,
			@userId = 1,
			@originalUserId = 1,
			@valueInt = @makeModel,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '638,223,75', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @activeStepId,
			@parentAttributeValueId = @vehicleReportId,
			@userId = 1,
			@originalUserId = 1,
			@valueInt = @reportMileage,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '638,223,76', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @activeStepId,
			@parentAttributeValueId = @vehicleReportId,
			@userId = 1,
			@originalUserId = 1,
			@valueInt = @reportDmc,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '638,223,380', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @activeStepId,
			@parentAttributeValueId = @vehicleReportId,
			@userId = 1,
			@originalUserId = 1,
			@valueText = @reportComment,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			
			
		END 
	END
		
	
	
		-- kontraktor rozpoczął holowanie
--	IF @status = 6 AND @activeStepId = '1009.004'
--	BEGIN
--			
--		EXEC [dbo].[p_process_next]
--			@previousProcessId = @activeInstanceId,
--			@userId = 1,
--			@variant = 2,
--			@originalUserId = 1,
--			@err = @err OUTPUT,
--			@message = @message OUTPUT,
--			@processInstanceIds = @processInstanceIds OUTPUT
--			
--		SET @nextProcessInstanceId = CAST(@processInstanceIds as INT)
--		UPDATE process_instance SET created_at = @statusDate WHERE id = @nextProcessInstanceId
--		
--	END 
	
	-- raport zamknięcia
	IF (@status = 7 AND @activeStepId IN ('1009.004', '1009.005', '1009.006') ) or @rootId=46778
	BEGIN
		IF @activeStepId <> '1009.006'
		BEGIN
			EXEC [dbo].[p_process_next]
			@previousProcessId = @activeInstanceId,
			@userId = 1,
			@originalUserId = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT,
			@processInstanceIds = @processInstanceIds OUTPUT
			
			SET @activeInstanceId = CAST(@processInstanceIds AS INT)
			SET @activeStepId = '1009.006'
		END
		ELSE
		BEGIN
			SET @variant = 0
		END 
		
	END
		
	IF (@fixingOrTowing = 2 AND @activeStepId = '1009.006') OR (@fixingOrTowing = 1 AND EXISTS(SELECT id from dbo.process_instance where step_id = '1009.074' and active = 0 and group_process_id = @groupProcessInstanceId))
	BEGIN
--		EXEC [dbo].[p_process_next]
--		@previousProcessId = @activeInstanceId,
--		@userId = 1,
--		@originalUserId = 1,
--		@err = @err OUTPUT,
--		@message = @message OUTPUT,
--		@processInstanceIds = @processInstanceIds OUTPUT
		
--		UPDATE process_instance SET active = 0 WHERE id = @previousProcessId
		
		SET @variant = 0
	END 
	
	
	IF OBJECT_ID('tempdb..#tempNoteTable') IS NOT NULL
	BEGIN
		DROP TABLE #tempNoteTable
	END
	
	UPDATE dbo.process_instance SET user_id = NULL, updated_at = GETDATE(), postpone_date = dateadd(second, 30, getdate()) WHERE id = @previousProcessId
	
	PRINT '---active'
	PRINT @activeStepId
END


