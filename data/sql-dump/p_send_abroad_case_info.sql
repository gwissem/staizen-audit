ALTER PROCEDURE p_send_abroad_case_info
    @groupProcessId int,
    @serviceId      int
AS
  BEGIN


    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))

    DECLARE @content nvarchar(4000)
    DECLARE @subject nvarchar(500)
    DECLARE @mailto nvarchar(500)
    DECLARE @rootId int
    DECLARE @platformName nvarchar(100)
    DECLARE @platformId int
    DECLARE @err int
    DECLARE @message nvarchar(100)
    DECLARE @serviceName nvarchar(4000)
    DECLARE @starterEmail nvarchar(200)
    DECLARE @country NVARCHAR(100) = null
    DECLARE @alreadySent tinyint = 0


    SELECT @rootId = root_id from process_instance where id = @groupProcessId

    DECLARE @createdAt DATETIME
    SELECT @createdAt = created_at from process_instance where id = @rootId




    INSERT @values
        EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                              @attributePath = '253'
    SELECT @platformId = value_int FROM @values
    SELECT @platformName = name FROM AtlasDB_def.dbo.platform where id = @platformId


    IF @platformId in (25,43,48,53)
      BEGIN

        IF isnull(@serviceId, 0) <> 0
          BEGIN
            SELECT @serviceName = name
            from AtlasDB_def.dbo.service_definition_translation
            where translatable_id = @serviceId
              and locale = 'pl'
          end


        SET @subject = dbo.f_translate('Sprawa zagraniczna ',default) + isnull(@platformName, '') + ' ' + dbo.f_caseId(@groupProcessId)
        SET @content = dbo.f_translate('Nowa sprawa zagraniczna, zorganizowano ',default) + isnull(@serviceName, '')


        DECLARE @subType nvarchar(100)
        DELETE from @values
        INSERT @values
            EXEC p_attribute_get2 @rootProcessInstanceId = @rootId, @attributePath = '514'

        SELECT @subType = value_string from @values

        IF isnull(@subType, '') = 'kz_abroad'
          BEGIN
            SET @alreadySent = 1
          end
        ELSE
          BEGIN
            SET @alreadySent = 0
          END

        DELETE from @values

        INSERT @values
            EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                                  @attributePath = '101,85,86'
        SELECT @country = value_string FROM @values


        DECLARE @datediff int

        SELECT @datediff = DATEDIFF(hour ,'2019-02-20',getdate())

        IF isnull(@country, dbo.f_translate('Polska',default)) <> dbo.f_translate('Polska',default) and isnull(@alreadySent, 0) = 0 and @serviceId in (1, 2, 3) and @datediff  >=0
          BEGIN

            SET @mailto = dbo.f_getRealEmailOrTest('kzcfm@starter24.pl')


            SET @starterEmail = [dbo].[f_getEmail]('callcenter')

            EXEC p_attribute_edit
                @attributePath = '514',
                @groupProcessInstanceId = @groupProcessId,
                @stepId = 'xxx',
                @valueString = 'kz_abroad',
                @err = @err OUTPUT,
                @message = @message OUTPUT
            EXEC p_note_new
                @groupProcessId = @groupProcessId,
                @type = dbo.f_translate('email',default),
                @content = @content,
                @phoneNumber = null,
                @email = @mailTo,
                @subject = @subject,
                @direction = 1,
                @emailRegards = 1,
                @err = @err OUTPUT,
                @message = @message OUTPUT,
                -- FOR EMAIL
                @dw = '',
                @udw = '',
                @sender = @starterEmail,
                @additionalAttachments = '',
                @emailBody = @content
          end
      END
  END