ALTER PROCEDURE [dbo].[p_create_ADHOC_partner] (@partner_long_name nvarchar(255)
											  ,@partner_short_name nvarchar(255) 
											  ,@country_c nvarchar(255) 
											  ,@postal_code nvarchar(6) 
											  ,@city_c nvarchar(255) 
											  ,@street_s nvarchar(255) 
											  ,@house_nr nvarchar(100) 
											  ,@company_owner_name nvarchar(255) 
											  ,@company_owner_surname nvarchar(255) 
											  ,@owner_phone_number nvarchar(50) 
											  ,@owner_email nvarchar(255) 
											  ,@nip varchar(20)
											  ,@ha varchar(500)=''
											  ,@process_instance_id_from int=null
											  ,@id int = null output
											  )
  
  as
  /*
	2018-03-23 Maciej Grzelak
	procedura dodaje kontraktora jednorazowego (AD-HOC), procedura dziala w Atlasie
  */
   
 begin
	SET NOCOUNT ON


	 DECLARE @inputContent nvarchar(MAX)
	 SELECT @inputContent =
	 concat(@partner_long_name
					,@partner_short_name
					,@country_c
					,@postal_code
					,@city_c
					,@street_s
					,@house_nr
					,@company_owner_name
					,@company_owner_surname
					,@owner_phone_number
					,isnull(@owner_email,'')
					,isnull(@nip,'')
					,isnull(@ha,'')
					,@process_instance_id_from
					)
	 exec p_add_case_log @name = dbo.f_translate('nowy kontraktor jednorazowy',default),
		 @inputContent =                             @inputContent


    declare @attribute_value_idExists int
	declare @group_process_instance_idExist int
	declare @attribute_value_id int
	declare @group_process_instance_id int
	declare @process_instance_id int
	declare @locId int
	declare @partnerExists int
	set @partnerExists=0
	-- sprawdź NIP
	if @process_instance_id_from=0 set @process_instance_id_from=null

	if len(isnull(@nip,''))>8
	begin
		
		select	@attribute_value_idExists=parent_attribute_value_id,
				@group_process_instance_idExist=group_process_instance_id
		from	dbo.attribute_value 
		where	attribute_path = '595,83' and value_string=@nip

		if @attribute_value_idExists is not null
		begin
			set @partnerExists=1
		end
	end

	
	exec dbo.p_create_partner 
		@attribute_value_id=@attribute_value_id output,
		@process_instance_id=@process_instance_id output,
		@rs=0

	select @group_process_instance_id=group_process_id
	from dbo.process_instance
	where id=@process_instance_id
	
	set @id=@attribute_value_id
	declare @err int
	declare @message nvarchar
	declare @errSum int
	declare @messageSum nvarchar(4000)
	
	set @errSum=0
	set @messageSum=''

	begin tran

	if @attribute_value_id is not null
	begin

	  declare @attributeValueId int 
	 

	  if @partner_long_name is not null
	  begin
		  
			set @attributeValueId = null
		  
			Execute dbo.p_attribute_edit 
						@attributePath='595,84'
					,@groupProcessInstanceId=@group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@partner_long_name 
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1

		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
		

		set @attributeValueId = null

		Execute dbo.p_attribute_edit 
                     @attributePath='595,597,84'
                    ,@groupProcessInstanceId=@group_process_instance_id
                    ,@stepId='xxx' 
                    ,@userId=1 
                    ,@attributeValueId=@attributeValueId OUTPUT 
                    ,@valueInt= null
                    ,@valueDate= null
                    ,@valueString=@partner_long_name 
                    ,@valueText= null
                    ,@valueDecimal= null
                    ,@err=@err OUTPUT 
                    ,@message=@message OUTPUT
					,@skipTransaction=1

		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
	  
		
		set @attributeValueId = null
		Execute dbo.p_attribute_edit 
						@attributePath='595,614'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx'  
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@partner_long_name 
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
	end

	  if @partner_long_name is not null
	  begin
		
			  set @attributeValueId = null

			  Execute dbo.p_attribute_edit 
						 @attributePath='595,83'
						,@groupProcessInstanceId=@group_process_instance_id
						,@stepId='xxx' 
						,@userId=1 
						,@attributeValueId=@attributeValueId OUTPUT 
						,@valueInt= null
						,@valueDate= null
						,@valueString=@nip 
						,@valueText= null
						,@valueDecimal= null
						,@err=@err OUTPUT 
						,@message=@message OUTPUT
						,@skipTransaction=1

			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'
		
	  end
	
	  if @country_c is not null
	  begin
		  
			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
						@attributePath='595,624,626,86'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx'  
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@country_c
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
		
	    set @attributeValueId = null
		  Execute dbo.p_attribute_edit 
                     @attributePath='595,597,85,86'
                    ,@groupProcessInstanceId= @group_process_instance_id
                    ,@stepId='xxx'  
                    ,@userId=1 
                    ,@attributeValueId=@attributeValueId OUTPUT 
                    ,@valueInt= null
                    ,@valueDate= null
                    ,@valueString=@country_c
                    ,@valueText= null
                    ,@valueDecimal= null
                    ,@err=@err OUTPUT 
                    ,@message=@message OUTPUT
					,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
	  end

	  if @postal_code is not null
	  begin
		
		set @attributeValueId = null
		Execute dbo.p_attribute_edit 
                    @attributePath='595,624,626,89'
                ,@groupProcessInstanceId= @group_process_instance_id
                ,@stepId='xxx'  
                ,@userId=1 
                ,@attributeValueId=@attributeValueId OUTPUT 
                ,@valueInt= null
                ,@valueDate= null
                ,@valueString=@postal_code
                ,@valueText= null
                ,@valueDecimal= null
                ,@err=@err OUTPUT 
                ,@message=@message OUTPUT
				,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
		

		set @attributeValueId = null
			Execute dbo.p_attribute_edit 
                     @attributePath='595,597,85,89'
                    ,@groupProcessInstanceId= @group_process_instance_id
                    ,@stepId='xxx'  
                    ,@userId=1 
                    ,@attributeValueId=@attributeValueId OUTPUT 
                    ,@valueInt= null
                    ,@valueDate= null
                    ,@valueString=@postal_code
                    ,@valueText= null
                    ,@valueDecimal= null
                    ,@err=@err OUTPUT 
                    ,@message=@message OUTPUT
					,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
	  end
	
	  if @city_c  is not null
	  begin
		  
			  set @attributeValueId = null
			  Execute dbo.p_attribute_edit 
						 @attributePath='595,624,626,627'
						,@groupProcessInstanceId= @group_process_instance_id
						,@stepId='xxx' 
						,@userId=1 
						,@attributeValueId=@attributeValueId OUTPUT 
						,@valueInt= null
						,@valueDate= null
						,@valueString=@city_c 
						,@valueText= null
						,@valueDecimal= null
						,@err=@err OUTPUT 
						,@message=@message OUTPUT
						,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'
		

		 set @attributeValueId = null
		  Execute dbo.p_attribute_edit 
                     @attributePath='595,597,85,87'
                    ,@groupProcessInstanceId= @group_process_instance_id
                    ,@stepId='xxx' 
					,@userId=1 
                    ,@attributeValueId=@attributeValueId OUTPUT 
                    ,@valueInt= null
                    ,@valueDate= null
                    ,@valueString=@city_c 
                    ,@valueText= null
                    ,@valueDecimal= null
                    ,@err=@err OUTPUT 
                    ,@message=@message OUTPUT
					,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
	  end
	
	  if @street_s is not null
	  begin
		
			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
						@attributePath='595,624,626,94'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@street_s
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
	

		set @attributeValueId = null
		  Execute dbo.p_attribute_edit 
                     @attributePath='595,597,85,94'
                    ,@groupProcessInstanceId= @group_process_instance_id
                    ,@stepId='xxx' 
                    ,@userId=1 
                    ,@attributeValueId=@attributeValueId OUTPUT 
                    ,@valueInt= null
                    ,@valueDate= null
                    ,@valueString=@street_s
                    ,@valueText= null
                    ,@valueDecimal= null
                    ,@err=@err OUTPUT 
                    ,@message=@message OUTPUT
					,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'
	  end
	
	  if @house_nr is not null
	  begin 
			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
					 @attributePath='595,624,626,95'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@house_nr
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'
	
			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
                     @attributePath='595,597,85,95'
                    ,@groupProcessInstanceId= @group_process_instance_id
                    ,@stepId='xxx' 
                    ,@userId=1 
                    ,@attributeValueId=@attributeValueId OUTPUT 
                    ,@valueInt= null
                    ,@valueDate= null
                    ,@valueString=@house_nr
                    ,@valueText= null
                    ,@valueDecimal= null
                    ,@err=@err OUTPUT 
                    ,@message=@message OUTPUT
					,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'
	  end
	
	  if @company_owner_name is not null
	  begin
		
			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
                 @attributePath='595,633,64'
                ,@groupProcessInstanceId= @group_process_instance_id
                ,@stepId='xxx'
                ,@userId=1 
                ,@attributeValueId=@attributeValueId OUTPUT 
                ,@valueInt= null
                ,@valueDate= null
                ,@valueString=@company_owner_name
                ,@valueText= null
                ,@valueDecimal= null
                ,@err=@err OUTPUT 
                ,@message=@message OUTPUT
				,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'

	  end
	
	  if @company_owner_surname is not null
	  begin
			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
					 @attributePath='595,633,66'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@company_owner_surname
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'
		  
	  end
	
	  if @owner_phone_number is not null
	  begin
			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
					 @attributePath='595,633,197'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@owner_phone_number
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'

			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
					 @attributePath='595,597,611,642,656'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@owner_phone_number
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1

			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'

			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
						@attributePath='595,597,611,642,643'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt=2
					,@valueDate= null
					,@valueString=null
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'
	  end
	
	  if @owner_email is not null
	  begin
		  
			set @attributeValueId = null
			Execute dbo.p_attribute_edit 
					 @attributePath='595,633,368'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@owner_email
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'
		  
			select  @attributeValueId=id
			from	dbo.attribute_value
			where	group_process_instance_id=@group_process_instance_id and
					attribute_path='595,597,611'		

			EXECUTE dbo.P_attribute_add_structure 
			   @parent_attribute_value_id=@attributeValueId
			  ,@def_attribute_id=642
			  ,@use_temp_table=0
			  ,@disable_result_select=1

			select top 1 @attributeValueId=id
			from	dbo.attribute_value
			where	group_process_instance_id=@group_process_instance_id and
					attribute_path='595,597,611,642,656'	
			order by id desc

			Execute dbo.p_attribute_edit 
					 @attributePath='595,597,611,642,656'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt= null
					,@valueDate= null
					,@valueString=@owner_email
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1

			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'

			select top 1 @attributeValueId=id
			from	dbo.attribute_value
			where	group_process_instance_id=@group_process_instance_id and
					attribute_path='595,597,611,642,643'	
			order by id desc

			Execute dbo.p_attribute_edit 
					 @attributePath='595,597,611,642,643'
					,@groupProcessInstanceId= @group_process_instance_id
					,@stepId='xxx' 
					,@userId=1 
					,@attributeValueId=@attributeValueId OUTPUT 
					,@valueInt=16
					,@valueDate= null
					,@valueString=null
					,@valueText= null
					,@valueDecimal= null
					,@err=@err OUTPUT 
					,@message=@message OUTPUT
					,@skipTransaction=1
					
			set @errSum=@errSum+@err
			set @messageSum=@messageSum+@message +'|'
	  end

	if isnull(@ha,'')<>''
	begin
		set @attributeValueId = null

		Execute dbo.p_attribute_edit 
				 @attributePath='595,597,611,641'
				,@groupProcessInstanceId= @group_process_instance_id
				,@stepId='xxx' 
				,@userId=1 
				,@attributeValueId=@attributeValueId OUTPUT 
				,@valueInt=null
				,@valueDate= null
				,@valueString=@ha
				,@valueText= null
				,@valueDecimal= null
				,@err=@err OUTPUT 
				,@message=@message OUTPUT
				,@skipTransaction=1
					
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'

	end
	
	set @attributeValueId = null
	Execute dbo.p_attribute_edit 
			 @attributePath='595,676'
			,@groupProcessInstanceId= @group_process_instance_id
			,@stepId='xxx' 
			,@userId=1 
			,@attributeValueId=@attributeValueId OUTPUT 
			,@valueInt= 6
			,@valueDate= null
			,@valueString=null
			,@valueText= null
			,@valueDecimal= null
			,@err=@err OUTPUT 
			,@message=@message OUTPUT
			,@skipTransaction=1
					
	set @errSum=@errSum+@err
	set @messageSum=@messageSum+@message +'|'


	set @attributeValueId = null
		Execute dbo.p_attribute_edit 
			 @attributePath='595,597,676'
			,@groupProcessInstanceId= @group_process_instance_id
			,@stepId='xxx' 
			,@userId=1 
			,@attributeValueId=@attributeValueId OUTPUT 
			,@valueInt= 6
			,@valueDate= null
			,@valueString=null
			,@valueText= null
			,@valueDecimal= null
			,@err=@err OUTPUT 
			,@message=@message OUTPUT
			,@skipTransaction=1
					
	set @errSum=@errSum+@err
	set @messageSum=@messageSum+@message +'|'

	set @house_nr=iif(@house_nr='',null,@house_nr)
	set @street_s=replace(@street_s,'ul.','')
	set @street_s=replace(@street_s,'al.','')

	DECLARE @URL VARCHAR(8000) 
	SET @URL = dbo.f_getDomain() + dbo.f_translate('/geocode/',default)+dbo.f_remove_pl(isnull(@city_c,dbo.f_translate('Poznan',default)))+','+dbo.f_remove_pl(isnull(@street_s,dbo.f_translate('Kraszewskiego',default)))+'%201'
	--SET @URL = 'https://atlas.starter24.pl:8033/geocode/Pozna%C5%84,G%C3%B3rczy%C5%84ska%2046'
	print @URL
	DECLARE @Response varchar(8000)
	DECLARE @Obj int 
	DECLARE @HTTPStatus int 
	DECLARE @Text as table ( answer nvarchar(4000) )

	EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
	EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
	EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
	EXEC sp_OAMethod @Obj, send, NULL, ''
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
	
	INSERT @Text
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseText',default)
	EXEC sp_OADestroy @Obj

	declare @lat nvarchar(100)
	declare @lon nvarchar(100)

	select @lat=substring(data,10,100) from (
	select replace(replace(replace(data,'"',''),'}',''),']','') data from @text cross apply dbo.f_split(answer,',')
	) a where a.data like '%lati%'

	select @lon=substring(data,11,100) from (
	select replace(replace(replace(data,'"',''),'}',''),']','') data from @text cross apply dbo.f_split(answer,',')
	) a where a.data like '%long%'


	set @lat=isnull(@lat,52.407995533)
	set @lon=isnull(@lon,16.901661157)

--	select @lat, @lon

	set @attributeValueId = null
	Execute dbo.p_attribute_edit 
             @attributePath='595,597,85,93'
            ,@groupProcessInstanceId= @group_process_instance_id
            ,@stepId='xxx' 
            ,@userId=1 
            ,@attributeValueId=@attributeValueId OUTPUT 
            ,@valueInt= null
            ,@valueDate= null
            ,@valueString=@lat
            ,@valueText= null
            ,@valueDecimal= null
            ,@err=@err OUTPUT 
            ,@message=@message OUTPUT
			,@skipTransaction=1
					
	set @errSum=@errSum+@err
	set @messageSum=@messageSum+@message +'|'

	set @attributeValueId = null
	Execute dbo.p_attribute_edit 
                @attributePath='595,597,85,92'
            ,@groupProcessInstanceId= @group_process_instance_id
            ,@stepId='xxx' 
            ,@userId=1 
            ,@attributeValueId=@attributeValueId OUTPUT 
            ,@valueInt= null
            ,@valueDate= null
            ,@valueString=@lon
            ,@valueText= null
            ,@valueDecimal= null
            ,@err=@err OUTPUT 
            ,@message=@message OUTPUT
			,@skipTransaction=1
					
	set @errSum=@errSum+@err
	set @messageSum=@messageSum+@message +'|'

	if @process_instance_id_from is not null
	begin
		declare @serviceid int
		declare @stepId NVARCHAR(100)
		
		select @stepId = step_id from dbo.process_instance where id = @process_instance_id_from
		if @stepId in ('1009.048','1141.002', '1021.026')
		BEGIN
			SET @serviceid = 1
			
			EXEC dbo.p_attribute_edit 
			 @attributePath='595,597,611,641'
			,@groupProcessInstanceId= @group_process_instance_id
			,@stepId='xxx' 
			,@userId=1 
			,@valueString='80,81'
			,@err=@err OUTPUT 
			,@message=@message OUTPUT
			,@skipTransaction=1
			
		END
		ELSE IF left(@stepId,4) = '1190' and @process_instance_id_from is not null
			BEGIN


				DECLARE @groupProcessOfService int

				SELECT @groupProcessOfService = group_process_id from process_instance where id = @process_instance_id_from

				select top 1 @serviceid = value_int from attribute_value where attribute_path = '526' and group_process_instance_id = @groupProcessOfService
			end
		ELSE
		BEGIN			
			set @serviceId=dbo.f_processToService(@process_instance_id_from)
		END
		
		EXEC dbo.p_attribute_edit 
				 @attributePath='595,597,611,612'
				,@groupProcessInstanceId= @group_process_instance_id
				,@stepId='xxx' 
				,@userId=1 
				,@valueInt= @serviceId
				,@err=@err OUTPUT 
				,@message=@message OUTPUT
				,@skipTransaction=1
				
		set @errSum=@errSum+@err
		set @messageSum=@messageSum+@message +'|'


	end

	if @partnerExists=1
	begin
		print @attribute_value_idExists

		UPDATE	dbo.attribute_value
		set		parent_attribute_value_id=@attribute_value_idExists 
		where	attribute_path='595,597' and parent_attribute_value_id=@attribute_value_id 

		
		EXECUTE dbo.p_attribute_delete 
		   @attributeValueId=@attribute_value_id 
		  ,@userId=1
		  ,@force=1
		  ,@withoutHistory=1
		  
		UPDATE	dbo.attribute_value
		set		group_process_instance_id=@group_process_instance_idExist
		WHERE	group_process_instance_id=@group_process_instance_id
	end


	if @errSum=0 
	begin
		commit tran
	end
	else
	begin
		rollback tran
	end

--	  select @errSum Err, @messageSum Message
	 -- print 'New ADHOC partner root_process_instance_id ' + str(@root_id)
	end  
end