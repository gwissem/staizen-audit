ALTER PROCEDURE [dbo].[p_send_authorization_request_truck]
    @groupProcessInstanceId int,
    @userId                 int
AS
  BEGIN


    declare @root_id int
    SELECT @root_id = root_id from process_instance where id = @groupProcessInstanceId
    DECLARE @mailBody NVARCHAR(MAX)
    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))



    DECLARE @engLang tinyint = 0

    DECLARE @langTxt nvarchar(5)
    INSERT @values EXEC
    p_attribute_get2 @attributePath = '1021,1025', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @langTxt = value_string from @values

    IF isnull(@langTxt,'PL') = dbo.f_translate('ENG',default)
      BEGIN
        SET @engLang =1
      end


    IF @engLang = 1
      BEGIN

        EXEC Dbo.P_get_email_template @name = 'truck_authorization_request_eng', @responseText = @mailBody output

      end
    ELSE
      BEGIN

        EXEC Dbo.P_get_email_template @name = 'truck_autorization_request', @responseText = @mailBody output
      end


    DECLARE @sendToMail nvarchar(200)
    DECLARE @authorizationDetails nvarchar(1000)


    DECLARE @currency NVARCHAR(30)

    INSERT @values EXEC
    p_attribute_get2 @attributePath = '1021,556', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @currency = value_string from @values


    DELETE from @values
    INSERT @values EXEC
    p_attribute_get2 @attributePath = '1021,368', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @sendToMail = value_string from @values


    SET @sendToMail = dbo.f_getRealEmailOrTest(@sendToMail)



    --     __COMPANYNAME__
    --   __TODAY__
    --       __CASE_ID__
    DECLARE @caseId nvarchar(40)
    SET @caseId = dbo.f_caseId(@groupProcessInstanceId)



    --   __LOCATION_PLACE__
    --   zdarzenia
    --   __OTHER_NR__
    --   __ACCIDENT_DATE__
    --   __companyusing__
    --   __COMPANY_NIP__
    --   __CARD_ID__
    --   __CARD_DATE__
    --   __CLIENT_NR__
    --   __SIGN__
    --   __AUTORIZATION_DETAILS__
    --

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1021,63', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @authorizationDetails = value_text FROM @values

    --   __USERNAME__


    DECLARE @companyName nvarchar(200)
    DECLARE @companyType nvarchar(50)
    DECLARE @locationPlace nvarchar(200)
    DECLARE @accidentType nvarchar(200)
    DECLARE @accidentDate datetime
    DECLARE @carUsedByCompany nvarchar(200)
    DECLARE @companyNIP nvarchar(200)
    DECLARE @cardNumber nvarchar(200)
    DECLARE @cardDate nvarchar(200)

    DELETE from @values
    INSERT @values EXEC p_attribute_get2  @attributePath = '101,85,86',
                                          @groupProcessInstanceId = @groupProcessInstanceId

    SELECT @locationPlace = value_string from @values


    DELETE from @values
    INSERT @values exec p_attribute_get2 @attributePath = '176', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardNumber = value_string from @values
    DELETE from @values


    DECLARE @userFullName nvarchar(200)
    --     Username
    SELECT @userFullName = (isnull(firstname, '') + ' ' + isnull(lastname, '')) from fos_user where id = @userId


    SELECT TOP 1 @accidentDate = created_at from process_instance where root_id = @root_id order by id desc

    DELETE from @values
    INSERT @values exec p_attribute_get2 @attributePath = '186', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardDate = value_string from @values
    DELETE from @values


    DECLARE @accidentTypeId int

    INSERT @values exec p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @accidentTypeId = value_int from @values
    DELETE from @values


    IF @engLang =1
      BEGIN
        SELECT top 1@accidentType = argument1 from AtlasDB_def.dbo.dictionary where typeD = 'EventType' and value = @accidentTypeId
      end
    ELSE
      BEGIN
    SELECT @accidentType = dbo.f_dictionaryDesc(@accidentTypeId, 'EventType')
END

    INSERT @values EXEC
    p_attribute_get2 @attributePath = '84', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @carUsedByCompany = value_string from @values

    DELETE from @values
    INSERT @values EXEC
    p_attribute_get2 @attributePath = '83', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @companyNIP = value_string from @values


    DECLARE @accidentDateText NVARCHAR(50)
    SET @accidentDateText = convert(varchar, @accidentDate, 103) + ' ' + convert(varchar, @accidentDate, 108)

    -- table of cost
    DECLARE @tableOfCosts nvarchar(3000)
    DECLARE @typeOfService int
    DECLARE @typeOfServiceText nvarchar(150)

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '190', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @typeOfService = value_int FROM @values

    IF @engLang =1
      BEGIN
        SELECT top 1 @typeOfServiceText = argument1 from AtlasDB_def.dbo.dictionary where typeD = 'STSServicesNeeded' and value = @typeOfService
      end
    ELSE
      BEGIN
        SELECT @typeOfServiceText = dbo.f_dictionaryDesc(@typeOfService, 'STSServicesNeeded')
      END


    PRINT '@typeOfService'
    PRINT @typeOfService
    PRINT '@typeOfServiceText'
    PRINT @typeOfServiceText


    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1021,1024', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @companyType = value_string FROM @values

    IF @companyType = dbo.f_translate('COMPANY',default)
      BEGIN
        DELETE FROM @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '84', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @companyName = value_string FROM @values
      end
    ELSE IF @companyType = dbo.f_translate('Shell',default)
      BEGIN
        SET @companyName = dbo.f_translate('Shell',default)

      end
    ELSE IF @companyType = dbo.f_translate('ESSO',default)
      BEGIN
        SET @companyName = dbo.f_translate('ESSO',default)

      end

    DECLARE @cardType nvarchar(20)

    DECLARE @cardEurope int
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '705', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardEurope = value_int FROM @values


    DECLARE @cardUta int
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '165', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardUta = value_int FROM @values
    DECLARE @cardKey int
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '191', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardKey = value_int FROM @values


    if isnull(@cardEurope, 0) <> 0
      BEGIN
        SET @cardType = dbo.f_translate('EU / EUROPE',default)
      end
    if isnull(@cardUta, 0) <> 0
      BEGIN
        SET @cardType = dbo.f_translate('FULL SERVICE CARD / Mercedes Service Card ',default)
      end
    if isnull(@cardKey, 0) <> 0
      BEGIN
        SET @cardType = dbo.f_translate('KEY / EUROPE',default)
      end


    declare @otherNr int
    declare @otherNrText NVARCHAR(30)


    DELETE from @values

    INSERT @values EXEC p_attribute_get2 @attributePath = '293', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @otherNrText = value_string FROM @values
   -- SET @otherNrText = CAST(@otherNr as nvarchar(40))

    --     IF @typeOfService =6
    --     BEGIN
    --
    --     end
    DECLARE @authorizationFull decimal(18, 2)

    DELETE from @values

    IF @currency = dbo.f_translate('EUR',default)
      BEGIN
        INSERT @values EXEC p_attribute_get2 @attributePath = '1021,466',
                                             @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @authorizationFull = value_decimal FROM @values
      end
    ELSE
      BEGIN
        INSERT @values EXEC p_attribute_get2 @attributePath = '1021,1022',
                                             @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @authorizationFull = value_decimal FROM @values
      end


    PRINT dbo.f_translate('typeOfServiceText',default)
    PRINT @typeOfServiceText
    PRINT dbo.f_translate('authorizationFull',default)
    PRINT cast(@authorizationFull as nvarchar(50))


    DECLARE @comission decimal(10,2)
    DECLARE @comissionText NVARCHAR(500) =''

    IF isnull(@typeOfService,0) = 6
      BEGIN

    INSERT @values EXEC p_attribute_get2 @attributePath = '1021,1023',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @comission = value_decimal FROM @values

        IF @engLang = 1
          BEGIN
--             po angielsku
            SET @comissionText = '<tr><td>Intervention cost </td><td  style="padding:30px 60px">'+cast(@comission as nvarchar(50))+' '+isnull(@currency, dbo.f_translate('PLN',default)) +'</td></tr>'
          end
        ELSE
        BEGIN
--            Po polsku
          SET @comissionText = '<tr><td>Prowizja za dowóz gotówki</td><td  style="padding:30px 60px">'+cast(@comission as nvarchar(50)) +' '+isnull(@currency, dbo.f_translate('PLN',default)) +'</td></tr>'
        end

    END


    IF @engLang = 0
      BEGIN
    SET @tableOfCosts = '<div class="clearfix" style="height:1px;width:100%;overflow:hidden"></div>' +
                        '<div><div style="width:50%;float:left">' +
                        '<table>'
                        + '<tr>' +
                        '<th>Opis</th>' +
                        '<th>Wartość</th></tr>'
                        + '<tr>' +
                        '<td>' + isnull(@typeOfServiceText, '') + '</td>' +
                        '<td style="padding:30px 60px">' + cast(@authorizationFull as nvarchar(50)) + ' ' +
                        isnull(@currency, dbo.f_translate('PLN',default)) + '</td>' +
                        '</tr>'+isnull(@comissionText,'')+'</table></div>'

    SET @tableOfCosts = @tableOfCosts + '<div style="width:50%;float:left;">' +
                        '<table>' +
                        '<tr>' +
                        '<th>Opis</th>' +
                        '</tr>' +
                        '<tr>' +
                        '<td rowspan="4" style="border:1px solid black; vertical-align:middle;text-align:center;padding:30px 60px">'
                        +
                        dbo.f_translate('pieczątka i podpis',default) +
                        '</td>' +
                        '</tr>' +
                        '</table>' +
                        '</div></div>'

    END
    ELSE
      BEGIN
        SET @tableOfCosts = '<div class="clearfix" style="height:1px;width:100%;overflow:hidden"></div>' +
                            '<div><div style="width:50%;float:left">' +
                            '<table>'
                            + '<tr>' +
                            '<th>Description</th>' +
                            '<th>Value</th></tr>'
                            + '<tr>' +
                            '<td>' + isnull(@typeOfServiceText, '') + '</td>' +
                            '<td style="padding:30px 60px">' + cast(@authorizationFull as nvarchar(50)) + ' ' +
                            isnull(@currency, dbo.f_translate('PLN',default)) + '</td>' +
                            '</tr>'+isnull(@comissionText,'')+'</table></div>'

        SET @tableOfCosts = @tableOfCosts + '<div style="width:50%;float:left;">' +
                            '<table>' +
                            '<tr>' +
                            '<th>Notes</th>' +
                            '</tr>' +
                            '<tr>' +
                            '<td rowspan="4" style="border:1px solid black; vertical-align:middle;text-align:center;padding:30px 60px">'
                            +
                            dbo.f_translate('stamp and signature',default) +
                            '</td></tr>' +
                            '</table>' +
                            '</div></div>'
      end

    print '@tableOfCosts'
    print @tableOfCosts

    DECLARE @platform nvarchar(100)
    DECLARE @platformId int
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int FROM @values
    SELECT @platform = name from AtlasDB_def.dbo.platform where id = @platformId

    DECLARE @stsTypeID int
    DECLARE @stsTypeName nvarchar(30)
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '173', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @stsTypeID = value_int FROM @values
    IF @engLang =1
      BEGIN
        SELECT TOP 1 @stsTypeName = argument1 from AtlasDB_def.dbo.dictionary where typeD = 'STSPayment' and value = @stsTypeID
      end
    ELSE
      BEGIN
    SET @stsTypeName = dbo.f_dictionaryDesc(@stsTypeID, 'STSPayment')

      END

    If isnull(@stsTypeID, -1) <> -1
      BEGIN
        IF @stsTypeID = 1
          BEGIN
            SET @stsTypeName = dbo.f_translate('euroShell',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @stsTypeID = 2
          BEGIN
            SET @stsTypeName = dbo.f_translate('UTA',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @stsTypeID = 3
          BEGIN
            SET @stsTypeName = dbo.f_translate('ESSO',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @stsTypeID = 4
          BEGIN
            SET @stsTypeName = dbo.f_translate('KRONE',default)
            SET @platform = dbo.f_translate('KRONE',default)
          end
        IF @stsTypeID = 5
          BEGIN
            SET @stsTypeName = dbo.f_translate('KOGEL',default)
            SET @platform = dbo.f_translate('KOGEL',default)
          end
        IF @stsTypeID = 6
          BEGIN
            SET @stsTypeName = dbo.f_translate('OTOKAR',default)
            SET @platform = dbo.f_translate('OTOKAR',default)
          end
        IF @stsTypeID = 7
          BEGIN
            SET @stsTypeName = dbo.f_translate('BPW ',default)
            SET @platform = dbo.f_translate('BPW',default)
          end
        IF @stsTypeID = 8
          BEGIN
            SET @stsTypeName = dbo.f_translate('WEBASTO ',default)
            SET @platform = dbo.f_translate('WEBASTO',default)
          end
        IF @stsTypeID = 9
          BEGIN
            SET @stsTypeName = dbo.f_translate('FLEET',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end

        --         Setujemy po odpowiedniej
      end
    ELSE
      BEGIN
        SET @stsTypeName = @platform
        --         bierzemy nazwę platformy
      end


    PRINT @accidentDateText


    PRINT @tableOfCosts
    PRINT @mailBody

    SET @mailBody = REPLACE(@mailBody, '__TODAY__', dbo.FN_VDateHour(GETDATE()))
    SET @mailBody = REPLACE(@mailBody, '__TABLE_OF_COST__', isnull(@tableOfCosts, ''))
    SET @mailBody = REPLACE(@mailBody, '__COMPANYNAME__', isnull(@companyName, ''))
    SET @mailBody = REPLACE(@mailBody, '__CASE_ID__', isnull(@caseId, ''))
    SET @mailBody = REPLACE(@mailBody, '__LOCATION_PLACE__', ISNULL(@locationPlace, ''))
    SET @mailBody = REPLACE(@mailBody, '__ACCIDENT_TYPE__', isnull(@accidentType, ''))
    SET @mailBody = REPLACE(@mailBody, '__OTHER_NR__', isnull(@otherNrText, ''))
    SET @mailBody = REPLACE(@mailBody, '__ACCIDENT_DATE__', isnull(@accidentDateText, ''))
    SET @mailBody = REPLACE(@mailBody, '__companyusing__', isnull(@carUsedByCompany, ''))
    SET @mailBody = REPLACE(@mailBody, '__COMPANY_NIP__', isnull(@companyNIP, ''))
    SET @mailBody = REPLACE(@mailBody, '__CARD_ID__', isnull(@cardNumber, ''))
    SET @mailBody = REPLACE(@mailBody, '__CARD_DATE__', isnull(@cardDate, ''))

    SET @mailBody = REPLACE(@mailBody, '__CARD_TYPE__', isnull(@stsTypeName, '')) --oznaczenie?
    SET @mailBody = REPLACE(@mailBody, '__SIGN__', isnull(@cardType, '')) --oznaczenie?

    SET @mailBody = REPLACE(@mailBody, '__AUTORIZATION_DETAILS__', isnull(@authorizationDetails, ''))
    SET @mailBody = REPLACE(@mailBody, '__USERNAME__', isnull(@userFullName, ''))

    print '______________'
    PRINT @tableOfCosts
    PRINT @mailBody
    DECLARE @subject nvarchar(300)
    SET @subject = dbo.f_translate('Truck additional authorization: ',default) + ISNULL(@caseId, '') + ' / ' + ISNULL(@companyName, '')


    DECLARE @err int
    DECLARE @message nvarchar(2000)


    DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('truck')

    DECLARE @attachment nvarchar(MAX)


    EXEC dbo.p_attribute_edit
        @attributePath = '1021,959',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @stepId = 'xxx',
        @valueText = @mailBody,
        @err = @err OUTPUT,
        @message = @message OUTPUT
    declare @pdfId int
    SELECT TOP 1 @pdfId = id
    FROM dbo.attribute_value
    WHERE attribute_path = '1021,959'
      AND group_process_instance_id = @groupProcessInstanceId

    print '@pdfId'
    print @pdfId


    SET @attachment =
    '{ATTRIBUTE::' + CONVERT(NVARCHAR(100), @pdfId) + dbo.f_translate('::Truck additional authorization for case ',default) + @caseId +
    '::false}'


    EXECUTE dbo.p_note_new
        @groupProcessId = @groupProcessInstanceId
        , @type = dbo.f_translate('email',default)
        , @content = dbo.f_translate('Truck Organization Request - STS / STANDARD',default)
        , @email = @sendToMail
        , @userId = 1  -- automat
        , @subject = @subject
        , @direction = 1
        , @dw = ''
        , @udw = ''
        , @sender = @senderEmail
        , @additionalAttachments = @attachment
        , @emailBody = @mailBody
        , @err = @err OUTPUT
        , @message = @message OUTPUT


  END
