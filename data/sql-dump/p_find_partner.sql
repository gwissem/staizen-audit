ALTER PROCEDURE [dbo].[p_find_partner]
( 
	@groupProcessInstanceId INT,
	@type int=0,
	@resultType int=0,
	@maxDistance int = null,
	@limit int = 50,
	@partnerId INT = NULL,
	@skipHistory INT = 0,
	@manual INT = 0,
	@addEmpty INT = 0,
	@addCurrentWorhshop INT = 1,
	@addClosestWorkshops INT = 0,
	@useRootProgram INT = 0,
	@debugMode TINYINT = 0
) 
AS
BEGIN
	if @type=0 set @addCurrentWorhshop=0
	
	/* ==================================== 
	
		# @resultType 			- określa co ma zostać zwrócone z procedury
			
			= 1 - Wrzuca wyniki do tymczasowej tabeli - #partnerTempTable
			
			= 2 - Zwykły SELECT wyników
	
			= 3 - Tak jak 2, tylko do nazwy partnera dodawany jest dystans i priorytet
	
		# @type  				- Typ wyników jakie zostaną zwrócone  (kontraktorzy/serwisy)
			
			= 0 - Zwraca listę kontraktorów wykonujących naprawe / holowanie
			
			= 1 - Zwraca listę serwisów gdzie będzie odholowany pojazd
			
			= 98 - Lista serwisów dla danej platformy (Wykorzystywane dla DC)
			
			= 99 - Lista wszystkich serwisów (sortowane od Poznania)
		
		# @limit 				- Ile wyników ma zostać zwróconych
		
		# @maxDistance 			- Maksymalna dystans od miejsca zdarzenia (domyślnie od 101,85)
		
		# @addEmpty 			- ??
		
		# @manual 				- Jeżeli 0, to nie zwraca wyników kontraktorów którzy dostali SMS'a w przeciągu 3 min
		
		# skipHistory 			- ??
		
		# @addCurrentWorhshop 	- ??
		
		# @addClosestWorkshops 	- ??
		
		# @debugMode			- czy debagować kod (dodatkowe selecty / printy)
		
		# EXAMPLE:
		
		EXEC P_find_partner 
              @groupProcessInstanceId = GROUP_ID, 
              @resultType = 2, 
              @type = 1, 
              @limit = 999999, 
              @maxDistance = 9999
              
	==================================== */
	
	
	SET NOCOUNT ON

	PRINT '------------------ EXEC p_find_partner --------------------'
	declare @startTime datetime=getdate()
	declare @ts int
	set @ts=datediff(ms,getdate(),@startTime)
	print '01 - '+cast(@ts as varchar(20))
	set @startTime=getdate()

	declare @platformId int
	declare @rootPlatformId int
	declare @DMC int
	declare @customLocationTypes nvarchar(255)
	declare @makeModelProgram NVARCHAR(300)
	declare @makeModelType INT
	declare @makeModelId int
	DECLARE @city NVARCHAR(100)
	declare @withCFM int
	DECLARE @debugAmount INT
	DECLARE @resultTableCount int
	DECLARE @maxAutoRequests int = 3
	declare @consult int

	set @withCFM=1

	create table #resultTable(
		id int identity(1,1),
		partnerId int,
		partnerName nvarchar(400),
		priority tinyint,
		priority2 tinyint,
		distance decimal(10,2),
		distancePriority decimal(10,2),
		phoneNumber nvarchar(100),
		reasonForRefusing NVARCHAR(100) DEFAULT NULL,
		partnerServiceId INT,
		city nvarchar(100)
	)

	create table #partners (
	 partnerId int,
	 partnerName nvarchar(400),
	 service tinyint,
	 contractor tinyint,
	 distance decimal(10,2),
	 fixing tinyint,
	 towing tinyint,
	 computer tinyint,
	 X decimal(18,6),
	 Y decimal(18,6),
	 priority tinyint,
	 towingTarget tinyint,
	 transport tinyint,
	 parking tinyint,
	 maxDMC int,
	 phoneNumber nvarchar(100),
	 partnerServiceId int,
	 avUnavSerFromD datetime,
	 avUnavSerToD datetime,
	 avUnavLocFromD datetime,
	 avUnavLocToD datetime,
	 avUnavPartFromD datetime,
	 avUnavPartToD datetime,
	 city nvarchar(100),
	 possibilities nvarchar(500),
	 type nvarchar(4000),
	 extCode nvarchar(500),
	 weightMax int,
	 heightMax decimal(10,5)
	)

	--CREATE NONCLUSTERED INDEX in1 ON #partners (type);
	--CREATE NONCLUSTERED INDEX in2 ON #partners (possibilities);

----- import list of partners distance < 70 km ------

declare @X nvarchar(200)
declare @Y nvarchar(200)
DECLARE @xPath NVARCHAR(10)
DECLARE @yPath NVARCHAR(10)
declare @parkingNeeded INT
declare @service int
declare @targetService int
declare @startLocationPath NVARCHAR(20)
declare @rootId INT 
DECLARE @carStatus INT

DECLARE @debugTable as TABLE (label NVARCHAR(255), message NVARCHAR(4000))

DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

SELECT @rootId = root_id FROM dbo.process_instance  with(nolock) where id = @groupProcessInstanceId 

DECLARE @platformGroup nvarchar(255)
declare @closestWorkshops nvarchar(255)
declare @processPath int
declare @rootProgramId int 

EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '907', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @closestWorkshops = value_string FROM @values

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '115', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @processPath = value_int FROM @values

IF @processPath = 2
BEGIN
	SET @limit = 1000
END 
ELSE IF @type = 0 AND @manual = 0 
BEGIN
	SET @limit = @maxAutoRequests
END 

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '522',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @targetService = value_int FROM @values

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '74,76',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @DMC = value_int FROM @values

set @DMC=isnull(@DMC,2100)

DECLARE @height DECIMAL(10,5)

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '74,263',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @height = value_decimal FROM @values

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '253',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @platformId = value_int FROM @values

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '253',
		@groupProcessInstanceId = @rootId
SELECT @rootPlatformId = value_int FROM @values

delete from @values
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '74,73',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @makeModelId = value_int FROM @values

declare @make nvarchar(50)
DECLARE @makeModelV nvarchar(50)


SELECT @makeModelProgram = descriptionD, @makeModelType = ISNULL(argument2,1), @makeModelV=textD
FROM dictionary with(nolock)
where typeD = 'makeModel' AND value = @makeModelId


select top 1 @make=data
from dbo.f_split(@makeModelV,' ')
order by id

select top 1 @make=data
from dbo.f_split(@make,'-')
order by id

set @make=isnull(@make,'')
set @make=replace(@make,dbo.f_translate('Volkswagen',default),'VW')

DECLARE @platformGroupName nvarchar(255)
declare @programIds nvarchar(200)

EXECUTE dbo.p_platform_group_name
	@groupProcessInstanceId=@groupProcessInstanceId
	,@name=@platformGroupName OUTPUT
	-- HELIS - TEMP


INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '204', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @programIds = value_string FROM @values


declare @programId NVARCHAR(255)
INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @programId = value_string FROM @values

IF @useRootProgram = 1
BEGIN
	delete from @values	
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
	SELECT @programId = value_string FROM @values
	
	delete from @values	
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	SELECT @platformId = value_int FROM @values
END 



DECLARE @nearHome int

delete from @values
INSERT  @values EXEC dbo.p_attribute_get2
	@attributePath = '776',
	@groupProcessInstanceId = @groupProcessInstanceId
SELECT @nearHome = value_int FROM @values

DECLARE @starterContractorAbroad int
	delete from @values
	INSERT @values EXEC dbo.p_attribute_get2
	@attributePath = '1003',
		@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @starterContractorAbroad = value_int from @values


-- HELIS - TEMP- END
	PRINT '@type'
	PRINT @type
	PRINT '@starterContractorAbroad'
	PRINT @starterContractorAbroad

	-- ASO dla platformy ARC (serwisy z danej marki + sieć starter + bosch)
IF @platformId = 27 and @type <> 0 
BEGIN
	SELECT @customLocationTypes = isnull([dbo].[f_typeOfService](@makeModelId)+',','')+'1,72' 	
END 

SET @parkingNeeded = 0

SET @startLocationPath = '101,85'

/*	Stan pojazdu, potrzebne do stwierdzenia czy jest to szukanie serwisu dla Monitoringu
____________________________________*/
DELETE FROM @values
INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '116,119', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @carStatus = value_int FROM @values

--if SYSTEM_USER = dbo.f_translate('andrzej.dziekonski',default)
--BEGIN
--	SELECT @carStatus
--END 



IF @carStatus IS NOT NULL
BEGIN
	
	/*	Unieruchomiony 
	 * LUB Skradziony - fix 07.06.2019
 	____________________________________*/
	IF @carStatus = 0 OR @carStatus = 2
	BEGIN
			
		DELETE FROM @values
		
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '101,85,92',
				@groupProcessInstanceId = @groupProcessInstanceId
				
		/*	Jeżeli nie ma miejsca zdarzenia, to bierzemy miejsce SZKODY
  		____________________________________*/
				
		IF (SELECT count(id) FROM @values WHERE value_string IS NOT NULL) = 0
		BEGIN
			
			-- Dla Alphabet, w przypadku Unieruchomionnego samochodu (lub skradzinego)
			DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '500,85,92',
					@groupProcessInstanceId = @groupProcessInstanceId
						
			IF (SELECT count(id) FROM @values WHERE value_string IS NOT NULL) > 0
			BEGIN
				SET @startLocationPath = '500,85'
			END
		
		END
	END
	ELSE IF @carStatus = 1 AND @type IN(1,99)
	BEGIN

		-- Jezdy przy kierowalności do serwisu, dla wszystkich CFM: jeśli szkoda jezdna  bierzemy miejsce lokalizacji jako "Miejsce użytkowania pojazdu"
		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '755,85,92',
				@groupProcessInstanceId = @groupProcessInstanceId
				
		IF (SELECT count(id) FROM @values WHERE value_string IS NOT NULL) > 0
		BEGIN
			SET @startLocationPath = '755,85'
		END
		
	END
	
END


IF @debugMode = 1
BEGIN
	
	PRINT '@carStatus - ' + CAST(@carStatus AS NVARCHAR(100))
	PRINT '@startLocationPath - ' + @startLocationPath
	
	INSERT INTO @debugTable (label, message)
    VALUES	('INTRO', '------------------------------------'),
    		('@carStatus', CAST(@carStatus AS NVARCHAR(100))),
    		('@startLocationPath', @startLocationPath)
    
END

declare @eventType int --MG2018	

delete from @values
INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @eventType = value_int FROM @values -- 1 wypadek else awaria

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '697',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @parkingNeeded = value_int FROM @values

SET @parkingNeeded = ISNULL(@parkingNeeded, 0)

DELETE FROM @values
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '211,85,92',
		@groupProcessInstanceId = @groupProcessInstanceId


IF @parkingNeeded = 1 and exists(SELECT id FROM @values WHERE isnull(value_string,'')<>'') 
BEGIN
	SET @startLocationPath = '211,85'
END 

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '560',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @service = value_int FROM @values

declare @isTransport int

delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '550',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @isTransport = value_int FROM @values
set @isTransport=ISNULL(@isTransport,0)
	


	IF
	isnull(@starterContractorAbroad,0) = 1
		BEGIN

			SET @customLocationTypes = '1' -- tylko starter
			SET @maxDistance = 999999
			IF @nearHome = 1 -- blisko miejsca docelowego
				BEGIN
					SET @startLocationPath = '211,85'
				end
			ELSE
				BEGIN
					SET @startLocationPath = '101,85' --blisko zdarzenia
				END

		end

	DECLARE @abroadLocationLocalPartner int
	delete from @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '206',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @abroadLocationLocalPartner = value_int FROM @values
	IF
	isnull(@abroadLocationLocalPartner,0) = 1
		BEGIN

			SET @maxDistance = 999999

		end




		PRINT '-- USER startLocationPath --'
		PRINT @startLocationPath
declare @cityPath nvarchar(20)

SET @xPath = @startLocationPath+',92'
SET @yPath = @startLocationPath+',93'
set @cityPath = @startLocationPath+',87'


delete from @values	
INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = @xPath,
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @X = value_string FROM @values
PRINT '@X'
PRINT @X
PRINT '---------------------xPATH'
PRINT @xPath
PRINT '---------------------yPATH'
PRINT @yPath

delete from @values

INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = @yPath,
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @Y = value_string FROM @values
	PRINT '@y'
	PRINT @Y
delete from @values


INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = @cityPath,
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @city = value_string FROM @values
	
delete from @values

	print '@type'
	print @type
	print '@maxDistance'
	print @maxDistance
	print '@startLocationPath'
	print @startLocationPath


if @maxDistance is NULL
begin
	
	if @type=0 
	begin
		set @maxDistance=150
	end 
	else
	begin
		set @maxDistance=1000
	end

end 
ELSE IF @addClosestWorkshops = 1 and @type IN (1,98,99)
BEGIN
	SET @maxDistance = 1000
END 

declare @accidentCrash int
declare @blacklist NVARCHAR(255)
declare @contactType int --MG2018


if @platformId = 2  
begin
	if @eventType in (1,3)-- and 1=0-- jesli wypadek szkoda w fordzie
	begin
		set @contactType = 17 -- CLS - Telefon
	end
	else
	begin
		set @contactType = 1  -- realizacja SMS
	end
end
else
begin
	set @contactType = 1
end;
-- select @contactType

IF @type IN (98) OR (@type = 99 AND (ISNULL(@X,'') = '' OR ISNULL(@X,'') = ''))   
BEGIN
	-- X === lng - 16.7615836
	-- Y === lat - 52.4004458
	-- Ustalenie fikcyjnych współrzędnych	
	
	SET @Y = '52.4004458'
	SET @X = '16.7615836'
	
END

declare @point1 geography
SET @point1 = GEOGRAPHY::Point(@Y , @X , 4326)

delete from @values

INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '491',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @accidentCrash = value_int FROM @values

delete from @values


delete from @values

INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '719',
		@groupProcessInstanceId = @groupProcessInstanceId
SELECT @blacklist = value_string FROM @values

	
set @ts=datediff(ms,getdate(),@startTime)
	print '02 - '+cast(@ts as varchar(20))
	set @startTime=getdate()
print  '@partnerId'
print  @partnerId

IF @debugMode = 1
BEGIN
	
	INSERT INTO @debugTable (label, message)
    VALUES	('ARGUMENTS', '------------------------------------'),
    		('@type', CAST(@type AS NVARCHAR(100))),
    		('@resultType', CAST(@resultType AS NVARCHAR(100))),
    		('@maxDistance', CAST(@maxDistance AS NVARCHAR(100))),
    		('@limit', CAST(@limit AS NVARCHAR(100))),
    		('@manual', CAST(@manual AS NVARCHAR(100))),
    		('MAIN INFO', '------------------------------------'), 
    		('@partnerId', CAST(@partnerId AS NVARCHAR(100)) ), 
    		('@accidentCrash', CAST(@accidentCrash AS NVARCHAR(100))),
           	('@eventType', CAST(@accidentCrash AS NVARCHAR(100))), 
           	('@startLocationPath', @startLocationPath),
           	('@xPath', CAST(@xPath AS NVARCHAR(100))),
           	('@yPath', CAST(@yPath AS NVARCHAR(100))),
           	('@Y', CAST(@Y AS NVARCHAR(100))),
           	('@X', CAST(@X AS NVARCHAR(100))),
           	('@groupProcessInstanceId', CAST(@groupProcessInstanceId AS NVARCHAR(100))),
           	('@programId', CAST(@programId AS NVARCHAR(100))),
           	('@platformId', CAST(@platformId AS NVARCHAR(100)));

END

if @platformId=25
begin
	declare @BAT_ParkingLocId int
	declare @BAT_locId int
	declare @BATgroupProcessInstanceId int
	delete from @values	
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '305',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @BAT_ParkingLocId = value_int FROM @values
	
	if @BAT_ParkingLocId is not null
	begin
		select	@BAT_locId=id,
				@BATgroupProcessInstanceId=group_process_instance_id
		from	dbo.attribute_value avloc with(nolock)
		where	parent_attribute_value_id=@BAT_ParkingLocId and 
				attribute_path='595,597,85'

		delete from @values	
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '595,597,85,92',
				@groupProcessInstanceId = @BATgroupProcessInstanceId,
				@parentAttributeId=@BAT_locId
		SELECT @X = value_string FROM @values

		delete from @values

		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath =  '595,597,85,93',
				@groupProcessInstanceId = @BATgroupProcessInstanceId,
				@parentAttributeId=@BAT_locId
		SELECT @Y = value_string FROM @values

		SET @point1 = GEOGRAPHY::Point(@Y , @X , 4326)
	end
end

DECLARE @arcCode NVARCHAR(20)
SELECT @arcCode = dbo.f_diagnosis_code(@rootId)
		


if @type=7  
begin

	set @maxDistance=200

	insert into #partners (
			partnerId ,
			partnerName,
			service,
			contractor,
			distance,
			fixing,
			towing,
			computer,
			X,
			Y,
			priority,
			towingTarget,
			transport,
			parking,
			maxDMC,
			phoneNumber,
			partnerServiceId,
			avUnavSerFromD,
			avUnavSerToD,
			avUnavLocFromD,
			avUnavLocToD,
			avUnavPartFromD,
			avUnavPartToD,
			city
		)
	select id,
			name + ' [id: '+cast(id as nvarchar(20))+']' name,
			max(service) service,
			max(contractor) contractor,
			min(distance) distance,
			max(fixing) fixing,
			max(towing) towing,
			max(computer) computer,
			X,
			Y,
			priority,
			max(towingTarget) towingTarget,
			max(transport) transport,
			max(parking) parking,
			isnull(max(maxDMC),9999) maxDMC,
			dbo.f_partner_contact(@groupProcessInstanceId, id, 1, 2) phone,
			null,
			min(avUnavSerFromD) avUnavSerFromD,
			min(avUnavSerToD) avUnavSerToD,
			min(avUnavLocFromD) avUnavLocFromD,
			min(avUnavLocToD) avUnavLocToD,
			min(avUnavPartFromD) avUnavPartFromD,
			min(avUnavPartToD) avUnavPartToD,
			city
	from (
		SELECT   id
				,name name
				,1 service
				,1 contractor
				,ISNULL(@point1.STDistance(loc)*0.0012,999) distance
				,1 fixing
				,1 towing
				,0 computer
				,X
				,Y
				,1 priority
				,1 towingTarget
				,1 transport
				,1 parking
				,4000 maxDMC
				,avUnavSerFromD
				,avUnavSerToD
				,avUnavLocFromD
				,avUnavLocToD
				,avUnavPartFromD
				,avUnavPartToD
				,city
		FROM dbo.partners_services_cache pc with(nolock)
		where name like '%parking%'	and type like '%leaseplan%'
	) a
	GROUP BY id,
			name,
			X,
			Y,
			priority,
			city
			

	insert into #resultTable
	select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
	from	#partners p left join 
			#resultTable r on p.partnerId=r.partnerId 
	where	r.id is null
	order by p.distance
end
else if @type=0
begin
	if @partnerId is null
	begin 
		if @manual = 1
		begin

			declare @contractorSwitch int
				
			if @platformId in (48,78)
			begin
				declare @fleetK nvarchar(100)
				exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('Leasingobiorca',default), @output=@fleetK OUTPUT

				delete from @values

				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '985,292',
						@groupProcessInstanceId = @groupProcessInstanceId
				SELECT @contractorSwitch = value_int FROM @values
			
				

			end

			set @contractorSwitch=isnull(@contractorSwitch,0)
			
			set @consult=0

			if @platformId=78
			begin
				EXEC dbo.p_lp_consult_workshop_routing 
						@groupProcessInstanceId = @groupProcessInstanceId, 
						@consult = @consult output
			end

			if @fleetK like '%kaufland%' or @platformId not in (48,78) or @service in (0,2) or @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%' or @eventType not in (1,5) or @contractorSwitch=1 or @consult=1
			begin
			
			insert into #partners (
					 partnerId ,
					 partnerName,
					 service,
					 contractor,
					 distance,
					 fixing,
					 towing,
					 computer,
					 X,
					 Y,
					 priority,
					 towingTarget,
					 transport,
					 parking,
					 maxDMC,
					 phoneNumber,
					 partnerServiceId,
					 avUnavSerFromD,
					 avUnavSerToD,
					 avUnavLocFromD,
					 avUnavLocToD,
					 avUnavPartFromD,
					 avUnavPartToD,
					 city
					)
			select id,
				   name,
				   max(service) service,
				   max(contractor) contractor,
				   min(distance) distance,
				   max(fixing) fixing,
				   max(towing) towing,
				   max(computer) computer,
				   X,
				   Y,
				   priority,
				   max(towingTarget) towingTarget,
				   max(transport) transport,
				   max(parking) parking,
				   isnull(max(maxDMC),9999) maxDMC,
				   max(phone) phone,
				   null,
				   min(avUnavSerFromD) avUnavSerFromD,
				   min(avUnavSerToD) avUnavSerToD,
				   min(avUnavLocFromD) avUnavLocFromD,
				   min(avUnavLocToD) avUnavLocToD,
				   min(avUnavPartFromD) avUnavPartFromD,
				   min(avUnavPartToD) avUnavPartToD,
				   city
			from (
				SELECT   id
						,name +  ' [id: '+cast(id as nvarchar(20))+']' name
						,service
						,contractor
						,ISNULL(@point1.STDistance(loc)*0.0012,999) distance
						,fixing
						,towing
						,computer
						,X
						,Y
						,priority
						,case when id=@targetService then 1 else 0 end towingTarget
						,transport
						,parking
						,maxDMC
						,case 
							when @service=2 and serviceType=2 then contactNumber
							when @service=1 and serviceType=3 then contactNumber 
							else '' end  phone
						,avUnavSerFromD
						,avUnavSerToD
						,avUnavLocFromD
						,avUnavLocToD
						,avUnavPartFromD
						,avUnavPartToD
						,city
				FROM dbo.partners_cache pc with(nolock)
				where contactType=case when contractor=1 then 1 else @contactType end 
				and ISNULL(@point1.STDistance(loc)*0.0012,999)<@maxDistance --and sms.sms_id is null
			) a
			GROUP BY id,
				   name,
				   X,
				   Y,
				   priority,
				   city
			end
			else
			begin
				set @maxDistance=200
				--if @platformId=78 set @maxDistance=50

				insert into #partners (
					 partnerId ,
					 partnerName,
					 service,
					 contractor,
					 distance,
					 fixing,
					 towing,
					 computer,
					 X,
					 Y,
					 priority,
					 towingTarget,
					 transport,
					 parking,
					 maxDMC,
					 phoneNumber,
					 partnerServiceId,
					 avUnavSerFromD,
					 avUnavSerToD,
					 avUnavLocFromD,
					 avUnavLocToD,
					 avUnavPartFromD,
					 avUnavPartToD,
					 city
					)
				select id,
					   name + ' [id: '+cast(id as nvarchar(20))+']' name,
					   max(service) service,
					   max(contractor) contractor,
					   min(distance) distance,
					   max(fixing) fixing,
					   max(towing) towing,
					   max(computer) computer,
					   X,
					   Y,
					   priority,
					   max(towingTarget) towingTarget,
					   max(transport) transport,
					   max(parking) parking,
					   isnull(max(maxDMC),9999) maxDMC,
					   dbo.f_partner_contact(@groupProcessInstanceId, id, 1, 2) phone,
					   null,
					   min(avUnavSerFromD) avUnavSerFromD,
					   min(avUnavSerToD) avUnavSerToD,
					   min(avUnavLocFromD) avUnavLocFromD,
					   min(avUnavLocToD) avUnavLocToD,
					   min(avUnavPartFromD) avUnavPartFromD,
					   min(avUnavPartToD) avUnavPartToD,
					   city
				from (
					SELECT   id
							,name+' ('+type+')' name
							,1 service
							,1 contractor
							,ISNULL(@point1.STDistance(loc)*0.0012,999) distance
							,1 fixing
							,1 towing
							,0 computer
							,X
							,Y
							,1 priority
							,1 towingTarget
							,1 transport
							,1 parking
							,4000 maxDMC
							,avUnavSerFromD
							,avUnavSerToD
							,avUnavLocFromD
							,avUnavLocToD
							,avUnavPartFromD
							,avUnavPartToD
							,city
					FROM dbo.partners_services_cache pc with(nolock)
					where  ISNULL(@point1.STDistance(loc)*0.0012,999)<@maxDistance --and sms.sms_id is null					 
					and (type like '%'+@make+'%' or type like '%marki%')
					and 
					(
						(@platformId = 48 and (type like '%business lease%' and dbo.f_exists_in_split(kind,'81')=1)
						and type like '%BL Blach.%') 
						or 
						(@platformId = 78 and type like '%Carefleet BRS 1%')					
					)
					
				) a
				GROUP BY id,
					   name,
					   X,
					   Y,
					   priority,
					   city
				
				
				insert into #resultTable
				select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				from	#partners p left join 
						#resultTable r on p.partnerId=r.partnerId 
				where	r.id is null and (p.distance<50 or @platformId<>78)
				order by p.distance

				if @platformId=78 and not exists(select * from #resultTable)
				begin
					insert into #resultTable
					select top 1  p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join 
							#resultTable r on p.partnerId=r.partnerId 
					where	r.id is null
					order by p.distance

				end



				set @type=1
			end
		end
		else
		begin
			insert into #partners (
				 partnerId ,
				 partnerName,
				 service,
				 contractor,
				 distance,
				 fixing,
				 towing,
				 computer,
				 X,
				 Y,
				 priority,
				 towingTarget,
				 transport,
				 parking,
				 maxDMC,
				 phoneNumber,
				 partnerServiceId,
				 avUnavSerFromD,
				 avUnavSerToD,
				 avUnavLocFromD,
				 avUnavLocToD,
				 avUnavPartFromD,
				 avUnavPartToD,
				 city
				)
			select id,
				   name + ' [id: '+cast(id as nvarchar(20))+']' name,
				   max(service) service,
				   max(contractor) contractor,
				   min(distance) distance,
				   max(fixing) fixing,
				   max(towing) towing,
				   max(computer) computer,
				   X,
				   Y,
				   priority,
				   max(towingTarget) towingTarget,
				   max(transport) transport,
				   max(parking) parking,
				   isnull(max(maxDMC),9999) maxDMC,
				   max(phone) phone,
				   null,
				   min(avUnavSerFromD) avUnavSerFromD,
				   min(avUnavSerToD) avUnavSerToD,
				   min(avUnavLocFromD) avUnavLocFromD,
				   min(avUnavLocToD) avUnavLocToD,
				   min(avUnavPartFromD) avUnavPartFromD,
				   min(avUnavPartToD) avUnavPartToD,
				   city
			from (
				SELECT   id
						,name
						,service
						,contractor
						,ISNULL(@point1.STDistance(loc)*0.0012,999) distance
						,fixing
						,towing
						,computer
						,X
						,Y
						,priority
						,case when id=@targetService then 1 else 0 end towingTarget
						,transport
						,parking
						,maxDMC
						,case 
							when @service=2 and serviceType=2 then contactNumber
							when @service=1 and serviceType=3 then contactNumber 
							else '' end  phone
						,avUnavSerFromD
						,avUnavSerToD
						,avUnavLocFromD
						,avUnavLocToD
						,avUnavPartFromD
						,avUnavPartToD
						,city
				FROM dbo.partners_cache pc with(nolock) left join
					 dbo.sms_outbox sms with(nolock) on sms.sms_number='48'+contactNumber and sms.sms_created>dateadd(minute,-3,getdate()) and sms.sms_text like 'Tu Starter24%TAK%' and @type=0 
				where contactType=case when contractor=1 then 1 else @contactType end 
				and ISNULL(@point1.STDistance(loc)*0.0012,999)<@maxDistance --and sms.sms_id is null
				AND (sms.sms_id is null)
			) a
			GROUP BY id,
				   name,
				   X,
				   Y,
				   priority,
				   city
		end
		
	end
	else
	begin
			
			insert into #partners (
				 partnerId ,
				 partnerName,
				 service,
				 contractor,
				 distance,
				 fixing,
				 towing,
				 computer,
				 X,
				 Y,
				 priority,
				 towingTarget,
				 transport,
				 parking,
				 maxDMC,
				 phoneNumber,
				 partnerServiceId,
				 avUnavSerFromD,
				 avUnavSerToD,
				 avUnavLocFromD,
				 avUnavLocToD,
				 avUnavPartFromD,
				 avUnavPartToD,
				 city
				)
		select id,
			   name + ' [id: '+cast(id as nvarchar(20))+']' name,
			   max(service) service,
			   max(contractor) contractor,
			   min(distance) distance,
			   max(fixing) fixing,
			   max(towing) towing,
			   max(computer) computer,
			   X,
			   Y,
			   priority,
			   max(towingTarget) towingTarget,
			   max(transport) transport,
			   max(parking) parking,
			   isnull(max(maxDMC),9999) maxDMC,
			   max(phone) phone,
			   null,
			   min(avUnavSerFromD) avUnavSerFromD,
			   min(avUnavSerToD) avUnavSerToD,
			   min(avUnavLocFromD) avUnavLocFromD,
			   min(avUnavLocToD) avUnavLocToD,
			   min(avUnavPartFromD) avUnavPartFromD,
			   min(avUnavPartToD) avUnavPartToD,
			   city
		from (

		SELECT   id
					,name
					,service
					,contractor
					,ISNULL(@point1.STDistance(loc)*0.0012,999) distance
					,fixing
					,towing
					,computer
					,X
					,Y
					,priority
					,case when id=@targetService then 1 else 0 end towingTarget
					,transport
					,parking
					,maxDMC
					,case 
						when @service=2 and serviceType=2 then contactNumber
						when @service=1 and serviceType=3 then contactNumber 
						else '' end  phone
					,avUnavSerFromD
					,avUnavSerToD
					,avUnavLocFromD
					,avUnavLocToD
					,avUnavPartFromD
					,avUnavPartToD
					,city
			FROM dbo.partners_cache pc with(nolock) 
			where contactType=case when contractor=1 then 1 else @contactType end 
			and ISNULL(@point1.STDistance(loc)*0.0012,999)<@maxDistance --and sms.sms_id is null
			AND id = @partnerId 
		) a
	
		GROUP BY id,
			   name,
			   X,
			   Y,
			   priority,
			   city
	end

	IF @debugMode = 1
	BEGIN
		SELECT @debugAmount = count(partnerId) FROM #partners
		
		INSERT INTO @debugTable (label, message)
    	VALUES	('PO PIERWSZYM PRZESZUKANIU', '------------------------------------'),
    		('#partners COUNT', CAST(@debugAmount AS NVARCHAR(100)))
    		
	END
    		
	set @ts=datediff(ms,getdate(),@startTime)
	print '03g - '+cast(@ts as varchar(20))
	set @startTime=getdate()


	delete from  #partners 
	where  (dbo.fp_isCorrectPartner(@groupProcessInstanceId, partnerId, @type, @rootId, @platformId, @makeModelProgram, @makeModelType,
		@accidentCrash, @programId, @blacklist, @targetService, @manual, @customLocationTypes) = 0)

	IF @service = 1
	BEGIN		
		DECLARE @fixFailPartnerId INT 
		
		SELECT @fixFailPartnerId = avPartnerId.value_int 
		FROM  dbo.attribute_value avPartnerId with(nolock)
		INNER JOIN dbo.attribute_value avCode with(nolock) on avCode.group_process_instance_id = avPartnerId.group_process_instance_id and avCode.attribute_path = '638,216'
		INNER JOIN dbo.attribute_value fixingOrTowing with(nolock) on fixingOrTowing.group_process_instance_id = avPartnerId.group_process_instance_id and fixingOrTowing.attribute_path = '560'
		INNER JOIN #partners partners on partners.partnerId = avPartnerId.value_int 
		where avPartnerId.attribute_path = '610'
		and avPartnerId.root_process_instance_id = @rootId
		and fixingOrTowing.value_int = 2
		
		IF @fixFailPartnerId IS NOT NULL
		BEGIN			
			insert into #resultTable
			select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
			from #partners
			where partnerId = @fixFailPartnerId and towing = 1
			
			
		END 		
	END
	
	
	IF @debugMode = 1
	BEGIN
		SELECT @debugAmount = count(partnerId) FROM #partners
		
		INSERT INTO @debugTable (label, message)
    	VALUES	('Po odpaleniu fp_isCorrectPartner', '------------------------------------'),
    		('#partners COUNT', CAST(@debugAmount AS NVARCHAR(100)))
    		
	END
	
end
else if @type in (1,98,99)
begin

	if @type=1
	begin
		insert into #partners (
				 partnerId ,
				 partnerName,
				 service,
				 contractor,
				 distance,
				 fixing,
				 towing,
				 computer,
				 X,
				 Y,
				 priority,
				 towingTarget,
				 transport,
				 parking,
				 maxDMC,
				 phoneNumber,
				 partnerServiceId,
				 avUnavSerFromD,
				 avUnavSerToD,
				 avUnavLocFromD,
				 avUnavLocToD,
				 avUnavPartFromD,
				 avUnavPartToD,
				 city,
				 possibilities,
				 type,
				 extCode,
				 heightMax,
				 weightMax
				)
					SELECT id
						  ,name + ' [id: '+cast(id as nvarchar(20))+']' name 
						  ,1 service
						  ,null contractor
						  ,ISNULL(@point1.STDistance(loc)*0.0012,999) distance
						  ,null fixing
						  ,null towing
						  ,null computer
						  ,X
						  ,Y
						  ,0 priority
						  ,null towingTarget
						  ,null transport
						  ,null parking
						  ,null maxDMC
						  ,null phone
						  ,serviceId
						  ,avUnavSerFromD
						  ,avUnavSerToD
						  ,avUnavLocFromD
						  ,avUnavLocToD
						  ,avUnavPartFromD
						  ,avUnavPartToD
						  ,city
						  ,possibilities
						  ,type
						  ,externalCode
 						  ,heightMax
						  ,weightMax
					  FROM dbo.partners_services_cache
					where ISNULL(@point1.STDistance(loc)*0.0012,999)<1000
					AND id = ISNULL(@partnerId, id)

	end
	else
	begin
		insert into #resultTable
		SELECT	 id
				,name 
				,1 priority
				,1 priority2
				,ISNULL(@point1.STDistance(loc)*0.0012,999) distance
				,null distancePriority
				,null phoneNumber
				,null
				,serviceId
				,city
		FROM dbo.partners_services_cache
		where id = ISNULL(@partnerId, id)
		order by 5
	end

	set @ts=datediff(ms,getdate(),@startTime)
	print '03a - '+cast(@ts as varchar(20))
	set @startTime=getdate()

	
	if @type=1
	begin	
		set @ts=datediff(ms,getdate(),@startTime)
		print '03b - '+cast(@ts as varchar(20))
		set @startTime=getdate()

		delete from  #partners 
		where (dbo.fp_isCorrectPartner(@groupProcessInstanceId, partnerId, @type, @rootId, @platformId, @makeModelProgram, @makeModelType,
			@accidentCrash, @programId, @blacklist, @targetService, @manual, @customLocationTypes) = 0)
			
		IF @makeModelId IN (2629,2654)
		BEGIN
			delete from  #partners where type NOT LIKE '%ASO DS specjalne modele%'	
		END
		
		IF @DMC IS NOT NULL
		BEGIN
			delete from  #partners where ISNULL(weightMax,9999999) < @DMC	
		END
		
		PRINT '-----height'
		PRINT @height
		
		IF @height IS NOT NULL
		BEGIN
			delete from  #partners where ISNULL(heightMax,99999.99) < @height	
		END
		
	end

	if @type in (98,99)
	begin
	--select @platformId
		delete from  #resultTable 
		where (dbo.fp_isCorrectPartner(@groupProcessInstanceId, partnerId, @type, @rootId, @platformId, @makeModelProgram, @makeModelType,
			@accidentCrash, @programId, @blacklist, @targetService, @manual, null) = 0)
	end
end
	set @ts=datediff(ms,getdate(),@startTime)
	print '03c - '+cast(@ts as varchar(20))
	set @startTime=getdate()

	if @type=0
	begin
		declare @partnerIdDist as table (partnerId int, X decimal(8,6), Y decimal(8,6), distance decimal(10,3))

		insert into @partnerIdDist(partnerId, X, Y)
		select top 5 partnerId, X, Y from (
		select distinct partnerId,X,Y, distance from #partners ) a
		where  distance<40
		order by distance 

		
		DECLARE @URL VARCHAR(8000) 
		SET @URL = dbo.f_getDomain() + '/get-routes-points?data=0:'+cast(@X as varchar(10))+':'+cast(@Y as varchar(10))+'|'

		select @URL=@URL+cast(partnerId as varchar(20))+':'+cast(X as varchar(10))+':'+cast(Y as varchar(10))+'|'
		from @partnerIdDist


		DECLARE @Response varchar(8000)
		DECLARE @Obj int 
		DECLARE @HTTPStatus int 
		DECLARE @Text as table ( answer varchar(4000) )
 
		EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
		EXEC sp_OAMethod @obj, dbo.f_translate('setTimeouts',default),NULL,5000,5000,5000,5000
		EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
		EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
		EXEC sp_OAMethod @Obj, send, NULL, ''
		EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
	
		INSERT @Text
		EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseText',default)
		EXEC sp_OADestroy @Obj

		update pd
		set pd.distance=o.km
		from @partnerIdDist pd inner join 
		(
		select cast(substring(data,0,charindex(':',data)) as int) id,
				cast(substring(data,charindex(':',data)+1,100) as decimal(18,2)) km  
		from   @text cross apply 
				dbo.f_split(answer,'|')
		) o on o.id=pd.partnerId  and isnull(o.km,0)<>0
	
		UPDATE p
		set		p.distance=pd.distance
		from	#partners p inner join	
				@partnerIdDist pd on pd.partnerId=p.partnerId
				and p.partnerId <> isnull(@fixFailPartnerId,-1)

	end
	
	set @ts=datediff(ms,getdate(),@startTime)
	print '03d - '+cast(@ts as varchar(20))
	set @startTime=getdate()

	-----------------------------------------------------

	DECLARE @diagnosisSummary NVARCHAR(255)
	
   -- czy prosta naprawa?
	SET @diagnosisSummary = dbo.f_diagnosis_summary(@rootId)
		
	IF @addEmpty = 1
	BEGIN
		INSERT INTO #resultTable SELECT null, '---', 0, 0, 0, 0, '',null,null,null
	END 
	
	-- zwróć wszystkie ASO z danej platformy
	if(@type in (98,99))
	BEGIN
		insert into #resultTable
		select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
		from #partners
		where service=1
		order by priority,distance
	END 

	if (@type=0)
	begin
	
		IF @diagnosisSummary = '[PN]' and @service = 2
		BEGIN
			SET @service = 0
		END 
	
		if @service=0 --('Prosta naprawa')
		begin
			
			IF @debugMode = 1
			BEGIN
				INSERT INTO @debugTable (label, message) VALUES	('-------------------------------', 'WESZŁO DO PROSTA NAPRAWA')
			END
	
			print dbo.f_translate('prosta naprawa',default)
			if @platformId=6
			begin
				-- 1
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and distance<=50 and towingTarget=1 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end
			else if @platformId not in (4,9,18,35,36,42,59)
			begin
				-- 1
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and distance<=25 and towingTarget=1 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end
			
			if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
			begin
				
			
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where distance<=50 and fixing=2 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end

			if ((select count(*) from #resultTable)<10)
			begin
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and contractor=1 and (/* @manual = 1 OR */ distance<=35.99) and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end

			if ((select count(*) from #resultTable)<10)
			begin
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and contractor=1 and (/* @manual = 1 OR */ distance<=50.99) and partnerId not in (select partnerId from #resultTable)
				order by distance,priority
			end
			
			if ((select count(*) from #resultTable)<20) AND @manual = 1
			begin
				-- 2,3,4
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and contractor=1 and partnerId not in (select partnerId from #resultTable)
				order by distance
			end
			
		end

		if @service=2 --('Naprawa')
		begin
			
			IF @debugMode = 1
			BEGIN
				INSERT INTO @debugTable (label, message) VALUES	('-------------------------------', 'WESZŁO DO NAPRAWA')
			END
			
			print dbo.f_translate('naprawa',default)
			if @platformId=6
			begin
				-- 1
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and distance<=50 and towingTarget=1 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end
			else if @platformId not in (4,9,18,35,36,42,59)
			begin
				-- 1
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and distance<=25 and towingTarget=1 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end
			
			if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
			begin
			
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where distance<=50 and fixing=2 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end

			if ((select count(*) from #resultTable)<10)
			begin
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and contractor=1 and (/* @manual = 1 OR */ distance<=35.99) and priority<5 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end

			if ((select count(*) from #resultTable)<10)
			begin
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and contractor=1 and (/* @manual = 1 OR */ distance<=50.99) and priority<5  and partnerId not in (select partnerId from #resultTable)
				order by distance,priority
			end
			
			if ((select count(*) from #resultTable)<20) AND @manual = 1
			begin
				-- 2,3,4
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where fixing=1 and contractor=1 and priority<5 and partnerId not in (select partnerId from #resultTable)
				order by distance
			end
		end

			

		if @service=1 or @isTransport=1 -- in ('Holowanie','Transport')
		begin
			
			IF @debugMode = 1
			BEGIN
				INSERT INTO @debugTable (label, message) VALUES	('-------------------------------', 'WESZŁO DO HOLOWANIE / TRANSPORT')
			END
		
			print dbo.f_translate('holowanie',default)
			print @isTransport 
	 
			if @platformId=6
			begin
				-- 1
				print 'a'
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where towing=1 and distance<=50 and towingTarget=1  and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end
			else if @platformId=25
			begin
				-- 1 
				if exists(
					select id from dbo.partners_services_cache with(nolock) where id=@targetService and name like '%Delik%' 
				)
				begin
					insert into #resultTable
					select top 1 partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
					from #partners
					where distance<=60 and partnerId not in (select partnerId from #resultTable) and (partnerName like '%delik%')
					order by distance
				end

				if exists(
				select id from dbo.partners_services_cache with(nolock) where id=@targetService and name like '%lacart%'
				)
				begin
					insert into #resultTable
					select top 1 partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
					from #partners
					where distance<=60 and partnerId not in (select partnerId from #resultTable) and (partnerName like '%lacart%')
					order by distance
				end
			end
			else if @platformId=48
			begin
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where towing=1 and (service=1 or @platformId=48) and maxDMC>=@DMC  /* and (@parkingNeeded=0 or parking=1) and (@isTransport=0 or transport=1)*/ and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end
			else if @platformId IN (79,80,81,82)
			BEGIN
				
				PRINT '-----------------targetService'
				PRINT @targetService
				
				insert into #resultTable
				select partnerId,partnerName,case when partnerId = @targetService then 0 else priority end priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where distance<=30 and towing=1 and service = 1 and maxDMC>=@DMC  /* and (@parkingNeeded=0 or parking=1) and (@isTransport=0 or transport=1)*/ and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
				
				select @resultTableCount = count(*) from #resultTable
				
				if @resultTableCount is null 
				BEGIN
					set @resultTableCount = 0
				END 
				
				if @manual = 0 
				BEGIN
					set @limit = @resultTableCount + @maxAutoRequests
				END 
				
				PRINT '------------------------------limit'
				PRINT @limit
				
			END 
			else if @platformId not in (4,9,18,35,36,42,59)
			begin
				-- 1
				print 'b'
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where towing=1 and distance<=25 and towingTarget=1 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end 
			

			if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
			begin
				insert into #resultTable
				select partnerId,partnerName,1 priority,1 priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where distance<=50 and fixing=2 and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end

			
			if ((select count(*) from #resultTable)<10)
			begin
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where towing=1 and (contractor=1 or @platformId=48) and (/* @manual = 1 OR */ distance<=40) and maxDMC>=@DMC  /* and (@parkingNeeded=0 or parking=1) and (@isTransport=0 or transport=1)*/ and partnerId not in (select partnerId from #resultTable)
				order by priority,distance
			end
			
			if ((select count(*) from #resultTable)<10)
			begin
				-- 2,3,4
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where towing=1 and (contractor=1 or @platformId=48) and (/* @manual = 1 OR */ distance<=50.99) and maxDMC>=@DMC  /*and (@parkingNeeded=0 or parking=1) and (@isTransport=0 or transport=1)*/ and partnerId not in (select partnerId from #resultTable)
				order by distance,priority
			end
			
			if ((select count(*) from #resultTable)<20) AND @manual = 1
			begin
				-- 2,3,4
				insert into #resultTable
				select partnerId,partnerName,priority,priority priority2,distance,distance distancePriority,phoneNumber, NULL, partnerServiceId, city
				from #partners
				where towing=1 and (contractor=1 or @platformId=48) and maxDMC>=@DMC  /*and (@parkingNeeded=0 or parking=1) and (@isTransport=0 or transport=1)*/ and partnerId not in (select partnerId from #resultTable)
				order by distance
			end
		end
		
	end
	else if @type = 1
	begin
		
		DECLARE @damageType int
		declare @firsRegDate datetime
		
		delete from @values	
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '116,244', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @damageType = value_int FROM @values
		set @damageType=isnull(@damageType,0)
		
		DECLARE @workshopType int 
		
		delete from @values	
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '242', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopType = value_int FROM @values
		
		--- fake na potrzeby przekazywania informacji
		IF isnull(@workshopType,-1) = 2
		BEGIN			
			SET @arcCode = '2721497'
		END 
		if @workshopType=4
		begin
			insert into #resultTable
			select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
			from	#partners p left join
								#resultTable r on p.partnerId=r.partnerId
			where	type like '%Stacje%' and
					(p.distance<40)
			order by p.distance
		end 
		else if @programId IN (select id from dbo.vin_program where platform_id=5) -- ADAC
		begin
			set @withCFM=0
			
			if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
			begin
				insert into #resultTable
				select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				from	#partners p left join
									#resultTable r on p.partnerId=r.partnerId
				where	type like '%OPON%' and
						(p.distance<40)
				order by p.distance
			end
			else
			begin
			
				insert into #resultTable
				select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				from	#partners p left join
						#resultTable r on p.partnerId=r.partnerId
				where	(type like '%'+@make+'%') and
						(dbo.f_isProducerService(p.type)=1) --and
						--(
						--(dbo.f_exists_in_split (p.possibilities,80)=1 and @eventType in (1,5)) or
						--(dbo.f_exists_in_split (p.possibilities,81)=1 and @eventType=2) or 
						--((dbo.f_exists_in_split (p.possibilities,80)=1 or dbo.f_exists_in_split (p.possibilities,80)=1) and @eventType is null)
						--)
				order by p.distance

				if ((select count(*) from #resultTable)<10)
				begin
					insert into #resultTable
					select top 20 p.partnerId,p.partnerName+' (wszystkie marki)' partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
							#resultTable r on p.partnerId=r.partnerId
					where	(type like '%marki%') and
					        (dbo.f_isProducerService(p.type)=1) and
							(
							(dbo.f_exists_in_split (p.possibilities,80)=1 and @eventType in (1,5)) or
							(dbo.f_exists_in_split (p.possibilities,81)=1 and @eventType=2) or 
							((dbo.f_exists_in_split (p.possibilities,80)=1 or dbo.f_exists_in_split (p.possibilities,80)=1) and @eventType is null)
							)
					order by p.distance
				end
			end

		end 
		else if @programId IN (select id from dbo.vin_program where platform_id=48) and @eventType in (1,5) -- WSZYSTKO Z BL
		begin

			insert into #resultTable
			select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
			from	#partners p left join
					#resultTable r on p.partnerId=r.partnerId
			--where	type like '%Business Lease%' and 
			--		(type like '%'+@make+'%') and
			--		dbo.f_exists_in_split (p.possibilities,80)=1 
			--order by p.distance

			where   (p.type like '%business lease%' and dbo.f_exists_in_split(p.possibilities,'81')=1) 
					and (p.type like '%'+@make+'%' or p.type like '%marki%') 
					and p.type like '%BL Blach.%'
			order by p.distance

		end
		else if @programId IN (select id from dbo.vin_program where platform_id=48) and @eventType not in (1,5) -- WSZYSTKO Z BL
		begin

			set @withCFM=0
			
			if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
			begin

				insert into #resultTable
				select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				from	#partners p left join
									#resultTable r on p.partnerId=r.partnerId
				where	type like '%bl%opony%prio%1%' 
				order by p.distance

				if ((select count(*) from #resultTable)<3)
				begin
					insert into #resultTable
					select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
							#resultTable r on p.partnerId=r.partnerId
					where	type like '%bl%opony%prio%2%'
					order by p.distance
				end

				if ((select count(*) from #resultTable)<3)
				begin
					insert into #resultTable
					select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
							#resultTable r on p.partnerId=r.partnerId
					where type like '%wulka%' and	 type like '%business%'		
					order by p.distance
				end
			end
			else
			begin
				--a. Jeżeli marka pojazdu bez obsługi S24 ale na gwarancji (data zdarzenia – data 1 rejestracji =< do 2 lat) 
				--   wybieramy serwis ASO danej marki z grupy BL. Lista pokaże jedynie serwisy Businesslease z grupy BL ASO w promieniu do 100km.
				--b. Jeżeli pojazd bez obsługi S24 i pojazd po gwarancji (data zdarzenia – data 1 rejestracji > niż 2 lata) 
				--   to system sprawdza w bazie pojazdów kolumnę O „grupa” i jeżeli jest wpis:
 
				--   MR01 – Alternatywna – zabieramy do serwisu z grupy BL standardowe w promieniu do 25km,  
				--   jeżeli serwis znajduje się dalej to holujemy do najbliższego ASO z grupy BL ASO 
				--   (autoryzowany serwis danej marki współpracujący z businesslease).
				--   Jeżeli inny niż MR01 – Alternatywna - zabieramy pojazd do Aso z grupy BL ASO 
				--   (autoryzowany serwis danej marki współpracujący z businesslease)
				--select 666
				delete from @values	
				INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '74,561', @groupProcessInstanceId = @groupProcessInstanceId
				
				SELECT @firsRegDate = value_date FROM @values
				
				if @firsRegDate>dateadd(year,-2,getdate())
				begin
					PRINT '----------------------aaaaaaaaaaaa-00000000'
					insert into #resultTable
					select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
							#resultTable r on p.partnerId=r.partnerId
					where	type like '%Business Lease%' and 
							(type like '%'+@make+'%') and
							dbo.f_exists_in_split (p.possibilities,80)=1 
					order by p.distance

					if ((select count(*) from #resultTable)<10)
					begin
						insert into #resultTable
						select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
								#resultTable r on p.partnerId=r.partnerId
						where	type like '%Business Lease%' and 
								(type like '%marki%') and
								dbo.f_exists_in_split (p.possibilities,80)=1 
						order by p.distance
					end
				end
				else
				begin
					PRINT '----------------------aaaaaaaaaaaa-MRRRRRRRRR'
					declare @groupBL nvarchar(100)
					exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('Grupa',default), @output=@groupBL OUTPUT
					--select 55
					if @groupBL LIKE 'MR01%Alternatywna'
					begin
						
						PRINT '----------------------aaaaaaaaaaaa-11111111111111111'
						
						insert into #resultTable
						select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
								#resultTable r on p.partnerId=r.partnerId
						where	type like '%BL Standard%' and 
								(type like '%'+@make+'%' or type like '%marki%') and
								type not like '%szyb%' and
								p.distance<25
						order by p.distance
						
						if ((select count(*) from #resultTable)<1)
						begin
							PRINT '----------------------aaaaaaaaaaaa-22222222222222'
							insert into #resultTable
							select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join
									#resultTable r on p.partnerId=r.partnerId
							where	type like '%BL ASO%%' and 
									(type like '%'+@make+'%' or type like '%marki%') and
									type not like '%szyb%'
								--	(type like '%ASO%' or type like '%PS%') 
							order by p.distance
						end
					end
					else
					begin
						insert into #resultTable
						select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
								#resultTable r on p.partnerId=r.partnerId
						where	type like '%BL ASO%' and 
								(type like '%'+@make+'%' or type like '%marki%') and
								type not like '%szyb%'
							--	(type like '%ASO%' or type like '%PS%')
						order by p.distance

					end
				end
			end  
		end
		else if @programId IN (select id from dbo.vin_program where platform_id=43) -- WSZYSTKO Z ALD
		begin
			set @withCFM=0

			declare @province nvarchar(100)
			declare @caseCity nvarchar(100)
			declare @ALDServiceName nvarchar(100)
			declare @provincePath nvarchar(100)
			declare @driveableALD int
			set @provincePath=@startLocationPath+',88'
			

			delete from @values	
			INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = @provincePath, @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @province = value_string FROM @values

			delete from @values	
			INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '101,85,87', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @caseCity = value_string FROM @values

			declare @fleet nvarchar(100)
			exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('Klient',default), @output=@fleet OUTPUT

			declare @ALDProgram nvarchar(100)
			exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('ProgramALD',default), @output=@ALDProgram OUTPUT

			DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '116,119', @groupProcessInstanceId = @groupProcessInstanceId -- jezdne/niejezdne
				SELECT	@driveableALD=value_int FROM @values

			IF @eventType in (1,5) -- wypadek/szkoda
			BEGIN
					
	-- Opis @damageType: 
	--			1	Szybowa (tylko szyba) /*w zaleznosci czy jezdne niejezdne od miejsca uzytkowania lub zdarzenia do najblizszego Nord Glass*/
	--			7	Szkoda standardowa 
	--			6	Drobne uszkodzenie (takie jak rysy, delikatne przetarcia, drobne wgniecenia)


				
						       
				--select @damageType
				if @arcCode like '11910%' set @damageType=1
				--set @damageType=2
				if @damageType=1 or @arcCode like '119109%' /*Szyby: szkody szybowe kierowalność wyłącznie do najbliższego  Nord Glass.*/
				--begin
				--		insert into #resultTable
				--		select top 1 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				--		from	#partners p left join
				--							#resultTable r on p.partnerId=r.partnerId
				--		where type like '%szyb%' and 
				--										 (p.partnerName like '%nord%') 
				--		order by p.distance

				--end
				-------------
				begin
					if  @driveableALD=1
					begin
						insert into #resultTable
						select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
											#resultTable r on p.partnerId=r.partnerId
						where type like '%szyb%' and 
								(p.partnerName like '%nord%')
				
						order by p.distance
						
				
					end
					else 
					begin
						insert into #resultTable
						select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
											#resultTable r on p.partnerId=r.partnerId
						where type like '%szyb%' and 
								( p.partnerName like '%nord%')
								
						order by p.distance
						
				
					end
				end



				----------------------

				--begin
				--	if  @driveableALD=1
				--	begin
				--		insert into #resultTable
				--		select top 1 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				--		from	#partners p left join
				--							#resultTable r on p.partnerId=r.partnerId
				--		where type like '%szyb%' and 
				--				(p.partnerName like '%saint%' or p.partnerName like '%nord%')
				--				 (p.partnerName like '%Saint-Gobain Centrala%' or p.partnerName like '%Nordglass Centrala%') 
				--		order by NEWID()---p.distance
						
				
				--	end
				--	else 
				--	begin
				--		insert into #resultTable
				--		select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				--		from	#partners p left join
				--							#resultTable r on p.partnerId=r.partnerId
				--		where type like '%szyb%' and 
				--				(p.partnerName like '%saint%' or p.partnerName like '%nord%')
								
				--		order by p.distance
						
				
				--	end
				--end
				else if @damageType in (6) -- drobna
				---- jesli województwo Dolnośjąskie i Wielkopolskie Pomorskie Małopolskie Ślaskie 'łódzkie Mazowiecki do konkretnego S-plusa, pozostałe wyjewództwa do najbliższego szkodowego
				/*2019-04-29 Dla flot które są kierowane do Dyszkiewicza na terenie Mazowieckiego (Nutricia, Danone, Żywiec Zdrój) proszę ustawić kierowalność do tego serwisu również jak jest zaznaczone szkoda drobna.*/
				begin
			
					
					if @province in ('Wielkopolskie','Dolnośląskie','Pomorskie','Małopolskie','Śląskie','Łódzkie','Mazowieckie') and @ALDServiceName is null
					begin
												
						select  top 1 @ALDServiceName=serviceName
						from	dbo.ALD_servicesRules
						where	(province=@province or province='%') and
								(fleet like '%'+@fleet+'%' or fleet is null) and
								 program=dbo.f_translate('Drobne',default) and
								(city=@city or city='%') 
		
								
						order by priority, make desc, fleet desc
					--select dbo.f_translate('serwisname',default)	
					--select @ALDServiceName
						--select dbo.f_translate('jaki serwis woj',default)
						--select @ALDServiceName, @province
						insert into #resultTable
						select top 1 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
								#resultTable r on p.partnerId=r.partnerId
						where	(type like '%ALD%') and 
								(p.partnerName like '%'+@ALDServiceName+'%') and
								r.id is null
						order by p.distance





					end
					else 
					--begin --pozostałe wyjewództwa to S-Plus najbliższy ->  pozostałe wyjewództwa do najbliższego szkodowego
					--	insert into #resultTable
					--	select top 1 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					--	from	#partners p left join
					--						#resultTable r on p.partnerId=r.partnerId

					--	where	type like '%warsztaty S-Plus%'
					--	order by p.distance
					--end 
					---
					
					begin		--pozostałe wyjewództwa to S-Plus najbliższy ->  pozostałe wyjewództwa do najbliższego szkodowego	
					
						select  top 1 @ALDServiceName=serviceName
						from	dbo.ALD_servicesRules
						where	(province=@province or province='%') and
								(fleet like '%'+@fleet+'%' or fleet is null) and
								(program=@ALDProgram or program is null) and
								(city=@city or city='%') and   active=1 and
								(make=@make or make='%') 
						order by priority, make desc, fleet desc
				
						insert into #resultTable
						select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
								#resultTable r on p.partnerId=r.partnerId
						where	(type like '%ALD%') and 
								(p.partnerName like '%'+@ALDServiceName+'%') and
								r.id is null
						order by p.distance
					end
										
					--insert into #resultTable
					--select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					--from	#partners p left join
					--					#resultTable r on p.partnerId=r.partnerId

					--where	type like '%warsztaty S-Plus%'
					--order by p.distance
				end
				else --			7	Szkoda standardowa 
								--start copy
				begin
				print '----------------------------------ccccccccccccccccccccccccccccccc  @damageType'
				---select @damageType
					select  top 1 @ALDServiceName=serviceName
					from	dbo.ALD_servicesRules
					where	(province=@province or province='%') and
							(fleet like '%'+@fleet+'%' or fleet is null) and
							(program=@ALDProgram or program is null) and  active=1 and
							(city=@city or city='%') and
							(make=@make or make='%') 
					order by priority, make desc, fleet desc
					/*Popr 2151 Opolskie:  Wszystkie marki kierujemy do CMP Holding Wrocław ALDSZKODY044. Obecnie pojazdy są kierowany do serwisów z sąsiednich województw w zależności co bliżej. */
					--if @province=dbo.f_translate('opolskie',default) and @ALDServiceName is null
					--begin
					--	set @province=dbo.f_translate('śląskie',default)
						
					--	select  top 1 @ALDServiceName=serviceName
					--	from	dbo.ALD_servicesRules
					--	where	(province=@province or province='%') and
					--			(fleet like '%'+@fleet+'%' or fleet is null) and
					--			(program=@ALDProgram or program is null) and
					--			(city=@city or city='%') and
					--			(make=@make or make='%') 
					--	order by priority, make desc, fleet desc
					--end
					-----
					--if @groupProcessInstanceId =  3256246
					--begin
					--	select   *
					--	from	dbo.ALD_servicesRules
					--	where	(province=@province or province='%') and
					--			(fleet like '%'+@fleet+'%' or fleet is null) and
					--			(program=@ALDProgram or program is null) and
					--			(city=@city or city='%') and
					--			(make=@make or make='%') 
					--	order by priority, make desc, fleet desc
					--end
					------
					--****Dwa serwisy Krotoski-Cichy w Zachodniopomorskim************---
					if @province=dbo.f_translate('zachodniopomorskie',default) and @ALDServiceName like '%Krotoski-Cichy%'
					begin
						print '22222222222222222222222222222'
						insert into #resultTable
						select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
								#resultTable r on p.partnerId=r.partnerId
						where	p.partnerName like '%Krotoski-Cichy%' and type like '%ALD%' and p.partnerName like '%ALDSZKODY%' and
								r.id is null
						order by p.distance


					end
					else 
					begin
				
					
						--****************---
						print 'ALD --- ALD --- ALD --- ALD --- ALD --- ALD --- ALD --- ALD --- ALD --- ALD'
						print @province
						print @fleet
						print @ALDProgram
						print @city
						print @make
						print @ALDServiceName
						print 'ALD --- ALD --- ALD --- ALD --- ALD --- ALD --- ALD --- ALD --- ALD --- ALD'

						if @ALDServiceName=dbo.f_translate('ASO Ford',default)
						begin
							insert into #resultTable
							select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join
									#resultTable r on p.partnerId=r.partnerId
							where	type like '%ASO Ford%' and
									r.id is null
							order by p.distance
						end
						else

						begin					
							insert into #resultTable
							select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join
									#resultTable r on p.partnerId=r.partnerId
							where	(type like '%ALD%') and 
									(p.partnerName like '%'+@ALDServiceName+'%') and
									r.id is null
							order by p.distance
						end
					end
				end
				---koniec kopy
			end
			else
			begin
				-- Dostawca_opon
				if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
				begin
					declare @tire nvarchar(100)
					
					exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName='Dostawca_opon', @output=@tire OUTPUT
					
					if @tire like '%Continental%' set @tire=dbo.f_translate('Continental',default)
					else if @tire like '%euromaster%' set @tire=dbo.f_translate('euromaster',default)
					else if @tire like '%bridgestone%' set @tire=dbo.f_translate(dbo.f_translate('bridgestone',default),default)
					else set @tire='xxxxx'

					
					insert into #resultTable
					select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	(p.partnername like '%'+@tire+'%' or (p.type like '%ALD%' and p.type like '%wulkani%' and @tire='xxxxx'))  and
								r.id is null
					order by p.distance



				end
				else
				begin
				
					insert into #resultTable 
					select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
							#resultTable r on p.partnerId=r.partnerId
					where	(type like '%ALD%') and 
							dbo.f_exists_in_split (p.possibilities,80)=1 and
							(type like '%'+@make+'%' or type like '%marki%') and
								r.id is null
					order by p.distance
					
				end
			end
		end
		else if @programId IN (select id from dbo.vin_program where platform_id in (25,75)) -- WSZYSTKO Z LP
		begin
		
			set @withCFM=0
			IF @eventType in (1,5) -- wypadek/szkoda
			BEGIN


				declare @regNumber nvarchar(20)
				delete from @values	
				INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @regNumber = value_string FROM @values


				declare @fleetA nvarchar(100)
				exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('Wlasciciel',default), @output=@fleetA OUTPUT
				
				--grupa: Serwis szkodowy -> LeasePlan priorytetowy
				--(z dodatkową możliwością wpisania adresu przybywania przez Użytkownika i wyboru najbliższego od tej lokalizacji)	
			
				if @damageType=1 or @arcCode like '119109%' -- szybowa
				begin
					insert into #resultTable
					select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	type like '%Leaseplan%' and
							type like '%SZYB%' 
					order by p.priority, p.distance
				end
				else if left(@regNumber,2) in ('WD','WW') and @fleetA like '%cola%'
				begin 
					insert into #resultTable
					select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
							#resultTable r on p.partnerId=r.partnerId
					where	type like '%LP WTW%' 
					order by p.distance
				end
				else
				begin
					--
					-- Jeżeli odległość miejsca zdarzenia jest mniejsza niż 50km od serwisu Lacart (wrocław) lub Delik (Przeźmierowo) – w pierwszej kolejności staramy się wysłać Lacart lub Delik na holowanie. 
				
					insert into #resultTable
					select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	type like '%Leaseplan sz%' and
							(p.partnerName like '%lacart%' or p.partnerName like '%delik%') and
							dbo.f_exists_in_split (p.possibilities,81)=1 and
							p.distance<50
					order by p.distance
					
					if ((select count(*) from #resultTable)<10)
					begin
						insert into #resultTable
						select p.partnerId,p.partnerName partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
											#resultTable r on p.partnerId=r.partnerId
						where	type like '%Leaseplan sz%' and
								dbo.f_exists_in_split (p.possibilities,81)=1 and
								(type like '%'+@make+'%') and type not like '%rezerwo%' and
								 p.distance<100
						order by p.distance
					end
				
					if ((select count(*) from #resultTable)<10)
					begin
						insert into #resultTable
						select  top 10 p.partnerId,p.partnerName+' (Wszystkie marki)' partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join 
								#resultTable r on p.partnerId=r.partnerId 
						where	type like '%Leaseplan sz%' and type not like '%rezerwo%' and 
								type not like '%OPONY%' and
								(type not like '%STACJE%') and
								dbo.f_exists_in_split (p.possibilities,81)=1 and
								type like '%marki%' and
								p.distance<100
						order by p.distance
					end

					if ((select count(*) from #resultTable)<20)
					begin
						insert into #resultTable
						select  top 100 p.partnerId,p.partnerName+' (Wszystkie marki)' partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join 
								#resultTable r on p.partnerId=r.partnerId 
						where	type like '%Leaseplan sz%' and type not like '%rezerwo%' and
								type not like '%OPONY%' and
								(type not like '%STACJE%') and
								dbo.f_exists_in_split (p.possibilities,81)=1 and
								(type like '%'+@make+'%' or type like '%marki%') and
								p.distance<999
						order by p.distance
					end
				end
			end
			else
			begin
				PRINT '------------------------aaaaaaaaaa-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx'
				if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
				begin
					
					insert into #resultTable
					select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join 
							#resultTable r on p.partnerId=r.partnerId 
					where	type like '%Leaseplan%' and type like '%wulk%'
					order by p.distance
				end
				else
				begin
					
					--delete from @values	
					--INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '74,561', @groupProcessInstanceId = @groupProcessInstanceId
					--SELECT @firsRegDate = value_date FROM @values

					
					set @consult=0

					EXEC dbo.p_lp_consult_workshop_routing 
							@groupProcessInstanceId = @groupProcessInstanceId, 
							@consult = @consult output

					declare @producerProgram nvarchar(100)
					set @producerProgram=dbo.f_producer_program (@programIds)
					
					if @consult=1
					begin
					
						insert into #resultTable
						select p.partnerId,p.partnerName+' (Status 1)',3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
								#resultTable r on p.partnerId=r.partnerId
						where	type like '%Leaseplan%' and
								type not like '%SZYB%' and
								type not like '%OPONY%' and
								type like '%LP Status 1%' and
								p.distance<50  and 
								(type like '%'+@make+'%' or type like '%marki%') -- and 0=1
						order by p.distance

						if ((select count(*) from #resultTable)<5) and @producerProgram is null
						begin
							insert into #resultTable
							select  top 5 p.partnerId, p.partnerName+' ('+case when type like '%LP Status 1%' then 'Status 1' else 'Status 4' end+ ')',3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join 
									#resultTable r on p.partnerId=r.partnerId 
							where	type like '%Leaseplan%' and
									type not like '%OPONY%' and
									type like '%LP Status%' and
									(type like '%'+@make+'%' or type like '%marki%') and 
									p.distance<150
							order by p.distance
						end

						declare @err2 int
						declare @message2 nvarchar(300)

						EXEC [dbo].[p_attribute_edit]
		   							@attributePath = '238', 
		   							@groupProcessInstanceId = @groupProcessInstanceId,
		   							@stepId = 'xxx',
		   							@userId = 1,
		   							@originalUserId = 1,
		   							@valueInt = 1,
		   							@err = @err2 OUTPUT,
		   							@message = @message2 OUTPUT

						if ((select count(*) from #resultTable)<5) 
						begin
							insert into #resultTable
							select  top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join 
									#resultTable r on p.partnerId=r.partnerId 
							where	type like '%(Inter cars)%' and
									(type like '%'+@make+'%' or type like '%marki%')
							order by p.distance

							if ((select count(*) from #resultTable)= 0) 
							begin
								
								EXEC [dbo].[p_attribute_edit]
		   							@attributePath = '238', 
		   							@groupProcessInstanceId = @groupProcessInstanceId,
		   							@stepId = 'xxx',
		   							@userId = 1,
		   							@originalUserId = 1,
		   							@valueInt = 0,
		   							@err = @err2 OUTPUT,
		   							@message = @message2 OUTPUT
							end
						end 

						if ((select count(*) from #resultTable)<5) and @producerProgram is null
						begin
						
							insert into #resultTable
							select  top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join 
									#resultTable r on p.partnerId=r.partnerId 
							where	(type like '%'+@make+'%')
							order by p.distance
						end
					end
--					else if @consult=2
--					begin	
--						insert into #resultTable
--						select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
--						from	#partners p left join
--											#resultTable r on p.partnerId=r.partnerId
--						where	type like '%OPONY%' and
--								(p.distance<100)
--						order by p.distance
--					end
					else
					begin
					
						insert into #resultTable
						select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
											#resultTable r on p.partnerId=r.partnerId
						where	type like '%Leaseplan%' and
								dbo.f_exists_in_split (p.possibilities,80)=1
							and type like '%'+@make+'%'
						order by p.distance
				
						if ((select count(*) from #resultTable)<1)
						begin
							insert into #resultTable
							select  top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join 
									#resultTable r on p.partnerId=r.partnerId 
							where	type like '%Leaseplan%' and
									dbo.f_exists_in_split (p.possibilities,80)=1 and
									type like '%marki%'
							order by p.distance
						end			
					end

					
				end


			end

		end
		else if @programId IN (select id from dbo.vin_program where platform_id in (53,58)) -- WSZYSTKO Z Alphabetu (53,58)
		begin
		
			set @withCFM=0
			IF @eventType in (1,5) -- wypadek/szkoda
			BEGIN
				
				IF @debugMode = 1
				BEGIN
					INSERT INTO @debugTable (label, message) VALUES	('-------------------------------', 'Kierowalność ALPHABET (SZKODA)')
				END
			
				------------------------------------------------ Kierowalność ALPHABET (SZKODA) --------------------------------------------------------
				
				DECLARE @OCAC int
				DECLARE @driveable int
				DECLARE @relevant int
				DECLARE @headerIds nvarchar(200)
				
	
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '116,119', @groupProcessInstanceId = @groupProcessInstanceId -- jezdne/niejezdne
				SELECT	@driveable=value_int FROM @values
	
				-- Opis @damageType: 
	--			1	Szybowa (tylko szyba)
	--			2	Wyłącznie zarysowania zderzaków (przód/tył) bez naruszania ciągłości elementu (pęknięcie/rozerwanie/dziury) S- PLIS
	--			3	Wyłączenie uszkodzenia progów lub błotników tylnych pojazdu – ASN
	--			4	Pozostałe (uszkodzenia większe ale nie wymagające holowania
	--			5	Holowanie – uszkodzenia wielu elementów blacharskich lub drobne uszkodzenia ale uniemożliwiające dalsze poruszanie się pojazdem
				--select @damageType
				IF @damageType IN (2,3)
				BEGIN
					-- Szkoda nie istotna
					SET @relevant = 0
				END
				ELSE
				BEGIN
					-- Szkoda istotna
					SET @relevant = 1	
				END
				
				
				declare @alphabetPrefferedService nvarchar(200)
				select @alphabetPrefferedService=value
				from	dbo.vin_element e 
				where header_id in (
					select data from dbo.f_split(@headerIds,',')
				) and dictionary_id=2961
	

				if @makeModelV like 'BMW%' or @makeModelV like 'Mini%'  
				begin
				
					IF @debugMode = 1
					BEGIN
						INSERT INTO @debugTable (label, message) VALUES	('BMW MINI', '-------')
					END
				
					if @driveable=0
					begin
						if @relevant=1
						begin
							--BMW MINI niejezdne istotne
							--Multimake OC niejezdne
							if isnull(@alphabetPrefferedService,'')<>''
							begin
								--czy dany pojazd w bazie pojazdów w kolumnie "Preferoway Serwis" ma zapisany jakiś preferowany serwis do obsługi, jeśli tak to
								--czy w jakiejkolwiek z grup (NW18, NW19, NW23, NW24, NW26, NW31) w promieniu 100km znajduje się serwis (ten sam, który widnieje w bazie pojazdów w kolumnie "Preferowany Serwis"), jeśli tak to
								--dedykowany preferowany serwis w promieniu 100km, jeśli go nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,1 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.extCode like '%ALP:'+@alphabetPrefferedService+'%' and
										p.distance<100 and
										dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
										r.id is null
								order by distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
								--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest preferowany serwis BMW / MINI w promieniu 100km, jeśli tak to
								--dowolny serwis BMW / MINI, jeśli nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and /*z,m*/
										dbo.f_exists_in_split(possibilities,'113')=1 and
										type like '%'+@make+'%' and
										r.id is null
								order by distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
								--czy w grupie NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją) jest serwis w promieniu 100km
								--najbliższy serwis dedykowany w promieniu 100km jeśli takowy jest, jeśli nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and
										dbo.f_exists_in_split(possibilities,'113')=1 and
										r.id is null
								order by p.distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
								--serwisy z 2 poniższych grup                                                                                                                             1. NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją)                                                                                      2. NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji)                                                                              
								--najbliższy serwis z 2 w/w grup do 50km, jeśli takowego brak - powinien pojawic się komunikat o konieczności holowania pojazdu na parking pomocy drogowej, dodatkowo patrz niżej
	
								--czy w grupie NW26-Naprawa blach.-lak. ALPHABET (Poszerzony obszar obsługi BMW MINI) jest serwis BMW MINI o poszerzonym obszarze działania
								--powinien pojawić się komunikat o konieczności zlecenia organizacji podstawienia pojazdu zastępczego oraz odbioru uszkodzonego pojazdu przez ASO BMW MINI o poszerzonym obszarze działania
								insert into #resultTable
								select p.partnerId,p.partnerName,4 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<50 and
										dbo.f_exists_in_split(possibilities,'112')=1 and
										r.id is null
								order by p.distance
							end
						end
						else
						begin
							--BMW MINI niejezdne nieistotne
							if isnull(@alphabetPrefferedService,'')<>''
							begin
								--czy dany pojazd w bazie pojazdów w kolumnie "Preferoway Serwis" ma zapisany jakiś preferowany serwis do obsługi, jeśli tak to
								--czy w jakiejkolwiek z grup (NW18, NW19, NW23, NW24, NW26, NW31) w promieniu 100km znajduje się serwis (ten sam, który widnieje w bazie pojazdów w kolumnie "Preferowany Serwis"), jeśli tak to
								--dedykowany preferowany serwis w promieniu 100km, jeśli go nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,1 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	extCode like '%ALP:'+@alphabetPrefferedService+'%' and
										p.distance<100 and
										dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
										r.id is null
								order by p.distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
								--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest preferowany serwis BMW / MINI w promieniu 100km, jeśli tak to
								--dowolny serwis BMW / MINI, jeśli nie ma to patrz niżejinsert into #resultTable
								select p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and
										dbo.f_exists_in_split(possibilities,'113')=1 and
										type like '%'+@make+'%' and
										r.id is null
								order by distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
								--czy w grupie NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją) jest serwis w promieniu 100km
								--najbliższy serwis dedykowany w promieniu 100km jeśli takowy jest, jeśli nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and
										dbo.f_exists_in_split(possibilities,'113')=1 and
										r.id is null
								order by distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
								--serwisy z grupy NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji) 
								--najbliższy serwis z w/w grupy
								insert into #resultTable
								select p.partnerId,p.partnerName,4 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<50 and
										dbo.f_exists_in_split(possibilities,'112')=1 and
										r.id is null
								order by distance
							end
						end
					end
					else
					begin
						--BMW MINI AC OC jezdne
						if isnull(@alphabetPrefferedService,'')<>''
						begin
							--czy dany pojazd w bazie pojazdów w kolumnie "Preferoway Serwis" ma zapisany jakiś preferowany serwis do obsługi, jeśli tak to
							--czy w jakiejkolwiek z grup (NW18, NW19, NW23, NW24, NW26, NW31) w promieniu 100km znajduje się serwis (ten sam, który widnieje w bazie pojazdów w kolumnie "Preferowany Serwis"), jeśli tak to
							--dedykowany preferowany serwis w promieniu 100km, jeśli go nie ma to patrz niżej
							insert into #resultTable
							select p.partnerId,p.partnerName,1 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join 
									#resultTable r on p.partnerId=r.partnerId 
							where	p.extCode like '%ALP:'+@alphabetPrefferedService+'%' and
									p.distance<100 and
									dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
									r.id is null
							order by p.distance
						end
	
						if ((select count(*) from #resultTable)<5)
						begin
							--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest preferowany serwis BMW / MINI w promieniu 100km, jeśli tak to
							--dowolny serwis BMW / MINI, jeśli nie ma to patrz niżejinsert into #resultTable
							select p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join 
									#resultTable r on p.partnerId=r.partnerId 
							where	p.distance<100 and
									dbo.f_exists_in_split(possibilities,'113')=1 and
									p.type like '%'+@make+'%' and
									r.id is null
							order by p.distance
						end
	
						if ((select count(*) from #resultTable)<5)
						begin
							--czy w grupie NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją) jest serwis w promieniu 100km
							--najbliższy serwis dedykowany w promieniu 100km jeśli takowy jest, jeśli nie ma to patrz niżej
							insert into #resultTable
							select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join 
									#resultTable r on p.partnerId=r.partnerId 
							where	p.distance<100 and
									dbo.f_exists_in_split(possibilities,'113')=1 and
									r.id is null
							order by distance
						end
	
						if ((select count(*) from #resultTable)<5)
						begin
							--serwisy z grupy NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji) 
							--najbliższy serwis z w/w grupy
							insert into #resultTable
							select p.partnerId,p.partnerName,4 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							from	#partners p left join 
									#resultTable r on p.partnerId=r.partnerId 
							where	p.distance<50 and
									dbo.f_exists_in_split(possibilities,'112')=1 and
									r.id is null
							order by p.distance
						end
					end
				end
				else
				begin
					DELETE FROM @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '116,201', @groupProcessInstanceId = @groupProcessInstanceId -- OC/AC
					SELECT	@OCAC=value_int FROM @values
	
					-- OC == 1
					-- AC == 2
			
					
					IF @debugMode = 1
					BEGIN
						INSERT INTO @debugTable (label, message) VALUES	('INNE NIŻ BMW MINI', '-------');
					END
					
					if @OCAC=1
					begin
					
						IF @debugMode = 1
						BEGIN
							INSERT INTO @debugTable (label, message) VALUES	('OC', '----'), ('@driveable', CAST(@driveable AS NVARCHAR(10)));
						END
					
						if @driveable=1
						begin
		
							--Multimake OC jezdne
							if isnull(@alphabetPrefferedService,'')<>''
							begin
								--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest jakiś serwiś w promieniu 100km, jeśli tak to
								--czy wśród serwisów z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) w promieniu 100km znajduje się serwis obsługujący taką samą markę jak pojazd, dla którego następuje weryfikacja
								--wszyskie serwisy obsługujące daną markę, jeśli nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,1 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	extCode like '%ALP:'+@alphabetPrefferedService+'%' and
										type not like '%zawieszone%' and
										p.distance<100 and
										dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
										r.id is null
								order by p.distance
							end

							if ((select count(*) from #resultTable)<5)
							begin
								--wszystkie serwisy z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany)
								--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest jakiś serwiś w promieniu 100km, jeśli nie to
								insert into #resultTable
								select top 5 p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and
										type not like '%zawieszone%' and
										dbo.f_exists_in_split(possibilities,'113')=1 and
										(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
										r.id is null
								order by p.distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
		
								--czy w grupie NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją) jest jakiś serwis w promieniu 100km
								--wszystkie serwisy w promieniu 100km jeśli takowe są, jeśli ich nie ma to patrz niżej
								insert into #resultTable
								select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and
										dbo.f_exists_in_split(possibilities,'108')=1 and
										type not like '%zawieszone%' and
										(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
										r.id is null
								order by p.distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
								--czy w grupie NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji) znajduje się jakiś serwis
								--najbliższy serwis z w/w grupy
								insert into #resultTable
								select top 5 p.partnerId,p.partnerName,4 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	dbo.f_exists_in_split(possibilities,'109')=1 and
										type not like '%zawieszone%' and
										r.id is null
								order by p.distance
							end
						end
						else
						begin

							
							--Multimake OC niejezdne
							if isnull(@alphabetPrefferedService,'')<>''
							begin
								--czy dany pojazd w bazie pojazdów w kolumnie "Preferoway Serwis" ma zapisany jakiś preferowany serwis do obsługi, jeśli tak to
								--czy w jakiejkolwiek z grup (NW18, NW19, NW23, NW24, NW26, NW31) w promieniu 100km znajduje się serwis (ten sam, który widnieje w bazie pojazdów w kolumnie "Preferowany Serwis"), jeśli tak to
								--dedykowany preferowany serwis w promieniu 100km, jeśli go nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,1 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	extCode like '%ALP:'+@alphabetPrefferedService+'%' and
										p.distance<100 and
										type not like '%zawieszone%' and
										dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
										r.id is null
								order by p.distance

							end
							
							if ((select count(*) from #resultTable)<5)
							begin
								--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest jakiś serwiś w promieniu 100km, jeśli tak to
								--czy wśród serwisów z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) w promieniu 100km znajduje się serwis obsługujący taką samą markę jak pojazd, dla którego następuje weryfikacja
								--wszyskie serwisy obsługujące daną markę, jeśli nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and
										dbo.f_exists_in_split(possibilities,'113')=1 and
										type not like '%zawieszone%' and
										(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
										r.id is null
								order by p.distance
							end
							
							if ((select count(*) from #resultTable)<5)
							begin
								--wszystkie serwisy z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany)
								--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest jakiś serwiś w promieniu 100km, jeśli nie to
								insert into #resultTable
								select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and
										dbo.f_exists_in_split(possibilities,'113')=1 and
										type not like '%zawieszone%' and
										(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
										r.id is null
								order by p.distance
							end
							
							if ((select count(*) from #resultTable)<5)
							begin
								--czy w grupie NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją) jest jakiś serwis w promieniu 100km
								--wszystkie serwisy w promieniu 100km jeśli takowe są, jeśli ich nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,4 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	p.distance<100 and
										dbo.f_exists_in_split(possibilities,'108')=1 and
										type not like '%zawieszone%' and
										(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
										r.id is null
								order by p.distance
							end
	
							if ((select count(*) from #resultTable)<5)
							begin
								--czy w grupie NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji) znajduje się jakiś serwis
								--najbliższy serwis z w/w grupy
								insert into #resultTable
								select p.partnerId,p.partnerName,5 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId
								where	dbo.f_exists_in_split(possibilities,'109')=1 and
										type not like '%zawieszone%' and 
										r.id is null
								order by p.distance
							end
						end
					end
					else
					begin
						
						IF @debugMode = 1
						BEGIN
							INSERT INTO @debugTable (label, message) VALUES	('AC', '----'), ('@driveable', CAST(@driveable AS NVARCHAR(10)));
						END
	
						if @driveable=1
						begin
							
							--Multimake AC jezdne
							if isnull(@alphabetPrefferedService,'')<>''
							begin
								--czy dany pojazd w bazie pojazdów w kolumnie "Preferoway Serwis" ma zapisany jakiś preferowany serwis do obsługi, jeśli tak to
								--czy w jakiejkolwiek z grup (NW18, NW19, NW23, NW24, NW26, NW31) w promieniu 100km znajduje się serwis (ten sam, który widnieje w bazie pojazdów w kolumnie "Preferowany Serwis"), jeśli tak to
								--dedykowany preferowany serwis w promieniu 100km, jeśli go nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,1 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	extCode like '%ALP:'+@alphabetPrefferedService+'%' and
										p.distance<100 and
										type not like '%zawieszone%' and
										dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
										r.id is null
								order by p.distance
							end
							
							if @damageType = 1  -- szybowa
							begin
								if ((select count(*) from #resultTable)<5)
								begin
									--NW08-Naprawa szyb (Naprawa szyb)
									--SAINT-GOBAIN (CENTRALA SIECI)   epsilon  Dąbrowa Górnicza Szklanych Domów Saint-Gobain Centrala (8209751)  , deltasql Dąbrowa Górnicza Szklanych Domów Saint-Gobain Centrala (33121357)
									--NORDGLASS (CENTRALA SIECI)      epsilon Koszalin Bohaterów Warszawy Nordglass Centrala (5390972)           , deltasql Koszalin Bohaterów Warszawy Nordglass Centrala (22203090)
									--epsilon np A00568086 


									insert into #resultTable
									select top 1 p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'83')=1 and
										   (p.partnerName like '%Saint-Gobain Centrala%' or p.partnerName like '%Nordglass Centrala%') and
											r.id is null
									order by  newid()   --p.distance
								end
							end else if @damageType = 2  -- drobna
							begin
							
								if ((select count(*) from #resultTable)<5)
								begin
									--wyłącznie zarysowania zderzaków przód / tył bez naruszenia ciągłości elementu (pęknięcie, rozerwanie, dziury) - S-PLUS
									--NW24-Smart Repair S-PLUS dla ALPHABET (Szybkie naprawy małych szkód komunikacyjnych oraz zaawansowana kosmetyka pojazdów)
									--czy w grupie NW24-Smart Repair S-PLUS dla ALPHABET (Szybkie naprawy małych szkód komunikacyjnych oraz zaawansowana kosmetyka pojazdów) znajduje się jakiś serwis w promieniu 100 km
									--najbliższy serwis w promieniu 100km, jeśli go nie ma
									insert into #resultTable
									select top 5 p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	--dbo.f_exists_in_split(possibilities,'111')=1 and
											type like '%Alphabet%s-plus%' AND
											p.distance<50 and  /*Dla uszkodzeń typu SMART (SPLUS) oraz ASN (naprawy panelowe) proszę o zmniejszeni promienia funkcjonowania reguł do 50km*/
											r.id is null
									order by p.distance
								end
								
								if ((select count(*) from #resultTable)<1)
								begin
									--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest jakiś serwiś w promieniu 100km, jeśli tak to
									--czy wśród serwisów z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) w promieniu 100km znajduje się serwis obsługujący taką samą markę jak pojazd, dla którego następuje weryfikacja
									--wszyskie serwisy obsługujące daną markę, jeśli nie ma to patrz niżej
									--wszystkie serwisy z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany)
									--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest jakiś serwiś w promieniu 100km, jeśli nie to
									insert into #resultTable
									select p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'113')=1 and
										type not like '%zawieszone%' and
										(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
									--		p.distance<100 and
											r.id is null
									order by p.distance
								end
	
								if ((select count(*) from #resultTable)<1)
								begin
									--czy w grupie NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją) jest jakiś serwis w promieniu 100km
									--wszystkie serwisy w promieniu 100km jeśli takowe są, jeśli ich nie ma to patrz niżej
									insert into #resultTable
									select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'108')=1 and
									(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
										type not like '%zawieszone%' and
											p.distance<100 and
											r.id is null
									order by distance
								end
							end else if @damageType = 3  -- progi błotniki
							begin
								if ((select count(*) from #resultTable)<5)
								begin
									--wyłącznie uszkodzenia progów lub błotników tylnych pojazdu - ASN
									--NW23-ASN (szkody jezdne) (Specjalizacja w odbudowie elementów nadwozia)
									--W bazie danych z kolumnie "Uwagi" nie widnieje już oznaczenie "ASN"
									
									insert into #resultTable
									select top 5 p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'110')=1 and
										type not like '%zawieszone%' and
											--p.distance<50 and
											r.id is null
									order by distance
		 
								end
							end else if @damageType IN (4,5) -- reszta
							begin
								if ((select count(*) from #resultTable)<5)
								begin

							
									--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest jakiś serwiś w promieniu 100km, jeśli tak to
									--czy wśród serwisów z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) w promieniu 100km znajduje się serwis obsługujący taką samą markę jak pojazd, dla którego następuje weryfikacja
									--wszyskie serwisy obsługujące daną markę, jeśli nie ma to patrz niżej
									--wszystkie serwisy z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany)
									--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) jest jakiś serwiś w promieniu 100km, jeśli nie to
									insert into #resultTable
									select p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'113')=1 and
										(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
										type not like '%zawieszone%' and
											p.distance<100 and
											r.id is null
									order by p.distance
								end
			
								if ((select count(*) from #resultTable)<5)
								begin
									--czy w grupie NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją) jest jakiś serwis w promieniu 100km
									--wszystkie serwisy w promieniu 100km jeśli takowe są, jeśli ich nie ma to patrz niżej
									insert into #resultTable
									select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'108')=1 and
									(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
										type not like '%zawieszone%' and
											p.distance<100 and
											r.id is null
									order by distance
								end
	

								if ((select count(*) from #resultTable)<5)
								begin
									--czy w grupie NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji) znajduje się jakiś serwis
									--najbliższy serwis z w/w grupy
									insert into #resultTable
									select p.partnerId,p.partnerName,4 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'109')=1 and
										type not like '%zawieszone%' and
											--p.distance<100 and
											r.id is null
									order by distance
								end
							end
						end
						else
						begin
													
							--Multimake AC niejezdne
							if isnull(@alphabetPrefferedService,'')<>''
							begin

								--czy dany pojazd w bazie pojazdów w kolumnie "Preferoway Serwis" ma zapisany jakiś preferowany serwis do obsługi, jeśli tak to
								--czy w jakiejkolwiek z grup (NW18, NW19, NW23, NW24, NW26, NW31) w promieniu 100km znajduje się serwis (ten sam, który widnieje w bazie pojazdów w kolumnie "Preferowany Serwis"), jeśli tak to
								--dedykowany preferowany serwis w promieniu 100km, jeśli go nie ma to patrz niżej
								insert into #resultTable
								select p.partnerId,p.partnerName,1 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
								from	#partners p left join 
										#resultTable r on p.partnerId=r.partnerId 
								where	extCode like '%ALP:'+@alphabetPrefferedService+'%' and
										type not like '%zawieszone%' and
										p.distance<100 and
										dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
										r.id is null
								order by p.distance
							end
	
							--if @damageType=1 or @arcCode like '119109%'  -- szybowa
							--        --NW08-Naprawa szyb (Naprawa szyb)
							--		--Szkody szybowe niejezdne – szkodowe (bez SAINT-GOBAIN (CENTRALA SIECI), BEZ NORDGLASS (CENTRALA SIECI))  zawsze zgodnie z kierowalnościa Blachcarsko - lakierniczą
							--		--BEZ SAINT-GOBAIN (CENTRALA SIECI)   epsilon  Dąbrowa Górnicza Szklanych Domów Saint-Gobain Centrala (8209751)  , deltasql Dąbrowa Górnicza Szklanych Domów Saint-Gobain Centrala (33121357)
							--		--BEZ NORDGLASS (CENTRALA SIECI)      epsilon Koszalin Bohaterów Warszawy Nordglass Centrala (5390972)           , deltasql Koszalin Bohaterów Warszawy Nordglass Centrala (22203090)
							--		--epsilon np A00568086 

							--begin
							--	--if @groupProcessInstanceId=558409 select 555
							--	--if @groupProcessInstanceId=558409 select * from #resultTable
							--	if ((select count(*) from #resultTable)<5)
								
							--	begin
							--		--NW08-Naprawa szyb (Naprawa szyb)
							--		--Nordglass Centrala
							--		--Saint-Gobain Centrala
							--		insert into #resultTable
							--		select top 1 p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
							--		from	#partners p left join 
							--				#resultTable r on p.partnerId=r.partnerId 
							--		where	dbo.f_exists_in_split(possibilities,'83')=1 and
							--			    --(p.partnerName like '%saint-gob%' or p.partnerName like '%nordg%') and
							--				 (p.partnerName not like '%Saint-Gobain Centrala%' or p.partnerName not like '%Nordglass Centrala%') and
							--				r.id is null
							--		order by p.distance
							--	end
							--end 
							--else
							--begin
								
								if ((select count(*) from #resultTable)<5)
								begin
							
									--holowanie – uszkodzenia wielu elementów blacharskich lub drobne uszkodzenia ale uniemożliwiające dalsze poruszanie się pojazdem
									--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serws preferowany) jest jakiś serwiś w promieniu 100km, jeśli tak to
									--czy wśród serwisów z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) w promieniu 100km znajduje się serwis obsługujący taką samą markę jak pojazd, dla którego następuje weryfikacja
									--wszyskie serwisy obsługujące daną markę, jeśli nie ma to patrz niżej
									insert into #resultTable
									select top 5 p.partnerId,p.partnerName,2 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'113')=1 and
										type not like '%zawieszone%' and
											p.distance<100 and
											type like '%'+@make+'%' and
											r.id is null
									order by distance
								end
	
								if ((select count(*) from #resultTable)<5)
								begin
									--holowanie – uszkodzenia wielu elementów blacharskich lub drobne uszkodzenia ale uniemożliwiające dalsze poruszanie się pojazdem
									--czy w grupie NW31-Naprawa blach.-lak. ALPHABET (Serws preferowany) jest jakiś serwiś w promieniu 100km, jeśli tak to
									--czy wśród serwisów z grupy NW31-Naprawa blach.-lak. ALPHABET (Serwis preferowany) w promieniu 100km znajduje się serwis obsługujący taką samą markę jak pojazd, dla którego następuje weryfikacja
									--wszyskie serwisy obsługujące daną markę, jeśli nie ma to patrz niżej
									insert into #resultTable
									select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'113')=1 and
										type not like '%zawieszone%' and
											(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
											p.distance<100 and
											r.id is null
									order by p.distance
								end
	
								if ((select count(*) from #resultTable)<5)
								begin
									--czy w grupie NW18-Naprawa blach.-lak. ALPHABET z autoryzacją (Naprawa blacharsko-lakiernicza ALPHABET z autoryzacją) jest jakiś serwis w promieniu 100km
									--wszystkie serwisy w promieniu 100km jeśli takowe są, jeśli ich nie ma to patrz niżej
									insert into #resultTable
									select p.partnerId,p.partnerName,4 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'108')=1 and
										type not like '%zawieszone%' and
											(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
											p.distance<100 and
											r.id is null
									order by p.distance
								end
	
								if ((select count(*) from #resultTable)<5)
								begin
									--czy w grupie NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji) znajduje się jakiś serwis
									--najbliższy serwis z w/w grupy

									insert into #resultTable
									select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'109')=1 and
										type not like '%zawieszone%' and
											(type like '%marki%' or type like '%'+isnull(@make,'xxxxxx')+'%') and
											p.distance<100 and
											r.id is null
									order by p.distance
								end

								if ((select count(*) from #resultTable)<1)
								begin
									--czy w grupie NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji) znajduje się jakiś serwis
									--najbliższy serwis z w/w grupy
									insert into #resultTable
									select top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
									from	#partners p left join 
											#resultTable r on p.partnerId=r.partnerId 
									where	dbo.f_exists_in_split(possibilities,'109')=1 and
										type not like '%zawieszone%' and
											r.id is null
									order by p.distance
								end
							--end
						end
					end	
				end
				-------------------------------------------------------------------------------------------------------------------------------
			END
			ELSE
			BEGIN
				set @make=isnull(@make,'')
			
				if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
				begin
					insert into #resultTable
					select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	--dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
							type like '%ALPHABET%' and
							type like '%OPON%' and
							type not like '%zawieszone%' and
							((p.distance<200 OR isnull(@starterContractorAbroad,0) = 1) OR ISNULL(@processPath,1) = 2)
					order by p.priority, p.distance

					if ((select count(*) from #resultTable)<1)
					begin
						insert into #resultTable
						select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join 
								#resultTable r on p.partnerId=r.partnerId 
						where	--dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
								type like '%ALPHABET%' and
								type like '%OPON%' and
								type not like '%zawieszone%' 
						order by p.distance
					end			
				end
				else
				begin
					--czy w grupie NW19-Naprawa blach.-lak. ALPHABET bez autoryzacji (Naprawa blacharsko-lakiernicza ALPHABET bez autoryzacji) znajduje się jakiś serwis
					--najbliższy serwis z w/w grupy
				
			
					insert into #resultTable
					select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	--dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
							type like '%ALPHABET%' and
							type not like '%SZYB%' and
							type not like '%STACJE%' and
							type not like '%OPONY%' and
							type not like '%zawieszone%' and
							((p.distance<200 OR isnull(@starterContractorAbroad,0) = 1) OR ISNULL(@processPath,1) = 2)
						and type like '%'+@make+'%'
					order by p.priority, p.distance
				
			
					if ((select count(*) from #resultTable)<1)
					begin
						insert into #resultTable
						select  top 10 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join 
								#resultTable r on p.partnerId=r.partnerId 
						where	--dbo.f_exists_in_split(possibilities,'108,109,110,111,112,112,113')=1 and
								type like '%ALPHABET%' and
								type not like '%OPONY%' and
								type not like '%STACJE%' and
								type not like '%zawieszone%' and
								type not like '%SZYB%' and 
								type like '%marki%'
						
						order by p.distance
					end			
				end
			END


			
		end
		else if @programId IN (select id from dbo.vin_program where platform_id=78) -- WSZYSTKO Z CF
		begin

			
			set @maxDistance = IIF(@abroadLocationLocalPartner = 1, 9999, 50)
			set @withCFM=0
			
			if @arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%'
			begin
				declare @tireCompany nvarchar(500)
				declare @nameFilter nvarchar(500)
				
				exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName='Dostawca_opon', @output=@tireCompany OUTPUT
				
				IF @tireCompany like '%polskie składy opon%'
				BEGIN
					set @nameFilter = dbo.f_translate('PL. Skł. Opon',default)					
				END
				ELSE IF @tireCompany like '%polski serwis floty%'
				BEGIN
					set @nameFilter = dbo.f_translate('PL. Serw. Floty',default)					
				END
				else IF @tireCompany like '%bridgestone%' 
				BEGIN
					set @nameFilter = dbo.f_translate(dbo.f_translate('bridgestone',default),default)
				END 
				else IF @tireCompany like '%continental%' 
				BEGIN
					set @nameFilter = dbo.f_translate('continental',default)
				END 
				ELSE IF @tireCompany like '%fleet master%'
				BEGIN
					set @nameFilter = dbo.f_translate('fleet master',default)
				END 
				
				IF @nameFilter is not null 
				BEGIN
					insert into #resultTable
					select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	type like '%carefleet%opony%' and p.partnerName like '%'+@nameFilter+'%' 
					order by p.distance
				END 
				
				
				if ((select count(*) from #resultTable) = 0)
				BEGIN
					insert into #resultTable
					select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	type like '%carefleet%opony%' 
					order by p.distance
				END 
					

				if ((select count(*) from #resultTable)<3)
				begin
					insert into #resultTable
					select top 5 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
							#resultTable r on p.partnerId=r.partnerId
					where type like '%wulka%' and	 type like '%carefleet%'		
					order by p.distance
				end
			end
			else IF @eventType = 2
			begin
				
				insert into #resultTable
				select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				from	#partners p left join
						#resultTable r on p.partnerId=r.partnerId
				where	(type like '%'+@make+'%') 
						and dbo.is_ASO(p.partnerId) <> dbo.f_translate('Inny',default)
						and type not like '%szyb%'
						and p.distance<@maxDistance
				order by p.distance
				
			end  
			else if @eventType IN (1,5)
			BEGIN
				
				
				if @damageType=1 or @arcCode like '119109%' -- szybowa
				begin
					insert into #resultTable
					select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	type like '%Carefleet%szyb%'
					order by p.priority, p.distance
				end
				else
				begin
					--
					-- Jeżeli odległość miejsca zdarzenia jest mniejsza niż 50km od serwisu Lacart (wrocław) lub Delik (Przeźmierowo) – w pierwszej kolejności staramy się wysłać Lacart lub Delik na holowanie. 
				
					insert into #resultTable
					select p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
					from	#partners p left join
										#resultTable r on p.partnerId=r.partnerId
					where	type like '%Carefleet BRS 1%' 
							and dbo.f_exists_in_split (p.possibilities,'81,121')=1 
							and p.distance<@maxDistance
					order by p.distance
					
					if ((select count(*) from #resultTable)=0)
					BEGIN
						insert into #resultTable
						select top 1 p.partnerId,p.partnerName,3 priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
						from	#partners p left join
											#resultTable r on p.partnerId=r.partnerId
						where	type like '%Carefleet BRS P24%' 
								and dbo.f_exists_in_split (p.possibilities,'81,121')=1
						order by p.distance
						
					END 
					
				END
			END 
		end
		else
		begin
		--select 555
			
			insert into #resultTable
			select p.partnerId,p.partnerName,p.priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
			from #partners p 
			left join #resultTable r on r.partnerId = p.partnerId 
			where p.service=1 and p.distance<@maxDistance
			and r.id is null
			order by p.priority,p.distance

			if not exists( select partnerId from #resultTable)
			BEGIN
				insert into #resultTable
				select top 1 p.partnerId,p.partnerName,p.priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
				from #partners p 
				left join #resultTable r on r.id = p.partnerId 
				where p.service=1				
				order by p.priority,p.distance				
			END 
			
			
			insert into #resultTable
			select distinct p.partnerId,p.partnerName,p.priority,p.priority priority2,p.distance,p.distance distancePriority,p.phoneNumber, NULL, p.partnerServiceId, p.city
			from #partners p 
			left join #resultTable r on r.partnerId=p.partnerId			
			where p.service=1 and p.city in (select distinct city from #resultTable)
			and r.id is null
			order by p.priority,p.distance

			
		end
	end
	
	-- WRZUCANIE POWODÓW ODMOWY JEŻELI TAKIE ISTNIEJĄ
		 
	DECLARE @reasonLocalizationId INT
	DECLARE @reasonText VARCHAR(100)
	
	
	DECLARE kur cursor LOCAL for
			SELECT
			a2.value_int as localizationId, 
			CASE 
			WHEN ISNULL(a4.value_text,'') <> '' 
			THEN a4.value_text
			ELSE Left(UPPER(d.textD), 1)+Right(LOWER(d.textD), len(d.textD)-1)  END              reasonText
			FROM dbo.attribute_value as a1 with(nolock)
			INNER JOIN dbo.attribute_value as a2 with(nolock) ON a2.parent_attribute_value_id = a1.id AND a2.attribute_id = 704 AND a2.group_process_instance_id = @groupProcessInstanceId
			INNER JOIN dbo.attribute_value as a3 with(nolock) ON a3.parent_attribute_value_id = a1.id AND a3.attribute_id = 703 AND a3.group_process_instance_id = @groupProcessInstanceId
			INNER JOIN dbo.attribute_value as a4 with(nolock) ON a4.parent_attribute_value_id = a1.id AND a4.attribute_id = 63 AND a4.group_process_instance_id = @groupProcessInstanceId
			inner join dbo.dictionary d with(nolock) on d.value = a3.value_int and typeD = 'partnerReason' and active = 1  
			WHERE a1.attribute_path = '701,702'
			AND a1.group_process_instance_id = @groupProcessInstanceId and a2.value_int is not null

			OPEN kur;
			FETCH NEXT FROM kur INTO @reasonLocalizationId, @reasonText;
			WHILE @@FETCH_STATUS=0
			BEGIN
				-- WHILE BEGIN

					IF @reasonLocalizationId IS NOT NULL
					BEGIN
						UPDATE #resultTable
						SET reasonForRefusing = @reasonText
						WHERE partnerId = @reasonLocalizationId
					END 
					
				-- WHILE END
			FETCH NEXT FROM kur INTO @reasonLocalizationId, @reasonText;
		END
	CLOSE kur
	DEALLOCATE kur
	
	set @ts=datediff(ms,getdate(),@startTime)
	print '04 - '+cast(@ts as varchar(20))
	set @startTime=getdate()
	
	-- usunięcie z listy kontraktorów którzy w danej sprawie odmówili
	IF @skipHistory = 1
	BEGIN
		
		DECLARE @partnerAttributeId INT
		
		DELETE FROM @values 
		INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '610',
			@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerAttributeId = id FROM @values
		
		DELETE FROM #resultTable WHERE partnerId IN (
			SELECT value_int
			FROM dbo.attribute_value_history with(nolock)
			WHERE attribute_value_id = @partnerAttributeId AND value_int IS NOT NULL
		)
		
	END 
	
	DECLARE @minDistance DECIMAL(10,2)
	SELECT top 1 @minDistance = 1.25*distance, @city = city
	from #resultTable
	where city is not null
	order by distance ASC
	insert into dbo.log (name, content, created_at) select dbo.f_translate('city',default), CAST(@city AS NVARCHAR(255)), getdate()
	
	IF @type <> 0
	BEGIN
		
		DECLARE @err INT
		DECLARE @message NVARCHAR(255)
		
		
		IF @minDistance IS NOT NULL
		BEGIN
			
		   	EXEC [dbo].[p_attribute_edit]
		   	@attributePath = '903', 
		   	@groupProcessInstanceId = @groupProcessInstanceId,
		   	@stepId = 'xxx',
		   	@userId = 1,
		   	@originalUserId = 1,
		   	@valueDecimal = @minDistance,
	--	   	@skipTransaction = 1,
		   	@err = @err OUTPUT,
		   	@message = @message OUTPUT
		   	
		END 
--	   	insert into dbo.log (name, content) select dbo.f_translate('test min distance',default), @minDistance
	   	
	END
	
	IF @type = 1
	BEGIN
		
		DECLARE @partnersInCity NVARCHAR(2000)
		declare @closestPartnerCity nvarchar(255)
		declare @closestPartner int 
		
		select top 1 @closestPartner = partnerId from #resultTable 
		select @closestPartnerCity = city from dbo.partners_services_cache with(nolock) where id = @closestPartner 
		
		SELECT @partnersInCity = dbo.concatenate(distinct partnerId) 
		from (
			select	partnerId 
			FROM	#resultTable rt
			inner join dbo.partners_services_cache psc with(nolock) on psc.id = rt.partnerId
			where 
			psc.city = @closestPartnerCity
			ORDER BY distance desc OFFSET 0 ROWS FETCH NEXT 20 ROWS ONLY 
	   	) a
	   	
	   	IF @addClosestWorkshops = 1
	   	BEGIN
			delete from #resultTable
			where partnerId NOT IN (
				select	partnerId 
				FROM	#resultTable rt
				inner join dbo.partners_services_cache psc with(nolock) on psc.id = rt.partnerId
				where 
				psc.city = @closestPartnerCity
			)			
	   	END

		--if @groupProcessInstanceId = 728571
		--begin
		--	select	* 
		--	FROM	#resultTable
		--	ORDER BY distance desc 

		--	select @partnersInCity
		--end 

		IF ISNULL(@partnersInCity,'') <> '' AND @useRootProgram = 0
		BEGIN
			
		   	EXEC [dbo].[p_attribute_edit]
		   	@attributePath = '907', 
		   	@groupProcessInstanceId = @groupProcessInstanceId,
		   	@stepId = 'xxx',
		   	@userId = 1,
		   	@originalUserId = 1,
		   	@valueString = @partnersInCity,
	--	   	@skipTransaction = 1,
		   	@err = @err OUTPUT,
		   	@message = @message OUTPUT
		   	
		END 
	   	
	END 
	
	
	IF @addCurrentWorhshop = 1
	BEGIN
		DECLARE @currentWorkshop INT
		
		delete from @values	
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @currentWorkshop = value_int FROM @values
		
		IF NOT EXISTS(SELECT partnerId FROM #resultTable WHERE partnerId = @currentWorkshop) and @currentWorkshop is not null
		BEGIN
			INSERT INTO #resultTable
			SELECT @currentWorkshop, dbo.f_partnerNameFull(@currentWorkshop)+ ' [id: '+cast(@currentWorkshop as nvarchar(255))+']', 0, null, null, null, null, null, null, null
		END 
	END 
	
	
	if ((select count(*) from #resultTable)=0) and @type <> 0 AND dbo.f_abroad_case(@groupProcessInstanceId) = 0
	BEGIN
		EXEC [dbo].[p_attribute_edit]
       @attributePath = '603', 
       @groupProcessInstanceId = @groupProcessInstanceId,
       @stepId = 'xxx',
       @userId = 1,
       @originalUserId = 1,
       @valueInt = 1,
       @err = @err OUTPUT,
       @message = @message OUTPUT	
	END 
	
	
	--if @platformGroupName=dbo.f_translate('CFM',default) -- and (select count(*) from #resultTable)<51
	--begin
	--	UPDATE #resultTable
	--	set partnerName=isnull(dbo.partner_CFM_preffered(partnerId)+' ','')+partnerName
	--end
--	select	top (@limit) partnerId,
--			partnerName,
--			priority,
--			distance,
--			phoneNumber,
--			reasonForRefusing,
--			partnerServiceId
--	from	#resultTable 
----	order by priority, id
--	order by id


	if @resultType = 1
	BEGIN
	 	INSERT INTO #partnerTempTable
		select	top (@limit) partnerId,
				CONCAT(case when @platformGroupName=dbo.f_translate('CFM',default) and @type not in (98,99) and @withCFM=1 then isnull(dbo.partner_CFM_preffered(partnerId, @rootPlatformId)+' ','') else '' end+partnerName, ', O: ', CAST(distance AS NVARCHAR(50)), 'km)' ) partnerName,
				priority,
				distance,
				phoneNumber ,
				reasonForRefusing,
				partnerServiceId
		from	#resultTable 
		order by id 
	END 
	ELSE IF @resultType = 2
	BEGIN		
		SELECT TOP (@limit) partnerId, 
				CONCAT(case when @platformGroupName=dbo.f_translate('CFM',default) and @type not in (98,99) and @withCFM=1 then isnull(dbo.partner_CFM_preffered(partnerId, @rootPlatformId)+' ','') else '' end+partnerName, case when @type<>98 then ', O: '+ CAST(distance AS NVARCHAR(50))+ 'km' else '' end,'')  partnerName,
				priority,
				distance,
				phoneNumber,
				reasonForRefusing,
				partnerServiceId
				FROM #resultTable 
				order by id
	END 
	ELSE IF @resultType = 3
	BEGIN		
		SELECT TOP (@limit) partnerId, CONCAT(case when @platformGroupName=dbo.f_translate('CFM',default) and @type not in (98,99) and @withCFM=1 then isnull(dbo.partner_CFM_preffered(partnerId, @rootPlatformId)+' ','') else '' end +partnerName, ' (P: ', CAST(priority AS NVARCHAR(20)), ', O: ', CAST(distance AS NVARCHAR(50)), 'km)' ),
				priority,
				distance,
				phoneNumber,
				reasonForRefusing,
				partnerServiceId
				FROM #resultTable 
				order by id
	END 
	ELSE
	BEGIN

		select	top (@limit) partnerId,
				CONCAT(case when @platformGroupName=dbo.f_translate('CFM',default) and @type not in (98,99) then isnull(dbo.partner_CFM_preffered(partnerId, @rootPlatformId)+' ','') else '' end +partnerName, ', O: ', CAST(distance AS NVARCHAR(50)), 'km)' ) partnerName,
				priority,
				distance,
				phoneNumber,
				reasonForRefusing,
				partnerServiceId
		from	#resultTable 
		order by id
	END 
	
	DROP table #partners
	DROP TABLE #resultTable

	IF @debugMode = 1
	BEGIN
		SELECT * FROM @debugTable
	END
		
	PRINT '--------------------------- END ------------------------'
	
END