ALTER PROCEDURE [dbo].[s_1009_049]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @userId INT
	DECLARE @originalUserId INT
	DECLARE @message NVARCHAR(255)
	DECLARE @newProcessInstanceId INT
	DECLARE @groupProcessInstanceId INT 
	DECLARE @stepId NVARCHAR(20)
	DECLARE @executorPhone NVARCHAR(20)
	DECLARE @postponeDate DATETIME
	DECLARE @rootId INT	
	DECLARE @parentId INT
	DECLARE @icsId INT
	DECLARE @eta DATETIME
	DECLARE @sql NVARCHAR(4000)
	DECLARE @fixingOrTowing INT
	DECLARE @content NVARCHAR(4000)
	DECLARE @partnerId INT
	
	SET @postponeDate = DATEADD(MINUTE, 5, GETDATE())		
	
	SELECT @groupProcessInstanceId = p.group_process_id, 
	@rootId = p.root_id,
	@parentId = p.parent_id
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s with(nolock) on s.id = p.step_id
	LEFT JOIN process_instance pp with(nolock) on pp.id = p.previous_process_instance_id
	WHERE p.id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	IF @variant = 2
	BEGIN
		
		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '610',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerId = value_int FROM @values
		
		EXEC p_add_service_refuse_reason
		@groupProcessInstanceId = @groupProcessInstanceId,
		@partnerLocationId = @partnerId,
		@reasonId = -1
		
		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '609',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @icsId = value_int FROM @values
		
		IF @icsId > 0
		BEGIN
			
			SET @sql = 'insert into StarterTeka.order_statuses (order_id, status_id,  submitted_at,  created_at,  updated_at, reason) values ('+
		    CAST(@icsId as NVARCHAR(20))+', 10, '''+CONVERT(VARCHAR, GETDATE(),20)+''', '''+CONVERT(VARCHAR, GETDATE(),20)+''', '''+CONVERT(VARCHAR, GETDATE(),20)+''', ''Zbyt długi czas dojazdu na miejsce.'')'
			EXEC (@sql) at Teka2
	
			SET @sql = 'update StarterTeka.orders set active = 0 where id = '+CAST(@icsId as NVARCHAR(20))
			EXEC (@sql) at Teka2
			
		END 
		-- zerowanie bieżącego partnera i typu zlecenia sprawy
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '610', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = NULL,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '609', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = NULL,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '691', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueString = NULL,
		@err = @err OUTPUT,
		@message = @message OUTPUT
			
		SET @content = dbo.f_translate('Tu Starter24. Propozycja zlecenia na ',default)+dbo.f_conditionalText(@fixingOrTowing,dbo.f_translate('holowanie',default),dbo.f_translate('naprawę',default))+dbo.f_translate(' dla sprawy ',default)+dbo.f_caseId(@previousProcessId)+dbo.f_translate(' w ICS jest juz nieaktualna ze względu na brak zmiany zbyt wysokiego ETA w ciągu 5 minut.',default)
				
		EXEC p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @executorPhone,
		@userId = 1,
		@originalUserId = 1,
		@addInfo = 0,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
	END
		
END

