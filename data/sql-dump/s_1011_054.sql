
ALTER PROCEDURE [dbo].[s_1011_054]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, 
    @errId int=0 output
) 
AS
BEGIN
		
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @zmienna INT
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	SET @errId=0
	SET @variant=1

	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @noteText NVARCHAR(MAX)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '380', @groupProcessInstanceId = @rootId
	SET @noteText = (SELECT TOP 1 value_text FROM @values WHERE value_text IS NOT NULL)	

	IF ISNULL(@noteText,'')<>''
	BEGIN

		SET @noteText = dbo.f_translate('Powód innego rozliczenia matrycy: ',default) + @noteText

		DECLARE @err TINYINT
		DECLARE @message NVARCHAR(255)
		EXECUTE dbo.p_note_new -- Dodawanie notatki
			@groupProcessId = @RootId
			,@type = dbo.f_translate('text',default)
			,@content = @noteText
			,@userId = 1  -- automat
			,@err = @err
			,@message = @message
	END
	ELSE
	BEGIN
		SET @errId = -404
	END

	PRINT '-----------------------------------------------------'
	PRINT dbo.f_translate('Końcowy error ID: ',default) + CAST(@errId AS VARCHAR)
	PRINT '-----------------------------------------------------'

	IF @errId = -3 --> Niezgodność z RS, tylko dla VGP bez Seat'a
	BEGIN
		SET @variant = 2
	END
END
