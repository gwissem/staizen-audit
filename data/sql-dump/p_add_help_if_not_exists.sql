ALTER PROCEDURE [dbo].[p_add_help_if_not_exists]
( 
	@groupProcessInstanceId INT,
    @programId INT,
    @platformId INT
) 
AS
BEGIN
	
	/* ______________________________________________________
	 
	 	Dodaje do sprawy HELP'a, jeżeli jeszcze nie zostało dodane
	 	Chodzi o HELPY, które są skonfigurowane w tabeli: help_case
	 	
	 	Szukanie jest WYŁĄCZNIE po PROGRAMIE i PLATFORMIE
	 	
	 ________________________________________________________*/
	
	PRINT '-------------------------- EXEC [dbo].[p_add_help_if_not_exists]'
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @helpId INT
	DECLARE @vipId INT
	DECLARE @helpText NVARCHAR(MAX)
	DECLARE @rootId int
	DECLARE @companyName NVARCHAR(MAX)
  SELECT @rootId = root_id  from process_instance with (nolock ) where id = @groupProcessInstanceId

	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1067', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @helpId = value_int FROM @values
	
    PRINT '--- @helpId: ' + CAST(ISNULL(@helpId, '') AS NVARCHAR(200))
    
    -- Sprawdzenie, czy już HELP został dodany
    
    IF ISNULL(@helpId, '') = ''
    BEGIN


      DECLARE @isVip int
      DELETE from @values
      INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1004', @groupProcessInstanceId = @rootId
	  SELECT @isVip = (select 1 from @values where value_string is not null )

	  DELETE from @values
      INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '981,438', @groupProcessInstanceId = @rootId
	  SELECT @companyName = (select 1 from @values where value_string is not null )
	  
      DECLARE @vipText nvarchar(MAX)
      IF @isVip is not null
        BEGIN

          SELECT @vipText = note, @vipId = id
          from help_case with (nolock )
          WHERE
              (ISNULL(platform_id, 0) != 0 OR ISNULL(program_id, 0) != 0 )
            AND
              (
                  (platform_id = @platformId OR platform_id = 0)
                    AND
                  (program_id = @programId OR program_id = 0)
              )
              AND name = dbo.f_translate('VIP',default)
        end

	    /*	
	      	Sprawdzenie, czy jest zdefiniowany program/platforma w konfiguratorze 
	      
	    	BIERZE POD UWAGĘ TYLKO HELPY, KTÓRE MAJĄ USTAWIONE PROGRAMY LUB PLATFORMĘ
 
		 	Dodatkowo musi zgadzać się program I platforma (chyba, że program lub platforma jest 0)
		____________________________________*/
		
		SELECT TOP 1 
			@helpId = id, @helpText = note 
		FROM dbo.help_case WITH(NOLOCK) 
		WHERE 
			(ISNULL(platform_id, 0) != 0 OR ISNULL(program_id, 0) != 0 )
		AND 
			((platform_id = @platformId OR platform_id = 0) AND (program_id = @programId OR program_id = 0))
		AND name = @companyName	
		
		ORDER BY id ASC
		
			PRINT '--- @helpId: ' + CAST(ISNULL(@helpId, '') AS NVARCHAR(200))
		
		
		IF @helpText IS NOT NULL OR @vipText IS NOT NULL
		BEGIN
		
			IF ISNULL(@helpId, 0) <> 0
			BEGIN		
				SET @helpText = ISNULL(@helpText,'') + @helpText
			END
	        IF @isVip is not null
	        BEGIN
	          SET @helpText = ISNULL(@helpText,'') + @vipText
	        end

	        SET @helpText = '<strong>SPECJALNE WARUNKI OBSŁUGI</strong><br>' + @helpText
	        SET @helpId = ISNULL(@vipId,@helpId)
	        
			EXEC [dbo].[p_attribute_edit]
				@attributePath = '1067',
				@stepId = 'xxx',
				@groupProcessInstanceId = @groupProcessInstanceId,
				@valueInt = @helpId,
				@userId = 1,
				@originalUserId = 1,
				@err = @err,
				@message = @message

			EXECUTE dbo.p_note_new
				@groupProcessId = @groupProcessInstanceId
				,@type=dbo.f_translate('text',default)
				,@content = @helpText
				,@userId= 1
				,@err=@err OUTPUT
				,@message=@message OUTPUT
				,@special=1
				,@subject=dbo.f_translate('Uwagi zapisane w bazie uprawnień',default)

			-- Nie do końca wiem, po co ten atrybut jest
--			
--			EXEC [dbo].[p_attribute_edit]
--					@attributePath = '987',
--					@stepId = 'xxx',
--					@groupProcessInstanceId = @groupProcessInstanceId,
--					@valueInt = 1,
--					@err = @err,
--					@message = @message
					
		
		END
      
    END
										    
END