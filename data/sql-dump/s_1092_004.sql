ALTER PROCEDURE [dbo].[s_1092_004]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @rootId INT
	DECLARE @rootNewCase INT = NULL
	DECLARE @isFlowToCase INT = NULL
	
	--DECLARE @processInstanceId INT
	
	-- Pobranie danych o kroku --
	SELECT	@groupProcessInstanceId = group_process_id, 
			@rootId = root_id,
			@stepId = step_id
	FROM process_instance 
	WHERE id = @previousProcessId 
	
	-- Stworzenie pomocniczej tabelki
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	-- pobranie id sprawy do powiązania 
	INSERT @values EXEC [dbo].[p_attribute_get2]
		@attributePath = '707,235', 
		@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @rootNewCase = value_int FROM @values 
	
	------------------------------------------------------------
	------- POBRANIE INFO DLA NOTATKI --------------------------
	------------------------------------------------------------
	
	DECLARE @subject VARCHAR(400)
	DECLARE @address VARCHAR(100)
	DECLARE @addressFrom VARCHAR(100)
	DECLARE @documentEML INT
	
	IF @rootNewCase IS NOT NULL
	BEGIN 
		
		DELETE FROM @values
		-- pobranie tematu 
		INSERT @values EXEC [dbo].[p_attribute_get2]
			@attributePath = '707,409', 
			@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @subject = value_string FROM @values
			
		DELETE FROM @values
		-- pobranie adresata
		INSERT @values EXEC [dbo].[p_attribute_get2]
			@attributePath = '707,709', 
			@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @addressFrom = value_string FROM @values
		
		DELETE FROM @values
		-- pobranie odbiorcy
		INSERT @values EXEC [dbo].[p_attribute_get2]
			@attributePath = '707,710', 
			@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @address = value_string FROM @values
		
		DELETE FROM @values
		-- pobranie dokumentu z emailem
		INSERT @values EXEC [dbo].[p_attribute_get2]
			@attributePath = '707,706', 
			@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @documentEML = value_int FROM @values
	
		
		PRINT dbo.f_translate('rootNewCase: ',default) + CAST(@rootNewCase AS VARCHAR)
		PRINT dbo.f_translate('Title: ',default) + CAST(@subject AS VARCHAR)
		PRINT dbo.f_translate('Address: ',default) + CAST(@address AS VARCHAR)
		PRINT dbo.f_translate('documentEML: ',default) + CAST(@documentEML AS VARCHAR)
		
		-- CZASAMI W rootNewCase może być ID konkretnego kroku
		
		DECLARE @realyGroupProcessInstanceId INT
		
		-- Pobranie danych o kroku --
		SELECT	@realyGroupProcessInstanceId = group_process_id
		FROM process_instance 
		WHERE id = @rootNewCase
		
		EXEC dbo.p_note_new
			@groupProcessId = @realyGroupProcessInstanceId,
			@type = dbo.f_translate('email',default),
			@subject = @subject,
			@content = @subject,
			@email = @address,
			@sender = @addressFrom,
			@attachment = @documentEML,
			@phoneNumber = NULL,
			@userId = 1,
			@originalUserId = 1,
			@subType = NULL,
			@direction = 2,  -- 1: out  2: in
			@err = @err OUTPUT,
			@message = @message OUTPUT	
		
		PRINT dbo.f_translate('Output z notatki:',default)
		PRINT @err
		PRINT @message
			
	END 
	
	
	DELETE FROM @values
	-- Sprawdzenie czy robić flow'a
	INSERT @values EXEC [dbo].[p_attribute_get2]
		@attributePath = '715',
		@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isFlowToCase = value_int FROM @values 
	
	-- jeżeli 1 to FLOW do nowej sprawy
	IF ISNULL(@isFlowToCase, 0) = 1
	BEGIN
		
		SET @variant = 2
		  
	END 
	
	
END

