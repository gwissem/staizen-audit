ALTER PROCEDURE [dbo].[p_alphabet_monit_harm] @groupProcessInstanceId INT  
,                                             @type int                    
,                                             @overrideEmails NVARCHAR(500) = ''
AS
BEGIN
	DECLARE @rootId int
	DECLARE @values Table ( id            INT
	,                       value_int     INT
	,                       value_string  NVARCHAR(255)
	,                       value_text    NVARCHAR(MAX)
	,                       value_date    DATETIME
	,                       value_decimal DECIMAL(18, 5) )
	SELECT @rootId = root_id
	FROM process_instance with(nolock)
	where group_process_id = @groupProcessInstanceId

	DECLARE @caseId nvarchar(30)
	SELECT @caseId = dbo.f_caseId(@groupProcessInstanceId)


	DECLARE @regNumber nvarchar(10)
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string
	FROM @values


	DECLARE @VIN nvarchar(50)
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VIN = value_string
	FROM @values

	DECLARE @makeModel int
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = value_int
	FROM @values
	DECLARE @makeModelText nvarchar(100)

	SELECT @makeModelText = textD
	from AtlasDB_def.dbo.dictionary
	where typeD = 'makeModel'
		and value = @makeModel

	DECLARE @milage int
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @milage = value_int
	FROM @values

	DECLARE @eventTypeInt int
	DECLARE @eventType nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventTypeInt = value_int
	FROM @values

	select @eventType = textD
	from AtlasDB_def.dbo.dictionary
	where typeD = 'EventType'
		and value = @eventTypeInt

	DECLARE @userName NVARCHAR(50)
	DECLARE @userSurname NVARCHAR(50)
	DECLARE @PESEL NVARCHAR(50)
	DECLARE @userMail NVARCHAR(50)
	DECLARE @phoneNumber NVARCHAR(50)
	DECLARE @accidentDate DATETIME
	DECLARE @accidentDamage NVARCHAR(1000)
	DECLARE @accidentDescription NVARCHAR(1000)
	DECLARE @perpetorOtherText NVARCHAR(1000)
	DECLARE @accidentType NVARCHAR(150)
	DECLARE @accidentTypeId int
	DECLARE @vehicleStatusId int
	DECLARE @vehicleStatus nvarchar(150)
	DECLARE @perpetorTypeId int
	DECLARE @perpetorTypeText nvarchar(150)
	DECLARE @perpetorFirstName nvarchar(150)
	DECLARE @perpetorLastName nvarchar(150)
	DECLARE @typeOfHarmRealisationId int
	DECLARE @typeOfHarmRealisation nvarchar(60)
	DECLARE @policeConfirmation int
	DECLARE @policeConfirmationText nvarchar(10)
	DECLARE @caseDocuments nvarchar(150)
	DECLARE @caseDocumentsTEXT nvarchar(4000)
	DECLARE @noteTitle nvarchar(500)


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,64',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @userName = value_string
	FROM @values

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,66',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @userSurname = value_string
	FROM @values

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,327',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @PESEL = value_string
	FROM @values


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,368',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @userMail = value_string
	FROM @values


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @phoneNumber = value_string
	FROM @values


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '101,104',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @accidentDate = value_date
	FROM @values


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,443',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @accidentDamage = value_text
	FROM @values

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,442',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @accidentDescription = value_text
	FROM @values


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,444',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @accidentTypeId = value_int
	FROM @values

	SELECT @accidentType = textD
	FROM dbo.dictionary
	where typeD = 'claimType2'
		and value = @accidentTypeId

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,119',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleStatusId = value_int
	FROM @values
	SELECT @vehicleStatus = textD
	FROM dbo.dictionary
	where typeD = 'claimVehicleState'
		and value = @vehicleStatusId


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,811',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @perpetorTypeId = value_int
	FROM @values

	SELECT @perpetorTypeText = textD
	FROM dbo.dictionary
	where typeD = 'perpetratorTypeOc'
		and value = @perpetorTypeId


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,64',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @perpetorFirstName = value_string
	from @values

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,66',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @perpetorLastName = value_string
	from @values


	DECLARE @perpetorRegNumber nvarchar(10)
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,72',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @perpetorRegNumber = value_string
	FROM @values

	DECLARE @perpetorVin nvarchar(20) = ''
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,71',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @perpetorVin = value_string
	FROM @values

	DECLARE @perpetorMakeModel int
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,73',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @perpetorMakeModel = value_int
	FROM @values

	DECLARE @perpetorMakeModelText nvarchar(100)

	SELECT @perpetorMakeModelText = textD
	from AtlasDB_def.dbo.dictionary
	where typeD = 'makeModel'
		and value = @perpetorMakeModel

	DECLARE @perpetorMail nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,368',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @perpetorMail = value_string
	FROM @values

	DECLARE @perpetorOCNumber nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,440',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @perpetorOCNumber = value_string
	FROM @values

	SET @perpetorOtherText = 'Dane sprawcy: <br />'

	IF isnull(@perpetorRegNumber, '') <> ''
	BEGIN
		SET @perpetorOtherText = @perpetorOtherText + dbo.f_translate(' Nr rejestracyjny: ',default) + @perpetorRegNumber + ', '
	end
	IF isnull(@perpetorVin, '') <> ''
	BEGIN
		SET @perpetorOtherText = @perpetorOtherText + '<br /> VIN: ' + @perpetorVin + ', '
	end
	IF isnull(@perpetorMakeModelText, '') <> ''
	BEGIN
		SET @perpetorOtherText = @perpetorOtherText + '<br /> Model samochodu: ' + @perpetorMakeModelText + ', '
	end
	IF isnull(@perpetorMail, '') <> ''
	BEGIN
		SET @perpetorOtherText = @perpetorOtherText + '<br /> E-mail sprawcy:' + @perpetorMail + ', '
	end
	IF isnull(@perpetorOCNumber, '') <> ''
	BEGIN
		SET @perpetorOtherText = @perpetorOtherText + '<br /> Nr polisy:' + @perpetorOCNumber + ' '
	end
	IF @perpetorOtherText = 'Dane sprawcy: <br />'
	BEGIN
		SET @perpetorOtherText = null
	end
	ELSE
	BEGIN
		SET @perpetorOtherText = ' <br /><br />' + @perpetorOtherText + ' <br /><br />'
	end


	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,201',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @typeOfHarmRealisationId = value_int
	from @values


	IF @typeOfHarmRealisationId = 1
	BEGIN
		SET @typeOfHarmRealisation = dbo.f_translate('OC sprawcy',default)
	end
	ELSE
	BEGIN
		SET @typeOfHarmRealisation = dbo.f_translate('AC właściciela',default)
	end

	DECLARE @policeCity NVARCHAR(400)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1006', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @policeCity = value_string
	FROM @values


	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '542', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @policeConfirmation = value_int
	FROM @values

	IF ISNULL(@policeCity,'') <> '' OR @policeConfirmation = 1
	BEGIN
		SET @policeConfirmationText = dbo.f_translate('TAK',default)
	end
	ELSE
	BEGIN
		SET @policeConfirmationText = dbo.f_translate('NIE',default)
	end


	DECLARE @harmLocationString NVARCHAR(400)
	DECLARE @partnerLocation NVARCHAR(400)
	DECLARE @partnerLocationId int
	DECLARE @partnerMail nvarchar(150)
	DECLARE @mailbody nvarchar(MAX)
	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('cfm')
	DECLARE @dwMail nvarchar(40) = ''

	EXEC dbo.p_location_string @attributePath          = '500,85'
	,                          @groupProcessInstanceId = @groupProcessInstanceId
	,                          @locationString         = @harmLocationString OUTPUT

	DECLARE @carLocationString NVARCHAR(400)

	EXEC dbo.p_location_string @attributePath          = '755,85'
	,                          @groupProcessInstanceId = @groupProcessInstanceId
	,                          @locationString         = @carLocationString OUTPUT

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '116,445', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @caseDocuments = value_string
	FROM @values

	SELECT @caseDocumentsTEXT = dbo.f_dictionaryDesc(@caseDocuments, 'claimDocuments')

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerLocationId = value_int
	FROM @values

	SELECT @partnerLocation = dbo.f_partnerNameFull(@partnerLocationId)

	IF @type = 1
	BEGIN

		IF ISNULL(@overrideEmails, '') <> ''
		BEGIN

			SET @partnerMail = dbo.f_getRealEmailOrTest(@overrideEmails)

		END
		ELSE
		BEGIN

			SET @partnerMail = dbo.f_getRealEmailOrTest('szkody@alphabet.pl,szkodyalphabet@starter24.pl')

		END

	END
	ELSE
	BEGIN

		SELECT @partnerMail = dbo.f_partner_contact_all(@groupProcessInstanceId,@partnerLocationId,1,4)
		SET @partnerMail = isnull(@partnerMail, '')
		SET @dwMail = dbo.f_getRealEmailOrTest('szkody@alphabet.pl')

	END

	--       Type 1 = szkoda
	-- Automat informuje o każdej otwartej sprawie z typem zdarzenia:
	-- Szkoda jezdna, Wypadek, Kradzież.
	--  DW: szkodyalphabet@starter24.pl
	IF @type = 1
	BEGIN

		DECLARE @caseViewURL NVARCHAR(100)
		SET @caseViewURL = [dbo].[f_getDomain]() + '/cfm-case-viewer/index?case-number=' +
		cast(isnull(@rootId, '') as nvarchar(20))

		SET @mailbody =
		dbo.f_translate('Nr sprawy: ',default) + isnull(@caseId, '') + '<br />' +
		dbo.f_translate('Nr rejestracyjny: ',default) + isnull(@regNumber, '') + '<br />' +
		dbo.f_translate('VIN: ',default) + isnull(@VIN, '') + '<br />' +
		dbo.f_translate('Marka i model pojazdu: ',default) + isnull(@makeModelText, '') + '<br />' +
		dbo.f_translate('Przebieg: ',default) + cast(isnull(@milage, '') as nvarchar(20)) + '<br />' +
		dbo.f_translate('Użytkownik: ',default) + isnull(@userName, '') + ' ' + isnull(@userSurname, '') + '<br />
      Nr telefonu: ' + cast(isnull(@phoneNumber, '') as nvarchar(20)) + '<br />
    E-mail: ' + isnull(@userMail, '') + '<br />
      PESEL: ' + isnull(@PESEL, '') + '<br />
      Rodzaj zdarzenia: ' + isnull(@eventType, '') + '<br />
    Data szkody: ' + convert(varchar, ISNULL(@accidentDate, GETDATE()), 103) + '<br />
    Miejsce szkody: ' + isnull(@harmLocationString, '') + '<br />
      Uszkodzenia: ' + isnull(@accidentDamage, '') + '<br />
      Okoliczności szkody: ' + isnull(@accidentDescription, '') + '<br />
    Rodzaj szkody: ' + isnull(@accidentType, '') + '<br />
    Stan pojazdu: ' + isnull(@vehicleStatus, '') + '<br />
      Sprawca: ' + isnull(@perpetorTypeText, '') + '<br />
      Imię i nazwisko sprawcy: ' + isnull(@perpetorFirstName, '') + ' ' + isnull(@perpetorLastName, '') + '<br />'
		+ isnull(@perpetorOtherText, '') + '
    Szkoda realizowana z: ' + isnull(@typeOfHarmRealisation, '') + '<br />
    Miejsce użytkowania pojazdu: ' + isnull(@carLocationString, '') + '<br />
    Czy wezwano Policję: ' + isnull(@policeConfirmationText, '') + '<br />' +
		dbo.f_translate('Policja z miasta:  ',default) + isnull(@policeCity, '') + '<br />' + '
    Dostępne dokumenty: ' + isnull(@caseDocumentsTEXT, '') + '<br />
    Dane serwisu: ' + isnull(@partnerLocation, '') + '<br /> <br /> <br /> <br />
    Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres cfm@starter24.pl
    Brakujące informacje w powyższym zestawieniu zostaną uzupełnione w najszybszym możliwym terminie. Ich podgląd jest możliwy na platformie zleceń ATLAS Viewer. <br />
    Link do sprawy: <a target="_blank" href="' + isnull(@caseViewURL, '#') + '">' + isnull(@caseViewURL, '') + '</a>'


	end
	ELSE IF @type = 2
		BEGIN
			SET @mailbody = '

Do '+isnull(@partnerLocation,'')+'<br />

Od Starter24 sp. z o.o.<br /><br />' +
			'Telefon kontaktowy dla Państwa: +48 61 83 19 894 <br />
 Infolinia dla Klienta: +48 509 464 464 <br />
 e-mail kontaktowy: cfm@starter24.pl <br />
 Data ' + convert(varchar, getdate(), 103) +' '+ CONVERT(varchar, GETDATE(), 108) + '<br /><br />
Informujemy, że do Państwa serwisu zostanie dostarczony pojazd należący do floty Alphabet Polska Fleet Management Sp. z o.o. <br />'
			+ isnull(@regNumber, '') + ' ' + isnull(@makeModelText, '') + ' <br />
 Użytkownik:' + isnull(@userName, '') + ' ' + isnull(@userSurname, '') + '<br />
Numer telefonu: ' + cast(isnull(@phoneNumber, '') as nvarchar(20)) + '<br /><br />' +
			dbo.f_translate('Prosimy o niezwłoczną organizację, udostępnienie / podmianę pojazdu zastępczego na czas naprawy w/w pojazdu, zgodnie z warunkami współpracy z Alphabet Polska Fleet Management Sp. z o.o..',default)
		end
		ELSE IF @type = 3
			BEGIN


				DECLARE @costTable TABLE ( rownum            int
				,                          days              int
				,                          costdaily         decimal
				,                          startDate         date
				,                          partnerLocationId int )
				insert into @costTable
				SELECT ROW_NUMBER() over (order by isnull(av3.value_date, p.created_at) asc)
				,      sum(isnull(av.value_int, 0))                                         
				,      av4.value_decimal                                                     as decimal
				,      isnull(av3.value_date, p.created_at)                                  as date4
				,      av6.value_int                                                         as partnerLocId
				from       (select *                                                                
				,                  row_number()over (partition by group_process_id order by id desc) rowN
				FROM dbo.service_status with(nolock))    ss              
				inner join dbo.attribute_value           av with(nolock)  ON ss.group_process_id = av.group_process_instance_id
				left join  dbo.attribute_value           av2 with(nolock) on av2.group_process_instance_id = av.group_process_instance_id and av2.attribute_path = '168,839'
				inner join dbo.attribute_value           av3 with(nolock) on av3.group_process_instance_id = av.group_process_instance_id and av3.attribute_path = '540'
				inner join dbo.attribute_value           av4 with(nolock) on av4.parent_attribute_value_id = av.parent_attribute_value_id and
						av4.attribute_path = '789,786,240,788'
				left join  dbo.process_instance          p                on p.id = av.group_process_instance_id
				inner join dbo.service_status_dictionary ssd              ON ssd.id = ss.status_dictionary_id
				left join  dbo.attribute_value           av5 with(nolock) ON av5.group_process_instance_id = av.group_process_instance_id and av5.attribute_path = '168,159'
				left join  dbo.attribute_value           av6 with(nolock) ON av6.group_process_instance_id = av.group_process_instance_id and av6.attribute_path = '764,742'
				where av.root_process_instance_id = @rootId
					and av.attribute_path = '789,786,240,153'
					and rowN = 1
					and p.id = (SELECT top 1 p.id
					from process_instance p2 with(nolock)
					where p2.group_process_id = av.group_process_instance_id
					order by p2.created_at asc)
					and ssd.progress NOT IN (5, 6)
					and isnull(av5.value_int, 0) = 0
				group by av2.value_int
				,        av4.value_decimal
				,        isnull(av3.value_date, p.created_at)
				,        av6.value_int
				order by isnull(av3.value_date, p.created_at) asc


				SET @mailbody = '' +
				'Do '+isnull(@partnerLocation,'')+'<br />

Od Starter24 sp. z o.o.<br /><br />' +
				'Telefon kontaktowy dla Państwa: +48 61 83 19 894 <br />
       Infolinia dla Klienta: +48 509 464 464 <br />
       e-mail kontaktowy: cfm@starter24.pl <br />
 Data: ' + convert(varchar, getdate(), 103) +' '+ CONVERT(varchar, GETDATE(), 108) +'<br/><br />'+
				'Informujemy, że do Państwa serwisu zostanie dostarczony pojazd należący do floty Alphabet Polska Fleet Management Sp. z o.o. <br />'
				+ isnull(@regNumber, '') + ',  ' + isnull(@makeModelText, '') + ' <br />
 Użytkownik: ' + isnull(@userName, '') + ' ' + isnull(@userSurname, '') + '<br />
Numer telefonu: ' + cast(isnull(@phoneNumber, '') as nvarchar(20))



				DECLARE @singleMWInfo nvarchar(300) = ''
				DECLARE @MWRentalLocationId int = null
				DECLARE @MWRentalLocationName nvarchar(200) = null
				DECLARE @MWStart date = null
				DECLARE @MWDailyCost decimal = null

				IF exists(select 1
					from @costTable)
				BEGIN
					SET @mailbody = @mailbody +'<br /><br /> W sprawie zorganizowano wynajem pojazdu zastępczego:'
				END
				while exists(select 1
					from @costTable)
				begin

					select top 1 @MWRentalLocationId = partnerLocationId
					,            @MWStart = startDate
					,            @MWDailyCost = costdaily
					from @costTable
					order by rownum asc
					SET @MWRentalLocationName = null
					SELECT @MWRentalLocationName = dbo.f_partnerNameFull(@MWRentalLocationId)


					SET @singleMWInfo = '<br /><br />Wypożyczalnia:' + isnull(@MWRentalLocationName, '') + '<br />' +
					dbo.f_translate('Koszt za dobę:',default) + cast(isnull(@MWDailyCost, 0) as nvarchar(10)) +
					'zł netto / doba <br />' +
					dbo.f_translate('Data rozpoczęcia wynajmu:',default) + convert(varchar, isnull(@MWStart, getdate()), 103) +
					'<br />'
					SET @mailbody = @mailbody + @singleMWInfo + '<br /> <br/>'

					delete from @costTable
					where rownum = (SELECT top 1 rownum
						from @costTable
						order by rownum asc)


				end

			END

	DECLARE @err int
	DECLARE @message nvarchar(400)

	DECLARE @subject nvarchar(350)
	SET @subject = dbo.f_translate('POWIADOMIENIE DO SPRAWY: ',default) + isnull(@caseId, '') + dbo.f_translate(' szkoda / ',default) + isnull(@regNumber, '')


	IF @type = 1
	BEGIN

		IF ISNULL(@overrideEmails, '') <> ''
		BEGIN

			SET @noteTitle = dbo.f_translate('Wysłano informacje o szkodzie do ',default) + @overrideEmails

		END
		ELSE
		BEGIN

			SET @noteTitle = 'Wysłano informacje o szkodzie do szkody@alphabet.pl, szkodyalphabet@starter24.pl'

		END

	END
	ELSE
	BEGIN

		SET @noteTitle = dbo.f_translate('Wysłano monit o szkodzie do serwisu - ',default) + isnull(cast(@type as nvarchar(1)), '')

	END


	IF @eventTypeInt in (1,
		5,
		6,
		8
		)
	BEGIN
		EXEC p_note_new @groupProcessId        = @groupProcessInstanceId
		,               @type                  = dbo.f_translate('email',default)
		,               @content               = @noteTitle
		,               @phoneNumber           = NULL
		,               @email                 = @partnerMail
		,               @subject               = @subject
		,               @direction             = 1
		,               @emailRegards          = 1
		,               @err                   = @err OUTPUT
		,               @message               = @message OUTPUT
		,
		-- FOR EMAIL
		                @dw                    = @dwMail
		,               @udw                   = ''
		,               @sender                = @senderEmail
		,               @additionalAttachments = ''
		,               @emailBody             = @mailbody
	END
END
