



ALTER PROCEDURE [dbo].[s_1148_003]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @previousStepId NVARCHAR(255)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessId INT
	DECLARE @replacementCarStartLocationId INT
	DECLARE @rootId INT
	DECLARE @eta DATETIME
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	SELECT @groupProcessId = group_process_id, @rootId = root_id
	FROM process_instance
	where id = @previousProcessId
			
	DECLARE @value NVARCHAR(255)
	DECLARE @description NVARCHAR(4000)
		
		EXEC dbo.p_get_business_config
		@key = dbo.f_translate('ha.conditions',default), 
		@groupProcessInstanceId = @groupProcessId, 
		@value = @value OUTPUT,
		@description = @description OUTPUT

	IF isnull(@description,'') <> ''
	BEGIN
		EXEC p_attribute_edit
		@attributePath = '951', 
		@groupProcessInstanceId = @groupProcessId,
		@stepId = 'xxx',
		@valueText = @description,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	END

	set @variant = 1

	 
END
