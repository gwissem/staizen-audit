ALTER PROCEDURE [dbo].[s_1007_064]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int = 1,
	@errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @newProcessId INT
	DECLARE @stepId VARCHAR(32)
	DECLARE @body VARCHAR(MAX)
	DECLARE @email VARCHAR(400)
	DECLARE @subject VARCHAR(400)
	DECLARE @request INT
	DECLARE @towingFixingGroup INT
	DECLARE @sourceId INT
	DECLARE @targetId INT
	DECLARE @notes NVARCHAR(MAX)
	DECLARE @pickUpLocation NVARCHAR(MAX)
	DECLARE @dropOffLocation NVARCHAR(MAX)
	DECLARE @makeModelClass NVARCHAR(255)
	DECLARE @rentalDays INT
	DECLARE @makeModelId INT
	DECLARE @platformId INT
	DECLARE @programId NVARCHAR(255)
	DECLARE @rentalDuration INT
	DECLARE @customDate DATETIME
	DECLARE @wishes NVARCHAR(MAX)
	DECLARE @eta datetime 
	DECLARE @programIds NVARCHAR(255)
	declare @partnerLocId int 
	SELECT 
		@groupProcessInstanceId = group_process_id, 
		@stepId = step_id,
		@rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	EXEC p_form_controls @instance_id = @previousProcessId, @returnResults = 0
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	SELECT TOP 1 @towingFixingGroup = group_process_id FROM dbo.process_instance with(nolock) WHERE step_id like '1009.%' AND root_id = @rootId order by id desc
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @partnerLocId = value_int FROM @values
 	
 	SELECT @email = dbo.f_partner_contact(@groupProcessInstanceId,@partnerLocId, null, 4)
 	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @platformId = value_int FROM @values
 	
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @programId = value_string FROM @values
 	
 	UPDATE av1 SET av1.value_string = av2.value_string, av1.value_int = av2.value_int, av1.value_text = av2.value_text, av1.updated_at = GETDATE() 
 	FROM dbo.attribute_value av1  with(nolock)
 	INNER JOIN dbo.attribute_value av2  with(nolock) ON av2.attribute_path = av1.attribute_path AND av2.attribute_path like '838,%'
 	WHERE av1.group_process_instance_id = @groupProcessInstanceId
 	AND av2.group_process_instance_id = @towingFixingGroup
 	
--	DELETE FROM @values
--	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '838', @groupProcessInstanceId = @towingFixingGroup
-- 	SELECT @sourceId = id FROM @values
-- 	
-- 	DELETE FROM @values
--	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '838', @groupProcessInstanceId = @groupProcessInstanceId
-- 	SELECT @targetId = id FROM @values
-- 	
-- 	EXEC  [dbo].[P_attribute_copy_structure]
--	@source_attribute_value_id = @sourceId,
--	@target_attribute_value_id = @targetId,
--	@process_instance_id = @previousProcessId
	
   	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '838,63', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @notes = value_text FROM @values
 	
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @makeModelId = value_int FROM @values
 	
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,285', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @customDate = value_date FROM @values
 	
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,63', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @wishes = value_text FROM @values
 	
 	SELECT @makeModelClass = dc.textD
	FROM dbo.dictionary dc
	INNER JOIN dbo.dictionary d ON TRY_PARSE(d.argument3 AS INT) = dc.value AND d.typeD = 'makeModel'
	WHERE dc.typeD = 'makeModelClass'
	AND d.value = @makeModelId
	
	IF @makeModelClass IS NULL
	BEGIN
		SET @makeModelClass = dbo.f_translate('SAME',default)
	END 
	
 	EXEC p_location_string
	@attributePath = '283,110,85',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@type = 1,
	@locationString = @dropOffLocation OUTPUT
 	
	EXEC p_location_string
	@attributePath = '283,111,85',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@type = 1,
	@locationString = @pickUpLocation OUTPUT
	
	DECLARE @dropOffLocationText NVARCHAR(MAX)
	DECLARE @LocationText NVARCHAR(MAX)
	DECLARE @wishesText NVARCHAR(MAX)
	
	IF ISNULL(@dropOffLocation,'') <> '' OR ISNULL(@pickUpLocation,'') <> '' OR @customDate IS NOT NULL OR ISNULL(@wishes,'') <> ''
	BEGIN
		SET @notes = '<br/>--- REPLACEMENT VEHICLE ---'
		
		IF ISNULL(@dropOffLocation,'') <> ''
		BEGIN
			SET @notes = @notes + '<br/>Drop off location: '+ISNULL(@dropOffLocation,'')
		END
		
		IF ISNULL(@pickUpLocation,'') <> ''
		BEGIN
			SET @notes = @notes + '<br/>Pick up location: '+ISNULL(@pickUpLocation,'')
		END 
		
		IF @customDate IS NOT NULL
		BEGIN
			SET @notes = @notes + '<br/>Pick up scheduled at: '+dbo.FN_VDateHour(@customDate)
		END 
		
		IF ISNULL(@wishes,'') <> ''
		BEGIN
			SET @notes = @notes + '<br/>Client wishes: '+ISNULL(@wishes,'')
		END 
	END 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
 	
 	EXEC [dbo].[p_attribute_edit]
   	@attributePath = '838,839', 
   	@groupProcessInstanceId = @groupProcessInstanceId,
   	@stepId = 'xxx',
   	@userId = 1,
   	@originalUserId = 1,
   	@valueInt = 3,
   	@err = @err OUTPUT,
   	@message = @message OUTPUT
	
   	EXEC [dbo].[p_attribute_edit]
   	@attributePath = '838,63', 
   	@groupProcessInstanceId = @groupProcessInstanceId,
   	@stepId = 'xxx',
   	@userId = 1,
   	@originalUserId = 1,
   	@valueText = @notes,
   	@err = @err OUTPUT,
   	@message = @message OUTPUT
	
   		
	DECLARE @workingDaysOnly INT
	
	EXEC [dbo].[p_service_day_limit] 
	@groupProcessInstanceId = @groupProcessInstanceId, 
	@serviceId = 3,
	@days = @rentalDuration OUTPUT,
	@workingDaysOnly = @workingDaysOnly OUTPUT
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values

	IF @workingDaysOnly = 1
	BEGIN
		SELECT @rentalDuration = dbo.f_working_to_calendar_days(@rentalDuration, @eta)
	END 
	
	EXEC [dbo].[p_attribute_edit]
   	@attributePath = '837,134', 
   	@groupProcessInstanceId = @groupProcessInstanceId,
   	@stepId = 'xxx',
   	@userId = 1,
   	@originalUserId = 1,
   	@valueInt = @rentalDuration,
   	@err = @err OUTPUT,
   	@message = @message OUTPUT
   	
   	EXEC [dbo].[p_attribute_edit]
   	@attributePath = '837,230', 
   	@groupProcessInstanceId = @groupProcessInstanceId,
   	@stepId = 'xxx',
   	@userId = 1,
   	@originalUserId = 1,
   	@valueString = @makeModelClass,
   	@err = @err OUTPUT,
   	@message = @message OUTPUT
	
	-- WYSYŁANIE E-MAILA DO KLIENTA
--	DELETE FROM @values
--	INSERT  @values EXEC dbo.p_attribute_get2
-- 		@attributePath = '837,773,368',
-- 		@groupProcessInstanceId = @groupProcessInstanceId
-- 	SELECT @email = value_string FROM @values
 	
	SET @body='Assistance Organization Request.
			<BR>
			Best regards,<BR>
			Starter24'
					
	SET @subject = dbo.f_translate('Assistance Organization Request. Ref: ',default) + dbo.f_caseId(@previousProcessId)
	
	-- TODO , dynamiczny załącznik
	
	PRINT dbo.f_translate('Email: ',default) + CAST(ISNULL(@email, '') AS VARCHAR(MAX))
	PRINT dbo.f_translate('Subject: ',default) + CAST(@subject AS VARCHAR(MAX))
	
	
	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rentals')
	DECLARE @platformGroup nvarchar(255)	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		SET @senderEmail = dbo.f_getEmail('cfm')
	END

	EXECUTE dbo.p_note_new 
		 @groupProcessId = @groupProcessInstanceId
		,@type = dbo.f_translate('email',default)
		,@content = dbo.f_translate('Assistance Organization Request',default)
		,@email = @email
		,@userId = 1  -- automat
		,@subject = @subject
		,@direction=1
		,@dw = ''
		,@udw = ''
		,@sender = @senderEmail
		,@additionalAttachments = '{FILE::assistance_organization_request::AssistanceOrganizationRequest::true}'
		,@emailBody = @body
		,@err=@err OUTPUT
		,@message=@message OUTPUT
	
		
		
--	DECLARE @allowJoin INT
--	
--	EXEC dbo.p_can_join_programs @groupProcessInstanceId = @groupProcessInstanceId, @allowJoin = @allowJoin output
--	
--	if @allowJoin = 1
--	begin		
--		DECLARE @hoursTillFix int 
--		declare @fixedRentalDuration int 
--		declare @realDuration int 
--		DECLARE @supervisorAcceptedJoningPrograms INT 
--		declare @fixingCarNotReadyByDate datetime 
--		
--		DELETE FROM @values
--		INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
--		SELECT @eta = value_date FROM @values
--		
--		DELETE FROM @values
--		INSERT @values EXEC p_attribute_get2 @attributePath = '204', @groupProcessInstanceId = @groupProcessInstanceId
--		SELECT @programIds = value_string FROM @values
--		
--		
--		EXEC [dbo].[p_fixing_end_date] 
--		@rootId = @rootId, 
--		@fixingEndDate = @fixingCarNotReadyByDate output
--	
--		SELECT @hoursTillFix = DATEDIFF(hour, @eta, ISNULL(@fixingCarNotReadyByDate,GETDATE()))
--		SELECT @fixedRentalDuration = CEILING(CAST(@hoursTillFix AS FLOAT)/24)
--		
--		DELETE FROM @values
--		INSERT @values EXEC p_attribute_get2 @attributePath = '900', @groupProcessInstanceId = @groupProcessInstanceId
--		SELECT @supervisorAcceptedJoningPrograms = value_int FROM @values
--		
--		DELETE FROM @values
--		INSERT @values EXEC p_attribute_get2 @attributePath = '837,134', @groupProcessInstanceId = @groupProcessInstanceId
--		SELECT @realDuration = value_int FROM @values
--		
--		IF @supervisorAcceptedJoningPrograms IS NULL
--		AND NOT EXISTS(SELECT id from dbo.process_instance where group_process_id = @groupProcessInstanceId and step_id = '1007.072' and active = 1)
--		AND @fixedRentalDuration > @realDuration
--		AND dbo.f_programs_joinable(@programIds, @programId) = 1
--		BEGIN			
--			DECLARE @processInstanceId int
--			
--			EXEC [dbo].[p_process_new] 
--			@stepId = '1007.072', 
--			@userId = 1, 
--			@originalUserId = 1,   
--			@parentProcessId = @rootId, 
--			@rootId = @rootId,
--			@groupProcessId = @groupProcessInstanceId,
--			@err = @err OUTPUT, 
--			@message = @message OUTPUT, 
--			@processInstanceId = @processInstanceId OUTPUT
--			
--		END 		
--	end
	
END


