
ALTER PROCEDURE [dbo].[p_lp_consult_workshop_routing] @groupProcessInstanceId int, @consult int output 
AS
BEGIN
--	set @consult = 0
--	return
--	
	
	SET @consult = 0 
	
	declare @platformId int 
	declare @caseAbroad int 
	declare @eventType int 
	declare @makeModelId int
	declare @firstRegistrationDate datetime
	declare @rootId int 
	declare @programId int 
	declare @rootProgramId int 
	declare @err int 
	declare @message nvarchar(255)
	declare @damageType int 
	
	
	
	select @rootId = root_id from dbo.process_instance where id = @groupProcessInstanceId
	IF @rootId = 812168
	BEGIN 
		SET @consult = 1
		return
	END 
	SELECT @caseAbroad = dbo.f_abroad_case(@groupProcessInstanceId)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
	SELECT @rootProgramId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @rootId
	SELECT @eventType = value_int FROM @values

	delete from @values	
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,244', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @damageType = value_int FROM @values

	DECLARE @arcCode NVARCHAR(20)
	SELECT @arcCode = dbo.f_diagnosis_code(@rootId)
	
	IF @platformId IN (25) AND @caseAbroad = 0 AND @eventType = 2
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @rootId
		SELECT @makeModelId = value_int FROM @values
		
		declare @makeModel nvarchar(50)
		select @makeModel=textD
		from dbo.dictionary
		where value=@makeModelId and typeD='makeModel'

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,561', @groupProcessInstanceId = @rootId
		SELECT @firstRegistrationDate = value_date FROM @values
		
		declare @fleet nvarchar(100)
		exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('wlasciciel',default), @output=@fleet OUTPUT
		
--		if (@arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%')
--		BEGIN
--			SET @consult = 2
--			
--			IF @programId <> @rootProgramId
--			BEGIN
--				declare @newPlatformId int 
--				
--				SELECT top 1 @newPlatformId = platform_id from dbo.vin_program where id = @rootProgramId
--				
--				EXEC [dbo].[p_attribute_edit]
--				   @attributePath = '202', 
--				   @groupProcessInstanceId = @groupProcessInstanceId,
--				   @stepId = 'xxx',
--				   @userId = 1,
--				   @originalUserId = 1,
--				   @valueString = @rootProgramId,
--				   @err = @err OUTPUT,
--				   @message = @message OUTPUT
--				   
--			   EXEC [dbo].[p_attribute_edit]
--				   @attributePath = '253', 
--				   @groupProcessInstanceId = @groupProcessInstanceId,
--				   @stepId = 'xxx',
--				   @userId = 1,
--				   @originalUserId = 1,
--				   @valueInt = @newPlatformId,
--				   @err = @err OUTPUT,
--				   @message = @message OUTPUT
--				   
--			END	
--		END
		if ((@arcCode='3592497' or @arcCode='3002497') and @makeModel like 'Ford%') --and @fleet not like '%APTEKA GEMINI%'
		BEGIN
			SET @consult = 1
			EXEC [dbo].[p_attribute_edit]
				   @attributePath = '238', 
				   @groupProcessInstanceId = @groupProcessInstanceId,
				   @stepId = 'xxx',
				   @userId = 1,
				   @originalUserId = 1,
				   @valueInt = 1,
				   @err = @err OUTPUT,
				   @message = @message OUTPUT
			
		END 
--		else IF dbo.f_isVehiclePremium(@makeModelId) = 0 or DATEDIFF(YEAR,@firstRegistrationDate,GETDATE()) > 2		
--		BEGIN		
--			SET @consult = 1
--		END
		
	END 
	ELSE IF @platformId = 78 AND @caseAbroad = 0 AND (@damageType = 1 OR @arcCode like '119109%' OR @arcCode like '25914%' or @arcCode like '24614%' or @arcCode like '27214%')
	BEGIN
		DECLARE @inRange nvarchar(255)
		DECLARE @parsedText nvarchar(255)
		
		SET @parsedText = IIF(@arcCode like '25914%' or @arcCode like '24614%' or @arcCode like '27214%', '{#isInDateRange(08:00|19:00|09:00|13:00)#}', '{#isInDateRange(08:00|16:00)#}')
		
		EXEC [dbo].[P_parse_string]
		@processInstanceId = @groupProcessInstanceId,
		@simpleText = @parsedText,
		@parsedText = @inRange OUTPUT
		
--		select @inRange, @damageType, @arcCode
		
		IF @inRange = '1' 
		BEGIN
			SET @consult = 1
		END 
		
	END 
	

	--select @platformId platformId, @caseAbroad caseAbroad,@damageType damageType, @arcCode arcCode 
END
