ALTER PROCEDURE [dbo].[P_send_email_to_lease_plan_cancel_towing_notification]
	@previousProcessId INT	-- Może być GROUP / ROOT
AS
BEGIN
	
	/*		
	Odwołanie powiadomienu o Holowaniu jeżeli była tylko N
	- jeżeli wcześniej wysłano powiadomienie o holowaniu, holowanie zostaje odwołane
	- warunek wysłania: holowanie zostaje zmienione na N na drodze
	- wysyłane do: flota@intercars.eu, dw: kzcfm
 		____________________________________*/
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @title NVARCHAR(MAX)
	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	

	SET @content = 'Szanowni Państwo,</br></br>

		Informujemy, o odwołaniu powiadomienia do sprawy {#caseid#}. Awaria /{@74,72@}</br>
		Pojazd nie będzie holowany - samochód został naprawiony na miejscu zdarzenia.</br></br>

		W korespondencji z nami należy zawsze używać adresu mailowego cfm@starter24.pl (kontakt w sprawach Car Fleet Management), pamiętając by w temacie wiadomości zawrzeć nasz numer sprawy (widoczny w temacie niniejszego maila). W sprawach bardzo pilnych uprzejmie prosimy o dodatkowy kontakt telefoniczny pod numerem +48 61 83 19 969
	'

	EXEC [dbo].[P_parse_string]
	 @processInstanceId = @previousProcessId,
	 @simpleText = 'Odwołanie powiadomienia do sprawy [{#caseid#} / {@74,72@}]',
	 @parsedText = @title OUTPUT

	EXEC [dbo].[P_get_body_email]
		@body = @body OUTPUT,
		@contentEmail  = @content,
		@title = @title,
		@previousProcessId = @previousProcessId	
		
	SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'CFM')
	SET @body = REPLACE(@body,'__EMAIL__', 'cfm@starter24.pl')
	 
	 DECLARE @email VARCHAR(400)
	 DECLARE @dw NVARCHAR(400)
	 DECLARE @sender NVARCHAR(400)
	 
	 SET @email = dbo.f_getRealEmailOrTest('flota@intercars.eu')
	 SET @dw = dbo.f_getRealEmailOrTest('kzcfm@starter24.pl')
	 SET @sender = dbo.f_getEmail('cfm')
	 
	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @groupProcessInstanceId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = dbo.f_translate('Odwołanie powiadomienia o holowaniu do InterCars',default)
	 	,@email = @email
	 	,@userId = 1  -- automat
	 	,@subject = @title
	 	,@direction=1
	 	,@dw = @dw
	 	,@udw = ''
	 	,@sender = @sender
	 	,@emailBody = @body
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT
	
END
