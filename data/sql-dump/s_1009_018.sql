ALTER PROCEDURE [dbo].[s_1009_018]
(
	@previousProcessId INT,
    @variant TINYINT OUTPUT,
    @currentUser int, @errId int=0 output
)
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @stepId NVARCHAR(20)
	DECLARE @partnerId INT
	DECLARE @rootId INT
	DECLARE @parentId INT
	DECLARE @processInstanceId INT
	DECLARE @icsId INT
	DECLARE @status INT
	DECLARE @executorPhone NVARCHAR(255)
	DECLARE @uid NVARCHAR(50)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @platformId INT
	DECLARE @eta DATETIME
	DECLARE @fixingOrTowing INT
	DECLARE @platformName NVARCHAR(200)
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @p1 VARCHAR(255)
	DECLARE @p3 VARCHAR(255)
	DECLARE @etaText NVARCHAR(300)
	DECLARE @isParking INT
	DECLARE @onTime TINYINT

	DECLARE @clsPhoneNo NVARCHAR(20)
	DECLARE @partnerLocationId INT
	DECLARE @eventType INT
	DECLARE @clsIsPoland INT
	DECLARE @clsExtraInfo NVARCHAR(100)
	DECLARE @clsServiceName NVARCHAR(100)

	EXEC [dbo].[p_form_controls]
	@instance_id = @previousProcessId,
	@returnResults = 0,
	@returnWithNote = 0

	SELECT @groupProcessInstanceId = p.group_process_id,
	@rootId = p.root_id,
	@parentId = p.parent_id
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s with(nolock) on s.id = p.step_id
	WHERE p.id = @previousProcessId

	declare @caseNumber nvarchar(200)
	SET @caseNumber = ISNULL(dbo.f_caseId(@rootId),'')
			
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '609', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '697', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isParking = value_int FROM @values

	IF ISNULL(@isParking,0) = 0
	BEGIN

		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '107',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @eta = value_date FROM @values

		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '253',
			@groupProcessInstanceId = @rootId
		SELECT @platformId = value_int FROM @values

		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '80,342,408,197',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @callerPhone = value_string FROM @values

		SELECT @platformName = name FROM dbo.platform where id = @platformId

		SET @etaText = ''


		-- Czy na termin
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '215', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @onTime = value_int FROM @values


		SET @p3 = dbo.f_translate('Błąd - brak terminu i ety',default)

		-- Sprawdzanie czy na termin czy ETA
		IF ISNULL(@onTime, 0) = 1
		BEGIN

			DECLARE @onDateTime DATETIME

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @onDateTime = value_date FROM @values

			SET @etaText = dbo.f_translate('Zlecenie umówione na ',default) + dbo.FN_VDateHour(@onDateTime) + '. '
			SET @p3 = convert(nvarchar(255), @onDateTime, 120)
			SET @p3 = @p3 + ' (na termin)'
		END
		ELSE IF @eta IS NOT NULL
		BEGIN

			SET @etaText = dbo.f_translate('Orientacyjny czas dojazdu pomocy to ',default)+dbo.FN_VDateHour(@eta)+'.'
			SET @p3 = convert(nvarchar(255), @eta, 120)
			SET @p3 = @p3 + ' (eta)'
		END

		DECLARE @workshopCapability INT 
		DECLARE @replacementCarText NVARCHAR(1000) = ''
		
		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '700', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopCapability = value_int FROM @values
		
		IF @workshopCapability IN (109,111)
		BEGIN
			SET @replacementCarText = dbo.f_translate('Uprzejmie informujemy, że serwis do którego holowane będzie Pański/Pani samochód, może udostępnić pojazd zastępczy.',default)	
		END 
		
		
		IF @fixingOrTowing = 1
		BEGIN
			SET @content = dbo.f_translate('Uprzejmie informujemy, ze zorganizowalismy holowanie pojazdu. ',default)+isnull(@etaText,'') + isnull(@replacementCarText,'')
		END
		ELSE
		BEGIN
			SET @content = dbo.f_translate('Uprzejmie informujemy, ze zostal wyslany mechanik, ktory podejmie próbe usprawnienia pojazdu na miejscu. ',default)+isnull(@etaText,'')
		END

		-- LOGGER START
		SET @p1 = dbo.f_caseId(@rootId)
		EXEC dbo.p_log_automat
		@name = 'serviceNH_automat',
		@content = dbo.f_translate('Zaplanowane zadanie: "Kontraktor na miejscu."',default),
		@param1 = @p1,
		@param3 = @p3
		-- LOGGER END

		DECLARE @smartviewUrl nvarchar(255)

		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '314', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @smartviewUrl = value_string FROM @values

		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '610',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerId = value_int FROM @values
		DELETE FROM @values


		DECLARE @platformLine NVARCHAR(100)
		SELECT @platformLine = official_line_number from AtlasDB_def.dbo.platform where id = @platformId


		IF (SELECT top 1 1 from partners_cache where id = @partnerId and fixing = 2 ) is not null and
			 left(dbo.f_diagnosis_code(@groupProcessInstanceId),3) in (259, 272, 246)
		BEGIN
				SET @content = dbo.f_translate('Uprzejmie informujemy, ze została  zorganizowana mobilna wulkanizacja, która podejmie próbę usprawnienia pojazdu na miejscu.',default)
			IF @eta IS NOT NULL
				BEGIN

					SET @etaText = dbo.f_translate('Orientacyjny czas dojazdu pomocy to ',default)+dbo.FN_VDateHour(@eta)+'.'
					SET @content = @content +@etaText
				END
				SET @content = @content +dbo.f_translate(' Z powazaniem ',default)+@platformName+dbo.f_translate(', tel. ',default)+isnull(@platformLine,'')
			end

		IF @smartviewUrl IS NOT NULL
		BEGIN
			SET @content = dbo.f_translate('Państwa sprawa numer ',default)+@caseNumber+dbo.f_translate(' w ramach ',default)+@platformName+dbo.f_translate(' jest w trakcie realizacji. Mogą ją Państwo śledzić, klikając następujący link: ',default)+ @smartviewUrl
		END
		
		EXEC dbo.p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @callerPhone,
		@err = @err OUTPUT,
		@message = @message OUTPUT

		DECLARE @isVip TINYINT = 0
		DECLARE @vipStatus nvarchar(20)

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '1004', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @vipStatus = value_string FROM @values

		IF isnull(@vipStatus, '') <> ''
			BEGIN
				SET @isVip = 1
			end


		IF @isVIP = 1
			BEGIN
				DECLARE @vipInfoTask int

				EXEC p_process_new @stepId = '1009.099',
													 @groupProcessId = @groupProcessInstanceId,
													 @rootId = @rootId,
														@parentProcessId = @parentId,
													 @processInstanceId = @vipInfoTask output,
														@err = @err OUTPUT,
					@message = @message OUTPUT

				UPDATE process_instance SET postpone_date = @eta where id = @vipInfoTask
			end


		DECLARE @arcCode nvarchar(255)
		SELECT @arcCode = dbo.f_diagnosis_code(@groupProcessInstanceId)

		DECLARE @partnerPhone nvarchar(255)

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '691', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerPhone = value_string FROM @values

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @eventType = value_int FROM @values






		IF (@arcCode LIKE '262%'
		OR @arcCode LIKE '246%'
		OR @arcCode LIKE '247%'
		OR @arcCode like '259%'
		OR @arcCode like '265%'
		OR @arcCode like '272%'
		OR @arcCode like '273%'
		OR @arcCode like '274%'
		OR @arcCode like '275%'
		OR @arcCode like '276%'
		OR @arcCode like '280%') and @platformId = 48
		BEGIN

			SET @content = 'W przypadku uszkodzenia więcej niż jednej opony informujemy o konieczności wykonania dokumentacji zdjęciowej z miejsca zdarzenia. W przypadku gdy po wymianie uszkodzonego koła na dojazdowe pojazd okaże się niejezdny (np. skrzywiona oś), prosimy o niezwłoczny kontakt z Centrum Zgłoszeniowym.'

			EXEC dbo.p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @partnerPhone,
			@err = @err OUTPUT,
			@message = @message OUTPUT

		END
		ELSE IF @platformId = 25 AND @eventType IN (1,3)
		BEGIN

			SET @content = dbo.f_translate('Informujemy o konieczności wykonania dokumentacji zdjęciowej z miejsca zdarzenia.',default)

			EXEC dbo.p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @partnerPhone,
			@err = @err OUTPUT,
			@message = @message OUTPUT

		END


		declare @isService int

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '212', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @isService = value_int FROM @values

		set @isService=isnull(@isService,1)

		if @platformId=2 and @eventType in (1,3) and @isService<>0
		begin
			declare @regNumber nvarchar(200)
			declare @vinNumber nvarchar(200)
			declare @dateOfCall datetime
			declare @dateOfCallString nvarchar(200)			
			declare @makeModel nvarchar(200)
			declare @programName nvarchar(200)
			declare @driverPhoneNumber nvarchar(200)
			declare @towingOrRepair int
			declare @clsClientDecision int


			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @regNumber = value_string FROM @values

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @vinNumber = value_string FROM @values

			SELECT TOP 1 @dateOfCall = pin.created_at FROM dbo.process_instance pin WITH(NOLOCK) WHERE pin.id = @rootId
			SET @dateOfCallString = ''
			IF @dateOfCall IS NOT NULL
			BEGIN
				SET @dateOfCallString = CONVERT(nvarchar(10),@dateOfCall,120)
			END

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT TOP 1 @makeModel = isnull(d.textD,'') FROM @values v
			join dbo.dictionary d WITH(NOLOCK) ON d.value = v.value_int and d.typeD = 'MakeModel' and d.active = 1

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT TOP 1 @programName = isnull(vp.name,'') FROM @values v
			join dbo.vin_program vp WITH(NOLOCK) ON vp.id = v.value_string

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT  @driverPhoneNumber = value_string FROM @values

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT  @towingOrRepair = value_int FROM @values

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '926', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @clsClientDecision = value_int FROM @values

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @partnerLocationId = value_int FROM @values

			--e-mail do Koordynatora CLS
            declare @mailTo nvarchar(200)
			SET @mailTo =  dbo.f_partner_contact(@groupProcessInstanceId,@partnerLocationId,1,18)

			declare @info nvarchar(100)
			SET @info = dbo.f_translate('Pojazd jest jezdny',default)
			IF @towingOrRepair = 1
			BEGIN
				SET @info = dbo.f_translate('Pojazd wymaga holowania',default)
			END

			declare @clsClientDecisionString nvarchar(100)

			SET @clsClientDecisionString = ''

			IF @clsClientDecision = 1
			BEGIN
				SET @clsClientDecisionString = dbo.f_translate('Tak',default)
			END
			ELSE IF @clsClientDecision = 2
			BEGIN
				SET @clsClientDecisionString = dbo.f_translate('Szkoda została już zgłoszona',default)
			END
			ELSE
			BEGIN
				SET @clsClientDecisionString = dbo.f_translate('Nie',default)
			END

			declare @mailSubject nvarchar(500)

			SET @mailSubject = dbo.f_translate('Informacja do sprawy CLS ',default) + @caseNumber + dbo.f_translate(', nr rej. ',default)
								+ @regNumber + dbo.f_translate(', nr VIN ',default) +  @vinNumber

			declare @body nvarchar(4000)

			SET @body = 'Szanowni Państwo,
				</br></br>
				Poniżej istotne informacje dot. udzielonych świadczeń dla użytkownika pojazdu: '
				+'</br> Marka i model: ' + '<b>' + @makeModel + '</b>'
				+'</br> Numer rejestracyjny: ' + '<b>'+ @regNumber + '</b>'
				+'</br>Numer VIN: ' +'<b>'+ @vinNumber + '</b>'
				+'</br></br> Dodatkowe informacje:'
				+'</br> ' + @info
				+'</br>Decyzja klienta w sprawie przekazania danych do Dealera datyczących szkody: ' + @clsClientDecisionString
				+'</br></br>Uszkodzony pojazd będzie holowany w dniu dzisiejszym do Państwa serwisu w ramach programu: ' + '<b>' + @programName + '</b>'
				+'</br></br>Kontakt telefoniczny do użytkownika pojazdu:'
				+'</br>' + '<b>'+ @driverPhoneNumber + '</b>'
			--	+'</br></br>Inne uwagi:'
				--miejsce na uwagi TODO
				+'</br></br>'

			DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')

			EXECUTE dbo.p_note_new
                 @groupProcessId = @groupProcessInstanceId
                ,@type = dbo.f_translate('email',default)
                ,@content = dbo.f_translate('CLS',default)
                ,@email = @mailTo
                ,@userId = 1  -- automat
                ,@subject = @mailSubject
                ,@direction=1
                ,@dw = ''
                ,@udw = ''
                ,@sender = @senderEmail
                ,@emailBody = @body
                ,@emailRegards = 2
                ,@err=@err OUTPUT
                ,@message=@message OUTPUT

		end
	END



	DECLARE  @platformGrupName NVARCHAR(100)
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGrupName OUTPUT


	IF isnull(@platformGrupName, '') like dbo.f_translate('PSA',default) and isnull(@fixingOrTowing, 1) = 1
		BEGIN
			DECLARE @isASO int = 0

			SET @isASO = (SELECT 1
										from dbo.attribute_value avRodzajPartnera with(nolock)
										where avRodzajPartnera.attribute_path = '595,597,596'
											and avRodzajPartnera.parent_attribute_value_id = @partnerLocationId
											and (SELECT top 1 1
													 from dbo.f_split(value_text, ',')
													 where data in (22, 43, 192, 193)) is not null)

			IF @isASO = 1
				EXEC p_send_email_to_PSA_ASO_towing @previousProcessId = @groupProcessInstanceId
		end
	DECLARE @country nvarchar(100)
	DELETE from @values
	INSERT @values exec p_attribute_get2  @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @country = value_string from @values

  IF isnull(@platformGrupName,'') like dbo.f_translate('PSA',default) and isnull(@country,dbo.f_translate(dbo.f_translate('Polska',default),default)) = dbo.f_translate(dbo.f_translate('Polska',default),default)
    BEGIN
      IF @eventType = 2
				BEGIN
					DECLARE @process30minID int
-- 					monit 30 min
					EXEC p_process_new @stepId = '1009.101',
														 @groupProcessId = @groupProcessInstanceId,
														 @rootId = @rootId,
														 @parentProcessId = @parentId,
														 @processInstanceId = @process30minID output,
														 @err = @err OUTPUT,
														 @message = @message OUTPUT
-- 					monit 24h
					DECLARE @process24hID int
					-- 					monit 30 min
					EXEC p_process_new @stepId = '1009.102',
														 @groupProcessId = @groupProcessInstanceId,
														 @rootId = @rootId,
														 @parentProcessId = @parentId,
														 @processInstanceId = @process24hID output,
														 @err = @err OUTPUT,
														 @message = @message OUTPUT
				end
    end





	SET @variant = 1
	IF @icsId = -1
	BEGIN
		-- SMS
		SET @variant = 3
	END

END
