ALTER PROCEDURE [dbo].[p_towingAutoClose]
@groupProcessInstanceId INT = NULL,
@error INT = 0 OUTPUT
AS
BEGIN
set nocount on
declare @processInstanceId int
DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
declare @icsId int
declare @arrivalKm DECIMAL(18,2)
declare @towingKm DECIMAL(18,2)
declare @towingToParkingKm DECIMAL(18,2)
declare @backKM DECIMAL(18,2)
declare @eta datetime
declare @towing int
declare @partnerX varchar(50)
declare @partnerY varchar(50)
declare @parkingPartnerX varchar(50)
declare @parkingPartnerY varchar(50)
declare @parkingGroup int
declare @parkingPartnerId int
DECLARE @X NVARCHAR(20)
DECLARE @Y NVARCHAR(20)
DECLARE @Xservice NVARCHAR(20)
DECLARE @Yservice NVARCHAR(20)
DECLARE @isParking INT
declare @vehicleType INT
declare @partnerSkills NVARCHAR(255)
declare @arcCode1Value INT
declare @arcCode2Value INT
declare @arcCode3Value INT
DECLARE @newProcessInstanceId INT
DECLARE @peopleToTransport INT = 0
DECLARE @peopleInRsaVehicle INT = 0
declare @peopleInSeparateCar INT		
DECLARE @taxiNextIds varchar(4000)	    
declare @sqlQuery nvarchar(4000)
declare @seats int = 2
DECLARE @processInstanceIds varchar(4000)
declare @code nvarchar(50)
declare @rootId INT
declare @mileage int 
declare @vehicleMileage int 
declare @reportModel int
declare @arrivalDate datetime
DECLARE @noReport int = 0
declare @reportScheduled int 
declare @scheduled int 
declare @platformId int 
declare @programId int
declare @err int
declare @message nvarchar(500)
declare @platformGroup nvarchar(100)
declare @fileReport int 
declare @dateEnd datetime
declare @makeModel INT
declare @rsaVehicle as table (
	code NVARCHAR(300),
	type INT,
	regNmuber nvarchar(50),
	seats int
)


	EXEC [dbo].[p_platform_group_name] @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup OUTPUT
	
	SET @error = 0

	select @rootId = root_id from dbo.process_instance where id = @groupProcessInstanceId

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,845', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fileReport = value_int FROM @values
	
	/** Nie zamykamy gdy zuploadowany raport z drogi **/
	IF @fileReport IS NOT NULL AND EXISTS(select id from dbo.files where document_id = @fileReport) and @groupProcessInstanceId <> 6221940
	BEGIN			
		SET @error = 11
		return
	END 
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '560',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towing = value_int FROM @values
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '428', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @peopleToTransport = isnull(value_int,0) FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @reportModel = value_int FROM @values
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '609',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,214', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @arrivalDate = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,268', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @dateEnd = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '107', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,273', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleType = value_int FROM @values	
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,75', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleMileage = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @mileage = value_int FROM @values
	 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '697', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isParking = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,720', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @arcCode1Value = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,721', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @arcCode2Value = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,722', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @arcCode3Value = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,215', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @reportScheduled = value_int FROM @values
	
	IF @arcCode1Value IS NULL OR @arcCode2Value IS NULL 
	BEGIN
		declare @ARCCode nvarchar(100)
		set @ARCCode=dbo.f_diagnosis_code(@groupProcessInstanceId)
		
		select @arcCode1Value=value from dbo.dictionary where argument2= left(@ArcCode,3) and typeD='ArcCode1'
		select @arcCode2Value=value from dbo.dictionary where argument2= substring(@ArcCode,4,2)and typeD='ArcCode2'
		
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '638,720', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@userId = 1,
		@originalUserId = 1,
		@valueint = @arcCode1Value,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '638,721', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@userId = 1,
		@originalUserId = 1,
		@valueint = @arcCode2Value,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
	END 
	
	IF @reportModel is null 
	BEGIN  
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '638,223,73', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = @makeModel,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT	
	END 
	
	IF @reportScheduled is null 
	begin 
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '215', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @scheduled = value_int FROM @values
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '638,215', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = @scheduled,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	end 

	if @vehicleMileage is NULL
	BEGIN
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '638,223,75', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = @mileage,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	END 
	ELSE IF (@platformGroup IN ('VGP','PSA') AND ISNULL(@mileage,@vehicleMileage) <> @vehicleMileage) OR (@vehicleMileage > @mileage)
	BEGIN			
		SET @error = 12
		return
	END 
	
	PRINT '-----------------------iCS'
	PRINT @icsId
	PRINT '-----------------------TOWING'
	PRINT @towing
	PRINT '-----------------------ISPARKING'
	PRINT @isParking
	
	IF @towing = 1 AND ISNULL(@isParking,0) = 0
	BEGIN
		SELECT @parkingGroup = a.groupId from (
			select pin.group_process_id groupId, row_number()over(partition by ss.group_process_id order by ss.id desc) rowN 
			FROM dbo.service_status ss
			inner join dbo.process_instance pin with(nolock) on ss.group_process_id = pin.group_process_id
			inner join dbo.process_instance pin2 with(nolock) on pin2.root_id = pin.root_id
			inner join dbo.attribute_value av with(nolock) on av.group_process_instance_id = pin.group_process_id and av.attribute_path = '697'
			inner join dbo.service_status_dictionary ssd with(nolock) on ssd.id = ss.status_dictionary_id
			where pin2.group_process_id = @groupProcessInstanceId
			and av.value_int = 1
			and ssd.progress not in (5,6)
		) a
		where a.rowN = 1
		
		IF @parkingGroup is not NULL
		BEGIN
			DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @parkingGroup
			SELECT @parkingPartnerId = value_int FROM @values
		
			select	@parkingPartnerX=avX.value_string, 
					@parkingPartnerY=avY.value_string
			from	dbo.attribute_value avLoc with(nolock)
					inner join dbo.attribute_value avX with(nolock) on avX.parent_attribute_value_id=avLoc.id and avX.attribute_path='595,597,85,92' and avX.value_string is not null
					inner join dbo.attribute_value avY with(nolock) on avY.parent_attribute_value_id=avLoc.id and avY.attribute_path='595,597,85,93' and avY.value_string is not null
					inner join dbo.attribute_value avServices with(nolock) on avServices.parent_attribute_value_id=avLoc.parent_attribute_value_id and avServices.attribute_path='595,597,611'
					inner join dbo.attribute_value avServiceType with(nolock) on avServiceType.parent_attribute_value_id=avServices.id and avServiceType.attribute_path='595,597,611,612' and avServiceType.value_int = 3 
					inner join dbo.attribute_value avSkills with(nolock) on avSkills.parent_attribute_value_id=avServices.id and avSkills.attribute_path='595,597,611,641'
			where	avLoc.attribute_path='595,597,85' and 
					avLoc.parent_attribute_value_id=@parkingPartnerId
					
			PRINT '---------------parkingGroup'
			PRINT @parkingGroup
		END 
	END 
		
	PRINT '---noReport'
	PRINT @noReport

--IF SYSTEM_USER = dbo.f_translate('andrzej.dziekonski',default)
--BEGIN
-- 	SELECT * from dbo.attribute_value with(nolock) where group_process_instance_id = @groupProcessInstanceId and attribute_path in ('638,214','638,130','638,268')
--END

	if @icsId<=0 OR (@arrivalDate is null and @dateEnd is null)
	begin
		print dbo.f_translate('Zamknięcie SMS',default)

		IF @isParking = 1
		BEGIN
			SELECT @eta = dbo.f_find_first_working_day_after_with_saturday(@eta,'08:30:00') 
		END 
		
		EXEC dbo.p_attribute_edit
			@attributePath = '638,214', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueint = null,
			@valuestring = null,
			@valuedate = @eta,
			@valuedecimal = null,
			@valuetext = null,
			@err = @err OUTPUT,
			@message = @message OUTPUT 
		
		declare @dateEndOnSite datetime
		set @dateEndOnSite=dateadd(minute,30,@eta)

		EXEC dbo.p_attribute_edit
			@attributePath = '638,130', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueint = null,
			@valuestring = null,
			@valuedate = @dateEndOnSite,
			@valuedecimal = null,
			@valuetext = null,
			@err = @err OUTPUT,
			@message = @message OUTPUT 
		
		if @towing = 1
		BEGIN			
			set @dateEnd=dateadd(minute,120,@eta)

			EXEC dbo.p_attribute_edit
			@attributePath = '638,268', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueint = null,
			@valuestring = null,
			@valuedate = @dateEnd,
			@valuedecimal = null,
			@valuetext = null,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		END
		
	END
	
--IF SYSTEM_USER = dbo.f_translate('andrzej.dziekonski',default)
--BEGIN
-- 	SELECT * from dbo.attribute_value with(nolock) where group_process_instance_id = @groupProcessInstanceId and attribute_path in ('638,214','638,130','638,268')
--END
 
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '638,217',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @arrivalKm = value_decimal FROM @values
	
	PRINT '-------------000'
	
	IF @icsId<=0 OR ISNULL(@arrivalKm,0) = 0 OR 1=1
	BEGIN
		PRINT '-------------111'
		declare @partnerId int

		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '610',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerId = value_int FROM @values

		declare @serviceType int = 2
		if @towing = 1
		BEGIN
			SET @serviceType = 3
		END 		

		select	@partnerX=avX.value_string, 
				@partnerY=avY.value_string,
				@partnerSkills = avSkills.value_string
		from	dbo.attribute_value avLoc with(nolock)
				inner join dbo.attribute_value avX with(nolock) on avX.parent_attribute_value_id=avLoc.id and avX.attribute_path='595,597,85,92' and avX.value_string is not null
				inner join dbo.attribute_value avY with(nolock) on avY.parent_attribute_value_id=avLoc.id and avY.attribute_path='595,597,85,93' and avY.value_string is not null
				inner join dbo.attribute_value avServices with(nolock) on avServices.parent_attribute_value_id=avLoc.parent_attribute_value_id and avServices.attribute_path='595,597,611'
				inner join dbo.attribute_value avServiceType with(nolock) on avServiceType.parent_attribute_value_id=avServices.id and avServiceType.attribute_path='595,597,611,612' and avServiceType.value_int = @serviceType 
				inner join dbo.attribute_value avSkills with(nolock) on avSkills.parent_attribute_value_id=avServices.id and avSkills.attribute_path='595,597,611,641'
		where	avLoc.attribute_path='595,597,85' and 
				avLoc.parent_attribute_value_id=@partnerId
		
		delete from @values	
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '101,85,92',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @X = value_string FROM @values

		delete from @values

		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '101,85,93',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @Y = value_string FROM @values

		
		delete from @values	
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '211,85,92',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @Xservice = value_string FROM @values

		delete from @values

		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '211,85,93',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @Yservice = value_string FROM @values


		IF @isParking = 1
		BEGIN			
			EXEC [dbo].[p_attribute_edit]
		     @attributePath = '638,665', 
		     @groupProcessInstanceId = @groupProcessInstanceId,
		     @stepId = 'xxx',
		     @userId = 1,
		     @originalUserId = 1,
		     @valueInt = 1,
		     @err = @err OUTPUT,
		     @message = @message OUTPUT
			
			EXEC [dbo].[p_add_rsa_extra_cost]
			@groupProcessInstanceId = @groupProcessInstanceId,
			@type = 5,
			@quantity = 1,
			@value = 20.00
			print dbo.f_translate('parking ppppppppppp',default)
			SET @arrivalKm = 0.0
		END 
		ELSE
		BEGIN
			EXECUTE dbo.p_map_distance 
			   @longitude1=@X
			  ,@latitude1=@Y
			  ,@longitude2=@partnerX
			  ,@latitude2=@partnerY
			  ,@roundResult=1
			  ,@distance=@arrivalKm OUTPUT	
		END 
		

		
		PRINT '-------------222'
		
		--set @arrivalKm=1.2*dbo.FN_distance(@partnerY,@partnerX,@Y,@X) 

		if @towing=1
		begin
			PRINT '-------------333'
			
			-- pojazd wykonujący
			IF @vehicleType is NULL
			BEGIN
				EXEC [dbo].[p_attribute_edit]
			      @attributePath = '638,273', 
			      @groupProcessInstanceId = @groupProcessInstanceId,
			      @stepId = 'xxx',
			      @userId = 1,
			      @originalUserId = 1,
			      @valueInt = 1,
			      @err = @err OUTPUT,
			     @message = @message OUTPUT
			END 
			
			IF @isParking = 1
			BEGIN
				print '5555555555555555555555555555555555555555555'
				print @Xservice
				print @Yservice
				print @partnerX
				print @partnerY
			

				EXECUTE dbo.p_map_distance 
			   @longitude1=@Xservice
			  ,@latitude1=@Yservice
			  ,@longitude2=@partnerX
			  ,@latitude2=@partnerY
			  ,@roundResult=1
			  ,@distance=@towingKm OUTPUT

			  print @towingKm
			  print '5555555555555555555555555555555555555555555'

			  

			END 
			ELSE
			BEGIN				
				PRINT '----towingKM'
				PRINT @towingKM
				PRINT '---X'
				PRINT @X
				PRINT '---Y'
				PRINT @Y
				PRINT '---Xs'
				PRINT @Xservice
				PRINT '---Ys'
				PRINT @Yservice
				EXECUTE dbo.p_map_distance 
				   @longitude1=@X
				  ,@latitude1=@Y
				  ,@longitude2=@Xservice
				  ,@latitude2=@Yservice
				  ,@roundResult=1
				  ,@distance=@towingKm OUTPUT
			  
			  IF @parkingGroup is not null
			  BEGIN					
				  DECLARE @parkingTowingKm DECIMAL(10,2)				  
				  EXECUTE dbo.p_map_distance 
				   @longitude1=@Xservice
				  ,@latitude1=@Yservice
				  ,@longitude2=@parkingPartnerX
				  ,@latitude2=@parkingPartnerY
				  ,@roundResult=1
				  ,@distance=@parkingTowingKm OUTPUT
				  
				  PRINT '---------------parkingX'
				  PRINT @Xservice
				  PRINT '---------------parkingY'
				  PRINT @Yservice
				  PRINT '---------------parkingX'
				  PRINT @parkingPartnerX
				  PRINT '---------------parkingY'
				  PRINT @parkingPartnerY
				  
				  SET @towingKm = @towingKm + ISNULL(@parkingTowingKm,0.00)
				  
				  PRINT '--------------parkingTowingiKm'
				  PRINT @parkingTowingKm
			  END 
			  
			  
			END 
			
			EXECUTE dbo.p_map_distance 
			   @longitude1=@Xservice
			  ,@latitude1=@Yservice
			  ,@longitude2=@partnerX
			  ,@latitude2=@partnerY
			  ,@roundResult=1
			  ,@distance=@backKM OUTPUT

		  
		  
		  -- kod ARC-3 (holowanie ogólne)
	  	  SET @arcCode3Value = 140  
		   
	--		set @towingKm=1.2*dbo.FN_distance(@Yservice,@Xservice,@Y,@X) 
	--		set @backKM=1.2*dbo.FN_distance(@partnerY,@partnerX,@Yservice,@Xservice)
			
		end
		else
		begin
			PRINT '-------------444'
			set @backKM=@arrivalKm
			
			
			-- kod ARC-3 (naprawa ugólne)
			IF @arcCode3Value <> 200
			BEGIN
				SET @arcCode3Value = 130	
			END 		  	
	       
	       -- pojazd wykonujący
			IF @vehicleType is NULL AND dbo.f_exists_in_split(@partnerSkills,'60') = 1 
			BEGIN
				EXEC [dbo].[p_attribute_edit]
			      @attributePath = '638,273', 
			      @groupProcessInstanceId = @groupProcessInstanceId,
			      @stepId = 'xxx',
			      @userId = 1,
			      @originalUserId = 1,
			      @valueInt = 2,
			      @err = @err OUTPUT,
			      @message = @message OUTPUT
			END 
		end

		EXEC [dbo].[p_attribute_edit]
	       @attributePath = '638,722', 
	       @groupProcessInstanceId = @groupProcessInstanceId,
	       @stepId = 'xxx',
	       @userId = 1,
	       @originalUserId = 1,
	       @valueInt = @arcCode3Value,
	       @err = @err OUTPUT,
	       @message = @message OUTPUT
	       
		EXEC dbo.p_attribute_edit
			@attributePath = '638,217', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueint = null,
			@valuestring = null,
			@valuedate = null,
			@valuedecimal = @arrivalKm,
			@valuetext = null,
			@err = @err OUTPUT,
			@message = @message OUTPUT 

		if @towing=1 
		begin
			EXEC dbo.p_attribute_edit
				@attributePath = '638,225', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueint = null,
				@valuestring = null,
				@valuedate = null,
				@valuedecimal = @towingKm,
				@valuetext = null,
				@err = @err OUTPUT,
				@message = @message OUTPUT 
		end

		IF @parkingGroup IS NOT NULL AND @parkingPartnerId = @partnerId
		BEGIN			
			SET @backKM = IIF(@groupProcessInstanceId = @parkingGroup,@towingKm,0)	
		END 
		
		EXEC dbo.p_attribute_edit
			@attributePath = '638,218', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueint = null,
			@valuestring = null,
			@valuedate = null,
			@valuedecimal = @backKM,
			@valuetext = null,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
	end

	EXEC dbo.p_attribute_edit
			@attributePath = '692', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueint = 2,
			@err = @err OUTPUT,
			@message = @message OUTPUT
	
	------------------------------------
	----- taxi
	------------------------------------
	
	if isnull(@peopleToTransport,0) = 0 OR @towing <> 1 OR @isParking = 1
	BEGIN
		EXEC p_attribute_edit
		@attributePath = '638,145', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 0,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		   			
	END 
	ELSE
	BEGIN
		
		EXEC p_attribute_edit
		@attributePath = '638,145', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
		IF @icsId > 0
		BEGIN
			-- sprawdzenie jakim pojazdem wykonał zlecenie ICS
			SET @sqlQuery = 'select * from OPENQUERY(teka2, ''select o.service_vehicle_data, null bbb, null aaa, null ccc FROM StarterTeka.orders o where o.id = '''''+CAST(@icsId AS NVARCHAR(10))+'''''   '') a'
			insert into @rsaVehicle
			exec (@sqlQuery)
			
			SELECT TOP 1 @code = code FROM @rsaVehicle
			
			SET @sqlQuery = 'select * from OPENQUERY(teka2, ''select '''''+@code+''''', s.rodzajPojazdu, s.nrRej, s.iloscMiejsc FROM teka2.samochody s where s.QRCode = '''''+@code+'''''   '') a'
			
			delete from @rsaVehicle
			insert into @rsaVehicle
			exec (@sqlQuery)
			
			SELECT TOP 1 @seats = isnull(seats,2) FROM @rsaVehicle
		END
		
		if @peopleToTransport > isnull(@seats,2) AND @towing = 1
		BEGIN
			set @peopleInSeparateCar = @peopleToTransport - isnull(@seats,2)
			DECLARE @separateCarKm INT 
			
			SET @separateCarKm = ISNULL(@arrivalKm,0) + ISNULL(@towingKm,0)
			
			EXEC p_attribute_edit
			@attributePath = '638,645,145', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC p_attribute_edit
			@attributePath = '638,645,154', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = @peopleInSeparateCar,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC p_attribute_edit
			@attributePath = '638,645,647', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = @separateCarKm,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC p_attribute_edit
			@attributePath = '638,644,154', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = 2,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
		END
		ELSE IF @towing = 1
		BEGIN
			
			EXEC p_attribute_edit
			@attributePath = '638,645,145', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = 0,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC p_attribute_edit
			@attributePath = '638,644,154', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = @peopleToTransport,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
		END 
		
		------------------------------------
		----- new process taxi
		------------------------------------
		
		IF @peopleToTransport > 2 AND NOT EXISTS (
			SELECT id from dbo.process_instance with(nolock) where step_id = '1016.021' and root_id = @rootId 
		)
		BEGIN
			EXEC [dbo].[p_process_new] 
			@stepId = '1016.021', 
			@userId = 1, 
			@originalUserId = 1, 
			@previousProcessId = NULL, 		
			@parentProcessId = @rootId, 
			@rootId = @rootId,
			@groupProcessId = NULL,
			@err = @err OUTPUT, 
			@message = @message OUTPUT, 
			@processInstanceId = @newProcessInstanceId OUTPUT
			
			EXEC [dbo].[p_attribute_edit]
		    @attributePath = '856', 
		    @groupProcessInstanceId = @newProcessInstanceId,
		    @stepId = 'xxx',
		    @userId = 1,
		    @originalUserId = 1,
		    @valueInt = @groupProcessInstanceId,
		    @err = @err OUTPUT,
		    @message = @message OUTPUT
			
		    EXEC [dbo].[p_process_next]
	         @previousProcessId = @newProcessInstanceId,
	         @err = @err OUTPUT,
	         @message = @message OUTPUT,
	         @processInstanceIds = @taxiNextIds OUTPUT		    
		END 
		
	END 
	
--	IF @doNext = 1
--	BEGIN
--		
--		EXECUTE dbo.p_process_next 
--		   @previousProcessId=@processInstanceId
--		  ,@userId=1
--		  ,@variant=2
--		  ,@err=@err OUTPUT
--		  ,@message=@message OUTPUT
--		  ,@processInstanceIds=@processInstanceIds OUTPUT
--		  
--	END 

	DECLARE @status INT 
	SET @status = IIF(@towing = 1, 44, 1005)
	
	EXEC [dbo].[p_add_service_status]
	@groupProcessInstanceId = @groupProcessInstanceId,
	@status = @status,
	@serviceId = @towing
	
	IF @error = 0 
	BEGIN
		SET @newProcessInstanceId = null 
		
		EXEC [dbo].[p_process_new]
		@stepId = '1009.015',
		@userId = 1,
		@originalUserId = 1,
		@parentProcessId = @rootId,
		@rootId = @rootId,
		@groupProcessId = @groupProcessInstanceId,
		@err = @err OUTPUT,
		@message = @message OUTPUT,
		@processInstanceId = @newProcessInstanceId OUTPUT
		
		update dbo.process_instance set active = 0 where id = @newProcessInstanceId
		
	END 
	
	PRINT '##################'
	
	
	
	PRINT '### isParking ###'
	PRINT @isParking
	
	PRINT '### X ###'
	PRINT @X
	
	PRINT '### Y ###'
	PRINT @Y

	PRINT '### partnerX ###'
	PRINT @partnerX
	
	PRINT '### partnerY ###'
	PRINT @partnerY
	
	PRINT '### serviceX ###'
	PRINT @Xservice
	
	PRINT '### serviceY ###'
	PRINT @Yservice
	
	PRINT '### ARRIVAL KM ###'
	PRINT @arrivalKm
	
	PRINT '### TOWING KM ###'
	PRINT @towingKm
	
	PRINT '### BACK KM ###'
	PRINT @backKm
	
	IF ISNULL(@arrivalKm,0.00) = 0.00 AND ISNULL(@towingKm,0.00) = 0.00 AND ISNULL(@backKm,0.00) = 0.00
	BEGIN
		SET @error = 13		
		RETURN
	END 
	
	EXEC [dbo].[p_attribute_edit]
    @attributePath = '638,63', 
    @groupProcessInstanceId = @groupProcessInstanceId,
    @stepId = 'xxx',
    @userId = 1,
    @originalUserId = 1,
    @valueText = 'OK (zamknięte odgórnie)',
    @err = @err OUTPUT,
    @message = @message OUTPUT
 
    EXEC [dbo].[p_attribute_edit]
    @attributePath = '690', 
    @groupProcessInstanceId = @groupProcessInstanceId,
    @stepId = 'xxx',
    @userId = 1,
    @originalUserId = 1,
    @valueString = dbo.f_translate('Odgórne zamknięcie',default),
    @err = @err OUTPUT,
    @message = @message OUTPUT
    
--	
--	IF SYSTEM_USER = dbo.f_translate('andrzej.dziekonski',default)
--	BEGIN	 	 	
--		declare @serviceId int 
--		declare @updatedAt datetime 
--		declare @orgDate datetime 
--		declare @id int 
--		declare @plContractor int 
--		select top 1 @orgDate = created_at from dbo.process_instance with(nolock) where group_process_id = @groupProcessInstanceId and step_id = '1009.040'
--		
--		select @plContractor = 1 from dbo.process_instance with(nolock) where group_process_id = @groupProcessInstanceId and step_id = '1009.103'
--		
--		DELETE FROM @values
--		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
--		SELECT @serviceId = value_int, @id = id FROM @values
--		
--		select top 1 @updatedAt = updated_at from dbo.attribute_value with(nolock) where id = @id
--		
--		SELECT @isParking dbo.f_translate('is.parking',default), @plContractor dbo.f_translate('pl.kontraktor',default), @arrivalKm dbo.f_translate('arrival',default),@towingKm dbo.f_translate('towing',default),@backKM dbo.f_translate('return',default), @serviceId dbo.f_translate('id.serwis',default) ,@updatedAt dbo.f_translate('set.serwis',default), @orgDate dbo.f_translate('data.org',default), @Xservice dbo.f_translate('x.serwis',default), @Yservice dbo.f_translate('y.serwis',default), @partnerX dbo.f_translate('x.kontraktor',default), @partnerY dbo.f_translate('y.kontrator',default)
--		RETURN
--		
--	 END
	 
	
	/** WYCENA **/
	BEGIN TRY  
    	EXEC p_towingPrice @groupProcessId = @groupProcessInstanceId  
	END TRY  
	BEGIN CATCH  
	    DECLARE @errMsg NVARCHAR(MAX)
	    DECLARE @errLine INT
	    SET @errLine = ERROR_LINE()
	    SET @errMsg = CAST(@errLine AS NVARCHAR(200)) + ': ' + ERROR_MESSAGE () 
		INSERT INTO dbo.log (name,content,param1,param2) VALUES ('towing_price_error',@errMsg ,@rootId, @groupProcessInstanceId)	
	END CATCH  
	
END




