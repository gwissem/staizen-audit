ALTER PROCEDURE [dbo].[s_1153_001]
	(
		@previousProcessId INT,
		@variant TINYINT OUTPUT,
		@currentUser int,
		@errId int=0 output
	)
AS
	BEGIN
		DECLARE @err INT
		DECLARE @message NVARCHAR(255)
		DECLARE @groupProcessInstanceId INT
		DECLARE @programId nvarchar(255)
		declare @header nvarchar(4000)
		declare @footer nvarchar(4000)
		declare @body nvarchar(4000)
		DECLARE @answerId INT
		declare @email nvarchar(200) = 'CHANGE_EMAIL_IN_1153_001'
		--	set @email = 'uslugi@alphabet.pl'
		declare @senderEmail nvarchar(200) = [dbo].[f_getEmail]( dbo.f_translate('cfm',default) )
		declare @subject nvarchar(255)
		declare @regNumber nvarchar(255)


		DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

		SELECT @groupProcessInstanceId = group_process_id FROM process_instance with(nolock) where id = @previousProcessId

		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '737', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @body = value_text FROM @values

		delete from @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '737', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @body = value_text FROM @values

		delete from @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '113', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @header = value_text FROM @values

		delete from @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '114', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @footer = value_text FROM @values

		delete from @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @regNumber = value_string FROM @values

		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '865', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @answerId = value_int FROM @values

		IF @answerId IN (100, 90, 190, 320, 360)
			BEGIN
				SET @email = [dbo].[f_getRealEmailOrTest]('serwis@alphabet.pl')
			END
		ELSE IF @answerId in ( 260, 280)
			BEGIN
				SET @email = [dbo].[f_getRealEmailOrTest]('ubezpieczenia@alphabet.pl')
			END
		ELSE IF @answerId IN (730, 740, 870)
			BEGIN
				SET @email = [dbo].[f_getRealEmailOrTest]('uslugi@alphabet.pl')
			END
		ELSE IF @answerId IN (430)
			BEGIN
				SET @email = [dbo].[f_getRealEmailOrTest]('szkody@alphabet.pl')
			END

		set @subject = @regNumber + ' / ' + dbo.f_caseId(@groupProcessInstanceId)
		set @body = isnull(@header+'<br/><br/>','')+isnull(@body+'<br/><br/>','')+isnull('<hr/>'+@footer+'<br/>','')

		if @body <> ''
			BEGIN
				EXECUTE dbo.p_note_new
						@groupProcessId = @groupProcessInstanceId
						,@type = dbo.f_translate('email',default)
						,@content = @body
						,@email = @email
						,@userId = 1  -- automat
						,@subject = @subject
						,@direction=1
						,@dw = ''
						,@udw = ''
						,@sender = @senderEmail
						--	    ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
						,@emailBody = @body
						,@emailRegards = 1
						,@err=@err OUTPUT
						,@message=@message OUTPUT
			END

		EXEC dbo.p_jump_to_service_panel @previousProcessId = @previousProcessId

	END