ALTER PROCEDURE [dbo].[p_send_truck_authorization_from_buttons]
  (
    @groupProcessInstanceId int,
    @variant                TINYINT = 0,
    @user                   int
  )
AS
  BEGIN


    DECLARE @platformId INT
    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))
    DECLARE @info NVARCHAR(MAX)
    DECLARE @stepId NVARCHAR(255)
    DECLARE @programId INT
    DECLARE @err INT
    DECLARE @message NVARCHAR(255)

    declare @content nvarchar(max)
    declare @email nvarchar(200)
    declare @subject nvarchar(200)
    declare @starterEmail nvarchar(200)
    declare @helpline nvarchar(100)
    declare @platform nvarchar(100)
    declare @caseId nvarchar(200)
    declare @partnerName nvarchar(200)
    declare @cardNumber int
    declare @cardNumberText NVARCHAR(20)
    declare @footer nvarchar(200)
    declare @customerPhone nvarchar(200)
    declare @regNumber nvarchar(100)
    declare @service nvarchar(200)

    declare @additionalInfoAuthorization nvarchar(2000)


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,63',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @additionalInfoAuthorization = value_text FROM @values


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int FROM @values

    DECLARE @engLang tinyint = 0;
    DECLARE @langTxt nvarchar(4)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,1025',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @langTxt = value_string from @values
    IF isnull(@langTxt, 'PL') = dbo.f_translate('ENG',default)
      BEGIN

        SET @engLang = 1
      END

    IF @platformId = 8 --dkv
      BEGIN

        SELECT @platform = name, @helpline = official_line_number FROM dbo.platform where id = @platformId
        SET @starterEmail = [dbo].[f_getEmail]('truck')

        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '84',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @partnerName = value_string FROM @values

        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '176',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @cardNumberText = value_string FROM @values


        PRINT @cardNumberText
        DECLARE @cardNumberTypeId int
        DECLARE @cardNumberType nvarchar(5)
        DECLARE @cardActiveTill nvarchar(50)
        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '266',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @cardNumberTypeId = value_int from @values

        SET @cardNumberType = dbo.f_dictionaryDesc(@cardNumberTypeId, 'DKVCards')
        PRINT @cardNumberType

        PRINT @cardNumberText
        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '197',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @customerPhone = value_string FROM @values


        DELETE from @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '186', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @cardActiveTill = value_string FROM @values

        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @regNumber = value_string FROM @values

        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @regNumber = value_string FROM @values

        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '260',
                                                 @groupProcessInstanceId = @groupProcessInstanceId

        IF @engLang = 0
          BEGIN
            SELECT @service = textD
            from dbo.dictionary
            where value = (select top 1 value_int FROM @values)
              and typeD = 'DKVProduct'
          END
        ELSE
          BEGIN
            SELECT @service = argument1
            from dbo.dictionary
            where value = (select top 1 value_int FROM @values)
              and typeD = 'DKVProduct'
          end


        If isnull(@service, '') = ''
          BEGIN

            DELETE FROM @values
            INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '190',
                                                     @groupProcessInstanceId = @groupProcessInstanceId

            IF @engLang = 0
              BEGIN
                SELECT @service = textD
                from dbo.dictionary
                where value = (select top 1 value_int FROM @values)
                  and typeD = 'STSServicesNeeded'
              END
            ELSE
              BEGIN
                SELECT @service = argument1
                from dbo.dictionary
                where value = (select top 1 value_int FROM @values)
                  and typeD = 'STSServicesNeeded'
              end
          end

        DECLARE @attributePath nvarchar(100)
        DECLARE @type int
        DECLARE @geoLink int
        DECLARE @locationString nvarchar(400)

        set @caseId = dbo.f_caseId(@groupProcessInstanceId)
        print @caseId
        DECLARE @tickBox NVARCHAR(400) = '<div style="height:25px; width:25px; border:1px solid black;text-align:center;float:left;display:inline-block;margin-right:20px">&#10004;</div>'
        DECLARE @tickBoxEmpty NVARCHAR(400) = '<div style="height:25px; width:25px; border:1px solid black;text-align:center;float:left;display:inline-block;margin-right:20px"></div>'

        DECLARE @commission int

        DELETE from @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,1034',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @commission = value_int from @values


        DECLARE @authorizationOld nvarchar(15)
        DECLARE @authorizationOldDecimal decimal(10, 2)
        DECLARE @authorizationDelta nvarchar(15)
        DECLARE @authorizationDeltaDecimal decimal(10, 2)
        DECLARE @authorization nvarchar(15)
        DECLARE @authorizationDecimal decimal(10, 2)
        DECLARE @authorizationCurrencyText nvarchar(3)


        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,1022',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @authorizationDecimal = value_decimal from @values


        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,556',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @authorizationCurrencyText = value_string from @values


        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,1032',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @authorizationOldDecimal = value_decimal from @values

        SET @authorizationOldDecimal = isnull(@authorizationOldDecimal, 0)

        SET @authorizationDeltaDecimal = @authorizationDecimal - @authorizationOldDecimal

        SET @authorizationOld = convert(nvarchar, @authorizationOldDecimal)
        SET @authorizationDelta = convert(nvarchar, @authorizationDeltaDecimal)
        SET @authorization = convert(nvarchar, @authorizationDecimal)



        IF @variant in (3, 4)
          BEGIN

            declare @body nvarchar(max)
            DECLARE @domain VARCHAR(100) = [dbo].[f_getDomain]()


            IF @engLang = 0
              BEGIN
                select @body = '
	<!DOCTYPE html>
	<html lang="pl">
	<head>
	    <meta charset="utf-8">
	            <link rel="stylesheet" type="text/css"  href="' + @domain + '/bundles/web/css/vendor/bootstrap/bootstrap.min.css" >
	    </head>
	<body>
	     <div class="container-fluid" style="padding-top: 20px; padding-bottom: 20px;">
	         <div class="row">
	            <div class="col-xs-9 text-left">
	                <h3>___TITLE___</h3>
	            </div>
	        </div>

	         <div class="row" style="padding-top: 30px;">
	            <div class="col-xs-8">
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Data:</b>
	                    </div>
	               <div class="col-xs-10">
	                        ___DATE_TIME___
	                    </div>
	              </div>
	            </div>
	             <div class="col-xs-4">
	                 <div class="row text-right">
	                     <div class="col-xs-12">
	                         <h5>___PLATFORM_NAME___ </h5>
	                     </div>
	                     <div class="col-xs-12">
	                         ___STARTER_EMAIL___
	                     </div>
	                 </div>
	             </div>
	         </div>
	     </div>
	    <div class="container-fluid">
	        <div class="row" style="border-top: 1px #000 dotted; padding-top: 15px; padding-bottom: 15px;">
	            <div class="col-xs-10 col-xs-offset-1">
	                ___CONTENT___

	            </div>
	        </div>
	        <div class="row" style="padding-top: 10px">
	            ___FOOTER___
	        </div>
	    </div>
	</body>
	</html>'
              end
            ELSE
              BEGIN
                -- angielski
                SET @body = '
	<!DOCTYPE html>
	<html lang="pl">
	<head>
	    <meta charset="utf-8">
	            <link rel="stylesheet" type="text/css"  href="' + @domain + '/bundles/web/css/vendor/bootstrap/bootstrap.min.css" >
	    </head>
	<body>
	     <div class="container-fluid" style="padding-top: 20px; padding-bottom: 20px;">


	         <div class="row" style="padding-top: 30px;">
	            <div class="col-xs-8">
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Date:</b>
	                    </div>
	               <div class="col-xs-10">
	                        ___DATE_TIME___
	                    </div>
	              </div>
	            </div>
	             <div class="col-xs-4">
	                 <div class="row text-right">
	                     <div class="col-xs-12">
	                         <h5>___PLATFORM_NAME___ </h5>
	                     </div>
	                     <div class="col-xs-12">
	                         ___STARTER_EMAIL___
	                     </div>
	                 </div>
	             </div>
	         </div>
	     </div>
	    <div class="container-fluid">
	        <div class="row" style="border-top: 1px #000 dotted; padding-top: 15px; padding-bottom: 15px;">
	            <div class="col-xs-10 col-xs-offset-1">
	                ___CONTENT___

	            </div>
	        </div>
	        <div class="row" style="padding-top: 10px">
	            ___FOOTER___
	        </div>
	    </div>
	</body>
	</html>'
              end


            DECLARE @limitClientDecimal decimal(10, 2)
            DECLARE @limit nvarchar(20)

            DECLARE @limitDKVDecimal decimal(10, 2)
            DECLARE @limitDKV nvarchar(20)


            DELETE FROM @values
            INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,1030',
                                                     @groupProcessInstanceId = @groupProcessInstanceId
            SELECT @limitClientDecimal = value_decimal from @values

            If isnull(@limitClientDecimal, -1) <> -1
              BEGIN
                SET @limit = cast(@limitClientDecimal as nvarchar(20))
              end
            DELETE FROM @values
            INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,1031',
                                                     @groupProcessInstanceId = @groupProcessInstanceId
            SELECT @limitDKVDecimal = value_decimal from @values
            If isnull(@limitDKVDecimal, -1) <> -1
              BEGIN
                SET @limitDKV = cast(@limitDKVDecimal as nvarchar(20))
              end



            if @variant = 3 -- treść variant 3
              begin
                DELETE FROM @values
                INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '368',
                                                         @groupProcessInstanceId = @groupProcessInstanceId
                SELECT @email = value_string FROM @values


                EXECUTE dbo.p_location_string
                    @attributePath = '624,626'
                    , @groupProcessInstanceId = @groupProcessInstanceId
                    , @locationString = @locationString OUTPUT


                IF @engLang = 0
                  BEGIN
                    set @subject = dbo.f_translate('Autoryzacja ',default) + @caseId
                    set @partnerName = isnull(@partnerName, '') + '<br>' + isnull(@locationString, '')

                    set @content = 'Szanowni Państwo,<br><br> uprzejmie prosimy o autoryzację. Bez pisemnego potwierdzenia nie będzie możliwości udzielenia autoryzacji dla partnera świadczącego usługę.<br><br><br>'
                    set @content = @content + '<table>'

                    set @content = @content + '<tr><td><b>Autoryzacja dla:</b></div><td>' + isnull(@partnerName, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Numer karty:</b><td>' + isnull(@cardNumberText, '') +
                                   '</td></tr>'
                    set @content =
                    @content + '<tr><td><b>Kod RC / data ważności:</b><td>' + isnull(@cardNumberType, '') + ' / ' +
                    isnull(@cardActiveTill, '') + '</td></tr>'
                    set @content = @content + '<tr><td><b>Numer telefonu:</b><td>' + isnull(@customerPhone, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Numer rejestracyjny:</b><td>' + isnull(@regNumber, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Produkt / usługa:</b><td>' + isnull(@service, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td>(Państwa limit)</td></tr>'
                    set @content = @content + '<tr><td>(A)<b>Limit:</b><td>' + isnull(@limit, '') +
                                   ' EUR</td></tr></table>'


                    SET @content = @content + '<div >'
                  end

                IF @engLang = 1
                  BEGIN
                    set @subject = dbo.f_translate('Authorization ',default) + @caseId
                    set @partnerName = isnull(@partnerName, '') + '<br>' + isnull(@locationString, '')

                    set @content = 'Dear ladies and gentlemen,<br><br> we kindly ask you for an authorisation. Without your written confirmation we will not give the authorisation.<br><br><br>'
                    set @content = @content + '<table>'

                    set @content = @content + '<tr><td><b>Name of customer:</b></div><td>' + isnull(@partnerName, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Card Number:</b><td>' + isnull(@cardNumberText, '') +
                                   '</td></tr>'
                    set @content =
                    @content + '<tr><td><b>RC Code / Date of Expiry:</b><td>' + isnull(@cardNumberType, '') + ' / ' +
                    isnull(@cardActiveTill, '') + '</td></tr>'
                    set @content = @content + '<tr><td><b>Tel. Customer:</b><td>' + isnull(@customerPhone, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>License plate:</b><td>' + isnull(@regNumber, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Product/service:</b><td>' + isnull(@service, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td>(your deposited limit)</td></tr>'
                    set @content = @content + '<tr><td>(A)<b>Customer limit:</b><td>' + isnull(@limit, '') +
                                   ' EUR</td></tr></table>'


                    SET @content = @content + '<div >'
                  end


                DECLARE @questionType int = 0
                DECLARE @questionTypeBaseId int

                DELETE FROM @values
                INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '248',
                                                         @groupProcessInstanceId = @groupProcessInstanceId
                SELECT @questionTypeBaseId = value_int from @values
                DELETE from @values

                IF @questionTypeBaseId in (1, 2, 6, 7)
                  BEGIN
                    SET @questionType = 1
                  end
                ELSE IF @questionTypeBaseId = 3
                  BEGIN
                    SET @questionType = 5
                  end
                ELSE IF @questionTypeBaseId = 4
                  BEGIN
                    SET @questionType = 3
                  end


                SET @content = @content + '<div><table style="line-height:20px">' +
                               '<tr><td>'
                IF @questionType = 1
                  BEGIN
                    SET @content = @content + @tickBox
                  end
                ELSE
                  BEGIN
                    SET @content = @content + @tickBoxEmpty
                  end
                IF @engLang = 0
                  BEGIN
                    SET @content =
                    @content + dbo.f_translate('To jest prośba o udzielenie autoryzacji na kwotę ',default) + isnull(@authorization, '') + ' ' +
                    isnull(@authorizationCurrencyText, dbo.f_translate('PLN',default)) + ' +VAT'
                  end
                ELSE
                  BEGIN
                    SET @content =
                    @content + dbo.f_translate('It is an authorisation request for an amount of ',default) + isnull(@authorization, '') + ' ' +
                    isnull(@authorizationCurrencyText, dbo.f_translate('PLN',default)) + ' +VAT'
                  end
                SET @content = @content + '</td></tr>'


                SET @content = @content + '<tr><td>'
                IF @questionType = 2
                  BEGIN
                    SET @content = @content + @tickBox
                  end
                ELSE
                  BEGIN
                    SET @content = @content + @tickBoxEmpty
                  end
                IF @engLang = 0
                  BEGIN
                    SET @content = @content + dbo.f_translate('Prośba jest związana z istniejącą autoryzacją na kwotę  ',default) +
                                   isnull(@authorizationOld, '') + dbo.f_translate(', która musi być zwiększona o ',default) +
                                   isnull(@authorizationDelta, '') + dbo.f_translate('. Nowa łączna wartośc autoryzacji to:  ',default) +
                                   isnull(@authorization, '') + ' ' + isnull(@authorizationCurrencyText, dbo.f_translate('PLN',default)) +
                                   ' + VAT.'
                  END
                ELSE
                  BEGIN

                    SET @content =
                    @content + dbo.f_translate('This request is related to an existing authorisation of',default) + isnull(@authorizationOld, '')
                    + ', this authorisation is to be increased
                  by' + isnull(@authorizationDelta, '') + dbo.f_translate(' . The new complete authorisation amount is now: ',default) +
                    isnull(@authorization, '') + ' ' + isnull(@authorizationCurrencyText, dbo.f_translate('PLN',default)) + ' + VAT.'
                  END


                SET @content = @content + '</td></tr>'

                SET @content = @content + '<tr><td>'
                IF @questionType = 3
                  BEGIN
                    SET @content = @content + @tickBox
                  end
                ELSE
                  BEGIN
                    SET @content = @content + @tickBoxEmpty
                  end

                IF @engLang = 0
                  BEGIN
                    SET @content = @content + 'Nieprawidłowy kod RC, Karta nie ma autoryzacji na tą usługę (osobno będą fakturowane koszty
	obsługi tej autoryzacji)'
                  END
                ELSE
                  BEGIN
                    SET @content = @content + dbo.f_translate('wrong RC code, Card isn',default)'t authorised for the requested service (invoicing separate intervention cost
for this authorisation)'
                  end
                SET @content = @content + '</td></tr>'

                SET @content = @content + '<tr><td>'
                IF @questionType = 4
                  BEGIN
                    SET @content = @content + @tickBox
                  end
                ELSE
                  BEGIN
                    SET @content = @content + @tickBoxEmpty
                  end

                If @engLang = 0
                  BEGIN

                    SET @content = @content +
                                   '	Karta DKV zgubiona lub zablokowana (osobno będą fakturowane koszty obsługi tej autoryzacji)'
                  END
                ELSE
                  BEGIN
                    SET @content = @content +
                                   ' Missing DKV card or DKV Card is blocked (invoicing separate intervention cost for this authorisation)'
                  end
                SET @content = @content + '</td></tr>'

                SET @content = @content + '<tr><td>'

                IF @questionType = 5
                  BEGIN
                    SET @content = @content + @tickBox
                  end
                ELSE
                  BEGIN
                    SET @content = @content + @tickBoxEmpty
                  end

                IF @engLang = 0
                  BEGIN
                    SET @content = @content +
                                   'Użyto złego lub zapomniano kodu PIN (osobno będą fakturowane koszty obsługi tej autoryzacji)'
                  END
                ELSE
                  BEGIN
                    SET @content = @content +
                                   ' Using wrong PIN Code or missing PIN Code (invoicing separate intervention cost for this authorisation)'
                  end
                SET @content = @content + '</td></tr></table></div>'


                SET @content = @content + '</div><br /> <br />' + '<div style="font-weight:small;">'

                IF @engLang = 0
                  BEGIN
                    SET @content = @content +
                                   'Uwagi: Kwota autoryzacji usług paliwowych uwzględnia VAT, (kwota paliwa razem z VAT = kwota żądanej autoryzacji).</div>'
                  END
                ELSE
                  BEGIN
                    SET @content = @content +
                                   'Info: For fuel service authorisations the amount includes the VAT, (fuel amount incl. VAT = authorisation request amount).</div>'
                  end


                IF @engLang = 0
                  BEGIN
                    SET @content = @content + '<br /> <div>Usługa / specyfikacja:<br> ' +
                                   +isnull(@additionalInfoAuthorization, '') +
                                   '</div>'
                  END
                ELSE
                  BEGIN
                    SET @content = @content + '<br /> <div>Service/specifics <br> ' +
                                   +isnull(@additionalInfoAuthorization, '') +
                                   '</div>'


                  end

                if isnull(@commission, 0) > 0
                  BEGIN

                    IF @engLang = 0
                      BEGIN
                        SET @content = @content + 'Dla tej usługi zostaną Państwo obciążeni osobno kosztem interwencji w wysokości 99 EUR + VAT. <br />
Powyższe koszty interwencji nie są uwzględnione w żądanej kwocie autoryzacji'
                      END
                    ELSE
                      BEGIN
                        SET @content = @content + 'For this service would a separate intervention cost of 99 EUR + VAT charged.
This intervention cost is not included in the authorisation request'
                      END

                  end

                SET @content = @content + '<hr>'

                IF @engLang = 0
                  BEGIN

                    SET @content = @content +
                                   '<div><div>(Potwierdzenie KLIENTA, proszę zaznaczyć TAK lub NIE i odesłać) </div><BR />'
                  END
                ELSE
                  BEGIN
                    SET @content = @content +
                                   '<div><div>(Confirmation by CUSTOMER, please mark YES or NO and sent it back) </div><BR />'
                  end


                IF @engLang = 0
                  BEGIN

                    SET @content =
                    @content +
                    '<div>Limit klienta wskazany w (A) został przekroczony. Akceptacja pełnej kwoty autoryzacji '
                    +
                    isnull(@authorization, '') + ' ' + isnull(@authorizationCurrencyText, '') + ' +VAT </div>'

                  END
                ELSE
                  BEGIN
                    SET @content = @content +
                                   '<div>The CUSTOMER limit referred (A) has been exceeded. The authorisation of the full amount '
                                   +
                                   isnull(@authorization, '') + ' ' + isnull(@authorizationCurrencyText, '') +
                                   ' +VAT is approved</div>'

                  end


                IF @engLang = 0
                  BEGIN
                    SET @content =
                    @content + '<table><tr><td style="min-width:100px;line-height:25px;">' + @tickBoxEmpty +
                    'TAK</td><td style="min-width:100px;line-height:25px;"> ' + @tickBoxEmpty +
                    'NIE </td></tr></table></div>'
                  END
                ELSE
                  BEGIN
                    SET @content =
                    @content + '<table><tr><td style="min-width:100px;line-height:25px;">' + @tickBoxEmpty +
                    'YES</td><td style="min-width:100px;line-height:25px;"> ' + @tickBoxEmpty +
                    'NO </td></tr></table></div>'
                  end
                SET @content = @content + '<br />'

                IF @engLang = 0
                  BEGIN

                    SET @content = @content +
                                   'W celu autoryzacji za zgubioną lub zablokowaną kartę proszę podać numer innej Karty DKV do użycia w celu autoryzacji: <br /><br />'

                    SET @content = @content + '<br />' +
                                   '<table width="100%"><tr>' +
                                   '<td> Data:</td><td>Czytelny podpis</td><td>Pieczątka firmowa </td>' +
                                   '</tr></table>' +
                                   '<br /> ' +
                                   '<br /> ' +
                                   '<div> Niniejszy dokument może być odesłany jako PDF (Email) lub Faks.</div>'
                  END
                ELSE
                  BEGIN

                    SET @content = @content +
                                   'For authorisation by missing or blocked DKV Card please give another DKV Card number
    for using authorisation: <br /><br />'

                    SET @content = @content + '<br />' +
                                   '<table width="100%"><tr>' +
                                   '<td> Date:</td><td>Name (block letter)</td><td>signature/Stamp CUSTOMER </td>' +
                                   '</tr></table>' +
                                   '<br /> ' +
                                   '<br /> ' +
                                   '<div>This document can be send as PDF (Email) or Fax.</div>'
                  end


              END
            ELSE IF @variant = 4  -- treść 4
              BEGIN

                DELETE FROM @values
                INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1021,368',
                                                         @groupProcessInstanceId = @groupProcessInstanceId
                SELECT @email = value_string FROM @values


                print '@content'
                print @content
                EXECUTE dbo.p_location_string
                    @attributePath = '624,626'
                    , @groupProcessInstanceId = @groupProcessInstanceId
                    , @locationString = @locationString OUTPUT


                PRINT '@caseId'
                PRINT @caseId
                PRINT '@caseId'
                PRINT @caseId
                PRINT '@caseId'
                PRINT @caseId

                IF @engLang = 0
                  BEGIN
                    set @subject = dbo.f_translate('Autoryzacja ',default) + @caseId
                    set @partnerName = isnull(@partnerName, '') + '<br>' + isnull(@locationString, '')

                    set @content = 'Szanowni Państwo,<br><br> uprzejmie prosimy o autoryzację. Bez pisemnego potwierdzenia nie będzie możliwości udzielenia autoryzacji dla partnera świadczącego usługę.<br><br><br>'
                    set @content = @content + '<table>'

                    set @content = @content + '<tr><td><b>Autoryzacja dla:</b></div><td>' + isnull(@partnerName, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Numer karty:</b><td>' + isnull(@cardNumberText, '') +
                                   '</td></tr>'
                    set @content =
                    @content + '<tr><td><b>Kod RC / data ważności:</b><td>' + isnull(@cardNumberType, '') + ' / ' +
                    isnull(@cardActiveTill, '') + '</td></tr>'
                    set @content =
                    @content + '<tr><td><b>Numer telefonu do klienta:</b><td>' + isnull(@customerPhone, '') +
                    '</td></tr>'
                    set @content = @content + '<tr><td><b>Numer rejestracyjny:</b><td>' + isnull(@regNumber, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Produkt / usługa:</b><td>' + isnull(@service, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td>(Limit w DKV webtool)</td></tr>'
                    set @content = @content + '<tr><td><b>Limit DKV	:</b><td>' + isnull(@limitDKV, '') +
                                   ' EUR</td></tr>'
                    set @content = @content + '<tr><td><b>Limit klienta	:</b><td>' + isnull(@limit, '') +
                                   ' EUR</td></tr></table><br>'


                    SET @content = @content + '<div>' + @tickBox + ''
                    SET @content =
                    @content + dbo.f_translate('To jest prośba o udzielenie autoryzacji na kwotę  ',default) + @authorization + ' ' +
                    isnull(@authorizationCurrencyText, dbo.f_translate('PLN',default)) + ' + VAT'
                    SET @content = @content + '</div><br>'


                    SET @content = @content + '<div>' + @tickBoxEmpty + ''
                    SET @content =
                    @content + dbo.f_translate('Prośba jest związana z istniejącą autoryzacją na kwotę ',default) + isnull(@authorizationOld, 0)
                    +
                    dbo.f_translate(', która musi być zwiększona o ',default) + isnull(@authorizationDelta, 0) + '.' +
                    dbo.f_translate(' Nowa łączna wartośc autoryzacji to: ',default) + isnull(@authorization, '') + ' ' +
                    isnull(@authorizationCurrencyText, dbo.f_translate('PLN',default)) + ' +VAT.'


                    SET @content = @content + '</div><br>'
                    SET @content = @content +
                                   '<div style="font-size:small">Uwagi: Kwota autoryzacji usług paliwowych uwzględnia VAT, (kwota paliwa razem z VAT = kwota żądanej autoryzacji).</div><br><br><br>'
                    SET @content = @content + '<div>Usługa/specyfika:</div> ' + isnull(@additionalInfoAuthorization, '')
                                   +
                                   '<br><br><br>'


                    if isnull(@commission, 0) > 0
                      BEGIN
                        SET @content = @content + '<div>Dla tej usługi zostaną Państwo obciążeni osobno kosztem interwencji w wysokości 99 EUR + VAT. <br />
Powyższe koszty interwencji nie są uwzględnione w żądanej kwocie autoryzacji</div><br/>'
                      END


                    SET @content = @content +
                                   '<hr ><div>(Potwierdzenie przez organizację DKV proszę zaznaczyć TAK lub NIE i odesłać)</div>'


                    SET @content = @content + '<div><table>'
                    SET @content = @content + '<tr>'
                    SET @content = @content + '<td colspan="2"> Limit DKV wskazany w punkcie 1a) został przekroczony.<br>
Akceptacja dla pełnej kwoty autoryzacji   ' + isnull(@authorization, '') + ' ' + isnull(@authorizationCurrencyText, '')
                                   + ' + VAT</td>'

                    -- 				TAK NIE
                    SET @content = @content + '<td style="line-height:25px;">' + @tickBoxEmpty + 'TAK</td>'
                    SET @content = @content + '<td style="line-height:25px;">' + @tickBoxEmpty + 'NIE</td>'


                    SET @content = @content + '</tr>'
                    SET @content = @content + '<tr>'
                    SET @content = @content + '<td colspan="2"> Jeżeli limit klienta wskazany w punkcie 1b) został przekroczony:<br />
 Klient został poinformowany przez organizację DKV o prośbie
 autoryzacji; ta autoryzacja zawiera zgodę klienta.
</td>'

                    -- 				TAK NIE
                    SET @content = @content + '<td style="line-height:25px;">' + @tickBoxEmpty + 'TAK</td>'
                    SET @content = @content + '<td style="line-height:25px;">' + @tickBoxEmpty + 'NIE</td>'

                    SET @content = @content + '</tr>'
                    -- 				plusinfo
                    SET @content = @content + '<tr><td colspan="4>' +
                                   '(W przypadku zaznaczenia TAK, DKV nie będzie kontaktować się z klientem w celu uzyskania osobnej autoryzacji)</td></tr>'
                    SET @content = @content + '<tr><td colspan="4>' +
                                   '(W przypadku zaznaczenia NIE, DKV skontaktuje się z Klientem, prosząc o autoryzację, jeśli będzie to konieczne)</td></tr>'


                    SET @content = @content + '</table></div><br />'

                    SET @content = @content + '<br />' +
                                   '<table width="100%"><tr>' +
                                   '<td width="33%"> Data:</td><td width="33%">Imię i nazwisko<br>' +
                                   'pracownika DKV (czytelnie)</td><td width="33%">Podpis i pieczątka pracownika DKV </td>'
                                   +
                                   '</tr></table>' +
                                   '<br /> ' +
                                   '<br /> ' +
                                   '<div> Niniejszy dokument może być odesłany jako PDF (Email) lub Faks.</div>'


                  end
                ELSE
                  BEGIN

                    set @subject = dbo.f_translate('Authorization ',default) + isnull(@caseId, '')
                    set @partnerName = isnull(@partnerName, '') + '<br>' + isnull(@locationString, '')

                    set @content = 'Dear ladies and gentlemen,<br><br> we kindly ask you for an authorisation. Without your written confirmation we will not give the authorisation.<br><br><br>'
                    set @content = @content + '<table>'


                    set @content = @content + '<tr><td><b>Name of customer:</b></div><td>' + isnull(@partnerName, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Card Number:</b><td>' + isnull(@cardNumberText, '') +
                                   '</td></tr>'
                    set @content =
                    @content + '<tr><td><b>RC Code/ Date of Expiry:</b><td>' + isnull(@cardNumberType, '') + ' / ' +
                    isnull(@cardActiveTill, '') + '</td></tr>'
                    set @content = @content + '<tr><td><b>Tel. Customer:</b><td>' + isnull(@customerPhone, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>License plate:</b><td>' + isnull(@regNumber, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td><b>Product/service:</b><td>' + isnull(@service, '') +
                                   '</td></tr>'
                    set @content = @content + '<tr><td>(limit in DKV webtool)</td></tr>'
                    set @content = @content + '<tr><td><b>Limit DKV:</b><td>' + isnull(@limitDKV, '') + ' EUR</td></tr>'
                    set @content = @content + '<tr><td>(A)<b>Customer limit:</b><td>' + isnull(@limit, '') +
                                   ' EUR</td></tr></table>'


                    SET @content = @content + '<div>' + @tickBox + ''
                    SET @content =
                    @content + dbo.f_translate('It is an authorisation request for an amount of ',default) + @authorization + ' ' +
                    isnull(@authorizationCurrencyText, dbo.f_translate('PLN',default)) + ' + VAT'
                    SET @content = @content + '</div><br>'

                    PRINT '@content'
                    PRINT '@content'
                    PRINT @content
                    SET @content = @content + '<div>' + @tickBoxEmpty + ''
                    SET @content =
                    @content + dbo.f_translate('This request is related to an existing authorisation of',default) + isnull(@authorizationOld, '')
                    + ', this authorisation is to be increased
                  by' + isnull(@authorizationDelta, '') + dbo.f_translate(' . The new complete authorisation amount is now: ',default) +
                    isnull(@authorization, '') + ' ' + isnull(@authorizationCurrencyText, dbo.f_translate('PLN',default)) + ' + VAT.'
                    SET @content = @content + '</div><br>'
                    SET @content = @content +
                                   'Info: For fuel service authorisations the amount includes the VAT, (fuel amount incl. VAT = authorisation request amount).</div>'
                    SET @content =
                    @content + '<div>Service/specifics:</div> ' + isnull(@additionalInfoAuthorization, '') +
                    '<br><br><br>'


                    if isnull(@commission, 0) > 0
                      BEGIN
                        SET @content = @content + 'For this service would a separate intervention cost of 99 EUR + VAT charged.
This intervention cost is not included in the authorisation request'
                      END


                    SET @content = @content +
                                   '<hr ><div>' + '(Confirmation by DKV, please mark YES or NO and sent it back)</div>'


                    SET @content = @content + '<div><table>'
                    SET @content = @content + '<tr>'
                    SET @content = @content + '<td colspan="2"> The DKV limit referred in 1a) has been exceeded.<br>
The authorization of the full ' + isnull(@authorization, '') + ' ' + isnull(@authorizationCurrencyText, '')
                                   + ' + VAT</td>'

                    -- 				TAK NIE
                    SET @content = @content + '<td style="line-height:25px;">' + @tickBoxEmpty + 'YES</td>'
                    SET @content = @content + '<td style="line-height:25px;">' + @tickBoxEmpty + 'NO</td>'


                    SET @content = @content + '</tr>'
                    SET @content = @content + '<tr>'
                    SET @content = @content + '<td colspan="2"> ' +
                                   'If the customer limit reffered to in 1b) has been exceeded:
        The customer has been informed by DKV organisation about the authorisation
        request; this authorisation includes the customer approval.
        </td>'

                    -- 				TAK NIE
                    SET @content = @content + '<td style="line-height:25px;">' + @tickBoxEmpty + 'YES</td>'
                    SET @content = @content + '<td style="line-height:25px;">' + @tickBoxEmpty + 'NO</td>'

                    SET @content = @content + '</tr>'
                    -- 				plusinfo
                    SET @content = @content + '<tr><td colspan="4>' +
                                   '(if YES is marked, DKV ASSIST will not contact the customer for a separate authorisation)</td></tr>'
                    SET @content = @content + '<tr><td colspan="4>' +
                                   '(if NO is marked, DKV ASSIST will contact the customer for an authorisation request; if necessary)</td></tr>'


                    SET @content = @content + '</table></div><br />'

                    SET @content = @content + '<br />' +
                                   '<table width="100%"><tr>' +
                                   '<td width="33%"> Date:</td><td width="33%"Name DKV employee' +
                                   ' <br/ >(block letter)</td><td width="33%"> signature / stamp: DKV employee</td>'
                                   +
                                   '</tr></table>' +
                                   '<br /> '


                  END
              END
            print @content
            set @footer = ''


            SET @body = REPLACE(@body, '___FOOTER___', isnull(@footer, ''))
            SET @body = REPLACE(@body, '___CASEID___', isnull(@caseId, ''))
            SET @body = REPLACE(@body, '___TITLE___', isnull(@subject, ''))
            SET @body = REPLACE(@body, '___PARTNER_LOCATION___', ISNULL(@partnerName, ''))
            SET @body = REPLACE(@body, '___CONTENT___', isnull(@content, ''))
            SET @body = REPLACE(@body, '___DATE_TIME___', dbo.FN_VDateHour(GETDATE()))
            SET @body = REPLACE(@body, '___HELPLINE___', isnull(@helpline, '+48 600 222 222'))
            SET @body = REPLACE(@body, '___STARTER_EMAIL___', isnull(@starterEmail, ''))
            SET @body = REPLACE(@body, '___PLATFORM_NAME___', isnull(@platform, ''))


            declare @attributeValueId int
            declare @attachment nvarchar(200)
            declare @fileName nvarchar(100)

            set @fileName = @caseId


            EXEC [dbo].[p_attribute_edit]
                @attributePath = '962',
                @groupProcessInstanceId = @groupProcessInstanceId,
                @stepId = 'xxx',
                @userId = 1,
                @originalUserId = 1,
                @valueText = @body,
                @err = @err OUTPUT,
                @message = @message OUTPUT,
                @attributeValueId = @attributeValueId OUTPUT

            SET @attachment = '{ATTRIBUTE::' + CAST(@attributeValueId AS NVARCHAR(20)) + '::' + @fileName + '::true}'


            EXECUTE dbo.p_note_new
                @groupProcessId = @groupProcessInstanceId
                , @type = dbo.f_translate('email',default)
                , @content = @subject
                , @email = @email
                , @userId = 1  -- automat
                , @subject = @subject
                , @direction = 1
                , @dw = ''
                , @udw = ''
                , @sender = @starterEmail
                , @additionalAttachments = @attachment
                , @emailBody = @body
                , @err = @err OUTPUT
                , @message = @message OUTPUT

          END
      END
    ELSE
      BEGIN
        print dbo.f_translate('STS',default)
        print '@user'
        print @user
        exec p_send_authorization_request_truck @groupProcessInstanceId = @groupProcessInstanceId, @userId = @user
      end
  END