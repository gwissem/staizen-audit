ALTER PROCEDURE [dbo].[s_1009_062]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int = 1,
	@errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @newProcessId INT
	DECLARE @stepId VARCHAR(32)
	DECLARE @body VARCHAR(MAX)
	DECLARE @email VARCHAR(400)
	DECLARE @subject VARCHAR(400)
	
	SELECT 
		@groupProcessInstanceId = group_process_id, 
		@stepId = step_id,
		@rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	-- WYSYŁANIE E-MAILA DO KLIENTA
	
	INSERT  @values EXEC dbo.p_attribute_get2
 		@attributePath = '767,773,368',
 		@groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @email = value_string FROM @values
 	
	SET @body='Assistance Organization Request.
			<BR>
			Best regards,<BR>
			Starter24'
					
	SET @subject = dbo.f_translate('Assistance Organization Request. Ref: ',default) + dbo.f_caseId(@previousProcessId)
	
	-- TODO , dynamiczny załącznik
	
	PRINT dbo.f_translate('Email: ',default) + CAST(ISNULL(@email, '') AS VARCHAR(MAX))
	PRINT dbo.f_translate('Subject: ',default) + CAST(@subject AS VARCHAR(MAX))
	
	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('Arc')

	EXECUTE dbo.p_note_new 
		 @groupProcessId = @groupProcessInstanceId
		,@type = dbo.f_translate('email',default)
		,@content = dbo.f_translate('Assistance Organization Request',default)
		,@email = @email
		,@userId = 1  -- automat
		,@subject = @subject
		,@direction=1
		,@dw = ''
		,@udw = ''
		,@sender = @senderEmail
		,@additionalAttachments = '{FILE::assistance_organization_request::AssistanceOrganizationRequest::true}'
		,@emailBody = @body
		,@err=@err OUTPUT
		,@message=@message OUTPUT
	
	
	-- SKOK DO PANELU ŚWIADCZEŃ
	
	SELECT TOP 1 @newProcessId = p.id
	FROM dbo.process_instance p
	WHERE p.root_id = @rootId
	AND p.step_id = '1011.010'
	AND p.active = 1
	ORDER BY p.id DESC
	
	IF @newProcessId IS NOT NULL
	BEGIN
		
		INSERT INTO dbo.process_instance_flow (previous_process_instance_id, next_process_instance_id, active, variant)
		VALUES (@previousProcessId, @newProcessId, 1, 1)
		
	END
	ELSE
	BEGIN
		
		EXEC p_process_jump
		@previousProcessId = @previousProcessId,
		@nextStepId = '1011.010',
		@doNext = 0,
		@parentId = NULL,
		@rootId = @rootId,
		@variant = 1,
		@groupId = @rootId,
	    @err = @errId output,
	    @message = @message output
	    
	END

END



