ALTER PROCEDURE [dbo].[s_1009_051]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @previousStepId NVARCHAR(255)
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @icsId INT
	DECLARE @sqlQuery NVARCHAR(4000)
	DECLARE @status INT
	DECLARE @eta DATETIME
	DECLARE @statusDate DATETIME
	DECLARE @executorPhone NVARCHAR(255)
	DECLARE @postponeCount INT
	DECLARE @postponeLimit INT
	DECLARE @createdAt DATETIME
	DECLARE @smsId INT
	DECLARE @smsText NVARCHAR(1000)
	DECLARE @receivedAt DATETIME
	DECLARE @processId INT
	
	SELECT @groupProcessInstanceId = p.group_process_id,
	@postponeCount = p.postpone_count,
	@postponeLimit = s.postpone_count,
	@createdAt = p.created_at
	FROM dbo.process_instance p with(nolock)
	INNER JOIN dbo.step s with(nolock) ON s.id = p.step_id
	WHERE p.id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	INSERT @values EXEC dbo.p_attribute_get2
	@attributePath = '609',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values
	
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2
	@attributePath = '691',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @executorPhone = value_string FROM @values
			
	IF @icsId > 0
	BEGIN
		
		SET @variant = 1
		
		SET @sqlQuery = 'select * from OPENQUERY(teka2, ''select s.id, IFNULL(o.eta,o.pre_eta), s.status_id, o.id order_id, s.created_at FROM StarterTeka.order_statuses s 
						 inner join StarterTeka.orders o ON o.id = s.order_id where o.id = '''''+CAST(@icsId AS NVARCHAR(10))+'''''   '') a'
			
		declare @statuses as table (
			id int, 
			eta DATETIME,
			status_id INT, 
			order_id INT, 
			created_at DATETIME
		)
	
		delete from @statuses
		insert into @statuses
		exec (@sqlQuery)
		
		SELECT top 1 @status = status_id, @eta = eta, @statusDate = created_at FROM @statuses ORDER by id desc 
		
		IF ISNULL(@status,0) < 5 
		BEGIN
			
			
			UPDATE dbo.process_instance SET user_id = 1 WHERE id = @previousProcessId 
			
			EXEC dbo.p_callTo
			@phoneNumber = @executorPhone,
			@recordNumber = 3,
			@err = @err output,
			@description = @message output
			
			UPDATE dbo.process_instance SET user_id = NULL WHERE id = @previousProcessId
			
		END 
		
		SELECT TOP 1 @processId = id FROM dbo.process_instance with(nolock) WHERE step_id = '1009.004' and active = 1 and group_process_id = @groupProcessInstanceId order by id desc
		IF @processId IS NOT NULL
		BEGIN
			INSERT INTO dbo.process_instance_flow (previous_process_instance_id, next_process_instance_id, created_at, active)
			SELECT @previousProcessId, @processId, GETDATE(), 1
		END 
		
		
	END 
	ELSE
	BEGIN
		
		-- SMS
		SET @variant = 99
		
		DECLARE @content NVARCHAR(4000)
		DECLARE @regNumber NVARCHAR(20)
		DECLARE @makeModel NVARCHAR(100)
		DECLARE @caseId NVARCHAR(20)
		
		IF @postponeCount >= @postponeLimit
		BEGIN
			SET @variant = 1
			SELECT TOP 1 @processId = id FROM dbo.process_instance with(nolock) WHERE step_id = '1009.004' and active = 1 and group_process_id = @groupProcessInstanceId order by id desc
			IF @processId IS NOT NULL
			BEGIN
				INSERT INTO dbo.process_instance_flow (previous_process_instance_id, next_process_instance_id, created_at, active)
				SELECT @previousProcessId, @processId, GETDATE(), 1
			END 
			
			RETURN
		END
		
		IF @postponeCount = 0
		BEGIN
			
			INSERT @values EXEC dbo.p_attribute_get2
			@attributePath = '691',
			@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @executorPhone = value_string FROM @values
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @regNumber = value_string FROM @values
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @makeModel = textD FROM dbo.dictionary WHERE value IN (SELECT value_int FROM @values) AND typeD = 'makeModel'
			
			SET @content = dbo.f_translate('Tu Starter24. Prosimy o potwierdzenie dotarcia na miejsce. Odpisz TAK lub NIE. Zlecenie numer ',default)+dbo.f_caseId(@groupProcessInstanceId)+dbo.f_translate(', pojazd ',default)+ISNULL(@makeModel,'')+' '+ISNULL(@regNumber,'')+'.'
			
			EXEC dbo.p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @executorPhone,
			@addInfo = 0,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
		END
		ELSE
		BEGIN
			SELECT TOP 1 @smsId = id, @smsText = sms_text, @receivedAt = data_otrzymania FROM sms_inbox with(nolock) WHERE numer_telefonu = '48'+@executorPhone and data_otrzymania >= @createdAt AND (sms_text LIKE dbo.f_translate('tak',default) OR sms_text LIKE dbo.f_translate('nie',default))
			
			IF ISNULL(@smsText,'') = dbo.f_translate('tak',default)
			BEGIN
				SET @variant = 2
				
				EXEC [dbo].[p_form_controls]
				@instance_id = @previousProcessId,
				@returnResults = 0
				
				EXEC [dbo].[p_attribute_edit]
			    @attributePath = '638,224', 
			    @groupProcessInstanceId = @groupProcessInstanceId,
			    @stepId = 'xxx',
			    @userId = 1,
			    @originalUserId = 1,
			    @valueDate = @receivedAt,
			    @err = @err OUTPUT,
			    @message = @message OUTPUT
			    
			    EXEC [dbo].[p_attribute_edit]
				@attributePath = '208', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@userId = 1,
				@originalUserId = 1,
				@valueInt = 1,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				EXEC dbo.p_note_new
				@groupProcessId = @groupProcessInstanceId,
				@type = dbo.f_translate('sms',default),
				@content = @smsText,
				@phoneNumber = @executorPhone,
				@addInfo = 0,
				@direction = 2,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
			END 
			ELSE IF ISNULL(@smsText,'') = dbo.f_translate('nie',default)
			BEGIN
				SET @variant = 1
				
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '208', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@userId = 1,
				@originalUserId = 1,
				@valueInt = 0,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
				EXEC dbo.p_note_new
				@groupProcessId = @groupProcessInstanceId,
				@type = dbo.f_translate('sms',default),
				@content = @smsText,
				@phoneNumber = @executorPhone,
				@addInfo = 0,
				@direction = 2,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
			END
			
		END 
		
		UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(SECOND, 30, GETDATE()) where id = @previousProcessId
		
	END 
	
END

