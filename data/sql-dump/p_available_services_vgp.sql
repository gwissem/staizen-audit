ALTER PROCEDURE [dbo].[p_available_services_vgp]
@processInstanceId INT,
@rootId INT = NULL,
@stepId NVARCHAR = NULL,
@servicesIds NVARCHAR(255) = '',
@arcCode NVARCHAR(255) = '',
@fixingEndDate DATETIME = null, 
@eventType INT = null,
@locale NVARCHAR(10) = 'pl',
@init INT = 0
AS
BEGIN
	
	DECLARE @currentPrograms_String NVARCHAR(255)
	DECLARE @currentPrograms_Table TABLE(data NVARCHAR(MAX), id int)
	DECLARE @programId NVARCHAR(25)
	DECLARE @cancel INT
	
	DECLARE @taxiError INT
	DECLARE @fixError INT
	DECLARE @towingError INT
	DECLARE @replacementCarError INT
	DECLARE @partsError INT	
	DECLARE @hotelError INT
	DECLARE @tripError INT
	DECLARE @transportError INT
	DECLARE @loanError INT
	DECLARE @medicalLawAdviceError INT	
	DECLARE @programName NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @servicePartnerId INT
	DECLARE @eventLocationCountry NVARCHAR(255)
	DECLARE @platformName NVARCHAR(255)
	DECLARE @platformId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @programIds NVARCHAR(255)
	DECLARE @accessDeniedConditions INT
	DECLARE @prefferedService INT
	
	SELECT @platformName = name FROM platform with(nolock) where id = @platformId
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	
	IF @rootId IS NULL OR @stepId IS NULL
	BEGIN
		SELECT @rootId = root_id, @stepId = step_id, @groupProcessInstanceId = group_process_id FROM dbo.process_instance WITH(nolock) WHERE id = @processInstanceId
	END
	
	----------------------------------
	--- WARUNKI OGÓLNE 
	----------------------------------
	
	DECLARE @diagnosisSummary NVARCHAR(255)
	SELECT @diagnosisSummary = dbo.f_diagnosis_summary(@groupProcessInstanceId)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventLocationCountry = value_string FROM @values

	
	-- diagnoza wskazuje na NPT
	IF @err = 0 AND @diagnosisSummary IN ('[NpT]', '[NpT-P]', '[NpT-T]') AND ISNULL(@eventLocationCountry,'') = dbo.f_translate('Polska',default)
	BEGIN
		
		DECLARE @partnerName NVARCHAR(200)
		
		CREATE TABLE #partnerTempTable (partnerId INT, partnerName NVARCHAR(300), priority INT, distance DECIMAL(18,2), phoneNumber NVARCHAR(100), reasonForRefusing NVARCHAR(100), partnerServiceId INT, city NVARCHAR(100))
		EXEC p_find_services @groupProcessInstanceId = @groupProcessInstanceId
		SELECT TOP 1 @partnerName = partnerName FROM #partnerTempTable order by distance asc
		DROP TABLE #partnerTempTable
		
		set @message = dbo.f_translate('Telefoniczna diagnoza usterki wskazuje, że może Pan/Pani bezpiecznie dojechać do najbliższego autoryzowanego Partnera Serwisowego ',default)
									 +ISNULL(@platformName,'')+' '+ISNULL(@partnerName,'')+'.'
		 
     	UPDATE #availableServices
		SET
		active = 0,
		message = @message,
		variant = 97
	END
	
	
	
	-- Pojazd należący do służb
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '538', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @accessDeniedConditions = value_int FROM @values
	
	IF @accessDeniedConditions = 1
	BEGIN
		SET @fixError = 1
		SET @taxiError = 1
		SET @replacementCarError = 1
		SET @taxiError = 1
		SET @towingError = 0
		SET @partsError = 1	
		SET @hotelError = 1
		SET @tripError = 1
		SET @transportError = 1
		SET @loanError = 1
		
		UPDATE #availableServices 
		SET
		active = 1,
		message = ''
		WHERE id = 1
		
		UPDATE #availableServices 
		SET
		active = 0,
		message = dbo.f_translate('Pojazdy należące do służb państwowych posiadają assistance ograniczony do gwarancyjnej odpowiedzialności producenta, iż usterka i udzielone świadczenia nie mieszczą się w zakresie tej odpowiedzialności, powstałe koszty doliczone zostan�',default)
		WHERE id = 2
		
		UPDATE #availableServices 
		SET
		active = 0,
		message = dbo.f_translate('Pojazdy należące do Policji, straży pożarnej lub wojska mają ograniczony assistance. Dalsze świadczenia tylko i wyłącznie po potwierdzeniu od partnera serwisowego, że będą rozliczone z producentem',default)
		WHERE id > 2
--			
--			UPDATE #availableServices 
--			SET
--			active = 0,
--			message = dbo.f_translate('Pojazdy należące do Policji, straży pożarnej lub wojska mają ograniczony assistance. Dalsze świadczenia tylko i wyłącznie po potwierdzeniu od partnera serwisowego, że będą rozliczone z producentem',default)
--			WHERE id = 5
	END 
	
	-- AUDI - woli holowanie
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '539', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @prefferedService = value_int FROM @values
	
	IF @prefferedService = 2
	BEGIN
		SET @fixError = 1
		
		UPDATE #availableServices 
		SET
		active = 0,
		message = dbo.f_translate('Klient preferuje holowanie.',default)
		WHERE id = 2
		
		UPDATE #availableServices 
		SET
		active = 1,
		message = ''
		WHERE id = 1 
		-- by Dylesiu  - PO CO TO TUTAJ JEST ? :D
--			and dbo.f_exists_in_split(@servicesIds,'1') = 0
		
	END 
	ELSE IF @prefferedService = 1
	BEGIN
		
		SET @towingError = 1
		
		UPDATE #availableServices 
		SET
		active = 1,
		message = ''
		WHERE id = 2 and dbo.f_exists_in_split(@servicesIds,'2') = 0
		
--			UPDATE #availableServices 
--			SET
--			active = 0,
--			message = dbo.f_translate('Klient preferuje naprawę na miejscu.',default)
--			WHERE id = 1
		
		-- Problem był w AUDI, gdy klient preferował naprawe na drodze, a ona sie odbyła a potem ropoczęto holowanie - Wszystko było nieaktywne - nie można odpalić HOL-PARK-HOL
		IF dbo.f_exists_in_split(@servicesIds,'1') = 0
		BEGIN
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Klient preferuje naprawę na miejscu.',default)
			WHERE id = 1
		
		END
		ELSE
		BEGIN
			
			UPDATE #availableServices 
			SET
			active = 1,
			message = ''
			WHERE id = 1
			
		END
		
	END 

	
	----------------------------------
	--- AUTO ZASTĘPCZE
	----------------------------------
	
	DECLARE @programRuleDate DATETIME 
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @fixingEndDate = value_date FROM @values
	
	-- data naprawy warsztatowej krótsza niż minimalna wg owu
	
	IF @platformId = 31
	BEGIN
		SET @programRuleDate = GETDATE()
	END 
	ELSE IF @platformId = 11
	BEGIN
		SET @programRuleDate = DATEADD(HOUR, 2, GETDATE())
	END 
	ELSE IF @platformId = 6
	BEGIN
		SET @programRuleDate = DATEADD(HOUR, 4 , GETDATE())
	END 
	
	IF @fixingEndDate < @programRuleDate AND @replacementCarError = 0
	BEGIN
		SET @replacementCarError = 1
		
		IF @fixingEndDate < GETDATE()
		BEGIN
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Wg daty naprawy warsztatowej - ',default)+dbo.FN_VDateHour(@fixingEndDate)+' (zadeklarowanej przez serwis w Raporcie Serwisowym) pojazd powinien być już naprawiony. W związku z tym nie możemy zorganizować pojazdu zastępczego,'
			WHERE id = 3	
		END 
		
		UPDATE #availableServices 
		SET
		active = 0,
		message = dbo.f_translate('Data naprawy warsztatowej - ',default)+dbo.FN_VDateHour(@fixingEndDate)+' (zadeklarowana przez serwis w Raporcie Serwisowym) jest krótsza niż minimalna wymagana data wg warunków umowy dla pojazdu zastępczego.'
		WHERE id = 3
	END 
	
	-- Nie organizujemy holowania i nie mamy kompletnego raportu serwisowego
	IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @replacementCarError = 0 AND @arcCode <> '1464391'
	BEGIN
		
		SET @replacementCarError = 1
		
		SET @message = dbo.f_translate('Jeżeli pomoc drogowa nie była organizowana przez ',default)+@platformName+dbo.f_translate(' Assistance, możemy jedynie autoryzować wynajem samochodu zastępczego przez Partnera Serwisowego jeżeli dysponuje takim pojazdem oraz spełnione są warunki assistance. W tym celu prosimy o kontakt z Partnerem Serwisowym zajmującym się napraw�',default)
		IF @platformId = 31 
		BEGIN
			SET @message = dbo.f_translate('Nie mamy możliwości zorganizowania pojazdu zastępczego jeżeli nie był holowany przez ',default)+@platformName+dbo.f_translate(' Assistance.',default)
		END 
		
		UPDATE #availableServices 
		SET
		active = 0,
		message = @message
		WHERE id = 3
	END
	
	
	----------------------------------
	--- NOCLEG
	----------------------------------
	
	-- Jeśli nie organizujemy holowania
	IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @hotelError = 0 AND @arcCode <> '1464391'
	BEGIN
		SET @hotelError = 1

		UPDATE #availableServices
		SET
		active = 0,
		message = dbo.f_translate('Nie mamy możliwości zorganizowania noclegu jeżeli wcześniej nie korzystał Pan/Pani z usługi holowania w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu.',default)
		WHERE id = 4
	END
	
END


