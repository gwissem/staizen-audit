


ALTER PROCEDURE [dbo].[s_1148_009_a]
( 
	 @nextProcessId INT,
	@currentUser INT = NULL,
	@token VARCHAR(50) = NULL OUTPUT
) 
AS
BEGIN
	declare @groupProcessId int
	DECLARE @stepId VARCHAR(100)
	declare @platformSettedId int
	declare @partnerLocId int
	DECLARE @urlMessage NVARCHAR(MAX)
	DECLARE @hashedUrl nvarchar(300)
	DECLARE @partnerMail nvarchar(100) 
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	declare @message nvarchar
	declare @err int
	declare @rootId int
	DECLARE @partnerId INT
	
	select	@groupProcessId=group_process_id, @stepId = step_id,@rootId = root_id
	from	dbo.process_instance
	where	id = @nextProcessId
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessId
	SELECT @partnerId = value_int FROM @values
	
	select @partnerMail=dbo.f_partner_contact(@groupProcessId,@partnerId,19,16)
	if isnull(@partnerMail,'')=''
	begin
		select @partnerMail=dbo.f_partner_contact(@groupProcessId,@partnerId,19,4)
	end

	DECLARE  @newToken VARCHAR(50)
    set @newToken=newid()
    print dbo.f_translate('generuje nowy token',default)
    UPDATE    dbo.process_instance
    SET        token=@newToken            
    where
    id = @nextProcessId

	UPDATE    dbo.process_instance
    SET        postpone_date= dateadd(day,2,getdate())        
    where
    id = @nextProcessId
	and root_id = @rootId
	
	SELECT @hashedUrl = dbo.f_hashTaskUrl(
												@newToken
			)
	SET @hashedUrl = '<a href ="'+@hashedUrl+'" />'+@hashedUrl+'</a>'

	SET @urlMessage = '<HTML>' +
													'<h4>Szanowni Państwo,</h4><br>
Przesyłamy do uzupełnienia raport zamknięcia usługi Home Assistance. Prosimy kliknąć w poniższy link i postępować zgodnie z dalszymi instrukcjami.<br>'
													+ @hashedUrl +
													'</HTML>';
												
        DECLARE @senderEmail VARCHAR(200)
        DECLARE @mailTitle NVARCHAR(255)
		DECLARE @caseId nvarchar(50)
	
		set @caseId=dbo.f_caseId(@rootId)

		SET @mailTitle = dbo.f_translate('Starter24 - linkt aktywacyjny do uzupełnienia raportu zamknięcia usługi Home Assistance ',default) + ISNULL(@caseId,'')

        SET  @senderEmail = [dbo].[f_getEmail]('ha');
        
				EXEC p_note_new
						@groupProcessId =@groupProcessId,
						@type =dbo.f_translate('email',default),
						@content = dbo.f_translate('Wysłano raport zamknięcia usługi Home Assistance',default),
						@phoneNumber = NULL,
						@email = @partnerMail,
						@subject = @mailTitle,
						@direction = 1,
						@err = @err OUTPUT,
						@message = @message OUTPUT,
						@emailRegards = 1,
						-- FOR EMAIL
						@dw = '',
						@udw = '',
						@sender = @senderEmail,
						@additionalAttachments = '',
						@emailBody  = @urlMessage


END
