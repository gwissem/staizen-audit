

ALTER PROCEDURE [dbo].[p_mapReverseGeocoding]
@lat decimal(9,6),
@lng decimal(9,6),
@address nvarchar(1000) output
AS
BEGIN 
	DECLARE @URL VARCHAR(8000) 
	SET @URL = 'https://maps.googleapis.com/maps/api/geocode/xml?latlng='+cast(@lat as varchar(50))+','+cast(@lng as varchar(50))+'&key=AIzaSyDMUZXCjq_3Y6ZHHLhapOrU4LcpwjZVPpY'

	DECLARE @Response varchar(8000)
	DECLARE @Obj int 
	DECLARE @HTTPStatus int 
	DECLARE @xmlT as table ( answerXML XML )
 

	EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
	EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
	EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
	EXEC sp_OAMethod @Obj, send, NULL, ''
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
	
	INSERT @xmlT 
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseXML.xml',default)
	EXEC sp_OADestroy @Obj

	declare @responseXML XML
	select top 1 @responseXML=answerXML
	from @xmlT

	set @address=@responseXML.query('/GeocodeResponse/result[1]/formatted_address').value('.','nvarchar(1000)')
 
END

