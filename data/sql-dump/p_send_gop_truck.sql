
ALTER PROCEDURE [dbo].[p_send_gop_truck]
  (
    @groupProcessInstanceId INT
  )
AS
  BEGIN
    DECLARE @mailBody NVARCHAR(MAX)
    DECLARE @rootId int
    SELECT @rootId = root_id from process_instance where group_process_id = @groupProcessInstanceId
    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))

    DECLARE @crossBorder int
    DECLARE @location nvarchar(500)
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @location = value_string FROM @values
    SELECT @crossBorder = IIF(ISNULL(@location, dbo.f_translate('Polska',default)) = dbo.f_translate('Polska',default), 0, 1)

    DECLARE @englang tinyint = 0

    IF @crossBorder =1
      BEGIN
        SET @englang = 1
      end
    ELSE
      BEGIN
        SET @englang = 0
      END

    IF @engLang = 0
      BEGIN
        EXEC Dbo.P_get_email_template @name = 'truck_gop_request', @responseText = @mailBody output

      END
    ELSE
      BEGIN
        EXEC Dbo.P_get_email_template @name = 'truck_gop_request_eng', @responseText = @mailBody output

      end

    DECLARE @sendToMail nvarchar(100)
    DECLARE @partnerId INT



    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerId = value_int FROM @values

    IF isnull(@partnerId, 0 )  = 0
      BEGIN
        DELETE FROM @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @partnerId = value_int FROM @values
      end


    declare @serviceId int

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '526', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @serviceId = value_int FROM @values


    SET @sendToMail = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, @serviceId, 4)
    if isnull(@sendToMail,'')='' SET @sendToMail = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, @serviceId, 8)
    if isnull(@sendToMail,'')='' SET @sendToMail = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, @serviceId, 16)


    SET @sendToMail =  dbo.f_getRealEmailOrTest(@sendToMail)



    DECLARE @caseId nvarchar(40)
    DECLARE @caseDate nvarchar(40)
    DECLARE @caseCreatedAt DATETIME
    SET @caseId = dbo.f_caseId(@groupProcessInstanceId)

    SELECT top 1 @caseCreatedAt = created_at from process_instance where root_id = @rootId order by id asc
    SET @caseDate = convert(nvarchar(50), @caseCreatedAt, 103)

    DECLARE @subject nvarchar(300)
    IF @englang = 1
      BEGIN
        SET @subject = dbo.f_translate('Job order - ',default) + ISNULL(@caseId, '')
      end
    ELSE
      BEGIN
        SET @subject = dbo.f_translate('Zlecenie / Gwarancja płatności - ',default) + ISNULL(@caseId, '')
      end


    DECLARE @platform nvarchar(100)
    DECLARE @makeModel nvarchar(100)
    DECLARE @platformId int
    DECLARE @makeModelId int
    DECLARE @vin NVARCHAR(20)
    DECLARE @regNumber NVARCHAR(20)
    DECLARE @regDate DATETIME
    DECLARE @regDateText NVARCHAR(50)
    DECLARE @carType NVARCHAR(50)
    DECLARE @carTypeId int
    DECLARE @partnerLocationId int
    DECLARE @partnerLocation NVARCHAR(500)
    DECLARE @locationCountry NVARCHAR(50)
    DECLARE @locationDetails NVARCHAR(500)
    DECLARE @accidentDetails NVARCHAR(500)
    DECLARE @accidentMoreInfo NVARCHAR(500)
    DECLARE @cardDetails NVARCHAR(500)
    DECLARE @gopDetails NVARCHAR(2000)
    DECLARE @driverPhone NVARCHAR(20)
    DECLARE @driverName NVARCHAR(100)

    DECLARE @userCompanyCountry NVARCHAR(100)
    DECLARE @userCompanyName NVARCHAR(100)
    DECLARE @userCompanyStreet NVARCHAR(100)
    DECLARE @userCompanyCity NVARCHAR(100)
    DECLARE @userCompanyNIP NVARCHAR(100)
    DECLARE @userCompanyFax NVARCHAR(100)
    DECLARE @userCompanyPerson NVARCHAR(100)
    DECLARE @userCompanyPhone NVARCHAR(100)
    DECLARE @userCompanyBuildingNumber NVARCHAR(100)
    DECLARE @userCompanyApartamentNumber NVARCHAR(100)
    DECLARE @userCompanyPostalCode NVARCHAR(100)
    DECLARE @gureTruck int


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int FROM @values
    SELECT @platform = name from AtlasDB_def.dbo.platform where id = @platformId


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @vin = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @regNumber = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,233',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @regDate = value_date FROM @values

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @makeModelId = value_int from @values
    SELECT @makeModel = dbo.f_dictionaryDesc(@makeModelId, 'makeModel')

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '836', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @carTypeId = value_int from @values


    DELETE from @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '278', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @gureTruck = value_int from @values

    IF @englang = 1
      BEGIN
        select @carType = argument1 from AtlasDB_def.dbo.dictionary where value = @carTypeId and typeD = 'truckType'
      end
    ELSE
      BEGIN

        SELECT @carType = dbo.f_dictionaryDesc(@carTypeId, 'truckType')
      end

    -- kierowca
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @driverPhone = value_string from @values

    DECLARE @driverFirstName nvarchar(50)
    DECLARE @driverSurname nvarchar(50)


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,64',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @driverFirstName = value_string from @values
    print @driverFirstName
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,66',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @driverSurname = value_string from @values
    print @driverSurname
    SET @driverName = isnull(@driverFirstName, '') + ' ' + isnull(@driverSurname, '')
    -- dzwoniący
    IF isnull(@driverFirstName, '') = ''
      BEGIN
        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '81,342,408,197',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @driverPhone = value_string from @values


        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '81,342,64',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @driverFirstName = value_string from @values

        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '81,342,66',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
     SELECT @driverSurname = value_string from @values
        print @driverSurname
        SET @driverName = isnull(@driverFirstName, '') + ' ' + isnull(@driverSurname, '')
      end
    -- Zgłaszający
    IF isnull(@driverFirstName, '') = ''
      BEGIN
        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,408,197',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @driverPhone = value_string from @values


        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,64',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @driverFirstName = value_string from @values

        DELETE FROM @values
        INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,66',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @driverSurname = value_string from @values
        print @driverSurname
        SET @driverName = isnull(@driverFirstName, '') + ' ' + isnull(@driverSurname, '')
      end

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerLocationId = value_int FROM @values
    IF isnull(@partnerLocationId, 0) = 0
      BEGIN
        DELETE FROM @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @partnerLocationId = value_int from @values
        DELETE FROM @values
      end

    SELECT @partnerLocation = dbo.f_partnerNameFull(@partnerLocationId)

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @locationCountry = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,63', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @locationDetails = value_text FROM @values


    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '417', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @accidentDetails = value_text FROM @values


    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,86',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyCountry = value_string FROM @values


    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '84', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyName = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,627',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyCity = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,94',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyStreet = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '83', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyNIP = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,95',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyBuildingNumber = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,96',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyApartamentNumber = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,89',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyPostalCode = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '197', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyPhone = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '197', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyFax = value_string FROM @values


    --     DELETE FROM @values
    --     INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,', @groupProcessInstanceId = @groupProcessInstanceId
    --     SELECT @userCompanyCountry = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1028,63', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @accidentMoreInfo = value_text FROM @values


    SET @cardDetails = ''


    DECLARE @stsTypeID int
    DECLARE @stsTypeName nvarchar(30)
    DECLARE @cardNumber nvarchar(30)
    DECLARE @cardActiveTill nvarchar(30)
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '173', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @stsTypeID = value_int FROM @values
    SET @stsTypeName = dbo.f_dictionaryDesc(@stsTypeID, 'STSPayment')

    If isnull(@stsTypeID, -1) <> -1
      BEGIN
        IF @stsTypeID = 1
          BEGIN
            SET @stsTypeName = dbo.f_translate('euroShell',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @stsTypeID = 2
          BEGIN
            SET @stsTypeName = dbo.f_translate('UTA',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @stsTypeID = 3
          BEGIN
            SET @stsTypeName = dbo.f_translate('ESSO',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @stsTypeID = 4
          BEGIN
            SET @stsTypeName = dbo.f_translate('KRONE',default)
            SET @platform = dbo.f_translate('KRONE',default)
          end
        IF @stsTypeID = 5
          BEGIN
            SET @stsTypeName = dbo.f_translate('KOGEL',default)
            SET @platform = dbo.f_translate('KOGEL',default)
          end
        IF @stsTypeID = 6
          BEGIN
            SET @stsTypeName = dbo.f_translate('OTOKAR',default)
            SET @platform = dbo.f_translate('OTOKAR',default)
          end
        IF @stsTypeID = 7
          BEGIN
            SET @stsTypeName = dbo.f_translate('BPW ',default)
            SET @platform = dbo.f_translate('BPW',default)
          end
        IF @stsTypeID = 8
          BEGIN
            SET @stsTypeName = dbo.f_translate('WEBASTO ',default)
            SET @platform = dbo.f_translate('WEBASTO',default)
          end
        IF @stsTypeID = 9
          BEGIN
            SET @stsTypeName = dbo.f_translate('FLEET',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end

        --         Setujemy po odpowiedniej
      end
    ELSE
      BEGIN
        SET @stsTypeName = @platform
        --         bierzemy nazwę platformy
      end


    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '176', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardNumber = value_string FROM @values
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '186', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardActiveTill = value_string FROM @values

    DECLARE @cardTypeSign nvarchar(100)

    DECLARE @cardEurope int
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '705', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardEurope = value_int FROM @values


    DECLARE @cardUta int
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '165', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardUta = value_int FROM @values
    DECLARE @cardKey int
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '191', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardKey = value_int FROM @values

    DECLARE @cardCategoryID int
    DECLARE @cardCategory nvarchar(100)
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '163', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardCategoryID = value_int FROM @values

    SET @cardCategory = dbo.f_dictionaryDesc(@cardCategoryID, 'STSCardCategory')


    IF @englang = 0
      BEGIN
        if isnull(@stsTypeName, '') <> ''
          BEGIN
            SET @cardDetails = @cardDetails + '<br/>Typ karty: ' + @stsTypeName
          end
        if isnull(@cardNumber, '') <> ''
          BEGIN
            SET @cardDetails = @cardDetails + '<br/>Nr Karty: ' + @cardNumber
          end
        if isnull(@cardActiveTill, '') <> ''
          BEGIN
            SET @cardDetails = @cardDetails + '<br/>Karta aktywna do: ' + @cardActiveTill
          end

        if isnull(@cardEurope, 0) <> 0
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Oznaczona: EU / EUROPE'
          end
        if isnull(@cardUta, 0) <> 0
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Oznaczona: UTA'
          end
        if isnull(@cardKey, 0) <> 0
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Oznaczona: symbol „klucza” lub oznaczenie EUROPE'
          end
        if isnull(@cardCategory, '') <> ''
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Kategoria: ' + @cardCategory
          end
      END
    ELSE

      BEGIN
        if isnull(@stsTypeName, '') <> ''
          BEGIN
            SET @cardDetails = @cardDetails + '<br/>Card Type: ' + @stsTypeName
          end
        if isnull(@cardNumber, '') <> ''
          BEGIN
            SET @cardDetails = @cardDetails + '<br/>Card NO: ' + @cardNumber
          end
        if isnull(@cardActiveTill, '') <> ''
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Card Valid thu: ' + @cardActiveTill
          end

        if isnull(@cardEurope, 0) <> 0
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Card detail: EU / EUROPE'
          end
        if isnull(@cardUta, 0) <> 0
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Card detail: UTA'
          end
        if isnull(@cardKey, 0) <> 0
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Card detail: "key" icon / EUROPE'
          end
        if isnull(@cardCategory, '') <> ''
          BEGIN
            SET @cardDetails = @cardDetails + '<br/> Card category: ' + @cardCategory
          end
      END


    DECLARE @isItGOP int
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '992', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @isItGOP = value_int FROM @values

    DECLARE @gopSetted nvarchar(2000) = ''
    print @isItGOP
    IF isnull(@isItGOP, 0) = 1 OR isnull(@gureTruck,0) =1
      BEGIN
        DECLARE @gopPrice decimal(8, 2)
        DELETE FROM @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '1028,1029',
                                             @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @gopPrice = value_decimal from @values
        delete from @values


        DECLARE @starterData nvarchar(400)
        SET @starterData = ' Starter24 Sp. z o.o.<br />
                ul. J. Kraszewskiego 30<br />
                60-519 Poznań<br />
                NIP: 525-21-83-310<br />
                REGON: 016387736<br />'
        DECLARE @starterDataSmall nvarchar(400)
        SET @starterDataSmall = ' Starter24 Sp. z o.o.<br />
                ul. J. Kraszewskiego 30<br />
                60-519 Poznań<br />'


        IF @englang =1
          BEGIN
            SET @gopSetted = dbo.f_translate('Yes - ',default) + convert(varchar, convert(decimal(8, 2), @gopPrice)) + IIF(isnull(@gureTruck,0) = 1,dbo.f_translate('EUR',default),dbo.f_translate('PLN',default))+'<br>'
                            + '<table width="100%" style="width:100%"><tr><td style="width:50%">' +
            ''+ dbo.f_translate(' Invoice for:',default) + @starterData + '</td><td style="width:50%"><br />'
                             + dbo.f_translate(' Send Invoice to:',default) + @starterDataSmall + '</td></tr></table><br />'


          end
        ELSE
          BEGIN
        SET @gopSetted = dbo.f_translate('TAK - ',default) + convert(varchar, convert(decimal(8, 2), @gopPrice)) + IIF(isnull(@gureTruck,0) = 1,dbo.f_translate('EUR',default),dbo.f_translate('PLN',default))+'<br>'
                        +'<table width="100%" style="width:100%"><tr><td style="width:50%">' +
         dbo.f_translate(' Fakturę wystawić na:',default) + @starterData + '</td><td style="width:50%"><br />'
                         + dbo.f_translate(' Fakturę wysłać na:',default) + @starterDataSmall + '</td></tr></table><br />'

          end
        END
    ELSE
      BEGIN
        IF @englang = 1
          BEGIN
            SET @gopSetted = dbo.f_translate('No - Settlement in accordance with the fuel card procedures or warranty conditions',default)
          end
        ELSE
          BEGIN
            SET @gopSetted = dbo.f_translate('NIE - Rozliczenie zgodnie z procedurami karty paliwowej lub warunki gwarancji',default)
          end
      end

    IF @englang =1
      BEGIN
        SET @gopDetails = dbo.f_translate('Guarantee of payment: ',default)+@gopSetted
      end
    ELSE
      BEGIN
    SET @gopDetails = dbo.f_translate('Gwarancja płatności: ',default) + @gopSetted
END

    PRINT '@isItGOP'
    PRINT @isItGOP
    PRINT '@gureTruck'
    PRINT @gureTruck

    PRINT '@gopDetails'
    PRINT @gopDetails
    IF @englang = 1 and isnull(@gureTruck,0) =0
      BEGIN
        SET @gopDetails = ''
      end
    IF isnull(@regDate, 0) <> 0
      BEGIN
        SET @regDateText = convert(nvarchar, @regDate, 103)
      end


    DECLARE @fuelCardInfo nvarchar(2000) = ''
    IF @platformId in (8, 16, 22) OR isnull(@stsTypeID, 0) in (1, 2, 3)
      BEGIN
        IF @englang = 0
          BEGIN
            SET @fuelCardInfo = '<div class="row" style="border-bottom: 1px solid black;padding:10px 10px;">
            <div class="col-md-12 " style="width: 81.2%; margin-left: 8%;display: inline-block;">
                <div class="col-md-12" style="display:block;width: 100%; text-align: center">
                    <h4 style="text-decoration: underline;margin-top: 0px;">Dane karty paliwowej</h4>
                </div>
                <p>__CARD_DETAILS__</p>
            </div>
        </div>'
          end
        ELSE
          BEGIN
            SET @fuelCardInfo = '<div class="row" style="border-bottom: 1px solid black;padding:10px 10px;">
            <div class="col-md-12 " style="width: 81.2%; margin-left: 8%;display: inline-block;">
                <div class="col-md-12" style="display:block;width: 100%; text-align: center">
                    <h4 style="text-decoration: underline;margin-top: 0px;">Fuel Card Details</h4>
                </div>
                <p>__CARD_DETAILS__</p>
            </div>
        </div>'
          end

      end
    DECLARE @gopComment nvarchar(2000) = ''
    IF isnull(@isItGOP, 0) = 1
      BEGIN

      IF @englang =1
        BEGIN

          SET @gopComment = '    <div class="row" style="border-bottom: 1px solid black;padding:10px 10px;">
        <p>
          Please send a copy of the completed service report immediately after completing the repair. <br/>
             Invoices received later than 7 days after the completion of the repair will not be accepted. The payment will be made within 30 days of receipt of the invoice. <br>
             <b> PLEASE ADD CARD NUMBER TO INVOICE </ b>
        </p>
    </div>'
        end
        ELSE
          BEGIN
        SET @gopComment = '    <div class="row" style="border-bottom: 1px solid black;padding:10px 10px;">
        <p>
            Prosimy o przesłanie kopii wypełnionego raportu serwisowego bezpośrednio po zakończeniu naprawy.<br/>
            Faktury otrzymane później niż 7 dni od zakończenia naprawy nie będą akceptowane. Płatność nastąpi w ciągu 30 dni od otrzymania faktury.<br>
            <b>PROSIMY NA FAKTURZE ZAMIEŚCIĆ NR KARTY</b>
        </p>
    </div>'
            END
      end

    DECLARE @gopGuaranty nvarchar(2000) = ''
    IF isnull(@isItGOP, 0) = 1
      BEGIN
        IF @englang = 1
          BEGIN
            SET @gopGuaranty = '        <div class="row" style="border-bottom: 1px solid black;padding:10px 10px;">
            <div class="col-md-12 " style="width: 81.2%; margin-left: 8%;display: inline-block;">
                <div class="col-md-12" style="display:block;width: 100%; text-align: center">
                    <h4 style="text-decoration: underline;margin-top: 0px;">Guarantee of payment</h4>
                </div>
                <p>__GOP_DETAILS__</p>
            </div>
        </div>'
          end
        ELSE
          BEGIN
        SET @gopGuaranty = '        <div class="row" style="border-bottom: 1px solid black;padding:10px 10px;">
            <div class="col-md-12 " style="width: 81.2%; margin-left: 8%;display: inline-block;">
                <div class="col-md-12" style="display:block;width: 100%; text-align: center">
                    <h4 style="text-decoration: underline;margin-top: 0px;">Gwarancja płatności</h4>
                </div>
                <p>__GOP_DETAILS__</p>
            </div>
        </div>'
            END
      end


    SET @mailBody = REPLACE(@mailBody, '__COMPANY_NAME__', isnull(@platform, ''))
    SET @mailBody = REPLACE(@mailBody, '__CASE_ID__', isnull(@caseId, ''))
    SET @mailBody = REPLACE(@mailBody, '__CASE_DATE__', isnull(@caseDate, ''))
    SET @mailBody = REPLACE(@mailBody, '__VIN__', isnull(@vin, ''))
    SET @mailBody = REPLACE(@mailBody, '__REG_NUMBER__', isnull(@regNumber, ''))
    SET @mailBody = REPLACE(@mailBody, '__FUEL_CARD__', isnull(@fuelCardInfo, ''))
    SET @mailBody = REPLACE(@mailBody, '__FIRST_REG_DATE__', isnull(@regDateText, ''))
    SET @mailBody = REPLACE(@mailBody, '__CAR_TYPE__', isnull(@carType, ''))
    SET @mailBody = REPLACE(@mailBody, '__CAR_MAKEMODEL__', isnull(@makeModel, ''))
    SET @mailBody = REPLACE(@mailBody, '__PARTNER_INFO__', isnull(@partnerLocation, ''))
    SET @mailBody = REPLACE(@mailBody, '__DRIVER_PHONE__', isnull(@driverPhone, ''))
    SET @mailBody = REPLACE(@mailBody, '__DRIVER_NAME__', isnull(@driverName, ''))
    SET @mailBody = REPLACE(@mailBody, '__LOCATION_COUNTRY__', isnull(@locationCountry, ''))
    SET @mailBody = REPLACE(@mailBody, '__LOCATION_DETAILS__', isnull(@locationDetails, ''))
    SET @mailBody = REPLACE(@mailBody, '__ACCIDENT_DETAILS__', isnull(@accidentDetails, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_COUNTRY__', isnull(@userCompanyCountry, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_NAME__', isnull(@userCompanyName, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_LOCATION_STREET__', isnull(@userCompanyStreet, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_CITY__', isnull(@userCompanyCity, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_BUILDING_NUMBER__', isnull(@userCompanyBuildingNumber, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_APART_NUMBER__', isnull(@userCompanyApartamentNumber, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_POSTAL__', isnull(@userCompanyPostalCode, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_NIP__', isnull(@userCompanyNIP, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_FAX__', isnull(@userCompanyFax, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_PERSON__', isnull(@userCompanyPerson, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_PHONE__', isnull(@userCompanyPhone, ''))
    SET @mailBody = REPLACE(@mailBody, '__GOP_GUARANTY_ROW__', isnull(@gopGuaranty, ''))
    SET @mailBody = REPLACE(@mailBody, '__DRIVER_INFO__', isnull(@accidentMoreInfo, ''))
    SET @mailBody = REPLACE(@mailBody, '__CARD_DETAILS__', isnull(@cardDetails, ''))
    SET @mailBody = REPLACE(@mailBody, '__GOP_DETAILS__', isnull(@gopDetails, ''))
    SET @mailBody = REPLACE(@mailBody, '__GOP_COMMENT__', isnull(@gopComment, ''))

    DECLARE @err int
    DECLARE @message nvarchar(2000)


    DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('truck')

    DECLARE @attachment nvarchar(MAX)


    EXEC dbo.p_attribute_edit
        @attributePath = '959',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @stepId = 'xxx',
        @valueText = @mailBody,
        @err = @err OUTPUT,
        @message = @message OUTPUT
    declare @pdfId int
    SELECT TOP 1 @pdfId = id
    FROM dbo.attribute_value
    WHERE attribute_path = '959'
      AND group_process_instance_id = @groupProcessInstanceId


    print '@pdfId'
    print @pdfId


    IF @crossBorder = 1
      BEGIN
        SET @attachment =
        '{ATTRIBUTE::' + CONVERT(NVARCHAR(100), @pdfId) + dbo.f_translate('::Truck GOP for case ',default) + isnull(@caseId, '') + '::false}'
      END
    ELSE
      BEGIN
        DECLARE @rsAttId int
        EXEC p_send_rs_truck_attachement_id @groupProcessInstanceId = @groupProcessInstanceId,
                                            @attachementId = @rsAttId OUTPUT


        SET @attachment =
        '{ATTRIBUTE::' + CONVERT(NVARCHAR(100), @pdfId) + dbo.f_translate('::Truck GOP for case ',default) + isnull(@caseId, '') + '::false}'
        if isnull(@rsAttId, 0) != 0
          BEGIN
            SET @attachment =
            @attachment + ',{ATTRIBUTE::' + CONVERT(NVARCHAR(100), @rsAttId) + dbo.f_translate('::RS for case ',default) + isnull(@caseId, '') +
            '::false}'
          end
      end
    print '@attachmentList'
    print @attachment

    EXECUTE dbo.p_note_new
        @groupProcessId = @groupProcessInstanceId
        , @type = dbo.f_translate('email',default)
        , @content = dbo.f_translate('Truck invoice',default)
        , @email = @sendToMail
        , @userId = 1  -- automat
        , @subject = @subject
        , @direction = 1
        , @dw = ''
        , @udw = ''
        , @sender = @senderEmail
        , @additionalAttachments = @attachment
        , @emailBody = @mailBody
        , @err = @err OUTPUT
        , @message = @message OUTPUT

  end
