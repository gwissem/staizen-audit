ALTER PROCEDURE [dbo].[s_1007_074]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @groupProcessInstanceId INT
	DECLARE @vin NVARCHAR(255)
	DECLARE @sameDiagnosisStepCount INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	declare @header nvarchar(4000)
	declare @footer nvarchar(4000)
	declare @body nvarchar(4000)
	declare @email nvarchar(200)
	declare @senderEmail nvarchar(200) = [dbo].[f_getEmail]( dbo.f_translate('cfm',default) )
	declare @subject nvarchar(255)
	declare @regNumber nvarchar(255)
	DECLARE @platformId int
	declare @programId int 
	DECLARE @rootId int
	DECLARE @platformGroup nvarchar(255) 
	declare @emailAlreadySent int
	declare @decision int
	declare @callerPhone nvarchar(100)
	declare @platformName nvarchar(100)
	DECLARE @status INT 
	DECLARE @sourceGroupId int 
	declare @sourceId int 
	declare @targetId int 
	declare @newGroupId int 
	declare @processInstanceIds nvarchar(255) 
	declare @rentalDuration int 
	declare @dateStart datetime 
	declare @carCost decimal(10,2)
	DECLARE @customerDecision int 
	DECLARE @goBackToAutomat int 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance with(nolock)
	WHERE id = @previousProcessId 
	
	exec dbo.p_cfm_partner_contact @groupProcessInstanceId = @groupProcessInstanceId, @contact = @email output
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1044', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @goBackToAutomat = value_int FROM @values

	declare @canContinue int 
	
	exec dbo.p_can_rental_continue @groupProcessInstanceId = @groupProcessInstanceId, @canContinue = @canContinue OUTPUT
 
	IF ISNULL(@goBackToAutomat,0) = 1 AND @canContinue = 1
	BEGIN
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '1044', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = NULL,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
		declare @automatId int 
		
		select top 1 @automatId = id from dbo.process_instance with(nolock) where step_id = '1007.053' and group_process_id = @groupProcessInstanceId order by id desc
		
		if @automatId is not null
		BEGIN
			update dbo.process_instance set active = 1 where id = @automatId 		
			
			EXEC [dbo].[p_process_next]
		       @previousProcessId = @automatId,
		       @err = @err OUTPUT,
		       @message = @message OUTPUT
		       
		END 
	    
		exec [dbo].[p_jump_to_service_panel]
	    @previousProcessId = @previousProcessId,
	    @variant = 2
		
	    return
	    
	END 
	ELSE IF ISNULL(@goBackToAutomat,0) = 1 AND @canContinue = 0
	BEGIN
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '1044', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = NULL,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
	END 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '168,171', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @decision = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '168,170', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @customerDecision = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '168,59,61', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @status = value_int FROM @values
	
	delete from @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	SELECT @platformName = name FROM platform with(nolock) where id = @platformId
	
	set @variant = IIF(@variant = 10,10,99)
	
	IF @decision = 3 OR @status = 0 OR ISNULL(@customerDecision,0) = 1 OR @variant = 10
	BEGIN
		
	   IF @variant = 10 
       BEGIN
	       EXEC [dbo].[p_attribute_edit]
	          @attributePath = '668', 
	          @groupProcessInstanceId = @groupProcessInstanceId,
	          @stepId = 'xxx',
	          @userId = 1,
	          @originalUserId = 1,
	          @valueInt = 0,
	          @err = @err OUTPUT,
	          @message = @message OUTPUT
       END
       
	   EXEC p_create_summary_rental_process @groupProcessInstanceId = @groupProcessInstanceId, @currentUser = @currentUser, @sendGop = 1

       SET @variant = 2
       
       if exists(select id from dbo.process_instance with(nolock) where step_id = '1007.053' and group_process_id = @groupProcessInstanceId and active = 1)
       BEGIN
	       update dbo.process_instance set active = 0 where step_id = '1007.053' and group_process_id = @groupProcessInstanceId and active = 1
       END 
        
	END 
	else if @decision = 2 OR @status = 1
	BEGIN
		set @variant = 1
		
		DECLARE @endDate datetime
		
		CREATE TABLE #replacecmentCarSummary (duration INT, real_duration INT, days_left INT, end_date DATETIME)
		EXEC p_replacement_car_summary
		@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @endDate = end_date FROM #replacecmentCarSummary
		
		DROP TABLE #replacecmentCarSummary
		
	   	if exists(select id from dbo.process_instance with(nolock) where step_id = '1007.053' and group_process_id = @groupProcessInstanceId and active = 1)
       	BEGIN
	       update dbo.process_instance set active = 0 where step_id = '1007.053' and group_process_id = @groupProcessInstanceId and active = 1
       	END 
       
--		IF @endDate > getdate()
--		BEGIN
--			SET @variant = 3
--		END
--		
		return 
	END 
	else if @decision = 1 OR ISNULL(@status,-1) = -1
	BEGIN
		
		
		INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @programId = value_string FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @vin = value_string FROM @values
				
		exec [dbo].[p_platform_group_name]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@name = @platformGroup OUTPUT
	
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '168,59,65', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @emailAlreadySent = value_int FROM @values
		
		delete from @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '168,59,737', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @body = value_text FROM @values
	
		if isnull(@emailAlreadySent,0) = 0 and ISNULL(LTRIM(RTRIM(@body)),'') <> ''  
		BEGIN
			
			set @header = 'Szanowni Państwo<br/>'
		
			set @subject = @regNumber + ' / ' + dbo.f_caseId(@groupProcessInstanceId)
			set @body = isnull(@header+'<br/><br/>','')+isnull(@body+'<br/><br/>','')
			
			EXECUTE dbo.p_note_new 
		     @groupProcessId = @groupProcessInstanceId
		    ,@type = dbo.f_translate('email',default)
		    ,@content = @body
		    ,@email = @email
		 ,@userId = 1  -- automat
		    ,@subject = @subject
		    ,@direction=1
		    ,@dw = ''
		    ,@udw = ''
		    ,@sender = @senderEmail
	--	    ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
		    ,@emailBody = @body
		    ,@emailRegards = 1
		    ,@err=@err OUTPUT
		    ,@message=@message OUTPUT	
		    
		    EXEC [dbo].[p_attribute_edit]
		       @attributePath = '168,59,65', 
		       @groupProcessInstanceId = @groupProcessInstanceId,
		       @stepId = 'xxx',
		       @userId = 1,
		       @originalUserId = 1,
		       @valueInt = 1,
		       @err = @err OUTPUT,
		       @message = @message OUTPUT
		END 
		
		update dbo.process_instance set postpone_count = isnull(postpone_count,0)+1, postpone_date = dateadd(hour, 1, getdate()) where id = @previousProcessId 
		 
	END	
	
END


