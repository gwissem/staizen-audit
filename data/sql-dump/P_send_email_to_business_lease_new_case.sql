ALTER PROCEDURE [dbo].[P_send_email_to_business_lease_new_case]
	@previousProcessId INT,	-- Może być GROUP / ROOT
	@emailTo NVARCHAR(400)
AS
BEGIN
	
	/*		
	 
	TYLKO Business Lease 
	
	Powiadomienie o holowaniu pojazdu do ASO   LUB   Powiadomienie o holowaniu pojazdu do motofloty 
	(RÓŻNI SIĘ TYLKO E-mailem)
	
	-- ASO:
	- Warunek wysłania: sprawa typu awaria, przypisany serwis z grupy BL ASO, aktywowana usługa holowania
	- Wysyłane do: assistance@businesslease.pl	
	
	-- Motoflota:
	-- Warunek wysłania: sprawa typu awaria, przypisany serwis z grupy BL Standard, aktywowana usługa holowania
	-- Wysłane do: bl@motoflota.pl
	
 	____________________________________*/

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @title NVARCHAR(MAX)
	
	-- Pobranie podstawowych danych --
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	SET @content = 'Szanowni Państwo,</br></br>

		przesyłamy powiadomienie dot. transportu pojazdu Business Lease.</br>
		Informujemy, że do Państwa serwisu zostanie dostarczony pojazd należący do floty Business Lease Poland Sp. z o.o.</br></br>
		
		Powiadomienie do sprawy {#caseid#}</br>
		Nr rejestracyjny auta: {@74,72@}</br>
		Marka i model pojazdu: {@74,73@}</br>
		Użytkownik: {@80,342,64@} {@80,342,66@}</br>
		Nr tel.: {@80,342,408,197@}</br>
		Wstępna diagnoza: {#diagnosisDescription(true)#} / {@417@}</br>
		Pojazd kierowany jest do:  {#towingPlace()#} {@767,781,84@} {@767,781,687@}</br></br>
		
		Telefon kontaktowy dla Państwa: +48 61 83 19 969
		Infolinia dla Klienta: +48 22 46 000 46

		W korespondencji z nami należy zawsze używać adresu mailowego cfm@starter24.pl (kontakt w sprawach Car Fleet Management), pamiętając by w temacie wiadomości zawrzeć nasz numer sprawy (widoczny w temacie niniejszego maila). W sprawach bardzo pilnych uprzejmie prosimy o dodatkowy kontakt telefoniczny pod numerem +48 61 83 19 969
	'

	EXEC [dbo].[P_parse_string]
	 @processInstanceId = @previousProcessId,
	 @simpleText = 'Powiadomienie do sprawy [{#caseid#} / {@74,72@}]',
	 @parsedText = @title OUTPUT

	EXEC [dbo].[P_get_body_email]
		@body = @body OUTPUT,
		@contentEmail  = @content,
		@title = @title,
		@previousProcessId = @previousProcessId	
		
	SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'CFM')
	SET @body = REPLACE(@body,'__EMAIL__', 'cfm@starter24.pl')
	 
	 DECLARE @email VARCHAR(400)
	 DECLARE @dw NVARCHAR(400)
	 DECLARE @sender NVARCHAR(400)
	 DECLARE @contentNote NVARCHAR(1000) = [dbo].[f_showEmailInfo](@emailTo) + '(dla businesslease) Powiadomienie o holowaniu pojazdu do ASO'
	 
	 SET @email = dbo.f_getRealEmailOrTest(@emailTo)
	 SET @sender = dbo.f_getEmail('cfm')
	 
	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @groupProcessInstanceId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = @contentNote
	 	,@email = @email
	 	,@userId = 1  -- automat
	 	,@subject = @title
	 	,@direction=1
	 	,@sender = @sender
	 	,@emailBody = @body
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT

  EXEC p_attribute_edit
      @attributePath = '1043',
      @groupProcessInstanceId = @groupProcessInstanceId,
      @stepId = 'xxx',
      @valueInt = 20,
      @err = @err OUTPUT,
      @message = @message OUTPUT

END
