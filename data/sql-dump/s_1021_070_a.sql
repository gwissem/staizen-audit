ALTER PROCEDURE [dbo].[s_1021_070_a]
( 
	@nextProcessId INT,
	@currentUser INT = NULL,
	@token VARCHAR(50) = NULL OUTPUT
) 
AS
BEGIN
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	declare @groupProcessId int
	declare @step_id nvarchar(50)
	declare @root_id int
	declare @groupProcessInstanceId int
	declare @addedMatrixElement int
	
	select	@groupProcessId=group_process_id,
			@step_id=step_id,
			@root_id=root_id,
			@groupProcessInstanceId=group_process_id
	from	dbo.process_instance
	where	id=@nextProcessId

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '855', @groupProcessInstanceId = @root_id
	SELECT @addedMatrixElement = value_int FROM @values


	DECLARE @programId nvarchar(5)
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @root_id
	SELECT @programId = value_string FROM @values


	if @step_id='1021.003' 
	begin
		
		select top 1 @token=token
		from	dbo.process_instance
		where	group_process_id=@groupProcessId and
				token is not null
		order by id

		UPDATE	dbo.process_instance 
		SET		token=@token
		WHERE	id = @nextProcessId

		PRINT '---ADDED MATRIX EL'
		PRINT CAST(@addedMatrixElement as nvarchar)
		if ISNULL(@addedMatrixElement,0) = 1 or @programId in (424, 425, 426) --wyłom pod arc
		begin
			declare @partnerEmail nvarchar(200)
			declare @partnerId int
			declare @vin varchar(50)
			declare @regNumber varchar(20)
			DECLARE @err INT
			DECLARE @message NVARCHAR(255)
			declare @towGroupId int
	
			select	top 1 @towGroupId=group_process_id
			from	dbo.process_instance
			where	step_id like '1009.%' and
					root_id=@root_id
			order by id desc
	
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @partnerId = value_int FROM @values
	
			if @partnerId is null
			begin
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towGroupId
				SELECT @partnerId = value_int FROM @values
				
				EXEC [dbo].[p_attribute_edit]
			      @attributePath = '522', 
			      @groupProcessInstanceId = @groupProcessInstanceId,
			      @stepId = 'xxx',
			      @userId = 1,
			      @originalUserId = 1,
			      @valueInt = @partnerId,
			      @err = @err OUTPUT,
			      @message = @message OUTPUT
			end
			
			PRINT '---partnerIdL'
			print @partnerId
			
			-- Pobranie Emaila do kontraktora (3 próby)
			-- 8 - Email RS
			-- 4 - Realizacja - email
			-- 16 - Email
			SELECT @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId,@partnerId,1,8)
		
			IF ISNULL(@partnerEmail, '') = ''
			BEGIN
				
				SET @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId,@partnerId,1,4)
				
				IF ISNULL(@partnerEmail, '') = ''
				BEGIN
					SET @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId,@partnerId,1,16)
				END
				
			END
			
			print '--@partnerEmail'
			print @partnerEmail
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @VIN = value_string FROM @values
	
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @regNumber = value_string FROM @values
		
			declare @platform varchar(100)
	
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @platform = name from dbo.platform where id =(select top 1 value_int FROM @values)

			declare @platformId int

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @platformId = value_int FROM @values

			declare @makeModel nvarchar(100)
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @makeModel = textD from dbo.dictionary where value =(select top 1 value_int FROM @values) and typeD='makeModel'
	
		
	
			declare @subject nvarchar(300)
			declare @body nvarchar(4000)
			DECLARE @caseId nvarchar(50)
	
	
			set @caseId=dbo.f_caseId(@nextProcessId)
	
-- 			set @subject=dbo.f_translate('Formularz raportu serwisowego ',default)+isnull(@platform,'')+dbo.f_translate(' do sprawy ',default)+isnull(@caseId,'')+' ('+isnull(@makeModel,'')+' / '+isnull(@vin,'')+' / '+isnull(@regNumber,'')+')'
      set @subject=isnull(@makeModel,'')+' / '+isnull(@regNumber,'') +' / '+isnull(@vin,'')+dbo.f_translate(' / Sprawa nr ',default)+isnull(@caseId,'') + dbo.f_translate(' / Formularz raportu serwisowego ',default)+isnull(@platform,'')

      
      IF @platformId NOT IN (2,79,80,81,82)
			BEGIN
				set @body='WAŻNY KOMUNIKAT<BR>
				Prosimy o wejście w poniższy link i uzupełnienie: <BR>
				• Szacowanej daty zakończenia naprawy – gdy naprawa jest w trakcie<BR>
				• Daty zakończenia oraz sposobu rozliczenia – gdy znany jest termin zakończenia naprawy<BR><BR>
				<BR>
				Aby otworzyć link kliknij <A href="'+dbo.f_getDomain()+dbo.f_translate('/operational-dashboard/public/',default)+@token+'">tutaj</A><BR>
				<BR>
				Po przesłaniu raportu otrzymają Państwo na adres email kopię raportu. <BR>
				<BR>
				Jeżeli jedyną zorganizowaną usługą jest naprawa na miejscu zdarzenia, nie muszą Państwo uzupełniać raportu serwisowego. Przesłany przez nas dokument stanowi formę powiadomienia o udzielonym świadczeniu. We wszystkich innych przypadkach prosimy o uzupełnienie raportu (maksimum 2 dni po zakończeniu naprawy serwisowej).
				W razie pytań, prosimy o kontakt telefoniczny pod numerem telefonu 61 83 19 894.<BR>
				<BR>
				Uwaga: W przypadku pojazdów zagranicznych (zaopatrzonych w zagraniczne numery rejestracyjne lub z uprawnieniami assistance nadanymi za granicą), jeśli nie będzie możliwości rozliczenia całości usług assistance z producentem, prosimy o jak najszybszy kontakt telefoniczny (pod numerem telefonu 61 83 19 894), w celu otrzymania informacji o kosztach dotychczas zorganizowanych usług. W takich przypadkach kosztami usług assistance należy obciążyć klienta.<BR><BR>
				Gdybyście Państwo chcieli złożyć wniosek Dealer’s Call prosimy o użycie poniższego linku: <BR>
				Kliknij <A href="'+dbo.f_getDomain()+'/operational-dashboard/public/raport-serwisowy">tutaj</A><BR><BR><BR>'
			END
			ELSE
			BEGIN
				set @body = 'Szanowni Państwo, <br>
				w związku ze zorganizowanymi dla Klienta dodatkowymi usługami assistance prosimy o przekazanie nam informacji o terminie zakończenia naprawy.<br>
				WAŻNY KOMUNIKAT <br>
				Prosimy o wejście w poniższy link i uzupełnienie:<br>
				• Szacowanej daty zakończenia naprawy – gdy naprawa jest w trakcie i nie jest znany termin zakończenia naprawy <br>
				• Daty zakończenia – gdy znany jest termin zakończenia naprawy<br>
				<br>
				Aby otworzyć link kliknij tutaj  <A href="'+dbo.f_getDomain()+dbo.f_translate('/operational-dashboard/public/',default)+@token+'">tutaj</A> <br>
				<br>
				Po przesłaniu raportu otrzymają Państwo na adres email kopię raportu.<br>
				
				W razie pytań, prosimy o kontakt telefoniczny pod numerem telefonu 61 83 19 894. <br> '
			end
		

			declare @RSV varchar(200)
			declare @sender nvarchar(200)
			
			set @sender = dbo.f_getEmail('rs')
			set @RSV='{FORM::'+cast(@nextProcessId as varchar(100))+'::raport serwisowy}'
	
				EXECUTE dbo.p_note_new 
				   @sender=@sender
				  ,@groupProcessId=@groupProcessInstanceId
				  ,@type=dbo.f_translate('email',default)
				  ,@content=dbo.f_translate('Formularz raportu serwisowego',default)
				  ,@phoneNumber=null
				  ,@email=@partnerEmail
				  ,@userId=@currentUser
				  ,@subType=dbo.f_translate('unstoppable',default)
				  ,@attachment=null
				  ,@subject=@subject
				  ,@direction=1
				  ,@addInfo=null
				  ,@emailBody=@body
				  ,@err=@err OUTPUT
				  ,@message=@message OUTPUT
				  ,@additionalAttachments=@RSV
				  ,@emailRegards=1
			
				EXECUTE dbo.p_note_new 
				   @sender=@sender
				  ,@groupProcessId=@groupProcessInstanceId
				  ,@type=dbo.f_translate('email',default)
				  ,@content=dbo.f_translate('Kopia raportu serwisowego',default)
				  ,@phoneNumber=null
				  ,@email='skrzynkapodawcza@starter24.pl'
				  ,@userId=@currentUser
				  ,@subType=dbo.f_translate('unstoppable',default)
				  ,@attachment=null
				  ,@subject=@subject
				  ,@direction=1
				  ,@addInfo=null
				  ,@emailBody=dbo.f_translate('Kopia raportu serwisowego',default)
				  ,@err=@err OUTPUT
				  ,@message=@message OUTPUT
				  ,@additionalAttachments=@RSV
				  ,@emailRegards=1


			  EXEC [dbo].[p_attribute_edit]
				@attributePath = '855', 
				@groupProcessInstanceId = @root_id,
				@stepId = 'xxx',
				@userId = 1,
				@originalUserId = 1,
				@valueInt = 0,
				@err = @err OUTPUT,
				@message = @message OUTPUT
		end
	end
	else if @step_id='1021.034'
	begin
		
		UPDATE	dbo.process_instance 
		SET		postpone_date=getdate()+1
		WHERE	id = @nextProcessId
	end
END