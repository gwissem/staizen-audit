ALTER PROCEDURE [dbo].[p_simple_note_new]
(
	@groupProcessId	INT,
	@content 	NVARCHAR(2000),
	@phoneNumber NVARCHAR(20)
)
AS
BEGIN

	DECLARE @err int
	DECLARE @message nvarchar(1000)
	EXEC p_note_new
			@groupProcessId = @groupProcessId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @phoneNumber,
			@direction = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
END