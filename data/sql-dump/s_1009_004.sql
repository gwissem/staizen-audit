ALTER PROCEDURE [dbo].[s_1009_004]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @processInstanceId INT
	DECLARE @partnerOnPlace INT
	DECLARE @eta DATETIME
	DECLARE @lateReason INT
	DECLARE @platformId INT
	DECLARE @platformName NVARCHAR(255)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @icsId INT
	DECLARE @status INT	
	DECLARE @etaMinutes INT
	
	IF @variant = 7
	BEGIN
		RETURN
	END 
	
	-- Pobranie GroupProcessInstanceId --
	SELECT	@groupProcessInstanceId = group_process_id, 
			@stepId = step_id,
				 @rootId =root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	-- Stworzenie pomocniczej tabelki
	DECLARE @Values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
				
	DELETE from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '208', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerOnPlace = value_int FROM @values
	
	
	-- USTAWIENIE @ETA z minut na date
	IF ISNULL(@partnerOnPlace,0) = 0
	BEGIN
		
		DELETE from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '698', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @etaMinutes = value_int FROM @values
		
		-- Gdyby jakimś cudem @etaMinutes była pusta to +20 minutes 
		SET @eta = DATEADD(minute , ISNULL(@etaMinutes, 20) , GETDATE() )  
		
		EXEC p_attribute_edit
			@attributePath = '107', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueDate = @eta,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
	END
		
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '107', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '617', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @lateReason = value_int FROM @values

	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
	@attributePath = '80,342,408,197',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	IF @variant = 6
	BEGIN
		RETURN
	END 
	
	SET @variant = 1
	
	PRINT dbo.f_translate('partner',default)
	PRINT @partnerOnPlace
	
	INSERT  @values EXEC dbo.p_attribute_get2
	@attributePath = '609',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values
	
	IF ISNULL(@partnerOnPlace,0) = 0 AND @eta is not null
	BEGIN
		
		IF @icsId > 0
		BEGIN
			SET @variant = 99
			UPDATE dbo.process_instance SET postpone_date = DATEADD(MINUTE,10,@eta), postpone_count = postpone_count + 1 where id = @previousProcessId
		END
		ELSE
		BEGIN
			SET @variant = 5
		END 
		
		IF ISNULL(@lateReason,0) > 0
		BEGIN
			DECLARE @reasonText NVARCHAR(255)
			IF @lateReason <> 999
			BEGIN
				SELECT @reasonText = textD FROM dbo.dictionary WHERE typeD = 'partnerLateReason' and value = @lateReason
				SELECT @reasonText = dbo.f_translate('Przyczyna zwloki: ',default)+@reasonText+'.' 
			END 
			
			INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '253',
			@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @platformId = value_int FROM @values
		
			SELECT @platformName = name FROM dbo.platform where id = @platformId
			
			SET @content = dbo.f_translate('Chcielibysmy poinformowac, ze kierowca przybedzie z opoznieniem. Nowy szacowany czas dojazdu: ',default)+dbo.FN_VDateHour(@eta)+'.'+ ISNULL(@reasonText,'') +dbo.f_translate(' Przepraszamy za niedogodnosci z tym zwiazane.',default)
			PRINT @content
			
			EXEC p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @callerPhone,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		END 
		
	END
	ELSE IF ISNULL(@partnerOnPlace,0) = 1 AND ISNULL(@icsId,0) > 0
	BEGIN
		EXEC dbo.p_get_ics_status @icsId = @icsId, @status = @status OUTPUT
		
		IF ISNULL(@status,0) < 5
		BEGIN
--			SET @variant = 99	
--			UPDATE dbo.process_instance SET postpone_date = DATEADD(MINUTE,5,GETDATE()), postpone_count = postpone_count + 1 where id = @previousProcessId
			SET @variant = 3
		END
		ELSE
		BEGIN
			SET @variant = 2
		END
		
		EXEC [dbo].[p_rsa_close_report_sync] @groupProcessInstanceId = @groupProcessInstanceId
		
	END 
	ELSE IF ISNULL(@partnerOnPlace,0) = 1 AND ISNULL(@icsId,0) = -1
	BEGIN
		SET @variant = 3
		
		-- Skok do panelu usług
		EXEC dbo.p_jump_to_service_panel @previousProcessId = @previousProcessId, @variant = @variant
		
	END
		DELETE from @values
	DECLARE @starterContractorAbroad int


	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '1003',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @starterContractorAbroad = value_int FROM @values

	DECLARE  @etaMinutesTowingBack int
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '1005,698',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @etaMinutesTowingBack = value_int FROM @values

	DECLARE  @processInstanceIdNew int


  IF ISNULL(@starterContractorAbroad,0) =1
		 AND
		 ISNULL(@partnerOnPlace,0) = 1
	BEGIN
			SET @variant =3
    end
END
