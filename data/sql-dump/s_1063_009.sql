


ALTER PROCEDURE [dbo].[s_1063_009]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
DECLARE @err INT
DECLARE @message NVARCHAR(255)
DECLARE @maxMileage INT
DECLARE @currentMileage DATETIME
DECLARE @stepId nvarchar(30)
DECLARE @info NVARCHAR(4000)
Declare @platformId int

DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
DECLARE @groupProcessInstanceId INT
SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id FROM process_instance where id = @previousProcessId

INSERT @values EXEC p_attribute_get2 @attributePath = '479', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @maxMileage = value_int FROM @values

DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @currentMileage = value_int FROM @values

DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @platformId = value_int FROM @values

SET @variant = 2

IF @maxMileage IS NOT NULL AND @currentMileage IS NOT NULL AND @currentMileage < @maxMileage
BEGIN
	SET @variant = 1
	
	if @platformId = 10
	begin
		EXEC [dbo].[p_attribute_set2]
		@attributePath = '202', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueString = '462',
		@err = @err OUTPUT,
		@message = @message OUTPUT
	end
	else
	begin

		EXEC [dbo].[p_attribute_set2]
		@attributePath = '202', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueString = '402',
		@err = @err OUTPUT,
		@message = @message OUTPUT
	end
END
ELSE IF @maxMileage = -1
BEGIN
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '521', 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = @stepId,
	@valueString = dbo.f_translate('Limit przebiegu jest informacją niezbędną do weryfikacji Pańskich / Pani uprawnień do assistance. Jeśli jest taka możliwość, proszę odnaleźć dokumenty z zapisanym limitem, a następnie oddzwonić. W obecnej sytuacji mogę zaoferować pom',default),
	@err = @err OUTPUT,
	@message = @message OUTPUT
END 
ELSE
BEGIN
	SET @info = dbo.f_translate('Uprawnienie było ważne aż do osiągnięcia ',default)+ CAST(@maxMileage AS NVARCHAR(20))+dbo.f_translate(' tysięcy kilometrów. W tej sytuacji mogę zaoferować pomoc odpłatną.',default)
	
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '521', 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = @stepId,
	@valueString = @info,
	@err = @err OUTPUT,
	@message = @message OUTPUT
END
