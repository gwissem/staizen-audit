ALTER PROCEDURE [dbo].[s_1021_020]
(
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
)
AS
BEGIN
	DECLARE @err TINYINT
	DECLARE @message NVARCHAR(255)
	DECLARE @caseId NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	declare @val nvarchar(4000)
	declare @programId int

	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id FROM process_instance where id = @previousProcessId

	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	declare @partnerEmail nvarchar(200)
	declare @vin varchar(50)
	declare @regNumber varchar(20)
	declare @platformId int

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '129,800,368', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerEmail = value_string FROM @values


	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VIN = value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values

	declare @DC int
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '802', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @DC = value_int FROM @values

	declare @makeModel nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = textD from dbo.dictionary where value =(select top 1 value_int FROM @values) and typeD='makeModel'

	declare @platform varchar(100)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platform = name from dbo.platform where id =(select top 1 value_int FROM @values)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	DELETE FROM @values


	DECLARE @platformGroupName nvarchar(20)

	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroupName output


-- 	Oświadczenie VGP



	DELETE from @values
	DECLARE @repairDateStart DATETIME
	INSERT @values exec p_attribute_get2  @attributePath = '883,896', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @repairDateStart = value_date from @values

	DELETE from @values
	DECLARE @repairDateEnd DATETIME
	INSERT @values exec p_attribute_get2  @attributePath = '129,130', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @repairDateEnd = value_date from @values

	DELETE from @values

	DECLARE @diagnosticStarted int
	INSERT @values exec p_attribute_get2  @attributePath = '129,281', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @diagnosticStarted = value_int from @values

	DELETE from @values


	set @caseId=dbo.f_caseId(@groupProcessInstanceId)

	declare @subject nvarchar(300)
	declare @body nvarchar(4000)
	declare @token nvarchar(50)

	select top 1 @token=token
	from	dbo.process_instance
	where	group_process_id=@groupProcessInstanceId and
				 token is not null
	order by id

	IF (@platformId =2)
		BEGIN
			set @subject=isnull(@makeModel,'')+' / '+isnull(@regNumber,'') +'/'+isnull(@vin,'')+dbo.f_translate(' / Sprawa nr ',default)+isnull(@caseId,'') + dbo.f_translate(' / Kopia złożonego formularza raportu serwisowego ',default)+isnull(@platform,'')
		end
	ELSE
		BEGIN
			set @subject=isnull(@makeModel,'')+' / '+isnull(@regNumber,'') +' / '+isnull(@vin,'')+dbo.f_translate(' / Sprawa nr ',default)+isnull(@caseId,'') + dbo.f_translate(' / Formularz raportu serwisowego ',default)+isnull(@platform,'')
	end

  IF @platformId = 2
    BEGIN
      if @DC=1
        begin
          set @body='Szanowni Państwo,<BR>' +
										dbo.f_translate('W załączeniu przesyłamy kopię złożonego przez Państwa wniosku o zorganizowanie świadczeń dodatkowych do zgłoszenia ',default)+isnull(@caseId,'')+'. <BR>' +
										'Prosimy oczekiwać na decyzję dotyczącą akceptacji. Zostanie ona przesłana na wskazany we wniosku adres email.<BR>' +
										'<b>Uwaga:</b> wnioskowi został nadany numer tymczasowy. Po rozpatrzeniu wniosku otrzymają Państwo w osobnej wiadomości informacje o właściwym numerze sprawy.'
        end
      else
        begin
          set @body='Szanowni Państwo,<BR>
					W załączeniu przesyłamy kopię wypełnionego Raportu Serwisowego do zgłoszenia  '+isnull(@caseId,'')+'. ' +
										'<br>Aby uaktualnić dane raportu prosimy kliknąć <A href="'+dbo.f_getDomain()+dbo.f_translate('/operational-dashboard/public/',default)+@token+'">tutaj</A>  '

        end
    end
  ELSE
    BEGIN
      if @DC=1
        begin
          set @body='Szanowni Państwo,<BR>
				W załączeniu przesyłamy kopię złożonego przez Państwa wniosku Dealer’s Call do zgłoszenia '+isnull(@caseId,'')+'. <BR>Prosimy oczekiwać na decyzję dotyczącą akceptacji. Zostanie ona przesłana na wskazany we wniosku adres email.'
        end
      else
        begin
          set @body='Szanowni Państwo,<BR>
					W załączeniu przesyłamy kopię wypełnionego Raportu Serwisowego do zgłoszenia '+isnull(@caseId,'')+'.'
        end
    end

	DECLARE @idRS INT

	SELECT top 1 @idRS = id
	FROM	process_instance
	where	group_process_id = @groupProcessInstanceId and
			step_id='1021.003'
	order by id desc

	declare @RSV varchar(200)
	set @RSV='{FORM::'+cast(@idRS as varchar(100))+'::raport serwisowy}'

	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rs')

	EXECUTE dbo.p_note_new
		@sender=@senderEmail
		,@groupProcessId=@groupProcessInstanceId
		,@type=dbo.f_translate('email',default)
		,@content=@body
		,@phoneNumber=null
		,@email=@partnerEmail
		,@userId=@currentUser
		,@subType=null
		,@attachment=null
		,@subject=@subject
		,@direction=1
		,@addInfo=null
		,@emailBody=@body
		,@emailRegards=1
		,@err=@err OUTPUT
		,@message=@message OUTPUT
		,@additionalAttachments=@RSV

	if @DC=1
	begin
		set @variant=1

		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2
				@attributePath = '74,75',
				@groupProcessInstanceId = @groupProcessInstanceId

		DECLARE @mileage int;
		SELECT @mileage = value_int FROM @values;

		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2
				@attributePath = '74,71',
				@groupProcessInstanceId = @groupProcessInstanceId

		set @regNumber=replace(@regNumber,' ','')

		IF @platformId <> 2  -- nie robimy powtórnego sprawdzenia dla forda
		BEGIN

			IF @platformId=76
			begin
				declare @firstRegDate datetime

				DELETE FROM @values
				INSERT @values EXEC dbo.p_attribute_get2
						@attributePath = '74,233',
						@groupProcessInstanceId = @groupProcessInstanceId
				SELECT @firstRegDate = value_date FROM @values;

				if @firstRegDate>dateadd(year,-2,getdate())
				begin
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '204',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueString = '585',
						@err = @err,
						@message = @message

					EXEC [dbo].[p_attribute_edit]
						@attributePath = '202',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueString = '585',
						@err = @err,
						@message = @message

					SET @val=dbo.f_translate('Dane wskazują na ważne uprawnienie Cupra Assistance',default)

				end
				else
				begin
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '204',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueString = null,
						@err = @err,
						@message = @message

					EXEC [dbo].[p_attribute_edit]
						@attributePath = '202',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueString = null,
						@err = @err,
						@message = @message

					SET @val='<span class="font-red bold">Dane nie są w pełni wiarygodne. Sprawdź model i datę 1 rejestracji</span>'

				end
			end
			else
			begin

				declare @name nvarchar(200)
				declare @start nvarchar(20)
				declare @end nvarchar(20)
				declare @startDate datetime
				declare @mileageLimit nvarchar(20)

				declare @casesForVehicle INT

				DECLARE @tempTable TABLE (id INT IDENTITY(1,1), programId INT, startDate DATETIME, endDate DATETIME, mileageLimit NVARCHAR(20), headerId INT)

				INSERT INTO @tempTable (programId, startDate, endDate, mileageLimit, headerId)
				select h.program_id , --p.name
						 h.start_date
						 ,h.end_date
						 ,e2.value
						 ,h.id
				FROM	dbo.vin_element e1 with(nolock) inner join
						dbo.vin_header h with(nolock) on h.id=e1.header_id and h.status=1 left join
						dbo.vin_element e2 with(nolock) on h.id=e2.header_id and e2.dictionary_id in (2385)
				where e1.value IN (@regNumber, @vin)
				and disable_date is null
				and dateadd(day,1,end_date)>=convert(datetime,convert(varchar(8),getdate(),112))
				and isnull(start_date,'1981-01-01')<=convert(datetime,convert(varchar(8),getdate(),112))
				order by h.end_date asc

		--				UPDATE t1
		--				SET t1.mileageLimit =
		--				ISNULL(
		--					ISNULL(
		--					TRY_PARSE(t1.mileageLimit as INT)
		--					,(SELECT MIN(t2.mileageLimit) FROM @tempTable t2 WHERE t2.id > t1.id))
		--				,9999999)
		--				FROM @tempTable t1


				UPDATE t1
				SET t1.mileageLimit = ISNULL(TRY_PARSE(t1.mileageLimit as INT),9999999)
				FROM @tempTable t1

				delete from @tempTable
				where programId in (

					select id
					from dbo.vin_program
					where platform_id in (

						select  platform_id
						from	dbo.platform_group_platforms
						where platform_group_id=6

						)

					)
				delete from @tempTable where programId not in ( SELECT id from AtlasDB_def.dbo.vin_program where platform_id = @platformId )

				select TOP 1 @programId = programId , --p.name
				@start=dbo.FN_fdate(startDate)
				,@startDate=startDate
				,@end=dbo.FN_fdate(endDate)
				,@mileageLimit=mileageLimit
				FROM @tempTable
				WHERE
						mileageLimit >= ISNULL(@mileage, 1)
				ORDER BY mileageLimit DESC,
								 endDate DESC
				-- 		AND programId IN (402,414,415,416,417,418,419,420,421,424,425,426)



				select @name=name
				from dbo.vin_program with(nolock)
				where id=@programId


				declare @result_register_number nvarchar(20)
				declare @result_vin nvarchar(32)
				if @name is not null
				begin
					IF @programId IS NOT NULL
					BEGIN
						declare @programIds nvarchar(100)
						set @programIds=cast(@programId as nvarchar(100))

						EXEC [dbo].[p_attribute_edit]
						@attributePath = '202',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueString = @programIds,
						@err = @err,
						@message = @message

						--DECLARE @PlatformId INT
						--SELECT TOP 1 @PlatformId = platform_id FROM vin_program WHERE id = @programId

						--EXEC dbo.p_attribute_edit
						--@attributePath = '253',
						--@groupProcessInstanceId = @groupProcessInstanceId,
						--@stepId = 'xxx',
						--@valueint = @PlatformId,
						--@err = @err OUTPUT,
						--@message = @message OUTPUT
					END

					DECLARE @mileageText NVARCHAR(300)
					SET @mileageText = ''

					IF NULLIF(@mileageLimit,'') IS NOT NULL AND @mileageLimit <> 9999999
					BEGIN
						SET @mileageText = dbo.f_translate(' do przebiegu ',default)+@mileageLimit+' km.'
					END

					SET @val= dbo.f_translate('Pojazd jest objęty ochroną assistance w ramach programu ',default)+@name+dbo.f_translate(' w okresie od ',default)+@start+' do '+@end + @mileageText

					EXEC [dbo].[p_attribute_edit]
							@attributePath = '423',
							@stepId = 'xxx',
							@groupProcessInstanceId = @groupProcessInstanceId,
							@valueText = @val,
							@err = @err,
							@message = @message

					--> Dodawanie atrybutów polisa od i polisa do
					IF NOT EXISTS ( --> Tworzenie korzenia atrybutów polisy
						SELECT TOP 1 1 FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessInstanceId AND attribute_path='981'
					)
					BEGIN
							EXEC [dbo].[p_attribute_edit] @attributePath          = '981'
								,@groupProcessInstanceId = @groupProcessInstanceId
								,@isParent				 = 1
								,@stepId                 = 'xxx'
								,@userId                 = 1
								,@originalUserId         = 1
								,@err                    = @err OUTPUT
								,@message                = @message OUTPUT                
					END

					DECLARE @id_981 INT = (
						SELECT TOP 1 id FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessInstanceId AND attribute_path='981'
					)

					IF ISNULL(@start,'')<>''
					BEGIN
						DECLARE @startDateP DATE = CONVERT(DATE,@start,104)
						EXEC dbo.p_attribute_edit
							@attributePath = '981,67', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@parentAttributeValueId= @id_981,
							@stepId = 'xxx',
							@valueint = null,
							@valuestring = null,
							@valuedate = @startDateP,
							@valuedecimal = null,
							@valuetext = null,
							@err = @err OUTPUT,
							@message = @message OUTPUT 
					END

					IF ISNULL(@end,'')<>''
					BEGIN
						DECLARE @endDateP DATE = CONVERT(DATE,@end,104)
						EXEC dbo.p_attribute_edit
							@attributePath = '981,68', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@parentAttributeValueId= @id_981,
							@stepId = 'xxx',
							@valueint = null,
							@valuestring = null,
							@valuedate = @endDateP,
							@valuedecimal = null,
							@valuetext = null,
							@err = @err OUTPUT,
							@message = @message OUTPUT 
					END
				end
				else
				begin
					SELECT  top 1 @programId = h.program_id , --p.name
							@start=dbo.FN_fdate(h.start_date)
							,@startDate = h.start_date
							,@end=dbo.FN_fdate(h.end_date)
							,@mileageLimit=e2.value
						FROM	dbo.vin_element e1 with(nolock) inner join
							dbo.vin_header h with(nolock) on h.id=e1.header_id and h.status=1 left join
							dbo.vin_element e2 with(nolock) on h.id=e2.header_id and e2.dictionary_id in (2385)
						where e1.value IN (@VIN,@regNumber) and
							disable_date is null
					order by e2.value desc, h.end_date desc, h.id desc

					select @name=name
					from dbo.vin_program with(nolock)
					where id=@programId

					PRINT dbo.f_translate('vin:',default)
					PRINT @VIN
					PRINT dbo.f_translate('nr rej:',default)
					PRINT @regNumber
					PRINT '-----'
					PRINT dbo.f_translate('limit2:',default)
					PRINT @mileageLimit


					if isnull(@name,'')<>''
					begin
						SET @val='<span class="font-red bold">Pojazd nie jest objęty ochroną assistance. Ostatnie zarejestrowane uprawnienie w ramach programu '+@name
						SET @val= @val+isnull(' obowiązywało w okresie od '+@start+' do '+@end,'')
						if @mileageLimit<>'' SET @val= @val+isnull(' (do przebiegu '+@mileageLimit+' km).' ,'')
						SET @val = @val+'</span>'
					end
					else
					begin
						SET @val='<span class="font-red bold">Pojazd nie jest objęty ochroną assistance. Nie znaleźliśmy uprawnień w bazie danych.</span>'
					end
					print @val

					DELETE FROM @values


				end
			end

			EXEC [dbo].[p_attribute_edit]
					@attributePath = '423',
					@stepId = 'xxx',
					@groupProcessInstanceId = @groupProcessInstanceId,
					@valueText = @val,
					@err = @err,
					@message = @message
				--	select @val





			IF isnull(@platformGroupName,'') = dbo.f_translate('VGP',default)
				BEGIN

					DECLARE @confirmationFirst INT = 0
					DECLARE @confirmationSec INT = 0
					DECLARE @confirmationThrid INT = 0
					DECLARE @confirmationFourth INT = 0


					delete from @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '1069', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @confirmationFirst= value_int FROM @values

					delete from @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '1075', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @confirmationSec= value_int FROM @values

					delete from @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '1076', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @confirmationThrid= value_int FROM @values

					delete from @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '1077', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @confirmationFourth= value_int FROM @values




					DECLARE @date datetime
					delete from @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '101,104', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @date= value_date FROM @values

					DECLARE @programIdCheck nvarchar(100)
					delete from @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @programIdCheck = value_string FROM @values








					IF (
							 isnull(@diagnosticStarted,0) = 0
							 OR
							 datediff(minute,@repairDateStart, @repairDateEnd ) < 121
							 OR
							 isnull(@programIdCheck,'') = ''
-- 						 or isnull(@confirmationFirst,0) = 0
-- 						 or isnull(@confirmationSec,0) = 0
-- 						 or isnull(@confirmationThrid,0) = 0
-- 						 or isnull(@confirmationFourth,0) = 0
						 )
						 and isnull(@platformGroupName,'') = dbo.f_translate('VGP',default) and isnull(@DC,0) = 1
						BEGIN
							DECLARE @rejectionInfoContent NVARCHAR(MAX)

							SET @rejectionInfoContent = dbo.f_translate('Wniosek automatycznie odrzucony ze względu na poniższe powody:',default)
							IF isnull(@diagnosticStarted, 0 ) = 0
								BEGIN
									SET @rejectionInfoContent +=dbo.f_translate(' Brak rozpoczęczia diagnostyki,',default)
								end
							IF datediff(minute,@repairDateStart, @repairDateEnd ) < 121
								BEGIN
									SET @rejectionInfoContent +=dbo.f_translate(' Zbyt krótki czas naprawy,',default)
								end
							IF isnull(@programIdCheck,'') = ''
								BEGIN
									SET @rejectionInfoContent +=dbo.f_translate(' Pojazd nie jest objęty gwarancją mobilności,',default)
							end

							IF isnull(@confirmationFirst,0) = 0
								BEGIN
									SET @rejectionInfoContent +=dbo.f_translate(' Nie potwierdzono oświadczenia nr 1,',default)
							end
							IF isnull(@confirmationSec,0) = 0
								BEGIN
									SET @rejectionInfoContent +=dbo.f_translate(' Nie potwierdzono oświadczenia nr 2,',default)
							end
							IF isnull(@confirmationThrid,0) = 0
								BEGIN
									SET @rejectionInfoContent +=dbo.f_translate(' Nie potwierdzono oświadczenia nr 3,',default)
							end
							IF isnull(@confirmationFourth,0) = 0
								BEGIN
									SET @rejectionInfoContent +=dbo.f_translate(' Nie potwierdzono oświadczenia nr 4,',default)
							end




-- 							ISOK - False
							exec p_attribute_edit
								@attributePath =  '129,808,801',
								@stepId = 'xxx',
								@groupProcessInstanceId = @groupProcessInstanceId,
								@valueInt = 0,
								@err = @err,
								@message = @message


							exec p_attribute_edit
									@attributePath =  '129,808,63',
									@stepId = 'xxx',
									@groupProcessInstanceId = @groupProcessInstanceId,
									@valueText=  @rejectionInfoContent,
									@err = @err,
									@message = @message

-- 							Disable monits
							UPDATE dbo.process_instance SET active = 0 WHERE group_process_id = @groupProcessInstanceId and step_id  = '1021.300'


							DECLARE @processNewId int

							exec 		p_process_new
									@stepId = '1021.030',
									@groupProcessId = @groupProcessInstanceId,
									@previousProcessId = @previousProcessId,
									@rootId= @rootId,
									@parentProcessId = @previousProcessId,
									@processInstanceId = @processNewId OUTPUT,
									@err = @err OUTPUT ,
									@message = @message OUTPUT



							UPDATE dbo.process_instance
							SET active = 999
							WHERE id = @processNewId



							EXEC [dbo].[p_attribute_edit]
									@attributePath = '734',
									@stepId = 'xxx',
									@groupProcessInstanceId = @rootId,
									@valueInt= 999,
									@err = @err,
									@message = @message


							EXEC [dbo].[p_attribute_edit]
									@attributePath = '1070',
									@stepId = 'xxx',
									@groupProcessInstanceId = @groupProcessInstanceId,
									@valueInt= 1,
									@err = @err,
									@message = @message

							SET @variant = 3

						end
				end




		end
	end
	else
	begin
		set @variant=2
	end

	-- Import do CDN » Wyłącz automaty
	Update [dbo].[process_instance] SET active = 0 WHERE step_id = '1021.081' AND group_process_id=@groupProcessInstanceId

end