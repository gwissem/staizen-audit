ALTER PROCEDURE [dbo].[s_1009_005]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @processInstanceId INT
	DECLARE @partnerOnPlace INT
	DECLARE @eta DATETIME
	DECLARE @lateReason INT
	DECLARE @platformId INT
	DECLARE @platformName NVARCHAR(255)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @icsId INT
	DECLARE @status INT
	DECLARE @fixingOrTowing INT
	DECLARE @newProcessInstanceId INT
	DECLARE @code NVARCHAR(100)
	DECLARE @rootId INT
	DECLARE @partnerId INT
	DECLARE @fixingStatus INT
	DECLARE @towingStatus INT
	DECLARE @createdAt DATETIME 
	
	-- Pobranie GroupProcessInstanceId --
	SELECT	@groupProcessInstanceId = group_process_id, 
			@rootId = root_id,
			@stepId = step_id,
			@createdAt = created_at
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	IF @createdAt < DATEADD(hour, -12, getdate())
	BEGIN
		SET @variant = 4
	END 
	
	DECLARE @Values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '609', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	DECLARE @sqlQuery NVARCHAR(4000)
	SET @sqlQuery = 'select * from OPENQUERY(teka2, ''select s.id, IFNULL(o.eta,o.pre_eta), s.status_id, o.id order_id, s.created_at FROM StarterTeka.order_statuses s 
					 inner join StarterTeka.orders o ON o.id = s.order_id where o.id = '''''+CAST(@icsId AS NVARCHAR(10))+'''''   '') a'
		
	declare @statuses as table (
		id int, 
		eta DATETIME,
		status_id INT, 
		order_id INT, 
		created_at DATETIME
	)

	delete from @statuses
	insert into @statuses
	exec (@sqlQuery)
	
	SELECT top 1 @status = status_id FROM @statuses ORDER by id desc 
	
	
	IF ISNULL(@status,5) IN (6,7)
	BEGIN
		SET @variant = 1
		IF EXISTS(SELECT id FROM @statuses where status_id = 6)
		BEGIN
			 SET @fixingStatus = NULL
		     SET @towingStatus = 1
			 
		     SET @variant = 2
		END
		ELSE
		BEGIN
			SET @fixingStatus = 1
			SET @towingStatus = NULL
		END 
		
		IF @status = 7 
		BEGIN
			EXEC dbo.p_get_ics_finish_code @icsId = @icsId, @code = @code OUTPUT
			
 			-- nie naprawił na miejscu i nie holuje
			IF SUBSTRING(ISNULL(@code,''),6,2) IN ('6S')
			BEGIN
				SET @fixingStatus = 0
				SET @towingStatus = 0
				
				IF @fixingOrTowing = 2 AND @variant = 1
				BEGIN								
					
					EXEC [dbo].[p_rsa_process_new]
					@processInstanceId = @previousProcessId,
					@auto = 1,
					@fixingOrTowing = 1,
					@newProcessInstanceId = @newProcessInstanceId output																 
				END 
			END
			ELSE IF @code IN ('9214685','9214687','9214688','9214689','9214690')
			BEGIN
				-- pusty wyjazd
				SET @fixingStatus = 0
				SET @towingStatus = 0
				
				EXEC [dbo].[p_add_service_refuse_reason]
				@groupProcessInstanceId = @groupProcessInstanceId,
				@partnerLocationId = @partnerId,
				@reasonId = 999,
				@reason = dbo.f_translate('Zakończono zlecenie ze statusem ',default)'Pusty wyjazd'''
				
				EXEC p_process_jump
				@previousProcessId = @previousProcessId,
				@nextStepId = '1009.052',
				@doNext = 0,
				@parentId = @rootId,
				@rootId = @rootId,
				@groupId = @groupProcessInstanceId,
				@variant = @variant,
			    @err = @err output,
			    @message = @message output
	
			END
		END 
		
		EXEC p_attribute_edit
		@attributePath = '109', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = @towingStatus,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		EXEC p_attribute_edit
		@attributePath = '209', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = @fixingStatus,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
	END	
	ELSE IF @variant NOT IN (3,4)
	BEGIN
		
		SET @variant = 99
		UPDATE dbo.process_instance SET postpone_date = DATEADD(MINUTE,1,GETDATE()), postpone_count = postpone_count + 1 where id = @previousProcessId
	END
	
END



