
ALTER PROCEDURE [dbo].[s_1028_019]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	SET NOCOUNT ON
	PRINT 'wywoluje _019'
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @valueString NVARCHAR(255)
	declare @attributeValueId int
	declare @parent_attribute_value_id int
	declare @claimType int
	declare @updateOnly tinyint

	set @updateOnly=0

	SELECT @groupProcessInstanceId = group_process_id
	FROM process_instance where id = @previousProcessId
	
	declare @lastEmailsDate datetime

	select	@lastEmailsDate=date_leave
	from	dbo.process_instance 
	where	step_id='1028.019' and 
			id<>@previousProcessId and 
			group_process_id=@groupProcessInstanceId
	order by id desc

	DECLARE @Values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	declare @database int
	declare @caseNumber varchar(100)
	
	declare @tabResult2 as table
	(	description nvarchar(200), 
		val nvarchar(4000)
	)

	set @caseNumber='A'+right('000000000'+cast(@groupProcessInstanceId as varchar(20)),8)

	if exists (select id from dbo.process_instance with(nolock) WHERE group_process_id=@groupProcessInstanceId and step_id='1028.003' and active=0)
	begin
		set @database=1
	end
	else
	begin
		set @database=0
	end

	declare @counter int
	set @counter=1

	declare kur cursor LOCAL for
		select	id, parent_attribute_value_id
		from	dbo.attribute_value av
		where	av.attribute_path='501,462,463' and 
				av.group_process_instance_id=@groupProcessInstanceId

	OPEN kur;
	FETCH NEXT FROM kur INTO @attributeValueId,@parent_attribute_value_id
	WHILE @@FETCH_STATUS=0
	BEGIN
		declare @mailBody nvarchar(max)
		set @mailBody=''
		declare @tab as table(name nvarchar(500), path varchar(200), atrId int)
	
			declare @tabResult as table(description nvarchar(200), path nvarchar(100), atrId int)
			delete from @tabResult
			delete from @tabResult2
			
			declare @processStartDate datetime

			select @processStartDate=min(date_enter) 
			from dbo.process_instance with(nolock) WHERE 
			group_process_id=@groupProcessInstanceId
			
			declare @description nvarchar(500) 
			declare @val nvarchar(4000)
			declare @query nvarchar(1000)
			declare @value_int int
			declare @parentAtrId int
			declare @path nvarchar(100)
			declare @attribute_id int
			declare @updatedAt datetime

			declare kur2 cursor LOCAL for
				select	r.description,
						case 
						when r.path=dbo.f_translate('caseId',default) then 'A'+right('000000'+cast(@groupProcessInstanceId as varchar(20)),8)
						when r.path=dbo.f_translate('caseDate',default) then dbo.FN_VDateHour(@processStartDate)
						when r.path='517' then dbo.FN_VDateHour(av.value_date)
						else
							case when av.value_date is not null then dbo.FN_VDate(av.value_date) else '' end+
							isnull(cast(av.value_int as varchar(20)),'')+
							isnull(av.value_string,'')+
							isnull(av.value_text,'')
						end val,
						a.query,
						av.value_int,
						r.path,
						a.id,
						av.updated_at
				from	dbo.emailTemplates r left join
						dbo.attribute_value av on r.path=av.attribute_path and av.group_process_instance_id=@groupProcessInstanceId and av.id not in (
						
						select	id
						from	dbo.attribute_value
						where	attribute_path like '501,462,%' and 
								parent_attribute_value_id<>@parent_attribute_value_id and 
								group_process_instance_id=@groupProcessInstanceId
						
						) left join
						dbo.attribute a on a.id=r.atrId	
				WHERE  (templateId = 1)
				order by r.orderBy
				
				--order by r.path
			OPEN kur2;
			FETCH NEXT FROM kur2 INTO @description,@val,@query,@value_int,@path,@attribute_id,@updatedAt;
			WHILE @@FETCH_STATUS=0
			BEGIN
				
				if isnull(left(@query,3),'')<>''
				begin
						declare @valDict nvarchar(200)
						declare @idDict nvarchar(200)
				 
						SELECT top 1 @idDict=name 
						FROM 
						sys.dm_exec_describe_first_result_set
						(@query, NULL, 0) ;

						SELECT @valDict=name 
						FROM 
						sys.dm_exec_describe_first_result_set
						(@query, NULL, 0) ;
				
						set @query=replace(@query,dbo.f_translate('SELECT ',default),dbo.f_translate('SELECT TOP 99999 ',default))

						declare @SQLString nvarchar(4000)
						declare @ParmDefinition nvarchar(500)
						
						if @path='74,73'
						begin
							SELECT @val = textD FROM dbo.dictionary where typeD='makeModel' and value=@value_int
						end
						else
						begin
							if @path='505'
							begin
						

								if @value_int=-1 
								begin
									set @val=dbo.f_translate('inny',default)
								end

								if @value_int=0 
								begin
									set @val=dbo.f_translate('brak',default)
								end

								if @value_int>0
								begin
									select @val=shortName+' ('+city+')'
									from dbo.services
									where id=@value_int

								
								end
							end
							else
							begin
								if @path<>'445'
								begin
									SET @SQLString = 'SELECT @val = '+@valDict+' FROM ('+@query+') xxx where '+@idDict+'='''+cast(@value_int as varchar(20))+'''';
								END
								else
								begin
									SET @SQLString = 'SELECT @val = dbo.Concatenate('+@valDict+')  FROM ('+@query+') xxx where '+@idDict+' in (select data from dbo.f_split('''+cast(@val as varchar(20))+''','',''))';
								end

								SET @ParmDefinition = '@val nvarchar(200) OUTPUT';
									EXECUTE sp_executesql @SQLString, @ParmDefinition, @val=@val OUTPUT;		
								end
							end
						end
						
				if exists(
				
				SELECT	id
				FROM	dbo.attribute where id=@attribute_id and 
						input_type=dbo.f_translate('yes-no',default)
				)
				begin
					if @val='1' 
					begin
						set @val=dbo.f_translate('tak',default)
					end
					else
					begin
						set @val=dbo.f_translate('nie',default)
					end 
				end
			
				if isnull(@lastEmailsDate,'2050-01-01')<@updatedAt
				begin
					set @val='<span style='dbo.f_translate('background:red;',default)'>'+isnull(@val,'&nbsp;')+'</span>'
					set @updateOnly =1
				end
				
				INSERT INTO @tabResult2
				select @description, @val
				
				-------------------

				FETCH NEXT FROM kur2 INTO @description,@val,@query,@value_int,@path,@attribute_id,@updatedAt;
			END
			CLOSE kur2
			DEALLOCATE kur2
	
			declare @tabHTML as varchar(max)
	
			set @tabHTML='<BR><BR><BR><table cellpadding=1 cellspacing=0 border=1>'

			select @tabHTML=@tabHTML+'<tr><td style=''background:#EAEAEA;''>'+
					isnull(description,'&nbsp;')+'</td><td>'+
					isnull(val,'&nbsp;')+'</td></tr>' 			
			from	@tabResult2
	
			set @tabHTML=@tabHTML+'</table><BR><BR><BR>Mail wygenerowany automatycznie<BR>'

		declare @fileName varchar(200)
		set @fileName=@caseNumber+dbo.f_translate('.csv',default)

		delete from @Values
		INSERT @Values 
		EXEC p_attribute_get2	@attributePath = '501,462,463', 
								@groupProcessInstanceId = @groupProcessInstanceId,
								@attributeValueId=@attributeValueId
	
		SELECT	@claimType = value_int 
		FROM	@Values

		declare @to varchar(100)
		declare @CC varchar(100)
		declare @subject varchar(100)

		set @subject=@caseNumber+'/'+cast(@counter as varchar(10)) + dbo.f_translate(' - zgłoszenie szkody',default)
		--set @to='polska@vanameyde.com' --''
		set @to='polska@vanameyde.com;kzcfm@starter24.pl;szkodylp@starter24.pl;sebastian.baranowski@starter24.pl' 
		
		set @counter=@counter+1
		--10	Szkoda na pojeździe
		--20	Szkoda osobowa
		--30	Szkoda majątkowa
		
		if isnull(@claimType,10)=10 and @database=1
		begin
			if @updateOnly=0
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR>
				informujemy o rejestracji szkody z polisy OC ppm LeasePlan. <BR><BR>
				Przekazujemy sprawę do likwidacji technicznej do Działu Likwidacji Szkód Starter24.'+@tabHTML
			end
			else
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR> 
				informujemy o aktualizacji danych we wcześniej przyjętym zgłoszeniu szkody z polisy OC ppm LeasePlan. <BR>
				Zaktualizowane dane zaznaczone są w poniższej tabeli na czerwono.<BR>
				<BR><BR>
				Prosimy nie rejestrować nowej szkody.'+@tabHTML
			end
		end

		if isnull(@claimType,10)=10 and @dataBase=0
		begin
			if @updateOnly=0
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR>
				informujemy o rejestracji szkody z polisy OC ppm LeasePlan. <BR><BR>
				<B>UWAGA!</B><BR> Nie odnajdujemy polisy wskazanego sprawcy szkody w bazie polis. Uprzejmie prosimy o potwierdzenie, że wskazany pojazd sprawcy szkody posiadał polisę OC ppm aktualną w momencie zdarzenia szkodowego. Potwierdzenie prosimy wysłać na adres szkodylp@starter24.pl.
				Do czasu otrzymania potwierdzenia o ochronie nie będziemy podejmować dalszych czynności likwidacyjnych.'+@tabHTML
			end
			else
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR> 
				informujemy o aktualizacji danych we wcześniej przyjętym zgłoszeniu szkody z polisy OC ppm LeasePlan. <BR>
				Zaktualizowane dane zaznaczone są w poniższej tabeli na czerwono.<BR>
				<BR><BR>
				Prosimy nie rejestrować nowej szkody.'+@tabHTML
			end 		
		end
		
		if @claimType=20 and @database=1
		begin
			if @updateOnly=0
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR>
				informujemy o rejestracji szkody z polisy OC ppm LeasePlan. '+@tabHTML
			end
			else
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR> 
				informujemy o aktualizacji danych we wcześniej przyjętym zgłoszeniu szkody z polisy OC ppm LeasePlan. <BR>
				Zaktualizowane dane zaznaczone są w poniższej tabeli na czerwono.<BR>
				<BR><BR>
				Prosimy nie rejestrować nowej szkody.'+@tabHTML
			end
		end

		if @claimType=20 and @dataBase=0
		begin
			if @updateOnly=0
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR>
				informujemy o rejestracji szkody z polisy OC ppm LeasePlan. Informacje na temat szkody znajdują się w załączniku.<BR><BR>
				<B>UWAGA!</B><BR> Nie odnajdujemy polisy wskazanego sprawcy szkody w bazie polis.'+@tabHTML
			end
			else
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR> 
				informujemy o aktualizacji danych we wcześniej przyjętym zgłoszeniu szkody z polisy OC ppm LeasePlan. <BR>
				Zaktualizowane dane zaznaczone są w poniższej tabeli na czerwono.<BR>
				<BR><BR>
				Prosimy nie rejestrować nowej szkody.'+@tabHTML
			end
		end
		 
		if @claimType=30 and @database=1
		begin
			if @updateOnly=0
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR>
				informujemy o rejestracji szkody z polisy OC ppm LeasePlan. '+@tabHTML
			end
			else
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR> 
				informujemy o aktualizacji danych we wcześniej przyjętym zgłoszeniu szkody z polisy OC ppm LeasePlan. <BR>
				Zaktualizowane dane zaznaczone są w poniższej tabeli na czerwono.<BR>
				<BR><BR>
				Prosimy nie rejestrować nowej szkody.'+@tabHTML
			end
		end

		if @claimType=30 and @dataBase=0
		begin
			if @updateOnly=0
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR>
				informujemy o rejestracji szkody z polisy OC ppm LeasePlan. <BR><BR>
				<B>UWAGA!</B><BR> Nie odnajdujemy polisy wskazanego sprawcy szkody w bazie polis.'+@tabHTML
			end
			else
			begin
				set @mailBody=@mailBody+'Szanowni Państwo, <BR><BR> 
				informujemy o aktualizacji danych we wcześniej przyjętym zgłoszeniu szkody z polisy OC ppm LeasePlan.<BR> 
				Zaktualizowane dane zaznaczone są w poniższej tabeli na czerwono.<BR>
				<BR><BR>
				Prosimy nie rejestrować nowej szkody.'+@tabHTML
			end
		end
		
		EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('DOK',default)
		,                            @recipients		= @to
		,							 @copy_recipients	= @CC
		,                            @subject			= @subject
		,                            @body				= @mailBody
		,	                         @body_format		= dbo.f_translate('HTML',default)	
	

		FETCH NEXT FROM kur INTO @attributeValueId,@parent_attribute_value_id;
	END
	CLOSE kur
	DEALLOCATE kur

	declare @isReplacementVehicle int

	select @attributeValueId=id
	from	dbo.attribute_value av
	where	av.attribute_path='449' and 
			av.group_process_instance_id=@groupProcessInstanceId

	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '449', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=@attributeValueId
	
	SELECT	@isReplacementVehicle = value_int 
	FROM	@Values



	
	declare @firstname nvarchar(50)

	select @attributeValueId=id
	from	dbo.attribute_value av
	where	av.attribute_path='437,342,64' and 
			av.group_process_instance_id=@groupProcessInstanceId

	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '437,342,64', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=@attributeValueId
	
	SELECT	@firstname = value_string
	FROM	@Values

	declare @lastname nvarchar(50)

	select @attributeValueId=id
	from	dbo.attribute_value av
	where	av.attribute_path='437,342,66' and 
			av.group_process_instance_id=@groupProcessInstanceId

	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '437,342,66', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=@attributeValueId
	
	SELECT	@lastname = value_string
	FROM	@Values

	declare @phone nvarchar(50)

	select @attributeValueId=id
	from	dbo.attribute_value av
	where	av.attribute_path='437,342,408,197' and 
			av.group_process_instance_id=@groupProcessInstanceId

	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '437,342,408,197', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=@attributeValueId
	
	SELECT	@phone = value_string
	FROM	@Values


	declare @regNum nvarchar(50)

	select @attributeValueId=id
	from	dbo.attribute_value av
	where	av.attribute_path='74,72' and 
			av.group_process_instance_id=@groupProcessInstanceId

	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '74,72', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=@attributeValueId
	
	SELECT	@regNum = value_string
	FROM	@Values

	declare @makeModel int

	select @attributeValueId=id
	from	dbo.attribute_value av
	where	av.attribute_path='74,73' and 
			av.group_process_instance_id=@groupProcessInstanceId

	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '74,73', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=@attributeValueId
	
	SELECT	@makeModel = value_int
	FROM	@Values

	declare @rent int

	select @attributeValueId=id
	from	dbo.attribute_value av
	where	av.attribute_path='506' and 
			av.group_process_instance_id=@groupProcessInstanceId

	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '506', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=@attributeValueId
	
	SELECT	@rent = value_int
	FROM	@Values

	if @isReplacementVehicle=1
	begin
		set @to=null

		select top 1 @to=email
		from dbo.services
		where id=@rent

		set @mailBody=''
		set @subject=dbo.f_translate('Ubezpieczenie OC ppm LeasePlan / prośba o kontakt z Klientem / ',default)+@caseNumber
		--Adresat: adres e-mail do wypożyczalni (tutaj zaraz dołączę specyfikację kiedy i jak wypożyczalnia ma się dodawać i skąd ma być brany adres mailowy, nie zawsze to będzie adres ogólny jak wstępnie informowałam)
		--set @to='sebastian.baranowski@starter24.pl;katarzyna.sosnowska@starter24.pl'
		set @cc='kzcfm@starter24.pl;sebastian.baranowski@starter24.pl'  
	
		set @mailBody=@mailBody+'Szanowni Państwo,<BR><BR>
							W ramach roszczenia z ubezpieczenia OC ppm LeasePlan Ubezpieczenia, Poszkodowany wyraził chęć wynajmu pojazdu zastępczego do zgłoszonej szkody. Prosimy o nawiązanie kontaktu z Klientem i wyjaśnienie szczegółów wynajmu, zgodnie z Państwa ustaleniami z LPPL. W kwestiach merytorycznych prosimy o kontakt z Van Ameyde (e-mail: polska@vanameyde.com, w temacie powołując się na nasz numer sprawy: '+@caseNumber+').
							<BR><BR><BR>Poszkodowany: '+isnull(@firstname,'')+' '+isnull(@lastname,'')+dbo.f_translate(', tel. ',default)+isnull(@phone,'')



		EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('DOK',default)
		,                            @recipients		= @to
		,							 @copy_recipients	= @CC
		,                            @subject			= @subject
		,                            @body				= @mailBody
		,	                         @body_format		= dbo.f_translate('HTML',default)	
	end

	--Wysyłany dla serwisów z listy (BRS lub InterCars), jeśli wybrany inny to mail nie wychodzi.
	--Adresat: adres e-mail do serwisu przypisanego do sprawy
	declare @service int

	select @attributeValueId=id
	from	dbo.attribute_value av
	where	av.attribute_path='505' and 
			av.group_process_instance_id=@groupProcessInstanceId

	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '505', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=@attributeValueId
	
	SELECT	@service = value_int
	FROM	@Values


	
	if isnull(@service,0) not in (0,-1)
	begin
		set @to=null

		select top 1 @to=email
		from dbo.services
		where id=@service

		declare @makeModelV nvarchar(200)
		
		select @makeModelV=textD
		from dbo.dictionary
		where typeD='makeModel' and value=@makeModel


		set @mailBody=''
		set @subject=dbo.f_translate('Ubezpieczenie OC ppm LeasePlan / prośba o kontakt z Klientem / ',default)+isnull(@caseNumber,'')+' / '+isnull(@regNum,'')
	
		--set @to='sebastian.baranowski@starter24.pl;katarzyna.sosnowska@starter24.pl' -- @to
		set @cc='kzcfm@starter24.pl' -- 
	
		set @mailBody=@mailBody+'	Szanowni Państwo,<BR><BR>
		w ramach roszczenia z ubezpieczenia OC ppm LeasePlan Ubezpieczenia, Poszkodowany wyraził chęć naprawy uszkodzonego pojazdu w Państwa serwisie. Prosimy o nawiązanie kontaktu z Klientem i wyjaśnienie szczegółów naprawy, zgodnie z Państwa ustaleniami z LPPL. W kwestiach merytorycznych prosimy o kontakt z Van Ameyde (e-mail: polska@vanameyde.com, w temacie powołując się na nasz numer sprawy '+@caseNumber+').
		Uprzejmie prosimy o przesłanie kalkulacji naprawy wraz ze stosowną dokumentacją na adres polska@vanameyde.com w celu weryfikacji i akceptacji kosztów naprawy.
		W przypadku stwierdzenia szkody całkowitej prosimy odstąpić od naprawy i wysłać informację o szkodzie całkowitej na adres polska@vanameyde.com.
		Płatnikiem kosztów naprawy jest ubezpieczyciel.
		<BR><BR><BR>Poszkodowany: '+isnull(@firstname,'')+' '+isnull(@lastname,'')+dbo.f_translate(', tel. ',default)+isnull(@phone,'')+'<BR>
		Pojazd: '+isnull(@makeModelV,'')+'<BR>
		Nr rej: '+isnuLL(@regNum,'')+''


		EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('DOK',default)
		,                            @recipients		= @to
		,							 @copy_recipients	= @CC
		,                            @subject			= @subject
		,                            @body				= @mailBody
		,	                         @body_format		= dbo.f_translate('HTML',default)
	end


	delete from @Values
	INSERT @Values 
	EXEC p_attribute_get2	@attributePath = '528', 
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId=null
	
	declare @pathId int
	SELECT	@pathId = value_int
	FROM	@Values
	
	if @updateOnly=0
	begin

		set @to=null

		delete from @Values
		INSERT @Values 
		EXEC p_attribute_get2	@attributePath = '437,342,368', 
								@groupProcessInstanceId = @groupProcessInstanceId,
								@attributeValueId=null

		SELECT	@to = value_string
		FROM	@Values

		declare @subjectK nvarchar(500)
		declare @mailbodyK nvarchar(4000)
		set @mailBodyK=''
		set @subjectK=dbo.f_translate('Ubezpieczenie OC ppm LeasePlan / ',default)+isnull(@caseNumber,'')+' / '+isnull(@regNum,'')
	
		set @cc='kzcfm@starter24.pl;sebastian.baranowski@starter24.pl' -- 
	
		if @pathId=2 
		begin
			set @mailBodyK=@mailBodyK+'Szanowni Państwo,<BR><BR> 
			W imieniu ubezpieczyciela OC Euro Insurances w związku ze zgłoszoną szkodą '+isnull(@caseNumber,'')+' w pojeździe prosimy o przesłanie na adres mailowy szkodylp@starter24.pl zdjęć pojazdu, w celu wykonania kalkulacji kosztów naprawy:<BR><BR>
			- 4 zdjęcia obrazujące cały pojazd po przekątnych wraz z nr rejestracyjnym,<BR>
			- 2-3 zdjęć uszkodzeń,<BR>
			- zdjęcia nr VIN (17 cyfrowy nr nadwozia),<BR>
			- zdjęcie licznika – z przebiegiem,<BR>
			- zdjęć dowodu rejestracyjnego z obu stron.<BR>
			<BR><BR>
			W ciągu 7 dni roboczych w sprawie likwidacji szkody skontaktuje się z Panią/Panem ubezpieczyciel i poinformuje o dalszym procesie likwidacji. Nr Pani/Pana zgłoszenia to: '+isnull(@caseNumber,'')+'. Wszelkie informację dot. likwidacji zgłoszonej szkody można uzyskać pod numerem telefonu: (22) 500 66 00 oraz pisząc na adres email: polska@vanameyde.com.<BR><BR>
			<BR><img src="https://atlasdev.starter24.pl/files/image001.png">'
		
		end
		else 
		begin
			set @mailBodyK=@mailBodyK+'Szanowni Państwo,<BR><BR> 
			W ciągu 7 dni roboczych w sprawie likwidacji szkody skontaktuje się z Panią/Panem ubezpieczyciel i poinformuje o dalszym procesie likwidacji.
			Nr Pani/Pana zgłoszenia to: '+isnull(@caseNumber,'')+'.<BR>
			Wszelkie informację dot. likwidacji zgłoszonej szkody można uzyskać pod numerem telefonu: (22) 500 66 00 oraz pisząc na adres email: polska@vanameyde.com.<BR><BR>'

		end

		EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('DOK',default)
			,                            @recipients		= @to
			,							 @copy_recipients	= @CC
			,                            @subject			= @subjectK
			,                            @body				= @mailBodyK
			,	                         @body_format		= dbo.f_translate('HTML',default)
	end

	set @variant=1
END

