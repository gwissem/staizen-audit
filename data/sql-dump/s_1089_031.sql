ALTER PROCEDURE [dbo].[s_1089_031]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessId INT
	DECLARE @content NVARCHAR(MAX)
	DECLARE @phoneNumber NVARCHAR(100)
	DECLARE @attrTable Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @platformId INT
	DECLARE @platformPhone NVARCHAR(100)
	DECLARE @platformName NVARCHAR(100)
	DECLARE @verification INT
	DECLARE @caseId VARCHAR(8)
	DECLARE @programIds NVARCHAR(255)
	DECLARE @rootId INT
	DECLARE @regNo NVARCHAR(10)
	DECLARE @makeModelId INT
	DECLARE @makeModel NVARCHAR(255)
	DECLARE @driverPhone NVARCHAR(100)
	DECLARE @callerPhone NVARCHAR(100)
	
	SELECT @groupProcessId = group_process_id, @rootId = root_id FROM process_instance where id = @previousProcessId
	
	-- SET @caseId = 'A'+RIGHT('0000000'+CAST(@rootId AS NVARCHAR(8)),8)
	
	DELETE FROM @attrTable
	INSERT  @attrTable EXEC dbo.p_attribute_get2
			@attributePath = '80,342,408,197',
			@groupProcessInstanceId = @groupProcessId
	SELECT @driverPhone = value_string FROM @attrTable
	
	SET @phoneNumber = @driverPhone
	
	IF ISNULL(@driverPhone,'') = ''
	BEGIN
		
		DELETE FROM @attrTable
		INSERT  @attrTable EXEC dbo.p_attribute_get2
				@attributePath = '418,342,408,197',
				@groupProcessInstanceId = @groupProcessId
		SELECT @callerPhone = value_string FROM @attrTable
	
		SET @phoneNumber = @callerPhone
	END 
	
	DELETE FROM @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '253',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @platformId = value_int FROM @attrTable
	
	DELETE FROM @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '573',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @verification = value_int FROM @attrTable
	
	DELETE FROM @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '74,73',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @makeModelId = value_int FROM @attrTable
	
	SELECT @makeModel = textD FROM dictionary where typeD = 'makeModel' AND value = @makeModelId
	
	DELETE FROM @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '74,72',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @regNo = value_string FROM @attrTable
	
	SELECT @platformPhone = number from platform_phone where platform_id = @platformId
	SELECT @platformName = name FROM platform where id = @platformId
	SELECT @caseId = CAST(@groupProcessId AS VARCHAR(8))

	IF @verification = 1
	BEGIN
		SET @content = dbo.f_translate('Assistance dla pojazdu ',default) + ISNULL(@makeModel, '') + dbo.f_translate(' o numerze rejestracyjnym ',default)+@regNo+dbo.f_translate(' zostalo pozytywnie zweryfikowane.',default)
	END 
	ELSE IF @verification = 0 
	BEGIN
		SET @content = dbo.f_translate('Assistance dla pojazdu ',default) + ISNULL(@makeModel, '') + dbo.f_translate(' o numerze rejestracyjnym ',default)+@regNo+dbo.f_translate(' nie zostalo pozytywnie zweryfikowane.',default)
	END 
		
	EXEC p_note_new
	@groupProcessId = @groupProcessId,
	@type = dbo.f_translate('sms',default),
	@content = @content,
	@phoneNumber = @phoneNumber,
	@userId = 1,
	@originalUserId = 1,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	
END

