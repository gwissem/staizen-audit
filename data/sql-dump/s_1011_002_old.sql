
ALTER PROCEDURE [dbo].[s_1011_002_old]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @groupProcessInstanceId INT
	DECLARE @vinAndRegNumberMatch TINYINT
	DECLARE @vinAndRegNumberMatchId INT
	DECLARE @register_number NVARCHAR(255)
	DECLARE @mismatchVal NVARCHAR(4000)
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	declare @programIds NVARCHAR(255)
	declare @headerIds nvarchar(500)
	declare @removePrograms int
	set @removePrograms=0

	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id, @stepId = step_id FROM dbo.process_instance with(nolock) where id = @previousProcessId
	declare @alreadyProgramAssigned INT

	DECLARE @atribute_table Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	DELETE FROM @atribute_table
	INSERT @atribute_table EXEC dbo.p_attribute_get2
	@attributePath = '202',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @alreadyProgramAssigned = value_string FROM @atribute_table
	
	-- jeśli już wcześniej zgodzono się na usługi płatne nie aktualizujemy wartości
	IF @variant <> 99 AND ISNULL(@alreadyProgramAssigned, '') like '%423%'
	BEGIN
		SET @variant = 5	
	END 
	ELSE
	BEGIN
		
		DELETE FROM @atribute_table
		INSERT @atribute_table EXEC dbo.p_attribute_get2
				@attributePath = '74,72',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @register_number = value_string FROM @atribute_table;
	
		if @register_number=''
		begin
			set @errId=1
			return
		end
	
		DELETE FROM @atribute_table
		INSERT @atribute_table EXEC dbo.p_attribute_get2
				@attributePath = '74,75',
				@groupProcessInstanceId = @groupProcessInstanceId
	
		DECLARE @mileage int;
		SELECT @mileage = value_int FROM @atribute_table;
	
		DELETE FROM @atribute_table
		INSERT @atribute_table EXEC dbo.p_attribute_get2
				@attributePath = '74,71',
				@groupProcessInstanceId = @groupProcessInstanceId
	
		DECLARE @VIN varchar(20);
		SELECT @VIN = value_string FROM @atribute_table;
	
		set @register_number=replace(@register_number,' ','')
		
		declare @err int
		declare @message nvarchar(200) 
		declare @attributeValueId int
		DECLARE @tempTable TABLE (id INT IDENTITY(1,1), programId INT, startDate DATETIME, endDate DATETIME, mileageLimit NVARCHAR(20), headerId INT)
				
		IF len(isnull(@register_number,''))<4 and @variant=99
		BEGIN
			return
		end 

		IF len(isnull(@register_number,''))>4 and @variant=99
		BEGIN
			DECLARE @header_id INT
			SELECT top 1 @header_id = header_id FROM vin_element with(nolock) WHERE value = @register_number AND dictionary_id in (
			
				select id from dbo.vin_dictionary with(nolock) where description_org like '%rej%' and is_key=1
			
			)
			order by header_id desc
			;
	
			SET @variant = 99

			INSERT INTO @tempTable (programId, startDate, endDate, mileageLimit, headerId)
			values(null,null,null,null,@header_id)
		END
		else
		begin
			if isnull(@VIN,'')<>''
			begin

		
				declare @name nvarchar(200)
				declare @start nvarchar(20)
				declare @end nvarchar(20)
				declare @startDate datetime
				declare @mileageLimit nvarchar(20)
				declare @programId int
				declare @casesForVehicle INT
				
				iF OBJECT_ID('tempdb..#idHTable', 'U') IS NOT NULL
				/*Then it exists*/
				DROP TABLE #idHTable
				create table #idHTable  (idH int) 

				INSERT INTO #idHTable (idH)
				select distinct e1.header_id
				FROM   dbo.vin_element e1 inner join 
						dbo.vin_dictionary d on d.id=e1.dictionary_id and d.is_key=1
				where e1.value in (@register_number,@vin)  
				

				INSERT INTO @tempTable (programId, startDate, endDate, mileageLimit, headerId)
				select h.program_id --p.name
					   ,h.start_date
					   ,h.end_date 
					   ,e2.value
					   ,h.id
				FROM	dbo.vin_header h with(nolock) inner join
						#idHTable idh on idh.idH=h.id left join
						dbo.vin_element e2 with(nolock) on h.id=e2.header_id and e2.dictionary_id in (2385)
				where h.status=1
				and disable_date is null 
				and end_date>=convert(datetime,convert(varchar(8),getdate(),112)) 
				and isnull(start_date,'1981-01-01')<=convert(datetime,convert(varchar(8),getdate(),112))
				order by h.end_date asc
			


				declare @eventType int
				DELETE FROM @atribute_table
				INSERT @atribute_table EXEC dbo.p_attribute_get2
				@attributePath = '491',
				@groupProcessInstanceId = @groupProcessInstanceId
	
				select @eventType=value_int from @atribute_table

				if @eventType<>2
				begin
					delete from @tempTable where programId in (429,430)
					set @removePrograms=1
				end
					

				UPDATE t1 
				SET t1.mileageLimit = ISNULL(TRY_PARSE(t1.mileageLimit as INT),9999999) 
				FROM @tempTable t1 

				
				select @programIds = dbo.GROUP_CONCAT_S(DISTINCT programId,1), 
						@headerIds=dbo.GROUP_CONCAT_S(distinct headerId,1)			   
				FROM @tempTable
				WHERE mileageLimit >= ISNULL(@mileage, 1)

				select TOP 1 @programId = programId --p.name
					   ,@start=dbo.FN_fdate(startDate)
					   ,@startDate=startDate
					   ,@end=dbo.FN_fdate(endDate)
					   ,@mileageLimit=mileageLimit
					   ,@header_id=headerId
				FROM @tempTable tt inner join
						dbo.vin_program p on p.id=tt.programId
				WHERE mileageLimit >= ISNULL(@mileage, 1)
--				AND programId IN (402,414,415,416,417,418,419,420,421,424,425,426)
				ORDER BY p.breakdown_priority, mileageLimit DESC,endDate DESC
				


				
--				FROM	dbo.vin_element e1 with(nolock) inner join 
--						dbo.vin_header h with(nolock) on h.id=e1.header_id and h.status=1 inner join
--						dbo.vin_element e2 with(nolock) on h.id=e2.header_id and e2.dictionary_id in (2385)
--				where e1.value IN (@VIN,@register_number) and 
--						disable_date is null and 
--						end_date>getdate() and 
--						isnull(start_date,'1981-01-01')<getdate()
----				and ISNULL(NULLIF(e2.value, ''),9999999) >= isnull(@mileage,0)
--				order by h.end_date desc, h.id desc
				
				select @name=name
				from dbo.vin_program with(nolock)
				where id=@programId

				if @name is not null
				begin
					SET @variant = 1
					declare @val nvarchar(4000)
					declare @result_register_number nvarchar(20)
					declare @result_vin nvarchar(32)
					DECLARE @isMale INT
					EXEC p_gender 
						@groupProcessInstanceId = @rootId,
						@firstnameAttributePath = '80,342,64',
						@isMale = @isMale OUTPUT
					
					select top 1 @result_register_number = value FROM vin_element with(nolock) WHERE header_id in (select headerId from @tempTable) AND dictionary_id in (
						select id from dbo.vin_dictionary where description_org like '%rej%' and is_key=1
					) and nullif(value,dbo.f_translate('null',default)) is not null
					order by value desc
					
					select top 1 @result_vin = value FROM vin_element with(nolock) WHERE header_id in (select headerId from @tempTable) AND dictionary_id in (
						select id from dbo.vin_dictionary where description_org like '%vin%' and is_key=1
					)and nullif(value,dbo.f_translate('null',default)) is not null
					order by value desc
					

		  			IF @programId IS NOT NULL
		  			BEGIN
						EXEC [dbo].[p_attribute_edit]
						@attributePath = '207',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,						
						@valueString = @headerIds,
						@err = @err,
						@message = @message

			  			EXEC [dbo].[p_attribute_edit]
						@attributePath = '204',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,						
						@valueString = @programIds,
						@err = @err,
						@message = @message
						
						set @programIds=cast(@programId as nvarchar(100))
						
						EXEC [dbo].[p_attribute_edit]
						@attributePath = '202',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@attributeValueId = @attributeValueId,
						@valueString = @programIds,
						@err = @err,
						@message = @message
		  			END 
	
					SET @vinAndRegNumberMatch = 0
					
					DECLARE @mileageText NVARCHAR(300)
					SET @mileageText = ''

					IF NULLIF(@mileageLimit,'') IS NOT NULL AND @mileageLimit <> 9999999
					BEGIN
						SET @mileageText = dbo.f_translate(' do przebiegu ',default)+@mileageLimit+' km (jeżeli po weryfikacji okaże się, że przebieg samochodu jest wyższy, usługi assistance będą dla ' + dbo.f_conditionalText(@isMale, dbo.f_translate('Pana',default), dbo.f_translate('Pani',default)) +' płatne).'
					END 
					
					SET @val= dbo.f_conditionalText(@isMale, dbo.f_translate('Pana',default), dbo.f_translate('Pani',default)) + dbo.f_translate(' pojazd jest objęty ochroną assistance w ramach programu ',default)+@name+dbo.f_translate(' w okresie od ',default)+@start+' do '+@end + @mileageText
					
					set @val=@val+'<BR><table class='dbo.f_translate('table table-bordered',default)'><tr><td>Nazwa</td><td>Od</td><td>Do</td><td>Limit</td></tr>'

					select @val=@val+concat('<tr><td>dbo.f_translate(',aa.name, ',default)</td><td>',dbo.fn_vdate(aa.startDate), '</td><td>', dbo.fn_vdate(aa.endDate), '</td><td>', isnull(nullif(aa.mileageLimit,9999999),'-'), '</td></tr>') 
					from (
					select distinct p.name, tt.startDate, tt.endDate, tt.mileageLimit 
					from @tempTable tt inner join	
						dbo.vin_program p on p.id=tt.programId
						) aa




					set @val=@val+'</table>'
					
					if isnull(nullIF(@result_register_number,dbo.f_translate('null',default)),'')<>''
					begin
					
						SET @mismatchVal = ''
						IF ((@result_register_number = '' OR ISNULL(@result_register_number, @register_number) = @register_number) AND @result_vin = @VIN)
						BEGIN
							SET @vinAndRegNumberMatch = 1 
						END 
						ELSE IF @result_vin = @VIN
						BEGIN
							SET @misMatchVal=dbo.f_translate('Prawdopodobnie wprowadzono zły numer rejestracyjny. Odnaleziono uprawnienia dla wskazanego numeru nadwozia, lecz numer rejestracyjny się nie zgadza. Potwierdź numer rejestracyjny z Klientem i upewnij się, że wprowadzono prawidłowo.',default) 
						END 
						ELSE IF @result_register_number = @register_number
						BEGIN
							SET @misMatchVal=dbo.f_translate('Prawdopodobnie wprowadzono zły numer VIN. Odnaleziono uprawnienia dla wskazanego numeru rejestracyjnego, potwierdź VIN z Klientem i upewnij się czy wprowadzono prawidłowy.',default) 
						END 
					end
					else
					begin
						SET @vinAndRegNumberMatch = 1  
					end

					------ START
					------ Uzupełnienie danych o zakresie ochrony
					
					DECLARE @okresOchrony VARCHAR(200)
					
					SET @okresOchrony = 'od ' + @start + ' do ' + @end
					
					IF @mileageLimit <> 9999999
					BEGIN
						SET @okresOchrony = @okresOchrony + dbo.f_translate(', limit przebiegu: ',default) + @mileageLimit + ' km'
					END
					
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '854',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueString = @okresOchrony,
						@err = @err,
						@message = @message
						
					------ END
						
					IF @mismatchVal <> ''
					BEGIN
						EXEC [dbo].[p_attribute_edit]
						@attributePath = '535',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueText = @mismatchVal,
						@err = @err,
						@message = @message
					END 
					
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '493',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueInt = @vinAndRegNumberMatch,
						@err = @err,
						@message = @message
					
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '423',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueText = @val,
						@err = @err,
						@message = @message
	
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '74,105',
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueDate = @startDate,
						@err = @err,
						@message = @message
	
				end
				else
				begin
					SET @variant = 2
				end
			end
			else
			begin
				SET @variant = 2
			end
		end
		
		if @variant=2
		begin
			
			SELECT  top 1 @programId = h.program_id --p.name
					,@start=dbo.FN_fdate(h.start_date)
					,@startDate = h.start_date
					,@end=dbo.FN_fdate(h.end_date) 
					,@mileageLimit=e2.value
					,@header_id=h.id
				FROM	dbo.vin_element e1 with(nolock) inner join 
					dbo.vin_header h with(nolock) on h.id=e1.header_id and h.status=1 left join
					dbo.vin_element e2 with(nolock) on h.id=e2.header_id and e2.dictionary_id in (2385)
				where e1.value IN (@VIN,@register_number) and 
					disable_date is null 
			order by e2.value desc, h.end_date desc, h.id desc
			
			DECLARE @isMale2 INT
			EXEC p_gender 
				@groupProcessInstanceId = @rootId,
				@firstnameAttributePath = '80,342,64',
				@isMale = @isMale2 OUTPUT
			
			select @name=name
			from dbo.vin_program with(nolock)
			where id=@programId
			
			PRINT dbo.f_translate('vin:',default)
			PRINT @VIN
			PRINT dbo.f_translate('nr rej:',default)
			PRINT @register_number
			PRINT '-----'
			PRINT dbo.f_translate('limit2:',default)
			PRINT @mileageLimit
				
			if isnull(@name,'')<>''
			begin
--				SET @val=dbo.f_conditionalText(@isMale2, dbo.f_translate('Pana',default), dbo.f_translate('Pani',default)) + dbo.f_translate(' pojazd nie jest objęty ochroną assistance. Ostatnie zarejestrowane uprawnienie w ramach programu ',default)+@name
				
				if @removePrograms=0
				begin
					SET @val= dbo.f_translate('System nie potwierdził uprawnień asssistance dla pojazdu z tym numerem nadwozia. Ostatnie zarejestrowane uprawnienie w ramach programu ',default)+@name
					SET @val= @val+isnull(' obowiązywało w okresie od '+@start+' do '+@end,'')
					if @mileageLimit<>'' SET @val= @val+isnull(' (do przebiegu '+@mileageLimit+' km).' ,'')
				end
				else
				begin
					SET @val= dbo.f_translate('System nie potwierdził uprawnień asssistance dla pojazdu z tym numerem nadwozia. Uprawnienie w ramach programu ',default)+@name
					SET @val= @val+dbo.f_translate(' nie obowiązuje dla danego typu zdarzenia.',default)

					EXEC [dbo].[p_attribute_edit]
							@attributePath = '920',
							@stepId = 'xxx',
							@groupProcessInstanceId = @groupProcessInstanceId,
							@valueInt = 1,
							@err = @err,
							@message = @message
				end
			end
			else
			begin
				-- SET @val=dbo.f_conditionalText(@isMale2, dbo.f_translate('Pana',default), dbo.f_translate('Pani',default))+dbo.f_translate(' pojazd nie jest objęty ochroną assistance. Nie znaleźliśmy uprawnień w bazie danych.',default) 
				SET @val = dbo.f_translate('System nie potwierdził uprawnień asssistance dla pojazdu z tym numerem nadwozia.',default) 
			end
			print @val
	
			DELETE FROM @atribute_table
			INSERT @atribute_table EXEC dbo.p_attribute_get2
			@attributePath = '423',
			@groupProcessInstanceId = @groupProcessInstanceId
			select @attributeValueId = id from @atribute_table
	
			EXEC [dbo].[p_attribute_set2]
					@attributePath = '423',
					@stepId = 'xxx',
					@groupProcessInstanceId = @groupProcessInstanceId,
					@attributeValueId = @attributeValueId,
					@valueText = @val,
					@err = @err,
					@message = @message
				--	select @val
		end
	
		declare @cid int
		declare @cvalue nvarchar(1000)
		declare @cdescription nvarchar(1000)
		declare @cattribute_path varchar(200);
	
		if @variant=99
		begin
			declare kur cursor LOCAL for
				select	av.id,null,v.description,v.attribute_path
				from dbo.vin_dictionary v with(nolock) inner join
					dbo.attribute_value av with(nolock) on av.attribute_path=v.attribute_path and av.group_process_instance_id=@groupProcessInstanceId and v.attribute_path is not null
			OPEN kur;
			FETCH NEXT FROM kur INTO @cid,@cvalue,@cdescription,@cattribute_path;
			WHILE @@FETCH_STATUS=0
			BEGIN
				if @cattribute_path='74,561'
				begin
					EXEC [dbo].[p_attribute_set2]
							@attributePath = @cattribute_path,
							@stepId = 'xxx',
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId = @cid,
							@valueDate = null,
							@err = @err,
							@message = @message
				end
				else
				begin
					EXEC [dbo].[p_attribute_set2]
							@attributePath = @cattribute_path,
							@stepId = 'xxx',
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId = @cid,
							@valueString = @cvalue,
							@err = @err,
							@message = @message
				end
	
				FETCH NEXT FROM kur INTO @cid,@cvalue,@cdescription,@cattribute_path;
			END
			CLOSE kur
			DEALLOCATE kur
		end
		ELSE IF @mileage > 0
		BEGIN
			-- sprawdzenie czy w systemie jest już sprawa dla tego vinu z wyższym przebiegiem
			
			declare @wrongMileageText nvarchar(max)
			declare @wrongMileageHistoryValue INT
			declare @wrongMileageHistoryCaseNumber INT
			declare @wrongMileageHistoryCaseDate DATETIME
			
			select top 1 @wrongMileageHistoryCaseNumber = av.root_process_instance_id, 
			@wrongMileageHistoryValue = av.value_int, 
			@wrongMileageHistoryCaseDate = av.created_at
			from dbo.attribute_value av  with(nolock)
			where attribute_path = '74,75'
			and root_process_instance_id <> @rootId
			and value_int > @mileage
			and exists(select * from dbo.attribute_value av2 with(nolock) where attribute_path = '74,71' and value_string = @VIN and av2.parent_attribute_value_id = av.parent_attribute_value_id)
			order by created_at desc
			
			if @wrongMileageHistoryCaseNumber is not NULL AND @stepId = '1011.002'
			BEGIN
				
				SET @wrongMileageText = dbo.f_translate('W sprawie A',default)+ RIGHT('000000'+CAST(@wrongMileageHistoryCaseNumber AS NVARCHAR(10)), 8)+ dbo.f_translate(' z dnia ',default)+CONVERT(VARCHAR(10), @wrongMileageHistoryCaseDate, 120)+dbo.f_translate(' dla pojazdu o nr VIN ',default)+@VIN+dbo.f_translate(', zarejestrowano przebieg o wartości ',default)+CAST(@wrongMileageHistoryValue as NVARCHAR(10))+'  km, który jest wyższy niż zadeklarowany przez klienta ('+CAST(@mileage as NVARCHAR(10))+' km)'
				
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '594',
				@stepId = @stepId,
				@groupProcessInstanceId = @groupProcessInstanceId,
				@valueText = @wrongMileageText,
				@err = @err,
				@message = @message
				
				SET @variant = 3
			END 
		END 
		
	
		if @header_id is not null
		begin
	
			DECLARE @vin_table TABLE (id INT, value nvarchar(300), description nvarchar(255),attribute_path varchar(200));
	
			INSERT @vin_table 
			SELECT distinct top 300 vd.id, ve.value, vd.description, vd.attribute_path 
			FROM	vin_element ve with(nolock) INNER JOIN 
					vin_dictionary vd with(nolock) ON ve.dictionary_id = vd.id 
			WHERE	ve.header_id=@header_id and
					vd.attribute_path is not null
			order by vd.id 
		
			declare @makeModel nvarchar(300)
			declare @makeModelAttrId int
			set @makeModel=''
	

			
			declare kur cursor LOCAL for
				select	av.id,v.value,v.description,v.attribute_path
				from @vin_table v inner join
					dbo.attribute_value av with(nolock) on av.attribute_path=v.attribute_path and av.group_process_instance_id=@groupProcessInstanceId and v.attribute_path is not null
				order by v.id
			OPEN kur;
			FETCH NEXT FROM kur INTO @cid,@cvalue,@cdescription,@cattribute_path;
			WHILE @@FETCH_STATUS=0
			BEGIN
				if @cattribute_path in ('74,71','74,72')
				begin
					set @cvalue = UPPER(@cvalue)
				end
				
				if @cattribute_path='74,73' 
				begin
					set @makeModel=@makeModel+@cvalue+' ' 
					set @makeModelAttrId=@cid
				end
				else
				begin
					if @cattribute_path='74,561' 
					begin
						EXEC [dbo].[p_attribute_edit]
							@attributePath = @cattribute_path,
							@stepId = 'xxx',
							@groupProcessInstanceId = @groupProcessInstanceId,
							@attributeValueId = @cid,
							@valueDate = @cvalue,
							@err = @err,
							@message = @message
					
						declare @firstRegDate datetime

						DELETE FROM @atribute_table
						INSERT @atribute_table EXEC dbo.p_attribute_get2
						@attributePath = '74,233',
						@groupProcessInstanceId = @groupProcessInstanceId
						select @firstRegDate = value_date from @atribute_table
					end
					else --if @variant=99
					begin
						EXEC [dbo].[p_attribute_set2]
								@attributePath = @cattribute_path,
								@stepId = 'xxx',
								@groupProcessInstanceId = @groupProcessInstanceId,
								@attributeValueId = @cid,
								@valueString = @cvalue,
								@err = @err,
								@message = @message
					end
				end
				FETCH NEXT FROM kur INTO @cid,@cvalue,@cdescription,@cattribute_path;
			END
			CLOSE kur
			DEALLOCATE kur
	
			if @makeModel <> ''
			begin
				declare @makeModelAtlasId int

				SELECT @makeModelAtlasId = dbo.f_makeModelStandard(@makeModel, 1)
				IF @makeModelAtlasId IS NOT NULL
				BEGIN
					EXEC [dbo].[p_attribute_set2]
						@attributePath = @cattribute_path,
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@attributeValueId = @makeModelAttrId,
						@valueInt = @makeModelAtlasId,
						@err = @err,
						@message = @message
				END
			end
	
		end
		
	END
	
	UPDATE dbo.attribute_value 
	set value_string=upper(value_string)
	where root_process_instance_id=@rootId and attribute_id in (71,72)



END

