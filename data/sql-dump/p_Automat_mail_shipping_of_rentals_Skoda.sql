



ALTER PROCEDURE [dbo].[p_Automat_mail_shipping_of_rentals_Skoda]
@send tinyint
as
	begin
	set nocount on
	return
	declare @result table (
							RootId int
							,GroupProcessId int
							,Client_car nvarchar(255)
							,Replacement_car nvarchar(255)
							,Client_car_make nvarchar(255)
							,Replacement_car_make nvarchar(255)
							,Contractor nvarchar(255)
							,Assigned_date datetime
							,Assigned_by nvarchar(255)
							,Reason nvarchar(255)
							,is_Skoda int
							,is_VGP int
							,Except_VGP int
							,is_Other int
							,is_Empty int
							,Occurence int
			)
	
	insert into @result
	Select
	main2.RootId
	,main2.GroupProcessId
	,main2.Client_car
	,main2.Replacement_car
	,main2.Client_car_make
	,main2.Replacement_car_make
	,main2.Wykonujacy as Contractor
	,main2.Data_przypisania
	,main2.Assigned_by
	,main2.Reason
	,case when main2.Replacement_car_make = dbo.f_translate('Skoda',default) then 1 else 0 end as is_Skoda
	,case when main2.Replacement_car_make in ('Audi', dbo.f_translate('Skoda',default), dbo.f_translate('Volkswagen',default), dbo.f_translate('Seat',default)) then 1 else 0 end as is_vgp
	,case when main2.Replacement_car_make not in ('Audi', dbo.f_translate('Skoda',default), dbo.f_translate('Volkswagen',default), dbo.f_translate('Seat',default)) then 1 else 0 end as except_vgp
	,case when main2.Replacement_car_make not in ('Audi', dbo.f_translate('Skoda',default), dbo.f_translate('Volkswagen',default), dbo.f_translate('Seat',default),dbo.f_translate('BMW',default)) and main2.Replacement_car_make is not null then 1 else 0 end as is_other
	,case when main2.Replacement_car_make is null then 1 else 0 end as is_empty
	,1 as Occurence
	from
	(
	select
		main.RootId
		,main.GroupProcessId
		,d.textD as Client_car
		,d2.textD as Replacement_car
		,LEFT(d.textD, CHARINDEX(' ',d.textD)-1) as Client_car_make
		,LEFT(d2.textD, CHARINDEX(' ',case when d2.textD = dbo.f_translate('inna',default) then dbo.f_translate('inna inna',default) else d2.textD end)-1) as Replacement_car_make
		,avRental.updated_at as Data_przypisania
		,[dbo].[f_PartnerLocation_name](avRental.value_int) as Wykonujacy
		,concat(f.firstname,' ',f.lastname) as Assigned_by
		,a.value_int
		,d3.textD as Reason
		from
		(
		select distinct
		ReplacementVehicle.root_id as RootId
		,ReplacementVehicle.group_process_id as GroupProcessId
		from dbo.process_instance ReplacementVehicle with(nolock)
		where ReplacementVehicle.step_id = '1007.037'
		and year(ReplacementVehicle.created_at) = year(getdate())  -- >= getdate() - 90
		and month(ReplacementVehicle.created_at) = month(getdate())
		and case when exists (Select top 1 1 from dbo.service_status ss with(nolock) where ss.group_process_id = ReplacementVehicle.group_process_id and ss.status_dictionary_id = '-1' order by ss.created_at desc) then 1 else 0 end =0		
		) main 
		join dbo.attribute_value avKlientAuto with(nolock) on main.RootId = avKlientAuto.root_process_instance_id and avKlientAuto.attribute_path = '74,73' 
		join dbo.dictionary d with(nolock) on d.value = avKlientAuto.value_int and d.typeD = 'makeModel' and d.active = 1 and d.textD like '%Skoda%'
		left join dbo.attribute_value avReplacementVehicle with(nolock) on avReplacementVehicle.group_process_instance_id = main.GroupProcessId and avReplacementVehicle.attribute_path = '764,73'
		left join dbo.dictionary d2 with(nolock) on d2.value = avReplacementVehicle.value_int and d2.typeD = 'makeModel' and d2.active = 1
		left join dbo.attribute_value avRental with(nolock) on avRental.group_process_instance_id = main.GroupProcessId and avRental.attribute_path = '764,742'		
		left join dbo.fos_user f with(nolock) on f.id = avRental.updated_by_id
		left join dbo.attribute_value a with(nolock) on a.group_process_instance_id = main.GroupProcessId and a.attribute_path = '764,136'
		left join dbo.dictionary d3 with(nolock) on d3.value = a.value_int and d3.typeD = 'rentalOfferRefuse' and ISNULL(d3.argument1,'') <> dbo.f_translate('hidden',default)
		left join dbo.attribute_value av_program with(nolock) on av_program.group_process_instance_id = main.GroupProcessId and av_program.attribute_path = '202'
		WHERE av_program.value_string IN (select id from vin_program where platform_id=6)
	)main2

--select * from @result


	if (@send = 1)
	begin
		declare @subject nvarchar(100) = dbo.f_translate('Wynajmy Skoda',default)
		declare @body as nvarchar(max)
		
	
	
		set @body = '<BR>
							 Stan na chwilę obecną:
							<BR>'

		select @body=@body+
						+dbo.f_translate('Wynajmów rozpoczętych od początku miesiąca: ',default) + isnull(cast(sum(Occurence) as nvarchar(20)),'&nbsp;') + '<BR>' +
						+dbo.f_translate('MW - Skoda: ',default) + isnull(cast(sum(is_Skoda) as nvarchar(20)),'&nbsp;')+ ';	'+ isnull((cast(cast(cast(sum(is_Skoda) as decimal (10,2))/cast(sum(Occurence) as decimal (10,2)) * 100.00 as decimal(5,2)) as nvarchar(20)) + '%'),'&nbsp;') +'<BR>'+
						+'MW - VGP(poza Skoda): ' + isnull(cast(sum(is_VGP)-sum(is_Skoda) as nvarchar(20)),'&nbsp;') +';	'+ isnull((cast(cast(cast((sum(is_VGP)-sum(is_Skoda)) as decimal (10,2))/cast(sum(Occurence) as decimal (10,2)) * 100.00 as decimal(5,2)) as nvarchar(20)) + '%'),'&nbsp;') +'<br>'+
						+dbo.f_translate('MW - inne: ',default) + isnull(cast(sum(Except_VGP) as nvarchar(20)),'&nbsp;') +';	'+ isnull((cast(cast(cast(sum(Except_VGP) as decimal (10,2))/cast(sum(Occurence) as decimal (10,2)) * 100.00 as decimal(5,2)) as nvarchar(20)) + '%'),'&nbsp;') +'<br>'
						+dbo.f_translate('MW - brak uzupełnionego pojazdu zastępczego: ',default) + isnull(cast(sum(is_Empty) as nvarchar(20)),'&nbsp;') +';	'+ isnull((cast(cast(cast(sum(is_Empty) as decimal (10,2))/cast(sum(Occurence) as decimal (10,2)) * 100.00 as decimal(5,2)) as nvarchar(20)) + '%'),'&nbsp;') +'<br>'
						 from @result 
			
		set @body=@body+'<BR>'
	
		set @body =@body + '<BR>
						 Inne:
						<BR>
						<table cellpadding=1 cellspacing=0 border=1>
						<tr style=''background:#e7e8de;''>
						<td>Numer sprawy</td>
						<td>Nazwa RAC</td>
						<td>Data przypisania RAC</td>
						<td>RAC przypisany przez</td>
						<td>Marka i model pojazdu zastępczego</td>
						<td>Uzasadnienie</td>
						</tr>'
		
						select @body=@body+'<tr><td>'+
						isnull(cast([dbo].[f_caseId](RootId) as nvarchar(30)),'&nbsp;')+'</td><td>'+
						isnull(Contractor,'&nbsp;') +'</td><td>'+
						isnull(convert(varchar(16),Assigned_date,120),'&nbsp;') +'</td><td>'+
						isnull(Assigned_by,'&nbsp;') +'</td><td>'+
						isnull(Replacement_car,'&nbsp;') +'</td><td>'+
						isnull(Reason,'&nbsp;') +'</td><td>'+'</td></tr>' 			from @result 
						where Replacement_car is not null
						and Contractor is not null
						and Assigned_by is not null
						and Assigned_by is not null
						and Except_VGP = 1	
			
			set @body=@body+'</table><BR>'

			set @body = @body +'
							<HR>
							 Brak informacji:
							<BR>
							<table cellpadding=1 cellspacing=0 border=1>
							<tr style=''background:#e7e8de;''>
							<td>Numer sprawy</td>
							<td>Nazwa RAC</td>
							<td>Data przypisania RAC</td>
							<td>RAC przypisany przez</td>
							<td>Marka i model pojazdu zastępczego</td>
							<td>Uzasadnienie</td>
							</tr>'  

						select @body=@body+'<tr><td>'+
						isnull(cast([dbo].[f_caseId](RootId) as nvarchar(30)),'&nbsp;')+'</td><td>'+
						isnull(Contractor,'&nbsp;') +'</td><td>'+
						isnull(convert(varchar(16),Assigned_date,120),'&nbsp;') +'</td><td>'+
						isnull(Assigned_by,'&nbsp;') +'</td><td>'+
						isnull(Replacement_car,'&nbsp;') +'</td><td>'+
						isnull(Reason,'&nbsp;') +'</td><td>'+'</td></tr>' 			from @result 
						where is_Empty = 1	


			set @body=@body+'</table><BR>'


	
			
	
				EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('DOK',default)
				,                            @recipients  = 'Anna.Piotrowska@starter24.pl;Waldemar.Kulagowski@starter24.pl;Alicja.Rymkiewicz@starter24.pl;Lukasz.Wiacek@starter24.pl;kz@starter24.pl;Patrycja.Petroczko@starter24.pl;analizy@starter24.pl;maciej.grzelak@starter24.pl;maria.przybylska@starter24.pl'
				,                            @subject     = @subject
				,                            @body        = @body
				,                            @body_format = dbo.f_translate('HTML',default) ;
	
	
	end			
	else
	begin
		print dbo.f_translate('Tylko raport',default)
		select * from @result
	end
end


