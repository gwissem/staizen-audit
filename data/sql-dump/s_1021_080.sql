ALTER PROCEDURE [dbo].[s_1021_080]
(
	@previousProcessId INT,
	@variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
)
AS
BEGIN
	DECLARE @err TINYINT
	DECLARE @message NVARCHAR(255)
	DECLARE @caseId int -- (235)
	DECLARE @groupProcessInstanceId INT
	DECLARE @parentId INT
	DECLARE @caseParentId INT
	declare @acceptStepId int
	declare @platformId int
	declare @root_id int
	DECLARE @partnerMail NVARCHAR(200)
	DECLARE @subjectMail NVARCHAR(200)
	DECLARE @messageMail NVARCHAR(MAX)
	DECLARE @senderMail VARCHAR(200)

	SELECT @groupProcessInstanceId = group_process_id, @parentId = parent_id, @root_id=root_id FROM process_instance where id = @previousProcessId

	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '235', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @caseId = value_int FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values

	declare @askType int

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '883,890', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @askType = value_int FROM @values

	declare @newCase int
	declare @rootId int
	set @newCase=0

	PRINT '@platformId'
	PRINT CAST(@platformId as nvarchar(10))
	PRINT '@asktype'
	PRINT CAST(@askType as nvarchar(10))
	PRINT '@caseID'
	PRINT CAST(@caseId as nvarchar(10))

	if @caseId=-1 and @platformId in (2,76) and @askType=1
	begin
		print dbo.f_translate('new case',default)
		EXEC dbo.p_process_new
				@stepId = '1011.061',
				@userId = @currentUser,
				@originalUserId = @currentUser,
				@skipTransaction = 1,
				@rootId=@root_id,
				@previousProcessId=@previousProcessId,
				@groupProcessId=@groupProcessInstanceId,
				@err = @err OUTPUT,
				@message = @message OUTPUT,
				@processInstanceId = @caseId OUTPUT

		set @newCase=1
		set @rootId = @groupProcessInstanceId
	end
		
	IF @caseId=-1 and @platformId in (2,76)  -- Pozostaje ten nr co był, Platforma Ford
	BEGIN
		EXEC [dbo].[p_attribute_edit]
				@attributePath = '452',
				@stepId = 'xxx',
				@groupProcessInstanceId = @groupProcessInstanceId,
				@valueInt = 1,
				@err = @err,
				@message = @message

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '129,800,368', @groupProcessInstanceId = @groupProcessInstanceId

		SELECT @partnerMail = value_string FROM @values
		--				SELECT @partnerMail = dbo.f_partner_contact(@groupProcessInstanceId,@groupProcessInstanceId,@partnerLocId,1,8)

		SET @messageMail = '<HTML>' +
												'<h4>Szanowni Państwo,</h4><br>' +
												dbo.f_translate('Informujemy o aktualizacji tymczasowego nr sprawy -',default) +
												CAST( isnull(dbo.f_caseId(@root_id),'') as nvarchar(10))
												+ dbo.f_translate('. Sprawa będzie od tego momentu identyfikowana nowym numerem -  ',default)
												+ cast(isnull(dbo.f_caseId(@root_Id),'') as nvarchar(10)) +'<br>' +
												dbo.f_translate('Prosimy się posługiwać tym numerem sprawy przy kontaktach w sprawie wniosku/wystawianiu faktury.',default)
												+'<br></HTML>';
		PRINT isnull(@messageMail,dbo.f_translate('NULL MAIL MESSAGE',default))

		PRINT CAST( isnull(dbo.f_caseId(@root_id),dbo.f_translate('root id',default)) as nvarchar(10))

		if @platformId=2
		begin
			SET @subjectMail = dbo.f_translate('Ford Pomoc - Starter24 - aktualizacja numeru sprawy - ',default) +	CAST( isnull(dbo.f_caseId(@root_id),'') as nvarchar(10))
		end
		else
		begin
			SET @subjectMail = dbo.f_translate('Seat Assistance - Starter24 - aktualizacja numeru sprawy - ',default) +	CAST( isnull(dbo.f_caseId(@root_id),'') as nvarchar(10))
		end
		SET @senderMail = [dbo].[f_getEmail]('rs')
		PRINT '--@senderMail'
		PRINT @senderMail
		EXEC p_note_new
				@groupProcessId =@groupProcessInstanceId,
				@type =dbo.f_translate('email',default),
				@content = dbo.f_translate('Wysłano informację o zmianie nr sprawy na adres email serwisu',default),
				@phoneNumber = NULL,
				@email = @partnerMail,
				@subject = @subjectMail,
				@direction = 1,
				@emailRegards = 2,
				@err = @err OUTPUT,
				@message = @message OUTPUT,
				-- FOR EMAIL
				@dw = '',
				@udw = '',
				@sender = @senderMail,
				@additionalAttachments = '',
				@emailBody  = @messageMail
	END
		
	IF @caseId > -1 and @newCase=0
	BEGIN
		IF @root_id <> @caseId
		BEGIN
			-- Ustawienie Daty przyjęcia na warsztat:
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '129,125', @groupProcessInstanceId = @groupProcessInstanceId
			DECLARE @acceptanceDate DATETIME = (SELECT TOP 1 value_date FROM @values WHERE value_date IS NOT NULL)
			IF @acceptanceDate IS NULL
			BEGIN
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '883,896', @groupProcessInstanceId = @groupProcessInstanceId
				SET @acceptanceDate = (SELECT TOP 1 value_date FROM @values WHERE value_date IS NOT NULL)

				DECLARE @acceptanceDateParrent INT = (
					SELECT TOP 1 id FROM attribute_value WHERE group_process_instance_id = @groupProcessInstanceId AND attribute_path='129'
				)
				IF @acceptanceDateParrent IS NOT NULL
				BEGIN
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '129,125',
						@parentAttributeValueId= @acceptanceDateParrent,
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueDate = @acceptanceDate,
						@err = @err,
						@message = @message
				END
			END

			EXEC dbo.p_serviceDetaching @rootId = @caseId

			EXEC dbo.p_process_change_ancestors
					@groupProcessId = @groupProcessInstanceId,
					@parentProcessId = @caseId,
					@rootProcessId = @caseId,
					@userId = 1,
					@originalUserId = 1,
					@skipTransaction = 1,
					@err = @err OUTPUT,
					@message = @message OUTPUT
			
			DECLARE @RSpreviousProcessId INT = (
				SELECT TOP 1 id FROM process_instance WHERE root_id=@caseId AND step_id LIKE '1021.%' AND active=1 ORDER BY id DESC
			)

			IF @RSpreviousProcessId IS NOT NULL
			BEGIN
				EXEC p_process_jump
				@previousProcessId = @RSpreviousProcessId,
				@nextStepId = '1021.015',
				@doNext = 0,
				@parentId = @caseId,
				@rootId = @caseId,
				@groupId = @groupProcessInstanceId,
				@variant = 1,
				@err = @err output,
				@message = @message output

				UPDATE process_instance SET active=0 WHERE step_id LIKE '1021.%' AND root_id=@caseId AND step_id<>'1021.015'
			END

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '129,800,368', @groupProcessInstanceId = @groupProcessInstanceId

			SELECT @partnerMail = value_string FROM @values
			--				SELECT @partnerMail = dbo.f_partner_contact(@groupProcessInstanceId,@groupProcessInstanceId,@partnerLocId,1,8)

			SET @messageMail = '<HTML>' +
													'<h4>Szanowni Państwo,</h4><br>' +
													dbo.f_translate('Informujemy o aktualizacji tymczasowego nr sprawy -',default) +
													CAST( isnull(dbo.f_caseId(@root_id),'') as nvarchar(10))
													+ dbo.f_translate('. Sprawa będzie od tego momentu identyfikowana nowym numerem -  ',default)
													+ cast(isnull(dbo.f_caseId(@caseId),'') as nvarchar(10)) +'<br>' +
													dbo.f_translate('Prosimy się posługiwać tym numerem sprawy przy kontaktach w sprawie wniosku/wystawianiu faktury.',default)
													+'<br></HTML>';

			SET @subjectMail = dbo.f_translate('Ford Pomoc - Starter24 - aktualizacja numeru sprawy - ',default) +	CAST( isnull(dbo.f_caseId(@root_id),'') as nvarchar(10))
			SET @senderMail = [dbo].[f_getEmail]('rs')

			EXEC p_note_new
					@groupProcessId =@groupProcessInstanceId,
					@type =dbo.f_translate('email',default),
					@content = dbo.f_translate('Wysłano informację o zmianie nr sprawy na adres email serwisu',default),
					@phoneNumber = NULL,
					@email = @partnerMail,
					@subject = @subjectMail,
					@direction = 1,
					@emailRegards = 2,
					@err = @err OUTPUT,
					@message = @message OUTPUT,
					-- FOR EMAIL
					@dw = '',
					@udw = '',
					@sender = @senderMail,
					@additionalAttachments = '',
					@emailBody  = @messageMail

			set @rootId = @caseId
		END
		ELSE
		BEGIN
			SET @variant = 99
		END
	END

	IF @newCase=1
	BEGIN
		declare @partnerId int

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerId = value_int FROM @values

		declare @partnerLocId int
		declare @partnerGroupProcessInstanceId int
		select	@partnerLocId=id,@partnerGroupProcessInstanceId=group_process_instance_id
		from	dbo.attribute_value av1
		where	parent_attribute_value_id=@partnerId and attribute_path='595,597,85'

		--	exec dbo.p_form_controls @instance_id=@caseId, @returnResults=0
		-- typ zdarzenia
		declare @attribute_id int

		declare kur cursor LOCAL for
			select attribute_id from dbo.attribute_attributes where parent_attribute_id=85
		OPEN kur;
		FETCH NEXT FROM kur INTO @attribute_id;
		WHILE @@FETCH_STATUS=0
			BEGIN
				declare @attributePathS nvarchar(50)
				declare @attributePathT nvarchar(50)
				declare @int int
				declare @string nvarchar(200)
				declare @datetime datetime
				declare @text nvarchar(500)
				declare @decimal decimal(18,6)

				set @int = null
				set @string = null
				set @datetime = null
				set @text = null
				set @decimal = null

				set @attributePathS='595,597,85,'+cast(@attribute_id as varchar(20))
				set @attributePathT='101,85,'+cast(@attribute_id as varchar(20))

				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = @attributePathS, @groupProcessInstanceId = @partnerGroupProcessInstanceId, @parentAttributeId=@partnerLocId
				SELECT	@int = value_int,
								@string = value_string,
								@datetime = value_date,
								@text = value_text,
								@decimal = value_decimal
				FROM	@values

				EXEC [dbo].[p_attribute_edit]
						@attributePath = @attributePathT,
						@stepId = 'xxx',
						@groupProcessInstanceId = @groupProcessInstanceId,
						@valueInt = @int,
						@valueDate = @datetime,
						@valueString = @string,
						@valueText = @text,
						@valueDecimal = @decimal,
						@err = @err,
						@message = @message

				FETCH NEXT FROM kur INTO @attribute_id;
			END
		CLOSE kur
		DEALLOCATE kur

		INSERT INTO process_instance_flow (previous_process_instance_id, next_process_instance_id, active)
		VALUES (@previousProcessId, @caseId, 1)
	END
	ELSE
	BEGIN
		----
		select top 1 @acceptStepId = id from dbo.process_instance with(nolock) WHERE group_process_id = @groupProcessInstanceId and step_id = '1021.030'

		IF @acceptStepId is not null
		BEGIN
			update dbo.process_instance set active = 999 where id = @acceptStepId
		END
	END

	declare @fixingEndDate datetime
	declare @estimated int

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '129,798', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @estimated = value_int FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '129,130', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingEndDate = value_date FROM @values

	EXEC [dbo].[p_attribute_edit]
			@attributePath = '286',
			@groupProcessInstanceId = @rootId,
			@stepId = 'xxx',
			@userId = 1,
			@originalUserId = 1,
			@valueDate = @fixingEndDate,
			@err = @err OUTPUT,
			@message = @message OUTPUT

	EXEC [dbo].[p_attribute_edit]
			@attributePath = '798',
			@groupProcessInstanceId = @rootId,
			@stepId = 'xxx',
			@userId = 1,
			@originalUserId = 1,
			@valueint = @estimated,
			@err = @err OUTPUT,
			@message = @message OUTPUT

	-- Weryfikacja czy istnieje więcej niż jedna matryca.
	IF (SELECT COUNT(1) FROM attribute_value WHERE root_process_instance_id=@caseId AND attribute_path='820') <> 1
	BEGIN
		DECLARE @matrixId INT
		DECLARE @delIndex BIT = 0
		DECLARE del820 CURSOR
		FOR SELECT id FROM attribute_value WHERE root_process_instance_id=@caseId AND attribute_path='820' ORDER BY id ASC
		OPEN del820
		FETCH NEXT FROM del820 INTO @matrixId
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @delIndex = 1 -- Pomija pierwszy ID
			BEGIN
				EXEC dbo.p_attribute_delete @attributeValueId=@matrixId, @force=1
			END
			SET @delIndex = 1
			FETCH NEXT FROM del820 INTO @matrixId
		END
		CLOSE del820;
		DEALLOCATE del820;
	END
end
