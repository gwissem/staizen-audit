ALTER PROCEDURE p_find_and_send_survey_link
AS
  BEGIN
    /**
    Procedura odpowiadająca za zbieranie odpowiedzi do ankiet, worker odpalany co 180 sekund. Szuka odpowiedzi na wysłane ankiety i wysyła link
     */

    DROP TABLE IF EXISTS #tempSurveysActive
    DROP TABLE IF EXISTS #tempSurveysSendResponse
    CREATE TABLE #tempSurveysActive (
      survey_id int,
      phone     nvarchar(20),
      rootId    nvarchar(20),

    )

    INSERT into #tempSurveysActive
    SELECT id, phone_number, rootId
    from survey
    where active = 1
      and state = 1;


    CREATE TABLE #tempSurveysSendResponse (
      survey_id   int,
      rootId      nvarchar(20),
      phoneNumber nvarchar(100),
      rating      int,
      sms_inbox_id      int
    )

    INSERT into #tempSurveysSendResponse
    SELECT survey_id, rootId, phone, sms_text, sms_inbox_id
    from (SELECT #tempSurveysActive.survey_id,
                 #tempSurveysActive.rootId,
                 phone,
                 case
                   when ISNUMERIC(sms_text) = 1
                           then cast(sms_text as int)
                   else null end                                                   as sms_text,
                 row_number() over (partition by phone order by sms_inbox.id desc) as rn,
                 sms_inbox.id as sms_inbox_id
          from sms_inbox with (nolock)
                 join #tempSurveysActive with (nolock)
                   on right(#tempSurveysActive.phone, 9) = right(sms_inbox.numer_telefonu, 9)
          where (select top 1 1 from survey with(nolock ) where survey.sms_inbox_id = sms_inbox.id  ) is null
         ) k
  where k.rn = 1


    DECLARE @survey int
    DECLARE @rootId int
    DECLARE @smsInboxId int
    DECLARE @phoneNumber nvarchar(10)
    DECLARE @rating int

    DECLARE @link nvarchar(150)
    DECLARE @err int
    DECLARE @msg nvarchar(100)

    DECLARE @responseText nvarchar(500)
    DECLARE db_cursor CURSOR FOR
      SELECT survey_id, rootId, phoneNumber, rating, sms_inbox_id FROM #tempSurveysSendResponse WHERE rating is not null

    OPEN db_cursor
    FETCH NEXT FROM db_cursor
    INTO @survey, @rootId, @phoneNumber, @rating, @smsInboxId

    WHILE @@FETCH_STATUS = 0
      BEGIN
        DECLARE @newToken VARCHAR(50)
        set @newToken = left( newid(),8)
        WHILE (SELECT id from survey where uid = @newToken) is not null
          BEGIN
            set @newToken =left( newid(),8)
          end
        --         GENERATING NEW TOKE
        UPDATE survey
        SET uid        = @newToken,
            updated_at = GETDATE(),
            state      = 2,
            rating     = @rating,
            sms_inbox_id = @smsInboxId
        where id = @survey

        SELECT @link = dbo.f_getDomain() + dbo.f_translate('/surveys/',default) + @newToken


        IF @rating <= 3
          BEGIN
            SET @responseText = 'Dziękujemy za odpowiedź. Kliknij w link i powiedz nam co poszło nie tak.
' + isnull(@link, '')
          end
        ELSE
          BEGIN
            SET @responseText = 'Dziękujemy za odpowiedź. Jeśli chcesz podzielić się z nami swoim komentarzem, kliknij w poniższy link.
' + isnull(@link, '')
          end

        EXEC p_note_new
            @groupProcessId = @rootId,
            @type = dbo.f_translate('sms',default),
            @content = @responseText,
            @phoneNumber = @phoneNumber,
            @direction = 1,
            @err = @err OUTPUT,
            @message = @msg OUTPUT

        FETCH NEXT FROM db_cursor
        INTO @survey, @rootId, @phoneNumber, @rating, @smsInboxId
      END

    CLOSE db_cursor
    DEALLOCATE db_cursor


  end