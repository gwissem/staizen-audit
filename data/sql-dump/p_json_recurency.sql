ALTER PROCEDURE [dbo].[p_json_recurency]
@json NVARCHAR(MAX), 
@parent NVARCHAR(1000)='',
@rootId INT,
@event nvarchar(255) = dbo.f_translate('create',default),
@groupProcessInstanceId INT OUTPUT,
@note nvarchar(max) = '' output
AS
BEGIN 
	SET NOCOUNT ON
	declare @jsonkey NVARCHAR(MAX)
	declare @jsonvalue nvarchar(max)
	declare @jsontype int
	declare @parent_temp nvarchar(1000)
	declare @attribute_path nvarchar(500)
	declare @serviceType nvarchar(255)
	declare @serviceId int 
	declare @serviceGroupId int 
	declare @status int 
	declare @numberofPeople int = 0
	declare @translated nvarchar(255)
	declare @doChange int
	declare @important int 
		
	SET @rootId = IIF(@rootId IS NULL, @groupProcessInstanceId, @rootId)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	set @note=isnull(@note,'')
	
	set @parent_temp=@parent
	
	declare kur cursor LOCAL for
		SELECT * FROM OPENJSON (@json)
	OPEN kur;
	FETCH NEXT FROM kur INTO @jsonkey, @jsonvalue, @jsontype;
	WHILE @@FETCH_STATUS=0
	BEGIN
		set @serviceGroupId = null
		set @serviceId = null
		set @status = null
		set @doChange = 1
		
		print @parent_temp
		print @jsontype
		if try_parse(@jsonkey as int) is null 
		begin
			set @parent_temp=@parent+'.'+@jsonkey
		end
		else
		begin
			set @parent_temp=@parent
		end

		IF (ISJSON(@jsonvalue) > 0)  
		begin
			exec dbo.p_json_recurency @json=@jsonvalue, @parent=@parent_temp, @rootId = @rootId, @event = @event, @groupProcessInstanceId=@groupProcessInstanceId output, @note=@note OUTPUT
		end
		else
		begin
			declare @valueX nvarchar(1000)
			declare @info nvarchar(1000)
			set @info=null
			set @valueX=null
			set @attribute_path = null
			set @numberofPeople = 0			
			
			select	@attribute_path=attribute_path,
					@info=[info],
					@valueX=@jsonvalue,
					@important = important
			from sparx_json_definition
			where '.'+json_key=@parent_temp

			-- jeśli nie jesteśmy w usługach to zmieniamy parametry w root
			IF @parent_temp NOT LIKE '.Services.%'
			BEGIN
				SET @groupProcessInstanceId = @rootId
			END 
			ELSE IF @parent_temp = '.Services.$type'
			BEGIN
				PRINT '---------------SERVICEEEEEEEEEEEEEEE'
				SELECT top 1 @serviceId = id from dbo.service_definition where sparx_service_name = @jsonvalue
				PRINT '---------------SERVICID'
				PRINT @serviceId
				IF @serviceId IS NOT NULL
				BEGIN
					SELECT @groupProcessInstanceId = [dbo].[f_service_top_progress_group](@rootId, @serviceId)		
					
					IF @event = dbo.f_translate('create',default) and @groupProcessInstanceId IS NULL 
					BEGIN
						SET @groupProcessInstanceId = @rootId	
					END
					
				END 
			END 
			
			if @parent_temp IN ('.Vehicle.NumberOfAdults','.Vehicle.NumberOfChildren')
			BEGIN				
				IF NOT EXISTS(select * from #sparxAttributeValueTempTable where attribute_path = '79') 
				BEGIN
					INSERT INTO #sparxAttributeValueTempTable (attribute_path, value_int)
					SELECT '79', 0
				END			
				SELECT top 1 @numberofPeople = value_int from #sparxAttributeValueTempTable where attribute_path = '79'
				SET @numberofPeople = ISNULL(@numberofPeople,0) + TRY_PARSE(@valueX AS INT)				
				UPDATE #sparxAttributeValueTempTable SET value_int = ISNULL(@numberofPeople,0) where attribute_path = '79'
						
			END 
			else if @attribute_path='80,342,408,197'
			begin
				set @valueX=replace(@valueX,'+','00')
			end
			

			declare @type int
			set @type=null

			select	@type=type
			from	dbo.attribute 
			where id=(
				select top 1 data from dbo.f_split (@attribute_path,',') order by id desc
			)

			
			declare @valueInt int
			declare @valueDate datetime
			declare @valueString nvarchar(200)
			declare @valueText nvarchar(500)
			declare @valueDecimal decimal(18,6)

			declare @oldValueInt int
			declare @oldValueDate datetime
			declare @oldValueString nvarchar(200)
			declare @oldValueText nvarchar(500)
			declare @oldValueDecimal decimal(18,6)
			
			set @valueInt=null
			set @valueDate=null
			set @valueString=null
			set @valueText=null
			set @valueDecimal=null

			set @oldValueInt=null
			set @oldValueDate=null
			set @oldValueString=null
			set @oldValueText=null
			set @oldValueDecimal=null
			
			if @type=1 -- 1 string
			begin
				
				exec dbo.p_translate_sparx
				@jsonPath = @parent_temp, 
				@value = @valueX,
				@groupProcessInstanceId = @groupProcessInstanceId,
				@translated = @valueString output,
				@doChange = @doChange output

				IF @event = dbo.f_translate('update',default)
				BEGIN					
					DELETE FROM @values
					INSERT @values EXEC dbo.p_attribute_get2 @attributePath = @attribute_path, @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @oldValueString = value_string FROM @values	
					
					IF @oldValueString <> @valueString AND @doChange = 1
					BEGIN						
						INSERT INTO #sparxAttributeValueChangeTempTable (name, old_value, new_value) 
						SELECT isnull(@info,RIGHT(@parent_temp,LEN(@parent_temp)-1)), ISNULL(@oldValueString,''), ISNULL(@valueString,'')
					END 
				END 
				 
			end
		
			if @type in (2,6) -- 2	integer -- 6	boolean
			begin				
				
				exec dbo.p_translate_sparx
				@jsonPath = @parent_temp, 
				@value = @valueX,
				@groupProcessInstanceId = @groupProcessInstanceId,
				@translated = @valueString output,
				@doChange = @doChange output
				
				set @valueInt=try_parse(@valueString as int)
				
				IF @event = dbo.f_translate('update',default)
				BEGIN					
					DELETE FROM @values
					INSERT @values EXEC dbo.p_attribute_get2 @attributePath = @attribute_path, @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @oldValueInt = value_int FROM @values	
					
					IF @oldValueInt <> @valueInt AND @doChange = 1
					BEGIN						
						INSERT INTO #sparxAttributeValueChangeTempTable (name, old_value, new_value) 
						SELECT isnull(@info,RIGHT(@parent_temp,LEN(@parent_temp)-1)), ISNULL(CAST(@oldValueInt AS NVARCHAR(255)),''), ISNULL(@valueX,'')
					END 					
				END 
				
			end
		
			if @type=3 -- 3	datetime
			begin
				set @valueDate=try_parse(@valueX as datetime)
				if @valueDate is not NULL
				BEGIN
					set @valueDate = CONVERT(datetime, SWITCHOFFSET(@valueDate, DATEPART(TZOFFSET, @valueDate AT TIME ZONE dbo.f_translate('Central Europe Standard Time',default))))	
				END

				IF @event = dbo.f_translate('update',default)
				BEGIN					
					DELETE FROM @values
					INSERT @values EXEC dbo.p_attribute_get2 @attributePath = @attribute_path, @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @oldValueDate = value_date FROM @values	
					
					IF @oldValueDate <> @valueDate
					BEGIN						
						INSERT INTO #sparxAttributeValueChangeTempTable (name, old_value, new_value) 
						SELECT isnull(@info,RIGHT(@parent_temp,LEN(@parent_temp)-1)), ISNULL(dbo.FN_VDate(@oldValueDate),''), ISNULL(@valueX,'')
					END 					
				END 				
			end
		
			if @type=4 -- 4	text
			begin
				set @valueText=@valueX
				
				IF @event = dbo.f_translate('update',default)
				BEGIN					
					DELETE FROM @values
					INSERT @values EXEC dbo.p_attribute_get2 @attributePath = @attribute_path, @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @oldValueText = value_text FROM @values	
					
					IF @oldValueText <> @valueText
					BEGIN						
						INSERT INTO #sparxAttributeValueChangeTempTable (name, old_value, new_value) 
						SELECT isnull(@info,RIGHT(@parent_temp,LEN(@parent_temp)-1)), ISNULL(@oldValueText,''), ISNULL(@valueX,'')
					END
				END
			end
		
			if @type=5 -- 5	decimal
			begin
				set @valueDecimal=try_parse(@valueX as decimal(18,6))
				
				IF @parent_temp IN ('.Vehicle.VehicleDetails.Dimensions.Length', '.Vehicle.VehicleDetails.Dimensions.Height', '.Vehicle.VehicleDetails.Dimensions.Width')
				BEGIN
					SET @valueDecimal = @valueDecimal / 1000
				END
				
				IF @event = dbo.f_translate('update',default)
				BEGIN					
					DELETE FROM @values
					INSERT @values EXEC dbo.p_attribute_get2 @attributePath = @attribute_path, @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @oldValueDecimal = value_decimal FROM @values	
					
					IF @oldValueDecimal <> @valueDecimal
					BEGIN						
						INSERT INTO #sparxAttributeValueChangeTempTable (name, old_value, new_value) 
						SELECT isnull(@info,RIGHT(@parent_temp,LEN(@parent_temp)-1)), ISNULL(CAST(@valueDecimal AS NVARCHAR(255)),''), ISNULL(@valueX,'')
					END
				END
			end
		
			declare @err int
			declare @message nvarchar(500)
			
			IF (@event = dbo.f_translate('create',default) AND @doChange = 1) OR (@event = dbo.f_translate('update',default) AND @parent_temp = dbo.f_translate('.Features.SmartView.Url',default))
			BEGIN
			

			
				EXECUTE dbo.p_attribute_edit 
				   @attributePath=@attribute_path
				  ,@groupProcessInstanceId=@groupProcessInstanceId
				  ,@stepId='xxx'
				  ,@userId=1
				  ,@valueInt=@valueInt
				  ,@valueDate=@valueDate
				  ,@valueString=@valueString
				  ,@valueText=@valueText
				  ,@valueDecimal=@valueDecimal
				  ,@err=@err OUTPUT
				  ,@message=@message OUTPUT
			
			END 
			
			if @info is not null
			begin
				IF @important = 1 
				and 
				NOT EXISTS (select id from dbo.process_instance where group_process_id = try_parse(REPLACE(@jsonvalue,'a','') as int)) 
				BEGIN
					if @parent_temp = '.Services.$type'
					begin
						set @note=isnull(@note,'')+'<hr/>'	
						set @jsonvalue = '<span class="font-blue-soft">'+@jsonvalue+'</span>'
					end
					else if @parent_temp = dbo.f_translate('.Services.Status',default)
					BEGIN
						if @jsonvalue = dbo.f_translate('CompletedSuccessfully',default)
						BEGIN
							set @jsonvalue = '<span class="font-green-meadow">'+@jsonvalue+'</span>'
						END 
						else if @jsonvalue = dbo.f_translate('CompletedUnsuccessfully',default)
						BEGIN
							set @jsonvalue = '<span class="text-danger">'+@jsonvalue+'</span>'
						END 
						ELSE
						BEGIN
							set @jsonvalue = '<span class="text-warning">'+@jsonvalue+'</span>'
						END 
					END 
					
					set @note=isnull(@note,'')+@info+': <b>'+isnull(@jsonvalue,'')+'</b><br/>'	
				END 

				if @parent_temp = 'Services.$type'
				BEGIN					
					truncate table #serviceTypeTempTable
					
					insert into #serviceTypeTempTable (name) 
					select @jsonvalue 										
				END

				if @parent_temp = dbo.f_translate('Services.Status',default)
				BEGIN
					
					select @serviceId = id from dbo.service_definition where sparx_service_name = 
					(
						select top 1 name from #serviceTypeTempTable
					)
					select @serviceGroupId = dbo.f_service_top_progress_group(@groupProcessInstanceId, @serviceId)
					select top 1 @status = id from dbo.service_status_dictionary where sparxStatus = @jsonvalue order by id desc 
					
--					if @serviceId is not null and @serviceGroupId is not null and @status is not null and dbo.f_abroad_case(@groupProcessInstanceId) = 0
--					begin 
--						exec [dbo].[p_add_service_status]
--						@groupProcessInstanceId = @serviceGroupId,
--						@status = @status,
--						@serviceId = @serviceId,
--						@sendSparx = 0
--					end
					
					
				END 
				
			end
		end

		FETCH NEXT FROM kur INTO @jsonkey, @jsonvalue, @jsontype;
	END
	CLOSE kur
	DEALLOCATE kur

	
	
END
