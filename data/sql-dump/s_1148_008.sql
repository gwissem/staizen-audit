ALTER PROCEDURE [dbo].[s_1148_008]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN

	--DECLARE @previousProcessId INT = 493674 
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT 
	DECLARE @stepId NVARCHAR(20)
	DECLARE @partnerId INT
	DECLARE @rootId INT
	DECLARE @parentId INT
	DECLARE @processInstanceId INT
	DECLARE @etaText NVARCHAR(255)
	DECLARE @eta datetime
	DECLARE @specialistTitle NVARCHAR(255)
	Declare @platformId  NVARCHAR(255)
	DECLARE @platformName NVARCHAR(255)
	DECLARE @officialLineNumber NVARCHAR(100)
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @servicePhone NVARCHAR(20)
	DECLARE @smsService nvarchar(500)

	SELECT @groupProcessInstanceId = p.group_process_id, 
	@rootId = p.root_id,
	@parentId = p.parent_id
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s with(nolock) on s.id = p.step_id
	WHERE p.id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
		
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '107',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
	DELETE FROM @values

	SELECT top 1 @specialistTitle = [dbo].[f_remove_pl](d.textD) FROM dbo.attribute_value av with(nolock)
	join dbo.dictionary d with(nolock) on d.value = av.value_int and d.typeD = 'homeAssistanceServices' and d.active = 1
	where av.attribute_path = '954'
	and av.root_process_instance_id = @rootId
	and av.value_int is not null
	order by av.id desc

	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '253',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	DELETE FROM @values
	
	SELECT top 1 @platformName = p.name from dbo.platform p with(nolock)
	where p.id =@platformId
		

	SELECT top 1 @officialLineNumber = p.official_line_number from dbo.platform p with(nolock)
	where p.id =@platformId

	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,408,197', @groupProcessInstanceId = @rootId
	SELECT @callerPhone = value_string FROM @values
	DELETE FROM @values

	DECLARE @cost  NVARCHAR (20)
	IF @platformId = 42
	BEGIN
		SET @cost = dbo.f_translate('400 zl brutto.',default)
	END
	ELSE
	BEGIN
		SET @cost = dbo.f_translate('450 zl brutto.',default)
	END
	------------Do klienta-------------------

	set @etaText = dbo.f_translate('Został zorganizowany specjalista: ',default) + ISNULL(@specialistTitle,'') + dbo.f_translate('. Prosimy oczekiwać na kontakt ze strony specjalisty.',default) + dbo.f_translate(' Limit kosztów wynosi ',default) + replace(@cost,'.',',') + dbo.f_translate(' koszty części pokrywa klient.',default)
--	IF @eta IS NOT NULL
	--BEGIN
	--	SET @etaText = @etaText+ dbo.f_translate(' Data umowionej wizyty ',default)  + dbo.FN_VDateHour(@eta)  + '.'-- Z powazaniem ' + ISNULL(@platformName,'') + dbo.f_translate('/Home Assistance. ',default) + ISNULL(@officialLineNumber,'')
	--END
	--ELSE
	--BEGIN
	--	SET @etaText = @etaText
	--END
	
	PRINT '@callerPhone@callerPhone@callerPhone'
	PRINT @callerPhone
	    	
	EXEC dbo.p_note_new
	@groupProcessId = @groupProcessInstanceId,
	@type = dbo.f_translate('sms',default),
	@content = @etaText,
	@phoneNumber = @callerPhone,
	@err = @err OUTPUT,
	@message = @message OUTPUT
		
-------------Do kontraktora --------------
	declare @serviceToLocation nvarchar(1000)
	DECLARE @Firstname NVARCHAR(100)
	DECLARE @Lastname NVARCHAR(100)
	DECLARE @clientName NVARCHAR(200)
	DECLARE @diagnosis nvarchar(100)
	DECLARE @diagnosisString nvarchar(100)



	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '417', @groupProcessInstanceId = @rootId
	SELECT @diagnosis = value_text FROM @values
	DELETE FROM @values

	set @diagnosisString = ' ,' + @diagnosis

	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,64', @groupProcessInstanceId = @rootId
	SELECT @Firstname = value_string FROM @values
	DELETE FROM @values

	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,66', @groupProcessInstanceId = @rootId
	SELECT @Lastname = value_string FROM @values
	DELETE FROM @values

	SET @clientName = ISNULL(@Firstname,'') + ' '+ ISNULL(@Lastname,'')

	set @serviceToLocation = dbo.f_location_string(@rootId)
	
	EXEC p_attribute_edit
		@attributePath = '967', 
		@groupProcessInstanceId = @rootId,
		@stepId = 'xxx',
		@valueText = @serviceToLocation,
		@err = @err OUTPUT,
		@message = @message OUTPUT

	delete from @values
	DECLARE @serviceLocationAdditionalInfo NVARCHAR(1000)
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,63', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @serviceLocationAdditionalInfo = value_text FROM @values
	delete from @values



	IF @eta IS NOT NULL
	BEGIN
		SET @smsService = dbo.f_translate('Tu Starter24. Zlecenie Home Assistance nr ',default) + ISNULL([dbo].[f_caseId](@rootId),'') +'. ' + ISNULL(replace([dbo].[f_diagnosis_description](@rootId,'pl'),'[Miejsce na opis] - [?].',''),'') +isnull(@diagnosisString,'') + dbo.f_translate(' Lokalizacja: ',default) + ISNULL(@serviceToLocation,'') +' '+ isnull(@serviceLocationAdditionalInfo,'')+  '. ' + ISNULL(@clientName,'') + ' ' + ISNULL(@callerPhone,'') +dbo.f_translate('. Data umowionej wizyty ',default)  + ISNULL(dbo.FN_VDateHour(@eta),'') + dbo.f_translate(dbo.f_translate('. Koszt czesci zawsze pokrywa Klient. Limit na wykonanie uslugi to: ',default),default) + ISNULL(@cost,'')
	END
	ELSE
	BEGIN
		SET @smsService = dbo.f_translate('Tu Starter24. Zlecenie Home Assistance nr ',default) + ISNULL([dbo].[f_caseId](@rootId),'') +'. ' + ISNULL(replace([dbo].[f_diagnosis_description](@rootId,'pl'),'[Miejsce na opis] - [?].',''),'')+isnull(@diagnosisString,'') + dbo.f_translate(' Lokalizacja: ',default) + ISNULL(@serviceToLocation,'') + ' '+ isnull(@serviceLocationAdditionalInfo,'')+ '. ' + ISNULL(@clientName,'') + ' ' + ISNULL(@callerPhone,'') +dbo.f_translate(dbo.f_translate('. Koszt czesci zawsze pokrywa Klient. Limit na wykonanie uslugi to: ',default),default) + ISNULL(@cost,'')
	END
	 
	INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	select @servicePhone=dbo.f_partner_contact(@groupProcessInstanceId,@partnerId,19,2)
	
	-- SET @smsService = [dbo].[f_remove_pl](@smsService)

	EXEC dbo.p_note_new
	@groupProcessId = @groupProcessInstanceId,
	@type = dbo.f_translate('sms',default),
	@content = @smsService,
	@phoneNumber = @servicePhone,
	@err = @err OUTPUT,
	@message = @message OUTPUT


	SET @variant = 1
END