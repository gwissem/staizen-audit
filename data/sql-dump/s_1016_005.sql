ALTER PROCEDURE [dbo].[s_1016_005]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err TINYINT
	DECLARE @message NVARCHAR(255)
	DECLARE @caseId NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	
	SELECT @groupProcessInstanceId = group_process_id FROM process_instance where id = @previousProcessId
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	declare @partnerEmail nvarchar(200)
	declare @body nvarchar(4000)
	declare @firstname nvarchar(100)
	declare @lastname nvarchar(100)
	declare @VIN nvarchar(100)
	declare @regNumber nvarchar(100)
		
	declare @partnerLocId int
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerLocId = value_int FROM @values

--	SELECT	@partnerEmail=avContactEmail.value_string
--	from dbo.attribute_value avLocation with(nolock) inner join
--			dbo.attribute_value avPartnerLocStatus with(nolock) on avPartnerLocStatus.parent_attribute_value_id=avLocation.id and avPartnerLocStatus.attribute_path='595,597,676' and avPartnerLocStatus.value_int=3 inner join
--			dbo.attribute_value avContact with(nolock) on avContact.parent_attribute_value_id=avLocation.id and avContact.attribute_path='595,597,642' inner join
--			dbo.attribute_value avContactType with(nolock) on avContactType.parent_attribute_value_id=avContact.id and avContactType.attribute_path='595,597,642,643' and avContactType.value_int=4 inner join
--			dbo.attribute_value avContactEmail with(nolock) on avContactEmail.parent_attribute_value_id=avContact.id and avContactEmail.attribute_path='595,597,642,656'  and avContactEmail.value_string is not null 
--	where avLocation.attribute_path='595,597' and avLocation.id=@partnerLocId
--		
	
	SELECT @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId,@partnerLocId, null, 4)
	
	declare @extCaseId nvarchar(100)
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '711', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @extCaseId = value_string FROM @values
		
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '73,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VIN = value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
	
	declare @persons int
		
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '152,154', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @persons = value_int FROM @values
		
	declare @address1 nvarchar(100)
	declare @address2 nvarchar(100)

	Select @address1=dbo.f_addressText(id) from dbo.attribute_value where group_process_instance_id=@groupProcessInstanceId and attribute_path='152,175,85'
	Select @address2=dbo.f_addressText(id) from dbo.attribute_value where group_process_instance_id=@groupProcessInstanceId and attribute_path='152,320,85'
	

	set @caseId=dbo.f_caseId(@groupProcessInstanceId)

	declare @subject nvarchar(100)

	set @subject=dbo.f_translate('Request for service - case nr: ',default)+isnull(@caseId,'')
	set @subject=''+isnull(@extCaseId,'')+' '+isnull(@caseId,'')
	set @body='Hello from Poland,<BR><BR>
				Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR>'+
				isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
				Please organize taxi for '+isnull(cast(@persons as varchar(10)),'')+' persons<BR>'+
			   dbo.f_translate('From: ',default)+isnull(@address1,'')+'<BR>'+
			   'To: '+isnull(@address2,'')+'<BR>
				<BR>
				Best regards,<BR>
				Starter24'

	declare @remarks nvarchar(4000)
	set @remarks=dbo.f_translate('Please organize taxi for ',default)+isnull(cast(@persons as varchar(10)),'')+' persons<BR>'+
			   dbo.f_translate('From: ',default)+isnull(@address1,'')+'<BR>'+
			   'To: '+isnull(@address2,'')+'<BR>'


	declare @userName nvarchar(100)
	select @userName=isnull(firstname,'') +' '+isnull(lastname,'') from dbo.fos_user where id=@currentUser

	-- Przekopiowanie atrybutu AssistanceOrganizationRequest
	EXEC [dbo].[p_copyAssistanceOrganizationRequest] @groupProcessInstanceId = @groupProcessInstanceId
	
	EXEC dbo.p_attribute_edit
		@attributePath = '838,840', -- konsultant 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valuestring = @userName,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	
	EXEC dbo.p_attribute_edit
		@attributePath = '838,839', -- id usługi
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueint = 5,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	

	EXEC dbo.p_attribute_edit
		@attributePath = '838,63', -- uwagi
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valuetext = @remarks,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
		DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')
	
		EXECUTE dbo.p_note_new 
		   @sender=@senderEmail
		  ,@groupProcessId=@groupProcessInstanceId
		  ,@type=dbo.f_translate('email',default)
		  ,@content=@body
		  ,@phoneNumber=null
		  ,@email=@partnerEmail
		  ,@userId=@currentUser
		  ,@subType=null
		  ,@attachment=null
		  ,@subject=@subject
		  ,@direction=1
		  ,@addInfo=null
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT
		  ,@emailBody=@body
		  ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'

	
	set @variant=1
	
	-- Skok do panelu usług
	EXEC dbo.p_jump_to_service_panel @previousProcessId = @previousProcessId, @variant = 1
	
end



