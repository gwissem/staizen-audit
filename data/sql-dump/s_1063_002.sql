ALTER PROCEDURE [dbo].[s_1063_002]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @ownedAssistance INT
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @getDealerText NVARCHAR(4000)
	DECLARE @rootId INT
	
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id FROM process_instance where id = @previousProcessId
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '519', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @ownedAssistance = value_int FROM @values
	
	SET @variant = 4
	
	SET @getDealerText = dbo.f_translate('Proszę o podanie partnera serwisowego, u którego został przeprowadzony ostatni przegląd wraz z usunięciem wykrytych usterek.',default)
	
	IF @ownedAssistance = 0
		SET @variant = 98
	ELSE IF @ownedAssistance = 1
	BEGIN
		SET @variant = 1
		SET @getDealerText = dbo.f_translate('Poproszę o wskazanie dealera, gdzie został zakupiony pojazd',default)
	END
	ELSE IF @ownedAssistance = 2
		SET @variant = 2
	ELSE IF @ownedAssistance = 3
		SET @variant = 3
	ELSE IF @ownedAssistance = 4
	BEGIN
		EXEC p_attribute_edit
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Uprzejmie proszę o kontakt z centrum zgłoszeniowym ubezpieczyciela. Numer kontaktowy powinien być zapisany na potwierdzeniu OC lub polisie. Ze swej strony możemy zaoferować pomoc odpłatną',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
	END 
	ELSE IF @ownedAssistance = 5
	BEGIN
		EXEC p_attribute_edit
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Uprzejmie proszę o kontakt z centrum zgłoszeniowym leasingodawcy. Jeśli nie posiada Pan / Pani numeru kontaktowego, proszę o kontakt z opiekunem firmowej floty.  Ze swej strony mogę zaoferować pomoc odpłatną.',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
	END 
	ELSE IF @ownedAssistance = 6
	BEGIN
		EXEC p_attribute_edit
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Nasze centrum zgłoszeniowe obsługuje pakiety assistance udzielane pojazdom kupionym i serwisowanym w POLSKIEJ autoryzowanej sieci partnerów marki. Jeśli pojazd został zakupiony lub jest serwisowany za granicą, należy zgłosić się do partner',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
	END 
	ELSE IF @ownedAssistance = 7
	BEGIN
		EXEC p_attribute_edit
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Wskazywanego przez Pana / Panią pakietu nie odnajduję na oficjalnej liście rodzajów assistance obsługiwanych przez nasze centrum zgłoszeniowe. W tej sytuacji mogę zaoferować pomoc odpłatną z możliwością późniejszego ubiegania się o z',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
	END
	ELSE IF @ownedAssistance = 9
	BEGIN
		SET @variant = 3
	END
	ELSE IF @ownedAssistance = 10
	BEGIN
		SET @variant = 1
	END
	
	EXEC p_attribute_edit
	@attributePath = '537', 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = @stepId,
	@valueText = @getDealerText,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	EXEC p_attribute_edit
	@attributePath = '573', 
	@groupProcessInstanceId = @rootId,
	@stepId = 'xxx',
	@valueInt = -1,
	@err = @err OUTPUT,
	@message = @message OUTPUT

	DECLARE @platformId int

	DELETE from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values



	IF @platformId = 6 AND isnull(@ownedAssistance ,0) = 10 or isnull(@ownedAssistance,0) =11
		BEGIN
			SET @variant = 96
		end

	IF isnull(@platformId,0) = 72
		BEGIN
			IF @variant <> 4  -- nie płatne
				BEGIN
					EXEC p_attribute_edit
							@attributePath = '202',
							@groupProcessInstanceId = @rootId,
							@stepId = 'xxx',
							@valueString = '505', -- husq
					@err = @err OUTPUT,
					@message = @message OUTPUT
				end
		end

END