ALTER PROCEDURE [dbo].[p_KTM_bwb_veryfcation_email]
  (
    @previousProcessId INT,
    @variant TINYINT OUTPUT,
    @currentUser int = 1,
    @errId int=0 output
  )
AS
  BEGIN

    DECLARE @err INT
    DECLARE @message VARCHAR(400)
    DECLARE @groupProcessInstanceId INT
    DECLARE @rootId INT
    DECLARE @stepId VARCHAR(32)
    DECLARE @body NVARCHAR(MAX)
    DECLARE @mailSubject NVARCHAR(255)


    DECLARE @mailTo NVARCHAR(100)
    DECLARE @validFrom DATETIME
    DECLARE @validTo DATETIME
    DECLARE @vin NVARCHAR(30)
    DECLARE @case NVARCHAR(30)
    DECLARE @makeModel NVARCHAR(255)
    DECLARE @registered DATETIME
    DECLARE @licenceplate NVARCHAR(30)
    DECLARE @countryOfSale NVARCHAR(50)

    SELECT
           @groupProcessInstanceId = group_process_id,
           @stepId = step_id,
           @rootId = root_id
    FROM process_instance  WITH(NOLOCK)
    WHERE id = @previousProcessId

    EXEC p_form_controls @instance_id = @previousProcessId, @returnResults = 0

    DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @rootId
    SELECT @vin = value_string FROM @values
    DELETE FROM @values


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @rootId
    SELECT @licenceplate = value_string FROM @values
    DELETE FROM @values


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @rootId
    SELECT @makeModel = isnull(d.textD,'') FROM @values
                                                  join dbo.dictionary d with(nolock) on d.value = value_int and d.typeD = 'makemodel' and d.active = 1
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,233', @groupProcessInstanceId = @rootId
    SELECT @registered = value_date FROM @values
    DELETE FROM @values


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,270', @groupProcessInstanceId = @rootId
    SELECT @countryOfSale = value_text FROM @values
    DELETE FROM @values


    declare @registered_string nvarchar(20)
    set @registered_string = try_convert(nvarchar(10),@registered,120)

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '981,67', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @validFrom = value_date FROM @values
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '981,68', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @validTo = value_date FROM @values
    DELETE FROM @values

    declare @validFrom_string nvarchar(30)
    declare @validTo_string nvarchar(30)

    set @validFrom_string = try_convert(nvarchar(10),@validFrom,120)
    set @validTo_string = try_convert(nvarchar(10),@validTo,120)

    SET @case = isnull([dbo].[f_caseId](@rootId),'')

    DECLARE @value NVARCHAR(255)
    DECLARE @description NVARCHAR(255)
    EXEC dbo.p_get_business_config
        @key = 'bwb.request_veryfication_email',
        @groupProcessInstanceId =@groupProcessInstanceId,
        @value = @value OUTPUT,
        @description = @description OUTPUT

    set @mailTo = '' --'patryk.kapron@tuz.pl'
    IF @value <> ''
      BEGIN
        SET @mailTo= dbo.f_getRealEmailOrTest(@value)
      END

    SET @mailSubject = dbo.f_translate('KTM / Husqvarna VIN not found, ',default) + isnull(@vin,'') + dbo.f_translate(' Starter24 case number: ',default) +@case


    SET @body = dbo.f_translate('Hello,',default)
                +'</br></br>'
                +'We would like to report VIN missing in the database of entitled KTM / Husqvarna motorbikes. Below you will find details:'
                +'</br>Make and model: ' + @makeModel
                +'</br>Registered: ' + isnull(@registered_string,'')
                +'</br>Last maintenence (if applicable): ' + isnull(@validFrom_string,'')
                +'</br>Licence plate: ' + isnull(@licenceplate,'')
                +'</br>Country of sale: ' + isnull(@countryOfSale,'')
                +'</br>VIN: ' + isnull(@vin,'')
                +'</br></br></br>'
                +dbo.f_translate('Kind regards,',default)
                +'</br>KTM / Husqvarna Assistance team in Starter24 (PL)'
                +'</br>ul. Józefa Kraszewskiego 30, 60-519 Poznań'
                +'</br>The information in this email is confidential and may be legally privileged. It is intended solely for the addressee. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited and may be unlawful. If you received this email as the unintended recipient, please inform the sender and delete this message.'
                +'</br></br>'

    DECLARE @sendMail nvarchar(100)
    SET @sendMail = dbo.f_getRealEmailOrTest ('callcenter@starter24.pl')

    EXECUTE dbo.p_note_new
        @groupProcessId = @groupProcessInstanceId
        ,@type = dbo.f_translate('email',default)
        ,@content = dbo.f_translate('KTM',default)
        ,@email = @mailTo
        ,@userId = 1  -- automat
        ,@subject = @mailSubject
        ,@direction=1
        ,@dw = ''
        ,@udw = ''
        ,@sender = @sendMail
        ,@emailBody = @body
        ,@err=@err OUTPUT
        ,@message=@message OUTPUT

  END