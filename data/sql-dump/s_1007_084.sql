ALTER PROCEDURE [dbo].[s_1007_084]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @iteration int
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @newProcessInstanceId INT
	DECLARE @groupProcessInstanceId INT 	
	DECLARE @postponeCount INT
	DECLARE @partnerId INT
	DECLARE @rootId INT
	DECLARE @dateEnd datetime
	DECLARE @postponeInterval INT 
	DECLARE @content nvarchar(max)
	DECLARE @token nvarchar(50)
	DECLARE @email nvarchar(255)
	DECLARE @subject nvarchar(1000)
	declare @newGroupId int
	declare @starterEmail nvarchar(255)
	declare @phone nvarchar(255)
	declare @winnerGroupId int
	declare @processInstanceId int 
	declare @platformGroup nvarchar(255)
	declare @rentalsCount int 
	declare @doNothing int = 0
	declare @createdAt datetime 
	declare @limit int 
	declare @scheduled datetime 
	declare @makeModel nvarchar(255)
	declare @starterClass nvarchar(255)
	declare @startLocation nvarchar(1000)
	declare @endLocation nvarchar(1000)
	declare @makeModelClassId int 
	declare @gearbox nvarchar(255)
	declare @engineType nvarchar(255)
	declare @comment nvarchar(2000)
	declare @isofix int
	declare @childSeat int
	declare @abroad nvarchar(255)
	declare @abroadText nvarchar(1000)
	declare @features nvarchar(255)
	declare @featuresText nvarchar(255)
	
	set @starterEmail = dbo.f_getEmail('rentals')
	
	-- zablokowanie instancji na czas jej wykonywania
	UPDATE dbo.process_instance SET user_id = 1 WHERE id = @previousProcessId
	
	-- 5 min czasu na odp.
	SET @postponeInterval = 360
	
	SELECT @groupProcessInstanceId = p.group_process_id,
	@postponeCount = isnull(p.postpone_count,0),
	@createdAt = p.created_at,
	@rootId = p.root_id
	FROM dbo.process_instance p with(nolock)
	WHERE p.id = @previousProcessId 
	
	EXEC [dbo].[p_rental_offer_max_class]
	@groupProcessInstanceId = @groupProcessInstanceId,
	@class = @starterClass OUTPUT

	SET @dateEnd = dateadd(second, @postponeInterval, getdate())
	SET @variant = 99
	set @limit = IIF(@postponeCount = 0,3,8)
	set @iteration = IIF(@postponeCount - 1 <= 0, 0, 1)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	exec dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output
	exec dbo.p_winner_rental_offer @groupProcessInstanceId = @groupProcessInstanceId, @iteration = @iteration, @winnerGroupId = @winnerGroupId output
	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		SET @starterEmail = dbo.f_getEmail('cfm')
	END

	IF @winnerGroupId is not null 
	BEGIN		
		exec dbo.p_auto_rental_offers_transfer @groupProcessInstanceId = @groupProcessInstanceId, @winnerGroupId = @winnerGroupId
		update dbo.process_instance set user_id = null, active = 0 where id = @previousProcessId
		update dbo.process_instance set active = 0 where step_id = '1007.085' and active = 1 and parent_id = @groupProcessInstanceId
		return
	END 
	
	IF @postponeCount > 1 
	BEGIN
		exec dbo.p_auto_rental_offers_transfer @groupProcessInstanceId = @groupProcessInstanceId, @winnerGroupId = @winnerGroupId
		SET @variant = 1
		update dbo.process_instance set user_id = null where id = @previousProcessId
		return
	END 
	
	CREATE TABLE #resultTable (priority int, partnerId int, partnerName nvarchar(1000), distance decimal(10,2), location nvarchar(1000), reasonForRefusing nvarchar(1000), partnerServiceId int, phone nvarchar(100), active int, offer nvarchar(1000))

	EXEC [dbo].[p_find_rentals2]
	@groupProcessInstanceId = @groupProcessInstanceId,
	@resultType = 1,
	@limit = @limit

	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		DELETE FROM #resultTable where priority = 99  
	END 
	
	IF @postponeCount = 1
	BEGIN				
		DELETE TOP (3) FROM #resultTable 	
		update dbo.attribute_value set value_date = @dateEnd where root_process_instance_id = @rootId and attribute_path = '764,68' and value_date is not null
	END 
	
	SELECT @rentalsCount = count(*) from #resultTable 
	
	IF @rentalsCount = 0
	BEGIN
		SET @variant = 1
		DROP TABLE #resultTable
		return
	END 

	IF @variant = 99
	BEGIN
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @makeModel = textD, @makeModelClassId = argument3 FROM dbo.dictionary WHERE typeD = 'makeModel' AND value = (SELECT value_int FROM @values)
--	    SELECT @starterClass = textD FROM dbo.dictionary where value = @makeModelClassId AND typeD = 'makeModelClass'
		
		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,285', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @scheduled = value_date FROM @values
		
		EXEC p_location_string
		@attributePath = '283,110,85',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@locationString = @startLocation OUTPUT
		
		EXEC p_location_string
		@attributePath = '283,111,85',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@locationString = @endLocation OUTPUT
		
		
		declare kur cursor LOCAL for	 
		 	SELECT partnerId, phone FROM #resultTable  
		 OPEN kur;
		 FETCH NEXT FROM kur INTO @partnerId, @phone;
		 WHILE @@FETCH_STATUS=0
		 BEGIN
			set @newGroupId = null
			SET @content = ''
			set @token = ''
			SET @subject = ''
			
			SET @token = newid()
			
			-- nowy proces 
			EXEC dbo.p_process_new @stepId = '1007.085', @userId = 1, @originalUserId = 1, @err = @err, @message = @message, @parentProcessId = @groupProcessInstanceId, @rootId = @rootId, @processInstanceId = @newGroupId OUTPUT
			
			UPDATE dbo.process_instance set token = @token where id = @newGroupId
			declare @inserted int
			
			INSERT INTO dbo.attribute_value (root_process_instance_id, group_process_instance_id, attribute_path, created_at, updated_at, created_by_original_id, created_by_id, attribute_id)
			SELECT @rootId, @newGroupId, '764', getdate(), getdate(), 1, 1, 764
			set @inserted = scope_identity()
			
			exec [dbo].[P_attribute_refresh]
			@attribute_value_id = @inserted
			
--			EXEC [dbo].[p_form_controls]
--				@instance_id = @newGroupId,
--				@returnResults = 0,
--				@returnWithNote = 0
			
			EXEC [dbo].[p_attribute_edit]
			     @attributePath = '764,742', 
			     @groupProcessInstanceId = @newGroupId,
			     @stepId = 'xxx',
			     @userId = 1,
			     @originalUserId = 1,
			     @valueInt = @partnerId,
			     @err = @err OUTPUT,
			     @message = @message OUTPUT
			
		     EXEC [dbo].[p_attribute_edit]
			     @attributePath = '764,68', 
			     @groupProcessInstanceId = @newGroupId,
			     @stepId = 'xxx',
			     @userId = 1,
			     @originalUserId = 1,
			     @valueDate = @dateEnd,
			     @err = @err OUTPUT,
			     @message = @message OUTPUT
			     
		     EXEC [dbo].[p_attribute_edit]
			     @attributePath = '691', 
			     @groupProcessInstanceId = @newGroupId,
			     @stepId = 'xxx',
			     @userId = 1,
			     @originalUserId = 1,
			     @valueDate = @dateEnd,
			     @err = @err OUTPUT,
			     @message = @message OUTPUT
			     
	     	
			-- wysłanie maila 	
		 	SELECT @email = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 3, 4)		
		 	SELECT @subject = dbo.f_translate('Prośba o przesłanie oferty wynajmu pojazdu zastępczego dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
		    SELECT @content = 'Szanowni Państwo,<br>'+
		    				  'W związku z dodatkowymi usługami assistance poszukujemy dla naszego Klienta pojazdu zastępczego za: <b>'+@makeModel+'</b><br/>'+
		    				  '<ul>'+
		    				  '<li>'+IIF(@scheduled is not null,dbo.f_translate('Na termin: ',default)+dbo.FN_VDateHour(@scheduled),dbo.f_translate('Jak najszybciej',default))+'</li>'+
		    				  '<li>Podstawienie do: '+ISNULL(@startLocation,'')+'</li>'+
			   				  IIF(@endLocation is not null,'<li>Odbiór od klienta z: '+ISNULL(@endLocation,'')+'</li>','')+
		    				  '<li>Klasa pojazdu wg Starter: '+ISNULL(@starterClass,'')+'</li>'
			
			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,431', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @gearBox = textD FROM dbo.dictionary WHERE value = (SELECT value_int FROM @values) AND typeD = 'gearbox'
		
			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,120', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @engineType = textD FROM dbo.dictionary WHERE value = (SELECT value_int FROM @values) AND typeD = 'engineType'
			
			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,121', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @isofix = value_int FROM @values
			
			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,284', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @childSeat = value_int FROM @values
			
			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '577', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @abroad = value_string FROM @values
			
			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,785', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @features = value_string FROM @values
			
			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '283,63', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @comment = value_text FROM @values
	
			IF ISNULL(@gearBox,'---')<>'---' OR ISNULL(@engineType,'---') <> '---' OR ISNULL(@isofix,0)<>0 OR ISNULL(@childSeat,0)<>0 OR ISNULL(@comment,'')<>''
			BEGIN			
				
				IF ISNULL(@gearBox,'---')<>'---' 
				BEGIN
					SET @content = @content + '<li>Skrzynia biegów: '+@gearBox+'</li>'	
				END
				
				IF ISNULL(@engineType,'---') <> '---' 
				BEGIN
					SET @content = @content + '<li>Rodzaj silnika: '+@engineType+'</li>'
				END 
				
				IF ISNULL(@features,'')<>''
				BEGIN
					SELECT @featuresText = dbo.concatenate(textD)
					from (
						select	textD 
						FROM	dbo.dictionary
						where	typeD = 'vehicleFeatures'
						and dbo.f_exists_in_split(@features,value) = 1  
				   	) a
				   	
					SET @content = @content + '<li>Cechy szczególne: '+ISNULL(@featuresText,'')+'</li>'
				END
				
				IF ISNULL(@isofix,0)<>0 
				BEGIN
					SET @content = @content + '<li>Isofix</li>'
				END 
				
				IF ISNULL(@childSeat,0)<>0
				BEGIN
					SET @content = @content + '<li>Fotelik dziecięcy</li>'
				END 
				
				IF ISNULL(@abroad,'')<>''
				BEGIN
					SELECT @abroadText = dbo.concatenate(textD)
					from (
						select	textD 
						FROM	dbo.dictionary
						where	typeD = 'europeanCountry'
						and dbo.f_exists_in_split(@abroad,value) = 1  
				   	) a
				   	
					SET @content = @content + '<li>Wyjazd pojazdem zastępczym za granicę ('+ISNULL(@abroadText,'')+')</li>'
				END
				
				IF ISNULL(@comment,'')<>''
				BEGIN
					SET @content = @content + '<li>'+@comment+'</li>'
				END

			END
				
			
		    SELECT @content = @content + '</ul>'+
		    				  'Prosimy o wejście w poniższy link i uzupełnienie oferty wynajmu:<br/>'+
							  '<b>Aby otworzyć link kliknij <a target="_new" href="'+dbo.f_getDomain()+dbo.f_translate('/operational-dashboard/public/',default)+@token+'">tutaj</a>.</b><br/> 
							   Formularz będzie aktywny do '+dbo.FN_VDateHour(@dateEnd)+'.' 		 	
		 	
		    EXECUTE dbo.p_note_new 
			 @groupProcessId = @groupProcessInstanceId
			,@type = dbo.f_translate('email',default)
			,@content = @content
			,@email = @email
			,@userId = 1  -- automat
			,@subject = @subject
			,@direction=1
			,@dw = ''
			,@udw = ''
			,@sender = @starterEmail
			,@emailBody = @content
			,@emailRegards = 1
			,@err=@err OUTPUT
			,@message=@message OUTPUT
		    		
		 	-- dialer
			
			INSERT INTO dbo.phone_call_queue (process_instance_id, record_number, phone_number, active, created_at )
			SELECT @previousProcessId, 6, @phone, 1, GETDATE()
		 	
		 FETCH NEXT FROM kur INTO @partnerId, @phone;
		 END
		 CLOSE kur
		 DEALLOCATE kur
	END
	
	UPDATE dbo.process_instance SET user_id = NULL, postpone_count = postpone_count + 1, postpone_date = @dateEnd, updated_at = GETDATE() WHERE id = @previousProcessId
	DROP TABLE #resultTable
	
END




