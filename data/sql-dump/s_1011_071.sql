ALTER PROCEDURE [dbo].[s_1011_071]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	declare @tips nvarchar(max)
	declare @firstRegDate datetime 
	DECLARE @forceDamageHandling INT = 0 -- czy wymusić przyjęcie szkody 
	DECLARE @damageHandlingPermission INT
	
	SELECT	@groupProcessInstanceId = group_process_id	
	FROM process_instance
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	declare @noteId int 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '994', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @noteId = id, @tips = value_text FROM @values
	
	IF isnull(@tips,'') <> '' AND NOT EXISTS (
		SELECT id from dbo.attribute_value where group_process_instance_id = @groupProcessInstanceId and value_text = @tips and id <> @noteId
	)
	BEGIN
		EXEC [dbo].[p_note_new]
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('text',default),
		@content = @tips,
		@userId = @currentUser,
		@originalUserId = @currentUser,
		@special = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	END 
	
	IF @variant <> 3
	BEGIN
		
		EXEC [dbo].[s_1011_002]
		@previousProcessId = @previousProcessId,
	    @variant = @variant OUTPUT, 
	    @currentUser = @currentUser, @errId = @errId output
	 	
	   	DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1074', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @forceDamageHandling = value_int FROM @values
	    
	    -- Wymuszenie, że MUSIMY przyjać szkodę, nie ważne, czy jest w naszej bazie czy ma do tego uprawnienia 
	    IF ISNULL(@forceDamageHandling, 0) = 1
	    BEGIN
		    
		    IF @variant > 1
			BEGIN
				SET @variant = 4
			END
			
			-- Ustalenie uprawnień
			
--		    DELETE FROM @values
--	      	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '143', @groupProcessInstanceId = @groupProcessInstanceId
--	      	SELECT @damageHandlingPermission = value_int FROM @values
--	      	
--			IF ISNULL(@damageHandlingPermission, 0) IN (0,4)
--	      	BEGIN
--		     	SET @damageHandlingPermission = 2 -- 'Upraweniania Obsługi Szkody
--	      	END
--	      	ELSE IF ISNULL(@damageHandlingPermission, 0) = 1
--	     	BEGIN
--		     	SET @damageHandlingPermission = 3 -- dbo.f_translate('Upraweniania Assistance i Obsługi Szkody',default) 
--	     	END
--	     	
--			EXEC p_attribute_edit
--    			@attributePath = '143',
--    			@groupProcessInstanceId = @groupProcessInstanceId,
--    			@stepId = 'xxx',
--    			@userId = @currentUser,
--    			@originalUserId = @currentUser,
--    			@valueInt = @damageHandlingPermission,
--    			@err = @err OUTPUT,
--    			@message = @message OUTPUT
		    
	    END
	    ELSE
	    BEGIN
		    
		    SET @variant = IIF(@variant > 2, 1, @variant)
		    
	    END
	
	    DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,561', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @firstRegDate = value_date FROM @values
	    
		IF @firstRegDate IS NOT NULL
		BEGIN
			EXEC [dbo].[p_attribute_edit]
		    @attributePath = '74,233', 
		    @groupProcessInstanceId = @groupProcessInstanceId,
		    @stepId = 'xxx',
		    @userId = 1,
		    @originalUserId = 1,
		    @valueDate = @firstRegDate,
		    @err = @err OUTPUT,
		    @message = @message OUTPUT
		END 
		
	END
	
END



