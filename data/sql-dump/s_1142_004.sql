ALTER PROCEDURE [dbo].[s_1142_004]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT,
    @currentUser INT, 
    @errId INT = 0 OUTPUT
) 
AS
BEGIN
	
	-- ==================================== --
	-- Wysłanie GOP i SMS
	-- ==================================== --

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	
	DECLARE @phoneNummberPartner NVARCHAR (30)
	DECLARE @phoneNummberClient NVARCHAR (30)
	DECLARE @costString VARCHAR(100)
	DECLARE @cost DECIMAL(10,2)
	DECLARE @typeCost VARCHAR(10)
	DECLARE @currency VARCHAR(10)
	DECLARE @days INT
	DECLARE @caseNumber NVARCHAR(30)
	DECLARE @startParking DATETIME
	DECLARE @endParking DATETIME
	DECLARE @makeModel NVARCHAR(255)
 	DECLARE @regNumber NVARCHAR(12)
 	DECLARE @partnerId INT 
 	DECLARE @isPostFactum INT 
 	
 	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, 
			@stepId = step_id,
			@rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	-- Stworzenie pomocniczej tabelki (attrGet)
	DECLARE @Values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	-- ======= USTAWIANIE DANYCH ===========
	
 	SET @caseNumber = [dbo].[f_caseId](@previousProcessId)
 	
 	-- location partner id
	INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
 	-- numer partnera
 	DELETE FROM @Values
 	INSERT  @values EXEC dbo.p_attribute_get2
  		@attributePath = '862,864,197',
  		@groupProcessInstanceId = @groupProcessInstanceId
  	SELECT @phoneNummberPartner = value_string FROM @values
 	
 	-- Data Start parkowania
 	DELETE FROM @Values
 	INSERT  @values EXEC dbo.p_attribute_get2
  		@attributePath = '862,823',
  		@groupProcessInstanceId = @groupProcessInstanceId
  	SELECT @startParking = value_date FROM @values
 	
  	-- Data End parkowania
	INSERT  @values EXEC dbo.p_attribute_get2
 		@attributePath = '862,853',
 		@groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @endParking = value_date FROM @values
 
	-- Pobranie kosztów parkingu
	SELECT @cost = cost, @typeCost = type_cost, @currency = currency, @days = days FROM  [dbo].[f_getCostParking] (@groupProcessInstanceId)
	
	SET @costString = CAST(ISNULL(@cost, '-') as VARCHAR(100)) + ' ' + ISNULL(@currency, '-') + ' (' + ISNULL(@typeCost, '-') + ')'
	
 	DELETE FROM @Values
	INSERT @Values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @Values
	
 	DELETE FROM @Values
	INSERT @Values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT  @makeModel = textD FROM dbo.dictionary WHERE value IN (SELECT value_int FROM @Values) AND typeD = 'makeModel'
	
	-- ustawienie dni składowania na parkingu
	EXEC p_attribute_edit
 			@attributePath = '862,138',
 			@groupProcessInstanceId = @groupProcessInstanceId,
 			@stepId = 'xxx',
 			@valueInt = @days,
 			@err = @err OUTPUT,
 			@message = @message OUTPUT
	
	
 			
	DELETE FROM @Values
	INSERT  @values EXEC dbo.p_attribute_get2
	@attributePath = '859',
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isPostFactum = value_int FROM @values
	
  
	-- JEŻELI POST_FACTUM TO NIE WYSYŁAJ SMSÓW
	IF ISNULL(@isPostFactum, 0) = 0
	BEGIN
  		
  		-- ========== WYSYŁANIE SMSa DO KONTRAKTORA =======================
	
	 	DECLARE @smsMessage VARCHAR(MAX)
		SET @smsMessage = ISNULL(@caseNumber, '') + '[' + ISNULL( @makeModel, '') + ' ' + ISNULL( @regNumber, '') + ']. '
		SET @smsMessage = @smsMessage + dbo.f_translate('Parking od ',default)+dbo.FN_VDateHour(@startParking)+' do '+dbo.FN_VDateHour(@endParking)+dbo.f_translate('. Kwota z GOP: ',default)+ ISNULL(@costString, '-') +dbo.f_translate('. Z poważaniem, Starter24',default)
		
		EXEC p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @smsMessage,
			@phoneNumber = @phoneNummberPartner,
			@userId = @currentUser,
			@originalUserId = @currentUser,
			@addInfo = 0,
			@err = @err OUTPUT,
			@message = @message OUTPUT
					
			
		-- ========== WYSYŁANIE SMSa DO KLIENTA =======================
			
		SET @smsMessage = ''	
		SET @smsMessage = '[' + ISNULL( @makeModel, '') + ' ' + ISNULL( @regNumber, '') + '] pojazdu został zdeponowany na parkingu ' + ISNULL([dbo].[f_partnerAdress](@partnerId), '')
	
		DELETE FROM @Values
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	    SELECT @phoneNummberClient = value_string FROM @values
			    
		EXEC p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @smsMessage,
			@phoneNumber = @phoneNummberClient,
			@userId = @currentUser,
			@originalUserId = @currentUser,
			@addInfo = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
  		
	END
 

	-- ============== WYSYŁANIE GOP'a DO KONTRAKTORA ==================
	
	EXEC [dbo].[p_send_gop]  @groupProcessInstanceId = @groupProcessInstanceId
	
	
	IF ISNULL(@isPostFactum, 0) = 1
	BEGIN
		
		SET @variant = 2
		
	END
	
	-- SKOK DO PANELU ŚWIADCZEŃ
	EXEC [dbo].[p_jump_to_service_panel] @previousProcessId = @previousProcessId, @variant = @variant
	
END



