ALTER PROCEDURE [dbo].[s_1063_011]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS

DECLARE @serviceDate DATETIME
DECLARE @serviceDateTo DATETIME
DECLARE @serviceDateChoice INT
DECLARE @howOftenService INT
DECLARE @nextServiceDate DATETIME
DECLARE @createdAt DATETIME
DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
DECLARE @groupProcessInstanceId INT
DECLARE @stepId NVARCHAR(200)
DECLARE @err INT
DECLARE @message NVARCHAR(255)
SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @createdAt = created_at FROM process_instance where id = @previousProcessId

INSERT @values EXEC p_attribute_get2 @attributePath = '74,105', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @serviceDate = value_date FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '525', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @serviceDateTo = value_date FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '531', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @serviceDateChoice = value_int FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '531', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @howOftenService = value_int FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '476', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @nextServiceDate = value_date FROM @values

SET @variant = 1

IF ISNULL(@howOftenService,3) < 2 OR ISNULL(@nextServiceDate,@createdAt) < @createdAt
BEGIN 
	SET @variant = 2
	
	EXEC p_attribute_set2
		@attributePath = '532', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueText = dbo.f_translate('Uprawnienie assistance obowiązuje do określonej daty, która najprawdopodobniej minęła. Wobec tego mogę zaoferować pomoc odpłatną z możliwością ubiegania się o zwrot kosztów w późniejszym terminie.',default),
		@err = @err OUTPUT,
		@message = @err OUTPUT
END
