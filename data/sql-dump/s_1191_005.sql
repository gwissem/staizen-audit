ALTER PROCEDURE [dbo].[s_1191_005]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT,
    @currentUser INT, 
    @errId INT = 0 OUTPUT
) 
AS
BEGIN
	
	-- ==================================== --
	-- < •   WYSYŁANIE E-MAILA do Leasingodawcy		• > --
	-- ==================================== --

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @platformId INT
	declare @regNumber nvarchar(200)
	declare @vinNumber nvarchar(200)
	DECLARE @caseNumber NVARCHAR(100)
	DECLARE @mapAttributeValue INT
	DECLARE @platformName NVARCHAR(100)
	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, 
			@stepId = step_id,
			@rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	-- Stworzenie pomocniczej tabelki (attrGet)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
 
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vinNumber = value_string FROM @values
           
	SELECT @platformName = name FROM dbo.platform WHERE id = @platformId 			

	SET @caseNumber = ISNULL(dbo.f_caseId(@rootId),'')
	
	 /*		Wysłanie e-maila
  	____________________________________*/
	
	DECLARE @body VARCHAR(MAX)
	DECLARE @subject VARCHAR(400)
	DEClARE @email NVARCHAR(255)
	DECLARE @sender NVARCHAR(255)
	
	SET @sender = dbo.f_getEmail('cfm@starter24.pl');

	IF ISNULL(@platformId, 0) = 53 -- Alphabet
	BEGIN
		SET @email = dbo.f_getRealEmailOrTest('serwis@alphabet.pl');
	END
	ELSE
	BEGIN
		SET @email = 'PLS CONFIG THIS EMAIL IN [dbo].[s_1191_005]';
	END
	
	SET @body = ''					
	 
	IF @regNumber IS NULL
	BEGIN
		SET @regNumber = '---'
	END
	
	IF @vinNumber IS NULL
	BEGIN
		SET @vinNumber = '---'
	END
	
	SET @subject = dbo.f_translate('Zgoda na usługę DoorToDoor. ',default) + @caseNumber + dbo.f_translate(', nr rej. ',default) + @regNumber + dbo.f_translate(', nr VIN ',default) +  @vinNumber
						

	/*	Pobieranie template e-maila
 	____________________________________*/

	EXEC [dbo].[P_get_email_template] @name = 'modern_email?lang_footer=pl', @responseText = @body OUTPUT
	
	SET @body = REPLACE(@body,'__CONTENT_TITLE__', 'Zgoda na usługę DoorToDoor')
	SET @body = REPLACE(@body,'___TITLE___','Zgoda na usługę DoorToDoor')    
	

	/*	Parsowanie contentu	body e-maila
 	____________________________________*/
	
	DECLARE @simpleText NVARCHAR(MAX) = '{@164,737@} </br></br><p>Dodatkowe informacje:</p>Czy na termin: <b>{@215*@} ```("{@215@}" == "1") ? ", {@540@}" : ""```</b><br>Czas naprawy: <b>{@164,1016@} </b> (```({@164,1017@}) ? "dni" : "godzin"```)<br>Czynności do wykonania: <b>{#dictionary(serviceActions|{@164,1019*@})#}</b>'
	DECLARE @parsedText NVARCHAR(MAX) = ''
	 
	EXEC [dbo].[P_parse_string]
	 @processInstanceId = @previousProcessId,
	 @simpleText = @simpleText,
	 @parsedText = @parsedText OUTPUT
	 
	SET @body = REPLACE(@body,'__CONTENT__', @parsedText)
	SET @body = REPLACE(@body,'__PLATFORM_NAME__', ISNULL(@platformName, ''))
	SET @body = REPLACE(@body,'__EMAIL__', @email)
	
--	SELECT @body
	
	EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @groupProcessInstanceId
	 	,@type = dbo.f_translate('email',default)
		,@content = dbo.f_translate('Mail do leasingodawcy o zgodę na usługę Door2Door',default)
		,@email = @email
		,@userId = 1  -- automat
		,@subject = @subject
		,@direction= 1
		,@sender = @sender
		,@emailBody = @body
		,@err=@err OUTPUT
		,@message=@message OUTPUT
		

	/*		Zostaje odroczne +1h i przekierowanie do Panelu świadczeń
 	____________________________________*/

 	EXEC [dbo].[p_jump_to_service_panel] @previousProcessId = @previousProcessId, @variant = 1

END



