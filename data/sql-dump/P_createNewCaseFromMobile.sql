ALTER PROCEDURE [dbo].[P_createNewCaseFromMobile]
( 	
    @callNumber NVARCHAR(100),
    @platformCallNumber NVARCHAR(30) = '',
    @token NVARCHAR(40) OUTPUT,
    @disabledSMS INT = 0
) 
AS
BEGIN
	SET NOCOUNT ON 
	/* ______________________________________________________
	 
	 	Procedura tworzy nowy process przyjęcia sprawy dla klienta mobilnego
	 	
	 	Przykład:
	 	
	 	```
	 		DECLARE @token NVARCHAR(40)
			
			EXEC [dbo].[P_createNewCaseFromMobile] 
				@platformCallNumber = '721',
			    @callNumber = '111222333',
			    @token = @token OUTPUT
			    
			SELECT @token
		```
	 ________________________________________________________*/
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @processInstanceId INT 
	DECLARE @processInstanceIds varchar(4000)
	DECLARE @platformId INT 
	
	SET @platformCallNumber = IIF(@platformCallNumber = '999','721',@platformCallNumber)
	SELECT @platformId = platform_id FROM dbo.platform_phone where number = @platformCallNumber 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	EXEC [dbo].[p_process_new]
	@stepId = '1011.001',
	@err = @err OUTPUT,
	@message = @message OUTPUT,
	@processInstanceId = @processInstanceId OUTPUT
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance WITH(NOLOCK) WHERE id = @processInstanceId
	
	/*	Ustawienie platformy
 		____________________________________*/
	EXEC p_attribute_edit
	@attributePath = '253',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@userId = 1,
	@originalUserId = 1,
	@valueInt = @platformId,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	
	DECLARE @startInstanceId INT 
	SET @startInstanceId = @processInstanceId
	
	EXEC [dbo].[p_form_controls]
	@instance_id = @processInstanceId,
	@returnResults = 0,
	@returnWithNote = 0
	
 	EXEC [dbo].[p_process_next]
	@previousProcessId = @processInstanceId,
	@variant = 4,
	@err = @err OUTPUT,
	@message = @message OUTPUT,
	@processInstanceIds = @processInstanceIds OUTPUT
 
	SELECT top 1 @processInstanceId = id from dbo.process_instance where group_process_id = @startInstanceId and step_id IN ( '1011.007','1011.087')
	
--	SET @processInstanceId = CAST(@processInstanceIds AS INT)
	
	EXEC [dbo].[p_form_controls]
	@instance_id = @processInstanceId,
	@returnResults = 0,
	@returnWithNote = 0
	
	 
	
	/*	Ustawienie numeru infolini na którą dzwonił klient
 		____________________________________*/
	EXEC p_attribute_edit
	@attributePath = '321',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@userId = 1,
	@originalUserId = 1,
	@valueString = @platformCallNumber,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	/*	Ustawienie numeru klienta
 		____________________________________*/
	EXEC p_attribute_edit
	@attributePath = '197',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@userId = 1,
	@originalUserId = 1,
	@valueString = @callNumber,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	EXEC p_attribute_edit
	@attributePath = '80,342,408,197',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@userId = 1,
	@originalUserId = 1,
	@valueString = @callNumber,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	SET @token = NEWID()
	
	UPDATE dbo.process_instance SET token = @token WHERE id = @processInstanceId

	IF @disabledSMS = 0
	BEGIN
		
		DECLARE @smsContent NVARCHAR(4000)
	
		SET @smsContent = dbo.f_translate('Aby utworzyć nowe zgłoszenie Assistance, kliknij w poniższy link: ',default)+dbo.f_getDomain()+'/m/'+@token + dbo.f_translate('. Pamiętaj, zanim wypełnisz formularz zadbaj o bezpieczeństwo własne i pasażerów. Jeżeli znajdujesz się w miejscu niebezpiecznym np. na autostradzie pamiętaj o ubraniu kamizelki odblaskowej i wystawieniu trójkąta oraz oczekiwaniu na pomoc',default)
		
	 	EXEC dbo.p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @smsContent,
		@phoneNumber = @callNumber,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	END
	
END