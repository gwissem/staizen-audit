ALTER PROCEDURE [dbo].[s_1021_081]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @rootId INT
	DECLARE @groupProcessInstanceId INT
	DECLARE @nextStepId INT
	
	-- Pobranie GroupProcessInstanceId --
	SELECT	@groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance 
	WHERE id = @previousProcessId 
	
	DECLARE @doImport BIT = 1
	DECLARE @Authorization BIT = (SELECT TOP 1 value_int FROM attribute_value WITH(NOLOCK) WHERE root_process_instance_id=@RootId AND attribute_path='129,808,801' AND value_int IS NOT NULL)

	--> Weryfikacja czy jest pozytywna autoryzacja
	IF ISNULL(@Authorization,0) = 0
	BEGIN
		SET @doImport = 0
		PRINT 'A' + RIGHT('00000000' + CAST(@RootId AS NVARCHAR(10)), 8) + dbo.f_translate(' Brak Autoryzacji',default) 
	END

	--> Zabezpieczenie przed ponownym importem
	IF EXISTS(SELECT TOP 1 1 FROM SYNC.ZamNag WITH(NOLOCK) WHERE rootId=@rootId)
	BEGIN
		SET @doImport = 0
	END

	DECLARE @makeModel INT = (SELECT TOP 1 value_int FROM attribute_value WITH(NOLOCK) WHERE root_process_instance_id=@RootId AND attribute_path='74,73' AND value_int IS NOT NULL)

	--> Pobieranie programu
	DECLARE @Program VARCHAR(10) = (
		SELECT TOP 1 value_string FROM attribute_value WHERE root_process_instance_id = @RootId AND attribute_path = '202' AND value_string IS NOT NULL
	)
	--> Pobieranie platformy
	DECLARE @Platform INT = (
		SELECT TOP 1 value_int FROM attribute_value WHERE root_process_instance_id = @RootId AND attribute_path = '253' AND value_int IS NOT NULL
	)

	IF @Platform IS NULL
	BEGIN
		IF @Program IS NOT NULL --> Nie mamy platformy mamy program
		BEGIN
			SET @Platform = ( 	--> /!\ Ustawia platformę na podstawie programu
					SELECT TOP 1 platform_id
					FROM vin_program
					WHERE id = @Program
					)
			END
		ELSE
		BEGIN --> Nie mamy ani platformy ani programu, pobiera platformę z marki model /!\
			SET @Platform = (select top 1 argument1 from dictionary with(nolock) where typeD='makeModel' and value=@makeModel)
		END

		IF @Platform IS NOT NULL --> Aktualizacja Platformy
		BEGIN
			EXEC dbo.p_attribute_edit @attributePath = '253'
				,@groupProcessInstanceId = @RootId
				,@stepId = 'xxx'
				,@valueint = @Platform
				,@err = @err OUTPUT
				,@message = @message OUTPUT
		END
	END

	IF @Platform IS NOT NULL AND @Program IS NULL --> Mamy platformę, nie mamy programu
	BEGIN
		DECLARE @StepId020 INT = (SELECT TOP 1 id FROM process_instance WITH(NOLOCK) WHERE step_id='1021.020' AND root_id=@RootId ORDER BY id DESC)		
		IF @StepId020 IS NOT NULL
		BEGIN
			--> Ponowna aktywacja kroku odpowiedzialnego za weryfikacje uprawnień
			UPDATE process_instance SET active=1 WHERE root_id=@RootId AND id=@StepId020
			DECLARE @processInstanceIds varchar(4000)
			EXEC	[dbo].[p_process_next]
					@previousProcessId = @StepId020,
					@err = @err OUTPUT,
					@message = @message OUTPUT,
					@processInstanceIds = @processInstanceIds OUTPUT,
					@variant = 1
		END

		SET @Program = (
			SELECT TOP 1 value_string FROM attribute_value WHERE root_process_instance_id = @RootId AND attribute_path = '202' AND value_string IS NOT NULL
		)
	END

	IF @Program IS NULL
	BEGIN
		PRINT dbo.f_translate('Brak programu.',default)
	END
	ELSE
	BEGIN
		DECLARE @ProcessID INT = (
				SELECT TOP 1 id
				FROM process_instance
				WHERE root_id = @RootId
					AND step_id = '1021.080'
				ORDER BY updated_at DESC
				)
		--exec dbo.p_form_controls @instance_id = @ProcessID
		--> Wymuszenie nie podpinania pod sprawę
		--EXEC dbo.p_attribute_edit @attributePath = '452'
		--	,@groupProcessInstanceId = @RootId
		--	,@stepId = 'xxx'
		--	,@valueint = 0
		--	,@err = @err OUTPUT
		--	,@message = @message OUTPUT

		DECLARE @caseExists INT = (
				SELECT TOP 1 value_int
				FROM attribute_value WITH (NOLOCK)
				WHERE attribute_path = '452'
					AND group_process_instance_id = @RootId
					AND value_int IS NOT NULL
				)

		DECLARE @VIN NVARCHAR(255) = (
				SELECT TOP 1 value_string
				FROM attribute_value WITH (NOLOCK)
				WHERE root_process_instance_id = @rootId
					AND attribute_path = '74,71'
					AND value_string IS NOT NULL
				)
		DECLARE @VIN_COUNT INT = (
				SELECT COUNT(root_process_instance_id)
				FROM attribute_value WITH (NOLOCK)
				WHERE value_string = @VIN
					AND attribute_path = '74,71'
				)

		IF @caseExists = 1
			AND @VIN_COUNT > 1 --> Nie powinno być podpięcie
		BEGIN
			UPDATE process_instance
			SET active = 1
				,created_at = GETDATE()
				,updated_at = GETDATE()
				,date_leave = NULL
				,date_enter = GETDATE()
			WHERE id = @ProcessID

			SET @doImport = 0

			DECLARE @matrixRootId INT = (
					SELECT id
					FROM attribute_value
					WHERE attribute_path = '820'
						AND root_process_instance_id = @RootId
					)

			EXEC dbo.p_attribute_delete @attributeValueId = @matrixRootId
				,@force = 1
		END

		IF @doImport = 1
		BEGIN
			UPDATE process_instance
			SET active = 0
			WHERE id = @ProcessID

			EXEC dbo.p_matrixRefresh @rootProcessInstanceId = @RootId

			EXEC [dbo].[p_entity_export] @rootId = @RootId

			EXECUTE dbo.p_note_new -- Dodawanie notatki
				@groupProcessId = @RootId
				,@type = dbo.f_translate('text',default)
				,@content = dbo.f_translate('Przeniesiono do CDNa.',default)
				,@userId = 1 -- automat
				,@err = @err
				,@message = @message

			UPDATE dbo.process_instance
			SET active = 0
			WHERE group_process_id = @RootId -- Zablokowanie edycji danych.

			DECLARE @acceptStepId NVARCHAR(20)

			SELECT TOP 1 @acceptStepId = id
			FROM dbo.process_instance WITH (NOLOCK)
			WHERE group_process_id = @RootId
				AND step_id = '1021.030'

			IF @acceptStepId IS NOT NULL
			BEGIN
				UPDATE dbo.process_instance
				SET active = 999
				WHERE id = @acceptStepId
			END

			--> Zaznaczenie podpięcia pod sprawę NIE
			EXEC dbo.p_attribute_edit @attributePath = '452'
				,@groupProcessInstanceId = @RootId
				,@stepId = 'xxx'
				,@valueint = 0
				,@err = @err OUTPUT
				,@message = @message OUTPUT

			PRINT @RootId + dbo.f_translate('Zaimportowane',default)
		END
	END

	/*
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	DECLARE @platformId INT = (SELECT value_int FROM @values)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '452', @groupProcessInstanceId = @groupProcessInstanceId
	DECLARE @caseExists INT = (SELECT value_int FROM @values) -- Czy raport serwisowy dotyczy istniejącej sprawy (1-TAK 0-NIE)?


	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	DECLARE @programId INT = (SELECT value_int FROM @values) -- Czy raport serwisowy dotyczy istniejącej sprawy (1-TAK 0-NIE)?

	-------------------------------------------------------------------------------------
	-- Dealer Call, wypełnienie macierzy oraz przeniesienie do tabeli ZamNag, ZamElem. --
	-------------------------------------------------------------------------------------
	DECLARE @agreement INT = (SELECT value_int FROM attribute_value WHERE attribute_path = '129,808,801' AND group_process_instance_id = @groupProcessInstanceId AND root_process_instance_id = @rootId) -- Jest autoryzacja na samochód zastępczy?
	IF (@agreement = 1 AND @programId IS NOT NULL)
	BEGIN
		IF @caseExists = 0 OR (@platformId NOT IN (2,60) AND @caseExists = 1) --DC dla Forda podpiętego pod sprawę ma być traktowany jak zwykłe MW.
		BEGIN
			EXEC dbo.p_matrixRefresh @rootProcessInstanceId=@rootId -- Wypełnienie macierzy.
			EXEC dbo.p_entity_export @rootId=@rootId -- Przeniesienie do tabeli ZamNag, ZamElem.
			EXEC dbo.p_note_new -- Dodawanie notatki
				@groupProcessId = @groupProcessInstanceId
				,@type = dbo.f_translate('text',default)
				,@content = dbo.f_translate('Dealers Call przeniesiono do CDNa.',default)
				,@userId = 1  -- automat
				,@err = @err
				,@message = @message
		END

		UPDATE dbo.process_instance SET active = 0 WHERE group_process_id=@groupProcessInstanceId -- Zablokowanie edycji danych.

		DECLARE @acceptStepId NVARCHAR(20)
		SELECT TOP 1 @acceptStepId = id FROM dbo.process_instance with(nolock) WHERE group_process_id = @groupProcessInstanceId and step_id = '1021.030'
		IF @acceptStepId is not null
		BEGIN
			UPDATE dbo.process_instance SET active = 999 WHERE id = @acceptStepId
		END 
	END
	ELSE
	BEGIN
		IF @programId IS NULL
		BEGIN
			EXEC dbo.p_note_new -- Dodawanie notatki
				@groupProcessId = @groupProcessInstanceId
				,@type = dbo.f_translate('text',default)
				,@content = 'Dealers Call brak programu!'
				,@userId = 1  -- automat
				,@err = @err
				,@message = @message
		END
	END
	*/
END
