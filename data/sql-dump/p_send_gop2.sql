ALTER PROCEDURE [dbo].[p_send_gop2]
( 
	@groupProcessInstanceId INT,
	@email NVARCHAR(255) = null,
	@final INT = 0,
	@test INT = 0,
	@preview int = 0,
	@userName NVARCHAR(255) = null
) 
AS
BEGIN	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @stepId NVARCHAR(255)
	DECLARE @attributeValueId INT
	DECLARE @partnerId INT	
	DECLARE @subject NVARCHAR(300)
	DECLARE @fileName NVARCHAR(300)
	DECLARE @attachment NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @partnerName NVARCHAR(300)
	DECLARE @helpline NVARCHAR(20)
	DECLARE @platform NVARCHAR(50)
	DECLARE @starterEmail NVARCHAR(200)
	DECLARE @platformId INT	
	DECLARE @partnerIdPath NVARCHAR(255) = '610'
	DECLARE @partnerGroupId INT = @groupProcessInstanceId
	DECLARE @Text as table ( answer varchar(MAX) )
	DECLARE @location NVARCHAR(255)
	DECLARE @crossBorder INT	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @serviceDictionaryId INT = null
	DECLARE @caseId NVARCHAR(20)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @location = value_string FROM @values	
	SELECT @crossBorder = IIF(ISNULL(@location,dbo.f_translate(dbo.f_translate('Polska',default),default)) = dbo.f_translate(dbo.f_translate('Polska',default),default),0,1)
	
	SET @starterEmail = [dbo].[f_getEmail]('callcenter')
	IF @crossBorder = 1
	BEGIN
		SET @partnerIdPath = '741'
		SET @starterEmail = [dbo].[f_getEmail]('arc')

		declare @extCaseId nvarchar(100)
		declare @VIN NVARCHAR(50)
		declare @regNumber nvarchar(20)
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '711', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @extCaseId = value_string FROM @values
			
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '73,71', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @VIN = value_string FROM @values
	
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @regNumber = value_string FROM @values
		
		set @caseId=dbo.f_caseId(@groupProcessInstanceId)
		
		IF @userName IS NOT NULL
		BEGIN
			EXEC p_attribute_edit
			@attributePath = '838,840', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueString = @userName,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		END
	END 
	ELSE
	BEGIN
		SET @subject = dbo.f_conditionalText(@final,dbo.f_translate('Ostateczna gwarancja',default),dbo.f_translate('Gwarancja',default))+dbo.f_translate(' płatności dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
		SET @fileName = dbo.f_conditionalText(@final,'ostateczna_','')+'gwarancja_platnosci_'+dbo.f_caseId(@groupProcessInstanceId)+'_'+CAST(CAST(GETDATE() AS DATE) AS NVARCHAR(200))
	END 
	
	SELECT @stepId = step_id FROM dbo.process_instance WHERE id = @groupProcessInstanceId
	
	IF @stepId LIKE '1007.%'
	BEGIN
		IF @crossBorder = 1
		BEGIN
			SET @partnerIdPath = '837,704'

			set @body=dbo.f_translate('Regarding case no ',default)+isnull(@caseId,'')+'<BR>'+
				isnull('Registration number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
				Please organize the replacement vehicle as in attachment<BR><BR><BR>
				Best regards<BR>
				Starter24'
		END 
		ELSE
		BEGIN
			SET @serviceDictionaryId = 5
			SET @partnerIdPath = '764,742'
			
			EXEC p_gop_data_replacement_car
			@groupProcessInstanceId = @groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT
		END 
		
		
	END
	ELSE IF @stepId LIKE '1016.%'
	BEGIN
		
		SET @serviceDictionaryId = IIF(@crossBorder = 0,14,null)
	
		if @crossBorder = 1
		BEGIN
			declare @persons int
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '152,154', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @persons = value_int FROM @values
				
			declare @address1 nvarchar(100)
			declare @address2 nvarchar(100)
		
			Select @address1=dbo.f_addressText(id) from dbo.attribute_value where group_process_instance_id=@groupProcessInstanceId and attribute_path='152,175,85'
			Select @address2=dbo.f_addressText(id) from dbo.attribute_value where group_process_instance_id=@groupProcessInstanceId and attribute_path='152,320,85'
			
			set @body='Hello from Poland,<BR><BR>
			Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR>'+
			isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
			Please organize taxi for '+isnull(cast(@persons as varchar(10)),'')+' persons<BR>'+
		   dbo.f_translate('From: ',default)+isnull(@address1,'')+'<BR>'+
		   'To: '+isnull(@address2,'')+'<BR>
			<BR>
			Best regards,<BR>
			Starter24'		   
		END
		ELSE
		BEGIN			
			EXEC p_gop_data_taxi
			@groupProcessInstanceId = @groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT				
		END 
	
	END 
	ELSE IF @stepId LIKE '1014.%'
	BEGIN
	
		SET @serviceDictionaryId = IIF(@crossBorder = 0,6,null)
		
		IF @crossBorder = 1
		BEGIN
			set @body=dbo.f_translate('Regarding case no ',default)+isnull(@caseId,'')+'<BR>'+
				isnull('Registration number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
				Please organize the hotel as in attachment<BR><BR><BR>
				Best regards<BR>
				Starter24'
		END 
		ELSE
		BEGIN
			EXEC p_gop_data_hotel
			@groupProcessInstanceId = @groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT	
		END 
		
		
	END 
	ELSE IF @stepId LIKE '1018.%'
	BEGIN
	
		if @crossBorder = 1
		BEGIN
			set @body='Hello from Poland,<BR<BR>
				Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR>'+
				isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
				Please provide us information about information listed below: Workshop:<BR>
				Adress of the workshop:<BR>
				Phone number:<BR>
				Opening hours:<BR>
				Repair on warranty?*(yes/no)<BR>
				*(if no) cost of the repair that Client has to pay in workshop<BR>
				Repair status?**<BR>
				**(if not finished) when it will be?<BR>
				Parking cost (yes/no)<BR>
				***(if yes) total cost<BR>
				Best regards,<BR>
				Starter24'		   
		END
		
		EXEC p_gop_data_transport
		@groupProcessInstanceId = @groupProcessInstanceId,
		@final = @final,
		@content = @content OUTPUT,
		@partnerId = @partnerId OUTPUT	
		
	END 
	ELSE IF @stepId LIKE '1142.%'
	BEGIN
		
		-- 8 - ID USŁUGI PARKING W BAZIE KONTRAKTORÓW
		SET @serviceDictionaryId = 8
	
		IF @crossBorder = 1
		BEGIN
			
			-- Ustawienie pola UWAGI

			DECLARE @days INT
			DECLARE @startParking DATETIME
			DECLARE @endParking DATETIME
			DECLARE @info NVARCHAR(MAX)
			
			-- Data Start parkowania
		 	DELETE FROM @Values
		 	INSERT  @values EXEC dbo.p_attribute_get2
		  		@attributePath = '862,823',
		  		@groupProcessInstanceId = @groupProcessInstanceId
		  	SELECT @startParking = value_date FROM @values
		 	
		  	-- Data End parkowania
			INSERT  @values EXEC dbo.p_attribute_get2
		 		@attributePath = '862,853',
		 		@groupProcessInstanceId = @groupProcessInstanceId
		 	SELECT @endParking = value_date FROM @values
		 	
		 	SET @days = dbo.f_getParkingDays(@groupProcessInstanceId)
		 	SET @info = dbo.f_translate('Please organize parking for ',default) + CAST(ISNULL(@days, 1) AS VARCHAR(10)) + ' days<BR>'+ '<br> Start day: ' + dbo.FN_VDateHour(@startParking) + '<br> End day: ' + dbo.FN_VDateHour(@endParking) 

		 	EXEC dbo.p_attribute_edit
			@attributePath = '838,63', -- UWAGI
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueText = @info,
			@err = @err OUTPUT,
			@message = @message OUTPUT
				
			SET @body='Hello from Poland,<BR><BR>
			Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR>'+
			isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
			Please organize parking for '+isnull(cast(@days as varchar(10)),'')+' days<BR>'+
		   dbo.f_translate('From: ',default) + dbo.FN_VDateHour(@startParking) + '<BR>'+
		   'To: ' + dbo.FN_VDateHour(@endParking) + '<BR>
			<BR>
			Best regards,<BR>
			Starter24'		
		   
		   
		END
		ELSE
		BEGIN			
			EXEC p_gop_data_parking
			@groupProcessInstanceId = @groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT				
		END 
	END 
	
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = @partnerIdPath, @groupProcessInstanceId = @partnerGroupId
	SELECT @partnerId = value_int FROM @values
	
	IF @email is null 
	BEGIN
		SELECT TOP 1 @email = dbo.f_partner_contact(@groupProcessInstanceId,@partnerId, @serviceDictionaryId, 4)	
	END
	SELECT @partnerName = dbo.f_partnerName(@partnerId)	
	
	IF @crossBorder = 1 AND @preview = 0
	BEGIN		
					
		SET @subject = dbo.f_translate('Assistance Organization Request. Ref: ',default) + dbo.f_caseId(@groupProcessInstanceId)
		   	SELECT @email, @partnerName, @partnerId, @partnerIdPath
		   	RETURN 
		EXECUTE dbo.p_note_new 
			 @groupProcessId = @groupProcessInstanceId
			,@type = dbo.f_translate('email',default)
			,@content = @body
			,@email = @email
			,@userId = 1  -- automat
			,@subject = @subject
			,@direction=1
			,@dw = ''
			,@udw = ''
			,@sender = @starterEmail
			,@additionalAttachments = '{FILE::assistance_organization_request::AssistanceOrganizationRequest::true}'
			,@emailBody = @body
			,@err=@err OUTPUT
			,@message=@message OUTPUT
			
		RETURN
	END 
	
    DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	SELECT @platform = name, @helpline = official_line_number FROM dbo.platform where id = @platformId
	
	DECLARE @domain_ VARCHAR(100) = [dbo].[f_getDomain]()
    
    select @body = '
	<!DOCTYPE html>
	<html lang="pl">
	<head>
	    <meta charset="utf-8">
	            <link rel="stylesheet" type="text/css"  href="'+@domain_+'/bundles/web/css/vendor/bootstrap/bootstrap.min.css" >
	    </head>
	<body>
	     <div class="container-fluid" style="padding-top: 20px; padding-bottom: 20px;">
	         <div class="row">
	            <div class="col-xs-9 text-left">
	                <h3>___TITLE___ ___CASEID___</h3>
	            </div>
	            <div class="col-xs-3">
	                <img style="display: inline-block; max-width: 100%" src="'+@domain_+'/images/email_assets/starter24.png" alt="Logo starer">
	            </div>
	        </div>
	
	         <div class="row" style="padding-top: 30px;">
	            <div class="col-xs-8">
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Do:</b>
	                    </div>
	                    <div class="col-xs-10">
	                        ___PARTNER_LOCATION___
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Od:</b>
	                    </div>
	                    <div class="col-xs-10">
	                        <p>
	                            Starter24 Sp. z o.o.
	                                                    </p>
	                        <p><b>Telefon kontaktowy dla Państwa: +48 61 83 19 894</b></p>
	              <p>Infolinia dla Klienta: ___HELPLINE___</p>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Data:</b>
	                    </div>
	                    <div class="col-xs-10">
	                        ___DATE_TIME___
	                    </div>
	              </div>
	            </div>
	             <div class="col-xs-4">
	                 <div class="row text-right">
	                     <div class="col-xs-12">
	                         <h5>___PLATFORM_NAME___ Assistance</h5>
	                     </div>
	                     <div class="col-xs-12">
	                         ___STARTER_EMAIL___
	                     </div>
	                 </div>
	             </div>
	         </div>
	     </div>
	    <div class="container-fluid">
	        <div class="row" style="border-top: 1px #000 dotted; padding-top: 15px; padding-bottom: 15px;">
	            <div class="col-xs-10 col-xs-offset-1">
	                ___CONTENT___
	
	            </div>
	        </div>
	
	                                                                                                    </div>
	
	    <div class="container-fluid">
	        <div class="row" style="border: 1px #000 dotted; border-width: 1px 0; padding-top: 10px; padding-bottom: 10px">
	            <div class="col-xs-6">
	                <p><b>Dane do wystawienia faktury:</b></p>
	                <div class="row">
	                    <div class="col-xs-10 col-xs-offset-2">
	                        Starter24 Sp. z o.o.<br>
	                        ul. Józefa Kraszewskiego 30<br>
	                        60-519 Poznań<br>
	                        NIP: 525-21-83-310<br>
	                    </div>
	                </div>
	            </div>
	            <div class="col-xs-6" style="border-left: 1px #000 dotted">
	                <p><b>Fakturę prosimy wysyłać na adres:</b></p>
	                <div class="row">
	                    <div class="col-xs-10 col-xs-offset-2">
	                        Starter24 Sp. z o.o.<br>
	                        ul. Józefa Kraszewskiego 30<br>
	                        60-519 Poznań<br>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row" style="padding-top: 10px">
	            <div class="col-xs-12 small">
	                <ul style="list-style: none">
	                    <li>1) Jako nabywcę faktury prosimy wpisać nazwę wymienioną obok pozycji „Dane do wystawienia faktury:”, natomiast fakturę prosimy wysłać na adres: Starter24 Sp. z o.o., ul. Józefa Kraszewskiego 30, 60-519 Poznań.
	                    </li>
	                    <li>2) Koniecznym załącznikiem do faktury jest niniejsza gwarancja płatności oraz RZW.</li>
	                    <li>3) Fakturę z załącznikami należy przesłać do firmy Starter24 w ciągu 5 dni od daty wykonania usługi.</li>
	                    <li>4) Niespełnienie powyższych warunków może spowodować wstrzymanie płatności za usługę.</li>
	                    <li>5) W przypadku zmiany kwoty gwarancji - kolejna gwarancja anuluje poprzednie.</li>
	                    <li>6) Płatność nastąpi w ciągu 30 dni od otrzymania faktury.</li>
	                    <li>7) W przypadku wynajmu auta prosimy o zaznaczenie na Państwa fakturze, czy wynajmowany samochód posiadał homologację auta ciężarowego.</li>
	                    <li>8) Niedotrzymanie powyższych warunków może opóźnić lub wstrzymać płatność za usługę.</li>
	                </ul>
	            </div>
	        </div>
	    </div>
	</body>
	</html>'
    
    SET @body = REPLACE(@body,'___CASEID___','')
    SET @body = REPLACE(@body,'___TITLE___',@subject)    
    SET @body = REPLACE(@body,'___PARTNER_LOCATION___',ISNULL(@partnerName,''))    
    SET @body = REPLACE(@body,'___CONTENT___',@content)    
    SET @body = REPLACE(@body,'___DATE_TIME___', dbo.FN_VDateHour(GETDATE()))
    SET @body = REPLACE(@body,'___HELPLINE___', @helpline)
    SET @body = REPLACE(@body,'___STARTER_EMAIL___', @starterEmail)
    SET @body = REPLACE(@body,'___PLATFORM_NAME___', @platform)
    
	SET @content = 'Szanowni Państwo,<br/> 
					Niniejszym przesyłamy '+dbo.f_conditionalText(@final,dbo.f_translate('ostateczną ',default),'')+'gwarancję płatności.<br/>
					Szczegóły znajdą Państwo w załączonym pliku.<br/><br/>

					W korespondencji z nami należy zawsze używać adresu mailowego rentals@starter24.pl (kontakt w sprawie wynajmów), pamiętając by w temacie wiadomości zawrzeć nasz numer sprawy (widoczny w temacie niniejszego maila).<br/>
					W sprawach bardzo pilnych uprzejmie prosimy o dodatkowy kontakt telefoniczny pod numerem 61 83 19 984.<br/><br/>

					<b>UWAGA!</b><br/>
					W celu usprawnienia komunikacji i rozliczeń pomiędzy naszymi firmami oraz koniecznością wyeliminowania zmian danych dotyczących wynajmu po zrealizowaniu i zamknięciu usługi, uprzejmie prosimy o przestrzeganie następujących zasad:
					<ul><li>firma wynajmująca pojazd w ciągu 24h od zakończenia wynajmu dostarczy kompletną informację o kosztach zorganizowanych usług</li> 
					<li>ostatniego dnia wynajmu na adres email Starter24 prześle firmie wynajmującej link do formularza RZW</li>
					<li>Starter24 odeśle firmie wynajmującej kopię uzupełnionego RZW</li>
					<li>po weryfikacji Starter24 odeśle firmie wynajmującej potwierdzenie akceptacji RZW w formie "Ostatecznej gwarancji płatności"</li>
					<li>firma wynajmująca wystawi fakturę nie wcześniej niż po uzyskaniu potwierdzenia o akceptacji RZW oraz zgodnie z treścią ostatecznej gwarancji płatności</li>
					<li>Informujemy że po upływie 24h od zakończenia wynajmu nie będą mogły być uwzględnione prośby o zmianę parametrów usługi. Jeżeli Starter24 nie otrzyma RZW w terminie może nie być w stanie uwzględnić w rozliczeniu usługi dodatkowych opcji zgłaszanych przez wypożyczalnię (dodatkowy dzień wynajmu, podstawienie/odbiór, wydanie/odbiór poza godzinami pracy oddziału itp.). Będzie to skutkowało odmową zarejestrowania zmian w systemie Starter24 oraz odmową zapłaty faktury zawierającej dane niezgodne z danymi zarejestrowanymi w systemie Starter24.Podpis pod wiadomością</li></ul>'

	
	SET @body = REPLACE(REPLACE(@body,'href="atlas.','href="http://atlas.'),'src="atlas.','src="http://atlas.') 
				
	if @preview = 1
	BEGIN
		SELECT @body
		RETURN
	END 
	
    EXEC [dbo].[p_attribute_edit]
   	@attributePath = '813', 
   	@groupProcessInstanceId = @groupProcessInstanceId,
   	@stepId = 'xxx',
   	@userId = 1,
   	@originalUserId = 1,
   	@valueText = @body,
   	@err = @err OUTPUT,
   	@message = @message OUTPUT,
   	@attributeValueId = @attributeValueId OUTPUT
	
   	SET @attachment = '{ATTRIBUTE::'+CAST(@attributeValueId AS NVARCHAR(20))+'::'+@fileName+'::true}'
   	
   	IF @test = 1
   	BEGIN 
		SET @email = 'andrzej@sigmeo.pl'   	
   	END 
   	   	

	EXECUTE dbo.p_note_new 
	     @groupProcessId = @groupProcessInstanceId
	    ,@type = dbo.f_translate('email',default)
	    ,@content = @content
	    ,@email = @email
	    ,@userId = 1  -- automat
	    ,@subject = @subject
	    ,@direction=1
	    ,@dw = ''
	    ,@udw = ''
	    ,@sender = @starterEmail
	    ,@additionalAttachments = @attachment
	    ,@emailBody = @content
	    ,@emailRegards = 1
	    ,@err=@err OUTPUT
	    ,@message=@message OUTPUT
	
    
END 

