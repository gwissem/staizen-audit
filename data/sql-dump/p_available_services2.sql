ALTER PROCEDURE [dbo].[p_available_services2]
@processInstanceId INT,
@init INT = 1,
@locale NVARCHAR(10) = 'pl'
AS
BEGIN
	
	SET NOCOUNT ON
	
	PRINT '------------------ START p_available_services'
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(MAX)
	DECLARE @weightLimit INT
	DECLARE @platformId INT
	DECLARE @platformName NVARCHAR(255)
	DECLARE @isHighway INT
	DECLARE @notSupportedCountry INT
	DECLARE @notSupportedDiagnosis INT
	DECLARE @accessDeniedConditions INT
	DECLARE @productionDate DATETIME
	DECLARE @otherCaseLastMonth INT
	DECLARE @lastCaseFixed INT
	DECLARE @eventLocationCountry NVARCHAR(255)
	DECLARE @vehicleAtService INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @variant INT
	DECLARE @mainProcessId INT
	DECLARE @policeConfirmation INT
	DECLARE @programId INT
	DECLARE @servicesIds NVARCHAR(255)
	DECLARE @isDealerCall INT
	DECLARE @eventType INT
	DECLARE @programNotFound INT
	DECLARE @towingError INT
	DECLARE @fixError INT
	DECLARE @taxiError INT
	DECLARE @partsError INT
	DECLARE @replacementCarError INT
	DECLARE @hotelError INT
	DECLARE @tripError INT
	DECLARE @transportError INT
	DECLARE @loanError INT
	DECLARE @medicalLawAdviceError INT
	DECLARE @cancel INT
	DECLARE @caseFromPZ SMALLINT
	DECLARE @groupProcessInstanceId INT
	DECLARE @caseSource INT
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	SET @cancel = 0
	SET @err = 0
	SET @taxiError = 0
	SET @fixError = 0
	SET @towingError = 0
	SET @replacementCarError = 0
	SET @partsError = 0	
	SET @hotelError = 0
	SET @tripError = 0
	SET @transportError = 0
	SET @loanError = 0
	SET @medicalLawAdviceError = 0
	
	SELECT @mainProcessId = root_id, @groupProcessInstanceId = group_process_id, @stepId = step_id FROM process_instance with(nolock) WHERE id = @processInstanceId
	-- TODO: DEALERCALL
	SELECT @isDealerCall = 0
	
	DECLARE @arcCode NVARCHAR(20)
	SELECT @arcCode = dbo.f_diagnosis_code(@mainProcessId)
	
	EXEC p_running_services
	@groupProcessInstanceId = @groupProcessInstanceId,
	@servicesIds = @servicesIds OUTPUT --activated services

	PRINT '@servicesIds'
	PRINT @servicesIds

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventLocationCountry = value_string FROM @values




 	DECLARE @allAvailableInAppServices TABLE ( id INT, alias nvarchar(100))

 	INSERT INTO @allAvailableInAppServices (id, alias)  --Taking all services with setted alias and active
 	SELECT sd.id , sd.alias
	FROM AtlasDB_def.dbo.service_definition sd
 	INNER JOIN AtlasDB_def.dbo.service_definition_translation trans
 		ON trans.translatable_id = sd.id
 	WHERE sd.alias IS NOT NULL
			AND
				sd.active = 1
	GROUP BY sd.id, sd.alias





	--		SELECT @programNotFound = dbo.f_isStepVariantInProcess(@groupProcessInstanceId, '1011.038', default)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '573', @groupProcessInstanceId = @mainProcessId
	SELECT @programNotFound = value_int FROM @values
	
	DECLARE @diagnosisSummary NVARCHAR(255)
	SELECT @diagnosisSummary = dbo.f_diagnosis_summary(@groupProcessInstanceId)
	
	PRINT '---diagnosis'
	PRINT @diagnosisSummary
	
	IF @init = 1
	BEGIN
		CREATE TABLE #availableServices (id INT, name NVARCHAR(255), active TINYINT, message NVARCHAR(MAX), description NVARCHAR(MAX), icon NVARCHAR(MAX), variant INT, start_step_id NVARCHAR(255), pos INT)
	END 
	

	--PRINT '-- IN P_AVAILABLE_SERVICES --'
	--PRINT 'STEP_ID:'
	--PRINT @stepId
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = ISNULL(value_string,'') FROM @values
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	SELECT @platformName = name FROM platform with(nolock) where id = @platformId 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventType = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '542', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @policeConfirmation = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,102', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @caseSource = value_int FROM @values
	
	
	-- NOWE ZAPYTANIE  -> SERWISY PER PLATFORMA
	INSERT INTO #availableServices (id, name, active, message, description, icon, variant, start_step_id, pos)
	SELECT sd.id, sdt.name, 1, dbo.f_translate('Obsługa świadczenia nie jest jeszcze dostępna w systemie',default), sdp.description, sd.icon, NULL, sd.start_step_id, sd.[position]
	FROM AtlasDB_def.dbo.service_definition_program sdp with(nolock)
	INNER JOIN service_definition sd with(nolock) ON sd.id = sdp.service_definition_id
	INNER JOIN service_definition_translation sdt with(nolock) ON sd.id = sdt.translatable_id
	AND sdp.program_id = @programId
	WHERE sd.active = 1
	AND sdt.locale = @locale
	ORDER BY sdp.position

	-- Stare zapytanie
--	INSERT INTO #availableServices (id, name, active, message, description, icon, variant, start_step_id, pos)
--	SELECT sd.id, sdt.name, 1, dbo.f_translate('Obsługa świadczenia nie jest jeszcze dostępna w systemie',default), spd.description, sd.icon, NULL, sd.start_step_id, sd.[position]
--	FROM service_definition sd with(nolock)
--	INNER JOIN service_definition_translation sdt with(nolock) ON sd.id = sdt.translatable_id
--	LEFT JOIN dbo.service_platform_description spd with(nolock) ON sd.id = spd.service_definition_id AND spd.platform_id = @platformId
--	WHERE sd.active = 1
--	AND sdt.locale = @locale
--	ORDER BY sd.position
	
	UPDATE #availableServices SET active = 0 WHERE id IN (
		SELECT id FROM dbo.service_definition with(nolock) WHERE start_step_id IS NULL
	)

	IF ISNULL(@programNotFound,-1) = 0 AND @stepId <> '1063.017'
	BEGIN
		UPDATE #availableServices set active = 0, variant = 96, message = dbo.f_translate('W związku z negatywną weryfikacją uprawnień, możemy Panu/Pani zaoferować tylko usługi płatne',default)
		IF @init = 1
		BEGIN
			SELECT * FROM #availableServices ORDER BY pos
		END 
		PRINT '------------------ END p_available_services'
		RETURN
	END 
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2
		   @attributePath = '716',
		   @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @caseFromPZ = value_int FROM @values
	
	IF ISNULL(@caseFromPZ, 0) = 1
	BEGIN
		
		PRINT dbo.f_translate('Zlecenie od PZ',default)
		
		DECLARE @programFromPZ SMALLINT
			
		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2
			   @attributePath = '713',
			   @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @programFromPZ = value_int FROM @values
			
		-- @programFromPZ  { 1: Ad-hoc, 0: ARC }
		
		IF @programFromPZ = 0 AND ISNULL(@caseSource,0) < 3
		BEGIN
			
			-- DLA ARC uproszczeone "i" na kafelkach  
			UPDATE #availableServices 
			SET
			active = 1,
			description = dbo.f_translate('Pomoc organizowana na podstawie zlecenia otrzymanego od Partnera zagranicznego.',default)
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Ta usługa jest niedostępna w zleceniu od PZ',default)
			WHERE id IN (9, 8, 6)
			
			IF @init = 1
			BEGIN
				
				SELECT * FROM #availableServices ORDER BY pos
				
			END
			PRINT '------------------ END p_available_services'
			RETURN
			
		END 
		
	END 
	
	-------------------------------------------------------
	-- WARUNKI OGÓLNE
	-------------------------------------------------------
	
	-- DMC > 5000
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,76', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @weightLimit = value_int FROM @values
	DELETE FROM @values

	if @weightLimit > 5000 
	BEGIN
		set @message = dbo.f_translate('Pojazdy o dopuszczalnej masie całkowitej przekraczającej 5 ton nie są obsługiwane w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jeśli chciałby / chciałaby Pan / Pani skorzystać z płatnych usług, uprzejmie proszę o bezpośredni kontakt z Centrum Zgłoszeniowym obsługującym ciężkie pojazdy pod numerem 609 222 222',default)
		set @err = 1
		set @cancel = 1
		set @variant = 97
	END
	
	if @weightLimit > 3500 AND @programId like '%423%'
	BEGIN
		set @message = dbo.f_translate('Nie mamy możliwości organizacji płatnej pomocy dla pojazdów o dopuszczalnej masie całkowitej przekraczających 3,5 tony. Jeśli chciałby / chciałaby Pan / Pani skorzystać z płatnych usług dla pojazdów ciężarowych, uprzejmie proszę o be',default)
		set @err = 1
		set @cancel = 1
		set @variant = 97
	END
	
	-- zdarzenie w kraju nieobjętym assistance
	-- autostrada we Francji
	IF @err = 0
	BEGIN
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '513', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @isHighway = value_int FROM @values

-- 		IF @eventLocationCountry NOT IN (
-- 		dbo.f_translate(dbo.f_translate('Polska',default),default), dbo.f_translate('Andora',default), dbo.f_translate('Anglia',default), dbo.f_translate('Austria',default), dbo.f_translate('Belgia',default), dbo.f_translate('Bułgaria',default), dbo.f_translate('Chorwacja',default), dbo.f_translate('Cypr',default), dbo.f_translate('Czechy',default), dbo.f_translate('Dania',default),
-- 		dbo.f_translate('Estonia',default), dbo.f_translate('Finlandia',default), dbo.f_translate('Francja',default),  dbo.f_translate('Gibraltar',default), dbo.f_translate('Grecja',default), dbo.f_translate('Hiszpania',default), dbo.f_translate('Holandia',default), dbo.f_translate('Irlandia',default),
-- 		dbo.f_translate('Islandia',default), dbo.f_translate('Serbia',default), dbo.f_translate('Czarnogóra',default),dbo.f_translate('Izrael',default), dbo.f_translate('Lichtenstein',default), dbo.f_translate('Litwa',default), dbo.f_translate('Luksemburg',default), dbo.f_translate('Łotwa',default),
-- 		dbo.f_translate('Macedonia',default), dbo.f_translate('Malta',default), dbo.f_translate('Monako',default), dbo.f_translate('Niemcy',default), dbo.f_translate('Norwegia',default), dbo.f_translate('Portugalia',default), dbo.f_translate('Rumunia',default), dbo.f_translate('San Marino',default), dbo.f_translate('Słowacja',default),
-- 		dbo.f_translate('Słowenia',default), dbo.f_translate('Szwajcaria',default), dbo.f_translate('Szwecja',default), dbo.f_translate('Tunezja',default), dbo.f_translate('Watykan',default), dbo.f_translate('Węgry',default), dbo.f_translate('Wielka Brytania',default), dbo.f_translate('Włochy',default),dbo.f_translate('Zjednoczone Królestwo',default)
-- 		)
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			set @err = 1
			set @message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance. Nie mamy możliwości zorganizowania dla Pana/Pani pomocy Assistance',default)
			set @variant = 97
			set @cancel = 1
		END 
	
		IF @isHighway = 1 AND @eventLocationCountry = dbo.f_translate('Francja',default) AND @stepId != '1011.95'
		BEGIN
			set @err = 1
			set @variant = 95
			set @message = 'Ze względu na specyfikę działania prywatnych operatorów autostrad na terenie Francji, nie mamy możliwości organizacji bezpośredniej pomocy. Proszę zwrócić się o pomoc do służb autostradowych korzystając z najbliższej budki SOS, umiejscowionej co ok. 2 km przy poboczu drogi lub skorzystać z aplikacji mobilnej &lt;&lt;SOS Autoroute&gt;&gt;. Jeśli po interwencji tych służb nadal nie można poruszać się samochodem, a znajduje się on już poza chronionym obszarem autostrady, prosimy o ponowny kontakt i podanie nowej lokalizacji. Proszę pamiętać o zachowaniu imiennych rachunków w celu ubiegania się o zwrot kosztów.'
			set @cancel = 1
		END 
	END
	
--  TODO: Zdarzenia nie objęte Assistance tj. dla platformy:
--	AUDI – dolanie AdBlue
--	VW – dolanie płynów eksploatacyjnych; zdarzenia nie powodujące unieruchomienia; przepalone żarówki; zły kluczyk w stacyjce
--	SKODA – dolanie adBlue; zdarzenia nie powodu jące unierucho mienia, 
--	przepalone żarówki, uszkodzone pióra wycieraczek, klimatyzacja, zły kluczyk w stacyjce	
	
	
	IF @err = 0
	BEGIN
	
		IF (@platformId = 6 AND LEFT(@arcCode,5) IN ('45028', '14810', '10236','10246'))
		OR (@platformId = 11 AND LEFT(@arcCode,5) IN ('45028'))
		OR LEFT(@arcCode,5) IN ('78117')
--		OR (@platformId = 31 AND LEFT(@arcCode,5) IN (''))		
		BEGIN
			set @err = 1
			set @variant = 96
			set @message = dbo.f_translate('Awaria rozpoznana w Pana/Pani pojeździe nie jest obsługiwana przez ',default)+@platformName+dbo.f_translate(' Assistance. W tej sytuacji możemy zaoferować pomoc odpłatną',default)
		
		END 
	END 
	
	-- wykluczone okoliczności
	IF @err = 0
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '538', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @accessDeniedConditions = value_int FROM @values
		
		PRINT '@accessDeniedConditions'
		PRINT @accessDeniedConditions
		
		DECLARE @accessDeniedConditionsText NVARCHAR(255)
		SELECT @accessDeniedConditionsText = textD FROM dictionary with(nolock) where typeD = 'accessDeniedConditions' and value = @accessDeniedConditions
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,427', @groupProcessInstanceId = @groupProcessInstanceId
		
		SELECT @productionDate = value_date FROM @values	
		
		IF @accessDeniedConditions > 1 OR (@accessDeniedConditions = 1 AND @productionDate < DATEADD(yy,-2,GETDATE()))
		BEGIN
			set @err = 1
			set @variant = 96
			set @message = dbo.f_translate('Zgodnie z ogólnymi warunkami ubezpieczenia awarie i szkody spowodowane przez ',default)+@accessDeniedConditionsText+' nie są objęte ochroną Assistance. W związku z tym w opisanej przez Pana/Panią sytuacji nie mamy możliwości zorganizowania pomocy bezpłatnej. Czy jest Pan/Pani zainteresowany/a pomocą odpłatną?'
		END 
	END

	IF @err = 0 AND @arcCode IN ('1452897','1452897','1474697','1991030','1991097','1991097') 
	BEGIN
		set @err = 1
		set @variant = 96
		set @message = 'Zgodnie z ogólnymi warunkami ubezpieczenia opisana przez Pana/Panią sytuacja nie jest objęte ochroną Assistance. Czy jest Pan/Pani zainteresowany/a pomocą odpłatną?'
	END 
	
	-- diagnoza wskazuje na NPT
	IF @err = 0 AND @diagnosisSummary IN ('[NpT]', '[NpT-P]', '[NpT-T]') AND ISNULL(@eventLocationCountry,'') = dbo.f_translate(dbo.f_translate('Polska',default),default)
	BEGIN
		
		DECLARE @partnerName NVARCHAR(200)
		
		CREATE TABLE #partnerTempTable (partnerId INT, partnerName NVARCHAR(300), priority INT, distance DECIMAL(18,2), phoneNumber NVARCHAR(100), reasonForRefusing NVARCHAR(100), partnerServiceId INT, city NVARCHAR(255))
		EXEC p_find_services @groupProcessInstanceId = @groupProcessInstanceId
		SELECT TOP 1 @partnerName = partnerName FROM #partnerTempTable order by distance asc
		DROP TABLE #partnerTempTable
		
		set @err = 1
		set @variant = 97
		set @message = dbo.f_translate('Telefoniczna diagnoza usterki wskazuje, że może Pan/Pani bezpiecznie dojechać do najbliższego autoryzowanego Partnera Serwisowego ',default)
									 +ISNULL(@platformName,'')+' '+ISNULL(@partnerName,'')+'.'
		 
	END
	
	----------------------------------------------------
	-- DISABLED / ENABLED BY CONFIGURATION WITH ALREADY ACTIVATED SERVICES
	------------------------------------------------------

	-- TODO: Zgłoszenie od Klienta czy od serwisu (dealer call) + AUDI
	
	
	-- Czy kradzież + czy mamy dosłane potwierdzenie zgłoszenia sprawy na policję
	IF @arcCode = '1464391' AND ISNULL(@policeConfirmation,0) = 0 AND @stepId NOT IN ('1011.055','1012.006')
	BEGIN
		set @err = 1
		set @message = dbo.f_translate('By móc świadczy usługi assistance, potrzebujemy otrzymać potwierdzenie zgłoszenia zdarzenia na Policję. Proszę dokonać zgłoszenia i przesłać do nas kopię tego dokumentu.',default)
		set @variant = 93
	END
	
--	IF @err = 0
--	BEGIN
--		DELETE FROM @values
--		INSERT @values EXEC p_attribute_get2 @attributePath = '542', @groupProcessInstanceId = @groupProcessInstanceId
--		SELECT @policeConfirmation = value_int FROM @values
--		
--		
--		DELETE FROM @forbiddenDiagnosis 
--		INSERT INTO @forbiddenDiagnosis 
--		SELECT dbo.f_isStepVariantInProcess(@groupProcessInstanceId, @diagnosisId+'.037', default)
--		UNION ALL
--		SELECT dbo.f_isStepVariantInProcess(@groupProcessInstanceId, @diagnosisId+'.044', default)
--		
--		SELECT @forbiddenDiagnosisStepCount = COUNT(*) FROM @forbiddenDiagnosis WHERE result = 1
--		
--		IF @forbiddenDiagnosisStepCount > 0 AND @policeConfirmation <> 1 AND @stepId != '1011.95'
--		BEGIN
--			set @err = 1
--			set @message = dbo.f_translate('By móc świadczy usługi assistance, potrzebujemy otrzymać potwierdzenie zgłoszenia zdarzenia na Policję. Proszę dokonać zgłoszenia, i przesłać do nas kopię tego dokumentu',default)
--			set @variant = 95
--		END 
--	END 


	-- 	WHILE ITERATOR
	DECLARE @cnt int = 1;
	DECLARE @max int = (select count(*) from @allAvailableInAppServices);
	-- 	WHILE USED VARIABLES:
	DECLARE @tempAlias nvarchar(100)
	DECLARE @tempID int
	DECLARE @tempDescriptionEnabled NVARCHAR(255)
	DECLARE @tempDescriptionDisabled NVARCHAR(255)
	DECLARE @string_disabledWhenActive nvarchar(250)
	DECLARE @string_enabledToActive nvarchar(250)
	DECLARE @abroadText nvarchar(40) = ''
	DECLARE @HOMELAND_LOCATION_NAME nvarchar(20) = dbo.f_translate('POLSKA',default) -- @todo: change this to setting!!!

	DECLARE @currentServicesList Table(data int)
	DECLARE @disableWhenServices Table(data int)
	DECLARE @enableWhenServices Table(data int)
	
	-- 	WHILE USED VARIABLES END
	WHILE @cnt <= @max
		BEGIN
			DELETE FROM  @currentServicesList
			DELETE FROM  @disableWhenServices
			DELETE FROM  @enableWhenServices
			SET @tempDescriptionEnabled  = ''
			SET @tempDescriptionDisabled  = ''
			SET @abroadText=''
			SET  @string_enabledToActive =''
			SET @string_disabledWhenActive =''
			--       Select ROW
			SELECT @tempAlias =  alias, @tempID = id
			FROM (SELECT id, alias, ROW_NUMBER() OVER(ORDER BY (select 1)) as RowId
					FROM @allAvailableInAppServices
			 	 ) T1
			WHERE t1.RowId = @cnt

			IF ISNULL(@eventLocationCountry, '') <> '' AND @eventLocationCountry != @HOMELAND_LOCATION_NAME
				BEGIN
					SET @abroadText = '_abroad'
				end


			SELECT @string_enabledToActive =  [value], @tempDescriptionEnabled = description
			from dbo.f_get_platform_key_with_description(@tempAlias+'.enabled_when_active' +isnull(@abroadText,''),
			@platformId,
			@programId
			)


			SELECT @string_disabledWhenActive = [value], @tempDescriptionDisabled = description 
			from dbo.f_get_platform_key_with_description(@tempAlias+'.disabled_when_active'+isnull(@abroadText,''),
			@platformId,
			@programId
			)

			INSERT INTO @currentServicesList SELECT cast(data as int) from f_split(@servicesIds, ',')
			INSERT INTO @enableWhenServices SELECT cast(data as int) from f_split(@string_enabledToActive, ',')
			INSERT INTO @disableWhenServices SELECT cast(data as int) from f_split(@string_disabledWhenActive, ',')
			
			DECLARE @commonRows INT
			
			SELECT @commonRows = count(*) 
			FROM @currentServicesList currentServices 
			INNER JOIN @enableWhenServices enabledConfig ON enabledConfig.data = currentServices.data
			
			IF @commonRows = 0 AND ISNULL(@string_enabledToActive,'') <> '' 
			BEGIN
				UPDATE #availableServices
				SET
				active = 0,
				message = isnull(@tempDescriptionEnabled,'')
				where id = @tempID
			END 
			
			SET @commonRows = 0
			
			SELECT @commonRows = count(*) 
			FROM @currentServicesList currentServices 
			INNER JOIN @disableWhenServices disabledConfig ON disabledConfig.data = currentServices.data
			
			IF @commonRows > 0 AND ISNULL(@string_enabledToActive,'') <> '' 
			BEGIN				
				UPDATE #availableServices
				SET
				active = 0,
				message = isnull(@tempDescriptionEnabled,'')
				where id = @tempID
			END
			

			set @cnt = @cnt+1
		end


	-------------------------------------------------------
	-- ZGODA NA USŁUGĘ PŁATNĄ
	-------------------------------------------------------
		
	IF @programId like '%423%' and @cancel = 0
	BEGIN
		
		-- aby nie wpadać już ponownie w krok z pytaniem o usługi płatne
		UPDATE #availableServices 
		SET
		message = '',
		variant = NULL
		WHERE
		variant = 96
		
		-- tylko świadczenia naprawy i holowania
		UPDATE #availableServices 
		SET
		active = 0,
		message = dbo.f_translate('Świadczenie nie jest dostępne w ramach programu ad-hoc',default)
		WHERE id > 2
		
		SET @taxiError = 1
		SET @partsError = 1
		SET @replacementCarError = 1
		SET @hotelError = 1
		SET @tripError = 1
		SET @transportError = 1
		SET @loanError = 1
		
		-- jeśli tylko płatne, to już nie błąd
		IF @variant = 96
		BEGIN
			SET @err = 0
		END 
	END
		
	IF @err = 1
	BEGIN
		UPDATE #availableServices 
		SET
		active = 0,
		variant = @variant,
		message = @message
--		WHERE variant IS NOT NULL
	END 
	ELSE
	BEGIN
		
		DECLARE @placeSafety INT
		DECLARE @prefferedService INT
		DECLARE @fixingOrTowing INT
		DECLARE @serviceOpened INT
		
		-- TODO: godziny otwarcia serwisu macierzystego
		SELECT @serviceOpened = 1 
		
		-- Wskazanie diagnozy
		DECLARE @overrideTowingFixing INT
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '723', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @overrideTowingFixing = value_int FROM @values
		
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
 	 	SELECT @fixingOrTowing = value_int FROM @values
		
 	 	IF @overrideTowingFixing IS NOT NULL
 	 	BEGIN
	 	 	SET @fixingOrTowing = @overrideTowingFixing
 	 	END
 	 	
 	 	
		IF @overrideTowingFixing IS NOT NULL
		BEGIN
			IF @overrideTowingFixing = 1
			BEGIN
				SET @fixError = 1
				
				UPDATE #availableServices 
				SET
				active = 0,
				message = dbo.f_translate('Nieudana naprawa na drodze, lub brak możliwości znalezienia kontraktora na holowanie.',default)
				WHERE id = 2
			END 
			ELSE IF @overrideTowingFixing = 2
			BEGIN
				SET @towingError = 1
				
				UPDATE #availableServices 
				SET
				active = 0,
				message = dbo.f_translate('Brak możliwości znalezienia kontraktora na naprawę na drodzę.',default)
				WHERE id = 1
			END 
		END 
		ELSE
		BEGIN
			
			IF ISNULL(@diagnosisSummary,'') = '[H]'
			BEGIN
				SET @fixError = 1
				
				UPDATE #availableServices 
				SET
				active = 0,
				message = dbo.f_translate('Diagnoza wskazuje na holowanie',default)
				WHERE id = 2
			END 
			ELSE IF ISNULL(@diagnosisSummary,'') IN ('[N]','[PN]')
			BEGIN
				SET @towingError = 1
				
				UPDATE #availableServices 
				SET
				active = 0,
				message = dbo.f_translate('Diagnoza wskazuje na naprawę na drodzę',default)
				WHERE id = 1
			END 
			
		END

		-- pozostałe usługi są niedostępne zanim nie zostanie organizowana naprawa/holowanie
		IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND dbo.f_exists_in_split(@servicesIds,'2') = 0 AND @arcCode <> '1464391'
		BEGIN
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Usługa jest niedostępna przed organizacją świadczenia holowania, lub naprawy na drodze.',default)
			WHERE id > 2 AND id NOT IN (10,15)
			AND active = 1
		END 

				
		-- Pojazd należący do służb
		IF @accessDeniedConditions = 1
		BEGIN
			SET @fixError = 1
			SET @taxiError = 1
			SET @replacementCarError = 1
			SET @taxiError = 1
			SET @towingError = 0
			SET @partsError = 1	
			SET @hotelError = 1
			SET @tripError = 1
			SET @transportError = 1
			SET @loanError = 1
			
			UPDATE #availableServices 
			SET
			active = 1,
			message = ''
			WHERE id = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Pojazdy należące do służb państwowych posiadają assistance ograniczony do gwarancyjnej odpowiedzialności producenta, iż usterka i udzielone świadczenia nie mieszczą się w zakresie tej odpowiedzialności, powstałe koszty doliczone zostan�',default)
			WHERE id = 2
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Pojazdy należące do Policji, straży pożarnej lub wojska mają ograniczony assistance. Dalsze świadczenia tylko i wyłącznie po potwierdzeniu od partnera serwisowego, że będą rozliczone z producentem',default)
			WHERE id > 2
--			
--			UPDATE #availableServices 
--			SET
--			active = 0,
--			message = dbo.f_translate('Pojazdy należące do Policji, straży pożarnej lub wojska mają ograniczony assistance. Dalsze świadczenia tylko i wyłącznie po potwierdzeniu od partnera serwisowego, że będą rozliczone z producentem',default)
--			WHERE id = 5
		END 
		
		
		IF @arcCode = '1464391'
		BEGIN
			SET @towingError = 1
			SET @fixError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Pojazd został skradziony',default)
			WHERE id IN (1,2)
		END
		
		
		-------------------------------------------------------
		-- HOLOWANIE
		-------------------------------------------------------
		
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('towing.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @towingError = 1
			
			UPDATE #availableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 1
		END 
		
		-------------------------------------------------------
		-- NAPRAWA
		-------------------------------------------------------

		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '414', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @placeSafety = value_int FROM @values
		
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('rsa.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @fixError = 1
			
			UPDATE #availableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 2
		END 
		
		-- istnieje już naprawa
 		IF dbo.f_exists_in_split(@servicesIds,'2') = 1 AND @fixError = 0
 		BEGIN
 			SET @fixError = 1

 			UPDATE #availableServices
 			SET
 			active = 0,
 			message = dbo.f_translate('W systemie jest już zorganizowana naprawa na drodze. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
 			WHERE id = 2
 		END
		
		-- Pojazd w niebezpiecznym miejscu
		IF @placeSafety IN (1,2,3) and @fixError = 0
		BEGIN
			IF @fixError = 0
			BEGIN
				SET @fixError = 1
				
				UPDATE #availableServices
				SET
				active = 0,
				message = dbo.f_translate('Ze względu na miejsce, w którym znajduje się pojazd, najpierw przetransportujemy go do serwisu lub w bezpieczną lokalizację, gdzie będzie kontynuowana naprawa. ',default)
				WHERE id = 2
				
				UPDATE #availableServices 
				SET
				active = 1,
				message = ''
				WHERE id = 1
				
			END
			
--			SET @fixingOrTowing = 1
			SET @diagnosisSummary = '[H]'
			
		END
		-- AUDI - woli holowanie
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '539', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @prefferedService = value_int FROM @values
		
		IF @prefferedService = 2
		BEGIN
			SET @fixError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Klient preferuje holowanie.',default)
			WHERE id = 2
			
			UPDATE #availableServices 
			SET
			active = 1,
			message = ''
			WHERE id = 1 
			-- by Dylesiu  - PO CO TO TUTAJ JEST ? :D
--			and dbo.f_exists_in_split(@servicesIds,'1') = 0
			
		END 
		ELSE IF @prefferedService = 1
		BEGIN
			
			SET @towingError = 1
			
			UPDATE #availableServices 
			SET
			active = 1,
			message = ''
			WHERE id = 2 and dbo.f_exists_in_split(@servicesIds,'2') = 0
			
--			UPDATE #availableServices 
--			SET
--			active = 0,
--			message = dbo.f_translate('Klient preferuje naprawę na miejscu.',default)
--			WHERE id = 1
			
			-- Problem był w AUDI, gdy klient preferował naprawe na drodze, a ona sie odbyła a potem ropoczęto holowanie - Wszystko było nieaktywne - nie można odpalić HOL-PARK-HOL
			IF dbo.f_exists_in_split(@servicesIds,'1') = 0
			BEGIN
				
				UPDATE #availableServices 
				SET
				active = 0,
				message = dbo.f_translate('Klient preferuje naprawę na miejscu.',default)
				WHERE id = 1
			
			END
			ELSE
			BEGIN
				
				UPDATE #availableServices 
				SET
				active = 1,
				message = ''
				WHERE id = 1
				
			END
			
		END 
		

		-------------------------------------------------------
		-- AUTO ZASTĘPCZE
		-------------------------------------------------------
		
		DECLARE @correctServiceReport INT
		DECLARE @programRuleDate DATETIME
		DECLARE @fixingEndDate DATETIME
		
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('replacement_car.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @replacementCarError = 1
			
			UPDATE #availableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 3
		END
		
		-- istnieje już auto zastępcze
 		IF dbo.f_exists_in_split(@servicesIds,'3') = 1
 		BEGIN
 			SET @replacementCarError = 1

 			UPDATE #availableServices
 			SET
 			active = 0,
 			message = dbo.f_translate('W systemie jest już zorganizowane auto zastępcze. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
 			WHERE id = 3
 		END
		
		
		INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @groupProcessInstanceId
 	 	SELECT @fixingEndDate = value_date FROM @values
 	 	
		SET @correctServiceReport = 0
		
		-- data naprawy warsztatowej krótsza niż minimalna wg owu
		
		IF @platformId = 31
		BEGIN
			SET @programRuleDate = GETDATE()
		END 
		ELSE IF @platformId = 11
		BEGIN
			SET @programRuleDate = DATEADD(HOUR, 2, GETDATE())
		END 
		ELSE IF @platformId = 6
		BEGIN
			SET @programRuleDate = DATEADD(HOUR, 4 , GETDATE())
		END 
		
		IF @fixingEndDate < @programRuleDate AND @replacementCarError = 0
		BEGIN
			SET @replacementCarError = 1
			
			IF @fixingEndDate < GETDATE()
			BEGIN
				UPDATE #availableServices 
				SET
				active = 0,
				message = dbo.f_translate('Wg daty naprawy warsztatowej - ',default)+dbo.FN_VDateHour(@fixingEndDate)+' (zadeklarowanej przez serwis w Raporcie Serwisowym) pojazd powinien być już naprawiony. W związku z tym nie możemy zorganizować pojazdu zastępczego,'
				WHERE id = 3	
			END 
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Data naprawy warsztatowej - ',default)+dbo.FN_VDateHour(@fixingEndDate)+' (zadeklarowana przez serwis w Raporcie Serwisowym) jest krótsza niż minimalna wymagana data wg warunków umowy dla pojazdu zastępczego.'
			WHERE id = 3
		END 
		
		-- zorganizowano hotel
-- 		IF dbo.f_exists_in_split(@servicesIds,'4') = 1 AND @replacementCarError = 0
-- 		BEGIN
--
-- 			SET @replacementCarError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Nie mamy możliwości zorganizowania pojazdu zastępczego jeżeli wcześniej korzystał Pan/Pani z usługi hotelu w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu',default)
-- 			WHERE id = 3
--
-- 		END
		
		-- organizujemy podróż
-- 		IF dbo.f_exists_in_split(@servicesIds,'6') = 1 AND @replacementCarError = 0
-- 		BEGIN
--
-- 			SET @replacementCarError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Nie mamy możliwości zorganizowania pojazdu zastępczego jeżeli wcześniej korzystał Pan/Pani z usługi podróży w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu',default)
-- 			WHERE id = 3
-- 		END
--
		-- Brak w bazie + zamknięty serwis macierzysty
		IF @programNotFound = -1 AND @serviceOpened = 0 AND @replacementCarError = 0
		BEGIN
			
			SET @replacementCarError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z Assistance nie widnieją w systemie skontaktujemy się z odpowiednim Dealerem/ Partnerem Serwisowym i potwierdzimy uprawnienia. Po potwierdzeniu uprawnień otrzyma Pan/Pani wiadomość z SMS',default)
			WHERE id = 3
		END 
		
		-- Nie organizujemy holowania i nie mamy kompletnego raportu serwisowego
		IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @correctServiceReport <> 1 AND @replacementCarError = 0 AND @arcCode <> '1464391'
		BEGIN
			
			SET @replacementCarError = 1
			
			SET @message = dbo.f_translate('Jeżeli pomoc drogowa nie była organizowana przez ',default)+@platformName+dbo.f_translate(' Assistance, możemy jedynie autoryzować wynajem samochodu zastępczego przez Partnera Serwisowego jeżeli dysponuje takim pojazdem oraz spełnione są warunki assistance. W tym celu prosimy o kontakt z Partnerem Serwisowym zajmującym się napraw�',default)
			IF @platformId = 31 
			BEGIN
				SET @message = dbo.f_translate('Nie mamy możliwości zorganizowania pojazdu zastępczego jeżeli nie był holowany przez ',default)+@platformName+dbo.f_translate(' Assistance.',default)
			END 
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = @message
			WHERE id = 3
		END 
		
		-------------------------------------------------------
		-- NOCLEG
		-------------------------------------------------------
		
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('hotel.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @hotelError = 1
			
			UPDATE #availableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 4
		END
		
-- istnieje już nocleg
-- 		IF dbo.f_exists_in_split(@servicesIds,'4') = 1
-- 		BEGIN
-- 			SET @hotelError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('W systemie jest już zorganizowany nocleg. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
-- 			WHERE id = 4
-- 		END
		
		
		-- Organizujemy auto zastępcze
-- 		IF dbo.f_exists_in_split(@servicesIds,'3') = 1 AND @hotelError = 0
-- 		BEGIN
-- 			SET @hotelError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Nie mamy możliwości zorganizowania noclegu jeżeli wcześniej korzystał Pan/Pani z usługi pojazdu zastępczego w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu',default)
-- 			WHERE id = 4
-- 		END
		
		-- Organizujemy auto zastępcze
-- 		IF dbo.f_exists_in_split(@servicesIds,'6') = 1 AND @hotelError = 0
-- 		BEGIN
-- 			SET @hotelError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Nie mamy możliwości zorganizowania noclegu jeżeli wcześniej korzystał Pan/Pani z podróży w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu.',default)
-- 			WHERE id = 4
-- 		END
		
		-- Jeśli nie organizujemy holowania
 		IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @hotelError = 0 AND @arcCode <> '1464391'
 		BEGIN
 			SET @hotelError = 1

 			UPDATE #availableServices
 			SET
 			active = 0,
 			message = dbo.f_translate('Nie mamy możliwości zorganizowania noclegu jeżeli wcześniej nie korzystał Pan/Pani z usługi holowania w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu.',default)
 			WHERE id = 4
 		END
		
		-- Brak w bazie + otwarty serwis macierzysty
		IF @programNotFound = -1 AND @serviceOpened = 0 AND @hotelError = 0
		BEGIN
			SET @hotelError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z ',default)+@platformName+dbo.f_translate(' Assistance nie widnieją w systemie skontaktujemy się z odpowiednim Dealerem/Partnerem Serwisowym i potwierdzimy uprawnienia. Po potwierdzeniu uprawnień otrzyma Pan/Pani wiadomość z SMS z numerem sprawy i prośbą o ponowny kontakt.',default)
			WHERE id = 4
		END 
		
		-------------------------------------------------------
		-- PODRÓŻ
		-------------------------------------------------------
		
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('travel.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @tripError = 1
			
			UPDATE #availableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 6
		END
		
		-- istnieje już podróż
 		IF dbo.f_exists_in_split(@servicesIds,'6') = 1
 		BEGIN
 			SET @tripError = 1

 			UPDATE #availableServices
 			SET
 			active = 0,
 			message = dbo.f_translate('W systemie jest już zorganizowany nocleg. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
 			WHERE id = 6
 		END
		
		-- Organizujemy auto zastępcze
-- 		IF dbo.f_exists_in_split(@servicesIds,'3') = 1 AND @tripError = 0
-- 		BEGIN
-- 			SET @hotelError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Nie mamy możliwości zorganizowania podróży jeżeli wcześniej korzystał Pan/Pani z usługi pojazdu zastępczego w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu',default)
-- 			WHERE id = 6
-- 		END
		
		-- Organizujemy nocleg
-- 		IF dbo.f_exists_in_split(@servicesIds,'4') = 1 AND @tripError = 0
-- 		BEGIN
-- 			SET @hotelError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Nie mamy możliwości zorganizowania podróży jeżeli wcześniej korzystał Pan/Pani z usługi noclegu w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu',default)
-- 			WHERE id = 6
-- 		END
		
--  		 Jeśli nie organizujemy holowania
 		IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @tripError = 0 AND @arcCode <> '1464391'

 		BEGIN
 			SET @hotelError = 1

 			UPDATE #availableServices
 			SET
 			active = 0,
 			message = dbo.f_translate('Nie mamy możliwości zorganizowania podróży jeżeli wcześniej nie korzystał Pan/Pani z usługi holowania w ramach ',default)+@platformName+dbo.f_translate(' Assistance. Jest to niezgodne z Ogólnymi Warunkami Programu',default)
 			WHERE id = 6
 		END
		
		-------------------------------------------------------
		-- TRANSPORT
		-------------------------------------------------------
		
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('transport.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @transportError = 1
			
			UPDATE #availableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 7
		END
		
-- 		-- istnieje już transport
 		IF dbo.f_exists_in_split(@servicesIds,'7') = 1 AND @transportError = 0
 		BEGIN
 			SET @transportError = 1

 			UPDATE #availableServices
 			SET
 			active = 0,
 			message = dbo.f_translate('W systemie jest już zorganizowany transport. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
 			WHERE id = 7
 		END
--
-- 		-- Organizujemy auto zastępcze
-- 		IF @programNotFound = -1 AND @transportError = 0
-- 		BEGIN
-- 			SET @transportError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z ',default)+@platformName+dbo.f_translate(' Assistance nie widnieją w systemie skontaktujemy się z odpowiednim Dealerem/Partnerem Serwisowym i potwierdzimy uprawnienia. Po potwierdzeniu uprawnień otrzyma Pan/Pani wiadomość z SMS z numerem sprawy i prośbą o ponowny kontakt',default)
-- 			WHERE id = 7
-- 		END
--
-- 		-- Jeśli nie organizujemy holowania
-- 		IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @transportError = 0
-- 		BEGIN
-- 			SET @transportError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Warunkiem koniecznym do organizacji transportu samochodu do serwisu jest uprzednia realizacja holowania unieruchomionego pojazdu do autoryzowanego partnera serwisowego.',default)
-- 			WHERE id = 7
-- 		END
--
-- 		-------------------------------------------------------
-- 		-- KREDYT
-- 		-------------------------------------------------------
--
-- 		-- istnieje już kredyt
 		IF dbo.f_exists_in_split(@servicesIds,'9') = 1
 		BEGIN
 			SET @fixError = 1

 			UPDATE #availableServices
 			SET
 			active = 0,
 			message = dbo.f_translate('W systemie jest już zorganizowany kredyt. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
 			WHERE id = 9
 		END
		
		-- Zdarzenie w Polsce
		IF @eventLocationCountry = dbo.f_translate(dbo.f_translate('Polska',default),default) AND @loanError = 0
		BEGIN
			SET @loanError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Nie mamy możliwości zorganizowania usługi, jeśli pojazd znajduje się na terenie Polski. Jest to niezgodne z Ogólnymi Warunkami Programu',default)
			WHERE id = 9
		END 
		
		-- Jeśli nie organizujemy holowania
-- 		IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @loanError = 0
-- 		BEGIN
-- 			SET @loanError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Nie mamy możliwości zorganizowania kredytu jeżeli nie był holowany przez ',default)+@platformName+dbo.f_translate(' Assistance.',default)
-- 			WHERE id = 9
-- 		END
		
		-- TODO: Auto naprawiane w PS
		DECLARE @fixedAtSp INT
		SET @fixedAtSp = 1
		
		IF @fixedAtSp = 0 AND @loanError = 0
		BEGIN
			SET @loanError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Nie mamy możliwości zorganizowania kredytu jeżeli pojazd nie jest naprawiany u Partnera Serwisowego',default)
			WHERE id = 9
		END 
		
		-------------------------------------------------------
		-- POMOC PRAWNA I MEDYCZNA
		-------------------------------------------------------
		
		-- Tylko volksvagen
		IF @platformId <> 11 AND @medicalLawAdviceError = 0
		BEGIN
			SET @medicalLawAdviceError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Świadczenie dostępne tylko dla platformy VW',default)
			WHERE id = 11
		END 
		
		-- Tylko za granicą
		IF @eventLocationCountry = dbo.f_translate(dbo.f_translate('Polska',default),default) AND @medicalLawAdviceError = 0
		BEGIN
			SET @medicalLawAdviceError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Świadczenie przysługuje tylko jeśli lokalizacja zdarzenia jest za granicą.',default)
			WHERE id = 11
		END 
		
		-------------------------------------------------------
		-- TAXI
		-------------------------------------------------------
		
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('transport.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @taxiError = 1
			
			UPDATE #availableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 5
		END
		
		-- Brak w bazie + zamknięty serwis macierzysty
		IF @programNotFound = -1 AND @serviceOpened = 0 AND @taxiError = 0
		BEGIN
			
			-- organizowano już taxi
			DECLARE @taxiProcessesCount INT
			SELECT @taxiProcessesCount = COUNT(*) 
			FROM process_instance  with(nolock)
			WHERE step_id LIKE '1016.%'
			AND active = 1
			AND parent_id = @mainProcessId
			
			
			IF @taxiProcessesCount > 0
			BEGIN
				SET @taxiError = 1
			
				UPDATE #availableServices 
				SET
				active = 0,
				message = dbo.f_translate('W związku z tym, że uprawnienia pojazdu do korzystania z Assistance nie widnieją w systemie skontaktujemy się z odpowiednim Dealerem/Partnerem Serwisowym i potwierdzimy uprawnienia. Po potwierdzeniu uprawnień otrzyma Pan wiadomość z SMS z num',default)
				WHERE id = 5
			END
		END
		
		-- Zdarzenie za granicą
		IF @eventLocationCountry <> dbo.f_translate(dbo.f_translate('Polska',default),default) AND @taxiError = 0
		BEGIN
			-- klient sam organizuje i będzie ubiegał się o zwrot
			DECLARE @taxiSelfOrganisation INT
			IF @taxiSelfOrganisation = 1
			BEGIN
				SET @taxiError = 1
				
				UPDATE #availableServices 
				SET
				active = 0,
				message = dbo.f_translate('Klient będzie organizował samodzielnie z opcją ubiegania się o zwrot kosztów',default)
				WHERE id = 5
			END 
		END 
		
		IF ISNULL(@prefferedService,1) <> 2 AND @diagnosisSummary IN ('[N]','[PN]') AND dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @taxiError = 0
		BEGIN
			SET @taxiError = 1
				
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Świadczenie TAXI nie przysługuje w przypadku naprawy na drodze.',default)
			WHERE id = 5
		END 
	
		
		-------------------------------------------------------
		-- CZĘŚCI ZAMIENNE
		-------------------------------------------------------
			
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('parts.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			SET @partsError = 1
			
			UPDATE #availableServices
			SET
			active = 0,
			message = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance w zakresie wybranej usługi.',default)
			where id = 8
		END
		
-- 		-- istnieje już cz
 		IF dbo.f_exists_in_split(@servicesIds,'8') = 1 AND @partsError = 0
 		BEGIN
 			SET @partsError = 1

 			UPDATE #availableServices
 			SET
 			active = 0,
 			message = dbo.f_translate('W systemie jest już zorganizowany kredyt. Skorzystaj z anulowania poprzedniej usługi, aby zlecić nową organizację.',default)
 			WHERE id = 8
 		END
		
		-- Lokalizacja w Polsce i nie AUDI i usługa Holowania
 	 	
		IF @platformId <> 31 AND @eventLocationCountry = dbo.f_translate(dbo.f_translate('Polska',default),default) and @partsError = 0
		BEGIN
			set @partsError = 1
			
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Organizacja części zamiennych jest możliwa tylko jeżeli zdarzenie ma miejsce poza granicami Polski. W związku z tym nie mamy możliwości zorganizowania tej usługi dla Pana/Pani.',default)
			WHERE id = 8
		END 
		
		-- Jeśli nie organizujemy holowania
-- 		IF dbo.f_exists_in_split(@servicesIds,'1') = 0 AND @partsError = 0
-- 		BEGIN
-- 			SET @partsError = 1
--
-- 			UPDATE #availableServices
-- 			SET
-- 			active = 0,
-- 			message = dbo.f_translate('Nie mamy możliwości zorganizowania kredytu jeżeli nie był holowany przez ',default)+@platformName+dbo.f_translate(' Assistance.',default)
-- 			WHERE id = 8
--
-- 		END
		
		
		IF @partsError = 0
		BEGIN
			DECLARE @partsCostsAccepted INT
			
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '559', @groupProcessInstanceId = @mainProcessId, @rootProcessInstanceId = @mainProcessId
			SELECT @partsCostsAccepted = MAX(value_int) FROM @values
			
--			IF @partsCostsAccepted = 0 
--			BEGIN
--				set @partsError = 1
--			
--				UPDATE #availableServices 
--				SET
--				active = 0,
--				message = dbo.f_translate('Organizacja Części zamiennych jest możliwa tylko jeżeli zgodzi się Pan/Pani na pokrycie ich kosztów. W związku z brakiem zgody nie mamy możliwości zorganizowania tej usługi dla Pana/Pani',default)
--				WHERE id = 8
--			END 
			
		END 
		
		-------------------------------------------------------
		-- HOLOWANIE POSZKODOWANEGO
		-------------------------------------------------------
		
		IF @eventType <> 1
		BEGIN
--			SET @victimCarError = 1
			UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Nie mamy możliwości zorganizowania holowania poszkodowanego pojazdu jeśli nie brał udziału w wypadku.',default)
			WHERE id = 15
		END 
		
		
		-- ===============================
		-- Dodatkowe warunki dla platformy FORD
		-- ===============================
		
		IF @platformId = 2
		BEGIN
			
			EXEC [dbo].[p_available_services_ford]
				@processInstanceId = processInstanceId,
				@rootId = @mainProcessId,
				@stepId = @stepId

		END
		
		
		
	END
	
	
	PRINT 'AFTER P_Available_Services'
	PRINT dbo.f_translate('Variant:',default)
	PRINT @variant
	PRINT '---'
	
	if exists (
		select id from dbo.process_instance with(nolock) where step_id in ('1021.018','1021.017','1021.015') and active=1 and root_id=@mainProcessId
	)
	UPDATE #availableServices 
			SET
			active = 0,
			message = dbo.f_translate('Monitoring usługi warsztatowej jest aktywny.',default)
			WHERE id = 12
	
	if exists(
		select	pin.group_process_id
		from	dbo.process_instance pin with(nolock) inner join			
				dbo.service_status ss with(nolock) on ss.group_process_id = pin.group_process_id inner join
				dbo.service_status_dictionary ssd with(nolock) on ssd.id = ss.status_dictionary_id 					 
		where	root_id=@mainProcessId and ss.serviceId=12 and ssd.progress = 4
	)
	UPDATE #availableServices 
	SET
	active = 0,
	message = dbo.f_translate('Naprawa warsztatowa zakończona',default)
	WHERE id = 12
	
			
	-- anulowanie świadczenia
	UPDATE #availableServices SET active = 0, variant = NULL where id = 13

	if exists(
		select	pin.group_process_id
		from	dbo.process_instance pin with(nolock) inner join
				dbo.service_definition sd with(nolock) on left(sd.start_step_id,4)=left(step_Id,4) inner join 
				dbo.service_definition_translation st with(nolock) on st.translatable_id=sd.id and st.locale='pl' inner join
				(
		
					select	ss.group_process_id, ss.serviceId,max(ssd.progress) progress
					from	dbo.service_status ss with(nolock) inner join
							dbo.service_status_dictionary ssd with(nolock) on ssd.id=ss.status_dictionary_id 
					group by ss.group_process_id, ss.serviceId
		
				) ssd1 on ssd1.group_process_id=pin.group_process_id and ssd1.serviceId=sd.id and ssd1.progress in (1,2,3,4) 
		where	root_id=@mainProcessId and sd.id<>13
	
	)
	begin
		UPDATE #availableServices SET active = 1, variant = NULL where id = 13
	end
	
	-------------------------------------------


	IF @init = 1
	BEGIN
		SELECT * FROM #availableServices ORDER BY pos
	END 
	
	PRINT '------------------ END p_available_services'
	
END
