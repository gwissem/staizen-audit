ALTER PROCEDURE [dbo].[p_auto_rental_offers_transfer]
( 
	@groupProcessInstanceId int,
	@winnerGroupId int
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @platformId INT 
	DECLARE @subGroupId int 
	DECLARE @makeModelId int 
	DECLARE @minutes int 
	DECLARE @correctOffer int 
	declare @customerMakeModelId int 
	declare @offerGroupId int 
	declare @offerId int 
	DECLARE @newProcessInstanceId int 
	DECLARE @rootId INT 
	DECLARE @sourceId INT 
	DECLARE @targetId INT 
	DECLARE @processInstanceIds NVARCHAR(255) 
	DECLARE @dayCost DECIMAL(10,2)
	DECLARE @days INT 
	DECLARE @status INT = 0
	DECLARE @partnerId int 
	DECLARE @content nvarchar(max)
	DECLARE @subject nvarchar(1000)
	DECLARE @starterEmail nvarchar(255)
	DECLARE @email nvarchar(255)
	DECLARE @offerDone INT 
	declare @reason nvarchar(255)
	declare @reasonId int 

	set @starterEmail = dbo.f_getEmail('rentals')
	
	DECLARE @platformGroup nvarchar(255)	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		SET @starterEmail = dbo.f_getEmail('cfm')
	END
	
	SELECT @subject = dbo.f_translate('Rozstrzygnięcie wyboru oferty wynajmu pojazdu zastępczego dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
	
	SELECT @rootId = root_id FROM dbo.process_instance with(nolock) where id = @groupProcessInstanceId
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	declare kur cursor LOCAL for
	 
		 select distinct pin.group_process_id
		 from dbo.attribute_value offer with(nolock)
		 inner join dbo.process_instance pin with(nolock) on pin.group_process_id = offer.group_process_instance_id
		 where pin.parent_id = @groupProcessInstanceId
		 and offer.attribute_path = '764'
	 
	 OPEN kur;
	 FETCH NEXT FROM kur INTO @offerGroupId;
	 WHILE @@FETCH_STATUS=0
	 BEGIN	 	
		set @reasonId = null 
		set @reason = null 
		set @offerDone = null 
		set @partnerId = null 
		
		 IF @offerGroupId <> isnull(@winnerGroupId,-1)  
		 BEGIN
			SELECT top 1 @offerDone = value_int from dbo.attribute_value with(nolock) where group_process_instance_id = @offerGroupId and attribute_path = '764,999'

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '764,136', @groupProcessInstanceId = @offerGroupId
			SELECT @reasonId = value_int FROM @values
			
			IF @offerDone is null
			BEGIN
				-- powód nie wybrania oferty			
				EXEC p_attribute_edit
				@attributePath = '764,136', 
				@groupProcessInstanceId = @offerGroupId,
				@stepId = 'xxx',
				@valueInt = 7,
				@err = @err OUTPUT,
				@message = @message OUTPUT				
			END 
			ELSE IF @offerDone = 1 AND @winnerGroupId IS NOT NULL
			BEGIN
				
					SELECT top 1 @partnerId = value_int from dbo.attribute_value with(nolock) where group_process_instance_id = @offerGroupId and attribute_path = '764,742'
	
					IF @reasonId IS NOT NULL 
					BEGIN						
						SELECT top 1 @reason = textD from dbo.dictionary where typeD = 'rentalOfferRefuse' and value = @reasonId 
		
						SELECT @content = 'Szanowni Państwo,<br/>Dziękujemy za złożenie oferty wynajmu dla sprawy '+dbo.f_caseId(@groupProcessInstanceId)+'. Oferta nie została przyjęta ze względu na:<br>- '+@reason+'.'
				   		SELECT @email = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 3, 4)	
					
				   		EXECUTE dbo.p_note_new 
						 @groupProcessId = @groupProcessInstanceId
						,@type = dbo.f_translate('email',default)
						,@content = @content
						,@email = @email
						,@userId = 1  -- automat
						,@subject = @subject
						,@direction=1
						,@dw = ''
						,@udw = ''
						,@sender = @starterEmail
						,@emailBody = @content
						,@emailRegards = 1
						,@err=@err OUTPUT
						,@message=@message OUTPUT
					
				END
			END 
		 END 
		 
		 EXEC [dbo].[p_add_rental_offer] @instanceId = @offerGroupId, @auto = 1
	
	 FETCH NEXT FROM kur INTO @offerGroupId;
	 END
	 CLOSE kur
	 DEALLOCATE kur
	 
	 IF @winnerGroupId IS NOT NULL
	 BEGIN
		
		 EXEC dbo.p_process_new @stepId = '1007.003', @userId = 1, @originalUserId = 1, @err = @err, @groupProcessId = @groupProcessInstanceId, @message = @message, @parentProcessId = @rootId, @rootId = @rootId, @processInstanceId = @newProcessInstanceId OUTPUT
		 
		 EXEC [dbo].[p_form_controls]
			@instance_id = @newProcessInstanceId,
			@returnResults = 0,
			@returnWithNote = 0
			
		 SELECT @sourceId = id from dbo.attribute_value with(nolock) where attribute_path = '764' and group_process_instance_id = @winnerGroupId
		 SELECT @targetId = id from dbo.attribute_value with(nolock) where attribute_path = '764' and group_process_instance_id = @groupProcessInstanceId
		 
		 EXEC [dbo].[P_attribute_copy_structure]
			@source_attribute_value_id = @sourceId,
			@target_attribute_value_id = @targetId,
			@process_instance_id = @newProcessInstanceId
			
		 -- 1007.042
	     EXEC [dbo].[p_process_next]
	        @previousProcessId = @newProcessInstanceId,
	        @err = @err OUTPUT,
	        @message = @message OUTPUT,
	        @processInstanceIds = @processInstanceIds OUTPUT
		 
	     SET @newProcessInstanceId = @processInstanceIds
	     
	     EXEC [dbo].[p_form_controls]
			@instance_id = @newProcessInstanceId,
			@returnResults = 0,
			@returnWithNote = 0
			
	     -- 1007.043
         EXEC [dbo].[p_process_next]
	        @previousProcessId = @newProcessInstanceId,
	        @err = @err OUTPUT,
	        @message = @message OUTPUT,
	        @processInstanceIds = @processInstanceIds OUTPUT
		                   
         SET @newProcessInstanceId = @processInstanceIds
         
         EXEC [dbo].[p_form_controls]
			@instance_id = @newProcessInstanceId,
			@returnResults = 0,
			@returnWithNote = 0
		 	
         SELECT @dayCost = daycost.value_decimal, @days = days.value_int 
         FROM dbo.attribute_value daycost with(nolock)
         inner join dbo.attribute_value days with(nolock) on days.group_process_instance_id = daycost.group_process_instance_id and days.attribute_path = '789,786,240,153'
         where daycost.attribute_path = '789,786,240,788'
         and daycost.group_process_instance_id = @groupProcessInstanceId
         
         
         IF ISNULL(@dayCost,0.00) = 0.00 OR ISNULL(@days,0) = 0
         BEGIN
	     	 SET @status = 2
	         SELECT @content = 'Szanowni Państwo,<br/>Dziękujemy za złożenie oferty wynajmu dla sprawy '+dbo.f_caseId(@groupProcessInstanceId)+dbo.f_translate('. Oferta została przyjęta. Za chwilę skontaktuje się z Państwem konsultant w celu potwierdzenia kosztów wynajmu.',default)
	         
        	 SELECT top 1 @partnerId = value_int from dbo.attribute_value where group_process_instance_id = @winnerGroupId and attribute_path = '764,742'
 			 SELECT @email = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 3, 4)	
	 
	 		 EXECUTE dbo.p_note_new 
			 @groupProcessId = @groupProcessInstanceId
			,@type = dbo.f_translate('email',default)
			,@content = @content
			,@email = @email
			,@userId = 1  -- automat
			,@subject = @subject
			,@direction=1
			,@dw = ''
			,@udw = ''
			,@sender = @starterEmail
			,@emailBody = @content
			,@emailRegards = 1
			,@err=@err OUTPUT
			,@message=@message OUTPUT
			
         END
         ELSE
         BEGIN
	         
	         -- GOP
	         EXEC [dbo].[p_process_next]
		        @previousProcessId = @newProcessInstanceId,
		        @err = @err OUTPUT,
		        @message = @message OUTPUT,
		        @processInstanceIds = @processInstanceIds OUTPUT
		        
        	 SET @status = 1
        	 
         END 
         
	 END 
	 		
		   		
	 EXEC [dbo].[p_attribute_edit]
	    @attributePath = '288', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = @status,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
		
END



	 
