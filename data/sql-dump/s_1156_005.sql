ALTER PROCEDURE [dbo].[s_1156_005]
  (
    @previousProcessId INT,
    @variant           TINYINT OUTPUT,
    @currentUser int,
    @errId             int = 0 output
  )
AS
  BEGIN
    SET @variant = 1
    DECLARE @userId INT
    DECLARE @originalUserId INT
    DECLARE @rootId INT
    DECLARE @err INT
    DECLARE @message NVARCHAR(255)
    DECLARE @newProcessId INT
    DECLARE @nextProcessInstances varchar(4000)
    DECLARE @groupProcessInstanceId INT
    DECLARE @conditionalAssistance INT


    SELECT @groupProcessInstanceId = group_process_id,
           @userId = created_by_id,
           @rootId = root_id,
           @originalUserId = created_by_original_id
    FROM process_instance with(nolock)
    WHERE id = @previousProcessId


    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))

    DECLARE @companyUsingCar nvarchar(500)
    DECLARE @regNumber nvarchar(20)

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @regNumber = value_string from @values

    DELETE FROM @values

    DECLARE @platformId int
    DECLARE @platformName NVARCHAR(100)
    INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int from @values
    SELECT @platformName = name from AtlasDB_def.dbo.platform where id = @platformId


    DECLARE @aldFound int
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '999', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @aldFound = value_int from @values


    IF @platformId = 43 and isnull(@aldFound, 0) = 1
      BEGIN

        SET @variant = 2
        return
      end

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1000', @groupProcessInstanceId = @groupProcessInstanceId

    SELECT @companyUsingCar = value_string from @values

    DECLARE @mailTo nvarchar(500)
    DECLARE @valueMailConfig NVARCHAR(255)
    DECLARE @descriptionMailConfig NVARCHAR(255)
    EXEC dbo.p_get_business_config
        @key = 'bwb.request_veryfication_email',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @value = @valueMailConfig OUTPUT,
        @description = @descriptionMailConfig OUTPUT


    IF @platformId = 53
      BEGIN
        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('serwis@alphabet.pl') + ', ' +
                         [dbo].[f_getRealEmailOrTest]('szkody@alphabet.pl')
      end
    ELSE IF @platformId = 25
      BEGIN
        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('serwisyfa@leaseplan.pl')
      end
    ELSE IF @platformId = 48
      BEGIN

        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('serwis@businesslease.pl') + ', ' +
                         [dbo].[f_getRealEmailOrTest]('szkody@businesslease.pl')
      end
    ELSE IF @platformId = 43
      BEGIN

        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('assistance.pl@aldautomotive.com') + ', ' +
                         [dbo].[f_getRealEmailOrTest]('adrianna.adamowicz@aldautomotive.com')
      end
    else IF @platformId = 78
      BEGIN


        EXEC p_attribute_edit @attributePath = '656',
                              @groupProcessInstanceId = @groupProcessInstanceId,
                              @userId = @userId,
                              @stepId = 'xxx',
                              @valueString = '0713777555',
                              @err = @err output,
                              @message = @message out

        DECLARE @eventType int
        DELETE from @values
        INSERT into @values EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessInstanceId,
                                                  @attributePath = '491'
        SELECT @eventType = value_int from @values

        DELETE from @values


        IF @eventType in (2, 7)
          BEGIN
            --szkoda
            SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('szkody@carefleet.com.pl')
          end
        else
          BEGIN
            SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('cok@carefleet.com.pl')
          end
      END


    IF @valueMailConfig <> ''
      BEGIN
        SET @mailTo = dbo.f_getRealEmailOrTest(@valueMailConfig)
      END

    DECLARE @subject nvarchar(500)
    DECLARE @body nvarchar(MAX)

    SET @subject = isnull(@regNumber, '') + dbo.f_translate('  BRAK W BAZIE PROŚBA O POTWIERDZENIE UPRAWNIEŃ',default)

    SET @body = 'Szanowni Państwo <BR>' +
                dbo.f_translate('skontaktował się z nami użytkownik pojazdu ',default) + isnull(@regNumber, '') +
                '. Prosi o wezwanie pomocy assistance. <BR>' +
                dbo.f_translate('Pojazd nie widnieje w bazie uprawnień. Użytkownik jest z firmy ',default) + isnull(@companyUsingCar, '') +
                '. <BR>' +
                'Prosimy o potwierdzenie możliwości organizacji pomocy oraz przekazanie danych pojazdy (w tym VIN). '


    IF @platformId = 78
      BEGIN

        SET @subject = dbo.f_translate('Brak w bazie - ',default) + dbo.f_caseId(@previousProcessId) + ' / ' + isnull(@regNumber, 0)
        DECLARE @companyLocationString nvarchar(400)
        EXEC p_location_mini_string @attributePath = '', @groupProcessInstanceId = @groupProcessInstanceId,
                               @locationString = @companyLocationString output

        SET @body = 'Nie zweryfikowano uprawnień dla pojazdu {@74,73@}, {@74,72@}. <br /> <br />
Informacje o sprawie: <br />
Imię i nazwisko: {@80,342,64@} {@80,342,66@} <br />
Nr telefonu: {@80,342,408,197@}  <br />
Nazwa firmy: ' + isnull(@companyUsingCar, '') + '<br />
Adres firmy: ' + isnull(@companyLocationString, '') + ' <br />
Notatki: {@63@} <br />
Zorganizowane usługi:  <br />'
                    +
                    '<br />' +
                    '<br />' +
                    dbo.f_translate('Uprzejmie prosimy o zweryfikowanie uprawnień pojazdu do korzystania z Assistance oraz potwierdzenie programu, którym powinien być objęty pojazd. ',default)

        DECLARE @parsed NVARCHAR(MAX)
        exec P_parse_string @processInstanceId = @previousProcessId, @simpleText = @body, @parsedText = @parsed OUTPUT
        select @body = isnull(@parsed, '')


        SET @variant = 2
      end

    DECLARE @content NVARCHAR(100) = dbo.f_translate('Wysłano zapytanie o BWB do ',default)

    SET @content = @content + @platformName


    DECLARE @dwd nvarchar(200) = ''

    IF @platformId = 48
      BEGIN
        SELECT @dwd = dbo.f_getRealEmailOrTest('assistance@busienesslease.pl')
      end

    EXEC p_note_new
        @groupProcessId = @groupProcessInstanceId,
        @type = dbo.f_translate('email',default),
        @content = @content,
        @phoneNumber = NULL,
        @dw = @dwd,
        @email = @mailTo,
        @subject = @subject,
        @direction = 1,
        @emailRegards = 1,
        @err = @err OUTPUT,
        @message = @message OUTPUT,
        -- FOR EMAIL
        @udw = '',
        @sender = 'cfm@starter24.pl',
        @additionalAttachments = '',
        @emailBody = @body



    IF @variant != 2
      BEGIN
        -- confirm step
        EXEC [dbo].[p_process_new]
            @stepId = '1156.007',
            @userId = 1,
            @originalUserId = 1,
            @skipTransaction = 1,
            @parentProcessId = @rootId,
            @rootId = @rootId,
            @groupProcessId = @groupProcessInstanceId,
            @err = @err OUTPUT,
            @message = @message OUTPUT,
            @processInstanceId = @newProcessId OUTPUT
END
  END