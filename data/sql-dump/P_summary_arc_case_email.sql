ALTER PROCEDURE [dbo].[P_summary_arc_case_email]
@rootId INT,
@overrideEmail NVARCHAR(100) = NULL
AS
begin
	
	PRINT '----------------------- EXEC dbo.P_summary_arc_case_email --------------------------'
	
	---------------------------------------------------------------------------------------------
	
	-- PROCEDURA WYSYŁA EMAIL JAKO PODSUMOWANIE SPRAWY DO PARTNERA ZAJAKOSGRACZNICNEGO ----
	
	---- !!!!!!!!!!!!!!!!!!!!!!!!!!!!  NIE PLATFORMY MERCEDES JEST INNA FORMATKA! P_summary_mercedes_case_email --------
	
	---------------------------------------------------------------------------------------------
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	 
	DECLARE @contentTitle NVARCHAR(200)
	
	DECLARE @caseNumberAtlas NVARCHAR(30)
	DECLARE @caseNumberPZ NVARCHAR(30)
	DECLARE @programName NVARCHAR(100)
	DECLARE @programId NVARCHAR(30)
	DECLARE @fdds INT
	DECLARE @createdDateCase DATETIME
	DECLARE @makeModel NVARCHAR(100)
	DECLARE @makeModelId INT
	DECLARE @regNumber NVARCHAR(100)
	DECLARE @vin NVARCHAR(20)
	DECLARE @mileage INT
	DECLARE @diagnosisDescription NVARCHAR(400)
	DECLARE @diagnosisCode NVARCHAR(20)
	DECLARE @emailContent NVARCHAR(MAX)
	DECLARE @pzName NVARCHAR(200)
	DECLARE @pzId INT
	DECLARE @ringGroupProcess INT
	DECLARE @contactEmailPz NVARCHAR(100)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @rootId
 	SELECT @pzId = value_int FROM @values
	
 	IF @pzId IS NULL
 	BEGIN
	 	
		SELECT @ringGroupProcess = group_process_id FROM dbo.process_instance WHERE root_id = @rootId AND step_id like '1012%'
		
		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @ringGroupProcess
		SELECT @pzId = value_int FROM @values
		 	
 	END
 	
 	SET @pzName = dbo.f_partnerName(@pzId)
 	
 	SET @contactEmailPz = [dbo].[f_partner_contact](@rootId, @pzId, 0, 4)
 	
	--	1. Numery sprawy – partnera i Starter24

	SET @caseNumberAtlas = dbo.[f_caseId](@rootId)
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '711', @groupProcessInstanceId = @rootId
 	SELECT @caseNumberPZ = value_string FROM @values
 
	--	2. Data i godzina zarejestrowania sprawy

 	SELECT TOP 1 @createdDateCase = created_at FROM dbo.process_instance WHERE root_id = @rootId ORDER BY id ASC
 		
	--	3. Program – nazwa i numer FDDS
 		
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
 	SELECT @programName = [dbo].[f_program_names](value_string) FROM @values
 	
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '712', @groupProcessInstanceId = @rootId
 	SELECT @fdds = value_string FROM @values
 	
	--	4. Pojazd – marka i model, nr rejestracyjny, VIN, przebieg

 	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @rootId
	SELECT @makeModelId = value_int FROM @values
	
 	SELECT @makeModel = textD FROM dbo.dictionary
	WHERE typeD='makeModel' AND value = @makeModelId AND active = 1
	
 	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @rootId
	SELECT @VIN = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @rootId
	SELECT @regNumber = value_string FROM @values
 	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @rootId
	SELECT @mileage = value_int FROM @values
	
	--	5. Zdarzenie – opis diagnozy EN i kod ARC
	
	SET @diagnosisDescription = [dbo].[f_diagnosis_description](@rootId, 'en')
	SET @diagnosisCode = [dbo].[f_diagnosis_code](@rootId)
	
	--	6. Usługi (te, które nie zostały anulowane)
	
		--	a. Naprawa – data i godzina przybycia na miejsce, data i godzina zakończenia, wynik (udana, nieudana)
		--	b. Holowanie - data i godzina przybycia na miejsce (pierwsze holowanie), data i godzina zakończenia (ostatniego holowania), liczba km holowania, adres docelowy (ostatniego holowania; jeśli serwis, także nazwa)
		--	c. Auto zastępcze - data i godzina rozpoczęcia wynajmu, data i godzina zakończenia, marka i model auta zastępczego, liczba dni, koszt całkowity brutto
		--	d. Nocleg – nazwa i adres hotelu, data rozpoczęcia noclegu, data zakńczenia, ilość dób, koszt całkowity brutto
		--	e. Taxi – liczba km, koszt całkowity brutto
		--	f. Parking – liczba dni, koszt całkowity brutto
		--	g. Podróż – data początku i końca podróży, miejsce początkowe podróży, miejsce docelowe poróży, całkowity koszt brutto
		--	h. Inne – wymienione po kolei – nazwa i koszt całkowity brutto
	
	
--	{#NICE <span style="color: #5caad2;">TITLE</span>#}

	SET @contentTitle = 'Feedback form to <br><span style="color: #526197;">' + ISNULL( @pzName, '') + '</span>'
	
	-- Pobranie Kontentu e-maila
	EXEC [dbo].[P_get_email_template] @name = 'modern_email', @responseText = @emailContent OUTPUT
	
	DECLARE @content NVARCHAR(MAX)
	
	----------------------------------
	-- Początek budowania kontentu
	---------------------------------- 
	
	SET @content = '<table style="font-size: 18px;width: 100%;border-collapse: collapse;" border="1"><tbody>';
	
	DECLARE @tr_ NVARCHAR(500) = '<tr><td style="padding: 5px; width: 50%;">__KEY__</td><td style="padding: 5px;">__VALUE__</td></tr>';
	
	--	1. Numery sprawy – partnera i Starter24

	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'Your case number'),'__VALUE__', ISNULL(@caseNumberPZ, ''));
	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'Our case number'), '__VALUE__', ISNULL(@caseNumberAtlas, ''));
		
	--	2. Data i godzina zarejestrowania sprawy

	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'Datatime created of case'), '__VALUE__', [dbo].[FN_VDateHour](@createdDateCase));

	--	3. Program – nazwa i numer FDDS

	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'Program name'), '__VALUE__', ISNULL(@programName, ''));
 
	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'FDDS number'), '__VALUE__', ISNULL(@fdds, ''));

	--	4. Pojazd – marka i model, nr rejestracyjny, VIN, przebieg

	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'Car'), '__VALUE__', ISNULL(@makeModel, ''));
	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'VIN'), '__VALUE__', ISNULL(@VIN, ''));
	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'Reg. plate:'), '__VALUE__', ISNULL(@regNumber, ''));
	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'Mileage'), '__VALUE__', ISNULL(@mileage, ''));

	--	5. Zdarzenie – opis diagnozy EN i kod ARC
	
	SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', 'Diagnosis (Arc code)'), '__VALUE__', CONCAT(@diagnosisDescription, ' (', @diagnosisCode, ')'));

	-- Zakończenie głównych informacji
	SET @content = @content + '</tbody></table>';

	PRINT '---------------------------------------------------------------'
	PRINT '@content before CURSOR'
	PRINT @content
	PRINT '---------------------------------------------------------------'
	
	SET @content = @content + '<p><br><span style="font-size: 20px; color: #526197;">Services:</span></p>'
	
	-- PĘTLA PO AKTYWNYCH USŁUGACH
	
	DECLARE @servicesCursor as CURSOR;
	DECLARE @servicesDataCursor as CURSOR;
	DECLARE @servicesIds NVARCHAR(100)
	DECLARE @serviceId INT
	DECLARE @groupProcessOfService INT
	DECLARE @serviceName NVARCHAR(200)
	DECLARE @description_en NVARCHAR(500)
	DECLARE @valueText NVARCHAR(500)
	
	CREATE TABLE #serviceRows (attr_path NVARCHAR(255), value_text NVARCHAR(MAX), description_pl NVARCHAR(MAX), description_en NVARCHAR(MAX))	
	
	EXEC [dbo].[p_running_services] @groupProcessInstanceId = @rootId, @servicesIds = @servicesIds OUTPUT

	SET @servicesCursor = CURSOR FOR
	SELECT data FROM dbo.f_split(@servicesIds,',') WHERE data <> ''
	
	OPEN @servicesCursor;
	    FETCH NEXT FROM @servicesCursor INTO @serviceId;
	    WHILE @@FETCH_STATUS = 0
	        BEGIN
		        
		        PRINT '@serviceId'
		        PRINT @serviceId
		        
		        PRINT '@groupProcessOfService'
	        	PRINT @groupProcessOfService
	        	
	        	SET @groupProcessOfService = [dbo].[f_service_top_progress_group](@rootId, @serviceId)
	        	
	        	DELETE FROM #serviceRows
				EXEC [dbo].[P_data_of_service] @serviceId = @serviceId, @groupProcessInstanceId = @groupProcessOfService
	        	
				IF (SELECT count(attr_path) FROM #serviceRows) > 0
				BEGIN
					
					-- Zapisanie nazwy usługi
					
		        	SELECT @serviceName = name FROM AtlasDB_def.dbo.service_definition_translation WHERE translatable_id = @serviceId and locale = 'en'
		        	SET @content = @content + '<br><p><span style="font-size: 18px; color: #000;">' + @serviceName + ':</span></p><br>'
		        	
					-- KURSOR PO INFORMACJACH O USŁUDZE
	
					SET @content = @content + '<table style="font-size: 18px;width: 100%;border-collapse: collapse;" border="1"><tbody>';
		
					SET @servicesDataCursor = CURSOR FOR
					SELECT value_text, description_en FROM #serviceRows
					OPEN @servicesDataCursor;
					    FETCH NEXT FROM @servicesDataCursor INTO @valueText, @description_en;
					    WHILE @@FETCH_STATUS = 0
						BEGIN
							
						SET @content = @content + REPLACE(REPLACE(@tr_,'__KEY__', @description_en), '__VALUE__', ISNULL(@valueText, ''));
							
						FETCH NEXT FROM @servicesDataCursor INTO @valueText, @description_en;
					END
					
					SET @content = @content + '</tbody></table>';
		        	
				END
				
				
				PRINT '---------------------------------------------------------------'
				PRINT '@content on END CURSOR'
				PRINT @content
				PRINT '---------------------------------------------------------------'
			
	    FETCH NEXT FROM @servicesCursor INTO @serviceId;
	END

	DROP TABLE #serviceRows
	
	----------------------------------
	-- Zakończenie tabelki i kontentu
	----------------------------------
	
	
	SET @emailContent = REPLACE(@emailContent,'__CONTENT_TITLE__', @contentTitle)
    SET @emailContent = REPLACE(@emailContent,'___TITLE___','Title')    
    SET @emailContent = REPLACE(@emailContent,'__CONTENT__', @content)
    
    PRINT '@contentTitle'
    PRINT @contentTitle
    
--    PRINT @content
--    SELECT @emailContent

	--  Stworzenie nowego procesu Przychodzi Mail

	 DECLARE @email VARCHAR(400)
	 DECLARE @sender VARCHAR(400) = dbo.f_getEmail('arc')
	 DECLARE @subject VARCHAR(400)
	 					
	 SET @subject = dbo.f_translate('Feedback form of case ',default) + @caseNumberAtlas + '.'
 
	 IF @overrideEmail IS NOT NULL
	 BEGIN
		 SET @contactEmailPz = @overrideEmail
	 END
	 
	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @rootId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = dbo.f_translate('Feedback form to PZ.',default)
	 	,@email = @contactEmailPz
	 	,@userId = 1  -- automat
	 	,@subject = @subject
	 	,@direction = 1
	 	,@sender = @sender
	 	,@emailBody = @emailContent
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT
		
	
	PRINT '---------------------- END dbo.P_summary_arc_case_email -------------------------------'
	
end
