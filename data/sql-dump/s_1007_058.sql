ALTER PROCEDURE [dbo].[s_1007_058]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @groupProcessInstanceId INT
	DECLARE @extraCosts INT
	DECLARE @rootId INT
	DECLARE @autoAccepted INT
	DECLARE @stepId NVARCHAR(20)
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @c NVARCHAR(MAX)
	DECLARE @p1 VARCHAR(255)
	DECLARE @partnerId INT
	DECLARE @partnerNip NVARCHAR(100)
	DECLARE @accepted INT
	declare @info nvarchar(max)
	declare @newProcessId int 
	SELECT @groupProcessInstanceId = group_process_id , @stepId = step_id, @rootId = root_id
	FROM dbo.process_instance with(nolock) WHERE id = @previousProcessId
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	SELECT @partnerNip = av.value_string
	FROM dbo.attribute_value av with(nolock)
	INNER JOIN dbo.attribute_value av2 with(nolock) ON av2.parent_attribute_value_id = av.parent_attribute_value_id AND av2.attribute_path = '595,597'
	WHERE av.attribute_path = '595,83'
	AND av2.id = @partnerId
	
	SET @variant = 1
	
	EXEC p_verify_replacement_car_costs
	@groupProcessInstanceId = @groupProcessInstanceId,
	@accepted = @accepted OUTPUT,
	@info = @info output
	
	SET @c = 'Automat zweryfikował dane wynajmu negatywnie ['+isnull(@info,'')+'] Zadanie ręcznej weryfikacji kosztów'
	
	IF ISNULL(@accepted,0) IN (-1,1)
	BEGIN
		SET @variant = 2	
		-- Potwierdź koszty
		IF @accepted = 1
		BEGIN
			SET @c = dbo.f_translate('Automat zweryfikował dane wynajmu pozytywnie. Wysyłam OGOP',default)
			
			EXEC p_attribute_edit
			@attributePath = '690', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueString = dbo.f_translate('Wypożyczalnia',default),
			@err = @err OUTPUT,
			@message = @message OUTPUT
		END 
		ELSE IF @accepted = -1
		BEGIN
			SET @c = dbo.f_translate('Wypożyczalnia nie uzupełniła RZW. Automatyczne zamknięcie.',default)
			
			declare @targetId int 
			declare @sourceId int 
	
			SELECT TOP 1 @sourceId = id FROM dbo.attribute_value with(nolock) WHERE group_process_instance_id = @groupProcessInstanceId and attribute_path = '789' order by id desc
			SELECT TOP 1 @targetId = id FROM dbo.attribute_value with(nolock) WHERE group_process_instance_id = @groupProcessInstanceId and attribute_path = '812,789' order by id desc
			
			EXEC [dbo].[P_attribute_copy_structure]
			@source_attribute_value_id = @sourceId,
			@target_attribute_value_id = @targetId,
			@process_instance_id = @previousProcessId
			
			IF NOT EXISTS( SELECT id from dbo.process_instance where step_id = '1007.060' and group_process_id = @groupProcessInstanceId)
			BEGIN
				
				EXEC [dbo].[p_process_new]
				@stepId = '1007.060',
				@userId = 1,
				@originalUserId = 1,
				@parentProcessId = @rootId,
				@rootId = @rootId,
				@groupProcessId= @groupProcessInstanceId,
				@err = @err OUTPUT,
				@message = @message OUTPUT,
				@processInstanceId = @newProcessId OUTPUT
				
				UPDATE dbo.process_instance set active = 0 where id = @newProcessId
				
				EXEC p_attribute_edit
				@attributePath = '690', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueString = dbo.f_translate('Odgórne zamknięcie',default),
				@err = @err OUTPUT,
				@message = @message OUTPUT
			END 
			
			EXEC [dbo].[p_attribute_edit]
		    @attributePath = '692', 
		    @groupProcessInstanceId = @groupProcessInstanceId,
		    @stepId = 'xxx',
		    @userId = 1,
		    @originalUserId = 1,
		    @valueInt = 1,
		    @err = @err OUTPUT,
		    @message = @message OUTPUT
		    
			EXEC [dbo].[p_attribute_edit]
		    @attributePath = '812,63', 
		    @groupProcessInstanceId = @groupProcessInstanceId,
		    @stepId = 'xxx',
		    @userId = 1,
		    @originalUserId = 1,
		    @valueText = 'OK (zamknięte odgórnie)',
		    @err = @err OUTPUT,
		    @message = @message OUTPUT
		END 				
	END
	ELSE
	BEGIN
		IF SYSTEM_USER = dbo.f_translate('andrzej.dziekonski',default)
		 BEGIN
		 	SELECT @info
		 END
		
		EXEC p_attribute_edit
		@attributePath = '674', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueText = @info,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	END 
	
--	IF @partnerNip IN (SELECT nip FROM dbo.rental_price_list_usage)
--	BEGIN
--		EXEC p_verify_replacement_car_costs
--		@groupProcessInstanceId = @groupProcessInstanceId,
--		@accepted = @accepted OUTPUT
--		
--		IF @accepted = 1
--		BEGIN
--			SET @variant = 2	
--			-- Potwierdź koszty
--			SET @c = dbo.f_translate('Automat zweryfikował dane wynajmu pozytywnie. Wysyłam OGOP',default)
--			
--		END	
--	END 
--	ELSE
--	BEGIN
--		SET @c = dbo.f_translate('Brak zaszytego cennika. Zadanie ręcznej weryfikacji kosztów',default)
--	END 
--	

	-- LOGGER START
	SET @p1 = dbo.f_caseId(@rootId)
	EXEC dbo.p_log_automat
	@name = 'rental_automat',
	@content = @c,
	@param1 = @p1
	-- LOGGER END
	
END 

