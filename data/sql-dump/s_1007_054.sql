ALTER PROCEDURE [dbo].[s_1007_054]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @platformName NVARCHAR(250)
	DECLARE @fixingEndDate DATETIME
	DECLARE @partnerId INT
	DECLARE @fixingPartnerId INT
	DECLARE @daysLeft INT
	DECLARE @endDate DATETIME
	DECLARE @duration INT
	DECLARE @clientPhone NVARCHAR(255)
	DECLARE @platformId INT
	DECLARE @makeModel NVARCHAR(255)
	DECLARE @smsContent NVARCHAR(4000)
	DECLARE @emailContent NVARCHAR(MAX)
	DECLARE @rzwHash NVARCHAR(40)
	DECLARE @email NVARCHAR(100)
	DECLARE @subject NVARCHAR(300)
	DECLARE @rootId INT
	DECLARE @workshopGroup INT
	DECLARE @sourceId INT
	DECLARE @targetId INT
	DECLARE @c NVARCHAR(500)
	DECLARE @p1 NVARCHAR(100)
	DECLARE @offerMakeModelId INT
	DECLARE @eta DATETIME
	DECLARE @override INT
	DECLARE @fixEndDaterentalDuration INT
	declare @rentalDuration INT
	declare @postFactum int
	declare @platformGroup nvarchar(255)
	declare @sendNotice int
	declare @previousGroupInstanceId int 
	declare @rentalGroup int 
	declare @pickUpLocation nvarchar(1000)
	declare @dropOffLocation nvarchar(1000)
	
	SELECT @groupProcessInstanceId = pin.group_process_id, @rootId = pin.root_id, @previousGroupInstanceId = isnull(pin2.group_process_id,pin.group_process_id) 
	FROM dbo.process_instance pin with(nolock)
	LEFT JOIN dbo.process_instance pin2 on pin.previous_process_instance_id = pin2.id
	WHERE pin.id = @previousProcessId 
	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup OUTPUT
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	CREATE TABLE #replacecmentCarSummary (duration INT, real_duration INT, days_left INT, end_date DATETIME)
	
	SELECT TOP 1 @workshopGroup = dbo.f_service_top_progress_group(@groupProcessInstanceId, 12)
	
	EXEC p_form_controls @instance_id = @previousProcessId, @returnResults = 0
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '859', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @postFactum = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '168,839', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @rentalGroup = value_int FROM @values
	
	SET @rentalGroup = IIF(@rentalGroup IS NOT NULL, @rentalGroup, @groupProcessInstanceId)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '858', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @override = value_int FROM @values
	
--	DELETE FROM @values
--	INSERT @values EXEC p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @groupProcessInstanceId
--	SELECT @fixingEndDate = value_date FROM @values
	
	exec [dbo].[p_fixing_end_date] 
	@rootId = @rootId, 
	@fixingEndDate = @fixingEndDate output

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @workshopGroup
	SELECT @fixingPartnerId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = textD FROM dbo.dictionary where value = (SELECT value_int FROM @values) AND typeD = 'makeModel'
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @rentalGroup
	SELECT @offerMakeModelId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformName = name FROM platform where id = (SELECT value_int FROM @values)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '789,786,240,153', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @rentalDuration = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '668', @groupProcessInstanceId = @previousGroupInstanceId
	SELECT @sendNotice = value_int FROM @values
	
	-- sprawdzenie czy naprawa warsztatowa nie zakończyła się wcześniej
	DECLARE @hoursTillFix INT
	
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
	
	SELECT @hoursTillFix = DATEDIFF(minute, @eta, dateadd(hour, 1, @fixingEndDate))
	SELECT @fixEndDaterentalDuration = CEILING(CAST(@hoursTillFix AS FLOAT)/1440)	
	
--	IF @rentalDuration > @fixEndDaterentalDuration AND @estimated = 0
--	BEGIN
--		EXEC [dbo].[p_attribute_edit]
--	    @attributePath = '789,786,240,153', 
--	    @groupProcessInstanceId = @groupProcessInstanceId,
--	    @stepId = 'xxx',
--	    @userId = 1,
--	    @originalUserId = 1,
--	    @valueInt = @fixEndDaterentalDuration,
--	    @err = @err OUTPUT,
--	    @message = @message OUTPUT	
--	END 
		
	SELECT top 1 @rzwHash = token from dbo.process_instance where group_process_id = @rentalGroup and step_id = '1007.079' and active = 1
	
	IF @rzwHash IS NULL	
	BEGIN
		SET @rzwHash = NEWID()	
	END 
	
	EXEC p_attribute_edit
	@attributePath = '610', 
	@groupProcessInstanceId = @groupProcessInstanceId,
	@stepId = 'xxx',
	@valueInt = @partnerId,
	@err = @err OUTPUT,
	@message = @message OUTPUT

	EXEC [dbo].[p_attribute_edit]
   	@attributePath = '810', 
   	@groupProcessInstanceId = @groupProcessInstanceId,
   	@stepId = 'xxx',
   	@userId = 1,
   	@originalUserId = 1,
   	@valueString = @rzwHash,
   	@err = @err OUTPUT,
   	@message = @message OUTPUT
	
	 
	EXEC p_replacement_car_summary
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @daysLeft = days_left, @endDate = end_date, @duration = real_duration FROM #replacecmentCarSummary
	DROP TABLE #replacecmentCarSummary	
	
	SELECT TOP 1 @sourceId = id FROM dbo.attribute_value with(nolock) WHERE group_process_instance_id = @groupProcessInstanceId and attribute_path = '789' order by id desc
	SELECT TOP 1 @targetId = id FROM dbo.attribute_value with(nolock) WHERE group_process_instance_id = @groupProcessInstanceId and attribute_path = '812,789' order by id desc
	
	EXEC [dbo].[P_attribute_copy_structure]
	@source_attribute_value_id = @sourceId,
	@target_attribute_value_id = @targetId,
	@process_instance_id = @previousProcessId
	
--	EXEC [dbo].[p_attribute_edit]
--    @attributePath = '812,63', 
--    @groupProcessInstanceId = @groupProcessInstanceId,
--    @stepId = 'xxx',
--    @userId = 1,
--    @originalUserId = 1,
--    @valueText = 'OK (zamknięte odgórnie)',
--    @err = @err OUTPUT,
--    @message = @message OUTPUT
    
    EXEC [dbo].[p_attribute_edit]
  	@attributePath = '812,789,786,73', 
  	@groupProcessInstanceId = @groupProcessInstanceId,
  	@stepId = 'xxx',
  	@userId = 1,
  	@originalUserId = 1,
  	@valueInt = @offerMakeModelId,
  	@err = @err OUTPUT,
  	@message = @message OUTPUT
    
  	EXEC [dbo].[p_location_string]
	@attributePath = '283,110,85',
	@groupProcessInstanceId = @rentalGroup,	
	@locationString = @dropOffLocation OUTPUT 
  	
	EXEC [dbo].[p_location_string]
	@attributePath = '283,111,85',
	@groupProcessInstanceId = @rentalGroup,	
	@locationString = @pickUpLocation OUTPUT
	
	EXEC [dbo].[p_attribute_edit]
  	@attributePath = '812,851', 
  	@groupProcessInstanceId = @groupProcessInstanceId,
  	@stepId = 'xxx',
  	@userId = 1,
  	@originalUserId = 1,
  	@valueString = @dropOffLocation,
  	@err = @err OUTPUT,
  	@message = @message OUTPUT
  	
	EXEC [dbo].[p_attribute_edit]
  	@attributePath = '812,852', 
  	@groupProcessInstanceId = @groupProcessInstanceId,
  	@stepId = 'xxx',
  	@userId = 1,
  	@originalUserId = 1,
  	@valueString = @pickUpLocation,
  	@err = @err OUTPUT,
  	@message = @message OUTPUT
  	
  	EXEC [dbo].[p_attribute_edit]
  	@attributePath = '812,823', 
  	@groupProcessInstanceId = @groupProcessInstanceId,
  	@stepId = 'xxx',
  	@userId = 1,
  	@originalUserId = 1,
  	@valueDate = @eta,
  	@err = @err OUTPUT,
  	@message = @message OUTPUT
  	
  	EXEC [dbo].[p_attribute_edit]
  	@attributePath = '812,853', 
  	@groupProcessInstanceId = @groupProcessInstanceId,
  	@stepId = 'xxx',
  	@userId = 1,
  	@originalUserId = 1,
  	@valueDate = @endDate,
  	@err = @err OUTPUT,
  	@message = @message OUTPUT
  	
  	
	IF @fixingPartnerId = @partnerId or ISNULL(@postFactum,0) = 1
	BEGIN
		
		SET @variant = 2
		
		IF ISNULL(@postFactum,0) = 1
		BEGIN
			EXEC dbo.p_change_priority @priority = null, @type = 'rental_fixing_date', @groupProcessInstanceId = @groupProcessInstanceId
			
			RETURN 
		END 
		
		SET @emailContent = 'Szanowni Państwo,<br/>Potwierdzamy zakończenie wynajmu dla sprawy '+dbo.f_caseId(@groupProcessInstanceId)+dbo.f_translate('. Marka/ model - ',default)+ISNULL(@makeModel,'')+dbo.f_translate(', Liczba dni: ',default)+CAST(@duration AS NVARCHAR(10))+dbo.f_translate('. Prosimy o przesłanie Raportu Serwisowego.',default)
		
		SET @c = dbo.f_translate('Zakończenie wynajmu.',default)
		
		EXEC p_attribute_edit
		@attributePath = '812,789,786,73', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = @offerMakeModelId,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		DECLARE @newProcessId INT 
	
		EXEC [dbo].[p_process_new]
		@stepId = '1007.060',
		@userId = 1,
		@originalUserId = 1,
		@parentProcessId = @rootId,
		@rootId = @rootId,
		@groupProcessId= @groupProcessInstanceId,
		@err = @err OUTPUT,
		@message = @message OUTPUT,
		@processInstanceId = @newProcessId OUTPUT
		
		UPDATE dbo.process_instance set active = 0 where id = @newProcessId
		
	END 
	ELSE
	BEGIN
		SET @emailContent = 'Szanowni Państwo,<br/>Prosimy o kontakt z Klientem i umówienie zwrotu oraz <a href='''+dbo.f_hashTaskUrl(@rzwHash)+''' target=''_new''>uzupełnienie RZW dla wynajmu '+dbo.f_caseId(@groupProcessInstanceId)+'</a>, bezpłatny okres wynajmu dla Klienta upływa '+dbo.FN_VDateHour(@endDate)+dbo.f_translate('. Mają Państwo na to czas do 48 godzin od zakończenia wynajmu. W innym przypadku zlecenie zostanie zamknięte orientacyjnie na podstawie informacji w naszym systemie oraz Państwa cenniku.',default)
		SET @c = dbo.f_translate('Zakończenie wynajmu. Wysyłam RZW',default)
		
	END 
	
	 -- LOGGER START
		SET @p1 = dbo.f_caseId(@rootId)		
		EXEC dbo.p_log_automat
		@name = 'rental_automat',
		@content = @c,
		@param1 = @p1
	-- LOGGER END
	
   	
	select TOP 1 @email = avContactEmail.value_string
	from   dbo.attribute_value avLocation with(nolock) inner join
    dbo.attribute_value avName with(nolock) on avName.parent_attribute_value_id=avLocation.id and avName.attribute_path='595,597,84' inner join
    dbo.attribute_value avService with(nolock) on avService.parent_attribute_value_id=avLocation.id and avService.attribute_path='595,597,611' inner join
    dbo.attribute_value avServiceType with(nolock) on avServiceType.parent_attribute_value_id=avService.id and avServiceType.attribute_path='595,597,611,612' and avServiceType.value_int = 5 inner join
    dbo.attribute_value avPosibility with(nolock) on avPosibility.parent_attribute_value_id=avService.id and avPosibility.attribute_path='595,597,611,641' inner join
    dbo.attribute_value avContact with(nolock) on avContact.parent_attribute_value_id=avService.id and avContact.attribute_path='595,597,611,642' inner join
    dbo.attribute_value avContactType with(nolock) on avContactType.parent_attribute_value_id=avContact.id and avContactType.attribute_path='595,597,611,642,643' and avContactType.value_int = 4 inner join
   dbo.attribute_value avContactEmail with(nolock) on avContactEmail.parent_attribute_value_id=avContact.id and avContactEmail.attribute_path='595,597,611,642,656'  and avContactEmail.value_string is not null		 
    where avLocation.attribute_path='595,597' 
    and avLocation.id = @partnerId
 
    
   SET @subject = dbo.f_translate('ZAKOŃCZENIE WYNAJMU W SPRAWIE NR ',default)+dbo.f_caseId(@groupProcessInstanceId) 
    
--    SET @email = 'andrzej@sigmeo.pl'
	
  
	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rentals')	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		SET @senderEmail = dbo.f_getEmail('cfm')
		SET @sendNotice = IIF(@sendNotice IS NULL,0,1)
	END

	EXECUTE dbo.p_note_new 
	     @groupProcessId = @groupProcessInstanceId
	    ,@type = dbo.f_translate('email',default)
	    ,@content = @emailContent
	    ,@email = @email
	    ,@userId = 1  -- automat
	    ,@subject = @subject
	    ,@direction=1
	    ,@dw = ''
	    ,@udw = ''
	    ,@sender = @senderEmail
--	    ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
	    ,@emailBody = @emailContent
	    ,@emailRegards = 1
	    ,@err=@err OUTPUT
	    ,@message=@message OUTPUT
	 
	IF isnull(@override,0) <> 1 AND ISNULL(@sendNotice,1) = 1
	BEGIN
		
		IF @fixingEndDate < GETDATE()
		BEGIN			  
			 SET @smsContent = dbo.f_translate('W związku z zakończeniem naprawy Pana/Pani pojazdu informujemy o konieczności zwrotu pojazdu zastępczego, bezpłatny okres wynajmu upływa ',default)+dbo.FN_VDateHour(@endDate)+'.'			  		
		END 
		ELSE
		BEGIN
			SET @smsContent = dbo.f_translate('Zgodnie z obowiązującymi warunkami Assistance, ',default)+dbo.FN_VDateHour(@endDate)+dbo.f_translate(' upływa bezpłatny okres wynajmu pojazdu zastępczego',default)+IIF(@platformGroup = dbo.f_translate('CFM',default), '' , 'w ramach' +@platformName )+dbo.f_translate('. Prosimy o zwrot.',default)
		END



    IF @platformGroup = dbo.f_translate('CFM',default)
      BEGIN
        DECLARE @programid NVARCHAR(10)
        DELETE from @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @programId = value_string FROM @values
        DECLARE @isExtra int
        SELECT @isExtra = is_extra from vin_program where id = @programid
        DELETE from @values
        IF isnull(@isExtra,0) = 1
          BEGIN
            IF @fixingEndDate < GETDATE()
              BEGIN
                SET @smsContent = dbo.f_translate('W związku z zakończeniem naprawy Pana/Pani pojazdu informujemy o konieczności zwrotu pojazdu zastępczego, okres wynajmu upływa ',default)+dbo.FN_VDateHour(@endDate)+'.'
              END
            ELSE
              BEGIN
                SET @smsContent = dbo.f_translate('Zgodnie z obowiązującymi warunkami Assistance, ',default)+dbo.FN_VDateHour(@endDate)+dbo.f_translate(' upływa okres wynajmu pojazdu zastępczego',default)+IIF(@platformGroup = dbo.f_translate('CFM',default), '' , 'w ramach' +@platformName )+dbo.f_translate('. Prosimy o zwrot.',default)
              END
        end
    END

		IF @endDate > GETDATE()
		BEGIN
			EXEC dbo.p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @smsContent,
			@phoneNumber = @callerPhone,
			@err = @err OUTPUT,
			@message = @message OUTPUT				 
		END 
		
		
	END 
	
	EXEC dbo.p_change_priority @priority = null, @type = 'rental_fixing_date', @groupProcessInstanceId = @groupProcessInstanceId
	
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '855', 
	@groupProcessInstanceId = @rootId,
	@stepId = 'xxx',
	@userId = 1,
	@originalUserId = 1,
	@valueInt = 1,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
END