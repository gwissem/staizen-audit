ALTER PROCEDURE [dbo].[s_1007_057]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rzwHash NVARCHAR(40)
	DECLARE @endDate DATETIME
	DECLARE @postponeCount INT
	DECLARE @postponeLimit INT
	DECLARE @rzwId INT
	DECLARE @newDate DATETIME
	DECLARE @createdAt DATETIME
	DECLARE @emailContent NVARCHAR(MAX)
	DECLARE @email NVARCHAR(200)
	DECLARE @partnerId INT
	DECLARE @subject NVARCHAR(300)
	DECLARE @override int
	DECLARE @p1 nvarchar(255)
	DECLARE @rootId INT
	
	SELECT	@groupProcessInstanceId = p.group_process_id, @postponeCount = p.postpone_count, @postponeLimit = s.postpone_count, @createdAt = p.created_at, @rootId = p.root_id
	FROM process_instance p with(nolock)
	INNER JOIN dbo.step s with(nolock) ON p.step_id = s.id  
	WHERE p.id = @previousProcessId 
	
	SELECT top 1 @rzwId = id, @rzwHash = token FROM dbo.process_instance with(nolock) WHERE group_process_id = @groupProcessInstanceId and step_id = '1007.055' and active = 1
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	CREATE TABLE #replacecmentCarSummary (duration INT, real_duration INT, days_left INT, end_date DATETIME)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '858', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @override = value_int FROM @values
	
	IF @variant = 3
	BEGIN
		RETURN
	END


	IF @override = 3
	BEGIN		
		EXEC p_attribute_edit
		@attributePath = '858', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = NULL ,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		SET @variant = 2
		UPDATE dbo.process_instance set active = 0 where group_process_id = @groupProcessInstanceId and step_id = '1007.055' and active = 1
		
		SET @p1 = dbo.f_caseId(@groupProcessInstanceId)
		EXEC dbo.p_log_automat
		@name = 'rental_automat',
		@content = dbo.f_translate('Wymuszone wznowienie wynajmu pojazdu zastępczego.',default),
		@param1 = @p1
		return 
	END 
	
	EXEC p_replacement_car_summary
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @endDate = end_date FROM #replacecmentCarSummary
	
	IF @endDate < @createdAt 
	BEGIN
		SET @endDate = @createdAt
	END 
	
	DROP TABLE #replacecmentCarSummary
	
	SET @variant = 99
	
	IF GETDATE() >= DATEADD(DAY, 2, @endDate) AND @rzwId IS NOT NULL
	BEGIN
		IF @rzwId IS NOT NULL
		BEGIN
			UPDATE dbo.process_instance SET active = 0 where id = @rzwId	             
		END		
		SET @variant = 1	
		return
	END 
	ELSE IF GETDATE() >= DATEADD(DAY,1,@endDate) AND @rzwId IS NOT NULL AND @postponeCount = 0
	BEGIN
		SET @variant = 99
		
		SET @emailContent = 'Szanowni Państwo,<br/>Przypominamy o <a href='''+dbo.f_hashTaskUrl(@rzwHash)+''' target=''_new''>uzupełnieniu RZW dla wynajmu '+dbo.f_caseId(@groupProcessInstanceId)+'</a>. Mają Państwo na to czas do 48 godzin od zakończenia wynajmu. W innym przypadku zlecenie zostanie zamknięte orientacyjnie na podstawie informacji w naszym systemie oraz państwa cenniku.<br/>Z poważaniem, Zespół Starter24'
	
		select TOP 1 @email = avContactEmail.value_string
		from   dbo.attribute_value avLocation with(nolock) inner join
	    dbo.attribute_value avName with(nolock) on avName.parent_attribute_value_id=avLocation.id and avName.attribute_path='595,597,84' inner join
	    dbo.attribute_value avService with(nolock) on avService.parent_attribute_value_id=avLocation.id and avService.attribute_path='595,597,611' inner join
	    dbo.attribute_value avServiceType with(nolock) on avServiceType.parent_attribute_value_id=avService.id and avServiceType.attribute_path='595,597,611,612' and avServiceType.value_int = 5 inner join
	    dbo.attribute_value avPosibility with(nolock) on avPosibility.parent_attribute_value_id=avService.id and avPosibility.attribute_path='595,597,611,641' inner join
	    dbo.attribute_value avContact with(nolock) on avContact.parent_attribute_value_id=avService.id and avContact.attribute_path='595,597,611,642' inner join
	    dbo.attribute_value avContactType with(nolock) on avContactType.parent_attribute_value_id=avContact.id and avContactType.attribute_path='595,597,611,642,643' and avContactType.value_int = 4 inner join
	    dbo.attribute_value avContactEmail with(nolock) on avContactEmail.parent_attribute_value_id=avContact.id and avContactEmail.attribute_path='595,597,611,642,656'  and avContactEmail.value_string is not null		 
	    where avLocation.attribute_path='595,597' 
	    and avLocation.id = @partnerId
	    
	    SET @subject = dbo.f_translate('PRZYPOMNIENIE O BRAKU ZAKOŃCZENIA WYNAJMU W SPRAWIE NR ',default)+dbo.f_caseId(@groupProcessInstanceId) 
	    
	    
		DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rentals')

		DECLARE @platformGroup nvarchar(255)	
		EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output	
		IF @platformGroup = dbo.f_translate('CFM',default)
		BEGIN
			SET @senderEmail = dbo.f_getEmail('cfm')
		END

		EXECUTE dbo.p_note_new 
		     @groupProcessId = @groupProcessInstanceId
		    ,@type = dbo.f_translate('email',default)
		    ,@content = @emailContent
		    ,@email = @email
		    ,@userId = 1  -- automat
		    ,@subject = @subject
		    ,@direction=1
		    ,@dw = ''
		    ,@udw = ''
		    ,@sender = @senderEmail
	--	    ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
		    ,@emailBody = @emailContent
		    ,@emailRegards = 1
		    ,@err=@err OUTPUT
		    ,@message=@message OUTPUT
	    
	    UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(DAY, 1, @endDate) where id = @previousProcessId
	    
	END 
END






