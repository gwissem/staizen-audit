ALTER PROCEDURE [dbo].[P_checkCaseForCloseInViewer]
( 
	@processInstanceId INT,
    @serviceId INT
) 
AS
BEGIN
	
	/* ______________________________________________________
	 
	 	Procedura, która jest stworzona przez schedule mssql job.
	 	Sprawdza czy są aktywne jeszcze jakieś usługi poza tą co została uruchomiona.
	 	Jeżeli nie ma nic aktywnego, to sprawa zostaje zamknięta w CFM Viewer.
	 	
	 ________________________________________________________*/
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @services NVARCHAR(MAX)
	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @processInstanceId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	CREATE TABLE #servicesTempTable (id int, group_process_id int, serviceId int, status_dictionary_id int, status_label nvarchar(255), status_progress int, icon nvarchar(max), service_name nvarchar(255))

	EXEC [dbo].[p_get_status_active_services]
		@processInstanceId = @processInstanceId,
		@showAll = 1,
		@useTempTable = 1
		
	
	/*	Sprawdzenie czy w sprawie istnieją inne aktywne usługi
 		____________________________________*/
	
	IF EXISTS
	(
		SELECT id FROM #servicesTempTable
		WHERE serviceId <> @serviceId
		AND status_progress NOT IN (4,5)
	)
	BEGIN
		
		/*	Zalogowanie, jaki usługi jeszcze występują
  		____________________________________*/
		
		PRINT dbo.f_translate('Znaleziono usługi',default)
		
		SELECT @services = STRING_AGG(CONCAT('Service: dbo.f_translate(', serviceId, ',default), GroupId: ', group_process_id, dbo.f_translate(', Progress: ',default), status_progress), ' | ') FROM #servicesTempTable
		WHERE serviceId <> @serviceId
		AND status_progress NOT IN (4,5)
		
		PRINT @services
		
		EXEC [dbo].[p_add_case_log] @name = dbo.f_translate('checkCaseForCloseInViewer',default), 
									@rootId = @rootId, 
									@groupId = @groupProcessInstanceId, 
									@param1 = @serviceId, 
									@param2 = 0,
									@outputContent = @services
		        
	END
	ELSE
	BEGIN
		
		/*	Zamknięcie sprawy w Viewer i logowanie
  		____________________________________*/
		
		PRINT dbo.f_translate('Nie znaleziono usług. Zamykam sprawę.',default)
		
		EXEC p_attribute_edit
  			@attributePath = '465',
  			@groupProcessInstanceId = @rootId,
  			@stepId = 'xxx',
  			@valueInt = 1,
  			@userId = 1,
  			@originalUserId = 1,
  			@err = @err OUTPUT,
  			@message = @message OUTPUT
  			
  		EXEC [dbo].[p_add_case_log] @name = dbo.f_translate('checkCaseForCloseInViewer',default), 
									@rootId = @rootId, 
									@groupId = @groupProcessInstanceId, 
									@param1 = @serviceId, 
									@param2 = 1
									
	END
	
	IF OBJECT_ID('tempdb..#servicesTempTable') IS NOT NULL
    DROP TABLE #servicesTempTable
		
END



