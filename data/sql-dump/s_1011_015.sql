ALTER PROCEDURE [dbo].[s_1011_015]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessId INT
	DECLARE @content NVARCHAR(MAX)
	DECLARE @phoneNumber NVARCHAR(255)
	DECLARE @attrTable Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @platformId INT
	DECLARE @platformPhone NVARCHAR(100)
	DECLARE @platformName NVARCHAR(100)
	DECLARE @emergency INT
	DECLARE @caseId VARCHAR(8)
	DECLARE @programIds NVARCHAR(255)
	
	SELECT @groupProcessId = group_process_id FROM process_instance with(nolock) where id = @previousProcessId
	
	delete from @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '512',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @emergency = value_int FROM @attrTable
	
	declare @isAccident int
	declare @eventType int
--	
	delete from @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '419',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @isAccident = value_int FROM @attrTable

	DELETE FROM @attrTable
 	INSERT  @attrTable EXEC dbo.p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessId
 	SELECT @eventType = value_int FROM @attrTable
	
 	DELETE FROM @attrTable
	INSERT @attrTable EXEC dbo.p_attribute_get2
		   @attributePath = '253',
		   @groupProcessInstanceId = @groupProcessId
	SELECT @platformId = value_int FROM @attrTable
		
	set @variant=1
	
	/*	DLa LeasePlan, nie możemy przyjąć szkody Jezdnej
 	____________________________________*/
	
	IF @platformId = 25 AND @eventType = 5
	BEGIN
	
		DECLARE @newProcessInstanceId INT
		
		EXEC [dbo].[p_process_jump]
			@previousProcessId = @previousProcessId,
			@nextStepId = '1011.023',
			@doNext = 0,
			@deactivateCurrent = 999,
			@parentId = NULL,
			@rootId = @groupProcessId, -- w tym przypadku root i groupId to to samo
			@groupId = @groupProcessId,
			@variant = NULL,
		    @err = @err output,
		    @message = @message output,
		    @newProcessInstanceId = @newProcessInstanceId output
		    
		RETURN
		
	END
	
	
	if @isAccident in(0,2,3)
	begin
		EXEC [dbo].[p_attribute_edit]
				@attributePath = '512',
				@stepId = 'xxx',
				@groupProcessInstanceId = @groupProcessId,
				@valueInt = null,
				@err = @err,
				@message = @message

		set @emergency=0
	end


	IF @emergency=1 and @isAccident=1
	BEGIN
		DELETE FROM @attrTable
		INSERT  @attrTable EXEC dbo.p_attribute_get2
				@attributePath = '418,342,408,197',
				@groupProcessInstanceId = @groupProcessId
		SELECT @phoneNumber = value_string FROM @attrTable
		
		
		SELECT @platformPhone = number from platform_phone where platform_id = @platformId
		SELECT @platformName = name FROM platform where id = @platformId
		SELECT @caseId = CAST(@groupProcessId AS VARCHAR(8))
	
		SET @content = dbo.f_translate('Nr. zgloszenia: A',default)+RIGHT('0000000'+@caseId,8)+dbo.f_translate('. Po wezwaniu sluzb ratunkowych prosimy o ponowny kontakt.',default)	
	
		EXEC p_note_new
		@groupProcessId = @groupProcessId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @phoneNumber,
		@userId = 1,
		@originalUserId = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		set @variant=2
	
	END
	ELSE
	BEGIN
		delete from @attrTable
		INSERT @attrTable EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessId
		SELECT @programIds = value_string FROM @attrTable
		
		DELETE FROM @attrTable
		INSERT @attrTable EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessId
		SELECT @platformId = value_int FROM @attrTable
		
		-- Usługi płatne, Skoda i VW pomijają 1011.007
		if @platformId=5
		begin
			set @variant=5
		end
		else IF ISNULL(@programIds,'') LIKE '%423%' OR @platformId NOT IN (31)
		BEGIN
			SET @variant = 3
		END 

		--if @isAccident=3
		--begin
		--	set @variant=4
		--end
	END 
	
END


