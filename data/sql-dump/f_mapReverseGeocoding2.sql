

ALTER FUNCTION [dbo].[f_mapReverseGeocoding2](@latlng varchar(100))
RETURNS varchar(3000) AS  
BEGIN 
	DECLARE @URL VARCHAR(8000) 
	SET @URL = 'https://maps.googleapis.com/maps/api/distancematrix/xml?latlng='+@latlng+'&key=AIzaSyDMUZXCjq_3Y6ZHHLhapOrU4LcpwjZVPpY'

	DECLARE @Response varchar(8000)
	DECLARE @XML xml
	DECLARE @Obj int 
	DECLARE @Result int 
	DECLARE @HTTPStatus int 
	
	EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
	EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
	EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
	EXEC sp_OAMethod @Obj, send, NULL, ''
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
	
	declare @TempCycles table ( CycleID CHAR(3), ID INT)
   INSERT INTO @TempCycles VALUES ('EOD',1)
   INSERT INTO @TempCycles VALUES ('EOW',2)
   INSERT INTO @TempCycles VALUES ('EOF',3)
   INSERT INTO @TempCycles VALUES ('EOM',4)
   INSERT INTO @TempCycles VALUES ('EOQ',5)
   INSERT INTO @TempCycles VALUES ('EOH',6)
   INSERT INTO @TempCycles VALUES ('EOY',7)
	
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseXML.xml',default), @Response OUT 

	declare @responseXML XML
	set @responseXML=cast(@response as XML)
	set @result=@XML.query('/GeocodeResponse/result/formatted_address').value('.','nvarchar(300)')

	return @result
END

