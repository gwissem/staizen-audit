ALTER PROCEDURE [dbo].[s_1018_022]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	set @variant=1
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @driverEmail NVARCHAR(255)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @body NVARCHAR(4000)
	declare @transport int

	SELECT @groupProcessInstanceId = group_process_id, @rootId=root_id FROM process_instance where id = @previousProcessId

	INSERT @values EXEC p_attribute_get2 @attributePath = '590', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @transport = value_int FROM @values
	DELETE FROM @values
	
	DECLARE @platformId INT
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2
		   @attributePath = '253',
		   @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values

	DECLARE @name nvarchar(100)

	EXECUTE dbo.p_platform_group_name
	   @groupProcessInstanceId=@groupProcessInstanceId
	  ,@name=@name OUTPUT

	if @transport=1 and @name<>dbo.f_translate('CFM',default)
	begin
		declare @regNumber nvarchar(100)

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @regNumber = value_string FROM @values

		declare @caseId nvarchar(100)
		set @caseId=dbo.f_caseId(@groupProcessInstanceId)
		
		declare @locationString nvarchar(1000)
		EXECUTE dbo.p_location_string 
			@attributePath='62,591,85'
			,@groupProcessInstanceId=@groupProcessInstanceId
			,@locationString=@locationString OUTPUT

		declare @subject nvarchar(100)
		set @subject=dbo.f_translate('Upoważnienie ',default)+isnull(@caseId,'')
	

		INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,368', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @driverEmail = value_string FROM @values
	
		set @body='Szanowni Państwo, <br>
		w związku z organizacją transportu należy uzupełnić dołączony w pliku dokument. Jest on niezbędny do zorganizowania transportu pojazdu. 
		<br><br>
		Dokument należy odesłać na adres callcenter@starter24.pl. 
					'
		DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')
	
		declare @bodyHtml nvarchar(max)
		declare @firstname nvarchar(100)
		declare @lastname nvarchar(100)
		declare @VIN nvarchar(100)
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @VIN = value_string FROM @values

		SELECT @bodyHtml=body
		FROM   dls.templates
		where	id=6

		declare @firstNameP nvarchar(100)
		declare @lastnameP nvarchar(100)
		declare @makeModel int
		declare @makeModelV nvarchar(100)
		declare @IDnumber nvarchar(100)

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,64', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @firstNameP = value_string FROM @values

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,66', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @lastnameP = value_string FROM @values

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @makeModelV = textD from dbo.dictionary where value = (select top 1 value_int FROM @values) and  typeD like 'makeModel'

		declare @partnerId int

		
		declare @keyAddress nvarchar(200)
		declare @isKey int

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '62,112', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @isKey = value_int FROM @values
		
		if @isKey=0
		begin
			declare @keyAddressId int
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '62,775,85', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @keyAddressId = id FROM @values
		
			set @keyAddress=dbo.f_addressText(@keyAddressId)
		end
		
		declare @country nvarchar(100)
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @country = value_string FROM @values

		declare @partnerLocation nvarchar(1000)
		declare @towGroupProcessId int
		set @towGroupProcessId=dbo.f_service_top_progress_group(@rootId,12)

		if @country=dbo.f_translate('Polska',default)
		begin
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towGroupProcessId
			SELECT @partnerId = value_int FROM @values

			EXEC [dbo].[p_attribute_edit]
   				@attributePath = '522', 
   				@groupProcessInstanceId = @groupProcessInstanceId,
   				@stepId = 'xxx',
   				@userId = 1,
   				@originalUserId = 1,
   				@valueInt = @partnerId,
   				@err = @err OUTPUT,
   				@message = @message OUTPUT

			set @partnerLocation=[dbo].[f_partnerAdress](@partnerId)
			print dbo.f_translate('Polska',default)
			
			declare @partnerName nvarchar(100)

			select @partnerName=value_string 
			from	dbo.attribute_value 
			where	parent_attribute_value_id=@partnerId and 
					attribute_path ='595,597,84'

			set @partnerLocation=isnull(@partnerName,'')+' '+@partnerLocation

		end
		else
		begin
			
			
			declare @serviceName nvarchar(100)
			declare @serviceAddress nvarchar(100)
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '815,781,84', @groupProcessInstanceId = @towGroupProcessId
			SELECT @serviceName = value_string FROM @values

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '815,781,687', @groupProcessInstanceId = @towGroupProcessId
			SELECT @serviceAddress = value_string FROM @values

			set @partnerLocation=isnull(@serviceName,'')+isnull(@serviceAddress,'')
		end

	

	declare @userName nvarchar(100)
	declare @lineNumber nvarchar(100)
	
	DECLARE @platformPhone NVARCHAR(100)
	DECLARE @platformName NVARCHAR(100)

	SELECT @platformPhone = number from platform_phone where platform_id = @platformId
	SELECT @platformName = name FROM platform where id = @platformId
	

	select @lineNumber=official_line_number
	from dbo.platform
	where id=@platformId

	 select @userName=firstName+' '+lastname
	 from dbo.fos_user
	 where id=@currentUser


	-- select * from dbo.platform

	 declare @cost1 nvarchar(200)
	 declare @cost2 nvarchar(200)
	 declare @cost3 nvarchar(200)

	 if @platformId=2
	 begin
		set @cost1='Jednocześnie zobowiązuję się do pokrycia kosztów paliwa i opłat za autostrady*.'
		set @cost2=dbo.f_translate('Furthermore I declare to cover all optional insurance, fuel and road toll costs concerned with vehicle',default)'s collection / Ich verpflichte mich optionale Versicherungs, Benzinkosten und Autobahngebühren zu tragen'
		set @cost3='* Właściciel pojazdu może we własnym zakresie zawrzeć dodatkowe ubezpieczenie obejmujące ochroną pojazd w czasie realizacji usługi jego dostarczenia.'
	 end

	 if @platformId in (11,6)
	 begin
		set @cost1=dbo.f_translate('Jednocześnie zobowiązuję się do pokrycia kosztów usługi powyżej limitu 400 Euro',default)
	 end

	 if @platformId=31
	 begin
		set @cost1=dbo.f_translate('Jednocześnie zobowiązuję się do pokrycia kosztów usługi powyżej limitu 500 Euro',default)
	 end

	declare @isKeyV nvarchar(20)
	declare @isKeyV2 nvarchar(20)

	set @isKeyV=dbo.f_translate('checked',default)
	set @isKeyV2=''
	if @isKey=0 
	begin
		set @isKeyV=''
		set @isKeyV2=dbo.f_translate('checked',default)
	end

	set @bodyHtml=   REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					 REPLACE(
					  @bodyHtml,
					 '{@74,71@}',isnull(@VIN,'..........................')),
					 '{@74,72@}',isnull(@regNumber,'....................')),
					 '{@80,342,64@}',isnull(@firstNameP,'.....................')),
					 '{@80,342,66@}',isnull(@lastnameP,'............................')),
					 '{@74,73@}',isnull(@makeModelV,'.................................')),
					 '{@62,591,85@}',isnull(@locationString,'.....................................')),
					 '{@62,778,779@}',isnull(@IDnumber,'.....................................')),
					 '{@PS@}',isnull(@partnerLocation,'..................................')),
					 '{@Date@}',isnull(dbo.fn_vdate(getdate()),'..................................')),
					 '{@username@}',isnull(@userName,'..................................')),
					 '{@lineNumber@}',isnull(@lineNumber,'600 222 222')),
					 '{@cost1@}',isnull(@cost1,'')),
					 '{@cost2@}',isnull(@cost2,'')),
					 '{@cost3@}',isnull(@cost3,'')),
					 '{@62,775,85@}',isnull(@keyAddress,'......................................................................................................................................................................................')),
					 '{@isKeyV@}',isnull(@isKeyV,'')),
					 '{@isKeyV2@}',isnull(@isKeyV2,''))

	set @subject=dbo.f_translate('Upoważnienie do odbioru auta ',default)+isnull(@caseId,'')

	declare @attributeValueId int

    EXEC [dbo].[p_attribute_edit]
   	@attributePath = '962', 
   	@groupProcessInstanceId = @groupProcessInstanceId,
   	@stepId = 'xxx',
   	@userId = 1,
   	@originalUserId = 1,
   	@valueText = @bodyHtml,
   	@err = @err OUTPUT,
   	@message = @message OUTPUT,
   	@attributeValueId = @attributeValueId OUTPUT
	
	declare @attachment nvarchar(200)
	declare @fileName nvarchar(100)
	set @fileName=@caseId 

   	SET @attachment = '{ATTRIBUTE::'+CAST(@attributeValueId AS NVARCHAR(20))+'::'+@fileName+'::true}'
   	

		EXECUTE dbo.p_note_new 
		   @sender=@senderEmail
		  ,@groupProcessId=@groupProcessInstanceId
		  ,@type=dbo.f_translate('email',default)
		  ,@content=@subject
		  ,@emailBody=@body
		  ,@phoneNumber=null
		  ,@email=@driverEmail
		  ,@emailRegards=1
		  ,@userId=@currentUser
		  ,@subType=null
		  ,@attachment=null
		  ,@additionalAttachments=@attachment
		  ,@subject=@subject
		  ,@direction=1
		  ,@addInfo=null
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT

		 EXEC dbo.p_jump_to_service_panel @previousProcessId = @previousProcessId, @variant = 1
	
--	

	end

	IF @name=dbo.f_translate('CFM',default) AND @variant <> 3
	begin

		set @variant=2
		if @platformId=25
		begin
			declare @X nvarchar(20)
			declare @Y nvarchar(20)
			declare @XPs nvarchar(20)
			declare @YPS nvarchar(20)

			delete from @values	
			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '62,591,85,92',
					@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @X = value_string FROM @values
			
			delete from @values

			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '62,591,85,93',
					@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @Y = value_string FROM @values

			declare @PSid int 
			declare @PSidLoc int 

			delete from @values	
			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '522',
					@rootProcessInstanceId = @rootId
			SELECT @PSid = value_int FROM @values

			declare @psGroupProcessId int

			select @psGroupProcessId=group_process_instance_id
			from	dbo.attribute_value  
			where	id=@PSid

			select @PSidLoc=id
			from	dbo.attribute_value  
			where	parent_attribute_value_id=@PSid and attribute_path='595,597,85'

			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '595,597,85,92',
					@groupProcessInstanceId = @psGroupProcessId,
					@parentAttributeId=@PSidLoc
			SELECT @XPS = value_string FROM @values

			delete from @values

			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '595,597,85,93',
					@groupProcessInstanceId = @psGroupProcessId,
					@parentAttributeId=@PSidLoc
			SELECT @YPS = value_string FROM @values

			declare @point1 geography
			declare @point2 geography

			if @XPs is not null and @YPS is not null
			begin
				SET @point1 = GEOGRAPHY::Point(@Y , @X , 4326)
				SET @point2 = GEOGRAPHY::Point(@YPS , @XPS , 4326)

			--	select @point1,@point2

			
				declare @distancePS decimal(18,2)
			
				set @distancePS=ISNULL(@point1.STDistance(@point2)*0.0012,999)

				DECLARE @isVIP nvarchar

				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '1004', @groupProcessInstanceId = @towGroupProcessId
				SELECT @isVIP = value_string FROM @values


				if @distancePS<100 and isnull(@isVIP,'') = ''
					BEGIN
						set @variant=3
					END
			end
			

		--	select @distancePS
		end
	end


	-- Skok do panelu usług

--	EXEC p_process_jump
--	@previousProcessId = @previousProcessId,
--	@nextStepId = '1011.010',
--	@doNext = 0,
--	@parentId = NULL,
--	@variant=1,
--	@rootId = @rootId,
--	@groupId = @rootId,
--	@err = @err output,
--	@message = @message output
--	

END
