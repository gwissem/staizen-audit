ALTER PROCEDURE p_send_vip_case_info
    @groupProcessId int
AS
  BEGIN


    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))

    DECLARE @content nvarchar(4000)
    DECLARE @subject nvarchar(500)
    DECLARE @mailto nvarchar(500)
    DECLARE @rootId int
    DECLARE @platformName nvarchar(100)
    DECLARE @platformId int
    DECLARE @err int
    DECLARE @message nvarchar(100)
    DECLARE @serviceName nvarchar(4000)
    DECLARE @starterEmail nvarchar(200)
    DECLARE @country NVARCHAR(100) = null
    DECLARE @alreadySent tinyint = 0


    SELECT @rootId = root_id from process_instance where id = @groupProcessId


    DECLARE @caseId nvarchar(20)
    SELECT @caseId = dbo.f_caseId(@groupProcessId)



    DECLARE @carRegNumber NVARCHAR(30)

    DELETE FROM @values
    INSERT @values
        EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                              @attributePath = '74,72'
    SELECT @carRegNumber = value_string FROM @values

    DECLARE @createdAt DATETIME
    SELECT @createdAt = created_at from process_instance where id = @rootId

    DELETE FROM @values
    INSERT @values
        EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                              @attributePath = '253'
    SELECT @platformId = value_int FROM @values
    SELECT @platformName = name FROM AtlasDB_def.dbo.platform where id = @platformId
    DECLARE @mailSent int = 0


    DELETE FROM @values
    INSERT into @values
        EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId, @attributePath = '1050'
    SELECT @mailSent = value_int from @values
    IF isnull(@mailSent, 0) = 0
      BEGIN


        DECLARE @driverName nvarchar(125)
        DECLARE @driverSurname nvarchar(125)
        DECLARE @fullname nvarchar(250)

        DELETE FROM @values
        INSERT @values
            EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                                  @attributePath = '80,342,64'
        SELECT @driverName = value_string FROM @values


        DELETE FROM @values
        INSERT @values
            EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                                  @attributePath = '80,342,66'
        SELECT @driverSurname = value_string FROM @values

        SET @fullname = isnull(@driverName, '') + ' ' + isnull(@driverSurname, '')


        IF isnull(@fullname, '') = ''
          BEGIN
            DELETE FROM @values
            INSERT @values
                EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                                      @attributePath = '418,342,64'
            SELECT @driverName = value_string FROM @values


            DELETE FROM @values
            INSERT @values
                EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                                      @attributePath = '418,342,66'
            SELECT @driverSurname = value_string FROM @values

            SET @fullname = isnull(@driverName, '') + ' ' + isnull(@driverSurname, '')
          end

        IF isnull(@fullname, '') = ''
          BEGIN
            DELETE FROM @values
            INSERT @values
                EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                                      @attributePath = '81,342,64'
            SELECT @driverName = value_string FROM @values


            DELETE FROM @values
            INSERT @values
                EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessId,
                                      @attributePath = '81,342,66'
            SELECT @driverSurname = value_string FROM @values

            SET @fullname = isnull(@driverName, '') + ' ' + isnull(@driverSurname, '')
          end


        SET @subject = isnull(@platformName, '') + dbo.f_translate(' Klient VIP ',default) +isnull(@caseId,'') +' / '+ isnull(@carRegNumber,'')
        SET @content = 'Nowa sprawa VIP<BR />

        ' + isnull(@fullname, '') + '<BR />
        ' + dbo.f_caseId(@groupProcessId)


        IF isnull(@platformId, 0) in (25, 43, 48, 53,78)
          BEGIN
            SET @mailto = dbo.f_getRealEmailOrTest('kzcfm@starter24.pl')
          END
        ELSE
          BEGIN
            SET @mailto = dbo.f_getRealEmailOrTest('kz@starter24.pl')
          end

        SET @starterEmail = [dbo].[f_getEmail]('callcenter')

        SET @mailto = @mailto + ', ' + dbo.f_getRealEmailOrTest('anna.piotrowska@starter24.pl')
        --             EXEC p_attribute_edit
        --                 @attributePath = '514',
        --                 @groupProcessInstanceId = @groupProcessId,
        --                 @stepId = 'xxx',
        --                 @valueString = 'kz_abroad',
        --                 @err = @err OUTPUT,
        --                 @message = @message OUTPUT

        EXEC p_note_new
            @groupProcessId = @groupProcessId,
            @type = dbo.f_translate('email',default),
            @content = @content,
            @phoneNumber = null,
            @email = @mailTo,
            @subject = @subject,
            @direction = 1,
            @emailRegards = 1,
            @err = @err OUTPUT,
            @message = @message OUTPUT,
            -- FOR EMAIL
            @dw = '',
            @udw = '',
            @sender = @starterEmail,
            @additionalAttachments = '',
            @emailBody = @content


        EXEC p_attribute_edit
            @groupProcessInstanceId = @groupProcessId,
            @stepId = 'xxx',
            @attributePath = '1050',
            @valueInt = 1,
            @userId = 1,
            @originalUserId = 1,
            @err = @err OUTPUT,
            @message = @message OUTPUT

      END
  END