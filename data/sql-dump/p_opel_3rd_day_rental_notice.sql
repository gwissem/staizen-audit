ALTER PROCEDURE [dbo].[p_opel_3rd_day_rental_notice]
AS
BEGIN
	
	DECLARE @rootId int 
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
    DECLARE @body nvarchar(max)
    DECLARE @vin nvarchar(100)
    DECLARE @subject nvarchar(1000)
    DECLARE @email nvarchar(255)
    DECLARE @senderEmail nvarchar(255)
    DECLARE @dw nvarchar(255)
    DECLARE @groupProcessInstanceId int 
    
	declare kur cursor LOCAL for
	 	select pin.root_id, duration.group_process_instance_id from dbo.attribute_value duration with(nolock)
		inner join dbo.attribute_value startDate with(nolock) on startDate.group_process_instance_id = duration.group_process_instance_id and startDate.attribute_path = '540' 
		inner join dbo.process_instance pin with(nolock) on pin.group_process_id = duration.group_process_instance_id and pin.step_id = '1007.053' and active = 1
		inner join dbo.attribute_value program with(nolock) on program.group_process_instance_id = pin.group_process_id and program.attribute_path = '202' and program.value_string IN ('590','591')
		left join dbo.attribute_value note with(nolock) on note.root_process_instance_id = duration.root_process_instance_id and note.value_string = dbo.f_translate('3rd-day-notice',default) and note.attribute_path = '406,226,197'
		where duration.attribute_path = '789,786,240,153'
		and note.id is null 
		and dateadd(day,2,startDate.value_date) < getdate()  
--		and dateadd(day,3,startDate.value_date) > getdate() /** zakomentowane bo powiadomienie nie szło gdy przedłużaliśmy z opóźnieniem z 1 dnia na 5 **/
		and dateadd(day,duration.value_int,startDate.value_date) > getdate()
		and duration.value_int >= 3
	OPEN kur;
	FETCH NEXT FROM kur INTO @rootId, @groupProcessInstanceId;
	WHILE @@FETCH_STATUS=0
	BEGIN
	
	    SELECT TOP 1 @vin = value_string from dbo.attribute_value with(nolock) where group_process_instance_id = @rootId and attribute_path = '74,71'
		SET @subject = dbo.f_caseId(@rootId)+ISNULL(', '+@vin,'')+dbo.f_translate(', Powiadomienie o wynajmie',default)
		
		SET @body = 'Szanowni Państwo,<br>Przesyłamy powiadomienie dot. wynajmu w przedmiotowej sprawie:<br><br>
					Nr sprawy: {#caseid#}<br>
					Nr VIN: {@74,71@}<br>
					Marka i model pojazdu: {@74,73@}<br>
					Nazwa serwisu: {#workshopName()#}<br>
					Data rozpoczęcia wynajmu: {#showDatetime({@540@}|1)#}<br>
					Data końca wynajmu: {#showDatetime({#rentalSummary(end_date)#}|1)#}<br>
					Liczba dni wynajmu: {@789,786,240,153@}<br><br>
					Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres kz@starter24.pl'		
		
		EXEC [dbo].[P_get_body_email]
			@contentEmail = @body,
			@title = @subject,
			@previousProcessId = @groupProcessInstanceId,
			@body = @body OUTPUT
		
		SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'OPEL')
		SET @body = REPLACE(@body,'__EMAIL__', 'callcenter@starter24.pl')
	
					
		SELECT @email = dbo.f_getRealEmailOrTest('lukasz.chorylo@opel.com')
		SELECT @dw =  dbo.f_getRealEmailOrTest('waldemar.szajkowski@opel.com,')+', '+dbo.f_getRealEmailOrTest('kz@starter24.pl')
		SELECT @senderEmail = dbo.f_getRealEmailOrTest('callcenter@starter24.pl')
  		
		EXECUTE dbo.p_note_new
        @sender = @senderEmail
        , @groupProcessId = @groupProcessInstanceId
        , @type = dbo.f_translate('email',default)
        , @content = dbo.f_translate('Powiadomienie o rozpoczęciu 3. doby wynajmu',default)        
        , @email = @email		
        , @dw = @dw
        , @userId = 1
        , @direction = 1
        , @subType = dbo.f_translate('3rd-day-notice',default)
        , @subject = @subject        
        , @emailBody = @body
        , @err = @err OUTPUT
        , @message = @message OUTPUT
		
	FETCH NEXT FROM kur INTO @rootId, @groupProcessInstanceId;
	END
	CLOSE kur
	DEALLOCATE kur
	
END 