ALTER PROCEDURE [dbo].[s_1009_003]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @processInstanceId INT
	DECLARE @czyPrzyjalZlecenie TINYINT
	DECLARE @partnerPhone NVARCHAR(20)
	DECLARE @parking INT
	DECLARE @partnerId INT
	DECLARE @makeModel NVARCHAR(60)
	DECLARE @regNumber NVARCHAR(20)
	DECLARE @vin NVARCHAR(40)
	DECLARE @etaMinutes INT
	DECLARE @eta DATETIME
	DECLARE @serviceId INT
	DECLARE @locationString NVARCHAR(1000)
	DECLARE @towingRouteText NVARCHAR(1000)
	DECLARE @programName NVARCHAR(200)
	DECLARE @driverFirstname NVARCHAR(100)
	DECLARE @driverLastname NVARCHAR(100)
	DECLARE @driverName NVARCHAR(200)
	DECLARE @driverPhone NVARCHAR(20)
	DECLARE @icsId INT
	DECLARE @towingType INT
	DECLARE @trailerTowingSameDestination INT
	DECLARE @isGop INT
	DECLARE @rootId int
	DECLARE @routing int 
	
	-- Pobranie GroupProcessInstanceId --
	SELECT	@groupProcessInstanceId = group_process_id, @rootId = root_id,
			@stepId = step_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	-- Stworzenie pomocniczej tabelki
	DECLARE @Values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '670', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @czyPrzyjalZlecenie = value_int FROM @values
	
	
	DECLARE @reasonId INT
	DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '700', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @reasonId = value_int FROM @values
    
    
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '291', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towingType = value_int FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '985,292', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @routing = value_int FROM @values

	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '868,869', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @trailerTowingSameDestination = value_int FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1005,992', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isGop = value_int FROM @values
	
	IF @isGop = 1
	BEGIN
		EXEC dbo.p_send_gop @groupProcessInstanceId = @groupProcessInstanceId	
	END 
	
	-- trailer 
	IF @towingType = 1 AND @trailerTowingSameDestination = 0
	BEGIN
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '874', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = 0,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	END 
	
	IF @variant = 5
	BEGIN
		PRINT dbo.f_translate('sciezka post-factum',default)
		RETURN
	END 
	
	-- Może wejść z @variant = 3, a to oznacza, że nie odebrał
	
	DECLARE @SOImportCase INT
	SELECT @SOImportCase = 1
	FROM process_instance  
	WHERE group_process_id = @groupProcessInstanceId
	AND step_id = '1009.041'
	AND last_run = 1
	
	
	IF (ISNULL(@czyPrzyjalZlecenie,0) = 1 OR @reasonId IN (109,110,111,112)) AND @variant = 1
	BEGIN
		-- Kontraktor przyjął zlecenie 
		
		DECLARE @token NVARCHAR(36)
		DECLARE @urlForm NVARCHAR(100)
		DECLARE @haveICS TINYINT
		DECLARE @fixingOrTowing INT
		DECLARE @onTime TINYINT
		DECLARE @content NVARCHAR(MAX)
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '691', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerPhone = LTRIM(RTRIM(value_string)) FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '697', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @parking = value_int FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @serviceId = value_int FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerId = value_int FROM @values
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @makeModel = textD FROM dbo.dictionary WHERE value IN (SELECT value_int FROM @values) AND typeD = 'makeModel'
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @programName = name FROM dbo.vin_program WHERE id IN (SELECT value_string FROM @values)
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,64', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @driverFirstname = value_string FROM @values
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,66', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @driverLastname = value_string FROM @values
		
		SET @driverName = @driverFirstname + ' ' + @driverLastname
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @driverPhone = value_string FROM @values
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @regNumber = value_string FROM @values
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @vin = value_string FROM @values
		
		-- Czy na termin
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '215', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @onTime = value_int FROM @values
		
		DECLARE @customDateText NVARCHAR(1000)
		SET @customDateText = ''
		
		-- Sprawdzanie czy na termin czy ETA
		IF ISNULL(@onTime, 0) = 1
		BEGIN
			
			DECLARE @onDateTime DATETIME
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @onDateTime = value_date FROM @values
			
			SET @customDateText = dbo.f_translate('Zlecenie umówione na ',default) + dbo.FN_VDateHour(@onDateTime) + '. ' 
				
		END
		ELSE
		BEGIN
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '698', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @etaMinutes = value_int FROM @values
			
			-- Gdyby jakimś cudem @etaMinutes była pusta to +20 minutes 
			SET @eta = DATEADD(minute , ISNULL(@etaMinutes, 20) , GETDATE() )  
			
			EXEC p_attribute_edit
				@attributePath = '107', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueDate = @eta,
				@err = @err OUTPUT,
				@message = @message OUTPUT
				
			IF @eta is not NULL
			BEGIN
				SET @customDateText = dbo.f_translate('Czas dojazdu wynosi ',default) + dbo.FN_VDateHour(@eta) + '. '
			END 
			
			SET @customDateText = @customDateText + dbo.f_translate('W razie trudnosci z dojazdem w podanym czasie prosimy o zaznaczenie odpowiedniej opcji w aplikacji lub kontakt telefoniczny.',default)
			
		END 
		
--		EXEC p_attribute_edit
--			@attributePath = '671', 
--			@groupProcessInstanceId = @groupProcessInstanceId,
--			@stepId = 'xxx',
--			@valueString = @token,
--			@err = @err OUTPUT,
--			@message = @message OUTPUT
			
		
		-- Zlecenie na ICS'a
		EXEC [dbo].[p_rsa_new_order]
			@groupProcessInstanceId = @groupProcessInstanceId,
			@partnerPhone = @partnerPhone,
			@alreadyConfirmed = 1
		
			

		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @fixingOrTowing = value_int FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '609', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @icsId = value_int FROM @values
		
--		IF @fixingOrTowing = 1
--		BEGIN
--			-- HOLOWANIE
--			
--			DECLARE @startLocationString NVARCHAR(1000)
--			DECLARE @endLocationString NVARCHAR(1000)
--			DECLARE @serviceName NVARCHAR(300)
--			
--			IF @parking = 1
--			BEGIN
--				SET @startLocationString = dbo.f_translate('z Państwa parkingu',default)
--			END 
--			ELSE
--			BEGIN
--				EXEC p_location_string
--				@attributePath = '101,85',
--				@groupProcessInstanceId = @groupProcessInstanceId,
--				@locationString = @startLocationString OUTPUT
--				
--				SET @startLocationString = dbo.f_translate('z lokalizacji zdarzenia: ',default)+ @startLocationString
--			END 
--			
--			EXEC p_location_string
--			@attributePath = '211,85',
--			@groupProcessInstanceId = @groupProcessInstanceId,
--			@locationString = @endLocationString OUTPUT
--
--			SELECT @serviceName = value_string FROM attribute_value where parent_attribute_value_id = @serviceId and attribute_path = '595,597,84'
--			SET @endlocationString = dbo.f_translate(' do partnera serwisowego ',default)+@serviceName+': '+@endlocationString
--			SET @towingRouteText = @startLocationString + ' ' + @endLocationString
--			
--			SET @content = dbo.f_translate('Zlecenie holowania ',default)+dbo.f_caseId(@groupProcessInstanceId)+' '+@towingRouteText+ dbo.f_translate(dbo.f_translate('/ Program: ',default),default)+@programName+dbo.f_translate('/ Kierowca: ',default)+@driverName+' '+@driverPhone+dbo.f_translate('/ Pojazd: ',default)+@makeModel+' '+@regNumber + '. Przypominamy o koniecznosci zakonczenia zlecenia w ciągu 8 godzin za posrednictwem aplikacji https://atlasdev.starter24.pl. '+@customDateText
--			
--		END 
--		ELSE
--		BEGIN
--			-- NAPRAWA
--			SET @content = dbo.f_translate('Zlecenie naprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)+dbo.f_translate(dbo.f_translate('/ Program: ',default),default)+@programName+dbo.f_translate('/ Kierowca: ',default)+@driverName+' '+@driverPhone+dbo.f_translate('/ Pojazd: ',default)+@makeModel+' '+@regNumber + '. Przypominamy o koniecznosci zakonczenia zlecenia w ciągu 8 godzin za posrednictwem aplikacji https://atlasdev.starter24.pl. '+@customDateText
--	
--		END 
		

		
		IF @reasonId IN (109,110,111,112) OR @routing = 1
		BEGIN
			
			
			IF isnull(@routing,0) = 1
			BEGIN
				CREATE TABLE #partnerTempTable (partnerId INT, partnerName NVARCHAR(300), priority INT, distance DECIMAL(18,2), phoneNumber NVARCHAR(100), reasonForRefusing NVARCHAR(100), partnerServiceId INT DEFAULT NULL)
				
				
				EXEC [dbo].[p_find_services]
				@groupProcessInstanceId = @groupProcessInstanceId

				SELECT TOP 1 @partnerId = partnerId FROM #partnerTempTable where partnerId IS NOT NULL
				
				EXEC [dbo].[p_attribute_edit]
				@attributePath = '522', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueInt = @partnerId,
				@err = @err OUTPUT,
				@message = @message OUTPUT		
				
				DROP TABLE #partnerTempTable
				
			END 
			ELSE
			BEGIN
				
				
				EXEC [dbo].[p_attribute_edit]
			     @attributePath = '522', 
			     @groupProcessInstanceId = @groupProcessInstanceId,
			     @stepId = 'xxx',
			     @userId = 1,
			     @originalUserId = 1,
			     @valueInt = @partnerId,
			     @err = @err OUTPUT,
			     @message = @message OUTPUT
			
			END 
			
			
	     	DECLARE @sourceId int
	     	DECLARE @targetId int
	     	
		    SELECT @sourceId = id FROM dbo.attribute_value with(nolock) where parent_attribute_value_id = @partnerId AND attribute_path = '595,597,85'
		
		    IF NOT EXISTS(SELECT id from dbo.attribute_value where attribute_path = '211' and group_process_instance_id = @groupProcessInstanceId)
		    BEGIN			    
	    		DECLARE @insertedAttributeValue int 
			    INSERT INTO dbo.attribute_value (root_process_instance_id, group_process_instance_id, attribute_path, created_at, updated_at, created_by_original_id, created_by_id, attribute_id)
		   		SELECT @rootId, @groupProcessInstanceId, '211', getdate(), getdate(), 1, 1, 211		
		   		SET @insertedAttributeValue = SCOPE_IDENTITY()		   		
		   		EXEC [dbo].[P_attribute_refresh] @attribute_value_id = @insertedAttributeValue
		    END 
		    
			DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '211,85', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @targetId = id FROM @values
			
			EXEC [dbo].[P_attribute_copy_structure]
			@source_attribute_value_id  = @sourceId,
			@target_attribute_value_id = @targetId,
			@process_instance_id = @previousProcessId
			
			
		END

    IF @icsId < 0
      BEGIN
        -- WYsłanie SMS'a do kontraktora


        EXEC [dbo].[p_sms_case_info]
            @groupProcessInstanceId = @groupProcessInstanceId,
            @addEtaChangeInfo = 0,
            @details = 1,
            @content = @content OUTPUT


        EXEC p_note_new
            @groupProcessId = @groupProcessInstanceId,
            @type = dbo.f_translate('sms',default),
            @content = @content,
            @phoneNumber = @partnerPhone,
            @addInfo = 0,
            @err = @err OUTPUT,
            @message = @message OUTPUT


      END




    -- SO - kontraktor przyjął
		IF ISNULL(@SOImportCase, 0) = 1
		BEGIN
			SET @variant = 4
		END 
		
		-- Skok do panelu usług
		EXEC dbo.p_jump_to_service_panel @previousProcessId = @previousProcessId, @variant = @variant
	
	END 
	ELSE
	BEGIN
		
		-- ZAPISANIE POWODU ODMOWY
		
		DECLARE @reasonMultiId INT
	    DECLARE @reasonPartnerId INT
	    DECLARE @reasonExists SMALLINT
	    DECLARE @parentReasonId INT
	    
		IF @variant = 3 AND ISNULL(@reasonId,0) NOT IN (109,110,111,112)  
		BEGIN
			-- Gdy kontraktor nie odbierze
			SET @reasonId = 0
			
		END
	    
	    DELETE FROM @values
	    INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
	    SELECT @reasonPartnerId = value_int FROM @values
	    
	    EXEC p_add_service_refuse_reason
		@groupProcessInstanceId = @groupProcessInstanceId,
		@partnerLocationId = @reasonPartnerId,
		@reasonId = @reasonId
	    
		/** SO - KONTRAKTOR NIE PRZYJĄŁ! **/

		IF ISNULL(@SOImportCase, 0) = 1
		BEGIN
			declare @now datetime
			DECLARE @idUslugi int
			DECLARE @kodStarter varchar(100)
			set @now=getdate()
						
			declare @locId int
			SELECT @locId = value_int FROM dbo.attribute_value with(nolock) where group_process_instance_id=@groupProcessInstanceId and attribute_path='610'
			SELECT @kodStarter = value_string FROM dbo.attribute_value with(nolock) where parent_attribute_value_id=@locId and attribute_path='595,597,631'

			DELETE FROM @values 
			INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '695',
				@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @idUslugi = value_int FROM @values
				
			EXECUTE StarterOperations_Test.dbo.p_uslugaKontraktorAkcje 
					@idUslugi
				,@kodStarter=@kodStarter
				,@data=@now
				,@eta=null
				,@czyOdrzucil=1
				,@powod=null
		END 
		-- koniec odmowy  
			
		EXEC p_attribute_edit
			@attributePath = '670', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
		EXEC p_attribute_edit
			@attributePath = '610', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
		EXEC p_attribute_edit
			@attributePath = '691', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueString = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
		-- ZRESETOWANIE POWODU 		
		EXEC p_attribute_edit
			@attributePath = '700', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = NULL,
			@err = @err OUTPUT,
			@message = @message OUTPUT
				
		SET @variant = 3
		
	END 
	
	PRINT '------------------------------- END ---------------------------------'
END