
ALTER PROCEDURE [dbo].[s_1007_077_a]
( 
	@nextProcessId INT,
	@currentUser INT = NULL, @token VARCHAR(50) = NULL OUTPUT
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @rootId INT
	DECLARE @groupProcessInstanceId INT	
	DECLARE @stepId NVARCHAR(20)
	DECLARE @companyId INT
	DECLARE @regNumber nvarchar(100)
	DECLARE @subject nvarchar(500)
	declare @body nvarchar(max)
	declare @email nvarchar(100)
	declare @senderEmail nvarchar(100)
	
	SELECT @senderEmail = dbo.[f_getEmail]('cfm')
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	-- Pobranie GroupProcessInstanceId --
	SELECT	@groupProcessInstanceId = group_process_id,			
			@rootId = root_id,
			@stepId = step_id
	FROM process_instance  with(nolock)
	WHERE id = @nextProcessId 
	
	IF @stepId = '1007.078'
	BEGIN
		UPDATE dbo.process_instance SET company_id = 7896 WHERE id = @nextProcessId -- alphabet
		
		delete from @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @regNumber = value_string FROM @values
		
		set @subject = @regNumber + ' / ' + dbo.f_caseId(@groupProcessInstanceId) + dbo.f_translate(' / Autoryzacja kosztów EXTRA',default)
		
		SET @body = 'Szanowni Panstwo,<br/>wypożyczalnia poinformowała nas o powstaniu dodatkowych kosztów przy wynajmie pojazdu zastępczego. Prosimy o zapoznanie się ze sprawą i załącznikami na <a target="_new" href="'+dbo.f_getDomain()+'/operational-dashboard?action=telephony-pick-up&isforce=1&process_instance='+CAST(@nextProcessId AS NVARCHAR(20))+'">Viewerze</a> oraz autoryzację dodatkowych kosztów.'

		EXEC [dbo].[p_cfm_partner_contact]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@contact = @email output
		
		EXECUTE dbo.p_note_new 
	     @groupProcessId = @groupProcessInstanceId
	    ,@type = dbo.f_translate('email',default)
	    ,@content = @body
	    ,@email = @email
	    ,@userId = 1  -- automat
	    ,@subject = @subject
	    ,@direction=1
	    ,@dw = ''
	    ,@udw = ''
	    ,@sender = @senderEmail
--	    ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
	    ,@emailBody = @body
	    ,@emailRegards = 1
	    ,@err=@err OUTPUT
	    ,@message=@message OUTPUT	
	    
	    
	END 
	
END




