ALTER PROCEDURE [dbo].[s_1011_050]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @valid int 
	DECLARE @programId nvarchar(255)
	DECLARE @isArc int 
	DECLARE @verifyFDDS NVARCHAR(MAX)
	declare @tips nvarchar(max)
	
	SELECT	@groupProcessInstanceId = group_process_id	
	FROM process_instance
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	IF @variant = 99
	BEGIN
		 RETURN
	END
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '941', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @valid = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '994', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @tips = value_text FROM @values
	
	IF @valid = 0
	BEGIN
		SET @variant = 2
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '202', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueString = null,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
	END 
	ELSE IF @programId is not null
	BEGIN
		
		if @programId = '423'
		BEGIN
			SET @isArc = 0	
		END	
		ELSE
		BEGIN
			SET @isArc = 1
		END 
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '713', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = @isArc,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
	    EXEC [dbo].[p_attribute_edit]
	       @attributePath = '204', 
	       @groupProcessInstanceId = @groupProcessInstanceId,
	       @stepId = 'xxx',
	       @userId = 1,
	       @originalUserId = 1,
	       @valueString = @programId,
	       @err = @err OUTPUT,
	       @message = @message OUTPUT
	    
	    INSERT @values EXEC p_attribute_get2 @attributePath = '950', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @verifyFDDS = value_text FROM @values
	    
		IF ISNULL(@verifyFDDS, '') <> ''
		BEGIN
			
			EXECUTE dbo.p_note_new 
		  	 @groupProcessId = @groupProcessInstanceId
		  	,@type = dbo.f_translate('text',default)
		  	,@content = @verifyFDDS
		  	,@subject = 'Wynik weryfikacji w serwsie http://fdds.arctransistance.com'
		  	,@userId = 1  -- automat
		  	,@direction=2
		  	,@err=@err OUTPUT
		  	,@message=@message OUTPUT
		  	
		END
		
	END 
	 
	IF isnull(@tips,'') <> ''
	BEGIN
		EXEC [dbo].[p_note_new]
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('text',default),
		@content = @tips,
		@userId = @currentUser,
		@originalUserId = @currentUser,
		@special = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	END 
 	
END



