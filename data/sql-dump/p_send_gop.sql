ALTER PROCEDURE [dbo].[p_send_gop]
( 
	@groupProcessInstanceId INT,
	@email NVARCHAR(255) = null,
	@final INT = 0,
	@test INT = 0,
	@preview int = 0,
	@userName NVARCHAR(255) = null
) 
AS
BEGIN	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @stepId NVARCHAR(255)
	DECLARE @attributeValueId INT
	DECLARE @partnerId INT	
	DECLARE @subject NVARCHAR(300)
	DECLARE @fileName NVARCHAR(300)
	DECLARE @attachment NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @partnerName NVARCHAR(300)
	DECLARE @helpline NVARCHAR(20)
	DECLARE @platform NVARCHAR(50)
	DECLARE @starterEmail NVARCHAR(200)
	DECLARE @platformId INT	
	DECLARE @partnerIdPath NVARCHAR(255) = '610'
	DECLARE @partnerGroupId INT = @groupProcessInstanceId
	DECLARE @Text as table ( answer varchar(MAX) )
	DECLARE @location NVARCHAR(255)
	DECLARE @crossBorder INT	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @serviceDictionaryId INT = null
	DECLARE @caseId NVARCHAR(20)
	declare @serviceId INT 
	declare @towingGop int 
	declare @rootId int 
	declare @rootPlatformId int 
	declare @organisationFromHome int
	declare @regNumber nvarchar(20)
	select @rootId = root_id from dbo.process_instance where id = @groupProcessInstanceId
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	SELECT @rootPlatformId = value_int FROM @values
	
	SELECT @platform = name, @helpline = official_line_number FROM dbo.platform where id = @rootPlatformId
	

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @location = value_string FROM @values	
	SELECT @crossBorder = IIF(ISNULL(@location,dbo.f_translate(dbo.f_translate('Polska',default),default)) = dbo.f_translate(dbo.f_translate('Polska',default),default),0,1)
	

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '206', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @organisationFromHome = value_int FROM @values

	if @platformId=5
    BEGIN
		set @crossBorder=0 --ADAC
	END

	SET @starterEmail = [dbo].[f_getEmail]('rentals')
	SELECT @serviceId = dbo.f_group_service_id(@groupProcessInstanceId)
	
	IF @serviceId = 17
	BEGIN
		SET @starterEmail = [dbo].[f_getEmail]('ha')
	END 

	IF @serviceId = 18
	BEGIN
		SET @starterEmail = [dbo].[f_getEmail]('callcenter')
	END 

	DECLARE @platformGroup nvarchar(255)	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		SET @starterEmail = dbo.f_getEmail('cfm')
	END

	if @platformId=5
		BEGIN
			SET @starterEmail = dbo.f_getEmail('adac')
		END
	declare @starterAbroad int
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1003', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @starterAbroad = value_int FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1005,992', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towingGop = value_int FROM @values
	
	IF isnull(@organisationFromHome,0) = 1
	BEGIN
		SET @crossBorder = 0
	END 

	IF @crossBorder = 1 AND ISNULL(@starterAbroad,0) = 0
	BEGIN
		SET @partnerIdPath = '741'
		SET @starterEmail = [dbo].[f_getEmail]('arc')

		declare @extCaseId nvarchar(100)
		declare @VIN NVARCHAR(50)

		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '711', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @extCaseId = value_string FROM @values
			
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @VIN = value_string FROM @values
	
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @regNumber = value_string FROM @values
		
		set @caseId=dbo.f_caseId(@groupProcessInstanceId)
		
		IF @userName IS NOT NULL
		BEGIN
			EXEC p_attribute_edit
			@attributePath = '838,840', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueString = @userName,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		END
	END 
	ELSE
	BEGIN
	
		SET @subject = dbo.f_conditionalText(@final,dbo.f_translate('Ostateczna gwarancja',default),dbo.f_translate('Gwarancja',default))+dbo.f_translate(' płatności dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
		SET @fileName = dbo.f_conditionalText(@final,'ostateczna_','')+'gwarancja_platnosci_'+dbo.f_caseId(@groupProcessInstanceId)+'_'+CAST(CAST(GETDATE() AS DATE) AS NVARCHAR(200))
	END 
	
	SELECT @stepId = step_id FROM dbo.process_instance WHERE id = @groupProcessInstanceId
	
	IF @stepId like '1009.%' AND @towingGop = 1
	BEGIN
		
		SET @partnerIdPath = '610' 
		
		EXEC p_gop_data_towing
		@groupProcessInstanceId = @groupProcessInstanceId,
		@final = @final,
		@content = @content OUTPUT,
		@partnerId = @partnerId OUTPUT
	END
	ELSE IF @stepId LIKE '1007.%'
	BEGIN
		IF @crossBorder = 1
		BEGIN
			SET @partnerIdPath = '837,704'

			set @body=dbo.f_translate('Regarding case no ',default)+isnull(@caseId,'')+'<BR>'+
				isnull('Registration number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
				Please organize the replacement vehicle as in attachment<BR><BR><BR>
				Best regards<BR>
				Starter24'
		END 
		ELSE
		BEGIN
			SET @serviceDictionaryId = 5
			SET @partnerIdPath = '764,742'



      IF @platformId = 5
        BEGIN

					EXEC p_gop_data_adac @groupProcessInstanceId = @groupProcessInstanceId,
						@final = @final,
						@userName =@userName,
						@serviceId = 3,
						@partnerId = @partnerId output ,
						@content = @content OUTPUT


      end
      ELSE
        BEGIN



			EXEC p_gop_data_replacement_car
			@groupProcessInstanceId = @groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT


					DECLARE @platformGroupName NVARCHAR(50) = null

					EXEC p_platform_group_name  @groupProcessInstanceId = @groupProcessInstanceId,
																			@name = @platformGroupName OUTPUT

					IF isnull(@platformGroupName,'') = dbo.f_translate('CFM',default)
						BEGIN

							DECLARE @accidentType nvarchar(400)


							DECLARE @eventTypeId int



							DELETE FROM @values
							INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
							SELECT @eventTypeId = value_int from @values


							SELECT @accidentType = textD from dictionary where typeD = 'EventType' and value = @eventTypeId


							DELETE FROM @values
							INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
							SELECT @regNumber = value_string from @values



							SET @content =  +'<br>' +  isnull(@accidentType,'') + ' / ' + isnull(@regNumber,'') + '<br><br><br>' +  isnull(@content,'')
						end
						
						
					DECLARE @closeToken NVARCHAR(255)
					DECLARE @rentalGroup INT 
					
					DELETE FROM @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '168,839', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @rentalGroup = value_int FROM @values
					
					SELECT top 1 @closeToken = token from dbo.process_instance where group_process_id = ISNULL(@rentalGroup,@groupProcessInstanceId) and step_id = '1007.079' and active = 1 
					
					IF @closeToken IS NOT NULL AND @final = 0
				    BEGIN
					    SET @content = @content + '<p>Jeśli wynajem zakończy się przed czasem mogą Państwo przyspieszyć wygenerowania Raportu Zakończenia Wynajmu za pomocą formularza który znajduje się <a target="_new" href="'+dbo.f_getDomain()+dbo.f_translate('/operational-dashboard/public/',default)+@closeToken+'">tutaj</a>'
				    END 
          END
		END 
		
		
	END
	ELSE IF @stepId LIKE '1016.%'
	BEGIN
		
		SET @serviceDictionaryId = IIF(@crossBorder = 0,14,null)
	
		if @crossBorder = 1
		BEGIN
			declare @persons int
			
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '152,154', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @persons = value_int FROM @values
				
			declare @address1 nvarchar(100)
			declare @address2 nvarchar(100)
		
			Select @address1=dbo.f_addressText(id) from dbo.attribute_value where group_process_instance_id=@groupProcessInstanceId and attribute_path='152,175,85'
			Select @address2=dbo.f_addressText(id) from dbo.attribute_value where group_process_instance_id=@groupProcessInstanceId and attribute_path='152,320,85'
			
			set @body='Hello from Poland,<BR><BR>
			Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR>'+
			isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
			Please organize taxi for '+isnull(cast(@persons as varchar(10)),'')+' persons<BR>'+
		   dbo.f_translate('From: ',default)+isnull(@address1,'')+'<BR>'+
		   'To: '+isnull(@address2,'')+'<BR>
			<BR>
			Best regards,<BR>
			Starter24'		   
		END
		ELSE
		BEGIN
			IF @platformId = 5
				BEGIN
					exec p_gop_data_adac
							@groupProcessInstanceId = @groupProcessInstanceId,
							@final = @final,
							@serviceId = @serviceId,
							@userName =@userName,
							@content = @content OUTPUT,
							@partnerId = @partnerId OUTPUT

					print @content
				end
			ELSE
				BEGIN
			EXEC p_gop_data_taxi
			@groupProcessInstanceId = @groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT
					END
		END 
	
	END 
	ELSE IF @stepId LIKE '1014.%'
	BEGIN
	
		SET @serviceDictionaryId = IIF(@crossBorder = 0,6,null)
		
		IF @crossBorder = 1
		BEGIN
			set @body=dbo.f_translate('Regarding case no ',default)+isnull(@caseId,'')+'<BR>'+
				isnull('Registration number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
				Please organize the hotel as in attachment<BR><BR><BR>
				Best regards<BR>
				Starter24'
		END 
		ELSE
		BEGIN
			IF @platformId = 5
				BEGIN
					exec p_gop_data_adac
							@groupProcessInstanceId = @groupProcessInstanceId,
							@final = @final,
							@serviceId = @serviceId,
							@userName =@userName,
							@content = @content OUTPUT,
							@partnerId = @partnerId OUTPUT

				end
			ELSE
				BEGIN
			EXEC p_gop_data_hotel
			@groupProcessInstanceId = @groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT	
		END
			END
		
		
	END 
	ELSE IF @stepId LIKE '1018.%'
	BEGIN
	
		--if @crossBorder = 1
		--BEGIN
		--	set @body='Hello from Poland,<BR<BR>
		--		Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR>'+
		--		isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
		--		Please provide us information about information listed below: Workshop:<BR>
		--		Adress of the workshop:<BR>
		--		Phone number:<BR>
		--		Opening hours:<BR>
		--		Repair on warranty?*(yes/no)<BR>
		--		*(if no) cost of the repair that Client has to pay in workshop<BR>
		--		Repair status?**<BR>
		--		**(if not finished) when it will be?<BR>
		--		Parking cost (yes/no)<BR>
		--		***(if yes) total cost<BR>
		--		Best regards,<BR>
		--		Starter24'		   
		--END
		IF @platformId = 5
			BEGIN
				EXEC p_gop_data_adac @groupProcessInstanceId = @groupProcessInstanceId,
														 @final = @final,
														 @userName =@userName,
														 @serviceId = @serviceId,
														 @partnerId = @partnerId output ,
														 @content = @content OUTPUT

			end
		ELSE
			BEGIN
		EXEC p_gop_data_transport
		@groupProcessInstanceId = @groupProcessInstanceId,
		@final = @final,
		@content = @content OUTPUT,
		@partnerId = @partnerId OUTPUT
				END

		SET @subject = dbo.f_conditionalText(@final,dbo.f_translate('Ostateczna gwarancja',default),dbo.f_translate('Gwarancja',default))+dbo.f_translate(' płatności dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
		SET @fileName = dbo.f_conditionalText(@final,'ostateczna_','')+'gwarancja_platnosci_'+dbo.f_caseId(@groupProcessInstanceId)+'_'+CAST(CAST(GETDATE() AS DATE) AS NVARCHAR(200))
		SET @partnerIdPath = '610'
	END 
	ELSE IF @stepId LIKE '1142.%'
	BEGIN
		
		-- 8 - ID USŁUGI PARKING W BAZIE KONTRAKTORÓW
		SET @serviceDictionaryId = 8
	
		IF @crossBorder = 1
		BEGIN
			
			-- Ustawienie pola UWAGI

			DECLARE @days INT
			DECLARE @startParking DATETIME
			DECLARE @endParking DATETIME
			DECLARE @info NVARCHAR(MAX)
			
			-- Data Start parkowania
		 	DELETE FROM @Values
		 	INSERT  @values EXEC dbo.p_attribute_get2
		  		@attributePath = '862,823',
		  		@groupProcessInstanceId = @groupProcessInstanceId
		  	SELECT @startParking = value_date FROM @values
		 	
		  	-- Data End parkowania
			INSERT  @values EXEC dbo.p_attribute_get2
		 		@attributePath = '862,853',
		 		@groupProcessInstanceId = @groupProcessInstanceId
		 	SELECT @endParking = value_date FROM @values
		 	
		 	SET @days = dbo.f_getParkingDays(@groupProcessInstanceId)
		 	SET @info = dbo.f_translate('Please organize parking for ',default) + CAST(ISNULL(@days, 1) AS VARCHAR(10)) + ' days<BR>'+ '<br> Start day: ' + dbo.FN_VDateHour(@startParking) + '<br> End day: ' + dbo.FN_VDateHour(@endParking) 

		 	EXEC dbo.p_attribute_edit
			@attributePath = '838,63', -- UWAGI
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueText = @info,
			@err = @err OUTPUT,
			@message = @message OUTPUT
				
			SET @body='Hello from Poland,<BR><BR>
			Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR>'+
			isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
			Please organize parking for '+isnull(cast(@days as varchar(10)),'')+' days<BR>'+
		   dbo.f_translate('From: ',default) + dbo.FN_VDateHour(@startParking) + '<BR>'+
		   'To: ' + dbo.FN_VDateHour(@endParking) + '<BR>
			<BR>
			Best regards,<BR>
			Starter24'		
		   
		   
		END
		ELSE
		BEGIN
			IF @platformId = 5
			BEGIN
				EXEC p_gop_data_adac @groupProcessInstanceId = @groupProcessInstanceId,
														 @final = @final,
														 @userName =@userName,
														 @serviceId = @serviceId,
														 @partnerId = @partnerId output ,
														 @content = @content OUTPUT

			end
			ELSE
				BEGIN


			EXEC p_gop_data_parking
			@groupProcessInstanceId = @groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT
				end
		END 
	END
	ELSE IF @stepId LIKE '1148.%'
	BEGIN
		
		SET @serviceDictionaryId = 19
		
		 EXEC dbo.p_gop_data_HomeAssistance 
			@groupProcessInstanceId =@groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT
	END
	ELSE IF @stepId LIKE '1152.%'
	BEGIN
		IF @platformId = 5
			BEGIN
				SET @starterEmail = [dbo].[f_getEmail]('callcenter')

				EXEC p_gop_data_adac @groupProcessInstanceId = @groupProcessInstanceId,
														 @final = @final,
														 @userName =@userName,
														 @serviceId = 80, -- stuczne id, bo medical ma wiele opcji
														 @partnerId = @partnerId output ,
														 @content = @content OUTPUT
			end
		ELSE
			BEGIN
		EXEC dbo.p_gop_data_medical
			@groupProcessInstanceId =@groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT
		
		SET @subject = dbo.f_caseId(@groupProcessInstanceId)+dbo.f_translate(' E-Konsultacja z lekarzem',default) 
		SET @fileName = dbo.f_caseId(@groupProcessInstanceId)+'_EKonsultacja_z_lekarzem_'+CAST(CAST(GETDATE() AS DATE) AS NVARCHAR(200))
		END
		if @crossBorder=1
		begin
			SET @partnerIdPath = '741'
		end
		else
		begin
			SET @partnerIdPath = '610'
		end
	--	SET @serviceDictionaryId = 19
	END
	ELSE IF @stepId LIKE '1154.%'
	BEGIN
		IF @platformId = 5
			BEGIN
				EXEC p_gop_data_adac @groupProcessInstanceId = @groupProcessInstanceId,
														 @final = @final,
														 @userName =@userName,
														 @serviceId = @serviceId,
														 @partnerId = @partnerId output ,
														 @content = @content OUTPUT
			end
		ELSE
			BEGIN
		EXEC dbo.p_gop_data_scrapping
			@groupProcessInstanceId =@groupProcessInstanceId,
			@final = @final,
			@content = @content OUTPUT,
			@partnerId = @partnerId OUTPUT

		SET @subject = dbo.f_conditionalText(@final,dbo.f_translate('Ostateczna gwarancja',default),dbo.f_translate('Gwarancja',default))+dbo.f_translate(' płatności dla sprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)
		SET @fileName = dbo.f_conditionalText(@final,'ostateczna_','')+'gwarancja_platnosci_'+dbo.f_caseId(@groupProcessInstanceId)+'_'+CAST(CAST(GETDATE() AS DATE) AS NVARCHAR(200))
		SET @partnerIdPath = '610'
	--	SET @serviceDictionaryId = 19

		End

	END
	
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = @partnerIdPath, @groupProcessInstanceId = @partnerGroupId
	SELECT @partnerId = value_int FROM @values
	
	--- serwisy BRS: nie wysyłamy GOP	
	DECLARE @partnerKind nvarchar(255) 
	
	SELECT @partnerKind = av.value_string 
	from dbo.attribute_value av 
	where av.parent_attribute_value_id = @partnerId and av.attribute_path = '595,597,596'
	
	IF dbo.f_exists_in_split('85,94,95,98,99,100,101',@partnerKind) = 1 AND @preview = 0
	BEGIN
		RETURN
	END 
	
	IF @email is null 
	BEGIN
		
		DECLARE @overrideEmail nvarchar(255)
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '764,368', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @overrideEmail = value_string FROM @values
		
		if @overrideEmail is NULL
		BEGIN
		
			SELECT TOP 1 @email = dbo.f_partner_contact(@groupProcessInstanceId,@partnerId, @serviceDictionaryId, 4)	
			
			-- Szuka e-maila w innej pozycji
			IF ISNULL(@email, '') = '' 
			BEGIN
				SELECT TOP 1 @email = dbo.f_partner_contact(@groupProcessInstanceId,@partnerId, @serviceDictionaryId, 16)	
			END
			
		END 
		ELSE 
		BEGIN
			SET @email = @overrideEmail
		END 
	END



	
	SELECT @partnerName = dbo.f_partnerName(@partnerId)	
	
	IF @crossBorder = 1 AND @preview = 0 AND @stepId NOT LIKE '1018.%' AND @stepId NOT LIKE '1152.%'
	BEGIN	
					
		SET @subject = dbo.f_translate('Assistance Organization Request. Ref: ',default) + dbo.f_caseId(@groupProcessInstanceId)
		
		PRINT '------------------------ CHECK'
		PRINT @email
		PRINT @starterEmail
		PRINT @content

-- 		 Zabezpieczenie na wypadek nieoczekiwanej sytuacji - nie usuwać
		if @body is null
			BEGIN
				SET @body = 'Assistance Organization Request. <br>
Best regards,<br>
Starter24 <br>'
			end

		EXECUTE dbo.p_note_new 
			 @groupProcessId = @groupProcessInstanceId
			,@type = dbo.f_translate('email',default)
			,@content = @body
			,@email = @email
			,@userId = 1  -- automat
			,@subject = @subject
			,@direction=1
			,@dw = ''
			,@udw = ''
			,@sender = @starterEmail
			,@additionalAttachments = '{FILE::assistance_organization_request::AssistanceOrganizationRequest::true}'
			,@emailBody = @body
			,@err=@err OUTPUT
			,@message=@message OUTPUT
			
		RETURN
	END 
	
    
	DECLARE @domain_ VARCHAR(100) = [dbo].[f_getDomain]()
    
	if @stepId like '1148.%'
	begin
	select @body = '
	<!DOCTYPE html>
	<html lang="pl">
	<head>
	    <meta charset="utf-8">
	            <link rel="stylesheet" type="text/css"  href="'+@domain_+'/bundles/web/css/vendor/bootstrap/bootstrap.min.css" >
	    </head>
	<body>
	     <div class="container-fluid" style="padding-top: 20px; padding-bottom: 20px;">
	         <div class="row">
	            <div class="col-xs-9 text-left">
	                <h3>___TITLE___ ___CASEID___</h3>
	            </div>
	            <div class="col-xs-3">
	                <img style="display: inline-block; max-width: 100%" src="'+@domain_+'/images/email_assets/starter24.png" alt="Logo starer">
	            </div>
	        </div>
	
	         <div class="row" style="padding-top: 30px;">
	            <div class="col-xs-8">
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Do:</b>
	                    </div>
	                    <div class="col-xs-10">
	                        ___PARTNER_LOCATION___
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Od:</b>
	                    </div>
	                    <div class="col-xs-10">
	                        <p>
	                            Starter24 Sp. z o.o.
	                                                    </p>
	                   <p><b>Telefon kontaktowy dla Państwa: +48 61 83 19 894</b></p>
	              <p>Infolinia dla Klienta: ___HELPLINE___</p>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Data:</b>
	                    </div>
	               <div class="col-xs-10">
	                        ___DATE_TIME___
	                    </div>
	              </div>
	            </div>
	             <div class="col-xs-4">
	                 <div class="row text-right">
	       <div class="col-xs-12">
	                         <h5>___PLATFORM_NAME___ Assistance</h5>
	                     </div>
	                     <div class="col-xs-12">
	                         ___STARTER_EMAIL___
	                     </div>
	                 </div>
	             </div>
	         </div>
	     </div>
	    <div class="container-fluid">
	        <div class="row" style="border-top: 1px #000 dotted; padding-top: 15px; padding-bottom: 15px;">
	            <div class="col-xs-10 col-xs-offset-1">
	                ___CONTENT___
	
	            </div>
	        </div>
	
	                                                                                                    </div>
	
	    <div class="container-fluid">
	        <div class="row" style="border: 1px #000 dotted; border-width: 1px 0; padding-top: 10px; padding-bottom: 10px">
	            <div class="col-xs-6">
	                <p><b>Dane do wystawienia faktury:</b></p>
	                <div class="row">
	                    <div class="col-xs-10 col-xs-offset-2">
	                        Starter24 Sp. z o.o.<br>
	                        ul. Józefa Kraszewskiego 30<br>
	                        60-519 Poznań<br>
	                        NIP: 525-21-83-310<br>
	     </div>
	               </div>
	            </div>
	            <div class="col-xs-6" style="border-left: 1px #000 dotted">
	                <p><b>Fakturę prosimy wysyłać na adres:</b></p>
	                <div class="row">
	                    <div class="col-xs-10 col-xs-offset-2">
	                        Starter24 Sp. z o.o.<br>
	                        ul. Józefa Kraszewskiego 30<br>
	                        60-519 Poznań<br>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row" style="padding-top: 10px">
	            ___FOOTER___
	        </div>
	    </div>
	</body>
	</html>'
	end
	else
	begin
    select @body = '
	<!DOCTYPE html>
	<html lang="pl">
	<head>
	    <meta charset="utf-8">
	            <link rel="stylesheet" type="text/css"  href="'+@domain_+'/bundles/web/css/vendor/bootstrap/bootstrap.min.css" >
	    </head>
	<body>
	     <div class="container-fluid" style="padding-top: 20px; padding-bottom: 20px;">
	         <div class="row">
	            <div class="col-xs-9 text-left">
	                <h3>___TITLE___ ___CASEID___</h3>
	            </div>
	            <div class="col-xs-3">
	                <img style="display: inline-block; max-width: 100%" src="'+@domain_+'/images/email_assets/starter24.png" alt="Logo starer">
	            </div>
	        </div>
	
	         <div class="row" style="padding-top: 30px;">
	            <div class="col-xs-8">
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Do:</b>
	                    </div>
	                    <div class="col-xs-10">
	                        ___PARTNER_LOCATION___
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Od:</b>
	                    </div>
	                    <div class="col-xs-10">
	                        <p>
	                            Starter24 Sp. z o.o.
	                                                    </p>
	                        <p><b>Telefon kontaktowy dla Państwa: +48 61 83 19 894</b></p>
	              <p>Infolinia dla Klienta: ___HELPLINE___</p>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-xs-2">
	                       <b>Data:</b>
	                    </div>
	                    <div class="col-xs-10">
	                        ___DATE_TIME___
	                    </div>
	              </div>
	            </div>
	             <div class="col-xs-4">
	                 <div class="row text-right">
	                     <div class="col-xs-12">
	                         <h5>___PLATFORM_NAME___ Assistance</h5>
	                   </div>
	                     <div class="col-xs-12">
	                         ___STARTER_EMAIL___
	                     </div>
	                 </div>
	             </div>
	         </div>
	     </div>
	    <div class="container-fluid">
	        <div class="row" style="border-top: 1px #000 dotted; padding-top: 15px; padding-bottom: 15px;">
	            <div class="col-xs-10 col-xs-offset-1">
	                ___CONTENT___
	
	            </div>
	        </div>
	
	                                                                                                    </div>
	
	    <div class="container-fluid">
	        <div class="row" style="border: 1px #000 dotted; border-width: 1px 0; padding-top: 10px; padding-bottom: 10px">
	            <div class="col-xs-6">
	                <p><b>Dane do wystawienia faktury:</b></p>
	                <div class="row">
	                    <div class="col-xs-10 col-xs-offset-2">
	                        Starter24 Sp. z o.o.<br>
	                        ul. Józefa Kraszewskiego 30<br>
	                        60-519 Poznań<br>
	                        NIP: 525-21-83-310<br>
	                    </div>
	                </div>
	            </div>
	            <div class="col-xs-6" style="border-left: 1px #000 dotted">
	     <p><b>Fakturę prosimy wysyłać na adres:</b></p>
	                <div class="row">
	                    <div class="col-xs-10 col-xs-offset-2">
	                        Starter24 Sp. z o.o.<br>
	                        ul. Józefa Kraszewskiego 30<br>
	                        60-519 Poznań<br>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row" style="padding-top: 10px">
	            ___FOOTER___
	        </div>
	    </div>
	</body>
	</html>'
    end
	
    DECLARE @footer NVARCHAR(MAX)

    IF @stepId LIKE '1152.%'
    BEGIN
	    
	    SET @starterEmail = [dbo].[f_getEmail]('arc')
	    
	    SET @helpline = '+48 61 83 19 907'
	    
	    SET @footer = '<div class="col-xs-12 small">
		                <ul style="list-style: none">
		                    <li>1) Jako nabywcę faktury prosimy wpisać nazwę wymienioną obok pozycji „Dane do wystawienia faktury”.</li>
		                    <li>2) W opisie usługi na fakturze prosimy zawrzeć imię i nazwisko Zlecającego (pacjenta) oraz numer sprawy Starter24. </li>
		                    <li>3) Koniecznym załącznikiem do faktury jest niniejsza gwarancja płatności.</li>
		                    <li>4) Fakturę z załącznikami należy przesłać do firmy Starter24 w ciągu 5 dni od daty wykonania usługi.</li>
		                    <li>5) W przypadku zmiany kwoty gwarancji – każda kolejna gwarancja anuluje poprzednie.</li>
		                    <li>6) Płatność nastąpi w ciągu 30 dni od otrzymania faktury.</li>
		                    <li>7) Niedotrzymanie powyższych warunków może opóźnić lub wstrzymać płatność za usługę.</li>
		                </ul>
						<div>W każdym przypadku to wskazany przez Starter24 Klient (bądź wskazany przez Klienta Beneficjent - Zleceniodawca) jest dla Zleceniobiorcy stroną umowy o świadczenie usług pomocy medycznej. Strony zgodnie potwierdzają, że Starter24 nie jest podmiotem leczniczym, ponieważ nie zleca we własnym imieniu wykonywania zleceniobiorcy usług medycznych, ale jedynie przekazuje informacje o Kliencie i Beneficjencie - Zleceniodawcy, które są stroną zlecającą usługę Zleceniobiorcy jako podmiotowi leczniczemu.</div>
		            </div>'
    END
    ELSE 
    BEGIN
			DECLARE @footerList NVARCHAR(MAX) = '<li>1) Jako nabywcę faktury prosimy wpisać nazwę wymienioną obok pozycji „Dane do wystawienia faktury:”, natomiast fakturę prosimy wysłać na adres: Starter24 Sp. z o.o., ul. Józefa Kraszewskiego 30, 60-519 Poznań.
		                    </li>
		                    <li>2) Koniecznym załącznikiem do faktury jest niniejsza gwarancja płatności oraz RZW.</li>
		                    <li>3) Fakturę z załącznikami należy przesłać do firmy Starter24 w ciągu 5 dni od daty wykonania usługi.</li>
		                    <li>4) Niespełnienie powyższych warunków może spowodować wstrzymanie płatności za usługę.</li>
		                    <li>5) W przypadku zmiany kwoty gwarancji - kolejna gwarancja anuluje poprzednie.</li>
		                    <li>6) Płatność nastąpi w ciągu 30 dni od otrzymania faktury.</li>
		                    <li>7) W przypadku wynajmu auta prosimy o zaznaczenie na Państwa fakturze, czy wynajmowany samochód posiadał homologację auta ciężarowego.</li>
		                    <li>8) Niedotrzymanie powyższych warunków może opóźnić lub wstrzymać płatność za usługę.</li>'
			IF @platformGroup = dbo.f_translate('CFM',default)
				BEGIN
					SET @footerList = @footerList+'<li> 9) Kwota podana na GOP jest kwot maksymalna, a wystawiona za wynajem faktura nie może przewyższać kosztów/stawek wynikajcych z aktualnego cennika ustalonego przez strony oraz z umów zawartych przez wynajmujcego z podmiotem trzecim (m. in. firm Car Fleet Management), o ile umowa taka ma zastosowanie w niniejszym przypadku.'
				end
			ELSE IF @platformGroup = dbo.f_translate('PASS2',default) or @platformGroup = dbo.f_translate('VGP',default) or @platformGroup = dbo.f_translate('PSA',default)
				BEGIN
					SET @footerList = @footerList+'<li> 9) Kwota podana na GOP jest kwot maksymalna, a wystawiona za wynajem faktura nie może przewyższać kosztów/stawek wynikajcych z aktualnego cennika ustalonego przez strony.</li>'
				end

			SET @footer = '<div class="col-xs-12 small">
		                <ul style="list-style: none">
		                    '+@footerList+'
		                </ul>
		            </div>'

    END
   
   


	SET @body = REPLACE(@body,'___FOOTER___',@footer)
    SET @body = REPLACE(@body,'___CASEID___','')
    if @stepId like '1152%'
	begin
		SET @body = REPLACE(@body,'___TITLE___',dbo.f_caseId(@groupProcessInstanceId))   
	end
	else
	begin
		SET @body = REPLACE(@body,'___TITLE___',@subject)   
	end
    SET @body = REPLACE(@body,'___PARTNER_LOCATION___',ISNULL(@partnerName,'')) 
    SET @body = REPLACE(@body,'___CONTENT___',@content)    
    SET @body = REPLACE(@body,'___DATE_TIME___', dbo.FN_VDateHour(GETDATE()))
	SET @body = REPLACE(@body,'___HELPLINE___', isnull(@helpline,'+48 600 222 222'))
	SET @body = REPLACE(@body,'___STARTER_EMAIL___', @starterEmail)
    SET @body = REPLACE(@body,'___PLATFORM_NAME___', @platform)
    
	if @content is null
	begin
	SET @content = 'Szanowni Państwo,<br/> 
					Niniejszym przesyłamy '+dbo.f_conditionalText(@final,dbo.f_translate('ostateczną ',default),'')+'gwarancję płatności.<br/>
					Szczegóły znajdą Państwo w załączonym pliku.<br/><br/>

					W korespondencji z nami należy zawsze używać adresu mailowego '+@starterEmail+', pamiętając by w temacie wiadomości zawrzeć nasz numer sprawy (widoczny w temacie niniejszego maila).<br/>
					W sprawach bardzo pilnych uprzejmie prosimy o dodatkowy kontakt telefoniczny pod numerem 61 83 19 984.<br/><br/>

					<b>UWAGA!</b><br/>
					W celu usprawnienia komunikacji i rozliczeń pomiędzy naszymi firmami oraz koniecznością wyeliminowania zmian danych dotyczących wynajmu po zrealizowaniu i zamknięciu usługi, uprzejmie prosimy o przestrzeganie następujących zasad:
					<ul><li>firma wynajmująca pojazd w ciągu 24h od zakończenia wynajmu dostarczy kompletną informację o kosztach zorganizowanych usług</li> 
					<li>ostatniego dnia wynajmu na adres email Starter24 prześle firmie wynajmującej link do formularza RZW</li>
					<li>Starter24 odeśle firmie wynajmującej kopię uzupełnionego RZW</li>
					<li>po weryfikacji Starter24 odeśle firmie wynajmującej potwierdzenie akceptacji RZW w formie "Ostatecznej gwarancji płatności"</li>
					<li>firma wynajmująca wystawi fakturę nie wcześniej niż po uzyskaniu potwierdzenia o akceptacji RZW oraz zgodnie z treścią ostatecznej gwarancji płatności</li>
					<li>Informujemy że po upływie 24h od zakończenia wynajmu nie będą mogły być uwzględnione prośby o zmianę parametrów usługi. Jeżeli Starter24 nie otrzyma RZW w terminie może nie być w stanie uwzględnić w rozliczeniu usługi dodatkowych opcji zgłaszanych przez wypożyczalnię (dodatkowy dzień wynajmu, podstawienie/odbiór, wydanie/odbiór poza godzinami pracy oddziału itp.). Będzie to skutkowało odmową zarejestrowania zmian w systemie Starter24 oraz odmową zapłaty faktury zawierającej dane niezgodne z danymi zarejestrowanymi w systemie Starter24.Podpis pod wiadomością</li></ul>'
	end
	
	SET @body = REPLACE(REPLACE(@body,'href="atlas.','href="http://atlas.'),'src="atlas.','src="http://atlas.') 

	IF @platformId = 5
		BEGIN
			SET @body = @content
		end

	if @preview = 1
	BEGIN
		SELECT @body
		RETURN
	END 

    EXEC [dbo].[p_attribute_edit]
   	@attributePath = '813', 
   	@groupProcessInstanceId = @groupProcessInstanceId,
   	@stepId = 'xxx',
   	@userId = 1,
   	@originalUserId = 1,
   	@valueText = @body,
   	@err = @err OUTPUT,
   	@message = @message OUTPUT,
   	@attributeValueId = @attributeValueId OUTPUT
	
   	SET @attachment = '{ATTRIBUTE::'+CAST(@attributeValueId AS NVARCHAR(20))+'::'+@fileName+'::true}'
   	
   	------------------------------------------------------
   	-- DLA Home Assistance, zawsze wysyłane jeszcze 2 pliki: kosztorys + zasady rozliczenia zlecenia
   	IF @stepId LIKE '1148.%'
   	BEGIN
	   	SET @attachment = @attachment + ',[ha_kosztorys],[ha_zasady_rozliczenia_zlecenia]'
   	END
   	-----------------------------------------------------
   	IF @stepId LIKE '1152.%'
   	BEGIN
	   	SET @attachment = @attachment + ',[zlecenie_przygotowania_raportu_medycznego]'
   	END
   	-----------------------------------------------------
   	
   	IF @test = 1 and @stepId not like '1148.%'
   	BEGIN 
		SET @email = 'andrzej@sigmeo.pl'   	
   	END ;
	
	if @test = 1 and @stepId like '1148.%'
	begin
		SET @email = 'maciej.grzelak@starter24.pl'  
	end;


	EXECUTE dbo.p_note_new 
	     @groupProcessId = @groupProcessInstanceId
	    ,@type = dbo.f_translate('email',default)
	    ,@content = @content
	    ,@email = @email
	    ,@userId = 1  -- automat
	    ,@subject = @subject
	    ,@direction=1
	    ,@dw = ''
	    ,@udw = ''
	    ,@sender = @starterEmail
	    ,@additionalAttachments = @attachment
	    ,@emailBody = @content
	    ,@emailRegards = 1
	    ,@err=@err OUTPUT
	    ,@message=@message OUTPUT
	

END