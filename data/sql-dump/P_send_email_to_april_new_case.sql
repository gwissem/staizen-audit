ALTER PROCEDURE [dbo].[P_send_email_to_april_new_case]
	@previousProcessId INT	-- Może być ofc GROUP / ROOT
AS
BEGIN
	
	/*		
	! only ALD
	
	2. Powiadomienie o nowej szkodzie April – 
		a. Warunek wysłania: otwarcie sprawy typu wypadek dla pojazdu którego likwidacja szkody przewidziana jest w April (nie ma go w bazie ALD_szkody) 
		b. Wysyłane do: ald_szkody@pl.april.com – po zapisaniu sprawy
 		____________________________________*/

	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @title NVARCHAR(MAX)
	DECLARE @partnerName NVARCHAR(300)
	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	
	/*	Poszukanie serwisu gdzie było by Holowanie
 	____________________________________*/
	
	CREATE TABLE #partnerTempTable (partnerId INT, partnerName NVARCHAR(300), priority INT, distance DECIMAL(18,2), phoneNumber NVARCHAR(100), reasonForRefusing NVARCHAR(100), partnerServiceId INT)
	
	EXEC [dbo].[p_find_partner]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@type=1,
		@resultType=1,
		@maxDistance = 1000,
		@limit = 1
	
	SELECT TOP 1 @partnerName = partnerName FROM #partnerTempTable
	DROP TABLE #partnerTempTable
	
	IF @partnerName IS NULL
	BEGIN
		SET @partnerName = '---'
	END
	
	SET @content = 'Szanowni Państwo,</br></br>

		Nr sprawy: {#caseid#}</br>
		Nr rejestracyjny: {@74,72@}</br>
		Marka i model pojazdu: {@74,73@}</br>
		Miejsce zdarzenia: {#displayLocalization(101,85)#}</br>
		Data zdarzenia: {#showDatetime({#processInfo(1011.006)#}|1)#}</br>
		Rodzaj zdarzenia: {@491@}</br>
		Diagnoza: {#diagnosisDescription(true)#} / {@417@}</br>
		Użytkownik: {@80,342,64@} {@80,342,66@}</br>
		Nr telefonu użytkownika: {@80,342,408,197@}</br>

		Dane serwisu: ' + @partnerName + '</br></br>
		 
		Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres cfm@starter24.pl,

	'

	EXEC [dbo].[P_parse_string]
	 @processInstanceId = @previousProcessId,
	 @simpleText = 'Powiadomienie do sprawy [{#caseid#} / {@74,72@}]',
	 @parsedText = @title OUTPUT

	EXEC [dbo].[P_get_body_email]
		@body = @body OUTPUT,
		@contentEmail  = @content,
		@title = @title,
		@previousProcessId = @previousProcessId	
		
	 SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'CFM')
	 SET @body = REPLACE(@body,'__EMAIL__', 'cfm@starter24.pl')
	 
	 DECLARE @email VARCHAR(400)
	 DECLARE @sender NVARCHAR(400)
	 DECLARE @contentNote NVARCHAR(1000) = [dbo].[f_showEmailInfo]('ald_szkody@pl.april.com') + dbo.f_translate('Powiadomienie dla APRIL o nowej szkodzie w obsłudze starter',default)
	 
	 SET @email = dbo.f_getRealEmailOrTest('ald_szkody@pl.april.com')
	 SET @sender = dbo.f_getEmail('cfm')
	 
	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @groupProcessInstanceId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = @contentNote
	 	,@email = @email
	 	,@userId = 1  -- automat
	 	,@subject = @title
	 	,@direction=1
	 	,@dw = ''
	 	,@udw = ''
	 	,@sender = @sender
	 	,@emailBody = @body
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT
	
END
