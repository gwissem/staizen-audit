ALTER PROCEDURE [dbo].[s_1009_048]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @userId INT
	DECLARE @originalUserId INT
	DECLARE @message NVARCHAR(1000)
	DECLARE @newProcessInstanceId INT
	DECLARE @groupProcessInstanceId INT
	DECLARE @processInstanceIds varchar(4000)
	DECLARE @country NVARCHAR(50)
	DECLARE @isAccident INT
	DECLARE @programId NVARCHAR(50)
	DECLARE @parkingNeeded INT
	DECLARE @rootId INT
	DECLARE @toWorkshop INT
	DECLARE @abroadCase INT
	DECLARE @acceptExtraCosts INT
	DECLARE @accept_AD_HOC_Costs INT
	DECLARE @typeEvent INT
	DECLARE @abroadTowing SMALLINT
	DECLARE @skipLimit int 
	DECLARE @homePatrolAbroad INT 
	
	SELECT @groupProcessInstanceId = group_process_id, 
	@rootId = root_id,
	@userId = created_by_id, 
	@originalUserId = created_by_original_id 
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	SELECT @abroadCase = dbo.f_abroad_case(@rootId)
	
	DECLARE @platformId INT
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '533', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @accept_AD_HOC_Costs =  value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1003', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @homePatrolAbroad =  value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '562', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @acceptExtraCosts =  value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @typeEvent = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '973', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @skipLimit = value_int FROM @values
	
	---------------------------------------------
	-- DLA Concordia / TUW TUZ / TUW Pocztowe (35,42) ustawienie 212 (Czy holować do ps?) na 0
	---------------------------------------------
	IF isnull(dbo.f_get_platform_key('towing_to_partner',@platformId, @programId),0) = 0
	BEGIN
		
		EXEC [dbo].[p_attribute_edit]
     	@attributePath = '212', 
     	@groupProcessInstanceId = @groupProcessInstanceId,
     	@stepId = 'xxx',
     	@userId = @currentUser,
     	@originalUserId = @currentUser,
     	@valueInt = 0,
     	@err = @err OUTPUT,
     	@message = @message OUTPUT
		
	END
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '212', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @toWorkshop = value_int FROM @values
	
	
	SET @variant = 1 
	
	IF ISNULL(@toWorkshop,0) = 0
	BEGIN
		SET @variant = 2
	END 
	ELSE
	BEGIN
		IF @abroadCase = 1 AND ISNULL(@homePatrolAbroad,0) = 0
		BEGIN
			SET @variant = 2	
		END 
		
		UPDATE dbo.attribute_value set value_string = null, value_int = null, value_decimal = null, value_text = null 
		where group_process_instance_id = @groupProcessInstanceId and attribute_path like '211,85,%'
	END 
	
	
	-------------------------
	-- Sprawdzenie, czy jest:
	--		- wypadek/szkoda 
	--		- miejsce docelowe holowania w kraju
	-- Wtedy nie przepuszczac przez krok i wyświetlić komunikat
	-------------------------

	-- resetowanie wartości
  	EXEC [dbo].[p_attribute_edit]
	     @attributePath = '924', 
	     @groupProcessInstanceId = @groupProcessInstanceId,
	     @stepId = 'xxx',
	     @userId = @currentUser,
	     @originalUserId = @currentUser,
	     @valueText = NULL,
	     @err = @err OUTPUT,
	     @message = @message OUTPUT
	     
	SELECT @abroadTowing = dbo.f_abroad_towing(@groupProcessInstanceId)
	
	-- za granicą
	-- &&
	-- (
	-- TUZ Comfort od 4.6.2018  i wypadek/szkoda
	-- OR
	-- TUZ VIP od 4.6.2018
	-- )
	IF @abroadCase = 0 AND @abroadTowing = 1 AND @typeEvent IN (1,3) AND @programId IN ('481','482') 
	BEGIN
		
		DECLARE @errorMessage NVARCHAR(400)
		DECLARE @nameProgram NVARCHAR(200)
		
		SELECT @nameProgram = p.name FROM AtlasDB_def.dbo.vin_program p WHERE p.id = CAST(@programId AS INT)
	
		SET @errorMessage = 'Holowanie w ramach programu <b>'+@nameProgram+'</b> jest dostępne tylko na terenie kraju.'  
			
	  	EXEC [dbo].[p_attribute_edit]
		     @attributePath = '924', 
		     @groupProcessInstanceId = @groupProcessInstanceId,
		     @stepId = 'xxx',
		     @userId = @currentUser,
		     @originalUserId = @currentUser,
		     @valueText = @errorMessage,
		     @err = @err OUTPUT,
		     @message = @message OUTPUT
	     
		SET @variant = 99
		RETURN
	END
	
	--------------------------
	--------------------------
	
	EXEC dbo.p_validate_towing_destination @groupProcessInstanceId = @groupProcessInstanceId, @message = @message OUTPUT, @err = @err OUTPUT
	
	IF @err = 1 and isnull(@skipLimit,0) <> 1 AND isnull(@acceptExtraCosts,0) <> 1 and @programId <> '423'
	BEGIN		
	    SET @variant = 99			    		
	END 
	
	-- FORD / TUW Pocztowe / TUW TUZ
	-- Jeżeli AD HOC, to olewamy komunikaty o płatności, bo i tak już koszta zostały zaakceptowane przez klienta
--	IF @platformId IN(2,18,35,42) AND ISNULL(@accept_AD_HOC_Costs, 0) <> 1
--	BEGIN
--		DECLARE @distance DECIMAL(10,2)		
--		EXEC dbo.p_validate_towing_destination @groupProcessInstanceId = @groupProcessInstanceId, @message = @message OUTPUT, @err = @err OUTPUT
--		
--		IF @err = 1
--		BEGIN			
--		 
--		     IF @abroadCase = 0 OR (@abroadCase = 1 AND @acceptExtraCosts IS NULL AND ISNULL(@toWorkshop,0) = 0) 
--		     BEGIN
--			    SET @variant = 99			    
--		     END
--		     
--		END 
--	
--	END 
	
	IF ISNULL(@acceptExtraCosts,-1) = 0 AND ISNULL(@skipLimit,-1) <> 1
    BEGIN
	     SET @variant = 97
	     
	     EXEC [dbo].[p_attribute_edit]
          @attributePath = '554', 
          @groupProcessInstanceId = @groupProcessInstanceId,
          @stepId = 'xxx',
          @userId = 1,
          @originalUserId = 1,
          @valueText = dbo.f_translate('Brak zgody na pokrycie dodatkowych kosztów oznacza rezygnację z usług.',default),
          @err = @err OUTPUT,
          @message = @message OUTPUT
	     
	     RETURN
    END 
    
	---------------------------------------------
	---------------------------------------------
	
	DECLARE @locationType INT
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,257', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @locationType = value_int FROM @values



	IF @variant <> 99 AND isnull(@toWorkshop,0) = 0 AND @locationType =2 AND @abroadCase = 0
	BEGIN
	
		DECLARE @workshopName NVARCHAR(100)
		DECLARE @workshopEmail NVARCHAR(100)
		DECLARE @workshopPhone NVARCHAR(20)
		DECLARE @workshopCountry NVARCHAR(255)
		DECLARE @workshopPostalCode NVARCHAR(255)
		DECLARE @workshopCity NVARCHAR(255)
		DECLARE @workshopHouseNr NVARCHAR(255)		
		DECLARE @workshopStreet NVARCHAR(255)
		DECLARE @partnerId INT
		DECLARE @partnerRootId INT
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '908,84', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopName = value_string FROM @values

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '908,368', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopEmail = value_string FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '908,197', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopPhone = value_string FROM @values
	
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,86', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopCountry = value_string FROM @values
	
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,87', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopCity = value_string FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,89', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopPostalCode = value_string FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,94', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopStreet = value_string FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,95', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @workshopHouseNr = value_string FROM @values
		
		EXEC [dbo].[p_create_ADHOC_partner]
		   @partner_long_name = @workshopName
		  ,@partner_short_name = @workshopName
		  ,@country_c = @workshopCountry
		  ,@postal_code = @workshopPostalCode 
		  ,@city_c = @workshopCity
		  ,@street_s = @workshopStreet
		  ,@house_nr = @workshopHouseNr
		  ,@company_owner_name = '' 
		  ,@company_owner_surname = '' 
		  ,@owner_phone_number = @workshopPhone
		  ,@owner_email = @workshopEmail 
		  ,@nip = ''
		  ,@process_instance_id_from = @previousProcessId
		  ,@id = @partnerRootId output



		SELECT @partnerId = partner.id 
		FROM dbo.attribute_value partner
		where partner.attribute_path = '595,597' 
		and partner.parent_attribute_value_id = @partnerRootId

		EXEC [dbo].[p_attribute_edit]
     	@attributePath = '522', 
     	@groupProcessInstanceId = @groupProcessInstanceId,
     	@stepId = 'xxx',
     	@userId = 1,
     	@originalUserId = 1,
     	@valueInt = @partnerId,
     	@err = @err OUTPUT,
     	@message = @message OUTPUT
	     
	END 
	else if isnull(@toWorkshop,0) = 0 and not (@locationType = 1 and  isnull(@platformId,0) in (35,42))
	BEGIN
		
		EXEC [dbo].[p_attribute_edit]
     	@attributePath = '522', 
     	@groupProcessInstanceId = @groupProcessInstanceId,
     	@stepId = 'xxx',
     	@userId = 1,
     	@originalUserId = 1,
     	@valueInt = null,
     	@err = @err OUTPUT,
     	@message = @message OUTPUT
     	
	END 
	
	
END