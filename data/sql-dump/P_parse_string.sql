ALTER PROCEDURE [dbo].[P_parse_string]
@processInstanceId INT,
@simpleText NVARCHAR(MAX),
@parsedText NVARCHAR(MAX) = '' OUTPUT
AS
begin
	
	/*	Prosta procedura, która wykorzystuje nasz parser w Atlasie.
	 	Wykonywany jest Request HTTP (POST), do Atlasa
 	____________________________________*/
	
	DECLARE @ret INT;
	DECLARE @status NVARCHAR(32);
	DECLARE @statusText NVARCHAR(32);
	DECLARE @token INT;
	DECLARE @url NVARCHAR(256);
	DECLARE @postData NVARCHAR(MAX);
	DECLARE @contentType NVARCHAR(64);
	DECLARE @Text as table ( answer NVARCHAR(MAX) )
	
	/*	Na początek w razie błędu INTPUT => OUTPUT
 	____________________________________*/
	
	SET @parsedText = @simpleText
	
	SET @contentType = dbo.f_translate('application/x-www-form-urlencoded',default);
	SET @url = dbo.f_getDomain() + dbo.f_translate('/database-api/parse-string',default)
	SET @postData = 'token=21AD4FFD-B18D-4123-841C-759954643953&string=' + @simpleText + '&processInstanceId=' + CAST(@processInstanceId AS NVARCHAR(100)) 
	
	PRINT @postData
	PRINT @url
	
	-- Open the connection.
	EXEC @ret = sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @token OUT;
	IF @ret <> 0 RAISERROR('Unable to open HTTP connection.', 10, 1);
		
	-- Send the request.
	EXEC @ret = sp_OAMethod @token, dbo.f_translate('open',default), NULL, dbo.f_translate('POST',default), @url, false;
	EXEC @ret = sp_OAMethod @token, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-type',default), @contentType;
	EXEC @ret = sp_OAMethod @token, dbo.f_translate('send',default), NULL, @postData;

	-- Handle the response.
	EXEC @ret = sp_OAGetProperty @token, dbo.f_translate('status',default), @status OUT;
	EXEC @ret = sp_OAGetProperty @token, dbo.f_translate('statusText',default), @statusText OUT;
	
	-- Show the response.
	PRINT dbo.f_translate('Status: ',default) + @status + ' (' + @statusText + ')';

	IF @status <> '200'
	BEGIN
		
		SET @parsedText = ''
		
	END
	ELSE
	BEGIN
		
		INSERT INTO @Text
		EXEC sp_OAGetProperty @token, dbo.f_translate('responseText',default) 
		SELECT @parsedText = answer FROM @Text
		
	END
	
	-- Close the connection.
	EXEC @ret = sp_OADestroy @token;
	IF @ret <> 0 RAISERROR('Unable to close HTTP connection.', 10, 1);
	
END

