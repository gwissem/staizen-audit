ALTER PROCEDURE [dbo].[s_1148_012]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @previousStepId NVARCHAR(255)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @replacementCarStartLocationId INT
	DECLARE @rootId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	declare @step_id nvarchar(50)
	declare @groupProcessInstanceId int
	declare @partnerId int
	declare @partnerEmail nvarchar(100)
	declare @platformId int
	declare @platform nvarchar(100)
	DECLARE @idProcessOfMonitorHa INT
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance
	where id = @previousProcessId
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values

	select @platform=name 
	from dbo.platform 
	where id=@platformId

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	select @partnerEmail=dbo.f_partner_contact(@groupProcessInstanceId,@partnerId,19,16)
	if isnull(@partnerEmail,'')=''
	begin
		select @partnerEmail=dbo.f_partner_contact(@groupProcessInstanceId,@partnerId,19,4)
	end
	
	declare @subject nvarchar(100)
	declare @body nvarchar(4000)
	DECLARE @caseId nvarchar(50)
	
	set @caseId=dbo.f_caseId(@rootId)
	
	set @subject=dbo.f_translate('Formularz raportu ',default)+isnull(@platform,'')+dbo.f_translate(' do sprawy ',default)+isnull(@caseId,'')
		
	set @body = 'Szanowni Państwo, <br>
		w załączniku przesyłamy kopię uzupełnionego raportu zamknięcia sprawy. <br> '
			
	
	declare @RSV varchar(200)
	declare @sender nvarchar(200)
			
	set @sender = dbo.f_getEmail('ha')
	set @RSV='{FORM::'+cast(@previousProcessId as varchar(100))+'::raport zamknięcia}'
	
	EXECUTE dbo.p_note_new 
		@sender=@sender
		,@groupProcessId=@groupProcessInstanceId
		,@type=dbo.f_translate('email',default)
		,@content=dbo.f_translate('Formularz raportu zamknięcia',default)
		,@phoneNumber=null
		,@email=@partnerEmail
		,@userId=@currentUser
		,@attachment=null
		,@subject=@subject
		,@direction=1
		,@addInfo=null
		,@emailBody=@body
		,@err=@err OUTPUT
		,@message=@message OUTPUT
		,@additionalAttachments=@RSV
		,@emailRegards=1
			
	SELECT @idProcessOfMonitorHa = id FROM dbo.process_instance WHERE step_id = '1148.011' AND active = 1 AND group_process_id = @groupProcessInstanceId

	PRINT '@idProcessOfMonitorHa'
	PRINT @idProcessOfMonitorHa
	
	-- Jeżeli istnieje monitoring HA i nie został przeklikany (to klikamy automatycznie)
	IF @idProcessOfMonitorHa IS NOT NULL
	BEGIN
		
		DECLARE @processInstanceIds VARCHAR(4000)
		
		-- (czy kontraktor wykonał usługę?)
		EXEC p_attribute_edit
  			@attributePath = '965',
  			@groupProcessInstanceId = @groupProcessInstanceId,
  			@stepId = 'xxx',
  			@valueInt = 1, -- tak
  			@err = @err OUTPUT,
  			@message = @message OUTPUT
		
  			-- Next monitoringu
  		EXEC [dbo].[p_process_next]
			@previousProcessId = @idProcessOfMonitorHa,
			@userId = @currentUser,
			@originalUserId = @currentUser,
			@err = @err OUTPUT,
			@message = @message OUTPUT,
			@processInstanceIds = @processInstanceIds OUTPUT
  			
	END
		
	set @variant=1
	 
END

