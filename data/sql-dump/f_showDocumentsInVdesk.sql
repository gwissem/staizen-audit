ALTER PROCEDURE [dbo].[f_showDocumentsInVdesk]
( 
	@caseNumber NVARCHAR(20),
	@simpleResult SMALLINT = 0
) 
AS
BEGIN
	
	SET NOCOUNT ON;

	/*	Legenda:
	 
	  DocumentType:
	  	3	Faktura
		4	Raport
		6	Zalacznik

	  IndexTypeId: 
	  	25 - Numer sprawy faktury
	  	33 - Załacznik do faktury (Raport)
	  	251 - Numer sprawy załącznika
 		____________________________________*/
	
	DECLARE @documents TABLE(id INT, documentIndexType INT)
	
	/*	Pobieranie załączników i faktur
 	____________________________________*/
	INSERT INTO @documents
	SELECT document.id, dIndex.IndexTypeId
	FROM
	[SETTE].[vdesk].[dbo].[Document] as document WITH(NOLOCK)
	INNER JOIN [SETTE].[vdesk].[dbo].DocumentIndex as dIndex WITH(NOLOCK) ON dIndex.DocumentId = document.id AND dIndex.StringValue = @caseNumber AND dIndex.IndexTypeId IN (25,251)
	
	/*	Pobieranie załączników do faktur - Raportów
 	____________________________________*/
	INSERT INTO @documents
	SELECT dIndex.NumberValue, dIndex.IndexTypeId
	FROM [SETTE].[vdesk].[dbo].DocumentIndex as dIndex WITH(NOLOCK)
	INNER JOIN @documents as attachInvoice ON attachInvoice.id = dIndex.DocumentId AND attachInvoice.documentIndexType = 25
	WHERE dIndex.IndexTypeId = 33
	
	IF @simpleResult = 1
	BEGIN
		
		SELECT 
		d.id as documentId,
		d.OrigFileName as dbo.f_translate('name',default)
		FROM @documents as doc
		INNER JOIN [SETTE].[vdesk].[dbo].[Document] as d WITH(NOLOCK) ON d.id = doc.id
		LEFT JOIN [SETTE].[vdesk].[dbo].DocumentType as dType WITH(NOLOCK) ON d.DocumentTypeId = dType.id
		
	END
	ELSE
	BEGIN
		
		SELECT 
		d.id as documentId,
		d.InputDate as dbo.f_translate('Data dodania',default),
		d.OrigFileName as dbo.f_translate('Nazwa dokumentu',default),
		dType.Name as dbo.f_translate('Typ dokumentu',default),
		dbo.f_getUrlDocumentFromVDesk(d.id, 1) as dbo.f_translate('Pobierz',default)
		FROM @documents as doc
		INNER JOIN [SETTE].[vdesk].[dbo].[Document] as d WITH(NOLOCK) ON d.id = doc.id
		LEFT JOIN [SETTE].[vdesk].[dbo].DocumentType as dType WITH(NOLOCK) ON d.DocumentTypeId = dType.id
		
	END
	
END