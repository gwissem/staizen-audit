
ALTER PROCEDURE [dbo].[s_1011_052]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @platformId INT
	DECLARE @makeModel INT
	DECLARE @rootId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	SELECT	@groupProcessInstanceId = p.group_process_id, 
			@rootId = p.root_id 
	FROM	process_instance p with(nolock) 
	WHERE p.id = @previousProcessId 
	

	declare @platformMakeModel int
	declare @change int

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '878', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @change = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '876', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformMakeModel = value_int FROM @values
	

	set @variant=1

	if @change=1
	begin
		declare @platformOriginalName nvarchar(200)
		
		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '253',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @platformId = value_int FROM @values

		SELECT	@platformOriginalName = name
		FROM	platform with(nolock)
		where	id = @platformId

		EXEC [dbo].[p_attribute_edit]
			@attributePath = '253',
			@stepId = 'xxx',
			@groupProcessInstanceId = @groupProcessInstanceId,
			@valueInt = @platformMakeModel,
			@err = @err,
			@message = @message
			
		declare @requestorPhoneNumber varchar(200)
		declare @platformName nvarchar(200)
		declare @platformPhone nvarchar(20)

		DELETE FROM @values
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '80,342,408,197',
				@groupProcessInstanceId = @groupProcessInstanceId
		SELECT @requestorPhoneNumber = value_string FROM @values
	
		SELECT	@platformName = name, 
				@platformPhone = official_line_number
		FROM	platform with(nolock)
		where	id = @platformMakeModel
	
		declare @content nvarchar(500)
		SET @content = dbo.f_translate('Szanowni Państwo numer telefonu do ',default)+@platformName+dbo.f_translate(' Assistance to ',default)+@platformPhone+dbo.f_translate('. Z poważaniem ',default)+@platformOriginalName+dbo.f_translate(' Assistance',default)
	
		EXEC p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @requestorPhoneNumber,
		@userId = 1,
		@originalUserId = 1,
		@addInfo = 0,
		@err = @err OUTPUT,
		@message = @message OUTPUT	
	end

END


