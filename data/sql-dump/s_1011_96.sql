ALTER PROCEDURE [dbo].[s_1011_96]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	
	DECLARE @userId INT
	DECLARE @originalUserId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @previousStepId NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @newProcessInstanceId INT
	DECLARE @nextProcessInstances varchar(4000)
	DECLARE @acceptedExtraCosts INT
	DECLARE @previousId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @processInstanceIds NVARCHAR(255)
	DECLARE @rootId INT
	DECLARE @platformGroup nvarchar(255)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	SELECT @groupProcessInstanceId = group_process_id,
	@stepId = step_id,
	@rootId = root_id,
	@previousId = previous_process_instance_id,
	@userId = created_by_id, 
	@originalUserId = created_by_original_id 
	FROM process_instance with(nolock)
	WHERE id = @previousProcessId 
	
	exec [dbo].[p_platform_group_name]
	@groupProcessInstanceId = @groupProcessInstanceId,
	@name = @platformGroup OUTPUT
	
	SELECT @previousStepId = step_id
	FROM process_instance with(nolock)
	WHERE id = @previousId 
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '533', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @acceptedExtraCosts = value_int FROM @values
	
	IF @acceptedExtraCosts IS NULL
	BEGIN
		SET @variant = 99 
		RETURN
	END 
	
	PRINT '---accepted'
	PRINT @acceptedExtraCosts
	
	IF @acceptedExtraCosts = 1
	BEGIN
		
		IF @platformGroup = dbo.f_translate('CFM',default)
		BEGIN
			declare @body nvarchar(4000)
			declare @email nvarchar(200)
			declare @senderEmail nvarchar(200) = [dbo].[f_getEmail]( dbo.f_translate('cfm',default) )
			declare @subject nvarchar(255)
			declare @regNumber nvarchar(255)
			
			exec [dbo].[p_cfm_partner_contact]
			@groupProcessInstanceId = @groupProcessInstanceId,
			@contact = @email output
			
			delete from @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @regNumber = value_string FROM @values
			
			set @subject = @regNumber + ' / ' + dbo.f_caseId(@groupProcessInstanceId)
			set @body = 'Szanowni Państwo,<br/>zgłosił się do nas użytkownik pojazdu: '+@regNumber+dbo.f_translate('z prośbą o zorganizowanie pomocy ',default)+isnull('w przypadku "'+dbo.f_diagnosis_description(@groupProcessInstanceId,'pl')+'"','')+dbo.f_translate('. Został poinformowany o tym, że jest to zdarzenie nieobsługiwane w ramach pakietu. Mimo wszystko zadeklarował, że firma pokryje koszty interwencji i poprosił o zorganizowanie pomocy.',default)	
			
			EXECUTE dbo.p_note_new 
		     @groupProcessId = @groupProcessInstanceId
		    ,@type = dbo.f_translate('email',default)
		    ,@content = @body
		    ,@email = @email
		    ,@userId = 1  -- automat
		    ,@subject = @subject
		    ,@direction=1
		    ,@dw = ''
		    ,@udw = ''
		    ,@sender = @senderEmail
	--	    ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
		    ,@emailBody = @body
		    ,@emailRegards = 1
		    ,@err=@err OUTPUT
		    ,@message=@message OUTPUT	
		END 
		ELSE
		BEGIN
			
		 
			EXEC p_attribute_edit
			@attributePath = '202', 
			@groupProcessInstanceId = @rootId,
			@stepId = 'xxx',
			@valueString = '423',
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
			EXEC p_attribute_edit
			@attributePath = '204', 
			@groupProcessInstanceId = @rootId,
			@stepId = 'xxx',
			@valueString = '423',
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
			EXEC p_attribute_edit
			@attributePath = '207', 
			@groupProcessInstanceId = @rootId,
			@stepId = 'xxx',
			@valueInt = null,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
		END 
		begin tran
		
		PRINT '---programChanged'		
		PRINT @err
		PRINT @message
				
			IF (@previousStepId = '1011.016'  or @previousStepId = '1011.017' or @previousStepId ='1144.017' or @previousStepId ='1145.003' or @previousStepId = '1145.005' or @previousStepId ='1145.002' or @previousStepId = '1145.007' or @previousStepId = '1149.003' or @previousStepId = '1149.008' or @previousStepId = '1149.002' or @previousStepId = '1147.012' or @previousStepId = '1147.010' or  @previousStepId = '1203.002' or @previousStepId = '1201.002')  -- MG20180926 dodanie warunku or @previousStepId ='1144.017'
			BEGIN
				IF @previousStepId = '1011.017' -- kluczyki
					BEGIN
						SET @previousStepId = '1011.004'
				end
				ELSE
				BEGIN
					SET @previousStepId = '1011.009'
				END


				EXEC [dbo].[p_process_new] 
				@stepId = @previousStepId, 
				@userId = @userId, 
				@originalUserId = @originalUserId, 
				@skipTransaction = 1,
				@rootId = @rootId,
				@groupProcessId = @rootId,
				@err = @err OUTPUT, 
				@message = @message OUTPUT, 
				@processInstanceId = @newProcessInstanceId OUTPUT
				
			END 
			ELSE
			BEGIN
				
				UPDATE process_instance set active = 1 where id = @previousId
				
				EXEC p_process_next
				@previousProcessId = @previousId,
				@err = @err OUTPUT,
				@message = @message OUTPUT,
				@processInstanceIds = @processInstanceIds OUTPUT
				
				SET @newProcessInstanceId = CAST(@processInstanceIds AS INT)

			END
			
			INSERT INTO process_instance_flow (previous_process_instance_id, next_process_instance_id, active)
			VALUES (@previousProcessId, @newProcessInstanceId, 1)
	
			UPDATE process_instance SET active = 0 where id = @previousProcessId
		commit tran
	END
	ELSE
	BEGIN
		SET @variant = 97
		
		IF @platformGroup = dbo.f_translate('CFM',default)
		BEGIN
			EXEC [dbo].[p_attribute_edit]
		     @attributePath = '554', 
		     @groupProcessInstanceId = @groupProcessInstanceId,
		     @stepId = 'xxx',
		     @userId = 1,
		     @originalUserId = 1,
		     @valueText = dbo.f_translate('Prosimy o kontakt z osobą odpowiedzialną za flotę w firmie. W razie pytań prosimy o kontakt zwrotny.',default),
		     @err = @err OUTPUT,
		     @message = @message OUTPUT
		END 
	END 
		
END