

ALTER PROCEDURE [ics].[P_import]
AS
SET NOCOUNT ON
SET TEXTSIZE 2147483647;

declare @id	int
declare @number varchar(50)	
declare @external_contractor_id varchar(50)	
declare @name nvarchar(200)
declare @registration_number nvarchar(50)	
declare @registration_number2 nvarchar(50)
declare @responsible_user_phone	nvarchar(20)
declare @brand nvarchar(50)	
declare @model nvarchar(50)	
declare @VIN nvarchar(50)	
declare @brand2 nvarchar(50)	
declare @model2 nvarchar(50)	
declare @VIN2 nvarchar(50)	
declare @DMC int	
declare @DMC2 int	
declare @mileage int	
declare @service  nvarchar(5)
declare @drive nvarchar(50)
declare @scheduled int
declare @created_at datetime	
declare @startDate datetime	
declare @onSite	datetime
declare @finishOnSite datetime
declare @finishTowing datetime	
declare @XStart	varchar(20)
declare @YStart	varchar(20)
declare @XOnSite varchar(20)
declare @YOnSite varchar(20)
declare @XFinish varchar(20)
declare @YFinish varchar(20)
declare @X1 varchar(20)
declare @Y1 varchar(20)
declare @custom_arrival decimal(8,2)
declare @custom_return decimal(8,2)
declare @custom_towing decimal(8,2)
declare @man_hour_time decimal(8,0)
declare @man_hour_time_reason nvarchar(1000)	
declare @lift_man_hour_time decimal(8,0)
declare @lift_man_hour_time_reason nvarchar(1000)	
declare @transported_people_number int
declare @viatoll_cost decimal(8,2)
declare @paid_for_viatoll tinyint
declare @highway_cost decimal(8,2)
declare @are_people_transported_separately tinyint	
declare @is_photo_documented tinyint
declare @code nvarchar(100)
declare @code_desc nvarchar(500)
declare @UslugaId int
declare @platforma nvarchar(200)
declare @program nvarchar(200)
declare @dataPrzekazania datetime
declare @comment nvarchar(500)
declare @updatedAt datetime
declare @statusTeka int
declare @eta datetime
declare @image_file_name nvarchar(1000)
declare @service_vehicle_data nvarchar(100)
declare @registration_number_of_separated_car nvarchar(200)
declare @distance_of_transported_people_in_separate_car varchar(100)
declare @towing_destination nvarchar(500)



declare kur cursor LOCAL for


		select  a.* from openquery(teka2,'SELECT o.id,
				o.number,
				c.external_contractor_id,
				c.name,
				o.registration_number,
				r.registration_number registration_number2,
				ifnull(o.assigned_user_phone,o.responsible_user_phone) responsible_user_phone,
				o.brand,
				o.model,
				o.VIN,
				r.brand brand2,
				r.model model2,
				r.VIN VIN2,
				o.DMC,
				r.DMC DMC2,
				r.mileage mileage,
				case when exists (select id from StarterTeka.order_statuses where created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and order_id=o.id and status_id=6 limit 1) then ''H'' else ''N'' end service,
				case when exists (select id from StarterTeka.order_statuses where created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and order_id=o.id and status_id=6 limit 1) then 'dbo.f_translate('Niemożliwa',default)' else '''' end drive,	
				od.scheduled,
				o.created_at,
				(select submitted_at from StarterTeka.order_statuses where created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and order_id=o.id and status_id=4 limit 1) startDate,		
				(select submitted_at from StarterTeka.order_statuses where created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and order_id=o.id and status_id=5 limit 1) onSite,		
				(select submitted_at from StarterTeka.order_statuses where created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and order_id=o.id and status_id>5 order by submitted_at limit 1) finishOnSite,		
				(select submitted_at from StarterTeka.order_statuses where created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and order_id=o.id and status_id>6 order by submitted_at limit 1) finishTowing,		
				(select longitude from StarterTeka.routes r inner join StarterTeka.points p on r.id=p.route_id where r.created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and r.order_id=o.id order by p.id limit 1) XStart,
				(select latitude from StarterTeka.routes r inner join StarterTeka.points p on r.id=p.route_id where r.created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and  r.order_id=o.id order by p.id limit 1) YStart,
				(select longitude from StarterTeka.routes r inner join StarterTeka.points p on r.id=p.route_id where r.created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and  r.order_id=o.id and p.status_id=5 order by p.id limit 1) XOnSite,
				(select latitude from StarterTeka.routes r inner join StarterTeka.points p on r.id=p.route_id where r.created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and  r.order_id=o.id and p.status_id=5 order by p.id limit 1) YOnSite,
				(select longitude from StarterTeka.routes r inner join StarterTeka.points p on r.id=p.route_id where r.created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and  r.order_id=o.id and p.status_id>=5 and latitude is not null order by p.id desc limit 1) XFinish,
				(select latitude from StarterTeka.routes r inner join StarterTeka.points p on r.id=p.route_id where r.created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and  r.order_id=o.id and p.status_id>=5 and longitude is not null order by p.id desc limit 1) YFinish,
				o.longitude X1,
				o.latitude Y1,
				odi.custom_arrival,
				odi.custom_return,
				odi.custom_towing,
				od.man_hour_time,
				od.man_hour_time_reason,
				od.lift_man_hour_time,
				od.lift_man_hour_time_reason,
				od.transported_people_number,
				od.viatoll_cost,
				od.paid_for_viatoll,
				od.highway_cost,
				od.are_people_transported_separately,
				od.is_photo_documented,
				tc.code,
				tc.code_desc,
				s.UslugaId,
				s.platforma,
				s.program,
				s.dataPrzekazania,
				r.comment,
				(select max(submitted_at) from StarterTeka.order_statuses where order_id=o.id limit 1) updatedAt,
				(select max(status_id) from StarterTeka.order_statuses where order_id=o.id limit 1) status,
				o.eta,
				p.image_file_name,
				sam.rodzajPojazdu,
				o.registration_number_of_separated_car,
				odi.distance_of_transported_people_in_separate_car,
				o.towing_destination
		FROM StarterTeka.orders o left join 
			 StarterTeka.teka_codes tc on o.id=tc.order_id inner join
			 StarterTeka.users u on u.phone_number=o.responsible_user_phone  inner join
			 StarterTeka.companies c on c.id=u.company_id left join
			 (select * from StarterTeka.order_details group by order_id) od on od.order_id=o.id left join
			 StarterTeka.order_distances odi on odi.order_id=o.id and odi.created_at>DATE_ADD(NOW(), INTERVAL -5 DAY)  left join
			 (select * from StarterTeka.reports where created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) group by order_id) r on r.order_id=o.id inner join
			 teka2.Sprawy s on s.nrSprawy=o.number COLLATE utf8_unicode_ci  left join
		 	 StarterTeka.photos p on p.report_id=r.id and category=''filled_report''    left join
			 teka2.samochody sam on sam.QRCode=o.service_vehicle_data COLLATE utf8_unicode_ci 
		where o.created_at>DATE_ADD(NOW(), INTERVAL -5 DAY) and
				(
				(select max(submitted_at) from StarterTeka.order_statuses where order_id=o.id)>DATE_ADD(NOW(), INTERVAL -5 hour) or
				(select max(created_at) from StarterTeka.routes r inner join StarterTeka.points p on r.id=p.route_id where r.created_at>DATE_ADD(NOW(), INTERVAL -5 hour) and r.order_id=o.id) is not null
				) 
		group by o.number 
		order by o.id desc') a 
		--where a.number='U513864'
		--U481597
		-- and o.number not like ''A%''
		left join
		ics.headers h on h.serviceId=a.UslugaId 
		where (h.serviceId is null or h.statusTeka<30) --and a.updatedAt>dateadd(hour,-3,getdate())
	OPEN kur;
	FETCH NEXT FROM kur INTO @id, @number, @external_contractor_id, @name, @registration_number, @registration_number2, @responsible_user_phone, @brand, @model, @VIN, @brand2, @model2, @VIN2, @DMC, @DMC2, @mileage, @service, @drive, @scheduled, @created_at, @startDate, @onSite, @finishOnSite, @finishTowing, @XStart, @YStart, @XOnSite, @YOnSite, @XFinish, @YFinish, @X1, @Y1, @custom_arrival, @custom_return, @custom_towing, @man_hour_time, @man_hour_time_reason, @lift_man_hour_time, @lift_man_hour_time_reason, @transported_people_number, @viatoll_cost, @paid_for_viatoll, @highway_cost, @are_people_transported_separately, @is_photo_documented, @code, @code_desc, @UslugaId, @platforma, @program, @dataPrzekazania,@comment,@updatedAt,@statusTeka,@eta,@image_file_name,@service_vehicle_data,@registration_number_of_separated_car,@distance_of_transported_people_in_separate_car,@towing_destination;
	WHILE @@FETCH_STATUS=0
	BEGIN
	print '33333'
		declare @number2 varchar(50)

		if @XOnSite is null set @XOnSite=@X1
		if @YOnSite is null set @YOnSite=@Y1

		if @number like 'A%U%'
		begin
			set @number2=substring(@number,12,7)
		end
		else
		begin
			set @number2=@number
		end

		-- o.created_at>DATE_ADD(NOW(), INTERVAL -2 DAY)  or
		declare @SQlCheck nvarchar(500) 
		set @SQlCheck='SELECT blad, powod, APKversion, dojazd, blad2 FROM OPENQUERY(Teka2,''CALL StarterTeka.veryfication2('''''+@number+''''');'')a'
		print @SQlCheck
	--	set @SQlCheck=dbo.f_translate('SELECT 0 blad, ',default)''dbo.f_translate(' powod , ',default)''dbo.f_translate(' APKversion  ,0 dojazd',default)

		declare @checkTable as table (error int, powod varchar(500), APKversion varchar(20), dojazd decimal(18,4), blad2 int)

		delete from @checkTable
		
		insert into @checkTable
		exec (@SQLcheck)

		declare @error int 
		declare @powod varchar(500)
		declare @APKversion varchar(20) 
		declare @error2 int 

		select	@error=error,
				@powod=powod,
				@APKversion=APKversion,
				@custom_arrival=dojazd,
				@error2=blad2
		from	@checkTable
		
		set @powod=isnull(@powod,'')+' ('+isnull(@APKversion,'')+')'

	--	select @number,@SQlCheck,* from @checkTable

		--if @error=0 --and @APKversion>'1.6.0'
		--begin

			begin tran

			set @XStart=left(@XStart,9)
			set @YStart=left(@YStart,9)
			set @XOnSite=left(@XOnSite,9)
			set @YOnSite=left(@YOnSite,9)

			if @error<>@error2
			begin
				set @XOnSite=left(@X1,9)
				set @YOnSite=left(@Y1,9)
				
			end

			set @XFinish=left(@XFinish,9)
			set @YFinish=left(@YFinish,9)
			set @X1=left(@X1,9)
			set @Y1=left(@Y1,9)

			declare @idH int
			declare @status int

			set @idH=null

			select  @idH=id,
					@status=status
			from	ics.headers 
			where	id=@id


			declare @statusH int
			if (@error=0 or @statusTeka<5) and @statusTeka<>10
			begin
				set @statusH=1
			end
			else
			begin
				set @statusH=0
			end

			--if @number='U488203' set @statusH=1
		

				if @idH is not null
				begin
					UPDATE ics.headers
					   SET contractor = @external_contractor_id
						  ,contractorName = @name
						  ,registrationNumber = @registration_number
						  ,phoneNumber = @responsible_user_phone
						  ,serviceId = @UslugaId
						  ,platform = @platforma
						  ,updatedAt=@updatedAt
						  ,statusTeka=@statusTeka
						  ,status=@statusH
						  ,description=@powod
					 WHERE caseNumber=@number
				end
				else
				begin
					INSERT INTO ics.headers
					   (id
					   ,caseNumber
					   ,contractor
					   ,contractorName
					   ,registrationNumber
					   ,phoneNumber
					   ,serviceId
					   ,platform
					   ,updatedAt
					   ,statusTeka
					   ,status
					   ,description)
					 VALUES
						   (@id
						   ,@Number
						   ,@external_contractor_id
						   ,@name
						   ,@registration_number
						   ,@responsible_user_phone
						   ,@UslugaId
						   ,@platforma
						   ,@updatedAt
						   ,@statusTeka
						   ,@statusH
						   ,@powod)


				end
				
				DECLARE	@addressTowing nvarchar(500)
				DECLARE	@addressStart nvarchar(500)
				DECLARE	@address nvarchar(500)
				DECLARE	@addressOnSite nvarchar(500)	
				set @addressOnSite=null
				set @addressTowing=null
				set @addressStart=null
				set @address=null


				if @statusH=1
				begin
					print 'Status=1'
					if @statusTeka>6
					begin
						select @addressTowing=value2
						from	ics.elementsV e
						where	headerId=@idH and categoryId=49
					end

					select @addressStart=value2
					from	ics.elementsV e
					where	headerId=@idH and categoryId=48

					select @address=value1
					from	ics.elementsV e
					where	headerId=@idH and categoryId=47

					if @statusTeka>4
					begin
						select @addressOnSite=value2
						from	ics.elementsV e
						where	headerId=@idH and categoryId=47
					end 

					delete from ics.elementsV where headerId=@idH

					set @idH=@id
					--1Platforma
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,1,@platforma,@platforma,@platforma,null)

					--2Program
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,2,@program,@program,@program,null)

					--55Status ICS
					declare @statusTekaV varchar(50)

					set @statusTekaV=null

					select @statusTekaV=textD	 
					from	dbo.dictionary
					where	typeD='StatusTeka' and  value=@statusTeka

					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,55,null,@statusTekaV,@number2,null)

					
					--3Marka / model pojazdu klienta
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,3,isnull(@brand,'')+' '+isnull(@model,''),isnull(@brand2,'')+' '+isnull(@model2,''),isnull(@brand2,'')+' '+isnull(@model2,''),null)

					--4Numer rejestracyjny pojazdu klienta
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,4,@registration_number,@registration_number2,@registration_number2,null)

					--5VIN pojazdu klienta
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,5,@VIN,@VIN2,@VIN2,null)

					--6DMC pojazdu klienta
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,6,@DMC,@DMC,@DMC2,null)

					--7Rodzaj usługi podstawowej wykonanej
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,7,null,@service,@service,null)

					--8Dalsza jazda
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,8,null,@drive,@drive,null)

					-----------------------------------------------
					declare @SOtable as table (scheduled1 varchar(5), dojazd1 decimal(18,2), holowanie1 decimal(18,2), powrot1 decimal(18,2), adresDoc varchar(400),XFinishSO decimal(18,2), YFinishSO decimal(18,2), mileage int)
	
					delete from @SOtable

					declare @scheduled1 varchar(20)
					declare @scheduled1V int
					declare @dojazd1 decimal(18,0)
					declare @holowanie1 decimal(18,0)
					declare @powrot1 decimal(18,0)
					declare @adresDoc varchar(300)
					declare @XFinishSO decimal(18,8)
					declare @YFinishSO decimal(18,8)
					declare @mileageSO int
					
					declare @SQLSO varchar(5000)
							set @SQLSO='select * from openquery(sette,''select	su.PomocUmowionaNaTermin scheduled1,
										StarterOperations_Stage.dbo.f_sprawausluga_holowanie_dojazd(su.id) dojazd1,
										StarterOperations_Stage.dbo.f_sprawausluga_holowanie_holowanie(su.id) holowanie1,
										StarterOperations_Stage.dbo.f_sprawausluga_holowanie_powrot(su.id) powrot1,
										isnull(StarterOperations_Stage.[dbo].[f_sprawa_warsztat_docelowy_nazwa_dealera](s.id) COLLATE Polish_CI_AS,'''''''')+'''', ''''+
										isnull( StarterOperations_Stage.[dbo].[f_sprawa_warsztat_docelowe_ulica_ics](s.id),'''''''') COLLATE Polish_CI_AS+'''' ''''+
										isnull( cast( StarterOperations_Stage.[dbo].[f_sprawa_warsztat_docelowe_nr_posesji_ics](s.id) as varchar(100)),'''''''') COLLATE Polish_CI_AS+'''', ''''+
										isnull(  StarterOperations_Stage.[dbo].[f_sprawa_warsztat_docelowe_kod_ics](s.id),'''''''') COLLATE Polish_CI_AS+'''' ''''+
										isnull(  StarterOperations_Stage.[dbo].[f_sprawa_warsztat_docelowe_miasto_ics](s.id) COLLATE Polish_CI_AS,'''''''') adresDoc ,
										StarterOperations_Stage.[dbo].[f_sprawa_warsztat_docelowe_dlugosc_ics](s.id) XFinishSO,
										StarterOperations_Stage.[dbo].[f_sprawa_warsztat_docelowe_szerokosc_ics](s.id) YFinishSO,
										StarterOperations_Stage.[dbo].[f_sprawa_przebieg_auta](s.id) mileage
								from	StarterOperations_Stage.dbo.sprawa s inner join
										StarterOperations_Stage.dbo.sprawaUsluga su on s.id=su.Sprawa_id
								where	su.id='+cast(@UslugaId as varchar(20))+''')'

					print @SQLSO

					insert into @SOtable
					exec (@SQLSO)

					select	@scheduled1V=scheduled1,
							@dojazd1=dojazd1,
							@holowanie1=@holowanie1,
							@powrot1=@powrot1,
							@adresDoc=adresDoc,
							@XFinishSO=XFinishSO,
							@YFinishSO=YFinishSO,
							@mileageSO=mileage
					from @SOtable

					set @scheduled1=dbo.f_translate('nie',default)
					if @scheduled1V=0
					begin 
						set @scheduled1=dbo.f_translate('nie',default)
					end
					else
					begin
						set @scheduled1=dbo.f_translate('tak',default)
					end

					declare @scheduled2 varchar(5)
					if @scheduled=0
					begin 
						set @scheduled2=dbo.f_translate('nie',default)
					end
					else
					begin
						set @scheduled2=dbo.f_translate('tak',default)
					end

					-----------------------------------------------


					--9Czy umówione na termin?
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,9,@scheduled1,@scheduled2,@scheduled2,null)
					

					--10Data i czas przekazania zlecenia do pomocy drogowej
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,10,dbo.FN_VDateHour(@dataPrzekazania),null,null,null)

					--59Data i czas przekazania zlecenia do pomocy drogowej
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,59,null, dbo.FN_VDateHour(@eta),dbo.FN_VDateHour(@eta),null)

					--11Data i czas "kontraktor wyjechał"
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,11,null,dbo.FN_VDateHour(@startDate),dbo.FN_VDateHour(@startDate),null)

					--12Data i czas "na miejscu zdarzenia"
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,12,null,dbo.FN_VDateHour(@onSite),dbo.FN_VDateHour(@onSite),null)

					--13Data i czas "zakończono na miejscu"
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,13,null,dbo.FN_VDateHour(@finishOnSite),dbo.FN_VDateHour(@finishOnSite),null)

					--14Data i czas "zakończono holowanie"
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,14,null,dbo.FN_VDateHour(@finishTowing),dbo.FN_VDateHour(@finishTowing),null)

					--15Czas dojazdu (gg:mm)
					declare @arrivalTime nvarchar(50)
					set @arrivalTime=left(convert(char(8), dateadd(ss, DATEDIFF(ss,@startDate,@onSite),cast(0 as DateTime)), 108),5)

					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,15,null,@arrivalTime,@arrivalTime,null)

					--16Czas trwania interwencji na miejscu (gg:mm)
					declare @onSiteTime nvarchar(50)
					set @onSiteTime=left(convert(char(8), dateadd(ss, DATEDIFF(ss,@onSite,@finishOnSite),cast(0 as DateTime)), 108),5)

					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,16,null,@onSiteTime,@onSiteTime,null)

					--17Czas holowania (gg:mm)
					declare @towingTime nvarchar(50)
					set @towingTime=left(convert(char(8), dateadd(ss, DATEDIFF(ss,@finishOnSite,@finishTowing),cast(0 as DateTime)), 108),5)

					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,17,null,@towingTime,@towingTime,null)

					--18Czas oczekiwania na pomoc łącznie (gg:mm)
					declare @waitingTime nvarchar(50)
					set @waitingTime=left(convert(char(8), dateadd(ss, DATEDIFF(ss,@created_at,@onSite),cast(0 as DateTime)), 108),5)

					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,18,null,@waitingTime,@waitingTime,null)

					--19Miejsce początkowe pomocy drogowej
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,19,null,@YStart+','+@XStart,@YStart+','+@XStart,null)

					if isnull(@addressStart,'')=''
					begin
						EXEC	dbo.p_mapReverseGeocoding
								@lat = @YStart,
								@lng = @XStart,
								@address = @addressStart OUTPUT
					end

					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,48,null,@addressStart,@addressStart,null)

					--20Miejsce zdarzenia
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,20,@Y1+','+@X1,@YOnSite+','+@XOnSite,@YOnSite+','+@XOnSite,null)

					
					if isnull(@address,'')=''
					begin
						EXEC	dbo.p_mapReverseGeocoding
								@lat = @Y1,
								@lng = @X1,
								@address = @address OUTPUT
					end
					print isnull(@addressOnSite,'')+'****'
					if isnull(@addressOnSite,'')='' and @statusTeka>4
					begin
						print dbo.f_translate('jestem',default)
	
						EXEC	dbo.p_mapReverseGeocoding
								@lat = @YOnSite,
								@lng = @XOnSite,
								@address = @addressOnSite OUTPUT

								print @addressOnSite
					end
		
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,47,@address,@addressOnSite,@addressOnSite,null)
		
					--21Miejsce zdarzenia różnica w metrach
					declare @distanceDifference decimal(18,1)
					print @number
					print dbo.FN_distance(@Y1, @X1, @YOnSite, @XOnSite)
					print '--------------------------------'
					set @distanceDifference=dbo.FN_distance(@Y1, @X1, @YOnSite, @XOnSite)
					--set @distanceDifference=@distanceDifference



					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,21,null,@distanceDifference,@distanceDifference,null)


					if @statusTeka<6
					begin
						set @XFinish=null
						set @YFinish=null
					end

					declare @YFinishSOV varchar(100)
					declare @XFinishSOV varchar(100)

					set @XFinishSOV=cast(@XFinishSO as varchar(100))
					set @YFinishSOV=cast(@YFinishSO as varchar(100))

					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,22,@YFinishSOV+','+@XFinishSOV,@YFinish+','+@XFinish,@YFinish+','+@XFinish,null)

					--22Miejsce docelowego holowania
					-- *************
					
					if @statusTeka>6 and @service='H'
					begin
					
						print dbo.f_translate('jestem tttt',default)
						declare @distanceDifferenceTarget decimal(18,1)
						set @distanceDifferenceTarget=null
						set @distanceDifferenceTarget=dbo.FN_distance(@YFinishSOV, @XFinishSOV, @YFinish, @XFinish)
						set @distanceDifferenceTarget=@distanceDifferenceTarget

						INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
						VALUES	    (@idH,56,null,@distanceDifferenceTarget,@distanceDifferenceTarget,null)

						if @distanceDifferenceTarget<1.0 
						begin
							set @towing_destination=@adresDoc
						end
						else
						begin
							print dbo.f_translate('AAAAAAAAAAAAAAAAAA',default)
							if isnull(@addressTowing,'')=''
							begin
								EXEC	dbo.p_mapReverseGeocoding
										@lat = @YFinish,
										@lng = @XFinish,
										@address = @towing_destination OUTPUT
										set @towing_destination=','+@towing_destination
										print @towing_destination
							end
						end
					end 
					
					if (@service='N') or @statusTeka<=6
					begin
						set @addressTowing=''
						set @towing_destination=''
					end

					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,49,@adresDoc,@towing_destination,@towing_destination,null)

					set @custom_arrival=CEILING(@custom_arrival)
					set @custom_towing=CEILING(@custom_towing)
					set @custom_return=CEILING(@custom_return)


					declare @custom_arrival2 int
					declare @custom_towing2 int
					declare @custom_return2 int

					set @custom_arrival2=cast(@custom_arrival as int)
					set @custom_towing2=cast(@custom_towing as int)
					set @custom_return2=cast(@custom_return as int)

					--23Liczba km dojazdu
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,23,@dojazd1,@custom_arrival2,@custom_arrival2,null)


					--24Liczba km holowania
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,24,@holowanie1,@custom_towing2,@custom_towing2,null)

					--53Liczba km transportu osób
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,53,@holowanie1,@custom_towing2,@custom_towing2,null)


					--25Liczba km powrotu
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,25,@powrot1,@custom_return2,@custom_return2,null)

					--26Liczba RBH Zwykłe
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,26,null,@man_hour_time,@man_hour_time,null)

					--27Uzasadnienie RBH Zwykłe
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,27,null,@man_hour_time_reason,@man_hour_time_reason,null)

					--28Liczba RBH Dźwig
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,28,null,@lift_man_hour_time,@lift_man_hour_time,null)

					--29Uzasadnienie RBH Dźwig
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,29,null,@lift_man_hour_time_reason,@lift_man_hour_time_reason,null)

					--30Liczba osób przewiezionych w lawecie
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,30,null,@transported_people_number,@transported_people_number,null)


					declare @paid_for_viatollV varchar(10)
					if @paid_for_viatoll=1
					begin
						set @paid_for_viatollV=dbo.f_translate('Tak',default)
					end
					else
					begin
						set @paid_for_viatollV=dbo.f_translate('Nie',default)
					end 

					--31Viatoll
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,31,null,@paid_for_viatollV,@paid_for_viatollV,null)

					--32Viatoll łączny koszt netto
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,32,null,@viatoll_cost,@viatoll_cost,null)

					declare @highway_costV varchar(10)
					if isnull(@highway_cost,0)>0
					begin
						set @highway_costV=dbo.f_translate('Tak',default)
					end
					else
					begin
						set @highway_costV=dbo.f_translate('Nie',default)
					end 

					--33Koszty Autostrady
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,33,null,@highway_costV,@highway_costV,null)

					--34Koszty Autostrady netto
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,34,null,@highway_cost,@highway_cost,null)


					--34Transport osób osobnym pojazdem
					declare @are_people_transported_separatelyV varchar(10)
					if @are_people_transported_separately=1
					begin
						set @are_people_transported_separatelyV=dbo.f_translate('Tak',default)
					end
					else
					begin
						set @are_people_transported_separatelyV=dbo.f_translate('Nie',default)
					end 


					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,35,null,@are_people_transported_separatelyV,@are_people_transported_separatelyV,null)

					--35Dokumentacja zdjęciowa
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,36,null,@is_photo_documented,@is_photo_documented,null)

					--36kod zamknięcia ICS
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,37,null,@code,@code,null)

					--37kod zamknięcia ICS - opis
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,38,null,@code_desc,@code_desc,null)

					--54Dodatkowy komentarz
					INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
					VALUES	    (@idH,54,null,@comment,@comment,null)

					--60 raport z drogi
					if @image_file_name is not null
					begin
						declare @paths as table(document nvarchar(2000)) 

						declare @path nvarchar(1000)
						
						--set @number='U481652'

						set @path='dir \\ics.starter24.pl\udzial\'+lower(@number)+' /s /b'
						delete from @paths

						insert into @paths
						EXECUTE xp_cmdshell @path

						set @image_file_name=null

						select	@image_file_name='<A href="'+replace(replace(document,'\\ics.starter24.pl\udzial\','http://10.10.77.93/ics/'),'\','/')+'" target=_blank>Pokaż raport</A>' 
						from	@paths 
						where	document like '%filled%'
						print @image_file_name
						INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
						VALUES	    (@idH,60,null,@image_file_name,@image_file_name,null)
					end
				end

				
				if @service_vehicle_data='1' set @service_vehicle_data=dbo.f_translate('holownik',default)
				else if @service_vehicle_data='2' set @service_vehicle_data=dbo.f_translate('pojazd patrolowy Starter24',default)
				else if @service_vehicle_data='3' set @service_vehicle_data=dbo.f_translate('pojazd patrolowy kontraktora',default)
				else set @service_vehicle_data=dbo.f_translate('holownik',default)

				INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
				VALUES	    (@idH,61,null,@service_vehicle_data,@service_vehicle_data,null)

				--

				INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
				VALUES	    (@idH,51,null,@registration_number_of_separated_car,@registration_number_of_separated_car,null)

				INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
				VALUES	    (@idH,52,null,@distance_of_transported_people_in_separate_car,@distance_of_transported_people_in_separate_car,null)

				INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
				VALUES	    (@idH,63,dbo.f_translate('Holowanie do warsztatu',default),null,dbo.f_translate('Holowanie do warsztatu',default),null)

				INSERT INTO ics.elementsV (headerId, categoryId, value1, value2, valueResult, remarks)
				VALUES	    (@idH,64,@mileageSO,@mileage,@mileage,null)


				commit tran
		--	end
		--end
		FETCH NEXT FROM kur INTO @id, @number, @external_contractor_id, @name, @registration_number, @registration_number2, @responsible_user_phone, @brand, @model, @VIN, @brand2, @model2, @VIN2, @DMC, @DMC2, @mileage, @service, @drive, @scheduled, @created_at, @startDate, @onSite, @finishOnSite, @finishTowing, @XStart, @YStart, @XOnSite, @YOnSite, @XFinish, @YFinish, @X1, @Y1, @custom_arrival, @custom_return, @custom_towing, @man_hour_time, @man_hour_time_reason, @lift_man_hour_time, @lift_man_hour_time_reason, @transported_people_number, @viatoll_cost, @paid_for_viatoll, @highway_cost, @are_people_transported_separately, @is_photo_documented, @code, @code_desc, @UslugaId, @platforma, @program, @dataPrzekazania,@comment,@updatedAt,@statusTeka,@eta,@image_file_name,@service_vehicle_data,@registration_number_of_separated_car,@distance_of_transported_people_in_separate_car,@towing_destination;
	END
	CLOSE kur
	DEALLOCATE kur

