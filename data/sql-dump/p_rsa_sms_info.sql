ALTER PROCEDURE [dbo].[p_rsa_sms_info]
( 
	@groupProcessInstanceId INT
) 
AS
BEGIN
	
	PRINT '------------------ START s_1009_003 --------------------'
	
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @stepId NVARCHAR(255)
	DECLARE @processInstanceId INT
	DECLARE @czyPrzyjalZlecenie TINYINT
	DECLARE @partnerPhone NVARCHAR(20)
	DECLARE @parking INT
	DECLARE @partnerId INT
	DECLARE @makeModel NVARCHAR(60)
	DECLARE @regNumber NVARCHAR(20)
	DECLARE @vin NVARCHAR(40)
	DECLARE @etaMinutes INT
	DECLARE @eta DATETIME
	DECLARE @serviceId INT
	DECLARE @locationString NVARCHAR(1000)
	DECLARE @towingRouteText NVARCHAR(1000)
	DECLARE @programName NVARCHAR(200)
	DECLARE @driverFirstname NVARCHAR(100)
	DECLARE @driverLastname NVARCHAR(100)
	DECLARE @driverName NVARCHAR(200)
	DECLARE @driverPhone NVARCHAR(20)
	DECLARE @fixingOrTowing INT
	DECLARE @onTime TINYINT
	DECLARE @content NVARCHAR(MAX)
	
	-- Stworzenie pomocniczej tabelki
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '691', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '697', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @parking = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @serviceId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = textD FROM dbo.dictionary WHERE value IN (SELECT value_int FROM @values) AND typeD = 'makeModel'
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programName = name FROM dbo.vin_program WHERE id IN (SELECT value_string FROM @values)
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,64', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @driverFirstname = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,66', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @driverLastname = value_string FROM @values
	
	SET @driverName = @driverFirstname + ' ' + @driverLastname
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @driverPhone = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vin = value_string FROM @values
	
	-- Czy na termin
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '215', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @onTime = value_int FROM @values
	
	DECLARE @customDateText NVARCHAR(1000)
	SET @customDateText = ''
	
	-- Sprawdzanie czy na termin czy ETA
	IF ISNULL(@onTime, 0) = 1
	BEGIN
		
		DECLARE @onDateTime DATETIME
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @onDateTime = value_date FROM @values
		
		SET @customDateText = dbo.f_translate('Zlecenie umówione na ',default) + dbo.FN_VDateHour(@onDateTime) + '. ' 
			
	END
	ELSE
	BEGIN
		
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '698', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @etaMinutes = value_int FROM @values
		
		-- Gdyby jakimś cudem @etaMinutes była pusta to +20 minutes 
		SET @eta = DATEADD(minute , ISNULL(@etaMinutes, 20) , GETDATE() )  
		
		EXEC p_attribute_edit
			@attributePath = '107', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueDate = @eta,
			@err = @err OUTPUT,
			@message = @message OUTPUT
			
		IF @eta is not NULL
		BEGIN
			SET @customDateText = dbo.f_translate('Czas dojazdu wynosi ',default) + dbo.FN_VDateHour(@eta) + '. '
		END 
		
		SET @customDateText = @customDateText + dbo.f_translate('W razie trudnosci z dojazdem w podanym czasie prosimy o zaznaczenie odpowiedniej opcji w aplikacji lub kontakt telefoniczny.',default)
		
	END 
	
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	
	IF @fixingOrTowing = 1
	BEGIN
		-- HOLOWANIE
		DECLARE @startLocationString NVARCHAR(1000)
		DECLARE @endLocationString NVARCHAR(1000)
		DECLARE @serviceName NVARCHAR(300)
		
		IF @parking = 1
		BEGIN
			SET @startLocationString = dbo.f_translate('z Państwa parkingu',default)
		END 
		ELSE
		BEGIN
			EXEC p_location_string
			@attributePath = '101,85',
			@groupProcessInstanceId = @groupProcessInstanceId,
			@locationString = @startLocationString OUTPUT
			
			SET @startLocationString = dbo.f_translate('z lokalizacji zdarzenia: ',default)+ @startLocationString
		END 
		
		EXEC p_location_string
		@attributePath = '211,85',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@locationString = @endLocationString OUTPUT
	
		SELECT @serviceName = value_string FROM attribute_value where parent_attribute_value_id = @serviceId and attribute_path = '595,597,84'
		SET @endlocationString = dbo.f_translate(' do partnera serwisowego ',default)+@serviceName+': '+@endlocationString
		SET @towingRouteText = @startLocationString + ' ' + @endLocationString
		
		SET @content = dbo.f_translate('Zlecenie holowania ',default)+dbo.f_caseId(@groupProcessInstanceId)+' '+@towingRouteText+ dbo.f_translate(dbo.f_translate('/ Program: ',default),default)+@programName+dbo.f_translate('/ Kierowca: ',default)+@driverName+' '+@driverPhone+dbo.f_translate('/ Pojazd: ',default)+@makeModel+' '+@regNumber + dbo.f_translate('. Przypominamy o koniecznosci zakonczenia zlecenia w ciągu 8 godzin za posrednictwem aplikacji ',default) + dbo.f_getDomain() + '. '+@customDateText
		
	END 
	ELSE
	BEGIN
		-- NAPRAWA
		SET @content = dbo.f_translate('Zlecenie naprawy ',default)+dbo.f_caseId(@groupProcessInstanceId)+dbo.f_translate(dbo.f_translate('/ Program: ',default),default)+@programName+dbo.f_translate('/ Kierowca: ',default)+@driverName+' '+@driverPhone+dbo.f_translate('/ Pojazd: ',default)+@makeModel+' '+@regNumber + dbo.f_translate('. Przypominamy o koniecznosci zakonczenia zlecenia w ciągu 8 godzin za posrednictwem aplikacji ',default) + dbo.f_getDomain() + '. '+@customDateText
	
	END 
		
	PRINT @content
	-- WYsłanie SMS'a do kontraktora
	EXEC p_note_new
	@groupProcessId = @groupProcessInstanceId,
	@type = dbo.f_translate('sms',default),
	@content = @content,
	@phoneNumber = @partnerPhone,
	@addInfo = 0,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
END


