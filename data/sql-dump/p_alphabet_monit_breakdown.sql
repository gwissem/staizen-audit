ALTER PROCEDURE [dbo].[p_alphabet_monit_breakdown]
    @groupProcessInstanceId INT
AS
  BEGIN
    DECLARE @rootId int
    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))
    SELECT @rootId = root_id FROM process_instance with(nolock) where group_process_id = @groupProcessInstanceId

    DECLARE @caseId nvarchar(30)
    SELECT @caseId = dbo.f_caseId(@groupProcessInstanceId)


    DECLARE @regNumber nvarchar(10)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @regNumber = value_string FROM @values


    DECLARE @VIN nvarchar(50)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @VIN = value_string FROM @values

    DECLARE @makeModel int
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @makeModel = value_int FROM @values
    DECLARE @makeModelText nvarchar(100)

    SELECT @makeModelText = textD
    from AtlasDB_def.dbo.dictionary
    where typeD = 'makeModel'
      and value = @makeModel

    DECLARE @milage int
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @milage = value_int FROM @values

    DECLARE @eventTypeInt int
    DECLARE @eventType nvarchar(100)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @eventTypeInt = value_int FROM @values

    select @eventType = textD
    from AtlasDB_def.dbo.dictionary
    where typeD = 'EventType'
      and value = @eventTypeInt

    DECLARE @userName NVARCHAR(50)
    DECLARE @userSurname NVARCHAR(50)
    DECLARE @phoneNumber NVARCHAR(50)
    DECLARE @accidentDate DATETIME

    DECLARE @noteTitle nvarchar(500)


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,64',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userName = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,66',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userSurname = value_string FROM @values




    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @phoneNumber = value_string FROM @values



    SELECT TOP 1 @accidentDate = created_at from process_instance where root_id = @rootId order by ID ASC








    DECLARE @partnerLocation NVARCHAR(400)
    DECLARE @breakdownLocationString NVARCHAR(1000)
    DECLARE @partnerLocationId int

    DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('cfm')

    


    EXEC dbo.p_location_string
        @attributePath = '101,85',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @locationString = @breakdownLocationString OUTPUT





    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerLocationId = value_int FROM @values

    SELECT @partnerLocation = dbo.f_partnerNameFull(@partnerLocationId)
    






        DECLARE @caseViewURL NVARCHAR(100)
        SET @caseViewURL = [dbo].[f_getDomain]() + '/cfm-case-viewer/index?case-number=' +
                           cast(isnull(@rootId, '') as nvarchar(20))

        DECLARE @mailbody nvarchar(MAX)
        DECLARE @diagnosis nvarchar(1000)
     SET @diagnosis = dbo.f_diagnosis_description(@groupProcessInstanceId, 'pl')
        SET @mailbody =
          dbo.f_translate('Nr sprawy: ',default)+@caseId+' <br>' +
        dbo.f_translate('Nr rejestracyjny: ',default) + isnull(@regNumber, '') + '<br />' +
          dbo.f_translate('Marka i model pojazdu: ',default) + isnull(@makeModelText, '') + '<br />' +
          dbo.f_translate('Przebieg: ',default) + cast(isnull(@milage, '') as nvarchar(20)) + '<br />' +
dbo.f_translate('Miejsce szkody: ',default) + isnull(@breakdownLocationString, '''') + '<br />' +
            dbo.f_translate('   Data zdarzenia: ',default) + convert(varchar, ISNULL(@accidentDate, GETDATE()), 103) + ' '+convert(nvarchar(5), @accidentDate, 108)+'<br />'+
          dbo.f_translate('Rodzaj zdarzenia: ',default) + isnull(@eventType, '') + '<br />' +
dbo.f_translate('Diagnoza: ',default) +isnull(@diagnosis,'') +' <br />' +
 dbo.f_translate('Użytkownik: ',default) + isnull(@userName, '') + ' ' + isnull(@userSurname, '') + '<br />'+
dbo.f_translate('Nr telefonu użytkownika: ',default) + cast(isnull(@phoneNumber, '') as nvarchar(20)) + '<br />'+

    dbo.f_translate('Dane serwisu: ',default) + isnull(@partnerLocation, '') + '<br /> <br /> <br /> <br />'+
        '<i>
Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres cfm@starter24.pl
<br /><br />
Link do sprawy: <a href="'+isnull(@caseViewURL,'')+'" target="_blank">'+isnull(@caseViewURL,'')+'</a> </i>'

    DECLARE @err int
    DECLARE @message nvarchar(400)

    DECLARE @subject nvarchar(350)
    SET @subject = isnull(@caseId, '') + ' / ' + isnull(@regNumber, '')+' / '+ isnull(@eventType,'')+ dbo.f_translate(' / Otwarto nową sprawę',default)
    
    
	    SET @noteTitle = dbo.f_translate('Wysłano informacje o ',default)+isnull(@eventType,'')+' do serwis@alphabet.pl'



        DECLARE @email nvarchar(150)
        SET @email = dbo.f_getRealEmailOrTest('serwis@alphabet.pl')


    IF @eventTypeInt in (2,7)
      BEGIN
    EXEC p_note_new
        @groupProcessId = @groupProcessInstanceId,
        @type = dbo.f_translate('email',default),
        @content = @noteTitle,
        @phoneNumber = NULL,
        @email = @email,
        @subject = @subject,
        @direction = 1,
        @emailRegards = 1,
        @err = @err OUTPUT,
        @message = @message OUTPUT,
        -- FOR EMAIL
        @sender = @senderEmail,
        @additionalAttachments = '',
        @emailBody = @mailbody
  END
END