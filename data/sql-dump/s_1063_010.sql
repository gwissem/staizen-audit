ALTER PROCEDURE [dbo].[s_1063_010]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS

DECLARE @serviceDate DATETIME
DECLARE @serviceDateTo DATETIME
DECLARE @serviceDateChoice INT
DECLARE @createdAt DATETIME
DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
DECLARE @groupProcessInstanceId INT
DECLARE @stepId NVARCHAR(255)
DECLARE @err INT
DECLARE @message NVARCHAR(255)
SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @createdAt = created_at FROM process_instance where id = @previousProcessId

INSERT @values EXEC p_attribute_get2 @attributePath = '74,105', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @serviceDate = value_date FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '525', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @serviceDateTo = value_date FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '531', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @serviceDateChoice = value_int FROM @values

SET @serviceDate = CONVERT(datetime, CAST(@serviceDate as NVARCHAR(12))+'00:00')
SET @createdAt = CONVERT(datetime, CAST(@createdAt as NVARCHAR(12))+'00:00')

IF @serviceDate >= DATEADD(yy, -1, @createdAt) 
BEGIN
	SET @variant = 2
END
ELSE IF @serviceDateChoice = 2 AND @serviceDate < DATEADD(yy, -1, @createdAt) AND @serviceDateTo >= DATEADD(yy, -2, @createdAt)
BEGIN
	SET @variant = 1
END
ELSE 
BEGIN
	EXEC p_attribute_set2
		@attributePath = '532', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueText = dbo.f_translate('Uprawnienie assistance obowiązuje do określonej daty. Zebrane dane wskazują że ona minęła. W obecnej sytuacji mogę zaoferować pomoc odpłatną.',default),
		@err = @err OUTPUT,
		@message = @err OUTPUT
		
	SET @variant = 3
END
