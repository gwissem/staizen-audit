ALTER PROCEDURE [dbo].[s_1156_010]
  (
    @previousProcessId INT,
    @variant           TINYINT OUTPUT, @currentUser int,
    @errId             int = 0 output
  )
AS
  BEGIN
    DECLARE @userId INT
    DECLARE @originalUserId INT
    DECLARE @rootId INT
    DECLARE @err INT
    DECLARE @message NVARCHAR(255)
    DECLARE @newProcessId INT
    DECLARE @nextProcessInstances varchar(4000)
    DECLARE @groupProcessInstanceId INT
    DECLARE @conditionalAssistance INT


    SELECT @groupProcessInstanceId = group_process_id,
           @userId = created_by_id,
           @rootId = root_id,
           @originalUserId = created_by_original_id
    FROM process_instance with(nolock)
    WHERE id = @previousProcessId
    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))


    DECLARE @subject nvarchar(200)
    DECLARE @regNumber nvarchar(200)
    DECLARE @body nvarchar(max)
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId

    SELECT @regNumber = value_string from @values

    SET @subject = dbo.f_translate('Brak w bazie - ',default) + dbo.f_caseId(@previousProcessId) + ' / ' + isnull(@regNumber, 0)
    DECLARE @companyUsingCar nvarchar(200)
    DECLARE @companyLocationString nvarchar(400)
    EXEC p_location_mini_string @groupProcessInstanceId = @groupProcessInstanceId,
                           @locationString = @companyLocationString output

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1000', @groupProcessInstanceId = @groupProcessInstanceId

    SELECT top 1 @companyUsingCar = value_string from @values

    DECLARE @services TABLE(name nvarchar(100))

    insert into @services
    SELECT distinct sdt.name
    from service_status ss with (nolock)
           left join service_status_dictionary ssd with (nolock) on ss.status_dictionary_id = ssd.id
           left join service_definition sd with (nolock) on ss.serviceId = sd.id
           left join service_definition_translation sdt with (nolock) on sd.id = sdt.translatable_id and locale = 'pl'
    where group_process_id in
          (select distinct process_instance.group_process_id  from process_instance with (nolock) where root_id = @rootId)
      and ssd.progress >= 2
      and ssd.progress < 5


    DECLARE @Names VARCHAR(8000)
    SELECT @Names = COALESCE(@Names + ',<br /> ', '') + name FROM @services



    SET @body = 'Nie zweryfikowano uprawnień dla pojazdu {@74,73@}, {@74,72@}. <br /> <br />
Informacje o sprawie: <br />
Imię i nazwisko: {@80,342,64@} {@80,342,66@}<br />
Nr telefonu: {@80,342,408,197@}  <br />
Nazwa firmy: ' + isnull(@companyUsingCar, '') + '<br />
Adres firmy: ' + isnull(@companyLocationString, '') + ' <br />
Notatki: {@63@} <br />
Zorganizowane usługi:  <br />'
                + @Names +
                '<br />' +
                '<br />' +
                dbo.f_translate('Uprzejmie prosimy o zweryfikowanie uprawnień pojazdu do korzystania z Assistance oraz potwierdzenie programu, którym powinien być objęty pojazd. ',default)

    DECLARE @parsed NVARCHAR(MAX)
    exec P_parse_string @processInstanceId = @previousProcessId, @simpleText = @body, @parsedText = @parsed OUTPUT
    select @body = isnull(@parsed, '')





    DECLARE @eventType int
    DELETE from @values
    INSERT into @values EXEC p_attribute_get2 @groupProcessInstanceId = @groupProcessInstanceId,
                                              @attributePath = '491'
    SELECT @eventType = value_int from @values

    DELETE from @values


    DECLARE @mailTo nvarchar(100)
    IF @eventType in (2, 7)
      BEGIN
        --szkoda
        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('szkody@carefleet.com.pl')
      end
    else
      BEGIN
        SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('cok@carefleet.com.pl')
      end

    DECLARE @content NVARCHAR(100) = dbo.f_translate('Wysłano zapytanie o BWB do CareFleet - BWB po godzinach pracy CareFleet',default)
    EXEC p_note_new
        @groupProcessId = @groupProcessInstanceId,
        @type = dbo.f_translate('email',default),
        @content = @content,
        @phoneNumber = NULL,
        @email = @mailTo,
        @subject = @subject,
        @direction = 1,
        @emailRegards = 1,
        @err = @err OUTPUT,
        @message = @message OUTPUT,
        -- FOR EMAIL
        @udw = '',
        @sender = 'cfm@starter24.pl',
        @additionalAttachments = '',
        @emailBody = @body




    set @variant = 1


  END