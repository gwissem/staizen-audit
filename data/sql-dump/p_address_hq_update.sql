
ALTER PROCEDURE [dbo].[p_address_hq_update]
	-- Add the parameters for the stored procedure here

AS

-- ==========================================================================================
-- Author:		Krzysztof Róziecki
-- Create date: 28.03.2018
-- Description:	Procedura akrualizuje dane siedziby partnera na podstawie danych w CDN.
-- ==========================================================================================

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Tran_HQ_Sync VARCHAR(50);
	SELECT @Tran_HQ_Sync = dbo.f_translate('Partner HQ address update from CDN',default);

	BEGIN TRAN @Tran_HQ_Sync;

	DECLARE @ID int
	DECLARE @Miejscowosc varchar(50)
	DECLARE @Poczta varchar(50)
	DECLARE @Kraj varchar(50)
	DECLARE @KodP varchar(50)
	DECLARE @Ulica varchar(50)

	DECLARE kurs CURSOR FOR 
	(
		SELECT  avs.id AS ID,
				KK.Knt_Miasto AS Miejscowosc,
				KK.Knt_Miasto AS Poczta,
				KK.Knt_Kraj AS Kraj,
				KK.Knt_KodP AS KodP,
				KK.Knt_Ulica AS Ulica

		FROM	dbo.attribute_value avp with(nolock) inner join /* Partner */
				dbo.attribute_value avn with(nolock) on avp.id=avn.parent_attribute_value_id and avn.attribute_path='595,84' inner join /* Partner - Nazwa */
				dbo.attribute_value av_nip with(nolock) on avp.id=av_nip.parent_attribute_value_id and av_nip.attribute_path='595,83' inner join /* Partner - NIP */
				dbo.attribute_value avks with(nolock) on avp.id=avks.parent_attribute_value_id and avks.attribute_path='595,631' inner join /* Partner - Kod starter */

				dbo.attribute_value ava with(nolock) on avp.id=ava.parent_attribute_value_id and ava.attribute_path='595,624' inner join /* Partner - Adres siedziby */
				dbo.attribute_value avs with(nolock) on ava.id=avs.parent_attribute_value_id inner join /* Partner - Adres siedziby id mini */

				dbo.attribute_value av_miejscowosc with(nolock) on avs.id=av_miejscowosc.parent_attribute_value_id and av_miejscowosc.attribute_path='595,624,626,627' inner join
				dbo.attribute_value av_poczta with(nolock) on avs.id=av_poczta.parent_attribute_value_id and av_poczta.attribute_path='595,624,626,688' inner join
				dbo.attribute_value av_kraj with(nolock) on avs.id=av_kraj.parent_attribute_value_id and av_kraj.attribute_path='595,624,626,86' inner join
				dbo.attribute_value av_kodp with(nolock) on avs.id=av_kodp.parent_attribute_value_id and av_kodp.attribute_path='595,624,626,89' inner join
				dbo.attribute_value av_ulica with(nolock) on avs.id=av_ulica.parent_attribute_value_id and av_ulica.attribute_path='595,624,626,94' inner join

				[finansetest].[CDNXL_Starter].[CDN].[KntKarty] AS KK ON KK.Knt_Akronim = avks.value_string and KK.Knt_Nip = av_nip.value_string

		WHERE  	avp.attribute_path='595'
				AND
				(
					KK.Knt_Miasto != av_miejscowosc.value_string or
					KK.Knt_Kraj != av_kraj.value_string or 
					KK.Knt_KodP != av_kodp.value_string or
					KK.Knt_Ulica != av_ulica.value_string or
					KK.Knt_Miasto != av_poczta.value_string
				)
	)
	OPEN kurs
	FETCH NEXT FROM kurs
	INTO @ID,@Miejscowosc,@Poczta,@Kraj,@KodP,@Ulica
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		UPDATE [dbo].[attribute_value] SET value_string = @Miejscowosc	WHERE parent_attribute_value_id = @ID AND attribute_path = '595,624,626,627'
		UPDATE [dbo].[attribute_value] SET value_string = @Poczta		WHERE parent_attribute_value_id = @ID AND attribute_path = '595,624,626,688'
		UPDATE [dbo].[attribute_value] SET value_string = @Kraj			WHERE parent_attribute_value_id = @ID AND attribute_path = '595,624,626,86'
		UPDATE [dbo].[attribute_value] SET value_string = @KodP			WHERE parent_attribute_value_id = @ID AND attribute_path = '595,624,626,89'
		UPDATE [dbo].[attribute_value] SET value_string = @Ulica		WHERE parent_attribute_value_id = @ID AND attribute_path = '595,624,626,94'
		UPDATE [dbo].[attribute_value] SET value_string = ''			WHERE parent_attribute_value_id = @ID AND attribute_path = '595,624,626,95'
		UPDATE [dbo].[attribute_value] SET value_string = ''			WHERE parent_attribute_value_id = @ID AND attribute_path = '595,624,626,96'

		FETCH NEXT FROM kurs
		INTO @ID,@Miejscowosc,@Poczta,@Kraj,@KodP,@Ulica
	END
	CLOSE kurs
	DEALLOCATE kurs

	COMMIT TRAN @Tran_HQ_Sync;
END
