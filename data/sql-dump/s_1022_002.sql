ALTER PROCEDURE [dbo].[s_1022_002]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @postponeCount DATETIME
	DECLARE @postponeLimit INT
	DECLARE @serviceName NVARCHAR(255)
	DECLARE @subject NVARCHAR(500)
	DECLARE @cancelBy INT
	DECLARE @cancelGroupId INT
	DECLARE @partnerId INT		
	DECLARE @body NVARCHAR(MAX)
	DECLARE @email NVARCHAR(500)
	DECLARE @partnerType INT
	DECLARE @partnerPhone NVARCHAR(255)
	DECLARE @serviceId INT
	DECLARE @platformId INT

	SELECT @groupProcessInstanceId = p.group_process_id, @rootId = p.root_id, @postponeCount = p.postpone_count, @postponeLimit = s.postpone_count	
	FROM process_instance p WITH(NOLOCK)
	INNER JOIN step s ON s.id = p.step_id
	where p.id = @previousProcessId
	
	declare @country nvarchar(100)
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @country = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '843,834', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @cancelGroupId = value_int FROM @values


	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values


  IF @country <> dbo.f_translate(dbo.f_translate('Polska',default),default)

    BEGIN
      SELECT @serviceName = st.name, @serviceId = sd.id, @partnerType = d.value
      from	dbo.process_instance pin inner join
                dbo.service_definition sd on left(sd.start_step_id,4)=left(step_Id,4) inner join
                dbo.dictionary d ON CAST(d.descriptionD AS INT) = sd.id AND d.typeD = 'partnerServiceType' inner join
                dbo.service_definition_translation st on st.translatable_id=sd.id and st.locale='en' left join
                dbo.attribute_value avn on avn.group_process_instance_id=pin.group_process_id and avn.attribute_path='245,138' left join
                dbo.attribute_value avr on avr.group_process_instance_id=pin.group_process_id and avr.attribute_path='789,786,240,153' left join
                dbo.attribute_value avf on avf.group_process_instance_id=pin.group_process_id and avf.attribute_path='560' left join
                dbo.attribute_value avf2 on avf2.root_process_instance_id=pin.root_id and avf2.attribute_path='723'
      where	root_id=@rootId and
             ((isnull(avf2.value_int,avf.value_int)=2 and sd.id=2) or (isnull(avf2.value_int,avf.value_int)=1 and sd.id=1) or sd.id not in (1,2)) and sd.id<>13
        AND pin.group_process_id = @cancelGroupId
      group BY	group_process_id,
               st.name,
               d.value,
               sd.id
    end
  ELSE
    BEGIN
	SELECT @serviceName = st.name, @serviceId = sd.id, @partnerType = d.value
		 	from	dbo.process_instance pin inner join
					dbo.service_definition sd on left(sd.start_step_id,4)=left(step_Id,4) inner join 
					dbo.dictionary d ON CAST(d.descriptionD AS INT) = sd.id AND d.typeD = 'partnerServiceType' inner join
					dbo.service_definition_translation st on st.translatable_id=sd.id and st.locale='pl' left join
					dbo.attribute_value avn on avn.group_process_instance_id=pin.group_process_id and avn.attribute_path='245,138' left join
					dbo.attribute_value avr on avr.group_process_instance_id=pin.group_process_id and avr.attribute_path='789,786,240,153' left join
					dbo.attribute_value avf on avf.group_process_instance_id=pin.group_process_id and avf.attribute_path='560' left join
					dbo.attribute_value avf2 on avf2.root_process_instance_id=pin.root_id and avf2.attribute_path='723'
			where	root_id=@rootId and 
			((isnull(avf2.value_int,avf.value_int)=2 and sd.id=2) or (isnull(avf2.value_int,avf.value_int)=1 and sd.id=1) or sd.id not in (1,2)) and sd.id<>13
			AND pin.group_process_id = @cancelGroupId
			group BY	group_process_id,
						st.name,
						d.value,
						sd.id
		END
		
	if @country=dbo.f_translate(dbo.f_translate('Polska',default),default)
	begin
		SET @variant = 1
		
		
		IF @serviceId = 12
		BEGIN
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @cancelGroupId
			SELECT @partnerId = value_int FROM @values				
		END 
		ELSE
		BEGIN
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @cancelGroupId
			SELECT @partnerId = value_int FROM @values			
		END 
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '691', @groupProcessInstanceId = @cancelGroupId
		SELECT @partnerPhone = value_string FROM @values
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '843,610', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = @partnerId,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
	    EXEC [dbo].[p_attribute_edit]
	    @attributePath = '843,691', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueString = @partnerPhone,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
	end
	else
	begin
		SET @variant = 2

		-- przepisanie PZ
		
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @cancelGroupId
		SELECT @partnerId = value_int FROM @values


		IF isnull(@partnerId,0) = 0
			BEGIN
				-- Jeśli nie ma partnerid to bierzemy "niepotwierdzonego"
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '767,773,704', @groupProcessInstanceId = @cancelGroupId
				SELECT @partnerId = value_int FROM @values
			end
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '843,610', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = @partnerId,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	
	    DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '843,831', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @cancelBy = value_int FROM @values
		
		
		IF @cancelBy = 10
		BEGIN
			
			
			
			SELECT @email = dbo.f_partner_contact(@groupProcessInstanceId,@partnerId,@partnerType,4)
			
				
			SET @body='Hello from Starter24,<br/><b>CANCELLATION!</b> of case '+ dbo.f_caseId(@previousProcessId)  +'. Our Client doesn’t need '+@serviceName+' any more . Please let us know if case generate any costs.
					<BR><BR><BR>
					Best regards,<BR>
					Starter24'
							
			SET @subject = dbo.f_translate('Cancellation of service ',default) + @serviceName + dbo.f_translate(' for case ',default)+ dbo.f_caseId(@previousProcessId)
		
			DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')


			IF not (isnull(@platformId, 0 ) in (8,			13,			16,			21,			22,			68,			74))
				BEGIN
					EXECUTE dbo.p_note_new
							@groupProcessId = @groupProcessInstanceId
							,@type = dbo.f_translate('email',default)
							,@content = dbo.f_translate('Cancelling service',default)
							,@email = @email
							,@userId = 1  -- automat
							,@subject = @subject
							,@direction=1
							,@dw = ''
							,@udw = ''
							,@sender =  @senderEmail
							,@additionalAttachments = ''
							,@emailBody = @body
							,@err=@err OUTPUT
							,@message=@message OUTPUT

				end


			
		END
	end
	
END