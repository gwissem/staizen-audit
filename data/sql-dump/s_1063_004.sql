ALTER PROCEDURE [dbo].[s_1063_004]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
DECLARE @stepId NVARCHAR(10)
DECLARE @servicePartner INT
DECLARE @bougtAsNewAtSP INT
DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
DECLARE @groupProcessInstanceId INT
DECLARE @message NVARCHAR(255)
DECLARE @err INT
SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id FROM process_instance where id = @previousProcessId

INSERT @values EXEC p_attribute_get2 @attributePath = '521', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @bougtAsNewAtSP = value_int FROM @values

SET @variant = 1

IF @bougtAsNewAtSP < 1 
BEGIN
	IF @bougtAsNewAtSP = -1
	BEGIN
		EXEC p_attribute_set2
		@attributePath = '532', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueText = dbo.f_translate('Assistance dla pojazdów na gwarancji udzielane jest samochodom, które zostały zakupione jako nowe w polskiej autoryzowanej sieci dealerskiej marki. Proszę bardzo o ustalenie tego, na przykład na podstawie książki serwisowej auta i oddzwonieni',default),
		@err = @err OUTPUT,
		@message = @err OUTPUT
	END 
	ELSE
	BEGIN
		EXEC p_attribute_set2
		@attributePath = '532', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueText = dbo.f_translate('Assistance dla pojazdów na gwarancji udzielane jest samochodom, które zostały zakupione jako nowe w polskiej autoryzowanej sieci dealerskiej marki. W obecnej sytuacji mogę zaproponować usługi odpłatne.',default),
		@err = @err OUTPUT,
		@message = @err OUTPUT
	END 
	SET @variant = 2
END
