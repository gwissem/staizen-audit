ALTER PROCEDURE [dbo].[s_1007_055]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @c NVARCHAR(500)
	DECLARE @p1 NVARCHAR
	DECLARE @partnerId INT
	DECLARE @email NVARCHAR(100)
	DECLARE @subject NVARCHAR(300)
	DECLARE @emailContent NVARCHAR(4000)
	 
	SELECT @groupProcessInstanceId = group_process_id FROM dbo.process_instance with(nolock) WHERE id = @previousProcessId 
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	select TOP 1 @email = avContactEmail.value_string
	from   dbo.attribute_value avLocation with(nolock) inner join
    dbo.attribute_value avName with(nolock) on avName.parent_attribute_value_id=avLocation.id and avName.attribute_path='595,597,84' inner join
    dbo.attribute_value avService with(nolock) on avService.parent_attribute_value_id=avLocation.id and avService.attribute_path='595,597,611' inner join
    dbo.attribute_value avServiceType with(nolock) on avServiceType.parent_attribute_value_id=avService.id and avServiceType.attribute_path='595,597,611,612' and avServiceType.value_int = 5 inner join
    dbo.attribute_value avPosibility with(nolock) on avPosibility.parent_attribute_value_id=avService.id and avPosibility.attribute_path='595,597,611,641' inner join
    dbo.attribute_value avContact with(nolock) on avContact.parent_attribute_value_id=avService.id and avContact.attribute_path='595,597,611,642' inner join
    dbo.attribute_value avContactType with(nolock) on avContactType.parent_attribute_value_id=avContact.id and avContactType.attribute_path='595,597,611,642,643' and avContactType.value_int = 4 inner join
    dbo.attribute_value avContactEmail with(nolock) on avContactEmail.parent_attribute_value_id=avContact.id and avContactEmail.attribute_path='595,597,611,642,656'  and avContactEmail.value_string is not null		 
    where avLocation.attribute_path='595,597' 
    and avLocation.id = @partnerId
    
    SET @subject = dbo.f_translate('Kopia Raportu Zakończenia Wynajmu do zgłoszenia ',default)+dbo.f_caseId(@groupProcessInstanceId) 
    SET @emailContent = 'Szanowni Państwo,<br/>W załączeniu przesyłamy kopię wypełnionego Raportu Zakończenia Wynajmu do zgłoszenia '+dbo.f_caseId(@groupProcessInstanceId) +'.<br/>Z poważaniem, Zespół Starter24'
    
    DECLARE @fileName NVARCHAR(255)
    SET @fileName = '{FORM::'+CAST(@previousProcessId AS NVARCHAR(20))+'::raport_zakonczenia_wynajmu_'+dbo.f_caseId(@groupProcessInstanceId)+'}'
	
    DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rentals')
    DECLARE @platformGroup nvarchar(255)	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		SET @senderEmail = dbo.f_getEmail('cfm')
	END

	EXECUTE dbo.p_note_new 
	     @groupProcessId = @groupProcessInstanceId
	    ,@type = dbo.f_translate('email',default)
	    ,@content = @emailContent
	    ,@email = @email
	    ,@userId = 1  -- automat
	    ,@subject = @subject
	    ,@direction=1
	    ,@dw = ''
	    ,@udw = ''
	    ,@sender = @senderEmail
		,@additionalAttachments = @fileName
	    ,@emailBody = @emailContent
	    ,@subType = dbo.f_translate('unstoppable',default)
	    ,@emailRegards = 1
	    ,@err=@err OUTPUT
	    ,@message=@message OUTPUT
	    
	SET @c = dbo.f_translate('Wypożyczalnia uzupełniła RZW',default)
	 -- LOGGER START
		SET @p1 = dbo.f_caseId(@groupProcessInstanceId)		
		EXEC dbo.p_log_automat
		@name = 'rental_automat',
		@content = @c,
		@param1 = @p1
	-- LOGGER END
	
	DECLARE @automatId INT 
	
	SELECT top 1 @automatId = id FROM dbo.process_instance with(nolock) where step_id = '1007.057' and active = 1 and group_process_id = @groupProcessInstanceId
	
	IF @automatId is not NULL
	BEGIN
	  
	  EXEC [dbo].[p_process_next]
	      @previousProcessId = @automatId,
	      @err = @err OUTPUT,
	      @variant = 3,
	      @message = @message OUTPUT
            
	END  
	
	IF ISNULL(@currentUser,1) <> 1 
	BEGIN
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '812,63', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueText = NULL,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	END 
	
END


