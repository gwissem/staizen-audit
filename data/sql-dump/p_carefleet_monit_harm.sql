ALTER PROCEDURE [dbo].[p_carefleet_monit_harm]
@groupProcessInstanceId INT,
@type                   int
AS
  BEGIN
    DECLARE @rootId int

    DECLARE @perpetorAdditionalText NVARCHAR(1000)

    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))
    SELECT @rootId = root_id FROM process_instance with(nolock) where group_process_id = @groupProcessInstanceId

    DECLARE @caseId nvarchar(30)
    SELECT @caseId = dbo.f_caseId(@groupProcessInstanceId)


    DECLARE @regNumber nvarchar(10)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @regNumber = value_string FROM @values


    DECLARE @VIN nvarchar(50)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @VIN = value_string FROM @values

    DECLARE @makeModel int
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @makeModel = value_int FROM @values
    DECLARE @makeModelText nvarchar(100)

    SELECT @makeModelText = textD
    from AtlasDB_def.dbo.dictionary
    where typeD = 'makeModel'
      and value = @makeModel

    DECLARE @milage int
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @milage = value_int FROM @values

    DECLARE @eventTypeInt int
    DECLARE @eventType nvarchar(100)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @eventTypeInt = value_int FROM @values

    select @eventType = textD
    from AtlasDB_def.dbo.dictionary
    where typeD = 'EventType'
      and value = @eventTypeInt

    DECLARE @userName NVARCHAR(50)
    DECLARE @userSurname NVARCHAR(50)
    DECLARE @PESEL NVARCHAR(50)
    DECLARE @userMail NVARCHAR(50)
    DECLARE @phoneNumber NVARCHAR(50)
    DECLARE @accidentDate DATETIME
    DECLARE @accidentDateText NVARCHAR(100)
    DECLARE @accidentHourText NVARCHAR(100)
    DECLARE @accidentDamage NVARCHAR(1000)
    DECLARE @accidentDescription NVARCHAR(1000)
    DECLARE @perpetorOtherText NVARCHAR(1000)
    DECLARE @accidentType NVARCHAR(150)
    DECLARE @accidentTypeId int
    DECLARE @vehicleStatusId int
    DECLARE @vehicleStatus nvarchar(150)
    DECLARE @perpetorTypeId int
    DECLARE @perpetorTypeText nvarchar(150)
    DECLARE @perpetorFirstName nvarchar(150)
    DECLARE @perpetorLastName nvarchar(150)
    DECLARE @typeOfHarmRealisationId int
    DECLARE @typeOfHarmRealisation nvarchar(60)
    DECLARE @policeConfirmation int
    DECLARE @policeConfirmationText nvarchar(10)
    DECLARE @caseDocuments nvarchar(150)
    DECLARE @caseDocumentsTEXT nvarchar(4000)
    DECLARE @noteTitle nvarchar(500)


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,64',
 	@groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userName = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,66',
 	@groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userSurname = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,327',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @PESEL = value_string FROM @values


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,368',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userMail = value_string FROM @values


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @phoneNumber = value_string FROM @values


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '101,104',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @accidentDate = value_date FROM @values


     IF @accidentDate is null
       BEGIN
         SELECT top 1 @accidentDate = date_enter from process_instance where  root_id = @rootId order by id asc

       end

    SET @accidentDateText = convert(nvarchar(100),@accidentDate,105)


    SET @accidentHourText = convert(nvarchar(100),@accidentDate,108)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,443',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @accidentDamage = value_text FROM @values

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,442',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @accidentDescription = value_text FROM @values


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,444',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @accidentTypeId = value_int FROM @values
    
    SELECT @accidentType = textD
    FROM dbo.dictionary
    where typeD = 'claimType2'
      and value = @accidentTypeId

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,119',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @vehicleStatusId = value_int FROM @values
    SELECT @vehicleStatus = textD
    FROM dbo.dictionary
    where typeD = 'claimVehicleState'
      and value = @vehicleStatusId


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,811',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorTypeId = value_int FROM @values
    
    SELECT @perpetorTypeText = textD
    FROM dbo.dictionary
    where typeD = 'perpetratorTypeOc'
      and value = @perpetorTypeId


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,63',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorAdditionalText = value_string from @values


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,64',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorFirstName = value_string from @values

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,66',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorLastName = value_string from @values


    DECLARE @perpetorRegNumber nvarchar(10)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,72',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorRegNumber = value_string FROM @values

    DECLARE @perpetorVin nvarchar(20) = ''
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,71',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorVin = value_string FROM @values

    DECLARE @perpetorMakeModel int
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,73',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorMakeModel = value_int FROM @values

    DECLARE @perpetorMakeModelText nvarchar(100)

    SELECT @perpetorMakeModelText = textD
    from AtlasDB_def.dbo.dictionary
    where typeD = 'makeModel'
      and value = @perpetorMakeModel

    DECLARE @perpetorMail nvarchar(100)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,368',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorMail = value_string FROM @values

    DECLARE @perpetorOCNumber nvarchar(100)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '254,440',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @perpetorOCNumber = value_string FROM @values

    SET @perpetorOtherText = 'Dane sprawcy: <br />'

    IF isnull(@perpetorRegNumber, '') <> ''
      BEGIN
        SET @perpetorOtherText = @perpetorOtherText + dbo.f_translate(' Nr rejestracyjny: ',default) + @perpetorRegNumber + ', '
      end
    IF isnull(@perpetorVin, '') <> ''
      BEGIN
        SET @perpetorOtherText = @perpetorOtherText + '<br /> VIN: ' + @perpetorVin + ', '
      end
    IF isnull(@perpetorMakeModelText, '') <> ''
      BEGIN
        SET @perpetorOtherText = @perpetorOtherText + '<br /> Model samochodu: ' + @perpetorMakeModelText + ', '
      end
    IF isnull(@perpetorMail, '') <> ''
      BEGIN
        SET @perpetorOtherText = @perpetorOtherText + '<br /> E-mail sprawcy:' + @perpetorMail + ', '
      end    
    IF @perpetorOtherText = 'Dane sprawcy: <br />'
      BEGIN
        SET @perpetorOtherText = null
      end
    ELSE
      BEGIN
        SET @perpetorOtherText = ' <br /><br />' + @perpetorOtherText + ' <br /><br />'
      end


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '116,201',
                                             @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @typeOfHarmRealisationId = value_int from @values


    IF @typeOfHarmRealisationId = 1
      BEGIN
        SET @typeOfHarmRealisation = dbo.f_translate('OC sprawcy',default)
      end
    ELSE
      BEGIN
        SET @typeOfHarmRealisation = dbo.f_translate('AC właściciela',default)
      end

    DECLARE @policeCity NVARCHAR(400)

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1006', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @policeCity = value_string FROM @values

    
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '542', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @policeConfirmation = value_int FROM @values

    IF ISNULL(@policeCity,'') <> '' OR @policeConfirmation = 1
      BEGIN
        SET @policeConfirmationText = dbo.f_translate('TAK',default)
      end
    ELSE
      BEGIN
        SET @policeConfirmationText = dbo.f_translate('NIE',default)
      end
      

    DECLARE @harmLocationString NVARCHAR(400)
    DECLARE @partnerLocation NVARCHAR(400)
    DECLARE @partnerLocationId int
	DECLARE @partnerMail nvarchar(150)
	DECLARE @mailbody nvarchar(MAX)
    DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('cfm')
    DECLARE @dwMail nvarchar(40) = ''
    
    EXEC dbo.p_location_string
        @attributePath = '500,85',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @locationString = @harmLocationString OUTPUT

    DECLARE @carLocationString NVARCHAR(400)

    EXEC dbo.p_location_string
        @attributePath = '755,85',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @locationString = @carLocationString OUTPUT

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '116,445', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @caseDocuments = value_string FROM @values

    SELECT @caseDocumentsTEXT = dbo.f_dictionaryDesc(@caseDocuments, 'claimDocuments')

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerLocationId = value_int FROM @values

    SELECT @partnerLocation = dbo.f_partnerNameFull(@partnerLocationId)
    
    SET @partnerMail = dbo.f_getRealEmailOrTest('szkody@carefleet.com.pl')

    DECLARE @caseViewURL NVARCHAR(100)
    SET @caseViewURL = [dbo].[f_getDomain]() + '/cfm-case-viewer/index?case-number=' +
                       cast(isnull(@rootId, '') as nvarchar(20))
    DECLARE @liquidationPath int
    DECLARE @liquidationPathText NVARCHAR(50)

    DELETE from @values

    INSERT INTO @values EXEC p_attribute_get2 @attributePath = '116,244', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @liquidationPath = value_int from @values

    SELECT @liquidationPathText = textD from dbo.dictionary where
        value = @liquidationPath and typeD = 'liquidationPath'



    DECLARE @lostRegistration INT

    DELETE from @values
    INSERT INTO @values EXEC p_attribute_get2 @attributePath = '116,244', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @lostRegistration = value_int from @values


    DECLARE @insurrenceCompany INT
    DECLARE @insurrenceCompanyText NVARCHAR(200)
    DECLARE @insurrenceCompanyTextOther NVARCHAR(500)
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '254,439', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @insurrenceCompany = value_int from @values
    SELECT @insurrenceCompanyText = textD
    from dbo.dictionary
    where typeD = 'insurancer'
      and value = @insurrenceCompany

    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '254,518', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @insurrenceCompanyTextOther = value_string from @values
    DELETE from @values





    SET @mailbody =
    dbo.f_translate('Nr sprawy: ',default) + isnull(@caseId, '') + '<br />' +
    dbo.f_translate('Nr rejestracyjny: ',default) + isnull(@regNumber, '') + '<br />' +
    dbo.f_translate('VIN: ',default) + isnull(@VIN, '') + '<br />' +
    dbo.f_translate('Marka i model pojazdu: ',default) + isnull(@makeModelText, '') + '<br />' +
    dbo.f_translate('Przebieg: ',default) + cast(isnull(@milage, '') as nvarchar(20)) + '<br />' +
    dbo.f_translate('Użytkownik / Kierowca: ',default) + isnull(@userName, '') + ' ' + isnull(@userSurname, '') + '<br />'+
    dbo.f_translate('Nr telefonu: ',default) + cast(isnull(@phoneNumber, '') as nvarchar(20)) + '<br />' +
    dbo.f_translate('Data zdarzenia: ',default) + @accidentDateText + '<br />' +
    dbo.f_translate('Godzina szkody: ',default)  + isnull(@accidentHourText,'') +' <br />'+
    dbo.f_translate('Rodzaj zdarzenia: ',default) + isnull(@eventType, '') + '<br />' +
    dbo.f_translate('Uszkodzenia: ',default) + isnull(@accidentDamage, '') + '<br />' +
    dbo.f_translate('Okoliczności szkody: ',default) + isnull(@accidentDescription, '') + '<br />' +
    dbo.f_translate('Rodzaj szkody: ',default) + isnull(@accidentType, '') + '<br />' +
    dbo.f_translate('Stan pojazdu: ',default) + isnull(@vehicleStatus, '') + '<br />' +
    dbo.f_translate('Szkoda realizowana z: ',default) + isnull(@typeOfHarmRealisation, '') + '<br />' +
    dbo.f_translate('Sprawca: ',default) + isnull(@perpetorTypeText, '') + '<br />' +
    dbo.f_translate('Imię i nazwisko sprawcy: ',default) + isnull(@perpetorFirstName, '') + ' ' + isnull(@perpetorLastName, '') + '<br />' +
    dbo.f_translate('Dostępne dokumenty: ',default) + isnull(@caseDocumentsTEXT, '') +'<br />' +
    dbo.f_translate('Ścieżka likwidacji: ',default)+ isnull(@liquidationPathText, '') +'<br />'+
    dbo.f_translate('Miejsce szkody: ',default)  + isnull(@harmLocationString,'') +' <br />'+
    dbo.f_translate('Miejsce użytkowania pojazdu: ',default) + isnull(@carLocationString,'') +'<br />'+
    dbo.f_translate('Czy wezwano Policje: ',default) +@policeConfirmationText+'<br />'+
    dbo.f_translate('Jednostka policji:  ',default) + isnull(@policeCity, '') + '<br />' +
    dbo.f_translate('Czy zgubione tablice: ',default) + iif(isnull(@lostRegistration,0) =1,dbo.f_translate('Tak',default),dbo.f_translate('Nie',default))+ '<br /> '+
    '<b>Informacje o OC </b><br>'+
    dbo.f_translate('Nr rejestracyjny: ',default) +isnull(@perpetorRegNumber,'') +'<br / >' +
    dbo.f_translate('Marka/Model: ',default) +isnull(@perpetorMakeModelText,'') +'<br />'+
    dbo.f_translate('Nr Polisy: ',default) +isnull(@perpetorOCNumber,'') +'<br/>'+
    dbo.f_translate('Ubezpieczyciel: ',default)+isnull(@insurrenceCompanyText,'')+'<br />'+
    dbo.f_translate('Ubezpieczyciel inny: ',default)+ isnull(@insurrenceCompanyTextOther,'')+' <br />'+
    dbo.f_translate('Uwagi: ',default)+ isnull(@perpetorAdditionalText,'')+' <br />'+
    '<br /> <br /> <br /> <br />
    
    Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres cfm@starter24.pl
    Brakujące informacje w powyższym zestawieniu zostaną uzupełnione w najszybszym możliwym terminie. Ich podgląd jest możliwy na platformie zleceń ATLAS Viewer. <br />
    Link do sprawy: <a target="_blank" href="' + isnull(@caseViewURL, '#') + '">' + isnull(@caseViewURL, '') + '</a>'


    DECLARE @err int
    DECLARE @message nvarchar(400)

    DECLARE @subject nvarchar(350)
    SET @subject = dbo.f_translate('Zarejestrowano szkodę CareFleet ',default) + isnull(@caseId, '') + '/ ' + isnull(@regNumber, '')
    SET @noteTitle = dbo.f_translate('Wysłano informacje o szkodzie do ',default)+@partnerMail
    
    EXEC p_note_new
        @groupProcessId = @groupProcessInstanceId,
        @type = dbo.f_translate('email',default),
        @content = @noteTitle,
        @phoneNumber = NULL,
        @email = @partnerMail,
        @subject = @subject,
        @direction = 1,
        @emailRegards = 1,
        @err = @err OUTPUT,
        @message = @message OUTPUT,
        -- FOR EMAIL
        @dw = @dwMail,
        @udw = '',
        @sender = @senderEmail,
        @additionalAttachments = '',
        @emailBody = @mailbody
  
END