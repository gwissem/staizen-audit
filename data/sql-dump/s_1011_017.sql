ALTER PROCEDURE [dbo].[s_1011_017]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @groupProcessInstanceId INT
	DECLARE @vin NVARCHAR(255)
	DECLARE @sameDiagnosisStepCount INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @towingDiagnosis INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @diagnosisSummary INT
	DECLARE @eventType INT
	DECLARE @programId NVARCHAR(255)
	DECLARE @caseFromPZ SMALLINT = 0
	DECLARE @platformId int
	DECLARE @rootId int
	DECLARE @makeModelId INT 
	DECLARE @makeModel NVARCHAR(255)
	
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id,
	@rootId = root_id
	FROM process_instance with(nolock)
	WHERE id = @previousProcessId 
	
	DECLARE @platformGroup nvarchar(255)
	EXEC dbo.p_platform_group_name @groupProcessInstanceId= @groupProcessInstanceId, @name =@platformGroup OUTPUT
		
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vin = value_string FROM @values


	declare @regNumber nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @rootId
	SELECT @makeModelId = value_int FROM @values
	
	select @makeModel=textD
	from dbo.dictionary
	where value=@makeModelId and typeD='makeModel'
	
	declare @case_category int
	declare @basic_service nvarchar(20)
	declare @code nvarchar(100)
	
	set		@code=dbo.f_diagnosis_code(@groupProcessInstanceId)
	
	SELECT @case_category=case_category, 
		   @basic_service=basic_service
	FROM   dbo.diagnosis_results
	where  platform_id=@platformId and @code like code+'%'
	
	DECLARE @diagnosisId NVARCHAR(255)
	SELECT @diagnosisId = CAST(process_definition_id as NVARCHAR(255)) FROM process_import where name = dbo.f_translate('diagnosis',default) and active = 1
	
	declare @isFilledVehicleData int
	declare @DMC int

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2
		   @attributePath = '74,76',
		   @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @DMC = value_int FROM @values
	
	if @DMC is null
	begin
		set @isFilledVehicleData=0
	end
	else
	begin
		set @isFilledVehicleData=1
	end

	EXEC [dbo].[p_attribute_edit]
				@attributePath = '210',
				@stepId = 'xxx',
				@groupProcessInstanceId = @groupProcessInstanceId,
				@valueInt = @isFilledVehicleData,
				@err = @err,
				@message = @message

	if @basic_service is not null
	begin
		declare @lastDiagnosisId int

		SELECT TOP 1 @lastDiagnosisId=p.id
		FROM dbo.step_translation st 
		INNER JOIN step s ON st.translatable_id = s.id
		INNER JOIN process_instance p ON p.step_id = s.id 
		WHERE  st.locale = 'pl' AND s.is_technical = 1 AND st.translatable_id LIKE @diagnosisId+'.%'
		AND p.root_id = @groupProcessInstanceId AND p.last_run = 1 AND RTRIM(LTRIM(st.name)) IN ('[H][hdt]', '[N][hdt]', '[H]','[N]', '[PN]', '[NpT]', '[NpT-P]', '[NpT-T]')
		order by p.id desc
	
		UPDATE	dbo.process_instance
		set		step_id=@basic_service
		where	id=@lastDiagnosisId
	end

	if @case_category is not null
	begin
		EXEC p_attribute_edit
		@attributePath = '419', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueInt = @case_category,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	end

	SET @variant = 1

	
	SET @diagnosisSummary = 2
	
	SELECT @towingDiagnosis = dbo.f_isStepVariantInProcess(@groupProcessInstanceId, @diagnosisId+'.0009', default) 
	
	IF @towingDiagnosis = 1
	BEGIN
		SET @diagnosisSummary = 1
	END 

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventType = value_int FROM @values
--	select dbo.f_diagnosis_code(747659)
	-- apteka gemini

	declare @arcCode nvarchar(20)
	set @arcCode=dbo.f_diagnosis_code(@groupProcessInstanceId)

	IF @platformId = 25 AND @eventType = 2 and (@arcCode like '300%' or @arcCode like '309%' or @arcCode like '35522%')
	BEGIN
		declare @fleet nvarchar(100)
		exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('wlasciciel',default), @output=@fleet OUTPUT
		if @fleet like '%APTEKA GEMINI%'
		begin
			if not exists(select id from dbo.attribute_value where attribute_path='406,226,227' and value_text='Specjalna procedura obsługi dla pojazdów Dostawczych z firmy Apteka Gemini – dla Awarii polegającej na problemach z układem chłodzenia, ZAWSZE HOLUJEMY.' and root_process_instance_id=@rootId)
			begin
				EXECUTE dbo.p_note_new
				@groupProcessId=@groupProcessInstanceId
				,@type=dbo.f_translate('text',default)
				,@content='Specjalna procedura obsługi dla pojazdów Dostawczych z firmy Apteka Gemini – dla Awarii polegającej na problemach z układem chłodzenia, ZAWSZE HOLUJEMY. '
				,@userId=@currentUser
				,@err=@err OUTPUT
				,@message=@message OUTPUT
				,@special=1
				,@subject=dbo.f_translate('Uwagi zapisane w bazie uprawnień',default)
			end
		end
	END
	IF @platformId = 25 and (@arcCode='3592497' or @arcCode='3002497') and @makeModel like 'Ford%'
	BEGIN
		EXECUTE dbo.p_note_new
				@groupProcessId=@groupProcessInstanceId
				,@type=dbo.f_translate('text',default)
				,@content=dbo.f_translate('Specjalna procedura organizacji N/H dla pojazdów Ford ze zdiagnozowanym wyciekiem oleju.',default)
				,@userId=@currentUser
				,@err=@err OUTPUT
				,@message=@message OUTPUT
				,@special=1
				,@subject=dbo.f_translate('Uwagi zapisane w bazie uprawnień',default)
	END 
	/*	Jeżeli LP i kradzież to próba stworzenia zadania na poinformowanie o kradzieży 
 	____________________________________*/

	IF @platformId = 25 AND @eventType = 6
	BEGIN
		EXEC [dbo].[p_try_create_step_stolen_car] @previousProcessId = @previousProcessId, @currentUser = @currentUser
	END

	EXEC p_attribute_edit
		@attributePath = '271', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueInt = @diagnosisSummary,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '716', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @caseFromPZ = value_int FROM @values	

	DECLARE @homeAssistance int
	DECLARE @homeAssistanceBWB int
	
	IF EXISTS(select 1 from dbo.process_instance pin with(nolock)
				where pin.step_id = 1011.049
				and root_id = @rootId)
	BEGIN
		SET @homeAssistance = 1
	END
	ELSE
	BEGIN
		SET @homeAssistance = 0
	END


	IF EXISTS(select 1 from dbo.process_instance pin with(nolock)
				where pin.step_id = 1147.007
				and root_id = @rootId)
	BEGIN
		SET @homeAssistanceBWB = 1
	END
	ELSE
	BEGIN
		SET @homeAssistanceBWB = 0
	END
	
	IF @caseFromPZ = 1 OR @homeAssistance = 1 OR @homeAssistanceBWB = 1
	BEGIN
		-- Zlecenie od PZ'ta - od razu do Zlokalizuj klienta
		SET @variant = 5
		
	END 
	ELSE IF @platformId=5
	BEGIN
		-- ADAC
		SET @variant = 10
		
		RETURN
		
	END 
	ELSE IF ISNULL(@programId,'') NOT LIKE '%423%'
	BEGIN
		
		IF @eventType = 2 AND @platformGroup NOT IN (dbo.f_translate('CFM',default),'PSA') -- tylko awarie
		BEGIN
			
			select @sameDiagnosisStepCount = COUNT(distinct op.step_id) from process_instance p with(nolock)
			inner join process_instance op with(nolock) on p.step_id = op.step_id and p.step_id like ISNULL(@diagnosisId,'')+'.%' and p.root_id <> op.root_id
			WHERE p.root_id = @groupProcessInstanceId
			AND op.last_run = 1
			AND p.last_run = 1
			AND exists (
				SELECT av.id FROM attribute_value av with(nolock) WHERE av.attribute_path = '74,71' and av.group_process_instance_id = op.root_id AND av.value_string = @vin
			)


			SET @variant = 1
			
			IF @sameDiagnosisStepCount > 2
			BEGIN
				SET @variant = 2
			END
			ELSE IF @platformId =18
				BEGIN
					SET @variant = 6
			end
		END

		------
		if @platformId=2
		begin

			declare @programIds nvarchar(200)

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '204', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @programIds = value_string FROM @values
		
			declare @headerIds nvarchar(500)

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '207', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @headerIds = value_string FROM @values
		
			--set @code='1134555'

			IF EXISTS(
				
				SELECT id 
				FROM dbo.rsa_close_event 
				where ISNULL(condition,'') = dbo.f_translate('mniej niż 30 km od miejsca zamieszkania',default) 
				AND @code LIKE arc_mask 
				AND description = dbo.f_translate('brak bezpłatnej obsługi',default) 
				AND platform_id=@platformId
				AND dbo.f_exists_in_split(@programIds,program_id)=1
			
			)
			BEGIN
				EXEC p_attribute_edit
				@attributePath = '192', 
				@groupProcessInstanceId = @groupProcessInstanceId,
				@stepId = 'xxx',
				@valueInt = 1,
				@err = @err OUTPUT,
				@message = @message OUTPUT
			END 
--			
--			IF EXISTS(
--				
--				SELECT id 
--				FROM dbo.rsa_close_event 
--				where ISNULL(condition,'') = dbo.f_translate('mniej niż 30 km od miejsca zamieszkania',default) 
--				AND @code LIKE arc_mask 
--				AND description = dbo.f_translate('brak bezpłatnej obsługi',default) 
--				AND platform_id=@platformId
--				AND dbo.f_exists_in_split(@programIds,program_id)=1
--			
--			)
--			BEGIN
--				EXEC p_attribute_edit
--				@attributePath = '192', 
--				@groupProcessInstanceId = @groupProcessInstanceId,
--				@stepId = 'xxx',
--				@valueInt = 2,
--				@err = @err OUTPUT,
--				@message = @message OUTPUT
--			END 
			
		end
	END
	DECLARE @accidentType INT

	DELETE  FROM @values

	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @accidentType = value_int FROM @values


	PRINT '--@accidentType'
	PRINT @accidentType
	IF @platformId  = 35 AND @accidentType = 2 -- tylko awaria
		BEGIN

			DECLARE @insuranceRegisterDate DATETIME
			DECLARE @productionYear INT


			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '981,67', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @insuranceRegisterDate = value_date FROM @values

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '74,427', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @productionYear = value_int FROM @values


			PRINT '--@productionYear'
			PRINT @productionYear
			PRINT @productionYear
			PRINT '--@insuranceRegisterDate'
			PRINT 	@insuranceRegisterDate
			PRINT 	YEAR(@insuranceRegisterDate)
			IF (
				(
					(
						(YEAR(@insuranceRegisterDate)  - @productionYear) > 10
					)
					AND
					@programId in (480, 476)
				)
				OR
				(
					(
						(
							YEAR(@insuranceRegisterDate)  - @productionYear) > 15
					)
					AND
					@programId in (478, 477, 481, 482)
				)
			)
				BEGIN
					SET @variant = 4
				end

      IF @arcCode like '68045%'
				BEGIN
-- 					Kluczyki
					DEClARE @caseTable TABLE(rootId int)
					INSERT into @caseTable
					SELECT l.root_process_instance_id
					from (SELECT distinct reg_nr.root_process_instance_id
								from attribute_value vin with (nolock)
											 join attribute_value reg_nr with (nolock)
												 on reg_nr.value_string = @regNumber and reg_nr.attribute_path = '74,72'
								where vin.attribute_path = '74,71'
									and vin.value_string = @vin)l
					join attribute_value arcDiagnosis with (nolock ) on attribute_path='638,216' and
									l.root_process_instance_id = arcDiagnosis.root_process_instance_id and arcDiagnosis.value_string like '68045%'

					IF (SELECT count(*) from @caseTable) > 2
						BEGIN
							DECLARE @casesIds nvarchar(100)
							SELECT @casesIds = dbo.concatenate( rootId ) from @caseTable

							DECLARE @messageKeys NVARCHAR(1000)
								SET @messageKeys = ('Limit interwencji (sprawy: ' + @casesIds+') w sprawie zatrzaśniętych kluczyków został przekroczony. W związku z tym możemy zaproponować jedynie pomoc odpłatną.')


							EXEC p_attribute_edit
									@attributePath = '532',
									@groupProcessInstanceId = @groupProcessInstanceId,
									@stepId = @stepId,
									@valueText= @messageKeys,
									@err = @err OUTPUT,
									@message = @message OUTPUT
							SET @variant = 96
							return
						end
				end


		end
		ELSE IF @platformGroup = dbo.f_translate('CFM',default)
		BEGIN

			set @variant = 1

			/*	Sprawdzamy czy diagnoza zwróciła Wypadek/Szkoda i nie został utworzony krok 1011.070 (czyli zmieniło z Awarii na Wypadek)
	     	____________________________________*/
			
			if not exists(
			select	pin.id 
			from	dbo.process_instance pin with(nolock) 
			where	root_id=@rootId and 
					last_run=1 and
					pin.step_id='1011.070'
			) and @eventType IN (1,3)
			begin 
				
				DECLARE @cfmPermission INT
			
				/*	Sprawdzenie również, czy ma uprawnienia dla obsługi szkody
	     		____________________________________*/
				
				DELETE FROM @values
				INSERT  @values EXEC dbo.p_attribute_get2
			  		@attributePath = '143',
			  		@groupProcessInstanceId = @groupProcessInstanceId
			  	SELECT @cfmPermission = value_int FROM @values
			 	
			  	IF ISNULL(@cfmPermission, 0) IN (2,3)
			  	BEGIN
				  	
				  	/*	Kieruje na 1011.070	
       				____________________________________*/
				  	
				  	IF @platformId = 78
					BEGIN
						DECLARE @inRange nvarchar(255)
						
						EXEC [dbo].[P_parse_string]
						@processInstanceId = @previousProcessId,
						@simpleText = '{#isInDateRange(08:00|19:00|09:00|13:00)#}',
						@parsedText = @inRange OUTPUT
						
						IF @inRange = '1'
						BEGIN
							
							SET @variant = 1
							
							RETURN 
						END 
					END 
				  	
				  	SET  @variant = 8
				 
			  	END
				
			end

			DECLARE @id int 
			DECLARE @weekDay int
			DECLARE @dateFrom int 
			DECLARE @dateTo int
			DECLARE @askCFM int
			DECLARE @diagnosisCode NVARCHAR(10) = dbo.f_diagnosis_code(@groupProcessInstanceId)
			DECLARE @currentDate datetime = getdate()
			DECLARE @emailAlreadySent int 
			DECLARE @header nvarchar(4000)
			DECLARE @footer nvarchar(4000)
			DECLARE @firstname nvarchar(50)
			DECLARE @lastname nvarchar(150)
			DECLARE @email nvarchar(150)
			DECLARE @phoneNumber nvarchar(150)
			declare @type int
			
			
			SELECT @id = id,@type=1
			FROM dbo.rsa_close_event 
			where @code LIKE arc_mask 
			AND description = 'brak bezpłatnej obsługi CFM (w godzinach pracy zapytanie do klienta/poza godzinami propozycja usług płatnych)'
			AND platform_id=@platformId
			AND (program_id=@programId or program_id is null)


			if @id is null
			begin
				SELECT @id = id, @type=2
				FROM dbo.rsa_close_event 
				where @code LIKE arc_mask 
				AND description = dbo.f_translate('brak bezpłatnej obsługi ALD',default)
				AND platform_id=@platformId
				AND (program_id=@programId or program_id is null)

			end

			if @id is null
			begin
			
				SELECT @id = id, @type=3
				FROM dbo.rsa_close_event 
				where @code LIKE arc_mask 
				AND description = dbo.f_translate('8-18 Kontakt z BLP',default)
				AND platform_id=@platformId
				AND (program_id=@programId or program_id is null)
			end

	
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '59,65', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @emailAlreadySent = value_int FROM @values
	

			delete from @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,64', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @firstname = value_string FROM @values
			
			delete from @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,66', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @lastname = value_string FROM @values
			
			delete from @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,368', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @email = value_string FROM @values
			
			delete from @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @phoneNumber = value_string FROM @values
			
   
			IF @id is not NULL
			BEGIN			
				SET @variant = 7
				
				if @type=3
				begin
					set @variant=9

					-- pn – pt 8-18 i sobo 8-13
					set @weekDay=datepart(weekday,@currentDate)

					IF (dbo.FN_IsHoliday(@currentDate) = 0 AND (datepart(hour,@currentDate) > 8 AND datepart(hour,@currentDate) < 18)) OR (@weekDay=7 and datepart(hour,@currentDate) < 8 AND datepart(hour,@currentDate) > 13)
					BEGIN
					
						SET @variant = 91
						EXEC [dbo].[p_attribute_edit]
						  @attributePath = '554', 
						  @groupProcessInstanceId = @groupProcessInstanceId,
						  @stepId = 'xxx',
						  @userId = 1,
						  @originalUserId = 1,
						  @valueText = 'Szanowny Panie/Pani, w godzinach pracy BusinessLease (pn – pt 8-18 i sobo 8-13) prosimy kontaktować się z infolinią pod numerem 0224634370.',
						  @err = @err OUTPUT,
						  @message = @message OUTPUT
					
					END 
				end
				else if @type=2
				begin
					SET @header = 'Szanowni Państwo,<br/>Przesyłamy wniosek użytkownika z prośbą o autoryzację organizacji świadczeń w sprawie '+dbo.f_caseId(@groupProcessInstanceId)+dbo.f_translate(' dla nieobsługiwanego rodzaju zdarzenia.',default)
					set @footer = 
							isnull('Diagnoza: '+dbo.f_diagnosis_description(@groupProcessInstanceId,'pl')+'<br/>','')+
							+ isnull('Nr rej pojazdu: '+@regNumber+'<br/>','')+
							+ isnull('Imię i nazwisko: '+@firstname+' '+@lastname+'<br/>','')
							+ isnull('Nr telefonu: '+@phoneNumber+'<br/>','')
							+ isnull('E-mail: '+@email+'<br/>','')
		--				  + '<br/><br/>Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres cfm@starter24.pl'
							
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '59,65', 
						@groupProcessInstanceId = @groupProcessInstanceId,
						@stepId = 'xxx',
						@userId = 1,
						@originalUserId = 1,
						@valueInt = 0,
						@err = @err OUTPUT,
						@message = @message OUTPUT
					   
					EXEC [dbo].[p_attribute_edit]
						@attributePath = '59,113', 
						@groupProcessInstanceId = @groupProcessInstanceId,
						@stepId = 'xxx',
						@userId = 1,
						@originalUserId = 1,
						@valueText = @header,
						@err = @err OUTPUT,
						@message = @message OUTPUT
					   
				   	EXEC [dbo].[p_attribute_edit]
						@attributePath = '59,114', 
						@groupProcessInstanceId = @groupProcessInstanceId,
						@stepId = 'xxx',
						@userId = 1,
						@originalUserId = 1,
						@valueText = @footer,
						@err = @err OUTPUT,
						@message = @message OUTPUT

				end
				else
				begin
					IF dbo.FN_IsHoliday(@currentDate) = 1 OR (datepart(hour,@currentDate) < 8 AND datepart(hour,@currentDate) > 18)
					BEGIN
						SET @variant = 96
						EXEC [dbo].[p_attribute_edit]
						  @attributePath = '532', 
						  @groupProcessInstanceId = @groupProcessInstanceId,
						  @stepId = 'xxx',
						  @userId = 1,
						  @originalUserId = 1,
						  @valueText = dbo.f_translate('Szanowny Panie/Pani, zdarzenie jest nieobsługiwane w ramach warunków Assistance przypisanych do tego pojazdu. W tej sytuacji nie możemy zorganizować pomocy. W sytuacji awaryjnej jesteśmy w stanie wysłać płatną. Koszty zostaną refakturowane',default),
						  @err = @err OUTPUT,
						  @message = @message OUTPUT
					
					END 
					ELSE  
					BEGIN
					
						SET @header = 'Szanowni Państwo,<br/>Przesyłamy wniosek użytkownika z prośbą o autoryzację organizacji świadczeń w sprawie '+dbo.f_caseId(@groupProcessInstanceId)+dbo.f_translate(' dla nieobsługiwanego rodzaju zdarzenia.',default)
						set @footer = 
							  isnull('Diagnoza: '+dbo.f_diagnosis_description(@groupProcessInstanceId,'pl')+'<br/>','')+
							  + isnull('Nr rej pojazdu: '+@regNumber+'<br/>','')+
							  + isnull('Imię i nazwisko: '+@firstname+' '+@lastname+'<br/>','')
							  + isnull('Nr telefonu: '+@phoneNumber+'<br/>','')
							  + isnull('E-mail: '+@email+'<br/>','')
			--				  + '<br/><br/>Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres cfm@starter24.pl'
							
						EXEC [dbo].[p_attribute_edit]
						   @attributePath = '59,65', 
						   @groupProcessInstanceId = @groupProcessInstanceId,
						   @stepId = 'xxx',
						   @userId = 1,
						   @originalUserId = 1,
						   @valueInt = 0,
						   @err = @err OUTPUT,
						   @message = @message OUTPUT
					   
						EXEC [dbo].[p_attribute_edit]
						   @attributePath = '59,113', 
						   @groupProcessInstanceId = @groupProcessInstanceId,
						   @stepId = 'xxx',
						   @userId = 1,
						   @originalUserId = 1,
						   @valueText = @header,
						   @err = @err OUTPUT,
						   @message = @message OUTPUT
					   
				   		EXEC [dbo].[p_attribute_edit]
						   @attributePath = '59,114', 
						   @groupProcessInstanceId = @groupProcessInstanceId,
						   @stepId = 'xxx',
						   @userId = 1,
						   @originalUserId = 1,
						   @valueText = @footer,
						   @err = @err OUTPUT,
						   @message = @message OUTPUT
					END 
				END
			END
		END
END