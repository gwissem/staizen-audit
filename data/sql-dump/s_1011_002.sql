

ALTER PROCEDURE [dbo].[s_1011_002] (
	@previousProcessId INT
	,@variant TINYINT OUTPUT
	,@currentUser INT
	,@errId INT = 0 OUTPUT
	)
AS
BEGIN
	SET NOCOUNT ON

	PRINT '-------------------------- EXEC [dbo].[s_1011_002]'

	DECLARE @groupProcessInstanceId INT
	DECLARE @vinAndRegNumberMatch TINYINT
	DECLARE @vinAndRegNumberMatchId INT
	DECLARE @register_number NVARCHAR(255)
	DECLARE @mismatchVal NVARCHAR(4000)
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @programIds NVARCHAR(255)
	DECLARE @headerIds NVARCHAR(500)
	DECLARE @removePrograms INT
	DECLARE @platformId INT
	DECLARE @casePath INT
	DECLARE @processPath INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(200)
	DECLARE @firstRegDate DATETIME
	DECLARE @inMondialDB TINYINT = 0
	DECLARE @programCam TINYINT = 0
	DECLARE @countryFirstSold NVARCHAR(100)
	
	declare @translationParams as translationParams

	SET @removePrograms = 0

	SELECT @groupProcessInstanceId = group_process_id
		,@rootId = root_id
		,@stepId = step_id
	FROM dbo.process_instance WITH (NOLOCK)
	WHERE id = @previousProcessId

	DECLARE @alreadyProgramAssigned INT
	DECLARE @platformGroup NVARCHAR(255)

	EXEC [dbo].[p_platform_group_name] @groupProcessInstanceId = @groupProcessInstanceId
		,@name = @platformGroup OUTPUT

	DECLARE @atribute_table TABLE (
		id INT
		,value_int INT
		,value_string NVARCHAR(255)
		,value_text NVARCHAR(MAX)
		,value_date DATETIME
		,value_decimal DECIMAL(18, 5)
		)

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '202'
		,@groupProcessInstanceId = @groupProcessInstanceId

	SELECT @alreadyProgramAssigned = value_string
	FROM @atribute_table

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '115'
		,@groupProcessInstanceId = @groupProcessInstanceId

	SELECT @casePath = value_int
	FROM @atribute_table

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '253'
		,@groupProcessInstanceId = @rootId

	SELECT @platformId = value_int
	FROM @atribute_table

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '115'
		,@groupProcessInstanceId = @rootId

	SELECT @processPath = value_int
	FROM @atribute_table

	DECLARE @verificationType INT

	SELECT @verificationType = [dbo].[f_get_platform_key]('policy_verification', @platformId, DEFAULT)

	IF isnull(@verificationType, 1) = 2
		AND @variant <> 99
	BEGIN
		SET @variant = 6

		RETURN
	END

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '74,233'
		,@groupProcessInstanceId = @groupProcessInstanceId

	SELECT @firstRegDate = value_date
	FROM @atribute_table;

	-- jeśli już wcześniej zgodzono się na usługi płatne nie aktualizujemy wartości
	IF (
			@variant <> 99
			AND ISNULL(@alreadyProgramAssigned, '') LIKE '%423%'
			)
		OR @platformId = 76 -- cupra
	BEGIN
		SET @variant = 5

		IF @platformId = 76
		BEGIN
			DECLARE @confirmation INT

			DELETE
			FROM @atribute_table

			INSERT @atribute_table
			EXEC dbo.p_attribute_get2 @attributePath = '936'
				,@groupProcessInstanceId = @groupProcessInstanceId

			SELECT @confirmation = value_int
			FROM @atribute_table;

			IF @firstRegDate < dateadd(year, - 2, getdate())
			BEGIN
				SET @confirmation = 0
			END

			IF @confirmation = 1
			BEGIN
				EXEC [dbo].[p_attribute_edit] @attributePath = '204'
					,@stepId = 'xxx'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@valueString = '585'
					,@err = @err
					,@message = @message

				EXEC [dbo].[p_attribute_edit] @attributePath = '202'
					,@stepId = 'xxx'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@valueString = '585'
					,@err = @err
					,@message = @message
			END
			ELSE
			BEGIN
				EXEC [dbo].[p_attribute_edit] @attributePath = '204'
					,@stepId = 'xxx'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@valueString = NULL
					,@err = @err
					,@message = @message

				EXEC [dbo].[p_attribute_edit] @attributePath = '202'
					,@stepId = 'xxx'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@valueString = NULL
					,@err = @err
					,@message = @message

				SET @variant = 96
			END
		END
	END
	ELSE
	BEGIN
		DELETE
		FROM @atribute_table

		INSERT @atribute_table
		EXEC dbo.p_attribute_get2 @attributePath = '74,72'
			,@groupProcessInstanceId = @groupProcessInstanceId

		SELECT @register_number = value_string
		FROM @atribute_table;

		SET @register_number = replace(@register_number, ' ', '')

		IF @register_number = ''
		BEGIN
			SET @variant = 99

			-- 						set @errId=1
			RETURN
		END

		DELETE
		FROM @atribute_table

		INSERT @atribute_table
		EXEC dbo.p_attribute_get2 @attributePath = '74,75'
			,@groupProcessInstanceId = @groupProcessInstanceId

		DECLARE @mileage INT;

		SELECT @mileage = value_int
		FROM @atribute_table;

		DELETE
		FROM @atribute_table

		INSERT @atribute_table
		EXEC dbo.p_attribute_get2 @attributePath = '74,71'
			,@groupProcessInstanceId = @groupProcessInstanceId

		DECLARE @VIN VARCHAR(20);

		SELECT @VIN = value_string
		FROM @atribute_table;

		DECLARE @attributeValueId INT
		DECLARE @tempTable TABLE (
			id INT IDENTITY(1, 1)
			,programId INT
			,startDate DATETIME
			,endDate DATETIME
			,mileageLimit NVARCHAR(20)
			,headerId INT
			,remarks NVARCHAR(4000)
			)

		IF len(isnull(@register_number, '')) < 4
			AND @variant = 99
		BEGIN
			RETURN
		END

		IF len(isnull(@register_number, '')) > 4
			AND @variant = 99
		BEGIN
			DECLARE @header_id INT

			SELECT TOP 1 @header_id = id
			FROM vin_header WITH (NOLOCK)
			WHERE search = @register_number
				OR search2 = @register_number
			ORDER BY id DESC;

			SET @variant = 99

			INSERT INTO @tempTable (
				programId
				,startDate
				,endDate
				,mileageLimit
				,headerId
				,remarks
				)
			VALUES (
				NULL
				,NULL
				,NULL
				,NULL
				,@header_id
				,''
				)
		END
		ELSE
		BEGIN
			IF isnull(@VIN, '') <> ''
				OR @platformGroup = 'CFM'
			BEGIN
				EXEC [dbo].[p_attribute_edit] @attributePath = '207'
					,@stepId = 'xxx'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@valueString = NULL
					,@err = @err
					,@message = @message

				EXEC [dbo].[p_attribute_edit] @attributePath = '204'
					,@stepId = 'xxx'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@valueString = NULL
					,@err = @err
					,@message = @message

				EXEC [dbo].[p_attribute_edit] @attributePath = '202'
					,@stepId = 'xxx'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@valueString = NULL
					,@err = @err
					,@message = @message

				DECLARE @name NVARCHAR(200)
				DECLARE @start NVARCHAR(20)
				DECLARE @end NVARCHAR(20)
				DECLARE @startDate DATETIME
				DECLARE @mileageLimit NVARCHAR(20)
				DECLARE @programId INT
				DECLARE @casesForVehicle INT

				IF @platformGroup = 'CFM'
				BEGIN
					--                     FIX IF empyu
					IF ISNULL(@register_number, '') = ''
					BEGIN
						SET @register_number = dbo.f_translate('loremipsumdolorsitamet', DEFAULT)
					END

					SELECT TOP 1 @VIN = ve.value
					FROM dbo.vin_element ve WITH (NOLOCK)
					WHERE header_id IN (
							SELECT id
							FROM dbo.vin_header WITH (NOLOCK)
							WHERE search = @register_number
								OR search2 = @register_number
							)
						AND dictionary_id IN (
							SELECT id
							FROM dbo.vin_dictionary
							WHERE attribute_path = '74,71'
								OR description_org LIKE '%vin%'
							)
						AND isnull(ve.value, '') <> ''

					EXEC [dbo].[p_attribute_edit] @attributePath = '74,71'
						,@stepId = 'xxx'
						,@groupProcessInstanceId = @groupProcessInstanceId
						,@attributeValueId = @attributeValueId
						,@valueString = @VIN
						,@err = @err
						,@message = @message
				END

				INSERT INTO @tempTable (
					programId
					,startDate
					,endDate
					,mileageLimit
					,headerId
					)
				SELECT program_id
					,start_date
					,end_date
					,limit
					,id
				FROM dbo.vin_header WITH (NOLOCK)
				WHERE STATUS = 1
					AND getdate() BETWEEN start_date
						AND DATEADD(day, + 1, end_date)
							--and package_id=1012
					AND (
						search IN (
							@register_number
							,@vin
							)
						OR search2 IN (
							@register_number
							,@vin
							)
						)

				IF @platformGroup = 'CFM'
				BEGIN
					-- 						Usuwanie PSA z wyników dla CFM
					DELETE
					FROM @tempTable
					WHERE headerId IN (
							SELECT headerId
							FROM dbo.vin_header vin
							WHERE vin.id = headerId
								AND vin.package_id IN (
									1047
									,1048
									)
							)
				END

				IF @platformId IN (
						79
						,80
						,81
						,82
						)
					AND @variant <> 99
				BEGIN
					-- 		Look into MONDIAL
					DECLARE @headerId INT

					SELECT @headerId = headerid
					FROM @tempTable
					ORDER BY headerId DESC

					DECLARE @mondialValue NVARCHAR(100)

					SELECT @mondialValue = vin
					FROM dbo.mondial_vins
					WHERE vin = @VIN

					IF isnull(@mondialValue, '') <> ''
					BEGIN
						SET @inMondialDB = 1
					END
				END

				DELETE
				FROM @tempTable
				WHERE programId IN (
						SELECT id
						FROM dbo.vin_program WITH (NOLOCK)
						WHERE platform_id IN (
								SELECT platform_id
								FROM dbo.platform_group_platforms WITH (NOLOCK)
								WHERE platform_group_id = 6
								)
							AND STATUS = 1
							AND platform_id <> @platformId
						)

				IF @platformId IN (
						79
						,81
						)
				BEGIN
					DELETE
					FROM @tempTable
					WHERE programId IN (
							SELECT id
							FROM dbo.vin_program WITH (NOLOCK)
							WHERE STATUS = 1
								AND platform_id NOT IN (
									79
									,81
									)
							)

					DECLARE @hadCam INT

					SELECT @hadCam = count(programId)
					FROM @tempTable
					WHERE programId IN (
							595
							,603
							)

					DELETE
					FROM @tempTable ---usuwanie CAM
					WHERE programId IN (
							595
							,603
							)

					DECLARE @countStillHas INT

					SELECT @countStillHas = count(programId)
					FROM @tempTable

					PRINT '@countStillHas'
					PRINT @countStillHas
					PRINT '@hadCam'
					PRINT @hadCam

					IF isnull(@countStillHas, 0) = 0
						AND @hadCam > 0
					BEGIN
						SET @programCam = 1
					END
					ELSE
					BEGIN
						SET @programCam = 0
					END
				END
				ELSE IF @platformGroup <> 'CFM'
				BEGIN
					DELETE
					FROM @tempTable
					WHERE programId IN (
							SELECT id
							FROM dbo.vin_program WITH (NOLOCK)
							WHERE STATUS = 1
								AND platform_id <> @platformId
							)
				END

				UPDATE tt
				SET tt.remarks = ve.value
				FROM @tempTable tt
				INNER JOIN dbo.vin_element ve ON ve.header_id = tt.headerId
					AND ve.dictionary_id IN (2844)

				DECLARE @eventType INT

				DELETE
				FROM @atribute_table

				INSERT @atribute_table
				EXEC dbo.p_attribute_get2 @attributePath = '491'
					,@groupProcessInstanceId = @groupProcessInstanceId

				SELECT @eventType = value_int
				FROM @atribute_table

				IF @eventType <> 2
					AND @platformId = 2
				BEGIN
					DELETE
					FROM @tempTable
					WHERE programId IN (
							429
							,430
							)

					SET @removePrograms = 1
				END

				UPDATE t1
				SET t1.mileageLimit = ISNULL(TRY_PARSE(t1.mileageLimit AS INT), 9999999)
				FROM @tempTable t1

				--select * from @tempTable
				SELECT @programIds = dbo.GROUP_CONCAT_S(DISTINCT t.programId, 1)
					,@headerIds = dbo.GROUP_CONCAT_S(DISTINCT t.headerId, 1)
				FROM @tempTable t
				JOIN dbo.vin_program vp WITH (NOLOCK) ON vp.id = t.programId
					AND (
						vp.platform_id = isnull(@platformId, 0)
						OR ISNULL(@platformGroup, '') = 'CFM'
						OR @platformId IN (
							6
							,11
							,31
							,79
							,81
							)
						) /* join do platformy w celu wykluczenia cls leaseplan w Fordzie 20181009 MG*/
				WHERE t.mileageLimit >= ISNULL(@mileage, 1)

				SELECT TOP 1 @programId = programId
					,--p.name
					@start = dbo.FN_fdate(startDate)
					,@startDate = startDate
					,@end = dbo.FN_fdate(endDate)
					,@mileageLimit = mileageLimit
					,@header_id = headerId
				FROM @tempTable tt
				INNER JOIN dbo.vin_program p ON p.id = tt.programId
					AND (
						(
							p.platform_id = isnull(@platformId, 0)
							OR ISNULL(@platformGroup, '') = 'CFM'
							)
						OR @platformId IN (
							6
							,11
							,31
							,79
							,81
							)
						) /* join do platformy w celu wykluczenia cls leaseplan w Fordzie 20181009 MG*/
				WHERE mileageLimit >= ISNULL(@mileage, 1)
					AND (
						p.platform_id = @platformId
						OR (
							@platformId IN (
								79
								,81
								)
							AND p.platform_id IN (
								79
								,81
								)
							)
						)
					AND programId NOT IN (422)
				ORDER BY CASE 
						WHEN @platformId NOT IN (
								79
								,81
								)
							AND p.platform_id = @platformId
							THEN - 2
						ELSE - 1
						END
					,isnull(p.breakdown_priority, - 1)
					,mileageLimit DESC
					,endDate DESC

				IF @platformGroup = 'CFM'
					AND isnull(@processPath, 1) = 2
				BEGIN
					DECLARE @onlyProducer INT
					DECLARE @extra INT

					PRINT '---------------------------------------PROGRAM IDS1111'
					PRINT @programIds

					IF EXISTS (
							SELECT id
							FROM dbo.vin_program
							WHERE dbo.f_exists_in_split(@programIds, id) = 1
								AND is_producer = 1
							)
						AND NOT EXISTS (
							SELECT id
							FROM dbo.vin_program
							WHERE dbo.f_exists_in_split(@programIds, id) = 1
								AND isnull(is_producer, 0) = 0
							)
					BEGIN
						SELECT TOP 1 @extra = id
						FROM dbo.vin_program
						WHERE platform_id = @platformId
							AND is_extra = 1

						SET @programIds = @programIds + ISNULL(',' + CAST(@extra AS NVARCHAR(255)), '')
						SET @programId = @extra
					END
				END

				SELECT @name = NAME
				FROM dbo.vin_program WITH (NOLOCK)
				WHERE id = @programId

				PRINT '-------------------------------------------------------name'
				PRINT @name
				PRINT @programId
				PRINT @programIds

				IF @name IS NOT NULL
				BEGIN
					SET @variant = 1

					DECLARE @val NVARCHAR(4000)
					DECLARE @result_register_number NVARCHAR(20)
					DECLARE @result_vin NVARCHAR(32)
					DECLARE @isMale INT

					EXEC p_gender @groupProcessInstanceId = @rootId
						,@firstnameAttributePath = '80,342,64'
						,@isMale = @isMale OUTPUT

					insert into @translationParams values(1,dbo.f_conditionalText(@isMale, dbo.f_translate('Pana',default), dbo.f_translate('Pani',default)))
						
					SELECT TOP 1 @result_register_number = value
					FROM vin_element WITH (NOLOCK)
					WHERE header_id IN (
							SELECT headerId
							FROM @tempTable
							)
						AND dictionary_id IN (
							SELECT id
							FROM dbo.vin_dictionary
							WHERE description_org LIKE '%rej%'
								AND is_key = 1
							)
						AND nullif(value, dbo.f_translate('null',default)) IS NOT NULL
					ORDER BY value DESC

					SELECT TOP 1 @result_vin = value
					FROM vin_element WITH (NOLOCK)
					WHERE header_id IN (
							SELECT headerId
							FROM @tempTable
							)
						AND dictionary_id IN (
							SELECT id
							FROM dbo.vin_dictionary
							WHERE description_org LIKE dbo.f_translate('%vin%',default)
								AND is_key = 1
							)
						AND nullif(value, dbo.f_translate('null',default)) IS NOT NULL
					ORDER BY value DESC

					IF @programId IS NOT NULL
					BEGIN
						EXEC [dbo].[p_attribute_edit] @attributePath = '207'
							,@stepId = 'xxx'
							,@groupProcessInstanceId = @groupProcessInstanceId
							,@valueString = @headerIds
							,@err = @err
							,@message = @message

						-- 												Checking If it's vip by the car details
						DECLARE @companyId INT
						DECLARE @VIPText NVARCHAR(500)

						SELECT TOP 1 @VIPText = '<b> '+dbo.f_translate('Klient VIP',default)+' - ' + isnull(company.full_name, '') + ' ' + isnull(position, '')
							,@companyId = vip.company_id
						FROM vip WITH (NOLOCK)
						LEFT JOIN company WITH (NOLOCK) ON vip.company_id = company.id
						WHERE vin = @VIN
							OR reg_number = @register_number

						IF isnull(@VIPText, '') <> ''
						BEGIN
							-- 														ADD TO ATTRIBUTE TEXT
							declare @valueVIP nvarchar(100)
							set @valueVIP=dbo.f_translate('Klient VIP',default)
								
							EXEC [dbo].[p_attribute_edit] @attributePath = '1004'
								,@stepId = 'xxx'
								,@groupProcessInstanceId = @groupProcessInstanceId
								,@valueString = @valueVIP
								,@err = @err
								,@message = @message

							EXEC [dbo].[p_attribute_edit] @attributePath = '1004,63'
								,@stepId = 'xxx'
								,@groupProcessInstanceId = @groupProcessInstanceId
								,@valueText = @VIPText
								,@err = @err
								,@message = @message

							IF isnull(@companyId, 0) IN (
									7913
									,7914
									,7915
									,7916
									,7917
									)
							BEGIN
								-- 														Carfleet
								EXEC dbo.p_send_vip_case_info @groupProcessId = @groupProcessInstanceId
							END
						END

						IF @platformId = 25
						BEGIN
							DECLARE @vipStatus NVARCHAR(100)

							EXEC p_get_vin_headers_by_rootId @root_id = @rootId
								,@columnName = dbo.f_translate('StatusVIP',default)
								,@output = @vipStatus OUTPUT

							EXEC [dbo].[p_attribute_edit] @attributePath = '1004'
								,@stepId = 'xxx'
								,@groupProcessInstanceId = @groupProcessInstanceId
								,@valueString = @vipStatus
								,@err = @err
								,@message = @message

							IF @vipStatus IN (
									dbo.f_translate('VIP special care',default)
									,dbo.f_translate('VIP relationship',default)
									,dbo.f_translate('VIP standard',default)
									)
							BEGIN
								EXEC dbo.p_send_vip_case_info @groupProcessId = @groupProcessInstanceId
							END
						END

						IF @platformId = 81
						BEGIN
							EXEC dbo.p_send_vip_case_info @groupProcessId = @groupProcessInstanceId
						END

						IF @platformId = 14
						BEGIN
							DECLARE @testDate DATETIME = CONVERT(DATETIME, '01.01.2019', 104)

							PRINT dbo.f_translate('INSIDE OPEL',default)

							IF DATEDIFF(day, @testDate, @firstRegDate) > 0
							BEGIN
								PRINT dbo.f_translate('inside x2 ',default)

								IF isnull(@programId, 0) = '457'
								BEGIN
									SET @programId = '591'
									SET @programIds = replace(@programIds, '457', '591')
								END

								IF isnull(@programId, 0) = '454'
								BEGIN
									SET @programId = '590'
									SET @programIds = replace(@programIds, '454', '590')
								END
							END

							UPDATE dbo.vin_header
							SET program_id = @programId
							WHERE id IN (
									SELECT data
									FROM dbo.f_split(@headerIds, ',')
									)
						END

						PRINT '-------------------------------------------------------name2'
						PRINT @programIds

						EXEC [dbo].[p_attribute_edit] @attributePath = '204'
							,@stepId = 'xxx'
							,@groupProcessInstanceId = @groupProcessInstanceId
							,@valueString = @programIds
							,@err = @err
							,@message = @message

						PRINT '-------------------------------------------------------name2'
						PRINT @message

						SET @programIds = cast(@programId AS NVARCHAR(100))

						EXEC [dbo].[p_attribute_edit] @attributePath = '202'
							,@stepId = 'xxx'
							,@groupProcessInstanceId = @groupProcessInstanceId
							,@attributeValueId = @attributeValueId
							,@valueString = @programIds
							,@err = @err
							,@message = @message
					END

					SET @vinAndRegNumberMatch = 0

					DECLARE @mileageText NVARCHAR(300)

					SET @mileageText = ''

					IF NULLIF(@mileageLimit, '') IS NOT NULL
						AND @mileageLimit <> 9999999
					BEGIN
						insert into @translationParams values(2,@mileageLimit)
	
						SET @mileageText = dbo.f_translate(' do przebiegu %2% km (jeżeli po weryfikacji okaże się, że przebieg samochodu jest wyższy, usługi assistance będą dla %1% płatne).',@translationParams)
					END

					insert into @translationParams values(3,@name)
					insert into @translationParams values(4,@start)
					insert into @translationParams values(5,@end)

					SET @val = dbo.f_translate('%1% pojazd jest objęty ochroną assistance w ramach programu %3% w okresie od %4% do %5%', @translationParams) + ' '+@mileageText

					IF @platformId IN (
							79
							,80
							,81
							)
					BEGIN
						--umowa serw
						IF @programId IN (
								594
								,598
								,602
								,607
								,608
								,615
								,618
								,621
								)
						BEGIN
							SET @val = dbo.f_translate('%1%  pojazd objęty jest umową serwisową  od %4% do %5%',@translationParams) + '. ' + @mileageText
						END
						ELSE IF @programId IN (
								594
								,595
								,599
								,603
								,609
								,610
								,616
								,619
								,622
								)
						BEGIN
							--cam
							SET @val = '%1% pojazd objęty jest pakietem assistance po przeglądzie od %4% do %5%' + '. ' + @mileageText
						END
						ELSE
						BEGIN
							SET @val = dbo.f_conditionalText(@isMale, dbo.f_translate('Pana',default), dbo.f_translate('Pani',default)) + dbo.f_translate(' pojazd objęty jest gwarancją umowną  od ',default) + @start + ' do ' + @end + @mileageText
						END
					END

					SET @val = @val + '<BR><table class='dbo.f_translate('table table-bordered',default)'><tr><td>'+dbo.f_translate('Nazwa',default)+'</td><td>'+dbo.f_translate('Od',default)+'</td><td>'+dbo.f_translate('Do',default)+'</td><td>'+dbo.f_translate('Limit',default)+'</td><td>'+dbo.f_translate('Uwagi',default)+'</td></tr>'

					SELECT @val = @val + CONCAT (
							'<tr><td>'
							,aa.NAME
							,'</td><td>'
							,dbo.fn_vdate(aa.startDate)
							,'</td><td>'
							,dbo.fn_vdate(aa.endDate)
							,'</td><td>'
							,isnull(nullif(aa.mileageLimit, 9999999), '-')
							,'</td><td>'
							,isnull(remarks, '')
							,'</td></tr>'
							)
					FROM (
						SELECT DISTINCT p.NAME
							,tt.startDate
							,tt.endDate
							,tt.mileageLimit
							,p.platform_id
							,tt.remarks
						FROM @tempTable tt
						INNER JOIN dbo.vin_program p ON p.id = tt.programId
							AND (
								p.platform_id = isnull(@platformId, 0)
								OR @platformId IN (
									6
									,11
									,31
									,79
									,81
									)
								OR ISNULL(@platformGroup, '') = 'CFM'
								) /* join do platformy w celu wykluczenia cls leaseplan w Fordzie 20181009 MG*/
						) aa
					ORDER BY aa.platform_id DESC

					SET @val = @val + '</table>'

					IF isnull(nullIF(@result_register_number, dbo.f_translate(dbo.f_translate('null',default), DEFAULT)), '') <> ''
					BEGIN
						PRINT '@result_register_number'
						PRINT @result_register_number

						SET @mismatchVal = ''

						IF (
								(
									@result_register_number = ''
									OR ISNULL(@result_register_number, @register_number) = @register_number
									)
								AND @result_vin = @VIN
								)
						BEGIN
							SET @vinAndRegNumberMatch = 1
						END
						ELSE IF @result_vin = @VIN
						BEGIN
							SET @misMatchVal = dbo.f_translate('Prawdopodobnie wprowadzono zły numer rejestracyjny. Odnaleziono uprawnienia dla wskazanego numeru nadwozia, lecz numer rejestracyjny się nie zgadza. Potwierdź numer rejestracyjny z Klientem i upewnij się, że wprowadzono prawidłowo.',default)
						END
						ELSE IF @result_register_number = @register_number
						BEGIN
							SET @misMatchVal = dbo.f_translate('Prawdopodobnie wprowadzono zły numer VIN. Odnaleziono uprawnienia dla wskazanego numeru rejestracyjnego, potwierdź VIN z Klientem i upewnij się czy wprowadzono prawidłowy.',default)
						END

					END
					ELSE
					BEGIN
						SET @vinAndRegNumberMatch = 1
					END

					----------------Concordia, settowanie nr polisy
					--IF @platformId = 18
					--	BEGIN
					--		DECLARE  @headerId int
					--		SELECT top 1 @headerId = cast(data  as int) from f_split(@headerIds, ',')
					--		DECLARE @policyId nvarchar(max)
					--		select @policyId = value  from dbo.vin_element where header_id = @headerId and dictionary_id = 2862
					--		If isnull(@policyId,'') <> ''
					--		BEGIN
					--			EXEC [dbo].[p_attribute_edit]
					--					@attributePath = '440',
					--					@stepId = 'xxx',
					--					@groupProcessInstanceId = @groupProcessInstanceId,
					--					@valueString = @policyId,
					--					@err = @err,
					--					@message = @message
					--		end
					--end
					---- Concordia END
					------ START
					------ Uzupełnienie danych o zakresie ochrony
					DECLARE @policyPeriod VARCHAR(200)

					SET @policyPeriod = dbo.f_translate('od %4% do %5%',@translationParams)

					IF @mileageLimit <> 9999999
					BEGIN
						SET @policyPeriod = @policyPeriod + ', '+ dbo.f_translate('limit przebiegu: %2% km',@translationParams)
					END

					-- CFM
					IF @platformGroup <> 'CFM'
					BEGIN
						EXEC [dbo].[p_attribute_edit] @attributePath = '854'
							,@stepId = 'xxx'
							,@groupProcessInstanceId = @groupProcessInstanceId
							,@valueString = @policyPeriod
							,@err = @err
							,@message = @message
					END

					------ END
					PRINT '@vinAndRegNumberMatch'
					PRINT @vinAndRegNumberMatch

					IF @mismatchVal <> ''
					BEGIN
						EXEC [dbo].[p_attribute_edit] @attributePath = '535'
							,@stepId = 'xxx'
							,@groupProcessInstanceId = @groupProcessInstanceId
							,@valueText = @mismatchVal
							,@err = @err
							,@message = @message
					END

					EXEC [dbo].[p_attribute_edit] @attributePath = '493'
						,@stepId = 'xxx'
						,@groupProcessInstanceId = @groupProcessInstanceId
						,@valueInt = @vinAndRegNumberMatch
						,@err = @err
						,@message = @message

					EXEC [dbo].[p_attribute_edit] @attributePath = '423'
						,@stepId = 'xxx'
						,@groupProcessInstanceId = @groupProcessInstanceId
						,@valueText = @val
						,@err = @err
						,@message = @message

					EXEC [dbo].[p_attribute_edit] @attributePath = '74,105'
						,@stepId = 'xxx'
						,@groupProcessInstanceId = @groupProcessInstanceId
						,@valueDate = @startDate
						,@err = @err
						,@message = @message
				END
				ELSE
				BEGIN
					SET @variant = 2
				END
			END
			ELSE
			BEGIN
				SET @variant = 2
			END
		END

		IF @variant = 2
		BEGIN
			--if @previousProcessId = 523158
			--begin
			--select @VIN,@register_number
			--SELECT  top 1  h.program_id --p.name
			--		,dbo.FN_fdate(h.start_date)
			--		,h.start_date
			--		,dbo.FN_fdate(h.end_date)
			--		,e2.value
			--		,h.id
			--	FROM	dbo.vin_element e1 with(nolock) inner join
			--		dbo.vin_header h with(nolock) on h.id=e1.header_id and h.status=1 left join
			--		dbo.vin_element e2 with(nolock) on h.id=e2.header_id and e2.dictionary_id in (2385)
			--		 inner join
			--			dbo.vin_program p on p.id=h.program_id and (p.platform_id = isnull(@platformId,0) )
			--	where e1.value IN (@VIN,@register_number) and
			--		disable_date is null
			--order by e2.value desc, h.end_date desc, h.id desc
			--end
			-- 						FIX IF null registernumber
			IF isnull(@register_number, '') = ''
			BEGIN
				SET @register_number = dbo.f_translate('loremipsumdolorsitamet', DEFAULT)
			END

			SELECT TOP 1 @programId = h.program_id
				,@start = dbo.FN_fdate(h.start_date)
				,@startDate = h.start_date
				,@end = dbo.FN_fdate(h.end_date)
				,@end = dbo.FN_fdate(h.end_date)
				,@mileageLimit = h.limit
				,@header_id = h.id
			FROM dbo.vin_header h WITH (NOLOCK)
			INNER JOIN dbo.vin_program p WITH (NOLOCK) ON p.id = h.program_id
				AND (p.platform_id = isnull(@platformId, 0))
			WHERE h.STATUS = 1
				--and package_id=1012
				AND (
					search IN (
						@register_number
						,@vin
						)
					OR search2 IN (
						@register_number
						,@vin
						)
					)
			ORDER BY h.limit DESC
				,h.end_date DESC
				,h.id DESC

			IF isnull(@platformGroup, '') = 'CFM'
			BEGIN
				-- 						Usuwanie PSA z wyników dla CFM
				DELETE
				FROM @tempTable
				WHERE headerId IN (
						SELECT headerId
						FROM dbo.vin_header vin
						WHERE vin.id = headerId
							AND vin.package_id IN (
								1047
								,1048
								)
						)
			END

			--SELECT  top 1 @programId = h.program_id --p.name
			--		,@start=dbo.FN_fdate(h.start_date)
			--		,@startDate = h.start_date
			--		,@end=dbo.FN_fdate(h.end_date)
			--		,@mileageLimit=e2.value
			--		,@header_id=h.id
			--	FROM	dbo.vin_element e1 with(nolock) inner join
			--		dbo.vin_header h with(nolock) on h.id=e1.header_id and h.status=1 left join
			--		dbo.vin_element e2 with(nolock) on h.id=e2.header_id and e2.dictionary_id in (2385)
			--		 inner join
			--			dbo.vin_program p on p.id=h.program_id and (p.platform_id = isnull(@platformId,0) )
			--	where e1.value IN (@VIN,@register_number) and
			--		disable_date is null
			--order by e2.value desc, h.end_date desc, h.id desc

			SELECT @name = NAME
			FROM dbo.vin_program WITH (NOLOCK)
			WHERE id = @programId

			
			
			PRINT dbo.f_translate('vin:',default)
			PRINT @VIN
			PRINT dbo.f_translate('nr rej:',default)
			PRINT @register_number
			PRINT '-----'
			PRINT dbo.f_translate('limit2:',default)
			PRINT @mileageLimit

			delete from @translationParams where id IN (2,3,4,5)
			insert into @translationParams values(2,@mileageLimit)
			insert into @translationParams values(3,@name)
			insert into @translationParams values(4,@start)
			insert into @translationParams values(5,@end)

			IF isnull(@name, '') <> ''
			BEGIN
				
				IF @removePrograms = 0
				BEGIN
					IF isnull(@programCam, 0) = 1
					BEGIN
						SET @val = dbo.f_translate('System nie potwierdził uprawnień asssistance dla pojazdu z tym numerem nadwozia.',default)
					END
					ELSE
					BEGIN
						SET @val = dbo.f_translate('System nie potwierdził uprawnień asssistance dla pojazdu z tym numerem nadwozia. Ostatnie zarejestrowane uprawnienie w ramach programu %3%', @translationParams)
						SET @val = @val + isnull(' ' +dbo.f_translate('obowiązywało w okresie od %4% do %5%',@translationParams), '')

						IF @mileageLimit <> ''
							SET @val = @val + isnull(' ('+dbo.f_translate('do przebiegu %2% km',@translationParams)+').', '')
					END
				END
				ELSE
				BEGIN
					SET @val = dbo.f_translate('System nie potwierdził uprawnień asssistance dla pojazdu z tym numerem nadwozia. Uprawnienie w ramach programu %2%', @translationParams)
					SET @val = @val + ' '+dbo.f_translate('nie obowiązuje dla danego typu zdarzenia',default)+'.'

					EXEC [dbo].[p_attribute_edit] @attributePath = '920'
						,@stepId = 'xxx'
						,@groupProcessInstanceId = @groupProcessInstanceId
						,@valueInt = 1
						,@err = @err
						,@message = @message
				END
			END
			ELSE
			BEGIN
				-- SET @val=dbo.f_conditionalText(@isMale2, dbo.f_translate('Pana',default), dbo.f_translate('Pani',default))+dbo.f_translate(' pojazd nie jest objęty ochroną assistance. Nie znaleźliśmy uprawnień w bazie danych.',default)
				SET @val = dbo.f_translate('System nie potwierdził uprawnień asssistance dla pojazdu z tym numerem nadwozia.',default)
			END

			PRINT @val

			DELETE
			FROM @atribute_table

			INSERT @atribute_table
			EXEC dbo.p_attribute_get2 @attributePath = '423'
				,@groupProcessInstanceId = @groupProcessInstanceId

			SELECT @attributeValueId = id
			FROM @atribute_table

			EXEC [dbo].[p_attribute_set2] @attributePath = '423'
				,@stepId = 'xxx'
				,@groupProcessInstanceId = @groupProcessInstanceId
				,@attributeValueId = @attributeValueId
				,@valueText = @val
				,@err = @err
				,@message = @message
				--	select @val
		END

		DECLARE @cid INT
		DECLARE @cvalue NVARCHAR(1000)
		DECLARE @cdescription NVARCHAR(1000)
		DECLARE @cattribute_path VARCHAR(200);

		IF @variant = 99
		BEGIN
			DECLARE kur CURSOR LOCAL
			FOR
			SELECT av.id
				,NULL
				,v.description
				,v.attribute_path
			FROM dbo.vin_dictionary v WITH (NOLOCK)
			INNER JOIN dbo.attribute_value av WITH (NOLOCK) ON av.attribute_path = v.attribute_path
				AND av.group_process_instance_id = @groupProcessInstanceId
				AND v.attribute_path IS NOT NULL
			WHERE v.attribute_path <> '74,72'

			OPEN kur;

			FETCH NEXT
			FROM kur
			INTO @cid
				,@cvalue
				,@cdescription
				,@cattribute_path;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF @cattribute_path = '74,561'
				BEGIN
					EXEC [dbo].[p_attribute_set2] @attributePath = @cattribute_path
						,@stepId = 'xxx'
						,@groupProcessInstanceId = @groupProcessInstanceId
						,@attributeValueId = @cid
						,@valueDate = NULL
						,@err = @err
						,@message = @message
				END
				ELSE
				BEGIN
					EXEC [dbo].[p_attribute_set2] @attributePath = @cattribute_path
						,@stepId = 'xxx'
						,@groupProcessInstanceId = @groupProcessInstanceId
						,@attributeValueId = @cid
						,@valueString = @cvalue
						,@err = @err
						,@message = @message
				END

				FETCH NEXT
				FROM kur
				INTO @cid
					,@cvalue
					,@cdescription
					,@cattribute_path;
			END

			CLOSE kur

			DEALLOCATE kur
		END
		ELSE IF @mileage > 0
		BEGIN
			-- sprawdzenie czy w systemie jest już sprawa dla tego vinu z wyższym przebiegiem
			DECLARE @wrongMileageText NVARCHAR(max)
			DECLARE @wrongMileageHistoryValue INT
			DECLARE @wrongMileageHistoryCaseNumber INT
			DECLARE @wrongMileageHistoryCaseDate DATETIME

			SELECT TOP 1 @wrongMileageHistoryCaseNumber = av.root_process_instance_id
				,@wrongMileageHistoryValue = av.value_int
				,@wrongMileageHistoryCaseDate = av.created_at
			FROM dbo.attribute_value av WITH (NOLOCK)
			WHERE attribute_path = '74,75'
				AND root_process_instance_id <> @rootId
				AND value_int > @mileage
				AND EXISTS (
					SELECT *
					FROM dbo.attribute_value av2 WITH (NOLOCK)
					WHERE attribute_path = '74,71'
						AND value_string = @VIN
						AND av2.parent_attribute_value_id = av.parent_attribute_value_id
					)
			ORDER BY created_at DESC

			IF @wrongMileageHistoryCaseNumber IS NOT NULL
				AND @stepId = '1011.002'
			BEGIN
				SET @wrongMileageText = dbo.f_translate('W sprawie A',default) + RIGHT('000000' + CAST(@wrongMileageHistoryCaseNumber AS NVARCHAR(10)), 8) + dbo.f_translate(' z dnia ',default) + CONVERT(VARCHAR(10), @wrongMileageHistoryCaseDate, 120) + dbo.f_translate(' dla pojazdu o nr VIN ',default) + @VIN + dbo.f_translate(', zarejestrowano przebieg o wartości ',default) + CAST(@wrongMileageHistoryValue AS NVARCHAR(10)) + '  km, który jest wyższy niż zadeklarowany przez klienta (' + CAST(@mileage AS NVARCHAR(10)) + ' km)'

				EXEC [dbo].[p_attribute_edit] @attributePath = '594'
					,@stepId = @stepId
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@valueText = @wrongMileageText
					,@err = @err
					,@message = @message

				SET @variant = 3
			END
		END

		IF @header_id IS NOT NULL
		BEGIN
			DECLARE @vin_table TABLE (
				id INT
				,value NVARCHAR(300)
				,description NVARCHAR(255)
				,attribute_path VARCHAR(200)
				);

			INSERT @vin_table
			SELECT DISTINCT TOP 300 vd.id
				,ve.value
				,vd.description
				,vd.attribute_path
			FROM vin_element ve WITH (NOLOCK)
			INNER JOIN vin_dictionary vd WITH (NOLOCK) ON ve.dictionary_id = vd.id
			WHERE ve.header_id = @header_id
				AND vd.attribute_path IS NOT NULL
			ORDER BY vd.id

			DECLARE @makeModel NVARCHAR(300)
			DECLARE @address NVARCHAR(500) = ''
			DECLARE @makeModelAttrId INT

			SET @makeModel = ''

			DECLARE kur CURSOR LOCAL
			FOR
			SELECT av.id
				,v.value
				,v.description
				,v.attribute_path
			FROM @vin_table v
			INNER JOIN dbo.attribute_value av WITH (NOLOCK) ON av.attribute_path = v.attribute_path
				AND av.root_process_instance_id = @rootId
				AND v.attribute_path IS NOT NULL
			ORDER BY v.id

			OPEN kur;

			FETCH NEXT
			FROM kur
			INTO @cid
				,@cvalue
				,@cdescription
				,@cattribute_path;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF @cattribute_path IN (
						'74,71'
						,'74,72'
						)
				BEGIN
					SET @cvalue = UPPER(@cvalue)
				END

				IF @cattribute_path = '74,76'
					AND @cvalue = '0'
				BEGIN
					SET @cvalue = NULL
				END

				IF @cattribute_path = '74,73'
				BEGIN
					SET @makeModel = @makeModel + @cvalue + ' '
					SET @makeModelAttrId = @cid
				END
				ELSE IF @cattribute_path = '979'
				BEGIN
					SET @address = isnull(@address, '') + iif(isnull(@address, '') = '', isnull(@cvalue, ''), isnull(', ' + @cvalue, ''))
				END
				ELSE IF @cattribute_path = '406,226'
					AND isnull(@cvalue, '') <> ''
				BEGIN
					PRINT '-------------------------- CONSOLE -------------'
					PRINT @cid
					PRINT @cvalue
					PRINT @cdescription
					PRINT @cattribute_path

					---- NOWY SPOSÓB HELP'ów  - 27.05.2019 ----
					-- Musi istnieć:  KomentarzOKliencie = Zasady obs-ugi patrz HELP LFM EIIE
					IF @cvalue LIKE '%EIIE%'
					BEGIN
						DECLARE @companyName NVARCHAR(200)
						DECLARE @helpId INT
						DECLARE @helpText NVARCHAR(MAX)

						DELETE
						FROM @atribute_table

						INSERT @atribute_table
						EXEC dbo.p_attribute_get2 @attributePath = '1067'
							,@groupProcessInstanceId = @groupProcessInstanceId

						SELECT @helpId = value_int
						FROM @atribute_table

						PRINT '---------- @helpId'
						PRINT @helpId

						-- Sprawdzenie, czy już HELP został dodany
						IF ISNULL(@helpId, '') = ''
						BEGIN
							SELECT @companyName = value
							FROM @vin_table
							WHERE attribute_path = '981,438'

							-- Istnieje header właściciela
							IF ISNULL(@companyName, '') <> ''
							BEGIN
								SELECT @helpId = id
									,@helpText = note
								FROM dbo.help_case WITH (NOLOCK)
								WHERE UPPER(NAME) = UPPER(@companyName)

								PRINT '@helpId'
								PRINT @helpId

								IF ISNULL(@helpId, 0) <> 0
								BEGIN
									SET @helpText = '<p>'+dbo.f_translate('SPECJALNE WARUNKI OBSŁUGI [HELP LFM EIIE]',default)+'</p>[more]' + @helpText + '[/more]'

									EXEC [dbo].[p_attribute_edit] @attributePath = '1067'
										,@stepId = 'xxx'
										,@groupProcessInstanceId = @groupProcessInstanceId
										,@valueInt = @helpId
										,@userId = @currentUser
										,@originalUserId = @currentUser
										,@err = @err
										,@message = @message

									EXECUTE dbo.p_note_new @groupProcessId = @groupProcessInstanceId
										,@type = dbo.f_translate('text',default)
										,@content = @helpText
										,@userId = @currentUser
										,@err = @err OUTPUT
										,@message = @message OUTPUT
										,@special = 1
										,@subject = dbo.f_translate('Uwagi zapisane w bazie uprawnień',default)

									EXEC [dbo].[p_attribute_edit] @attributePath = '987'
										,@stepId = 'xxx'
										,@groupProcessInstanceId = @groupProcessInstanceId
										,@valueInt = 1
										,@err = @err
										,@message = @message
								END
							END
						END
					END

					---- STARY SPOSÓB HELP'ów  - 27.05.2019 ----
					--										if @cvalue like '%EIIE%' 
					--										begin
					--											set @cvalue='<A href=''http://10.10.77.93/plikiatlas/HELP%20LEASEPLAN.doc?v=1'' target=_blank>Zasady obsługi patrz HELP LFM EIIE</A>'
					--										end
					--
					--
					IF NOT EXISTS (
							SELECT id
							FROM dbo.attribute_value
							WHERE attribute_path = '406,226,227'
								AND value_text = @cvalue
								AND root_process_instance_id = @rootId
							)
						AND @cvalue NOT LIKE '%EIIE%'
					BEGIN
						EXECUTE dbo.p_note_new @groupProcessId = @groupProcessInstanceId
							,@type = dbo.f_translate('text',default)
							,@content = @cvalue
							,@userId = @currentUser
							,@err = @err OUTPUT
							,@message = @message OUTPUT
							,@special = 1
							,@subject = dbo.f_translate('Uwagi zapisane w bazie uprawnień',default)

						EXEC [dbo].[p_attribute_edit] @attributePath = '987'
							,@stepId = 'xxx'
							,@groupProcessInstanceId = @groupProcessInstanceId
							,@valueInt = 1
							,@err = @err
							,@message = @message
					END
				END
				ELSE
				BEGIN
					-- Wyłom dla atrybutów, które są typu DATE
					IF @cattribute_path IN (
							'74,561'
							,'981,67'
							,'981,68'
							)
					BEGIN
						EXEC [dbo].[p_attribute_edit] @attributePath = @cattribute_path
							,@stepId = 'xxx'
							,@groupProcessInstanceId = @groupProcessInstanceId
							,@attributeValueId = @cid
							,@valueDate = @cvalue
							,@err = @err
							,@message = @message
							--						IF @cattribute_path = '74,561'
							--						BEGIN
							--
							--							DECLARE @firstRegDate datetime
							--
							--							DELETE FROM @atribute_table
							--							INSERT @atribute_table EXEC dbo.p_attribute_get2
							--							@attributePath = '74,233',
							--							@groupProcessInstanceId = @groupProcessInstanceId
							--							select @firstRegDate = value_date from @atribute_table
							--
							--						END
					END
					ELSE IF @cattribute_path IN (
							'74,427'
							,'74,76'
							) -- Rok produkcji, DMC
					BEGIN
						EXEC [dbo].[p_attribute_set2] @attributePath = @cattribute_path
							,@stepId = 'xxx'
							,@groupProcessInstanceId = @groupProcessInstanceId
							,@attributeValueId = @cid
							,@valueInt = @cvalue
							,@err = @err
							,@message = @message
					END
					ELSE --if @variant=99
					BEGIN
						IF @cattribute_path IN ('981,438')
						BEGIN
							SELECT @cvalue = dbo.f_removeSpecialChars(@cvalue)
						END

						IF NOT (
								@cattribute_path = '74,72'
								AND @register_number IS NOT NULL
								)
						BEGIN
							EXEC [dbo].[p_attribute_set2] @attributePath = @cattribute_path
								,@stepId = 'xxx'
								,@groupProcessInstanceId = @groupProcessInstanceId
								,@attributeValueId = @cid
								,@valueString = @cvalue
								,@err = @err
								,@message = @message
						END
					END
				END

				FETCH NEXT
				FROM kur
				INTO @cid
					,@cvalue
					,@cdescription
					,@cattribute_path;
			END

			CLOSE kur

			DEALLOCATE kur

			IF @makeModel <> ''
			BEGIN
				DECLARE @makeModelAtlasId INT

				PRINT '--- @makeModel: ' + CAST(ISNULL(@makeModel, '') AS NVARCHAR(200))

				SELECT @makeModelAtlasId = dbo.f_makeModelStandard(@makeModel, 1)

				IF @makeModelAtlasId IS NOT NULL
				BEGIN
					EXEC [dbo].[p_attribute_set2] @attributePath = @cattribute_path
						,@stepId = 'xxx'
						,@groupProcessInstanceId = @groupProcessInstanceId
						,@attributeValueId = @makeModelAttrId
						,@valueInt = @makeModelAtlasId
						,@err = @err
						,@message = @message
				END
			END

			IF @address <> ''
			BEGIN
				EXEC [dbo].[p_attribute_edit] @attributePath = '979'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@stepId = 'xxx'
					,@userId = 1
					,@originalUserId = 1
					,@valueString = @address
					,@err = @err OUTPUT
					,@message = @message OUTPUT
			END
		END
	END

	/*
     _________________________________________________________________________________________________________

      TYLKO GDY ZNALAZŁO JAKIEŚ UPRAWNIENIA
      Ustawienie odpowiednich uprawnień dla CFM'ów, jeżeli znaleziono polisy i przy ścieżsce standardowej
     _________________________________________________________________________________________________________
    */
	IF @variant = 1
		AND ISNULL(@platformGroup, '') = 'CFM'
		AND ISNULL(@casePath, 1) = 1
	BEGIN
		PRINT '--- SPRAWDZANIE 143 ---'
		PRINT '@programIds'
		PRINT @programIds

		/*	dla ALD uprawnienia assistance/szkody  sprwadzamy po dostepnych programach
          ____________________________________*/
		IF @platformId = 43
		BEGIN
			DECLARE @isClaimHandling TINYINT = 0
			DECLARE @isAssistance TINYINT = 0
			DECLARE @permission TINYINT
			DECLARE @programIdsString NVARCHAR(255)

			DELETE
			FROM @atribute_table

			INSERT @atribute_table
			EXEC dbo.p_attribute_get2 @attributePath = '204'
				,@groupProcessInstanceId = @groupProcessInstanceId

			SELECT @programIdsString = value_string
			FROM @atribute_table

			/*	Sprawdzenie czy jest program ALD_szkody
               ____________________________________*/
			IF dbo.f_exists_in_split('581', @programIdsString) = 1
			BEGIN
				SET @isClaimHandling = 1
			END

			/*	Jaki kolwiek program z ALD
               ____________________________________*/
			IF dbo.f_exists_in_split('534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550', @programIdsString) = 1
			BEGIN
				SET @isAssistance = 1
			END

			PRINT '@programIds'
			PRINT @programIds
			PRINT '@isClaimHandling'
			PRINT @isClaimHandling
			PRINT '@isAssistance'
			PRINT @isAssistance

			IF @isAssistance = 1
				AND @isClaimHandling = 1
			BEGIN
				SET @permission = 3
			END
			ELSE IF @isAssistance = 1
			BEGIN
				SET @permission = 1
			END
			ELSE IF @isClaimHandling = 1
			BEGIN
				SET @permission = 2
			END
			ELSE
			BEGIN
				SET @permission = 4
			END

			EXEC p_attribute_edit @attributePath = '143'
				,@groupProcessInstanceId = @rootId
				,@stepId = 'xxx'
				,@valueInt = @permission
				,@err = @err OUTPUT
				,@message = @message OUTPUT
		END
		ELSE IF @platformId = 48
		BEGIN
			/*	na 99% jak znajdzie uprawnienia to dla BL ma uprawnienia do szkody i assistance (By Dylo, po konsultacji z Dudim (01.02.2019)
               ____________________________________*/
			SET @permission = 3

			IF @programId = 561
			BEGIN
				IF @eventType = 2
				BEGIN
					SET @permission = 4
				END
				ELSE
				BEGIN
					SET @permission = 2
				END
			END

			EXEC p_attribute_edit @attributePath = '143'
				,@groupProcessInstanceId = @rootId
				,@stepId = 'xxx'
				,@valueInt = @permission
				,@err = @err OUTPUT
				,@message = @message OUTPUT
		END
		ELSE
		BEGIN
			/*	Dla innych (np. alphabetu), uprawnienia assistance/szkody sprawdzamy po nagłówkach
               ____________________________________*/
			EXEC [dbo].[p_set_cfm_permissions] @rootId = @rootId
		END

		PRINT '@platformId'
		PRINT @platformId

		-- Sprawdzenie, jeżeli Negatywne uprawnienia, to idzie do STOP ŚWIADCZEŃ
		DECLARE @cfmPermission INT
		DECLARE @stopMessage NVARCHAR(500)
		DECLARE @platformName NVARCHAR(100)
		DECLARE @programIdsTemp NVARCHAR(500)
		DECLARE @platformHelpLine NVARCHAR(100)

		SELECT @platformName = NAME
		FROM dbo.platform
		WHERE id = @platformId

		INSERT @atribute_table
		EXEC dbo.p_attribute_get2 @attributePath = '143'
			,@groupProcessInstanceId = @groupProcessInstanceId

		PRINT '143'

		-- 		select  value_int from @atribute_table
		-- 		TUTEJ
		SELECT @cfmPermission = value_int
		FROM @atribute_table

		PRINT '--- @cfmPermission: ' + CAST(ISNULL(@cfmPermission, '') AS NVARCHAR(200))

		-- 4 == dbo.f_translate('Brak uprawnień',default), kieruje do STOP ŚWIADCZEŃ  - GDY WYPADEK / SZKODA
		IF ISNULL(@cfmPermission, 0) = 4
			AND @eventType = 1
		BEGIN
			INSERT INTO @translationParams values (6,ISNULL(@platformName, dbo.f_translate('tej platformie',default)))
			
			SET @stopMessage = dbo.f_translate('Szanowny Panie/Pani, pojazd nie posiada uprawnień Assistance ani zarządzania likwidacją szkód w %6%. Celem uzyskania pomocy na drodze lub zgłoszenia szkody prosimy o kontakt z Opiekunem Floty w Państwa firmie lub ubezpieczycielem.<br><br><small>(Można polecić Klientowi aby sprawdził czy przy dokumentach pojazdu nie ma odcinka/dokumentu z innej ubezpieczalni, jeżeli tak, można zasugerować kontakt z tym ubezpieczycielem.)</small>', @translationParams)

			EXEC p_attribute_edit @attributePath = '554'
				,@groupProcessInstanceId = @groupProcessInstanceId
				,@stepId = 'xxx'
				,@valueText = @stopMessage
				,@err = @err OUTPUT
				,@message = @message OUTPUT

			SET @variant = 97
		END
		ELSE IF ISNULL(@cfmPermission, 0) NOT IN (
				1
				,3
				)
			AND @eventType = 2 -- gdy NIE MA ASSISTANCE W CFM i jest to AWARIA
		BEGIN
			DECLARE @producerProgram INT
			DECLARE @platformProducerName NVARCHAR(100)
			DECLARE @programsTempTable TABLE (programId INT)

			-- Gdy nie ma uprawnień do ASISTANCE, to kierujemy na STOP świadczeń
			INSERT @atribute_table
			EXEC dbo.p_attribute_get2 @attributePath = '204'
				,@groupProcessInstanceId = @groupProcessInstanceId

			SELECT @programIdsTemp = value_string
			FROM @atribute_table

			---
			INSERT INTO @programsTempTable
			SELECT data
			FROM dbo.f_split(@programIdsTemp, ',')

			-- Sprawdzenie czy ma jakiś program producencki  (wtedy inny komunikat)
			SELECT TOP 1 @producerProgram = id
			FROM dbo.vin_program AS vp
			INNER JOIN @programsTempTable AS ptt ON ptt.programId = vp.id
			WHERE is_producer = 1

			--	    	SELECT TOP 1 @producerProgram = id FROM dbo.vin_program WHERE is_producer = 1 AND id IN(@programIdsTemp)
			-- Audi, Skoda, VW, Ford, Seat, Kia, Opel
			IF ISNULL(@producerProgram, '') <> ''
			BEGIN
				SELECT @platformProducerName = p.shortname
					,@platformHelpLine = p.official_line_number
				FROM dbo.platform AS p
				LEFT JOIN dbo.vin_program AS vp ON p.id = vp.platform_id
				WHERE vp.id = @producerProgram

				INSERT INTO @translationParams values (7,ISNULL(@platformName, ''))
				INSERT INTO @translationParams values (8,ISNULL(@platformProducerName,'[marka]'))
				INSERT INTO @translationParams values (9,ISNULL(@platformHelpLine, ''))
				
				SET @stopMessage = dbo.f_translate('Dla tego pojazdu nie został wykupiony pakiet %7% assistance. Prosimy o kontakt z infolinią %8% Assistance.<br/><br/>Numer infolini: %9%',@translationParams);
			END
			ELSE
			BEGIN
				SET @stopMessage = dbo.f_translate('Dla tego pojazdu nie został wykupiony pakiet Alphabet assistance. W przypadku pytań lub wątpliwości, prosimy o kontakt z opiekunem floty.',default)
			END

			EXEC p_attribute_edit @attributePath = '423'
				,@groupProcessInstanceId = @groupProcessInstanceId
				,@stepId = 'xxx'
				,@valueText = @stopMessage
				,@err = @err OUTPUT
				,@message = @message OUTPUT

			SET @variant = 2
		END
	END

	UPDATE dbo.attribute_value
	SET value_string = upper(value_string)
	WHERE root_process_instance_id = @rootId
		AND attribute_id IN (
			71
			,72
			)

	-----------------------------------------------
	---- SPRAWDZENIE UPRAWNIEŃ PODUCENCKICH dbo.f_translate('NOWYCH',default)
	-----------------------------------------------
	DECLARE @makeModelPlatformId NVARCHAR(100)
	DECLARE @makeModelId INT

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '74,561'
		,@groupProcessInstanceId = @groupProcessInstanceId

	SELECT @firstRegDate = value_date
	FROM @atribute_table

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '204'
		,@groupProcessInstanceId = @groupProcessInstanceId

	SELECT @programIds = value_string
	FROM @atribute_table

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '74,73'
		,@groupProcessInstanceId = @groupProcessInstanceId

	SELECT @makeModelId = value_int
	FROM @atribute_table

	PRINT '@makeModelId'
	PRINT @makeModelId

	IF @platformGroup = 'CFM'
		AND @firstRegDate IS NOT NULL
		AND @programIds IS NOT NULL
	BEGIN
		EXEC [dbo].[p_attribute_edit] @attributePath = '74,233'
			,@groupProcessInstanceId = @groupProcessInstanceId
			,@stepId = 'xxx'
			,@userId = 1
			,@originalUserId = 1
			,@valueDate = @firstRegDate
			,@err = @err OUTPUT
			,@message = @message OUTPUT

		PRINT '@programIds'
		PRINT @programIds

		IF dbo.f_exists_in_split('429,416,461,417,419,454,414,448,449', @programIds) = 0
		BEGIN
			DECLARE @daysOld INT
			DECLARE @askSupervisor INT = 0
			DECLARE @processInstanceId INT

			SET @daysOld = DATEDIFF(day, @firstRegDate, getdate())

			PRINT '@makeModelId'
			PRINT @makeModelId

			SELECT TOP 1 @makeModelPlatformId = argument1
			FROM dbo.dictionary
			WHERE typeD = 'makeModel'
				AND value = @makeModelId

			PRINT '@makeModelPlatformId'
			PRINT @makeModelPlatformId

			DECLARE @makeModelPlatformIDs TABLE (id INT)

			INSERT INTO @makeModelPlatformIDs
			SELECT data
			FROM dbo.f_split(@makeModelPlatformId, ',')

			IF (
					@daysOld <= 730
					AND (
						SELECT TOP 1 id
						FROM @makeModelPlatformIDs
						WHERE id IN (
								2
								,6
								,10
								,11
								,14
								,31
								)
						) IS NOT NULL
					)
				OR (
					(
						SELECT 1
						FROM @makeModelPlatformIDs
						WHERE id = 32
						) IS NOT NULL
					AND @daysOld <= 1460
					)
				-- 							or ((select count(*)from @makeModelPlatformIDs ) = 0  and @daysOld <=730)
			BEGIN
				SET @askSupervisor = 1
			END

			PRINT '@askSupervisor'
			PRINT @askSupervisor

			IF @askSupervisor = 1
			BEGIN
				EXEC dbo.p_process_new @stepId = '1011.072'
					,@userId = @currentUser
					,@originalUserId = @currentUser
					,@rootId = @rootId
					,@groupProcessId = @groupProcessInstanceId
					,@err = @err
					,@message = @message
					,@processInstanceId = @processInstanceId OUT
			END
		END
	END

	IF @programId = 561
		AND @eventType = 2
	BEGIN
		SET @variant = 91

		SET @stopMessage = dbo.f_translate('UWAGA! BRAK ASSISTANCE – PO POMOC KIEROWAĆ do AXA +48 22 575 97 75',default)
		
		EXEC p_attribute_edit @attributePath = '554'
			,@groupProcessInstanceId = @groupProcessInstanceId
			,@stepId = 'xxx'
			,@valueText = @stopMessage
			,@err = @err OUTPUT
			,@message = @message OUTPUT
	END

	DELETE
	FROM @atribute_table

	INSERT @atribute_table
	EXEC dbo.p_attribute_get2 @attributePath = '74,270'
		,@groupProcessInstanceId = @groupProcessInstanceId

	SELECT @countryFirstSold = value_string
	FROM @atribute_table

	IF @platformId IN (
			79
			,80
			,81
			,82
			)
		AND @variant <> 99
	BEGIN
		IF isnull(@countryFirstSold, 'PL') <> 'PL'
			AND @programIds IS NOT NULL
		BEGIN
			SET @variant = 96

			SET @stopMessage = dbo.f_translate('Nie świadczymy bezpłatnej pomocy assistance dla pojazdów, których pierwsza sprzedaż nastąpiła poza granicami Polski. W związku z tym mogę Panu/Pani zaproponować pomoc odpłatną. ',default)
			
			EXEC p_attribute_edit @attributePath = '532'
				,@groupProcessInstanceId = @groupProcessInstanceId
				,@stepId = 'xxx'
				,@valueText = @stopMessage
				,@err = @err OUTPUT
				,@message = @message OUTPUT
		END
		ELSE IF @programIds IN (
				'595'
				,'599'
				,'603'
				,'619'
				)
		BEGIN
			SET @variant = 96

			IF @programIds = '619'
			BEGIN
				SET @stopMessage = dbo.f_translate('Szanowni Państwo, Program Peugeot Assistance dla pojazdów po zakończonej gwarancji umownej i nie objętych umowami serwisowymi jest w trakcie modyfikacji. Niebawem zaproponujemy Państwu pełną ofertę ochrony assistance producenta. Do tego czasu zapewniamy Państwu usługi assistance na zasadach odpłatnych. W razie wątpliwości prosimy o kontakt z dealerem gdzie był wykonywany ostatni przegląd.',default)
				EXEC p_attribute_edit @attributePath = '532'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@stepId = 'xxx'
					,@valueText = @stopMessage
					,@err = @err OUTPUT
					,@message = @message OUTPUT
			END
			ELSE
			BEGIN
				SET @stopMessage = dbo.f_translate('Szanowni Państwo, Państwa pojazd przeszedł przegląd przed 01.01.2019 i nie posiada pakietu assistance. Pakiet assistance dołączony jest do pojazdów, które przeszły przegląd po 01.01.2019 roku . W razie wątpliwości prosimy o kontakt z dealerem gdzie był wykonywany ostatni przegląd.',default)
				EXEC p_attribute_edit @attributePath = '532'
					,@groupProcessInstanceId = @groupProcessInstanceId
					,@stepId = 'xxx'
					,@valueText = @stopMessage
					,@err = @err OUTPUT
					,@message = @message OUTPUT
			END
		END

		IF @inMondialDB = 1
		BEGIN
			SET @variant = 7
		END
	END

	-- zmiana platformy citroen na ds i vice versa
	IF @platformId = 79
		AND EXISTS (
			SELECT id
			FROM vin_program
			WHERE platform_id = 81
				AND id = @programId
			)
		OR @platformId = 81
		AND EXISTS (
			SELECT id
			FROM vin_program
			WHERE platform_id = 79
				AND id = @programId
			)
	BEGIN
		SET @platformId = IIF(@platformId = 79, 81, 79)

		EXEC [dbo].[p_attribute_edit] @attributePath = '253'
			,@groupProcessInstanceId = @groupProcessInstanceId
			,@stepId = 'xxx'
			,@userId = 1
			,@originalUserId = 1
			,@valueInt = @platformId
			,@err = @err OUTPUT
			,@message = @message OUTPUT
	END

	/*	Dodanie HELP'a do sprawy. Szczegóły w procedurze. 
  		____________________________________*/
	IF @platformId IS NOT NULL
		AND @platformId IS NOT NULL
	BEGIN
		EXEC [dbo].[p_add_help_if_not_exists] @groupProcessInstanceId = @groupProcessInstanceId
			,@programId = @programId
			,@platformId = @platformId
	END

	PRINT '-------------------------- END [dbo].[s_1011_002]'
END