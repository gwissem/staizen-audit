
ALTER PROCEDURE [dok].[P_Accept]
@id int,
@userId int,
@accept tinyint,
@answer nvarchar(4000)
AS

	declare @email nvarchar(1000)
	declare @subject nvarchar(1000)
	declare @caseNumber nvarchar(50)
	declare @cNumber nvarchar(50)
	declare @body nvarchar(1000)
	declare @complaintOwner int

	declare @eType int
	declare @complaintId int
	declare @note nvarchar(1000)

	select	@eType=ce.eType,
			@complaintId=complaintId,
			@note=eDescription
	from    dok.complainsEvents ce with(nolock)
	where   ce.id=@id

	select @complaintOwner=userId
	from dok.complains with(nolock)
	where id=@complaintId

	select @email=email
	from dbo.fos_user with(nolock)
	where id=@complaintOwner

	select @caseNumber=caseNumber,
			@cNumber=cNumber
	from	dok.complains with(nolock)
	where	id=@complaintId

	if @eType=50
	begin
		set @subject=dbo.f_translate('Odpowiedź na zapytanie w sprawie reklamacji ',default)+@cNumber+dbo.f_translate(' do sprawy ',default)+@caseNumber
	end
	
	if @eType=46 and @accept=1
	begin
		set @subject=dbo.f_translate('Akceptacja w sprawie reklamacji ',default)+@cNumber+dbo.f_translate(' do sprawy ',default)+@caseNumber
	end

	if  @eType=46 and @accept=0
	begin
		set @subject=dbo.f_translate('Odrzucenie w sprawie reklamacji ',default)+@cNumber+dbo.f_translate(' do sprawy ',default)+@caseNumber
		print @subject
	end

	set @body='<BR>Zapytanie:<BR><BR>'+@note+'<BR><BR><HR><BR>Odpowiedź:<BR><BR>'+@answer

	if @email is not null
	begin
		EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('DOK',default)
		,                            @recipients  = @email
		,                            @subject     = @subject
		,                            @body        = @body
		,	                         @body_format = dbo.f_translate('HTML',default)	
	end

	if @eType=46 and @accept=1
	begin
		print dbo.f_translate('Wariant akceptuję',default)
		UPDATE dok.complainsEvents
		   SET eAcceptanceDate = getdate()
			  ,eUserIdA = @userId
			  ,eAcceptance=@accept
			  ,sendTo=null
			  ,eType=40
			  ,eAnswer=@answer
			  ,updatedBy=@userId
		 WHERE id=@id and sendTo=@userId
	end

	if @eType=46 and @accept=0
	begin
		print dbo.f_translate('Wariant odrzucam',default)
		UPDATE dok.complainsEvents
		   SET eAcceptanceDate = getdate()
			  ,eUserIdA = @userId
			  ,eAcceptance=@accept
			  ,sendTo=null
			  ,eType=45
			  ,eAnswer=@answer
			  ,updatedBy=@userId
		 WHERE id=@id and sendTo=@userId
	end


	if @eType=50
	begin
		UPDATE dok.complainsEvents
		   SET eAcceptanceDate = getdate()
			  ,eUserIdA = @userId
			  ,eAcceptance=@accept
			  ,sendTo=null
			  ,eType=51
			  ,eAnswer=@answer
			  ,updatedBy=@userId
		 WHERE id=@id and sendTo=@userId
	end


