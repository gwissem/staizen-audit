ALTER PROCEDURE [dbo].[s_1011_069]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @groupProcessInstanceId INT
	DECLARE @vin NVARCHAR(255)
	DECLARE @sameDiagnosisStepCount INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	declare @header nvarchar(4000)
	declare @footer nvarchar(4000)
	declare @body nvarchar(4000)
	declare @email nvarchar(200)
	declare @senderEmail nvarchar(200) = [dbo].[f_getEmail]( dbo.f_translate('cfm',default) )
	declare @subject nvarchar(255)
	declare @regNumber nvarchar(255)
	DECLARE @platformId int
	declare @programId int 
	DECLARE @rootId int
	DECLARE @platformGroup nvarchar(255) 
	declare @emailAlreadySent int
	declare @decision int
	declare @callerPhone nvarchar(100)
	declare @platformName nvarchar(100)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	SELECT @groupProcessInstanceId = group_process_id
	FROM process_instance with(nolock)
	WHERE id = @previousProcessId 
	
	exec dbo.p_cfm_partner_contact @groupProcessInstanceId = @groupProcessInstanceId, @contact = @email output
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '59,61', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @decision = value_int FROM @values
	
	delete from @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
	
	delete from @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	SELECT @platformName = name FROM platform where id = @platformId
	
	SET @variant = 99
	
	IF @decision = 0 
	BEGIN
		SET @variant = 97
	
		declare @content nvarchar(4000)
		set @content = dbo.f_translate('Szanowni Panstwo, dotyczy pojazdu ',default)+isnull(@regNumber,'')+dbo.f_translate('. Dzial Techniczny nie wyrazil zgody na pomoc assistance.',default)
		
  		EXEC dbo.p_note_new
  		@groupProcessId = @groupProcessInstanceId,
  		@type = dbo.f_translate('sms',default),
  		@content = @content,
  		@phoneNumber = @callerPhone,
  		@err = @err OUTPUT,
  		@message = @message OUTPUT
  		
		return
	END 
	else if @decision = 1
	BEGIN
		set @variant = 1
		return
	END 
	
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vin = value_string FROM @values
			
	exec [dbo].[p_platform_group_name]
	@groupProcessInstanceId = @groupProcessInstanceId,
	@name = @platformGroup OUTPUT

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '59,65', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @emailAlreadySent = value_int FROM @values
	
	delete from @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '59,737', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @body = value_text FROM @values
	
	delete from @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '59,113', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @header = value_text FROM @values
	
	delete from @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '59,114', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @footer = value_text FROM @values
	
	
	set @subject = @regNumber + ' / ' + dbo.f_caseId(@groupProcessInstanceId)
	set @body = isnull(@header+'<br/><br/>','')+isnull(@body+'<br/><br/>','')+isnull('<hr/>'+@footer+'<br/>','')

	if isnull(@emailAlreadySent,0) = 0  
	BEGIN
		EXECUTE dbo.p_note_new 
	     @groupProcessId = @groupProcessInstanceId
	    ,@type = dbo.f_translate('email',default)
	    ,@content = @body
	    ,@email = @email
	    ,@userId = 1  -- automat
	    ,@subject = @subject
	    ,@direction=1
	    ,@dw = ''
	    ,@udw = ''
	    ,@sender = @senderEmail
--	    ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'  -- {SOURCE::ID|FILENAME::NAME::WITH_PARSE}
	    ,@emailBody = @body
	    ,@emailRegards = 1
	    ,@err=@err OUTPUT
	    ,@message=@message OUTPUT	
	    
	    EXEC [dbo].[p_attribute_edit]
	       @attributePath = '59,65', 
	       @groupProcessInstanceId = @groupProcessInstanceId,
	       @stepId = 'xxx',
	       @userId = 1,
	       @originalUserId = 1,
	       @valueInt = 1,
	       @err = @err OUTPUT,
	       @message = @message OUTPUT
	END 
	
	IF @decision = -1
	BEGIN
		update dbo.process_instance set postpone_count = isnull(postpone_count,0)+1, postpone_date = dateadd(hour, 1, getdate()) where id = @previousProcessId 
	END 
	
END

