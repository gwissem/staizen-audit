
ALTER PROCEDURE [dbo].[s_1016_012]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @previousStepId NVARCHAR(255)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @stepId NVARCHAR(255)
	DECLARE @endLocationChoice INT
	DECLARE @eventLocationId INT
	DECLARE @taxiEndLocationId INT
	DECLARE @rootId INT
	DECLARE @isOrderPZ INT
	declare @programId INT 
	
	SET @variant = 1
	
	SELECT @groupProcessId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance WITH(NOLOCK)
	where id = @previousProcessId

	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '727', @groupProcessInstanceId = @groupProcessId
	SELECT @isOrderPZ = value_int FROM @values
	
	DELETE FROM @values 
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessId
	SELECT @programId = value_string FROM @values
	
	IF @programId IN (609,610,616,619,622,629,630)
	BEGIN
		SET @variant = 2
		RETURN
	END 
	
	IF ISNULL(@isOrderPZ, 0) = 1
	BEGIN
		
		EXEC p_process_jump
		@previousProcessId = @previousProcessId,
		@nextStepId = '1011.010',
		@doNext = 0,
		@parentId = NULL,
		@rootId = @rootId,
		@groupId = @rootId,
		@variant = 2,
	    @err = @err output,
	    @message = @message output
		
	END
	ELSE
	BEGIN
		declare @platformId int

		INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '253',
			@groupProcessInstanceId = @groupProcessId
		SELECT @platformId = value_int FROM @values
	
		if @platformId in (6,11,31)
		begin
			DECLARE @estimated DECIMAL(10,2)
			DECLARE @limit DECIMAL(10,2)
			SET @limit = 80
		
			if exists(
			select	id
			from	dbo.attribute_value 
			where	attribute_path='152,796' and 
					group_process_instance_id=@groupProcessId and
					value_int=1
			)
			begin
				set @limit=500
			end

			declare @usedLimit decimal(10,2)
			select	@usedLimit=sum(isnull(value_int,0.0))
			from	dbo.attribute_value 
			where	attribute_path='152,319' and 
					root_process_instance_id=@rootId and group_process_instance_id not in (
				
					select	group_process_instance_id 
					from	dbo.attribute_value 
					where	attribute_path='152,796' and 
							root_process_instance_id=@rootId and
							value_int=1
					UNION
					select	pin.group_process_id
					from	dbo.process_instance pin inner join
							dbo.service_status ssu on pin.group_process_id=ssu.group_process_id inner join 
							dbo.service_status_dictionary ssd on ssd.id=ssu.status_dictionary_id
					where	pin.root_id=@rootId and
							ssd.progress=5 and
							pin.step_id like '1016%'
					)

			declare @usedLimitTowing decimal(10,2)
			select	@usedLimitTowing=sum(avp.value_int * avkm.value_int * 1.5)
			from	dbo.attribute_value avp inner join
					dbo.attribute_value avkm on avp.group_process_instance_id=avkm.group_process_instance_id and avp.root_process_instance_id=avkm.root_process_instance_id and avkm.attribute_path='638,645,647'
			where   avp.attribute_path = '638,645,154' and avp.root_process_instance_id = @rootid

			if @usedLimitTowing is null set @usedLimitTowing=0.0
			set @usedLimit=@usedLimit+@usedLimitTowing

			DECLARE @exchangeCourse decimal(18,4)
			SET @exchangeCourse = dbo.FN_Exchange2(dbo.f_translate('EUR',default), GETDATE())
		
			set @usedLimit=@usedLimit/@exchangeCourse
			set @limit=@limit-@usedLimit
		
			EXEC p_transport_costs @previousProcessId = @previousProcessId,  @currency = dbo.f_translate('EUR',default), @estimated = @estimated OUTPUT, @limit = @limit OUTPUT
	
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '152,249', 
			@groupProcessInstanceId = @groupProcessId,
			@stepId = @stepId,
			@valueDecimal = @limit,
			@err = @err OUTPUT,
			@message = @message OUTPUT
	
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '152,549', 
			@groupProcessInstanceId = @groupProcessId,
			@stepId = @stepId,
			@valueDecimal = @estimated,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		end
		else
		begin
			EXEC [dbo].[p_attribute_edit]
			@attributePath = '152,796', 
			@groupProcessInstanceId = @groupProcessId,
			@stepId = @stepId,
			@valueInt = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT

		end
	END 
	 
END

