ALTER PROCEDURE [dbo].[p_sparx_incoming2]
@sparx_id int,
@processInstanceId int output
AS
begin
	SET NOCOUNT ON
	DECLARE @json NVARCHAR(MAX)
	declare @eventType nvarchar(100)
	declare @groupProcessInstanceId int
	DECLARE @err int
	DECLARE @message nvarchar(255)
	DECLARE @processInstanceIds varchar(4000)
	DECLARE @note nvarchar(max)=''
	declare @rootId INT 
	
	SELECT	@json=json, @eventType = event_type, @groupProcessInstanceId = group_process_id
	FROM	dbo.sparx_incoming
	where	id=@sparx_id

	set @rootId = @groupProcessInstanceId
	
	if @eventType = dbo.f_translate('cancel',default) and not exists (select id from dbo.process_instance where group_process_id = @groupProcessInstanceId and step_id = '1011.053' and active = 1)
	BEGIN
		-- create cancel services info step
		EXECUTE dbo.p_process_new
		   @stepId='1011.053'
		  ,@userId=1
		  ,@groupProcessId = @groupProcessInstanceId
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT
		  ,@processInstanceId=@processInstanceId OUTPUT
		  
	    DECLARE @servicesIds NVARCHAR(255)

		EXEC [dbo].[p_running_services]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@servicesIds = @servicesIds OUTPUT
		
		IF ISNULL(@servicesIds,'') = ''
		BEGIN	
		   EXEC [dbo].[p_process_next]
	       @previousProcessId = @processInstanceId,
	       @err = @err OUTPUT,
	       @message = @message OUTPUT,
	       @processInstanceIds = @processInstanceIds OUTPUT
		END 
	    
		return 
		  
	END 
	
	declare @sql nvarchar(max)
	declare @id int
	declare @json_key nvarchar(200)
	declare @json_type nvarchar(100)
	declare @attribute_path nvarchar(500)

	-- create process
	if @eventType = dbo.f_translate('create',default)
	begin
		
		-- send back ref case id
		EXECUTE dbo.p_process_new
		   @stepId='1011.50'
		  ,@userId=1
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT
		  ,@processInstanceId=@processInstanceId OUTPUT
	
		  EXECUTE dbo.p_form_controls 
		   @instance_id=@processInstanceId
		  ,@returnResults=0
		  ,@returnWithNote=0

	end

	select @groupProcessInstanceId=group_process_id, @rootId = group_process_id
	from dbo.process_instance 
	where id=@processInstanceId
	
	declare @attributeValueId int
	set  @attributeValueId=null

	EXECUTE dbo.p_attribute_edit 
		@attributePath='713'
		,@groupProcessInstanceId=@groupProcessInstanceId
		,@stepId='xxx'
		,@userId=1
		,@attributeValueId=@attributeValueId OUTPUT
		,@valueint=0
		,@err=@err OUTPUT
		,@message=@message OUTPUT
	
--	EXEC p_attribute_edit
--		@attributePath = '101,102' , 
--		@groupProcessInstanceId = @groupProcessInstanceId,
--		@stepId = 'xxx',
--		@valueInt = 3,
--		@err = @err OUTPUT,
--		@message = @message OUTPUT	

	IF OBJECT_ID('tempdb..#serviceTypeTempTable') IS NOT NULL 
	begin 
		drop table #serviceTypeTempTable
		drop table #sparxAttributeValueTempTable
		drop table #sparxAttributeValueChangeTempTable
	end 
	
	create table #serviceTypeTempTable ( name nvarchar(255) )
	create table #sparxAttributeValueTempTable ( attribute_path nvarchar(255), value_int int, value_string nvarchar(255), value_text nvarchar(4000), value_decimal decimal(10,5), value_date datetime)
	create table #sparxAttributeValueChangeTempTable (name nvarchar(255), old_value nvarchar(255), new_value nvarchar(255))	
	
	exec dbo.p_json_recurency @json=@json, @rootId = @rootId, @event = @eventType, @note=@note OUTPUT, @groupProcessInstanceId=@groupProcessInstanceId output

	set @groupProcessInstanceId = @rootId 
	
	IF EXISTS(select * from #sparxAttributeValueChangeTempTable)
	BEGIN
	 
	  declare @attrName nvarchar(255)
	  declare @attrOldValue nvarchar(255)
	  declare @attrNewValue nvarchar(255)
  	  declare @changedAttributes nvarchar(max)
  	  
	  declare kur cursor LOCAL for
	 	SELECT name, old_value, new_value FROM #sparxAttributeValueChangeTempTable 
	  OPEN kur;
	  FETCH NEXT FROM kur INTO @attrName, @attrOldValue, @attrNewValue;
	  WHILE @@FETCH_STATUS=0
	  BEGIN
	  	
	  	set @changedAttributes=isnull(@changedAttributes,'')+'<tr><td>'+@attrName+'</td><td>'+ISNULL(CAST(@attrOldValue AS NVARCHAR(255)),'')+'</td><td>'+ISNULL(CAST(@attrNewValue AS NVARCHAR(255)),'')+'</td></tr>'
	  	
	  FETCH NEXT FROM kur INTO @attrName, @attrOldValue, @attrNewValue;
	  END
	  CLOSE kur
	  DEALLOCATE kur
	  
	  SET @changedAttributes = '<p><b>Zmieniono atrybuty w sprawie:</b></p><table class="table table-bordered table-responsive"><thead><td>Atrybut</td><td>Poprzednia wartość</td><td>Nowa wartość</td></thead><tbody>'+@changedAttributes+'</tbody></table>'
	  SET @note = ISNULL('<p><b>Informacje o sprawie/usługach:</b></p>'+@note,'') + @changedAttributes
	  
	END 
	
	declare @valueInt int 
	declare @valueString nvarchar(255)
	declare @valueDecimal decimal(10,5)
	declare @valueText nvarchar(4000)
	declare @valueDate datetime 
	declare @attributePath nvarchar(255)
	 
	 declare kur cursor LOCAL for
	 	SELECT attribute_path, value_int, value_string, value_text, value_decimal, value_date	    
	    FROM #sparxAttributeValueTempTable	    
	 OPEN kur;
	 FETCH NEXT FROM kur INTO @attributePath, @valueInt, @valueString, @valueText, @valueDecimal, @valueDate;
	 WHILE @@FETCH_STATUS=0
	 BEGIN
	 	
	 	EXEC [dbo].[p_attribute_edit]
	     @attributePath = @attributePath, 
	     @groupProcessInstanceId = @groupProcessInstanceId,
	     @stepId = 'xxx',
	     @userId = 1,
	     @originalUserId = 1,
	     @valueInt = @valueInt,
	     @valueString = @valueString,
	     @valueText = @valueText,
	     @valueDecimal = @valueDecimal,
	     @valueDate = @valueDate,
	     @err = @err OUTPUT,
	     @message = @message OUTPUT
	 	
	 FETCH NEXT FROM kur INTO @attributePath, @valueInt, @valueString, @valueText, @valueDecimal, @valueDate;
	 END
	 CLOSE kur
	 DEALLOCATE kur
	
	drop table #serviceTypeTempTable
	drop table #sparxAttributeValueTempTable
	
	declare @jsonNote nvarchar(max)
	
	SET @note = dbo.f_translate('Data aktualizacji: ',default)+dbo.FN_VDateHour(getdate())+'<br/>'+@note
	
	if isnull(@note,'') <> '' and @eventType <> dbo.f_translate('create',default)	
	BEGIN
		
		EXEC [dbo].[p_attribute_edit]
	     @attributePath = 980, 
	     @groupProcessInstanceId = @groupProcessInstanceId,
	     @stepId = 'xxx',
	     @userId = 1,
	     @originalUserId = 1,
	     @valueText = @note,
	     @err = @err OUTPUT,
	     @message = @message OUTPUT
	     
	     IF NOT EXISTS( SELECT id from dbo.process_instance where group_process_id = @groupProcessInstanceId and active = 1 and step_id = '1011.058')
	     BEGIN
		     EXECUTE dbo.p_process_new
			   @stepId='1011.058'
			  ,@userId=1
			  ,@groupProcessId = @groupProcessInstanceId
			  ,@err=@err OUTPUT
			  ,@message=@message OUTPUT
			  ,@processInstanceId=@processInstanceId OUTPUT
			  
			  UPDATE dbo.process_instance SET priority = 1 where id = @processInstanceId 
	     END
	END 
	
	set @jsonNote = '[more]'+@json+'[/more]'
	set @note = isnull(@note,'') + isnull(@jsonNote,'')
	
	if isnull(@note,'')<>''
	begin
		
		EXEC dbo.p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('system',default),
		@content = @note,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	end

	DECLARE @IntVariable int;  
	DECLARE @SQLString nvarchar(500);  
	DECLARE @ParmDefinition nvarchar(500);  

	DECLARE @make NVARCHAR(100)
	DECLARE @model NVARCHAR(100)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,766', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @make = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,576', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @model = value_string FROM @values
	
	DECLARE @makeModel NVARCHAR(300)
	SELECT @makeModel = @make + ' ' + @model
	
	IF ISNULL(@makeModel,'') <> ''
	BEGIN		
		DECLARE @makeModelId INT
		SELECT @makeModelId = dbo.f_makeModelStandard(@makeModel,1)
		
		EXEC p_attribute_edit
		@attributePath = '74,73' , 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = @makeModelId,
		@err = @err OUTPUT,
		@message = @message OUTPUT		
	END 
	
	IF @eventType = dbo.f_translate('create',default)
	BEGIN
		UPDATE dbo.sparx_incoming set group_process_id = @groupProcessInstanceId where id = @sparx_id
		
		EXEC [dbo].[p_process_next]
	     @previousProcessId = @processInstanceId,
	     @err = @err OUTPUT,
	     @message = @message OUTPUT
	END 
end

