ALTER PROCEDURE [dbo].[s_1007_079]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output, @token nvarchar(50) = null output
)
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @partnerId INT
	DECLARE @caseNumber NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT 
	DECLARE @partnerEmail NVARCHAR(255)
	DECLARE @automatId INT
	DECLARE @processInstanceIds nvarchar(1000)
	DECLARE @rzwId int 
	DECLARE @automatGroupId INT 
	DECLARE @automatRootId INT 
	DECLARE @rzwGroupId INT 
	DECLARE @firstname nvarchar(255)
	DECLARE @lastname nvarchar(255)
	DECLARE @rentalGroup INT 
	DECLARE @parentId INT 

	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	SELECT @groupProcessInstanceId = group_process_id, @token = token, @rootId = root_id, @parentId = parent_id from  dbo.process_instance with(nolock) where id = @previousProcessId
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '326,64', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @firstname = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '326,66', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @lastname = value_string FROM @values
	
	SELECT top 1 @automatId = pin.id, @automatGroupId = pin.group_process_id, @automatRootId = pin.root_id
	FROM dbo.process_instance pin with(nolock)
	LEFT JOIN dbo.attribute_value av with(nolock) on av.group_process_instance_id = pin.group_process_id and av.attribute_path = '168,839'		
	where pin.active = 1 and pin.step_id IN ('1007.053','1007.074') and isnull(av.value_int,av.group_process_instance_id) = @groupProcessInstanceId and pin.root_id = @rootId 

	
	IF @automatId is null 
	BEGIN
		SET @variant = 99
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '326,63', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueText = dbo.f_translate('Nie znaleziono trwającego wynajmu pojazdu zastępczego dla poniższych danych',default),
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
	END 
	ELSE
	BEGIN
		
		SET @variant = 99    
	
		EXEC [dbo].[p_process_next]
	      @previousProcessId = @automatId,
	      @err = @err OUTPUT,
	      @message = @message OUTPUT,
	      @variant = 10,
	      @processInstanceIds = @processInstanceIds OUTPUT,
	      @token = @token output
		  
	    DECLARE @smsContent NVARCHAR(1000)
        SET @smsContent = 'Wypożyczalnia zamknęła wynajem i wygenrowała RZW (dla usługi: '+CAST(@rzwGroupId AS NVARCHAR(50))+' ).'
        
        EXEC dbo.p_note_new
  		@groupProcessId = @groupProcessInstanceId,
  		@type = dbo.f_translate('system',default),
  		@content = @smsContent,
  		@err = @err OUTPUT,
  		@message = @message OUTPUT
  		
--	    IF @processInstanceIds = '-999'
--	    BEGIN
--		    select 'xxx'
--		    RETURN
--	    END 
		    
        select @rzwId = pin.id, @rzwGroupId = pin.group_process_id from dbo.process_instance pin with(nolock)
        left join dbo.attribute_value av with(nolock) on av.group_process_instance_id = pin.group_process_id and av.attribute_path = '168,839'
--        inner join dbo.f_split(@processInstanceIds,',') ids on ids.data = pin.id
     	where root_id = @rootId and step_id = '1007.055' and active = 1 and isnull(av.value_int,av.group_process_instance_id) = @groupProcessInstanceId
      
     		IF SYSTEM_USER = dbo.f_translate('andrzej.dziekonski',default)
 BEGIN
 	SELECT @rzwId
 END
 
	
     	IF @rzwId is null 
     	BEGIN 
	     	SET @variant = 0
	     	RETURN
     	END 
     	
        insert into dbo.process_instance_flow (previous_process_instance_id,next_process_instance_id,active)
        select @previousProcessId, @rzwId, 1
        
    
  		EXEC [dbo].[p_attribute_edit]
	      @attributePath = '812,64', 
	      @groupProcessInstanceId = @rzwGroupId,
	      @stepId = 'xxx',
	      @userId = 1,
	      @originalUserId = 1,
	      @valueString = @firstname,
	      @err = @err OUTPUT,
	      @message = @message OUTPUT
	      
        EXEC [dbo].[p_attribute_edit]
	      @attributePath = '812,66', 
	      @groupProcessInstanceId = @rzwGroupId,
	      @stepId = 'xxx',
	      @userId = 1,
	      @originalUserId = 1,
	      @valueString = @lastname,
	      @err = @err OUTPUT,
	      @message = @message OUTPUT
	      
	END 

END

