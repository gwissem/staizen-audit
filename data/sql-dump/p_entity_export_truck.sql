
ALTER PROCEDURE [dbo].[p_entity_export_truck] @rootId INT
AS
BEGIN

BEGIN TRANSACTION
SET NOCOUNT ON

DECLARE @groupProcessId INT
DECLARE @serviceId INT
DECLARE @entityId INT
DECLARE @manufacturerQuantity DECIMAL(20,4)
DECLARE @insurancerQuantity DECIMAL(20,4)
DECLARE @partnerQuantity DECIMAL(20,4)
DECLARE @dealerQuantity DECIMAL(20,4)
DECLARE @clientQuantity DECIMAL(20,4)
DECLARE @partnerPath NVARCHAR(255)
DECLARE @country NVARCHAR(255)
DECLARE @partnerId INT
DECLARE @platformName NVARCHAR(255)
DECLARE @zzId INT
DECLARE @gidTypeZZ SMALLINT
DECLARE @gidTypeZS SMALLINT
DECLARE @finishedAt DATETIME
DECLARE @caseId NVARCHAR(255) = 'A' + RIGHT('00000000' + CAST(@rootId AS NVARCHAR(10)), 8)
DECLARE @partnerStarterCode NVARCHAR(255)
DECLARE @countryCode NVARCHAR(3)
DECLARE @platformId INT
DECLARE @programId INT
DECLARE @programName NVARCHAR(255)
DECLARE @type INT
DECLARE @headerType NVARCHAR(2)
DECLARE @serieZS varchar(5)
DECLARE @serieZZ varchar(5)
DECLARE @description NVARCHAR(255)
DECLARE @starterCode NVARCHAR(255)
DECLARE @quantity DECIMAL(20,4)
DECLARE @headerZS INT
DECLARE @headerZZ INT
DECLARE @vat INT
DECLARE @nettoBrutto VARCHAR(1)
DECLARE @nightFee DECIMAL(20,4)
DECLARE @discount DECIMAL(20,4)
DECLARE @vatGroup NVARCHAR(2)
DECLARE @twrCode NVARCHAR(100)
DECLARE @value DECIMAL(20,4)
DECLARE @currency nvarchar(3)
DECLARE @casePayment bit = 0
DECLARE @entityValueId int
DECLARE @akronimZSADH nvarchar(50)
DECLARE @additTownigADH int = 0
DECLARE @quantityZ DECIMAL(20,4)
DECLARE @onlyEmptyService int
DECLARE @akronimTarget nvarchar(50)
DECLARE @quantityXL int
DECLARE @descrTable as table (idHeader int, descr nvarchar(100)) 
DECLARE @serieO nvarchar(100)

set @onlyEmptyService=0

DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

delete from @values
INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
SELECT @platformId = value_int FROM @values	
SELECT @platformName = name FROM dbo.platform WHERE id = @platformId

print '#######################################################'
print dbo.f_translate('Platforma: ',default) + @platformName

DECLARE @existsActiveServices BIT = 0
/* Wyjątek CFM oraz VGP oraz Naprawa na drodze, przenoszenie po zakończeniu.
	25	LEASEPLAN
	48	Bussiness Lease
	53	ALPHABET
	43	ALD	*/
IF @platformId IN (25,48,53,43) --> CFM
BEGIN
	DECLARE @activeServicesCount INT = 0 --> Liczba aktywnych usług

	DECLARE @activeServices Table (
		id INT,
		group_process_id INT,
		serviceId INT,
		status_dictionary_id int,
		status_label NVARCHAR(255),
		status_progress INT,
		icon NVARCHAR(MAX),
		service_name NVARCHAR(255)
	)
	DECLARE @previousProcessId INT = (SELECT TOP 1 id FROM process_instance WHERE root_id=@rootId)
	INSERT @activeServices EXEC AtlasDB_v.[dbo].[p_get_status_active_services] @processInstanceId=@previousProcessId,@onlyActive=1,@showAll=1
	SET @activeServicesCount = ISNULL((SELECT COUNT(1) FROM @activeServices),0)

	IF @activeServicesCount > 0
	BEGIN
		SET @existsActiveServices = 1
	END
END

delete from @values
INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @rootId
SELECT @country = value_string FROM @values	
SELECT @countryCode = argument1 FROM dbo.dictionary WHERE typeD = 'country' and textD = @country

--SELECT @description = ISNULL(@programName,'')
--SELECT @caseId = dbo.f_caseId(@rootId)
DECLARE @caseDate DATETIME = (select top 1 created_at from dbo.process_instance  where root_id=@rootId order by id)

/******************************/

declare @name nvarchar(100)

SET @partnerPath = '610'
IF ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)) <> dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
BEGIN
	SET @partnerPath = '741'
END 

-- ============================================================= --
--		    CASE FLAGS • Flagi do zamówień dla sprawy			 --
-- ============================================================= --
declare @AllTowingKmInOneOrder bit = 1 -- Wszystkie kilometry holowania na jednym zamówieniu dla programów ARC,AA,TCB,ANWB
declare @IsMedicalInCase bit = 0 -- Czy w sprawie znajduje się usługa medical, jeśli tak to będzie opłata za sprawę.
declare @IsAdviceInCase bit = 0 -- Czy w sprawie znajduje się porada
declare @KIAonceNHpriceZS bit = 1 -- Dla programów KIA jest pobierany jeden ryczałt za usługi NH.
declare @POCZTOEonceNHpriceZS bit = 1 -- Dla programów TUW POCZTOWY jest pobierany raz ryczałt za usługi NH.
declare @isTransport bit = 0 -- Dla ALD, Czy w sprawie istnieje transport, jeśli tak to opłata za sprawę.
declare @isTowing bit = 0 -- Dla BL opłata za sprawę, gdy jest holowanie (pojazd wymagający holowania)


DECLARE @entityTempTable TABLE (
	entityValueId int,
	service_group_process_id int,
	serviceId int, 
	entityId int, 
	manufacturerQuantity decimal(10,2), 
	insurancerQuantity decimal(10,2),
	partnerQuantity decimal(10,2),
	dealerQuantity decimal(10,2),
	clientQuantity decimal(10,2),
	name  nvarchar(100),
	programId int
)

insert into @entityTempTable
select	ave.id entityValueId,
		avs.value_int service_group_process_id,
		avsn.value_int serviceId,
		ave.value_int entityId,
		avpr.value_int manufacturerQuantity,
		avu.value_int insurancerQuantity,
		avpz.value_int partnerQuantity,
		avd.value_int dealerQuantity,
		avkl.value_int clientQuantity,
		e.entity,
		avp.value_int   
from	dbo.attribute_value avs inner join
		dbo.attribute_value avsn on avsn.parent_attribute_value_id=avs.id and avsn.attribute_path='820,821,805' inner join
		dbo.attribute_value avp on avp.parent_attribute_value_id=avs.id and avp.attribute_path='820,821,830' inner join
		dbo.attribute_value ave on ave.parent_attribute_value_id=avs.id and ave.attribute_path='820,821,822' inner join
		dbo.attribute_value avpr on avpr.parent_attribute_value_id=ave.id and avpr.attribute_path='820,821,822,824' inner join
		dbo.attribute_value avu on avu.parent_attribute_value_id=ave.id and avu.attribute_path='820,821,822,825' inner join
		dbo.attribute_value avpz on avpz.parent_attribute_value_id=ave.id and avpz.attribute_path='820,821,822,826' inner join
		dbo.attribute_value avd on avd.parent_attribute_value_id=ave.id and avd.attribute_path='820,821,822,827' inner join
		dbo.attribute_value avkl on avkl.parent_attribute_value_id=ave.id and avkl.attribute_path='820,821,822,828' inner join
		dbo.attribute_value avq on avq.parent_attribute_value_id=ave.id and avq.attribute_path='820,821,822,221' left join		
		dbo.attribute_value avcdn on avcdn.parent_attribute_value_id=ave.id and avcdn.attribute_path='820,821,822,861' inner join		
		dbo.entities e on e.id=ave.value_int 
where	avs.attribute_path='820,821' and 
		avs.value_int is not null and
		ISNULL(avcdn.value_int,1) = 1 and
		avs.root_process_instance_id=@rootId

declare kurZ cursor LOCAL for
select * from(
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity*0.5 quantity, 1 type, name, manufacturerQuantity  quantityXL, programId
		from @entityTempTable 
		where programId in (418,420) and entityId IN(10,1,21,29,43,19,23,14,22,16,63)
		UNION
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity*0.5 quantity, 2 type, name , manufacturerQuantity quantityXL, programId
		from @entityTempTable 
		where programId in (418,420) and entityId IN(10,1,21,29,43,19,23,14,22,16,63)
		UNION ALL
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity*0.7 quantity, 1 type, name , manufacturerQuantity quantityXL, programId
		from @entityTempTable 
		where programId in (415) and entityId IN(10,1,21,29,43,19,23,14,22,16,63)
		UNION
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity*0.3 quantity, 2 type, name , manufacturerQuantity quantityXL, programId
		from @entityTempTable 
		where programId in (415) and entityId IN(10,1,21,29,43,19,23,14,22,16,63)	
		union all
		select entityValueId, service_group_process_id, serviceId, entityId, manufacturerQuantity quantity, 1 type, name , manufacturerQuantity quantityXL, programId
		from @entityTempTable 
		WHERE not(entityId IN(10,1,21,29,43,19,23,14,22,16,63) and programId in (415,418,420))
		union all
		select entityValueId, service_group_process_id, serviceId, entityId, insurancerQuantity quantity, 2 type, name , insurancerQuantity quantityXL, programId from @entityTempTable
		union all 
		select entityValueId, service_group_process_id, serviceId, entityId, partnerQuantity quantity, 3 type, name , partnerQuantity quantityXL, programId from @entityTempTable
		union all 
		select entityValueId, service_group_process_id, serviceId, entityId, dealerQuantity quantity, 4 type, name , dealerQuantity quantityXL, programId from @entityTempTable
		union all
		select entityValueId, service_group_process_id, serviceId, entityId, clientQuantity quantity, 5 type, name , clientQuantity quantityXL, programId from @entityTempTable
	) a where isnull(a.quantity,0.0) <> 0.0 
OPEN kurZ;
FETCH NEXT FROM kurZ INTO @entityValueId, @groupProcessId, @serviceId, @entityId, @quantity, @type, @name, @quantityXL,@programId;
WHILE @@FETCH_STATUS=0
BEGIN
	print '#######################################################'
	print @name  + dbo.f_translate(' entity: ',default) + CAST(@entityId AS VARCHAR(20))

	/* Flagi opcji. */
	declare @skipStarterCodeBL bit = 0
	declare @atrybutLeaseplan nvarchar(50)

	DECLARE @partnerKinds NVARCHAR(1000) = NULL
	DECLARE @partnerCode NVARCHAR(255) = NULL

	set @onlyEmptyService=0
	SET @headerZZ = NULL
	SET @headerZS = NULL
	declare @AkronimZS varchar(50)
	declare @czyASO int
	set @akronimTarget=''
	set @partnerId=0

	SELECT @programName = name FROM dbo.vin_program WHERE id = @programId
    set @description = ISNULL(@programName,'')

	-- Jeśli jest odwrotny GroupId to ustawia zmienną @reverseGroupId. Wykorzystywane dla rozdzielania usługi MW po programach.
	declare @reverseGroupId int = 0 -- standardowo brak
	if @groupProcessId < 0
	begin
		set @reverseGroupId = @groupProcessId
		set @groupProcessId = @groupProcessId * -1
		set @description = @description + dbo.f_translate('. Usługa łączona',default)
	end

	if @existsActiveServices = 1
	begin
		set @description = @description + ' » Sprawa niezakończona'
	end

	-- Sprawdzenie statusu pusty wyjazd, jeśli tak(1), jesli nie (0)
	declare @emptyService int = 0
	declare @serviceStatus int = (	SELECT TOP 1 status_dictionary_id
									FROM dbo.service_status
									WHERE group_process_id=@groupProcessId AND @entityId IN (1,10)
									ORDER BY id DESC)
	if @serviceStatus = -2
	begin
		set @emptyService=1
		print 'Pusty wyjazd!'
	end

	-- Naprawa nieudana
	declare @noFix int
	declare @arcCode3Id int
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,722', @groupProcessInstanceId = @groupProcessId
	SELECT @arcCode3Id = value_int FROM @values	


	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '209', @groupProcessInstanceId = @groupProcessId
	DECLARE @isPartnerFixed BIT = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)

	/*	120	6F naprawa na miejscu niemożliwa - podholowano do serwisu
		140	97 holowanie (ogólnie)
		150	6A naprawa na miejscu niemożliwa - dalsze usługi nie są konieczne
		160	2G brak narzędzi do naprawy na miejscu - holowanie do serwisu
		180	6G naprawa na miejscu niemożliwa - holowanie do serwisu
		190	6D naprawa na miejscu niemożliwa- holowanie do miejsca wskazanego przez klienta
		200	6S - naprawa niemożliwa. Konieczne holowanie innym pojazdem							*/
	if (@arcCode3Id in (120,140,150,160,180,190,200) AND @entityId=10) OR (@arcCode3Id IS NULL AND @isPartnerFixed=0 AND @entityId=10) --(@arcCode3Id in (150,200) AND @entityId = 1) OR Skasowane ze względu na to że kontraktorzy zamykają holowanie kodami z naprawy.
	begin
		set @noFix=1
		print 'Naprawa nieudana!'
	end
	else
	begin
		set @noFix=0
	end 

	IF EXISTS(SELECT 1 FROM attribute_value WHERE attribute_path = '610' AND group_process_instance_id = @groupProcessId AND value_int IS NOT NULL)
	BEGIN -- Partner polski
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessId
		SELECT @partnerId = value_int FROM @values	
	END
	ELSE
	BEGIN -- Partner zagraniczny
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '767,773,704', @groupProcessInstanceId = @groupProcessId
		SELECT @partnerId = value_int FROM @values	
	END

	IF ISNULL(@partnerId,0)=0 AND @entityId=29 --> MW Pobieranie partnera z pola Wypożyczalnia organizująca wynajem
	BEGIN
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessId
		SELECT @partnerId = value_int FROM @values		
	END

	IF (@type = 4)
	BEGIN
		SET @description = @description + ' (rozliczenie z Dealerem)'
	END

	SELECT @partnerStarterCode = av.value_string 
	FROM	dbo.attribute_value av inner join 
			dbo.attribute_value location ON location.parent_attribute_value_id = av.parent_attribute_value_id  
	where	av.attribute_path = '595,631'
		and location.id = @partnerId
	
	print '@partnerStarterCode: ' + @partnerStarterCode


	if @partnerId=0 set @partnerStarterCode='111111'

	print '@partnerStarterCode PO: ' + @partnerStarterCode

	-------------------------------------------------------
	------------- service Partner Code
	------------- kod partnera do którego holujemy (serwis)
	-------------------------------------------------------
	declare @servicePartnerId int = null
	declare @towingGroupProcessId int = null
	declare @servicePartnerStarterCode nvarchar(50) = null

	select  top 1 @towingGroupProcessId=group_process_id
	from	dbo.process_instance 
	where	root_id=@rootId and 
			step_id like '1009%'
	order by id desc

	-- Dealer Call zawsze na Serwis
	DECLARE @IsDealerCall BIT = ISNULL((SELECT TOP 1 value_int FROM attribute_value WHERE root_process_instance_id = @rootId AND attribute_path='802' AND group_process_instance_id=@groupProcessId ORDER BY id DESC),0)
	IF (@IsDealerCall = 1 AND @towingGroupProcessId IS NULL)
	BEGIN
		SET @towingGroupProcessId = @groupProcessId
	END

	print @country
	if isnull(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)) = dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
	begin
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towingGroupProcessId
		SELECT TOP 1 @servicePartnerId = value_int FROM @values WHERE value_int IS NOT NULL
	end
	else
	begin
		print @country
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '767,773,704', @groupProcessInstanceId = @towingGroupProcessId
		SELECT TOP 1 @servicePartnerId = value_int FROM @values WHERE value_int IS NOT NULL
	end

	SELECT @servicePartnerStarterCode = av.value_string
	FROM	dbo.attribute_value av inner join 
			dbo.attribute_value location ON location.parent_attribute_value_id = av.parent_attribute_value_id  
	where	av.attribute_path = '595,631'
		and location.id = @servicePartnerId
	-------------------------------------------------------
	if exists(
		SELECT av.value_text 
		FROM dbo.attribute_value av
		inner join dbo.attribute_value location ON location.id = av.parent_attribute_value_id cross apply dbo.f_split (av.value_text,',') s
		where av.attribute_path = '595,597,596'
		and location.id = @partnerId and s.data in (2,3,4,5,7,8,9,10,11,47) 
	) 
	begin
		set @czyASO=1
	end
	else
	begin
		set @czyASO=0
	end

	declare @towing int = null
	--/*1-hol, 2-naprawa, null*/
	SELECT TOP 1 @towing=value_int FROM attribute_value WITH(NOLOCK) WHERE attribute_path='560' AND group_process_instance_id=@groupProcessId AND value_int IS NOT NULL

	-- Usługi auta zastępczego.
	DECLARE @AutoReplacement BIT = 0
	IF (@entityId IN (29,30,31,32,33,34,35,36,37,38,39,40,41,42))
	BEGIN
		SET @AutoReplacement = 1
	END

	---------------Kontrahent Docelowy ---------------------------------------
	declare @Target varchar(20)
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towingGroupProcessId
	SELECT TOP 1 @servicePartnerId = value_int FROM @values WHERE value_int IS NOT NULL

	if @servicePartnerId is not null
	begin
		SELECT  @Target = avKod.value_string 
		FROM	dbo.attribute_value avL inner join 
				dbo.attribute_value avKod ON avKod.parent_attribute_value_id = avL.id and avKod.attribute_path='595,597,631'  
		WHERE	avL.id = @servicePartnerId and avL.attribute_path='595,597'
	end

	-------------------------------------------------------------------
	-- Tutaj wpisać wszelkie zmienne i dane apropos nagłówka sprawy
	-------------------------------------------------------------------
	SELECT @gidTypeZZ = 1152
	SELECT @serieZZ = ''

	SELECT @gidTypeZS = 1280
	SELECT @serieZS = ''

	-- Usługa kredytu tak samo jak klient płaci, nie ma się dzielić itd...
	if @entityId=18
	begin
		set @type=5
	end

	-- ============================================================= --
	--						Flagi do zamówień --
	-- ============================================================= --
	declare @XL_ExpoNormZS int = 0	
	declare @XL_ExpoNormZZ int = 0
	declare @XL_FlagaNBZS varchar(1) = 'N'
	declare @XL_FlagaNBZZ varchar(1) = 'N'
	declare @isTrialPrice bit = 0 --> Cenniki testowe dla wybranych kontraktorów na ZZ
	declare @isTireRepair bit = 0 --> Kontraktorzy z możliwością naprawy opon


	-- ============================================================== --
	--  Konfiguracja nagłówków i serii dla poszczególnych programów.  --
	-- ============================================================== --
	DECLARE @platformCFM INT = (SELECT TOP 1 platform_id FROM dbo.vin_program WHERE id=@programId AND platform_id IS NOT NULL)

	IF		@programId IN (402,414,415,416,417,418,419,420,421,424,425,426) --> VGP	BEZ SEAT IF @platformId IN (6,11,31) -- VGP
	BEGIN
		print dbo.f_translate('VGP',default)
		if (@programId in (414,415,427,426) AND @type in (1,4,5)) -- OR Audi ARC
		begin
			SET @serieZS = 'AU'
			SET @serieO = 'AU'
			set @AkronimZS='103600'

			if (@towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1 or @entityId in (21,63)) and (@entityId not in (30,31)) -- podstawienie i odbir
			begin
				set @AkronimZS=@servicePartnerStarterCode
				set @akronimTarget=@Target
			end
		end

		if @programId in (414,415) AND @type=2
		begin
			SET @serieZS = dbo.f_translate('AUB',default)
			SET @serieO = 'AU'
			set @AkronimZS='106000'
		end

		if (@programId in (416,402,421,427,425) AND @type in (1,4,5)) -- OR Skoda ARC
		begin
			SET @serieZS = dbo.f_translate('SKO',default)
			SET @serieO = dbo.f_translate('SKO',default)

			set @AkronimZS='103600'		
			if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1 or @entityId in (21,63)
			begin
				set @AkronimZS=@servicePartnerStarterCode
				set @akronimTarget=@Target
			end 
		end

		if @programId in (416,402,421,427) AND @type=2
		begin
			SET @serieZS = dbo.f_translate('SKOB',default)
			SET @serieO = dbo.f_translate('SKO',default)
			set @AkronimZS='106000'
		end

		if (@programId in (417,418,419,420,427,424) AND @type in (1,4,5)) -- OR Volkswagen ARC
		begin
			SET @serieZS = 'VW'
			SET @serieO = 'VW'

			set @AkronimZS='103600'
			if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1 or @entityId in (21,63)
			begin
				set @AkronimZS=@servicePartnerStarterCode
				set @akronimTarget=@Target
			end
		end
	
		if @programId in (417,418,419,420) AND @type=2
		begin
			SET @serieZS = dbo.f_translate('VWB',default)
			SET @serieO = 'VW'
			set @AkronimZS='106000'
		end

		if @type = 3
		begin
			SET @serieZS = dbo.f_translate('SARC',default)
			SET @serieO = dbo.f_translate('SARC',default)
			set @AkronimZS='111111'
		end

		if @type = 5
		begin
			SET @description = @description + dbo.f_translate(' Rozliczenie z KL',default)
			set @AkronimZS='103600'
		end

		if @programId in (424,425,426) and (isnull(@serieZS,'')='' or isnull(@serieZS,'')='')
		begin
			SET @serieZS = dbo.f_translate('SARC',default)
			SET @serieO = dbo.f_translate('SARC',default)
		end

		DELETE FROM @values	
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '573',
				@groupProcessInstanceId = @rootId

		IF ((SELECT value_int FROM @values) IN (0,-1))
		BEGIN
			SET @description = @description + dbo.f_translate('. Brak w bazie',default)
		END
	END
	ELSE IF @programId IN (429,430,431,432,433,434,435,436,437,438,439,440,441) --> Ford, Ford Vignale	ELSE IF @platformId IN (2,60) -- FORD,Ford Vignale
	BEGIN
		set @serieZS = dbo.f_translate('FORD',default) -- Seria uniwersalna w ramach platformy
		set @type = 1 -- Jeśli FORD, ustaw rozliczenie na Producenta

		/*	429	Ford Euroservice
			430	Ford Assistance 12
			431	Ford Protect
			432	Ford Inne Marki Standard
			433	Ford Inne Marki Exclusive
			434	Ford Inne Marki Plus
			435	Ford Ubezpieczenia Standard
			436	Ford Ubezpieczenia Plus
			437	Ford Ubezpieczenia Exclusive
			438	Ford Mini Car Assistance
			439	Ford Assistance Kolizyjny	*/
		if @programId in (429,430,431,432,433,434,435,436,437,438)
		begin
			set @XL_FlagaNBZS = 'B'
			set @AkronimZS='100009'
			--set @type = 2 -- Jeśli FORD, ustaw rozliczenie na Ubezpieczyciel
		end

		if @programId = 439 -- Ford Assistance Kolizyjny
		begin
			set @AkronimZS='100020'
		end

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1
		begin
			set @akronimTarget=@AkronimZS--@Target
		end

		if @programId = 441 -- Ford Vignale 60
		begin
			set @serieZS = dbo.f_translate('FORV',default)
			set @AkronimZS='100268' -- ARC EUROPE
			set @XL_ExpoNormZS = 6
			set @XL_ExpoNormZZ = 6
		end
	END
	ELSE IF	@programId IN (368,461,462,463,464) --> VGP	BEZ SEAT IF @platformId IN (6,11,31) -- VGP	ELSE IF @platformId = 10 -- Seat Assistance
	BEGIN
		print dbo.f_translate('SEAT',default)
		set @serieZS = dbo.f_translate('SEAT',default)
		set @serieO = dbo.f_translate('SEAT',default)

		/*	461	SEAT Assistance
			462	SEAT Przedłużone Assistance				*/
		if (@programId in (461,462)) -- 
		begin
			set @type = 1 -- Producent
			set @AkronimZS='103600'
		end

		/*	368	SEAT Gwarancja Mobilności od 1.7.2015
			463	Seat Gwarancja Mobilności				*/
		if (@programId in (368,463)) -- 
		begin
			set @type = 2 -- Ubezpieczyciel
			set @AkronimZS='106000'
		end

		/*	464	ARC SEAT  */
		if (@programId in (464))
		begin
			set @type = 3 -- Partner Zagraniczny
			SET @serieZS = dbo.f_translate('SARC',default)
			SET @serieO = dbo.f_translate('SARC',default)
			set @AkronimZS='111111'
		end

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1  or @entityId = 21
		begin
			--set @AkronimZS=@servicePartnerStarterCode
			set @akronimTarget=@AkronimZS--@Target
		end

		DELETE FROM @values	
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '573',
				@groupProcessInstanceId = @rootId

		IF ((SELECT value_int FROM @values) IN (0,-1))
		BEGIN
			SET @description = @description + dbo.f_translate('. Brak w bazie',default)
		END
	END
	ELSE IF @platformCFM = 76 --> Cupra Assistance
	BEGIN
		print dbo.f_translate('Cupra Assistance',default)
		set @serieZS = dbo.f_translate('CUP',default)
		set @serieO = dbo.f_translate('CUP',default)

		set @type = 1 -- Producent
		set @AkronimZS='103600'

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1  or @entityId = 21
		begin
			--set @AkronimZS=@servicePartnerStarterCode
			set @akronimTarget=@AkronimZS--@Target
		end

		DELETE FROM @values	
		INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '573',
				@groupProcessInstanceId = @rootId

		IF ((SELECT value_int FROM @values) IN (0,-1))
		BEGIN
			SET @description = @description + dbo.f_translate('. Brak w bazie',default)
		END
	END
	ELSE IF @programId IN (447,448,449) --> KIA	ELSE IF @platformId = 32 -- KIA
	BEGIN
		print dbo.f_translate('KIA',default)
		set @serieZS = dbo.f_translate('KIA',default)
		set @serieO = dbo.f_translate('KIA',default)
		set @AkronimZS='102900'

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1
		begin
			set @akronimTarget=@AkronimZS--@Target
		end
	END
	ELSE IF @programId IN (442,454,455,456,457,590,591,592) OR @platformCFM=14 --> OPEL
	BEGIN
		print dbo.f_translate('OPEL',default)
		set @serieZS = 'GM'
		set @serieO = 'GM'
		set @AkronimZS='102200'

		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1
		begin
			set @akronimTarget=@AkronimZS--@Target
		end
	END
	ELSE IF @programId IN (475,476,477,478,479,480,481,482,483) --> TUW TUZ	ELSE IF @platformId = 35 --	TUW TUZ
	BEGIN
		print dbo.f_translate('TUW TUZ',default)
		set @AkronimZS='103200'
		set @serieZS = dbo.f_translate('TUZ',default)
		set @serieO = dbo.f_translate('TUZ',default)
		if @towing = 1 or @AutoReplacement = 1 or @IsDealerCall = 1
		begin
			set @akronimTarget=@AkronimZS--@Target
		end
	END
	ELSE IF @programId IN (484,485,486) -->	TUW TUZ Home Assistance	ELSE IF @platformId = 36 --	TUW TUZ Home Assistance
	BEGIN
		print dbo.f_translate('TUW TUZ',default)
		set @AkronimZS='103200'
		set @serieZS = 'HA'
		set @serieO = 'HA'
		set @XL_FlagaNBZZ = 'B'
	END
	ELSE IF @programId IN (469,470,471,472,473,474) -->	TUW Pocztowe	ELSE IF @platformId = 42 --	TUW Pocztowe
	BEGIN
		print dbo.f_translate('TUW Pocztowe',default)
		set @AkronimZS='103903'
		set @serieZS = dbo.f_translate('POCZ',default)
		set @serieO = dbo.f_translate('POCZ',default)
		set @XL_FlagaNBZS = 'B'

		/*	469	42	TUW Assistance (Pocztowe OC)
			470	42	Moto Assistance Standard (TUW Pocztowe)
			471	42	Moto Assistance Komfort (TUW Pocztowe)*/
		if @programId in (469,470,471)
		begin
			set @XL_FlagaNBZZ = 'N'
		end

		/*	472	42	TUW Pocztowe Bezpieczny Dom
			473	42	TUW Pocztowe ubezpieczenie kredytów i pożyczek hipotecznych
			474	42	TUW Pocztowe ubezpieczenie ROR */
		if @programId in (472,473,474)
		begin
			set @XL_FlagaNBZZ = 'B'
		end
	END
	ELSE IF @programId IN (443,444,445,446,494,495,496,497) -->	Concordia Assistance	ELSE IF @platformId = 18 --	Concordia Assistance
	BEGIN
		print dbo.f_translate('Concordia',default)
		set @AkronimZS='101800'
		set @serieZS = dbo.f_translate('CON',default)
		set @serieO = dbo.f_translate('CON',default)
		set @XL_FlagaNBZS = 'B'
	END
	ELSE IF @programId IN (458,459,460) -->	Orix Standard,VIP,Exclusive
	BEGIN
		print dbo.f_translate('Orix',default)
		set @AkronimZS='101800'
		set @serieZS = dbo.f_translate('ORIX',default)
		set @serieO = dbo.f_translate('ORIX',default)
		set @XL_FlagaNBZS = 'B'
	END
	ELSE IF @programId IN (468) --> Comfort ERV
	BEGIN
		print dbo.f_translate('ERV',default)
		set @AkronimZS='104800'
		set @serieZS = dbo.f_translate('ERV',default)
		set @serieO = dbo.f_translate('ERV',default)
		set @XL_FlagaNBZS = 'B'
	END
	ELSE IF @programId IN (492,493) --> Centra Assistance, Exide Assistance
	BEGIN
		print dbo.f_translate('CENTRA',default)
		set @AkronimZS='100006'
		set @serieZS = dbo.f_translate('CEN',default)
		set @serieO = dbo.f_translate('CEN',default)
	END
	--» Od 06.12.2018r.
	ELSE IF @programId IN (498) --> AA (członkowie klubu brytyjskiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = 'AA'
		set @serieO = 'AA'
		set @AkronimZS='101612'
	END
	ELSE IF @programId IN (499) --> ANWB (członkowie klubu holenderskiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('ANWB',default)
		set @serieO = dbo.f_translate('ANWB',default)
		set @AkronimZS='106813'
	END
	ELSE IF @programId IN (507,501,502,488,489,487) --> ARC,RACE (członkowie klubu hiszpańskiego),Rosqvist (członkowie klubu fińskiego)
	BEGIN/*	489	Volvo Over Seas delivery
			488	Volvo Global Special Sales
			487	Volvo Tourist & Diplomat Sales*/
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('SARC',default)
		set @serieO = dbo.f_translate('SARC',default)
		set @AkronimZS='111111'
	END
	ELSE IF @programId IN (505) --> Husqvarna Assistance (Sylwia)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('HUSQ',default)
		set @serieO = dbo.f_translate('HUSQ',default)
		set @AkronimZS='111111'
	END
	ELSE IF @programId IN (450) --> KTM Assistance (Sylwia)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('KTM',default)
		set @serieO = dbo.f_translate('KTM',default)
		set @AkronimZS='100268'
	END
	ELSE IF @programId IN (451) --> Mercedes Wypadek (Łukasz)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('MERW',default)
		set @serieO = dbo.f_translate('MERW',default)
		set @AkronimZS='100268'
	END
	ELSE IF @programId IN (500) --> OAMTC (członkowie klubu austriackiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('OMTC',default)
		set @serieO = dbo.f_translate('OMTC',default)
		set @AkronimZS='100044'
	END
	ELSE IF @programId IN (465) --> Smart Assistance (Sylwia)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('SMA',default)
		set @serieO = dbo.f_translate('SMA',default)
		set @AkronimZS='100574'
	END
	ELSE IF @programId IN (503) --> TCB (członkowie klubu belgijskiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('TCB',default)
		set @serieO = dbo.f_translate('TCB',default)
		set @AkronimZS='100439'
	END
	ELSE IF @programId IN (504) --> TCS (członkowie klubu szwajcarskiego)
	BEGIN
		set @type = 3 -- Partner Zagraniczny
		set @XL_ExpoNormZS = 1
		set @XL_ExpoNormZZ = 1
		set @serieZS = dbo.f_translate('TCS',default)
		set @serieO = dbo.f_translate('TCS',default)
		set @AkronimZS='100267'
	END
	--» CFM Od 1,2,3? 2019r. 
	ELSE IF @programId IN (508,509,510,511,512,513,514,515,516,517,518,519,520,521) OR @platformCFM = 53 --> 53 Alphabet
	BEGIN
		PRINT dbo.f_translate('Alphabet',default)
		set @serieZS = dbo.f_translate('ALF',default)
		set @serieO = dbo.f_translate('ALF',default)
		set @AkronimZS='105000'
	END
	ELSE IF @programId IN (551,552,553) OR @platformCFM = 48--> 48 Bussiness Lease
	BEGIN
		PRINT dbo.f_translate('Bussiness Lease',default)
		set @serieZS = 'BL'
		set @serieO = 'BL'
		set @AkronimZS='104000'

		SELECT @partnerKinds=av_p.value_text FROM attribute_value av WITH(NOLOCK)
		JOIN attribute_value av_p WITH(NOLOCK) ON av.id=av_p.parent_attribute_value_id AND av_p.attribute_path = '595,597,596' 
		JOIN attribute_value av_ks WITH(NOLOCK) ON av_ks.group_process_instance_id=av_p.group_process_instance_id AND av_ks.attribute_path = '595,631' 
		WHERE av.id=@partnerId

		if @serviceId = 5 AND NOT EXISTS ( --> Auto zastępcze
			SELECT  d.textD FROM f_split(@partnerKinds,',') fs
			JOIN dictionary d WITH(NOLOCK) ON d.value=fs.data AND d.typeD = 'PartnerLocationType'
			WHERE d.textD LIKE '%ASO%'
		)
		begin
			set @AkronimZS='107176' --> Wszystkie koszty związane z obsługa wynajmów powinny iść na Motoflotę. Ale nie jeśli wykonuje ASO!
		end
	END
	ELSE IF @programId IN (0) OR @platformCFM = 25 --> 25 LEASEPLAN
	BEGIN
		PRINT dbo.f_translate('LEASEPLAN',default)
		set @serieZS = dbo.f_translate('LEAS',default)
		set @serieO = dbo.f_translate('LEAS',default)
		set @AkronimZS='102800'

		set @atrybutLeaseplan = (select top 1 value_string from attribute_value with(nolock) where root_process_instance_id=@rootId and attribute_path='440' and value_string is not null)
		if @programId = 575 --> LeasePlan Extra (dodatkowa autoryzacja LeasePlan)
		begin
			set @description = @description + ' » LP-Passon'
		end
		else if isnull(@atrybutLeaseplan,'') <> ''
		begin
			set @description = @description + ' » ' + @atrybutLeaseplan
		end
	END
	ELSE IF @programId IN (0) OR @platformCFM = 43 --> 43 ALD
	BEGIN
		PRINT dbo.f_translate('ALD',default)
		set @serieZS = dbo.f_translate('ALD',default)
		set @serieO = dbo.f_translate('ALD',default)
		set @AkronimZS='102725'
	END
	/*	8	DKV
		16	UTA Assistance    
		20	Euroshell Breakdown Service
		21	Shell Autoryzacje
		22	ESSO Assistance
		50	Shell Alarmowy
		64	Webasto Assistance
		68	KOGEL Assistance
		73	IDS
		74	Otokar Assistance	*/
	ELSE IF @platformCFM IN (8,13,16,20,21,22,50,64,68,73,74) --> TRUCK
	BEGIN
		PRINT dbo.f_translate('TRUCK',default)
		set @XL_ExpoNormZS = 6
		set @XL_ExpoNormZZ = 0
		set @serieZS = 'TR'
		set @serieO = 'TR'
		set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
		-- holowanie, naprawa na drodze, naprawa warsztatowa oraz dowóz gotówki.
		set @AkronimZS='111111'

		if @programId = 522 --> DKV
		begin
			set @AkronimZS='101756'
			set @currency = dbo.f_translate(dbo.f_translate('PLN',default),default)
			set @XL_ExpoNormZS = 0
		end

		if @programId in (526,525) --> UTA Assistance, Euroshell Breakdown Service
		begin
			set @AkronimZS='100390'
		end
	END
	ELSE IF @platformCFM IN (5) --> ADAC
	BEGIN
		PRINT dbo.f_translate('ADAC',default)
		set @serieZS = dbo.f_translate('ADAC',default)
		set @serieO = dbo.f_translate('ADAC',default)
		set @AkronimZS='100264'
	END

	--- Uniwersalne parametry dla wszystkich platform
	if @programId=428 -- Strata
	begin
		SET @serieZS = dbo.f_translate('STRA',default)
		SET @serieO = dbo.f_translate('STRA',default)
		set @partnerId=-1
		set @AkronimZS='111111'
	end

	SET @additTownigADH = 0
	if @programId=423 -- Usługi płatne
	begin
		SET @description = ISNULL(@programName,'') + dbo.f_translate(' Ad-hoc',default)
		SET @serieZS = dbo.f_translate('ADH',default)
		SET @serieO = dbo.f_translate('ADH',default)
		set @partnerId=-21
		set @AkronimZS=@partnerStarterCode
		--set @AkronimZS='111111'
		set @akronimZSADH=@partnerStarterCode
		--set @akronimZSADH='111111'
		set @type=10

		--> W przypadku AD-HOC kiedy jest ADH oraz inne holowanie z pokryciem różnym niż usługi płatne
		IF EXISTS(SELECT TOP 1 1 FROM attribute_value av WITH(NOLOCK) JOIN attribute_value av_p WITH(NOLOCK) 
			ON av.group_process_instance_id=av_p.group_process_instance_id AND av_p.attribute_path='202' AND av_p.value_string <> '423'
			WHERE av.root_process_instance_id=@rootId AND av.attribute_path='560' and av.value_int=1)
		BEGIN
			IF @programId = 423 --> Usługi Płatne
			BEGIN
				SET @additTownigADH = 1
			END
		END
	end

	if @entityId=18	-- Usługa kredytu zawsze na akronim jednorazowy
	begin
		set @AkronimZS='111111'
	end

	--Jeśli Dealer's Calls to na serwis
	if @IsDealerCall = 1
	begin
		-- Najlepiej zmienić na @Target ale wtedy nie importuje się jak trzeba i jest kontrahent,miasto 02
		set @partnerStarterCode=@servicePartnerStarterCode--@Target
		print '-- Partner: ' + @servicePartnerStarterCode
	end

	--if @entityId = 21 and isnull(@partnerStarterCode,'111111')='111111' --> Jeśli brak kontraktora taxi to serwis z holowania
	--begin
	--	set @partnerStarterCode = @servicePartnerStarterCode--@servicePartnerStarterCode
	--end

	/*	Organizowanie usługi N/H na GOP. Wtedy możemy oczekiwać wartości w atrybucie '638,957' [value_int] która mówi o kwocie netto za usługę. 
		Kontraktor nie jest zobligowany, aby podawać ilości przejechanych km dojazd/hol/powrót bo na gopa interesuje nas tylko kwota która została wynegocjowana. */
	DECLARE @isGopNH BIT = 0
	if @entityId in (1,10) and exists (SELECT 1 FROM attribute_value WITH(NOLOCK) WHERE attribute_path='1005,992' AND value_int=1 AND group_process_instance_id=@groupProcessId)
	begin
		set @isGopNH = 1
		SET @description = @description + ' » Usługi NH na GOP' 
	end

	if @partnerStarterCode in (	'36047', --> Ratownictwo Drogowe Pomoc Drogowa Jerzy Kijański
								'36042', --> Transport Ciężarowy Janusz Rapp
								'36034', --> Regmar Auto-Serwis
								'36029', --> Auto-Hilfe Pomoc Drogowa
								'36024', --> P.H.U. Jan Wengrzyn
								'36017', --> Tomala
								'36015') --> F.H.U. Holmax Andrzej Hoć
								and @caseDate >= '2019-03-01' and @caseDate <= '2019-05-31'
	begin
		set @isTrialPrice = 1
		print dbo.f_translate('Cenniki testowe dla wybranych kontraktorów',default)
		SET @description = @description + ' » Cennik testowy'
	end

	if @partnerStarterCode in ('35114') --> Opony u klienta Maciej Działowski
	begin
		set @isTireRepair = 1
		print dbo.f_translate('Naprawa opon',default)
		SET @description = @description + ' » Naprawa opon'
	end

	/************Nowa data @finishedAt dla ZZ jako data zakonczenia usługi, wyliczana indywidualnie**************/
	declare @prcId varchar(20)

	select top 1 @prcId=left(step_id,4)
	from dbo.process_instance
	where group_process_id=@groupProcessId

	if @prcId='1009' -- Data -> Holowanie lub naprawa
	begin
		/*Naprawa na drodze*/
	    if (select top 1 st.[serviceId] from [dbo].[service_status] st where st.group_process_id=@groupProcessId order by st.updated_at desc)=2
		begin
			SELECT TOP 1 @finishedAt=value_date FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path='638,130' AND value_date IS NOT NULL
		end
		else /*Holowanie*/
		begin 
			SELECT TOP 1 @finishedAt=value_date FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path='638,268' AND value_date IS NOT NULL
		end
	end
	else if @prcId='1007' -- Data -> Auto zastępcze
	begin
		begin 
			DECLARE @values5 Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

			insert into @values5	
			EXEC	dbo.p_attribute_get2
				@attributePath = '812,853', --> Data podana przez Kontraktora
				@groupProcessInstanceId =@groupProcessId

			select @finishedAt=value_date   from @values5 

			IF @finishedAt IS NULL --> Wyliczanie daty
			BEGIN
				DECLARE @rentalStartDate DATETIME = (
					SELECT TOP 1 value_date FROM attribute_value WITH(NOLOCK) WHERE group_process_instance_id=@groupProcessId AND attribute_path='540' AND value_date IS NOT NULL
				)
				SET @finishedAt = DATEADD(day, @quantity, @rentalStartDate)
			END
		end
	end	 
	else if @prcId='1018' -- Data -> Transport pojazdu
	begin
		
		begin 
			DECLARE @values6 Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

			insert into @values6	
			EXEC	dbo.p_attribute_get2
				@attributePath = '62,318',
				@groupProcessInstanceId =@groupProcessId

			select @finishedAt=value_date   from @values6 
		end 
	end
	else if @prcId='1021' -- Data -> Dealer Call's
	begin
		select top 1 @finishedAt=created_at from dbo.process_instance where root_id=@rootId order by id
	end
	else -- Data -> pozostałe przypadki
	begin	
		select @finishedAt=(select	top 1 ss.created_at  
		from	dbo.service_status ss inner join 
			dbo.service_status_dictionary ssd on ss.status_dictionary_id=ssd.id and ssd.progress=4
		where	ss.group_process_id=@groupProcessId
		order by ss.id )		
	end

	/*******************************/
	-- 2018-06-20 Poprawnienie pobierania daty oraz interpretowania dodatku nocnego (KR) 2H

	declare @startedAt datetime = (SELECT TOP 1 date_enter FROM process_instance WHERE group_process_id = @groupProcessId AND root_id = @rootId order by date_enter asc)
	declare @startHour int = datepart(HH, @startedAt)
	declare @night bit -- Dodatek nocny za zlecenie prac od 22 do 6

	if @startHour>=22 or @startHour<6
	begin
		set @night=1
		print dbo.f_translate('Dodatek nocny: ',default) + CAST(@night AS NVARCHAR(1))
	end
	else
	begin
		set @night=0
	end

	IF @programId IN (402,414,415,416,417,418,419,420,421,424,425,426)--VGP bez Seat'a
	BEGIN
		SELECT TOP 1 @headerZZ = id FROM sync.ZamNag WHERE partnerLocId = @partnerId AND type = @type and rootId=@rootId and XL_typ='ZZ' and AUD_WynikAPI is null
	END
	ELSE -- Pozostałe platformy
	BEGIN	
		select TOP 1 @headerZZ=zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
		where zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZZ' and zn.AUD_WynikAPI is null and zn.type = @type AND zn.XL_KntAkronim = @partnerStarterCode
	END
	
	-- Wykonywał pojazd (1 -> Laweta, 2 -> Patrolówka)----------------------------------------------------------
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,273', @groupProcessInstanceId = @groupProcessId
	declare @towVehicleType int = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)
	------------------------------------------------------------------------------------------------------------ 

	IF @entityId IN (1,10) AND (@emptyService = 1 OR (@noFix = 1 and @towVehicleType=2))
	begin
		set @onlyEmptyService=1
		if @emptyService = 1
		begin
			print '> Pusty wyjazd'
			set @description = @description + dbo.f_translate(' Pusty wyjazd',default)
		end
		else
		begin
			print '> Naprawa nieudana'
			set @description = @description + dbo.f_translate(' Naprawa nieudana',default)
		end

		if @programId NOT IN (483,482,481,480,479,478,477,476,475,497,496,495,494,446,445,444,443)--@platformId NOT IN (18,35) /* Bez Concordia i TUW TUZ,  rozliczamy pusty wyjazd i naprawę nieudaną. */
		begin
			set @AkronimZS = '111111'
			set @headerZZ = NULL
		end
		else
		begin
			if @noFix = 1 and @towVehicleType = 2 -- naprawa nieudana i patrolówka
			begin
				set @AkronimZS = '111111'
				set @headerZZ = NULL
			end
		end

		if @platformCFM = 48 and @emptyService = 1 --> Pusty wyjazd dla Business Lease na akronim BL
		begin
			set @AkronimZS = '104000'
		end

		if @platformCFM = 5 and ((@noFix = 1 and @towVehicleType=2) or @emptyService = 1) --> ADAC
		begin
			set @AkronimZS = '100264'
		end

		if @programId = 500 and @noFix = 1 and @towVehicleType=2 --> OAMTC
		begin
			set @AkronimZS = '100044'
		end
	end

	IF NOT (@noFix = 1 and @towVehicleType = 1) OR @platformCFM = 5 --> ADAC
	BEGIN
		PRINT '<*> Tworzenie nagłówków:'
		IF @headerZZ IS NULL
		BEGIN
			print dbo.f_translate(' - Dodaję nowy nagłówek ZZ.',default)
			INSERT INTO sync.ZamNag (
				AUD_GidTyp, -- 960
				XL_DataRealizacji, -- data statusu progres 4
				XL_DokumentObcy, -- caseId
				XL_KntAkronim,--XL_NabywcaDocelowyKntAkronim, -- kod starter
				XL_Opis, -- ProgramName
				XL_Typ, -- ZS/ZZ
				XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
				rootId, -- rootId
				type,
				partnerLocId,
				XL_DataWystawienia,
				XL_TargetKntAkronim,
				XL_ExpoNorm,
				XL_FlagaNB
			)
			SELECT 
			@gidTypeZZ, 
			getdate(),--@finishedAt, 
			@caseId, 
			@partnerStarterCode, 
			@description, 
			'ZZ',
			@serieZZ,	
			@rootId,
			@type,
			@partnerId,
			isnull(@finishedAt,getdate()),
			'',
			@XL_ExpoNormZZ,
			@XL_FlagaNBZZ

			SET @headerZZ = SCOPE_IDENTITY()
			print '@headerZZ='+cast(isnull(@headerZZ,-999) as varchar(20))
		
			set @headerZS=null

			IF @programId IN (402,414,415,416,417,418,419,420,421,424,425,426)--VGP bez Seat'a
			BEGIN
				SELECT @headerZS = id FROM sync.ZamNag WHERE XL_KntAkronim=@AkronimZS and  rootId=@rootId and XL_typ='ZS' and AUD_WynikAPI is null
			END
			ELSE -- Pozostałe platformy
			BEGIN	
				select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
				where zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null
			END

			IF @platformCFM = 25 --> 25 LEASEPLAN
			BEGIN
				IF @entityId = 12 AND @atrybutLeaseplan like '%EIIE%' --> 9901-Inne
				BEGIN
					set @headerZS = null
					set @description = @programName + ' » LP'
				END
				ELSE
				BEGIN
					select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
					where zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null and XL_Opis not like '%LP%'
				END
			END

			if @headerZS is null
			begin
				print dbo.f_translate(' - Dodaję nagłówek ZS',default)
				INSERT INTO sync.ZamNag (
					ParentZsId,
					AUD_GidTyp, -- 960
					XL_DataRealizacji, -- data statusu progres 4
					XL_DokumentObcy, -- caseId
					XL_KntAkronim,
					XL_Opis, -- Program name
					XL_Typ, -- ZS/ZZ
					XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
					rootId,--SO_SprawaId, -- rootId
					type,
					partnerLocId,
					XL_DataWystawienia,
					XL_TargetKntAkronim,
					XL_ExpoNorm,
					XL_FlagaNB
				)
				SELECT 
				null,
				@gidTypeZS, 
				getdate(),--@finishedAt, 
				@caseId,  
				@AkronimZS, 
				@description, 
				'ZS',
				@serieZS,	
				@rootId,
				@type,
				@partnerId,
				@caseDate,
				@akronimTarget,
				@XL_ExpoNormZS,
				@XL_FlagaNBZS

				SET @headerZS = SCOPE_IDENTITY()
			end
		
			print '@headerZS='+cast(isnull(@headerZS,-999) as varchar(20))
		
			UPDATE sync.ZamNag set ParentZsId=@headerZS where id=@headerZZ

		END
		ELSE -- Jesli Nagłówek ZZ już istnieje nagłówek dla kontrahenta to dopisuje pozycje zamówienia do istniejącego nagłówka
		BEGIN
			--SELECT @headerZS = id FROM sync.ZamNag WHERE XL_KntAkronim=@AkronimZS and  rootId=@rootId and XL_typ='ZS' and AUD_WynikAPI is null

			select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
			where XL_KntAkronim=@AkronimZS AND zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null

			IF @platformCFM = 25 --> 25 LEASEPLAN
			BEGIN
				IF @entityId = 12 AND @atrybutLeaseplan like '%EIIE%' --> 9901-Inne
				BEGIN
					set @headerZS = null
					set @description = @programName + ' » LP'
				END
				ELSE
				BEGIN
					select TOP 1 @headerZS=zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
					where XL_KntAkronim=@AkronimZS AND zn.rootId = @rootId AND ze.programId = @programId AND zn.XL_Typ = 'ZS' and zn.AUD_WynikAPI is null and XL_Opis not like '%LP%'
				END
			END

			-- KR
			if @headerZS is null --and @type = 5
			begin
				print dbo.f_translate(' - Dodaję nagłówek ZS',default)
				INSERT INTO sync.ZamNag (
					ParentZsId,
					AUD_GidTyp, -- 960
					XL_DataRealizacji, -- data statusu progres 4
					XL_DokumentObcy, -- caseId
					XL_KntAkronim,
					XL_Opis, -- Program name
					XL_Typ, -- ZS/ZZ
					XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
					rootId,--SO_SprawaId, -- rootId
					type,
					partnerLocId,
					XL_DataWystawienia,
					XL_TargetKntAkronim,
					XL_ExpoNorm,
					XL_FlagaNB
				)
				SELECT 
				null,
				@gidTypeZS, 
				getdate(),--@finishedAt, 
				@caseId,  
				@AkronimZS, 
				@description, 
				'ZS',
				@serieZS,	
				@rootId,
				@type,
				@partnerId,
				@caseDate,
				@akronimTarget,
				@XL_ExpoNormZS,
				@XL_FlagaNBZS

				SET @headerZS = SCOPE_IDENTITY()
			end
			-- /KR

			--SELECT dbo.f_translate('jest',default), @headerZS headerZS, @AkronimZS AkronimZS, @entityId entityId
		END 
	END

	/*	48 Business Lease
		53 ALPHABET			*/
	IF @platformCFM IN (48,53)
	BEGIN
		SELECT @partnerKinds=av_p.value_text,@partnerCode=av_ks.value_string FROM attribute_value av WITH(NOLOCK)
		JOIN attribute_value av_p WITH(NOLOCK) ON av.id=av_p.parent_attribute_value_id AND av_p.attribute_path = '595,597,596' 
		JOIN attribute_value av_ks WITH(NOLOCK) ON av_ks.group_process_instance_id=av_p.group_process_instance_id AND av_ks.attribute_path = '595,631' 
		WHERE av.id=@partnerId

		IF @platformCFM = 48 --> Business Lease
		BEGIN
			IF EXISTS (SELECT data FROM f_split(@partnerKinds,',') WHERE data = '188') --> BL blach.-lak.
			BEGIN
				SET @skipStarterCodeBL=1
			END
		END
	END

	print dbo.f_translate('Dodaję ',default)+@name + dbo.f_translate(' - ZZ typ:',default)+cast(@type as varchar(10))+dbo.f_translate(' headerZZ:',default)+cast(isnull(@headerZZ,-999) as varchar(100)) 
	--- element ZZ
	----------------------------------------------------------
	-- tutaj wpisać wszelkie zmienne i dane apropos entity ZZ

	declare @towingKm DECIMAL(20,4) = 0	-- Kilometry holowania
	declare @arrivalKm DECIMAL(20,4) = 0-- Kilometry dojazdu
	declare @backKm DECIMAL(20,4) = 0	-- Kilometry powrotu

	if @entityId in (1,8,10) -- Naprawa lub holowanie, Liczba osób transp. w lawecie powyżej 2 » Pobieranie liczby kilometrów.
	begin
		delete from @values
		INSERT	@values EXEC p_attribute_get2 @attributePath = '638,225', @groupProcessInstanceId = @groupProcessId
		SELECT	@towingKm = value_decimal FROM @values	
		SET		@towingKm = ROUND(ISNULL(@towingKm,0),0) 	-- Zaokrąglanie kilometrów

		delete from @values
		INSERT	@values EXEC p_attribute_get2 @attributePath = '638,217', @groupProcessInstanceId = @groupProcessId
		SELECT	@arrivalKm = value_decimal FROM @values		
		SET		@arrivalKm = ROUND(ISNULL(@arrivalKm,0),0) 	-- Zaokrąglanie kilometrów

		delete from @values
		INSERT	@values EXEC p_attribute_get2 @attributePath = '638,218', @groupProcessInstanceId = @groupProcessId
		SELECT	@backKm = value_decimal FROM @values	
		SET		@backKm = ROUND(ISNULL(@backKm,0),0) 	-- Zaokrąglanie kilometrów
	end

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,76', @groupProcessInstanceId = @groupProcessId
	DECLARE @dmc INT = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)
	IF @dmc IS NULL
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,76', @groupProcessInstanceId = @groupProcessId
		SET @dmc = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)
	END

	PRINT '@dmc:' + ISNULL(CAST(@dmc AS NVARCHAR(40)),dbo.f_translate('NULL',default))

	set @value=0.0

	BEGIN -- ZZ
		if @entityId=1 -- holowanie 
		begin
			set @twrCode='02'	
			set @isTowing = 1 --> Flaga dla BL, potrzeba do opłaty za sprawę.

			if @towVehicleType=2 -- pojazd patrolowy
			----	
			begin
				if @emptyService=1 or @noFix=1
				begin 
					set @onlyEmptyService=1
					if @type IN (1,2,4,5) 
					begin					
						set @type=4 
						--set @quantity=1
					end
					--else
					--begin
					--	set @quantity=null
					--end
					set @value=70.0
					---------------------------------------------------------
					--if @emptyService = 1
					--begin
					--	insert into @descrTable
					--	select @headerZZ,dbo.f_translate('Pusty wyjazd',default)

					--	insert into @descrTable
					--	select @headerZS,dbo.f_translate('Pusty wyjazd',default)
					--end
					--else if @noFix=1
					--begin
					--	insert into @descrTable
					--	select @headerZZ,dbo.f_translate('Naprawa nieudana',default)

					--	insert into @descrTable
					--	select @headerZS,dbo.f_translate('Naprawa nieudana',default)
					--end
				end
				else
				begin
					if @arrivalKm+@towingKm<=30
					begin
						if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
						begin
							set @value=(@arrivalKm+@towingKm)*3
						end
						else
						begin
							set @value=160.0
						end
					end
					else
					begin
						if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
						begin
							set @value=(@arrivalKm+@towingKm)*3
						end
						else
						begin
							set @value=160.0+(@arrivalKm+@towingKm-30.0)*3
						end
					end

					if @night=1
					begin
						set @value=@value+30
					end
				end
			end
			else -- laweta
			begin
				print '@noFix: ' + CAST(@noFix AS nvarchar(5))
				print '@emptyService: ' + CAST(@emptyService AS nvarchar(5))
				if @emptyService=1 or @noFix=1
				begin
					set @onlyEmptyService=1
					if @type IN (1,2,4,5) 
					begin
						set @type=4 
					end

					if @emptyService = 1
					begin
						set @value=70.0
					end
					else
					begin
						set @value=null
					end

					--insert into @descrTable
					--select @headerZZ,dbo.f_translate('Pusty wyjazd',default)
				
					--insert into @descrTable
					--select @headerZS,dbo.f_translate('Pusty wyjazd',default)
				end
				else
				begin
					declare @basePrice decimal(18,2)
					declare @kmPrice decimal(18,2)

					if @dmc<=2599
					begin
						set @basePrice=120.0
						set @kmPrice=4.0
						--select @arrivalKm, @towingKm arrivalKmtowingKm
					end		
					else if @dmc<=3399
					begin
						set @basePrice=155.00
						set @kmPrice=5.0
					end		
					else if @dmc<=5699
					begin
						set @basePrice=200.00
						set @kmPrice=6.0
					end		

					if @arrivalKm+@towingKm<=45
					begin
						if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
						begin
							set @value=(@arrivalKm+@towingKm)*@kmPrice
						end
						else
						begin
							set @value=@basePrice
						end
					end
					else
					begin
						if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
						begin
							set @value=(@arrivalKm+@towingKm)*@kmPrice
						end
						else
						begin
							set @value=@basePrice+(@arrivalKm+@towingKm-45.0)*@kmPrice
						end
					end

					if @night=1
					begin
						set @value=@value+30
					end
				end
			end
		
			if @isTrialPrice = 1 --> Cennik Testowy dla wybranych kontraktorów
			begin
				print dbo.f_translate('Cennik testowy.',default)

				if @towVehicleType=2 -- pojazd patrolowy
				----	
				begin
					/*
					if @emptyService=1 or @noFix=1
					begin 
						set @onlyEmptyService=1
						if @type IN (1,2,4,5) 
						begin					
							set @type=4 
							--set @quantity=1
						end
						--else
						--begin
						--	set @quantity=null
						--end
						set @value=70.0
						---------------------------------------------------------
						--if @emptyService = 1
						--begin
						--	insert into @descrTable
						--	select @headerZZ,dbo.f_translate('Pusty wyjazd',default)

						--	insert into @descrTable
						--	select @headerZS,dbo.f_translate('Pusty wyjazd',default)
						--end
						--else if @noFix=1
						--begin
						--	insert into @descrTable
						--	select @headerZZ,dbo.f_translate('Naprawa nieudana',default)

						--	insert into @descrTable
						--	select @headerZS,dbo.f_translate('Naprawa nieudana',default)
						--end
					end
					else
					begin
						if @arrivalKm+@towingKm<=30
						begin
							if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
							begin
								set @value=(@arrivalKm+@towingKm)*3
							end
							else
							begin
								set @value=160.0
							end
						end
						else
						begin
							if @partnerStarterCode = @akronimZSADH and @additTownigADH=1
							begin
								set @value=(@arrivalKm+@towingKm)*3
							end
							else
							begin
								set @value=160.0+(@arrivalKm+@towingKm-30.0)*3
							end
						end

						if @night=1
						begin
							set @value=@value+30
						end
					end
					*/

					set @value = 0.00 --> Nie ma cennika dla pojazdu patrolowego
					print dbo.f_translate('Nie ma cennika dla pojazdu patrolowego',default)

				end
				else -- laweta
				begin
					print '@noFix: ' + CAST(@noFix AS nvarchar(5))
					print '@emptyService: ' + CAST(@emptyService AS nvarchar(5))
					if @emptyService=1 or @noFix=1
					begin
						set @onlyEmptyService=1
						if @type IN (1,2,4,5) 
						begin
							set @type=4 
						end

						if @emptyService = 1
						begin
							set @value=70.0
						end
						else
						begin
							set @value=null
						end

						--insert into @descrTable
						--select @headerZZ,dbo.f_translate('Pusty wyjazd',default)
				
						--insert into @descrTable
						--select @headerZS,dbo.f_translate('Pusty wyjazd',default)
					end
					else
					begin
						if @emptyService=1 or @noFix=1
						begin
							set @onlyEmptyService=1
							if @type IN (1,2,4,5) 
							begin
								set @type=4 
							end

							if @emptyService = 1
							begin
								set @value=70.0
							end
							else
							begin
								set @value=null
							end
						end
						else
						begin
							if @arrivalKm+@towingKm<=35
							begin
								if @dmc<=2599
								begin
									set @value=115.0				
								end		
								else if @dmc<=3399
								begin
									set @value=130.00
								end		
								else if @dmc<=5699
								begin
									set @value=170.00
								end		
							end
							else --> Dojazd-Powrót powyżej 35km
							begin
								if @dmc<=2599
								begin
									if (@arrivalKm+@towingKm)*2 < 115
									begin
										set @value=115.0			
									end
									else
									begin
										set @value = (@towingKm+@arrivalKm+@backKm)*2
									end			
								end		
								else if @dmc<=3399
								begin
									if (@arrivalKm+@towingKm)*2 < 130
									begin
										set @value=130.0			
									end
									else
									begin
										set @value = (@towingKm+@arrivalKm+@backKm)*2.5
									end			
								end		
								else if @dmc<=5699
								begin
									if (@arrivalKm+@towingKm)*2 < 170
									begin
										set @value=170.0			
									end
									else
									begin
										set @value = (@towingKm+@arrivalKm+@backKm)*3
									end			
								end	
							end
						end
					end
				end
			end

			if @isTireRepair = 1
			begin
				set @value=80
			end
		end 

		if @entityId=2 -- holowanie przyczepy
		begin
			print dbo.f_translate('na razie nie ma',default)
		end 

		if @entityId=3 -- roboczogodziny
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			set @value=20.00 --*@value
			--select @value
		end 

		if @entityId=4 -- dokumentacja
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			IF @platformCFM = 25
			BEGIN
				set @value=20.0 --> Zdjęcia tylko w LP
			END
			ELSE
			BEGIN
				set @value=NULL
			END
		end 

		if @entityId=5 -- koszty autostrady
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
			print dbo.f_translate('koszty wpisane w raport nh',default)
		
			declare @structureId int
			declare @TypKosztu int 

			DECLARE kur cursor LOCAL for
				select  id 
				from      dbo.attribute_value 
				where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

			OPEN kur;
			FETCH NEXT FROM kur INTO @structureId;
			WHILE @@FETCH_STATUS=0
			BEGIN 
				delete from @values     
				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '638,219,220', --Typ
						@groupProcessInstanceId = @groupProcessId,
						@parentAttributeId=@structureId
                               
					SELECT @TypKosztu=value_int FROM @values

					if @TypKosztu=1 /*autostrada*/
					begin
						delete from @values     
						INSERT  @values EXEC dbo.p_attribute_get2
								@attributePath = '638,219,222', /*wartość*/
								@groupProcessInstanceId = @groupProcessId,
								@parentAttributeId=@structureId

						SELECT @value=value_decimal FROM @values
									
					end
				FETCH NEXT FROM kur INTO @structureId;
			END
			CLOSE kur
			DEALLOCATE kur
		end 

		if @entityId=6 -- wyciąganie z poza drogi
		begin
			print dbo.f_translate('koszt ',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
			set @value=0.0
		end 

		if @entityId=7 -- DO UZUPEŁNIENIA!!! 1301-Części zamienne
		begin
			set @twrCode='01'
			set @value=0.0
		end

		if @entityId=8 -- transport w holowniku
		begin
			set @twrCode='02'
		
			print dbo.f_translate('wg cennika',default)
			declare @persons int
			set @persons=0

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,644,154', @groupProcessInstanceId = @groupProcessId
			SELECT @persons = value_int FROM @values	
		
			if @persons>2
			begin
				set @value=@towingKm*1.5
			end
		end 

		if @entityId=9 -- viatoll
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			DECLARE kur cursor LOCAL for
				select  id 
				from      dbo.attribute_value 
				where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

			OPEN kur;
			FETCH NEXT FROM kur INTO @structureId;
			WHILE @@FETCH_STATUS=0
			BEGIN 
				delete from @values     
				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '638,219,220', --Typ
						@groupProcessInstanceId = @groupProcessId,
						@parentAttributeId=@structureId
                               
					SELECT @TypKosztu=value_int FROM @values

					if @TypKosztu=2 /*viatoll*/
					begin
						delete from @values     
						INSERT  @values EXEC dbo.p_attribute_get2
								@attributePath = '638,219,222', /*wartość*/
								@groupProcessInstanceId = @groupProcessId,
								@parentAttributeId=@structureId

						SELECT @value=value_decimal FROM @values
									
					end
				FETCH NEXT FROM kur INTO @structureId;
			END
			CLOSE kur
			DEALLOCATE kur
		end 

		if @entityId=10 -- naprawa na drodze
		begin
			print dbo.f_translate('wg cennika',default)
			set @twrCode='01'

			if @towVehicleType=2 -- pojazd patrolowy
			begin
				if @emptyService=1 or @noFix=1
				begin
					set @onlyEmptyService=1
					if @type IN (1,2,4,5) 
					begin
						set @type=4	
					end
					set @value=70.0

					print dbo.f_translate('Pusty wyjazd lub naprawa nieudana. Value: ',default) + ISNULL(CAST(@value AS NVARCHAR(30)),dbo.f_translate('NULL',default))
				end
				else
				begin
					if @arrivalKm<=30
					begin
						set @value=160.0
					end
					else
					begin
						set @value=160.0+(@arrivalKm-30.0)*3
					end

					if @night=1
					begin
						set @value=@value+30
					end
				end
			end
			else -- laweta
			begin	
				if @emptyService=1 or @noFix=1
				begin
					set @onlyEmptyService=1
					if @type IN (1,2,4,5) 
					begin
						set @type=4 
					end

					if @emptyService = 1
					begin
						set @value=70.0
					end
					else
					begin
						set @value=null
					end
				end
				else
				begin
					if @dmc<=2599
					begin
						set @basePrice=150.0
						set @kmPrice=4.0					
					end		
					else if @dmc<=3399
					begin
						set @basePrice=190.00
						set @kmPrice=5.0
					end		
					else if @dmc<=5699
					begin
						set @basePrice=240.00
						set @kmPrice=6.0
					end		
			
					if @arrivalKm<=45
					begin
						set @value=@basePrice
					
					end
					else
					begin
						set @value=@basePrice+(@arrivalKm-45.0)*@kmPrice
					end

					if @night=1
					begin
						set @value=@value+30
					end
				end
			end		
		
			if @isTrialPrice = 1 --> Cennik Testowy dla wybranych kontraktorów
			begin
				print dbo.f_translate('Cennik testowy.',default)

				if @towVehicleType=2 -- pojazd patrolowy
				begin
					/*
					if @emptyService=1 or @noFix=1
					begin
						set @onlyEmptyService=1
						if @type IN (1,2,4,5) 
						begin
							set @type=4	
						end
						set @value=70.0

						print dbo.f_translate('Pusty wyjazd lub naprawa nieudana. Value: ',default) + ISNULL(CAST(@value AS NVARCHAR(30)),dbo.f_translate('NULL',default))
					end
					else
					begin
						if @arrivalKm<=30
						begin
							set @value=160.0
						end
						else
						begin
							set @value=160.0+(@arrivalKm-30.0)*3
						end

						if @night=1
						begin
							set @value=@value+30
						end
					end
					*/
					set @value = 0.00 --> Nie ma cennika dla pojazdu patrolowego
					print dbo.f_translate('Nie ma cennika dla pojazdu patrolowego',default)
				end
				else -- laweta
				begin	
					if @emptyService=1 or @noFix=1
					begin
						set @onlyEmptyService=1
						if @type IN (1,2,4,5) 
						begin
							set @type=4 
						end

						if @emptyService = 1
						begin
							set @value=70.0
						end
						else
						begin
							set @value=null
						end
					end
					else
					begin
						if @arrivalKm+@backKm<=35
						begin
							if @dmc<=2599
							begin
								set @value=145.0				
							end		
							else if @dmc<=3399
							begin
								set @value=160.00
							end		
							else if @dmc<=5699
							begin
								set @value=200.00
							end		
						end
						else --> Dojazd-Powrót powyżej 35km
						begin
							if @dmc<=2599
							begin
								if (@arrivalKm+@backKm)*2 < 145
								begin
									set @value=145.0			
								end
								else
								begin
									set @value = (@arrivalKm+@backKm)*2
								end			
							end		
							else if @dmc<=3399
							begin
								if (@arrivalKm+@backKm)*2 < 160
								begin
									set @value=160.0			
								end
								else
								begin
									set @value = (@arrivalKm+@backKm)*2.5
								end			
							end		
							else if @dmc<=5699
							begin
								if (@arrivalKm+@backKm)*2 < 145
								begin
									set @value=145.0			
								end
								else
								begin
									set @value = (@arrivalKm+@backKm)*2
								end			
							end	
						end

						if @night=1
						begin
							set @value = @value + 30
						end

						set @value = @value + 30 --> Dodatek za skuteczną naprawę.
					end
				end		
			end

			if @isTireRepair = 1
			begin
				set @value=80
			end
		end 

		if @entityId=11 -- taxi
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			declare @km int

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,154', @groupProcessInstanceId = @groupProcessId
			SELECT @persons = value_int FROM @values	

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,647', @groupProcessInstanceId = @groupProcessId
			SELECT @km = value_int FROM @values	

	
			set @value=@km*1.5

			if @night=1
			begin
				set @value=@value+30
			end
		end 

		if @entityId=12 -- Nowość --> 9901-Inne
		begin
			DECLARE @serviceStatusId INT = NULL

			DELETE FROM @values
			INSERT	@values EXEC dbo.p_attribute_get2 
					@attributePath = '560', 
					@groupProcessInstanceId = @groupProcessId
			declare @NH int = (Select top 1 value_int from @values where value_int is not null)

			IF @NH IS NOT NULL -- Naprawa czy Holowanie
			BEGIN
				SET @serviceStatusId = (select top 1 serviceId from service_status WITH (NOLOCK) where group_process_id = @groupProcessId and serviceId = @NH order by updated_at DESC)
			END
			ELSE
			BEGIN
				SET @serviceStatusId = (select top 1 serviceId from service_status WITH (NOLOCK) where group_process_id = @groupProcessId order by updated_at DESC)
			END

			SET @twrCode = case when @serviceStatusId = 1  then '02' -- Holowanie
								when @serviceStatusId = 2  then '01' -- Naprawa
								when @serviceStatusId = 3  then '04' -- Auto zastępcze
								when @serviceStatusId = 4  then '05' -- Nocleg
								when @serviceStatusId = 5  then '07' -- Taxi
								when @serviceStatusId = 6  then '06' -- Podróż
								when @serviceStatusId = 7  then '10' -- Transport
								when @serviceStatusId = 8  then '04' -- Części zamienne
								when @serviceStatusId = 9  then '17' -- Kredyt
								when @serviceStatusId = 10 then '19' -- Przekazywanie informacji
								--when @serviceStatusId = 11 then '' -- 
								when @serviceStatusId = 12 then '0' + CAST(@NH AS nvarchar(2)) -- Monit. napr. warszt.
								when @serviceStatusId = 13 then '00' -- Odwołanie usługi
								when @serviceStatusId = 14 then '0' + CAST(@NH AS nvarchar(2)) -- Parking
								when @serviceStatusId = 15 then '02'  -- Holowanie poszkodowanego
								when @serviceStatusId = 16 then '11' -- Trans. nienapr. pojazdu
								when @serviceStatusId = 17 then '24' -- Home Assistance
								when @serviceStatusId = 18 then '16' -- Medical
								when @serviceStatusId = 19 then '00' -- Porada ALPHABET - Lokalizacja serwisu
								when @serviceStatusId = 20 then '00' -- Porada ALPHABET - udzielenie informacji
								when @serviceStatusId = 21 then '00' -- Złomowanie
							else '00' end

			EXEC [dbo].[p_f_other_costs] @groupId=@groupProcessId,@value = @value OUTPUT

			-- Jeśli naprawa lub holowanie i są dodatkowe koszty to dokładamy je do pozycji inne.
			IF (SELECT DISTINCT TOP 1 LEFT(step_id,4) FROM [AtlasDB_v].[dbo].[process_instance] WHERE group_process_id = @groupProcessId) = 1009 AND @value >= 70
			BEGIN
				SET @value = @value - 70
			END
		end 

		if @entityId=14 -- dodatkowe koszty
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			DECLARE @exist INT
			IF (SELECT COUNT(1) FROM [dbo].[process_instance] WHERE step_id LIKE '1018%' AND group_process_id=@groupProcessId) > 0 
			BEGIN --> Transport
				SET @exist=1
				DELETE FROM @values
				INSERT @values EXEC dbo.p_attribute_get2
							@attributePath = '62,319',
							@groupProcessInstanceId =@groupProcessId
				SET @value = (SELECT value_int from @values)

				DECLARE @CostType VARCHAR(20)
				DELETE FROM @values
				INSERT @values EXEC dbo.p_attribute_get2
							@attributePath = '62,747',
							@groupProcessInstanceId =@groupProcessId
				select @CostType=value_string from @values

				IF (@CostType = dbo.f_translate('brutto',default))
				BEGIN
					SET @value = @value / 1.23
				END
				ELSE
				BEGIN
					SET @value = @value
				END
			END
			ELSE
			BEGIN --> Holowanie lub Naprawa
				EXEC [dbo].[p_f_towing_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=6,@value=@value OUTPUT
			END
		end 

		if @entityId=16 -- dźwig
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			if @dmc<=2599
			begin
				set @value=150.0		
			end		
			else if @dmc<=3399
			begin
				set @value=170.0
			end		
			else if @dmc<=5699
			begin
				set @value=170.0
			end		
		end 

		if @entityId=17 -- NpT
		begin
			set @twrCode='01'
			set @value=0
		end 

		if @entityId=18 -- Kredyt
		begin
			set @twrCode='17'
		
			set @value=NULL -- KR Zawsze jest brak ZZ dla kredytu
		end 

		if @entityId=19 -- lot
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='06'
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '147,746', @groupProcessInstanceId = @groupProcessId
			SELECT @value = value_decimal FROM @values			
		end 

		IF @entityId=21 -- Taxi
		BEGIN
			PRINT dbo.f_translate('koszt taxi wpisany',default)
			SET @twrCode='07'
			SET @value=0

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '152,319', @groupProcessInstanceId = @groupProcessId			
			SET @value = (SELECT value_int FROM @values)

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '152,556', @groupProcessInstanceId = @groupProcessId
			SELECT @currency = value_string FROM @values	

			IF @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			BEGIN
				SET @value = dbo.FN_Exchange(@value,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate()) 
			END

			IF ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)) <> dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			BEGIN
				SET @value = 0
			END 
			ELSE
			BEGIN
				IF NOT EXISTS(SELECT 1 FROM process_instance WHERE step_id = '1016.021' AND group_process_id=@groupProcessId)
				BEGIN
					PRINT dbo.f_translate('Proces Taxi odpalony z kafelka',default)
					IF ISNULL(@value,0)=0
					BEGIN -- Jeżeli koszty rzeczywiste są nie wpisane to zostaną wyliczone z kilometrów.
						DELETE FROM @values
						INSERT @values EXEC p_attribute_get2 @attributePath = '152,647', @groupProcessInstanceId = @groupProcessId
						DECLARE @taxiDistance INT = (SELECT value_int FROM @values)

						SET @value=50			
						IF @taxiDistance>20.0
						BEGIN
							SET @value=@value+(@taxiDistance-20.0)*3.0
						END
					END
				END
				SET @value = ISNULL(@value,0)
			END

			-- Sprawdzenie czy netto/brutto (20180816)
			DELETE FROM @values -- Zamiana z brutto na netto
			INSERT  @values EXEC dbo.p_attribute_get2
					@attributePath = '152,747', -- • Brutto/netto
					@groupProcessInstanceId = @groupProcessId                              
			SELECT @CostType=value_string FROM @values
			IF ((@CostType = dbo.f_translate('brutto',default)) AND ISNULL(@value,0)>0)
			BEGIN
				SET @value = @value /1.23 -- Zamiana kosztu brutto na netto
			END

			declare	@linkedService int = (select top 1 value_int from attribute_value with(nolock) where group_process_instance_id=@groupProcessId and attribute_path='856' and value_int is not null)		

			if @night=1 and @linkedService is null
			begin
				set @value=@value+30
			end
		END 

		if @entityId=22 -- części zamienne
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='13'
			set @value=0
		end 

		if @entityId=23 -- pociąg
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='06'
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '147,746', @groupProcessInstanceId = @groupProcessId
			SELECT @value = value_decimal FROM @values	
		end 

		if @entityId=25 -- parking
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			DECLARE kur cursor LOCAL for
				select  id 
				from      dbo.attribute_value 
				where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

			OPEN kur;
			FETCH NEXT FROM kur INTO @structureId;
			WHILE @@FETCH_STATUS=0
			BEGIN 
				delete from @values     
				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '638,219,220', --Typ
						@groupProcessInstanceId = @groupProcessId,
						@parentAttributeId=@structureId
                               
					SELECT @TypKosztu=value_int FROM @values

					if @TypKosztu=5 /*parking*/
					begin
						delete from @values     
						INSERT  @values EXEC dbo.p_attribute_get2
								@attributePath = '638,219,222', /*wartość*/
								@groupProcessInstanceId = @groupProcessId,
								@parentAttributeId=@structureId

						SELECT @value=value_decimal FROM @values
									
					end
				FETCH NEXT FROM kur INTO @structureId;
			END
			CLOSE kur
			DEALLOCATE kur
		end 

		if @entityId=26 -- porada
		begin
			set @twrCode='19'
			set @value=null
			set @IsAdviceInCase = 1
		end 

		if @entityId=27 -- 1951-Formularz Ford Vignale
		begin
			set @twrCode='19'
			set @value=null
		end 

		if @entityId=29 /*wynajem auta. Jeśli wynajmuje auto serwis naprawiający to sam rozlicza kwoty wynajmu z producentem. */
		begin
			set @twrCode='04'
			declare @IleDni int
			EXEC [p_f_replacement_days] @groupProcessInstanceId=@groupProcessId,@quantity=@IleDni OUTPUT,@value=@value OUTPUT
			print dbo.f_translate('Wartość wynajmu całkowita: ',default) + cast (@value as varchar(20))
			SET @value = (@value /  @IleDni)
		end 

		if @entityId=30 -- podstawienie
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			-- Pobieranie wartości, Krzysztof
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,790,240,222', @groupProcessInstanceId = @groupProcessId
			SELECT @value = value_decimal FROM @values	
			--

		end 

		if @entityId=31 -- odbiór
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			-- Pobieranie wartości, Krzysztof
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,787,240,222', @groupProcessInstanceId = @groupProcessId
			SELECT @value = value_decimal FROM @values	
			--
		end 

		if @entityId=32 -- wynajm poza godzinami
		begin
			print dbo.f_translate('koszt wpisany / 20 pln',default)
			set @twrCode='04'

			-- Podstawienie poza godzinami pracy wypożyczalni
			declare @substitutionValue decimal(20,4) = 0
			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=1,@value=@substitutionValue output
			set @substitutionValue = isnull(@substitutionValue,0)

			-- Zwrot poza godzinami pracy wypożyczalni
			declare @returnValue decimal(20,4) = 0
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=2,@value=@returnValue output		
			set @returnValue = isnull(@returnValue,0)

			if (@substitutionValue != 0 AND @returnValue = 0) -- podstawienie
			begin
				set @value = @substitutionValue
			end

			else if (@substitutionValue = 0 AND @returnValue != 0) -- zwrot
			begin
				set @value = @returnValue
			end

			else if (@substitutionValue != 0 AND @returnValue != 0) -- podstawienie i zwrot
			begin
				set @value = @substitutionValue + @returnValue
			end
		end

		if @entityId=34 -- dodatkowe ubezp
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=4,@value=@value OUTPUT
		end 

		if @entityId=35 -- hak holowniczy
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=5,@value=@value OUTPUT
		end 

		if @entityId=36 -- fotelik
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=6,@value=@value OUTPUT
		end 

		if @entityId=37 -- bagażnik sam.
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=7,@value=@value OUTPUT
		end 

		if @entityId=38 -- wyjazd za granicę
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=3,@value=@value OUTPUT
		end 

		if @entityId=40 --> 0412-Dotankowanie
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=10,@value=@value output
		end 

		if @entityId=41 --> 0413-Uszkodzenie
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=9,@value=@value output
		end 

		if @entityId=42 --> 0414-Mycie
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=11,@value=@value output
		end 

		if @entityId=43 -- nocleg
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='05'

			EXEC [dbo].[p_f_accommodation] @groupProcessInstanceId=@groupProcessId,@value=@value OUTPUT
		end 

		if @entityId=44 -- śniadanie
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='05'

			EXEC [dbo].[p_f_breakfast] @groupProcessInstanceId=@groupProcessId,@value=@value OUTPUT
		end 

		if @entityId=63 -- transport MP  to jest netto ZZ
		begin
			SET @serviceStatusId = (select top 1 serviceId from service_status WITH (NOLOCK) where group_process_id = @groupProcessId order by updated_at DESC)
			SET @twrCode = case when @serviceStatusId = 7  then '10' -- Transport
								when @serviceStatusId = 16 then '11' -- Trans. nienapr. pojazdu
							else '10' end
			SET @isTransport = 1
			SELECT @value=value_int FROM attribute_value WHERE group_process_instance_id =@groupProcessId AND attribute_path = '62,319'
		end 

		if @entityId=64 -- Home Assistance
		begin
			set @twrCode='24'
			exec [dbo].[p_f_homeAssistance] @groupId=@groupProcessId,@value=@value output
		end 

		if @entityId=65 -- Medical
		begin
			set @twrCode='16'
			set @value=(select top 1 value_decimal from attribute_value with(nolock) where group_process_instance_id=@groupProcessId 
						and attribute_path='991,746' and value_decimal is not null)
		end 

		if @entityId in (66,69,71,73) --> Usługi Truck
		begin
			set @twrCode = case when @entityId=66 then '31' --> 3101-Naprawa na drodze Truck
								when @entityId=69 then '32' --> Holowanie Truck
								when @entityId=71 then '33' --> Dowóz gotówki na inne potrzeby
								when @entityId=73 then '34' --> Naprawa mechaniczna Truck
								else '00' end
			set @value = (select top 1 value_decimal from attribute_value with(nolock) where group_process_instance_id=@groupProcessId and attribute_path='1028,1029' and value_decimal is not null)
			set @value = isnull(@value,0.00)

			IF ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default))<>dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default) AND @entityId<>71 --> Za granicą tylko importuje się dowóz gotówki
			BEGIN
				set @value = NULL
			END
		end 

		if @entityId in (1,10) and @isGopNH=1 --> Organizowanie usługi N/H na GOP.
		begin
			set @value=(select top 1 value_int from attribute_value with(nolock) where group_process_instance_id=@groupProcessId 
						and attribute_path='638,957' and value_int is not null)

			--if @XL_FlagaNBZZ = 'B'
			--begin
			--??????????????????????????
			--end
		end

		/*	415	Audi Gwarancja Mobilności
			418	Volkswagen osobowe Gwarancja Mobilności
			420	Volkswagen użytkowe Gwarancja Mobilności
			421	Skoda Gwarancja Mobilności	
			Aso rozlicza swoje koszty z producentem - bez udziału Starter24		
		*/
		if (@type = 1 AND @czyASO = 1 AND @programId IN (415,418,420,421) AND @twrCode IN ('02','04')) 
		or @skipStarterCodeBL=1 
		or (@platformCFM = 5 and @twrCode='04') --> ADAC nie przenosimy wynajmów
		BEGIN
			set @value=NULL

			print 'Pomija wstawienie ZamElem(ZZ). (if (@type = 1 AND @czyASO = 1 AND @programId IN (415,418,420,421) AND @twrCode IN (''02'',''04'')) or @skipStarterCodeBL=1 ALBO MW DLA ADAC'
			print '@czyASO: ' + ISNULL(CAST(@czyASO AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@type: ' + ISNULL(CAST(@type AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@quantity: ' + ISNULL(CAST(@quantity AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@skipStarterCodeBL: ' + ISNULL(CAST(@skipStarterCodeBL AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@platformCFM: ' + ISNULL(CAST(@platformCFM AS NVARCHAR(50)),dbo.f_translate('NULL',default))
			print '@twrCode: ' + ISNULL(CAST(@twrCode AS NVARCHAR(50)),dbo.f_translate('NULL',default))
		END

		IF @programId IS NULL
		BEGIN
			set @value=NULL
			print 'BRAK PROGRAMU! nie importuje'
		END

	END

	print '@value: ' + ISNULL(CAST(@value AS NVARCHAR(30)),dbo.f_translate('NULL',default)) + ' @quantity: ' +  ISNULL(CAST(@quantity AS NVARCHAR(30)),dbo.f_translate('NULL',default))

	set @currency=dbo.f_translate(dbo.f_translate('PLN',default),default)
	set @nettoBrutto='N'
	set @vatGroup='23'
	set @value=@value*@quantity 
	
	print dbo.f_translate('TwrCode - ',default)+cast(isnull(@twrCode,'---') as varchar(20))+' entityId='+cast(@entityId as varchar(20))+' value='+isnull(cast(@value as varchar(20)),dbo.f_translate('NULL',default))
	----------------------------------------------------------
	set @quantityZ=ceiling(@quantity)

	if @entityId<>17 /*wykluczenie NpT*/
	begin
		Print dbo.f_translate('ZZ: Koszt value przed dodaniem do elem: ',default) + ISNULL(CAST(@value AS NVARCHAR(20)),dbo.f_translate('NULL',default))

		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			groupProcessId,
			programId
		)	
		SELECT  
		@headerZZ,
		@quantityXL,    ---@quantityZ MP
		@twrCode,
		@value,
		@currency,
		@entityValueId, 
		@entityId,
		@groupProcessId,--CASE WHEN (@reverseGroupId < 0) THEN @reverseGroupId ELSE @groupProcessId END,
		@programId
	end
	print @value

	print '-----------------------------------------'

	------------------------------------------------------------
	-- Tutaj wpisać wszelkie zmienne i dane apropos entity ZS --
	------------------------------------------------------------

	declare @valueZS decimal(18,2)

	set @valueZS=0.0
	set @quantityZ=@quantity
	declare @summaryQuantity int = null


	IF		@programId IN (368,402,414,415,416,417,418,419,420,421,424,425,426,461,462,463,464) or @platformCFM = 76 --> VGP or Cupra Assistance
	BEGIN
		IF @type IN (1,3,4,5) --> Producent, Dealer, Klient, PZ
		BEGIN
			if @entityId=1 -- 0201-Holowanie
			begin
				if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
				begin
					if @towingKm<=15 
					begin
						set @valueZS=174.74
					end
					else
					begin
						set @valueZS=174.74+(@towingKm-15.0)*4.62
					end	
				end
				else
				begin
					set @value=0.0
				end
			end

			if @entityId=3 -- 0205-Pomoc/Oczekiwanie
			begin
				set @valueZS=20.0
			end

			if @entityId=10 -- 0101-Naprawa na drodze
			begin
				if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
				begin
					set @valueZS=203.34
				end
				else
				begin
					set @valueZS=0.0
				end
			end

			if @entityId=16 -- 0203-Dźwig
			begin
				if @dmc<=2599
				begin
					set @valueZS=150.0		
				end		
				else if @dmc<=3399
				begin
					set @valueZS=170.0
				end		
				else if @dmc<=5699
				begin
					set @valueZS=170.0
				end
			end

			if @entityId=17 /*Naprawa przez telefon*/
			begin
				set @valueZS=110.74
			end

			if @entityId=18 -- Kredyt
			begin
				set @twrCode='17'
		
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '556', @groupProcessInstanceId = @groupProcessId
				SELECT @currency = value_string FROM @values	

				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '732', @groupProcessInstanceId = @groupProcessId
				SELECT @valueZS = value_decimal FROM @values	

				--if @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
				--begin
				--	set @valueZS = dbo.FN_Exchange(@valueZS,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate()) 
				--end
			end 

			if @entityId=21 -- taxi
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='07'
				set @valueZS=0.0 -- Koszty rzeczywiste, przepisane z ZZ
				--delete from @values
				--INSERT @values EXEC p_attribute_get2 @attributePath = '152,319', @groupProcessInstanceId = @groupProcessId
				--SELECT @valueZS = value_decimal FROM @values	
			end 

			if @entityId=25 -- parking
			begin
				print dbo.f_translate('koszty wpisane parking',default)
				if @towing=2
				begin
					set @twrCode='01'
				end
				else
				begin
					set @twrCode='02'
				end

				DECLARE kur cursor LOCAL for
					select  id 
					from      dbo.attribute_value 
					where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

				OPEN kur;
				FETCH NEXT FROM kur INTO @structureId;
				WHILE @@FETCH_STATUS=0
				BEGIN 
					delete from @values     
					INSERT  @values EXEC dbo.p_attribute_get2
							@attributePath = '638,219,220', --Typ
							@groupProcessInstanceId = @groupProcessId,
							@parentAttributeId=@structureId
                               
						SELECT @TypKosztu=value_int FROM @values

						if @TypKosztu=5 /*parking*/
						begin
							delete from @values     
							INSERT  @values EXEC dbo.p_attribute_get2
									@attributePath = '638,219,222', /*wartość*/
									@groupProcessInstanceId = @groupProcessId,
									@parentAttributeId=@structureId

							SELECT @valueZS=value_decimal FROM @values
									
						end
					FETCH NEXT FROM kur INTO @structureId;
				END
				CLOSE kur
				DEALLOCATE kur
			end 

			if @entityId=29 -- Wynajem pojazdu zastępczego
			begin
				set @twrCode='04'
				EXEC [p_f_replacement_days] @groupProcessInstanceId=@groupProcessId,@quantity=@IleDni OUTPUT,@value=@valueZS OUTPUT
				print dbo.f_translate('ZS: reszta Wartość wynajmu całkowita: ',default) + cast (@value as varchar(20))
				set @valueZS=(@valueZS/@IleDni)
			end

			if @entityId=30 -- podstawienie
			begin
				--print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				-- Pobieranie wartości, Krzysztof
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,790,240,222', @groupProcessInstanceId = @groupProcessId
				SELECT @valueZS = value_decimal FROM @values	
				--
				--print dbo.f_translate('TYP: ',default) + CAST(@type AS VARCHAR(10)) + dbo.f_translate(' ValZS: ',default) + CAST(@valueZS AS VARCHAR(10))
			end 

			if @entityId=31 -- odbiór
			begin
				--print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				-- Pobieranie wartości, Krzysztof
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,787,240,222', @groupProcessInstanceId = @groupProcessId
				SELECT @valueZS = value_decimal FROM @values	
				--

			end 

			if @entityId=32 -- wynajm poza godzinami
			begin
				set @twrCode='04'

				-- Podstawienie poza godzinami pracy wypożyczalni
				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=1,@value=@substitutionValue output
				set @substitutionValue = isnull(@substitutionValue,0)

				-- Zwrot poza godzinami pracy wypożyczalni
				exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=2,@value=@returnValue output		
				set @returnValue = isnull(@returnValue,0)

				if (@substitutionValue != 0 AND @returnValue = 0) -- podstawienie
				begin
					set @valueZS = @substitutionValue
				end

				else if (@substitutionValue = 0 AND @returnValue != 0) -- zwrot
				begin
					set @valueZS = @returnValue
				end

				else if (@substitutionValue != 0 AND @returnValue != 0) -- podstawienie i zwrot
				begin
					set @valueZS = @substitutionValue + @returnValue
				end
			end 

			if @entityId=34 -- dodatkowe ubezp
			begin
				--print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=4,@value=@valueZS OUTPUT
			end 

			if @entityId=35 -- hak holowniczy
			begin
				--print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=5,@value=@valueZS OUTPUT
			end 

			if @entityId=36 -- fotelik
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=6,@value=@valueZS OUTPUT
			end 

			if @entityId=37 -- bagażnik sam.
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=7,@value=@valueZS OUTPUT
			end 

			if @entityId=38 -- wyjazd za granicę
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='04'

				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=3,@value=@valueZS OUTPUT
			end 

			if @entityId=63 -- Transport pojazdu Producent
			begin			
				set @valueZS=(SELECT value_int FROM attribute_value WHERE group_process_instance_id =@groupProcessId AND attribute_path = '62,319')			
			end 

			/*	415	Audi Gwarancja Mobilności
				418	Volkswagen osobowe Gwarancja Mobilności
				420	Volkswagen użytkowe Gwarancja Mobilności
				421	Skoda Gwarancja Mobilności	
				Aso rozlicza swoje koszty z producentem - bez udziału Starter24	*/
			if (@type = 1 AND @czyASO = 1 AND @programId IN (415,418,420,421) AND @twrCode IN ('02','04')) 
			BEGIN
				print dbo.f_translate('Pomija ZS',default)
				set @valueZS=NULL
			END
		END

		IF @type = 2 --> Ubezpieczyciel
		BEGIN
			if @entityId=1 -- 0201-Holowanie
			begin
				if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
				begin
					set @valueZS=175.68/1.23
					if @towingKm>25.0
					begin
						set @valueZS=@valueZS+(@towingKm-25.0)*5.86/1.23
					end
				end	
				else
				begin -- brak km holowania za granicą (+113.98 jeśli jest 15-30km, powyżej 30km * 1.26 eur/KM)
					set @valueZS=dbo.FN_Exchange(75.70,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate())/1.23		
				end
			end

			if @entityId=3 -- 0205-Pomoc/Oczekiwanie
			begin
				set @valueZS=20.0
			end

			if @entityId=10 -- 0101-Naprawa na drodze
			begin
				if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
				begin
					set @valueZS=196.42/1.23
					if (@arrivalKm+@backKm)>25.0
					begin
						set @valueZS=@valueZS+(@arrivalKm+@backKm-25.0)*1.83/1.23
					end
				end
				else
				begin
					set @valueZS=dbo.FN_Exchange(60.51,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate())/1.23
				end
			end

			if @entityId=12 -- Nowość --> 9901-Inne
			begin
				EXEC [dbo].[p_f_other_costs] @groupId=@groupProcessId,@value = @valueZS OUTPUT

				-- Jeśli naprawa lub holowanie i są dodatkowe koszty to dokładamy je do pozycji inne.
				IF (SELECT DISTINCT TOP 1 LEFT(step_id,4) FROM [AtlasDB_v].[dbo].[process_instance] WHERE group_process_id = @groupProcessId) = 1009 AND @value >= 70
				BEGIN
					SET @valueZS = @valueZS - 70
				END
			end 

			if @entityId=16 -- 0203-Dźwig
			begin
				if @dmc<=2599
				begin
					set @valueZS=150.0		
				end		
				else if @dmc<=3399
				begin
					set @valueZS=170.0
				end		
				else if @dmc<=5699
				begin
					set @valueZS=170.0
				end
			end
		
			if @entityId=21 -- taxi
			begin
				print dbo.f_translate('koszt wpisany',default)
				set @twrCode='07'
				set @valueZS=0.0 -- Koszty rzeczywiste, przepisane z ZZ
				--delete from @values
				--INSERT @values EXEC p_attribute_get2 @attributePath = '152,319', @groupProcessInstanceId = @groupProcessId
				--SELECT @valueZS = value_decimal FROM @values	
			end 

			if @entityId=25 -- parking
			begin
				print dbo.f_translate('koszty wpisane parking',default)
				if @towing=2
				begin
					set @twrCode='01'
				end
				else
				begin
					set @twrCode='02'
				end

				DECLARE kur cursor LOCAL for
					select  id 
					from      dbo.attribute_value 
					where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

				OPEN kur;
				FETCH NEXT FROM kur INTO @structureId;
				WHILE @@FETCH_STATUS=0
				BEGIN 
					delete from @values     
					INSERT  @values EXEC dbo.p_attribute_get2
							@attributePath = '638,219,220', --Typ
							@groupProcessInstanceId = @groupProcessId,
							@parentAttributeId=@structureId
                               
						SELECT @TypKosztu=value_int FROM @values

						if @TypKosztu=5 /*parking*/
						begin
							delete from @values     
							INSERT  @values EXEC dbo.p_attribute_get2
									@attributePath = '638,219,222', /*wartość*/
									@groupProcessInstanceId = @groupProcessId,
									@parentAttributeId=@structureId

							SELECT @valueZS=value_decimal FROM @values
									
						end
					FETCH NEXT FROM kur INTO @structureId;
				END
				CLOSE kur
				DEALLOCATE kur
			end 

			if @entityId=29 -- Wynajem pojazdu zastępczego wg cennika Compensy dla nie ASO: tj. nie z wypozyczlni typu panek...
			begin
				set @twrCode='04'
				/*	415	Audi Gwarancja Mobilności
					418	Volkswagen osobowe Gwarancja Mobilności
					420	Volkswagen użytkowe Gwarancja Mobilności
					421	Skoda Gwarancja Mobilności	*/
				if ( (@czyASO=0) or (@programId in (415,418,420,421)) )
				begin				
					declare @dni int
					declare @markaModelAutaWyp int

					delete from @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
					SELECT @markaModelAutaWyp = value_int FROM @values	
				
					declare @class int
					select	@class=argument3
					from	dbo.dictionary 
					where	typeD='makeModel' and 
							value=@markaModelAutaWyp

					-- SELECT DO WYJĄTKÓW BENEFIA
					declare @klasaBenefia int = (SELECT [classBenefia] 
												FROM [AtlasDB_v].[dbo].[classException] 
												WHERE makeModelId = @markaModelAutaWyp)

					if (@czyASO=0 AND ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)) IN (dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default),'PL',dbo.f_translate('Poland',default)))
					begin 
						set @valueZS=0.01
						IF @klasaBenefia IS NOT NULL -- Cennik Benefia, wg. wykluczeń
						BEGIN
							if @klasaBenefia in (1)
							begin
								set @valueZS=107.57
							end

							if @klasaBenefia in (2)
							begin
								set @valueZS=120.15
							end

							if @klasaBenefia in (3)
							begin
								set @valueZS=132.84
							end

							if @klasaBenefia in (4)
							begin
								set @valueZS=148.86
							end

							if @klasaBenefia in (5)
							begin
								set @valueZS=197.82
							end

							if @klasaBenefia in (6)
							begin
								set @valueZS=214.01
							end

							if @klasaBenefia in (7)
							begin
								set @valueZS=169.89
							end

							if @klasaBenefia in (8)
							begin
								set @valueZS=261.28
							end

							if @klasaBenefia in (9)
							begin
								set @valueZS=171.62
							end

							if @klasaBenefia in (10)
							begin
								set @valueZS=214.05
							end

							if @klasaBenefia in (11)
							begin
								set @valueZS=334.50
							end

							if @klasaBenefia in (12)
							begin
								set @valueZS=230.71
							end

							if @klasaBenefia in (13)
							begin
								set @valueZS=283.00
							end

							if @klasaBenefia in (14)
							begin
								set @valueZS=290.99
							end

							if @klasaBenefia in (15)
							begin
								set @valueZS=353.62
							end

							if @klasaBenefia in (16)
							begin
								set @valueZS=487.45
							end

							if @klasaBenefia in (17)
							begin
								set @valueZS=535.12
							end

							if @klasaBenefia in (18)
							begin
								set @valueZS=319.45
							end

							if @klasaBenefia in (19)
							begin
								set @valueZS=351.24
							end

							if @klasaBenefia in (20)
							begin
								set @valueZS=373.80
							end
							if @klasaBenefia in (21)
							begin
								set @valueZS=162.60
							end
						END
						ELSE -- Cennik Starter24
						BEGIN
						
							if @class in (1,2)
							begin
								set @valueZS=107.57
							end

							if @class in (3)
							begin
								set @valueZS=120.15
							end

							if @class in (4)
							begin
								set @valueZS=171.62
							end

							--if @class in (5)
							--begin
							--	set @valueZS=132.84
							--end
				
							if @class in (5)
							begin
								set @valueZS=230.71
							end

							if @class in (6)
							begin
								set @valueZS=319.45
							end

							if @class in (7)
							begin
								set @valueZS=148.86
							end
					
							if @class in (9)
							begin
								set @valueZS=283.00
							end

							if @class in (10)
							begin
								set @valueZS=214.05
							end

							if @class in (11)
							begin
								set @valueZS=197.82
							end

							if @class in (13)
							begin
								set @valueZS=214.01
							end

							if @class in (15)
							begin
								set @valueZS=351.24
							end

							if @class in (16)
							begin
								set @valueZS=290.99
							end

							if @class in (18)
							begin
								set @valueZS=373.80
							end

							if @class in (19)
							begin
								set @valueZS=353.62
							end

							if @class in (21)
							begin
								set @valueZS=535.12
							end

							if @class in (22)
							begin
								set @valueZS=487.45
							end

							if @class in (23)
							begin
								set @valueZS=169.89
							end

							if @class in (24)
							begin
								set @valueZS=261.28
							end

							if @class in (25)
							begin
								set @valueZS=334.50
							end
						end
					end
					else -- Aso 1
					begin
						set @valueZS=0.00
					end
			
					if(@IsDealerCall = 1)
					begin
						EXEC [p_f_replacement_days] @groupProcessInstanceId=@groupProcessId,@quantity=@dni OUTPUT,@value = @valueZS OUTPUT
					end
					else
					begin			
						EXEC [p_f_replacement_days] @groupProcessInstanceId=@groupProcessId,@quantity=@dni OUTPUT
						set @valueZS=@valueZS*@dni--@quantityZ=@dni
					end
					print '-------'
					print '------- ZS:U Wartość wynajmu całkowita: ' + cast (@value as varchar(20))
					print '-------'
					---------------------------------------					
				end			
			end

			if (@entityId IN (30,31)) -- 0402-Podstawienie, 0403-Odbiór od klienta, 0404-Wynajem poza godzinami --> Przepisz wartość z ZZ
			begin
				SET @valueZS = @value
				print '@valueZS: ' + CAST(@valueZS AS VARCHAR(30)) + ' @value:' + CAST(@value AS VARCHAR(30))
			end

			if @entityId=32
			begin
				-- Podstawienie poza godzinami pracy wypożyczalni
				EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=1,@value=@substitutionValue output
				set @substitutionValue = isnull(@substitutionValue,0)

				-- Zwrot poza godzinami pracy wypożyczalni
				exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=2,@value=@returnValue output		
				set @returnValue = isnull(@returnValue,0)

				if (@substitutionValue != 0 AND @returnValue = 0) -- podstawienie
				begin
					set @valueZS = @substitutionValue
				end

				else if (@substitutionValue = 0 AND @returnValue != 0) -- zwrot
				begin
					set @valueZS = @returnValue
				end

				else if (@substitutionValue != 0 AND @returnValue != 0) -- podstawienie i zwrot
				begin
					set @valueZS = @substitutionValue + @returnValue
				end
			end

			if @entityId=63 -- MP Transport pojazdu Compensa
			begin
				set @twrCode='10'
				SELECT @valueZS=value_int FROM attribute_value WHERE group_process_instance_id =@groupProcessId AND attribute_path = '62,319'
			
			end 
		END
	END
	ELSE IF @programId IN (429,430,431,432,433,434,435,436,437,438,439,440,441) --> Ford, Ford Vignale
	BEGIN
		IF @entityId=1 -- 0201-Holowanie -- DOPISAC pusty wyjazd = 0PLN i naprawa nieudana na kontrahenta z 111111
		BEGIN
			set @twrCode='02'

			if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				if @programId = 430 -- Ford Assistance 12 Polska
				begin
					if @dmc<3500
					begin
						set @basePrice=178.40
						set @kmPrice=6.80
					end		
					else if 3500<=@dmc AND @dmc<=7500
					begin
						set @basePrice=756.50
						set @kmPrice=24.60
					end
					else -- powyżej DMC 7500
					begin
						set @basePrice=0.00
						set @kmPrice=0.00
					end

					if @towingKm<=25
					begin
						set @valueZS=@basePrice
					end
					else
					begin
						set @valueZS=@basePrice+(@towingKm-25.0)*@kmPrice
					end
				end

				if @programId IN (429,435,436,437,438) -- Ford Euroservice,Ford Ubezpieczenia,Ford Mini Car Assistance, Polska
				begin
					if @dmc<3500
					begin
						set @basePrice=228.80
						set @kmPrice=8.00
					end		
					else if 3500<=@dmc AND @dmc<=7500
					begin
						set @basePrice=756.50
						set @kmPrice=24.60
					end
					else -- powyżej DMC 7500
					begin
						set @basePrice=0.00
						set @kmPrice=0.00
					end

					if @towingKm<=25
					begin
						set @valueZS=@basePrice
					end
					else
					begin
						set @valueZS=@basePrice+(@towingKm-25.0)*@kmPrice
					end
				end

				if @programId IN (431,432,433,434) -- Ford Protect,Ford Inne Marki Wszystkie, Polska
				begin
					if @dmc<3500
					begin
						set @basePrice=186.00
						set @kmPrice=6.50
					end		
					else if 3500<=@dmc AND @dmc<=7500
					begin
						set @basePrice=615.00
						set @kmPrice=20.00
					end
					else -- powyżej DMC 7500
					begin
						set @basePrice=0.00
						set @kmPrice=0.00
					end

					if @towingKm<=25
					begin
						set @valueZS=@basePrice
					end
					else
					begin
						set @valueZS=@basePrice+(@towingKm-25.0)*@kmPrice
					end
				end

			end
			else -- Zagranica
			begin
				if @programId = 430 -- Ford Assistance 12, Zagranica
				begin
					if @dmc<=3500
					begin
						if @towingKm <= 15
						begin
							set @valueZS=393.60
						end
						else if 15 < @towingKm and @towingKm <= 30
						begin
							set @valueZS=590.40
						end
						else if 30 < @towingKm
						begin
							set @kmPrice=6.80
							set @valueZS=590.40+(@towingKm-30.0)*@kmPrice
						end
					end		
					else
					begin
						set @valueZS = 0.00
					end
				end

				if @programId IN (429,435,436,437,438) -- Ford Euroservice,Ford Ubezpieczenia,Ford Mini Car Assistance, Zagranica
				begin
					if @dmc<=3500
					begin
						if @towingKm <= 15
						begin
							set @valueZS=436.70
						end
						else if 15 < @towingKm and @towingKm <= 30
						begin
							set @valueZS=651.90
						end
						else if 30 < @towingKm
						begin
							set @kmPrice=7.50
							set @valueZS=651.90+(@towingKm-30.0)*@kmPrice
						end
					end		
					else
					begin
						set @valueZS = 0.00
					end
				end

				if @programId IN (431,432,433,434) -- Ford Protect,Ford Inne Marki Wszystkie, Zagranica
				begin
					if @dmc<=3500
					begin
						if @towingKm <= 15
						begin
							set @valueZS=355.00
						end
						else if 15 < @towingKm and @towingKm <= 30
						begin
							set @valueZS=530.00
						end
						else if 30 < @towingKm
						begin
							set @kmPrice=6.10
							set @valueZS=530.00+(@towingKm-30.0)*@kmPrice
						end
					end		
					else
					begin
						set @valueZS = 0.00
					end
				end

			end
			
			if @noFix = 1
			begin
				print dbo.f_translate('No FIX',default)
			end
		end

		if @entityId=3 -- 0205-Pomoc/Oczekiwanie
		begin
			set @valueZS=20.0
		end

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @twrCode='01'

			if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				if @programId = 430	-- Ford Assistance 12 Polska
				begin
					set @valueZS=203.00
				end

				if @programId IN (429,435,436,437,438) -- Ford Euroservice,Ford Ubezpieczenia,Ford Mini Car Assistance, Polska
				begin
					set @valueZS=252.20
				end

				if @programId IN (431,432,433,434) -- Ford Protect,Ford Inne Marki Wszystkie, Polska
				begin
					set @valueZS=205.00
				end

			end
			else -- Zagranica
			begin
				if @programId = 430	-- Ford Assistance 12, Zagranica
				begin
					set @valueZS=319.80
				end

				if @programId IN (429,435,436,437,438) -- Ford Euroservice,Ford Ubezpieczenia,Ford Mini Car Assistance, Zagranica
				begin
					if @dmc<3500
					begin
						set @valueZS=350.60
					end		
					else
					begin
						set @valueZS=0.00
					end
				end

				if @programId IN (431,432,433,434) -- Ford Protect,Ford Inne Marki Wszystkie, Zagranica
				begin
					if @dmc<3500
					begin
						set @valueZS=285.00
					end		
					else
					begin
						set @valueZS=0.00
					end
				end
			end
		end

		if @entityId=16 -- 0203-Dźwig
		begin
			if @dmc<=2599
			begin
				set @valueZS=150.0		
			end		
			else if @dmc<=3399
			begin
				set @valueZS=170.0
			end		
			else if @dmc<=5699
			begin
				set @valueZS=170.0
			end
		end

		if @entityId=17 /*Naprawa przez telefon*/
		begin
			set @valueZS=110.74
		end

		if @entityId=18 -- Kredyt
		begin
			set @twrCode='17'
		
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '556', @groupProcessInstanceId = @groupProcessId
			SELECT @currency = value_string FROM @values	

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '732', @groupProcessInstanceId = @groupProcessId
			SELECT @valueZS = value_decimal FROM @values	

			--if @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			--begin
			--	set @valueZS = dbo.FN_Exchange(@valueZS,dbo.f_translate(dbo.f_translate('EUR',default),default),getdate()) 
			--end
		end 

		if @entityId=21 -- taxi
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='07'
			set @valueZS=0.0 -- Koszty rzeczywiste, przepisane z ZZ
			--delete from @values
			--INSERT @values EXEC p_attribute_get2 @attributePath = '152,319', @groupProcessInstanceId = @groupProcessId
			--SELECT @valueZS = value_decimal FROM @values	
		end 

		if @entityId=25 -- parking
		begin
			print dbo.f_translate('koszty wpisane parking',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			DECLARE kur cursor LOCAL for
				select  id 
				from      dbo.attribute_value 
				where   attribute_path like '638,219' and group_process_instance_id=@groupProcessId 

			OPEN kur;
			FETCH NEXT FROM kur INTO @structureId;
			WHILE @@FETCH_STATUS=0
			BEGIN 
				delete from @values     
				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '638,219,220', --Typ
						@groupProcessInstanceId = @groupProcessId,
						@parentAttributeId=@structureId
                               
					SELECT @TypKosztu=value_int FROM @values

					if @TypKosztu=5 /*parking*/
					begin
						delete from @values     
						INSERT  @values EXEC dbo.p_attribute_get2
								@attributePath = '638,219,222', /*wartość*/
								@groupProcessInstanceId = @groupProcessId,
								@parentAttributeId=@structureId

						SELECT @valueZS=value_decimal FROM @values
									
					end
				FETCH NEXT FROM kur INTO @structureId;
			END
			CLOSE kur
			DEALLOCATE kur
		end 

		if @entityId=26 and @programId = 441-- @platformId = 60 -- 1901-Porada
		begin
			set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			set @twrCode='19'
			set @valueZS=3.88
			set @programId = 441
		end 

		if @entityId=27 and @programId = 441-- @platformId = 60 -- 1951-Formularz Ford Vignale
		begin
			set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			set @twrCode='19'
			set @valueZS=5.82
			set @programId = 441
		end 

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'
			set @valueZS = 0.00 -- 

			delete from @values
			insert into @values 
			EXEC	dbo.p_attribute_get2
					@attributePath = '74,73',
					@groupProcessInstanceId = @rootId
			declare @customerCarId int = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)

			-- SELECT DO WYJĄTKÓW FORD
			declare @klasaFord int = (SELECT [classFord] FROM [AtlasDB_v].[dbo].[classException] WHERE makeModelId = @customerCarId)
		
			if @programId IN (429,430) -- Ford Euroservice, Ford Assistance 12
			begin
				set @valueZS = 0.01 -- Brakuje klasy wynajmu Ford.
				if @klasaFord = 1 -- Klasa A
				begin
					set @valueZS = 100.00
				end

				if @klasaFord = 2 -- Klasa B
				begin
					set @valueZS = 112.00
				end

				if @klasaFord = 3 -- Klasa C
				begin
					set @valueZS = 141.00
				end

				if @klasaFord = 4 -- Klasa D/SUV/VAN
				begin
					set @valueZS = 186.00
				end

				if @klasaFord = 5 -- Klasa Sport
				begin
					set @valueZS = 236.00
				end

				if @klasaFord = 6 AND @dmc <= 2500 -- Klasa - Mały dostawczy / Pick-up (do 2,499 tony DMC)
				begin
					set @valueZS = 184.00
				end

				if @klasaFord = 7 AND 2500 < @dmc AND @dmc <= 7500 -- Klasa - Duży dostawczy (od 2,500 do 7,500 tony DMC)
				begin
					set @valueZS = 226.00
				end

				/* Modele aut, które występują w różnych DMC. */
				if @customerCarId IN (611,612,613,614,608,609,610,599) AND @dmc <= 2500
				begin
					set @valueZS = 184.00 -- Klasa - Mały dostawczy / Pick-up (do 2,499 tony DMC)
				end

				if @customerCarId IN (611,612,613,614,608,609,610,599) AND 2500 < @dmc AND @dmc <= 7500
				begin
					set @valueZS = 226.00 -- Klasa - Duży dostawczy (od 2,500 do 7,500 tony DMC)
				end
			end

			if @programId IN (435,436,437) -- Ford Ubezpieczenia *
			begin
				set @valueZS = 0.01 -- Brakuje klasy wynajmu Ford.

				if @klasaFord = 1 -- Klasa A
				begin
					set @valueZS = 101.00
				end

				if @klasaFord = 2 -- Klasa B
				begin
					set @valueZS = 107.00
				end

				if @klasaFord = 3 -- Klasa C
				begin
					set @valueZS = 135.00
				end

				if @klasaFord = 4 -- Klasa D/SUV/VAN
				begin
					set @valueZS = 167.00
				end

				if @klasaFord = 5 -- Klasa Sport
				begin
					set @valueZS = 148.00
				end

				if @klasaFord = 6 AND @dmc <= 2500 -- Klasa - Mały dostawczy / Pick-up (do 2,499 tony DMC)
				begin
					set @valueZS = 186.00
				end

				if @klasaFord = 7 AND 2500 < @dmc AND @dmc <= 7500 -- Klasa - Duży dostawczy (od 2,500 do 7,500 tony DMC)
				begin
					set @valueZS = 224.00
				end

				/* Modele aut, które występują w różnych DMC. */
				if @customerCarId IN (611,612,613,614,608,609,610,599) AND @dmc <= 2500
				begin
					set @valueZS = 186.00 -- Klasa - Mały dostawczy / Pick-up (do 2,499 tony DMC)
				end

				if @customerCarId IN (611,612,613,614,608,609,610,599) AND 2500 < @dmc AND @dmc <= 7500
				begin
					set @valueZS = 224.00 -- Klasa - Duży dostawczy (od 2,500 do 7,500 tony DMC)
				end
			end
		end

		if @entityId=30 -- podstawienie
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			-- Pobieranie wartości, Krzysztof
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,790,240,222', @groupProcessInstanceId = @groupProcessId
			SELECT @valueZS = value_decimal FROM @values	
			--
			--print dbo.f_translate('TYP: ',default) + CAST(@type AS VARCHAR(10)) + dbo.f_translate(' ValZS: ',default) + CAST(@valueZS AS VARCHAR(10))
		end 

		if @entityId=31 -- odbiór
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			-- Pobieranie wartości, Krzysztof
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '812,789,787,240,222', @groupProcessInstanceId = @groupProcessId
			SELECT @valueZS = value_decimal FROM @values	
			--

		end 

		if @entityId=32 -- wynajm poza godzinami
		begin
			set @twrCode='04'

			-- Podstawienie poza godzinami pracy wypożyczalni
			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=1,@value=@substitutionValue output
			set @substitutionValue = isnull(@substitutionValue,0)

			-- Zwrot poza godzinami pracy wypożyczalni
			exec [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=2,@value=@returnValue output		
			set @returnValue = isnull(@returnValue,0)

			if (@substitutionValue != 0 AND @returnValue = 0) -- podstawienie
			begin
				set @valueZS = @substitutionValue
			end

			else if (@substitutionValue = 0 AND @returnValue != 0) -- zwrot
			begin
				set @valueZS = @returnValue
			end

			else if (@substitutionValue != 0 AND @returnValue != 0) -- podstawienie i zwrot
			begin
				set @valueZS = @substitutionValue + @returnValue
			end
		end 

		if @entityId=34 -- dodatkowe ubezp
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=4,@value=@valueZS OUTPUT
		end 

		if @entityId=35 -- hak holowniczy
		begin
			--print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=5,@value=@valueZS OUTPUT
		end 

		if @entityId=36 -- fotelik
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=6,@value=@valueZS OUTPUT
		end 

		if @entityId=37 -- bagażnik sam.
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=7,@value=@valueZS OUTPUT
		end 

		if @entityId=38 -- wyjazd za granicę
		begin
			print dbo.f_translate('koszt wpisany',default)
			set @twrCode='04'

			EXEC [dbo].[p_f_auto_replacement_addit_costs] @groupProcessInstanceId=@groupProcessId,@CostType=3,@value=@valueZS OUTPUT
		end 

		if @entityId=63 -- Transport pojazdu Producent
		begin			
			set @valueZS=(SELECT value_int FROM attribute_value WHERE group_process_instance_id =@groupProcessId AND attribute_path = '62,319')			
		end 

		/*	415	Audi Gwarancja Mobilności
			418	Volkswagen osobowe Gwarancja Mobilności
			420	Volkswagen użytkowe Gwarancja Mobilności
			421	Skoda Gwarancja Mobilności	
			Aso rozlicza swoje koszty z producentem - bez udziału Starter24		
		*/
		if (@type = 1 AND @czyASO = 1 AND @programId IN (415,418,420,421) AND @twrCode IN ('02','04')) 
		BEGIN
			print dbo.f_translate('Pomija ZS',default)
			set @valueZS=NULL
		END

		if @programId = 439	-- Ford Assistance Kolizyjny
		begin
			set @AkronimZS = '100020' -- Ford Polska
			set @valueZS=0.00
		end

		/*	26 1901-Porada
			27 1951-Formularz Ford Vignale */
		if @entityId NOT IN (1,10,29) AND (@entityId NOT IN (28,29) AND @programId <> 441)-- @platformId != 60) -- Pozostałe usługi rozliczane wg kosztów rzeczywistych
		begin
			set @valueZS=0.00
		end
	END
	ELSE IF @programId IN (447,448,449) --> KIA
	BEGIN
		set @valueZS = 0 -- Pozostałe przypadki

		if @programId = 448 -- KIA Assistance
		begin
			if @entityId in (1,10) and @KIAonceNHpriceZS = 1
			begin
				set @valueZS = 333.21
				set @KIAonceNHpriceZS = 0 --> Ryczałt pobrany, kolejny nie zostanie naliczony
			end
		end

		if @programId = 449 -- KIA Assistance FLOTA 
		begin
			if @entityId in (1,10)
			begin
				set @valueZS = 463.64
			end
		end

		if @programId = 447 -- KIA Służby Państwowe
		begin
			if @entityId in (1,10)
			begin
				set @valueZS = 224.18
			end
		end
	END
	ELSE IF @programId IN (442,454,455,456,457,590,591,592) OR @platformCFM=14 --> OPEL
	BEGIN
		set @valueZS = 0 -- Pozostałe przypadki

		if @entityId in (10) -- 0101-Naprawa na drodze
		begin
			set @valueZS = 345.06
		end

		if @entityId in (17) -- 00N01-Naprawa przez telefon
		begin
			set @valueZS = 36.43
		end

		if @entityId in (1) -- 0201-Holowanie
		begin
			set @valueZS = 373.99
		end

		if @entityId in (21) -- 0701-Odwiezienie osobówką
		begin
			set @valueZS = 145.00
		end

		if @entityId in (19,23) -- 0602-Lot , 0601-Pociąg
		begin
			set @valueZS = 637.00
		end

		if @entityId in (29) -- 0401-Wynajem auta zastępczego
		begin
			set @valueZS = 524.02
		end

		if @entityId in (43) -- 0501-Nocleg
		begin
			set @valueZS = 352.00
		end

		if @entityId in (63) -- Transport
		begin
			set @valueZS = 2012.00
		end
	END	
	ELSE IF @programId IN (475,476,477,478,479,480,481,482,483) --> TUW TUZ
	BEGIN
		set @valueZS = 0.00 -- Pozostałe przypadki

		if @entityId=1 -- 0201-Holowanie
		begin
			if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				if @towingKm<=50 -- Ryczałt do 50km
				begin
					set @valueZS=120.00
				end
				else if @towingKm<=100 -- Ryczałt do 100km
				begin
					set @valueZS=200.00
				end
				else if @towingKm<=200 -- Ryczałt do 200km
				begin
					set @valueZS=400.00 
				end	
				else -- Ryczałt powyżej 200km
				begin
					set @valueZS=400.00 + (@towingKm - 200)*2.00
				end
				
				set @valueZS = @valueZS + (2.60 * (@arrivalKm + @backKm)) -- + Ryczałt za kilometry dojazdu i powrotu
			end
			else
			begin
				set @value=0.0
			end
		end

		if @entityId=3 -- 0205-Pomoc/Oczekiwanie
		begin
			set @valueZS=20.00
		end

		if @entityId=8 -- 0210-Transport pow 2 osób w holowniku
		begin
			set @valueZS=1.60 * @towingKm
		end

		if @entityId=10 -- 0101-Naprawa na drodze
		begin
			if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				set @valueZS=190.00

				if @towingKm > (@arrivalKm + @backKm)
				begin
					set @valueZS = @valueZS + ((@arrivalKm + @backKm - 50) * 1.60)
				end
			end
			else
			begin
				set @valueZS=0.0
			end
		end

		if @entityId=16 -- 0203-Dźwig
		begin
			set @valueZS=150.00
		end

		if @entityId=17 -- 00N01-Naprawa przez telefon
		begin
			set @valueZS=80.00
		end	

		if @entityId=21 -- 0701-Odwiezienie osobówką
		begin
			delete from @values
			insert @values exec p_attribute_get2 @attributePath = '152,647', @groupProcessInstanceId = @groupProcessId
			set @taxiDistance = (select value_int from @values)
			set @valueZS=50			
			if @taxiDistance>20.0
			begin
				set @valueZS=@valueZS+(@taxiDistance-20.0)*3.0
			end
		end	

		if @entityId=25 and @dmc < 3500 -- 2201-Parking
		begin
			set @valueZS=10.00
		end

		if @entityId=26 -- 1901-Porada
		begin
			set @valueZS=26.25  /*TUW TUZ cennik od 01.10.2018*/
		end

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			SET @valueZS = case when @class = 1  then 70.00  -- 'A'
								when @class = 2  then 80.00  -- 'B'
								when @class = 7  then 90.00  -- 'C'
								when @class = 5  then 130.00 -- dbo.f_translate('B Premium',default)
								when @class = 11 then 130.00 -- 'D'
								when @class = 23 then 110.00 -- 'M'
								when @class = 9  then 180.00 -- dbo.f_translate('C Premium',default)
								when @class = 16 then 350.00 -- dbo.f_translate('D Premium',default)
								when @class = 21 then 400.00 -- 'F'
								when @class = 24 then 200.00 -- 'N'
								when @class = 17 then 210.00 -- 'R'
								when @class IN (4,6,10,15,18,25) then 0.00 -- dbo.f_translate('SUV',default)
							else 0.01 end
		end
	END
	ELSE IF @programId IN (484,485,486) -->	TUW TUZ Home Assistance
	BEGIN
		set @valueZS = 0 -- Pozostałe przypadki

		if @entityId=26 -- 1901-Porada
		begin
			set @twrCode='19'
			set @valueZS=26.25 /*zmiana od 01.10.2018*/
		end

		if @entityId=64 -- Home Assistance
		begin
			/*	0	Ślusarz
				1	Elektryk
				2	Hydraulik
				3	Murarz
				4	Malarz
				5	Szklarz
				6	Dekarz
				7	Specjalista od systemów alarmowych
				8	Technik urządzeń grzewczych
				9	Technik urządzeń AGD
				10	Technik urządzeń RTV
				11	Opieka domowa po hospitalizacji
				12	Transport i dozór
				13	Opieka nad zwierzętami
				14	Organizacja wizyty u lekarza specjalisty
				15	Domowa wizyta lekarza pierwszego kontaktu
				16	Domowa wizyta pielęgniarki
				17	Dostawa leków lub sprzętu rehabilitacyjnego
				18	Konsultacja medyczna
				19	Raport medyczny
				20	Opieka psychologiczna
				21	Rehabilitacja
				22	Wizyta pielęgniarki u dziecka
				23	Wizyta pediatry
				24	Powtórna opinia medyczna
				25	Całodobowa opieka pielęgniarki w szpitalu
				26	Wizyta u chorego
				27	Transport medyczny inny niz samolot
				28	Inne */
			declare @haService int -- Numer usługi Home Assistance
			delete from @values
			insert @values exec p_attribute_get2 @attributePath = '954', @groupProcessInstanceId = @groupProcessId
			select @haService = value_int FROM @values	

			set @valueZS = case when @haService IN (0,1,2,3,4,5,6,7,8)  then 245.00  -- Ślusarz,Elektryk,Hydraulik,Murarz,Malarz,Szklarz,Dekarz,Specjalista od systemów alarmowych,Technik urządzeń grzewczych
								when @haService IN (9,10)  then 245.00  -- Technik urządzeń AGD i RTV
								when @haService IN (11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27)  then 122.00  -- Pakiet Assistance Medyczny
							else 0.00 end
		end
	END
	ELSE IF @programId IN (469,470,471,472,473,474) -->	TUW Pocztowe
	BEGIN
		set @valueZS = 0.00

		/*	469	42	TUW Assistance (Pocztowe OC)
			470	42	Moto Assistance Standard (TUW Pocztowe)
			471	42	Moto Assistance Komfort (TUW Pocztowe)*/
		if @programId in (469,470,471)
		begin

			if @entityId in (1,10) and @POCZTOEonceNHpriceZS = 1
			begin		
				set @valueZS = case when @programId = 469 then 364.53  -- TUW Assistance (Pocztowe OC)
									when @programId = 470 then 636.42  -- Moto Assistance Standard (TUW Pocztowe)
									when @programId = 471 then 878.10  -- Moto Assistance Komfort (TUW Pocztowe)
								else 0.00 end
				set @POCZTOEonceNHpriceZS = 0
			end
		end

		/*	472	42	TUW Pocztowe Bezpieczny Dom
			473	42	TUW Pocztowe ubezpieczenie kredytów i pożyczek hipotecznych
			474	42	TUW Pocztowe ubezpieczenie ROR */
		if @programId in (472,473,474)
		begin
			print 'T'
			--set @XL_FlagaNBZZ = 'B'
		end
	END
	ELSE IF @programId IN (443,444,445,446,494,495,496,497) -->	Concordia Assistance
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			if isnull(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)) = dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				if (@arrivalKm+@backKm<=50) -- ponizej 50 km
				begin
					set @valueZS=246.75
				end
				else 
				begin 
			 		set @valueZS=246.75+(2.10*(@arrivalKm+@backKm-50)) -- 2.10 pln brutto za km powyzej 50 km
				end
			end
		end

		if @entityId=1 -- 0201-Holowanie
		begin
			if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				if (@dmc < 2500)
				begin
					set @valueZS = 206.85			
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 6.56 * (@towingKm - 25)
					end
				end
				else if (@dmc between 2500 and 3499)
				begin
					set @valueZS = 252.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin								
						set @valueZS = @valueZS + 8.51 * (@towingKm - 25)
					end
				end
				else if (@dmc between 3500 and 7500)
				begin
					set @valueZS = 756.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 24.15 * (@towingKm - 25)
					end
				end
			end
		end

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			SET @valueZS = case when @class = 1  then 95.00  -- 'A'
								when @class = 2  then 100.00 -- 'B'
								when @class = 7  then 116.00 -- 'C'
								when @class = 11 then 163.00 -- 'D'
								when @class = 16 then 257.00 -- dbo.f_translate('D Premium',default)
								when @class = 19 then 336.00 -- 'E'
								when @class = 21 then 578.00 -- 'F'
								when @class in (4,6,10,15,18,25) then 221.00 -- dbo.f_translate('SUV',default)
								when @class = 13 then 236.00 -- dbo.f_translate('VAN',default)
								when @class = 23 then 173.00 -- 'M'
								when @class = 24 then 221.00 -- 'N'
								when @class = 17 then 221.00 -- 'R'
							else 0.01 end
		end

		if @entityId=16 -- 0203-Dźwig
		begin
			if @dmc < 2499
			begin
				set @valueZS=194.25
			end
			else if @dmc > 2500
			begin
				set @valueZS=220.50
			end
		end

		if @entityId=26 -- 1901-Porada
		begin
			set @valueZS=21.00
		end
	END
	ELSE IF @programId IN (458,459,460) -->	Orix Standard,VIP,Exclusive
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			if isnull(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)) = dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				if (@arrivalKm+@backKm<=50) -- ponizej 50 km
				begin
					set @valueZS=235.00
				end
				else 
				begin 
			 		set @valueZS=235.00+(2.00*(@arrivalKm+@backKm-50)) -- 2.10 pln brutto za km powyzej 50 km
				end
			end
		end

		if @entityId=1 -- 0201-Holowanie
		begin
			if @country=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				if (@dmc < 2500)
				begin
					set @valueZS = 197.00			
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 6.25 * (@towingKm - 25)
					end
				end
				else if (@dmc between 2500 and 3499)
				begin
					set @valueZS = 240.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin								
						set @valueZS = @valueZS + 7.13 * (@towingKm - 25)
					end
				end
				else if (@dmc between 3500 and 7500)
				begin
					set @valueZS = 720.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 23.00 * (@towingKm - 25)
					end
				end
				else if (@dmc > 7500)
				begin
					set @valueZS = 585.00
					if @towingKm > 25 -- Dolicz każdy kilometr ponad zakres bazowy
					begin					
						set @valueZS = @valueZS + 18.90 * (@towingKm - 25)
					end
				end
			end
		end

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			SET @valueZS = case when @class = 1  then 123.00  -- 'A'
								when @class = 2  then 130.00 -- 'B'
								when @class = 7  then 141.00 -- 'C'
								when @class = 11 then 178.00 -- 'D'
								when @class = 8  then 178.00 -- dbo.f_translate('DOSTAWCZY MAŁY',default)
								when @class = 14 then 220.00 -- dbo.f_translate('DOSTAWCZY duży',default)
								when @class = 19 then 245.00 -- 'E'
								when @class in (4,6,10,15,18,25) then 245.00 -- dbo.f_translate('SUV',default)
							else 0.01 end
		end

		if @entityId=26 -- 1901-Porada
		begin
			set @valueZS=12.00
		end
	END
	ELSE IF @programId IN (468) --> Comfort ERV
	BEGIN
		set @valueZS = 0.00

		if @entityId=1 -- 0201-Holowanie
		begin
			set @valueZS = 1333.00
		end
	END
	ELSE IF @programId IN (492,493) --> Centra Assistance, Exide Assistance
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS=170.00
		end
	END
	--» Od 06.12.2018r.
	ELSE IF @programId IN (487,488,489) --> Volvo
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (505) --> Husqvarna Assistance (Sylwia)
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (450) --> KTM Assistance (Sylwia)
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (451) --> Mercedes Wypadek (Łukasz)
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (500) --> OAMTC (członkowie klubu austriackiego)
	BEGIN
		set @currency = dbo.f_translate(dbo.f_translate('PLN',default),default)
		set @valueZS = 0.00 -- Koszty rzeczywiste

		if @entityId=1 -- 0201-Holowanie
		begin					
			if (@towingKm+@arrivalKm+@backKm) <= 25
			begin					
				set @valueZS = 245.51 -- Ryczałt 1
			end
			else if (@towingKm+@arrivalKm+@backKm) <= 50
			begin
				set @valueZS = 271.12 -- Ryczałt 2
			end
			else -- Kilometry ponad ryczałt (50km)
			begin
				set @valueZS = 271.12 + 3.01 * ((@towingKm+@arrivalKm+@backKm) - 50)
			end
		end

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS = 199.32
			if (@arrivalKm+@backKm) > 25
			begin -- Kilometry ponad ryczałt (25km)
				set @valueZS = @valueZS + 2.10 * ((@arrivalKm+@backKm) - 25)
			end		
		end
	END
	ELSE IF @programId IN (465) --> Smart Assistance (Sylwia)
	BEGIN
		set @valueZS = 0.00 -- Koszty rzeczywiste
	END
	ELSE IF @programId IN (507,498,499,501,502,503,504) --> ARC, AA, ANWB, RACE, Rosqvist, TCB, TCS
	BEGIN	/*	507	27	ARC
				498	27	AA (członkowie klubu brytyjskiego)
				499	27	ANWB (członkowie klubu holenderskiego)
				501	27	RACE (członkowie klubu hiszpańskiego)
				502	27	Rosqvist (członkowie klubu fińskiego)
				503	27	TCB (członkowie klubu belgijskiego)
				504	27	TCS (członkowie klubu szwajcarskiego)	*/	
		set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
		set @valueZS = 0.00 -- Koszty rzeczywiste

		if @entityId=1 -- 0201-Holowanie
		begin
			if @AllTowingKmInOneOrder = 1
			begin			
				declare @totalKM decimal(20,4) = 0 -- Suma wszystkich kilometów w sprawie dla usługi holowania.
				declare @totalKMtmp decimal(20,4) = 0

				declare totalKM cursor for
				select value_decimal from attribute_value where attribute_path='638,225' and root_process_instance_id=@rootId and value_decimal is not null
				open totalKM
				fetch next from totalKM into @totalKMtmp
				while @@FETCH_STATUS = 0
				begin
					set @totalKM = @totalKM + isnull(@totalKMtmp,0.00)
					fetch next from totalKM into @totalKMtmp
				end
				close totalKM;
				deallocate totalKM;

				set @totalKM = ROUND(ISNULL(@totalKM,0),0) 	-- Zaokrąglanie kilometrów

				if @dmc < 3500
				begin
					set @valueZS = 125.50
				end
				else
				begin -- Dodatkowy ryczałt za DMC powyżej 3,5t
					set @valueZS = 125.50 + 32.11
				end		
						
				if @totalKM > 30 -- Dolicz każdy kilometr ponad zakres bazowy
				begin					
					set @valueZS = @valueZS + 2.40 * (@totalKM - 30)
				end

				set @AllTowingKmInOneOrder = 0
			end
			else
			begin
				set @valueZS = null
			end
		end

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS = 125.50
			if @dmc > 3500
			begin -- Dodatkowy ryczałt za DMC powyżej 3,5t
				set @valueZS = @valueZS + 32.11
			end		
		end

		if @entityId=65 -- Medical
		begin
			set @IsMedicalInCase = 1
		end
	END
	--» CFM Od 1,2,3? 2019r.
	ELSE IF @programId IN (508,509,510,511,512,513,514,515,516,517,518,519,520,521) OR @platformCFM = 53 --> Alphabet
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS = 192.78 --> Ryczałt do 50 km
			if (@arrivalKm+@backKm) > 50
			begin -- Kilometry ponad ryczałt (50km)
				set @valueZS = @valueZS + 1.80 * ((@arrivalKm+@backKm) - 50)
			end	
		end

		if @entityId=1 -- 0201-Holowanie
		begin		
			if @dmc < 2200
			begin
				set @valueZS=163.20
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 5.90 * (@towingKm - 25)
				end
			end
			else if @dmc >= 2200 and @dmc < 3499
			begin
				set @valueZS=198.90
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 6.70 * (@towingKm - 25)
				end
			end
			if @dmc >= 3499
			begin
				set @valueZS=596.70
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 19.30 * (@towingKm - 25)
				end
			end
		end

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			set @summaryQuantity = (
				select SUM(av_quantity.value_int) as dbo.f_translate('Quantity',default)
				from   attribute_value av with(nolock) 
				join   attribute_value av_quantity with(nolock) on av_quantity.parent_attribute_value_id = av.id 
						and av_quantity.attribute_path='820,821,822,221'
				join   attribute_value av_groupId with(nolock) on av_groupId.id=av.parent_attribute_value_id
				join   attribute_value av_partner with(nolock) on av_partner.group_process_instance_id = av_groupId.value_int 
						and av_partner.attribute_path='764,742'
				join   attribute_value av_makeModel with(nolock) on av_makeModel.group_process_instance_id = av_groupId.value_int 
						and av_makeModel.attribute_path='764,73'
				join   dictionary d_class with(nolock) on d_class.value=av_makeModel.value_int and d_class.typeD='makeModel'
				where  av.attribute_path='820,821,822' and av.value_int=29 and av.root_process_instance_id=@rootId
						and av_partner.value_int=@partnerId and d_class.argument3=@class
			)


			-->	Wypo.Prefer.	Jupol ,Express,99Rent,Rentis, Anca ,Carsson,Kangoor
			IF @partnerCode IN (100414,100040 ,102562,103700,106090,106088 ,105820)
			OR EXISTS( --> Serwisy preferowane
				SELECT  d.textD FROM f_split(@partnerKinds,',') fs
				JOIN dictionary d WITH(NOLOCK) ON d.value=fs.data AND d.typeD = 'PartnerLocationType'
				WHERE ( d.textD LIKE '%BRS%' OR
						d.textD LIKE '%Warsztaty naprawy szyb%' OR --'%ALPHABET SZYBY%' OR
						d.textD LIKE '%Warsztat wulkanizacyjny%' OR --'%ALPHABET OPONY%' OR
						d.textD LIKE '%Alphabet ASO Preferowane%' OR
						d.textD LIKE '%ALPHABET ASN%' OR
						d.textD LIKE '%ALPHABET S-PLUS%' OR
						d.textD LIKE '%ASO%' )
			)
			BEGIN
				PRINT dbo.f_translate('Partner preferowany. Koszt 0.00zł',default)
				SET @valueZS = 0.00 --> Partner preferowany
			END
			ELSE
			BEGIN --> Serwisy niepreferowane
				SET @valueZS = CASE 
				WHEN @class IN (3) THEN CASE --> 'B'			
					when @summaryQuantity <= 5 then 87
					when @summaryQuantity <= 14 then 71
					when @summaryQuantity > 14 then 65 
					else null end
				WHEN @class IN (7) THEN CASE --> 'C'
					when @summaryQuantity <= 5 then 102
					when @summaryQuantity <= 14 then 96
					when @summaryQuantity > 14 then 85
					else null end
				WHEN @class IN (11) THEN CASE --> 'D'
					when @summaryQuantity <= 5 then 146
					when @summaryQuantity <= 14 then 137
					when @summaryQuantity > 14 then 125
					else null end
				WHEN @class IN (16,19) THEN CASE --> 'E'
					when @summaryQuantity <= 5 then 260
					when @summaryQuantity <= 14 then 250
					when @summaryQuantity > 14 then 240
					else null end
				WHEN @class IN (8,23) THEN CASE --> 'M' 
					when @summaryQuantity <= 5 then 122
					when @summaryQuantity <= 14 then 110
					when @summaryQuantity > 14 then 100
					else null end
				WHEN @class IN (14,17,24,26) THEN CASE--> dbo.f_translate('N i R',default)
					when @summaryQuantity <= 5 then 215
					when @summaryQuantity <= 14 then 210
					when @summaryQuantity > 14 then 180
					else null end	
				ELSE 0.00 END --> KOSZT RZECZYWISTY
			END
			print dbo.f_translate('KLASA WYNAJMOWANEGO AUTA: ',default) + CAST(@class AS NVARCHAR(10))
		end
	END
	ELSE IF @programId IN (0) OR @platformCFM = 48 --> 48 Bussiness Lease
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 --> 0101-Naprawa na drodze
		begin	
			set @valueZS = 206.20 --> Ryczałt do 50 km
			if (@arrivalKm+@backKm) > 50
			begin -- Kilometry ponad ryczałt (50km)
				set @valueZS = @valueZS + 1.90 * ((@arrivalKm+@backKm) - 50)
			end	
		end

		if @entityId=1 -- 0201-Holowanie
		begin		
			if @dmc < 2200
			begin
				set @valueZS=174.60
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 6.30 * (@towingKm - 25)
				end
			end
			else if @dmc >= 2200 and @dmc < 3499
			begin
				set @valueZS=212.90
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 7.20 * (@towingKm - 25)
				end
			end
			if @dmc >= 3499
			begin
				set @valueZS=638.50
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 19.70 * (@towingKm - 25)
				end
			end
		end
	END
	ELSE IF @programId IN (0) OR @platformCFM = 25 --> 25 LEASEPLAN
	BEGIN
		set @valueZS = 0.00

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			if isnull(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default))=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				set @valueZS = 187.00 --> Ryczałt do 50 km
				if (@arrivalKm+@backKm) > 50
				begin -- Kilometry ponad ryczałt (50km)
					set @valueZS = @valueZS + 1.70 * ((@arrivalKm+@backKm) - 50)
				end	
			end
			else --> Zagranica
			begin
				set @valueZS = 161.00
				set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
			end
		end

		if @entityId=1 -- 0201-Holowanie
		begin	
			if isnull(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default))=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)
			begin
				if @dmc < 2200
				begin
					set @valueZS=157.00
					if @towingKm > 25
					begin
						set @valueZS = @valueZS + 5.70 * (@towingKm - 25)
					end
				end
				else if @dmc >= 2200 and @dmc < 3499
				begin
					set @valueZS=192.00
					if @towingKm > 25
					begin
						set @valueZS = @valueZS + 6.50 * (@towingKm - 25)
					end
				end
				else if @dmc >= 3499
				begin
					set @valueZS=574.00
					if @towingKm > 25
					begin
						set @valueZS = @valueZS + 18.50 * (@towingKm - 25)
					end
				end
			end
			else --> Zagranica
			begin
				set @currency = dbo.f_translate(dbo.f_translate('EUR',default),default)
				if @dmc < 3500
				begin
					set @valueZS=161.00
					if @towingKm > 30
					begin
						set @valueZS = @valueZS + 2.70 * (@towingKm - 30)
					end
				end
			end
		end

		if @entityId=4 -- dokumentacja
		begin
			set @valueZS=20.0 --> Zdjęcia tylko w LP
		end 

		if @entityId=29 -- Wynajem pojazdu zastępczego
		begin
			set @twrCode='04'

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessId
			SELECT @markaModelAutaWyp = value_int FROM @values	
		
			select	@class=argument3
			from	dbo.dictionary 
			where	typeD='makeModel' and 
			value=@markaModelAutaWyp

			set @summaryQuantity = (
				select SUM(av_quantity.value_int) as dbo.f_translate('Quantity',default)
				from   attribute_value av with(nolock) 
				join   attribute_value av_quantity with(nolock) on av_quantity.parent_attribute_value_id = av.id 
						and av_quantity.attribute_path='820,821,822,221'
				join   attribute_value av_groupId with(nolock) on av_groupId.id=av.parent_attribute_value_id
				join   attribute_value av_partner with(nolock) on av_partner.group_process_instance_id = av_groupId.value_int 
						and av_partner.attribute_path='764,742'
				join   attribute_value av_makeModel with(nolock) on av_makeModel.group_process_instance_id = av_groupId.value_int 
						and av_makeModel.attribute_path='764,73'
				join   dictionary d_class with(nolock) on d_class.value=av_makeModel.value_int and d_class.typeD='makeModel'
				where  av.attribute_path='820,821,822' and av.value_int=29 and av.root_process_instance_id=@rootId
						and av_partner.value_int=@partnerId and d_class.argument3=@class
			)
			
			-->	Wypo.Prefer.
			IF @partnerCode IN (
			100414, --> Jupol 
			103700, --> Rentis
			102562, --> 99Rent
			101600, --> Partner
			102800) --> LEASEPLAN FLEET MANAGEMENT (POLSKA) SP. Z O.O. (Flexplan)
			BEGIN
				PRINT dbo.f_translate('Partner preferowany. Koszt 0.00zł',default)
				SET @valueZS = 0.00 --> Partner preferowany
			END
			ELSE
			BEGIN --> Serwisy niepreferowane
				SET @valueZS = CASE 
					WHEN @class IN (3) THEN CASE --> 'B'			
						when @summaryQuantity <= 3 then 90
						when @summaryQuantity <= 14 then 85
						when @summaryQuantity > 14 then 80 
						else null end
					WHEN @class IN (7) THEN CASE --> 'C'
						when @summaryQuantity <= 3 then 115
						when @summaryQuantity <= 14 then 105
						when @summaryQuantity > 14 then 100
						else null end
					WHEN @class IN (11) THEN CASE --> 'D'
						when @summaryQuantity <= 3 then 150
						when @summaryQuantity <= 14 then 140
						when @summaryQuantity > 14 then 130
						else null end
					WHEN @class IN (16,19) THEN CASE --> 'E'
						when @summaryQuantity <= 3 then 240
						when @summaryQuantity <= 14 then 220
						when @summaryQuantity > 14 then 210
						else null end
					WHEN @class IN (8,23) THEN CASE --> 'M' 
						when @summaryQuantity <= 3 then 135
						when @summaryQuantity <= 14 then 120
						when @summaryQuantity > 14 then 110
						else null end
					WHEN @class IN (14,17,24,26) THEN CASE--> dbo.f_translate('N i R',default)
						when @summaryQuantity <= 3 then 220
						when @summaryQuantity <= 14 then 210
						when @summaryQuantity > 14 then 200
						else null end	
					ELSE 0.00 END --> KOSZT RZECZYWISTY
			END

			print dbo.f_translate('KLASA WYNAJMOWANEGO AUTA: ',default) + CAST(@class AS NVARCHAR(10))
		end

	END
	ELSE IF @programId IN (0) OR @platformCFM = 43 --> 43 ALD
	BEGIN
		set @valueZS = 0.00 -- Pozostałe usługi z kosztami rzeczywistymi

		if @entityId=10 --> 0101-Naprawa na drodze
		begin	
			set @valueZS = 172.07 --> Ryczałt do 50 km
			if (@arrivalKm+@backKm) > 50
			begin -- Kilometry ponad ryczałt (50km)
				set @valueZS = @valueZS + 2.15 * ((@arrivalKm+@backKm) - 50)
			end	
		end

		if @entityId=1 --> 0201-Holowanie
		begin		
			if @dmc < 2500
			begin
				set @valueZS=147.90
				if @towingKm > 20
				begin
					set @valueZS = @valueZS + 4.70 * (@towingKm - 20)
				end
			end
			else
			begin
				set @valueZS=168.30
				if @towingKm > 20
				begin
					set @valueZS = @valueZS + 6.12 * (@towingKm - 20)
				end
			end
		end

		if @entityId = 26 --> 1901-Porada jest udzielana bezpłatnie
		begin
			set @valueZS = null
		end

		if isnull(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default)) <> dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default) --> Zdarzenia zagraniczne z kosztami rzeczywistymi
		begin
			set @valueZS = 0.00
		end
	END
	--» TRUCK
		/*	8	DKV
		13  Starter Truck Service
		16	UTA Assistance    
		20	Euroshell Breakdown Service
		21	Shell Autoryzacje
		22	ESSO Assistance
		50	Shell Alarmowy
		64	Webasto Assistance
		68	KOGEL Assistance
		73	IDS
		74	Otokar Assistance	*/
	ELSE IF @platformCFM IN (8,13,16,20,21,22,50,64,68,73,74) --> TRUCK
	BEGIN
		SET @valueZS = 0.00 --> Koszty rzeczywiste
		DECLARE @twrCodeTruck NVARCHAR(10) = NULL
		--SET @AkronimZSTruck = @AkronimZS
		IF @entityId IN (66,69) --> 3101-Naprawa na drodze Truck, Holowanie Truck
		BEGIN
			SET @twrCodeTruck = 'TR12'
		END

		IF @entityId IN (73) --> Naprawa mechaniczna Truck
		BEGIN

			SET @twrCodeTruck = CASE WHEN @partnerStarterCode = '103243' THEN 'TR10'
									WHEN @partnerStarterCode = '101751' THEN dbo.f_translate('SIGNELLA',default)
									WHEN @partnerStarterCode = '101403' THEN 'TR10'
									WHEN @partnerStarterCode = '102719' THEN 'TR10'
									WHEN @partnerStarterCode = '103006' THEN dbo.f_translate('BIASCAN',default)
									WHEN @partnerStarterCode = '100860' THEN 'TR10'
									WHEN @partnerStarterCode = '103974' THEN 'TR11' 
								ELSE 'TR11' END

			
		END

		IF @entityId <> 71 AND ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default))=dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default) --> Poza Dowóz gotówki na inne potrzeby
		BEGIN

		delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @towingGroupProcessId
			SELECT @servicePartnerId = value_int FROM @values
	
			if @servicePartnerId is null
			begin
				delete from @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @towingGroupProcessId
				SELECT @servicePartnerId = value_int FROM @values
			end

			if @servicePartnerId is not null
			begin
				SELECT  @akronimTarget = avKod.value_string 
				FROM	dbo.attribute_value avL inner join 
						dbo.attribute_value avKod ON avKod.parent_attribute_value_id = avL.id and avKod.attribute_path='595,597,631'  
				WHERE	avL.id = @servicePartnerId and avL.attribute_path='595,597'
			end

			SET @akronimTarget = CASE --WHEN @partnerStarterCode = '103243' THEN dbo.f_translate('ASO 032',default)
									--WHEN @partnerStarterCode = '101751' THEN dbo.f_translate('ASO 032',default)
									--WHEN @partnerStarterCode = '101403' THEN dbo.f_translate('ASO 032',default)
									--WHEN @partnerStarterCode = '102719' THEN dbo.f_translate('ASO 032',default)
									WHEN @partnerStarterCode = '103006' THEN dbo.f_translate('ASO 032',default)
									--WHEN @partnerStarterCode = '100860' THEN dbo.f_translate('ASO 032',default)
									--WHEN @partnerStarterCode = '103974' THEN dbo.f_translate('ASO 032',default) 
								END

			IF isnull(@akronimTarget,'')=''
			begin
				set @akronimTarget=@AkronimZS 
			end

			INSERT INTO sync.ZamNag (
							ParentZsId,
							AUD_GidTyp, -- 960
							XL_DataRealizacji, -- data statusu progres 4
							XL_DokumentObcy, -- caseId
							XL_KntAkronim,
							XL_Opis, -- platforma + dbo.f_translate('atlas',default)
							XL_Typ, -- ZS/ZZ
							XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
							rootId,--SO_SprawaId, -- rootId
							type,
							partnerLocId,
							XL_DataWystawienia,
							XL_TargetKntAkronim,
							XL_FlagaNB
						)
						SELECT 
						null,
						1280, 
						getdate(),--@finishedAt, 
						@caseId,  
						@partnerStarterCode, --Akronim VGP
						@description, -- Description (opis)
						'ZS',
						@serieZS,	
						@rootId,
						1, -- Type (Producent)
						null,--@partnerId, -- Czy to musi być?
						@caseDate, --> Numer sprawy A0XXXXXX
						@akronimTarget, /*Docelowy*/
						'N' --> Netto

			INSERT INTO sync.ZamElem(
				AUD_IdDok, -- id nagłówka
				XL_Ilosc, -- zmienna
				XL_TwrKod, -- zmienna 
				XL_Wartosc, -- zmienna 
				XL_Waluta, -- zmienna 
				entityValueId, -- entityId
				entityDefId,
				programId,
				groupProcessId
			)	
			SELECT  
			scope_identity(),
			1,
			@twrCodeTruck,
			0.00,
			@currency,
			null,
			null,
			@programId --> Alphabet wsparcie kierowcy
			,@groupProcessId
		END


		--IF @entityId = 66 --> 3101-Naprawa na drodze Truck
		--BEGIN
			
		--END

		--SET @valueZS =	CASE --> ZS - Koszty rzeczywiste 0.00
		--	WHEN @entityId = 66 THEN 
		--	0.00 
		--	--WHEN @entityId = 67 THEN 0.00 --> 3102-Diagnoza na drodze Truck
		--	--WHEN @entityId = 68 THEN 0.00 --> 3103-Serwis oponiarski na drodze Truck
		--	WHEN @entityId = 69 THEN 0.00 --> Holowanie Truck
		--	--WHEN @entityId = 70 THEN 0.00 --> Wyciąganie spoza drogi dla pojazdów ciężarowych
		--	WHEN @entityId = 71 THEN 0.00 --> Dowóz gotówki na inne potrzeby
		--	--WHEN @entityId = 72 THEN 0.00 --> Dowóz gotówki na opłacenie kary lub mandatu
		--	WHEN @entityId = 73 THEN 0.00 --> Naprawa mechaniczna Truck



		--ELSE 0.00 END

		IF ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default))<>dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default) AND @entityId <>71 --> Sprawy za granica powinny sie przenosić tylko z dowozem gotówki
		BEGIN
			PRINT dbo.f_translate('Sprawy za granica powinny sie przenosić tylko z dowozem gotówki',default)
			SET @valueZS = NULL
		END

		--set @valueZS = NULL -- IMPORT WYŁĄCZONY
	END
	ELSE IF @platformCFM IN (5) --> ADAC
	BEGIN
		set @valueZS = 0.00 --> Koszty rzeczywiste

		if @entityId=10 -- 0101-Naprawa na drodze
		begin	
			set @valueZS = 199.32 --> Ryczałt do 50 km
			if (@arrivalKm+@backKm) > 25
			begin -- Kilometry ponad ryczałt (50km)
				set @valueZS = @valueZS + 2.10 * ((@arrivalKm+@backKm) - 25)
			end
		end

		if @entityId=1 -- 0201-Holowanie
		begin
			set @valueZS=245.41
			if (@towingKm+@backKm+@arrivalKm) > 25 and (@towingKm+@backKm+@arrivalKm) <= 50
			begin
				set @valueZS=271.12
			end
			else if (@towingKm+@backKm+@arrivalKm) > 50
			begin
				set @valueZS = 271.12 + 3.10 * ((@towingKm+@backKm+@arrivalKm) - 50)
			end
		end

		if @twrCode='04' --> Wynajem pojazdu zastępczego nie przenosi się.
		begin
			set @valueZS=null
		end
	END

	IF @type = 10 --> Cennik ADH
	BEGIN
		print '@towingKm:' + cast(@towingKm as nvarchar(20))
		set @valueZS=0.0

		if @entityId = 10
		begin
			if @arcCode3Id = 80 or @isTireRepair = 1
			begin
				set @valueZS=160.00/1.23
			end
			else
			begin
				set @valueZS=240.00/1.23
			end
		end

		if @entityId = 1
		begin
			print '@dmc:' + cast(@dmc as nvarchar(20))
			if @dmc<=2190 -- 2820
			begin	
				if @additTownigADH=1
				begin			
					set @valueZS=@towingKm*(5.0/1.23)
				end
				else
				begin --> Skuteczna naprawa
					set @valueZS=240.00/1.23
					if @towingKm>25
					begin
						set @valueZS=@valueZS+(@towingKm-25)*(5.0/1.23)
					end
				end
			end 
			else if @dmc<=3390 -- 2820
			begin
				if @additTownigADH=1 
				begin
					set @valueZS=@towingKm*(7.0/1.23)
				end
				else
				begin
					set @valueZS=280.00/1.23
					if @towingKm>25
					begin
						set @valueZS=@valueZS+(@towingKm-25)*(7.0/1.23)
					end
				end
				print dbo.f_translate('ADH Test',default)
			end
			else
			begin
				if @additTownigADH=1 
				begin
					set @valueZS=@towingKm*(8.80/1.23)
				end
				else
				begin
					set @valueZS=380.00/1.23
					if @towingKm>25
					begin
						set @valueZS=@valueZS+(@towingKm-25)*(8.80/1.23)
					end
				end
			end
		end

		if @entityId=3 -- roboczogodziny
		begin
			print dbo.f_translate('koszty wpisane w raport nh',default)
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			set @valueZS=80.00/1.23
		end 
	
		if @entityId=16 -- dźwig
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end

			if @dmc<=2599
			begin
				set @valueZS=150.0		
			end		
			else if @dmc<=3399
			begin
				set @valueZS=170.0
			end		
			else if @dmc<=5699
			begin
				set @valueZS=170.0
			end
		end 
	
		if @emptyService=1
		begin
			if @type in (1,10) 
			begin
				set @type=4 
				set @quantity=1
			end
			else
			begin
				set @quantity=null
			end
		end

		if @noFix=1  
		begin
			set @valueZS=50.0/1.23
		end

		if @entityId=11 -- taxi w osobnym pojeździe w usłudze holowania (nieaktywny)
		begin
			if @towing=2
			begin
				set @twrCode='01'
			end
			else
			begin
				set @twrCode='02'
			end
		
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,154', @groupProcessInstanceId = @groupProcessId
			SELECT @persons = value_int FROM @values	

			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,647', @groupProcessInstanceId = @groupProcessId
			SELECT @km = value_int FROM @values	
	
			set @valueZS=@km*3.0/1.23
		end 

		print '@valueZS ADH: ' + ISNULL(CAST(@valueZS AS NVARCHAR(50)),dbo.f_translate('NULL',default))


	END

	--------------------

	if @dni>0 and @entityId=29
		begin 
			set @valueZS=@valueZS/@dni*@quantityZ 
			print dbo.f_translate('dnidd',default)
		end
	else 
		begin
		set @valueZS=@valueZS*@quantityZ 
		end

	-- ROZLICZENIE DLA PUSTEGO WYJAZDU I NAPRAWY NIEUDANEJ.
	if @entityId IN (1,10) AND (@noFix = 1 OR @emptyService = 1) 
	begin
		if @programId in (483,482,481,480,479,478,477,476,475,497,496,495,494,446,445,444,443)
		--@platformId in (35,18)-- Dla programów z listy, rozliczamy pusty wyjazd i naprawę nieudaną na ZS.
		or @programId in (507,498,499,501,502,503,504) -- ARC, AA, ANWB, RACE, Rosqvist, TCB, TCS (Brak pustych wyjazdów)
		or @programId in (500) -- OAMTC (członkowie klubu austriackiego)
		or @platformCFM = 5 --> ADAC
		begin
			set @valueZS=0.00

			if @emptyService = 1 and @programId in (475,476,477,478,479,480,481,482,483)--@platformId = 35 -- TUW TUZ		
			begin
				set @valueZS=70
			end

			if @emptyService = 1 and @programId in (443,444,445,446,494,495,496,497)--@platformId = 18  -- Concordia Assistance			
			begin
				set @valueZS=90.41
			end

			if @programId in (500) -- OAMTC (członkowie klubu austriackiego)
			begin
				set @valueZS = 83.59 -- Ryczałt do 25km
				if @towingKm > 25
				begin
					set @valueZS = @valueZS + 2.10 * (@towingKm - 25)
				end
			end

			if @platformCFM = 5 --> Pusty Wyjazd i Naprawa Nieudana - ADAC
			begin
				set @valueZS = 83.59 -- Ryczałt do 25km
				if (@towingKm+@backKm+@arrivalKm) > 25
				begin
					set @valueZS = @valueZS + 2.10 * ((@towingKm+@backKm+@arrivalKm) - 25)
				end
			end
		end
		else
		begin
			if @emptyService=1 
			begin
				set @valueZS=0
			end
			else if @towVehicleType=2 and @noFix=1 -- pojazd patrolowy, i naprawa nieudana (Płacimy, ZZ=70,ZS=0)
			begin
				set @valueZS=0
			end
			else
			begin
				set @valueZS=null
			end
		end
	end

	if @entityId in (1,10) and @isGopNH=1 --> Organizowanie usługi N/H na GOP.
	begin
		set @valueZS=0.00
		--(select top 1 value_int from attribute_value with(nolock) where group_process_instance_id=@groupProcessId 
					--and attribute_path='638,957' and value_int is not null)
	end
	
	--DEBUG
	if @entityId = 29
	begin
		print '@dni: ' + ISNULL(CAST(@dni AS NVARCHAR(50)),dbo.f_translate('NULL',default))	
	end
	print '@valueZS: ' + ISNULL(CAST(@valueZS AS NVARCHAR(50)),dbo.f_translate('NULL',default))
	print '@quantityZ: ' + ISNULL(CAST(@quantityZ AS NVARCHAR(50)),dbo.f_translate('NULL',default))

	if ((@czyASO=0 or @entityId in (10,18,19,21,22,23,25,26,43,44,63)) or @type<>1 or @programId=428 or 
		(select platform_id from vin_program where id=@programId)=48 --> Dla programów z platformy BL ma sie przenosić mimo że ASO
	) and @quantity>0 and @skipStarterCodeBL=0 AND @programId IS NOT NULL
	begin 
		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			groupProcessId,
			programId
		)	
		SELECT  
		@headerZS,
		@quantityXL,    ---@quantityZ MP
		@twrCode,
		@valueZS,
		@currency,
		@entityValueId,
		@entityId,
		@groupProcessId,--CASE WHEN (@reverseGroupId < 0) THEN @reverseGroupId ELSE @groupProcessId END,
		@programId
					
		print @valueZS
	end
	else
	begin
		print 'Pomija wstawienie ZamElem(ZS). ((@czyASO=0 or @entityId in (10,18,19,21,22,23,25,26,43,44,63)) or @type<>1 or @programId=428) and @quantity>0'
		print '@czyASO: ' + ISNULL(CAST(@czyASO AS NVARCHAR(50)),dbo.f_translate('NULL',default))
		print '@type: ' + ISNULL(CAST(@type AS NVARCHAR(50)),dbo.f_translate('NULL',default))
		print '@quantity: ' + ISNULL(CAST(@quantity AS NVARCHAR(50)),dbo.f_translate('NULL',default))
		print '@skipStarterCodeBL: ' + ISNULL(CAST(@skipStarterCodeBL AS NVARCHAR(50)),dbo.f_translate('NULL',default))	

		IF @programId IS NULL
		BEGIN
			set @valueZS=NULL
			print 'BRAK PROGRAMU! Nie importuje'
		END

	end
	
	FETCH NEXT FROM kurZ INTO @entityValueId, @groupProcessId, @serviceId, @entityId, @quantity, @type, @name, @quantityXL,@programId;
END
CLOSE kurZ
DEALLOCATE kurZ

UPDATE zn
set zn.XL_Opis=isnull(XL_opis,'')+isnull(' '+dt.descr,'')
from sync.ZamNag zn inner join
	@descrTable dt on dt.idHeader=zn.Id

declare @znsId int
declare @znzId int

select	@znsId=zns.Id,
		@znzId=znz.id
from	sync.ZamNag zns inner join
		sync.ZamNag znz on zns.id=znz.ParentZsId 
where	zns.rootId=@rootId and
		znz.rootId=@rootId and 
		zns.XL_ZamSeria=dbo.f_translate('ADH',default)

if @znsId is not null
begin  
	declare @sumZZ decimal(18,2)
	declare @sumZS decimal(18,2)

	select	@sumZS=sum(XL_Wartosc)
	from	sync.ZamElem ze 
	where	ze.AUD_IdDok=@znsId and entityDefId not in (9,5) /*bez autostradan viatol pusty przejazd i nieudana naprawa jesli hol ktos inny*/

	select	@sumZZ=sum(XL_Wartosc)
	from	sync.ZamElem ze 
	where	ze.AUD_IdDok=@znzId and entityDefId not in (9,5) /*bez autostradan viatol pusty przejazd i nieudana naprawa jesli hol ktos inny*/

	delete from sync.ZamElem where AUD_IdDok=@znsId and entityDefId not in (9,5)
	delete from sync.ZamElem where AUD_IdDok=@znzId and entityDefId not in (9,5)

	declare @adgGroupId int = (select top 1 group_process_instance_id from attribute_value where attribute_path='202' and value_string='423' and root_process_instance_id=@rootId order by id)

	declare @roznica decimal(18,2)=@sumZS-@sumZZ

	print '@sumZS :' + ISNULL(CAST(@sumZS AS NVARCHAR(20)),dbo.f_translate('NULL',default)) + ' - ' + '@sumZZ :' + ISNULL(CAST(@sumZZ AS NVARCHAR(20)),dbo.f_translate('NULL',default)) + ' = ' + '@roznica :' + ISNULL(CAST(@roznica AS NVARCHAR(20)),dbo.f_translate('NULL',default))

	if @roznica<0
	begin
		INSERT INTO sync.ZamNag (
			ParentZsId,
			AUD_GidTyp, -- 960
			XL_DataRealizacji, -- data statusu progres 4
			XL_DokumentObcy, -- caseId
			XL_KntAkronim,
			XL_Opis, -- program name
			XL_Typ, -- ZS/ZZ
			XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
			rootId,--SO_SprawaId, -- rootId
			type,
			partnerLocId,
			XL_DataWystawienia
		)
		SELECT 
		null,
		@gidTypeZS, 
		getdate(),--@finishedAt, 
		@caseId,  
		@akronimZSADH, 
		@description, 
		'ZZ',
		'',	
		@rootId,
		@type,
		@partnerId,
		@caseDate

		declare @headerOplataADH2 int
		set @headerOplataADH2=scope_identity()

		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			groupProcessId,
			programId
		)	
		SELECT  
		@headerOplataADH2,
		1,
		dbo.f_translate('KON1',default), -- ADH Prowizja nazwa było -1
		@roznica*-1,
		dbo.f_translate(dbo.f_translate('PLN',default),default),
		null,
		null,
		@adgGroupId,
		423

		set @roznica=0
	end
	-------------------

	if @roznica=0 -- W przypadku, gdy dla programu Ad-hoc mamy obciążenie po stronie ZZ, proszę aby w każdym takim przypadku na ZS był kontrahent 111111.
	begin
		set @akronimZSADH = '111111' 
	end 

	INSERT INTO sync.ZamNag (
			ParentZsId,
			AUD_GidTyp, -- 960
			XL_DataRealizacji, -- data statusu progres 4
			XL_DokumentObcy, -- caseId
			XL_KntAkronim,
			XL_Opis, -- platforma + dbo.f_translate('atlas',default)
			XL_Typ, -- ZS/ZZ
			XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
			rootId,--SO_SprawaId, -- rootId
			type,
			partnerLocId,
			XL_DataWystawienia
		)
		SELECT 
		null,
		@gidTypeZS, 
		getdate(), --@finishedAt, 
		@caseId,  
		@akronimZSADH, 
		@description, 
		'ZS',
		dbo.f_translate('ADH',default),	
		@rootId,
		@type,
		@partnerId,
		@caseDate

		declare @headerOplataADH int
		set @headerOplataADH=scope_identity()
		set @description=dbo.f_translate('ADHOC ',default)+ISNULL(@programName,'')
		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			groupProcessId,
			programId
		)	
		SELECT  
		@headerOplataADH,
		1,
		dbo.f_translate('KON1',default), -- ADH Prowizja nazwa było -1
		@roznica,
		dbo.f_translate(dbo.f_translate('PLN',default),default),
		null,
		null,
		@adgGroupId,
		423
end

--> ========================================================================================== <--
-- Opłata za sprawę / Opłata za sprawę / Opłata za sprawę / Opłata za sprawę / Opłata za sprawę / 
--> ========================================================================================== <--
declare @headerOplata int
declare @casePaymentType int

DECLARE @PROGRAMS AS TABLE(
	platformId INT,
	programId INT,
	entityDefId INT,
	entityValueId INT,
	wasNH BIT
)

DECLARE	@entityDefId INT
DECLARE	@wasNH BIT

-- ====================================================== --
-- \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CF --

/*	1	Wypadek/Szkoda
	2	Awaria
	3	Sprawa nie związana z pojazdem
	5	Szkoda jezdna
	6	Kradzież
	7	Błąd pilotażu
	8	Szkoda całkowita
	9	Porada	*/
DELETE FROM @values --> Rodzaj zdarzenia
INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @rootId
DECLARE @eventType INT = (SELECT TOP 1 value_int FROM @values WHERE value_int IS NOT NULL)

/*	85	BRS Priorytet 1
	94	BRS Priorytet 1A
	95	BRS Zawieszone
	98	BRS LUM
	99	BRS Poszerzony obszar obsługi
	100	BRS Serwis preferowany
	101	BRS COMPET	*/
DECLARE @isBRS BIT = 0
IF EXISTS (
	SELECT av_p.value_text,av.* FROM attribute_value av 
	JOIN attribute_value av_p ON av.value_int=av_p.parent_attribute_value_id AND av_p.attribute_path = '595,597,596' 
	AND EXISTS ( SELECT * FROM dbo.f_split (av_p.value_text,',') WHERE data in (85,94,95,98,99,100,101))
	WHERE av.root_process_instance_id=@rootId AND av.attribute_path='610' AND av.value_int IS NOT NULL
)BEGIN
	SET @isBRS = 1
END

DECLARE @onlyAlphabetPrograms BIT = 1 --> W sprawie istnieją tylko programy Alphabet
IF EXISTS(
	SELECT av.value_string FROM attribute_value av 
	WHERE av.root_process_instance_id=@rootId AND av.attribute_path='202' AND av.value_string IS NOT NULL
	AND av.value_string NOT IN (select id from dbo.vin_program where platform_id=53) --> Alphabet
)
BEGIN
	SET @onlyAlphabetPrograms = 0 --> Mix programów
END

DECLARE @onlyLeaseplanPrograms BIT = 1 --> W sprawie istnieją tylko programy Leaseplan
IF EXISTS(
	SELECT av.value_string FROM attribute_value av 
	WHERE av.root_process_instance_id=@rootId AND av.attribute_path='202' AND av.value_string IS NOT NULL
	AND av.value_string NOT IN (select id from dbo.vin_program where platform_id=25) --> Leaseplan
)
BEGIN
	SET @onlyLeaseplanPrograms = 0 --> Mix programów
END

--> Czy w sprawie jest Monitoring Szkody?
DECLARE @wasDetrimentMonitoring BIT = 0 
IF EXISTS(SELECT 1 FROM process_instance WITH(NOLOCK) WHERE root_id=@rootId AND step_id LIKE '1155.%')
BEGIN
	SET @wasDetrimentMonitoring = 1
END
-- \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CFM \\ CF --
-- ====================================================== --

DECLARE @tmpPlatformID INT

DECLARE programCursor CURSOR
FOR 
	SELECT DISTINCT vp.platform_id as platformId,ze.programId,ze.entityDefId,ze.entityValueId,
	(CASE WHEN vp.platform_id=2 AND ze.entityDefId IN (1,10) THEN 1 ELSE 0 END) wasNH

	FROM [sync].[ZamElem] ze 
	LEFT JOIN [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id AND zn.AUD_PrzeniesionoDoErpXl IS NULL
	LEFT JOIN dbo.vin_program vp ON vp.id=ze.programId
	WHERE zn.rootId = @rootId
	AND ze.XL_TwrKod <> '-1'
	AND (
	
	ze.programId IN (select distinct data from f_split((select CONCAT(programId,',') from AtlasDB_def.dbo.casePayment FOR XML PATH('')),','))

	OR 
	
	vp.platform_id IN (select cp.platformId from AtlasDB_def.dbo.casePayment cp where cp.platformId is not null)

	)

	--> Wykluczenie: VGP (Tylko usługi główne. Nie generuje się dla pustych wyjazdów i napraw nieudanych)
	AND (CASE WHEN vp.platform_id IN (6,11,31) AND ( --> Poniżej kryteria dla których ma niewystępować opłata za sprawę.
			   zn.XL_Opis LIKE '%Pusty wyjazd%' 
			OR zn.XL_Opis LIKE '%Naprawa nieudana%'
			OR ze.entityDefId NOT IN (10,1,21,29,43,19,23,14,22,63) --> Tylko usługi główne X
			OR (SELECT ISNULL(av_P.value_int,0) FROM attribute_value av_P WITH(NOLOCK) WHERE av_P.parent_attribute_value_id=ze.entityValueId AND attribute_path='820,821,822,824')=0 --> Rozliczenie na producenta
		)
	THEN 0 ELSE 1 END) = 1

	--> Wykluczenie: Ford (Brak opłaty dla programów z pustym wyjazdem i naprawą nieudaną)
	AND (CASE WHEN vp.platform_id = (2) AND ( --> Poniżej kryteria dla których ma niewystępować opłata za sprawę.
				zn.XL_Opis LIKE '%Pusty wyjazd%' 
			 OR zn.XL_Opis LIKE '%Naprawa nieudana%'
		)
	THEN 0 ELSE 1 END) = 1

	--> Wykluczenie: ARC, AA, ANWB, RACE, Rosqvist, TCB, TCS (Opłata za sprawę występuje tylko jeśli jest usługa MEDICAL)
	AND (CASE WHEN @programId IN (507,498,499,501,502,503,504) AND @IsMedicalInCase = 0 THEN 0 ELSE 1 END) = 1

	--> Wykluczenie: ALD tylko jeśli jest transport.
	AND (CASE WHEN @platformId = 43 AND @isTransport = 0 THEN 0 ELSE 1 END) = 1

OPEN programCursor FETCH NEXT FROM programCursor INTO @tmpPlatformID,@programId,@entityDefId,@entityValueId,@wasNH

WHILE @@FETCH_STATUS = 0
BEGIN
	-- sprawdzenie czy istnieje oplata
	INSERT INTO @PROGRAMS (platformId,programId,entityDefId,entityValueId,wasNH) VALUES (@tmpPlatformID,@programId,@entityDefId,@entityValueId,@wasNH)
	FETCH NEXT FROM programCursor INTO @tmpPlatformID,@programId,@entityDefId,@entityValueId,@wasNH
END
CLOSE programCursor;
DEALLOCATE programCursor;

DECLARE casePayment CURSOR
FOR 
SELECT sub.platformId,programId,wasNH FROM (SELECT ROW_NUMBER() OVER(PARTITION BY platformId ORDER BY platformId,wasNH DESC) AS RowN, * FROM @PROGRAMS) sub WHERE sub.RowN=1
OPEN casePayment
FETCH NEXT FROM casePayment INTO @platformId,@programId,@wasNH

WHILE @@FETCH_STATUS = 0
BEGIN
	-- //  
	SET @serieZS = NULL
	SET @XL_FlagaNBZS = NULL
	DECLARE @skipCasePayment BIT = 0 --> Mechanizm sprawdzający czy dla sprawy już istnieje opłata za sprawę. Jesli tak to pomija dodawanie.
	----------------------------------
	DECLARE @generateZS BIT = 0
	SET @casePaymentType = 1
	SET @programName = (select top 1 name from dbo.vin_program where id=@programId)

	--> Pobieranie zagłówna, do którego ma zostać podłączona opłata za sprawę.
	SET	@headerOplata = (	select TOP 1 zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
							where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	)

	IF @platformId = 2 AND @wasNH <> 1 --> Ford
	BEGIN
		SET @AkronimZS = '100009'
		SET @casePaymentType = 2
	END

	IF @platformId IN (6,11,31) --> VGP
	BEGIN
		SET @AkronimZS='103600'
		-------------------------
		if @programId in (414,415,427,426) -- Audi i ARC
		begin
			SET @serieZS = 'AU'
		end

		if @programId in (416,402,421,427,425) -- Skoda i ARC
		begin
			SET @serieZS = dbo.f_translate('SKO',default)
		end

		if @programId in (417,418,419,420,427,424) -- Volkswagen i ARC
		begin
			SET @serieZS = 'VW'
		end

		SET @generateZS = 1
	END

	IF @platformId IN (53) --> ALPHABET
	BEGIN
		set @AkronimZS='105000'
		set @casePaymentType = case 
		when @eventType in (1,2,3,6,7,8) AND @isBRS=0 AND @onlyAlphabetPrograms=1  then 1
		when @eventType in (1,2,3,6,7,8) AND @isBRS=1 AND @onlyAlphabetPrograms=1  then 2
		when /*@eventType =  5 AND*/ @wasDetrimentMonitoring=1 then 4
		when @onlyAlphabetPrograms=0 then 5
		else null end
	END

	IF @platformId IN (58) --> ALPHABET WSPARCIE KIEROWCY, DODAJE NA KOŃCU
	BEGIN
		set @AkronimZS='105000'
		set @casePaymentType = 3
	END

	IF @platformId IN (43) --> ALD
	BEGIN
		set @AkronimZS='102725'
		set @casePaymentType = case when @isTransport=1 then 1 else null end

		SET	@headerOplata = (select TOP 1 zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
							where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim=@AkronimZS
							and ze.entityDefId=63)
		
		IF @headerOplata IS NULL
		BEGIN
			SET	@headerOplata = (select TOP 1 zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
							where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim=@AkronimZS)
		END

		IF @isTransport=0 --> Transport jest wymagany by została zaliczona opłata za sprawę.
		BEGIN
			SET @skipCasePayment = 1
			PRINT 'ALD » Brak transportu.'
		END

	END
	/*	1	Wypadek/Szkoda
		2	Awaria
		3	Sprawa nie związana z pojazdem
		5	Szkoda jezdna
		6	Kradzież
		7	Błąd pilotażu
		8	Szkoda całkowita
		9	Porada	*/
	IF @platformId IN (48) --> BL
	BEGIN
		set @AkronimZS='104000'
		set @casePaymentType = case 
		when @eventType in (2,7) then 1 --> Awaria lub błąd pilotażu
		when @eventType in (1,5) then 2 --> Wypadek, pojazd jezdny
		when @eventType in (1,3,6,8) and @isTowing=1 then 3 --> Wypadek z holowaniem
		else null end


		--> Pobieranie zagłówna, do którego ma zostać podłączona opłata za sprawę.
		SET	@headerOplata = (	select TOP 1 zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
								where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim=@AkronimZS)
	END

	IF @platformId IN (25) --> Leaseplan
	BEGIN
		set @AkronimZS='102800'
		set @casePaymentType = case 
		when @onlyLeaseplanPrograms = 1 then 1 --> Programy Leaseplan
		when @onlyLeaseplanPrograms = 0 then 2 --> Mix programów
		else null end

		declare @casePaymentHeaderLP int = (
			(select TOP 1 zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
			where zn.rootId = @rootId and ze.programId in (select id from vin_program where platform_id=@platformId) and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_Opis like '%EIIE%')
		)

		if isnull(@casePaymentHeaderLP,0) <> 0 --> Opłata za sprawę w pierwszej kolejności powinna się przypisywać do zleceń EIIE 
		begin
			set @headerOplata = @casePaymentHeaderLP
		end
	END

	IF ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default))<>dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default) --> Dla spraw zagranicznych wartość typu jest na minusie.
	BEGIN -- Zagranica
		SET @casePaymentType = @casePaymentType * -1
	END

	IF @platformId IN (8,13,16,20,21,22,50,64,68,73,74) --> TRUCK
	BEGIN
		SET @casePaymentType = 1
		SET	@headerOplata = (	select TOP 1 zn.id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
								where zn.rootId = @rootId and ze.programId = @programId and zn.XL_Typ = 'ZS' and zn.AUD_PrzeniesionoDoErpXl is null	and XL_KntAkronim=@AkronimZS and XL_TwrKod in ('31','32','33','34'))

		IF ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default))<>dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default) --> Dla spraw zagranicznych wartość typu jest na minusie.
		BEGIN -- sprawy za granica powinny sie przenosić tylko z dowozem gotówki
			SET @valueZS = NULL
			SET @skipCasePayment = 1
		END

			--SET @valueZS = NULL
			--SET @skipCasePayment = 1
	END


	SET @currency = ISNULL(@currency,dbo.f_translate(dbo.f_translate('PLN',default),default))

	SELECT TOP 1 @valueZS=value,@currency=currency FROM [AtlasDB_def].[dbo].[casePayment] 
	WHERE (programId LIKE '%'+CAST(@programId AS nvarchar(10))+'%' OR platformId=@platformId) AND type = @casePaymentType
	--WHERE programId LIKE '%'+CAST(@programId AS nvarchar(10))+'%' AND type = @casePaymentType

	if @rootId = 2536991
	begin
		SELECT  * FROM [AtlasDB_def].[dbo].[casePayment] 
		WHERE (programId LIKE '%'+CAST(@programId AS nvarchar(10))+'%' OR platformId=@platformId) AND type = @casePaymentType
	end


	------------
	DECLARE @ZeValueZS DECIMAL(18,2) = ISNULL((SELECT sum(XL_Wartosc) FROM [sync].[ZamElem] ze LEFT JOIN [sync].[ZamNag] zn ON ze.AUD_IdDok=zn.Id 
				WHERE zn.rootId=@rootId AND ze.XL_TwrKod='-1' AND ze.programId = @programId),0.00)

	IF @ZeValueZS < @valueZS 
	BEGIN
		PRINT dbo.f_translate('Dodawanie opłaty za sprawe',default)
		IF @ZeValueZS < @valueZS AND @ZeValueZS<>0.00
		BEGIN
			PRINT dbo.f_translate('Dopisywanie pozostałej opłaty za sprawę',default)
			SET @valueZS = @valueZS - @ZeValueZS
			PRINT @platformId
			IF @platformId IN (8,13,16,20,21,22,50,64,68,73,74) --> TRUCK
			BEGIN
				SET @valueZS = 0.00
				IF EXISTS (SELECT TOP 1 zn.XL_ZamSeria FROM [sync].[ZamElem] ze LEFT JOIN [sync].[ZamNag] zn ON ze.AUD_IdDok=zn.Id WHERE XL_TwrKod = '-1')
				BEGIN
					SET @skipCasePayment = 0
				END

				IF ISNULL(@country,dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default))<>dbo.f_translate(dbo.f_translate(dbo.f_translate(dbo.f_translate('Polska',default),default),default),default) --> Dla spraw zagranicznych wartość typu jest na minusie.
				BEGIN -- sprawy za granica powinny sie przenosić tylko z dowozem gotówki bez opłaty za sprawę
					SET @valueZS = NULL
					SET @skipCasePayment = 1
				END
			END

			SET @serieZS = (
				SELECT TOP 1 zn.XL_ZamSeria FROM [sync].[ZamElem] ze LEFT JOIN [sync].[ZamNag] zn ON ze.AUD_IdDok=zn.Id 
				WHERE zn.rootId=@rootId AND ze.XL_TwrKod='-1' AND ze.programId = @programId AND zn.XL_ZamSeria IS NOT NULL ORDER BY XL_ZamSeria DESC
			)

			SET @XL_FlagaNBZS = (
				SELECT TOP 1 zn.XL_FlagaNB FROM [sync].[ZamElem] ze LEFT JOIN [sync].[ZamNag] zn ON ze.AUD_IdDok=zn.Id 
				WHERE zn.rootId=@rootId AND ze.XL_TwrKod='-1' AND ze.programId = @programId AND zn.XL_FlagaNB IS NOT NULL ORDER BY XL_FlagaNB DESC
			)

			SET @generateZS = 1
		END
	END
	ELSE
	BEGIN
		PRINT dbo.f_translate('Pomiń opłatę za sprawę: ',default) + ISNULL(CAST(@valueZS AS NVARCHAR(20)),dbo.f_translate('NULL',default)) + dbo.f_translate('. Program: ',default) + @programName
		SET @skipCasePayment = 1
	END
	------------

	IF @skipCasePayment = 0
	BEGIN
		IF @generateZS = 1
		BEGIN
			SET @headerOplata = NULL --> Dla VGP opłata za sprawę generuje się na osobnym nagłówku.

			print dbo.f_translate('Dodaje nagłówek ZS dla opłaty za sprawę',default)
			INSERT INTO sync.ZamNag (
				ParentZsId,
				AUD_GidTyp, -- 960
				XL_DataRealizacji, -- data statusu progres 4
				XL_DokumentObcy, -- caseId
				XL_KntAkronim,
				XL_Opis, -- platforma + dbo.f_translate('atlas',default)
				XL_Typ, -- ZS/ZZ
				XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
				rootId,--SO_SprawaId, -- rootId
				type,
				partnerLocId,
				XL_DataWystawienia,
				XL_TargetKntAkronim,
				XL_FlagaNB
			)
			SELECT 
			null,
			@gidTypeZS, 
			getdate(),--@finishedAt, 
			@caseId,  
			'103600', --Akronim VGP
			@programName, -- Description (opis)
			'ZS',
			@serieZS,	
			@rootId,
			1, -- Type (Producent)
			null,--@partnerId, -- Czy to musi być?
			@caseDate,
			'', /*Docelowy*/
			isnull(@XL_FlagaNBZS,'N')
		END

		IF @platformId = 8 OR @programId = 522 --> DKV
		BEGIN
			PRINT dbo.f_translate('DKV',default)
			SET @valueZS = 0.00
		END

		INSERT INTO sync.ZamElem(
			AUD_IdDok, -- id nagłówka
			XL_Ilosc, -- zmienna
			XL_TwrKod, -- zmienna 
			XL_Wartosc, -- zmienna 
			XL_Waluta, -- zmienna 
			entityValueId, -- entityId
			entityDefId,
			programId
		)	
		SELECT  
		ISNULL(@headerOplata,scope_identity()),
		1,
		'-1',
		@valueZS,
		@currency,
		null,
		null,
		@programId

		PRINT dbo.f_translate('Opłata za sprawę: ',default) + ISNULL(CAST(@valueZS AS NVARCHAR(20)),dbo.f_translate('NULL',default)) + dbo.f_translate('. Program: ',default) + @programName
	END

	FETCH NEXT FROM casePayment INTO @platformId,@programId,@wasNH
END
CLOSE casePayment;
DEALLOCATE casePayment;

-- ==================================================== --
-- Dodanie opłaty za sprawę Alphabet Wspracie Kierowcy. --
-- ==================================================== --
--IF @platformId = 58	AND NOT EXISTS(SELECT 1 FROM SYNC.ZamNag WITH(NOLOCK) WHERE rootId=@rootId) AND NOT EXISTS (SELECT root_id FROM process_instance WHERE step_id='1011.022' AND root_id=@rootId)
--BEGIN
--	PRINT dbo.f_translate('Dodaje opłatę za sprawę Alphabet Wspracie Kierowcy',default)
--	SET @valueZS = (SELECT TOP 1 value FROM AtlasDB_def.dbo.casePayment WITH(NOLOCK) WHERE type=3 AND platformId=58)
--	SET @currency = (SELECT TOP 1 currency FROM AtlasDB_def.dbo.casePayment WITH(NOLOCK) WHERE type=3 AND platformId=58)
--	SET @serieZS = dbo.f_translate('ALF',default)
--	SET @AkronimZS='105000'

--	INSERT INTO sync.ZamNag (
--					ParentZsId,
--					AUD_GidTyp, -- 960
--					XL_DataRealizacji, -- data statusu progres 4
--					XL_DokumentObcy, -- caseId
--					XL_KntAkronim,
--					XL_Opis, -- platforma + dbo.f_translate('atlas',default)
--					XL_Typ, -- ZS/ZZ
--					XL_ZamSeria, -- zadeklarować zmienną i wstawić wstępnie VW
--					rootId,--SO_SprawaId, -- rootId
--					type,
--					partnerLocId,
--					XL_DataWystawienia,
--					XL_TargetKntAkronim,
--					XL_FlagaNB
--				)
--				SELECT 
--				null,
--				1280, 
--				getdate(),--@finishedAt, 
--				@caseId,  
--				@AkronimZS,
--				dbo.f_translate('Alphabet wsparcie kierowcy',default), -- Description (opis)
--				'ZS',
--				@serieZS,	
--				@rootId,
--				1, -- Type (Producent)
--				null,--@partnerId, -- Czy to musi być?
--				@caseDate, --> Numer sprawy A0XXXXXX
--				'', /*Docelowy*/
--				'N' --> Netto

--	INSERT INTO sync.ZamElem(
--		AUD_IdDok, -- id nagłówka
--		XL_Ilosc, -- zmienna
--		XL_TwrKod, -- zmienna 
--		XL_Wartosc, -- zmienna 
--		XL_Waluta, -- zmienna 
--		entityValueId, -- entityId
--		entityDefId,
--		programId
--	)	
--	SELECT  
--	scope_identity(),
--	1,
--	'-1',
--	@valueZS,
--	@currency,
--	null,
--	null,
--	506 --> Alphabet wsparcie kierowcy

--	IF NOT EXISTS (SELECT TOP 1 value_text FROM attribute_value WITH(NOLOCK) WHERE attribute_path='406,226,227' AND root_process_instance_id=@rootId AND value_text=dbo.f_translate(dbo.f_translate('Przeniesiono do CDNa.',default),default))
--	BEGIN
--		DECLARE @err TINYINT
--		DECLARE @message NVARCHAR(255)
--		EXECUTE dbo.p_note_new -- Dodawanie notatki
--				@groupProcessId = @rootId
--				,@type = dbo.f_translate('text',default)
--				,@content = dbo.f_translate(dbo.f_translate('Przeniesiono do CDNa.',default),default)
--				,@userId = 1  -- automat
--				,@err = @err
--				,@message = @message
--	END
--END

--====================================--
--        Kasowanie zamówień          --
--====================================--

-- 1. Kasuje Nag, które nie mają Elem
delete from sync.ZamNag where ParentZsId in (
	select zn.id from sync.ZamNag zn 
		left join sync.ZamElem ze on ze.AUD_IdDok=zn.id
	where ze.id is null and zn.rootId=@rootId
) 

-- 2. Kasuje Elem z wartością NULL
delete from sync.zamElem where XL_Wartosc is null and AUD_IdDok in(select zn.id from sync.ZamNag zn where zn.rootId=@rootId) 

-- 3. Kasuje Nag ja w pkt 1. ??
delete from sync.ZamNag where rootId=@rootId and id in(
	select zn.id from sync.ZamNag zn 
		left join sync.ZamElem ze on ze.AUD_IdDok=zn.id
	where ze.id is null and zn.rootId=@rootId
) and AUD_WynikAPI is null

---- 5. Kasowanie ZZ jeśli nie posiada ZS
--DELETE FROM sync.ZamElem where Id IN (
--	select ze.Id from [sync].[ZamElem] ze left join [sync].[ZamNag] zn on ze.AUD_IdDok=zn.Id
--	where zn.rootId = @rootId and zn.XL_Typ = 'ZZ' and AUD_WynikAPI is null
--	and not exists(select * from sync.ZamNag zn2 
--		left join [sync].[ZamElem] ze2 on ze2.AUD_IdDok=zn2.Id
--		where zn2.XL_Typ = 'ZS' and zn2.rootId = @rootId and ze2.entityValueId = ze.entityValueId)
--) AND AUD_GidLp is null

COMMIT TRANSACTION

END
