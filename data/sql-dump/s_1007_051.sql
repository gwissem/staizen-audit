ALTER PROCEDURE [dbo].[s_1007_051]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @content NVARCHAR(4000)
	DECLARE @postponeCount INT
	DECLARE @postponeLimit INT
	DECLARE @p1 NVARCHAR(100)
	DECLARE @c NVARCHAR(300)
	
	SELECT	@groupProcessInstanceId = p.group_process_id, @postponeCount = p.postpone_count, @postponeLimit = s.postpone_count		
	FROM process_instance p with(nolock)
	INNER JOIN dbo.step s with(nolock) ON p.step_id = s.id  
	WHERE p.id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
		
	SET @variant = 99
	
	IF @postponeCount = 0
	BEGIN
		EXEC [dbo].[p_change_priority] @priority = 0, @type = 'rental_fixing_date', @groupProcessInstanceId = @groupProcessInstanceId
		
		SET @content = dbo.f_translate('Prosimy o potwierdzenie otrzymania samochodu zastępczego. Odpowiedz TAK lub NIE na niniejszą wiadomość.',default)
	
		EXEC dbo.p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @callerPhone,
		@addInfo = 1,
		@err = @err OUTPUT,
		@message = @message OUTPUT

	END 
	ELSE IF @postponeCount >= @postponeLimit
	BEGIN
		SET @variant = 2
		SET @c = dbo.f_translate('Klient nie potwierdził SMSem odbioru pojazdu zastępczego. Wygenerowanie zadania 1007.052 dla konsultanta',default)
	END
	ELSE
	BEGIN
		SET @variant = 99
		
		DECLARE @checkDate DATETIME
		DECLARE @smsYes INT
		DECLARE @smsNo INT
		SELECT @checkDate = created_at FROM dbo.process_instance with(nolock) WHERE id = @previousProcessId			
		SELECT TOP 1 @smsYes = id FROM sms_inbox with(nolock) WHERE numer_telefonu = '48'+right(@callerPhone,9) and data_otrzymania >= @checkDate AND sms_text like dbo.f_translate('tak',default)
		SELECT TOP 1 @smsNo = id FROM sms_inbox with(nolock) WHERE numer_telefonu = '48'+right(@callerPhone,9) and data_otrzymania >= @checkDate AND sms_text like dbo.f_translate('nie',default)
		
		IF @smsYes IS NOT NULL
		BEGIN
			SET @variant = 1 
			SET @c = dbo.f_translate('Klient potwierdził odbioru pojazdu zastępczego za pomocą SMS',default)
		END 
		ELSE IF @smsNo IS NOT NULL
		BEGIN
			SET @variant = 2
			
			EXEC [dbo].[p_attribute_edit]
		     @attributePath = '816', 
		     @groupProcessInstanceId = @groupProcessInstanceId,
		     @stepId = 'xxx',
		     @userId = 1,
		     @originalUserId = 1,
		     @valueInt = 0,
		     @err = @err OUTPUT,
		     @message = @message OUTPUT
		     
			SET @c = dbo.f_translate('Klient potwierdził brak odbioru pojazdu zastępczego za pomocą SMS. Wygenerowanie zadania 1007.052 dla konsultanta',default)
		END 
						
	END 
	
	UPDATE dbo.process_instance SET postpone_count = postpone_count + 1 , postpone_date = DATEADD(SECOND, 30, GETDATE()) where id = @previousProcessId
	
	-- LOGGER START
	SET @p1 = dbo.f_caseId(@groupProcessInstanceId)
	EXEC dbo.p_log_automat
	@name = 'rental_automat',
	@content = @c,
	@param1 = @p1
	-- LOGGER END
	
	
END




