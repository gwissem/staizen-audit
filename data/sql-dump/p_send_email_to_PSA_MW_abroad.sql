ALTER PROCEDURE [dbo].[p_send_email_to_PSA_MW_abroad] @groupProcessInstanceId INT 
AS
BEGIN

	/* Mail do PSA o MW za granicą, 4tego dnia roboczego */

	DECLARE @rootId int 
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
    DECLARE @body nvarchar(max)
    DECLARE @vin nvarchar(100)
    DECLARE @subject nvarchar(1000)
    DECLARE @email nvarchar(255)
    DECLARE @senderEmail nvarchar(255)
    DECLARE @dw nvarchar(255)
    DECLARE @stepId NVARCHAR(255)    
    DECLARE @emailTo NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
    DECLARE @title NVARCHAR(MAX)
    DECLARE @platformId INT
	DECLARE @platformName NVARCHAR(255)	   
    DECLARE @sender NVARCHAR(400)
    DECLARE @contentNote NVARCHAR(1000)
    declare @fixingDays int
    DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
    
    DECLARE @hoursTillFix int 
    declare @rentalStartDate datetime 
    declare @fixDate datetime 
    declare @requestedRentalDuration int
    
    DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixDate = value_date FROM @values
	
    DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @rentalStartDate = value_date FROM @values
	
	SELECT @hoursTillFix = DATEDIFF(minute, @rentalStartDate, @fixDate)
	SELECT @requestedRentalDuration = CEILING(CAST(@hoursTillFix AS FLOAT)/1440)
	
		SET @content = '
			Szanowni Państwo,</br></br>
	
			Przesyłamy powiadomienie dot. wynajmu w przedmiotowej sprawie. Prosimy o zgodę na przedłużenie wynajmu łącznie na '+cast(@requestedRentalDuration as nvarchar(255))+' dni kalendarzowych.<br><br>
			
			Nr sprawy: {#caseid#}
			Nr rej.:  {@74,72@}</br>
			Nr VIN:  {@74,71@}</br>
			Data i godzina zgłoszenia: {#showDatetime({#processInfo(1007.037|created_at)#}|1)#} <br>
			Opis usterki: {#diagnosisDescription(true)#} / {@417@}<br>
			Data i godzina początku wynajmu: {#showDatetime({@540@}|1)#} <br />
			Data i godzina końca wynajmu: {#showDatetime({#rentalSummary(end_date)#}|1)#} <br />
			Liczba dni wynajmu: {#rentalSummary(real_duration)#} <br />' +
			                   '
			Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres kz@starter24.pl
			
			Z poważaniem
			Starter24 Sp. z o.o. <br> <br>'
	
	    EXEC [dbo].[P_parse_string]
	        @processInstanceId = @groupProcessInstanceId,
	        @simpleText = '{#caseid#}, {@74,72@}, Powiadomienie o wynajmie w sprawie zagranicznej',
	        @parsedText = @title OUTPUT
	
	    EXEC [dbo].[P_get_body_email]
	        @body = @body OUTPUT,
	        @contentEmail  = @content,
	        @title = @title,
	        @previousProcessId = @groupProcessInstanceId
	
	    SELECT @contentNote = dbo.f_translate('Powiadomienie o 4tej dobie roboczej wynajmu',default)
	
	    DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @platformId = value_int FROM @values
	
	    IF @platformId IN (80)
	    BEGIN
			SET @emailTo = 'ap-assistance@peugeot.com'    
	    END 
		ELSE IF @platformId IN (79,81)
	    BEGIN
			SET @emailTo = 'ac-assistance@citroen.com'    
	    END 
	    
	    IF @emailTo IS NOT NULL
	    BEGIN		
		    SET @email = dbo.f_getRealEmailOrTest(@emailTo)    
		    SET @sender = dbo.f_getEmail('callcenter')
		    SELECT @platformName = name FROM dbo.platform where id = @platformId
			SET @body = REPLACE(@body,'__EMAIL__', @sender)
			SET @body = REPLACE(@body,'__PLATFORM_NAME__', @platformName)
		
		    EXECUTE dbo.p_note_new
		        @groupProcessId = @groupProcessInstanceId
		        ,@type = dbo.f_translate('email',default)
		        ,@content = @contentNote
		        ,@email = @email
		        ,@userId = 1  -- automat
		        ,@subject = @title
		        ,@direction=1
		        ,@sender = @sender
		 ,@emailBody = @body
		        ,@err=@err OUTPUT
		        ,@message=@message OUTPUT
		    	, @emailRegards =  1
		    
	    END 
	    

  END