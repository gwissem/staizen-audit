ALTER PROCEDURE [dbo].[s_1014_070]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err TINYINT
	DECLARE @message NVARCHAR(255)
	DECLARE @caseId NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	
	SELECT @groupProcessInstanceId = group_process_id FROM process_instance where id = @previousProcessId
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	declare @partnerEmail nvarchar(200)
	declare @vin varchar(50)
	declare @regNumber varchar(20)
	declare @partnerLocId int

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerLocId = value_int FROM @values
		
--	SELECT	@partnerEmail=avContactEmail.value_string
--	from dbo.attribute_value avLocation with(nolock) inner join
--			dbo.attribute_value avPartnerLocStatus with(nolock) on avPartnerLocStatus.parent_attribute_value_id=avLocation.id and avPartnerLocStatus.attribute_path='595,597,676' and avPartnerLocStatus.value_int=3 inner join
--			dbo.attribute_value avContact with(nolock) on avContact.parent_attribute_value_id=avLocation.id and avContact.attribute_path='595,597,642' inner join
--			dbo.attribute_value avContactType with(nolock) on avContactType.parent_attribute_value_id=avContact.id and avContactType.attribute_path='595,597,642,643' and avContactType.value_int=4 inner join
--			dbo.attribute_value avContactEmail with(nolock) on avContactEmail.parent_attribute_value_id=avContact.id and avContactEmail.attribute_path='595,597,642,656'  and avContactEmail.value_string is not null 
--	where avLocation.attribute_path='595,597' and avLocation.id=@partnerLocId

	SELECT @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId,@partnerLocId, null, 4)
	
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '73,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VIN = value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
		
	declare @persons int

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '137,154', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @persons = value_int FROM @values
		
	declare @when datetime

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '137,753', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @when = value_date FROM @values
		
	declare @addressId int

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '137,85', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @addressId = id FROM @values
	
	declare @remarksAtlas nvarchar(4000)

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '137,63', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @remarksAtlas = value_text FROM @values
	
		
	declare @address nvarchar(200)
	set @address=dbo.f_addressText(@addressId)

	set @caseId=dbo.f_caseId(@groupProcessInstanceId)

	declare @subject nvarchar(100)
	declare @remarks nvarchar(4000)
	declare @body nvarchar(4000)
	set @subject=dbo.f_translate('Request for service - case nr: ',default)+isnull(@caseId,'')
	
	declare @userName nvarchar(100)
	select @userName=isnull(firstname,'') +' '+isnull(lastname,'') from dbo.fos_user where id=@currentUser

	-- Przekopiowanie atrybutu AssistanceOrganizationRequest
	EXEC [dbo].[p_copyAssistanceOrganizationRequest] @groupProcessInstanceId = @groupProcessInstanceId
	
	EXEC dbo.p_attribute_edit
		@attributePath = '838,840', -- konsultant 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valuestring = @userName,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	
	EXEC dbo.p_attribute_edit
		@attributePath = '838,839', -- id usługi
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueint = 4,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	set @remarks=isnull(cast(@persons as varchar(10)),'')+' person(s)
				 on '+isnull(dbo.FN_VDate(@when),'')+'
				 near the address '+isnull(cast(@address as varchar(200)),'')+'
				 '+isnull(@remarksAtlas,'')


	EXEC dbo.p_attribute_edit
		@attributePath = '838,63', -- uwagi
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valuetext = @remarks,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	
	set @body=dbo.f_translate('Regarding case no ',default)+isnull(@caseId,'')+'<BR>'+
				isnull('Registration number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
				Please organize the hotel as in attachment<BR><BR><BR>

				Best regards<BR>
				Starter24'

		DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')

		EXECUTE dbo.p_note_new 
		   @sender=@senderEmail
		  ,@groupProcessId=@groupProcessInstanceId
		  ,@type=dbo.f_translate('email',default)
		  ,@content=@body
		  ,@phoneNumber=null
		  ,@email=@partnerEmail
		  ,@userId=@currentUser
		  ,@subType=null
		  ,@attachment=null
		  ,@subject=@subject
		  ,@direction=1
		  ,@addInfo=null
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT
		  ,@emailBody=@body
		  ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'

	set @variant=1
	
	-- Skok do panelu usług
	EXEC dbo.p_jump_to_service_panel @previousProcessId = @previousProcessId, @variant = @variant
end



