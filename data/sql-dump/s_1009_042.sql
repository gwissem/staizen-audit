ALTER PROCEDURE [dbo].[s_1009_042]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @platformId INT
	DECLARE @programId NVARCHAR(255)
	DECLARE @content NVARCHAR(MAX) = 'Nie uzupełniono komunikatu - 1009_042'
	DECLARE @message NVARCHAR(255)
	DECLARE @carDMC INT
	DECLARE @country NVARCHAR(50)
	DECLARE @transportedPeopleWithTowing INT
	DECLARE @err INT
	DECLARE @towingType INT
	DECLARE @caseAbroad int 
	DECLARE @homePatrolAbroad int
	DECLARE @finalDestinantionInCountrySpecial int
	DECLARE @partnerId int
	DECLARE @newProcessInstanceId int 
	DECLARE @eventType INT
	DECLARE @transshipmentId int 
	
	SET @variant = 1
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance WITH(NOLOCK)
	where id = @previousProcessId
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventType = value_int FROM @values
	
	
	
	-- LEASE PLAN 
	DECLARE @consult int = 0
	EXEC dbo.p_lp_consult_workshop_routing @groupProcessInstanceId = @groupProcessInstanceId, @consult = @consult output
	
--	select @eventType, @consult
	
	IF @consult = 1 and @partnerId is null
	BEGIN
		EXEC [dbo].[p_process_jump]
		@previousProcessId = @previousProcessId,
		@nextStepId = '1009.029',		
		@parentId = @rootId,
		@rootId = @rootId,
		@groupId = @groupProcessInstanceId,
	    @err = @err output,
	    @message = @message output,
	    @newProcessInstanceId = @newProcessInstanceId output
	    
	    EXEC [dbo].[p_attribute_edit]
		     @attributePath = '985,292', 
		     @groupProcessInstanceId = @groupProcessInstanceId,
		     @stepId = 'xxx',
		     @userId = 1,
		     @originalUserId = 1,
		     @valueInt = 1,
		     @err = @err OUTPUT,
		     @message = @message OUTPUT
		     
	    return
	END
	ELSE IF @platformId IN (48,78) AND @eventType = 1
	BEGIN
		declare @jump int
		set @jump=1
			
--		select @platformId, dbo.f_abroad_case(@groupProcessInstanceId)
		
		if @platformId=48
		begin
			declare @fleet nvarchar(100)
			exec dbo.p_get_vin_headers_by_rootId @root_id=@rootId, @columnName=dbo.f_translate('Leasingobiorca',default), @output=@fleet OUTPUT

			if @fleet like '%kaufland%' set @jump=0
		end
		else if @platformId=78 
		and dbo.f_abroad_case(@groupProcessInstanceId) = 1 or exists(select id from dbo.process_instance where step_id = '1009.029' and group_process_id = @groupProcessInstanceId)
		BEGIN
			set @jump = 0
			 
		END 
		
		if @jump=1 
		begin
			EXEC [dbo].[p_process_jump]
			@previousProcessId = @previousProcessId,
			@nextStepId = '1009.002',		
			@parentId = @rootId,
			@rootId = @rootId,
			@groupId = @groupProcessInstanceId,
			@err = @err output,
			@message = @message output,
			@newProcessInstanceId = @newProcessInstanceId output
	    
			return
		end
	END 
	

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '428', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @transportedPeopleWithTowing = value_int FROM @values
	
	PRINT '-- START s_1009_042 --'
	PRINT dbo.f_translate('programId:',default)
	PRINT @programId
	PRINT '@platformId:'
	PRINT @platformId


	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @country = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '291', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towingType = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1003', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @homePatrolAbroad = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '206', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @finalDestinantionInCountrySpecial = value_int FROM @values

	SELECT @caseAbroad = dbo.f_abroad_case(@groupProcessInstanceId)
	
	IF ISNULL(@homePatrolAbroad,0) = 1 or isnull(@finalDestinantionInCountrySpecial,0) = 1
	BEGIN
		SET @caseAbroad = 0
	END 
	
	IF @caseAbroad = 1
	BEGIN
		
		PRINT dbo.f_translate('Organizowanie usługi za granicą',default)
		
		-- Organizowanie usługi za granicą

		BEGIN
			SET @variant = 3
		END
--		EXEC p_process_jump
--			@previousProcessId = @previousProcessId,
--			@nextStepId = '1011.010',
--			@doNext = 0,
--			@parentId = NULL,
--			@rootId = @rootId,
--			@groupId = @rootId,
--			@variant = @variant,
--		    @err = @errId output,
--		    @message = @message output
--		    
		RETURN
		
	END 
	
	
	
	-- JEŻELI USŁUGI PŁATNE --
	IF ISNULL(@programId, '') like '%423%'
	BEGIN
		
		PRINT dbo.f_translate('SĄ USŁUGI PŁATNE',default)
	
		DECLARE @fixingOrTowing INT
		DECLARE @costMessage VARCHAR(255) = ''
		
		SET @variant = 2
		
		-- USTAWIENIE CZY HOLOWANIE (1) CZY NAPRAWA (0)
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @fixingOrTowing = value_int FROM @values
		
		-- POBRANIE DMC --
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,76', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @carDMC = value_int FROM @values
		
		PRINT dbo.f_translate('DMC:',default)
		PRINT @carDMC
		
		-- USTAWIENIE KOMUNIKATU O KOSZTACH -- 
		
		IF @fixingOrTowing = 1
		BEGIN
			-- HOLOWANIE --
			PRINT '-- HOLOWANIE --'
			
			IF @carDMC < 2200
			BEGIN
				-- DMC < 2200
				SET @costMessage = 'Koszt holowania do 25km wynosi 240zł (każdy kolejny km – 5zł). '
			END 
			ELSE IF @carDMC >= 2200 AND @carDMC < 3400
			BEGIN
				-- DMC >= 2200 && DMC < 3400
				SET @costMessage = 'Koszt holowania do 25km wynosi 280zł (każdy kolejny km – 7zł). '
			END 
			ELSE IF @carDMC >= 3400 AND @carDMC <= 3500
			BEGIN
				-- DMC >= 3400 && DMC <= 3500
				SET @costMessage = 'Koszt holowania do 25km wynosi 380zł (każdy kolejny km – 8,80zł). '
			END 

			SET @content = @costMessage
			SET @content = @content + 'Transport osób dodatkowym pojazdem (dojazd, transport, powrót) to koszt 3zł/km. Koszt użycia ciężkiego sprzętu to 180zł. Wszystkie podane ceny to kwoty brutto do zapłaty na miejscu gotówką.'
			
		END 
		ELSE
		BEGIN
			-- NAPRAWA NA DRODZE --
			PRINT '-- NAPRAWA NA DRODZE --'
			
			IF @carDMC < 2200
			BEGIN
				-- DMC < 2200
				SET @costMessage = '240zł do 25km (każdy kolejny km – 5zł). '
			END 
			ELSE IF @carDMC >= 2200 AND @carDMC < 3400
			BEGIN
				-- DMC >= 2200 && DMC < 3400
				SET @costMessage = '280zł do 25km (każdy kolejny km – 7zł). '
			END 
			ELSE IF @carDMC >= 3400 AND @carDMC <= 3500
			BEGIN
				-- DMC >= 3400 && DMC <= 3500
				SET @costMessage = '380zł do 25km (każdy kolejny km – 8,80zł). '
			END 
			
			SET @content = dbo.f_translate('Koszt naprawy na drodze wynosi 240zł, jeżeli będzie to uruchomienie zewnętrznego źródła zasilania jest to 160zł. Jeżeli pojazdu nie uda się usprawnić na miejscu możemy zorganizować holowanie w cenie ',default)
			SET @content = @content + @costMessage
			SET @content = @content + 'Jeżeli nie uda się usprawnić pojazdu koszt dojazdu pomocy drogowej na miejsce wynosi 50zł. Wszystkie ceny to kwoty brutto, płatne na miejscu gotówką. Kwota 240 zł nie uwzględnia kosztów zakupu dostarczonych materiałów eksploatacyjnych (paliwo, płyny, oleje, żarówki). Koszty te dodatkowo obciążają użytkownika pojazdu.'
			
		END 
		
		PRINT dbo.f_translate('Komunikat:',default)
		PRINT @content
		
		-- ZAPISANIE KOMUNIKATU O KOSZTACH -- 
		
		EXEC p_attribute_edit
		@attributePath = '694', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueText = @content,
		@err = @errId OUTPUT,
		@message = @message OUTPUT
		
		
	END 
	ELSE
	BEGIN
		
		DECLARE @helpdeskCase INT
		DECLARE @specialSituation INT
		DECLARE @processPath INT
		
		EXEC dbo.p_is_helpdesk_case @groupProcessInstanceId = @groupProcessInstanceId, @isHelpDesk = @helpdeskCase OUTPUT
		
		IF @helpdeskCase = 1 AND @variant <> 3
		BEGIN
			SET @variant = 5
		END
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '415', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @specialSituation = value_int FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '115', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @processPath = value_int FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '305', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @transshipmentId = value_int FROM @values
		
		IF (@specialSituation = 5 OR ISNULL(@processPath,1) = 2) OR (@towingType = 1 or @towingType = 2) OR @transshipmentId IS NOT NULL
		BEGIN
			SET @variant = 4
		END 
		ELSE
		BEGIN
			-- Skok do panelu świadczeń
			EXEC [dbo].[p_jump_to_service_panel] @previousProcessId = @previousProcessId, @variant = @variant	
		END 
		
		
--		
--		EXEC p_process_jump
--			@previousProcessId = @previousProcessId,
--			@nextStepId = '1011.010',
--			@doNext = 0,
--			@parentId = NULL,
--			@rootId = @rootId,
--			@groupId = @rootId,
--			@variant = @variant,
--		    @err = @errId output,
--		    @message = @message output
		    
	END 
	
	--IF @transportedPeopleWithTowing > 2
	--BEGIN
	--	DECLARE @newProcessInstanceId INT
		
	--	EXEC [dbo].[p_process_new] 
	--	@stepId = '1016.021', 
	--	@userId = 1, 
	--	@originalUserId = 1, 
	--	@previousProcessId = NULL, 		
	--	@parentProcessId = @rootId, 
	--	@rootId = @rootId,
	--	@groupProcessId = NULL,
	--	@err = @err OUTPUT, 
	--	@message = @message OUTPUT, 
	--	@processInstanceId = @newProcessInstanceId OUTPUT
		
	--	EXEC [dbo].[p_attribute_edit]
	--    @attributePath = '856', 
	--    @groupProcessInstanceId = @newProcessInstanceId,
	--    @stepId = 'xxx',
	--    @userId = 1,
	--    @originalUserId = 1,
	--    @valueInt = @groupProcessInstanceId,
	--    @err = @err OUTPUT,
	--    @message = @message OUTPUT
		
	--END 
	
	PRINT '-- END s_1009_042 --'
	
/*
	IF @previousStepId = '1009.031' AND @peopleTransported > 2
	BEGIN
		SET @variant = 2
	END 
	ELSE
	BEGIN
		DECLARE @err INT
		DECLARE @message NVARCHAR(255)
		
		EXEC p_process_jump
		@previousProcessId = @previousProcessId,
		@nextStepId = '1011.010',
		@doNext = 0,
		@parentId = NULL,
		@rootId = @rootId,
		@groupId = @rootId,
		@variant = 1,
	    @err = @err output,
	    @message = @message output
	    
	    -- TODO: równolegle wystawić zadanie na back
	END 
	*/
END