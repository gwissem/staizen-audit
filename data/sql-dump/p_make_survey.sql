ALTER PROCEDURE [dbo].[p_make_survey]
    @processInstanceId INT,
    @type              int
AS
  BEGIN

    DECLARE @rootId int
    DECLARE @phoneNumber nvarchar(20)
    SELECT @rootId = root_id from process_instance with (nolock) where id = @processInstanceId
    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))



    DECLARE @version int = 1




    --     TYLKO W POLSCE

    DECLARE @country nvarchar(255)

    DELETE FROM @values


    INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @rootId
    SELECT @country = value_string FROM @values

    DELETE from @values

    IF isnull(@country, dbo.f_translate('Polska',default)) <> dbo.f_translate('Polska',default)
      BEGIN
        return;
      end



    DECLARE @platformId int

    DELETE FROM @values


    INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
    SELECT @platformId = value_int FROM @values


    --     Tylko FORD
    IF isnull(@platformId, 0) != 2
      BEGIN
        return;
      end


    DECLARE  @surveysStartDate DATETIME = CONVERT( DATETIME, dbo.f_translate('19 JUL 2019',default), 106 )

    DECLARE @caseStartDate DATETIME = (select top 1 created_at from process_instance where  root_id = @rootId order by  id asc )



    IF @caseStartDate< @surveysStartDate
      BEGIN
        return ;

      end
-- Wersja webowa od 21 Sierpnia
      DECLARE  @surveysVersionChangeDATE DATETIME = CONVERT( DATETIME, dbo.f_translate('21 AUG 2019',default), 106 )
      if (@caseStartDate> @surveysVersionChangeDATE )
        BEGIN
          SET @version = 2
        END
      BEGIN
        SET @version = 1
      end

    SET @version = 2
    
    DECLARE @caseHasBeenClosed int

    SELECT top 1 @caseHasBeenClosed = 1 from process_instance where root_id = @rootId and (step_id = '1011.023' or step_id = '1011.056')

    IF isnull(@caseHasBeenClosed,0) = 1
      BEGIN
        return
      end


    --     Checking if not exists already

    IF (SELECT top 1 rootId from survey where rootId = @rootId and type = @type) is not null
      BEGIN
        return;
      end



    /* Pobieranie nr  */
    DELETE from @values
    INSERT @values exec p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @rootId
    SELECT @phoneNumber = value_string from @values

    if isnull(@phoneNumber,'') = ''
      BEGIN
        DELETE from @values
        INSERT @values exec p_attribute_get2 @attributePath = '81,342,408,197', @groupProcessInstanceId = @rootId
        SELECT @phoneNumber = value_string from @values
      end

    if isnull(@phoneNumber,'') = ''
      BEGIN
        DELETE FROM @values
        INSERT @values exec p_attribute_get2  @attributePath = '197', @groupProcessInstanceId = @rootId
        select @phoneNumber  = value_string from @values
      end

    DELETE from @values

    DECLARE @smsContent nvarchar(2000)


    DECLARE @sendDelay int



    IF isnull(@version, 1) = 1
      BEGIN

        IF ISNULL(@type, 0) = 1 /** - zlecenie z poradą (brak innych świadczeń) **/
          BEGIN

            SET @smsContent = 'Jak oceniasz otrzymaną pomoc? Wyślij odpowiedź: 1, 2, 3, 4 lub 5, gdzie: 1 oznacza – zupełnie niezadowolony/a, a 5 - bardzo zadowolony/a.'
            SET @sendDelay = 0
          end
        else If ISNULL(@type, 0) = 2 /** zlecenie z udaną naprawą na drodze **/
          BEGIN
            SET @smsContent = 'Jak oceniasz pomoc na drodze? Wyślij odpowiedź: 1, 2, 3, 4 lub 5, gdzie: 1 oznacza – zupełnie niezadowolony/a, a 5 - bardzo zadowolony/a.'
            SET @sendDelay = 4

          end
        else if ISNULL(@type, 0) = 3 /** zlecenie z holowaniem (również po nieudanej naprawie)**/
          begin

            SET @smsContent = 'Jak oceniasz usługę holowania? Wyślij odpowiedź: 1, 2, 3, 4 lub 5, gdzie: 1 oznacza – zupełnie niezadowolony/a, a 5 - bardzo zadowolony/a.'
            SET @sendDelay = 4

          end
        else if ISNULL(@type, 0) = 4 /** zlecenie z pojazdem zastępczym (w zleceniu mogą, ale nie muszą być inne usługi)**/
          begin

            SET @smsContent = 'Jak oceniasz usługę wynajmu pojazdu zastępczego? Wyślij odpowiedź: 1, 2, 3, 4 lub 5, gdzie: 1 oznacza – zupełnie niezadowolony/a, a 5 - bardzo zadowolony/a.'
            SET @sendDelay = 12
          end
        else if ISNULL(@type, 0) = 5 /** dodatkowy proces**/
          begin

            SET @smsContent = 'Jak ogólnie oceniasz otrzymaną pomoc assistance? Wyślij odpowiedź: 1, 2, 3, 4 lub 5, gdzie: 1 oznacza – zupełnie niezadowolony/a, a 5 - bardzo zadowolony/a.'
            SET @sendDelay = 24
          end
      END
      ELSE IF isnull(@version,1) = 2
        BEGIN
          DECLARE @newToken VARCHAR(50)

          set @newToken = left( newid(),8)
          WHILE (SELECT id from survey where uid = @newToken) is not null
            BEGIN
              set @newToken =left( newid(),8)
            end


          INSERT INTO AtlasDB_v.dbo.survey
                  (
                      rootId, phone_number, state,  active, type, version, uid
                      )
          VALUES
                 (
                     @rootId,
                     @phoneNumber,
                     2,
                     1,
                     @type,
                     @version,
                     @newToken
                );


          DECLARE @link nvarchar(200)
          SELECT @link = dbo.f_getDomain() + dbo.f_translate('/surveys/',default) + @newToken



          IF ISNULL(@type, 0) = 1 /** - zlecenie z poradą (brak innych świadczeń) **/
            BEGIN

              SET @smsContent = ' Jak oceniasz otrzymaną pomoc? Kliknij w link i podziel się swoją opinią. .' + isnull(@link,'')
              SET @sendDelay = 0
            end
          else If ISNULL(@type, 0) = 2 /** zlecenie z udaną naprawą na drodze **/
            BEGIN
              SET @smsContent = 'Jak oceniasz pomoc na drodze? Kliknij w link i podziel się swoją opinią. ' + isnull(@link,'')
              SET @sendDelay = 4

            end
          else if ISNULL(@type, 0) = 3 /** zlecenie z holowaniem (również po nieudanej naprawie)**/
            begin

              SET @smsContent = 'Jak oceniasz usługę wynajmu pojazdu zastępczego? Kliknij w link i podziel się swoją opinią. ' + isnull(@link,'')
              SET @sendDelay = 4

            end
          else if ISNULL(@type, 0) = 4 /** zlecenie z pojazdem zastępczym (w zleceniu mogą, ale nie muszą być inne usługi)**/
            begin

              SET @smsContent = 'Jak oceniasz usługę wynajmu pojazdu zastępczego? Kliknij w link i podziel się swoją opinią. '+ isnull(@link,'')
              SET @sendDelay = 4
            end
          else if ISNULL(@type, 0) = 5 /** dodatkowy proces**/
            begin

              SET @smsContent = 'Jak ogólnie oceniasz otrzymaną pomoc assistance? Kliknij w link i podziel się swoją opinią. ' + isnull(@link,'')
              SET @sendDelay = 24
            end

        end



      DECLARE @err int
    DECLARE @message nvarchar(max)


    DECLARE @sendDate datetime = GETDATE();
    DECLARE @sendInFuture int = 0

    /*
    Przy niektórych opcjach musi dojść opóźnienie z dokumentacji
     */
    IF isnull(@sendDelay,0) is not null AND isnull(@sendDelay,0) > 0
      BEGIN
        SET @sendDate = DATEADD(hour, @sendDelay,@sendDate)
      end

    /*
    Nie wysyłamy w nocy - przesunięte wtedy na następny dzień rano
     */

    IF isnull(@sendDelay,1) >0
      BEGIN
        SET @sendInFuture = 1
      end

    IF NOT (CAST(@sendDate as time) between '07:00:00' and '21:00:00')
      BEGIN
        SELECT @sendDate =  DATEADD(hh, 7, DATEADD(dd, DATEDIFF(dd, 0, @sendDate), 0)) --ustawia na 7
        if @sendDate< GETDATE()
          BEGIN
            SET @sendDate = DATEADD(day,1,@sendDate) -- ustawia na następny dzień
          end
        SET @sendInFuture = 1
      end

    /*
      Scheduled
     */
    IF @sendInFuture = 1
      BEGIN

        DECLARE @parameters TABLE(name NVARCHAR(50), type NVARCHAR(20), value NVARCHAR(255))
        DECLARE @jsonText NVARCHAR(MAX)

        INSERT INTO @parameters SELECT dbo.f_translate('groupProcessId',default), dbo.f_translate('INT',default), @rootId
        INSERT INTO @parameters SELECT dbo.f_translate('content',default), 'NVARCHAR(2000)', @smsContent
        INSERT INTO @parameters SELECT dbo.f_translate('phoneNumber',default), 'NVARCHAR(20)', @phoneNumber
        SELECT @jsonText = (SELECT * FROM @parameters FOR JSON AUTO)


        EXEC add_scheduled_job
            @dateExecute = @sendDate,
            @procedureName = 'p_simple_note_new'
            , @parameters = @jsonText

      end
    ELSE
      BEGIN

        EXEC p_note_new
            @groupProcessId = @rootId,
            @type = dbo.f_translate('sms',default),
            @content = @smsContent,
            @phoneNumber = @phoneNumber,
            @direction = 1,
            @err = @err OUTPUT,
            @message = @message OUTPUT

      end


    IF isnull(@version,0) = 1
      BEGIN
        INSERT INTO AtlasDB_v.dbo.survey (rootId, phone_number, state,  active, type, version)
        VALUES (@rootId, @phoneNumber, 1,  1, @type, @version);
      end

  end