ALTER PROCEDURE [dbo].[p_send_rs]
  (
    @groupProcessInstanceId INT,
    @currentUser            INT = 1,
    @sendCopy               SMALLINT = 0
  )
AS
  BEGIN


    ------------------------------------------------------------------------------------
    ---------- 						PROCEDURA DO WYSYŁANIA RS'a				  ----------

    -- 		+ groupProcessInstanceId 	- tutaj musi być GRUPA MONITORINGU WARSZTATOWEGO
    -- 		+ @sendCopy 				- czy wysłać kopię raportu serwisowego

    ------------------------------------------------------------------------------------


    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))

    DECLARE @err TINYINT
    DECLARE @message NVARCHAR(255)

    DECLARE @stepId NVARCHAR(50)
    DECLARE @rootId INT
    DECLARE @token NVARCHAR(36)
    DECLARE @partnerId INT
    DECLARE @partnerEmail NVARCHAR(200)
    DECLARE @step_1021_003_id INT

    DECLARE @subject NVARCHAR(100)
    DECLARE @body nvarchar(4000)
    DECLARE @RSV varchar(200)

    DECLARE @platform NVARCHAR(100)
    DECLARE @platformId int
    DECLARE @caseId NVARCHAR(50)
    DECLARE @makeModel NVARCHAR(100)
    DECLARE @vin NVARCHAR(50)
    DECLARE @regNumber NVARCHAR(20)


    SELECT @stepId = step_id, @rootId = root_id FROM dbo.process_instance WHERE id = @groupProcessInstanceId

    -- Pobranie TOKENA
    SELECT TOP 1 @token = token
    FROM dbo.process_instance
    WHERE group_process_id = @groupProcessInstanceId
      AND token IS NOT NULL
    ORDER BY id

    -- Pobranie ID kroku, z którego jest generowany PDF (1021.003)
    SELECT TOP 1 @step_1021_003_id = id
    FROM dbo.process_instance
    WHERE group_process_id = @groupProcessInstanceId
      AND step_id = '1021.003'
    ORDER BY id DESC

    -- pobranie ID partnera i pobranie adresu e-mail
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerId = value_int FROM @values

    -- Pobranie Emaila do kontraktora (3 próby)
    -- 8 - Email RS
    -- 4 - Realizacja - email
    -- 16 - Email
    SELECT @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 1, 8)

    IF ISNULL(@partnerEmail, '') = ''
      BEGIN

        SET @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 1, 4)

        IF ISNULL(@partnerEmail, '') = ''
          BEGIN
            SET @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 1, 16)
          END

      END

    -- Pobranie danych do e-maila

    SET @caseId = dbo.f_caseId(@rootId)

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platform = name from dbo.platform where id = (select top 1 value_int FROM @values)
    SELECT @platformId = value_int from @values
    IF @platformId <> 5
      BEGIN

        DELETE FROM @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @makeModel = textD
        from dbo.dictionary
        where value = (select top 1 value_int FROM @values)
          and typeD = 'makeModel'

        DELETE from @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @VIN = value_string FROM @values

        DELETE from @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @regNumber = value_string FROM @values

        DECLARE @url NVARCHAR(300) = [dbo].[f_hashTaskUrl](@token)

        SET @subject =
        dbo.f_translate('Formularz raportu serwisowego ',default) + isnull(@platform, '') + dbo.f_translate(' do sprawy ',default) + isnull(@caseId, '') + ' (' +
        isnull(@makeModel, '') + ' / ' + isnull(@vin, '') + ' / ' + isnull(@regNumber, '') + ')'
        SET @body = 'WAŻNY KOMUNIKAT<BR>
	Prosimy o wejście w poniższy link i uzupełnienie: <BR>
	• Szacowanej daty zakończenia naprawy – gdy naprawa jest w trakcie<BR>
	• Daty zakończenia oraz sposobu rozliczenia – gdy znany jest termin zakończenia naprawy<BR><BR>
	<BR>
	Aby otworzyć link kliknij <A href="' + @url + '">tutaj</A><BR>
	<BR>
	Po przesłaniu raportu otrzymają Państwo na adres email kopię raportu. <BR>
	<BR>
	Jeżeli jedyną zorganizowaną usługą jest naprawa na miejscu zdarzenia, nie muszą Państwo uzupełniać raportu serwisowego. Przesłany przez nas dokument stanowi formę powiadomienia o udzielonym świadczeniu. We wszystkich innych przypadkach prosimy o uzupełnienie raportu (maksimum 2 dni po zakończeniu naprawy serwisowej).
	W razie pytań, prosimy o kontakt telefoniczny pod numerem telefonu 61 83 19 894.<BR>
	<BR>
	Uwaga: W przypadku pojazdów zagranicznych (zaopatrzonych w zagraniczne numery rejestracyjne lub z uprawnieniami assistance nadanymi za granicą), jeśli nie będzie możliwości rozliczenia części bądź całości usług z producentem prosimy jak najszybszy kontakt telefoniczny (pod numerem telefonu 61 83 19 894) w tej sprawie. W takich przypadkach kosztami usług assistance należy obciążyć klienta.<BR><BR>
	Gdybyście Państwo chcieli złożyć wniosek Dealer’s Call prosimy o użycie poniższego linku: <BR>
	Kliknij <A href="' + dbo.f_getDomain() + '/operational-dashboard/public/raport-serwisowy">tutaj</A><BR><BR><BR>'

        SET @body = @body + 'Z poważaniem,<BR>
	Starter24 Sp. z o.o.<BR>
	Centrum Zgłoszeniowe Audi / VW / Seat / Skoda Assistance<BR>
	Tel.:  +48 61 83 19 894<BR>
	Faks: +48 61 83 19 850<BR>
	Mail: rs@starter24.pl<BR>
	ul. Józefa Kraszewskiego 30, 60-519 Poznań<BR>
	<font style='dbo.f_translate('font-size:8px;',default)'>Treść tego dokumentu jest poufna i prawnie chroniona. Odbiorcą może być jedynie jej adresat z wyłączeniem dostępu osób trzecich. Jeżeli nie jest Pani/Pan adresatem niniejszej wiadomości, jej rozpowszechnianie, kopiowanie lub inne działanie o podobnym charakterze jest prawnie zabronione. W razie otrzymania tej wiadomości jako niezamierzony odbiorca, proszę o poinformowanie o tym fakcie nadawcy, a następnie usunięcie wiadomości ze swojego systemu.<BR> 
	STARTER Sp. z o.o., ul. Józefa Kraszewskiego 30, 60-519 Poznań, wpisana przez Sąd Rejonowy w Poznaniu, VIII Wydział Gospodarczy Krajowego Rejestru Sądowego, KRS: 0000056095; kapitał zakładowy: 12 500 000,00 zł; NIP: 525-21-83-310; REGON: 016387736<BR>
	Niniejsza wiadomość oraz wszystkie załączone do niej pliki przeznaczone są do wyłącznego użytku zamierzonego adresata i mogą zawierać chronione lub poufne informacje. Przeglądanie, wykorzystywanie, ujawnianie lub dystrybuowanie przez osoby do tego nieupoważnione jest zabronione. Jeśli nie jest Pan/Pani wymienionym adresatem niniejszej wiadomości, prosimy o niezwłoczny kontakt z nadawcą i usunięcie oryginalnej wiadomości oraz zniszczenie wszystkich jej kopii.<BR>
	The information in this email is confidential and may be legally privileged. It is intended solely for the addressee. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited and may be unlawful. If you received this email as the unintended recipient, please inform the sender and delete this message.</font><BR>'


        SET @RSV = '{FORM::' + cast(@step_1021_003_id as varchar(100)) + '::raport serwisowy}'

        DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rs')

        EXECUTE dbo.p_note_new
            @sender = @senderEmail
            , @groupProcessId = @groupProcessInstanceId
            , @type = dbo.f_translate('email',default)
            , @content = dbo.f_translate('Formularz raportu serwisowego',default)
            , @phoneNumber = null
            , @email = @partnerEmail
            , @userId = @currentUser
            , @subType = null
            , @attachment = null
            , @subject = @subject
            , @direction = 1
            , @addInfo = null
            , @emailBody = @body
            , @err = @err OUTPUT
            , @message = @message OUTPUT
            , @additionalAttachments = @RSV


        -- WYSŁANIE KOPII

        IF @sendCopy = 1
          BEGIN

            EXECUTE dbo.p_note_new
                @sender = @senderEmail
                , @groupProcessId = @groupProcessInstanceId
                , @type = dbo.f_translate('email',default)
                , @content = dbo.f_translate('Kopia raportu serwisowego',default)
                , @phoneNumber = null
                , @email = 'skrzynkapodawcza@starter24.pl'
                , @userId = @currentUser
                , @subType = null
                , @attachment = null
                , @subject = @subject
                , @direction = 1
                , @addInfo = null
                , @emailBody = dbo.f_translate('Kopia raportu serwisowego',default)
                , @err = @err OUTPUT
                , @message = @message OUTPUT
                , @additionalAttachments = @RSV

          END
      END
  END
