ALTER PROCEDURE [dbo].[p_check_available_services_cfm]
	@processInstanceId INT,
	@rootId INT = NULL,
	@stepId NVARCHAR = NULL,
	@servicesIds NVARCHAR(255) = '',
	@arcCode NVARCHAR(255) = '',
	@fixingEndDate DATETIME = null, 
	@eventType INT = null,
	@locale NVARCHAR(10) = 'pl',
	@init INT = 0,
	@programId INT = NULL
	AS
	BEGIN
		
		-- ======================================================= --
		-- 	PROCEDURA SPRAWDZAJĄCA WARUNKI DLA USŁUG W PLATFORMACH CFM
		-- 
		-- 	!!!	REKOMENDOWANE JEST, ŻEBY WCZEŚNIEJ STWORZYĆ TABELKĘ TYMCZASOWĄ #availableServices, 
		--		KTÓRA ZDEFINIOWANA JEST W PROCEDURZE p_available_services,
		--		w innym przypadku ustawić parametr @init na 1
		  
		-- ======================================================= --
		
		PRINT 'EXEC p_check_available_services_cfm'


		DECLARE @currentPrograms_String NVARCHAR(255)
		DECLARE @currentPrograms_Table TABLE(data NVARCHAR(MAX), id int)		
		DECLARE @cancel INT
		DECLARE @err INT
		DECLARE @programName NVARCHAR(255)
		DECLARE @groupProcessInstanceId INT
		DECLARE @servicePartnerId INT
		DECLARE @platformName NVARCHAR(255)
		DECLARE @platformId INT	
		DECLARE @message NVARCHAR(255)
		DECLARE @allow int 
		DECLARE @serviceLimit int
		DECLARE @abroadCase int 
		DECLARE @damageMonitError INT
		DECLARE @errorMessage NVARCHAR(500)
		
		SELECT @abroadCase = dbo.f_abroad_case(@rootId)
		
		DECLARE @vinHeadersResult Table (description NVARCHAR(500), value NVARCHAR(500), header_id INT, orderBy INT)	
		
		DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
		
		SELECT @rootId = root_id, @stepId = step_id, @groupProcessInstanceId = group_process_id FROM dbo.process_instance WITH(nolock) WHERE id = @processInstanceId
		
		IF @init = 1
		BEGIN
			CREATE TABLE #availableServices (id INT, name NVARCHAR(255), active TINYINT, message NVARCHAR(MAX), description NVARCHAR(MAX), icon NVARCHAR(MAX), variant INT, start_step_id NVARCHAR(255), pos INT, programId INT)
			
			INSERT INTO #availableServices (id, name, active, message, description, icon, variant, start_step_id, pos)
			SELECT sd.id, sdt.name, 1, dbo.f_translate('Obsługa świadczenia nie jest jeszcze dostępna w systemie',default), sdp.description, sd.icon, NULL, sd.start_step_id, sdp.[position]
			FROM AtlasDB_def.dbo.service_definition_program sdp with(nolock)
			INNER JOIN service_definition sd with(nolock) ON sd.id = sdp.service_definition_id
			INNER JOIN service_definition_translation sdt with(nolock) ON sd.id = sdt.translatable_id
			AND sdp.program_id    = @programId
			WHERE sd.active = 1
			AND sdt.locale = @locale
			ORDER BY sdp.position
	
			SET @damageMonitError = 0
		END 
		ELSE
		BEGIN
			
			SELECT @damageMonitError = CASE WHEN active = 0 THEN 1 ELSE 0 END FROM #tempAvailableServices where id = 22
			
		END 
		
		/*	MONITORING SZKODY
  		____________________________________*/

		/*	OD 01.09.2019 USLUGA ZOSTAJE WYŁĄCZONA
 		____________________________________*/

		UPDATE #tempAvailableServices SET
		active = 0,
		message = dbo.f_translate('Wybrana usługa jest niedostępna',default)
		where id = 22
			
--		DECLARE @canMonit INT
--		
--		SET @errorMessage = dbo.f_translate('Wybrana usługa nie jest dostępna',default)
--		
--		EXEC [dbo].[p_can_run_damage_monitoring]
--		@rootId = @rootId,
--		@canMonit = @canMonit OUTPUT,
--		@errorMessage = @errorMessage OUTPUT
--		
--		IF @canMonit = 0
--		BEGIN
--		
--			PRINT dbo.f_translate('DISABLE MONITORING SZKODY',default)
--			
--			UPDATE #tempAvailableServices SET
--			active = 0,
--			message = dbo.f_translate('Wybrana usługa nie jest dostępna',default)
--			where id = 22
--				
--		END
		
		IF @programId IS NULL
		BEGIN			
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @programId = ISNULL(value_string,'') FROM @values			
		END 
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @platformId = value_int FROM @values
		
		SELECT @platformName = name FROM platform with(nolock) where id = @platformId
		
		--------------------------------
		---- AUTO ZASTĘPCZE
		--------------------------------
		
		--- brak auta zastępczego dla wariantu 2
		IF @programId IN (509,517)
		BEGIN
			SET @errorMessage = dbo.f_translate('Świadczenie jest niedostępne dla programu Alphabet Wariant 2',default)
			
			UPDATE #tempAvailableServices SET
			active = 0,
			message = @errorMessage
			where id = 3
		END 
		
		IF (@platformId = 53 AND @arcCode IN ('2461461','1023630')) 
		OR (@platformId = 48 AND @arcCode IN ('2461461','1023630','PP02K2XX','P02Z2XX','1984391'))
		OR (@platformId = 43 AND @arcCode IN ('1023630'))
		OR (@platformId = 25 AND @arcCode IN ('1023630','PP02K2XX','P02Z2XX','1984391'))
		BEGIN
			SET @errorMessage = dbo.f_translate('Świadczenie pojazdu zastępczego nie jest dostępne dla zdarzenia ',default)+dbo.f_diagnosis_description(@groupProcessInstanceId,'pl')
			
			UPDATE #tempAvailableServices SET
			active = 0,
			message = @errorMessage
			where id = 3
		END 
		
		
		IF @platformId = 25 AND (@arcCode like '300%' or @arcCode like '309%' or @arcCode like '35522%')
		BEGIN
			declare @fleet nvarchar(100)
			exec dbo.p_get_vin_headers_by_rootId @root_id=@groupProcessInstanceId, @columnName=dbo.f_translate('wlasciciel',default), @output=@fleet OUTPUT
			if @fleet like '%APTEKA GEMINI%'
			begin
				SET @errorMessage = 'Specjalna procedura obsługi dla pojazdów Dostawczych z firmy Apteka Gemini – dla Awarii polegającej na problemach z układem chłodzenia, ZAWSZE HOLUJEMY.'
			
				UPDATE #tempAvailableServices SET
				active = 0,
				message = @errorMessage
				where id = 2

				UPDATE #tempAvailableServices SET
				active = 1,
				message = ''
				where id = 1


			end
		end
		
		IF @platformId = 78 
		BEGIN
			DECLARE @followUpGroupId INT
			DECLARE @externalOrganisation nvarchar(1000)
			DECLARE @serviceId int 
			DECLARE @primaryService int 
			
			SELECT top 1 @followUpGroupId = group_process_id from dbo.process_instance where step_id = '1006.006' and root_id = @rootId order by id desc
			
			IF (@arcCode like '25914%' or @arcCode like '24614%'or @arcCode like '27214%')
			BEGIN
				UPDATE #tempAvailableServices 
				SET
				active = 0,
				message = 'Dla tego typu zdarzenia (opony) nie przysługuje świadczenie pojazdu zastępczego'
				where id = 3
			END 
			
			IF @followUpGroupId is not NULL
			BEGIN
				
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '545,464', @groupProcessInstanceId = @followUpGroupId
				SELECT @primaryService = value_int FROM @values
				
				IF @primaryService IN (1,2)
				BEGIN
					UPDATE #tempAvailableServices 
					SET
					active = 0,
					message = dbo.f_translate('Skorzystano już z organizacji usługi podstawowej w ramach pakietu producenta',default)
					where id IN (1,2)
				END 
				
				exec [dbo].[p_service_day_limit] 
				@groupProcessInstanceId = @groupProcessInstanceId, 
				@serviceId = 3,
				@max = 1,
				@days = @serviceLimit OUTPUT
				
				IF @serviceLimit <= 0 
				BEGIN
					UPDATE #tempAvailableServices 
					SET
					active = 0,
					message = 'Dla tego typu zdarzenia liimt dni auta zastępczego został wyczerpany (lub był niedostępny jeśli nie organizowano pojazdu zastępczego)'
					where id = 3
				END
				
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '545,546', @groupProcessInstanceId = @followUpGroupId
				SELECT @externalOrganisation = value_string FROM @values
				
			    
			    declare kur cursor LOCAL for
			    	SELECT data from dbo.f_split(@externalOrganisation,',')			
			    OPEN kur;
			    FETCH NEXT FROM kur INTO @serviceId;
			    WHILE @@FETCH_STATUS=0
			    BEGIN
			    	
			    	UPDATE #tempAvailableServices SET
					active = 0,
					message = dbo.f_translate('Skorzystano już z organizacji tej usługi w ramach pakietu producenta',default)
					where id = @serviceId
			    	
			    FETCH NEXT FROM kur INTO @serviceId;
			    END
			    CLOSE kur
			    DEALLOCATE kur
			    
			END 
		
			
		END





		--		SELECT @canMonit, @errorMessage
			
		----------------------------------
		--- MONITORING SZKODY
		
		-- biorą udział wszystkie pojazdy posiadające w bazie, w kolumnie S – Monitoring_szkod oznaczenie „tak”.
		-- Proces dotyczy wszystkich szkód tj. wypadków, szkód jezdnych, kradzieży itd. – wyłączone z procesu są Awarie i Błędy Pilotażu
		
		----------------------------------
--		IF @eventType IN (2,7) -- Awarie i Błędy Pilotażu
--		BEGIN			
--			
--			
--			PRINT dbo.f_translate('DISABLE MONITORING SZKODY',default)
--			
--			UPDATE #tempAvailableServices SET
--			active = 0,
--			message = dbo.f_translate('Wybrana usługa jest dostępna tylko w przypadku szkód, tj. wypadków, szkód jezdnych, kradzieży itd.',default)
--			where id = 22
--			
--			SET @damageMonitError = 1
--			
--		END
--		
--		IF @damageMonitError = 0
--		BEGIN
--			
--			INSERT INTO @vinHeadersResult
--			EXEC [dbo].[p_get_vin_headers_by_rootId] @root_id = @rootId
--		
--			IF NOT EXISTS(SELECT value FROM @vinHeadersResult WHERE description = 'Monitoring_szkod' AND value = dbo.f_translate('tak',default))
--			BEGIN
--				
--				PRINT dbo.f_translate('DISABLE MONITORING SZKODY',default)
--			
--				UPDATE #tempAvailableServices SET
--				active = 0,
--				message = dbo.f_translate('Pojazd nie posiada w bazie możliwości monitoringu szkody.',default)
--				where id = 22
--				
--				SET @damageMonitError = 1
--				
--			END
--			
--		END
		
	END