ALTER PROCEDURE [dbo].[s_1063_012]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
DECLARE @err INT
DECLARE @message NVARCHAR(255)
DECLARE @lastServiceMileage INT
DECLARE @currentMileage INT
DECLARE @stepId NVARCHAR(255)
DECLARE @programId INT
DECLARE @platformId INT
DECLARE @programVal NVARCHAR(255)

DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
DECLARE @groupProcessInstanceId INT
SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id FROM process_instance where id = @previousProcessId

INSERT @values EXEC p_attribute_get2 @attributePath = '74,456', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @lastServiceMileage = value_int FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @currentMileage = value_int FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @platformId = value_int FROM @values

SET @variant = 2

IF @lastServiceMileage IS NOT NULL AND @currentMileage IS NOT NULL AND (@currentMileage - @lastServiceMileage) < 30000
BEGIN
	SET @variant = 1
	
--	SELECT @programId = id FROM vin_program WHERE platform_id = @platformId AND name like '%gwarancja mobilności%'
--	SET @programVal = CAST(@programId AS NVARCHAR(255))
--	
--	EXEC [dbo].[p_attribute_set2]
--	@attributePath = '202', 
--	@groupProcessInstanceId = @groupProcessInstanceId,
--	@stepId = @stepId,
--	@valueString = @programVal,
--	@err = @err OUTPUT,
--	@message = @message OUTPUT
END
ELSE IF @lastServiceMileage IS NOT NULL AND @currentMileage IS NOT NULL AND (@currentMileage - @lastServiceMileage) >= 30000
BEGIN
	-- >= 30 000
	EXEC p_attribute_set2
		@attributePath = '532', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueText = dbo.f_translate('Uprawnienie assistance obowiązuje do określonego limitu przebiegu, który został przekroczony. Wobec tego mogę zaoferować usługi odpłatne',default),
		@err = @err OUTPUT,
		@message = @err OUTPUT
END 
ELSE
BEGIN
	-- "Nie wiem"
	EXEC p_attribute_set2
		@attributePath = '532', 
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueText = dbo.f_translate('Uprawnienie assistance obowiązuje do określonego limitu przebiegu. Brak tej informacji uniemożliwia weryfikację. Jeśli ma Pan / Pani taką możliwość, proszę sprawdzić przebieg w książce serwisowej lub na fakturze z przeglądu i oddzwoni�',default),
		@err = @err OUTPUT,
		@message = @err OUTPUT
END

