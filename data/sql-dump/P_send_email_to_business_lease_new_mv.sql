ALTER PROCEDURE [dbo].[P_send_email_to_business_lease_new_mv]
	@previousProcessId INT,	-- Może być GROUP / ROOT
	@emailTo NVARCHAR(400),
	@extension TINYINT = 0
AS
BEGIN
	
	/*		
	 
	TYLKO Business Lease 
	
	Powiadomienie o wydaniu pojazdu zastępczego do BL    LUB   4. Powiadomienie o wydaniu pojazdu zastępczego do MotoFloty
	LUB
	Powiadomienie o przedłużeniu wynajmu samochodu zastępczego do Motofloty LUB	 Powiadomienie o przedłużeniu wynajmu samochodu zastępczego do BL
	
	(RÓŻNI SIĘ TYLKO E-mailem i @Extension)
	
	WYNAJEM:
	-- BL:
	-- Warunek wysłania: sprawa typu awaria lub wypadek/szkoda, zorganizowanie auta zastępczego, przypisany warsztat należy do grupy BL ASO
	-- Wysłane do: assistance@businesslease.pl, DW: cfm@starter24.pl
	
	-- MotoFlota
	-- a. Warunek wysłania: sprawa typu awaria lub wypadek/szkoda, zorganizowanie auta zastępczego, przypisany warsztat z grupy BL STandard
	-- b. Wysłane do: bl@motoflota.pl, DW: cfm@starter24.pl
	
	PRZEDLUŻENIE:
	
	-- Motofloty
	-- warunek: sprawa typu awaria, przypisany warsztat z grupy BL_Standard, każdorazowo po przedłużeniu wynajmu 
	
	-- BL
	-- warunek: sprawa typu awaria, przypisany serwis z grupy BL ASO, każdorazowo po przedłużeniu wynajmu powyżej 3 dni
 	____________________________________*/

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @title NVARCHAR(MAX)
	
	-- Pobranie podstawowych danych --
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	

	DECLARE @towingDestination NVARCHAR(400) = '{#towingPlace()#} {@767,781,84@} {@767,781,687@}'
	
	IF @extension = 0
	BEGIN
			
		DECLARE @towingGroupId INT = [dbo].[f_service_top_progress_group](@rootId, 1)
		
		IF @towingGroupId IS NOT NULL
		BEGIN
			EXEC [dbo].[P_parse_string]
			 @processInstanceId = @towingGroupId,
			 @simpleText = @towingDestination,
			 @parsedText = @towingDestination OUTPUT
		END
		
	END
	
	SET @content = '';

	IF @extension = 1
	BEGIN
		
		DECLARE @rentalDuration INT
		
		SELECT @rentalDuration = sum(isnull(av.value_int,0)) 
                from (select *, row_number()over(partition by group_process_id order by id desc) rowN FROM dbo.service_status with(nolock) ) ss
                inner join dbo.attribute_value av with(nolock) ON ss.group_process_id = av.group_process_instance_id
                inner join dbo.service_status_dictionary ssd with(nolock) ON ssd.id = ss.status_dictionary_id
                left join dbo.attribute_value av5 with(nolock) ON av5.group_process_instance_id = av.group_process_instance_id and av5.attribute_path = '168,159'
                where av.root_process_instance_id = @rootId
                and av.attribute_path = '789,786,240,153'
                and rowN = 1
                and ssd.progress NOT IN (5,6)
                and isnull(av5.value_int,0) = 0
		
		SET @content = 'Szanowni Państwo,</br></br>

			Informujemy o przedłużeniu wynajmu pojazdu zastępczego dla sprawy: {#caseid#}</br>
			Data rozpoczęcia wynajmu dla powyższej sprawy: {#showDatetime({@540@}|1)#}</br>
			Aktualna suma dni wynajmu dla powyższej sprawy: ' + CAST(ISNULL(@rentalDuration, '-') AS NVARCHAR(20)) + '</br></br>

			Telefon kontaktowy dla Państwa: +48 61 83 19 969</br>
			Infolinia dla Klienta: +48 22 46 000 46</br></br>
	
			W korespondencji z nami należy zawsze używać adresu mailowego cfm@starter24.pl (kontakt w sprawach Car Fleet Management), pamiętając by w temacie wiadomości zawrzeć nasz numer sprawy (widoczny w temacie niniejszego maila). W sprawach bardzo pilnych uprzejmie prosimy o dodatkowy kontakt telefoniczny pod numerem +48 61 83 19 969
		
		'
	END
	ELSE 
	BEGIN
		
		SET @content = 'Szanowni Państwo,</br></br>

			przesyłamy powiadomienie dotyczące wynajmu pojazdu zastępczego</br></br>
	
			Powiadomienie do sprawy {#caseid#}</br>
			Nr rejestracyjny auta: {@74,72@}</br>
			Marka i model pojazdu: {@74,73@}</br>
			Użytkownik: {@80,342,64@} {@80,342,66@}</br>
			Nr tel.: {@80,342,408,197@}</br>
			Pojazd kierowany jest do: ' + ISNULL(@towingDestination, '-') + '</br>
			Liczba dni wynajmu: {@789,786,240,153@}</br>
			Data rozpoczęcia wynajmu: {#showDatetime({@540@}|1)#}</br></br>
			
			Telefon kontaktowy dla Państwa: +48 61 83 19 969
			Infolinia dla Klienta: +48 22 46 000 46
	
			W korespondencji z nami należy zawsze używać adresu mailowego cfm@starter24.pl (kontakt w sprawach Car Fleet Management), pamiętając by w temacie wiadomości zawrzeć nasz numer sprawy (widoczny w temacie niniejszego maila). W sprawach bardzo pilnych uprzejmie prosimy o dodatkowy kontakt telefoniczny pod numerem +48 61 83 19 969
		'
			
	END
	
	IF @extension = 1
	BEGIN
		SET @title = 'Powiadomienie o przedłużeniu usługi wynajmu pojazdu zastępczego do sprawy {#caseid#}'
	END
	ELSE 
	BEGIN
		SET @title = 'Powiadomienie o usłudze wynajmu pojazdu zastępczego do sprawy {#caseid#}'
	END
	
	EXEC [dbo].[P_parse_string]
	 @processInstanceId = @previousProcessId,
	 @simpleText = @title,
	 @parsedText = @title OUTPUT

	EXEC [dbo].[P_get_body_email]
		@body = @body OUTPUT,
		@contentEmail  = @content,
		@title = @title,
		@previousProcessId = @previousProcessId	
		
	SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'CFM')
	SET @body = REPLACE(@body,'__EMAIL__', 'cfm@starter24.pl')
	 
	 DECLARE @email VARCHAR(400)
	 DECLARE @dw NVARCHAR(400)
	 DECLARE @sender NVARCHAR(400)
	 DECLARE @contentNote NVARCHAR(1000) = [dbo].[f_showEmailInfo](@emailTo) + dbo.f_translate('Powiadomienie o wydaniu pojazdu zastępczego',default)
	 
	 SET @email = dbo.f_getRealEmailOrTest(@emailTo)
	 SET @sender = dbo.f_getEmail('cfm')
	 SET @dw = dbo.f_getRealEmailOrTest('cfm@starter24.pl')
	 
	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @groupProcessInstanceId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = @contentNote
	 	,@email = @email
	 	,@dw = @dw
	 	,@userId = 1  -- automat
	 	,@subject = @title
	 	,@direction=1
	 	,@sender = @sender
	 	,@emailBody = @body
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT
	
	
  EXEC p_attribute_edit
      @attributePath = '1043',
      @groupProcessInstanceId = @groupProcessInstanceId,
      @stepId = 'xxx',
      @valueInt = 33,
      @err = @err OUTPUT,
      @message = @message OUTPUT
END