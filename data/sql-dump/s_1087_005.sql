ALTER PROCEDURE [dbo].[s_1087_005]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @userId INT
	DECLARE @originalUserId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)	
	DECLARE @groupProcessInstanceId INT
	DECLARE @callerPhone INT
	DECLARE @content NVARCHAR(4000)
	DECLARE @email NVARCHAR(255)
	DECLARE @subject NVARCHAR(255)
	DECLARE @rootId int


	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId =root_id
	FROM process_instance with(nolock)
	WHERE id = @previousProcessId 


	DECLARE @isHA int

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1062', @rootProcessInstanceId = @rootId
	SELECT @isHA = value_int FROM @values

	IF isnull(@isHA,0) = 1
		BEGIN
			SET @content = dbo.f_translate('W celu ubiegania się o zwrot kosztów samodzielnie zorganizowanego świadczenia prosimy o przesłanie na adres Starter24, ',default) +
										 'ul. Kraszewskiego 30, 60-519 Poznań (z dopiskiem Dział Obsługi Klienta):' +
										 '<br>- krótkiego pisma przewodniego z informacją o zorganizowanych świadczeniach wraz z danymi kontaktowymi  (adres do korespondencji, adres email, numer telefonu)' +
										 '<br>- oryginału rachunków potwierdzających poniesiony koszt' +
										 '<br>- kopię polisy' +
										 '<br>- numer rachunku bankowego, na który życzy sobie Pan/Pani otrzymać zwrot uznanych kosztów.<br><br>Wniosek o refundacje kosztów może Pan/Pani złożyć również w formie elektronicznej przesyłając dokumenty na adres dok@starter24.pl . W przypadku pozytywnego rozpatrzenia, nasz Dział Obsługi Klienta poprosi Pana/Panią o przesłanie oryginałów rachunków w celu realizacji zwrotu. <br><br>Decyzję w zakresie sposobu rozpatrzenia otrzymają Państwo w terminie do 30 dni od daty wpływu wniosku.<br><br>Jeżeli po złożeniu wniosku mieliby Państwo jakiekolwiek pytania dotyczące procesu rozpatrzenia prosimy o bezpośredni kontakt z naszym Działem Obsługi Klienta:<br>tel. 61 83 19 913 (pon.-pt. 9-16) e-mail: dok@starter24.pl'

		end
	ELSE
		BEGIN
			SET @content = 'W celu ubiegania się o zwrot kosztów samodzielnie zorganizowanego świadczenia prosimy o przesłanie na adres Starter24, ul. Kraszewskiego 30, 60-519 Poznań (z dopiskiem Dział Obsługi Klienta):<br>- krótkiego pisma przewodniego z informacją o zorganizowanych świadczeniach wraz z danymi kontaktowymi  (adres do korespondencji, adres email, numer telefonu)<br>- oryginału rachunków potwierdzających poniesiony koszt<br>- kopii dowodu rejestracyjnego pojazdu (obie strony)<br>- numer rachunku bankowego, na który życzy sobie Pan/Pani otrzymać zwrot uznanych kosztów.<br><br>Wniosek o refundacje kosztów może Pan/Pani złożyć również w formie elektronicznej przesyłając dokumenty na adres dok@starter24.pl . W przypadku pozytywnego rozpatrzenia, nasz Dział Obsługi Klienta poprosi Pana/Panią o przesłanie oryginałów rachunków w celu realizacji zwrotu. <br><br>Decyzję w zakresie sposobu rozpatrzenia otrzymają Państwo w terminie do 30 dni od daty wpływu wniosku.<br><br>Jeżeli po złożeniu wniosku mieliby Państwo jakiekolwiek pytania dotyczące procesu rozpatrzenia prosimy o bezpośredni kontakt z naszym Działem Obsługi Klienta:<br>tel. 61 83 19 913 (pon.-pt. 9-16) e-mail: dok@starter24.pl'

		end

	SET @subject = dbo.f_translate('Informacja dotycząca refundacji kosztów świadczeń dla zgłoszenia ',default)+dbo.f_caseId(@groupProcessInstanceId)
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,368', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @email = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')


	SET @email = dbo.f_getRealEmailOrTest(@email)

	EXECUTE dbo.p_note_new 
     @groupProcessId = @groupProcessInstanceId
    ,@type = dbo.f_translate('email',default)
    ,@content = @content
    ,@email = @email
    ,@userId = 1  -- automat
    ,@subject = @subject
    ,@direction=1
    ,@dw = ''
    ,@udw = ''
    ,@sender = @senderEmail
    ,@emailBody = @content
    ,@emailRegards = 1
    ,@err=@err OUTPUT
    ,@message=@message OUTPUT

	IF isnull(@isHA,0) = 1
		BEGIN

			SET @content = 'Szanowni Państwo. W celu ubiegania się o zwrot kosztów samodzielnie zorganizowanego świadczenia uprzejmie prosimy o przesłanie na adres Starter24, ul. Kraszewskiego 30, 60-519 Poznań (z dopiskiem Dział Obsługi Klienta): krótkiego pisma przewodniego z informacją o zorganizowanych świadczeniach wraz z danymi kontaktowymi (adres do korespondencji, adres email, numer telefonu), oryginału rachunków potwierdzających poniesiony koszt, ' +
										 ' kopię polisy, numer rachunku bankowego, na który życzą sobie Państwo otrzymać zwrot uznanych kosztów. Jednocześnie informujemy, że wniosek o refundację kosztów mogą Państwo złożyć również w formie elektronicznej przesyłając dokumenty na adres dok@starter24.pl . W przypadku pozytywnego rozpatrzenia, nasz Dział Obsługi Klienta poprosi Państwa o przesłanie oryginałów rachunków w celu realizacji zwrotu.'
		end
ELSE
	BEGIN

			SET @content = 'Szanowni Państwo. W celu ubiegania się o zwrot kosztów samodzielnie zorganizowanego świadczenia uprzejmie prosimy o przesłanie na adres Starter24, ul. Kraszewskiego 30, 60-519 Poznań (z dopiskiem Dział Obsługi Klienta): krótkiego pisma przewodniego z informacją o zorganizowanych świadczeniach wraz z danymi kontaktowymi (adres do korespondencji, adres email, numer telefonu), oryginału rachunków potwierdzających poniesiony koszt, kopii dowodu rejestracyjnego pojazdu (obie strony), numer rachunku bankowego, na który życzą sobie Państwo otrzymać zwrot uznanych kosztów. Jednocześnie informujemy, że wniosek o refundację kosztów mogą Państwo złożyć również w formie elektronicznej przesyłając dokumenty na adres dok@starter24.pl . W przypadku pozytywnego rozpatrzenia, nasz Dział Obsługi Klienta poprosi Państwa o przesłanie oryginałów rachunków w celu realizacji zwrotu.'
		end

    EXEC dbo.p_note_new
	@groupProcessId = @groupProcessInstanceId,
	@type = dbo.f_translate('sms',default),
	@content = @content,
	@phoneNumber = @callerPhone,
	@err = @err OUTPUT,
	@message = @message OUTPUT
		
    
END