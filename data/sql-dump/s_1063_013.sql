


ALTER PROCEDURE [dbo].[s_1063_013]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @stepId NVARCHAR(10)
	DECLARE @createdAt DATETIME
	DECLARE @purchuaseDate DATETIME
	DECLARE @maxMileage INT
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @message NVARCHAR(255)
	DECLARE @err INT
	DECLARE @processInstanceId INT
	DECLARE @rootId INT
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id, @createdAt = created_at, @stepId = step_id FROM process_instance where id = @previousProcessId
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,523', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @purchuaseDate = value_date FROM @values
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '479', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @maxMileage = value_int FROM @values
	
	SET @variant = 1
	
	IF @purchuaseDate > DATEADD(yy, -2, @createdAt) OR @purchuaseDate IS NULL 
	BEGIN
		EXEC p_attribute_edit
			@attributePath = '534', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Warunkowo udzielimy podstawowych świadczeń oraz podejmiemy próbę ich zweryfikowania najszybciej jak to możliwe. W przypadku pozytywnej weryfikacji będziemy mogli zaoferować wszystkie dostępne w programie usługi, w przeciwnym przypadku zaś ',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
	END
	ELSE IF @maxMileage IS NOT NULL
	BEGIN
		EXEC p_attribute_edit
			@attributePath = '534', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Jak najszybciej postaramy się potwierdzić uprawnienia do assistance ze wskazanym partnerem serwisowym. Do tego czasu, zakładając iż posiada Pan/Pani przedłużoną gwarancję wraz z pakietem assistance, udzielimy pomocy w podstawowym zakresie. ',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
	END
END
