ALTER PROCEDURE [dbo].[p_check_available_programs]
@processInstanceId INT,
@overrideVariant INT OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON
	
	PRINT '------------------ START p_check_available_programs'
	
	DECLARE @err INT
	DECLARE @baseProgramId NVARCHAR(255) 	
	DECLARE @message NVARCHAR(4000)
	DECLARE @weightLimit INT
	DECLARE @platformId INT
	DECLARE @platformName NVARCHAR(255)
	DECLARE @isHighway INT
	DECLARE @notSupportedCountry INT
	DECLARE @notSupportedDiagnosis INT
	DECLARE @accessDeniedConditions INT
	DECLARE @productionDate DATETIME
	DECLARE @otherCaseLastMonth INT
	DECLARE @lastCaseFixed INT
	DECLARE @eventLocationCountry NVARCHAR(255)
	DECLARE @vehicleAtService INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @variant INT
	DECLARE @mainProcessId INT
	DECLARE @policeConfirmation INT
	DECLARE @programId INT
	DECLARE @servicesIds NVARCHAR(255)
	DECLARE @isDealerCall INT
	DECLARE @eventType INT
	DECLARE @programNotFound INT
	DECLARE @towingError INT
	DECLARE @fixError INT
	DECLARE @taxiError INT
	DECLARE @partsError INT
	DECLARE @replacementCarError INT
	DECLARE @hotelError INT
	DECLARE @tripError INT
	DECLARE @transportError INT
	DECLARE @loanError INT
	DECLARE @medicalLawAdviceError INT
	DECLARE @cancel INT
	DECLARE @errMessage NVARCHAR(4000)
	DECLARE @partnerName NVARCHAR(200)
	DECLARE @groupProcessInstanceId INT
	DECLARE @caseSource INT
	DECLARE @programIds NVARCHAR(255)
	DECLARE @xid INT
	DECLARE @xstart_date DATETIME
	DECLARE @xend_date DATETIME
	DECLARE @vin NVARCHAR(30)
	DECLARE @caseFromPartner INT 
	DECLARE @startPeriod DATETIME
	DECLARE @assistanceFrom DATETIME
	DECLARE @addedProgram int 
	DECLARE @processPath int 
	DECLARE @rootPlatformId INT 
	
	declare @diagnosisDescription nvarchar(1000)
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	CREATE TABLE #partnerTempTable (partnerId INT, partnerName NVARCHAR(300), priority INT, distance DECIMAL(18,2), phoneNumber NVARCHAR(100), reasonForRefusing NVARCHAR(100), partnerServiceId INT)

	SET @cancel = 0
	SET @err = 0
	SET @taxiError = 0
	SET @fixError = 0
	SET @towingError = 0
	SET @replacementCarError = 0
	SET @partsError = 0	
	SET @hotelError = 0
	SET @tripError = 0
	SET @transportError = 0
	SET @loanError = 0
	SET @medicalLawAdviceError = 0
	
	SELECT @mainProcessId = root_id, @groupProcessInstanceId = group_process_id, @stepId = step_id FROM process_instance with(nolock) WHERE id = @processInstanceId
	
	IF @stepId NOT LIKE '1011.%'
	BEGIN
		SET @groupProcessInstanceId = @mainProcessId
	END 
	
	DECLARE @platformGroup nvarchar(255)
	EXEC dbo.[p_platform_group_name] @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '115', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @processPath = value_int FROM @values
	
	IF @processPath = 2
	BEGIN
		RETURN
	END 
	
	-- TODO: DEALERCALL
	SELECT @isDealerCall = 0
	
	DECLARE @arcCode NVARCHAR(20)
	SELECT @arcCode = dbo.f_diagnosis_code(@mainProcessId)
	select @diagnosisDescription = dbo.f_diagnosis_description(@mainProcessId, default)

	EXEC p_running_services
	@groupProcessInstanceId = @groupProcessInstanceId,
	@servicesIds = @servicesIds OUTPUT --activated services

	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	SELECT @platformName = name FROM platform with(nolock) where id = @platformId
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @mainProcessId
	SELECT @rootPlatformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '265', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @addedProgram = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventLocationCountry = value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '573', @groupProcessInstanceId = @mainProcessId
	SELECT @programNotFound = value_int FROM @values
	
	DECLARE @diagnosisSummary NVARCHAR(255)
	SELECT @diagnosisSummary = dbo.f_diagnosis_summary(@groupProcessInstanceId)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = ISNULL(value_string,'') FROM @values
	
	SET @baseProgramId = @programId
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '716', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @caseFromPartner = value_int FROM @values
	
	IF @programId = '423' AND ISNULL(@eventLocationCountry,dbo.f_translate(dbo.f_translate('Polska',default),default)) <> dbo.f_translate(dbo.f_translate('Polska',default),default) 
	BEGIN
		PRINT '---xxx-aaaa'	
		CREATE TABLE #partners (partner_id INT, location_id INT, location_name VARCHAR(100), email VARCHAR(100), phone VARCHAR(20), priority INT, extraContent VARCHAR(400))
		
		EXEC [dbo].[p_find_PZ]
		@country = @eventLocationCountry,
		@platformId = @platformId,
		@groupProcessInstanceId = @groupProcessInstanceId		
		
		DECLARE @partnerPhone nvarchar(30)
		
		SELECT top 1 @partnerPhone = phone from #partners
		
		drop table #partners
		
		SET @err = 1
		SET @overrideVariant = 97
		SET @errMessage = dbo.f_translate('Nie mamy możliwości bezpośredniego organizowania oraz oszacowania kosztów usług ad-hoc za granicą.',default)
		IF ISNULL(@partnerPhone,'') <> ''
		BEGIN
			SET @errMessage = @errMessage + dbo.f_translate(' Uprzejmie proszę o bezpośredni kontakt z partnerem zagranicznym pod numerem ',default)+ @partnerPhone + dbo.f_translate(' w celu uzyskania informacji o możliwościach, kosztach i warunkach świadczeń',default)
		END
		
		EXEC p_attribute_edit
		@attributePath = '554',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = @stepId,
		@valueText = @errMessage,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		RETURN
	END
	ELSE IF ISNULL(@servicesIds,'') <> '' OR @stepId IN ('1011.98') OR @caseFromPartner = 1
	BEGIN		
		PRINT dbo.f_translate('aaaaxxxx',default)
		SET @overrideVariant = NULL
		RETURN
	END
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '204', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programIds = value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventType = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '542', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @policeConfirmation = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,102', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @caseSource = value_int FROM @values
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,76', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @weightLimit = value_int FROM @values
	DELETE FROM @values
	
	DECLARE @programFromPZ SMALLINT
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '713', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programFromPZ = value_int FROM @values
		
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vin = value_string FROM @values
	
	declare @homeAssistance int

	set @homeAssistance=0
	if @vin is null
	begin
		set @homeAssistance=1		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '440', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @vin = value_string FROM @values
	end

	DECLARE @allAvailableInAppServices TABLE ( id INT, alias nvarchar(100))

 	INSERT INTO @allAvailableInAppServices (id, alias)  --Taking all services with setted alias and active
 	SELECT sd.id , sd.alias
	FROM dbo.service_definition sd with(nolock)
 	INNER JOIN dbo.service_definition_translation trans  with(nolock)
 		ON trans.translatable_id = sd.id
 	WHERE sd.alias IS NOT NULL
			AND
				sd.active = 1
	GROUP BY sd.id, sd.alias

	
	-------- nagłówki programów
	
	DECLARE @headerIds NVARCHAR(255)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '207', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @headerIds = value_string FROM @values
	
	declare @programTempTable as table (id int, start_date datetime, end_date datetime, program_id int)

	insert into @programTempTable(id, start_date, end_date, program_id)
	select id, start_date, end_date, program_id
	from dbo.vin_header h 
	where id in (
		select data from dbo.f_split(@headerIds,',')
	)
	
	IF @headerIds IS NULL 
	BEGIN
		SET @programNotFound = 1
	END 
	
	IF @programNotFound IS NOT NULL
	BEGIN
		INSERT INTO @programTempTable
		SELECT null, null, null, @programId
	END 
	
	IF @addedProgram IS NOT NULL
	BEGIN
		INSERT INTO @programTempTable
		SELECT null, null, null, @addedProgram
	END 

 
	-- WYKLUCZONE OKOLICZNOŚCI
	IF @err = 0
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '538', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @accessDeniedConditions = value_int FROM @values
		
		DECLARE @accessDeniedConditionsText NVARCHAR(255)
		SELECT @accessDeniedConditionsText = d.textD 
		FROM dbo.dictionary d with(nolock) 
		where typeD = 'accessDeniedConditions' 
		and d.value = @accessDeniedConditions 
		and dbo.f_exists_in_split(ISNULL(NULLIF(d.argument1,''),@platformId),@platformId) = 1  
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,427', @groupProcessInstanceId = @groupProcessInstanceId			
		SELECT @productionDate = value_date FROM @values	
		
		declare @basicService int
		set @basicService=0

		select @basicService=psc.basicService
		from dbo.platform_special_conditions psc with(nolock)  
		where psc.accessDeniedConditionId=@accessDeniedConditions and dbo.f_exists_in_split(platformIds,@platformId)=1 

		IF (@accessDeniedConditions > 1 OR (@accessDeniedConditions = 1 AND @productionDate < DATEADD(yy,-2,GETDATE()))) and @basicService=0
		BEGIN
			set @err = 1				
			set @errMessage = dbo.f_translate('Zgodnie z ogólnymi warunkami ubezpieczenia awarie i szkody spowodowane przez ',default)+@accessDeniedConditionsText+' nie są objęte ochroną Assistance. W związku z tym w opisanej przez Pana/Panią sytuacji nie mamy możliwości zorganizowania pomocy bezpłatnej. Czy jest Pan/Pani zainteresowany/a pomocą odpłatną?'
			set @overrideVariant = 96
		END 
	END
	
	-------------------------------------	
	PRINT '---programId-A'
	PRINT @programId
	
	
	declare kur cursor LOCAL for
	 	SELECT id, start_date, end_date, program_id FROM @programTempTable	 	
	OPEN kur;
	FETCH NEXT FROM kur INTO @xid, @xstart_date, @xend_date, @programId
	WHILE @@FETCH_STATUS=0
	BEGIN	 		 
		DECLARE @configMaxDMC nvarchar(255)
		DECLARE @configMaxPeople int
		
		SET @err = 0
		set @configMaxDMC = null 
		set @configMaxPeople = null 
		
		SELECT @platformId = platform.id, @platformName = platform.name 
		FROM dbo.platform platform
		INNER JOIN dbo.vin_program program on program.platform_id = platform.id
		WHERE program.id = @programId
		
		-- BŁĘDNIE ZWERYFIKOWANY BRAK W BAZIE 
		IF ISNULL(@programNotFound,-1) = 0 AND @stepId <> '1063.017'
		BEGIN
			SET @err = 1
			SET @errMessage = dbo.f_translate('W związku z negatywną weryfikacją uprawnień, możemy Panu/Pani zaoferować tylko usługi płatne',default)
			SET @overrideVariant = 96
		END 
	
		PRINT '---programId-B'
		PRINT @programId
	
		-- ZBYT DUŻE DMC
		IF @err = 0
		BEGIN
			
				
			SELECT @configMaxDMC = [value], @message = description from dbo.f_get_platform_key_with_description('max_dmc', @platformId, @programId)	
			
			if @weightLimit > CAST(@configMaxDMC AS INT) AND ISNULL(@configMaxDMC,'') <> '' 
			BEGIN				
				set @err = 1
				set @errMessage = @message
				set @overrideVariant = 97
				
				
			END			
		END
		
		-- ILOŚĆ OSÓB W AUCIE
		IF @err = 0 
		BEGIN
						
			DECLARE @peopleInCar INT
			
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '79', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @peopleInCar = value_int FROM @values
			
			SELECT @configMaxPeople = [value], @message = description from dbo.f_get_platform_key_with_description('max_people_in_car', @platformId, @programId)	
			
			if @peopleInCar > CAST(@configMaxPeople AS INT) AND ISNULL(@configMaxPeople,'') <> '' 
			BEGIN				
				set @err = 1
				set @errMessage = @message
				set @overrideVariant = 96
			END		
			
		END
		
		PRINT '---programId-C'
		PRINT @programId
		
		-- WYKLUCZONA LOKALIZACJA ZDARZENIA
		IF @err = 0
		BEGIN
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '513', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @isHighway = value_int FROM @values
	
			IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
			BEGIN
				DECLARE @alreadyHasPerms int
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '534', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @alreadyHasPerms = value_int FROM @values

						
				IF @platformId = 43
					BEGIN

						IF isnull(@alreadyHasPerms,0)=0-- jeśli ald to wyślij maila
							BEGIN
								DECLARE @newProcessId NVARCHAR(30)
									EXEC p_process_new @stepId = '1011.081',
																		 @groupProcessId = @groupProcessInstanceId,
																		 @rootId = @mainProcessId,
																		 @parentProcessId =  @processInstanceId,
																			@message = @message OUTPUT,
																			@err = @err OUTPUT,
																		@processInstanceId = @newProcessId OUTPUT
									EXEC p_send_email_to_ald_without_packet @previousProcessId = @groupProcessInstanceId
									set @err = 1
									set @errMessage = dbo.f_translate('Niestety pakiet assistance, który zakupiła Państwa firma nie upoważnia do bezpłatnej pomocy poza terenem Polski. W celu potwierdzenia zgody na organizację assistance skontaktujemy się z Państwa firmą',default)
									set @overrideVariant = 97
								end
							END

				ELSE
					BEGIN
					set @err = 1
					set @errMessage = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance. Nie mamy możliwości zorganizowania dla Pana/Pani pomocy Assistance',default)
					set @overrideVariant = 97
				END
			END

      IF @isHighway = 1 AND @eventLocationCountry = dbo.f_translate('Francja',default) AND @stepId != '1011.95'
        BEGIN
          set @err = 1
          set @errMessage = 'Ze względu na specyfikę działania prywatnych operatorów autostrad na terenie Francji, nie mamy możliwości organizacji bezpośredniej pomocy. Proszę zwrócić się o pomoc do służb autostradowych korzystając z najbliższej budki SOS, umiejscowionej co ok. 2 km przy poboczu drogi lub skorzystać z aplikacji mobilnej &lt;&lt;SOS Autoroute&gt;&gt;. Jeśli po interwencji tych służb nadal nie można poruszać się samochodem, a znajduje się on już poza chronionym obszarem autostrady, prosimy o ponowny kontakt i podanie nowej lokalizacji. Proszę pamiętać o zachowaniu imiennych rachunków w celu ubiegania się o zwrot kosztów.'
				set @overrideVariant = 95
          END
		END
	
		--- WYKLUCZONE KODY DIAGNOZ
		IF @err = 0 
		BEGIN	
			IF EXISTS(
				SELECT id 
				FROM dbo.rsa_close_event with(nolock)
				where ISNULL(condition,'') = '' AND @arcCode LIKE arc_mask AND description = dbo.f_translate('brak bezpłatnej obsługi',default) AND ((platform_id = @platformId and program_id IS NULL) OR program_id = @programId) 
			)
			BEGIN
				set @err = 1
				set @overrideVariant = 96
				if @homeAssistance=1
				begin
					set @errMessage = 'Zgodnie z ogólnymi warunkami ubezpieczenia opisana przez Pana/Panią sytuacja ('+ @diagnosisDescription + ') nie jest objęte ochroną Assistance.'	
				end
				else
				begin
					set @errMessage = 'Zgodnie z ogólnymi warunkami ubezpieczenia opisana przez Pana/Panią sytuacja ('+ @diagnosisDescription + ') nie jest objęte ochroną Assistance. Czy jest Pan/Pani zainteresowany/a pomocą odpłatną?'
				end
			END 			
		END
		
		
--		
		--- WYKLUCZONE ZDARZENIA
		IF @err = 0 
		BEGIN	
			IF EXISTS(
				SELECT ce.id 
				FROM dbo.rsa_close_event ce with(nolock) inner join	
					 dbo.dictionary d with(nolock) on d.textD=ce.arc_mask and d.typeD='eventType'
				where ISNULL(condition,'') = '' AND 
					(d.value=@eventType) AND 
					description = dbo.f_translate('brak bezpłatnej obsługi',default) AND 
					((platform_id = @platformId and program_id IS NULL) OR program_id = @programId) 
			)
			BEGIN
				declare @eventTypeDesc nvarchar(200)

				select @eventTypeDesc=textD 
				from  dbo.dictionary 
				where	typeD='eventType' and 
						value=@eventType
			
				set @err = 1
				set @overrideVariant = 96
				if @homeAssistance=1
				begin
					set @errMessage = 'Zgodnie z ogólnymi warunkami ubezpieczenia ten typ zdarzenia ('+isnull(@eventTypeDesc,'')+') nie jest objęte ochroną Assistance.'
				end
				else
				begin
					set @errMessage = 'Zgodnie z ogólnymi warunkami ubezpieczenia ten typ zdarzenia ('+isnull(@eventTypeDesc,'')+') nie jest objęte ochroną Assistance. Czy jest Pan/Pani zainteresowany/a pomocą odpłatną?'
				end
			END 			
		END

		-- KRADZIEŻ I BRAK POTWIERDZENIA ZGŁOSZENIA NA POLICJĘ
		IF @arcCode = '1464391' AND ISNULL(@policeConfirmation,0) = 0 AND @stepId NOT IN ('1011.055','1012.006') AND @platformGroup <> dbo.f_translate('CFM',default)
		BEGIN
			set @err = 1
			set @errMessage = dbo.f_translate('By móc świadczy usługi assistance, potrzebujemy otrzymać potwierdzenie zgłoszenia zdarzenia na Policję. Proszę dokonać zgłoszenia i przesłać do nas kopię tego dokumentu.',default)
			set @overrideVariant = 93
		END
		
	--	select @platformGroup, @programId

--		IF @err=0 and @programId = 561 AND @platformGroup = dbo.f_translate('CFM',default)
--		BEGIN
--			set @err = 1
--			set @errMessage = 'UWAGA! BRAK ASSISTANCE – PO POMOC KIEROWAĆ do AXA +48 22 575 97 75'
--			set @overrideVariant = 91
--		END
--		

		IF @err=0 and @platformId in (25,75,48) AND @platformGroup = dbo.f_translate('CFM',default)
		BEGIN
		
			if exists(
			
			select	pin.id
			from	dbo.process_instance pin with(nolock) inner join
					dbo.step_translation st with(nolock) on st.translatable_id=pin.step_id and st.locale='pl' 
			where	pin.root_id=@mainProcessId and 
					st.description=dbo.f_translate(dbo.f_translate('Prosimy o zorganizowanie kluczy, w razie komplikacji prosimy o ponowny kontakt z assistance',default),default) and
					pin.last_run=1

			)
			begin
				set @err = 1
				set @errMessage = dbo.f_translate(dbo.f_translate('Prosimy o zorganizowanie kluczy, w razie komplikacji prosimy o ponowny kontakt z assistance',default),default)
				set @overrideVariant = 91

			end
		END
		

		PRINT '---programId-D'
		PRINT @programId
		
		-- OBSZAR USŁUGI HOLOWANIA (USŁUGA PODSTAWOWA RÓWNIEŻ WYRZUCA PROGRAM)
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('towing.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			set @err = 1
			set @errMessage = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance. Nie mamy możliwości zorganizowania dla Pana/Pani pomocy Assistance',default)
			set @overrideVariant = 97			
		END 
		
		-- OBSZAR USŁUGI NAPRAWY (USŁUGA PODSTAWOWA RÓWNIEŻ WYRZUCA PROGRAM)
		IF ISNULL(@eventLocationCountry, '') <> '' AND dbo.f_is_event_location_in_scope('rsa.event_place_disabled_countries',@eventLocationCountry,@platformId,@programId) >0
		BEGIN
			set @err = 1
			set @errMessage = dbo.f_translate('Kraj w którym się Pan/Pani znajduje nie jest objęty ochroną ',default)+@platformName+dbo.f_translate(' Assistance. Nie mamy możliwości zorganizowania dla Pana/Pani pomocy Assistance',default)
			set @overrideVariant = 97
		END 
		
		PRINT '---programId-E'
		PRINT @programId
		
		IF @err = 0 
		BEGIN
			declare @countVins int
			set @countVins=0
			declare @casesIds nvarchar(200)
			
			IF EXISTS(
				SELECT id 
				FROM dbo.rsa_close_event  with(nolock)
				where @arcCode LIKE arc_mask 
				AND  description like 'Dopu%2%razy%ubezpie%'
				AND platform_id=@platformId
				AND program_id=@programId
				
			)
			begin
				select	@countVins=count(distinct avvin.root_process_instance_id),
						@casesIds=dbo.GROUP_CONCAT_D(distinct dbo.f_caseId(avvin.root_process_instance_id),', ')
				from	dbo.attribute_value avvin with(nolock) inner join
						dbo.attribute_value avdiag with(nolock) on avdiag.root_process_instance_id=avvin.root_process_instance_id and avdiag.attribute_path='419' and avdiag.value_int=0
				where	avvin.attribute_path='74,71' and 
						avvin.created_at>@xstart_date and
						avvin.value_string = @vin
						and dbo.f_diagnosis_code(avvin.root_process_instance_id) = @arcCode

				if @countVins>2 AND (@programId <> 631 OR (ISNULL(@arcCode,'') <> '' AND (@arcCode NOT LIKE '678%' AND @arcCode NOT LIKE '679%' AND @arcCode NOT LIKE '680%'))) 
				begin
					set @err = 2
					set @overrideVariant = 96
					set @errMessage = 'Limit dwóch interwencji po awarii w trakcie obowiązywania obecnego ubezpieczenia został już wykorzystany (sprawy '+isnull(@casesIds,'')+'). W tej sytuacji możemy zaoferować pomoc odpłatną.'				
				end
			end
		END

		declare @countPolicyNumber int

		If @platformId = 9 and @err = 0
		BEGIN

			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '982,983', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @vin = value_string FROM @values
			
			select	@countPolicyNumber=count(distinct avvin.root_process_instance_id)
			from	dbo.attribute_value avvin with(nolock)
							inner join process_instance i with(nolock) on avvin.group_process_instance_id = i.root_id
							inner join service_status ss with(nolock) on ss.group_process_id = i.group_process_id
							inner join dbo.service_status_dictionary ssd with(nolock) on ssd.id = ss.status_dictionary_id
			where	avvin.attribute_path='982,983'
				and avvin.value_string = @vin
				and ss.serviceId =2 and ssd.progress >1 and ssd.progress<5

	
--				select	@countPolicyNumber=count(distinct avvin.root_process_instance_id)
--				from	dbo.attribute_value avvin inner join
--									dbo.attribute_value avdiag on avdiag.root_process_instance_id=avvin.root_process_instance_id
--				where	avvin.attribute_path='982,983' and
--					avvin.value_string = @vin
						 	 
			PRINT @countPolicyNumber
			
			IF @countPolicyNumber >= 2
				BEGIN 
				set @errMessage = dbo.f_translate('Limit dwóch interwencji został już wykorzystany. Nie mamy możliwości zorganizowania pomocy w ramach gwarancji.',default)
				set @err = 2
				set @overrideVariant = 97
			END 

		END

		IF @err = 0 
		BEGIN
			IF EXISTS(
				SELECT id 
				FROM dbo.rsa_close_event 
				where @arcCode LIKE arc_mask 
				AND description = dbo.f_translate('Świadczenie ograniczone do 2 interwencji w roku',default)
				AND platform_id=@platformId
				AND (program_id=@programId or program_id is null)
			)
			begin
				set @countPolicyNumber=0

				declare @casesIds2 nvarchar(2000)

				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '981,67',
						@groupProcessInstanceId = @groupProcessInstanceId
				SELECT @assistanceFrom = value_date FROM @values
				
				IF ISNULL(@assistanceFrom, '1980-01-01') < '2000-01-01'
				BEGIN
					SET @assistanceFrom = GETDATE()	
				END

				IF YEAR(GETDATE())=2019 AND MONTH(@assistanceFrom)=2 AND DAY(@assistanceFrom)=29
				BEGIN
					SET @startPeriod = '2019-03-01'
				END
				ELSE
				BEGIN
					SET @startPeriod = CONCAT(YEAR(GETDATE()), '-', MONTH(@assistanceFrom), '-', DAY(@assistanceFrom))
				END

				IF @platformId = 35
				BEGIN
					select	@countPolicyNumber=count(distinct avvin.root_process_instance_id),
							@casesIds2=dbo.GROUP_CONCAT_D(distinct dbo.f_caseId(avvin.root_process_instance_id)+' '+dbo.f_diagnosis_description (avvin.root_process_instance_id,'pl'),', ')
					from	dbo.attribute_value avvin  with(nolock)
					inner join dbo.attribute_value avprog with(nolock) on avprog.group_process_instance_id = avvin.group_process_instance_id and avprog.attribute_path = '202'
					inner join dbo.process_instance pin with(nolock) on pin.parent_id = avvin.group_process_instance_id 
					inner join dbo.service_status ss with(nolock) on ss.group_process_id = pin.group_process_id					
					where	avvin.attribute_path='74,71' and 
							avprog.value_string = @programId and
							avvin.created_at > @startPeriod and
							avvin.value_string = @vin and
							avvin.group_process_instance_id<>@groupProcessInstanceId
						 
				END 
				ELSE
				BEGIN
					select	@countPolicyNumber=count(distinct avvin.root_process_instance_id),
						@casesIds2=dbo.GROUP_CONCAT_D(distinct dbo.f_caseId(avvin.root_process_instance_id)+' '+dbo.f_diagnosis_description (avvin.root_process_instance_id,'pl'),', ')
					from	dbo.attribute_value avvin  with(nolock)
					where	avvin.attribute_path='440' and 
							dbo.f_service_top_progress_group(avvin.root_process_instance_id, 17) <> '' AND  -- sprwadzenie czy istnieje taka AKTYWNA usługa
							avvin.created_at > @startPeriod and
							avvin.value_string = @vin and
							avvin.group_process_instance_id<>@groupProcessInstanceId
				END 
				



				if @countPolicyNumber>=2
				begin
					set @err = 2
					set @overrideVariant = 96
							set @errMessage = 'Limit dwóch interwencji w roku został już wykorzystany (sprawy '+isnull(@casesIds2,'')+'). Nie mamy możliwości zorganizowania pomocy w ramach tego ubezpieczenia.'


				end
			end
		END 

		IF @err = 0 
		BEGIN
			
			IF EXISTS(
				SELECT id 
				FROM dbo.rsa_close_event  with(nolock)
				where @arcCode LIKE arc_mask 
				AND description = dbo.f_translate('Świadczenie ograniczone do 1 interwencji w roku',default)
				AND platform_id=@platformId
				AND (program_id=@programId or program_id is null)
			)
			begin
				
				DECLARE @countPolicyNumber2 int
				set @countPolicyNumber2=0
				DECLARE @casesIds3 nvarchar(500)

				INSERT  @values EXEC dbo.p_attribute_get2
						@attributePath = '981,67',
						@groupProcessInstanceId = @groupProcessInstanceId
				SELECT @assistanceFrom = value_date FROM @values
				
				IF ISNULL(@assistanceFrom, '1980-01-01') < '2000-01-01'
				BEGIN
					SET @assistanceFrom = GETDATE()	
				END

				SET @startPeriod = CONCAT(YEAR(GETDATE()), '-', MONTH(@assistanceFrom), '-', DAY(@assistanceFrom))
				
				select	@countPolicyNumber2=count(distinct avvin.root_process_instance_id),
						@casesIds3=dbo.GROUP_CONCAT_D(distinct dbo.f_caseId(avvin.root_process_instance_id)+' '+dbo.f_diagnosis_description (avvin.root_process_instance_id,'pl'),', ')
				from	dbo.attribute_value avvin with(nolock) 
				where	avvin.attribute_path='440' AND 
						dbo.f_service_top_progress_group(avvin.root_process_instance_id, 17) <> '' AND  -- sprwadzenie czy istnieje taka AKTYWNA usługa
						avvin.created_at > @startPeriod	 AND
						avvin.value_string = @vin AND
						avvin.group_process_instance_id<>@groupProcessInstanceId
			
				if @countPolicyNumber2>=1
				begin
					set @err = 2
					set @overrideVariant = 96
					set @errMessage = 'Limit jednej interwencji w roku został już wykorzystany (sprawy '+isnull(@casesIds3,'')+'). Nie mamy możliwości zorganizowania pomocy w ramach tego ubezpieczenia.'
				end
			end
		END 
		
		IF @err = 0 and @platformId in (53,58) 
		BEGIN
			
			DECLARE @makeModel nvarchar(255)
			declare @make2 nvarchar(50)

			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @makeModel = textD from dictionary where value = (select value_int FROM @values) and typeD = 'makeModel'
			
			declare @fleetK nvarchar(100)
			exec dbo.p_get_vin_headers_by_rootId @root_id=@groupProcessInstanceId, @columnName=dbo.f_translate('Marka',default), @output=@make2 OUTPUT

			
			IF isnull(@makeModel,'') like 'BMW %' OR isnull(@makeModel,'') like 'MINI %' or isnull(@make2,'') like 'BMW%' OR isnull(@make2,'') like 'MINI%'
			BEGIN
				SET @err = 1
				SET @overrideVariant = 91
				SET @errMessage = 'Szanowny Panie/Pani, obsługa BWM/MINI została przekierowana do innego Centrum Zgłoszeniowego. Prosimy o kontakt pod numer 880 300 100.'				
			END 
			
		END 
			
		IF @err = 0 
		BEGIN

			--- WARUNKI SZCZEGÓŁOWE DLA PLATFORM
			IF @platformId IN (6,11,31)
			BEGIN
					
				-- diagnoza wskazuje na NPT
				IF @err = 0 AND @diagnosisSummary IN ('[NpT]', '[NpT-P]', '[NpT-T]') AND ISNULL(@eventLocationCountry,'') = dbo.f_translate(dbo.f_translate('Polska',default),default)
				BEGIN
					
					EXEC p_find_services @groupProcessInstanceId = @groupProcessInstanceId
	
					DECLARE @distance DECIMAL(10,2)
					SELECT TOP 1 @partnerName = partnerName, @distance = distance FROM #partnerTempTable order by distance asc
	
					IF @distance <= 50
					BEGIN						
						set @err = 1					
						set @overrideVariant = 97
						set @errMessage = dbo.f_translate('Telefoniczna diagnoza usterki wskazuje, że może Pan/Pani bezpiecznie dojechać do najbliższego autoryzowanego Partnera Serwisowego ',default)
										 +ISNULL(@platformName,'')+' '+ISNULL(@partnerName,'')+'.'

					END
				END
				
			END 			
			ELSE IF @platformId = 2
			BEGIN
				
				-- diagnoza wskazuje na NPT
				IF @err = 0 AND @diagnosisSummary IN ('[NpT]', '[NpT-P]', '[NpT-T]') AND ISNULL(@eventLocationCountry,'') = dbo.f_translate(dbo.f_translate('Polska',default),default)
				BEGIN
					
					EXEC p_find_services @groupProcessInstanceId = @groupProcessInstanceId
					SELECT TOP 1 @partnerName = partnerName FROM #partnerTempTable order by distance asc
					
					set @err = 1					
					set @overrideVariant = 97
					set @errMessage = dbo.f_translate('Telefoniczna diagnoza usterki wskazuje, że może Pan/Pani bezpiecznie dojechać do najbliższego autoryzowanego Partnera Serwisowego ',default)
									 +ISNULL(@platformName,'')+' '+ISNULL(@partnerName,'')+'.'

				END
				
				PRINT '---programId-G'
			PRINT @programId
		
				IF @err = 0 AND @programId IN (432,435)
				BEGIN 
					
					-- POTWIERDENIE ZDARZENIA BLIŻEJ NIŻ 30KM LUB DALEJ NIŻ 30KM OD MIEJSCA ZAMIESZKANIA
					DECLARE @homeCloseDistance INT
					DECLARE @lessOrMore INT
					DECLARE @textDistance VARCHAR(100)
					
					DELETE FROM @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '906', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @homeCloseDistance = value_int FROM @values
					
					DELETE FROM @values
					INSERT @values EXEC p_attribute_get2 @attributePath = '192', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @lessOrMore = value_int FROM @values
					
					IF ISNULL(@homeCloseDistance, 1) = 0
					BEGIN
						
						IF @lessOrMore = 1
						BEGIN
							SET @textDistance = dbo.f_translate('mniejszej niż 30 km',default)
						END
						ELSE IF @lessOrMore = 2
						BEGIN
							SET @textDistance = dbo.f_translate('powyżej 30 km',default)
						END
						ELSE
						BEGIN
							SET @textDistance = '???'
						END
						
						set @err = 1					
						set @overrideVariant = 96
						set @errMessage = dbo.f_translate('W związku z tym, że zdarzenie ma miejsce w odgległości ',default) + @textDistance + dbo.f_translate(' od Pana/Pani miejsca zamieszkania możemy zaoferować usługi płatne.',default)
					END
					
				END 
	
			END
			
		END 
		
		-- SPRAWDZENIE, CZY POJAZD JEST ZA STARY NA ŚWIADCZENIE USŁUG
		IF @err = 0 
		BEGIN
			
			--- PRAWIE DLA WSZYSTKICH PROGRAMÓW W CONCORDII (W Basic nie ma naprawy)
			IF @eventType = 2 AND @programId IN (444, 445, 446, 495, 496, 497)
			BEGIN
				
				DECLARE @registerFirstDate DATETIME
				
				DELETE FROM @values
				INSERT @values EXEC p_attribute_get2 @attributePath = '74,233', @groupProcessInstanceId = @groupProcessInstanceId
				SELECT @registerFirstDate = value_date FROM @values
				
				IF @registerFirstDate IS NOT NULL
				BEGIN
					
					-- Starszy niż 20 lat wylatuje
					IF @registerFirstDate < DATEADD(year, -20, GetDate())
					BEGIN
						
						SET @err = 1
						SET @overrideVariant = 96
						SET @errMessage = dbo.f_translate('Zgodnie z ogólnymi warunkami ubezpieczenia awarie pojazdów starszych niz 20 lat nie sąobjęte ochroną Assistance. W tej sytuacji możemy zaoferować pomoc odpłatną.',default)
					
					END
					
				END
				
			END
			
		END 

    IF @err = 1 and @overrideVariant = 95
      BEGIN
        EXEC [dbo].[p_attribute_edit]
            @attributePath = '548',
            @stepId = 'xxx',
            @groupProcessInstanceId = @groupProcessInstanceId,
            @valueText = @errMessage,
            @err = @err,
            @message = @message
      end


		-- w cfm nie ma usług płatnych
		if @platformGroup = dbo.f_translate('CFM',default) AND @err > 0 and @overrideVariant = 96
		BEGIN
			set @err = 0
			set @overrideVariant = null
		END 
		
		IF @err = 1
		BEGIN						
			DELETE FROM @programTempTable WHERE program_id = @programId				
		END 
		ELSE IF @err = 2
		BEGIN
			DELETE FROM @programTempTable WHERE id = @xid
		END 
		
	FETCH NEXT FROM kur INTO @xid, @xstart_date, @xend_date, @programId;
	END
	CLOSE kur
	DEALLOCATE kur

	IF NOT EXISTS(
		select pt.id from dbo.vin_program p 
		inner join @programTempTable pt on pt.program_id = p.id
		where (p.platform_id = @rootPlatformId or p.platform_id is null)  
	) AND @programIds IS NOT NULL AND EXISTS (select id from @programTempTable)
	BEGIN
		delete from @programTempTable
		SET @errMessage = dbo.f_translate('Negatywna weryfikacaja uprawnień',default)
		SET @overrideVariant = 96
	END 
--	
	PRINT '----ver'
	PRINT @overrideVariant
	PRINT '-----ERRR'
	PRINT @err
	PRINT '---programId'
	PRINT @programId
	
	IF @err = 1 AND @overrideVariant = 93		
	BEGIN		
		RETURN		
	END
  IF @overrideVariant = 95
    BEGIN
      RETURN
    END

  IF (@programId = '423' and @overrideVariant <> 97)
	BEGIN		
		SET @overrideVariant = NULL
		RETURN
	END
	

	
	set @programIds = null
	set @headerIds = null
	
	select @programIds = dbo.group_concat_s(DISTINCT program_id,1)
	from   @programTempTable

	select @headerIds = dbo.group_concat_s(DISTINCT id,1)
	from   @programTempTable

	DECLARE @insuranceDateRange NVARCHAR(255)
	
	PRINT 'xxxx-----programIds'
	PRINT @programIds
	
	PRINT 'xxxx-----programId'
	PRINT @programId
	
	PRINT 'xxxx-----baseProgramId'
	PRINT @baseProgramId
	
	IF dbo.f_exists_in_split(@programIds,@baseProgramId) = 0 AND @programId IS NOT NULL
	BEGIN
		set @programId = null
		select top 1 @programId=program_id, 
		@insuranceDateRange = 'od ' + dbo.fn_vdate(tt.start_date) + ' do '+ dbo.fn_vdate(tt.end_date)
		from @programTempTable tt inner join dbo.vin_program p with(nolock) on p.id=tt.program_id
		where program_id = isnull(@programId,program_id) 
		order by p.breakdown_priority, tt.end_date	
	END
	ELSE
	BEGIN
		SET @programId = @baseProgramId	
	END 
	
	-- w tych programach nie proponujemy usług płatnych
	IF @overrideVariant = 96 AND (@baseProgramId IN (472,473,474,484,485,486,492,493) or @homeAssistance=1)
	BEGIN		
		SET @overrideVariant = 97	
		SET @errMessage = REPLACE(REPLACE(@errMessage,dbo.f_translate('W tej sytuacji możemy zaoferować pomoc odpłatną',default),''),'Czy jest Pan/Pani zainteresowany/a pomocą odpłatną?','')
		
	END 
	
	IF @overrideVariant = 96 AND @errMessage IS NOT NULL
	BEGIN
		
		EXEC [dbo].[p_attribute_edit]
		@attributePath = '532',
		@stepId = 'xxx',
		@groupProcessInstanceId = @groupProcessInstanceId,						
		@valueText = @errMessage,
		@err = @err,
		@message = @message
		
	END 
	ELSE IF @overrideVariant IN (91,97) AND @errMessage IS NOT NULL
	BEGIN		
		EXEC p_attribute_edit
		@attributePath = '554',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueText = @errMessage,
		@err = @err OUTPUT,
		@message = @message OUTPUT	
	END 
	
--  jak stop świadczeń, ale w puli jest jeszcze jakjakiś program to chyba OK?
	IF @programId IS NOT NULL -- AND @overrideVariant NOT IN (91,97)
	BEGIN
		SET @overrideVariant = null
	END

-- 	HELIS
	DECLARE @firstRegDate DATETIME
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2
			@attributePath = '74,233',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @firstRegDate = value_date FROM @values;


	IF @platformId = 14
		BEGIN
			DECLARE @testDate datetime = CONVERT(datetime, '01.01.2019', 104)


			IF isnull(@firstRegDate,'') = ''
				BEGIN
					DECLARE @in2019 int
					DELETE from @values
					INSERT INTO @values
							EXEC p_attribute_get2 @attributePath = '1051', @groupProcessInstanceId = @groupProcessInstanceId
					SELECT @in2019 = value_int from @values

					IF isnull(@in2019,0) =1
						BEGIN
							SET @firstRegDate = GETDATE()
						end


				end
			PRINT dbo.f_translate('INSIDE OPEL',default)
			IF DATEDIFF(day, @testDate, @firstRegDate) > 0
				BEGIN
					print dbo.f_translate('inside x2 ',default)
					IF isnull(@programId, 0) = '457'
						BEGIN
							SET @programId = '591'
							SET @programIds = replace(@programIds,'457','591')
						end
					IF isnull(@programId, 0) = '454'
						BEGIN
							SET @programId = '590'
							SET @programIds = replace(@programIds,'454','590')
						end
				end
			PRINT @programIds
			PRINT @programId
		END

  -- HELIS
  
  
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '207',
	@stepId = 'xxx',
	@groupProcessInstanceId = @groupProcessInstanceId,						
	@valueString = @headerIds,
	@err = @err,
	@message = @message

	EXEC [dbo].[p_attribute_edit]
	@attributePath = '204',
	@stepId = 'xxx',
	@groupProcessInstanceId = @groupProcessInstanceId,						
	@valueString = @programIds,
	@err = @err,
	@message = @message
				
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '202',
	@stepId = 'xxx',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@valueString = @programId,
	@err = @err,
	@message = @message	
	
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '854',
	@stepId = 'xxx',
	@groupProcessInstanceId = @groupProcessInstanceId,
	@valueString = @insuranceDateRange,
	@err = @err,
	@message = @message
	
	PRINT ' --------- RESULT --------------'
	PRINT '@programId'
	PRINT @programId
	PRINT '@overrideVariant'
	PRINT @overrideVariant
	PRINT '@errMessage'
	PRINT @errMessage
	PRINT @err
	
	DROP TABLE #partnerTempTable
	
END