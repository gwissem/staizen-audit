

ALTER PROCEDURE [dok].[P_ComplainSendTo]
@id int=0,
@complaintId int,
@eType int,
@userId int,
@sendTo int,
@finishDate datetime,
@sendTo2 int,
@finishDate2 datetime,
@createdAt datetime,
@note nvarchar(max),
@note2 nvarchar(max),
@link int,
@keyNote tinyint,
@fromTo tinyint,
@company nvarchar(1000),
@send tinyint
AS
begin
	SET NOCOUNT ON

	if @eType<>30 set @note2=null

	if (@sendTo=0)and @sendTo2<>0 set @sendTo=@sendTo2
	set @sendTo=isnull(@sendTo,@sendTo2)
	set @finishDate=isnull(@finishDate,@finishDate2)

	
	if @eType in (46,50)
	begin
		if @finishDate is null set @finishDate=getdate()+4

		begin tran

		INSERT INTO dok.complainsEvents
           (complaintId
           ,eType
           ,eDate
           ,eUserId
           ,eDescription
           ,eAcceptanceDate
           ,eAcceptance
           ,eUserIdA
           ,eAnswer
           ,documentId
           ,sendTo
		   ,finishDate
		   ,keyNote
		   ,updatedBy)
		VALUES
           (@complaintId
           ,@eType
           ,getdate()
           ,@userId
           ,@note
           ,null
		   ,null
		   ,null
		   ,null
		   ,@link
		   ,@sendTo
		   ,@finishDate
		   ,@keyNote
		   ,@userId)

		if (@eType=50)
		begin
			
			declare @dateStatus datetime

			set @dateStatus=getdate()

			--EXECUTE dok.P_ComplainEditStatus 
			--	   @id=@complaintId
			--	  ,@status=30
			--	  ,@userId=@userId
			--	  ,@date=@dateStatus
			--	  ,@cResult=null
		end

		if (@eType=46)
		begin
			
			set @dateStatus=getdate()

			--EXECUTE dok.P_ComplainEditStatus 
			--	   @id=@complaintId
			--	  ,@status=40
			--	  ,@userId=@userId
			--	  ,@date=@dateStatus
			--	  ,@cResult=null
		end
		
		declare @email nvarchar(1000)
		declare @subject nvarchar(1000)
		declare @caseNumber nvarchar(50)
		declare @cNumber nvarchar(50)
		declare @body nvarchar(1000)


		select @email=email
		from dbo.fos_user with(nolock)
		where id=@sendTo


		select @caseNumber=caseNumber,
				@cNumber=cNumber
		from	dok.complains with(nolock)
		where	id=@complaintId

		if @eType=46
		begin
			set @subject=dbo.f_translate('Masz nową prośbę o akceptację w sprawie reklamacji ',default)+@cNumber+dbo.f_translate(' do sprawy ',default)+@caseNumber
		end
		else
		begin
			set @subject=dbo.f_translate('Masz nowe zapytanie w sprawie reklamacji ',default)+@cNumber+dbo.f_translate(' do sprawy ',default)+@caseNumber
		end

		set @body=@note

		EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('DOK',default)
		,                            @recipients  = @email
		,                            @subject     = @subject
		,                            @body        = @body
		,	                         @body_format = dbo.f_translate('HTML',default)	

		commit tran

	end
	else
	begin
		if not exists(select * from dok.complainsEvents with(nolock) where id=@id)
		begin
		print '1'

			INSERT INTO dok.complainsEvents
			   (complaintId
			   ,eType
			   ,eDate
			   ,eUserId
			   ,eDescription
			   ,documentId
			   ,note2
			   ,keyNote
			   ,fromTo
			   ,company
			   ,updatedBy)
			 VALUES
				   (@complaintId
				   ,@eType
				   ,@createdAt
				   ,@userId
				   ,@note
				   ,@link
				   ,@note2
				   ,@keyNote
				   ,@fromTo
				   ,@company
				   ,@userId)

			if @eType=35 and @send=1
			begin
			
				select @caseNumber=caseNumber,
					   @cNumber=cNumber
				from	dok.complains with(nolock)
				where	id=@complaintId

				declare @attachmentId int
				declare @file_attachments nvarchar(4000)
				set @file_attachments=''
				
				declare kur cursor LOCAL for

					Select @link

				OPEN kur;
				FETCH NEXT FROM kur INTO @attachmentId;
				WHILE @@FETCH_STATUS=0
				BEGIN
		
					select	@file_attachments=@file_attachments+'\\atlas.starter24.pl\udzial'+f.path+';'
					from	dbo.files f with(nolock)
					where	f.document_id=@attachmentId

					FETCH NEXT FROM kur INTO @attachmentId;
				END
				CLOSE kur
				DEALLOCATE kur

				if len(@file_attachments)>1 
				begin
					set @file_attachments=left(@file_attachments,len(@file_attachments)-1)
				end

				set @file_attachments=replace(@file_attachments,'/','\')

				set @subject=dbo.f_translate('Informacja w sprawie reklamacji ',default)+@cNumber+dbo.f_translate(' do sprawy ',default)+@caseNumber
				
				set @body=@note

				EXEC msdb.dbo.sp_send_dbmail @profile_name= dbo.f_translate('DOK',default)
				,                            @recipients  = @company
				,                            @subject     = @subject
				,                            @body        = @body
				,	                         @body_format = dbo.f_translate('HTML',default)
				,							 @file_attachments=@file_attachments	



			end

		end
		else
		begin
			print '2'

			UPDATE dok.complainsEvents
			   SET eType = @eType
				  ,eDate = @createdAt
				 -- ,eUserId = @userId
				  ,eDescription = @note
				  ,documentId=@link
				  ,note2=@note2
				  ,keyNote=@keyNote
				  ,fromTo=@fromTo
				  ,company=@company
				  ,updatedBy=@userId
			 WHERE id=@id and eType<40 and (eUserId=@userId or eUserId=1)

		end


	end
		
end
