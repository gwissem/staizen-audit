ALTER PROCEDURE [dbo].[s_1007_052]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
)
AS
BEGIN
    DECLARE @groupProcessInstanceId INT
    DECLARE @sendDelayInfoSms INT
    DECLARE @status INT
    DECLARE @vehicleDelivered INT
    
	SELECT @groupProcessInstanceId = group_process_id FROM dbo.process_instance with(nolock) WHERE id = @previousProcessId
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '272', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @sendDelayInfoSms = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '764,797', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @status = value_int FROM @values
    
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '816', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vehicleDelivered = value_int FROM @values
    
	IF ISNULL(@vehicleDelivered,0) = 1 OR @variant = 3 -- wymuszenie
	BEGIN
		SET @variant = 3 
		RETURN
	END 
	ELSE
	BEGIN
		
		IF @status = 1
		BEGIN
			EXEC p_add_rental_offer @instanceId = @previousProcessId
			SET @variant = 2
		END 
		ELSE
		BEGIN
			SET @variant = 1
		END 
		
		IF ISNULL(@sendDelayInfoSms,0) = 1
		BEGIN
			DECLARE @err INT
			DECLARE @message NVARCHAR(255)
			DECLARE @callerPhone NVARCHAR(255)
			DECLARE @content NVARCHAR(4000)
			
			DELETE FROM @values
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @callerPhone = value_string FROM @values
			
			SET @content = dbo.f_translate('Przepraszamy za utrudnienia. Organizacja pojazdu zastępczego może ulec wydłużeniu, proszę spodziewać się kontaktu z naszej strony.',default)
			
	  		EXEC dbo.p_note_new
	  		@groupProcessId = @groupProcessInstanceId,
	  		@type = dbo.f_translate('sms',default),
	  		@content = @content,
	  		@phoneNumber = @callerPhone,
	  		@err = @err OUTPUT,
	  		@message = @message OUTPUT
	  		
		END 

	END
	
END


