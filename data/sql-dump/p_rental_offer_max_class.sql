ALTER PROCEDURE [dbo].[p_rental_offer_max_class]
@groupProcessInstanceId INT,
@class NVARCHAR(255) = null OUTPUT
AS
BEGIN
	
	DECLARE @rootId INT	
	DECLARE @offerMakeModelAcceptClasses NVARCHAR(255)
	DECLARE @clientClassPriceAgreement INT
	DECLARE @platformId INT 
	DECLARE @dataMatch int 
	DECLARE @startDate datetime 
	DECLARE @programId INT
	DECLARE @platformGroup NVARCHAR(255)
	DECLARE @maxClass NVARCHAR(255) 
	DECLARE @makeModelId INT 
	DECLARE @classPoints INT 
	DECLARE @type INT 
	
	SELECT @rootId = root_id FROM dbo.process_instance with(nolock) WHERE id = @groupProcessInstanceId
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '764,770', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @clientClassPriceAgreement = value_int FROM @values
	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output

	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
		SELECT @programId = value_string FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
		SELECT @platformId = value_int FROM @values			
	END

	DECLARE @isVip NVARCHAR(255) 
	
	IF @platformId = 48 -- BL
	BEGIN
		EXEC [dbo].[p_get_vin_headers_by_rootId]
		@root_id = @rootId, 
		@columnName = dbo.f_translate('Klasa',default),
		@output = @class output
		
		SET @class = IIF(@class = dbo.f_translate('LCV',default),dbo.f_translate(dbo.f_translate('DOSTAWCZY MAŁY',default),default),@class)		
	END	
	ELSE IF @platformId = 43 -- ALD
	BEGIN
		EXEC [dbo].[p_get_vin_headers_by_rootId]
		@root_id = @rootId, 
		@columnName = 'Sam_zastepczy',
		@output = @class output
		
		IF @class = 'M'
		BEGIN
			SET @class = dbo.f_translate(dbo.f_translate('DOSTAWCZY MAŁY',default),default)
		END 
		ELSE IF @class = 'N'
		BEGIN
			SET @class = dbo.f_translate('DOSTAWCZY DUŻY',default)
		END 
	END
	ELSE IF @platformId = 53 -- ALP
	BEGIN
		EXEC [dbo].[p_get_vin_headers_by_rootId]
		@root_id = @rootId, 
		@columnName = 'pojazd_zastepczy',
		@output = @class output
	
 
		IF @class = dbo.f_translate('auto zastępcze klasy e',default)
		BEGIN
			SET @class = null
		END 
--		ELSE IF @class = dbo.f_translate('auto zastępcze klasy auto zastępcze cięa1arowe do 3,5t',default)
--		BEGIN
--			SET @class = dbo.f_translate('DOSTAWCZY DUŻY',default)
--		END 
		ELSE
		BEGIN
			SET @class = REPLACE(REPLACE(@class,' (plus)',''),dbo.f_translate('auto zastępcze klasy',default),'')	
		END 
		
	END

	SET @class = LTRIM(RTRIM(@class))
	
	IF NOT EXISTS (select id from dbo.dictionary with(nolock) where typeD = 'makeModelClass' and textD = @class)
	BEGIN
		SET @class = null
		SELECT @class = [dbo].[f_get_platform_key]('replacement_car.rules.max_class', @platformId, @programId )
	
	END 
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '1004', @groupProcessInstanceId = @rootId
	SELECT @isVip = value_string FROM @values
	
	IF @class IS NULL OR @isVip IS NOT NULL
	BEGIN
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @makeModelId = value_int FROM @values
		
		SELECT @class = dc.textD, @type = d.argument2 FROM dbo.dictionary d with(nolock) 
		INNER JOIN dbo.dictionary dc with(nolock) ON TRY_PARSE(d.argument3 AS INT) = dc.value AND dc.typeD = 'makeModelClass'
		WHERE d.typeD = 'makeModel' AND d.value = @makeModelId
	END 
	
	select @classPoints = argument1 from dbo.dictionary with(nolock) where textD = @class and typeD = 'makeModelClass'

	IF @platformId = 25 -- LP
	BEGIN
		
		if @isVip IS NULL AND @classPoints > 70 AND isnull(@type,1) = 1
		BEGIN
			SET @class = 'D'
		END 
	END
	
	IF @programId IN (431,432,433,434,435,436,437,438,439)
	BEGIN
		IF @class = 'B'
		BEGIN
			SET @class = 'A'
		END 
		ELSE IF @class = 'C' OR @makeModelId IN (564,597,1845)
		BEGIN
			SET @class = 'B'
		END 
		ELSE IF @class = 'D' OR @makeModelId IN (565,570,589)
		BEGIN
			SET @class = dbo.f_translate('SUV MAŁY',default)			
		END
		ELSE IF @makeModelId = 599
		BEGIN
			SET @class = dbo.f_translate('SUV ŚREDNI',default)			
		END
	END 
	
	IF @class = '-1'
	BEGIN
		SET @class = '[TAKA SAMA JAK POJAZD KLIENTA]'
	END

END


