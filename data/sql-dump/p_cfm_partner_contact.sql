ALTER PROCEDURE [dbo].[p_cfm_partner_contact]
@groupProcessInstanceId int,
@type nvarchar(20) = dbo.f_translate('email',default),
@contact nvarchar(255) output
AS
begin
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @rootId int
	DECLARE @programId INT 
	DECLARE @platformId INT 
	DECLARE @eventType INT 
	
	SELECT @rootId = root_id FROM dbo.process_instance where id = @groupProcessInstanceId
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @rootId
	SELECT @eventType = value_int FROM @values
	
	if @platformId = 53
	BEGIN
		IF @type = dbo.f_translate('email',default)
		BEGIN
			SET @contact = 'serwis@alphabet.pl'
			IF @eventType IN (1,5,8)
			BEGIN
				SET @contact = 'szkody@alphabet.pl'
			END
		END 
	END
	if @platformId = 45
	BEGIN
		IF @type = dbo.f_translate('email',default)
		BEGIN
			SET @contact = 'assistance.pl@aldautomotive.com'
			IF @eventType IN (1,5,8)
			BEGIN
				SET @contact = 'assistance.pl@aldautomotive.com'
			END
		END 
	END
	
	IF ISNULL(@@SERVERNAME,'') <> dbo.f_translate('DELTASQL',default)
	BEGIN
		SET @contact = 'x'+@contact+'x'
	END 
	
end

