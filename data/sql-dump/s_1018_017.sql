ALTER PROCEDURE [dbo].[s_1018_017]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	IF @variant <>5
		BEGIN 
		SET @variant =5
	END 
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @driverEmail NVARCHAR(255)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @body NVARCHAR(4000)
	declare @sendEmail int

	SELECT @groupProcessInstanceId = group_process_id, @rootId=root_id FROM process_instance where id = @previousProcessId

	INSERT @values EXEC p_attribute_get2 @attributePath = '692', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @sendEmail = value_int FROM @values
	DELETE FROM @values
	
	if @sendEmail=1
	begin
		declare @regNumber nvarchar(100)

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @regNumber = value_string FROM @values

		declare @caseId nvarchar(100)
		set @caseId=dbo.f_caseId(@groupProcessInstanceId)
		
		declare @locationString nvarchar(1000)
		EXECUTE dbo.p_location_string 
			@attributePath='62,591,85'
			,@groupProcessInstanceId=@groupProcessInstanceId
			,@locationString=@locationString OUTPUT

		declare @subject nvarchar(100)
		set @subject=isnull(@regnumber,'')+ ' / '+isnull(@caseId,'')

		set @body='Dzień dobry,<br><br>Prosimy o uzyskanie zgodny na organizację transportu pojazdu po naprawie z serwisu do miejsca wskazanego przez Użytkownika: '+isnull(@locationString,'')+'.'
		DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('cfm')
	
		declare @to nvarchar(100)
		declare @cc nvarchar(100)

		declare @eventType int
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @eventType = value_string FROM @values

		/*	POBRANIE PlatformId Z ROOT'a!
  		____________________________________*/
		
		declare @platformId int
		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
		SELECT @platformId = value_int FROM @values

		--select * from dbo.platform where name like '%busin%'

		if @platformId=53
		begin
			if @eventType in (1,5)
			begin
				
				set @to = dbo.f_getRealEmailOrTest('szkody@alphabet.pl')
			end
			else
			begin
				set @to = dbo.f_getRealEmailOrTest('serwis@alphabet.pl')
			end
		
			set @cc = dbo.f_getRealEmailOrTest('serwis@alphabet.pl')
			
		end
		else if @platformId=43
		begin
			set @to = dbo.f_getRealEmailOrTest('assistance.pl@aldautomotive.com')
			set @cc = dbo.f_getRealEmailOrTest('cfm@starter24.pl')
		end
		else if @platformId=48
		begin
			if @eventType in (1,5)
			begin
				set @to = dbo.f_getRealEmailOrTest('szkody@businesslease.pl')
			end
			else
			begin
				set @to = dbo.f_getRealEmailOrTest('assistance@businesslease.pl')
			end
		
			set @cc = dbo.f_getRealEmailOrTest('cfm@starter24.pl')
		end
		ELSE IF @platformId = 25 -- LEASEPLAN
		BEGIN
			
			if @eventType in (1,5)
			begin
				set @to = dbo.f_getRealEmailOrTest('szkody@leaseplan.pl')
			end
			else
			begin
				set @to = dbo.f_getRealEmailOrTest('serwisyFA@leaseplan.pl')
			end
		
			set @cc = dbo.f_getRealEmailOrTest('teamlp@starter24.pl')
			
		END

		EXECUTE dbo.p_note_new 
		   @sender=@senderEmail
		  ,@groupProcessId=@groupProcessInstanceId
		  ,@type=dbo.f_translate('email',default)
		  ,@content=@subject
		  ,@emailBody=@body
		  ,@phoneNumber=null
		  ,@email=@to
		  ,@emailRegards=1
		  ,@userId=@currentUser
		  ,@subType=null
		  ,@attachment=null
		  ,@subject=@subject
		  ,@dw=@cc
		  ,@direction=1
		  ,@addInfo=null
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT

	end
	else
	begin
    IF @variant <> 5
      BEGIN
		set @variant=2
			END
	end
		 EXEC dbo.p_jump_to_service_panel @previousProcessId = @previousProcessId, @variant = @variant

	
END
