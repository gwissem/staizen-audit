ALTER PROCEDURE [dbo].[s_1021_300]
  (
    @previousProcessId INT,
    @variant           TINYINT OUTPUT,
    @currentUser       int, @errId int = 0 output
  )
AS
  BEGIN
    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))
    DECLARE @groupProcessInstanceId INT

    DECLARE @rootId INT
    DECLARE @monitId INT
    DECLARE @previousStepId NVARCHAR(255)
    DECLARE @postponeCount INT
    DECLARE @callerPhone NVARCHAR(255)
    --mail
    DECLARE @token NVARCHAR(36)
    DECLARE @err TINYINT
    DECLARE @message NVARCHAR(255)
    DECLARE @caseId NVARCHAR(255)
    --
    SET @variant = 99;

    DECLARE @rsUpdatedDate DATETIME
    DECLARE @rsCreatedDate DATETIME


    SELECT @groupProcessInstanceId = p.group_process_id,
           @rootId = p.root_id,
           @previousStepId = pp.step_id,
           @postponeCount = p.postpone_count,
           @monitId = p.id
    FROM process_instance p with(nolock)
           INNER JOIN dbo.process_instance pp with(nolock) ON pp.id = p.previous_process_instance_id
    WHERE p.id = @previousProcessId

    SELECT TOP 1 @rsUpdatedDate = updated_at, @rsCreatedDate = created_at
    from process_instance p with(nolock)
    where p.root_id = @rootId
      and step_id = '1021.003'
    order by p.created_at DESC

    DECLARE @acceptationOrRejectionControl int

    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath =  '129,808,801', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @acceptationOrRejectionControl = value_int from @values

    IF @rsUpdatedDate is null
      BEGIN
        SET @rsUpdatedDate = @rsCreatedDate
      end

    declare @vin varchar(50)
    declare @partnerEmail varchar(50)
    declare @partnerMailRS varchar(100)
    declare @regNumber varchar(20)
    declare @platformId int
    declare @platform nvarchar(100)

    INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int FROM @values

    delete from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platform = name from dbo.platform where id = (select top 1 value_int FROM @values)

    delete from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '129,800,368',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerEmail = value_string FROM @values


    delete from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @VIN = value_string FROM @values

    delete from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @regNumber = value_string FROM @values


    declare @makeModel nvarchar(100)
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @makeModel = textD
    from dbo.dictionary
    where value = (select top 1 value_int FROM @values)
      and typeD = 'makeModel'


    declare @estimatedFinalizaitionUpdateTime DATETIME
    declare @estimatedFinalizaitionUpdateId INT
    declare @estimatedFinalizaitionTime DATETIME

    INSERT @values EXEC p_attribute_get2 @attributePath = '129,130', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @estimatedFinalizaitionTime = value_date, @estimatedFinalizaitionUpdateId = id from @values

    SELECT @estimatedFinalizaitionUpdateTime = updated_at
    FROM attribute_value
    where id = @estimatedFinalizaitionUpdateId

    INSERT @values EXEC p_attribute_get2 @attributePath = '129,800,368',
                                         @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerEmail = value_string FROM @values
    declare @partnerLocId int
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerLocId = value_int FROM @values

    PRINT @partnerLocId;
    SELECT @partnerMailRS = TRIM(dbo.f_partner_contact(@groupProcessInstanceId,@partnerLocId,1,8))

    set @caseId = dbo.f_caseId(@groupProcessInstanceId)

    declare @subject nvarchar(300)

    SELECT TOP 1 @token = token
    FROM dbo.process_instance
    WHERE group_process_id = @groupProcessInstanceId
      AND token IS NOT NULL
      AND step_id like '%1021.003%'
    ORDER BY id

    DECLARE @url NVARCHAR(300) = [dbo].[f_hashTaskUrl](@token)

    set @subject =
    isnull(@makeModel, '') + ' / ' + isnull(@regNumber, '') + ' / ' + isnull(@vin, '') + dbo.f_translate(' / Sprawa nr ',default) +
    isnull(@caseId, '') + dbo.f_translate(' / Formularz raportu serwisowego ',default) + isnull(@platform, '')
    DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rs')
    DECLARE @emailBody NVARCHAR(MAX)
    declare @estimated int
    INSERT @values EXEC p_attribute_get2 @attributePath = '129,798', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @estimated = value_int FROM @values

    DECLARE @isActiveForm int =0


    SELECT top 1 @isActiveForm = 1 from process_instance with (nolock )
    where step_id = '1021.003'
                                                                    and active =1
                                                                    and group_process_id = @groupProcessInstanceId


    declare @hasBeenSent int

    INSERT @values EXEC p_attribute_get2 @attributePath = '412', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @hasBeenSent = value_int FROM @values

--     2h before attribute set
     	print dbo.f_translate('rsUpdatedDate',default)
     	PRINT cast(@rsUpdatedDate as nvarchar(30))
    
     	print dbo.f_translate('rsCreatedDate',default)
     	PRINT cast(@rsCreatedDate as nvarchar(30))
    
     	print dbo.f_translate('estimatedFinalizaitionUpdateTime',default)
     	PRINT cast(@estimatedFinalizaitionUpdateTime as nvarchar(30))
    
     	print dbo.f_translate('estimatedFinalizaitionTime',default)
     	PRINT cast(@estimatedFinalizaitionTime as nvarchar(30))

    DECLARE @hasTowingService int

    SET @hasTowingService = dbo.f_service_top_progress_group(@groupProcessInstanceId,1)

    if (isnull(@estimatedFinalizaitionTime, 0)) <> 0 AND isnull(@estimated, 0) = 1 AND isnull(@isActiveForm,0) = 1  and isnull(@hasTowingService,0) > 0
      BEGIN

        SET @emailBody = 'Szanowni Państwo, <BR>

Ostatnia zarejestrowana w sprawie szacowna data zakończenia naprawy to: ' +
                         isnull(cast(FORMAT(@estimatedFinalizaitionTime, dbo.f_translate('dd/MM/yyyy HH:mm',default)) AS nvarchar(50)), '') + '<br />

Prosimy o zaktualizowanie daty naprawy klikając w link <a href="' + @url + '">tutaj </a> <br />

Gdy data zakończenia naprawy jest znana przypominamy o odznaczeniu „haczyka” w polu Jeśli to data szacowna zaznacz tutaj. <br/>

<b>Brak informacji zwrotnej może skutkować przedłużeniem/organizacją świadczeń dodatkowych dla klienta na Państwa koszt</b> <br/>'

        IF isnull(@postponeCount,0) = 0  and isnull(@hasBeenSent, 0) = 0
          BEGIN
            EXEC p_attribute_edit
                @attributePath = '412',
                @stepId = 'xxx',
                @groupProcessInstanceId = @groupProcessInstanceId,
                @valueInt = 1,
                @err = @err OUTPUT ,
                @message = @message OUTPUT 

                EXEC p_note_new
                @groupProcessId = @groupProcessInstanceId,
                @type = dbo.f_translate('email',default),
                @content = dbo.f_translate('Wysłano monit 2h przed upłynięciem przewidywawnego czasu naprawy',default),
                @phoneNumber = NULL,
                @email = @partnerMailRS,
                @subject = @subject,
                @direction = 1,
                @emailRegards = 1,
                @err = @err OUTPUT,
                @message = @message OUTPUT,
                -- FOR EMAIL
                @dw = '',
                @udw = '',
                @sender = @senderEmail,
                @additionalAttachments = '',
                @emailBody = @emailBody
          end
        ELSE
          IF isnull(@postponeCount,0) >0
          BEGIN
            EXEC p_note_new
                @groupProcessId = @groupProcessInstanceId,
                @type = dbo.f_translate('email',default),
                @content = dbo.f_translate('Wysłano monit po upłynięciu przewidywawnego czasu naprawy',default),
                @phoneNumber = NULL,
                @email = @partnerMailRS,
                @subject = @subject,
                @direction = 1,
                @emailRegards = 1,
                @err = @err OUTPUT,
                @message = @message OUTPUT,
                -- FOR EMAIL
                @dw = '',
                @udw = '',
                @sender = @senderEmail,
                @additionalAttachments = '',
                @emailBody = @emailBody
          end
        
      


      END



--     JEŻELI DATA/ GODZINA SZACOWANE TO ODPALAMY MONIY:
    IF isnull(@estimated, 0) > 0 and (@acceptationOrRejectionControl is null ) and @isActiveForm = 1
      BEGIN
        Print '@estimatedFinalizaitionUpdateTime'
        Print @estimatedFinalizaitionUpdateTime
        Print '@rsUpdatedDate'
        Print @rsUpdatedDate

--         JEŻELI (OD OSTATNIEGO MONITU ZAKTUALIZOWANO) I  (2 godziny przed datą)  > teraz(Czyli w przyszłości)
        IF (
             (@estimatedFinalizaitionUpdateTime > @rsUpdatedDate)
--             OR (@estimatedFinalizaitionTime > @rsCreatedDate)
           )
           and
           DATEADD(HOUR , -2 ,@estimatedFinalizaitionTime) > GETDATE()
          BEGIN
            -- zaaktualizowane na datę przyszłą
            print '@previousProcessId'
            print @previousProcessId
            print '@monti'
            print @monitId

            -- zmieniony, ustawiamy timer
            UPDATE process_instance
            set postpone_count = 0,
                postpone_date  = DATEADD(HOUR, -2, @estimatedFinalizaitionTime)
            where id = @monitId

            EXEC p_attribute_edit
                @attributePath = '412',
                @stepId = 'xxx',
                @groupProcessInstanceId = @groupProcessInstanceId,
                @valueInt = 0,
                @err = @err OUTPUT ,
                @message = @message OUTPUT

          end
        ELSE
          -- nie aktualizowany później - 24h od teraz + zaaktualizowane na datę przeszłą
          UPDATE process_instance
          set postpone_count = postpone_count + 1,
              postpone_date  = DATEADD(HOUR, 24, GETDATE()),
              active         = 1
          where id = @monitId


      end
    ELSE
      BEGIN
        SET @variant = 1  --mamy podaną godzinę, nie potrzebujemy już monitorować
      end

  end