ALTER PROCEDURE [dbo].[p_send_email_to_PSA_outside_24h]
    @previousProcessId INT  -- Może być GROUP / ROOT
AS
  BEGIN

    DECLARE @err INT
    DECLARE @message NVARCHAR(255)

    DECLARE @groupProcessInstanceId INT
    DECLARE @rootId INT
    DECLARE @stepId NVARCHAR(255)
    DECLARE @body NVARCHAR(MAX)
    DECLARE @emailTo NVARCHAR(MAX)

    DECLARE @content NVARCHAR(MAX)
    DECLARE @title NVARCHAR(MAX)

    -- Pobranie podstawowych danych --
    SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
    FROM process_instance with(nolock)
    WHERE id = @previousProcessId

    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))

    DECLARE @symptoms nvarchar(400)

    DECLARE @diagnosisCode NVARCHAR(30)
    DECLARE @diagnosisDescription NVARCHAR(1000)
    SET @diagnosisDescription = dbo.f_diagnosis_description(@groupProcessInstanceId, 'en')

    SELECT @diagnosisCode = dbo.f_diagnosis_code(@groupProcessInstanceId)

    SELECT top 1  @symptoms =description from AtlasDB_def.dbo.psa_symptoms where arc_code = @diagnosisCode
    DECLARE @platformID int

    DECLARE @platformCode nvarchar(40)

    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformID = value_int FROM @values

    IF @platformID = 79 OR @platformID = 81 --citroen / ds
      BEGIN
        SET @platformCode = '389912C01'
      end
    IF @platformID = 80 --peugeot
      BEGIN
        SET @platformCode = dbo.f_translate('PL6666P01',default)
      end
    IF @platformID = 14 --opel
      BEGIN
        SET @platformCode = dbo.f_translate('0PLOPEL01',default)
      end


    DECLARE @caseid NVARCHAR(150)
    SELECT @caseid = dbo.f_caseId(@groupProcessInstanceId)
    SET @content = '' +
'BeginOfFile: True <br />
Assistance Code : '+isnull(@platformCode,'')+'<br />
Country Code : __COUNTRY_CODE__<br />
Task Id : __CASEID__<br />
Time of Incident :  __CASE_START_TIME__<br />
Make : __MAKE__ <br />
Model :  __MODEL__<br />
Energie : __FUEL__<br />
Date put on road : __FIRST_REGISTER_DATE__ <br />
VIN Number : {@74,71@}<br />
Reg No : {@74,72@}<br />
Customer effet : __SYMPTOMS__ <br />
Component fault : __DIAGNOSIS_DESCRIPTION__<br />
Tech Information :  __TECH_INFO__ __CANCEL_INFO__<br />
Date of Opening : __CASE_START_DATE__ <br />
Date of Response : __ACCEPTANCE_DATE__<br />
Time of Response : __ACCEPTANCE_HOUR__<br />
Breakdown Location : {@101,85,87@}<br />
Breakdown Area : __COUNTY_CODE__<br />
Breakdown Service : __CONTRACTOR_NAME__<br />
Service Tel Number 1 : <br />
Service Tel Number 2 : <br />
Patrol Address 1 : __CONTRACTOR_ZIP_CODE__<br />
Patrol Address 2 : <br />
Patrol Address V: __CONTRACTOR_LOCATION__<br />
User Name : <br />
User Tel Number : <br />
User Name P : <br />
Job Type : __FIXING_OR_TOWING__<br />
Dealer Address G : __PARTNER_NAME__<br />
Dealer Address 1 : __PARTNER_ZIP_CODE__ <br />
Dealer Address 2 : <br />
Dealer Address V : __PARTNER_LOCATION__<br />
Dealer Tel Number : __PARTNER_PHONE__ <br />' +
                   dbo.f_translate('EndOfFile: true',default)






    EXEC [dbo].P_parse_string @processInstanceId =@previousProcessId, @simpleText = @content,
                              @parsedText = @content OUTPUT




    DECLARE @COUNTRY_CODE NVARCHAR(2) = ''
    DECLARE @FUEL NVARCHAR(2) = ''
    DECLARE @CASE_START_DATE NVARCHAR(300) = ''
    DECLARE @CASE_START_TIME NVARCHAR(300) = ''
    DECLARE @COUNTY_CODE NVARCHAR(300) = ''
    DECLARE @CONTRACTOR_ZIP_CODE NVARCHAR(300) = ''
    DECLARE @CONTRACTOR_NAME NVARCHAR(300) = ''
    DECLARE @CONTRACTOR_LOCATION NVARCHAR(300) = ''
    DECLARE @FIXING_OR_TOWING NVARCHAR(300) = ''
    DECLARE @PARTNER_NAME NVARCHAR(300) = ''
    DECLARE @PARTNER_ZIP_CODE NVARCHAR(300) = ''
    DECLARE @PARTNER_LOCATION NVARCHAR(300) = ''
    DECLARE @PARTNER_PHONE NVARCHAR(300) = ''
    DECLARE @ACCEPTANCE_DATE NVARCHAR(300) = ''
    DECLARE @FIRST_REGISTER_DATE NVARCHAR(300) = ''
    DECLARE @ACCEPTANCE_HOUR NVARCHAR(300) = ''
    DECLARE @makeModelTextD NVARCHAR(130) = ''
    DECLARE @makeModelID int
    DECLARE @MAKE NVARCHAR(30) = ''
    DECLARE @MODEL NVARCHAR(100) = ''
    DECLARE @countryFull NVARCHAR(100) = ''
    DECLARE @fuelId int


    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @makeModelID = value_int from @values

    SELECT @makeModelTextD = textD
    from AtlasDB_def.dbo.dictionary
    where value = @makeModelID
      and typeD = 'makeModel'



    DECLARE @acceptanceFullDate DATETIME

    SELECT top 1 @acceptanceFullDate = date_enter from process_instance where root_id =  @rootId and step_id  = '1009.018'


    DECLARE @first_reg_date_full DATETIME
    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,233', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @first_reg_date_full = value_date from @values

    SET @FIRST_REGISTER_DATE = convert(nvarchar(300), @first_reg_date_full, 103)


    SET @ACCEPTANCE_DATE = convert(nvarchar(300), @acceptanceFullDate, 103)
    SET @ACCEPTANCE_HOUR = convert(nvarchar(5), @acceptanceFullDate, 108)

    SELECT @MAKE = SUBSTRING(@makeModelTextD, 1, CASE
                                                   WHEN CHARINDEX(' ', @makeModelTextD) > 0
                                                           THEN CHARINDEX(' ', @makeModelTextD) - 1
                                                   ELSE LEN(@makeModelTextD) END)
    DECLARE @MAKESHORT NVARCHAR(3)

    IF left(@MAKE,1) = 'P'
      BEGIN
        SET @MAKESHORT = dbo.f_translate('PEU',default)
      end
    ELSE IF left(@MAKE,1) ='C'
      BEGIN
        SET @MAKESHORT = dbo.f_translate('CIT',default)
      end
    ELSE
      BEGIN
        SET @MAKESHORT='OV'
      end





    SELECT @MODEL = SUBSTRING(@makeModelTextD, CHARINDEX(' ', @makeModelTextD) + 1, LEN(@makeModelTextD))


    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,120', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @fuelId = value_int from @values


    IF isnull(@fuelId,0) in (3, 5, 11)
      BEGIN
        SET @FUEL = 'D'
      END
    IF isnull(@fuelId,0) in (6,7,8) --hybryda
       BEGIN
        SET @FUEL = 'P'
        END
    IF isnull(@fuelId,0)  = 9 --hybryda
      BEGIN
        SET @FUEL = 'L'
      END

    IF isnull(@fuelId,0) in (2, 4, 10)
      BEGIN
        SET @FUEL = 'E'
      END

    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,86', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @countryFull = value_string from @values
    SELECT top 1 @COUNTRY_CODE = descriptionD
    from AtlasDB_def.dbo.dictionary
    where typeD = 'europeanCountry'
      and textD = @countryFull


    DECLARE @caseFirstDate datetime
    SELECT top 1 @caseFirstDate = created_at from process_instance where root_id = @rootId order by id asc


    SET @CASE_START_DATE = convert(nvarchar(300), @caseFirstDate, 103)
    SET @CASE_START_TIME = convert(nvarchar(5), @caseFirstDate, 108)

    DECLARE @county NVARCHAR(100)
    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath =  '101,85,88', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @county = value_string from @values

    SELECT @COUNTY_CODE = argument1 from AtlasDB_def.dbo.dictionary where typeD = 'county' and textD = @county


    DECLARE @towingOrFixingService INT
    print dbo.f_translate('fixitngOrTowingBool',default)
    DECLARE @fixitngOrTowingBool int
    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '271', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @fixitngOrTowingBool = value_int from @values

    IF isnull(@fixitngOrTowingBool, -1 ) =1
    BEGIN

        --         Towing
        SELECT @towingOrFixingService  = dbo.f_service_top_progress_group(@groupProcessInstanceId, 1)
      end
    ELSE
      begin
        --         FIXING
        SELECT @towingOrFixingService  = dbo.f_service_top_progress_group(@groupProcessInstanceId, 2)
      end



    DECLARE @contractorId int
    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @towingOrFixingService
    SELECT @contractorId = value_int from @values


    SELECT @CONTRACTOR_ZIP_CODE = dbo.f_PartnerLocation_PostalCode(@contractorId)
    SELECT @CONTRACTOR_LOCATION = dbo.f_partnerAdress(@contractorId)
    SELECT @CONTRACTOR_NAME = dbo.f_partnerName(@contractorId)

    print dbo.f_translate('XYZ',default)

    DECLARE @partnerId int
    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '522', @rootProcessInstanceId = @rootId
    SELECT @partnerId = value_int from @values

    IF @partnerId is not null
      BEGIN


        SELECT @PARTNER_ZIP_CODE = dbo.f_PartnerLocation_PostalCode(@partnerId)
        SELECT @PARTNER_LOCATION = dbo.f_partnerAdress(@partnerId)
        SELECT @PARTNER_NAME = DBO.f_partnerName(@partnerId)
        SELECT @PARTNER_PHONE = dbo.f_partnerPhoneNumber(@partnerId)
      end

     DECLARE @TECH_INFO nvarchar(200)


    DECLARE @monitServiceGroupID int

    SELECT top 1 @monitServiceGroupID = group_process_id  from process_instance where  root_id = @rootId and step_id ='1021.015'

     DELETE @values
     INSERT @values EXEC p_attribute_get2 @attributePath = '129,128', @groupProcessInstanceId = @monitServiceGroupID
     SELECT @TECH_INFO = value_text from @values



    DECLARE @CANCEL_INFO nvarchar(200)

    DECLARE @cancelationProgress int

    select top 1 @cancelationProgress = ssd.progress from dbo.service_status ss with(nolock)
                                              join dbo.service_status_dictionary ssd with(nolock) on ssd.id = ss.status_dictionary_id
    where ss.group_process_id = @groupProcessInstanceId


    IF @cancelationProgress = 5
      BEGIN
        SET @CANCEL_INFO = dbo.f_translate('mission cancelled',default)
      END
    ELSE
      BEGIN
        SET @CANCEL_INFO = ''
      end
    IF isnull(@CANCEL_INFO, '') <> ''
      BEGIN
        SET @TECH_INFO = ''
      end



    IF isnull(@fixitngOrTowingBool, -1 ) = 1
    BEGIN
        SET @FIXING_OR_TOWING = dbo.f_translate('REMO',default)
      end
    ELSE
      begin
        --         FIXING
        SET @FIXING_OR_TOWING = dbo.f_translate('DEPA',default)
      end


--         Initial diagnosis - do 50 znaków
--         Breakdown service - 50
--         Address of the breakdown service - 50
--         Placeo of Arrival (nazwa i adres) - maksymalnie po 50 znaków



    SET @content = REPLACE(@content, '__COUNTRY_CODE__', isnull(@COUNTRY_CODE, ''))
    SET @content = REPLACE(@content, '__CASEID__', isnull(@caseid, ''))
    SET @content = REPLACE(@content, '__FUEL__', isnull(@FUEL, ''))
    SET @content = REPLACE(@content, '__CASE_START_DATE__', isnull(@CASE_START_DATE, ''))
    SET @content = REPLACE(@content, '__CASE_START_TIME__', isnull(@CASE_START_TIME, ''))
    SET @content = REPLACE(@content, '__COUNTY_CODE__', isnull(@COUNTY_CODE, ''))
    SET @content = REPLACE(@content, '__CONTRACTOR_NAME__', left(isnull(@CONTRACTOR_NAME, ''),50))
    SET @content = REPLACE(@content, '__CONTRACTOR_ZIP_CODE__', isnull(@CONTRACTOR_ZIP_CODE, ''))
    SET @content = REPLACE(@content, '__CONTRACTOR_LOCATION__', left(isnull(@CONTRACTOR_LOCATION, ''),50))
    SET @content = REPLACE(@content, '__FIXING_OR_TOWING__', isnull(@FIXING_OR_TOWING, ''))
    SET @content = REPLACE(@content, '__PARTNER_NAME__', left(isnull(@PARTNER_NAME, ''),50))
    SET @content = REPLACE(@content, '__PARTNER_ZIP_CODE__', left(isnull(@PARTNER_ZIP_CODE, ''),50))
    SET @content = REPLACE(@content, '__CANCEL_INFO__', left(isnull(@CANCEL_INFO, ''),150))
    SET @content = REPLACE(@content, '__TECH_INFO__', left(isnull(@TECH_INFO, ''),150))
    SET @content = REPLACE(@content, '__PARTNER_LOCATION__', left(isnull(@PARTNER_LOCATION, ''),50))
    SET @content = REPLACE(@content, '__PARTNER_PHONE__', isnull(@PARTNER_PHONE, ''))
    SET @content = REPLACE(@content, '__MAKE__', isnull(@MAKESHORT, ''))
    SET @content = REPLACE(@content, '__MODEL__', isnull(@MODEL, ''))
    SET @content = REPLACE(@content, '__ACCEPTANCE_DATE__', isnull(@ACCEPTANCE_DATE, ''))
    SET @content = REPLACE(@content, '__FIRST_REGISTER_DATE__', isnull(@FIRST_REGISTER_DATE, ''))
    SET @content = REPLACE(@content, '__ACCEPTANCE_HOUR__', isnull(@ACCEPTANCE_HOUR, ''))
    SET @content = REPLACE(@content, '__SYMPTOMS__', isnull(@symptoms, ''))
    SET @content = REPLACE(@content, '__DIAGNOSIS_DESCRIPTION__', left(isnull(@diagnosisDescription, ''),50))








    DECLARE @email VARCHAR(400)



    SET @email = 'psa-depil@mpsa.com'
    select @email = dbo.f_getRealEmailOrTest(@email)


    DECLARE @sender NVARCHAR(400)
    DECLARE @contentNote NVARCHAR(1000) = [dbo].[f_showEmailInfo](@email) +
                                          dbo.f_translate(' - Powiadomienie do PSA po 24h  od aktywowania holowania / naprawy.',default)




    DECLARE @registerNumber nvarchar(100)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @registerNumber = value_string FROM @values


    DECLARE @vin nvarchar(100)
    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @vin = value_string FROM @values

    SET @title = dbo.f_translate('Code dossier ',default) + isnull(@registerNumber, '')  +' / ' + isnull(@vin,0)






    SET @sender = dbo.f_getEmail('callcenter')


    EXECUTE dbo.p_note_new
        @groupProcessId = @groupProcessInstanceId
        , @type = dbo.f_translate('email',default)
        , @content = @contentNote
        , @email = @email
        , @userId = 1  -- automat
        , @subject = @title
        , @direction = 1
        , @sender = @sender
        , @emailBody = @content
        , @err = @err OUTPUT
        , @message = @message OUTPUT
        , @emailRegards = 0

  END