ALTER PROCEDURE [dbo].[s_1007_062]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @content NVARCHAR(4000)
	DECLARE @postponeCount INT
	DECLARE @postponeLimit INT
	DECLARE @programId NVARCHAR(255)
	DECLARE @programNotFoundStatus INT
	DECLARE @verification INT
	DECLARE @daysCount INT
	DECLARE @daysLimit DECIMAL
	DECLARE @fixingCarNotReadyByDate DATETIME
	DECLARE @id INT
	DECLARE @rentalDuration INT
	DECLARE @platformName NVARCHAR(250)
	DECLARE @fixingEndDate DATETIME
	DECLARE @partnerId INT
	DECLARE @fixingPartnerId INT
	DECLARE @eta DATETIME
	DECLARE @daysLeft INT
	DECLARE @endDate DATETIME
	DECLARE @newDate DATETIME
	DECLARE @duration INT
	DECLARE @clientPhone NVARCHAR(255)
	DECLARE @platformId INT
	DECLARE @p1 NVARCHAR(100)
	DECLARE @rentalDurationAfter INT
	DECLARE @rootId INT
	DECLARE @platformGroup NVARCHAR(255)
	DECLARE @days INT
	DECLARE @workingDaysOnly INT 
	
	SELECT	@rootId = p.root_id, @groupProcessInstanceId = p.group_process_id, @postponeCount = p.postpone_count, @postponeLimit = s.postpone_count		
	FROM process_instance p with(nolock)
	INNER JOIN dbo.step s with(nolock) ON p.step_id = s.id  
	WHERE p.id = @previousProcessId 
	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	CREATE TABLE #replacecmentCarSummary (duration INT, real_duration INT, days_left INT, end_date DATETIME)
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformName = (SELECT name FROM platform WHERE id = (SELECT value_int FROM @values))
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '573', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @verification = value_int FROM @values
	
--	DELETE FROM @values
--	INSERT @values EXEC p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @groupProcessInstanceId
--	SELECT @fixingCarNotReadyByDate = value_date FROM @values
	
	exec [dbo].[p_fixing_end_date] 
	@rootId = @rootId, 
	@fixingEndDate = @fixingCarNotReadyByDate output

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '130', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingEndDate = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @rootProcessInstanceId = @rootId
	SELECT @fixingPartnerId = value_int FROM @values
	
	EXEC p_replacement_car_summary
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @daysLeft = days_left, @endDate = end_date FROM #replacecmentCarSummary	
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '789,786,240,153', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @rentalDuration = value_int FROM @values
	
	PRINT '---rental'
	PRINT @rentalDuration
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @clientPhone = value_string FROM @values

	EXEC p_replacement_car_calculate2
	@groupProcessInstanceId = @groupProcessInstanceId,
	@extend = 1,
	@skipLimitCalculation = 1
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '789,786,240,153', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @rentalDurationAfter = value_int FROM @values
	
	DELETE FROM #replacecmentCarSummary
	
--	EXEC p_replacement_car_summary
--	@groupProcessInstanceId = @groupProcessInstanceId
--	SELECT @daysLeft = days_left, @endDate = end_date FROM #replacecmentCarSummary
--	
	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
		SELECT @programId = value_string FROM @values
		
		SELECT @platformId = platform_id from dbo.vin_program with(nolock) where id = @programId
		EXEC [dbo].[p_rental_start_and_duration] @groupProcessInstanceId = @groupProcessInstanceId, @rentalDuration = @rentalDurationAfter OUTPUT, @rentalStartDate = @eta OUTPUT		
	END 
	
	EXEC [dbo].[p_service_day_limit] 
		@groupProcessInstanceId = @groupProcessInstanceId, 
		@serviceId = 3,
		@programId = @programId,
		@max = 1,
		@days = @days OUTPUT,
		@workingDaysOnly = @workingDaysOnly OUTPUT
		
	DECLARE @calendarDays INT = @days
	
	IF ISNULL(@workingDaysOnly,0) = 1
	BEGIN			
		SELECT @calendarDays = [dbo].[f_working_to_calendar_days](@days ,@eta)
	END 
	
	SET @endDate = DATEADD(day, @rentalDurationAfter, @eta)
	SET @content = dbo.f_translate('Uprzejmie informujemy, że w związku z trwającą naprawą warsztatową przedłużyliśmy wynajem samochodu zastępczego do: ',default)+dbo.FN_VDateHour(@endDate)+'.'
	
	if @rentalDurationAfter >= @calendarDays 
	BEGIN
		IF @platformGroup = dbo.f_translate('PSA',default)
		BEGIN
			SET @content = @content + dbo.f_translate(' W celu uzyskania informacji na temat przedłużenia wynajmu dla naprawy trwającej powyżej tego okresu prosimy o kontakt z serwisem wykonującym naprawę. Przed upływem ostatniej doby prosimy o skontaktowanie się z wypożyczalnią i zwrot pojazd',default)	
		END 
		ELSE
		BEGIN
		SET @content = @content + dbo.f_translate(' Jest to maksymalny okres wynajmu w ramach assistance. Przed upływem ostatniej doby prosimy o skontaktowanie się z wypożyczalnią i zwrot pojazdu zastępczego.',default)		
	END 
	
	END 
	
	
	SET @variant = 1
	
	-- sms do klienta	
	
	EXEC dbo.p_note_new
	@groupProcessId = @groupProcessInstanceId,
	@type = dbo.f_translate('sms',default),
	@content = @content,
	@phoneNumber = @clientPhone,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	-- 			Username added ( używane w ADAC)
	DECLARE @currentUserName nvarchar(150)
	SELECT @currentUserName = ( isnull(firstname,'') +' '+ isnull(lastname,'')) from fos_user where id = @currentUser
	-- gop
	EXEC p_send_gop
	@groupProcessInstanceId = @groupProcessInstanceId, @userName = @currentUserName
		
	-- LOGGER START
	SET @p1 = dbo.f_caseId(@groupProcessInstanceId)
	EXEC dbo.p_log_automat
	@name = 'rental_automat',
	@content = dbo.f_translate('Wysłanie GOP',default),
	@param1 = @p1
	-- LOGGER END

	DROP TABLE #replacecmentCarSummary	
	
	-- RS
	
	EXEC [dbo].[p_attribute_edit]
	@attributePath = '855', 
	@groupProcessInstanceId = @rootId,
	@stepId = 'xxx',
	@userId = 1,
	@originalUserId = 1,
	@valueInt = 1,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	
	DECLARE @producerProgram int 
	
	SELECT @producerProgram = dbo.f_producer_program(@programId)
	
	IF @producerProgram is not NULL
	BEGIN
		EXEC s_RS @processInstanceId = @rootId	
	END
	
	-- powiadomienia o przedłużeniu auta zastępczego: CFM
		
	declare @workshopGroup int 
	declare @fixingPartnerEmail nvarchar(255)
	declare @canSwap int 
	declare @locationType nvarchar(255) 
	
	SELECT @workshopGroup = dbo.f_service_top_progress_group(@groupProcessInstanceId, 12)
	select @fixingPartnerEmail = dbo.f_partner_contact(@workshopGroup, @fixingPartnerId, 3, 4)
	
	EXEC [dbo].[p_can_workshop_swap_rental]
	@groupProcessInstanceId = @workshopGroup,
	@canSwap = @canSwap OUTPUT

	IF @platformId = 48
	BEGIN
		
		SELECT	@locationType=avLocType.value_text 
		from	attribute_value avService with(nolock) inner join
				attribute_value avLocType with(nolock) on avLocType.parent_attribute_value_id=avService.id and avLocType.attribute_path='595,597,596'
		where	avService.id=@fixingPartnerId 
		and 	avService.attribute_path='595,597'



		IF dbo.f_exists_in_split(@locationType,'190') = 1 AND @rentalDurationAfter > 3
		BEGIN
			EXEC [dbo].[P_send_email_to_business_lease_new_mv] @previousProcessId = @previousProcessId, @emailTo = 'assistance@businesslease.pl', @extension = 1
		END
		ELSE IF dbo.f_exists_in_split(@locationType,'189') = 1
		BEGIN
			EXEC [dbo].[P_send_email_to_business_lease_new_mv] @previousProcessId = @previousProcessId, @emailTo = 'bl@motoflota.pl', @extension = 1
		END
	 
		
	END


  IF @platformGroup like dbo.f_translate('PSA',default)
    BEGIN

			DECLARE @partnerLocationId int
			DELETE FROM @values
			-- 	@todo: zmienić na serwis holowania?
			INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '522', @rootProcessInstanceId = @rootId
			SELECT @partnerLocationId = value_int FROM @values


			DECLARE @isASO int = 0

			SET @isASO = (
									 SELECT 1
										from dbo.attribute_value avRodzajPartnera with(nolock)
										where avRodzajPartnera.attribute_path = '595,597,596'
											and avRodzajPartnera.parent_attribute_value_id = @partnerLocationId
											and (SELECT top 1 1
													 from dbo.f_split(value_string, ',')
													 where data in (22, 43, 192, 193)) is not null
									 )

			print '@isASO'
			print @isASO
			print '@partnerLocationId'
			print @partnerLocationId
			IF @isASO = 1
				BEGIN
      		EXEC p_send_email_to_PSA_ASO_MW_changed @previousProcessId = @groupProcessInstanceId
				END
    END

	
END