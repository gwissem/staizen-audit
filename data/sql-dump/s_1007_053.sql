ALTER PROCEDURE [dbo].[s_1007_053]
( 
	@previousProcessId INT,
	@checkDate DATETIME = null,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @postponeCount INT
	DECLARE @postponeLimit INT
	DECLARE @programId NVARCHAR(255)
	DECLARE @programNotFoundStatus INT
	DECLARE @verification INT
	DECLARE @daysCount INT
	DECLARE @daysLimit DECIMAL
	DECLARE @fixingCarNotReadyByDate DATETIME
	DECLARE @id INT
	DECLARE @rentalDuration INT
	DECLARE @platformName NVARCHAR(250)
	DECLARE @fixingEndDate DATETIME
	DECLARE @partnerId INT
	DECLARE @fixingPartnerId INT
	DECLARE @eta DATETIME
	DECLARE @daysLeft INT
	DECLARE @endDate DATETIME
	DECLARE @newDate DATETIME
	DECLARE @duration INT
	DECLARE @rootId INT
	DECLARE @p1 NVARCHAR(100)
	DECLARE @p2 NVARCHAR(100)
	DECLARE @p3 NVARCHAR(100)
	DECLARE @currentDate DATETIME 
	DECLARE @logContent NVARCHAR(MAX)
	declare @override int
	declare @estimated int
	declare @joiningProgramsAllowed INT
	declare @supervisorAcceptedJoningPrograms INT
	declare @realDuration int
	DECLARE @hoursTillFix INT
	DECLARE @fixedRentalDuration INT
	declare @platformId int
	DECLARE @programIds NVARCHAR(255)
	DECLARE @platformGroup nvarchar(255)
	DECLARE @isProducerProgram int 
	declare @nextProcessInstanceId int 
	declare @checkFixingDate int 
	declare @rootProgramId int 
	declare @rootProgramDayLimit int
	DECLARE @createdAt datetime 
	declare @rootPlatformId int 
	declare @extraAuthorization int 
	
	SELECT	@groupProcessInstanceId = p.group_process_id, @postponeCount = p.postpone_count, @postponeLimit = s.postpone_count, @rootId = p.root_id, @createdAt = p.created_at 	
	FROM process_instance p with(nolock)
	INNER JOIN dbo.step s with(nolock) ON p.step_id = s.id  
	WHERE p.id = @previousProcessId 
	
	IF EXISTS (
		select id from dbo.process_instance where step_id IN ('1007.055','1007.060','1007.061') and active = 1 and group_process_id = @groupProcessInstanceId 
	)
	BEGIN
		SET @variant = 0 
		RETURN 
	END 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @rootId
	SELECT @rootPlatformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '798', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @estimated = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
	SELECT @rootProgramId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformName = (SELECT name FROM platform WHERE id = (SELECT value_int FROM @values))
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '573', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @verification = value_int FROM @values
	
--	DELETE FROM @values
--	INSERT @values EXEC p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @groupProcessInstanceId
--	SELECT @fixingCarNotReadyByDate = value_date FROM @values
--	
	EXEC [dbo].[p_fixing_end_date] 
	@rootId = @rootId, 
	@fixingEndDate = @fixingCarNotReadyByDate output
	
	EXEC [p_platform_group_name]
	@groupProcessInstanceId = @groupProcessInstanceId,
	@name = @platformGroup OUTPUT
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values


	-- 			Username added ( używane w ADAC)
	DECLARE @currentUserName nvarchar(150)
	SELECT @currentUserName = ( isnull(firstname,'') +' '+ isnull(lastname,'')) from fos_user where id = @currentUser
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingPartnerId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '858', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @override = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '204', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programIds = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '985,161', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @checkFixingDate = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '168,153', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @extraAuthorization = value_int FROM @values
	
	CREATE TABLE #replacecmentCarSummary (duration INT, real_duration INT, days_left INT, end_date DATETIME)
	EXEC p_replacement_car_summary
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @realDuration = real_duration, @daysLeft = days_left, @endDate = end_date FROM #replacecmentCarSummary
		
	DROP TABLE #replacecmentCarSummary
	
	DECLARE @autoPostponeDate datetime = @endDate
	
	IF isnull(@autoPostponeDate,getdate()) <= GETDATE()
	BEGIN
		IF @daysLeft > 100 -- pakiety dod końca naprawy warsztatowej 
		BEGIN
			SET @autoPostponeDate = DATEADD(month, 1, isnull(@createdAt,getdate()))
		END 
		ELSE 
		BEGIN
			SET @autoPostponeDate = DATEADD(minute, 30, getdate())
		END 
	END
	
	PRINT '----------------------------autoPostponeDate'
	PRINT @autoPostponeDate
	
	SELECT @hoursTillFix = DATEDIFF(minute, @eta, ISNULL(@fixingCarNotReadyByDate,GETDATE()))
	SELECT @fixedRentalDuration = CEILING(CAST(@hoursTillFix AS FLOAT)/1440)
		
	
	IF @variant = 10 -- ZAMKNIĘCIE PRZEZ WYPOŻYCZALNIĘ/PODMIANA
	BEGIN
		
		IF @fixingCarNotReadyByDate > GETDATE()
		BEGIN			
			SELECT @hoursTillFix = DATEDIFF(minute, @eta, GETDATE())
			SELECT @fixedRentalDuration = CEILING(CAST(@hoursTillFix AS FLOAT)/1440)		
		END 
		
		SET @fixedRentalDuration = IIF(@fixedRentalDuration > 0, @fixedRentalDuration, 0)
		
		EXEC [dbo].[p_attribute_edit]
	      @attributePath = '789,786,240,153', 
	      @groupProcessInstanceId = @groupProcessInstanceId,
	      @stepId = 'xxx',
	      @userId = 1,
	      @originalUserId = 1,
	      @valueInt = @fixedRentalDuration,
	      @err = @err OUTPUT,
	      @message = @message OUTPUT
		
  		-- przelicz stawki
		EXEC [dbo].[p_replacement_car_calculate2]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@extend = 2,
		@forceDuration = @fixedRentalDuration

		IF @platformGroup = dbo.f_translate('CFM',default)
		BEGIN
			EXEC p_create_summary_rental_process @groupProcessInstanceId = @groupProcessInstanceId, @currentUser = @currentUser, @calculateDuration = 1, @sendGop = 1
			SET @variant = 0
		END 
		
		RETURN
	END 
	
	IF @variant = 20 -- ZAMKNIĘCIE PRZEZ VIEWER
	BEGIN
		
		IF @fixingCarNotReadyByDate > GETDATE()
		BEGIN			
			SELECT @hoursTillFix = DATEDIFF(minute, @eta, GETDATE())
			SELECT @fixedRentalDuration = CEILING(CAST(@hoursTillFix AS FLOAT)/1440)		
		END 
		
		SET @fixedRentalDuration = IIF(@fixedRentalDuration > 0, @fixedRentalDuration, 0)
		
		EXEC [dbo].[p_attribute_edit]
	      @attributePath = '789,786,240,153', 
	      @groupProcessInstanceId = @groupProcessInstanceId,
	      @stepId = 'xxx',
	      @userId = 1,
	      @originalUserId = 1,
	      @valueInt = @fixedRentalDuration,
	      @err = @err OUTPUT,
	      @message = @message OUTPUT
		
  		-- przelicz stawki
		EXEC [dbo].[p_replacement_car_calculate2]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@extend = 2,
		@forceDuration = @fixedRentalDuration

		DECLARE @username nvarchar(255)
		
		select @username = username from dbo.fos_user where id = @currentUser 
		SET @content = dbo.f_translate('Klient CFM wygenerował zadanie na zakończenie wynajmu za pomocą Viewera ',default)+isnull('('+@username+', '+dbo.FN_VDateHour(getdate())+')','('+dbo.FN_VDateHour(getdate())+')')
		
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '672', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueText = @content,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
		EXEC dbo.p_note_new @groupProcessId = @groupProcessInstanceId
		, @type = dbo.f_translate('system',default)
		, @content = @content
		, @err = @err OUTPUT
		, @message = @message OUTPUT

		RETURN
	END 
	
	DECLARE @allowJoin INT
	EXEC dbo.p_can_join_programs @groupProcessInstanceId = @groupProcessInstanceId, @allowJoin = @allowJoin output
	
	if @allowJoin = 1 AND @platformGroup <> dbo.f_translate('CFM',default)
	begin		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '900', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @supervisorAcceptedJoningPrograms = value_int FROM @values
		
		
		IF @supervisorAcceptedJoningPrograms IS NULL
		AND NOT EXISTS(SELECT id from dbo.process_instance where group_process_id = @groupProcessInstanceId and step_id = '1007.072' and active = 1)
		AND @fixedRentalDuration > @realDuration
		AND dbo.f_programs_joinable(@programIds, @programId) = 1
		BEGIN			
			DECLARE @processInstanceId int
			
			EXEC [dbo].[p_process_new] 
			@stepId = '1007.072', 
			@userId = 1, 
			@originalUserId = 1,   
			@parentProcessId = @rootId, 
			@rootId = @rootId,
			@groupProcessId = @groupProcessInstanceId,
			@err = @err OUTPUT, 
			@message = @message OUTPUT, 
			@processInstanceId = @processInstanceId OUTPUT			
		END 
		
	end 
	
	PRINT '--fixingEnd'
	PRINT @fixingCarNotReadyByDate
	
	PRINT '--endDate'
	PRINT @endDate
	
	PRINT '--daysLeft'
	PRINT @daysLeft
	
	SET @currentDate = IIF(@checkDate IS NOT NULL, @checkDate, GETDATE())
	
	DECLARE @allowExtend INT = 1
	DECLARE @forceDuration INT 
	
	-- sprawdzenie czy negatywnie zweryfikowane BWB, lub max limitu z hotelem dla ford	
	EXEC [dbo].[p_service_allow_extend] 
	@groupProcessInstanceId = @groupProcessInstanceId, 
	@serviceId = 3,
	@allow = @allowExtend OUTPUT,
	@serviceLimit = @forceDuration OUTPUT

  
	IF @allowExtend = 0 AND @endDate < @currentDate
	BEGIN
		
		IF @platformGroup = dbo.f_translate('CFM',default)
		BEGIN
			SELECT @isProducerProgram = is_producer from dbo.vin_program where id = @programId
			
			IF @isProducerProgram = 1 AND (ISNULL(@daysLeft,999) <= 0)
			BEGIN
				SET @variant = 6
				
				IF @rootPlatformId = 78
				BEGIN
					
					IF ISNULL(@extraAuthorization,0) > 0 
					BEGIN
						SET @variant = 5
					END 
					ELSE
					BEGIN
						SET @variant = 0	
						EXEC p_create_summary_rental_process @groupProcessInstanceId = @groupProcessInstanceId, @currentUser = @currentUser, @sendGop = 1
					END						
				END
			END
			
			RETURN
		END 
		
		-- zakończenie
		SET @variant = 1
		
		-- przelicz (bo wynajem mógł się skończyć wcześniej)
		EXEC [dbo].[p_replacement_car_calculate2]
		@groupProcessInstanceId = @groupProcessInstanceId,
		@extend = 2,
		@forceDuration = @forceDuration
		
		SET @logContent = dbo.f_translate('Automatyczne zakończenie wynajmu pojazdu zastępczego.',default)


		EXEC p_send_gop @groupProcessInstanceId = @groupProcessInstanceId, @userName = @currentUserName
		
		RETURN 
	END
	ELSE IF isnull(@override,0) IN (1,2) 
	BEGIN
		if @override = 1
		BEGIN
			SET @logContent = dbo.f_translate('Wymuszone zakończenie wynajmu pojazdu zastępczego.',default)
			
			IF @platformGroup = dbo.f_translate('CFM',default)
			BEGIN
				EXEC p_create_summary_rental_process @groupProcessInstanceId = @groupProcessInstanceId, @currentUser = @currentUser, @forceEnd = 1, @nextProcessInstanceId = @nextProcessInstanceId output
				insert into dbo.process_instance_flow (previous_process_instance_id, next_process_instance_id, active)
				select @previousProcessId, @nextProcessInstanceId, 1
				
				RETURN
			END 
			ELSE
			BEGIN
				set @variant = 4	
			END
		END 
		else if @override = 2
		BEGIN
			SET @logContent = dbo.f_translate('Wymuszone przedłużenie wynajmu pojazdu zastępczego.',default)
			set @variant = 2
			
			EXEC p_attribute_edit
   			@attributePath = '858', 
   			@groupProcessInstanceId = @groupProcessInstanceId,
   			@stepId = 'xxx',
   			@valueInt = NULL ,
   			@err = @err OUTPUT,
   			@message = @message OUTPUT
   			
		END
	END 
	ELSE
	BEGIN		

		IF (@fixingCarNotReadyByDate IS NOT NULL AND ISNULL(@checkFixingDate,1) = 1 AND isnull(@estimated,0) = 0 /* AND @fixingCarNotReadyByDate < @currentDate */ ) OR (ISNULL(@daysLeft,999) <= 0 AND @endDate <= @currentDate)  
		BEGIN
			-- automatyczne przedłużenie dla producenckigo, jak nie:  zad. dla konsultanta
			IF @platformGroup = dbo.f_translate('CFM',default)
			BEGIN
		
				SET @variant = 6
				
				SELECT @isProducerProgram = isnull(is_producer,0) from dbo.vin_program with(nolock) where id = @programId
			
				IF (ISNULL(@daysLeft,999) <= 0 AND @endDate <= @currentDate)
				BEGIN			
					IF @isProducerProgram = 1
					BEGIN
						exec [dbo].[p_service_day_limit] 
						@groupProcessInstanceId = @groupProcessInstanceId, 
						@serviceId = 3,
						@programId = @rootProgramId,
						@days = @rootProgramDayLimit OUTPUT
						
						
					      
						
						IF @rootProgramDayLimit > 0 
						BEGIN
							SET @variant = 5
						END 
					END
				END
				
				/** CAREFLEET **/
				IF @rootPlatformId = 78
				BEGIN
					
					
					IF @variant = 6
					BEGIN						
						IF ISNULL(@extraAuthorization,0) > 0 
						BEGIN
							SET @variant = 5
						END 
						ELSE
						BEGIN
							SET @variant = 0	
							EXEC p_create_summary_rental_process @groupProcessInstanceId = @groupProcessInstanceId, @currentUser = @currentUser, @sendGop = 1
						END						
					END
					
				END 
				
				-- przelicz (bo wynajem mógł się skończyć wcześniej)
				EXEC [dbo].[p_replacement_car_calculate2]
				@groupProcessInstanceId = @groupProcessInstanceId,
				@extend = 2
				
				RETURN
			END 
			
			-- zakończenie
			SET @variant = 1
			
			-- przelicz (bo wynajem mógł się skończyć wcześniej)
			EXEC [dbo].[p_replacement_car_calculate2]
			@groupProcessInstanceId = @groupProcessInstanceId,
			@extend = 2
			
			
			SET @logContent = dbo.f_translate('Automatyczne zakończenie wynajmu pojazdu zastępczego.',default)


			EXEC p_send_gop @groupProcessInstanceId = @groupProcessInstanceId, @userName = @currentUserName
		 
		END 
		ELSE IF ISNULL(@fixingCarNotReadyByDate,@currentDate) > @currentDate
		BEGIN		
			
			
			DELETE FROM @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '789,786,240,153', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @rentalDuration = value_int FROM @values
		
			PRINT '@fixedRentalDuration'
			PRINT @fixedRentalDuration
			
			PRINT '@rentalDuration'
			PRINT @rentalDuration
			
			PRINT '@fixingCarNotReadyByDate'
			PRINT @fixingCarNotReadyByDate
			
			PRINT '@daysLeft'
			PRINT @daysLeft
				
			PRINT 'DATEDIFF(HOUR,@currentDate,@fixingCarNotReadyByDate)'
			PRINT DATEDIFF(HOUR,@currentDate,@fixingCarNotReadyByDate)
			
			IF  @fixingCarNotReadyByDate IS NOT NULL AND ISNULL(@checkFixingDate,1) = 1
			AND (
				(ISNULL(@fixedRentalDuration,0) > ISNULL(@rentalDuration,999) AND @daysLeft > 0) 
				OR 
				(ISNULL(@fixedRentalDuration,0) < ISNULL(@rentalDuration,999) AND DATEDIFF(HOUR,@currentDate,@fixingCarNotReadyByDate) > 24)				
			) 			
			BEGIN
--				
				PRINT '---------------------------------------- dEBUG ------------'
				
				PRINT '@fixedRentalDuration'
				PRINT @fixedRentalDuration
				
				PRINT '@rentalDuration'
				PRINT @rentalDuration
				
				PRINT '@fixingCarNotReadyByDate'
				PRINT @fixingCarNotReadyByDate
			
--				IF SYSTEM_USER = dbo.f_translate('andrzej.dziekonski',default)
--				BEGIN
--				  	select @eta, @fixingCarNotReadyByDate, @rentalDuration, @fixedRentalDuration, @daysLeft
--				END
				
				
				SET @logContent = dbo.f_translate('Automatyczne przedłużenie wynajmu pojazdu zastępczego. Zmieniono datę naprawy warsztatowej.',default)
				SET @p2 = CAST(ISNULL(@fixedRentalDuration,0) AS NVARCHAR(100))
				SET @p3 = CAST(ISNULL(@rentalDuration,0) AS NVARCHAR(100))
				
				DECLARE @newRentalDaysValue INT 
				
				SET @newRentalDaysValue = IIF(@fixedRentalDuration > @rentalDuration + @daysLeft, @rentalDuration + @daysLeft, @fixedRentalDuration) 
				
			    SET @newRentalDaysValue = IIF(@newRentalDaysValue > 0, @newRentalDaysValue, 0)
				
				EXEC [dbo].[p_attribute_edit]
			      @attributePath = '789,786,240,153', 
			      @groupProcessInstanceId = @groupProcessInstanceId,
			      @stepId = 'xxx',
			      @userId = 1,
			      @originalUserId = 1,
			      @valueInt = @newRentalDaysValue,
			      @err = @err OUTPUT,
			      @message = @message OUTPUT
			      
				SET @variant = 2
			END	
			ELSE
			BEGIN
				
				SET @variant = 99
				UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = @autoPostponeDate where id = @previousProcessId	
			END 		
		END
		ELSE IF @fixingCarNotReadyByDate IS NOT NULL AND ISNULL(@checkFixingDate,1) = 1 AND @fixingCarNotReadyByDate > @currentDate AND @endDate < @currentDate AND @daysLeft > 0
		BEGIN
			
			-- przedłużenie
			SET @logContent = dbo.f_translate('Automatyczne przedłużenie wynajmu pojazdu zastępczego. Minął czas szacowanej naprawy warsztatowej a nie osiągnięto limitu',default)
			SET @variant = 99
			
		END 
		ELSE IF @fixingCarNotReadyByDate IS NULL AND ISNULL(@checkFixingDate,1) = 1 AND @endDate < @currentDate AND NOT EXISTS (SELECT id FROM dbo.process_instance where group_process_id = @groupProcessInstanceId and step_id = '1007.056')
		BEGIN
			
			-- brak uzupełnienia rs
			SET @logContent = dbo.f_translate('Monit do serwisu o uzupełnienie daty naprawy warsztatowej w celu przedłużenia/zakończenia wynajmu pojazdu zastępczego.',default)
			SET @variant = 3
		END
		ELSE
		BEGIN
			SET @variant = 99
			UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = @autoPostponeDate where id = @previousProcessId
		END 
	END 
	
	EXEC dbo.p_change_priority @priority = 1, @type = 'rental_fixing_date', @groupProcessInstanceId = @groupProcessInstanceId
	
	IF ISNULL(@logContent,'') <> ''
	BEGIN
		SET @p1 = dbo.f_caseId(@rootId)
		EXEC dbo.p_log_automat
		@name = 'rental_automat',
		@content = @logContent,
		@param1 = @p1,
		@param2 = @p2,
		@param3 = @p3
	END
	
	--- czy jutro nie wskoczy zadanie na manualne PMW 
	IF @variant = 99 AND @platformGroup = dbo.f_translate('CFM',default) AND @checkDate IS NULL AND @rootPlatformId <> 78 
	AND @endDate > @currentDate -- AND @endDate < DATEADD(day, 3, @currentDate) 
	BEGIN

		DECLARE @postponeDate datetime
		DECLARE @newVariant INT
		DECLARE @taskId int 
		SELECT @postponeDate = dbo.f_find_first_working_day_before(@endDate, '07:00:00')
		
		SELECT @taskId = id from dbo.process_instance 
		where group_process_id = @groupProcessInstanceId 
		and step_id = '1007.074' 
		and active = 1
		
		IF @taskId IS NULL
		BEGIN	
			
			EXEC [dbo].[s_1007_053]
			@previousProcessId = @previousProcessId,
			@checkDate = @endDate,
		    @variant = @newVariant OUTPUT, 
		    @currentUser = @currentUser, @errId = @errId output
			
--		    SELECT @newVariant
		
		    IF @newVariant = 6 AND NOT EXISTS (SELECT id from dbo.process_instance where step_id = '1007.053' and root_id = @rootId and active = 1 and id <> @previousProcessId)
		    BEGIN						    			    
			    EXEC dbo.p_process_new @stepId = '1007.074', @userId = 1, @originalUserId = 1, @err = @err, @groupProcessId = @groupProcessInstanceId, @message = @message, @parentProcessId = @rootId, @rootId = @rootId, @processInstanceId = @taskId OUTPUT										
		    END			    
		END 
		
		IF @taskId IS NOT NULL
		BEGIN
			UPDATE dbo.process_instance set postpone_date = @postponeDate where id = @taskId and isnull(postpone_date,getdate()) <> @postponeDate	
		END 		
		
	END 
	
END

