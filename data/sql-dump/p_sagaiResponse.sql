-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[p_sagaiResponse]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	TRUNCATE TABLE [SYNC].[SagaiResponseRAW]

	DECLARE @ALLFILENAMES TABLE (WHICHPATH VARCHAR(255),WHICHFILE varchar(255))

	DECLARE @filename	VARCHAR(255),
			@Path		VARCHAR(255) = 'F:\importPSA\',
			@filesPath  VARCHAR(255),
			@cmd		VARCHAR(1000)

	SET @filesPath = @Path + 'FILES\'
	SET @cmd = dbo.f_translate('dir ',default) + @filesPath + '*.* /b'

	DECLARE @download VARCHAR(255) = @Path + '\download.cmd'
	EXECUTE xp_cmdshell @download

	INSERT INTO  @ALLFILENAMES(WHICHFILE)
	EXEC Master..xp_cmdShell @cmd
	UPDATE @ALLFILENAMES SET WHICHPATH = @filesPath WHERE WHICHPATH IS NULL

	DECLARE PSA_importCURSOR CURSOR FOR SELECT WHICHPATH,WHICHFILE FROM @ALLFILENAMES where WHICHFILE IS NOT NULL AND WHICHFILE LIKE '%KWOTY_DO_ZWROTU%'
	OPEN PSA_importCURSOR
	FETCH NEXT FROM PSA_importCURSOR INTO @filesPath,@filename
	WHILE @@FETCH_STATUS <> -1
	BEGIN
		SELECT @filesPath,@filename
	
		DECLARE @BulkFrom NVARCHAR(500) = @filesPath + @filename
		declare @sql varchar(max)='
			BULK INSERT [AtlasDB_v].[SYNC].[SagaiResponseRAW] FROM '''+@BulkFrom+'''
			WITH   (	
				CODEPAGE = 'dbo.f_translate('RAW',default)'
				,CHECK_CONSTRAINTS
				,FIRSTROW = 1
				,MAXERRORS = 10000
				,ROWTERMINATOR = ''0x0a''
				,TABLOCK
				,FIELDTERMINATOR = '';''
			)'
		exec (@sql)

		INSERT INTO [SYNC].[SagaiResponse] ([VAR]) SELECT (VAR) FROM [SYNC].[SagaiResponseRAW] new WITH(NOLOCK)
		WHERE NOT EXISTS(SELECT TOP 1 1 FROM [SYNC].[SagaiResponse] old WITH(NOLOCK) WHERE old.VAR = new.VAR) --> Nie powiela rekordów.

		FETCH NEXT FROM PSA_importCURSOR INTO @filesPath,@filename
	END
	CLOSE PSA_importCURSOR
	DEALLOCATE PSA_importCURSOR

	DECLARE @upload VARCHAR(255) = @Path + '\upload.cmd'
	EXECUTE xp_cmdshell @upload

	TRUNCATE TABLE [SYNC].[SagaiResponseRAW]
END
