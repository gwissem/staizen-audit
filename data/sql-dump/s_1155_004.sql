ALTER PROCEDURE [dbo].[s_1155_004]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT,
    @currentUser INT, 
    @errId INT = 0 OUTPUT
) 
AS
BEGIN
	
	-- ==================================== --
	-- < • TODO  			• > --
	-- ==================================== --

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @cfmPermission INT
	DECLARE @newProcessInstanceId INT
	DECLARE @eventType INT
	DECLARE @isMonitClaim INT
	DECLARE @claimPath INT
	DECLARE @platformId INT
	DECLARE @processInstanceIds NVARCHAR(100)
	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, 
			@stepId = step_id,
			@rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	-- Stworzenie pomocniczej tabelki (attrGet)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '143', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @cfmPermission = value_int FROM @values
	 	
 	-- @variant 10 jest szczególny ! 
 	-- Wymuszenie organizacji Assistance, nawet jeżeli nie przysługuje
 	-- Można go przeklikać tylko wtedy gdy krok jest nieaktywny!
 	IF @variant = 10
 	BEGIN
	 	
	 	-- Ustawienie uprawnien do Assistance + Szkody 
	 	
	 	IF ISNULL(@cfmPermission, 0) <> 3
	 	BEGIN
		 	
		 	EXEC p_attribute_edit
    			@attributePath = '143',
    			@groupProcessInstanceId = @groupProcessInstanceId,
    			@stepId = 'xxx',
    			@userId = @currentUser,
    			@originalUserId = @currentUser,
    			@valueInt = 3,
    			@err = @err OUTPUT,
    			@message = @message OUTPUT
		 	
	 	END
		
		EXEC [dbo].[p_add_case_log] @name = dbo.f_translate('forceActiveAssistance',default), 
									@rootId = @rootId, 
									@groupId = @groupProcessInstanceId, 
									@param1 = @currentUser, 
									@param2 = @platformId
									
	 	-- SKOK DO DIAGNOZY
	 	EXEC [dbo].[p_process_new]
		@stepId = '1011.009', 
		@userId = @currentUser, 
		@originalUserId = @currentUser,
		@err = @err OUTPUT, 
		@groupProcessId = @groupProcessInstanceId,
		@message = @message OUTPUT, 
		@processInstanceId = @newProcessInstanceId OUTPUT
		
		INSERT INTO process_instance_flow (previous_process_instance_id, next_process_instance_id, active)
		VALUES (@previousProcessId, @newProcessInstanceId, 1)
		
	 	RETURN
	 	
 	END
 	
 	
 	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventType = value_int FROM @values
 	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '116,277', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isMonitClaim = value_int FROM @values
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2 @attributePath = '116,244', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @claimPath = value_int FROM @values
	
	IF @platformId = 53
	BEGIN
		
		/*	Wysłanie e-maila do szkodyalphabet@starter24.pl 
		____________________________________*/
		EXEC [dbo].[p_alphabet_monit_harm] @groupProcessInstanceId = @groupProcessInstanceId, @type = 1, @overrideEmails = 'szkodyalphabet@starter24.pl'
	
	END
	ELSE IF @platformId = 43 -- PRZY ALD WYSYŁAMY e-maila do DLS
	BEGIN
		
		-- Dodatkowe zabezpieczenie to co jest przy wysyłce do Varyona
		IF [dbo].[f_canSendXMLtoVaryon](@rootId) = 1
		BEGIN
			
			EXEC [dbo].[P_send_email_to_dls_new_case] @previousProcessId = @groupProcessInstanceId
			
		END
		
	END
	ELSE IF @platformId = 78 
	BEGIN
		
		EXEC [dbo].[p_carefleet_monit_harm] @groupProcessInstanceId = @groupProcessInstanceId, @type = 1		
	END
	
	
	/*	W SPRAWIE JEST JUŻ AKTYWNY PANEL ŚWIADCZEŃ! Dlatego po przyjęciu szkody, to własnie tam wskoczymy
 		____________________________________*/
	
	IF [dbo].[f_exists_active_service_panel](@rootId) = 1
	BEGIN
		
		EXEC [dbo].[p_jump_to_service_panel] @previousProcessId = @previousProcessId, @variant = 1
		
		RETURN
	
	END

	
	/*	Jeżeli ( nie ma monitoringu szkody  i  ( jest Szkoda Jezdna  LUB ( wypadek/szkoda  i NIE MA uprawnień do Assistance) ) )
	  
	 	to kończy sprawe poradą 
 		____________________________________*/
	
	IF ISNULL(@isMonitClaim, 0) = 0 AND ((@eventType = 5 AND isnull(@claimPath,1) <> 8 ) OR (@eventType = 1 AND ISNULL(@cfmPermission, 0) NOT IN (1,3)) )
--	IF ISNULL(@isMonitClaim, 0) = 0 AND @eventType = 5
	BEGIN
		
		PRINT ' ZAKOŃCZYĆ PORADĄ!'
		
		EXEC [dbo].[p_attribute_edit]
		    @attributePath = '334', 
		    @groupProcessInstanceId = @groupProcessInstanceId,
		    @stepId = 'xxx',
		    @userId = 1,
		    @originalUserId = 1,
		    @valueInt = 2,
		    @err = @err OUTPUT,
		    @message = @message OUTPUT
		
		EXEC [dbo].[p_process_jump]
			@previousProcessId = @previousProcessId,
			@nextStepId = '1011.012',
			@doNext = 0,
			@deactivateCurrent = 999,
			@parentId = NULL,
			@rootId = @rootId,
			@groupId = @groupProcessInstanceId,
			@variant = 1,
		    @err = @err output,
		    @message = @message output,
		    @newProcessInstanceId = @newProcessInstanceId output
		    
		SET @variant = 1
--		
	
--		    
--		SELECT * FROM dbo.process_instance WHERE id = @previousProcessId
--		UNION ALL
--		SELECT * FROM dbo.process_instance WHERE id = @newProcessInstanceId
		    
		RETURN
	END
	
	
--		    
 	-- Jak ma uprawnienia do Assistance to skok do diagnozy
 	
 	IF ISNULL(@cfmPermission, 0) IN (1,3)
 	BEGIN
	 	

		/*	Jeżeli diagnoza już była, to robi na wyniku diagnozy NEXT i tam wskakuje
  		____________________________________*/

	 	DECLARE @diagnosisId INT
	 	
	 	SELECT	@diagnosisId = pin.id 
		FROM	dbo.process_instance pin with(nolock) 
		WHERE	root_id = @rootId 
		AND pin.step_id='1011.017'
		ORDER BY pin.id DESC
	 	
		IF @diagnosisId IS NOT NULL
		BEGIN 
			
			UPDATE dbo.process_instance SET active = 1 WHERE id = @diagnosisId 
			
			EXEC p_process_next
			@previousProcessId = @diagnosisId,
			@err = @err OUTPUT,
			@message = @message OUTPUT,
			@processInstanceIds = @processInstanceIds OUTPUT
	
			SET @newProcessInstanceId = CAST(@processInstanceIds AS INT)
			
			INSERT INTO process_instance_flow (previous_process_instance_id, next_process_instance_id, active, variant)
			VALUES (@previousProcessId, @newProcessInstanceId, 1, 1)

		END
		ELSE
		BEGIN
			
			EXEC [dbo].[p_process_new]
			@stepId = '1011.009', 
			@userId = @currentUser, 
			@originalUserId = @currentUser,
			@err = @err OUTPUT, 
			@groupProcessId = @groupProcessInstanceId,
			@message = @message OUTPUT, 
			@processInstanceId = @newProcessInstanceId OUTPUT
			
			INSERT INTO process_instance_flow (previous_process_instance_id, next_process_instance_id, active)
			VALUES (@previousProcessId, @newProcessInstanceId, 1)
			
		END
		
 	END
 	ELSE
 	BEGIN
	 	
	 	/* Prawdopodobnie trwa Monitoring Szkody, więc skok Do Panelu Świadczeń
   		____________________________________*/
	 	
	 	EXEC [dbo].[p_jump_to_service_panel] @previousProcessId = @previousProcessId, @variant = 1
   		
 	END
 	
END



