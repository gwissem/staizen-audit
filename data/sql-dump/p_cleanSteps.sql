ALTER PROCEDURE [dbo].[p_cleanSteps]
AS
  begin

    set nocount on

    ------------------------------------------------------------------------------------------------------------------
    -- usunięcie zdublowanych zadań
    ------------------------------------------------------------------------------------------------------------------

    update dbo.process_instance
    set active = 0
    where id in (select a.id
                 from (select pin.id,
                              pin.group_process_id,
                              pin.step_id,
                              pin.token,
                              pin.company_id,
                              ROW_NUMBER() over (partition by group_process_id, step_id, token, company_id order by id) rowN
                       from dbo.process_instance pin with(nolock)
                       where active = 1) a
                 where a.rowN > 1)

    if datepart(minute, getdate()) > 6
      return

    declare @instanceId int
    declare @rootId int
    DECLARE @err int
    DECLARE @message nvarchar(255)
    DECLARE @processInstanceIds varchar(4000)
    DECLARE @stepId nvarchar(255)
    DECLARE @variant int
    declare @groupProcessInstanceId int
    declare @currentHour int
    declare @startTime datetime = getdate()

    set @currentHour = datepart(hour, @startTime)

    declare @ts int
    set @ts = datediff(ms, getdate(), @startTime)
    print '01 - ' + cast(@ts as varchar(20))
    set @startTime = getdate()

    ------------------------------------------------------------------------------------------------------------------
    -- dzwoni telefon
    ------------------------------------------------------------------------------------------------------------------

    update dbo.process_instance
    set active = 0
    where step_id like '1012%'
      and step_id <> '1012.012'
      and created_at < dateadd(minute, -30, getdate())
      and active = 1

    set @ts = datediff(ms, getdate(), @startTime)
    print '02 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:05';
    set @startTime = getdate()

    ------------------------------------------------------------------------------------------------------------------
    -- początek DC
    ------------------------------------------------------------------------------------------------------------------

    update dbo.process_instance
    set active = 0
    where step_id like '1021.002'
      and created_at < dateadd(minute, -60, getdate())
      and active = 1

    set @ts = datediff(ms, getdate(), @startTime)
    print '03 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:05';
    set @startTime = getdate()
    ------------------------------------------------------------------------------------------------------------------
    -- przeterminowany monitoring ICS
    ------------------------------------------------------------------------------------------------------------------
    update pin
    set pin.active = 0
    from dbo.process_instance pin with(nolock)
           inner join dbo.attribute_value aveta with(nolock)
             on aveta.group_process_instance_id = pin.group_process_id and aveta.attribute_path = '107'
    where pin.step_id like '1009.036'
      and aveta.value_date < dateadd(hour, -2, getdate())
      and pin.active = 1

    set @ts = datediff(ms, getdate(), @startTime)
    print '04 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:05';
    set @startTime = getdate()
    ------------------------------------------------------------------------------------------------------------------
    -- popychamy NH po braku reakcji
    ------------------------------------------------------------------------------------------------------------------

    if @currentHour > 17 or @currentHour < 8 --or 1=1
      begin
        declare kur cursor LOCAL for
          select pin.id, pin.group_process_id, pin.step_id
          from dbo.process_instance pin with(nolock)
                 inner join dbo.attribute_value aveta with(nolock)
                   on aveta.group_process_instance_id = pin.group_process_id and aveta.attribute_path = '107'
          where pin.step_id IN ('1009.004', '1009.005', '1009.055')
            and aveta.value_date < dateadd(hour, -2, getdate())
            and pin.active = 1
        OPEN kur;
        FETCH NEXT FROM kur
        INTO @instanceId, @groupProcessInstanceId, @stepId;
        WHILE @@FETCH_STATUS = 0
          BEGIN

            set @variant = 1

            if @stepId = '1009.004'
              BEGIN
                SET @variant = 7
              END
            ELSE if @stepId = '1009.005'
              BEGIN
                SET @variant = 3
              END

            EXEC [dbo].[p_process_next] @previousProcessId = @instanceId
            , @err = @err OUTPUT
            , @variant = @variant
            , @message = @message OUTPUT
            , @processInstanceIds = @processInstanceIds OUTPUT

            EXEC dbo.p_note_new @groupProcessId = @groupProcessInstanceId
            , @type = dbo.f_translate('system',default)
            ,
                                @content = dbo.f_translate('Ze względu na brak reakcji kontraktora usługa N/H została automatycznie przekierowana do kroku zamknięcia dla kontraktora.',default)
            , @err = @err OUTPUT
            , @message = @message OUTPUT

            WAITFOR DELAY '00:00:05';

            FETCH NEXT FROM kur
            INTO @instanceId, @groupProcessInstanceId, @stepId;
          END
        CLOSE kur
        DEALLOCATE kur

      end

    set @instanceId = null
    set @processInstanceIds = null
    set @stepId = null
    set @variant = null
    set @groupProcessInstanceId = null

    set @ts = datediff(ms, getdate(), @startTime)
    print '05 - ' + cast(@ts as varchar(20))
    set @startTime = getdate()
    ------------------------------------------------------------------------------------------------------------------
    -- odwołaj usługę
    ------------------------------------------------------------------------------------------------------------------


    update dbo.process_instance
    set active = 0
    where step_id like '1022.%'
      and created_at < dateadd(minute, -30, getdate())
      and active = 1

    set @ts = datediff(ms, getdate(), @startTime)
    print '06 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:05';
    set @startTime = getdate()
    ------------------------------------------------------------------------------------------------------------------
    -- podwójne Rs-y
    ------------------------------------------------------------------------------------------------------------------

    UPDATE dbo.process_instance
    set active = 0
    where id in (select id
                 from (select pin.id,
                              pin.step_id,
                              pin.active,
                              pin.created_at,
                              pin.group_process_id,
                              token,
                              ROW_NUMBER() over (partition by group_process_id order by active desc, id desc, token desc) rowN
                       from dbo.process_instance pin with(nolock)
                       where step_id like '1021.003'
                         and active = 1) a
                 where rowN > 1)

    set @ts = datediff(ms, getdate(), @startTime)
    print '07 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:05';
    set @startTime = getdate()
    ------------------------------------------------------------------------------------------------------------------
    -- diagnoza gdzie jest panel świadczeń
    ------------------------------------------------------------------------------------------------------------------

    declare @diagnosisId int

    SELECT @diagnosisId = pin.process_definition_id
    FROM dbo.process_import pin with(nolock)
    where pin.active = 1
      and pin.name = dbo.f_translate('diagnosis',default)


    UPDATE dbo.process_instance
    set active = 0
    where id in (select pin.id
                 from dbo.process_instance pin with(nolock)
                 where pin.step_id like cast(@diagnosisId as varchar(20)) + '%'
                   and pin.active = 1
                   and pin.root_id in (select pin.root_id
                                       from dbo.process_instance pin with(nolock)
                                       where pin.step_id like '1011.010'
                                         and pin.active = 1)
                   and created_at < dateadd(minute, -30, getdate()))

    set @ts = datediff(ms, getdate(), @startTime)
    print '08 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:05';
    set @startTime = getdate()

    ------------------------------------------------------------------------------------------------------------------
    -- Po 50 godzinach zadanie zamykania NH przechodzi do konsultanta
    ------------------------------------------------------------------------------------------------------------------
    if @currentHour > 17 or @currentHour < 8
      begin
        declare kur cursor LOCAL for
          select id, root_id
          from dbo.process_instance with(nolock)
          where step_id = '1009.006'
            and company_id <> 1
            and created_at < dateadd(hour, -50, getdate())
            and active = 1 --and 1=0
        OPEN kur;
        FETCH NEXT FROM kur
        INTO @instanceId, @rootId;
        WHILE @@FETCH_STATUS = 0
          BEGIN


            DECLARE @token varchar(50)

            EXECUTE dbo.p_process_next @previousProcessId = @instanceId
            , @userId = 1
            , @err = @err OUTPUT
            , @message = @message OUTPUT
            , @processInstanceIds = @processInstanceIds OUTPUT
            , @token = @token OUTPUT

            WAITFOR DELAY '00:00:10';
            FETCH NEXT FROM kur
            INTO @instanceId, @rootId;
          END
        CLOSE kur
        DEALLOCATE kur
      end

    set @ts = datediff(ms, getdate(), @startTime)
    print '09 - ' + cast(@ts as varchar(20))
    set @startTime = getdate()
    ------------------------------------------------------------------------------------------------------------------
    -- Próba przesłania zawieszonych kroków technicznych
    ------------------------------------------------------------------------------------------------------------------

    declare kur cursor LOCAL for
      select distinct p.id, p.root_id
      from dbo.process_instance p with(nolock)
             inner join dbo.step s with(nolock) on s.id = p.step_id
             left join dbo.step_permissions sp with(nolock) on p.step_id = sp.step_id and sp.user_group_id <> 3374
      where p.active = 1
        and s.is_technical = 1
        and DATEDIFF(MINUTE, p.created_at, getdate()) > 5
        and isnull(p.postpone_date, getdate()) <= getdate()
        and p.created_at > getdate() - 10

    --	and 1=0
    OPEN kur;
    FETCH NEXT FROM kur
    INTO @instanceId, @rootId;
    WHILE @@FETCH_STATUS = 0
      BEGIN

        EXECUTE dbo.p_process_next @previousProcessId = @instanceId
        , @userId = 1
        , @err = @err OUTPUT
        , @message = @message OUTPUT
        , @processInstanceIds = @processInstanceIds OUTPUT
        , @token = @token OUTPUT

        --  WAITFOR DELAY '00:00:05';
        FETCH NEXT FROM kur
        INTO @instanceId, @rootId;
      END
    CLOSE kur
    DEALLOCATE kur

    set @ts = datediff(ms, getdate(), @startTime)
    print '10 - ' + cast(@ts as varchar(20))
    set @startTime = getdate()

    ------------------------------------------------------------------------------------------------------------------
    -- Podmiana udanych napraw CFM na programy producenckie
    ------------------------------------------------------------------------------------------------------------------
    declare @programId int

    if @currentHour > 17 or @currentHour < 8
      begin

        declare kur cursor LOCAL for
          select distinct pin.group_process_id, dbo.f_producer_program(av204.value_string) producerProgramId
          from dbo.process_instance pin with(nolock)
                 inner join dbo.attribute_value av204 with(nolock)
                   on av204.root_process_instance_id = pin.root_id and av204.attribute_path = '204' and
                      dbo.f_producer_program(av204.value_string) > 0
                 inner join dbo.attribute_value avp with(nolock)
                   on avp.root_process_instance_id = pin.root_id and avp.attribute_path = '253' and avp.value_int in
                                                                                                    (select platform_id
                                                                                                     from dbo.platform_group_platforms with(nolock)
                                                                                                     where platform_group_id = 6)
                 inner join dbo.attribute_value avpr with(nolock)
                   on avpr.group_process_instance_id = pin.group_process_id and avpr.attribute_path = '202' and
                      avpr.value_string in (select id
                                            from dbo.vin_program with(nolock)
                                            where platform_id in (select platform_id
                                                                  from dbo.platform_group_platforms with(nolock)
                                                                  where platform_group_id = 6))
                 inner join dbo.attribute_value av with(nolock)
                   on av.group_process_instance_id = pin.group_process_id and av.attribute_path = '560' and
                      av.value_int = 2
                 inner join dbo.service_status ss with(nolock) on ss.group_process_id = pin.group_process_id
                 inner join dbo.service_status_dictionary ssd with(nolock)
                   on ssd.progress = 4 and ssd.id = ss.status_dictionary_id

          where pin.step_id like '1009.%' --and pin.group_process_id=632134
              --and 1=0
          order by pin.group_process_id
        OPEN kur;
        FETCH NEXT FROM kur
        INTO @groupProcessInstanceId, @programId;
        WHILE @@FETCH_STATUS = 0
          BEGIN

            EXEC p_attribute_edit @attributePath = '202'
            , @groupProcessInstanceId = @groupProcessInstanceId
            , @stepId = 'xxx'
            , @valueString = @programId
            , @err = @err OUTPUT
            , @message = @message OUTPUT

            FETCH NEXT FROM kur
            INTO @groupProcessInstanceId, @programId;
          END
        CLOSE kur
        DEALLOCATE kur

      end

    set @ts = datediff(ms, getdate(), @startTime)
    print '11 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:10';
    set @startTime = getdate()


    ------------------------------------------------------------------------------------------------------------------
    -- Aktualizacja oznaczenia "szacowanej" naprawy warsztatowej (skopiowanie do nowej sprawy)
    ------------------------------------------------------------------------------------------------------------------

    update av798
    set av798.value_int = av129798.value_int
    from dbo.attribute_value av798 with(nolock)
           inner join dbo.attribute_value av286 with(nolock)
             on av286.attribute_path = '286' and av286.root_process_instance_id = av798.root_process_instance_id
           inner join dbo.attribute_value av129130 with(nolock) on av129130.attribute_path = '129,130' and
                                                                   av129130.root_process_instance_id = av798.root_process_instance_id
           inner join dbo.attribute_value av129798 with(nolock) on av129798.attribute_path = '129,798' and
                                                                   av129798.root_process_instance_id = av798.root_process_instance_id
    where av798.attribute_path = '798'
      and av129130.value_date = av286.value_date
      and av129130.value_date is not null
      and isnull(av798.value_int, 0) <> isnull(av129798.value_int, 0)

    set @ts = datediff(ms, getdate(), @startTime)
    print '12 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:10';
    set @startTime = getdate()
    ------------------------------------------------------------------------------------------------------------------
    -- Deaktywacja podwójnych paneli świadczeń
    ------------------------------------------------------------------------------------------------------------------

    update dbo.process_instance
    set active = 0
    where id in (select id
                 from (select id,
                              group_process_id,
                              root_id,
                              row_number() over (partition by root_id order by id desc) rowN
                       from dbo.process_instance pin with(nolock)
                       where step_id = '1011.010'
                         and active = 1) a
                 where a.rowN > 1)


    set @ts = datediff(ms, getdate(), @startTime)
    print '13 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:10';
    set @startTime = getdate()
    
    ------------------------------------------------------------------------------------------------------------------
    -- Ustawienie 999 na ostatnich nietechnicznych krokach spraw które nie doszły do panelu
    ------------------------------------------------------------------------------------------------------------------

    create table #inactiveCases (
      id      int
      ,
      root_id int
    )

    insert into #inactiveCases
    select id, root_id
    from (select pin.id, pin.root_id, row_number()over (partition by pin.root_id order by pin.id desc) rowN
          from dbo.process_instance pin with(nolock)
                 inner join dbo.step stepDef with(nolock)
                   on stepDef.id = pin.step_id and isnull(stepDef.is_technical, 0) = 0
                 inner join dbo.process_instance root with(nolock) ON pin.root_id = root.id
                 left JOIN dbo.step_permissions sp with(nolock) ON pin.step_id = sp.step_id AND sp.user_group_id = 3374
                 left join dbo.process_instance servicePanel with(nolock)
                   ON servicePanel.group_process_id = pin.root_id and servicePanel.step_id = '1011.010'
                 left join dbo.process_instance step with(nolock) on step.root_id = root.id and step.active IN (1, 999)
          where step.id is null
            and left(pin.step_id, 4) NOT IN (1091, 1092, 1012, 1021, 1028)
            and servicePanel.id is null
            and sp.step_id is null) a
    where a.rowN = 1

    update dbo.process_instance set active = 999, updated_at = getdate() where id in (select id from #inactiveCases)

    set @rootId = null

    declare kur cursor LOCAL for
      select root_id from #inactiveCases
    OPEN kur;
    FETCH NEXT FROM kur
    INTO @rootId;
    WHILE @@FETCH_STATUS = 0
      BEGIN

        EXEC [dbo].[p_attribute_edit] @attributePath = '734'
        , @groupProcessInstanceId = @rootId
        , @stepId = 'xxx'
        , @userId = 1
        , @originalUserId = 1
        , @valueInt = 999
        , @err = @err OUTPUT
        , @message = @message OUTPUT

        FETCH NEXT FROM kur
        INTO @rootId;
      END
    CLOSE kur
    DEALLOCATE kur


    drop table #inactiveCases

    set @ts = datediff(ms, getdate(), @startTime)
    print '14 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:10';
    set @startTime = getdate()

    ------------------------------------------------------------------------------------------------------------------
    -- Ustawienie platformId w process_instance_property
    ------------------------------------------------------------------------------------------------------------------

    update pip
    set platform_id = av.value_int
    from dbo.process_instance_property pip with(nolock)
           inner join dbo.process_instance pin with(nolock) on pin.id = pip.process_instance_id
           inner join dbo.attribute_value av with(nolock)
             on av.group_process_instance_id = pin.root_id and av.attribute_path = '253'
    where pip.platform_id is null
      and av.value_int is not null

    set @ts = datediff(ms, getdate(), @startTime)
    print '15 - ' + cast(@ts as varchar(20))
    WAITFOR DELAY '00:00:10';
    set @startTime = getdate()

    ------------------------------------------------------------------------------------------------------------------
    -- Dezaktywowanie powielonych automatów wynajmu dla tej samej grupy wynajmów / tego samego wynajmu niegrupowego
    ------------------------------------------------------------------------------------------------------------------

    update dbo.process_instance
    set active = 0
    where id IN (select min(pin.id)
                 from dbo.process_instance pin with(nolock)
                        inner join dbo.attribute_value av with(nolock)
                          on av.group_process_instance_id = pin.group_process_id and av.attribute_path = '168,839'
                 where pin.step_id = '1007.053'
                   and active = 1
                 group by av.value_int, pin.root_id, pin.group_process_id
                 having count(pin.id) > 1)

    ------------------------------------------------------------------------------------------------------------------------------
    -- Aplhabet wsparcie kierowcy zamykanie procesów starszych niæ dwa dni
    ------------------------------------------------------------------------------------------------------------------------------

    update dbo.process_instance
    set active     = case
when step_id = '1011.010' then 999
                   else 0 end,
        updated_at = getdate()
    where root_id in (select pin.root_id
                      from dbo.process_instance pin with(nolock)
                             inner join dbo.attribute_value avp with(nolock)
                               on pin.group_process_id = avp.group_process_instance_id and
                                  avp.attribute_path = '253' and
                                  avp.value_int = 58
                      where pin.step_id = '1011.010'
                        and pin.active = 1
                        and pin.created_at < getdate() - 2)

    ------------------------------------------------------------------------------------------------------------------
    -- ustal co sie stało z mw starsze niż 12h/data zak. naprawy
    ------------------------------------------------------------------------------------------------------------------

    declare kur cursor LOCAL for
      select pin.id, pin.group_process_id
      from dbo.process_instance pin with(nolock)
             inner join dbo.attribute_value fixDate with(nolock)
               on fixDate.group_process_instance_id = pin.root_id and fixDate.attribute_path = '286'
      where pin.step_id = '1007.052'
        and pin.active = 1
        and (pin.created_at < dateadd(hour, -12, getdate()) or fixDate.value_date < dateadd(hour, -2, getdate()))
    OPEN kur;
    FETCH NEXT FROM kur
    INTO @instanceId, @groupProcessInstanceId;
    WHILE @@FETCH_STATUS = 0
      BEGIN

        EXEC [dbo].[p_process_next] @previousProcessId = @instanceId
        , @err = @err OUTPUT
        , @variant = 3
        , @message = @message OUTPUT

        EXEC dbo.p_note_new @groupProcessId = @groupProcessInstanceId
        , @type = dbo.f_translate('system',default)
        ,
                            @content = dbo.f_translate('Usługa pojazdu zastępczego została automatycznie przekierowana do kroku automatycznego przedłużenia',default)
        , @err = @err OUTPUT
        , @message = @message OUTPUT

        WAITFOR DELAY '00:00:05';

        FETCH NEXT FROM kur
        INTO @instanceId, @groupProcessInstanceId;
      END
    CLOSE kur
    DEALLOCATE kur

    

    ------------------------------------------------------------------------------------------------------------------
    -- dezaktywowanie nieaktywnych ankiet
    ------------------------------------------------------------------------------------------------------------------

    DECLARE @yesterday DATETIME
    
    BEGIN
        SET @yesterday = DATEADD(hour, -12, GETDATE())
        UPDATE survey
        set active     = 0,
            updated_at = GETDATE()
        where created_at < @yesterday and active =1
    END
      
      
  end