ALTER PROCEDURE [dbo].[P_send_email_to_lease_plan_create_towing_notification]
	@previousProcessId INT	-- Może być ofc GROUP / ROOT
AS
BEGIN
	
	/*		
	 * 
	Powiadomienie o holowaniu do InterCars 
	- wysyłane do: flota@intercars.eu, dw: kzcfm
	- warunek wysłania: nowa sprawa, jeżeli wysyłamy holowanie, serwis nieautoryzowany
	- 
 		____________________________________*/

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @title NVARCHAR(MAX)
	
	-- Pobranie podstawowych danych --
	SELECT	@groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	/*	Ustawienie, że wysłano powiadomienie o organizacji Holowania	
 		____________________________________*/
	
	EXEC p_attribute_edit
		@attributePath = '1043',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = 10,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	
	SET @content = 'Szanowni Państwo,</br></br>

		przesyłamy powiadomienie dot. transportu pojazdu LeasePlan</br></br>
		
		Powiadomienie do sprawy: {#caseid#}</br>
		Nr rejestracyjny auta: {@74,72@}</br>
		Marka i model pojazdu: {@74,73@}</br>
		Użytkownik: {@80,342,64@} {@80,342,66@}</br>
		Nr tel.: {@80,342,408,197@}</br>
		Pojazd zostanie odholowany do serwisu: {#towingPlace()#} {@767,781,84@} {@767,781,687@}</br></br>

		W korespondencji z nami należy zawsze używać adresu mailowego cfm@starter24.pl (kontakt w sprawach Car Fleet Management), pamiętając by w temacie wiadomości zawrzeć nasz numer sprawy (widoczny w temacie niniejszego maila). W sprawach bardzo pilnych uprzejmie prosimy o dodatkowy kontakt telefoniczny pod numerem +48 61 83 19 969
	'

	EXEC [dbo].[P_parse_string]
	 @processInstanceId = @previousProcessId,
	 @simpleText = 'Powiadomienie do sprawy [{#caseid#} / {@74,72@}]',
	 @parsedText = @title OUTPUT

	EXEC [dbo].[P_get_body_email]
		@body = @body OUTPUT,
		@contentEmail  = @content,
		@title = @title,
		@previousProcessId = @previousProcessId	
		
	SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'CFM')
	SET @body = REPLACE(@body,'__EMAIL__', 'cfm@starter24.pl')
	 
	 DECLARE @email VARCHAR(400)
	 DECLARE @dw NVARCHAR(400)
	 DECLARE @sender NVARCHAR(400)
	 
	 SET @email = dbo.f_getRealEmailOrTest('flota@intercars.eu')
	 SET @dw = dbo.f_getRealEmailOrTest('kzcfm@starter24.pl')
	 SET @sender = dbo.f_getEmail('cfm')
	 
	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @groupProcessInstanceId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = dbo.f_translate('Powiadomienie o holowaniu do InterCars',default)
	 	,@email = @email
	 	,@userId = 1  -- automat
	 	,@subject = @title
	 	,@direction=1
	 	,@dw = @dw
	 	,@udw = ''
	 	,@sender = @sender
	 	,@emailBody = @body
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT
	
END
