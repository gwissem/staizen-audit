
ALTER PROCEDURE [dbo].[p_allServicesRep2] @rootId INT = NULL
AS
BEGIN
SET NOCOUNT ON

DECLARE @groupId INT
DECLARE @count INT = 0

DECLARE @Force BIT = 0
IF @rootId IS NOT NULL
BEGIN
	SET @Force = 1
END

DECLARE @ReportTable TABLE (
	caseId nchar(10), -- [Nr_sprawy]
	rootId int, -- [Id_sprawy]
	groupId int, -- [Id_usługi]
	attributeId int, -- [Id_opcji]
	ZSinCDN int,
	ZZinCDN int,
	entityId int, -- [opcjaId]
	entityName nvarchar(100), -- [NazwaUslugi]
	quantity int, -- [ilosc]
	manufacturer int,
	insurancer int,
	partner int,
	dealer int,
	client int,
	openDate datetime, -- [Data_otwarcia]
    openTime int, -- [Czas_otwarcia]
    openYear int, -- [Rok_otwarcia]
    openMonth int, -- [Miesiac_otwarcia]
    openDay int, -- [Dzien_otwarcia]
    openWeek int,-- [Dzien_tygodnia_otwarcia]
     -- [Czas_otwarcia_na_back]
     -- [Czas_przypisania_kontraktor]
	updateAt datetime, -- [Last_update]
	closeDate datetime, -- [Data_zamkniecia_operacyjnego]
    closeTime int, -- [Czas_zamkniecia]
    closeYear int, -- [Rok_zamkniecia]
    closeMonth int, -- [Miesiac_zamkniecia]
    closeDay int, -- [Dzien_zamkniecia]
    closeWeek int, -- [Dzien_tygodnia_zamkniecia]
	closedBy nvarchar(255), -- BRAK [ktoZamknalOperacyjnie]
	platformId int, -- [platforma_Id]
	platformName nvarchar(100), -- [Platforma]
	programId int, -- 
	programName nvarchar(100), -- [program]
	process nvarchar(100),
	caseCountry nvarchar(100), -- [Lokalizacja_kraj]
	caseCity nvarchar(100), -- [Lokalizacja_miasto]
	caseStreet nvarchar(100), -- [Lokalizacja_ulica]
	caseCode nvarchar(10), -- [Lokalizacja_kod]
	caseProvince nvarchar(100), -- [Lokalizacja_wojewodztwo]
	caseCommune nvarchar(100), -- [Lokalizacja_gmina]
	caseCounty nvarchar(100), -- [Lokalizacja_powiat]
	caseAddressType nvarchar(100), -- [Lokalizacja_rodzaj]
	-- [Lokalizacja_RKS]
	-- [Obszar_kod]
	-- [Miasto_powyzej_100tys]
	firstRegistrationDate date, -- [Data_pierwszej_rejestracji]
	mileage int, -- [Przebieg]
	makeModel nvarchar(100), -- [Marka] + [Model] = [Marka_model]
	DMC int, -- [DMC]
	VIN nchar(30), -- [VIN]
	licencePlate nvarchar(50), -- [Numer_rejestracyjny]
	caseReason nvarchar(255), -- [Przyczyna_zgloszenia]
	-- [Kategoria_awarii]
	caseType nvarchar(100), -- [klasyfikacja] (wypadek,awaria)
	executionAtOnce bit, -- [czyDoNatychmiastowegoWykonania]
	towingWay int, -- [kmHolowania]
	returnWay int, -- [kmDojazdPowrot] - toCustomerWay
	toCustomerWay int, -- [kmDojazdPowrot] - returnWay
	carType nvarchar(255),
	ARC1 int,
	ARC2 int,
	ARC3 int,
      -- [Diagnoza_NW] Raport zamknięcia NH?
      -- [Czy_gwarancyjna_NW]
      -- [Planowany_koniec_NW]
      -- [Status_NW]
      -- [Otrzymanie_RS_do_NW]
	repairFailed bit, -- ! [udanaNaprawa] -> 0, nieudana 1
	emptyService bit, -- [pustyWyjazd]
	serviceStatusId int,
	serviceStatus nvarchar(100), -- [Status]
	performingId int,
	performingStarterCode nvarchar(100), -- [Wykonujacy_nr] (kod starter)
	performingRKS nvarchar(255), -- [Wykonujacy_RKS]
	performingName nvarchar(255), -- [Wykonujacy_nazwa]
	performingCountry nvarchar(255),
	performingCity nvarchar(255), -- [Wykonujacy_miasto]
	performingStreet nvarchar(255),
	performingPostCode nvarchar(10), -- [Wykonujacy_kod]
	performingBuildingNr nvarchar(10),
	performingApartmentNr nvarchar(10),
	-- [Wykonujacy_odleglosc]
	rentDays int, -- [iloscDniWynajmu]
	rentModelId INT,
	rentModel nvarchar(255), -- [markaPojazduZastepczego] + [modelPojazduZastepczego]
	-- [klasaPojazduZastepczego]
	rentFirstname nvarchar(255),
	rentLastname nvarchar(255),
	-- [miastoSerwisu]
	rentPrice decimal(18,2), -- Koszty wynajmu
	rentReturnPrice decimal(18,2), -- Koszty zwrotu
	rentSubstitutionPrice decimal(18,2), -- Koszty podstawienia
	declarant nvarchar(255), -- [Zglaszajacy]
	declarantPhone nvarchar(255), -- [ZglaszajacyTelefon]
	driver nvarchar(255), -- [Kierowca]
	driverPhone nvarchar(255), -- [KierowcaTelefon]
	
	-- FINANSE
	Tre_GIDNumer int -- [Tre_GIDNumer]
	,Tre_GIDlp smallint -- [Tre_GIDlp]
	,Tre_TwrNazwa varchar(255) -- [Tre_TwrNazwa]
	,Tre_TwrKod	varchar(40) -- [Tre_TwrKod]
	,Tre_Ilosc numeric(11,4) -- [Tre_Ilosc]
	,Tre_RzeczywistaNetto numeric(15,2) -- [Tre_RzeczywistaNetto]
	,Tre_KsiegowaBrutto	numeric(15,2) -- [Tre_KsiegowaBrutto]
	,TrN_Waluta	varchar(3) -- [TrN_Waluta]
	,ZaN_DokumentObcy varchar(40) -- [ZaN_DokumentObcy]
	,TrN_DokumentObcy varchar(40) -- [TrN_DokumentObcy]
	,TrN_Stan smallint -- [TrN_Stan]
	,TrN_Zaksiegowano tinyint -- [TrN_Zaksiegowano]
	,DataRozliczenia datetime -- [DataRozliczenia]
	,DataWystawienia datetime -- [DataWystawienia]
	,ZaN_ZamTyp	smallint
	,nrZam varchar(50) -- [nrZamowienia]
	,wiersz	int -- [wiersz]
	-- END FINANSE
	

	,synchronizationDate datetime
	/*
      ,[NrZleceniaPartneraZagr]
      ,[NrZleceniaZewnetrznejAplikacji]
      ,[Sprawa_odwolana]
      ,[Storno_kontraktor]
      ,[Storno_klient]
      ,[Storno_inne]
      ,[Odwolane_Starter]
      ,[Storno]
      ,[Typ_silnika]
      ,[Sprawa_kwalifikacja]
      ,[czasOdebraniePolaczenia]
      ,[CzasZakonczeniaPolaczenia]   
      ,[nazwaSwiadczenia]
      ,[ktoryPrzyjal]
      ,[Usluga_diagnoza_kod]
      ,[Usluga_diagnoza_nazwa]
      ,[dataPrzyjeciaDoRealizacji]
      ,[Koszt]
      ,[przyczynaSwiadczenia]
      ,[statusId]
      ,[statusUslugi]
      ,[NumerUprawnienia]

      ,[wartosc]
      ,[Id_uslugi_def]
      ,[Id_programu_def]
      ,[dalszaJazda]
      ,[Kontraktor_id]
      ,[Kontraktor_skrocona_nazwa]
      ,[OplataZaSprawe_Netto]
      ,[OplataZaSprawe_Waluta]
      ,[DataPotwFZ]
      ,[KtoFZ]
      ,[DataZakUslugi]
      ,[WykonawcaUslKodStarter]
      ,[ktoZamknalOperacyjniePierwszyRaz] -- Created by origin id ? 
      ,[dataPierwszegoZamknieciaOperacyjnego]	-- Updated by origin id active 999 w 1011.010
	*/
)

IF (@rootId IS NULL)
BEGIN
	DECLARE Root_Cursor CURSOR
	FOR Select Distinct root_id from process_instance WITH (NOLOCK) where step_id LIKE '1011.024' 
	--SELECT root_id FROM process_instance pi WITH (NOLOCK) WHERE pi.id = pi.root_id 
	--AND EXISTS (SELECT 1 FROM process_instance pi2 WITH (NOLOCK) WHERE pi.root_id = pi2.root_id AND pi2.step_id = '1011.024')

END
ELSE -- Podany RootId
BEGIN
	DECLARE Root_Cursor CURSOR
	FOR Select @rootId
END

OPEN Root_Cursor
FETCH NEXT FROM Root_Cursor INTO @rootId
WHILE @@FETCH_STATUS = 0
BEGIN

	--DECLARE Service_Cursor CURSOR
	--FOR 

	--SELECT DISTINCT group_process_id FROM [dbo].[process_instance] WITH (NOLOCK) WHERE root_id=@rootId AND [group_process_id] IS NOT NULL-- AND created_by_original_id IS NULL--AND step_id LIKE '1011%'
	--AND LEFT(step_id,4) IN ('1007','1009','1014','1015','1016','1017','1018','1019','1088')

	--DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	--OPEN Service_Cursor
	--FETCH NEXT FROM Service_Cursor INTO @groupId

	--WHILE @@FETCH_STATUS = 0
	--BEGIN
		
		DECLARE	@attributeId int = NULL
		DECLARE @entityId int = NULL
		DECLARE @quantity INT = NULL
		DECLARE @manufacturer int = NULL
		DECLARE @insurancer int = NULL
		DECLARE @partner int = NULL
		DECLARE @dealer int = NULL
		DECLARE @client int = NULL

		-- Niezakończone usługi
		DECLARE @entityName NVARCHAR(4000) = NULL
		DECLARE @IsQuerry NVARCHAR(MAX)
		DECLARE @QuantityQuerry NVARCHAR(MAX)
		DECLARE @exist INT = NULL

		DECLARE Matrix_Cursor CURSOR
		FOR 
		
		-- Niezakończone usługi

		--SELECT id,entity,isQuery,quantityQuery FROM dbo.entities ORDER BY OrderBy

		-- Zakończone usługi
		select  avs.value_int serviceId,
				ave.id attributeId,
				ave.value_int entityId,
				avq.value_int quantity,
				avpr.value_int manufacturer,
				avu.value_int insurancer,
				avpz.value_int partner,
				avd.value_int dealer,
				avkl.value_int client
		from    dbo.attribute_value avs with(nolock) inner join
				dbo.attribute_value ave with(nolock) on ave.parent_attribute_value_id=avs.id and ave.attribute_path='820,821,822' inner join
				dbo.attribute_value avpr with(nolock) on avpr.parent_attribute_value_id=ave.id and avpr.attribute_path='820,821,822,824' inner join
				dbo.attribute_value avu with(nolock) on avu.parent_attribute_value_id=ave.id and avu.attribute_path='820,821,822,825' inner join
				dbo.attribute_value avpz with(nolock) on avpz.parent_attribute_value_id=ave.id and avpz.attribute_path='820,821,822,826' inner join
				dbo.attribute_value avd with(nolock) on avd.parent_attribute_value_id=ave.id and avd.attribute_path='820,821,822,827' inner join
				dbo.attribute_value avkl with(nolock) on avkl.parent_attribute_value_id=ave.id and avkl.attribute_path='820,821,822,828' inner join
				dbo.attribute_value avq with(nolock) on avq.parent_attribute_value_id=ave.id and avq.attribute_path='820,821,822,221'
		where	avs.attribute_path='820,821' and 
				avs.value_int is not null and
				avs.root_process_instance_id=@rootId


		OPEN Matrix_Cursor

		FETCH NEXT FROM Matrix_Cursor INTO @groupId, @attributeId ,@entityId, @quantity, @manufacturer, @insurancer, @partner, @dealer, @client

		WHILE @@FETCH_STATUS = 0
		BEGIN

			--IF (@IsQuerry IS NOT NULL AND @IsQuerry != '')
			--BEGIN
			--	EXECUTE sp_executesql @IsQuerry, N'@groupProcessInstanceId int, @exist INT OUTPUT', @groupProcessInstanceId=@groupId, @exist=@exist OUTPUT;

			--	IF (@exist != 0)
			--	BEGIN
			--		IF (@QuantityQuerry IS NOT NULL AND @QuantityQuerry != '')
			--		BEGIN
			--			EXECUTE sp_executesql @QuantityQuerry, N'@groupProcessInstanceId int, @quantity INT OUTPUT', @groupProcessInstanceId=@groupId, @quantity=@quantity OUTPUT;
			--		END

-----------------------------------------------------------------------------------------

		DECLARE @synchronizationDate datetime = (SELECT TOP 1 synchronizationDate FROM [AtlasDB_rep].[dbo].[ReportAllServices] with(nolock) WHERE rootId = @rootId AND groupId = @groupId AND entityId = @entityId)

		IF ((DATEADD(DAY,-1,GETDATE()) > @synchronizationDate OR @synchronizationDate IS NULL) AND @count < 50) OR @Force = 1 -- HOUR, MINUTE
		BEGIN
			SET @count = @count + 1

			DELETE FROM @ReportTable

			INSERT INTO @ReportTable
			SELECT TOP 1
			(SELECT 'A'+RIGHT('00000000'+CAST(@rootId AS NVARCHAR(10)), 8)) AS caseId,
			(SELECT @rootId) AS rootId,
			(SELECT @groupId) AS groupId,
			(SELECT @attributeId) AS attributeId,
			(SELECT COUNT(1) FROM [sync].[ZamElem] ze WITH(nolock) left join [sync].[ZamNag] zn WITH(nolock) on ze.AUD_IdDok=zn.Id WHERE entityvalueid = @attributeId and zn.XL_Typ = 'ZS') AS ZSinCDN,
			(SELECT COUNT(1) FROM [sync].[ZamElem] ze WITH(nolock) left join [sync].[ZamNag] zn WITH(nolock) on ze.AUD_IdDok=zn.Id WHERE entityvalueid = @attributeId and zn.XL_Typ = 'ZZ') AS ZZinCDN,
			(SELECT @entityId) AS entityId,
			(SELECT entity FROM entities WITH(nolock) WHERE id = @entityId) AS entityName,
			(SELECT @quantity) AS quantity,
			(SELECT @manufacturer) AS manufacturer,
			(SELECT @insurancer) AS insurancer,
			(SELECT @partner) AS partner,
			(SELECT @dealer) AS dealer,
			(SELECT @client) AS client,
			(pi_openDate.date_enter) AS openDate,
			datepart(HH, pi_openDate.date_enter) as openTime,
			year(pi_openDate.date_enter) as openYear,
			month(pi_openDate.date_enter) as openMonth,
			day(pi_openDate.date_enter) as openDay,
			datepart(dw,pi_openDate.date_enter) as caseWeek,
			(SELECT TOP 1 CONVERT(smalldatetime,date_enter,120) FROM process_instance WITH (NOLOCK) WHERE group_process_id = @groupId order by date_enter desc) AS updateAt,
			(pi_closeDate.date_leave) AS closeDate,
			datepart(HH, pi_closeDate.date_leave) as closeTime,
			year(pi_closeDate.date_leave) as closeYear,
			month(pi_closeDate.date_leave) as closeMonth,
			day(pi_closeDate.date_leave) as closeDay,
			datepart(dw,pi_closeDate.date_leave) as closeWeek,
			(select firstname + ' ' + lastname from fos_user WITH(nolock) where id = pi_closeDate.updated_by_id) AS closedBy,
			(av_platformId.value_int) AS platformId,
			(SELECT name FROM dbo.platform WITH (NOLOCK) WHERE id = av_platformId.value_int) AS platformName,
			(av_programId.value_string) AS programId,
			(SELECT name FROM dbo.vin_program WITH (NOLOCK) WHERE id = av_programId.value_string) AS programName,
			(SELECT name FROM [dbo].[process_definition_translation] WITH (NOLOCK) WHERE locale = 'pl' AND translatable_id = (SELECT TOP (1) LEFT([step_id],4) FROM [dbo].[process_instance] where group_process_id = @groupId)) AS process,
			(av_caseCountry.value_string) AS caseCountry,
			(av_caseCity.value_string) AS caseCity,
			(av_caseStreet.value_string) AS caseStreet,
			(av_caseCode.value_string) AS caseCode,
			(av_caseProvince.value_string) AS caseProvince,
			(av_caseCommune.value_string) AS caseCommune,
			(av_caseCounty.value_string) AS caseCounty,
			(av_caseAddressType.value_int) AS caseAddressType,
			(av_firstRegistrationDate.value_date) AS firstRegistrationDate,
			(av_mileage.value_int) AS mileage,
			(SELECT textD FROM dictionary WITH (NOLOCK) WHERE typeD = 'makeModel' AND active = 1 AND value = av_makeModel.value_int) AS makeModel,
			(av_DMC.value_int) AS DMC,
			(av_VIN.value_string) AS VIN,
			(av_licencePlate.value_string) AS licencePlate,
			(av_caseReason.value_text) AS caseReason,
			(av_caseType.value_int) AS caseType,
			(av_executionAtOnce.value_int) AS executionAtOnce,
			-- Group process instance id
			(av_towingWay.value_decimal) AS towingWay,
			(av_returnWay.value_decimal) AS returnWay,
			(av_toCustomerWay.value_decimal) AS toCustomerWay,
			(SELECT CASE av_carType.value_int WHEN 1 then dbo.f_translate('Laweta',default) else (SELECT CASE av_carType.value_int WHEN 2 then dbo.f_translate('Patrolówka',default) else '' end) end) AS carType,
			(av_ARC1.value_int) AS ARC1,
			(av_ARC2.value_int) AS ARC2,
			(av_ARC3.value_int) AS ARC3,


			(SELECT CASE WHEN (av_ARC3.value_int IN (150,200) OR (av_carType.value_int = 2 AND av_repairFailed.value_int = 0)) then 1 else 0 end) AS repairFailed,

			(SELECT CASE WHEN av_ARC3.value_int=170 OR (	
								SELECT TOP 1 [status_dictionary_id]
								FROM [dbo].[service_status] WITH (NOLOCK)
								WHERE group_process_id = @groupId ORDER BY updated_at DESC
								)=-2															then 1 else 0 end) AS emptyService,

			(select top 1 status_dictionary_id from service_status WITH (NOLOCK) where group_process_id = @groupId order by updated_at DESC) AS serviceStatusId,
			(select top 1 ssdt.message from dbo.service_status_dictionary_translation as ssdt WITH (NOLOCK) where ssdt.translatable_id = (select top 1 status_dictionary_id from service_status WITH (NOLOCK) where group_process_id = @groupId order by updated_at DESC)) AS serviceStatus,
			(select case WHEN av_performingId.value_int IS NOT NULL THEN av_performingId.value_int ELSE av_performingIdDC.value_int END) AS performingId,
			(av_performingStarterCode.value_string) AS performingStarterCode,
			(av_performingRKS.value_string) AS performingRKS,
			(av_performingName.value_string) AS performingName,
			(av_performingCountry.value_string) AS performingCountry,
			(av_performingCity.value_string) AS performingCity,
			(av_performingStreet.value_string) AS performingStreet,
			(av_performingPostCode.value_string) AS performingPostCode,
			(av_performingBuildingNr.value_string) AS performingBuildingNr,
			(av_performingApartmentNr.value_string) AS performingApartmentNr,	

			(av_rentDays.value_int) AS rentDays,
			(av_rentModelId.value_int) AS rentModelId,
			(SELECT textD FROM dictionary WITH (NOLOCK) WHERE typeD = 'makeModel' AND active = 1 AND value = av_rentModelId.value_int) AS rentModel,
			(av_rentFirstname.value_string) AS rentFirstname,
			(av_rentLastname.value_string) AS rentLastname,
			(av_rentPrice.value_decimal) AS rentPrice,
			(av_rentReturnPrice.value_decimal) AS rentPrice,
			(av_rentSubstitutionPrice.value_decimal) AS rentSubstitutionPrice,

			((SELECT TOP 1 value_string FROM attribute_value WITH (NOLOCK) WHERE root_process_instance_id = @rootId AND attribute_path = '81,342,64' ORDER BY created_at) + ' ' + (SELECT TOP 1 value_string FROM attribute_value WITH (NOLOCK) WHERE root_process_instance_id = @rootId AND attribute_path = '81,342,66' ORDER BY created_at)) AS declarant,
			(SELECT TOP 1 value_string FROM attribute_value WITH (NOLOCK) WHERE root_process_instance_id = @rootId AND attribute_path = '81,342,408,197' ORDER BY created_at) AS declarantPhone,
			((SELECT TOP 1 value_string FROM attribute_value WITH (NOLOCK) WHERE root_process_instance_id = @rootId AND attribute_path = '80,342,64' ORDER BY created_at) + ' ' + (SELECT TOP 1 value_string FROM attribute_value WITH (NOLOCK) WHERE root_process_instance_id = @rootId AND attribute_path = '80,342,66' ORDER BY created_at)) AS driver,
			(SELECT TOP 1 value_string FROM attribute_value WITH (NOLOCK) WHERE root_process_instance_id = @rootId AND attribute_path = '80,342,408,197' ORDER BY created_at) AS driverPhone
		
			,xl.Tre_GIDNumer AS Tre_GIDNumer
			,xl.Tre_GIDlp AS Tre_GIDlp
			,xl.Tre_TwrNazwa AS Tre_TwrNazwa
			,xl.Tre_TwrKod AS Tre_TwrKod
			,xl.Tre_Ilosc AS Tre_Ilosc
			,xl.Tre_RzeczywistaNetto AS Tre_RzeczywistaNetto
			,xl.Tre_KsiegowaBrutto AS Tre_KsiegowaBrutto
			,xl.TrN_Waluta AS TrN_Waluta
			,xl.ZaN_DokumentObcy AS ZaN_DokumentObcy
			,xl.TrN_DokumentObcy AS TrN_DokumentObcy
			,xl.TrN_Stan AS TrN_Stan
			,xl.TrN_Zaksiegowano AS TrN_Zaksiegowano
			,xl.DataRozliczenia AS DataRozliczenia
			,xl.DataWystawienia AS DataWystawienia

			,xl.ZaN_ZamTyp AS ZaN_ZamTyp
			,replace(xl.nrZam,dbo.f_translate('Zam',default),'ZS') AS nrZam
			,xl.wiersz AS wiersz
								
			,GETDATE() AS synchronizationDate

			FROM attribute_value av WITH (NOLOCK)

			LEFT JOIN attribute_value av_platformId WITH (NOLOCK) ON av_platformId.root_process_instance_id = @rootId AND av_platformId.attribute_path = '253' 
			LEFT JOIN attribute_value av_programId WITH (NOLOCK) ON av_programId.root_process_instance_id = @rootId AND av_programId.attribute_path = '202' 
			LEFT JOIN (SELECT TOP 1 CONVERT(smalldatetime,date_enter,120) AS date_enter,root_id FROM process_instance WHERE group_process_id = @groupId order by date_enter asc) pi_openDate on pi_openDate.root_id = @rootId
			LEFT JOIN (SELECT TOP 1 CONVERT(smalldatetime,date_leave,120) AS date_leave,root_id,updated_by_id FROM process_instance WHERE root_id = @rootId AND active = 999 order by date_leave desc) pi_closeDate on pi_closeDate.root_id = @rootId
			LEFT JOIN attribute_value av_caseCountry WITH (NOLOCK) ON av_caseCountry.root_process_instance_id = @rootId AND av_caseCountry.attribute_path = '101,85,86' 
			LEFT JOIN attribute_value av_caseCity WITH (NOLOCK) ON av_caseCity.root_process_instance_id = @rootId AND av_caseCity.attribute_path = '101,85,87' 
			LEFT JOIN attribute_value av_caseStreet  WITH (NOLOCK) ON av_caseStreet.root_process_instance_id = @rootId AND av_caseStreet.attribute_path = '101,85,94'
			LEFT JOIN attribute_value av_caseCode WITH (NOLOCK) ON av_caseCode.root_process_instance_id = @rootId AND av_caseCode.attribute_path = '101,85,89' 
			LEFT JOIN attribute_value av_caseProvince WITH (NOLOCK) ON av_caseProvince.root_process_instance_id = @rootId AND av_caseProvince.attribute_path = '101,85,88' 
			LEFT JOIN attribute_value av_caseCommune WITH (NOLOCK) ON av_caseCommune.root_process_instance_id = @rootId AND av_caseCommune.attribute_path = '101,85,90' 
			LEFT JOIN attribute_value av_caseCounty WITH (NOLOCK) ON av_caseCounty.root_process_instance_id = @rootId AND av_caseCounty.attribute_path = '101,85,91' 
			LEFT JOIN attribute_value av_caseAddressType WITH (NOLOCK) ON av_caseAddressType.root_process_instance_id = @rootId AND av_caseAddressType.attribute_path = '101,85,541' 
			LEFT JOIN attribute_value av_firstRegistrationDate WITH (NOLOCK) ON av_firstRegistrationDate.root_process_instance_id = @rootId AND av_firstRegistrationDate.attribute_path = '74,233' 
			LEFT JOIN attribute_value av_mileage WITH (NOLOCK) ON av_mileage.root_process_instance_id = @rootId AND av_mileage.attribute_path = '74,75' 
			LEFT JOIN attribute_value av_makeModel WITH (NOLOCK) ON av_makeModel.root_process_instance_id = @rootId AND av_makeModel.attribute_path = '74,73'
			LEFT JOIN attribute_value av_DMC WITH (NOLOCK) ON av_DMC.root_process_instance_id = @rootId AND av_DMC.attribute_path = '74,76'
			LEFT JOIN attribute_value av_VIN WITH (NOLOCK) ON av_VIN.root_process_instance_id = @rootId AND av_VIN.attribute_path = '74,71'
			LEFT JOIN attribute_value av_licencePlate WITH (NOLOCK) ON av_licencePlate.root_process_instance_id = @rootId AND av_licencePlate.attribute_path = '74,72'
			LEFT JOIN attribute_value av_caseReason WITH (NOLOCK) ON av_caseReason.root_process_instance_id = @rootId AND av_caseReason.attribute_path = '129,279' AND av_caseReason.value_text IS NOT NULL -- diagnoza --> '184,417'
			LEFT JOIN attribute_value av_caseType WITH (NOLOCK) ON av_caseType.root_process_instance_id = @rootId AND av_caseType.attribute_path = '419'
			LEFT JOIN attribute_value av_executionAtOnce WITH (NOLOCK) ON av_executionAtOnce.root_process_instance_id = @rootId AND av_executionAtOnce.attribute_path = '215'
			-- Group process instance id
			LEFT JOIN attribute_value av_towingWay WITH (NOLOCK) ON av_towingWay.group_process_instance_id = @groupId AND av_towingWay.attribute_path = '638,225'
			LEFT JOIN attribute_value av_returnWay WITH (NOLOCK) ON av_returnWay.group_process_instance_id = @groupId AND av_returnWay.attribute_path = '638,218'
			LEFT JOIN attribute_value av_toCustomerWay WITH (NOLOCK) ON av_toCustomerWay.group_process_instance_id = @groupId AND av_toCustomerWay.attribute_path = '638,217'
			LEFT JOIN attribute_value av_carType WITH (NOLOCK) ON av_carType.group_process_instance_id = @groupId AND av_carType.attribute_path = '638,273'
			LEFT JOIN attribute_value av_ARC1 WITH (NOLOCK) ON av_ARC1.group_process_instance_id = @groupId AND av_ARC1.attribute_path = '638,720'
			LEFT JOIN attribute_value av_ARC2 WITH (NOLOCK) ON av_ARC2.group_process_instance_id = @groupId AND av_ARC2.attribute_path = '638,721'
			LEFT JOIN attribute_value av_ARC3 WITH (NOLOCK) ON av_ARC3.group_process_instance_id = @groupId AND av_ARC3.attribute_path = '638,722'
			LEFT JOIN attribute_value av_repairFailed WITH (NOLOCK) ON av_repairFailed.group_process_instance_id = @groupId AND av_repairFailed.attribute_path = '209'



			LEFT JOIN attribute_value av_performingId WITH (NOLOCK) ON av_performingId.group_process_instance_id = @groupId AND av_performingId.attribute_path = '610'
			LEFT JOIN attribute_value av_performingIdDC WITH (NOLOCK) ON av_performingIdDC.group_process_instance_id = @groupId AND av_performingIdDC.attribute_path = '522'
			LEFT JOIN attribute_value av_performingPartnerId WITH (NOLOCK) ON (av_performingPartnerId.id = av_performingId.value_int OR (av_performingPartnerId.id = av_performingIdDC.value_int AND (SELECT value_int FROM attribute_value avdc WITH(nolock) WHERE avdc.attribute_path='802' AND avdc.root_process_instance_id=av_performingIdDC.root_process_instance_id)=1)) AND av_performingPartnerId.attribute_path = '595,597'
			LEFT JOIN attribute_value av_performingStarterCode WITH (NOLOCK) ON av_performingStarterCode.parent_attribute_value_id = av_performingPartnerId.parent_attribute_value_id AND av_performingStarterCode.attribute_path = '595,631'
			LEFT JOIN attribute_value av_performingRKS WITH (NOLOCK) ON av_performingRKS.parent_attribute_value_id = av_performingPartnerId.parent_attribute_value_id AND av_performingRKS.attribute_path = '595,632'
			LEFT JOIN attribute_value av_performingName WITH (NOLOCK) ON av_performingName.parent_attribute_value_id = av_performingPartnerId.id AND av_performingName.attribute_path = '595,597,84'
			LEFT JOIN attribute_value av_performingLocId WITH (NOLOCK) ON av_performingLocId.parent_attribute_value_id = av_performingPartnerId.id AND av_performingLocId.attribute_path = '595,597,85'
			LEFT JOIN attribute_value av_performingCountry WITH (NOLOCK) ON av_performingCountry.parent_attribute_value_id = av_performingLocId.id AND av_performingCountry.attribute_path = '595,597,85,86'
			LEFT JOIN attribute_value av_performingCity WITH (NOLOCK) ON av_performingCity.parent_attribute_value_id = av_performingLocId.id AND av_performingCity.attribute_path = '595,597,85,87'
			LEFT JOIN attribute_value av_performingStreet WITH (NOLOCK) ON av_performingStreet.parent_attribute_value_id = av_performingLocId.id AND av_performingStreet.attribute_path = '595,597,85,94'
			LEFT JOIN attribute_value av_performingPostCode WITH (NOLOCK) ON av_performingPostCode.parent_attribute_value_id = av_performingLocId.id AND av_performingPostCode.attribute_path = '595,597,85,89'
			LEFT JOIN attribute_value av_performingBuildingNr WITH (NOLOCK) ON av_performingBuildingNr.parent_attribute_value_id = av_performingLocId.id AND av_performingBuildingNr.attribute_path = '595,597,85,95'
			LEFT JOIN attribute_value av_performingApartmentNr WITH (NOLOCK) ON av_performingApartmentNr.parent_attribute_value_id = av_performingLocId.id AND av_performingApartmentNr.attribute_path = '595,597,85,96'

			LEFT JOIN attribute_value av_rentDays WITH (NOLOCK) ON av_rentDays.group_process_instance_id = @groupId AND av_rentDays.attribute_path = '812,789,786,240,153'
			LEFT JOIN attribute_value av_rentModelId WITH (NOLOCK) ON av_rentModelId.group_process_instance_id = @groupId AND av_rentModelId.attribute_path = '812,789,786,73'
			LEFT JOIN attribute_value av_rentFirstname WITH (NOLOCK) ON av_rentFirstname.group_process_instance_id = @groupId AND av_rentFirstname.attribute_path = '812,64'
			LEFT JOIN attribute_value av_rentLastname WITH (NOLOCK) ON av_rentLastname.group_process_instance_id = @groupId AND av_rentLastname.attribute_path = '812,66'
			LEFT JOIN attribute_value av_rentPrice WITH (NOLOCK) ON av_rentPrice.group_process_instance_id = @groupId AND av_rentPrice.attribute_path = '812,789,786,240,222'
			LEFT JOIN attribute_value av_rentReturnPrice WITH (NOLOCK) ON av_rentReturnPrice.group_process_instance_id = @groupId AND av_rentReturnPrice.attribute_path = '812,789,787,240,222'
			LEFT JOIN attribute_value av_rentSubstitutionPrice WITH (NOLOCK) ON av_rentSubstitutionPrice.group_process_instance_id = @groupId AND av_rentSubstitutionPrice.attribute_path = '812,789,790,240,222'

			-- FINANSE	
			LEFT JOIN	(SELECT	a.Atr_wartosc AS Atr_Wartosc
								,te.Tre_GIDNumer AS Tre_GIDNumer
								,te.Tre_GIDlp AS Tre_GIDlp
								,te.Tre_TwrNazwa AS Tre_TwrNazwa
								,te.Tre_TwrKod AS Tre_TwrKod
								,te.Tre_Ilosc AS Tre_Ilosc
								,te.Tre_RzeczywistaNetto AS Tre_RzeczywistaNetto
								,isnull(te.Tre_KsiegowaBrutto,ze.ZaE_WartoscPoRabacie) AS Tre_KsiegowaBrutto
								,trn.TrN_Waluta AS TrN_Waluta
								,zn.ZaN_DokumentObcy AS ZaN_DokumentObcy
								,trn.TrN_DokumentObcy AS TrN_DokumentObcy
								,trn.TrN_Stan AS TrN_Stan
								,trn.TrN_Zaksiegowano AS TrN_Zaksiegowano
								,dbo.FN_VDataXL(trn.TrN_DataRoz) AS DataRozliczenia
								,dbo.FN_VDataXL(trn.TrN_Data2) AS DataWystawienia
								,zn.ZaN_ZamTyp AS ZaN_ZamTyp
								,dbo.NumerDokumentu(zn.ZaN_GIDTyp,null,zn.ZaN_ZamTyp,zn.ZaN_ZamNumer,zn.ZaN_ZamRok,zn.ZaN_ZamSeria, zn.ZaN_ZamMiesiac) AS nrZam
								,isnull(row_number() over (partition by a.Atr_Wartosc order by TrE_GidNumer),1) wiersz
						FROM	finanse.CDNXL_Starter.CDN.Atrybuty a with(nolock)  
								inner join finanse.CDNXL_Starter.CDN.zamElem ZE with(nolock) on ZE.ZaE_GIDNumer=a.Atr_ObiNumer and ze.ZaE_GIDLp=a.Atr_ObiLp and a.Atr_AtkId=117
								inner join finanse.CDNXL_Starter.CDN.ZamNag zn with(nolock)  on zn.ZaN_gidNumer=ZE.ZaE_GIDNumer
								left join finanse.CDNXL_Starter.CDN.TraSElem tse with(nolock) on tse.TrS_ZlcNumer=ZE.ZaE_GIDNumer and tse.TrS_ZlcLp=ZE.ZaE_GIDLp
								left join finanse.CDNXL_Starter.CDN.TraElem te with(nolock) on te.TrE_GidNUmer=tse.TrS_GIDNumer and te.TrE_GidLp=tse.TrS_GIDLp
								left join finanse.CDNXL_Starter.CDN.TraNag trn with(nolock) on trn.TrN_GIDNumer=te.TrE_GidNUmer  and trn.TrN_Stan<6
						WHERE	zn.ZaN_DataWystawienia>78165   								
								and a.Atr_wartosc is not null 
								and zn.ZaN_ZamTyp=1280
								and te.Tre_GIDlp IS NOT NULL
								and a.Atr_Wartosc = CAST(@attributeId AS nvarchar(50))	
								and zn.ZaN_DokumentObcy LIKE 'A%'
						) xl ON xl.Atr_Wartosc = @attributeId	
			-- END FINANSE

			WHERE av.group_process_instance_id = @groupId

			DECLARE @ReportAllServicesID INT 
			SET @ReportAllServicesID = (SELECT [id] FROM [AtlasDB_rep].[dbo].[ReportAllServices] WITH (NOLOCK) WHERE [rootId] = @rootId AND [groupId] = @groupId AND [entityId] = @entityId)

			IF (ISNULL(@ReportAllServicesID,0)>0)
			BEGIN -- Update
				UPDATE [AtlasDB_rep].[dbo].[ReportAllServices]
					SET	[caseId] = RT.caseId
					  ,[rootId] = RT.rootId
					  ,[groupId] = RT.groupId
					  ,[attributeId] = RT.attributeId
					  ,[ZSinCDN] = RT.ZSinCDN
					  ,[ZZinCDN] = RT.ZZinCDN
					  ,[entityId] = RT.entityId
					  ,[entityName] = RT.entityName
					  ,[quantity] = RT.quantity
					  ,[manufacturer] = RT.manufacturer
					  ,[insurancer] = RT.insurancer
					  ,[partner] = RT.partner
					  ,[dealer] = RT.dealer
					  ,[client] = RT.client
					  ,[openDate] = RT.openDate
					  ,[openTime] = RT.openTime
					  ,[openYear] = RT.openYear
					  ,[openMonth] = RT.openMonth
					  ,[openDay] = RT.openDay
					  ,[openWeek] = RT.openWeek
					  ,[updateAt] = RT.updateAt
					  ,[closeDate] = RT.closeDate
					  ,[closeTime] = RT.closeTime
					  ,[closeYear] = RT.closeYear
					  ,[closeMonth] = RT.closeMonth
					  ,[closeDay] = RT.closeDay
					  ,[closeWeek] = RT.closeWeek
					  ,[closedBy] = RT.closedBy
					  ,[platformId] = RT.platformId
					  ,[platformName] = RT.platformName
					  ,[programId] = RT.programId
					  ,[programName] = RT.programName
					  ,[process] = RT.process
					  ,[caseCountry] = RT.caseCountry
					  ,[caseCity] = RT.caseCity
					  ,[caseStreet] = RT.caseStreet
					  ,[caseCode] = RT.caseCode
					  ,[caseProvince] = RT.caseProvince
					  ,[caseCommune] = RT.caseCommune
					  ,[caseCounty] = RT.caseCounty
					  ,[caseAddressType] = RT.caseAddressType
					  ,[firstRegistrationDate] = RT.firstRegistrationDate
					  ,[mileage] = RT.mileage
					  ,[makeModel] = RT.makeModel
					  ,[DMC] = RT.DMC
					  ,[VIN] = RT.VIN
					  ,[licencePlate] = RT.licencePlate
					  ,[caseReason] = RT.caseReason
					  ,[caseType] = RT.caseType
					  ,[executionAtOnce] = RT.executionAtOnce
					  ,[towingWay] = RT.towingWay
					  ,[returnWay] = RT.returnWay
					  ,[toCustomerWay] = RT.toCustomerWay
					  ,[carType] = RT.carType
					  ,[ARC1] = RT.ARC1
					  ,[ARC2] = RT.ARC2
					  ,[ARC3] = RT.ARC3
					  ,[emptyService] = RT.emptyService
					  ,[repairFailed] = RT.repairFailed
					  ,[serviceStatusId] = RT.serviceStatusId
					  ,[serviceStatus] = RT.serviceStatus
					  ,[performingId] = RT.performingId
					  ,[performingStarterCode] = RT.performingStarterCode
					  ,[performingRKS] = RT.performingRKS
					  ,[performingName] = RT.performingName
					  ,[performingCountry] = RT.performingCountry
					  ,[performingCity] = RT.performingCity
					  ,[performingStreet] = RT.performingStreet
					  ,[performingPostCode] = RT.performingPostCode
					  ,[performingBuildingNr] = RT.performingBuildingNr
					  ,[performingApartmentNr] = RT.performingApartmentNr
					  ,[rentDays] = RT.rentDays
					  ,[rentModel] = RT.rentModel
					  ,[rentFirstname] = RT.rentFirstname
					  ,[rentLastname] = RT.rentLastname
					  ,[rentPrice] = RT.rentPrice
					  ,[rentReturnPrice] = RT.rentReturnPrice
					  ,[rentSubstitutionPrice] = RT.rentSubstitutionPrice
					  ,[declarant] = RT.declarant
					  ,[declarantPhone] = RT.declarantPhone
					  ,[driver] = RT.driver
					  ,[driverPhone] = RT.driverPhone
					  ,[NrZleceniaPartneraZagr] = NULL
					  ,[NrZleceniaZewnetrznejAplikacji] = NULL
					  
					  -- FINANSE
					  ,[Tre_GIDNumer] = RT.Tre_GIDNumer
					  ,[Tre_GIDlp] = RT.Tre_GIDlp
					  ,[Tre_TwrNazwa] = RT.Tre_TwrNazwa
					  ,[Tre_TwrKod] = RT.Tre_TwrKod
					  ,[Tre_Ilosc] = RT.Tre_Ilosc
					  ,[Tre_RzeczywistaNetto] = RT.Tre_RzeczywistaNetto
					  ,[Tre_KsiegowaBrutto] = RT.Tre_KsiegowaBrutto
					  ,[TrN_Waluta] = RT.TrN_Waluta
					  ,[ZaN_DokumentObcy] = RT.ZaN_DokumentObcy
					  ,[TrN_DokumentObcy] = RT.TrN_DokumentObcy
					  ,[TrN_Stan] = RT.TrN_Stan
					  ,[TrN_Zaksiegowano] = RT.TrN_Zaksiegowano
					  ,[DataRozliczenia] = RT.DataRozliczenia
					  ,[DataWystawienia] = RT.DataWystawienia
					  ,[ZaN_ZamTyp] = RT.ZaN_ZamTyp
					  ,[nrZam] = RT.nrZam
					  ,[wiersz] = RT.wiersz
					  -- END FINANSE
					  
					  ,[synchronizationDate] = RT.synchronizationDate
					FROM @ReportTable AS RT
					WHERE [id] = @ReportAllServicesID
			END
			ELSE
			BEGIN -- CREATE
				INSERT INTO [AtlasDB_rep].[dbo].[ReportAllServices]
				   ([caseId]
				   ,[rootId]
				   ,[groupId]
				   ,[attributeId]
				   ,[ZSinCDN]
				   ,[ZZinCDN]
				   ,[entityId]
				   ,[entityName]
				   ,[quantity]
				   ,[manufacturer]
				   ,[insurancer]
				   ,[partner]
				   ,[dealer]
				   ,[client]
				   ,[openDate]
				   ,[openTime]
				   ,[openYear]
				   ,[openMonth]
				   ,[openDay]
				   ,[openWeek]
				   ,[updateAt]
				   ,[closeDate]
				   ,[closeTime]
				   ,[closeYear]
				   ,[closeMonth]
				   ,[closeDay]
				   ,[closeWeek]
				   ,[closedBy]
				   ,[platformId]
				   ,[platformName]
				   ,[programId]
				   ,[programName]
				   ,[process]
				   ,[caseCountry]
				   ,[caseCity]
				   ,[caseStreet]
				   ,[caseCode]
				   ,[caseProvince]
				   ,[caseCommune]
				   ,[caseCounty]
				   ,[caseAddressType]
				   ,[firstRegistrationDate]
				   ,[mileage]
				   ,[makeModel]
				   ,[DMC]
				   ,[VIN]
				   ,[licencePlate]
				   ,[caseReason]
				   ,[caseType]
				   ,[executionAtOnce]
				   ,[towingWay]
				   ,[returnWay]
				   ,[toCustomerWay]
				   ,[carType]
				   ,[ARC1]
				   ,[ARC2]
				   ,[ARC3]
				   ,[emptyService]
				   ,[repairFailed]
				   ,[serviceStatusId]
				   ,[serviceStatus]
				   ,[performingId]
				   ,[performingStarterCode]
				   ,[performingRKS]
				   ,[performingName]
				   ,[performingCountry]
				   ,[performingCity]
				   ,[performingStreet]
				   ,[performingPostCode]
				   ,[performingBuildingNr]
				   ,[performingApartmentNr]
				   ,[rentDays]
				   ,[rentModel]
				   ,[rentFirstname]
				   ,[rentLastname]
				   ,[rentPrice]
				   ,[rentReturnPrice]
				   ,[rentSubstitutionPrice]
				   ,[declarant]
				   ,[declarantPhone]
				   ,[driver]
				   ,[driverPhone]
				   
				   -- Finanse
				   ,[Tre_GIDNumer]
				   ,[Tre_GIDlp]
				   ,[Tre_TwrNazwa]
				   ,[Tre_TwrKod]
				   ,[Tre_Ilosc]
				   ,[Tre_RzeczywistaNetto]
				   ,[Tre_KsiegowaBrutto]
				   ,[TrN_Waluta]
				   ,[ZaN_DokumentObcy]
				   ,[TrN_DokumentObcy]
				   ,[TrN_Stan]
				   ,[TrN_Zaksiegowano]
				   ,[DataRozliczenia]
				   ,[DataWystawienia]
				   ,[ZaN_ZamTyp]
				   ,[nrZam]
				   ,[wiersz]
				   -- End Finanse
				   
				   ,[synchronizationDate]
					)
			 SELECT [caseId]
				   ,[rootId]
				   ,[groupId]
				   ,[attributeId]
				   ,[ZSinCDN]
				   ,[ZZinCDN]
				   ,[entityId]
				   ,[entityName]
				   ,[quantity]
				   ,[manufacturer]
				   ,[insurancer]
				   ,[partner]
				   ,[dealer]
				   ,[client]
				   ,[openDate]
				   ,[openTime]
				   ,[openYear]
				   ,[openMonth]
				   ,[openDay]
				   ,[openWeek]
				   ,[updateAt]
				   ,[closeDate]
				   ,[closeTime]
				   ,[closeYear]
				   ,[closeMonth]
				   ,[closeDay]
				   ,[closeWeek]
				   ,[closedBy]
				   ,[platformId]
				   ,[platformName]
				   ,[programId]
				   ,[programName]
				   ,[process]
				   ,[caseCountry]
				   ,[caseCity]
				   ,[caseStreet]
				   ,[caseCode]
				   ,[caseProvince]
				   ,[caseCommune]
				   ,[caseCounty]
				   ,[caseAddressType]
				   ,[firstRegistrationDate]
				   ,[mileage]
				   ,[makeModel]
				   ,[DMC]
				   ,[VIN]
				   ,[licencePlate]
				   ,[caseReason]
				   ,[caseType]
				   ,[executionAtOnce]
				   ,[towingWay]
				   ,[returnWay]
				   ,[toCustomerWay]
				   ,[carType]
				   ,[ARC1]
				   ,[ARC2]
				   ,[ARC3]
				   ,[emptyService]
				   ,[repairFailed]
				   ,[serviceStatusId]
				   ,[serviceStatus]
				   ,[performingId]
				   ,[performingStarterCode]
				   ,[performingRKS]
				   ,[performingName]
				   ,[performingCountry]
				   ,[performingCity]
				   ,[performingStreet]
				   ,[performingPostCode]
				   ,[performingBuildingNr]
				   ,[performingApartmentNr]
				   ,[rentDays]
				   ,[rentModel]
				   ,[rentFirstname]
				   ,[rentLastname]
				   ,[rentPrice]
				   ,[rentReturnPrice]
				   ,[rentSubstitutionPrice]
				   ,[declarant]
				   ,[declarantPhone]
				   ,[driver]
				   ,[driverPhone]
				   
				   -- Finanse
				   ,[Tre_GIDNumer]
				   ,[Tre_GIDlp]
				   ,[Tre_TwrNazwa]
				   ,[Tre_TwrKod]
				   ,[Tre_Ilosc]
				   ,[Tre_RzeczywistaNetto]
				   ,[Tre_KsiegowaBrutto]
				   ,[TrN_Waluta]
				   ,[ZaN_DokumentObcy]
				   ,[TrN_DokumentObcy]
				   ,[TrN_Stan]
				   ,[TrN_Zaksiegowano]
				   ,[DataRozliczenia]
				   ,[DataWystawienia]
				   ,[ZaN_ZamTyp]
				   ,[nrZam]
				   ,[wiersz]
				   -- End Finanse
				   
				   ,[synchronizationDate]
					FROM @ReportTable 
			END
		END
		

------------------------------------------------------------------------------------------
			--	END
			--END

			FETCH NEXT FROM Matrix_Cursor INTO @groupId, @attributeId ,@entityId, @quantity, @manufacturer, @insurancer, @partner, @dealer, @client
		END
		CLOSE Matrix_Cursor;
		DEALLOCATE Matrix_Cursor;
	
	--	FETCH NEXT FROM Service_Cursor INTO @groupId
	--END

	--CLOSE Service_Cursor;
	--DEALLOCATE Service_Cursor;

	FETCH NEXT FROM Root_Cursor INTO @rootId
END

CLOSE Root_Cursor;
DEALLOCATE Root_Cursor;

END -- Procedure End
