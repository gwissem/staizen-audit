ALTER PROCEDURE [dbo].[s_1009_064]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int = 1,
	@errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId VARCHAR(32)
	DECLARE @responseId INT
	DECLARE @startCase DATETIME
	DECLARE @fixingOrTowing INT
	
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	-- Sprawdzenie czy zadanie jest przekładane
	
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '767,773,768', @groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @responseId = value_int FROM @values
	
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	
 	SET @variant = 1
 	
 	IF @responseId = 1 AND @fixingOrTowing = 1
 	BEGIN
	 
	 	-- Zmiana z holowania na naprawę
	 	
	 	EXEC dbo.p_attribute_edit
   			@attributePath = '723',
   			@groupProcessInstanceId = @rootId,
   			@stepId = 'xxx',
   			@valueInt = 2,
   			@err = @err OUTPUT,
   			@message = @message OUTPUT
	 	
 	END
 	IF @responseId = 2
 	BEGIN
	 	-- Odholowano, przejście do dodatkowych danych 
	 	SET @variant = 2
	 	
	 	-- Zmiana z naprawy na holowanie
	 	IF @fixingOrTowing = 2
	 	BEGIN
		 	EXEC dbo.p_attribute_edit
	   			@attributePath = '723',
	   			@groupProcessInstanceId = @rootId,
	   			@stepId = 'xxx',
	   			@valueInt = 1,
	   			@err = @err OUTPUT,
	   			@message = @message OUTPUT
	 	END

-- 	POPR 674
		DECLARE  @nextProcessInstanceId int
		EXEC [dbo].[p_process_new] --dodanie monitoringu naprawy warsztatowej
				@stepId = '1021.017',
				@userId = 1,
				@originalUserId = 1,
				@parentProcessId = @rootId,
				@rootId = @rootId,
				@err = @err OUTPUT,
				@message = @message OUTPUT,
				@processInstanceId = @nextProcessInstanceId OUTPUT

	END
 	ELSE IF @responseId = 1
 	BEGIN
	 	
	 	-- Warunek i wysłanie sms'a 
	 	
	 	DECLARE @hourStartCase SMALLINT
	 	DECLARE @clientNumber VARCHAR(32)
	 	DECLARE @platformName VARCHAR(32)
	 	DECLARE @platformId INT
	 	DECLARE @content VARCHAR(MAX)
	 	
	 	SELECT @startCase = created_at FROM dbo.process_instance with(nolock) WHERE id = @rootId
	 	SELECT @hourStartCase = DATEPART(HOUR, @startCase);

	 	-- minęło mniej niż 4 godziny od utworzenia sprawy i (!23-7)
	 	IF DATEADD(day,-4,GETDATE()) < @startCase AND (@hourStartCase < 23 AND @hourStartCase >= 7)
	 	BEGIN
		 	
		 	INSERT  @values EXEC dbo.p_attribute_get2
		    		@attributePath = '80,342,408,197',
		    		@groupProcessInstanceId = @groupProcessInstanceId
		    SELECT @clientNumber = value_string FROM @values
		 	
		    DELETE FROM @values
			INSERT  @values EXEC dbo.p_attribute_get2
				@attributePath = '253',
				@groupProcessInstanceId = @groupProcessInstanceId
			SELECT @platformId = value_int FROM @values
		
			SELECT @platformName = name FROM dbo.platform with(nolock) where id = @platformId
		
		    SET @content = dbo.f_translate('Otrzymaliśmy informacje o zakończeniu naprawy na miejscu. Dziękujemy za skorzystanie z naszej pomocy Assistance. W przypadku dodatkowych życzeń, prosimy o ponowny kontakt z naszą infolinią. ',default)
--		    SET @content = dbo.f_translate('Tu Starter24. Propozycja zlecenia na ',default)+dbo.f_conditionalText(@fixingOrTowing,dbo.f_translate('holowanie',default),dbo.f_translate('naprawę',default))+dbo.f_translate(' dla sprawy ',default)+dbo.f_caseId(@previousProcessId)+dbo.f_translate(' w ICS jest juz nieaktualna ze względu na brak reakcji w ciągu 2 minut.',default)
					

		    PRINT dbo.f_translate('Send SMS',default)
			PRINT dbo.f_translate('clientNumber: ',default) + CAST(@clientNumber AS VARCHAR(200))

		
		    -- TODO Wysłanie SMS'a do klienta
			EXEC p_note_new
				@groupProcessId = @groupProcessInstanceId,
				@type = dbo.f_translate('sms',default),
				@content = @content,
				@phoneNumber = @clientNumber,
				@userId = @currentUser,
				@originalUserId = @currentUser,
				@addInfo = 1,
				@err = @err OUTPUT,
				@message = @message OUTPUT
		    
	 	END
	 	
 	END
 	ELSE IF @responseId = 5
 	BEGIN
	
	 	SET @variant = 3
	 	
	END
 	
END