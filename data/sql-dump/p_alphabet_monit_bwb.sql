ALTER PROCEDURE [dbo].[p_alphabet_monit_bwb]
    @serviceId INT,
    @rootId int
AS
  BEGIN
    DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

    DECLARE @err int
    DECLARE @message NVARCHAR(400)


    DECLARE @companyUsingCar nvarchar(500)
    DECLARE @regNumber nvarchar(20)

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @rootId
    SELECT @regNumber = value_string from @values

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '1000', @groupProcessInstanceId = @rootId
    SELECT @companyUsingCar = value_string from @values

    DECLARE @mailTo nvarchar(500)
    DECLARE @valueMailConfig NVARCHAR(255)
    DECLARE @descriptionMailConfig NVARCHAR(255)
    EXEC dbo.p_get_business_config
        @key = 'bwb.request_veryfication_email',
        @groupProcessInstanceId =@rootId,
        @value = @valueMailConfig OUTPUT,
        @description = @descriptionMailConfig OUTPUT

    SELECT @mailTo = [dbo].[f_getRealEmailOrTest]('serwis@alphabet.pl') +', '+[dbo].[f_getRealEmailOrTest]('szkody@alphabet.pl')
    IF @valueMailConfig <> ''
      BEGIN
        SET @mailTo=dbo.f_getRealEmailOrTest(@valueMailConfig)
      END

    DECLARE @subject nvarchar(500)
    DECLARE @body nvarchar(MAX)

    DECLARE @services nvarchar(max)

    EXEC p_running_services @groupProcessInstanceId = @rootId, @servicesIds = @services output
    print @services

    DECLARE @serviceIdCursor INT
    DECLARE @statusName nvarchar(200)
    DECLARE @servicesText NVARCHAR(MAX)
    SET @servicesText =''

    DECLARE kur cursor LOCAL for
      SELECT cast(data as int) FROM dbo.f_split(@services,',')
    OPEN kur;
    FETCH NEXT FROM kur INTO @serviceIdCursor;
    WHILE @@FETCH_STATUS=0
      BEGIN
        print @serviceIdCursor
        IF @serviceIdCursor !=22
          BEGIN
        SELECT  @statusName = t.name from service_definition join service_definition_translation t on service_definition.id = t.translatable_id where locale ='pl' and service_definition.id = @serviceIdCursor

        print @statusName
        if isnull(@statusName,'') <> ''
          BEGIN
            SET @servicesText = @servicesText +dbo.f_translate('Dla Klienta zorganizowano ',default) + isnull(@statusName,'')+'. <br />'
          end
        END


        -- WHILE END
    FETCH NEXT FROM kur INTO @serviceIdCursor;
  END
  CLOSE kur
  DEALLOCATE kur




    SET @subject = isnull(@regNumber,'')+dbo.f_translate('  BRAK W BAZIE PROŚBA O POTWIERDZENIE UPRAWNIEŃ',default)

    SET @body = 'Szanowni Państwo <BR>' +
                dbo.f_translate('skontaktował się z nami użytkownik pojazdu ',default)+isnull(@regNumber,'') +'. Prosi o wezwanie pomocy assistance. <BR>'+
                dbo.f_translate('Pojazd nie widnieje w bazie uprawnień. Użytkownik jest z firmy ',default)+isnull(@companyUsingCar,'')+'. <BR>' +
                'Prosimy o potwierdzenie możliwości organizacji pomocy oraz przekazanie danych pojazdy (w tym VIN). '


    IF isnull(@servicesText,'') <> ''
      BEGIN
        SET @body = @body +'<br><br>' + @servicesText + '<br><br>'
    end


    DECLARE @subType nvarchar(50) = dbo.f_translate('bwb',default)
    SET @subType = @subType+cast(@serviceId as nvarchar(5))
    print @subType

    EXEC p_note_new
        @groupProcessId =@rootId,
        @type =dbo.f_translate('email',default),
        @content = dbo.f_translate('Wysłano zapytanie o BWB do Alphabet',default),
        @phoneNumber = null ,
        @email = @mailTo,
        @subType = @subType ,
        @subject = @subject,
        @direction = 1,
        @emailRegards = 1,
        @err = @err OUTPUT,
        @message = @message OUTPUT,
        -- FOR EMAIL
        @dw = '',
        @udw = '',
        @sender = 'cfm@starter24.pl',
        @additionalAttachments = '',
        @emailBody  = @body
    END