ALTER PROCEDURE [dbo].[P_summary_mercedes_case_email]
@rootId INT,
@overrideEmail NVARCHAR(100) = NULL
AS
begin
	
	PRINT '----------------------- EXEC dbo.P_summary_mercedes_case_email --------------------------'
	
	---------------------------------------------------------------------------------------------
	
	-- PROCEDURA WYSYŁA EMAIL JAKO PODSUMOWANIE SPRAWY DO PARTNERA ZAGRACZNICNEGO (TYLKO PLATFROAM MERCEDES) ----

	---------------------------------------------------------------------------------------------
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	 
	DECLARE @contentTitle NVARCHAR(200) 
	
	DECLARE @caseNumberAtlas NVARCHAR(30)
	DECLARE @caseNumberPZ NVARCHAR(30)
	DECLARE @programName NVARCHAR(100)
	DECLARE @programId NVARCHAR(30)
	DECLARE @fdds INT
	DECLARE @createdDateCase DATETIME
	DECLARE @makeModel NVARCHAR(100)
	DECLARE @makeModelId INT
	DECLARE @regNumber NVARCHAR(100)
	DECLARE @vin NVARCHAR(20)
	DECLARE @mileage INT
	DECLARE @diagnosisDescription NVARCHAR(400)
	DECLARE @diagnosisCode NVARCHAR(20)
	DECLARE @emailContent NVARCHAR(MAX)
	DECLARE @pzName NVARCHAR(200)
	DECLARE @pzId INT
	DECLARE @ringGroupProcess INT
	DECLARE @contactEmailPz NVARCHAR(100)
	DECLARE @dateFirstReg DATETIME
	DECLARE @locationString NVARCHAR(1000)
	DECLARE @caseDetail NVARCHAR(200) = ''
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @rootId
 	SELECT @pzId = value_int FROM @values
	
-- 	IF @pzId IS NULL
-- 	BEGIN
--	 	
--		SELECT @ringGroupProcess = group_process_id FROM dbo.process_instance WHERE root_id = @rootId AND step_id like '1012%'
--		
--		DELETE FROM @values
--		INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @ringGroupProcess
--		SELECT @pzId = value_int FROM @values
--		 	
-- 	END
--
-- 	SET @pzName = dbo.f_partnerName(@pzId)
 	
-- 	SET @contactEmailPz = [dbo].[f_partner_contact](@rootId, @pzId, 0, 4)
 	
 	SET @contactEmailPz = [dbo].[f_getRealEmailOrTest]('cs.pol@cac.mercedes-benz.com')
 	
	--	1. Numery sprawy – partnera i Starter24

	SET @caseNumberAtlas = dbo.[f_caseId](@rootId)
	
	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '711', @groupProcessInstanceId = @rootId
 	SELECT @caseNumberPZ = value_string FROM @values
 
	--	2. Data i godzina zarejestrowania sprawy

 	SELECT TOP 1 @createdDateCase = created_at FROM dbo.process_instance WHERE root_id = @rootId ORDER BY id ASC
 		
	--	3. Program – nazwa i numer FDDS
 		
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
 	SELECT @programName = [dbo].[f_program_names](value_string) FROM @values
 	
 	DELETE FROM @values
	INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '712', @groupProcessInstanceId = @rootId
 	SELECT @fdds = value_string FROM @values
 	
	--	4. Pojazd – marka i model, nr rejestracyjny, VIN, przebieg

 	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @rootId
	SELECT @makeModelId = value_int FROM @values
	
 	SELECT @makeModel = textD FROM dbo.dictionary
	WHERE typeD='makeModel' AND value = @makeModelId AND active = 1
	
 	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @rootId
	SELECT @VIN = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @rootId
	SELECT @regNumber = value_string FROM @values
 	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @rootId
	SELECT @mileage = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,233', @groupProcessInstanceId = @rootId
	SELECT @dateFirstReg = value_date FROM @values
	
	--	5. Zdarzenie – opis diagnozy EN i kod ARC
	
	SET @diagnosisDescription = [dbo].[f_diagnosis_description](@rootId, 'en')
	SET @diagnosisCode = [dbo].[f_diagnosis_code](@rootId)
	
	SET @contentTitle = 'Feedback form to <br><span style="color: #526197;">Mercedes-Benz Customer Assistance Center Maastricht N.V.</span>'
	
	
	DECLARE @fixingOrTowingGroupProcessInstanceId INT
	DECLARE @fixingOrTowing INT
	DECLARE @url NVARCHAR(1000)
	DECLARE @successFixing NVARCHAR(5) = ''
	DECLARE @completeTemporarilyOrPermanently NVARCHAR(100)
	DECLARE @workshopId INT -- 522
	DECLARE @workshopCode NVARCHAR(100) = ''
	DECLARE @onlySecondaryBenefitsOrg NVARCHAR(5) = ''
	DECLARE @time_of_arrival DATETIME
	DECLARE @time_of_completion DATETIME
	
	SELECT @fixingOrTowing = service_id, @fixingOrTowingGroupProcessInstanceId = group_process_instance_id FROM [dbo].[f_complete_towing_or_fixing_info](@rootId)
	
	-- ZBUDOWANIE URL'a
	
	
	IF @fixingOrTowing IS NULL
	BEGIN
		SET @fixingOrTowing = 0
		SET @onlySecondaryBenefitsOrg = '1'
	END
	
	IF @fixingOrTowing = 2
	BEGIN
		
		EXEC [dbo].[p_rsa_finnish_type] @groupProcessInstanceId = @fixingOrTowingGroupProcessInstanceId, @type = @completeTemporarilyOrPermanently OUTPUT

		IF @completeTemporarilyOrPermanently = dbo.f_translate('CompletedPermanently',default)
		BEGIN
			SET @successFixing = '1'
		END
		ELSE 
		BEGIN
			SET @successFixing = '0'
		END
		
	END
	
	-- Actual Time of Arrival
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,214', @groupProcessInstanceId = @fixingOrTowingGroupProcessInstanceId
	SELECT @time_of_arrival = value_date FROM @values
	
	-- Actual Time of Completion
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,130', @groupProcessInstanceId = @fixingOrTowingGroupProcessInstanceId
	SELECT @time_of_completion = value_date FROM @values
	
	-- // Workshop ID
	
	IF @fixingOrTowing = 1
	BEGIN
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @fixingOrTowingGroupProcessInstanceId
		SELECT @workshopId = value_int FROM @values
		
		SELECT @workshopCode = value_string FROM dbo.attribute_value WHERE attribute_path = '595,597,630' AND parent_attribute_value_id = @workshopId
		
	END
	
	-- co to jest "	ONLY Secondary benefits organisation" ?
	
	SET @url = 'modern_email_mercedes?fixing_or_towing=' + CONVERT(VARCHAR(5), @fixingOrTowing) + '&repaired_permanently=' + CONVERT(VARCHAR(5), @successFixing) + '&only_secondary_benefits_org=' + @onlySecondaryBenefitsOrg 
	
	-- Pobranie Kontentu e-maila
	EXEC [dbo].[P_get_email_template] @name = @url, @responseText = @emailContent OUTPUT
	
	DECLARE @content NVARCHAR(MAX)
	
	----------------------------------
	-- Początek budowania kontentu
	---------------------------------- 

	
	-- DATE
	
	SET @emailContent = REPLACE(@emailContent,'__DATE__', [dbo].[FN_VDateHour](GETDATE()));	
	
	-- __CASE_DETAIL__
	
	SET @emailContent = REPLACE(@emailContent,'__case_detail__', ISNULL(@caseDetail, ''));

	-- __SERVICE_PARTNER_NAME__
	
	SET @emailContent = REPLACE(@emailContent,'__SERVICE_PARTNER_NAME__', ISNULL('STARTER 24', ''));
	
	-- case NUMBERS 
	
	SET @emailContent = REPLACE(@emailContent,'__CAC_CASE_NUMBER__', ISNULL(@caseNumberPZ, ''));
	SET @emailContent = REPLACE(@emailContent,'__OUR_CASE_NUMBER__', ISNULL(@caseNumberAtlas, ''));
	
	-- case detail
	
	-- // todo CO TUTAJ
	
	-- vin
	
	SET @emailContent = REPLACE(@emailContent,'__VIN__', ISNULL(@VIN, ''));
	
	-- __date_of_1st_registration__
	
	SET @emailContent = REPLACE(@emailContent,'__date_of_1st_registration__', [dbo].[FN_VDateHour](@dateFirstReg));

	-- __Regplate__
	
	SET @emailContent = REPLACE(@emailContent,'__Regplate__', ISNULL(@regNumber, ''));
	
	-- __mileage__

	SET @emailContent = REPLACE(@emailContent,'__mileage__', ISNULL(@mileage, ''));

	-- __breakdown_location__

	EXEC [dbo].[p_location_string] @attributePath = '101,85', @groupProcessInstanceId = @rootId, @locationString = @locationString OUTPUT
	SET @emailContent = REPLACE(@emailContent,'__breakdown_location__', ISNULL(@locationString, ''));

	-- __FDDS_number__

	SET @emailContent = REPLACE(@emailContent,'__FDDS_number__', ISNULL(@fdds, ''));

	-- workshop_id
	
	SET @emailContent = REPLACE(@emailContent,'__workshopID__', ISNULL(@workshopCode, ''));

	-- Actual Time of Arrival, Actual Time of Completion
	
	SET @emailContent = REPLACE(@emailContent,'__time_of_arrival__', [dbo].[FN_VDateHour](@time_of_arrival));
	SET @emailContent = REPLACE(@emailContent,'__time_of_completion__',[dbo].[FN_VDateHour](@time_of_completion));

	-- DIAGNOSIS
	
	DECLARE @code1 NVARCHAR(50)
	DECLARE @code1Desc NVARCHAR(200) = ''
	DECLARE @code2 NVARCHAR(50)	
	DECLARE @code2Desc NVARCHAR(200) = ''
	DECLARE @code3 NVARCHAR(50)	
	DECLARE @code3Desc NVARCHAR(200) = ''
	
	-- __component_code__  638,720
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,720', @groupProcessInstanceId = @fixingOrTowingGroupProcessInstanceId
	SELECT @code1 = argument2 FROM dictionary WHERE active =1 and value = (SELECT value_int FROM @values) and typeD = 'ARCCode1' 
	
-- 	SELECT @code1Desc = textD FROM dictionary WHERE active =1 and value = @code1 and typeD = 'ARCCode1'
 	
	-- __fault_code__ 638,721
	
 	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,721', @groupProcessInstanceId = @fixingOrTowingGroupProcessInstanceId
	SELECT @code2 = argument2 FROM dictionary WHERE active =1 and value = (SELECT value_int FROM @values) and typeD = 'ARCCode2'
	
-- 	SELECT @code2Desc = textD FROM dictionary WHERE active =1 and value = @code2 and typeD = 'ARCCode2'
 	
	-- __completion_code__ 638,722
	
 	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,722', @groupProcessInstanceId = @fixingOrTowingGroupProcessInstanceId
	SELECT @code3 = argument2 FROM dictionary WHERE active =1 and value = (SELECT value_int FROM @values) and typeD = 'ARCCode3' 
	
-- 	SELECT @code3Desc = textD FROM dictionary WHERE active =1 and value = @code3 and typeD = 'ARCCode3'

 	
	SET @emailContent = REPLACE(@emailContent,'__component_code__', ISNULL(@code1, ''));
	SET @emailContent = REPLACE(@emailContent,'__component_code_desc__', ISNULL(@code1Desc, ''));

	SET @emailContent = REPLACE(@emailContent,'__fault_code__', ISNULL(@code2, ''));
	SET @emailContent = REPLACE(@emailContent,'__fault_code_desc__', ISNULL(@code2Desc, ''));

	SET @emailContent = REPLACE(@emailContent,'__completion_code__', ISNULL(@code3, ''));
	SET @emailContent = REPLACE(@emailContent,'__completion_code_desc__', ISNULL(@code3Desc, ''));

	-- Opis diagnozy po EN

	SET @emailContent = REPLACE(@emailContent,'__diagnosis_description__', ISNULL(@diagnosisDescription, ''));

	-- COMMENTS
	
	-- //todo - skąd to brać?
	SET @emailContent = REPLACE(@emailContent,'__comments__', '');
	
	
	SET @emailContent = REPLACE(@emailContent,'__CONTENT_TITLE__', @contentTitle)
    SET @emailContent = REPLACE(@emailContent,'__TITLE__','Feedback form') 
    
--	SELECT @emailContent

	--  Stworzenie nowego procesu Przychodzi Mail
--
	 DECLARE @email VARCHAR(400)
	 DECLARE @sender VARCHAR(400) = dbo.f_getEmail('arc')
	 DECLARE @subject VARCHAR(400)
	 DECLARE @emailPDF INT
	 DECLARE @attach NVARCHAR(200)
	 
	 -- Case summary: [Nr sprawy partnera] / [nr sprawy starter] / [nr rejestracyjny]
	 
	 SET @subject = dbo.f_translate('Case summary: ',default) + ISNULL(@caseNumberPZ, '') + ' / ' + ISNULL(@caseNumberAtlas, '') + ' / ' + ISNULL(@regNumber, '')
	 
	 IF @overrideEmail IS NOT NULL
	 BEGIN
		 SET @contactEmailPz = @overrideEmail
	 END
	 
	EXEC p_attribute_edit
		@attributePath = '959',
		@groupProcessInstanceId = @rootId,
		@stepId = 'xxx',
		@valueText = @emailContent,
		@err = @err OUTPUT,
		@message = @message OUTPUT
			
		
	PRINT '@subject'
	PRINT @subject
	
	PRINT '@contactEmailPz'
	PRINT  @contactEmailPz
		
	PRINT '@sender'
	PRINT @sender
	
--	SELECT TOP 1 @emailPDF = id FROM dbo.attribute_value WHERE attribute_path = '959' AND group_process_instance_id = @rootId
--	SET @attach = '{ATTRIBUTE::' + CONVERT(NVARCHAR(100), @emailPDF) + dbo.f_translate('::Feedback form of case ',default) + @caseNumberAtlas + '::false}'

	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @rootId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = @subject
--	 	,@additionalAttachments = @attach
	 	,@email = @contactEmailPz
	 	,@userId = 1  -- automat
	 	,@subject = @subject
	 	,@direction = 1
	 	,@sender = @sender
	 	,@emailBody = @emailContent
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT
--		
	
	PRINT '---------------------- END dbo.P_summary_mercedes_case_email -------------------------------'
	
end
