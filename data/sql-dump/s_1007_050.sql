ALTER PROCEDURE [dbo].[s_1007_050]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @content NVARCHAR(4000)
	DECLARE @platformId INT
	DECLARE @programId NVARCHAR(200)
	DECLARE @rentalDuration INT
	DECLARE @limitText NVARCHAR(100)
	DECLARE @makeModel NVARCHAR(200)
	DECLARE @clientCustomDate INT	
	DECLARE @eta DATETIME
	DECLARE @verification INT
	DECLARE @partnerId INT
	DECLARE @fixingPartnerId INT
	DECLARE @towingFixingGroup INT
	DECLARE @rootId INT
	DECLARE @dc int 
	DECLARE @platformGroup NVARCHAR(255)
	
	SELECT	@groupProcessInstanceId = group_process_id, @rootId = root_id	
	FROM process_instance with(nolock)
	WHERE id = @previousProcessId 
	
	EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '80,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = textD FROM dbo.dictionary where value = (SELECT value_int FROM @values) and typeD = 'makeModel' 

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '215', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @clientCustomDate = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '789,786,240,153', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @rentalDuration = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	IF @platformGroup = dbo.f_translate('CFM',default)
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
		SELECT @programId = value_string FROM @values
		
		SELECT @platformId = platform_id from dbo.vin_program with(nolock) where id = @programId
	END 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '540', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '573', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @verification = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values
	
--	SELECT TOP 1 @towingFixingGroup = group_process_id FROM dbo.process_instance with(nolock) WHERE step_id LIKE '1009.%' AND root_id = @rootId ORDER BY id desc
	
	SELECT @towingFixingGroup = [dbo].[f_service_top_progress_group](@rootId, 12)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towingFixingGroup
	SELECT @fixingPartnerId = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '802', @groupProcessInstanceId = @rootId
	SELECT @dc = value_int FROM @values
	
	DECLARE @isExtra INT 
	SELECT @isExtra = is_extra FROM dbo.vin_program with(nolock) where id = @programId
	
	SET @variant = 1
	
	IF @partnerId = @fixingPartnerId
	BEGIN
		SET @variant = 2
	END 
	ELSE
	BEGIN
		
		SET @content = dbo.f_translate('Potwierdzamy zorganizowanie samochodu zastępczego',default)+IIF(@platformId <> 53,ISNULL(': '+@makeModel,''),'')+'.'
		
		IF ISNULL(@clientCustomDate,0) = 0
		BEGIN
			SET @content = @content + dbo.f_translate(' Szacowany czas jego dostarczenia to ',default)+dbo.FN_VDateHour(@eta)+'.'
		END 
		ELSE
		BEGIN		
			SET @content = @content + dbo.f_translate(' Przybycie zostało umówione na ',default)+dbo.FN_VDateHour(@eta)+'.'
		END 
	
		DECLARE @daysLimitText nvarchar(100)
		DECLARE @days INT
		DECLARE @workingDaysOnly INT
		
		EXEC [dbo].[p_service_day_limit] 
		@groupProcessInstanceId = @groupProcessInstanceId, 
		@serviceId = 3,
		@programId = @programId,
		@max = 1,
		@days = @days OUTPUT,
		@workingDaysOnly = @workingDaysOnly OUTPUT
		
		DECLARE @calendarDays INT = @days
		
		IF ISNULL(@workingDaysOnly,0) = 1
		BEGIN			
			SELECT @calendarDays = [dbo].[f_working_to_calendar_days](@days ,@eta)
		END 
		
		IF @days < 100
		BEGIN
			SELECT @daysLimitText = CAST(@days AS NVARCHAR(100)) + dbo.f_translate(' dni ',default) + IIF(@workingDaysOnly = 1, dbo.f_translate('roboczych',default), dbo.f_translate('kalendarzowych',default))	
		END 
		
	    SET @content = @content + dbo.f_translate(' Samochód został zorganizowany na ',default)+ISNULL(CAST(@rentalDuration AS NVARCHAR(10))+dbo.f_translate(' dni',default),'')+'.'
	    
	    IF @programId IN (516,518,519,520,521)
		BEGIN
			SET @content = @content + dbo.f_translate(' W przypadku nadal trwającej naprawy warsztatowej będziemy systematycznie przedłużać wynajem.',default)
		END 
	    ELSE IF @rentalDuration = @calendarDays OR @dc = 1
	    BEGIN
		    IF @platformGroup = dbo.f_translate('PSA',default)
			BEGIN
				SET @content = @content + dbo.f_translate(' W celu uzyskania informacji na temat przedłużenia wynajmu dla naprawy trwającej powyżej tego okresu prosimy o kontakt z serwisem wykonującym naprawę.',default)
			END 
			ELSE
			BEGIN
				SET @content = @content + dbo.f_translate(' Jest to maksymalny okres wynajmu w ramach assistance.',default)	
			END 		    
	    END     
		ELSE IF @verification = -1
		BEGIN
			SET @content = @content + dbo.f_translate(' Po potwierdzeniu uprawnień, w razie nadal trwającej naprawy warsztatowej będziemy systematycznie przedłużać wynajem, aż do limitu ',default)+ ISNULL(@daysLimitText,'')+dbo.f_translate(', przewidzianego warunkami assistance.',default)
		END
		ELSE IF ISNULL(@isExtra,0) = 0
		BEGIN
			IF @platformGroup = dbo.f_translate('PSA',default)
			BEGIN
				SET @content = @content + dbo.f_translate(' W przypadku nadal trwajacej naprawy warsztatowej wynajem zostanie wydłużony do 4 dni roboczych. W celu uzyskania informacji na temat przedłużenia wynajmu dla naprawy trwającej powyżej tego okresu prosimy o kontakt z serwisem wykonującym napr',default)
			END 
			ELSE
			BEGIN
				SET @content = @content + dbo.f_translate(' W przypadku nadal trwającej naprawy warsztatowej będziemy systematycznie przedłużać wynajem, aż do limitu ',default)+ISNULL(@daysLimitText,'')+dbo.f_translate(', przewidzianego warunkami assistance.',default)	
			END 
				
		END
		
		SET @content = @content + dbo.f_translate(' W przypadku wcześniejszego zakończenia naprawy warsztatowej będziemy się z Państwem kontaktować w celu zwrotu pojazdu zastępczego.',default)


    	PRINT '---content2'
	    PRINT @content
	
		EXEC dbo.p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @callerPhone,
		@err = @err OUTPUT,
		@message = @message OUTPUT

		-- powiadomienia o wydaniu auta zastępczego: CFM
		
		declare @workshopGroup int 
		declare @fixingPartnerEmail nvarchar(255)
		declare @canSwap int 
		declare @locationType nvarchar(255) 
		
		SELECT @workshopGroup = dbo.f_service_top_progress_group(@groupProcessInstanceId, 12)
		select @fixingPartnerEmail = dbo.f_partner_contact(@workshopGroup, @fixingPartnerId, 3, 4)

		IF @platformId = 43
		BEGIN
			
			EXEC [dbo].[p_can_workshop_swap_rental]
			@groupProcessInstanceId = @workshopGroup,
			@canSwap = @canSwap OUTPUT,
			@email = @fixingPartnerEmail output
		
			 -- LOGGER START
			DECLARE @c nvarchar(1000) = IIF(@canSwap = 1, dbo.f_translate('Pozytywne',default), dbo.f_translate('Negatywne',default))+dbo.f_translate(' spełnienie warunków wysyłki powiadomienia o podmianie MW',default)
			declare @p1 nvarchar(255)
			declare @p2 nvarchar(1000)
			
			SET @p1 = dbo.f_caseId(@rootId)		
			SET @p2 = dbo.f_partnerNameFull(@fixingPartnerId)
	
			EXEC dbo.p_log_automat
			@name = 'rental_swap_notice',
			@content = @c,
			@param1 = @p1,
			@param2 = @p2,
			@param3 = @fixingPartnerEmail
			-- LOGGER END
						
		END 
		
		IF @canSwap = 1 AND @platformId = 43
		BEGIN
			EXEC [dbo].[P_send_email_to_ald_swap_rental] @previousProcessId = @previousProcessId, @emailTo = @fixingPartnerEmail	
		END 
		ELSE IF @platformId = 48
		BEGIN
			
			SELECT	@locationType=avLocType.value_text 
			from	attribute_value avService with(nolock) inner join
					attribute_value avLocType with(nolock) on avLocType.parent_attribute_value_id=avService.id and avLocType.attribute_path='595,597,596'
			where	avService.id=@fixingPartnerId 
			and 	avService.attribute_path='595,597'

			IF dbo.f_exists_in_split(@locationType,'190') = 1
			BEGIN
				EXEC [dbo].[P_send_email_to_business_lease_new_mv] @previousProcessId = @previousProcessId, @emailTo = 'assistance@businesslease.pl'
				IF isnull(@rentalDuration,0) >3
					BEGIN
						EXEC [dbo].[P_send_email_to_business_lease_new_mv] @previousProcessId = @previousProcessId, @emailTo = 'assistance@businesslease.pl', @extension = 1
					end
			END
			ELSE IF dbo.f_exists_in_split(@locationType,'189') = 1
			BEGIN
				EXEC [dbo].[P_send_email_to_business_lease_new_mv] @previousProcessId = @previousProcessId, @emailTo = 'bl@motoflota.pl'
			END

		 
		END 
		
	END 	
END