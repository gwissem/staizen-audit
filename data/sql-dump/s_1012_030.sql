ALTER PROCEDURE [dbo].[s_1012_030]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @rootId INT
	DECLARE @groupProcessInstanceId INT
	DECLARE @nextStepId INT
	
	-- Pobranie GroupProcessInstanceId --
	SELECT	@groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance 
	WHERE id = @previousProcessId 
	
	-----------------------------------------------
	-- Import porady dla platformy Ford Vignale. --
	-----------------------------------------------

	EXEC dbo.p_matrixRefresh @rootProcessInstanceId=@rootId -- Wypełnienie macierzy.

	EXEC dbo.p_entity_export @rootId=@rootId -- Przeniesienie do tabeli ZamNag, ZamElem.

	UPDATE dbo.process_instance SET active = 0 WHERE group_process_id=@groupProcessInstanceId -- Zablokowanie edycji danych.

	EXECUTE dbo.p_note_new -- Dodawanie notatki
			@groupProcessId = @groupProcessInstanceId
			,@type = dbo.f_translate('text',default)
			,@content = dbo.f_translate('Przeniesiono do CDNa.',default)
			,@userId = 1  -- automat
			,@err = @err
			,@message = @message

	DECLARE @acceptStepId NVARCHAR(20)
	SELECT TOP 1 @acceptStepId = id FROM dbo.process_instance with(nolock) WHERE group_process_id = @groupProcessInstanceId and step_id = '1012.012'
	IF @acceptStepId is not null
	BEGIN
		UPDATE dbo.process_instance SET active = 999 WHERE id = @acceptStepId
	END 

	SET @variant = 1
END
