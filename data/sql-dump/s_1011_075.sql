
ALTER PROCEDURE [dbo].[s_1011_075]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, 
    @errId int=0 output
) 
AS
BEGIN
		
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @isAgree INT
	
	SELECT @groupProcessInstanceId = group_process_id, @rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '252', @groupProcessInstanceId = @rootId
	select @isAgree = value_int FROM @values 

	set @variant=1

	if @isAgree=1
	begin
		declare @err int
		declare @message nvarchar(500)

		SET @variant = 91
		EXEC [dbo].[p_attribute_edit]
			@attributePath = '554', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@userId = 1,
			@originalUserId = 1,
			@valueText = dbo.f_translate('Do widzenia',default),
			@err = @err OUTPUT,
			@message = @message OUTPUT
	end
	

END
