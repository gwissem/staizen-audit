

ALTER PROCEDURE [dbo].[s_1009_030]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT 
	DECLARE @stepId NVARCHAR(20)
	DECLARE @partnerId INT
	DECLARE @rootId INT
	DECLARE @parentId INT
	DECLARE @processInstanceId INT
	DECLARE @status INT
	DECLARE @executorPhone NVARCHAR(255)
	DECLARE @uid NVARCHAR(50)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @platformId INT
	DECLARE @fixingOrTowing INT
	DECLARE @platformName NVARCHAR(200)
	DECLARE @callerPhone NVARCHAR(20)

	DECLARE @clsPhoneNo NVARCHAR(20)
	DECLARE @partnerLocationId INT
	DECLARE @eventType INT
	DECLARE @clsIsPoland INT
	DECLARE @clsServiceName NVARCHAR(100)

	SELECT @groupProcessInstanceId = p.group_process_id, 
	@rootId = p.root_id,
	@parentId = p.parent_id
	FROM dbo.process_instance p with(nolock)
	INNER JOIN step s with(nolock) on s.id = p.step_id
	WHERE p.id = @previousProcessId 

	
	IF @variant = 10
	BEGIN
		DECLARE @newProcessInstanceId INT 
		
		EXEC [dbo].[p_process_jump]
		@previousProcessId = @previousProcessId,
		@nextStepId = '1141.001',
		@groupId = @groupProcessInstanceId, 
		@parentId = @rootId, 
		@rootId = @rootId,
	    @err = @err output,
	    @message = @message output,
	    @newProcessInstanceId = @newProcessInstanceId output
	    
	    update dbo.process_instance set previous_process_instance_id = @previousProcessId where id = @newProcessInstanceId
	    RETURN 
	END 
	
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
		
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
		@attributePath = '253',
		@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '80,342,408,197',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
		
	SELECT @platformName = name FROM dbo.platform where id = @platformId
		
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerLocationId = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '491', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eventType = value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '927', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @clsIsPoland = value_int FROM @values

	declare @isService int

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '212', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isService = value_int FROM @values

	set @isService=isnull(@isService,1)

	IF @platformId = 2 AND 	@eventType IN (1,3) AND @clsIsPoland = 1 and @isService<>0
	BEGIN			
		SET @clsPhoneNo = dbo.f_partner_contact(@groupProcessInstanceId,@partnerLocationId,1,17)
		SET @clsServiceName = dbo.f_partnerName(@partnerLocationId)
				
		IF @clsPhoneNo IS NOT NULL
		BEGIN	

			declare @platformPhone nvarchar(50)
			SELECT @platformPhone=official_line_number FROM dbo.platform WHERE id = @platformId


			SET @content = dbo.f_translate('Przesyłamy numer telefonu do Koordynatora CLS z serwisu ',default) + @clsServiceName + ' '+@clsPhoneNo+dbo.f_translate('. Z poważaniem, Ford Centrum Likwidacji Szkód.',default)+@platformPhone 
		
			EXEC dbo.p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @callerPhone,
			@err = @err OUTPUT,
			@message = @message OUTPUT,
			@addInfo = 0
		END
	END
	
	SET @variant = 1

END


