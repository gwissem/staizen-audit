ALTER PROCEDURE [dbo].[p_transport_costs]
@previousProcessId INT, 
@currency NVARCHAR(5) = dbo.f_translate('PLN',default),
@limit DECIMAL(10,2) OUTPUT,
@estimated DECIMAL(10,2) OUTPUT
AS
BEGIN
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @groupProcessInstanceId INT
	DECLARE @estimatedCosts decimal(18,4)
	DECLARE @distance decimal(20,6)
	DECLARE @distanceAdjusted decimal(20,6)
	DECLARE @isBigCity INT
	DECLARE @message NVARCHAR(255)
	DECLARE @exchangeCourse decimal(18,4)
	DECLARE @costsLimit decimal(18,5)
	DECLARE @latStart decimal(10,8)
	DECLARE @longStart decimal(10,8)
	DECLARE @latEnd decimal(10,8)
	DECLARE @longEnd decimal(10,8)
	DECLARE @cityName NVARCHAR(255)
	
	SELECT @groupProcessInstanceId = group_process_id
	FROM process_instance 
	where id = @previousProcessId
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '152,175,85,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @latStart = CAST(value_string as decimal(10,8)) FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '152,175,85,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @longStart = CAST(value_string as decimal(10,8)) FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '152,320,85,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @latEnd = CAST(value_string as decimal(10,8)) FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '152,320,85,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @longEnd = CAST(value_string as decimal(10,8)) FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '152,320,85,87', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @cityName = value_string FROM @values
	
	SELECT @distance = [dbo].[FN_distance](@latStart, @longStart, @latEnd, @longEnd)
	SELECT @isBigCity = [dbo].[f_is_big_city] (@cityName)
	
	SET @distanceAdjusted = (1.2 + (0.03 * @isBigCity)) * @distance
	
	SET @exchangeCourse = 1.0000
	
	IF @currency <> dbo.f_translate('PLN',default)
	BEGIN
		SET @exchangeCourse = dbo.FN_Exchange2(@currency, GETDATE())
    END
    PRINT @limit
    PRINT @distanceAdjusted
	SET @estimated = CAST(((140 + @distanceAdjusted) * 1.23) AS DECIMAL(10,2))
    SET @limit = CAST((@exchangeCourse * @limit) AS DECIMAL(10,2))
    
END
