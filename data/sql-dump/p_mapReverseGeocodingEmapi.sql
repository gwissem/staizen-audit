
ALTER PROCEDURE [dbo].[p_mapReverseGeocodingEmapi]
@lat decimal(9,6),
@lng decimal(9,6),
@address nvarchar(1000) output
AS
BEGIN 
	DECLARE @URL VARCHAR(8000) 
	SET @URL = 'https://atlasdev.starter24.pl/get_location/'+cast(@lat as varchar(50))+'/'+cast(@lng as varchar(50))+'?no-json'

	DECLARE @Response varchar(8000)
	DECLARE @Obj int 
	DECLARE @HTTPStatus int 
	DECLARE @Text as table ( answer varchar(4000) )
 

	EXEC sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @Obj OUT 
	EXEC sp_OAMethod @Obj, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @URL, false
	EXEC sp_OAMethod @Obj, dbo.f_translate('setRequestHeader',default), NULL, dbo.f_translate('Content-Type',default), dbo.f_translate('application/x-www-form-urlencoded',default)
	EXEC sp_OAMethod @Obj, send, NULL, ''
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('status',default), @HTTPStatus OUT 
	
	INSERT @Text
	EXEC sp_OAGetProperty @Obj, dbo.f_translate('responseText',default)
	EXEC sp_OADestroy @Obj

	declare @city nvarchar(100)
	declare @zipCode nvarchar(100)
	declare @street nvarchar(100)
	declare @number nvarchar(100)

	

	select @city=substring(data,6,100) from (
	select replace(replace(replace(data,'"',''),'}',''),']','') data from @text cross apply dbo.f_split(answer,',')
	) a where a.data like 'city%'

	select @zipCode=substring(data,9,100) from (
	select replace(replace(replace(data,'"',''),'}',''),']','') data from @text cross apply dbo.f_split(answer,',')
	) a where a.data like '%zipcode%'

	select @street=substring(data,8,100) from (
	select replace(replace(replace(data,'"',''),'}',''),']','') data from @text cross apply dbo.f_split(answer,',')
	) a where a.data like '%street:%'

	select @number=substring(data,14,100) from (
	select replace(replace(replace(data,'"',''),'}',''),']','') data from @text cross apply dbo.f_split(answer,',')
	) a where a.data like '%number%'

	set @address=@street+' '+@number+', '+@zipCode +' '+@city
	
END

