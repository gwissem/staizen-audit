ALTER PROCEDURE [dbo].[s_1063_005]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
DECLARE @stepId NVARCHAR(10)
DECLARE @servicePartner INT
DECLARE @boughtExtendedService INT
DECLARE @boughtAsNew INT
DECLARE @createdAt DATETIME
DECLARE @firstRegDate DATETIME
DECLARE @releaseDate DATETIME
DECLARE @servicedByPartner INT
DECLARE @platform INT
DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
DECLARE @groupProcessInstanceId INT
DECLARE @message NVARCHAR(255)
DECLARE @err INT
SELECT @groupProcessInstanceId = group_process_id, @createdAt = created_at, @stepId = step_id FROM process_instance where id = @previousProcessId

INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @servicePartner = value_int FROM @values

DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '530', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @boughtExtendedService = value_int FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '520', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @servicedByPartner = value_int FROM @values

DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '74,233', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @firstRegDate = value_date FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '74,106', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @releaseDate = value_date FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '521', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @boughtAsNew = value_int FROM @values
DELETE FROM @values
INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
SELECT @platform = value_int FROM @values
SET @variant = 1

IF @servicePartner < 1 
BEGIN
	IF @servicePartner = -1
	BEGIN
		IF @boughtAsNew = 1
		BEGIN
			EXEC p_attribute_set2
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Assistance dla pojazdów na gwarancji udzielane jest samochodom, które zostały zakupione jako nowe w polskiej autoryzowanej sieci dealerskiej marki. Proszę bardzo o ustalenie tego, na przykład na podstawie książki serwisowej auta i oddzwonieni',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
		END
		ELSE IF @servicedByPartner = 1
		BEGIN
			EXEC p_attribute_set2
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Warunkiem uzyskania pakietu assistance jest regularne serwisowanie samochodu w polskiej autoryzowanej sieci partnerów serwisowych. Prosiłbym odnaleźć książkę serwisową pojazdu, gdzie zapisana jest data ostatniego przeglądu, przebieg wtedy z',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
		END 
		ELSE IF @boughtExtendedService = 1
		BEGIN
			EXEC p_attribute_set2
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Nie znając dealera, do którego możemy się zwrócić o potwierdzenie uprawnień, nie będziemy w stanie ich zweryfikować. Na chwile obecną mogę zaoferować pomoc odpłatną z możliwością późniejszego ubiegania się o zwrot kosztów po pot',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
		END 
	END 
	ELSE IF @servicePartner = -2
	BEGIN
		IF @boughtAsNew = 1
		BEGIN
			EXEC p_attribute_set2
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Assistance dla pojazdów na gwarancji udzielane jest samochodom, które zostały zakupione jako nowe w polskiej autoryzowanej sieci dealerskiej marki. Proszę bardzo o ustalenie tego, na przykład na podstawie książki serwisowej auta i oddzwonieni',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
		END
		ELSE IF @servicedByPartner = 1
		BEGIN
			EXEC p_attribute_set2
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Warunkiem uzyskania pakietu assistance jest regularne serwisowanie samochodu w polskiej autoryzowanej sieci partnerów serwisowych marki. Podana firma nie figuruje na oficjalnej liście, wobec czego mogę zaoferować pomoc odpłatną.',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
		END 
		ELSE IF @boughtExtendedService = 1
		BEGIN
			EXEC p_attribute_set2
			@attributePath = '532', 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = @stepId,
			@valueText = dbo.f_translate('Podana firma nie figuruje na oficjalnej liście polskich autoryzowanych dealerów. W tej sytuacji mogę zaoferować pomoc odpłatną z możliwością późniejszego ubiegania się o zwrot kosztów po potwierdzeniu uprawnień do assistance.',default),
			@err = @err OUTPUT,
			@message = @err OUTPUT
		END 
	END 
	SET @variant = 2
END
ELSE IF @servicePartner > 0 AND @boughtExtendedService > 0 AND ISNULL(@releaseDate, @firstRegDate) > DATEADD(yy, -4, @createdAt)
	SET @variant = 3
ELSE IF @servicePartner > 0 AND @servicedByPartner = 1
BEGIN
	IF @platform = 31
	BEGIN
		SET @variant = 5
	END 
	ELSE
	BEGIN
		SET @variant = 4
	END
END
