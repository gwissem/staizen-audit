ALTER PROCEDURE [dbo].[p_send_rs_truck]
  (
    @groupProcessInstanceId int
  )
AS
  BEGIN

    DECLARE @mailBody NVARCHAR(MAX)
    DECLARE @rootId int
    SELECT @rootId = root_id from process_instance where group_process_id = @groupProcessInstanceId
    EXEC Dbo.P_get_email_template @name = 'truck_rs', @responseText = @mailBody output

    DECLARE @companyToRS NVARCHAR(1000)
    DECLARE @userCompanyFullInfo NVARCHAR(500)
    DECLARE @cardNumber NVARCHAR(20)
    DECLARE @cardDate NVARCHAR(20)
    DECLARE @caseId NVARCHAR(10)
    DECLARE @userCompanyName NVARCHAR(150)
    DECLARE @userCompanyCity NVARCHAR(40)
    DECLARE @caseDate NVARCHAR(40)
    DECLARE @gopValue NVARCHAR(15)
    DECLARE @gopValueDecimal decimal(10,2)
    DECLARE @platform nvarchar(100)
    DECLARE @makeModel nvarchar(100)
    DECLARE @platformId int

    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))


    DELETE FROM @values
    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int FROM @values
    SELECT @platform = name from AtlasDB_def.dbo.platform where id = @platformId



    DELETE from @values
    INSERT @values exec p_attribute_get2 @attributePath = '1028,1029', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @gopValueDecimal = value_decimal from @values
    DELETE from @values

    SET @gopValue = CONVERT(nvarchar(15), @gopValueDecimal )

--     User Company declarations
    DECLARE  @userCompanyCountry nvarchar(100)
    DECLARE  @userCompanyStreet nvarchar(100)
    DECLARE  @userCompanyNIP nvarchar(100)
    DECLARE  @userCompanyBuildingNumber nvarchar(100)
    DECLARE  @userCompanyApartamentNumber nvarchar(100)
    DECLARE  @userCompanyPostalCode nvarchar(100)
    DECLARE  @userCompanyPhone nvarchar(100)
    DECLARE  @userCompanyFax nvarchar(100)

    DELETE from @values
    INSERT @values exec p_attribute_get2 @attributePath = '176', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardNumber = value_string from @values
    DELETE from @values





    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,86', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyCountry = value_string FROM @values


    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '84', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyName = value_string FROM @values

    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,627', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyCity = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,94', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyStreet = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,95', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyBuildingNumber = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,96', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyApartamentNumber = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '624,626,89', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyPostalCode = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '197', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyPhone = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '197', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyFax = value_string FROM @values
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '83', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @userCompanyNIP = value_string FROM @values




    --     User Company Name

    SET @userCompanyFullInfo = ''
    IF isnull(@userCompanyName, '') <> ''
      BEGIN
        SET @userCompanyFullInfo = @userCompanyName
      END

    IF isnull(@userCompanyCity, '') <> ''
      BEGIN
        SET @userCompanyFullInfo = @userCompanyFullInfo + '<br />'
        SET @userCompanyFullInfo = @userCompanyFullInfo + isnull(@userCompanyStreet,'') +' '+ isnull(@userCompanyBuildingNumber,'')
        IF isnull(@userCompanyApartamentNumber,'') <>''
          BEGIN
            SET @userCompanyFullInfo = @userCompanyFullInfo + ' / ' + @userCompanyApartamentNumber
          END
        SET @userCompanyFullInfo = @userCompanyFullInfo + '<br />'+isnull(@userCompanyPostalCode,'') +' '+ @userCompanyCity
      END
    IF isnull(@userCompanyPhone,'')  <> ''
      BEGIN
        SET @userCompanyFullInfo = @userCompanyFullInfo + '<br /> TEL: ' + @userCompanyPhone
      end
    IF isnull(@userCompanyFax,'')  <> ''
      BEGIN
        SET @userCompanyFullInfo = @userCompanyFullInfo + '<br /> fax: ' + @userCompanyFax
      end





    DECLARE @cardTypeId int
    DECLARE @cardTypeName nvarchar(30)
  
    DECLARE @cardActiveTill nvarchar(30)
    DELETE from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '173', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @cardTypeId = value_int FROM @values
    SET @cardTypeName  = dbo.f_dictionaryDesc (isnull(@cardTypeId,-1), 'STSPayment')

    If isnull(@cardTypeId,-1) <> -1
      BEGIN
        IF @cardTypeId  = 1
          BEGIN
            SET @cardTypeName = dbo.f_translate('euroShell',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @cardTypeId  = 2
          BEGIN
            SET @cardTypeName = dbo.f_translate('UTA',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @cardTypeId  = 3
          BEGIN
            SET @cardTypeName = dbo.f_translate('ESSO',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end
        IF @cardTypeId  = 4
          BEGIN
            SET @cardTypeName = dbo.f_translate('KRONE',default)
            SET @platform = dbo.f_translate('KRONE',default)
          end
        IF @cardTypeId  = 5
          BEGIN
            SET @cardTypeName = dbo.f_translate('KOGEL',default)
            SET @platform = dbo.f_translate('KOGEL',default)
          end
        IF @cardTypeId  = 6
          BEGIN
            SET @cardTypeName = dbo.f_translate('OTOKAR',default)
            SET @platform = dbo.f_translate('OTOKAR',default)
          end
        IF @cardTypeId  = 7
          BEGIN
            SET @cardTypeName = dbo.f_translate('BPW ',default)
            SET @platform = dbo.f_translate('BPW',default)
          end
        IF @cardTypeId  = 8
          BEGIN
            SET @cardTypeName = dbo.f_translate('WEBASTO ',default)
            SET @platform = dbo.f_translate('WEBASTO',default)
          end
        IF @cardTypeId  = 9
          BEGIN
            SET @cardTypeName = dbo.f_translate('FLEET',default)
            SET @platform = dbo.f_translate('Starter Truck Service',default)
          end

        --         Setujemy po odpowiedniej
      end
    ELSE
      BEGIN
        SET @cardTypeName = @platform
        --         bierzemy nazwę platformy
      end





    SET @caseId = dbo.f_caseId(@groupProcessInstanceId)


    SET @caseDate = CONVERT(nvarchar, getdate(), 103) + ' ' + CONVERT(nvarchar, getdate(), 108)


    DECLARE @sendToMail nvarchar(20) = 'kuba@sigmeo.pl'
    DECLARE @subject nvarchar(100) = dbo.f_translate('TRUCK RS  ',default) + @caseId

    SET @mailBody = REPLACE(@mailBody, '__COMPANY_NAME__', isnull(@platform, ''))
    SET @mailBody = REPLACE(@mailBody, '__USER_COMPANY_INFO__', isnull(@userCompanyFullInfo, ''))
    SET @mailBody = REPLACE(@mailBody, '__CARD_NUMBER__', isnull(@cardNumber, ''))
    SET @mailBody = REPLACE(@mailBody, '__CARD_DATE__', isnull(@cardDate, ''))
    SET @mailBody = REPLACE(@mailBody, '__CARD_TYPE__', isnull(@cardTypeName, ''))
    SET @mailBody = REPLACE(@mailBody, '__CASE_ID__', isnull(@caseId, ''))
    SET @mailBody = REPLACE(@mailBody, '__CASE_DATE__', isnull(@caseDate, ''))
    SET @mailBody = REPLACE(@mailBody, '__GOP_VALUE__', isnull(@gopValue, ''))


    DECLARE @err int
    DECLARE @message nvarchar(2000)


    DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('truck')

    DECLARE @attachment nvarchar(MAX)



    EXEC dbo.p_attribute_edit
        @attributePath = '959',
        @groupProcessInstanceId = @groupProcessInstanceId,
        @stepId = 'xxx',
        @valueText = @mailBody,
        @err = @err OUTPUT,
        @message = @message OUTPUT
    declare @pdfId int
    SELECT TOP 1 @pdfId = id
    FROM dbo.attribute_value
    WHERE attribute_path = '959'
      AND group_process_instance_id = @groupProcessInstanceId




    SET @attachment = '{ATTRIBUTE::' + CONVERT(NVARCHAR(100), @pdfId) + dbo.f_translate('::Truck RS for case ',default) + isnull(@caseId, '') +'::false}'

    print @mailBody

    EXECUTE dbo.p_note_new
        @groupProcessId = @groupProcessInstanceId
        , @type = dbo.f_translate('email',default)
        , @content = dbo.f_translate('Truck invoice',default)
        , @email = @sendToMail
        , @userId = 1  -- automat
        , @subject = @subject
        , @direction = 1
        , @dw = ''
        , @udw = ''
        , @sender = @senderEmail
        , @additionalAttachments = @attachment
        , @emailBody = @mailBody
        , @err = @err OUTPUT
        , @message = @message OUTPUT


  END
