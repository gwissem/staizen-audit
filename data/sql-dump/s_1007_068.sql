ALTER PROCEDURE [dbo].[s_1007_068]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT 
	DECLARE @stepId NVARCHAR(20)
	DECLARE @partnerId INT
	DECLARE @rootId INT
	DECLARE @parentId INT
	DECLARE @processInstanceId INT
	DECLARE @content NVARCHAR(MAX)
	DECLARE @platformId INT
	DECLARE @programId NVARCHAR(255)
	DECLARE @callerPhone NVARCHAR(20)
	DECLARE @programLimit INT
	
	SELECT @groupProcessInstanceId = group_process_id FROM dbo.process_instance with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '80,342,408,197',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @callerPhone = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @programId = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platformId = value_int FROM @values
	
	DECLARE @daysLimitText nvarchar(100)
	DECLARE @days INT
	DECLARE @workingDaysOnly INT
	
	EXEC [dbo].[p_service_day_limit] 
	@groupProcessInstanceId = @groupProcessInstanceId, 
	@serviceId = 3,
	@max = 1,
	@days = @days OUTPUT,
	@workingDaysOnly = @workingDaysOnly OUTPUT
	
	SELECT @daysLimitText = CAST(@days AS NVARCHAR(100)) + dbo.f_translate(' dzień/dni ',default) + IIF(@workingDaysOnly = 1, dbo.f_translate('roboczy/ch',default), dbo.f_translate('kalendarzowy/ch',default))
		
	SET @content = dbo.f_translate('Uprzejmie potwierdzamy organizację samochodu zastępczego na czas naprawy uszkodzonego pojazdu, ale nie dłużej niż ',default)+@daysLimitText+dbo.f_translate('. Wypożyczalnia skontaktuje się bezpośrednio z Państwem w razie dodatkowych pytań',default)

	IF isnull(@platformId,0) = 53
		BEGIN
			SET @content = dbo.f_translate('Uprzejmie potwierdzamy organizację samochodu zastępczego na czas naprawy uszkodzonego pojazdu, Będziemy przedłużać wynajem zgodnie z możliwościami programu Assistance. Wypożyczalnia skontaktuje się bezpośrednio z Państwem w razie dodatko',default)


		end
	EXEC dbo.p_note_new
	@groupProcessId = @groupProcessInstanceId,
	@type = dbo.f_translate('sms',default),
	@content = @content,
	@phoneNumber = @callerPhone,
	@err = @err OUTPUT,
	@message = @message OUTPUT
	
	SET @variant = 1
	 
END
