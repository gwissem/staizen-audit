ALTER PROCEDURE [dbo].[s_1007_065]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int = 1,
	@errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @acceptedOrder SMALLINT = 0
	DECLARE @amountRefused SMALLINT
	DECLARE @stepId VARCHAR(32)
	DECLARE @email VARCHAR(400)
	DECLARE @subject VARCHAR(400)
	DECLARE @p1 VARCHAR(400)
	DECLARE @body VARCHAR(MAX)
	DECLARE @partnerLocId INT
	DECLARE @monitIdKey NVARCHAR(255)
	DECLARE @monitId INT
	DECLARE @createdAt DATETIME 
	DECLARE @postponeCount INT
	DECLARE @postponeLimit INT
	DECLARE @decision INT
	
	SELECT @groupProcessInstanceId = group_process_id, @createdAt = p.created_at, @postponeCount = p.postpone_count, @postponeLimit = s.postpone_count
	FROM process_instance p with(nolock)
	INNER JOIN step s with(nolock) ON p.step_id = s.id
	WHERE p.id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	-- Sprawdzenie czy zadanie jest przekładane
	
	SET @monitIdKey = dbo.f_translate('rental-PZ-',default)+CAST(@groupProcessInstanceId AS NVARCHAR(255))
	SELECT @monitId = id FROM dbo.attribute_value with(nolock) where value_string = @monitIdKey

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '837,769', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @decision = value_int FROM @values
	
	SET @variant = 99
	
			
	IF @decision = 0
	BEGIN
		SET @variant = 2
	END 
	ELSE IF @decision = 1
	BEGIN
		SET @variant = 3
	END 	    
 	ELSE IF @decision IS NULL
	BEGIN	
		IF @postponeCount >= @postponeLimit - 1 
		BEGIN
			SET @variant = 1
		END 
		ELSE
		BEGIN
			IF @postponeCount >= 1 AND @monitId IS NULL 
			BEGIN
								
				INSERT  @values EXEC dbo.p_attribute_get2
			 		@attributePath = '837,773,368',
			 		@groupProcessInstanceId = @groupProcessInstanceId
			 	SELECT @email = value_string FROM @values
				 	
				SET @body='Hello from Starter,<br/>Have you recieved an e-mail from us with the request to organize a rental car: ' + dbo.f_caseId(@previousProcessId)  + '?
						<BR><BR><BR>
						Best regards,<BR>
						Starter24'
								
				SET @subject = dbo.f_translate('Replacement car organisation request - ',default) + dbo.f_caseId(@previousProcessId)
	
				DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('rentals')
				DECLARE @platformGroup nvarchar(255)	
				EXEC dbo.p_platform_group_name @groupProcessInstanceId = @groupProcessInstanceId, @name = @platformGroup output	
				IF @platformGroup = dbo.f_translate('CFM',default)
				BEGIN
					SET @senderEmail = dbo.f_getEmail('cfm')
				END

				EXECUTE dbo.p_note_new 
					 @groupProcessId = @groupProcessInstanceId
					,@type = dbo.f_translate('email',default)
					,@content = dbo.f_translate('Zapytanie o otrzymaniu zlecenie',default)
					,@email = @email
					,@userId = 1  -- automat
					,@subject = @subject
					,@direction=1
					,@dw = ''
					,@udw = ''
					,@sender = @senderEmail
					,@additionalAttachments = ''
					,@emailBody = @body
					,@subType = @monitIdKey
					,@err=@err OUTPUT
					,@message=@message OUTPUT
				
			END
		END		
		
	END 
	
	IF @variant = 99
	BEGIN
		EXEC [dbo].[p_attribute_edit]
	    @attributePath = '842', 
	    @groupProcessInstanceId = @groupProcessInstanceId,
	    @stepId = 'xxx',
	    @userId = 1,
	    @originalUserId = 1,
	    @valueInt = NULL,
	    @err = @err OUTPUT,
	    @message = @message OUTPUT
	    
		UPDATE dbo.process_instance SET postpone_date = DATEADD(MINUTE, 15, postpone_date), postpone_count = postpone_count + 1 where id = @previousProcessId
	END 
	
	
END

