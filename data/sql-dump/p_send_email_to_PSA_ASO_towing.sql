ALTER PROCEDURE [dbo].[p_send_email_to_PSA_ASO_towing]
    @previousProcessId INT  -- Może być GROUP / ROOT
AS
  BEGIN

    /*

      1. W momencie potwierdzenia realizacji holowania, ASO do którego trafi pojazd powinno otrzymać powiadomienie z informacją, że w sprawie odbywa się holowanie.
          a. Mail jest wysyłany do ASO, do którego trafi pojazd
          b. Mail jest wysyłany w momencie kiedy mamy potwierdzenie, że w sprawie odbędzie się holowanie
              i. może być wysłane od razu gdy wysyłamy kontraktora na holowanie a w przypadku wysyłki na naprawę dopiero wtedy gdy potwierdzimy, że zamiast naprawy odbędzie się jednak holowanie
          c. Temat maila: [nr sprawy], Powiadomienie o holowaniu
          d. DO: adres mailowy do serwisu, podamy dokładną ścieżkę w strukturze partnera, po załadowaniu ich do Atlasa
          e. OD: callcenter@starter24.pl
          f. Treść maila:
  Szanowni Państwo,

     ____________________________________*/

    DECLARE @err INT
    DECLARE @message NVARCHAR(255)

    DECLARE @groupProcessInstanceId INT
    DECLARE @rootId INT
    DECLARE @stepId NVARCHAR(255)
    DECLARE @body NVARCHAR(MAX)
    DECLARE @emailTo NVARCHAR(MAX)

    DECLARE @content NVARCHAR(MAX)
    DECLARE @title NVARCHAR(MAX)

    -- Pobranie podstawowych danych --
    SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
    FROM process_instance with(nolock)
    WHERE id = @previousProcessId

    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))


    SET @content = '
		Szanowni Państwo,</br></br>

Przesyłamy powiadomienie dot. holowania w przedmiotowej sprawie:<br><br>

Nr sprawy: {#caseid#}<br>
Nr rej.:  {@74,72@}</br>
Nr VIN:  {@74,71@}</br>
Data i godzina zdarzenia:__ACCIDENT_DATE__ <br>
Opis usterki: {#diagnosisDescription(true)#} / {@417@}<br>

Wiadomość generowana automatycznie. Prosimy nie odpowiadać na ten adres. Dla komunikacji w sprawach bieżących prosimy o kontakt na adres kz@starter24.pl

Z poważaniem
Starter24 Sp. z o.o. <br> <br>'


    EXEC [dbo].[P_parse_string]
        @processInstanceId = @previousProcessId,
        @simpleText = '[{#caseid#}]  Powiadomienie o holowaniu ',
        @parsedText = @title OUTPUT

    EXEC [dbo].[P_get_body_email]
        @body = @body OUTPUT,
        @contentEmail = @content,
        @title = @title,
        @previousProcessId = @previousProcessId


    DECLARE @accidentDate DATETIME

    print @title
    print @body

    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '101,104', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @accidentDate = value_date from @values

    IF @accidentDate is null
      BEGIN
        SELECT top 1 @accidentDate = created_at from process_instance where root_id = @rootId order by ID asc
      end



    SET @body = REPLACE(@body, '__ACCIDENT_DATE__',
                        isnull(convert(nvarchar(50), @accidentDate, 104) +' '+ convert(nvarchar(5), @accidentDate, 108),''))

    DECLARE @email VARCHAR(400)
    SET @emailTo = 'kuba@sigmeo.pl'
    DECLARE @sender NVARCHAR(400)



    DECLARE @platformId int
    DELETE @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int from @values

    DECLARE @platformName nvarchar(100)
    SELECT @platformName = name from AtlasDB_def.dbo.platform where id = @platformId

    SET @body = REPLACE(@body,'__PLATFORM_NAME__', @platformName)
    SET @body = REPLACE(@body,'__EMAIL__', dbo.f_getEmail('callcenter'))

    DECLARE   @partnerId int
    DELETE FROM @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @partnerId = value_int FROM @values
    delete from @values


    SET @sender = dbo.f_getEmail('callcenter')
    -- Pobranie Emaila do kontraktora (3 próby)
    -- 8 - Email RS
    -- 4 - Realizacja - email
    -- 16 - Email
    SELECT @email = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 1, 8)

    IF ISNULL(@email, '') = ''
      BEGIN

        SET @email = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 1, 4)

        IF ISNULL(@email, '') = ''
          BEGIN
            SET @email = dbo.f_partner_contact(@groupProcessInstanceId, @partnerId, 1, 16)
          END

      END

    DECLARE @contentNote NVARCHAR(1000) = [dbo].[f_showEmailInfo](@email) + dbo.f_translate('Powiadomienie do ASO o holowaniu.',default)
    SET @email = dbo.f_getRealEmailOrTest(@email)

    SET @sender = dbo.f_getEmail('callcenter')


    EXECUTE dbo.p_note_new
        @groupProcessId = @groupProcessInstanceId
        , @type = dbo.f_translate('email',default)
        , @content = @contentNote
        , @email = @email
        , @userId = 1  -- automat
        , @subject = @title
        , @direction = 1
        , @sender = @sender
        , @emailBody = @body
        , @err = @err OUTPUT
        , @message = @message OUTPUT
        , @emailRegards = 1

  END