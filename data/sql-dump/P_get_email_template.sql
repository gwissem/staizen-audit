ALTER PROCEDURE [dbo].[P_get_email_template]
@name NVARCHAR(1000),
@responseText NVARCHAR(MAX) = '' OUTPUT
AS
begin
	
	/*	ZCACHOWANY TWIG 'modern_email?lang_footer=pl'
 		____________________________________*/
	IF @name = 'modern_email?lang_footer=pl'
	BEGIN
		
		EXEC [dbo].[P_get_email_template_modern_email_pl] @responseText = @responseText OUTPUT

	END
-- 	ELSE IF @name = 'adac_gop_request'
-- 	BEGIN
-- 		EXEC [dbo].[P_get_email_adac_gop_request] @responseText = @responseText OUTPUT
-- 	END 
	ELSE
	BEGIN
	
		DECLARE @ret INT;
		DECLARE @status NVARCHAR(32);
		DECLARE @statusText NVARCHAR(32);
		DECLARE @token INT;
		DECLARE @url NVARCHAR(256);
		DECLARE @Text as table ( answer VARCHAR(MAX) )
		
		SET @url = dbo.f_getDomain() + dbo.f_translate('/mailbox/get-email-template/',default) + @name
		
		-- Open the connection.
		EXEC @ret = sp_OACreate dbo.f_translate('MSXML2.XMLHttp',default), @token OUT;
		IF @ret <> 0 RAISERROR('Unable to open HTTP connection.', 10, 1);
			
		PRINT '@ret'
		PRINT @ret
		PRINT '@url'
		PRINT @url
	
		-- Send the request.
		EXEC @ret = sp_OAMethod @token, dbo.f_translate('open',default), NULL, dbo.f_translate('GET',default), @url, false;
		EXEC @ret = sp_OAMethod @token, dbo.f_translate('send',default), NULL, '';
		
		-- Handle the response.
		EXEC @ret = sp_OAGetProperty @token, dbo.f_translate('status',default), @status OUT;
		EXEC @ret = sp_OAGetProperty @token, dbo.f_translate('statusText',default), @statusText OUT;
		
		INSERT INTO @Text
		EXEC sp_OAGetProperty @token, dbo.f_translate('responseText',default) 
		
		SELECT @responseText = answer FROM @Text
	
		-- Show the response.
		PRINT dbo.f_translate('Status: ',default) + @status + ' (' + @statusText + ')';
		
		--	PRINT @responseText
		--	SELECT @responseText response
	
		-- Close the connection.
		EXEC @ret = sp_OADestroy @token;
		IF @ret <> 0 RAISERROR('Unable to close HTTP connection.', 10, 1);
	
		-- CONTENT POBIERANY Z : ATLAS /mailbox/get-email-template/modern_email
		
	END

END