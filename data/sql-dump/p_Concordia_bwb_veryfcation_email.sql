ALTER PROCEDURE [dbo].[p_Concordia_bwb_veryfcation_email]
  (
    @previousProcessId INT,
    @variant TINYINT OUTPUT,
    @currentUser int = 1,
    @errId int=0 output
  )
AS
  BEGIN

    DECLARE @err INT
    DECLARE @message VARCHAR(400)
    DECLARE @groupProcessInstanceId INT
    DECLARE @rootId INT
    DECLARE @stepId VARCHAR(32)
    DECLARE @body NVARCHAR(MAX)
    DECLARE @mailSubject NVARCHAR(255)


    DECLARE @mailTo NVARCHAR(100)
    DECLARE @firstName nvarchar(100)
    DECLARE @lastName nvarchar(100)
    DECLARE @makeModel nvarchar(255)
    DECLARE @licencePlate nvarchar(50)
    DECLARE @clientPhone nvarchar(50)
    DECLARE @programId nvarchar(10)
    DECLARE @serialAndNumber NVARCHAR(255)



    SELECT
           @groupProcessInstanceId = group_process_id,
           @stepId = step_id,
           @rootId = root_id
    FROM process_instance  WITH(NOLOCK)
    WHERE id = @previousProcessId

    EXEC p_form_controls @instance_id = @previousProcessId, @returnResults = 0

    DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))




    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,64', @groupProcessInstanceId = @rootId
    SELECT @firstName = value_string FROM @values
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,66', @groupProcessInstanceId = @rootId
    SELECT @lastName = value_string FROM @values
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '418,342,408,197', @groupProcessInstanceId = @rootId
    SELECT @clientPhone = value_string FROM @values
    DELETE FROM @values

    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @rootId
    SELECT @programId = value_string FROM @values
    DELETE FROM @values

    declare @programName nvarchar(100)
    select top 1 @programName = vp.name from dbo.vin_program vp with(nolock)
    where vp.id = @programId

    declare @clientContact nvarchar(255)
    set @clientContact= isnull(@firstName,'') + ' ' +  isnull(@lastName,'') + dbo.f_translate(' tel: ',default) + isnull(@clientPhone,'')


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @rootId
    SELECT @licencePlate = value_string FROM @values
    DELETE FROM @values


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @rootId
    SELECT top 1 @makeModel = d.textD FROM @values
                                             join dbo.dictionary d with(nolock) on d.value = value_int and d.typeD = 'MakeModel' and d.active = 1
    DELETE FROM @values


    INSERT @values EXEC dbo.p_attribute_get2 @attributePath = '440', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @serialAndNumber = value_string FROM @values
    DELETE FROM @values



    DECLARE @value NVARCHAR(255)
    DECLARE @description NVARCHAR(255)
    EXEC dbo.p_get_business_config
        @key = 'bwb.request_veryfication_email',
        @groupProcessInstanceId =@groupProcessInstanceId,
        @value = @value OUTPUT,
        @description = @description OUTPUT

    set @mailTo = ''
    IF @value <> ''
      BEGIN
        SET @mailTo = dbo.f_getRealEmailOrTest(@value)
      END

    SET @mailSubject = dbo.f_translate('Prośba o potwierdzenie uprawnień Concordia Assistance,',default)+ isnull(@makeModel,'') +', ' + isnull(@licencePlate,'')  + dbo.f_translate(', numer zgłoszenia Starter24: ',default) + isnull([dbo].[f_caseId](@rootId),'')


    SET @body = dbo.f_translate('Szanowni Państwo,',default)
                +'</br></br>'
                +dbo.f_translate('Chcielibyśmy poinformować, że nie udało nam się potwierdzić w przekazanej nam bazie uprawnionych pojazdów wpisu dotyczącego samochodu, który zgodnie z twierdzeniem Klienta, powinien być objęty assistance z ramienia ubezpieczenia w Concord',default)
                +dbo.f_translate('Zgodnie z uzgodnioną procedurą, udzieliliśmy pomocy na podstawie deklaracji Klienta.',default)
                +'</br>Poniżej znajdą Państwo szczegóły sprawy. '
                +'</br>Marka i model: ' + ISNULL(@makeModel,'')
                +'</br>Numer rejestracyjny: ' + ISNULL(@licencePlate,'')
                +'</br>Polisa numer: '+ isnull(@serialAndNumber,'')
                +'</br>Deklarowany wariant: ' + isnull(@programName,'')
                + '</br>Klient: ' + isnull(@clientContact,'')
                +'</br></br></br>'
                +dbo.f_translate('Z poważaniem,',default)
                +'</br>Zespół Concordia Assistance'
                +'</br>tel.: +48 61 83 19 915'
                +'</br>ul. Józefa Kraszewskiego 30, 60-519 Poznań'
                +'</br>Treść tego dokumentu jest poufna i prawnie chroniona. Odbiorcą może być jedynie jej adresat z wyłączeniem dostępu osób trzecich. Jeżeli nie jest Pani/Pan adresatem niniejszej wiadomości, jej rozpowszechnianie, kopiowanie lub inne działanie o podobnym charakterze jest prawnie zabronione. W razie otrzymania tej wiadomości jako niezamierzony odbiorca, proszę o poinformowanie o tym fakcie nadawcy, a następnie usunięcie wiadomości ze swojego systemu.
							  STARTER Sp. z o.o., ul. Józefa Kraszewskiego 30, 60-519 Poznań, wpisana przez Sąd Rejonowy w Poznaniu, VIII Wydział Gospodarczy Krajowego Rejestru Sądowego, KRS: 0000056095; kapitał zakładowy: 12 500 000,00 zł; NIP: 525-21-83-310; REGON: 016387736
							  Niniejsza wiadomość oraz wszystkie załączone do niej pliki przeznaczone są do wyłącznego użytku zamierzonego adresata i mogą zawierać chronione lub poufne informacje. Przeglądanie, wykorzystywanie, ujawnianie lub dystrybuowanie przez osoby do tego nieupoważnione jest zabronione. Jeśli nie jest Pan/Pani wymienionym adresatem niniejszej wiadomości, prosimy o niezwłoczny kontakt z nadawcą i usunięcie oryginalnej wiadomości oraz zniszczenie wszystkich jej kopii.
							  The information in this email is confidential and may be legally privileged. It is intended solely for the addressee. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited and may be unlawful. If you received this email as the unintended recipient, please inform the sender and delete this message.'
                +'</br></br>'

    DECLARE @sendMail nvarchar(100)
    SET @sendMail = dbo.f_getRealEmailOrTest ('callcenter@starter24.pl')

    EXECUTE dbo.p_note_new
        @groupProcessId = @groupProcessInstanceId
        ,@type = dbo.f_translate('email',default)
        ,@content = dbo.f_translate('CONCORDIA',default)
        ,@email = @mailTo
        ,@userId = 1  -- automat
        ,@subject = @mailSubject
        ,@direction=1
        ,@dw = ''
        ,@udw = ''
        ,@sender = @sendMail
        ,@emailBody = @body
        ,@err=@err OUTPUT
        ,@message=@message OUTPUT

  END