ALTER PROCEDURE [dbo].[s_1007_056]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @partnerId INT
	DECLARE @body VARCHAR(MAX)
	DECLARE @email VARCHAR(400)
	DECLARE @subject VARCHAR(400)
	DECLARE @fixingService INT
	DECLARE @fixingEndDate DATETIME
	DECLARE @hash NVARCHAR(40)
	DECLARE @daysCount INT
	DECLARE @daysLimit INT
	DECLARE @hashTaskId INT
	DECLARE @rentalDuration INT
	DECLARE @eta DATETIME
	DECLARE @monit1 INT
	DECLARE @monit2 INT
	DECLARE @makeModel NVARCHAR(255)
	DECLARE @regNumber NVARCHAR(255)
	DECLARE @vin NVARCHAR(255)
	DECLARE @endDate DATETIME
	DECLARE @content NVARCHAR(4000)
	DECLARE @fixingServicePhone NVARCHAR(255)
	DECLARE @daysLeft INT
	DECLARE @subType1 NVARCHAR(255)
	DECLARE @subType2 NVARCHAR(255)
	DECLARE @rootId INT
	DECLARE @fixingPartnerId INT
	DECLARE @towingFixingGroup INT
	declare @logContent NVARCHAR(500)
	declare @p1 NVARCHAR(255)
	declare @estimated int	
	
	SELECT	@groupProcessInstanceId = group_process_id,
	@rootId = root_id
	FROM process_instance with(nolock)
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	CREATE TABLE #replacecmentCarSummary (duration INT, real_duration INT, days_left INT, end_date DATETIME)
	
	SELECT TOP 1 @towingFixingGroup = group_process_id FROM dbo.process_instance with(nolock) WHERE root_id = @rootId AND step_id like '1009.%' order by id desc
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '522', @groupProcessInstanceId = @towingFixingGroup
	SELECT @fixingPartnerId = value_int FROM @values
	
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '764,742', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values

--	DELETE FROM @values
--	INSERT @values EXEC p_attribute_get2 @attributePath = '286', @groupProcessInstanceId = @rootId
--	SELECT @fixingEndDate = value_date FROM @values
	
	exec [dbo].[p_fixing_end_date] 
	@rootId = @rootId, 
	@fixingEndDate = @fixingEndDate output

	SELECT TOP 1 @hash = token FROM dbo.process_instance with(nolock) WHERE step_id = '1021.003' AND root_id = @rootId order by id desc
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @vin = value_string FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = textD FROM dbo.dictionary where value = (SELECT value_int FROM @values) AND typeD = 'makeModel'
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '798', @groupProcessInstanceId = @rootId
	SELECT @estimated = value_int from @values
	
	
	EXEC p_replacement_car_summary
	@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @daysLeft = days_left, @endDate = end_date FROM #replacecmentCarSummary
	
	DROP TABLE #replacecmentCarSummary
	
	SELECT @fixingServicePhone = dbo.f_partner_contact(@groupProcessInstanceId,@fixingPartnerId, 1, 3)
	SET @subType1 = 'AZ1-'+CAST(@previousProcessId AS NVARCHAR(20))
	SET @subType2 = 'AZ2-'+CAST(@previousProcessId AS NVARCHAR(20))
	SELECT @monit1 = id FROM dbo.attribute_value with(nolock) WHERE value_string = @subType1 AND root_process_instance_id = @rootId
	SELECT @monit2 = id FROM dbo.attribute_value with(nolock) WHERE value_string = @subType2 AND root_process_instance_id = @rootId
	
	IF @fixingEndDate IS NOT NULL OR (GETDATE() >= @endDate AND @monit2 IS NOT NULL)
	BEGIN
		IF (@fixingEndDate <= GETDATE() AND isnull(@estimated,0) = 0) OR @daysLeft <= 0
		BEGIN
			SET @logContent = dbo.f_translate('Automatyczne zakończenie wynajmu pojazdu zastępczego.',default)
			SET @variant = 2
		END 
		ELSE
		BEGIN
			SET @logContent = dbo.f_translate('Automatyczne przedłużenie wynajmu pojazdu zastępczego.',default)
			SET @variant = 1
		END
	END 
	ELSE
	BEGIN		
		
		SET @variant = 99
		
		-- 4 godziny przed końcem doby wynajmu
		IF GETDATE() >= DATEADD(HOUR, -4, @endDate) and @monit1 IS NULL
		BEGIN
			SET @content = dbo.f_translate('Prosimy o pilną aktualizację naprawy warsztatowej dotyczącej auta ',default)+@makeModel+dbo.f_translate(' o numerze rej. ',default)+@regNumber+dbo.f_translate(' oraz VIN ',default)+@vin+dbo.f_translate(' w celu przedłużenia wynajmu auta zastępczego dla klienta. Odnośnik do formularza: ',default)+dbo.f_hashTaskUrl(@hash)+dbo.f_translate(' / Z poważaniem zespół Starter24',default)
			
		   	EXEC dbo.p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @fixingServicePhone,
			@addInfo = 0,
			@subType = @subType1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
				 
		END 
		ELSE IF GETDATE() >= DATEADD(MINUTE, -150, @endDate) and @monit2 IS NULL
		BEGIN
			SET @content = dbo.f_translate('Prosimy o pilną aktualizację naprawy warsztatowej dotyczącej auta ',default)+@makeModel+dbo.f_translate(' o numerze rej. ',default)+@regNumber+dbo.f_translate(' oraz VIN ',default)+@vin+dbo.f_translate('. Dalszy brak aktualizacji spowoduje konieczność przedłużenia wynajmu auta zastępczego dla klienta, czego kosztami mogą być Państwo obciążeni. Odnośnik do formularza do aktualizacji statusu naprawy: ',default)+dbo.f_hashTaskUrl(@hash)+dbo.f_translate(' / Z poważaniem zespół Starter24',default)
			
			EXEC dbo.p_note_new
			@groupProcessId = @groupProcessInstanceId,
			@type = dbo.f_translate('sms',default),
			@content = @content,
			@phoneNumber = @fixingServicePhone,
			@addInfo = 0,
			@subType = @subType2,
			@err = @err OUTPUT,
			@message = @message OUTPUT
					
		END

		SELECT @content
		UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(MINUTE, 5, GETDATE()) where id = @previousProcessId
		
	END 
	
	IF ISNULL(@logContent,'') <> ''
	BEGIN
		SET @p1 = dbo.f_caseId(@rootId)
		EXEC dbo.p_log_automat
		@name = 'rental_automat',
		@content = @logContent,
		@param1 = @p1		
	END
END






