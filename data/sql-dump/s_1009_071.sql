ALTER PROCEDURE [dbo].[s_1009_071]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int = 1,
	@errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	DECLARE @groupProcessInstanceId INT
	DECLARE @amountRefused SMALLINT
	DECLARE @stepId VARCHAR(32)
	DECLARE @email VARCHAR(400)
	DECLARE @subject VARCHAR(400)
	DECLARE @body VARCHAR(MAX)
	declare @VIN nvarchar(100)
	declare @regNumber nvarchar(100)
	
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))

	-- Sprawdzenie czy zadanie jest przekładane
	IF @variant = 99
	BEGIN
		
		SELECT @amountRefused = postpone_count FROM dbo.process_instance with(nolock) WHERE id = @previousProcessId
		
		SET @amountRefused = ISNULL(@amountRefused, 0)
		
		IF @amountRefused = 0
		BEGIN
			
			-- Pierwsza próba przełożenia, wysłanie e-maila do PZ
			INSERT  @values EXEC dbo.p_attribute_get2
		 		@attributePath = '767,773,368',
		 		@groupProcessInstanceId = @groupProcessInstanceId
		 	SELECT @email = value_string FROM @values
		 	
		 	delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '73,71', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @VIN = value_string FROM @values
		
			delete from @values
			INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
			SELECT @regNumber = value_string FROM @values
	
			SET @body='Hello from Poland. Did you get our request for roadside assistance?
					<BR>
					Details:<BR>
					Starter24 Case Number: ' +  dbo.f_caseId(@previousProcessId) + ' <BR>
					' + ISNULL('Plate number: '+@regNumber,'')+' '+ ISNULL('VIN: '+@VIN,'')+'<BR>
					<BR><BR>
					Best regards,<BR>
					Starter24'
							
			SET @subject = 'Did you get our request for roadside assistance? - ' + dbo.f_caseId(@previousProcessId)

			UPDATE dbo.process_instance SET postpone_count = postpone_count + 1, postpone_date = DATEADD(minute, 15, GETDATE()) WHERE id = @previousProcessId
		
			DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('Arc')

			EXECUTE dbo.p_note_new 
				 @groupProcessId = @groupProcessInstanceId
				,@type = dbo.f_translate('email',default)
				,@content = dbo.f_translate('Zapytanie o otrzymaniu zlecenie',default)
				,@email = @email
				,@userId = 1  -- automat
				,@subject = @subject
				,@direction=1
				,@dw = ''
				,@udw = ''
				,@sender = @senderEmail
				,@additionalAttachments = ''
				,@emailBody = @body
				,@err=@err OUTPUT
				,@message=@message OUTPUT
			
		END
		
	END
	
END



