ALTER PROCEDURE [dbo].[P_send_email_to_ald_swap_rental]
	@previousProcessId INT,	-- Może być GROUP / ROOT
	@emailTo NVARCHAR(400) -- EMAIL SERWISU
AS
BEGIN
	
	/*		
	 
	TYLKO ALD
	
	Automatyczne powiadomienie o konieczności podmiany auta zastępczego przez serwis.
	
	a. Warunek wysłania: wszystkie sprawy szkodowe w których przypisany serwis jest blacharsko-lakierniczy oraz sprawy awarii w których przypisany serwis z grupy ALD preferowany – po zorganizowaniu pojazdu zastępczego wysyłamy mail.
	
	b. Wyjątek: nie wysyłamy tego powiadomienia w przypadku pojazdów dostawczych i małych dostawczych oraz gdy wynajem zostaje zorganizowany od razu z serwisu
 	
 	c. Wysyłane do: adres mailowy warsztatu, DW: szkodyald@starter24.pl
 	
 	____________________________________*/

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @content NVARCHAR(MAX)
	DECLARE @title NVARCHAR(MAX)
	
	-- Pobranie podstawowych danych --
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	
	SET @content = '
		Szanowni Państwo,</br></br>

		Informujemy, że zorganizowaliśmy pojazd zastępczy z wypożyczalni zewnętrznej dla Użytkownika pojazdu z tematu.
		
		Sprawa {#caseid#}</br>
		Nr rejestracyjny auta: {@74,72@}</br>
		Marka i model pojazdu: {@74,73@}</br>
		Użytkownik: {@80,342,64@} {@80,342,66@}</br>
		Nr tel.: {@80,342,408,197@}</br>
		Data rozpoczęcia wynajmu: {#showDatetime({@540@}|1)#}</br></br>
		
		W przypadku braku podmiany przez Państwa pojazdu zastępczego w wyznaczonym czasie (2 dni robocze od daty przyjęcia pojazdu do serwisu), zostaną Państwo obciążeni ewentualną różnicą między kosztem wynajętego przez nas pojazdu zastępczego wraz z kosztami dostarczenia i odbioru, a kosztami uzgodnionymi w Ofercie Handlowej (zgodnie z zapisami w umowie - § 5 p 12 pomiędzy ALD, a serwisem mechanicznym / § 5 p 13 pomiędzy ALD, a serwisem blacharsko-lakierniczym ). 

	'

	EXEC [dbo].[P_parse_string]
	 @processInstanceId = @previousProcessId,
	 @simpleText = 'Powiadomienie do sprawy [{#caseid#} / {@74,72@}]',
	 @parsedText = @title OUTPUT

	EXEC [dbo].[P_get_body_email]
		@body = @body OUTPUT,
		@contentEmail  = @content,
		@title = @title,
		@previousProcessId = @previousProcessId	
		
	SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'CFM')
	SET @body = REPLACE(@body,'__EMAIL__', 'cfm@starter24.pl')
	 
	 DECLARE @email VARCHAR(400)
	 DECLARE @dw NVARCHAR(400)
	 DECLARE @sender NVARCHAR(400)
	 DECLARE @contentNote NVARCHAR(1000) = [dbo].[f_showEmailInfo](@emailTo) + dbo.f_translate('Automatyczne powiadomienie o konieczności podmiany auta zastępczego przez serwis.',default)
	 
	 SET @email = dbo.f_getRealEmailOrTest(@emailTo)
	 SET @sender = dbo.f_getEmail('cfm')
	 SET @dw = dbo.f_getRealEmailOrTest('szkodyald@starter24.pl')
	 
	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @groupProcessInstanceId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = @contentNote
	 	,@email = @email
	 	,@userId = 1  -- automat
	 	,@subject = @title
	 	,@direction=1
	 	,@sender = @sender
	 	,@emailBody = @body
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT
	
END
