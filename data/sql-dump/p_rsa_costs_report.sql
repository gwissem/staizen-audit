
ALTER PROCEDURE [dbo].[p_rsa_costs_report]
( 
	@groupProcessInstanceId INT,
	@useTempTable INT = 0
) 
AS 
BEGIN
	DECLARE @report TABLE (name NVARCHAR(255), fromCase NVARCHAR(1000), fromContractor NVARCHAR(1000), err INT)
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	DECLARE @arrivalDistance DECIMAL(18,2)
	DECLARE @returnDistance DECIMAL(18,2)
	DECLARE @towingDistance DECIMAL(18,2)
	DECLARE @startDate DATETIME
	DECLARE @onPlaceDate DATETIME
	DECLARE @reportDate DATETIME
	DECLARE @finishDate DATETIME
	DECLARE @towingEndDate DATETIME 
	DECLARE @fixingEndDate DATETIME 
	DECLARE @diagnosisCode NVARCHAR(255)
	DECLARE @closeCode NVARCHAR(255)
	DECLARE @err INT
	DECLARE @calculatedArrivalDistance DECIMAL(18,2)
	DECLARE @calculatedReturnDistance DECIMAL(18,2)
	DECLARE @calculatedTowingDistance DECIMAL(18,2)
	DECLARE @peopleToTransport INT
	DECLARE @peopleTransportedInCabin INT
	DECLARE @peopleTransportedInSeparateCar INT
	DECLARE @separateCarDistance DECIMAL(18,2)
	DECLARE @calculatedSeparateCarDistance DECIMAL(18,2)
	DECLARE @manHourTime INT
	DECLARE @calculatedManHourTime INT
	DECLARE @liftManHourTime INT
	DECLARE @peopleInCabin INT
	DECLARE @towingProblems INT
	DECLARE @viatollCosts DECIMAL(18,2)
	DECLARE @otherCosts DECIMAL(18,2)
	DECLARE @highwayCosts DECIMAL(18,2)
	DECLARE @programId NVARCHAR(255)
	DECLARE @distance DECIMAL(18,5)
	DECLARE @latStart DECIMAL(18,5)
	DECLARE @longStart DECIMAL(18,5)
	DECLARE @latEventPlace DECIMAL(18,5)
	DECLARE @longEventPlace DECIMAL(18,5)
	DECLARE @latEnd DECIMAL(18,5)
	DECLARE @longEnd DECIMAL(18,5)
	DECLARE @partnerId INT
	DECLARE @label NVARCHAR(255)
	DECLARE @calculatedPeopleToTransport NVARCHAR(255)
	DECLARE @platform nvarchar(100)
	DECLARE @program nvarchar(100)
	DECLARE @makeModel nvarchar(100)
	declare @regNumber nvarchar(50)
	declare @VIN nvarchar(50)
	declare @DMC nvarchar(50)
	declare @mileage nvarchar(100)
	DECLARE @makeModelICS nvarchar(100)
	declare @regNumberICS nvarchar(50)
	declare @VINICS nvarchar(50)
	declare @DMCICS nvarchar(50)
	declare @mileageICS nvarchar(100)
	declare @fixingOrTowing int
	declare @caseId varchar(50)
	declare @root_id int
	declare @isGop int 
	declare @gopCost int 
	declare @gopCalculatedCost int 
	DECLARE @gopViatollCosts DECIMAL(18,2)
	DECLARE @gopOtherCosts DECIMAL(18,2)
	DECLARE @gopHighwayCosts DECIMAL(18,2)
	DECLARE @gopManHourTime INT	
	DECLARE @gopLiftManHourTime INT
	DECLARE @gopManHourTimeReason NVARCHAR(1000)
	DECLARE @gopLiftManHourTimeReason NVARCHAR(1000)
	
	select top 1 @root_id=root_id
	from dbo.process_instance
	Where group_process_id=@groupProcessInstanceId
	
	set @caseId=dbo.f_caseId(@root_id)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @platform = name from dbo.platform with(nolock) where id =(select top 1 value_int FROM @values)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @program = name from dbo.vin_program with(nolock) where id =(select top 1 value_int FROM @values)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModel = textD from dbo.dictionary with(nolock) where value =(select top 1 value_int FROM @values) and typeD='makeModel'

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regnumber=value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VIN=value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,76', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @DMC=value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,73', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @makeModelICS = textD from dbo.dictionary with(nolock) where value =(select top 1 value_int FROM @values) and typeD='makeModel'

	if isnull(@makeModelICS,'')='' set @makeModelICS=@makeModel

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regnumberICS=value_string FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VINICS=value_string FROM @values

	declare @err2 int
	declare @message2 nvarchar(300)

	if isnull(@VIN,'')<>'' and isnull(@VINICS,'')=''
	begin
		set @VINICS=@VIN
	end

	if isnull(@VIN,'')='' and isnull(@VINICS,'')<>''
	begin
		set @VIN=@VINICS

		EXEC [dbo].[p_attribute_edit]
		   	@attributePath = '74,71', 
		   	@groupProcessInstanceId = @groupProcessInstanceId,
		   	@stepId = 'xxx',
		   	@userId = 1,
		   	@originalUserId = 1,
		   	@valueString = @VIN,
		   	@err = @err2 OUTPUT,
		   	@message = @message2 OUTPUT
	end

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,76', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @DMCICS=value_int FROM @values

	if @DMCICS is null set @DMCICS=@DMC

	if @DMCICS<>@DMC and @DMCICS is not null
	begin
		EXEC [dbo].[p_attribute_edit]
		   	@attributePath = '74,76', 
		   	@groupProcessInstanceId = @groupProcessInstanceId,
		   	@stepId = 'xxx',
		   	@userId = 1,
		   	@originalUserId = 1,
		   	@valueInt = @DMCICS,
		   	@err = @err2 OUTPUT,
		   	@message = @message2 OUTPUT

		set @DMC=@DMCICS
	end

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,75', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @mileage=value_int FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,223,75', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @mileageICS=value_int FROM @values

	-- Jeżeli A(wart.z Atlas) jest równa „null” lub 1 oraz K(wart.z Kontraktor) > 1 to bierzemy K;
	if (@mileage is null or @mileage=1) and @mileageICS is not null
	begin
		set @mileage=@mileageICS

		EXEC [dbo].[p_attribute_edit]
		   	@attributePath = '74,75', 
		   	@groupProcessInstanceId = @groupProcessInstanceId,
		   	@stepId = 'xxx',
		   	@userId = 1,
		   	@originalUserId = 1,
		   	@valueInt = @mileage,
		   	@err = @err2 OUTPUT,
		   	@message = @message2 OUTPUT
	end

	-- Jeżeli K jest równa „null” lub 1 oraz A > 1 to bierzemy A;
	if @mileageICS is null and @mileage is not null and @mileage>1
	begin
		set @mileageICS=@mileage
	end
	
	-- Jeżeli A>1 oraz K>1 oraz dl(długość ciągu znaków ) A. = dł.K to bierzemy K
	if isnull(@mileageICS,0)>1 and isnull(@mileage,0)>1 and len(@mileage)=len(@mileageICS)
	begin
		set @mileage=@mileageICS

		EXEC [dbo].[p_attribute_edit]
		   	@attributePath = '74,75', 
		   	@groupProcessInstanceId = @groupProcessInstanceId,
		   	@stepId = 'xxx',
		   	@userId = 1,
		   	@originalUserId = 1,
		   	@valueInt = @mileage,
		   	@err = @err2 OUTPUT,
		   	@message = @message2 OUTPUT
	end
	
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,225', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towingDistance = value_decimal FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
		
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1005,957', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @gopCalculatedCost=value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,957', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @gopCost=value_int FROM @values
	
	declare @service nvarchar(100) 

	if @fixingOrTowing=1 --and isnull(@towingDistance,0.0)<>0.0
	begin
		set @service=dbo.f_translate('Holowanie',default)
	end
	else
	begin
		--if isnull(@towingDistance,0.0)<>0.0
		--begin
			set @service=dbo.f_translate('Naprawa',default)
		--end
	end
	declare @code1 nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,720', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @code1 = left(textD,3) from dbo.dictionary with(nolock) where value =(select top 1 value_int FROM @values) and typeD='ARCCode1'

	declare @code2 nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,721', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @code2 = left(textD,2) from dbo.dictionary with(nolock) where value =(select top 1 value_int FROM @values) and typeD='ARCCode2'

	declare @code3 nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,722', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @code3 = left(textD,2) from dbo.dictionary with(nolock) where value =(select top 1 value_int FROM @values) and typeD='ARCCode3'


	declare @drivePossible nvarchar(200)
	set @drivePossible=''

	Set @drivePossible=case 
			when right(@code3,2) in ('61','59','96','52','69','63','58','6A','16','20','60','1A','56','85','90','89','54','3A','4A','2A','88') then dbo.f_translate('Możliwa',default)
			when right(@code3,2) in ('55','51')	then dbo.f_translate('Możliwa warunkowo',default)
			when right(@code3,2) in ('$$')			then dbo.f_translate('Dopuszczona tylko jazda do najbl. serwisu',default)
			when right(@code3,2) in ('5A','5G')	then dbo.f_translate(dbo.f_translate('Niemożliwa',default),default)
			else  dbo.f_translate('Możliwa',default) end

	if isnull(@towingDistance,0.0) <> 0.0
	begin
		set @drivePossible=dbo.f_translate(dbo.f_translate('Niemożliwa',default),default)
	end

	declare @serviceVehicle nvarchar(100)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,273', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @serviceVehicle = case when value_int=2 then dbo.f_translate('Patrolowy',default) else dbo.f_translate('Holownik',default) end FROM @values

	declare @isTermICS nvarchar(100)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,215', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isTermICS = case when value_int=1 then dbo.f_translate(dbo.f_translate('Tak',default),default) else dbo.f_translate(dbo.f_translate('Nie',default),default) end FROM @values
	
	declare @isTerm nvarchar(100)

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '215', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isTerm = case when value_int=1 then dbo.f_translate(dbo.f_translate('Tak',default),default) else dbo.f_translate(dbo.f_translate('Nie',default),default) end FROM @values
	
	if @isTermICS=dbo.f_translate(dbo.f_translate('Tak',default),default) and @isTerm=dbo.f_translate(dbo.f_translate('Nie',default),default)
	begin
		EXEC [dbo].[p_attribute_edit]
		   	@attributePath = '215', 
		   	@groupProcessInstanceId = @groupProcessInstanceId,
		   	@stepId = 'xxx',
		   	@userId = 1,
		   	@originalUserId = 1,
		   	@valueInt = 1,
		   	@err = @err2 OUTPUT,
		   	@message = @message2 OUTPUT

		set @isTerm=dbo.f_translate(dbo.f_translate('Tak',default),default)
	end

	declare @partnerFindDate datetime
	
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,217', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @arrivalDistance = value_decimal FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,218', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @returnDistance = value_decimal FROM @values
	
	
	SELECT top 1 @startDate = created_at FROM dbo.process_instance with(nolock) WHERE step_id = '1009.004' AND group_process_id = @groupProcessInstanceId order by id 
	SELECT top 1 @partnerFindDate = created_at FROM dbo.process_instance with(nolock) WHERE step_id in ('1009.018','1009.003') AND group_process_id = @groupProcessInstanceId and date_leave is not null order by date_leave 
	SELECT top 1 @reportDate = created_at FROM dbo.process_instance with(nolock) WHERE step_id = '1009.016' AND group_process_id = @groupProcessInstanceId order by id 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,130', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingEndDate = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,268', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towingEndDate = value_date FROM @values
	
	SELECT @finishDate = ISNULL(@towingEndDate, @fixingEndDate)
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,214', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @onPlaceDate = value_date FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,216', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @closeCode = value_string FROM @values
	
	if isnull(@closeCode,'')=''
	begin
		set @closeCode=@code1+@code2+@code3
	end

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,644,154', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @peopleTransportedInCabin = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,154', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @peopleTransportedInSeparateCar = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '428', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @peopleToTransport = value_int FROM @values
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,645,647', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @separateCarDistance = value_int FROM @values
	
	DECLARE @parentId INT
	
	SELECT @parentId = parent_attribute_value_id FROM dbo.attribute_value with(nolock) 
	WHERE group_process_instance_id = @groupProcessInstanceId AND value_int = 1 AND attribute_path = '638,219,220'
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '1005,992', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isGop = value_int FROM @values
	
	IF @parentId IS NOT NULL
	BEGIN
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,222', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @highwayCosts = value_decimal FROM @values
		
		IF @isGop = 1
		BEGIN
			SELECT @gopHighwayCosts = av1.value_decimal
		    FROM  dbo.attribute_value av1 with(nolock)
		    INNER JOIN dbo.attribute_value av2 with(nolock) on av2.parent_attribute_value_id = av1.parent_attribute_value_id AND av2.attribute_path = '1005,219,220'
		    INNER JOIN dbo.attribute_value av3 with(nolock) on av3.parent_attribute_value_id = av1.parent_attribute_value_id AND av3.attribute_path = '1005,219,221'
		    INNER JOIN dbo.attribute_value av4 with(nolock) on av4.parent_attribute_value_id = av1.parent_attribute_value_id AND av4.attribute_path = '1005,219,63'		    
		    WHERE av1.attribute_path = '1005,219,222'
		    AND av1.group_process_instance_id = @groupProcessInstanceId
		    AND av1.value_decimal > 0
		    AND av2.value_int = 1
		END 
		
		SET @parentId = NULL
	END 
	
	SELECT @parentId = parent_attribute_value_id FROM dbo.attribute_value  with(nolock)
	WHERE group_process_instance_id = @groupProcessInstanceId AND value_int = 2 AND attribute_path = '638,219,220'
	
	IF @parentId IS NOT NULL
	BEGIN
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,222', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @viatollCosts = value_decimal FROM @values
		
		IF @isGop = 1
		BEGIN
			SELECT @gopViatollCosts = av1.value_decimal
		    FROM  dbo.attribute_value av1 with(nolock)
		    INNER JOIN dbo.attribute_value av2 with(nolock) on av2.parent_attribute_value_id = av1.parent_attribute_value_id AND av2.attribute_path = '1005,219,220'
		    INNER JOIN dbo.attribute_value av3 with(nolock) on av3.parent_attribute_value_id = av1.parent_attribute_value_id AND av3.attribute_path = '1005,219,221'
		    INNER JOIN dbo.attribute_value av4 with(nolock) on av4.parent_attribute_value_id = av1.parent_attribute_value_id AND av4.attribute_path = '1005,219,63'		    
		    WHERE av1.attribute_path = '1005,219,222'
		    AND av1.group_process_instance_id = @groupProcessInstanceId
		    AND av1.value_decimal > 0
		    AND av2.value_int = 2
		END 
		
		SET @parentId = NULL
	END 

	SELECT @parentId = parent_attribute_value_id FROM dbo.attribute_value  with(nolock)
	WHERE group_process_instance_id = @groupProcessInstanceId AND value_int = 6 AND attribute_path = '638,219,220'
	IF @parentId IS NOT NULL
	BEGIN
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,222', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @otherCosts = value_decimal FROM @values
		
		IF @isGop = 1
		BEGIN
			SELECT @gopOtherCosts = av1.value_decimal
		    FROM  dbo.attribute_value av1 with(nolock)
		    INNER JOIN dbo.attribute_value av2 with(nolock) on av2.parent_attribute_value_id = av1.parent_attribute_value_id AND av2.attribute_path = '1005,219,220'
		    INNER JOIN dbo.attribute_value av3 with(nolock) on av3.parent_attribute_value_id = av1.parent_attribute_value_id AND av3.attribute_path = '1005,219,221'
		    INNER JOIN dbo.attribute_value av4 with(nolock) on av4.parent_attribute_value_id = av1.parent_attribute_value_id AND av4.attribute_path = '1005,219,63'		    
		    WHERE av1.attribute_path = '1005,219,222'
		    AND av1.group_process_instance_id = @groupProcessInstanceId
		    AND av1.value_decimal > 0
		    AND av2.value_int = 6
		END 
		
		SET @parentId = NULL
	END 
	
	SELECT @parentId = parent_attribute_value_id FROM dbo.attribute_value with(nolock) 
	WHERE group_process_instance_id = @groupProcessInstanceId AND value_int = 3 AND attribute_path = '638,219,220'
	
	IF @parentId IS NOT NULL
	BEGIN
		
		PRINT '---'
		PRINT @parentId
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,221', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @manHourTime = value_int FROM @values
		
		declare @manHourTimeReason nvarchar(1000)
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,63', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @manHourTimeReason = value_text FROM @values

		IF @isGop = 1
		BEGIN
			SELECT @gopManHourTime = av3.value_int, @gopManHourTimeReason = av4.value_text
		    FROM  dbo.attribute_value av1 with(nolock)
		    INNER JOIN dbo.attribute_value av2 with(nolock) on av2.parent_attribute_value_id = av1.parent_attribute_value_id AND av2.attribute_path = '1005,219,220'
		    INNER JOIN dbo.attribute_value av3 with(nolock) on av3.parent_attribute_value_id = av1.parent_attribute_value_id AND av3.attribute_path = '1005,219,221'
		    INNER JOIN dbo.attribute_value av4 with(nolock) on av4.parent_attribute_value_id = av1.parent_attribute_value_id AND av4.attribute_path = '1005,219,63'		    
		    WHERE av1.attribute_path = '1005,219,222'
		    AND av1.group_process_instance_id = @groupProcessInstanceId
		    AND av1.value_decimal > 0
		    AND av2.value_int = 3
		END 
		PRINT '---'
		PRINT @manHourTime
		
		SET @parentId = NULL
	END 
	
	SELECT @parentId = parent_attribute_value_id FROM dbo.attribute_value with(nolock) 
	WHERE group_process_instance_id = @groupProcessInstanceId AND value_int = 4 AND attribute_path = '638,219,220'
	
	IF @parentId IS NOT NULL
	BEGIN
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,221', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @liftManHourTime = value_int FROM @values
		
		declare @liftManHourTimeReason nvarchar(1000)
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,63', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @liftManHourTimeReason = value_text FROM @values

		IF @isGop = 1
		BEGIN
			SELECT @gopLiftManHourTime = av3.value_int, @gopLiftManHourTimeReason = av4.value_text
		    FROM  dbo.attribute_value av1 with(nolock)
		    INNER JOIN dbo.attribute_value av2 with(nolock) on av2.parent_attribute_value_id = av1.parent_attribute_value_id AND av2.attribute_path = '1005,219,220'
		    INNER JOIN dbo.attribute_value av3 with(nolock) on av3.parent_attribute_value_id = av1.parent_attribute_value_id AND av3.attribute_path = '1005,219,221'
		    INNER JOIN dbo.attribute_value av4 with(nolock) on av4.parent_attribute_value_id = av1.parent_attribute_value_id AND av4.attribute_path = '1005,219,63'		    
		    WHERE av1.attribute_path = '1005,219,222'
		    AND av1.group_process_instance_id = @groupProcessInstanceId
		    AND av1.value_decimal > 0
		    AND av2.value_int = 4
		END 

		SET @parentId = NULL
	END 
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '610', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @partnerId = value_int FROM @values

	SELECT @latStart = value_string 
	FROM attribute_value with(nolock) 
	WHERE attribute_path = '595,597,85,93'
	and parent_attribute_value_id = (SELECT id FROM attribute_value with(nolock) where parent_attribute_value_id = @partnerId and attribute_path = '595,597,85')
	
	SELECT @longStart = value_string 
	FROM attribute_value with(nolock) 
	WHERE attribute_path = '595,597,85,92'
	and parent_attribute_value_id = (SELECT id FROM attribute_value with(nolock) where parent_attribute_value_id = @partnerId and attribute_path = '595,597,85')
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @latEventPlace = CAST(value_string as decimal(10,8)) FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @longEventPlace = CAST(value_string as decimal(10,8)) FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @latEnd = CAST(value_string as decimal(10,8)) FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '211,85,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @longEnd = CAST(value_string as decimal(10,8)) FROM @values

--	SELECT @calculatedArrivalDistance = 1.2 * [dbo].[FN_distance](@latStart, @longStart, @latEventPlace, @longEventPlace)
--  SELECT @calculatedTowingDistance = 1.2 * [dbo].[FN_distance](@latEventPlace, @longEventPlace, @latEnd, @longEnd)
--  SELECT @calculatedReturnDistance = 1.2 * [dbo].[FN_distance](@latEnd, @longEnd, @latStart, @longStart)

	EXEC p_map_distance
	@longitude1 = @longStart,
	@latitude1 = @latStart,
	@longitude2 = @longEventPlace,
	@latitude2 = @latEventPlace,
	@distance = @calculatedArrivalDistance OUTPUT

	PRINT '---longStart'
	PRINT @longStart
	PRINT '---latStart'
	PRINT @latStart
	PRINT '---longEventPlace'
	PRINT @longEventPlace
	PRINT '---latEventPlace'
	PRINT @latEventPlace
	
	EXEC p_map_distance
	@longitude1 = @longEventPlace,
	@latitude1 = @latEventPlace,
	@longitude2 = @longEnd,
	@latitude2 = @latEnd,
	@distance = @calculatedTowingDistance OUTPUT
	
	EXEC p_map_distance
	@longitude1 = @longEnd,
	@latitude1 = @latEnd,
	@longitude2 = @longStart,
	@latitude2 = @latStart,
	@distance = @calculatedReturnDistance OUTPUT

   	SELECT @calculatedSeparateCarDistance = ISNULL(@calculatedArrivalDistance,0) + ISNULL(@calculatedReturnDistance,0) + ISNULL(@calculatedTowingDistance,0)
   	
	SELECT @calculatedManHourTime = DATEDIFF(hh, @startDate, @finishDate)
	
	--Czas trwania dojazdu (gg:mm)
	declare @arrivalTime nvarchar(50)
	set @arrivalTime=left(convert(char(8), dateadd(ss, DATEDIFF(ss,@startDate,@onPlaceDate),cast(0 as DateTime)), 108),5)

	--Czas trwania interwencji na miejscu (gg:mm)
	declare @onSiteTime nvarchar(50)
	set @onSiteTime=left(convert(char(8), dateadd(ss, DATEDIFF(ss,@onPlaceDate,@fixingEndDate),cast(0 as DateTime)), 108),5)

	--Czas trwania holowania (gg:mm)
	declare @towingTime nvarchar(50)
	set @towingTime=left(convert(char(8), dateadd(ss, DATEDIFF(ss,@fixingEndDate,@towingEndDate),cast(0 as DateTime)), 108),5)

	--Czas oczekiwania na pomoc łącznie (gg:mm)
	declare @helpTime nvarchar(50)
	set @helpTime=left(convert(char(8), dateadd(ss, DATEDIFF(ss,@partnerFindDate,@onPlaceDate),cast(0 as DateTime)), 108),5)

	declare @Xstart decimal(18,7)
	declare @Ystart decimal(18,7)
	declare @XOnSite decimal(18,7)
	declare @YOnSite decimal(18,7)
	declare @XFinish decimal(18,7)
	declare @YFinish decimal(18,7)
	declare @XOnSiteICS decimal(18,7)
	declare @YOnSiteICS decimal(18,7)
	declare @XFinishICS decimal(18,7)
	declare @YFinishICS decimal(18,7)


	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,817,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @Xstart = CAST(value_string as decimal(10,8)) FROM @values order by value_string desc

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,817,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @Ystart = CAST(value_string as decimal(10,8)) FROM @values order by value_string desc

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,818,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @XOnSiteICS = CAST(value_string as decimal(10,8)) FROM @values order by value_string desc

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,818,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @YOnSiteICS = CAST(value_string as decimal(10,8)) FROM @values order by value_string desc

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @XOnSite = CAST(value_string as decimal(10,8)) FROM @values order by value_string desc

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '101,85,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @YOnSite = CAST(value_string as decimal(10,8)) FROM @values order by value_string desc
	
	declare @AtlasICSDistOnSIte int
	set @AtlasICSDistOnSIte = 1.2 * [dbo].[FN_distance](@YOnSite, @XOnSite, @YOnSiteICS, @XOnSiteICS)

	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,819,92', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @XFinishICS = CAST(value_string as decimal(10,8)) FROM @values

	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,819,93', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @YFinishICS = CAST(value_string as decimal(10,8)) FROM @values

	declare @AtlasICSDistFinish int
	set @AtlasICSDistFinish = 1.2 * [dbo].[FN_distance](@YFinish, @XFinish, @YFinishICS, @XFinishICS)

	declare @towing_destination nvarchar(1000)

	EXECUTE dbo.p_location_string
	   @attributePath='211,85'
	  ,@groupProcessInstanceId=@groupProcessInstanceId
	  ,@locationString=@towing_destination OUTPUT


	declare @towing_destinationICS nvarchar(1000)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,640', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @towing_destinationICS = value_text FROM @values

	declare @isPhotoDocumented nvarchar(1000)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,664', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @isPhotoDocumented = case when value_int=1 then dbo.f_translate(dbo.f_translate('Tak',default),default) else dbo.f_translate(dbo.f_translate('Nie',default),default) end FROM @values

	-- START < Edytowane by Dylesiu
	declare @image_file_name nvarchar(1000) = ''
	DECLARE @image_document_id INT
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,845', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @image_document_id = value_int FROM @values

	IF @image_document_id IS NOT NULL AND @image_document_id <> 0
	BEGIN
		
		DECLARE @atlasDomain NVARCHAR(100) =  dbo.f_getDomain();

		DECLARE @idFile uniqueidentifier
		DECLARE @nameFile VARCHAR(255)
		
		SET @image_file_name = @image_file_name + '<ul class="list-options">'
		
		DECLARE file_cursor CURSOR FOR SELECT uuid, name FROM dbo.files with(nolock) WHERE document_id = @image_document_id
		OPEN file_cursor  
		FETCH NEXT FROM file_cursor INTO @idFile, @nameFile  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			
			SET @image_file_name = @image_file_name + '<li><a href="' + @atlasDomain + dbo.f_translate('/documents/download-file/',default) + CAST(@idFile as NVARCHAR(100)) + '">'+@nameFile +'</a>'
			
		    FETCH NEXT FROM file_cursor INTO @idFile, @nameFile  
		END 
		CLOSE file_cursor  
		DEALLOCATE file_cursor 
		
		SET @image_file_name = @image_file_name + '</ul>'

	END
	
	-- Koniec <
	
	declare @eta nvarchar(1000)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '107', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @eta = dbo.FN_VDateHour(value_date) FROM @values
	

	Declare @startAddressICS nvarchar(1000)

	EXEC	 dbo.p_mapReverseGeocodingEmapi
		@lat = @Ystart,
		@lng = @Xstart,
		@address = @startAddressICS OUTPUT

	Declare @onSiteAddressICS nvarchar(1000)
	EXEC	 dbo.p_mapReverseGeocodingEmapi
		@lat = @YOnSiteICS,
		@lng = @XOnSiteICS,
		@address = @onSiteAddressICS OUTPUT

	Declare @onSiteAddress nvarchar(1000)
	EXEC	 dbo.p_mapReverseGeocodingEmapi
		@lat = @YOnSite,
		@lng = @XOnSite,
		@address = @onSiteAddress OUTPUT


	-- Platforma
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Platforma',default), @platform, '', @err
	
	-- Program
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Program',default), @program, '', @err
	
	-- Marka / model pojazdu klienta
	SET @err = 0
	--if isnull(@makeModel,'')<>isnull(@makeModelICS,'')
	--begin
	--	set @err=1
	--end
	INSERT INTO @report SELECT dbo.f_translate('Marka / model pojazdu klienta',default), @makeModel, @makeModelICS, @err
	
	-- Numer rejestracyjny pojazdu klienta
	SET @err = 0
	--if isnull(@regNumber,'')<>isnull(@regNumberICS,'')
	--begin
	--	set @err=1
	--end
	INSERT INTO @report SELECT dbo.f_translate('Numer rejestracyjny pojazdu klienta',default), @regNumber, @regNumberICS, @err
	
	-- VIN pojazdu klienta
	SET @err = 0
	if isnull(@VIN,'')<>isnull(@VINICS,'')
	begin
		set @err=1
	end
	INSERT INTO @report SELECT dbo.f_translate('VIN pojazdu klienta',default), @VIN, @VINICS, @err
	
	-- DMC pojazdu klienta
	SET @err = 0
	if isnull(@DMC,'')<>isnull(@DMCICS,'')
	begin
		set @err=1
	end
	INSERT INTO @report SELECT dbo.f_translate('DMC pojazdu klienta',default), @DMC, @DMCICS, @err
	
	-- Przebieg
	SET @err = 0
	if isnull(@mileage,'')<>isnull(@mileageICS,'')
	begin
		set @err=1
	end
	INSERT INTO @report SELECT dbo.f_translate('Przebieg',default), @mileage, @mileageICS, @err
	
	INSERT INTO @report SELECT dbo.f_translate('Status usługi',default), '', '', -1
	
	-- Rodzaj usługi podstawowej wykonanej
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Rodzaj usługi podstawowej wykonanej',default), @service, '', @err
	
	-- Dalsza jazda
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Dalsza jazda',default), @drivePossible, '', @err
	
	-- Wykonywał pojazd
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Wykonywał pojazd',default), @serviceVehicle, '', @err
	
	INSERT INTO @report SELECT dbo.f_translate('Daty i godziny realizacji usługi',default), '', '', -1
	
	-- Czy umówione na termin?
	SET @err = 0
	--if isnull(@isTerm,'')<>isnull(@isTermICS,'')
	--begin
	--	set @err=1
	--end
	INSERT INTO @report SELECT 'Czy umówione na termin?', @isTerm, @isTermICS, @err
	
	-- ETA podana przez kontraktora
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('ETA podana przez kontraktora',default), @eta, '', @err
	
	
	-- Data i czas przekazania zlecenia do pomocy drogowej
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Data i czas przekazania zlecenia do pomocy drogowej',default), dbo.FN_VDateHour2(@partnerFindDate,0), '', @err
	
	-- Data i czas dbo.f_translate('kontraktor wyjechał',default)
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Data i czas ',default)dbo.f_translate('kontraktor wyjechał',default)'','' ,dbo.FN_VDateHour2(@startDate,0), @err
	
	-- Data i czas dbo.f_translate('na miejscu zdarzenia',default)
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Data i czas ',default)dbo.f_translate('na miejscu zdarzenia',default)'','' ,dbo.FN_VDateHour2(@onPlaceDate,0), @err
	
	-- Data i czas dbo.f_translate('zakończono na miejscu',default)
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Data i czas ',default)dbo.f_translate('zakończono na miejscu',default)'','' ,dbo.FN_VDateHour2(@fixingEndDate,0), @err
	
	-- Data i czas dbo.f_translate('zakończono holowanie',default)
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Data i czas ',default)dbo.f_translate('zakończono holowanie',default)'','' ,dbo.FN_VDateHour2(@towingEndDate,0), @err
	
	INSERT INTO @report SELECT dbo.f_translate('Czasy trwania',default), '', '', -1
	
	-- Czas trwania dojazdu (gg:mm)
	SET @err = 0
	INSERT INTO @report SELECT 'Czas trwania dojazdu (gg:mm)','' ,@arrivalTime, @err
	
	-- Czas trwania interwencji na miejscu (gg:mm)
	SET @err = 0
	INSERT INTO @report SELECT 'Czas trwania interwencji na miejscu (gg:mm)','' ,@onSiteTime, @err
	
	-- Czas trwania holowania (gg:mm)
	SET @err = 0
	INSERT INTO @report SELECT 'Czas trwania holowania (gg:mm)','' ,@towingTime, @err
	
	-- Czas trwania holowania (gg:mm)
	SET @err = 0
	INSERT INTO @report SELECT 'Czas oczekiwania na pomoc łącznie (gg:mm)','' ,@helpTime, @err

	--****Koorynaty i adresy
	INSERT INTO @report SELECT dbo.f_translate('Koorynaty i adresy',default), '', '', -1
	
	
	--if @groupProcessInstanceId=69226 select @Xstart
	-- Koordynaty początkowe pomocy drogowej
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Koordynaty początkowe pomocy drogowej',default),'',concat(@Ystart,', ',@Xstart), @err

	-- Adres początkowy pomocy drogowej
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Adres początkowy pomocy drogowej',default),'',@startAddressICS, @err

	-- Koordynaty miejsca zdarzenia
	SET @err = 0
	if @AtlasICSDistOnSIte>25
	begin
		set @err=1
	end
	INSERT INTO @report SELECT dbo.f_translate('Koordynaty miejsca zdarzenia',default),concat(@YOnSite,', ',@XOnSite),concat(@YOnSiteICS,',',@XOnSiteICS), @err
	INSERT INTO @report SELECT dbo.f_translate('Adres miejsca zdarzenia',default),@onSiteAddress, @onSiteAddressICS, @err
	INSERT INTO @report SELECT dbo.f_translate('Miejsce zdarzenia: różnica w km',default),'',cast(@AtlasICSDistOnSIte as varchar(20)), @err

	-- Koordynaty miejsca zdarzenia
	SET @err = 0
	if @AtlasICSDistFinish>25
	begin
		set @err=1
	end
	INSERT INTO @report SELECT dbo.f_translate('Koordynaty miejsca docelowego holowania',default),concat(@YFinish,', ',@XFinish),concat(@YFinishICS,',',@XFinishICS), @err
	INSERT INTO @report SELECT dbo.f_translate('Adres holowania',default),@towing_destination , @towing_destinationICS, @err
	INSERT INTO @report SELECT dbo.f_translate('Miejsce docelowe holowania: różnica w km',default),'',cast(@AtlasICSDistFinish as varchar(20)), @err

	declare @icsId int
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
			@attributePath = '609',
			@groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values

	if isnull(@icsId,-1)<>-1
	begin
		INSERT INTO @report SELECT dbo.f_translate('Pokaż na mapie',default),'','<A target=_blank href=''https://teka.starter24.pl/Sprawa.php?oid='+cast(@icsId as varchar(100)) +'&teka=2&uid=727334B1-73FD-4D3E-9088-D6DBD4ACA529''>Pokaż na mapie</A>', @err
	end

	INSERT INTO @report SELECT dbo.f_translate('km DHP',default), '', '', -1
	--INSERT INTO @report SELECT dbo.f_translate('Liczba km dojazdu',default),@distance,cast(@AtlasICSDistOnSIte as varchar(20)), @err

	-- Liczba km dojazdu
	SET @err = 0
	IF @arrivalDistance > 40 AND (@calculatedArrivalDistance * 1.1) < @arrivalDistance
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('Liczba km dojazdu',default), CAST(@calculatedArrivalDistance AS NVARCHAR(100)), CAST(@arrivalDistance AS NVARCHAR(100)), @err
	

	-- Liczba km holowania
	SET @err = 0
	IF @towingDistance > 40 AND (@calculatedTowingDistance * 1.1) < @towingDistance
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('Liczba km holowania',default), CAST(@calculatedTowingDistance AS NVARCHAR(100)), CAST(@towingDistance AS NVARCHAR(100)), @err

	-- Liczba km powrotu
	SET @err = 0
	IF @returnDistance > 40 AND (@calculatedReturnDistance * 1.1) < @returnDistance
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('Liczba km powrotu',default), CAST(@calculatedReturnDistance AS NVARCHAR(100)), CAST(@returnDistance AS NVARCHAR(100)), @err
	
	declare @verifyKm nvarchar(1000)
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '638,63', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @verifyKm = value_text FROM @values

	INSERT INTO @report SELECT dbo.f_translate('Weryfikacja kilometrów',default), '', ISNULL(@verifyKm, ''), 0

	IF @isGop = 1
	BEGIN
		INSERT INTO @report SELECT 'Koszt holowania (GOP)', '', '', -1
		
		SET @err = IIF(ISNULL(@gopCalculatedCost,0) < ISNULL(@gopCost,0),1,0)		
		INSERT INTO @report SELECT dbo.f_translate('Koszt holowania',default), ISNULL(cast(@gopCalculatedCost as nvarchar(255)),''), ISNULL(CAST(@gopCost AS NVARCHAR(100)),''), @err
		
	END 
	
	INSERT INTO @report SELECT dbo.f_translate('Roboczogodziny',default), '', '', -1

	SET @err = IIF(ISNULL(@manHourTime,0) > ISNULL(@gopManHourTime,0),1,0)	
	INSERT INTO @report SELECT dbo.f_translate('Liczba RBH Zwykłe',default), ISNULL(cast(@gopManHourTime as nvarchar(255)),''), CAST(@manHourTime AS NVARCHAR(100)), @err
	INSERT INTO @report SELECT dbo.f_translate('Uzasadnienie RBH Zwykłe',default),isnull(cast(@gopManHourTimeReason as nvarchar(255)),''), @manHourTimeReason, @err
	
	SET @err = 0
	IF (ISNULL(@liftManHourTime,0) > ISNULL(@gopliftManHourTime,0)) OR (ISNULL(@isGop,0) = 0 AND ISNULL(@towingProblems,0) NOT IN (2,3) AND ISNULL(@liftManHourTime,0) > 0) -- i jeszcze diagnoza zatrzaśnięte kluczyki/zablokowana skrzynia automatyczna
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('Liczba RBH Dźwig',default), isnull(cast(@gopliftManHourTime as nvarchar(255)),''), CAST(@liftManHourTime AS NVARCHAR(100)), @err
	INSERT INTO @report SELECT dbo.f_translate('Uzasadnienie RBH Dźwig',default), isnull(cast(@gopLiftManHourTimeReason as nvarchar(255)),''), @liftManHourTimeReason, @err
	
	INSERT INTO @report SELECT dbo.f_translate('Liczba osób, koszty VIATOLL i autostrad',default), '', '', -1

	SET @err = 0
	IF ISNULL(@peopleToTransport,0) < 2 AND ISNULL(@peopleTransportedInSeparateCar,0) > 0
	BEGIN
		SET @err = 1
	END 
	
	IF ISNULL(@peopleToTransport,0) > 2
	BEGIN
		SET @calculatedPeopleToTransport = dbo.f_translate('laweta - 2, osobny pojazd - ',default)+ CAST((@peopleToTransport - 2) AS NVARCHAR(20))
	END 
	ELSE
	BEGIN
		SET @calculatedPeopleToTransport = dbo.f_translate('laweta - ',default)+CAST(@peopleToTransport AS NVARCHAR(20)) +dbo.f_translate(', osobny pojazd - 0',default)	
	END 
	
	SET @label = dbo.f_translate('laweta - ',default) +CAST(@peopleTransportedInCabin AS NVARCHAR(20)) +dbo.f_translate(', osobny pojazd - ',default) + CAST(@peopleTransportedInSeparateCar AS NVARCHAR(20))
	INSERT INTO @report SELECT dbo.f_translate('Ilość transportowanych osób',default), @calculatedPeopleToTransport, @label , @err
		
	SET @err = 0
	IF ISNULL(@separateCarDistance,0) > @calculatedSeparateCarDistance
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('Dystans transportu osobnym pojazdem',default), CAST(@calculatedSeparateCarDistance AS NVARCHAR(100)) , CAST(@separateCarDistance AS NVARCHAR(100)), @err
	
	SET @err = 0
	IF ISNULL(@viatollCosts,0) > ISNULL(@gopViatollCosts,30)
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('Koszty VIATOLL',default), 'do '+isnull(CAST(@gopViatollCosts AS NVARCHAR(255)),'30')+dbo.f_translate(' zł netto',default), @viatollCosts, @err
	
	SET @err = 0
	IF ISNULL(@otherCosts,0) > ISNULL(@gopOtherCosts,30)
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT 'Koszty inne ()', ISNULL(CAST(@gopOtherCosts AS NVARCHAR(255)),''), @otherCosts, @err
	
	SET @err = 0
	IF ISNULL(@highwayCosts,0) > ISNULL(@gopHighwayCosts,45) OR (ISNULL(@highwayCosts,0) > 0 AND @programId LIKE '%423%')
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('Koszty autostrad',default), 'do '+isnull(CAST(@gopHighwayCosts AS NVARCHAR(255)),'30')+' zł brutto (z wyłączeniem Ad-hoc)', CAST(@highwayCosts AS NVARCHAR(100)), @err
	
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Dokumentacja zdjęciowa',default), '', @isPhotoDocumented, @err

	-- DANE O PARKINGU
	
	INSERT INTO @report SELECT dbo.f_translate('Parking',default), '', '', -1
	
	DECLARE @parkingDays INT
	DECLARE @parkingCosts DECIMAL(18,2)
	DECLARE @gopParkingDays INT
	DECLARE @gopParkingCosts DECIMAL(18,2)
	
	SELECT @parentId = parent_attribute_value_id FROM dbo.attribute_value with(nolock)
	WHERE group_process_instance_id = @groupProcessInstanceId AND value_int = 5 AND attribute_path = '638,219,220'
	
	IF @parentId IS NOT NULL
	BEGIN
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,221', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @parkingDays = value_int FROM @values
		
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '638,219,222', @groupProcessInstanceId = @groupProcessInstanceId, @parentAttributeId = @parentId
		SELECT @parkingCosts = value_decimal FROM @values

		IF @isGop = 1
		BEGIN
			SELECT @gopParkingDays = av3.value_int, @gopParkingCosts = av1.value_text
		    FROM  dbo.attribute_value av1 with(nolock)
		    INNER JOIN dbo.attribute_value av2 with(nolock) on av2.parent_attribute_value_id = av1.parent_attribute_value_id AND av2.attribute_path = '1005,219,220'
		    INNER JOIN dbo.attribute_value av3 with(nolock) on av3.parent_attribute_value_id = av1.parent_attribute_value_id AND av3.attribute_path = '1005,219,221'
		    INNER JOIN dbo.attribute_value av4 with(nolock) on av4.parent_attribute_value_id = av1.parent_attribute_value_id AND av4.attribute_path = '1005,219,63'		    
		    WHERE av1.attribute_path = '1005,219,222'
		    AND av1.group_process_instance_id = @groupProcessInstanceId
		    AND av1.value_decimal > 0
		    AND av2.value_int = 5
		END 
		
		SET @parentId = NULL
	END 
	
	SET @err = 0
	IF ISNULL(@parkingCosts,0) > ISNULL(@gopParkingCosts,0) and (@parkingCosts>90 or isnull(@parkingCosts,0)/isnull(@parkingDays,1)>30)
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('Ilość dni',default), ISNULL(CAST(@gopParkingDays AS NVARCHAR(255)),''), CAST(@parkingDays as NVARCHAR(100)), @err
	INSERT INTO @report SELECT dbo.f_translate('Kwoty',default), ISNULL(CAST(@gopParkingCosts AS NVARCHAR(255)),''), CAST(@parkingCosts as NVARCHAR(100)), @err
	
	-- 
	
	INSERT INTO @report SELECT dbo.f_translate('Kody zamknięcia',default), '', '', -1

	SET @err = 0
	IF ISNULL(@closeCode,'') = ''
	BEGIN
		SET @err = 1
	END 
	INSERT INTO @report SELECT dbo.f_translate('kod zamknięcia',default), dbo.f_diagnosis_code(@root_id), @closeCode, @err
	
	declare @ArcCode1 nvarchar(10)
	declare @ArcCode2 nvarchar(10)
	declare @ArcCode3 nvarchar(10)

	declare @ArcCodeD nvarchar(1000)
	declare @ArcCode1D nvarchar(100)
	declare @ArcCode2D nvarchar(100)
	declare @ArcCode3D nvarchar(100)
	set @closeCode=replace(replace(@closeCode,'_',''),' ','')

	set @ArcCode1=left(@closeCode,3)
	set @ArcCode2=substring(@closeCode,4,2)
	set @ArcCode3=substring(@closeCode,6,2)

	select	 @ArcCode1D=textD
	from	dbo.dictionary with(nolock) where typeD like 'ARCCode1' and left(argument2,3)=@ArcCode1

	select	 @ArcCode2D=textD
	from	dbo.dictionary with(nolock) where typeD like 'ARCCode2' and left(argument2,3)=@ArcCode2

	select	 @ArcCode3D=textD
	from	dbo.dictionary with(nolock) where typeD like 'ARCCode3' and left(argument2,3)=@ArcCode3

	set @ArcCodeD=isnull(@ArcCode1D+', ','')+isnull(@ArcCode2D+', ','')+isnull(@ArcCode3D,'')

	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('kod zamknięcia - opis',default), dbo.f_diagnosis_description(@root_id,'pl'), @ArcCodeD, @err
	
	SET @err = 0
	INSERT INTO @report SELECT dbo.f_translate('Raport z drogi',default), '', @image_file_name, @err
	
	IF @useTempTable = 1
	BEGIN
		INSERT #rsaCostReportTable
		SELECT * FROM @report
	END 
	ELSE
	BEGIN
		SELECT * FROM @report
	END 
	
END 



