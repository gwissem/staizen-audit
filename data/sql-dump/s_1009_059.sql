ALTER PROCEDURE [dbo].[s_1009_059]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int, 
    @errId int=0 output
)
AS
BEGIN
    DECLARE @groupProcessInstanceId INT
    DECLARE @newProcessInstanceId INT
    DECLARE @auto INT
    DECLARE @content NVARCHAR(255)
    DECLARE @err INT
    DECLARE @message NVARCHAR(255)
	SELECT @groupProcessInstanceId = group_process_id FROM dbo.process_instance with(nolock) WHERE id = @previousProcessId
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	DECLARE @partnerPhone NVARCHAR(255)
	DECLARE @icsId INT
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '609', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @icsId = value_int FROM @values
	
	IF ISNULL(@icsId,-1) = -1 
	BEGIN
		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '691', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @partnerPhone = value_string FROM @values
		
		SET @content = dbo.f_translate('Zlecenie ',default)+dbo.f_caseId(@groupProcessInstanceId)+' jest już nieaktywne (odwołanie kontraktora). Pozdrawiamy, Starter24'
		
		EXEC p_note_new
		@groupProcessId = @groupProcessInstanceId,
		@type = dbo.f_translate('sms',default),
		@content = @content,
		@phoneNumber = @partnerPhone,
		@userId = 1,
		@originalUserId = 1,
		@addInfo = 0,
		@err = @err OUTPUT,
		@message = @message OUTPUT			
	END 
	ELSE
	BEGIN
		PRINT dbo.f_translate('aaa',default)

		INSERT OPENQUERY (Teka2, 'SELECT order_id, status_id, submitted_at, created_at, updated_at
								FROM   StarterTeka.order_statuses')
		select @icsId        
		, 10     
		, getdate()       
		, getdate()       
		, getdate()       
		
		declare @sql NVARCHAR(400) = 'UPDATE OPENQUERY (Teka2, ''SELECT * FROM StarterTeka.orders where id = '+CAST(@icsId AS VARCHAR(10))+' '') set active = 0'
		exec(@sql)
		
	END 
	
	SET @auto = 1	
	IF @variant = 2
	BEGIN
		SET @auto = 0
	END 
	
    EXEC [dbo].[p_rsa_process_new]
	@processInstanceId = @previousProcessId,
	@addPartnerToBlackList = 1,
	@auto = @auto,
	@cancelCurrent = 1,
	@mute = 2,
    @newProcessInstanceId = @newProcessInstanceId output
    
    INSERT INTO dbo.process_instance_flow (previous_process_instance_id, next_process_instance_id, active)
    VALUES (@previousProcessId, @newProcessInstanceId, 1)
    
    
END



