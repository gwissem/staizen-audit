ALTER PROCEDURE [dbo].[s_1009_061]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser int = 1,
	@errId int=0 output
) 
AS
BEGIN
	
	DECLARE @err INT
	DECLARE @message VARCHAR(400)
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId VARCHAR(32)
	DECLARE @country VARCHAR(64)
	DECLARE @email VARCHAR(100)
	DECLARE @phone VARCHAR(100)
	DECLARE @locationId INT
	DECLARE @amountResult INT
	DECLARE @p1 VARCHAR(400)
	DECLARE @tempContent VARCHAR(400)
	DECLARE @fdds INT 
	DECLARE @programId VARCHAR(64)
	DECLARE @platformID INT

	DECLARE @fixingOrTowing INT
	
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock)
	WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
--	DECLARE @partners Table (partner_id INT, location_id INT, location_name VARCHAR(100), email VARCHAR(100), phone VARCHAR(20))


	-- Platforma --ZMIANA NA POTRZEBY FORDA KTORY OMIJA KROK 1009.073
 	INSERT  @values EXEC dbo.p_attribute_get2
 		@attributePath = '253',
 		@groupProcessInstanceId = @rootId
 	SELECT @platformID = value_int FROM @values
	
	SET @variant = 1

	-- kraj zdarzenia
	DELETE FROM @values
	INSERT  @values EXEC dbo.p_attribute_get2
 		@attributePath = '101,85,86',
 		@groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @country = value_string FROM @values
	
 	DELETE FROM @values
 	INSERT  @values EXEC dbo.p_attribute_get2
 		@attributePath = '202',
 		@groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @programId = value_string FROM @values
 	
 	DELETE FROM @values
 	INSERT  @values EXEC dbo.p_attribute_get2
 		@attributePath = '560',
 		@groupProcessInstanceId = @groupProcessInstanceId
 	SELECT @fixingOrTowing = value_int FROM @values
 	
	-- Znalezienie partnerów
	CREATE TABLE #partners (partner_id INT, location_id INT, location_name VARCHAR(100), email VARCHAR(100), phone VARCHAR(20), priority INT, extraContent VARCHAR(400))
	EXEC [dbo].[p_find_PZ] @country = @country, @groupProcessInstanceId = @groupProcessInstanceId

	if @country=dbo.f_translate('Zjednoczone Królestwo',default) And not exists(select * from #partners)
	begin
		EXEC [dbo].[p_find_PZ] @country = dbo.f_translate('Wielka Brytania',default), @groupProcessInstanceId = @groupProcessInstanceId
	end

	DELETE FROM #partners 
	WHERE location_id IN (SELECT value_int FROM dbo.attribute_value with(nolock) WHERE attribute_path = '701,702,704' AND value_int IS NOT NULL AND group_process_instance_id = @groupProcessInstanceId)
	
	SELECT @amountResult = COUNT(partner_id) FROM #partners
	
	IF @amountResult = 0
	BEGIN
		
		SET @variant = 2
		DROP TABLE #partners
		
		SET @p1 = dbo.f_caseId(@rootId)
		EXEC dbo.p_log_automat
	 		@name = 'service_NHzG_automat',
	  		@content = dbo.f_translate('Nie znaleziono Partnera Zagranicznego.',default),
	  		@param1 = @p1,
	  		@param3 = @country
	  		
		RETURN
		
	END
	
	SELECT TOP 1 @phone = phone,  @email = email, @locationId = location_id FROM #partners ORDER BY priority ASC
--	SELECT TOP 1 @email = email FROM #partners ORDER BY priority ASC;	
--	SELECT TOP 1 @locationId = location_id FROM #partners ORDER BY priority ASC;
	
	SET @p1 = dbo.f_caseId(@rootId)
	SET @tempContent = dbo.f_translate('Znaleziono Partnera Zagranicznego.',default)
	
	IF @phone IS NOT NULL
	BEGIN
		SET @tempContent = @tempContent + dbo.f_translate(' Phone: ',default) + CAST(@phone AS VARCHAR(100))
	END
	
	IF @email IS NOT NULL
	BEGIN
		SET @tempContent = @tempContent + dbo.f_translate(', Email: ',default) + CAST(ISNULL(@email,'') AS VARCHAR(100))
	END
	
	EXEC dbo.p_log_automat
 		@name = 'service_NHzG_automat',
  		@content = @tempContent,
  		@param1 = @p1,
  		@param2 = @locationId,
  		@param3 = @country
	  		
	DROP TABLE #partners

	PRINT dbo.f_translate('Email: ',default) + CAST(@email AS VARCHAR)
	PRINT dbo.f_translate('Phone: ',default) + CAST(@phone AS VARCHAR)
	PRINT dbo.f_translate('locationId: ',default) + CAST(@locationId AS VARCHAR)
	
	-- Zapisanie poszukanego Partnera
	
	EXEC p_attribute_edit
		@attributePath = '767,773,368',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueString = @email,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	EXEC p_attribute_edit
		@attributePath = '767,773,197',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueString = @phone,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	EXEC p_attribute_edit
		@attributePath = '767,773,704',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueInt = @locationId,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		
	-- Reset atrybutu "Czy wpisać FDDS? 
	IF @platformID = 2
	BEGIN
		EXEC p_attribute_edit
			@attributePath = '838,841',
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
	END
	ELSE
	BEGIN
		EXEC p_attribute_edit
			@attributePath = '838,841',
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = 0,
			@err = @err OUTPUT,
			@message = @message OUTPUT
	END
	-- Zapisanie FDDS'a platformy
	
--	SELECT @fdds = vp.fdds FROM dbo.vin_program as vp with(nolock) WHERE vp.id = @programId
	SELECT @fdds = value from dbo.f_get_platform_key_with_description('fdds', @platformID, @programId)
	
	PRINT '@fdds'
	PRINT @fdds
	
	EXEC p_attribute_edit
		@attributePath = '838,712',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueString = @fdds,
		@err = @err OUTPUT,
		@message = @message OUTPUT
	
	
	-- ZAPISANIE ID PARTNERA DO ATRYBUTU W SPRAWIE
	EXEC p_attribute_edit
		@attributePath = '741',
		@groupProcessInstanceId = @rootId,
		@stepId = 'xxx',
		@valueInt = @locationId,
		@err = @err OUTPUT,
		@message = @message OUTPUT

	DECLARE @useFdds INT 
	DECLARE @configKey NVARCHAR(100)
	
	SET @configKey = IIF(@fixingOrTowing = 1, 'towing.always_use_fdds', 'rsa.always_use_fdds')
	SELECT @useFdds = value from dbo.f_get_platform_key_with_description(@configKey, @platformID, @programId)

	IF @useFdds = 1 AND ISNULL(@fdds,'') <> ''
	BEGIN
		SET @variant = 3
	END 
	
	-- Ustawienie potrzebnych danych do stworzenia Assistance Organization Request

	DECLARE @userName VARCHAR(200)
	
	IF @currentUser = 1
	BEGIN
		SET @userName = dbo.f_translate('Automat',default)	
	END
	ELSE 
	BEGIN
		SELECT @userName = CONCAT(fu.firstname, ' ', fu.lastname) FROM dbo.fos_user as fu WHERE fu.id = @currentUser
	END
	
	DELETE FROM @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '560', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @fixingOrTowing = value_int FROM @values
	
	-- Holowanie czy naprawa
	IF @fixingOrTowing = 1
	BEGIN
		
		EXEC p_attribute_edit
			@attributePath = '838,839',
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = 1,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
	END
	ELSE 
	BEGIN
		
		EXEC p_attribute_edit
			@attributePath = '838,839',
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueInt = 2,
			@err = @err OUTPUT,
			@message = @message OUTPUT
		
	END
	
	-- Nazwa użytkownika
	EXEC p_attribute_edit
		@attributePath = '838,840',
		@groupProcessInstanceId = @groupProcessInstanceId,
		@stepId = 'xxx',
		@valueString = @userName,
		@err = @err OUTPUT,
		@message = @message OUTPUT
		
		
	EXEC [dbo].[p_jump_to_service_panel]
	@previousProcessId = @previousProcessId,
	@variant = @variant

	
END



