ALTER PROCEDURE [dbo].[p_send_email_to_CF_harm_redirect]
	@previousProcessId INT	-- Może być GROUP / ROOT
AS
BEGIN
	
	/*		
	 
	TYLKO CF
	Powiadomienie o przekierowaniu szkody
	
 	
 	____________________________________*/

	DECLARE @err INT
	DECLARE @message NVARCHAR(255)
	
	DECLARE @groupProcessInstanceId INT
	DECLARE @rootId INT
	DECLARE @stepId NVARCHAR(255)
	DECLARE @body NVARCHAR(MAX)
	DECLARE @emailTo NVARCHAR(MAX)
	
	DECLARE @content NVARCHAR(MAX)
	DECLARE @title NVARCHAR(MAX)
	
	-- Pobranie podstawowych danych --
	SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
	FROM process_instance  with(nolock) WHERE id = @previousProcessId 
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))	
	
	
	SET @content = '
		Nr sprawy: {#caseid#}</br>
		Nr rejestracyjny: {@74,72@}</br>
		Marka i model pojazdu: {@74,73@}</br>
		Użytkownik: {@80,342,64@} {@80,342,66@}</br>
		Nr tel.: {@80,342,408,197@}</br>
	
	'

	EXEC [dbo].[P_parse_string]
	 @processInstanceId = @previousProcessId,
	 @simpleText = '{#caseid#} / {@74,72@}, Wypadek / szkoda. Otwarto nową sprawę',
	 @parsedText = @title OUTPUT

	EXEC [dbo].[P_get_body_email]
		@body = @body OUTPUT,
		@contentEmail  = @content,
		@title = @title,
		@previousProcessId = @previousProcessId	
		
	SET @body = REPLACE(@body,'__PLATFORM_NAME__', 'CFM')
	SET @body = REPLACE(@body,'__EMAIL__', 'cfm@starter24.pl')
	 
	 DECLARE @email VARCHAR(400)
		SET @emailTo = 'szkody@carefleet.com.pl'
	 DECLARE @sender NVARCHAR(400)
	 DECLARE @contentNote NVARCHAR(1000) = [dbo].[f_showEmailInfo](@emailTo) + dbo.f_translate('Powiadomienie o otwartej sprawie.',default)
	 
	 SET @email = dbo.f_getRealEmailOrTest(@emailTo)
	 SET @sender = dbo.f_getEmail('cfm')

	 
	 EXECUTE dbo.p_note_new 
	 	 @groupProcessId = @groupProcessInstanceId
	 	,@type = dbo.f_translate('email',default)
	 	,@content = @contentNote
	 	,@email = @email
	 	,@userId = 1  -- automat
	 	,@subject = @title
	 	,@direction=1
	 	,@sender = @sender
	 	,@emailBody = @body
	 	,@err=@err OUTPUT
	 	,@message=@message OUTPUT
	
END