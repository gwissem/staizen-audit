ALTER PROCEDURE [dbo].[s_1011_016]
  (
    @previousProcessId INT,
    @variant           TINYINT OUTPUT,
    @currentUser       int, @errId int = 0 output
  )
AS
  BEGIN
    DECLARE @groupProcessInstanceId INT
    DECLARE @stepId NVARCHAR(255)
    DECLARE @err INT
    DECLARE @message NVARCHAR(255)
    DECLARE @platformId INT
    DECLARE @rootId INT
    DECLARE @platformName NVARCHAR(255)
    DECLARE @programId NVARCHAR(255)

    SELECT @groupProcessInstanceId = group_process_id, @stepId = step_id, @rootId = root_id
    FROM process_instance with(nolock)
    WHERE id = @previousProcessId

    DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))

    INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @platformId = value_int FROM @values
    SELECT @platformName = name FROM platform where id = @platformId

    DECLARE @isVinOK INT
    delete from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '467', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @isVinOK = value_int FROM @values

    delete from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @programId = value_string FROM @values

    DECLARE @costsMessage NVARCHAR(MAX)

    DECLARE @useProgramExtra INT
    DECLARE @processPath INT

    delete from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '904', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @useProgramExtra = value_int FROM @values

    delete from @values
    INSERT @values EXEC p_attribute_get2 @attributePath = '115', @groupProcessInstanceId = @groupProcessInstanceId
    SELECT @processPath = value_int FROM @values


    SET @variant = 1

    IF @isVinOK = 0
      BEGIN
        set @variant = 2

        IF @platformId = 58
          BEGIN
            DECLARE @stepExist NVARCHAR(20)
            SELECT top 1 @stepExist = step_id
            from process_instance
            where group_process_id = @groupProcessInstanceId
              and step_id = '1011.071'
            IF isnull(@stepExist, '') != ''
              BEGIN
                set @variant = 5
              end
            else
              SET @variant = 7
          END

      END
    else
      begin

        IF @platformId = 25
          BEGIN
            DECLARE @isConfirmed INT
            DELETE from @values
            INSERT @values EXEC p_attribute_get2 @attributePath = '1041,1042',
                                                 @groupProcessInstanceId = @groupProcessInstanceId
            SELECT @isConfirmed = value_int FROM @values

            IF isnull(@isConfirmed, 0) = 1
              BEGIN
                exec p_process_jump
                    @previousProcessId = @previousProcessId,
                    @nextStepId = '1012.004',
                    @doNext = 0,
                    @parentId = NULL,
                    @rootId = @rootId,
                    @deactivateCurrent = 1,
                    @variant = @variant,
                    @groupId = @rootId,
                    @err = @errId output,
                    @message = @message output

                SET @variant = 99;
              end

          end


        IF @platformId = 58
          BEGIN
            set @variant = 97

            set @costsMessage =
            dbo.f_translate('Niestety, w związku z brakiem uprawnień, nie możemy świadczyć usług w ramach ',default) + @platformName +
            dbo.f_translate(' Assistance',default)

            EXEC p_attribute_edit
                @attributePath = '554',
                @groupProcessInstanceId = @groupProcessInstanceId,
                @stepId = @stepId,
                @valueText = @costsMessage,
                @err = @err OUTPUT,
                @message = @message OUTPUT

            return

          END

        DECLARE @verification INT
        delete from @values
        INSERT @values EXEC p_attribute_get2 @attributePath = '468', @groupProcessInstanceId = @groupProcessInstanceId
        SELECT @verification = value_int FROM @values


        PRINT '@verification'
        PRINT @verification
        PRINT dbo.f_translate('useProgramExtra',default)
        PRINT @useProgramExtra
        PRINT dbo.f_translate('Program',default)
        PRINT isnull(@programId,dbo.f_translate('progrram',default))
        print '@platformId'
        print @platformId



        if @verification = 1
          begin
            set @variant = 1
          end
        else if isnull(@programId, '') not like '%423%'
          begin

            SET @costsMessage =
            dbo.f_translate('Przekazane informacje wskazują iż nie posiada Pan/Pani ważnego pakietu ',default) + @platformName +
            dbo.f_translate(' Assistance. W tej sytuacji możemy zaoferować usługi odpłatne',default)


            if @platformId in (6, 35, 32
              , 18
              , 31
              , 42
              , 59)
            -- Dodane w ramach poprawki, BWB TUW TUZ, 1928

              begin
                EXEC p_attribute_edit
                    @attributePath = '532',
                    @groupProcessInstanceId = @groupProcessInstanceId,
                    @stepId = @stepId,
                    @valueText = @costsMessage,
                    @err = @err OUTPUT,
                    @message = @message OUTPUT

                set @variant = 96
              end
            else if @platformId = 2
              begin
                DECLARE @fordAccidentAssistance INT
                SET @fordAccidentAssistance = @useProgramExtra

                EXEC p_attribute_edit
                    @attributePath = '532',
                    @groupProcessInstanceId = @groupProcessInstanceId,
                    @stepId = @stepId,
                    @valueText = @costsMessage,
                    @err = @err OUTPUT,
                    @message = @message OUTPUT

                set @variant = 96

                if @fordAccidentAssistance = 1
                  begin
                    EXEC p_attribute_edit
                        @attributePath = '202',
                        @groupProcessInstanceId = @groupProcessInstanceId,
                        @stepId = @stepId,
                        @valueString = '439',
                        @err = @err OUTPUT,
                        @message = @message OUTPUT

                    EXEC p_attribute_edit
                        @attributePath = '204',
                        @groupProcessInstanceId = @groupProcessInstanceId,
                        @stepId = @stepId,
                        @valueString = '439',
                        @err = @err OUTPUT,
                        @message = @message OUTPUT

                    EXEC p_attribute_edit
                        @attributePath = '204',
                        @groupProcessInstanceId = @groupProcessInstanceId,
                        @stepId = @stepId,
                        @valueString = '439',
                        @err = @err OUTPUT,
                        @message = @message OUTPUT


                    EXEC p_attribute_edit
                        @attributePath = '423',
                        @groupProcessInstanceId = @groupProcessInstanceId,
                        @stepId = @stepId,
                        @valueText = dbo.f_translate('Wybrał Pan/Pani program Assistance kolizyjne Ford, umożliwia on bezpłatne holowanie do dowolnego serwisu Forda, pod warunkiem naprawy pojazdu w tym serwisie.',default),
                        --dbo.f_translate('Pana/Pani samochód nie posiada aktywnej polisy. W tej sytuacji możemy zaproponować holowanie w ramach programu Ford Assistance Kolizyjny. W ramach tego programu holowanie jest bezpłatne do najbliższego Dealera Forda pod warunkiem realizacji nap',default),

                        @err = @err OUTPUT,
                        @message = @message OUTPUT

                    set @variant = 4
                  end
              end
            else if @useProgramExtra = 1
              BEGIN
                set @variant = 4
                IF @platformId = 53
                  BEGIN

                    EXEC [dbo].[p_attribute_edit]
                        @attributePath = '202',
                        @groupProcessInstanceId = @groupProcessInstanceId,
                        @stepId = 'xxx',
                        @userId = 1,
                        @originalUserId = 1,
                        @valueString = '514',
                        @err = @err OUTPUT,
                        @message = @message OUTPUT

                    EXEC [dbo].[p_attribute_edit]
                        @attributePath = '204',
                        @groupProcessInstanceId = @groupProcessInstanceId,
                        @stepId = 'xxx',
                        @userId = 1,
                        @originalUserId = 1,
                        @valueString = '514',
                        @err = @err OUTPUT,
                        @message = @message OUTPUT

                  END
                ELSE IF @platformId = 25 --& lp
                BEGIN

                  EXEC [dbo].[p_attribute_edit]
                      @attributePath = '202',
                      @groupProcessInstanceId = @groupProcessInstanceId,
                      @stepId = 'xxx',
                      @userId = 1,
                      @originalUserId = 1,
                      @valueString = '575',
                      @err = @err OUTPUT,
                      @message = @message OUTPUT

                  EXEC [dbo].[p_attribute_edit]
                      @attributePath = '204',
                      @groupProcessInstanceId = @groupProcessInstanceId,
                      @stepId = 'xxx',
                      @userId = 1,
                      @originalUserId = 1,
                      @valueString = '575',
                      @err = @err OUTPUT,
                      @message = @message OUTPUT
                end
              END
          end
      end

    IF @processPath = 2 AND @variant = 2
      BEGIN
        SET @variant = 5

        -- wyzeruj VIN
        EXEC [dbo].[p_attribute_edit]
            @attributePath = '74,71',
            @groupProcessInstanceId = @groupProcessInstanceId,
            @stepId = 'xxx',
            @userId = 1,
            @originalUserId = 1,
            @valueString = NULL,
            @err = @err OUTPUT,
            @message = @message OUTPUT

      END
  END