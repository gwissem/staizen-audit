ALTER PROCEDURE [dbo].[s_1018_033]
( 
	@previousProcessId INT,
    @variant TINYINT OUTPUT, @currentUser int, @errId int=0 output
) 
AS
BEGIN
	DECLARE @err TINYINT
	DECLARE @message NVARCHAR(255)
	DECLARE @caseId NVARCHAR(255)
	DECLARE @groupProcessInstanceId INT
	DECLARE @root_id int

	SELECT @groupProcessInstanceId = group_process_id,
		@root_id=root_id
	FROM	process_instance where id = @previousProcessId
	
	DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
	
	declare @partnerEmail nvarchar(200)
	declare @body nvarchar(4000)
	declare @firstname nvarchar(100)
	declare @lastname nvarchar(100)
	declare @VIN nvarchar(100)
	declare @regNumber nvarchar(100)
	declare @towgroupProcessInstanceId int

	declare @partnerLocId int

	select top 1 @towgroupProcessInstanceId=group_process_id
	from dbo.process_instance
	where root_id=@root_id and step_id like '1009.%'
	order by id desc
	--select * from dbo.process_instance where root_id=499628
--	select @root_id
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '741', @groupProcessInstanceId = @towgroupProcessInstanceId
	SELECT @partnerLocId = value_int FROM @values

	SELECT @partnerEmail = dbo.f_partner_contact(@groupProcessInstanceId,@partnerLocId, 3, 4)
	
	declare @extCaseId nvarchar(100)
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '711', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @extCaseId = value_string FROM @values
		
	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '73,71', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @VIN = value_string FROM @values

	delete from @values
	INSERT @values EXEC p_attribute_get2 @attributePath = '74,72', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @regNumber = value_string FROM @values
		

	set @caseId=dbo.f_caseId(@groupProcessInstanceId)

	declare @subject nvarchar(100)

	
	DECLARE @senderEmail VARCHAR(200) = [dbo].[f_getEmail]('callcenter')

	declare @targetCountry nvarchar(50)

	INSERT @values EXEC p_attribute_get2 @attributePath = '62,591,85,86', @groupProcessInstanceId = @groupProcessInstanceId
	SELECT @targetCountry = value_string FROM @values
	DELETE FROM @values

	if @targetCountry=dbo.f_translate('Polska',default)
	begin
		set @subject=''+isnull(@extCaseId,'')+' '+isnull(@caseId,'')
		set @body='Hello from Poland,<BR<BR>
					Regarding case nr '+isnull(@extCaseId,'')+' '+isnull(@caseId,'')+'<BR>'+
					isnull('Plate number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
					Please provide us information about information listed below: Workshop:<BR>
					Adress of the workshop:<BR>
					Phone number:<BR>
					Opening hours:<BR>
					Repair on warranty?*(yes/no)<BR>
					*(if no) cost of the repair that Client has to pay in workshop<BR>
					Repair status?**<BR>
					**(if not finished) when it will be?<BR>
					Parking cost (yes/no)<BR>
					***(if yes) total cost<BR>
					Best regards,<BR>
					Starter24'

			EXECUTE dbo.p_note_new 
			   @sender=@senderEmail
			  ,@groupProcessId=@groupProcessInstanceId
			  ,@type=dbo.f_translate('email',default)
			  ,@content=@body
			  ,@emailBody=@body
			  ,@phoneNumber=null
			  ,@email=@partnerEmail
			  ,@userId=@currentUser
			  ,@subType=null
			  ,@attachment=null
			  ,@subject=@subject
			  ,@direction=1
			  ,@addInfo=null
			  ,@err=@err OUTPUT
			  ,@message=@message OUTPUT
	end
	else
	begin
	---------------------------------------
		
	
		declare @remarksAtlas nvarchar(4000)

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '62,63', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @remarksAtlas = value_text FROM @values
	
		declare @addressId int

		delete from @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '62,591,85', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @addressId = id FROM @values
	
		declare @address nvarchar(200)
		set @address=dbo.f_addressText(@addressId)

		set @caseId=dbo.f_caseId(@groupProcessInstanceId)

		declare @remarks nvarchar(4000)
		set @subject=dbo.f_translate('Request for service - case nr: ',default)+isnull(@caseId,'')
	
		declare @userName nvarchar(100)
		select @userName=isnull(firstname,'') +' '+isnull(lastname,'') from dbo.fos_user where id=@currentUser

		-- Przekopiowanie atrybutu AssistanceOrganizationRequest
		EXEC [dbo].[p_copyAssistanceOrganizationRequest] @groupProcessInstanceId = @groupProcessInstanceId
	
		EXEC dbo.p_attribute_edit
			@attributePath = '838,840', -- konsultant 
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valuestring = @userName,
			@err = @err OUTPUT,
			@message = @message OUTPUT
	
	
		EXEC dbo.p_attribute_edit
			@attributePath = '838,839', -- id usługi
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valueint = 7,
			@err = @err OUTPUT,
			@message = @message OUTPUT
	
		declare @platformId int
		declare @programId int

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @platformId = value_int FROM @values

		DELETE FROM @values
		INSERT @values EXEC p_attribute_get2 @attributePath = '202', @groupProcessInstanceId = @groupProcessInstanceId
		SELECT @programId = value_string FROM @values
    
		declare @abroadPartnerText nvarchar(200) 
		set @abroadPartnerText=dbo.f_get_platform_key('transport.abroad_partner_text',@platformId,@programId)

		

		set @remarks=dbo.f_translate('transport of the vehicle to the address ',default)+isnull(cast(@address as varchar(200)),'')+'
					 '+isnull(@remarksAtlas,'')+'
					 '+isnull(@abroadPartnerText,'')

		EXEC dbo.p_attribute_edit
			@attributePath = '838,63', -- uwagi
			@groupProcessInstanceId = @groupProcessInstanceId,
			@stepId = 'xxx',
			@valuetext = @remarks,
			@err = @err OUTPUT,
			@message = @message OUTPUT
	
	
		set @body=dbo.f_translate('Regarding case no ',default)+isnull(@caseId,'')+'<BR>'+
				isnull('Registration number: '+@regNumber,'')+' '+isnull('VIN: '+@VIN,'')+'<BR>
				Please organize the transport of the vehicle as in attachment<BR><BR><BR>

				Best regards<BR>
				Starter24'

		EXECUTE dbo.p_note_new 
		   @sender=@senderEmail
		  ,@groupProcessId=@groupProcessInstanceId
		  ,@type=dbo.f_translate('email',default)
		  ,@content=@body
		  ,@phoneNumber=null
		  ,@email=@partnerEmail
		  ,@userId=@currentUser
		  ,@subType=null
		  ,@attachment=null
		  ,@subject=@subject
		  ,@direction=1
		  ,@addInfo=null
		  ,@err=@err OUTPUT
		  ,@message=@message OUTPUT
		  ,@emailBody=@body
		  ,@additionalAttachments = '{FILE::assistance_organization_request::OrganizationRequest::true}'
	end

	set @variant=1
	
	-- Skok do panelu usług
	EXEC dbo.p_jump_to_service_panel @previousProcessId = @previousProcessId, @variant = @variant
end


