ALTER PROCEDURE [dbo].[s_1012_004]
  (
    @previousProcessId INT,
    @variant           TINYINT OUTPUT, @currentUser int, @errId int = 0 output
  )
AS
  DECLARE @err TINYINT
  DECLARE @message NVARCHAR(255)

  DECLARE @caseIdAttributePath VARCHAR(255)
  DECLARE @caseIdAttributeValue NVARCHAR(255)
  DECLARE @valueDecimal DECIMAL(18, 5)
  DECLARE @valueDate DATETIME
  DECLARE @valueText NVARCHAR(MAX)
  DECLARE @valueString NVARCHAR(255)
  DECLARE @valueInt INT
  DECLARE @attributeValueId INT
  DECLARE @groupProcessInstanceId INT
  SELECT @groupProcessInstanceId = group_process_id
  FROM process_instance with(nolock)
  where id = @previousProcessId

  SET @caseIdAttributePath = 235

  EXEC dbo.p_attribute_get
      @caseIdAttributePath,
      @groupProcessInstanceId,
      @attributeValueId = @attributeValueId OUTPUT,
      @valueInt = @valueInt OUTPUT,
      @valueString = @valueString OUTPUT,
      @valueText = @valueText OUTPUT,
      @valueDate = @valueDate OUTPUT,
      @valueDecimal = @valueDecimal OUTPUT,
      @err = @err OUTPUT,
      @message = @message OUTPUT


  DECLARE @values Table(id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18, 5))

  DECLARE @platformId int

  INSERT @values EXEC p_attribute_get2 @attributePath = '253', @groupProcessInstanceId = @groupProcessInstanceId
  SELECT @platformId = value_int FROM @values


  IF @variant != 99
    BEGIN
      	
	    IF @variant = 1 or @variant =3
	    BEGIN
			DECLARE @sendSms int
			DECLARE @content nvarchar(4000)
			DECLARE @callerPhone nvarchar(255)
			
		    DELETE FROM @values
		    INSERT @values EXEC p_attribute_get2 @attributePath = '668', @groupProcessInstanceId = @groupProcessInstanceId
	  	 	SELECT @sendSms = value_int FROM @values
		    
	  	 	DELETE FROM @values
		    INSERT @values EXEC p_attribute_get2 @attributePath = '81,342,408,197', @groupProcessInstanceId = @groupProcessInstanceId
	  	 	SELECT @callerPhone = value_string FROM @values
	  	 	
	  	 	IF @sendSms = 1
	  	 	BEGIN
		  	 	IF @platformId = 78
		  	 	BEGIN
			  		SET @content = 'Centrum Obsługi Kierowcy CareFleet – numer telefonu – 0 713 777 555 lub e-mail cok@carefleet.com.pl od poniedziałku do piątku w godzinach 08:00-19:00 i soboty 09:00 – 13:00' 	
		  	 	END 
		  	 	
		  	    IF @content IS NOT NULL
		  	    BEGIN
			  		EXEC dbo.p_note_new
		      		@groupProcessId = @groupProcessInstanceId,
		      		@type = dbo.f_translate('sms',default),
		      		@content = @content,
		      		@phoneNumber = @callerPhone,
		      		@err = @err OUTPUT,
		      		@message = @message OUTPUT    
		  	    END    
	  	 	END    
	    END  
      	 
	    --Check if there are op8en processes for this phone number
      --	SELECT @rootProcessInstanceId = p.id
      --	FROM dbo.attribute_value av WITH(NOLOCK)
      --	JOIN dbo.process_instance p WITH(NOLOCK) ON av.root_process_instance_id = p.id WHERE p.active = 1
      --	AND av.attribute_id = @caseIdAttributePath AND av.value_int = @valueString
      --
      --
      --	EXEC dbo.p_process_change_ancestors
      --	@groupProcessId = @groupProcessInstanceId,
      --	@rootProcessId = @rootProcessInstanceId,
      --	@currentUser int,
      --	@originalUserId INT,
      --	@skipTransaction TINYINT = 0,
      --	@err INT OUTPUT,
      --	@message NVARCHAR(255) OUTPUT
      IF @valueString IS NULL
        BEGIN
          SET @variant = 1
        END
      ELSE
        BEGIN
          SET @variant = 2
        END
    
        EXEC [dbo].[p_attribute_edit]
          @attributePath = '334', 
          @groupProcessInstanceId = @groupProcessInstanceId,
          @stepId = 'xxx',
          @userId = 1,
          @originalUserId = 1,
          @valueInt = 2,
          @err = @err OUTPUT,
          @message = @message OUTPUT

      IF isnull(@platformId,0) = 2
        BEGIN
          exec p_make_survey @processInstanceId = @previousProcessId, @type = 1
        end

    end