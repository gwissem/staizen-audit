<?php

namespace ChatBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use UserBundle\Entity\User;

class ChatVoter extends Voter
{
    const CHAT_ON = 'CHAT_ON';
    const CHAT_OBJECT = "CHAT_OBJECT";

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {

        if (!in_array(
            $attribute,
            array(self::CHAT_ON)
        )
        ) {
            return false;
        }

        if ($subject !== self::CHAT_OBJECT) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return false; // we do not use chat at the moment

        $user = $token->getUser();
        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
//            return true;
        }
        switch ($attribute) {
            case self::CHAT_ON:
                if($user->hasRole('ROLE_CHAT')){
                    return true;
                }
                else {
                    return false;
                }
        }

        throw new \LogicException('This code should not be reached!');
    }

}