<?php

namespace ChatBundle\Service;

use ChatBundle\Entity\ChatUser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class ChatUserManager
{

    const REDIS_CHAT_USERS = 'chat:users:ids';

    /** @var  ContainerInterface */
    private $container;

    /** @var EntityManager $emMssql */
    private $emMssql;

    /** @var EntityManager $emChat */
    private $emChat;

    /** @var  OutputInterface */
    private $output;

    /**
     * ChatUserManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getChatGroupId() {
        return $this->container->getParameter('chat_group_id');
    }

    public function migrateUser(User $user) {
        try {
            $this->setEntityManagers();

            $chatUser = $this->emChat->getRepository('ChatBundle:ChatUser')->find($user->getId());

            if (!$chatUser) $chatUser = new ChatUser($user);

            $this->emChat->persist($chatUser);
            $this->emChat->flush();
        } catch (\Exception $e) {

        }
    }

    private function setEntityManagers()
    {
        if (!$this->emMssql) $this->emMssql = $this->container->get('doctrine.orm.entity_manager');
        if (!$this->emChat) $this->emChat = $this->container->get('doctrine.orm.chat_entity_manager');
    }

    public function executeChatSql($sql) {

        if(!$this->emChat) $this->emChat = $this->container->get('doctrine.orm.chat_entity_manager');

        $statement = $this->emChat->getConnection()->prepare($sql);
        $statement->execute();

    }

    /**
     * @param User $user
     */
    public function updateChatUserInRedis(User $user) {

        if($user->hasRole('ROLE_CHAT')) {
            $this->container->get('snc_redis.default')->sadd(self::REDIS_CHAT_USERS, [$user->getId()]);
        }
        else {
            $this->container->get('snc_redis.default')->srem(self::REDIS_CHAT_USERS, $user->getId());
        }
    }

    public function runMigrations(OutputInterface $output = null)
    {

        $this->output = ($output) ? $output : null;

        $this->setEntityManagers();

        $this->writeln('comment', 'Pobieranie użytkowników Atlasa...');

        /** @var User[] $atlasUsers */
        $atlasUsers = $this->getAlasUsers();

        if(!$atlasUsers) {
            $this->writeln('error', 'Brak użytkowników w bazie');
            die();
        }

        $this->writeln('comment', 'Pobieranie użytkowników z chatu...');

        /** @var ChatUser[] $chatUsers */
        $chatUsers = $this->getChatUsers();

        $chatUsersIds = array_map(function($user) {
            return $user->getId();
        }, $chatUsers);

        $addedUsers = 0;

        $this->writeln('question', 'Rozpoczęcie przetwarzania', true);

        if($output) {
            $progress = new ProgressBar($output, count($atlasUsers));
            $progress->start();
        }

        foreach ($atlasUsers as $atlasUser) {

            $id = $atlasUser->getId();

            if(in_array($id, $chatUsersIds)) {

                /** Pobranie istniejącego użytkownika i zaktualizowanie danych */

                $users = array_filter(
                    $chatUsers,
                    function ($e) use (&$id) {
                        return $e->getId() == $id;
                    }
                );

                /** @var ChatUser $user */
                $user = (is_array($users)) ? reset($users) : $users;

                $user->setUserData($atlasUser);
            }
            else {
                $addedUsers++;
                $user = new ChatUser($atlasUser);
            }

            $this->emChat->persist($user);
            if(isset($progress)) $progress->advance();
        }

        if(isset($progress)) $progress->finish();

        if($addedUsers != 0) {
            $this->writeln('question', 'Insert...', true);
        }

        $this->emChat->flush();

        /** Wrzucenie IDs użytkowników z grupy chat do Redis */
        $this->reloadChatUsersInRedis();

        $this->writeln('comment', 'Dodanych użytkowników: ' . $addedUsers, true);

    }

    private function writeln($type, $msg, $margin = false) {
        if(!$this->output) return false;

        if($margin) $this->output->writeln('');

        switch ($type){
            case 'info': {
                $this->output->writeln('<info>'.$msg.'</info>');
                break;
            }
            case 'comment': {
                $this->output->writeln('<comment>'.$msg.'</comment>');
                break;
            }
            case 'question': {
                $this->output->writeln('<question>'.$msg.'</question>');
                break;
            }
            case 'error': {
                $this->output->writeln('<error>'.$msg.'</error>');
                break;
            }
        }

        if($margin) $this->output->writeln('');

        return true;
    }

    private function getAlasUsers()
    {
        if (!$this->emMssql) return [];
        return $this->emMssql->getRepository('UserBundle:User')->findBy([
            'company' => [1, 2],
            'enabled' => 1
        ]);
    }

    private function getChatUsers()
    {
        if (!$this->emChat) return [];
        return $this->emChat->getRepository('ChatBundle:ChatUser')->findAll();
    }

    /** Buduje od nowa liczbę użytkowników czatu */

    public function reloadChatUsersInRedis()
    {

        if (!$this->emMssql) $this->emMssql = $this->container->get('doctrine.orm.entity_manager');

        /** @var User[] $users */
        $users = $this->emMssql->getRepository('UserBundle:User')->findByRole('ROLE_CHAT');

        $ids = array_map(function (User $user) {
            return $user->getId();
        }, $users);

        $this->container->get('snc_redis.default')->del([self::REDIS_CHAT_USERS]);
        $this->container->get('snc_redis.default')->sadd(self::REDIS_CHAT_USERS, $ids);

    }
}