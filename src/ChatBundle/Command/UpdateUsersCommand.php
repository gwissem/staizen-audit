<?php

namespace ChatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateUsersCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('cometchat:migrations:user')
            ->setDescription('Migracja użytkowników z MSSQL do MySQL');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container = $this->getContainer();

        $chatUserManager = $container->get('chat_bundle.user_manager');

        $chatUserManager->runMigrations($output);

        $output->writeln('');
        $output->writeln('<info>Ukończono aktualizacje</info>');
        $output->writeln('');

    }
}
