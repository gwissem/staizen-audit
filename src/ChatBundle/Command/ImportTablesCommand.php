<?php

namespace ChatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class ImportTablesCommand extends ContainerAwareCommand
{
    const ROOT_PATH = __DIR__ . '/../../../';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('cometchat:import:tables')
            ->setDescription('Improt tabel do bazy danych');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $finder = new Finder();
        $finder->in( self::ROOT_PATH . 'app/Resources/database');
        $finder->name('chat-atlas.sql');

        if(!$finder->count()) {
            $output->writeln('<error>Nie znalezionio chat-atlas.sql</error>');
            die();
        }

        $chatUserManager = $this->getContainer()->get('chat_bundle.user_manager');

        foreach( $finder as $file ){
            $content = $file->getContents();
            $chatUserManager->executeChatSql($content);
        }

        $output->writeln('<info>Baza została zaktualizowana</info>');
    }
}
