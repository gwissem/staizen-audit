<?php

namespace ChatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * ChatUser
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="ChatBundle\Repository\ChatUserRepository")
 */
class ChatUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * ChatUser constructor.
     * @param null $user
     */
    public function __construct($user = null)
    {
        if($user instanceof User) {
            $this->setUserData($user);
        }
    }

    public function setUserData(User $user) {

        if(!$this->id) $this->id = $user->getId();

        $this->setEmail($user->getEmail());

        if(empty($user->getFirstname()) && empty($user->getLastname())) {
            $this->setUsername($user->getUsername());
        }
        else {
            $this->setUsername($user->getFirstname() . ' ' . $user->getLastname());
        }

    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return ChatUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ChatUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}

