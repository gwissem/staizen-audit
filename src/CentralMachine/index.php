<?php

namespace Machines;

use Composer\Autoload\ClassLoader;
use DOMElement;
use React\EventLoop\StreamSelectLoop;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;
use JabberBundle\Communication\ClientRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment;
use Twig_Loader_Filesystem;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../../vendor/autoload.php';

class CentralMachine {

    // Command: php web/machine_central/automatically-status.php

    // Logout old agents: php web/machine_central/automatically-status.php -l=1
    // show stats: php web/machine_central/automatically-status.php -t=1
    // only store stats: php web/machine_central/automatically-status.php -s=1
    // kill session: php web/machine_central/automatically-status.php -k=maciej.melech

    // Docs:    https://developer.cisco.com/media/finesseDevGuide2/

    const LOGIN = 'janusz.nietypowy2';
    const PASS = 'Starter1234#';
    const EXT = 713;

    const TELEPHONY_AGENTS = 'telephony_agents';

    /** Grażyna */
//    const LOGIN = 'grazyna.typowa';
//    const PASS = 'St@r4er0129!';
//    const EXT = 529;

    const TEAM = 18;

    const NOT_READY = "NOT_READY";
    const READY = "READY";
    const LOGOUT = "LOGOUT";
    const TALKING = "TALKING";
    const LOG_USER_STR = NULL;

    /** @var  Crawler */
    private $crawler;

    /** @var  Twig_Environment */
    private $twig;

    /** @var ClientRequest */
    private $clientRequest;

    /** @var  \Predis\Client */
    private $redis;

    private $userPair = [];
    private $team = [];
    private $xmlResponse = null;

    /** @var  OutputFormatter $output */
    private $output;
    /** @var  SymfonyStyle $io */
    private $io;

    public $inWorkState;
    public $usageMemory;
//    public $idCode = 6;
    public $idCode = null;

    private $ONLY_TEAM = false;
    private $SHOW_NOT_LOGGED = false;
    private $ONLY_LOGOUT = false;
    private $SHOW_STATS = true;
    private $STORE_RESPONSE_XML = true;
    private $ONLY_STORE = false;
    private $KILL_SESSION = null;

    public function __construct($argv)
    {
        $logoutOpt = getopt("l:");

        if(!empty($logoutOpt)) {
            $this->ONLY_LOGOUT = filter_var($logoutOpt["l"], FILTER_VALIDATE_BOOLEAN);
        }

        $teamOpt = getopt("t:");

        if(!empty($teamOpt)) {
            $this->ONLY_TEAM = filter_var($teamOpt["t"], FILTER_VALIDATE_BOOLEAN);
        }

        $storeOpt = getopt("s:");

        if(!empty($storeOpt)) {
            $this->ONLY_STORE = filter_var($storeOpt["s"], FILTER_VALIDATE_BOOLEAN);
        }

        $killOpt = getopt("k:");

        if(!empty($killOpt)) {
            $this->KILL_SESSION = $killOpt["k"];
        }

    }


    public function runLoop() {

        $this->loadConfig();
        $this->runReactLoop();

    }

    private function loadConfig() {

        $atlasConfig = new AtlasConfig();

        $this->crawler = new Crawler();

        $loader = new Twig_Loader_Filesystem(__DIR__.'/../JabberBundle/Resources/requestTemplate/');
        $this->twig = new Twig_Environment($loader, array(
//            'cache' => __DIR__ . '/cache_view/',
        ));

        $user = new \JabberBundle\Communication\Entity\CUCUMUser(null, null, [
            'id' => 0,
            'username' => self::LOGIN,
            'password' => self::PASS,
            'extension' => self::EXT
        ]);

        $this->clientRequest = new ClientRequest($user);

        $this->output = new ConsoleOutput();
        $input = new ArgvInput();
        $outputFormatter = new OutputFormatter(true);
        $this->io = new SymfonyStyle($input, $this->output);
        $this->output->setFormatter($outputFormatter);

        $this->inWorkState = [
            'RESERVED',
            'NOT_READY',
            'TALKING',
            'WORK',
            'HOLD'
        ];

        $memory = memory_get_usage() / 1024 / 1024;
        $this->usageMemory = number_format($memory, 3);

        $this->redis = new \Predis\Client('redis://'. $atlasConfig->getParameter('redis_host_proxy') .':'. $atlasConfig->getParameter('redis_port_proxy') . $atlasConfig->getParameter('redis_channel'));

    }

    private function runReactLoop() {

        $loop = \React\EventLoop\Factory::create();

        $this->listenerOnTelephonyCentral($loop);

        $loop->addPeriodicTimer(60, function () {
            $memory = memory_get_usage() / 1024 / 1024;
            $this->usageMemory = number_format($memory, 3);
        });

        $loop->run();

    }

    /**
     * @param StreamSelectLoop $loop
     */
    private function listenerOnTelephonyCentral($loop) {

        /** Zalogowanie i załadowanie potrzebnych danych */

        $isLogin = $this->loginMachine();

        if($isLogin) {

            if($this->KILL_SESSION) {
                $this->killSession();
            }
            elseif($this->ONLY_LOGOUT) {
                $this->showAndLogoutAgents();
            }
            else {
                $this->getUserPair();

                /** Odpalenie głównej pętli */

                $loop->addPeriodicTimer((($this->ONLY_STORE) ? 5 : 1), function () {

                    $this->checkStatuses();

                });
            }

        }

    }

    private function killSession() {

        $success = $this->logoutAction($this->KILL_SESSION);

        try {
            if($success) {
                $this->io->success('Usunięto sesje ' . $this->KILL_SESSION);
            }
            else {
                $this->io->error('Błąd przy usuwaniu.');
            }
        }
        catch (\Exception $exception) {
            $this->io->error('Błąd przy usuwaniu.');
        }

        die();
    }

    private function showAndLogoutAgents() {

        $table = new Table($this->output);
        $style = new TableStyle();
        $table->setStyle($style);

        $team = $this->getTeam();

        $table
            ->setHeaders(array('User (' . count($team) . ')', 'Status', 'Last Active', 'Logout'));

        usort($team, function($a, $b) {
            if ($a['stateChangeTime'] == $b['stateChangeTime']) {
                return 0;
            }
            return ($a['stateChangeTime'] < $b['stateChangeTime']) ? -1 : 1;
        });

        $limitTime = new \DateTime('-12 hour');
        $timeZone = new \DateTimeZone('Europe/Warsaw');
        $limitTime->setTimezone($timeZone);

        foreach ($team as $key => $item) {

            if($item['stateChangeTime']) {
                if($limitTime > $item['stateChangeTime'] && $item['state'] === self::NOT_READY) {

                    $this->logoutAction($item['loginId']);

                    $table->addRow(array(
                        $this->colorUser($item['loginId']),
                        $this->colorStyle($item['state']),
                        ($item['stateChangeTime']) ? $item['stateChangeTime']->format('Y-m-d H:i:s') : '-',
                        '<fg=green>✔</>'
                    ));

                }
                else {
                    $table->addRow(array(
                        $this->colorUser($item['loginId']),
                        $this->colorStyle($item['state']),
                        ($item['stateChangeTime']) ? $item['stateChangeTime']->format('Y-m-d H:i:s') : '-',
                        ''
                    ));
                }
            }

        }

        $table->render();
        die();

    }

    private function colorStyle($style) {
        switch ($style) {
            case self::NOT_READY: {
                return '<fg=red>'.$style.'</>';
            }
            case self::READY: {
                return '<fg=green>'.$style.'</>';
            }
            case self::TALKING: {
                return '<fg=blue>'.$style.'</>';
            }
            default: {
                return $style;
            }
        }
    }

    private function colorUser($userId) {

        if(strpos($userId, 'atlas.') !== false) {
            return '<fg=magenta>'.$userId.'</>';
        }

        return $userId;

    }

    private function checkStatuses() {

        $this->team = $this->getTeam();

//        foreach ($this->userPair as $item) {
//            echo "\r\n" . $item . ": ";
//            foreach ($this->team as $value) {
//                if($item == $value['loginId']) {
//                    echo "Znaleziono!";
//                    break;
//                }
//            }
//        }
//
//        die(1);

        if($this->team !== false) {

            foreach ($this->team as $key => $member) {

                $this->diffState($key, $member['state'], $member['stateChangeTime']);

            }

            if($this->SHOW_STATS) {
                $this->showStats();
            }

        }

    }

    private function diffState($loginId, $state, $stateChangeTime) {

        /** Sprawdzanie czy konsultant w ogóle istnieje w parach */
        if(isset($this->userPair[$loginId])) {

            /** Sprawdzanie czy jego para jest zalogowana */

            $member = (isset($this->team[$this->userPair[$loginId]])) ? $this->team[$this->userPair[$loginId]] : null;
            if($member === null) return false;

            /** Warunki do zmiany statusów */

            /** Sprawdzenie, czy czas zmiany statusu jest nowszy niż jego pary */

            if($stateChangeTime > $member['stateChangeTime']) {

                if(in_array($state, $this->inWorkState)) {

                    if($member['state'] == self::READY) {
                        $this->updateState($member['loginId'], self::NOT_READY);
                    }

                }
                elseif($state === self::READY && $member['state'] == self::NOT_READY) {
                    $this->updateState($member['loginId'], self::READY);
                }

            }

        }

        return false;
    }

    private function updateState($loginId, $state) {

        $this->team[$loginId]['state'] = $state;

        $isUpdated = $this->setStatus($loginId, $state);

        /** TODO - obsługa błędów czy np. dobrze zaktualizowało */

    }

    private function showStats() {

        $stats = $this->getStatsStatus($this->team);

        $outStats = [];

        $this->output->write(chr(27).chr(91).'H'.chr(27).chr(91).'J');

        $half = count($this->userPair) / 2;
        $i = 0;

        foreach ($this->userPair as $key => $stat) {
            if($i >= $half) break;
            $i++;
            if(isset($this->team[$key]) && isset($this->team[$stat])) {
//                $status = (isset($this->team[$key]) && isset($this->team[$stat])) ? '<comment>YES</comment>' : '<error>NOT</error>';
                $this->output->writeln("<info>" . $key . "</info>\t\t<=>\t" . "<info>" . $stat . "</info>");
            }

        }

        foreach ($stats as $key => $stat) {
            $outStats[] = ( "<info>" . $key . "</info>: " . $stat );
        }

        $this->output->writeln("\t\n" . (new \DateTime('now'))->format('Y-m-d H:i:s'));

        array_unshift($outStats, '-------------------');
        array_push($outStats, '-------------------');

        array_push($outStats, 'Current memory usage: <comment>' . $this->usageMemory . '</comment>');

        $this->output->writeln($outStats);

    }

    private function getStatsStatus($team) {

        $stats = [
            "TALKING" => 0,
            "NOT_READY" => 0,
            "READY" => 0,
            "RESERVED" => 0,
            "WORK" => 0
        ];

        foreach ($team as $item) {

            if(isset($stats[$item['state']])) {
                $stats[$item['state']]++;
            }
            else {
                $stats[$item['state']] = 1;
            }
        }

        return $stats;
    }

    private function getUserPair() {

        /** //TODO Dopisać zapytanie do bazy o wyciągnięcie par użytkowników */

//        $userArr = [
//            'atlas.arkadiusz.jani' => 'Arkadiusz.Janiak',
//            'atlas.kamila.bruczyn' => 'kamila.bruczynska',
//            'atlas.krzysztof.warc' => 'krzysztof_w',
//            'atlas.lukasz.rugowsk' => 'Lukasz.Rugowski',
//            'atlas.martyna.mydlar' => 'martyna.cichowicz',
//            'atlas.michal.budnik' => 'Michal.Budnik',
//            'atlas.mikolaj.manick' => 'mikolaj.manicki',
//            'atlas.natalia.kazmie' => 'Natalia.Kazmierczak',
//            'atlas.adam.dykas' => 'adam.dykas',
//            'atlas.blanka.skiba' => 'blanka_s',
//            'atlas.jan.hylla' => 'Jan.Hylla',
//            'atlas.natalia.wolacz' => 'Natalia.Wolaczynska',
//            'atlas.norbert.matcza' => 'norbert.matczak',
//            'atlas.oliwia.kudlack' => 'oliwia.kudlacka',
//            'atlas.paulina.sieber' => 'paulina.siebert',
//            'atlas.piotr.kacki' => 'Piotr.Kacki',
//            'atlas.piotr.Liszkows' => 'Piotr.Liszkowski',
//        ];


        $userArr = [
            'atlas.michal.baranow' => 'michal.baranowski',
            'atlas.renata.bartz' => 'renata.bartz',
            'atlas.mateusz.brozdo' => 'mateusz.brozdowski',
            'atlas.lukasz.dudek' => 'Lukasz.Dudek',
            'atlas.arkadiusz.jani' => 'Arkadiusz.Janiak',
            'atlas.kamila.bruczyn' => 'kamila.bruczynska', //-
            'atlas.krzysztof.warc' => 'krzysztof_w',
            'atlas.lukasz.rugowsk' => 'Lukasz.Rugowski',
            'atlas.martyna.mydlar' => 'martyna.cichowicz',
            'atlas.michal.budnik' => 'Michal.Budnik',
            'atlas.mikolaj.manick' => 'mikolaj.manicki',
            'atlas.natalia.kazmie' => 'Natalia.Kazmierczak',
            'atlas.adam.dykas' => 'adam.dykas', //-
            'atlas.blanka.skiba' => 'blanka_s',
            'atlas.jan.hylla' => 'Jan.Hylla',
            'atlas.natalia.wolacz' => 'Natalia.Wolaczynska',
            'atlas.norbert.matcza' => 'norbert.matczak',
            'atlas.oliwia.kudlack' => 'oliwia.kudlacka',
            'atlas.paulina.sieber' => 'paulina.siebert',
            'atlas.piotr.kacki' => 'Piotr.Kacki',
            'atlas.piotr.Liszkows' => 'Piotr.Liszkowski',
            'atlas.piotr.baluszyn' => 'piotr.baluszynski',
            'atlas.adrian.kossows' => 'adrian.kossowski',
            'atlas.aldona.kozlows' => 'aldona_s',
            'atlas.jakub.gmiterek' => 'jakub.gmiterek',
            'atlas.jakub.przybyls' => 'jakub.przybylski',
            'atlas.justyna.ettis' => 'justyna.ettis',
            'atlas.kacper.swiercz' => 'kacper.swierczynski',
            'atlas.karolina.skrzy' => 'karolina.skrzypczak',
            'atlas.katarzyna.kopr' => 'katarzyna_ko',
            'atlas.kornelia.wasza' => 'kornelia.waszak',
            'atlas.krzysztof.wrze' => 'krzysztof.wrzesinski',
            'atlas.lia.vorobieva' => 'lija_w',
            'atlas.maciej.melech' => 'maciej.melech',
            'atlas.malgorzata.dud' => 'malgorzata.dudek',
            'atlas.marcin.leoniak' => 'marcin_le',
            'atlas.marta.godawa' => 'marta_g',
            'atlas.mateusz.beczek' => 'mateusz.beczek',
            'atlas.natalia.fluks' => 'natalia.fluks',
            'atlas.pamela.szczesn' => 'pamela.szczesna',
            'atlas.patryk.kolodzi' => 'patryk.kolodziejczak',
            'atlas.paulina.jarmus' => 'paulina.jarmuszka',
            'atlas.paulina.lewick' => 'paulina.lewicka',
            'atlas.piotr.feliksia' => 'piotr.feliksiak',
            'atlas.sylwia.kolibab' => 'sylwia.kolibaba',
            'atlas.agnieszka.wars' => 'agnieszka.warszawska',
            'atlas.alicja.rymkiew' => 'alicja.rymkiewicz',
            'atlas.alina.juskowia' => 'alina.juskowiak',
            'atlas.aneta.nowicka' => 'aneta.nowicka',
            'atlas.arkadiusz.wawr' => 'arkadiusz.wawrzyniak',
            'atlas.filipina.wycis' => 'Filipina.Wycisk',
            'atlas.iwona.robaszyn' => 'iwona.robaszynska',
            'atlas.iwona.zalewska' => 'iwona.zaleska',
            'atlas.jedrzej.strzod' => 'jadrzej.strzodka',
            'atlas.kamil.ochocins' => 'kamil.ochocinski',
            'atlas.lechoslaw.szym' => 'lechoslaw.szymanski',
            'atlas.lukasz.wiacek' => 'Lukasz.Wiacek',
            'atlas.maciej.wojciec' => 'maciej.wojciechowski',
            'atlas.marcin.markows' => 'marcin_m',
            'atlas.marek.pankowsk' => 'marek_p',
            'atlas.milena.dybowsk' => 'milena.dybowska',
            'atlas.rafal.wietrzyn' => 'rafal.wietrzynski',
            'atlas.adam.jaroszyns' => 'Adam.Jaroszynski',
            'atlas.iwona.zaleska' => 'iwona.zaleska',
            'atlas.aleksander.kai' => 'aleksander.kaim',
            'atlas.ewelina.caban' => 'ewelina.caban',
            'atlas.michal.kuchars' => 'michal.kucharski',
            'atlas.anna.massiel' => 'anna.massiel',
            'atlas.patryk.kusz' => 'patryk.kusz'
        ];

        /** Dodatkowo musi być odwrotność tych użytkowników */

        $this->userPair = array_merge($userArr, array_flip($userArr));
    }

    private function getTeam() {

        try {

            $client = $this
                ->clientRequest
                ->setActionUrl(sprintf('Team/%s', self::TEAM))
                ->setOptions([])
                ->execute('get');


            if($client->getStatusCode() === Response::HTTP_OK) {

                $this->crawler->clear();
                $this->xmlResponse = $client->getResponse();
                $this->crawler->addXmlContent($this->xmlResponse);

                if($this->STORE_RESPONSE_XML) {
                    $this->redis->set(self::TELEPHONY_AGENTS, $this->xmlResponse);
                }

                if($this->ONLY_STORE) {
                    $this->output->write(chr(27).chr(91).'H'.chr(27).chr(91).'J');
                    $this->io->write("Saving telephony agent's status...\t\n");
                    return false;
                }

                /** @var \Symfony\Component\DomCrawler\Crawler $users */
                $memberOfTeam = $this->crawler->filter('User');
                $users = [];

                $timeZone = new \DateTimeZone('Europe/Warsaw');

                /** @var DOMElement $member */
                foreach ($memberOfTeam->getIterator() as $key => $member) {

                    $state = $member->getElementsByTagName('state')->item(0)->nodeValue;

                    if($state !== self::LOGOUT || $this->SHOW_NOT_LOGGED) {

                        $loginId = $member->getElementsByTagName('loginId')->item(0)->nodeValue;

                        $users[$loginId] = [
                            'state' => $member->getElementsByTagName('state')->item(0)->nodeValue,
                            'loginId' => $loginId,
                            'codeId' => null,
                            'stateChangeTime' => $member->getElementsByTagName('stateChangeTime')->item(0)->nodeValue,
                        ];

                        if(!empty($users[$loginId]['stateChangeTime'])) {
                            $users[$loginId]['stateChangeTime'] = new \DateTime($users[$loginId]['stateChangeTime']);
                            $users[$loginId]['stateChangeTime']->setTimezone($timeZone);
                        }

                        if($users[$loginId]['state'] == self::NOT_READY) {

                            if($member->getElementsByTagName('reasonCode')->length) {
                                $users[$loginId]['codeId'] = intval($member->getElementsByTagName('reasonCode')->item(0)->getElementsByTagName('code')->item(0)->nodeValue);
                            }
                        }

                        if(self::LOG_USER_STR !== NULL) {
                            $this->logUser($users[$loginId]);
                        }

                    }

                }

                if($this->ONLY_TEAM) {
                    $this->displayTeam($users);
                }

                if(self::LOG_USER_STR !== NULL) {
                    die();
                }

                return $users;
            }
            else {
                echo "Błąd pobrania drużyny\t\n";
                return false;
            }

        }
        catch (\Exception $exception) {
            return false;
        }
        catch (\Throwable $throwable) {
            return false;
        }

    }

    private function logUser($user) {

        if (strpos($user['loginId'], self::LOG_USER_STR) !== false) {

            $line = '<comment>' . $user['loginId'] . str_repeat(" ", (30 - strlen($user['loginId'])))  . '</comment> | ';
            $line .= ($user['state'] !== self::NOT_READY) ? "<info>".$user['state']."</info>" : '<fg=red;bg=black>'.$user['state'].'</>';
            $line .= ' | ' . str_repeat(" ", (15 - strlen($user['state']))) . ' | ';

            if($user['stateChangeTime']){
                $line .= $user['stateChangeTime']->format('Y-m-d H:i:s');
            }

            $this->output->writeln($line);
        }

    }

    private function displayTeam($users) {

        $i = 1;

        $table = new Table($this->output);
        $style = new TableStyle();
        $table->setStyle($style);
        $table->setHeaders(array('Lp. (' . count($users) . ')', 'User', 'Status', 'Last Active'));

        usort($users, function($a, $b) {
            if ($a['stateChangeTime'] == $b['stateChangeTime']) {
                return 0;
            }
            return ($a['stateChangeTime'] < $b['stateChangeTime']) ? -1 : 1;
        });

        $limitTime = new \DateTime();
        $limitTime->setTimezone(new \DateTimeZone('Europe/Warsaw'));

        foreach ($users as $key => $item) {

            $table->addRow(array(
                $i,
                $this->colorUser($item['loginId']),
                $this->colorStyle($item['state']),
                ($item['stateChangeTime']) ? $item['stateChangeTime']->format('Y-m-d H:i:s') : '-',
            ));

            $i++;

        }

        $table->render();

        die();

    }

    private function setStatus($login, $status) {

        $templateBody = $this->getStateTemplate($status);

        $client = $this->clientRequest
            ->setActionUrl(sprintf('User/%s', $login))
            ->setOptions([
                'body' => $templateBody,
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('put');

        return ($client->getStatusCode() === Response::HTTP_ACCEPTED);

    }

    private function logoutAction($login) {

        $templateBody = $this->getStateTemplate(self::LOGOUT);

        $client = $this->clientRequest
            ->setActionUrl(sprintf('User/%s', $login))
            ->setOptions([
                'body' => $templateBody,
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('put');

        return ($client->getStatusCode() === Response::HTTP_ACCEPTED);

    }

    private function loginMachine() {

        $client = $this
            ->clientRequest
            ->setActionUrl(sprintf('User/%s', self::LOGIN))
            ->setOptions([
                'body' => $this->getRequestTemplate(),
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('put');

        echo "Login: " . (($client->getStatusCode() == Response::HTTP_ACCEPTED) ? "Ok" : "Fail") . "\t\n";

        return ($client->getStatusCode() == Response::HTTP_ACCEPTED);
    }

    private function getRequestTemplate() {

        $template = $this->twig->load('authentication/login.xml.twig');

        return $template->render([
            'cucumExtension' => self::EXT
        ]);

    }

    private function getStateTemplate($state) {
        if($state === self::READY) {
            return $this->twig->load('profile/status/ready.xml.twig')->render();
        }
        else if($state === self::LOGOUT) {
            return $this->twig->load('authentication/logout.xml.twig')->render();
        }
        else {
            if($this->idCode !== null) {
                return $this->twig->load('profile/status/not_ready.xml.twig')->render([
                    'idCode' => $this->idCode
                ]);
            }
            else {
                return $this->twig->load('profile/status/not_ready.xml.twig')->render();
            }
        }
    }

    private function reloadUserPair() {
        //TODO - pobieranie par uzytkownikow z bazy
    }
}


class AtlasConfig {
    private $parameters;

    public function __construct()
    {
        $this->loadYaml();
    }

    private function loadYaml() {
        $data = Yaml::parse(file_get_contents(__DIR__ . '/../../app/config/parameters.yml'));
        if(is_array($data) && isset($data['parameters'])) $this->parameters = $data['parameters'];
    }

    public function getParameter($name) {

        if(isset($this->parameters[$name])) {
            return $this->parameters[$name];
        }

        return NULL;
    }
}

//        <User>
//      <dialogs>/finesse/api/User/Krzysztof.Rzepecki/Dialogs</dialogs>
//      <extension>628</extension>
//      <firstName>Krzysztof</firstName>
//      <lastName>Rzepecki</lastName>
//      <loginId>Krzysztof.Rzepecki</loginId>
//      <pendingState></pendingState>
//      <reasonCode>
//        <category>NOT_READY</category>
//        <code>5</code>
//        <forAll>true</forAll>
//        <id>5</id>
//        <label>Praca w Aplikacji</label>
//        <uri>/finesse/api/ReasonCode/5</uri>
//      </reasonCode>
//      <state>NOT_READY</state>
//      <stateChangeTime>2017-08-18T09:49:31.252Z</stateChangeTime>
//      <uri>/finesse/api/User/Krzysztof.Rzepecki</uri>
//    </User>