<?php

namespace SparxBundle\Repository;

use CaseBundle\Entity\FormControl;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Metadata\ClassMetadata;

class AttributesRepository extends NestedTreeRepository
{

    public function fetchSparxSelectedAttributes($qbOnly = false)
    {
        $qb = $this->createQueryBuilder('s')
            ->where('s.selected = 1');

        if ($qbOnly) {
            return $qb;
        }

        return $qb->getQuery()->getResult();
    }
}
