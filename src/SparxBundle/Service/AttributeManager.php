<?php

namespace SparxBundle\Service;


use CaseBundle\Service\ProcessHandler;
use Doctrine\ORM\EntityManagerInterface;
use SparxBundle\Entity\Attribute;
use SparxBundle\Entity\ServiceStatus;

class AttributeManager
{
    private $em;
    private $processHandler;

    public function __construct(EntityManagerInterface $em, ProcessHandler $processHandler)
    {
        $this->em = $em;
        $this->processHandler = $processHandler;
    }

    public function isWritable(Attribute $attribute)
    {
        return empty($this->em->getRepository(Attribute::class)->findBy(['parent' => $attribute->getId()]));
    }

    public function getAttribute($path, $serviceName, $groupProcessInstanceId)
    {
        $serviceStatuses = $this->em->getRepository(ServiceStatus::class)->findBy([
            'attributePath' => $path,
            'service' => $serviceName
        ]);

        $attrValue = $this->processHandler->getAttributeValue(
            $path,
            $groupProcessInstanceId
        );
        unset($attrValue[0]['id']);

        foreach ($serviceStatuses as $serviceStatus) {
            if ($serviceStatus->getValue() == $attrValue[0]) {
                return $attrValue;
            }
        }

        return false;
    }
}