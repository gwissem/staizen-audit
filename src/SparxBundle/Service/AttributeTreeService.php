<?php

namespace SparxBundle\Service;


use CaseBundle\Entity\AttributeValue;
use CaseBundle\Service\AttributeParserService;
use Doctrine\ORM\EntityManagerInterface;

class AttributeTreeService
{
    private $attributeParser;
    private $updatedDate;
    private $em;
    private $servicesNames = [];

    public function __construct(AttributeParserService $attributeParser, EntityManagerInterface $em)
    {
        $this->attributeParser = $attributeParser;
        $this->em = $em;

        $this->updatedDate = new \DateTime();
        $this->initServicesNames();
    }

    private function initServicesNames()
    {
        $this->servicesNames = [
            "SecondaryBenefitsService",
            "AccommodationService",
            "CallCenterService",
            "HopService",
            "OffRoadRecoveryService",
            "VehicleReplacementService",
            "RoadsideAssistanceService",
            "TelematicsService",
            "TravelService",
            "VehicleStorageService",
            "VehicleTransportationService",
        ];
    }

    public function transformTree($branch, &$newTree, $groupProcessId = null, $step, $skip = true, $parentName = null)
    {
        if ($groupProcessId) {
            $this->attributeParser->setGroupProcessInstanceId($groupProcessId);
        }

        $attributes = $step->getSparxAttributes();
        $attributesIds = [];
        foreach ($attributes as $attribute) {
            $attributesIds[] = $attribute->getId();
        }

        for ($i = 0; $i < count($branch); $i++) {
            if ($branch[$i]['title'] == 'Id' && $branch[$i]['lvl'] == 1) {
                continue;
            }

            if ($branch[$i]['title'] != 'Case' && $branch[$i]['title'] != 'Services') {
                if (in_array($branch[$i]['id'], $attributesIds) || !$skip) {
                    $skip = false;
                } else {
                    continue;
                }
            }

            if (!empty($branch[$i]['__children'])) {
                if ($branch[$i]['isArray'] || $branch[$i]['title'] == 'Services') {
                    $this->transformTree($branch[$i]['__children'], $newTree[$branch[$i]['title']][0], $groupProcessId, $step, $skip, $branch[$i]['title']);
                    if (empty($newTree[$branch[$i]['title']]) || $newTree[$branch[$i]['title']] == [[]]) {
                        unset($newTree[$branch[$i]['title']]);
                    }
                } else {
                    if (in_array($branch[$i]['title'], $this->servicesNames)) {
                        $this->transformTree($branch[$i]['__children'], $newTree, $groupProcessId, $step, $skip, $branch[$i]['title']);
                        if (empty($newTree[$branch[$i]['title']])) {
                            unset($newTree[$branch[$i]['title']]);
                        }
                    } else {
                        $this->transformTree($branch[$i]['__children'], $newTree[$branch[$i]['title']], $groupProcessId, $step, $skip, $branch[$i]['title']);
                        if (empty($newTree[$branch[$i]['title']])) {
                            unset($newTree[$branch[$i]['title']]);
                        }
                    }
                }
            } elseif ($branch[$i]['title'] == '$type' && !in_array($parentName, $this->servicesNames)) {
                if (empty($branch[$i]['attributePath'])) {
                    break;
                }
                $newTree[$branch[$i]['title']] = $this->attributeParser->parse($branch[$i]['attributePath']);
                if ($branch[$i + 1]['title'] == $newTree[$branch[$i]['title']]) {
                    $this->transformTree($branch[$i + 1]['__children'], $newTree, $groupProcessId, $step, $skip, $branch[$i + 1]['title']);
                } elseif ($branch[$i + 2]['title'] == $newTree[$branch[$i]['title']]) {
                    $this->transformTree($branch[$i + 2]['__children'], $newTree, $groupProcessId, $step, $skip, $branch[$i + 2]['title']);
                }

                break;

            } elseif ($branch[$i]['title'] == '$type' && in_array($parentName, $this->servicesNames)) {
                $newTree[$branch[$i]['title']] = $parentName;
            } else {
                if ($branch[$i]['attributePath'] && $groupProcessId) {
                    if ($this->isUpdated($groupProcessId, $branch[$i]['attributePath'])) {
                        $this->attributeParser->setGroupProcessInstanceId($groupProcessId);
                        $newTree[$branch[$i]['title']] = $this->attributeParser->parse($branch[$i]['attributePath']);
                        $this->updateSparxUpdatedAt($groupProcessId, $branch[$i]['attributePath']);

                        if ($this->validateDate($newTree[$branch[$i]['title']], 'd-m-Y H:i:s')) {
                            $newTree[$branch[$i]['title']] = str_replace('+00:00', 'Z', gmdate('c', strtotime($newTree[$branch[$i]['title']])));
                        }
                    }
                }
            }
        }
    }

    private function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function transformUpdatedTree($branch, &$newTree, $groupProcessId = null, $step, $skip = true, $parentName = null)
    {
        if ($groupProcessId) {
            $this->attributeParser->setGroupProcessInstanceId($groupProcessId);
        }

        $attributes = $step->getSparxAttributes();
        $attributesIds = [];
        foreach ($attributes as $attribute) {
            $attributesIds[] = $attribute->getId();
        }

        for ($i = 0; $i < count($branch); $i++) {
            if ($branch[$i]['title'] != 'Case' && ($branch[$i]['title'] != 'Id' || $branch[$i]['lvl'] != 1) && $branch[$i]['title'] != 'Services') {
                if (in_array($branch[$i]['id'], $attributesIds) || !$skip) {
                    $skip = false;
                } else {
                    continue;
                }
            }

            if (!empty($branch[$i]['__children'])) {
                if ($branch[$i]['isArray'] || $branch[$i]['title'] == 'Services') {
                    $this->transformUpdatedTree(
                        $branch[$i]['__children'],
                        $newTree[$branch[$i]['title']][0],
                        $groupProcessId,
                        $step,
                        $skip,
                        $branch[$i]['title']
                    );
                    if (empty($newTree[$branch[$i]['title']]) || $newTree[$branch[$i]['title']] == [[]]) {
                        unset($newTree[$branch[$i]['title']]);
                    }
                } else {
                    if (in_array($branch[$i]['title'], $this->servicesNames)) {
                        $this->transformUpdatedTree(
                            $branch[$i]['__children'],
                            $newTree,
                            $groupProcessId,
                            $step,
                            $skip,
                            $branch[$i]['title']
                        );
                        if (empty($newTree[$branch[$i]['title']])) {
                            unset($newTree[$branch[$i]['title']]);
                        }
                    } else {
                        $this->transformUpdatedTree(
                            $branch[$i]['__children'],
                            $newTree[$branch[$i]['title']],
                            $groupProcessId,
                            $step,
                            $skip,
                            $branch[$i]['title']
                        );
                        if (empty($newTree[$branch[$i]['title']])) {
                            unset($newTree[$branch[$i]['title']]);
                        }
                    }
                }
            } elseif ($branch[$i]['title'] == '$type' && !in_array($parentName, $this->servicesNames)) {
                if (empty($branch[$i]['attributePath'])) {
                    break;
                }
                $newTree[$branch[$i]['title']] = $this->attributeParser->parse($branch[$i]['attributePath']);
                if ($branch[$i + 1]['title'] == $newTree[$branch[$i]['title']]) {
                    $this->transformUpdatedTree($branch[$i + 1]['__children'], $newTree, $groupProcessId, $step, $skip, $branch[$i + 1]['title']);
                } elseif ($branch[$i + 2]['title'] == $newTree[$branch[$i]['title']]) {
                    $this->transformUpdatedTree($branch[$i + 2]['__children'], $newTree, $groupProcessId, $step, $skip, $branch[$i + 2]['title']);
                }

                break;

            } elseif ($branch[$i]['title'] == '$type' && in_array($parentName, $this->servicesNames)) {
                $newTree[$branch[$i]['title']] = $parentName;
            } else {
                if ($branch[$i]['attributePath'] && $groupProcessId) {
                    if ($this->isUpdated($groupProcessId, $branch[$i]['attributePath'])) {
                        $this->attributeParser->setGroupProcessInstanceId($groupProcessId);
                        $newTree[$branch[$i]['title']] = $this->attributeParser->parse($branch[$i]['attributePath']);
                        $this->updateSparxUpdatedAt($groupProcessId, $branch[$i]['attributePath']);

                        if ($this->validateDate($newTree[$branch[$i]['title']], 'd-m-Y H:i:s')) {
                            $newTree[$branch[$i]['title']] = str_replace('+00:00', 'Z', gmdate('c', strtotime($newTree[$branch[$i]['title']])));
                        }
                    }
                }
            }
        }
    }

    private function updateSparxUpdatedAt($groupProcessId, $path)
    {
        $path = trim(trim($path, '{@'), '@}');
        $this->em->getRepository(AttributeValue::class)->updateSparxUpdatedAt($groupProcessId, $path, new \DateTime());//ostatni now
    }

    private function isUpdated($groupProcessId, $path)
    {
        $attribute = $this->em->getRepository(AttributeValue::class)->findBy([
            'groupProcessInstance' => $groupProcessId,
            'attributePath' => trim(trim($path, '{@'), '@}')
        ]);

        if (empty($attribute)) {
            return false;
        }

        if (!empty($attribute[0]->getUpdatedAt()) && !empty($attribute[0]->getSparxUpdatedAt())) {
            if ($attribute[0]->getSparxUpdatedAt() < $attribute[0]->getUpdatedAt()) {
                return true;
            }
        } else {
            return true;
        }

        return false;
    }
}