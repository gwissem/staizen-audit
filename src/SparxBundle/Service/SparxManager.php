<?php

namespace SparxBundle\Service;


use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Service\ProcessHandler;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use SparxBundle\Entity\Attribute as SparxAttribute;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SparxManager
{
    private $em;
    private $attributeTree;
    private $processHandler;
    private $container;
    private $auth;

    public function __construct(
        EntityManagerInterface $em,
        AttributeTreeService $attributeTree,
        ProcessHandler $processHandler,
        ContainerInterface $container
    ) {
        $this->em = $em;
        $this->attributeTree = $attributeTree;
        $this->processHandler = $processHandler;
        $this->container = $container;
//        $this->checkConncection();
    }

    public function create($groupProcessId, $step)
    {
        $arrayTree = $this->em->getRepository(SparxAttribute::class)->childrenHierarchy();

        $newTree = [];
        $this->attributeTree->transformTree($arrayTree, $newTree, $groupProcessId, $step);

        $options = [];
        $options['body'] = $newTree ? json_encode($newTree['Case']) : '{}';
        $options['headers'] = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => $this->auth
        ];

        $info = $this->send('cases', 'POST', $options);
        $sparxId = $this->em->getRepository(Attribute::class)->fetchAttributesByName('Sparx Id');

        $this->processHandler->setAttributeValue(
            $sparxId[0]['id'],
            $info->CaseId,
            AttributeValue::VALUE_STRING,
            $step->getId(),
            $groupProcessId
        );
        return $info->Href;
    }



    public function toUpdate($groupProcessId)
    {
        $sparxId = $this->em->getRepository(Attribute::class)->fetchAttributesByName('Sparx Id');
        $attributeId = $this->processHandler->getAttributeValue(
            $sparxId[0]['id'],
            $groupProcessId
        );

        return (bool)!empty($attributeId[0]['value_string']);
    }

    public function update($groupProcessId, $step)
    {
        $arrayTree = $this->em->getRepository(SparxAttribute::class)->childrenHierarchy();

        $newTree = [];
        $this->attributeTree->transformUpdatedTree($arrayTree, $newTree, $groupProcessId, $step);

        $options = [];
        $options['body'] = $newTree ? json_encode($newTree['Case']) : '{}';
        $options['headers'] = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => $this->auth
        ];

        $info = $this->send('cases', 'POST', $options);
        return $info->Href;
    }

    public function cancel($caseId)
    {
        $options['headers'] = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'X-Sparx-Async' => false,
            'Authorization' => $this->auth
        ];

        $options = [];
        $options['body'] = '{}';

        return $this->send('cases/' . $caseId, 'DELETE', $options);
    }

    public function getSingle($caseId = null)
    {
        $options['headers'] = [
            'Accept' => 'application/json',
            'X-Sparx-Ignore-Null-Values' => true,
            'Authorization' => $this->auth
        ];

        return $this->send($caseId ? 'cases' . '/' . $caseId : 'cases', 'GET', $options);
    }

    public function send($uri, $method, $options)
    {
        $client = new Client([
            'base_uri' => self::URL,
            'timeout'  => 20.0,
        ]);

//        try {
            $response = $client->request($method, $uri, $options);
            $info = json_decode($response->getBody()->getContents());
            return $info;

//            if (!$response->hasHeader('Content-Length')) {
//                //log
//
//            }
//        } catch(\Exception $e) {
//            //log
//        }
    }

    public function checkConncection()
    {
        try {
            $this->getSingle();
        } catch (\Exception $e) {
            $this->setAutorization();
        }
    }

    private function setAutorization()
    {
        $json = json_decode($this->oauthlogin());

        $this->auth = implode(' ', [$json->token_type, $json->access_token]);
    }

    private function setOAuthParams()
    {
        return [
            "client_id" => $this->container->getParameter('sparx_oauth_client_id'),
            "client_secret" => $this->container->getParameter('sparx_oauth_client_secret'),
            'scope' => $this->container->getParameter('sparx_oauth_scope'),
            "grant_type" => $this->container->getParameter('sparx_oauth_grant_type')
        ];
    }

    public function getAuthResponse() {
        return $this->oauthlogin();
    }

    private function oauthlogin()
    {
        $endpoint = $this->container->getParameter('sparx_endpoint');
        $params = $this->setOAuthParams();

        $curl = curl_init($endpoint);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HEADER,'Content-Type: application/x-www-form-urlencoded');

        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $postData = "";

        foreach($params as $k => $v)
        {
            $postData .= $k . '='.urlencode($v).'&';
        }

        $postData = rtrim($postData, '&');

        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 200) {
            throw new \Exception("Error: call to URL $endpoint failed with status $status, response $json_response, curl_error "
                . curl_error($curl) . ", curl_errno " . curl_errno($curl) . "\n");
        }
        curl_close($curl);

        return $json_response;
    }
}