<?php

namespace SparxBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManagerInterface;
use SparxBundle\Entity\Attribute as SparxAttibute;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SparxAttributeType extends AbstractType
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $attributeRoot = $this->em->getRepository(SparxAttibute::class)->find($options['rootBranch']);
        $attributeTree = $this->em->getRepository(SparxAttibute::class)->childrenHierarchy($attributeRoot, false, [], true);

        $this->handleNode($builder, $attributeTree);
    }

    private function handleNode(FormBuilderInterface &$builder, $tree)
    {
        foreach ($tree as $node) {
            if (!empty($node['__children'])) {
                $builder
                    ->add($node['id'], null, [
                        'required' => false,
                        'label' => $node['title'],
                        'disabled' => 'disabled',
                        'label_attr' => [
                            'data-margin-lvl' => $node['lvl'],
                            'class' => 'sparx-label'
                        ],
                        'attr' => [
                            'class' => 'sparx-input parent-attr',
                            'data-attribute-id' => $node['id']
                        ],
                        'data' => $node['attributePath']
                    ])
                ;
                $this->handleNode($builder, $node['__children']);
            } else {
                $builder
                    ->add($node['id'], null, [
                        'required' => false,
                        'label' => $node['title'],
                        'label_attr' => [
                            'data-margin-lvl'=> $node['lvl'],
                        ],
                        'attr' => [
                            'class' => 'sparx-input',
                            'data-attribute-id' => $node['id']
                        ],
                        'data' => $node['attributePath']
                    ])
                ;
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'rootBranch' => null,
        ));
    }
}