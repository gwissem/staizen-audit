<?php

namespace SparxBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManagerInterface;
use SparxBundle\Entity\Attribute as SparxAttibute;

class SparxMainAttributesType extends AbstractType
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $attributes = $this->em->getRepository(SparxAttibute::class)->findBy(['selected' => 1]);

        $this->createSelect($builder, $attributes);
    }

    private function createSelect(FormBuilderInterface &$builder, $attributes)
    {
        $builder->add('mainAttribute', ChoiceType::class, [
            'choices' => $this->prepareChoices($attributes),
        ]);
    }

    private function prepareChoices($attributes)
    {
        $result = [];
        foreach ($attributes as $attribute) {
            $result[$attribute->getTitle()] = $attribute->getId();
        }

        return $result;
    }
}