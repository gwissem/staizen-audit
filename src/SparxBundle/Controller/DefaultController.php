<?php

namespace SparxBundle\Controller;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\FormControl;
use SparxBundle\Entity\Attribute as SparxAttribute;
use SparxBundle\Form\Type\SparxAttributeType;
use SparxBundle\Form\Type\SparxMainAttributesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("mapping", name="sparx_mapping")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function mappingForm(Request $request)
    {
        $form = $this->createForm(SparxMainAttributesType::class);

        return $this->render(
            'SparxBundle:Default:mappingForm.html.twig', ['form' => $form->createView()]
        );
    }

    /**
     * @Route("mapping-part-form", name="sparx_mapping_part")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function mappingPartForm(Request $request)
    {
        $rootBranch = $request->request->get('rootBranch');
        $form = $this->createForm(SparxAttributeType::class, [], [
            'rootBranch' => $rootBranch
        ]);

        return $this->render(
            'SparxBundle:Default:mappingPartForm.html.twig', ['form' => $form->createView()]
        );
    }

    /**
     * @Route(
     *     "save-mapping",
     *     name="save_mapping",
     *     requirements={
     *         "formControlId": "\d+"
     *     },
     *     options={"expose":true}
     * )
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function saveMapping(Request $request)
    {
        $path = $request->request->get('attributePath');
        $id = $request->request->get('sparxAttributeId');

        if (!$this->get('case.attribute_parser.service')->isSchemaFormat($path, true) || !is_numeric($id)) {
            return new JsonResponse(['errors' => 2, 'desc' => 'Wrong parameters']);
        }

        $attribute = $this->getDoctrine()->getRepository(SparxAttribute::class)->findById((int)$id);

        if (!empty($attribute) && $this->get('sparx.attribute_manager')->isWritable($attribute[0])) {
            $attribute[0]->setAttributePath($path);
            $em = $this->getDoctrine()->getManager();
            $em->persist($attribute[0]);
            $em->flush();

            return new JsonResponse(['errors' => 0, 'desc' => 'Attribute saved', 'id' => $attribute[0]->getId()]);
        } else {
            return new JsonResponse(['errors' => 1, 'desc' => 'Wrong attribute']);
        }
    }

    /**
     * @Route(
     *     "/setAttr/{formControlId}",
     *     name="sparx_set_attr",
     *     requirements={
     *         "formControlId": "\d+"
     *     },
     *     options={"expose":true}
     * )
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @param Request $request
     * @param int $formControlId
     * @return JsonResponse|Response
     */
    public function sparxAction(Request $request, $formControlId)
    {
        $form = $this->createForm(SparxAttributeType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $sparxAttribute = $em->getRepository(SparxAttribute::class)->find(
                $form->getData('sparx_attribute')['sparxAttribute']
            );
            if ($sparxAttribute === null) {

            }

            $formControl = $em->getRepository(FormControl::class)->find($formControlId);
            if ($formControl === null) {
                return new JsonResponse(['errors' => 1, 'desc' => 'Wrong form control']);
            }

            $sparxAttribute->getAttributes()->add($formControl->getAttribute());

            $em->persist($sparxAttribute);
            $em->flush();

            $errors = $form->getErrors();
            return new JsonResponse(['errors' => $errors, 'id' => $sparxAttribute->getId()]);
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(
                [
                    'form' => $this->renderView('SparxBundle:Default:sparx.html.twig',
                    ['form' => $form->createView(), 'formControlId' => $formControlId])
                ]
            );
        } else {
            return $this->render('SparxBundle:Default:sparx.html.twig',
                ['form' => $form->createView(), 'formControlId' => $formControlId]
            );
        }
    }
}
