<?php

namespace SparxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="SparxBundle\Repository\SparxIncomingRepository")
 * @ORM\Table(name="sparx_incoming")
 */

class SparxIncoming
{

    use ORMBehaviors\Timestampable\Timestampable;
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $json;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $xml;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $groupProcessId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $eventType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $sparxId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * @param mixed $json
     */
    public function setJson($json)
    {
        $this->json = $json;
    }

    /**
     * @return mixed
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * @param mixed $xml
     */
    public function setXml($xml)
    {
        $this->xml = $xml;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getGroupProcessId()
    {
        return $this->groupProcessId;
    }

    /**
     * @param mixed $groupProcessId
     */
    public function setGroupProcessId($groupProcessId)
    {
        $this->groupProcessId = $groupProcessId;
    }

    /**
     * @return mixed
     */
    public function getSparxId()
    {
        return $this->sparxId;
    }

    /**
     * @param mixed $sparxId
     */
    public function setSparxId($sparxId)
    {
        $this->sparxId = $sparxId;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param mixed $eventType
     * @return SparxIncoming
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
        return $this;
    }


}