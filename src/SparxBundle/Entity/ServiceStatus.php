<?php

namespace SparxBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="SparxBundle\Repository\ServiceStatusRepository")
 * @ORM\Table(name="sparx_service_status")
 */

class ServiceStatus
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $step;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $attributePath;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $valueInt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $valueString;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $valueText;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=6, nullable=true)
     */
    protected $valueDecimal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $valueDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $service;

    /**
     * @ORM\ManyToOne(targetEntity="SparxBundle\Entity\Status")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     */
    public function setStep($step)
    {
        $this->step = $step;
    }

    /**
     * @return mixed
     */
    public function getAttributePath()
    {
        return $this->attributePath;
    }

    /**
     * @param mixed $attributePath
     */
    public function setAttributePath($attributePath)
    {
        $this->attributePath = $attributePath;
    }

    /**
     * @return mixed
     */
    public function getValueInt()
    {
        return $this->valueInt;
    }

    /**
     * @param mixed $valueInt
     */
    public function setValueInt($valueInt)
    {
        $this->valueInt = $valueInt;
    }

    /**
     * @return mixed
     */
    public function getValueString()
    {
        return $this->valueString;
    }

    /**
     * @param mixed $valueString
     */
    public function setValueString($valueString)
    {
        $this->valueString = $valueString;
    }

    /**
     * @return mixed
     */
    public function getValueText()
    {
        return $this->valueText;
    }

    /**
     * @param mixed $valueText
     */
    public function setValueText($valueText)
    {
        $this->valueText = $valueText;
    }

    /**
     * @return mixed
     */
    public function getValueDecimal()
    {
        return $this->valueDecimal;
    }

    /**
     * @param mixed $valueDecimal
     */
    public function setValueDecimal($valueDecimal)
    {
        $this->valueDecimal = $valueDecimal;
    }

    /**
     * @return mixed
     */
    public function getValueDate()
    {
        return $this->valueDate;
    }

    /**
     * @param mixed $valueDate
     */
    public function setValueDate($valueDate)
    {
        $this->valueDate = $valueDate;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getValue()
    {
        return [
            "value_int" => $this->getValueInt(),
            "value_string" => $this->getValueString(),
            "value_text" => $this->getValueText(),
            "value_date" => $this->getValueDate(),
            "value_decimal" => $this->getValueDecimal()
        ];
    }
}