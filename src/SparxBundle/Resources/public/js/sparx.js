function Sparx() {
    this.marginLength = 40;

    this.init = function() {
        this.handleSelect();
    };

    this.handleSelect = function () {
        var that = this;
        $('#sparx_main_attributes_mainAttribute').change(function() {
            $('#sparx_main_attributes_mainAttribute').attr('disabled','disabled');
            $.ajax({
                url: 'mapping-part-form',
                method: 'POST',
                data: {
                    rootBranch: $(this).val()
                },
                error: function () {
                    toastr.error("", 'Connection problem', {timeOut: 5000});
                    $('#sparx_main_attributes_mainAttribute').removeAttr('disabled');
                },
                success: function (data) {
                    $('#attributesBranch').html('');
                    toastr.success("", 'Attributes loaded', {timeOut: 5000});
                    $('#attributesBranch').html(data);
                    that.setNestings();
                    that.inputListener();
                    $('#sparx_main_attributes_mainAttribute').removeAttr('disabled');
                }});
        });
    };

    this.setNestings = function() {
        var formGroup = $('#sparx_attribute .form-group');
        formGroup.each(function() {
            var $t = $(this),
                nestedLevel = $t.find('label').attr('data-margin-lvl'),
                nextElement = $t.next().find('label').attr('data-margin-lvl');
            if ( parseInt(nextElement) > parseInt(nestedLevel) ) {
                $t.addClass('parent-attribute');
            }
            $t.addClass('level-'+nestedLevel+'');
        });
        /*   for (var i = 0; i < $('#sparx_attribute .form-group').length; i++) {
         var $t = $(this),
         nestedLevel = $t.find('label').attr('data-margin-lvl');
         $t.addClass('level-"'+nestedLevel+'"');
         /!*  $($('#sparx_attribute label')[i]).css(
         'margin-left',
         ($($('#sparx_attribute label')[i]).data('margin-lvl') * this.marginLength) + 'px'
         );*!/
         console.log($t);
         }*/
    };

    this.inputListener = function() {
        $('#sparx_attribute input').on('focusout', function() {
            if (!$(this).val()) {
                resetInput(this);
            } else {
                var input = this;
                $.ajax({
                    url: 'save-mapping',
                    method: 'POST',
                    data: {
                        attributePath: $(this).val(),
                        sparxAttributeId: $(this).data('attribute-id')
                    },
                    dataType: 'json',
                    error: function () {
                        errorInput(input, 'Connecting error');
                    },
                    success: function (data) {
                        if (data.errors == 0) {
                            successInput(input, data.desc);
                            $(input).parents('div.form-group').addClass('has-success');
                        } else {
                            errorInput(input, data.desc);
                        }
                    }
                });
            }
        });
    };

    var successInput = function(input, desc) {
        resetInput(input);
        $(input).parents('div.form-group').addClass('has-success');
        toastr.success("", desc, {timeOut: 5000});
    };

    var errorInput = function (input, desc) {
        resetInput(input);
        $(input).parents('div.form-group').addClass('has-error');
        toastr.error("", desc, {timeOut: 5000});
    };

    var resetInput = function(input) {
        if ($(input).parents('div.form-group').hasClass('has-success')) {
            $(input).parents('div.form-group').removeClass('has-success');
        }

        if ($(input).parents('div.form-group').hasClass('has-error')) {
            $(input).parents('div.form-group').removeClass('has-error');
        }
    };



    this.init();
}

$(function() {
    var sparx = new Sparx();
});