<?php

namespace SparxBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SparxBundle\Entity\Status as SparxStatus;

class AuthCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('sparx:auth')
            ->setDescription('This command is to create auth token.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Creating auth.');

        $this->getContainer()->get('twig')->getBaseTemplateClass();

        $sparxManager = $this->getContainer()->get('sparx.sparx_manager')->getAuthResponse();

        $sparxManager = json_decode($sparxManager);

        $output->writeln('access_token:');
        $output->writeln('');
        $output->writeln($sparxManager->access_token);
        $output->writeln('');

        $output->writeln('Command finished.');
    }

}
