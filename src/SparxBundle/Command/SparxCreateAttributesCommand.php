<?php

namespace SparxBundle\Command;

use Doctrine\ORM\EntityManager;
use SparxBundle\Entity\Attribute;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SparxCreateAttributesCommand extends ContainerAwareCommand
{
    private $em;
    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('sparx:create-attributes')
            ->setDescription('Command to insert sparx attributes')
            ->addArgument('argument', InputArgument::OPTIONAL, 'No argument')
            ->addOption('option', null, InputOption::VALUE_NONE, 'No option')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        $this->addAttributes();

        $output->writeln('Command successfully.');
    }

    private function addAttributes()
    {
        $service = [
            "\$type",
            "Id",
            "ExternalReferences" => [
                "Reference",
                "Source"
            ],
            "CreationTimestamp",
            "LastUpdateTimestamp",
            "OperationalProcedureIdentifier",
            "ServiceExecutor" => [
                "\$type",
                "Person" => [
                    "ContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "FirstName",
                    "Gender",
                    "HomeAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "LastName",
                    "CommunicationLanguages",
                    "Id",
                    "ExternalReferences" => [
                        "Reference",
                        "Source"
                    ],
                ],
                "Company" => [
                    "Code",
                    "CodingScheme",
                    "LegalName",
                    "OperationalAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "OperationalContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "RegisteredOfficeAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "RegisteredOfficeContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "TradeName",
                    "VatNumber",
                    "Id",
                    "ExternalReferences" => [
                        "Reference",
                        "Source"
                    ],
                ],
            ],
            "ServiceProvider" => [
                "\$type",
                "Person" => [
                    "ContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "FirstName",
                    "Gender",
                    "HomeAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "LastName",
                    "CommunicationLanguages",
                    "Id",
                    "ExternalReferences" => [
                        "Reference",
                        "Source"
                    ],
                ],
                "Company" => [
                    "Code",
                    "CodingScheme",
                    "LegalName",
                    "OperationalAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "OperationalContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "RegisteredOfficeAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "RegisteredOfficeContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "TradeName",
                    "VatNumber",
                    "Id",
                    "ExternalReferences" => [
                        "Reference",
                        "Source"
                    ],
                ],
            ],
            "ServiceRequestor" => [
                "\$type",
                "Person" => [
                    "ContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "FirstName",
                    "Gender",
                    "HomeAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "LastName",
                    "CommunicationLanguages",
                    "Id",
                    "ExternalReferences" => [
                        "Reference",
                        "Source"
                    ],
                ],
                "Company" => [
                    "Code",
                    "CodingScheme",
                    "LegalName",
                    "OperationalAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "OperationalContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "RegisteredOfficeAddress" => [
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                        "LocationType",
                        "Address" => [
                            "AdministrativeAreaLevel1",
                            "AdministrativeAreaLevel2",
                            "City",
                            "Country",
                            "FormattedAddress",
                            "HouseNumber",
                            "PostalCode",
                            "RoadType",
                            "StreetName"
                        ],
                        "Comment",
                        "GeoPosition" => [
                            "Latitude",
                            "Longitude"
                        ],
                    ],
                    "RegisteredOfficeContactDetails" => [
                        "DaytimeNumber",
                        "EmailAddress",
                        "FaxNumber",
                        "MobileNumber",
                        "NighttimeNumber"
                    ],
                    "TradeName",
                    "VatNumber",
                    "Id",
                    "ExternalReferences" => [
                        "Reference",
                        "Source"
                    ],
                ],
            ],
            "Status",
            "StatusReason" => [
                "Code",
                "Comment"
            ],
            "IncurredCosts" => [
                "Comment",
                "CostCode",
                "Currency",
                "GrossAmount",
                "NetAmount",
                "PricingModel",
                "TaxationType",
                "Unit",
                "UnitQuantity",
                "UnitPrice",
                "VatAmount",
                "VatRate"
            ],
            "PerformedActions" => [
                "Code",
                "CreationTimestamp",
                "StartTimestamp",
                "Status",
                "StatusReason" => [
                    "Code",
                    "Comment"
                ],
                "StopTimestamp"
            ],
            "MeasuredPerformances" => [
                "Code",
                "Value",
                "Timestamp"
            ],
            "ApprovalRequests" => [
                "Code",
                "Approved",
                "Channel",
                "Initiator",
                "RequestTimestamp",
                "ResponseTimestamp",
                "Comment"
            ],
            "RequestedServiceProvider" => [
                "Country"
            ],
            "PreferredServiceArea" => [
                "AdministrativeAreaLevel1",
                "AdministrativeAreaLevel2",
                "City",
                "Country",
                "FormattedAddress",
                "HouseNumber",
                "PostalCode",
                "RoadType",
                "StreetName"
            ]
        ];

        $case = [
            "Case" => [
                "Id",
                "ExternalReferences" => [
                    "Reference",
                    "Source"
                ],
                "IncidentLocation" => [
                    "Id",
                    "ExternalReferences" => [
                        "Reference",
                        "Source"
                    ],
                    "LocationType",
                    "Address" => [
                        "AdministrativeAreaLevel1",
                        "AdministrativeAreaLevel2",
                        "City",
                        "Country",
                        "FormattedAddress",
                        "HouseNumber",
                        "PostalCode",
                        "RoadType",
                        "StreetName"
                    ],
                    "Comment",
                    "GeoPosition" => [
                        "Latitude",
                        "Longitude"
                    ],
                ],
                "IncidentTimestamp",
                "SubmissionTimestamp",
                "IncidentType",
                "Owner" => [
                    "\$type",
                    "Person" => [
                        "ContactDetails" => [
                            "DaytimeNumber",
                            "EmailAddress",
                            "FaxNumber",
                            "MobileNumber",
                            "NighttimeNumber"
                        ],
                        "FirstName",
                        "Gender",
                        "HomeAddress" => [
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                            "LocationType",
                            "Address" => [
                                "AdministrativeAreaLevel1",
                                "AdministrativeAreaLevel2",
                                "City",
                                "Country",
                                "FormattedAddress",
                                "HouseNumber",
                                "PostalCode",
                                "RoadType",
                                "StreetName"
                            ],
                            "Comment",
                            "GeoPosition" => [
                                "Latitude",
                                "Longitude"
                            ],
                        ],
                        "LastName",
                        "CommunicationLanguages",
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                    ],
                    "Company" => [
                        "Code",
                        "CodingScheme",
                        "LegalName",
                        "OperationalAddress" => [
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                            "LocationType",
                            "Address" => [
                                "AdministrativeAreaLevel1",
                                "AdministrativeAreaLevel2",
                                "City",
                                "Country",
                                "FormattedAddress",
                                "HouseNumber",
                                "PostalCode",
                                "RoadType",
                                "StreetName"
                            ],
                            "Comment",
                            "GeoPosition" => [
                                "Latitude",
                                "Longitude"
                            ],
                        ],
                        "OperationalContactDetails" => [
                            "DaytimeNumber",
                            "EmailAddress",
                            "FaxNumber",
                            "MobileNumber",
                            "NighttimeNumber"
                        ],
                        "RegisteredOfficeAddress" => [
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                            "LocationType",
                            "Address" => [
                                "AdministrativeAreaLevel1",
                                "AdministrativeAreaLevel2",
                                "City",
                                "Country",
                                "FormattedAddress",
                                "HouseNumber",
                                "PostalCode",
                                "RoadType",
                                "StreetName"
                            ],
                            "Comment",
                            "GeoPosition" => [
                                "Latitude",
                                "Longitude"
                            ],
                        ],
                        "RegisteredOfficeContactDetails" => [
                            "DaytimeNumber",
                            "EmailAddress",
                            "FaxNumber",
                            "MobileNumber",
                            "NighttimeNumber"
                        ],
                        "TradeName",
                        "VatNumber",
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                    ],
                ],
                "Vehicle" => [
                    "Driver" => [
                        "ContactDetails" => [
                            "DaytimeNumber",
                            "EmailAddress",
                            "FaxNumber",
                            "MobileNumber",
                            "NighttimeNumber"
                        ],
                        "FirstName",
                        "Gender",
                        "HomeAddress" => [
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                            "LocationType",
                            "Address" => [
                                "AdministrativeAreaLevel1",
                                "AdministrativeAreaLevel2",
                                "City",
                                "Country",
                                "FormattedAddress",
                                "HouseNumber",
                                "PostalCode",
                                "RoadType",
                                "StreetName"
                            ],
                            "Comment",
                            "GeoPosition" => [
                                "Latitude",
                                "Longitude"
                            ],
                        ],
                        "LastName",
                        "CommunicationLanguages",
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                    ],
                    "ContactPerson" => [
                        "ContactDetails" => [
                            "DaytimeNumber",
                            "EmailAddress",
                            "FaxNumber",
                            "MobileNumber",
                            "NighttimeNumber"
                        ],
                        "FirstName",
                        "Gender",
                        "HomeAddress" => [
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                            "LocationType",
                            "Address" => [
                                "AdministrativeAreaLevel1",
                                "AdministrativeAreaLevel2",
                                "City",
                                "Country",
                                "FormattedAddress",
                                "HouseNumber",
                                "PostalCode",
                                "RoadType",
                                "StreetName"
                            ],
                            "Comment",
                            "GeoPosition" => [
                                "Latitude",
                                "Longitude"
                            ],
                        ],
                        "LastName",
                        "CommunicationLanguages",
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                    ],
                    "Dealer" => [
                        "Code",
                        "CodingScheme",
                        "LegalName",
                        "OperationalAddress" => [
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                            "LocationType",
                            "Address" => [
                                "AdministrativeAreaLevel1",
                                "AdministrativeAreaLevel2",
                                "City",
                                "Country",
                                "FormattedAddress",
                                "HouseNumber",
                                "PostalCode",
                                "RoadType",
                                "StreetName"
                            ],
                            "Comment",
                            "GeoPosition" => [
                                "Latitude",
                                "Longitude"
                            ],
                        ],
                        "OperationalContactDetails" => [
                            "DaytimeNumber",
                            "EmailAddress",
                            "FaxNumber",
                            "MobileNumber",
                            "NighttimeNumber"
                        ],
                        "RegisteredOfficeAddress" => [
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                            "LocationType",
                            "Address" => [
                                "AdministrativeAreaLevel1",
                                "AdministrativeAreaLevel2",
                                "City",
                                "Country",
                                "FormattedAddress",
                                "HouseNumber",
                                "PostalCode",
                                "RoadType",
                                "StreetName"
                            ],
                            "Comment",
                            "GeoPosition" => [
                                "Latitude",
                                "Longitude"
                            ],
                        ],
                        "RegisteredOfficeContactDetails" => [
                            "DaytimeNumber",
                            "EmailAddress",
                            "FaxNumber",
                            "MobileNumber",
                            "NighttimeNumber"
                        ],
                        "TradeName",
                        "VatNumber",
                        "Id",
                        "ExternalReferences" => [
                            "Reference",
                            "Source"
                        ],
                    ],
                    "Owner" => [
                        "\$type",
                        "Person" => [
                            "ContactDetails" => [
                                "DaytimeNumber",
                                "EmailAddress",
                                "FaxNumber",
                                "MobileNumber",
                                "NighttimeNumber"
                            ],
                            "FirstName",
                            "Gender",
                            "HomeAddress" => [
                                "Id",
                                "ExternalReferences" => [
                                    "Reference",
                                    "Source"
                                ],
                                "LocationType",
                                "Address" => [
                                    "AdministrativeAreaLevel1",
                                    "AdministrativeAreaLevel2",
                                    "City",
                                    "Country",
                                    "FormattedAddress",
                                    "HouseNumber",
                                    "PostalCode",
                                    "RoadType",
                                    "StreetName"
                                ],
                                "Comment",
                                "GeoPosition" => [
                                    "Latitude",
                                    "Longitude"
                                ],
                            ],
                            "LastName",
                            "CommunicationLanguages",
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                        ],
                        "Company" => [
                            "Code",
                            "CodingScheme",
                            "LegalName",
                            "OperationalAddress" => [
                                "Id",
                                "ExternalReferences" => [
                                    "Reference",
                                    "Source"
                                ],
                                "LocationType",
                                "Address" => [
                                    "AdministrativeAreaLevel1",
                                    "AdministrativeAreaLevel2",
                                    "City",
                                    "Country",
                                    "FormattedAddress",
                                    "HouseNumber",
                                    "PostalCode",
                                    "RoadType",
                                    "StreetName"
                                ],
                                "Comment",
                                "GeoPosition" => [
                                    "Latitude",
                                    "Longitude"
                                ],
                            ],
                            "OperationalContactDetails" => [
                                "DaytimeNumber",
                                "EmailAddress",
                                "FaxNumber",
                                "MobileNumber",
                                "NighttimeNumber"
                            ],
                            "RegisteredOfficeAddress" => [
                                "Id",
                                "ExternalReferences" => [
                                    "Reference",
                                    "Source"
                                ],
                                "LocationType",
                                "Address" => [
                                    "AdministrativeAreaLevel1",
                                    "AdministrativeAreaLevel2",
                                    "City",
                                    "Country",
                                    "FormattedAddress",
                                    "HouseNumber",
                                    "PostalCode",
                                    "RoadType",
                                    "StreetName"
                                ],
                                "Comment",
                                "GeoPosition" => [
                                    "Latitude",
                                    "Longitude"
                                ],
                            ],
                            "RegisteredOfficeContactDetails" => [
                                "DaytimeNumber",
                                "EmailAddress",
                                "FaxNumber",
                                "MobileNumber",
                                "NighttimeNumber"
                            ],
                            "TradeName",
                            "VatNumber",
                            "Id",
                            "ExternalReferences" => [
                                "Reference",
                                "Source"
                            ],
                        ],
                    ],
                    "MileageReports" => [
                        "Value",
                        "Timestamp",
                        "Source"
                    ],
                    "PresentItems" => [
                        "Code",
                        "Comment"
                    ],
                    "PrimaryFaults" => [
                        "ComponentCode",
                        "FaultCode",
                        "OutcomeCode",
                        "FaultTimestamp",
                        "ObdCodes",
                        "SymptomDescription",
                        "Source"
                    ],
                    "TotalNumberOfPassengers",
                    "VehicleDetails" => [
                        "Vin",
                        "ArcVehicleModel",
                        "Make",
                        "Model",
                        "Type",
                        "Version",
                        "Color",
                        "FirstCountryOfSale",
                        "FirstCountryOfRegistration",
                        "CurrentCountryOfRegistration",
                        "EngineCapacity",
                        "FirstDateOfRegistration",
                        "FirstDateOfSale",
                        "IsRegistered",
                        "LastCountryOfInspection",
                        "LastCountryOfSale",
                        "LastDateOfInspection",
                        "LastDateOfRegistration",
                        "LastDateOfSale",
                        "LastInspectionMileage",
                        "LicensePlate",
                        "PropulsionType",
                        "TransmissionType",
                        "DaysToNextService",
                        "NextServiceMileage",
                        "LastDateOfService",
                        "LastServiceMileage"
                    ]
                ],
                "Services" => [
                    "SecondaryBenefitsService" => $service,
                    "AccommodationService" => $service,
                    "CallCenterService" => $service,
                    "HopService" => $service,
                    "OffRoadRecoveryService" => $service,
                    "VehicleReplacementService" => $service,
                    "RoadsideAssistanceService" => $service,
                    "TelematicsService" => $service,
                    "TravelService" => $service,
                    "VehicleStorageService" => $service,
                    "VehicleTransportationService" => $service,
                ],
                "Payment",
                "Notes" => [
                    "Timestamp",
                    "Source",
                    "OperatorName",
                    "Message"
                ]
            ]
        ];

        $this->readAttributeTree($case);
    }

    private function readAttributeTree($branch, $parent = null)
    {
        foreach ($branch as $key => $value) {
            $attribute = new Attribute();
            if (isset($parent)) {
                $attribute->setParent($parent);
            }
            $attribute->setTitle(is_array($value) ? $key : $value);

            $this->em->persist($attribute);
            $this->em->flush();

            if (is_array($value)) {
                $this->readAttributeTree($value, $attribute);
            }
        }
    }
}
