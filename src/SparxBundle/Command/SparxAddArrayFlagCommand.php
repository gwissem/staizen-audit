<?php

namespace SparxBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SparxBundle\Entity\Attribute;

class SparxAddArrayFlagCommand extends ContainerAwareCommand
{
    private $needArrays = [
        "ApprovalRequests",
        "ExternalReferences",
        "IncurredCosts",
        "MeasuredPerformances",
        "MileageReports",
        "Notes",
        "PaymentGuarantees",
        "PerformedActions",
        "PresentItems",
        "PrimaryFaults",
        "Services",
    ];

    private $em;
    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('sparx:add-array-flag')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'No arguments')
            ->addOption('option', null, InputOption::VALUE_NONE, 'No options')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $attributes = $this->em->getRepository(Attribute::class)->findAll();

        foreach ($attributes as $attribute) {
            if (in_array($attribute->getTitle(), $this->needArrays)) {
                $attribute->setIsArray(true);

                $this->em->persist($attribute);
                $this->em->flush();

                $output->writeln('Set flag on attribute ' . $attribute->getTitle() . '.');
            }
        }

        $output->writeln('Command result.');
    }
}