<?php

namespace SparxBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SparxBundle\Entity\Status as SparxStatus;

class SparxInsertStatusesCommand extends ContainerAwareCommand
{
    private $statuses = [];

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->initStatuses();

        parent::__construct();
    }

    private function initStatuses()
    {
        $this->statuses = [
            'Requested',
            'Planned',
            'InProgress',
            'Cancelled',
            'Rejected',
            'Refused',
            'CompletedUnsuccessfully',
            'CompletedSuccessfully'
        ];
    }

    protected function configure()
    {
        $this
            ->setName('sparx:insert-statuses')
            ->setDescription('This command is to insert statuses for Sparx Services.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Command started! Preparing statuses to insert.');

        $repository = $this->em->getRepository(SparxStatus::class);
        $insert = false;
        foreach ($this->statuses as $status) {
            if (empty($repository->findBy(['serviceName' => $status]))) {
                $insert = true;
                $output->writeln('Insert "' . $status . '" into database." ');
                $sparxStatus = new SparxStatus();
                $sparxStatus->setServiceName($status);

                $this->em->persist($sparxStatus);

                $this->em->flush();
            }
        }

        if (!$insert) {
            $output->writeln('There is nothing new to insert.');
        } else {
            $output->writeln('Command completed.');
        }

        $output->writeln('Command finished.');
    }

}
