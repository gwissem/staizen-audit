<?php

namespace MailboxBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MailboxCronCommand extends ContainerAwareCommand
{
    /**
     * Odczytanie 1 e-maila z przykładowej skrzynki
     *
     * php bin/console atlas:mailbox:run-cron --mailbox=atlas_test@starter24.pl
     */

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:mailbox:run-cron')
            ->addOption(
                'mailbox',
                'm',
                InputOption::VALUE_OPTIONAL,
                'Set for read last unseen email in this mailbox.',
                null
            )
            ->setDescription('Check list of mailboxes');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln([
            '',
            '<comment>Cron is running...</comment>',
            '===================',
            '',
        ]);

        $mailboxName = $input->getOption('mailbox');

        if(!empty($mailboxName)) {

            $this->getContainer()->get('atlas.mailbox.management.service')->processLastUnseenEmailInMailbox($mailboxName);
            $result = '';

        }
        else {

            $mailboxManagementService = $this->getContainer()->get('atlas.mailbox.management.service');
            $result = $mailboxManagementService->startCron(false, true);

        }

        $output->writeln([
            '<info>Result</info>',
            '==================='
        ]);

        $output->writeln([
            '',
            $result,
            ''
        ]);

        $output->writeln(sprintf("%s\n", "<info>End process.</info>"));
    }
}
