<?php

namespace MailboxBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NoteSendCommand extends ContainerAwareCommand
{

    /**
     *
     *  php bin/console atlas:mailbox:note-send 123
     */

    protected function configure()
    {
        $this
            ->setName('atlas:mailbox:note-send')
            ->addArgument('noteId', InputArgument::REQUIRED, 'Note attributes parent')
            ->setDescription('Command to send note')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $noteId = $input->getArgument('noteId');
        $response = $this->getContainer()->get('atlas.mailbox.outgoing_email.service')->sendNote($noteId);
        $output->writeln($response);
    }


}
