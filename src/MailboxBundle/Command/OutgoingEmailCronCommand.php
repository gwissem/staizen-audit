<?php

namespace MailboxBundle\Command;

use DocumentBundle\Utils\HandlerExtraFiles;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class OutgoingEmailCronCommand extends ContainerAwareCommand
{
    /**
     *  How run:
     *
     * FROM CONSOLE:
     * php bin/console atlas:mailbox:send --limit=20  --only_show=false --white_list=4508894
     *
     * JOB IN CRON: */

    // */1 * * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console atlas:mailbox:send --limit=20  --only_show=false


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:mailbox:send')
            ->setDescription('Send e-mail from notes')
            ->addOption(
                'limit',
                'l',
                InputOption::VALUE_OPTIONAL,
                'Set limit for send e-mails.',
                20
            )
            ->addOption(
                'only_show',
                's',
                InputOption::VALUE_OPTIONAL,
                'Set if only show email for send.',
                false
            )
            ->addOption(
                'white_list',
                'w',
                InputOption::VALUE_OPTIONAL,
                'Set whitelist of notes id.',
                false
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $io = new SymfonyStyle($input, $output);
        $limit = intval($input->getOption('limit'));
        $whiteListOption = $input->getOption('white_list');

        if(!empty($whiteListOption)) {
            $whiteListOption = explode(',', $whiteListOption);
        }

        $onlyShow = filter_var($input->getOption('only_show'), FILTER_VALIDATE_BOOLEAN);

        $io->title('Start.');

        $io->comment('Execute command with parameters:');
        $io->listing([
            'Limit: ' . $limit,
            'onlyShow: ' . $onlyShow
        ]);

        $mailboxOutgoingEmailService = $this->getContainer()->get('atlas.mailbox.outgoing_email.service');
        $mailboxOutgoingEmailService->setLimit($limit);

        if(!empty($whiteListOption)) {
            $mailboxOutgoingEmailService->setWhiteList($whiteListOption);
        }

        $mailboxOutgoingEmailService->startSending(false, $io, $onlyShow);

        $io->title('End.');

    }
}
