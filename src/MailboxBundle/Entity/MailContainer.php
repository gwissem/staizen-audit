<?php

namespace MailboxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DocumentBundle\Entity\File;

/**
 * MailContainer
 *
 * @ORM\Table(name="mail_container")
 * @ORM\Entity(repositoryClass="MailboxBundle\Repository\MailContainerRepository")
 */
class MailContainer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="email_id", type="integer", nullable=true)
     */
    private $emailId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="MailboxBundle\Entity\ConnectedMailbox")
     * @ORM\JoinColumn(name="mailbox_id", referencedColumnName="id", onDelete="CASCADE")
     */

    private $mailbox;

    /**
     * @ORM\OneToOne(targetEntity="DocumentBundle\Entity\File")
     * @ORM\JoinColumn(name="mail_file", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */

    private $mailFile = null;

    /**
     * @var string
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */

    private $subject;

    /**
     * @var string
     * @ORM\Column(name="sender", type="string", length=255, nullable=true)
     */

    private $sender;

    /**
     * @var string
     * @ORM\Column(name="procedureQuery", type="text", nullable=true)
     */

    private $procedureQuery;

    /**
     * MailContainer constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return MailContainer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getMailbox()
    {
        return $this->mailbox;
    }

    /**
     * @param mixed $mailbox
     */
    public function setMailbox($mailbox)
    {
        $this->mailbox = $mailbox;
    }

    /**
     * @return File
     */
    public function getMailFile()
    {
        return $this->mailFile;
    }

    /**
     * @param mixed $mailFile
     */
    public function setMailFile($mailFile)
    {
        $this->mailFile = $mailFile;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
    }

    /**
     * @return string
     */
    public function getProcedureQuery()
    {
        return $this->procedureQuery;
    }

    /**
     * @param string $procedureQuery
     */
    public function setProcedureQuery($procedureQuery)
    {
        $this->procedureQuery = $procedureQuery;
    }

    /**
     * @return int
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * @param int $emailId
     */
    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;
    }


}

