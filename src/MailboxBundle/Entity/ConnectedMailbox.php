<?php

namespace MailboxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MailboxBundle\Service\SecurityService;
use Symfony\Component\Validator\Constraints\DateTime;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ConnectedMailbox
 *
 * @ORM\Table(name="connected_mailbox")
 * @ORM\Entity(repositoryClass="MailboxBundle\Repository\ConnectedMailboxRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ConnectedMailbox
{

    const DEFAULT_PROTOCOL = self::PROTOCOL_IMAP;

    const PROTOCOL_POP3 = 'pop3';
    const PROTOCOL_IMAP = 'imap';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=255)
     */
    private $host;

    /**
     * @var integer
     *
     * @ORM\Column(name="port", type="integer")
     */

    private $port = 993;

    /**
     * @var string
     *
     * @ORM\Column(name="protocol", type="string", length=10)
     */

    private $protocol = self::DEFAULT_PROTOCOL;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ssl", type="boolean", nullable=true)
     */

    private $ssl = true;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="connection_string", type="string", length=255, nullable=true)
     */
    private $connectionString;

    /**
     * @var string
     *
     * @ORM\Column(name="procedure_text", type="text", nullable=true)
     */
    private $procedureText;

    /**
     * @var int
     *
     * @ORM\Column(name="amount_processed_emails", type="integer")
     */
    private $amountProcessedEmails = 0;

    /**
     * Data ostatniego przerobionego e-maila
     *
     * @var \DateTime
     *
     * @ORM\Column(name="last_processed", type="datetime", nullable=true)
     */
    private $lastProcessed;

    /**
     * Data ostatniego sprawdzenia skrzynki
     *
     * @var \DateTime
     *
     * @ORM\Column(name="last_run", type="datetime", nullable=true)
     */
    private $lastRun;

    /**
     * @var integer
     *
     * @ORM\Column(name="interval_cron", type="integer")
     */

    private $intervalCron = 300;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */

    private $enabled = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="novalidate_cert", type="boolean", nullable=true)
     */

    private $novalidateCert = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var int
     * @ORM\Column(name="filter_subject", type="smallint")
     */

    private $filterSubject = false;

    /**
     * ConnectedMailbox constructor.
     */

    public function __construct()
    {
        $this->lastProcessed = new \DateTime('0001-01-01 00:00:00');
        $this->lastRun = new \DateTime();
//        $this->lastRun = new \DateTime('0001-01-01 00:00:00');
    }

    public static function getProtocolArray()
    {
        return [
            'POP3' => self::PROTOCOL_POP3,
            'IMAP' => self::PROTOCOL_IMAP
        ];
    }

    public static function getAvailableIntervalArray(){
        return [
            '1 min' => 60,
            '2 min' => 120,
            '5 min' => 300,
            '10 min' => 600,
            '30 min' => 1800,
            '1 h' => 3600,
            '24 h' => 86400
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ConnectedMailbox
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set host
     *
     * @param string $host
     *
     * @return ConnectedMailbox
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return ConnectedMailbox
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return ConnectedMailbox
     */
    public function setPassword($password)
    {

        if(SecurityService::USE_IN_MAILBOX){
            $this->password = SecurityService::encrypt($password, SecurityService::SECRET_KEY);
        }
        else {
            $this->password = $password;
        }

        return $this;
    }

    /**
     * Get password
     *
     * @param bool $decrypt
     * @return string
     */
    public function getPassword($decrypt = true)
    {
        if(SecurityService::USE_IN_MAILBOX && $decrypt){
            return SecurityService::decrypt($this->password, SecurityService::SECRET_KEY);
        }

        return $this->password;
    }

    /**
     * Set procedureText
     *
     * @param string $procedureText
     *
     * @return ConnectedMailbox
     */
    public function setProcedureText($procedureText)
    {
        $this->procedureText = $procedureText;

        return $this;
    }

    /**
     * Get procedureText
     *
     * @return string
     */
    public function getProcedureText()
    {
        return $this->procedureText;
    }

    /**
     * Set amountProcessedEmails
     *
     * @param integer $amountProcessedEmails
     *
     * @return ConnectedMailbox
     */
    public function setAmountProcessedEmails($amountProcessedEmails)
    {
        $this->amountProcessedEmails = $amountProcessedEmails;

        return $this;
    }

    /**
     * Get amountProcessedEmails
     *
     * @return int
     */
    public function getAmountProcessedEmails()
    {
        return $this->amountProcessedEmails;
    }

    /**
     * Set lastProcessed
     *
     * @param \DateTime $lastProcessed
     *
     * @return ConnectedMailbox
     */
    public function setLastProcessed($lastProcessed)
    {
        $this->lastProcessed = $lastProcessed;

        return $this;
    }

    /**
     * Get lastProcessed
     *
     * @return \DateTime
     */
    public function getLastProcessed()
    {
        return $this->lastProcessed;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @param string $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @return boolean
     */
    public function isSsl()
    {
        return $this->ssl;
    }

    /**
     * @param boolean $ssl
     */
    public function setSsl($ssl)
    {
        $this->ssl = $ssl;
    }

    /**
     * @return \DateTime
     */
    public function getLastRun()
    {
        return $this->lastRun;
    }

    /**
     * @param \DateTime $lastRun
     */
    public function setLastRun($lastRun)
    {
        $this->lastRun = $lastRun;
    }

    /**
     * @return int
     */
    public function getIntervalCron()
    {
        return $this->intervalCron;
    }

    /**
     * @param int $intervalCron
     */
    public function setIntervalCron($intervalCron)
    {
        $this->intervalCron = $intervalCron;
    }

    /**
     * @return string
     */
    public function getConnectionString()
    {
        return $this->connectionString;
    }

    /**
     * @param string $connectionString
     */
    public function setConnectionString($connectionString)
    {
        $this->connectionString = $connectionString;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return boolean
     */
    public function isNovalidateCert()
    {
        return $this->novalidateCert;
    }

    /**
     * @param boolean $novalidateCert
     */
    public function setNovalidateCert($novalidateCert)
    {
        $this->novalidateCert = $novalidateCert;
    }

    /**
     * @return boolean
     */
    public function getFilterSubject()
    {
        return (filter_var($this->filterSubject, FILTER_VALIDATE_BOOLEAN));
    }

    /**
     * @param int $filterSubject
     */
    public function setFilterSubject($filterSubject)
    {
        $this->filterSubject = $filterSubject;
    }
}

