<?php

namespace MailboxBundle\Service;

use Symfony\Component\Translation\Exception\NotFoundResourceException;

class CustomContainerFile extends MailContainerFile {

    protected function checkFile()
    {
        parent::checkFile();

        $this->extension = pathinfo($this->sourceFile, PATHINFO_EXTENSION);
    }

}