<?php

namespace MailboxBundle\Service;

use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class MailContainerFile {

    protected $sourceFile;
    protected $fileSize;
    protected $mimeType;
    protected $extension;
    protected $originalName;

    /**
     * MailContainerFile constructor.
     * @param $sourceFile
     * @param $originalName
     * @internal param $name
     * @internal param $path
     */
    public function __construct($sourceFile, $originalName)
    {
        $this->sourceFile = $sourceFile;
        $this->originalName = $originalName;
        $this->checkFile();
    }

    protected function checkFile()
    {
        if(!file_exists($this->sourceFile)) throw new NotFoundResourceException('Nie znaleziono pliku: ' . $this->sourceFile);

        $this->fileSize = filesize($this->sourceFile);
        $this->mimeType = MimeTypeGuesser::getInstance()->guess($this->sourceFile);
        $this->extension = 'eml';

    }

    public function move($target)
    {
        rename($this->sourceFile, $target);
    }

    /**
     * @return mixed
     */
    public function getSourceFile()
    {
        return $this->sourceFile;
    }

    /**
     * @param mixed $sourceFile
     */
    public function setSourceFile($sourceFile)
    {
        $this->sourceFile = $sourceFile;
    }

    /**
     * @return mixed
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @param mixed $fileSize
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;
    }

    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param mixed $mimeType
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return mixed
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * @param mixed $originalName
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
    }


}