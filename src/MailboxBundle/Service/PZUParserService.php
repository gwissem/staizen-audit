<?php

namespace MailboxBundle\Service;

use AppBundle\Entity\Log;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\Step;
use MssqlBundle\PDO\PDO;
use Symfony\Component\DependencyInjection\Container;

class PZUParserService
{
    CONST STEP_ID_NEW_MAIL_ARRIVED = '1011.104';
    CONST ATTR_PZU_DRIVER_FULL_NAME_PHONE_KEY = 'ATTR_PZU_DRIVER_FULL_NAME_PHONE';

    CONST ATTR_PZU_MAKE_MODEL_TYPE_REG_NUMBER_KEY = 'ATTR_PZU_MAKE_MODEL_TYPE_REG_NUMBER';
    CONST ATTR_PZU_DATE_KEY = 'ATTR_PZU_DATE';
    CONST ATTR_PZU_PEOPLE_COUNT_KEY = 'ATTR_PZU_PEOPLE_COUNT';
    CONST ATTR_PZU_CAR_DMC_KEY = 'ATTR_PZU_CAR_DMC';
    CONST ATTR_PZU_CAR_DATE_KEY = 'ATTR_PZU_CAR_DATE';
    CONST ATTR_PZU_CAR_VIN_KEY = 'ATTR_PZU_CAR_VIN';
    CONST ATTR_PZU_LOCATION_EXTRA_KEY = 'ATTR_PZU_LOCATION_EXTRA';

    CONST ATTR_PZU_CALLER_FULL_NAME_KEY = 'ATTR_PZU_CONTACT_FULL_NAME';
    CONST ATTR_PZU_CALLER_PHONE_NUMBER_KEY = 'ATTR_PZU_CONTACT_PHONE_NUMBER';

    CONST ATTR_PZU_CALLER_FIRST_NAME_PATH = '80,342,64';
    CONST ATTR_PZU_CALLER_LAST_NAME_PATH = '80,342,66';
    CONST ATTR_PZU_CALLER_PHONE_PATH = '80,342,408,197';
    CONST ATTR_PZU_DRIVER_FIRST_NAME_PATH = '81,342,64';
    CONST ATTR_PZU_DRIVER_LAST_NAME_PATH = '81,342,66';
    CONST ATTR_PZU_DRIVER_PHONE_PATH = '81,342,408,197';
    CONST ATTR_PZU_DATE_PATH = '1122,1131';
    CONST ATTR_PZU_LOCATION_EXTRA_PATH = '1122,624,626,687';
    CONST ATTR_PZU_MAKE_MODEL_PATH = '74,73';
    CONST ATTR_PZU_REG_NUMBER_PATH = '74,72';
    CONST ATTR_PZU_CONTACT_FIRST_NAME_PATH = '81,342,64';
    CONST ATTR_PZU_CONTACT_LAST_NAME_PATH = '81,342,66';
    CONST ATTR_PZU_CONTACT_PHONE_NUMBER_PATH = '81,342,408,197';
    CONST ATTR_PZU_PEOPLE_COUNT_PATH = '79';
    CONST ATTR_PZU_SERVICES_ORDERED_PATH = '';
    CONST ATTR_PZU_CASE_NUMBER_PATH = '1120';
    CONST ATTR_PZU_CASE_TEXT_PATH = '1121';
    CONST ATTR_PLATFORM_PATH = '253';
    CONST ATTR_PROGRAM_PATH = '202';
    CONST ATTR_PROGRAMS_PATH = '204';
    CONST ATTR_PZU_CAR_DMC_PATH = '74,76';
    CONST ATTR_PZU_CAR_DATE_PATH = '74,233';
    CONST ATTR_PZU_CAR_VIN_PATH = '74,71';

    CONST ATTR_PZU_TOWING_LIMIT_PATH = '1121,1123';
    CONST ATTR_PZU_TOWING_LIMIT_KEY = 'ATTR_PZU_TOWING_LIMIT';

    private $resultArray = [];

    private $queryManager;
    private $container;
    private $processHandler;

    public function __construct(QueryManager $queryManager, Container $container)
    {
        $this->queryManager = $queryManager;
        $this->container = $container;
        $this->processHandler = $container->get('case.process_handler');
    }

    /**
     * @param $subject
     * @param $text
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function makeCase($subject, $text)
    {
        $PZUCaseNumber = $this->parseSubject($subject);

        if ($PZUCaseNumber) {

            $alreadyExistsQuery = 'SELECT top 1 root_process_instance_id root_id  from dbo.attribute_value with (nolock) where value_string = :PZUCaseNumber order by id asc';

            $parameters = [
                [
                    'key' => 'PZUCaseNumber',
                    'value' => $PZUCaseNumber,
                    'type' => PDO::PARAM_STR
                ]
            ];

            $alreadyExists = $this->queryManager->executeProcedure($alreadyExistsQuery, $parameters);

            if (isset($alreadyExists[0]['root_id']) && ((int)$alreadyExists[0]['root_id'] > 0)) {
                // CASE ALREADY EXISTS
                $rootId = (int)$alreadyExists[0]['root_id'];
                $processInstanceId = $this->processHandler->start(self::STEP_ID_NEW_MAIL_ARRIVED, $rootId);
                $this->processHandler->setPriorityOfProcess($processInstanceId);

                return;

            }
        }

        $this->parseMail($text);
        $rootId = $this->makeNewCase();

        try {
            $this->saveAttributes($rootId);

        } catch (\Exception $exception) {
            $log = new Log();
            $log->setName('Exception on PZU Mail Parsing');
            $log->setContent($exception->getMessage());
            $em = $this->container->get('doctrine')->getEntityManager();
            $em->persist($log);
            $em->flush();
        }
    }

    /**
     * @param $text
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function parseMail($text)
    {
        try {
            $parserAttributes =
                [
                    'ATTR_PZU_DRIVER_FULL_NAME_PHONE' => 'Imię i nazwisko poszkodowanego',
                    'ATTR_PZU_DATE' => 'Data powstania szkody',
                    'ATTR_PZU_MAKE_MODEL_TYPE_REG_NUMBER' => 'Marka, model, typ, nr rej. pojazdu',
                    'ATTR_PZU_CONTACT_FULL_NAME' => 'Imię i nazwisko osoby do kontaktu',
                    'ATTR_PZU_CONTACT_PHONE_NUMBER' => 'Numer telefonu osoby do kontaktu',
                    'ATTR_PZU_CAR_DMC' => 'DMC',
                    'ATTR_PZU_CAR_DATE' => 'Data I rejestracji',
                    'ATTR_PZU_PEOPLE_COUNT' => 'Liczba osób',
                    'ATTR_PZU_SERVICES_ORDERED' => 'Rodzaj usługi',
                    'ATTR_PZU_LOCATION_EXTRA' => 'Adres siedziby poszkodowanego',
                    'ATTR_PZU_TOWING_LIMIT' => 'Przysługujący limit na holowanie',
                    'ATTR_PZU_CAR_VIN' => 'Nr VIN pojazdu',

                ];

            $text = preg_replace('/<br>/', 'NEWLINESYMBOL', $text);
            $text = preg_replace('/<[^>]*>/', '', $text);
            $text = preg_replace('/\s+>/', '', $text);
            $text = preg_replace("/(^[\r\n]*|[\r\n]+)[\r\n]+/", "NEWLINESYMBOL", $text);
            $text = trim($text);
            $text = preg_replace('/&nbsp;/', '', $text);
//            Obcinanie wszystkich nieważnych rzeczy, obcinanie tagów outlooka
            if (strpos($text, 'Imię i nazwisko poszkodowanego') !== false) {
                $textToCut = explode('Imię i nazwisko poszkodowanego', $text)[0];
                $text = str_replace($textToCut, '', $text);
            }
//            Tniemy maila po nowych liniach
            $textArray = explode('NEWLINESYMBOL', $text);
            $textToExcerpt = '';
//        Przechodzimy po wszystkich liniach z maila
//                Usuwamy niepotrzebne iteracje
            foreach ($textArray as $line) {
                if(strlen($line) ==0){
                   continue;
               }
                if (
                (strpos($line, 'Z poważaniem') !== false) ||
                (strpos($line, 'Z wyrazami szacunku') !== false) ||
                (strpos($line, 'Pozdrawiam') !== false) ||
                (strpos($line, 'Powszechny Zakład Ubezpieczeń Spółka Akcyjna') !== false)
                ) {
                    break;
                }
//                Uzupełnianie pod headery
                if (trim($line) != "") {
                    if ((strpos($line, ':') !== false) || (strpos(trim($line), '-') === 0)) {
                        $lineSplited = explode(':', $line);
                        $header = $lineSplited[0];
                        $lineToAdd = str_replace($header, '<b>' . $header . '</b>', $line);
                        $textToExcerpt .= $lineToAdd . '<br>';
                    } else {
                        $textToExcerpt .= $line . '<br>';
                    }
                }
//            Sprawdzamy czy w linii jest klucz, który możemy spisać
                foreach ($parserAttributes as $key => $parserAttribute) {
                    //                Jeżeli w linii jest poszukiwany string ...

                    try {
//                        Jeżeli trafiamy na klucz
                        if (strpos($line, $parserAttribute) !== false) {
//                            Jeśli dwukropek - tniemy do samej wartości
                            if (strpos($line, ':') !== false) {

                                $value = explode(':', $line);
                                if (isset($value[1])) {
//                                    Set right hand
                                    $value = $value[1];
                                }
                            } else {
//                                Tniemy do zawartości całej linijki poza szukanym kluczem
                                $value = str_replace($parserAttribute, '', $line);
                            }

                            $value = trim($value);

                            switch ($key):
                                case self::ATTR_PZU_CALLER_FULL_NAME_KEY:
                                    $firstName = trim(explode(' ', $value)[0]);
                                    $lastName = trim(str_replace($firstName, '', $value));
                                    $this->addToArray(self::ATTR_PZU_CALLER_FIRST_NAME_PATH, $firstName);
                                    $this->addToArray(self::ATTR_PZU_CALLER_LAST_NAME_PATH, $lastName);
                                    break;
                                case self::ATTR_PZU_CALLER_PHONE_NUMBER_KEY:
                                    $digitsOnly = '/\d+/m';
                                    preg_match($digitsOnly, $value, $phoneMatches);
                                    $phone = $phoneMatches[0];
                                    $this->addToArray(self::ATTR_PZU_CALLER_PHONE_PATH, $phone);
                                    break;
                                case self::ATTR_PZU_DRIVER_FULL_NAME_PHONE_KEY:

//                                Imię, nazwisko, nr telefonu
                                    $fullName = trim(explode(',', $value)[0]);
                                    $phoneString = explode(',', $value)[1];
                                    $firstName = trim(explode(' ', $fullName)[0]);
                                    $lastName = trim(str_replace($firstName, '', $fullName));
                                    $digitsOnly = '/\d+/m';
                                    preg_match($digitsOnly, $phoneString, $phoneMatches);
                                    $phone = $phoneMatches[0];
                                    $this->addToArray(self::ATTR_PZU_DRIVER_PHONE_PATH, $phone);
                                    $this->addToArray(self::ATTR_PZU_DRIVER_FIRST_NAME_PATH, $firstName);
                                    $this->addToArray(self::ATTR_PZU_DRIVER_LAST_NAME_PATH, $lastName);
                                    break;
                                case self::ATTR_PZU_DATE_KEY:
//                                Data zdarzenia

                                    $this->addToArray(self::ATTR_PZU_DATE_PATH, $value);
                                    break;
                                case self::ATTR_PZU_CAR_DATE_KEY:
//                                Data zdarzenia
                                    $value = substr($value,0,10);
                                    $value = str_replace('/','-',$value);
                                    $this->addToArray(self::ATTR_PZU_CAR_DATE_PATH, $value);
                                    break;
                                case self::ATTR_PZU_TOWING_LIMIT_KEY:
                                    $digitsOnly = '/\d+/m';
                                    preg_match($digitsOnly, $value, $kmOfTowing);
                                    if (count($kmOfTowing)) {
                                        $this->addToArray(self::ATTR_PZU_TOWING_LIMIT_PATH, $kmOfTowing[0]);
                                    }
                                    break;
                                case self::ATTR_PZU_CAR_VIN_KEY:
                                    $regex ='/[A-HJ-NPR-Z0-9]{17}|[A-HJ-NPR-Z0-9]{12}/mi';
                                    preg_match($regex, $value, $vinNumber);
                                    if (count($vinNumber)) {
                                        $this->addToArray(self::ATTR_PZU_CAR_VIN_PATH, $vinNumber[0]);
                                    }
                                    break;
                                case self::ATTR_PZU_CAR_DMC_KEY:
                                    $this->addToArray(self::ATTR_PZU_CAR_DMC_PATH, $value);
                                    break;
                                case self::ATTR_PZU_PEOPLE_COUNT_KEY:
//                                Liczba osób
                                    $this->addToArray(self::ATTR_PZU_PEOPLE_COUNT_PATH, (int)$value);
                                    break;
                                case self::ATTR_PZU_MAKE_MODEL_TYPE_REG_NUMBER_KEY:
//                                Marka / model, nr rejestracyjny
                                    if (strpos($value, ',') !== false) {

                                        $values = explode(',', $value);
                                    } elseif (strpos($value, ' ') !== false) {
                                        $values = explode(' ', $value);
                                    }
                                    if (isset($values[0]) && isset($values[1])) {

                                        $make = trim($values[0]);
                                        $model = trim($values[1]);
                                        $makeModelToCheck = $make . ' ' . explode(' ', $model)[0];
                                        $regNumber = trim($values[count($values) - 1]);
                                    } else {
                                        $makeModelToCheck = '';
                                        $regNumber = '';
                                    }

                                    $parameters = [
                                        [
                                            'key' => 'makeModel',
                                            'value' => $makeModelToCheck,
                                            'type' => PDO::PARAM_STR
                                        ]
                                    ];
                                    $query = "SELECT dbo.f_makeModelStandard(:makeModel,1) value";
                                    $result = $this->queryManager->executeProcedure($query, $parameters);
                                    if (count($result) > 0) {
                                        $makeModelFound = $result[0]['value'];
                                        $this->addToArray(self::ATTR_PZU_MAKE_MODEL_PATH, $makeModelFound);
                                    }
                                    if ($regNumber) {
                                        $this->addToArray(self::ATTR_PZU_REG_NUMBER_PATH, $regNumber);
                                    }
                                    break;
                                case self::ATTR_PZU_LOCATION_EXTRA_KEY:
                                    $this->addToArray(self::ATTR_PZU_LOCATION_EXTRA_PATH, $value);
                                    break;
                                default:
                                    break;
                            endswitch;
                            break;
                        }
                    } catch (\Exception $exception) {
                        $log = new Log();
                        $log->setName('Exception on PZU Mail Parsing - attribute');
                        $log->setContent($exception->getMessage());
                        $log->setParam1($key ?? '');
                        $log->setParam1($value ?? '');
                        $log->setCreatedAt(new \DateTime());
                        $em = $this->container->get('doctrine')->getEntityManager();
                        $em->persist($log);
                        $em->flush();
                    }
                }
            }
            $this->addToArray(self::ATTR_PZU_CASE_TEXT_PATH, $textToExcerpt);
        } catch (\Exception $exception) {
            $log = new Log();
            $log->setName('Exception on PZU Mail Parsing');
            $log->setContent($exception->getMessage());
            $em = $this->container->get('doctrine')->getEntityManager();
            $em->persist($log);
            $em->flush();
        }
        return $this->resultArray;
    }

    private function parseSubject($subject)
    {
        if (preg_match('/([a-zA-Z][a-zA-Z]\d+)/', $subject, $caseNumber) !== false) {
            if (strpos($caseNumber[0], 'ZA') !== false) {
                $foundPlatform = 88;
                $foundProgram = "767";
            } else {
                $foundPlatform = 87;
                $foundProgram = "768";
            }

            $this->addToArray(self::ATTR_PZU_CASE_NUMBER_PATH, $caseNumber[0]);
            $this->addToArray(self::ATTR_PLATFORM_PATH, $foundPlatform);
            $this->addToArray(self::ATTR_PROGRAM_PATH, $foundProgram);
            $this->addToArray(self::ATTR_PROGRAMS_PATH, $foundProgram);
        }

        return $caseNumber[0];
    }

    /** Create New case and run as pzu  */
    private function makeNewCase()
    {
        $processInstanceId = $this->processHandler->start('1011.001', NULL,
            1, 1);

        $value = $this->resultArray[self::ATTR_PLATFORM_PATH] ?? null;

        if ($processInstanceId) {
            $this->processHandler->editAttributeValue(self::ATTR_PLATFORM_PATH, $processInstanceId, $value, AttributeValue::VALUE_INT, Step::ALWAYS_ACTIVE, 1, 1);
            $this->processHandler->next($processInstanceId, 5);
        }

        return $processInstanceId;

    }

    private function saveAttributes($rootId)
    {
        foreach ($this->resultArray as $path => $value) {
            $path = (string)$path;
            switch ($path):
                case self::ATTR_PZU_PEOPLE_COUNT_PATH;
                case self::ATTR_PZU_MAKE_MODEL_PATH;
                case self::ATTR_PZU_TOWING_LIMIT_PATH;
                case self::ATTR_PZU_CAR_DMC_KEY;
                case self::ATTR_PLATFORM_PATH:
                    $type = AttributeValue::VALUE_INT;
                    break;
                case self::ATTR_PZU_DATE_PATH;
                case self::ATTR_PZU_CAR_DATE_PATH:
                    $type = AttributeValue::VALUE_DATE;
                    break;
                case self::ATTR_PZU_CASE_TEXT_PATH;
                case self::ATTR_PZU_LOCATION_EXTRA_PATH:
                    $type = AttributeValue::VALUE_TEXT;
                    break;
                default:
                    $type = AttributeValue::VALUE_STRING;
                    break;
            endswitch;
            $this->processHandler->editAttributeValue($path, $rootId, $value, $type, Step::ALWAYS_ACTIVE, 1, 1);
        }

    }

    private function addToArray($key, $value)
    {
        $this->resultArray[(string)$key] = $value;
    }

}