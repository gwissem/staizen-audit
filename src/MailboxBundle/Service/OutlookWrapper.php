<?php

namespace MailboxBundle\Service;


use Doctrine\ORM\EntityManager;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAllItemsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseFolderIdsType;
use jamesiarmes\PhpEws\Client as OutlookClient;
use jamesiarmes\PhpEws\Client;
use jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use jamesiarmes\PhpEws\Enumeration\ItemQueryTraversalType;
use jamesiarmes\PhpEws\Enumeration\MessageDispositionType;
use jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use jamesiarmes\PhpEws\Request\CreateItemType;
use jamesiarmes\PhpEws\Request\FindItemType;
use jamesiarmes\PhpEws\Type\BodyType;
use jamesiarmes\PhpEws\Type\ConstantValueType;
use jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use jamesiarmes\PhpEws\Type\EmailAddressType;
use jamesiarmes\PhpEws\Type\FieldURIOrConstantType;
use jamesiarmes\PhpEws\Type\ForwardItemType;
use jamesiarmes\PhpEws\Type\IndexedPageViewType;
use jamesiarmes\PhpEws\Type\IsEqualToType;
use jamesiarmes\PhpEws\Type\ItemIdType;
use jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use jamesiarmes\PhpEws\Type\MessageType;
use jamesiarmes\PhpEws\Type\PathToUnindexedFieldType;
use jamesiarmes\PhpEws\Type\RestrictionType;
use MailboxBundle\Entity\ConnectedMailbox;

class OutlookWrapper
{

    /**
     * NA RAZIE NIE UŻYWANE!
     */

    /** @var OutlookClient */
    private $client;

    /** @var  ConnectedMailbox */
    private $mailbox;

    /** @var  EntityManager */
    private $em;

    /**
     * MailboxOutlook constructor.
     * @param ConnectedMailbox $mailbox
     */
    public function __construct(ConnectedMailbox $mailbox)
    {

        $host = $mailbox->getHost();

        if ($host === 'mail.starter24.pl') {

            $username = $mailbox->getLogin();
            $password = $mailbox->getPassword();
            $version = Client::VERSION_2010;

            $this->client = new Client($host, $username, $password, $version);

        }

        $this->mailbox = $mailbox;

    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManger(EntityManager $entityManager) {

        $this->em = $entityManager;

    }

    /**
     * @param $messageId
     * @return MessageType|null
     */
    public function findEmailByMessageId($messageId)
    {

        $request = new FindItemType();

        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;

        $request->Traversal = ItemQueryTraversalType::SHALLOW;

        $request->ParentFolderIds = new NonEmptyArrayOfBaseFolderIdsType();
        $folder_id = new DistinguishedFolderIdType();
        $folder_id->Id = DistinguishedFolderIdNameType::SENT;
        $request->ParentFolderIds->DistinguishedFolderId[] = $folder_id;

        $request->IndexedPageItemView = new IndexedPageViewType();
        $request->IndexedPageItemView->BasePoint = "Beginning";
        $request->IndexedPageItemView->Offset = 0;
        $request->IndexedPageItemView->MaxEntriesReturned = 1;

        $request->Restriction = new RestrictionType();
        $request->Restriction->IsEqualTo = new IsEqualToType();
        $request->Restriction->IsEqualTo->FieldURI = new PathToUnindexedFieldType();
        $request->Restriction->IsEqualTo->FieldURI->FieldURI = 'message:InternetMessageId';
        $request->Restriction->IsEqualTo->FieldURIOrConstant = new FieldURIOrConstantType();
        $request->Restriction->IsEqualTo->FieldURIOrConstant->Constant = new ConstantValueType();
        $request->Restriction->IsEqualTo->FieldURIOrConstant->Constant->Value = $messageId;

        $response = $this->client->FindItem($request);

        $response_messages = $response->ResponseMessages->FindItemResponseMessage;

        foreach ($response_messages as $response_message) {

            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                return null;
            }

            $items = $response_message->RootFolder->Items->Message;
            return (!empty($items)) ? $items[0] : null;

        }

        return null;

    }

    public function forwardEmail($id, $changeKey, $to = '', $cc = '', $bcc = '', $extraBodyContent = '')
    {

        $forwardItem = new ForwardItemType();

        $forwardItem->ToRecipients = array();
        $this->addRecipients($forwardItem->ToRecipients, $to);

        $forwardItem->CcRecipients = array();
        $this->addRecipients($forwardItem->CcRecipients, $cc);

        $forwardItem->BccRecipients = array();
        $this->addRecipients($forwardItem->BccRecipients, $bcc);

        if (!empty($extraBodyContent)) {
            $forwardItem->NewBodyContent = new BodyType();
            $forwardItem->NewBodyContent->BodyType = BodyTypeType::TEXT;
            $forwardItem->NewBodyContent->_ = 'HTML Content Goes Here';
        }


        $forwardItem->ReferenceItemId = new ItemIdType();
        $forwardItem->ReferenceItemId->Id = $id;
        $forwardItem->ReferenceItemId->ChangeKey = $changeKey;

        $request = new CreateItemType();
        $request->Items = new NonEmptyArrayOfAllItemsType();
        $request->Items->Message->
        $request->Items->ForwardItem = [$forwardItem];
        $request->MessageDisposition = MessageDispositionType::SEND_AND_SAVE_COPY;
        $request->MessageDispositionSpecified = true;

        $response = $this->client->CreateItem($request);

    }

    /**
     * @param $target
     * @param $source
     */
    private function addRecipients(&$target, $source) {

        if(empty($source)) return;

        $source = str_replace(';', ',', $source);

        foreach (explode(',', $source) as $receiver) {

            if(!$receiver) continue;

            $recipient = new EmailAddressType();
            $recipient->EmailAddress = trim($receiver);
            $target[] = $recipient;

        }

    }
}
