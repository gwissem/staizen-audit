<?php

namespace MailboxBundle\Service;


use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use DocumentBundle\Uploader\FileUploader;
use DocumentBundle\Utils\HandlerExtraFiles;
use jamesiarmes\PhpEws\ArrayType\ArrayOfRecipientsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAllItemsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAttachmentsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseFolderIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseItemIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfItemChangeDescriptionsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfPathsToElementType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfRequestAttachmentIdsType;
use jamesiarmes\PhpEws\Client as OutlookClient;
use jamesiarmes\PhpEws\Client;
use jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use jamesiarmes\PhpEws\Enumeration\ItemQueryTraversalType;
use jamesiarmes\PhpEws\Enumeration\MessageDispositionType;
use jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use jamesiarmes\PhpEws\Request\CreateAttachmentType;
use jamesiarmes\PhpEws\Request\CreateItemType;
use jamesiarmes\PhpEws\Request\FindItemType;
use jamesiarmes\PhpEws\Request\GetAttachmentType;
use jamesiarmes\PhpEws\Request\GetItemType;
use jamesiarmes\PhpEws\Request\SendItemType;
use jamesiarmes\PhpEws\Request\UpdateItemType;
use jamesiarmes\PhpEws\Response\ItemInfoResponseMessageType;
use jamesiarmes\PhpEws\Type\BodyType;
use jamesiarmes\PhpEws\Type\ConstantValueType;
use jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use jamesiarmes\PhpEws\Type\EmailAddressType;
use jamesiarmes\PhpEws\Type\FieldURIOrConstantType;
use jamesiarmes\PhpEws\Type\FileAttachmentType;
use jamesiarmes\PhpEws\Type\ForwardItemType;
use jamesiarmes\PhpEws\Type\IndexedPageViewType;
use jamesiarmes\PhpEws\Type\IsEqualToType;
use jamesiarmes\PhpEws\Type\ItemChangeType;
use jamesiarmes\PhpEws\Type\ItemIdType;
use jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use jamesiarmes\PhpEws\Type\ItemType;
use jamesiarmes\PhpEws\Type\MessageType;
use jamesiarmes\PhpEws\Type\PathToIndexedFieldType;
use jamesiarmes\PhpEws\Type\PathToUnindexedFieldType;
use jamesiarmes\PhpEws\Type\ReplyAllToItemType;
use jamesiarmes\PhpEws\Type\RequestAttachmentIdType;
use jamesiarmes\PhpEws\Type\RestrictionType;
use jamesiarmes\PhpEws\Type\SetItemFieldType;
use jamesiarmes\PhpEws\Type\SingleRecipientType;
use jamesiarmes\PhpEws\Type\TargetFolderIdType;
use MailboxBundle\Entity\ConnectedMailbox;
use MailboxBundle\Entity\MailContainer;
use Symfony\Component\DependencyInjection\Container;
use ToolBundle\Utils\ToolQueryManager;

class OutlookProvider
{

    /** @var OutlookClient */
    private $client;

    /** @var  ConnectedMailbox */
    private $mailbox;

    /** @var  EntityManager */
    private $em;

    private $fileUploader;

    private $container;

    /** @var  ToolQueryManager */
    private $toolQueryManager;

    /**
     * MailboxOutlook constructor.
     * @param ConnectedMailbox $mailbox
     * @param FileUploader|null $fileUploader
     * @param EntityManager $entityManager
     * @param ToolQueryManager $toolQueryManager
     */
    public function __construct(ConnectedMailbox $mailbox = null, FileUploader $fileUploader = null, EntityManager $entityManager, ToolQueryManager $toolQueryManager, Container $container)
    {

        if ($mailbox) {
            $host = $mailbox->getHost();

            if ($host === 'mail.starter24.pl') {

                $username = $mailbox->getLogin();
                $password = $mailbox->getPassword();
                $version = Client::VERSION_2010;

                $this->client = new Client($host, $username, $password, $version);

            }

            $this->mailbox = $mailbox;
        }

        $this->container =$container;
        $this->fileUploader = $fileUploader;
        $this->em = $entityManager;
        $this->toolQueryManager = $toolQueryManager;

    }

    public function setUploader(FileUploader $fileUploader) {
        $this->fileUploader = $fileUploader;
    }

    public function getIdsOfUnseenEmails() {

        // Replace with the date range you want to search in. As is, this will find all
    // messages within the current calendar year.
        $start_date = new \DateTime('January 1 00:00:00');
        $end_date = new \DateTime('December 31 23:59:59');

        $request = new FindItemType();
        $request->ParentFolderIds = new NonEmptyArrayOfBaseFolderIdsType();
// Build the start date restriction.
//        $greater_than = new IsGreaterThanOrEqualToType();
//        $greater_than->FieldURI = new PathToUnindexedFieldType();
//        $greater_than->FieldURI->FieldURI = UnindexedFieldURIType::ITEM_DATE_TIME_RECEIVED;
//        $greater_than->FieldURIOrConstant = new FieldURIOrConstantType();
//        $greater_than->FieldURIOrConstant->Constant = new ConstantValueType();
//        $greater_than->FieldURIOrConstant->Constant->Value = $start_date->format('c');
//// Build the end date restriction;
//        $less_than = new IsLessThanOrEqualToType();
//        $less_than->FieldURI = new PathToUnindexedFieldType();
//        $less_than->FieldURI->FieldURI = UnindexedFieldURIType::ITEM_DATE_TIME_RECEIVED;
//        $less_than->FieldURIOrConstant = new FieldURIOrConstantType();
//        $less_than->FieldURIOrConstant->Constant = new ConstantValueType();
//        $less_than->FieldURIOrConstant->Constant->Value = $end_date->format('c');
//// Build the restriction.
//        $request->Restriction = new RestrictionType();
//        $request->Restriction->And = new AndType();
//        $request->Restriction->And->IsGreaterThanOrEqualTo = $greater_than;
//        $request->Restriction->And->IsLessThanOrEqualTo = $less_than;
//
// Return all message properties.

        // Limits the number of items retrieved
        $request->IndexedPageItemView = new IndexedPageViewType();
        $request->IndexedPageItemView->BasePoint = "Beginning";
        $request->IndexedPageItemView->Offset = 0; // Item number you want to start at
        $request->IndexedPageItemView->MaxEntriesReturned = 100; // Numer of items to return in

        $request->Restriction = new RestrictionType();
//        $request->Restriction->

        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ID_ONLY;
        $request->Traversal = ItemQueryTraversalType::SHALLOW;

        $request->Restriction = new RestrictionType();
        $request->Restriction->IsEqualTo = new IsEqualToType();
        $request->Restriction->IsEqualTo->FieldURI = new PathToUnindexedFieldType();
        $request->Restriction->IsEqualTo->FieldURI->FieldURI = 'message:IsRead';
        $request->Restriction->IsEqualTo->FieldURIOrConstant = new FieldURIOrConstantType();
        $request->Restriction->IsEqualTo->FieldURIOrConstant->Constant = new ConstantValueType();
        $request->Restriction->IsEqualTo->FieldURIOrConstant->Constant->Value = "false";


        $folder_id = new DistinguishedFolderIdType();
        $folder_id->Id = DistinguishedFolderIdNameType::INBOX;

        $request->ParentFolderIds->DistinguishedFolderId[] = $folder_id;
        $response = $this->client->FindItem($request);

        $response_messages = $response->ResponseMessages->FindItemResponseMessage;
        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(
                    STDERR,
                    "Failed to search for messages with \"$code: $message\"\n"
                );
                continue;
            }
            // Iterate over the messages that were found, printing the subject for each.
            $items = $response_message->RootFolder->Items->Message;

            return $items;

        }

        return null;

    }


    public function runMailbox()
    {

        $mailboxResponse = new MailboxResponse($this->mailbox);

        try {

            $this->processMailbox($mailboxResponse);

        } catch (\Exception $e) {
            $mailboxResponse->setError($e);
        } finally {
            return $mailboxResponse;
        }

    }

    private function processMailbox(MailboxResponse $mailboxResponse)
    {


        /** @var MessageType[] $emails */
        $emails = $this->getIdsOfUnseenEmails();

        foreach ($emails as $email) {

            $response = $this->processEmail($email);

            if ($response === true) {
                $mailboxResponse->increment();
            } else {
                $mailboxResponse->setError($response);
            }

        }

        return $mailboxResponse;
    }

    public function getFullMessageType(MessageType $email)
    {


        /** Save E-mail in database and as File */

        $request = new GetItemType();
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
        $request->ItemShape->IncludeMimeContent = true;
        $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();

        $item = new ItemIdType();
        $item->Id = $email->ItemId->Id;
        $request->ItemIds->ItemId[] = $item;
        $response = $this->client->GetItem($request);

        $response_messages = $response->ResponseMessages->GetItemResponseMessage;

        $items = [];

        foreach ($response_messages as $response_message) {

            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                return null;
            }

            /** @var \jamesiarmes\PhpEws\Type\MessageType $item */
            foreach ($response_message->Items->Message as $item) {

                $items[] = $item;

            }
        }

        if (count($items) === 1) {
            return $items[0];
        }

        return $items;

    }

    public function saveMessageOnDisc(MessageType $item)
    {

        /** Zapisanie e-maila do cached_email */
        $nameFile = uniqid() . '.eml';
        $pathFile = MailboxOutgoingEmailService::EMAILS_CACHE_DIR . '/' . $nameFile;

        file_put_contents($pathFile, base64_decode($item->MimeContent->_));

        $originalName = 'email_body_in_' . $this->mailbox->getId() . '_' . uniqid() . '.eml';

        $mailContainer = new MailContainer();
        $mailContainer->setSender($item->Sender->Mailbox->EmailAddress);
        $mailContainer->setSubject($item->Subject);
        $mailContainer->setMailbox($this->mailbox);
        $mailContainer->setEmailId(null);

        $document = new Document();
        $document->setName($this->mailbox->getHost() . '_' . date('Ymd_His'));
        $this->em->persist($document);

        /** Przeniesienie e-maila do files */
        $mailFile = $this->fileUploader->upload(new MailContainerFile($pathFile, $originalName));
        $mailFile->setDocument($document);

        $mailContainer->setMailFile($mailFile);

        $this->em->persist($mailFile);
        $this->em->persist($mailContainer);
        $this->em->flush();

        return $mailContainer;

    }


    /**
     * @param MessageType $email
     * @return bool|\Exception
     */
    public function processEmail(MessageType $email)
    {


        /** Save E-mail in database and as File */

        $request = new GetItemType();
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
        $request->ItemShape->IncludeMimeContent = true;
        $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();

        $item = new ItemIdType();
        $item->Id = $email->ItemId->Id;
        $request->ItemIds->ItemId[] = $item;
        $response = $this->client->GetItem($request);

        $response_messages = $response->ResponseMessages->GetItemResponseMessage;

        try {
            foreach ($response_messages as $response_message) {

                if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {

                    /** NIE WCZYTANO EMAILA */

//                $code = $response_message->ResponseCode;
//                $message = $response_message->MessageText;
//                fwrite(STDERR, "Failed to get message with \"$code: $message\"\n");
//                continue;

                }

                /** @var \jamesiarmes\PhpEws\Type\MessageType $item */
                foreach ($response_message->Items->Message as $item) {

                    /** Sprawdzenie czy w temacie występuje numer sprawy */
                    if ($this->mailbox->getFilterSubject()) {

                        if (!$this->checkSubject($item->Subject)) {
                            continue;
                        }

                    }

                    /** Zapisanie e-maila do cached_email */
                    $nameFile = uniqid() . '.eml';
                    $pathFile = MailboxOutgoingEmailService::EMAILS_CACHE_DIR . '/' . $nameFile;
                    file_put_contents($pathFile, base64_decode($item->MimeContent->_));
                    $originalName = 'email_body_in_' . $this->mailbox->getId() . '_' . uniqid() . '.eml';

                    $mailContainer = new MailContainer();
                    $mailContainer->setSender($item->Sender->Mailbox->EmailAddress);
                    $mailContainer->setSubject($item->Subject);
                    $mailContainer->setMailbox($this->mailbox);
                    $mailContainer->setEmailId(null);

                    $document = new Document();
                    $document->setName($this->mailbox->getHost() . '_' . date('Ymd_His'));
                    $this->em->persist($document);

                    /** Przeniesienie e-maila do files */
                    $mailFile = $this->fileUploader->upload(new MailContainerFile($pathFile, $originalName));
                    $mailFile->setDocument($document);

                    $mailContainer->setMailFile($mailFile);

                    $this->em->persist($mailFile);
                    $this->em->persist($mailContainer);
                    $this->em->flush();

                    $parsedQuery = '';

//                    PZU - Wyciąganie zakładanie sprawy i wyciąganie danych;
                    if($this->mailbox->getName() ==='pzu@starter24.pl' || $this->mailbox->getName() ==='pzu.testowy@starter24.pl' )
                    {
                        $PZUParserService = $this->container->get('atlas.mailbox.pzu_parser.service');
                        $PZUParserService->makeCase($item->Subject,$item->Body->_);
                    }

                    try {
                        $resultProcedure = $this->runProcedure($item, $document->getId(), $parsedQuery);
                    } catch (\Exception $e) {
                        $parsedQuery = 'Error in execute procedure';
                    }

                    $mailContainer->setProcedureQuery($parsedQuery);

                    $this->markAsRead($item);

                    /** Zapisanie wszystkiego w bazie */
                    $this->em->persist($mailContainer);
                    $this->em->flush();

//                    if($resultProcedure == 1) {
//                        /** Jeżeli wynik procedury 1 to usuwa e-maila ze skrzynki */
//                        $phpImapMailbox->deleteMail($mail->id);
//                    }
//                    elseif($resultProcedure == 0)
//                    {
//                        /** Jeżeli 0 to nie usuwa. */
//                    }

                }

            }


        } catch (\Exception $e) {
            return $e;
        }

        return true;
    }

    /**
     * @param $subject
     * @return bool
     */
    private function checkSubject($subject)
    {

        preg_match(MailboxManagementService::REGEX_NUMBER_CASE_ATLAS, $subject, $matches);
        return (!empty($matches));

    }

    public function markAsRead($item)
    {

        $request = new UpdateItemType();
        $request->MessageDisposition = 'SaveOnly';
        $request->ConflictResolution = 'AlwaysOverwrite';
        $request->ItemChanges = [];

        $change = new ItemChangeType();
        $change->ItemId = new ItemIdType();
        $change->ItemId->Id = $item->ItemId->Id;
        $change->ItemId->ChangeKey = $item->ItemId->ChangeKey;
        $change->Updates = new NonEmptyArrayOfItemChangeDescriptionsType();
        $change->Updates->SetItemField = array();

        $field = new SetItemFieldType();
        $field->FieldURI = new PathToUnindexedFieldType();
        $field->FieldURI->FieldURI = 'message:IsRead';
        $field->Message = new MessageType();
        $field->Message->IsRead = true;

        $change->Updates->SetItemField[] = $field;
        $request->ItemChanges[] = $change;

        $this->client->UpdateItem($request);

    }

    private function parseEmail(\jamesiarmes\PhpEws\Type\MessageType $mail, $documentId = null)
    {

        $recipients = $mail->ToRecipients->Mailbox;

        if (is_array($recipients)) {

            $receivers = [];

            /** @var \jamesiarmes\PhpEws\Type\EmailAddressType $recipient */
            foreach ($recipients as $recipient) {
                $receivers[] = $recipient->EmailAddress;
            }

            $to = implode(',', $receivers);
        } else {
            $to = '';
        }

        $parsedMail = [
            'date' => $mail->DateTimeCreated,
            'subject' => $mail->Subject,
            'fromName' => $mail->From->Mailbox->Name,
            'fromAddress' => $mail->From->Mailbox->EmailAddress,
            'toString' => $to,
            'textPlain' => $mail->Body->_,
            'documentId' => $documentId,
            'caseNumber' => MailboxManagementService::tryGetCaseNumber($mail->Subject)
        ];

        return $parsedMail;
    }

    private function runProcedure($mail, $documentId, &$parsedQuery = null)
    {

        $query = $this->mailbox->getProcedureText();

        $resp = $this->toolQueryManager->executeCustomQuery($query, false, $this->mailbox->getConnectionString(), true, $this->parseEmail($mail, $documentId), null, $parsedQuery);

        if (is_array($resp) && isset($resp['error'])) {
            if ($resp['error'] == 1 || $resp['error'] == "1") {
                return 1;
            }
        }

        return 0;

    }

    private function saveAttachments(ItemInfoResponseMessageType $response_message)
    {

        $nameFile = uniqid() . '.eml';
        $fileDestination = MailboxManagementService::PATH_EMAILS_CACHE_DIR . '/' . $nameFile;

        $fileAttachments = array();
        $itemAttachments = array();

        foreach ($response_message->Items->Message as $item) {
            // If there are no attachments for the item, move on to the next
            // message.
            if (empty($item->Attachments)) {
                continue;
            }

            if (!empty($item->Attachments->FileAttachment)) {
                foreach ($item->Attachments->FileAttachment as $attachment) {
                    $fileAttachments[] = $attachment->AttachmentId->Id;
                }
            }

            if (!empty($item->Attachments->ItemAttachment)) {
                foreach ($item->Attachments->ItemAttachment as $attachment) {
                    $itemAttachments[] = $attachment->AttachmentId->Id;
                }
            }

        }


        if (!empty($fileAttachments)) {

            $request = new GetAttachmentType();
            $request->AttachmentIds = new NonEmptyArrayOfRequestAttachmentIdsType();

            foreach ($fileAttachments as $attachment_id) {
                $id = new RequestAttachmentIdType();
                $id->Id = $attachment_id;
                $request->AttachmentIds->AttachmentId[] = $id;
            }

            $response = $this->client->GetAttachment($request);

            $attachment_response_messages = $response->ResponseMessages->GetAttachmentResponseMessage;

            foreach ($attachment_response_messages as $attachment_response_message) {

                if ($attachment_response_message->ResponseClass != ResponseClassType::SUCCESS) {

                    /** Nie udało się załadować e-maila */

//                    $code = $response_message->ResponseCode;
//                    $message = $response_message->MessageText;
//                    fwrite(
//                        STDERR,
//                        "Failed to get attachment with \"$code: $message\"\n"
//                    );
//                    continue;

                }
                // Iterate over the file attachments, saving each one.
                $attachments = $attachment_response_message->Attachments->FileAttachment;

                foreach ($attachments as $attachment) {
//                    $path = "$file_destination/" . $attachment->Name;
//                    file_put_contents($path, $attachment->Content);

                }
            }

        }

    }

}
