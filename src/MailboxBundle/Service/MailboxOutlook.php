<?php

namespace MailboxBundle\Service;

use CaseBundle\Utils\TempFileHandler;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use DocumentBundle\Uploader\FileUploader;
use DocumentBundle\Utils\HandlerExtraFiles;
use jamesiarmes\PhpEws\ArrayType\ArrayOfRecipientsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAllItemsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAttachmentsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseFolderIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseItemIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfItemChangeDescriptionsType;
use jamesiarmes\PhpEws\Client as OutlookClient;
use jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use jamesiarmes\PhpEws\Enumeration\ConflictResolutionType;
use jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use jamesiarmes\PhpEws\Enumeration\ItemQueryTraversalType;
use jamesiarmes\PhpEws\Enumeration\MessageDispositionType;
use jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use jamesiarmes\PhpEws\Enumeration\UnindexedFieldURIType;
use jamesiarmes\PhpEws\Request\CreateAttachmentType;
use jamesiarmes\PhpEws\Request\CreateItemType;
use jamesiarmes\PhpEws\Request\FindItemType;
use jamesiarmes\PhpEws\Request\GetItemType;
use jamesiarmes\PhpEws\Request\SendItemType;
use jamesiarmes\PhpEws\Request\UpdateItemType;
use jamesiarmes\PhpEws\Type\BodyType;
use jamesiarmes\PhpEws\Type\ConstantValueType;
use jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use jamesiarmes\PhpEws\Type\EmailAddressType;
use jamesiarmes\PhpEws\Type\FieldURIOrConstantType;
use jamesiarmes\PhpEws\Type\FileAttachmentType;
use jamesiarmes\PhpEws\Type\ForwardItemType;
use jamesiarmes\PhpEws\Type\IndexedPageViewType;
use jamesiarmes\PhpEws\Type\IsEqualToType;
use jamesiarmes\PhpEws\Type\ItemChangeType;
use jamesiarmes\PhpEws\Type\ItemIdType;
use jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use jamesiarmes\PhpEws\Type\ItemType;
use jamesiarmes\PhpEws\Type\MessageType;
use jamesiarmes\PhpEws\Type\PathToUnindexedFieldType;
use jamesiarmes\PhpEws\Type\RestrictionType;
use jamesiarmes\PhpEws\Type\SetItemFieldType;
use jamesiarmes\PhpEws\Type\SingleRecipientType;
use jamesiarmes\PhpEws\Type\TargetFolderIdType;
use MailboxBundle\Entity\ConnectedMailbox;
use PhpMimeMailParser\Attachment;
use Symfony\Component\Filesystem\Filesystem;

class MailboxOutlook
{

    private $client;

    /**
     * array:11 [
            "id" => ID NOTATKI
            "to" => "odbiorca"
            "shortContent" => "krótka treść notatki"
            "eml" => ID document ENTITY (.eml file)
            "direction" =>  (in (2) / out (1))
            "subject" => ""
            "dw" => ""
            "udw" => ""
            "status" => "pending / sent / error"
            "from" => "nadawca"
            "files" => "załączniki"
            "body" => "Tresć emaila"
        ]

     */

    private $dataEmail;

    /** @var  ConnectedMailbox */
    private $mailbox;

    /** @var FileUploader */
    private $uploader;

    /** @var  EntityManager */
    private $em;

    /** @var  MailboxOutgoingEmailService */
    private $mailboxOutgoinEmailService;

    /**
     * MailboxOutlook constructor.
     * @param MailboxOutgoingEmailService $mailboxOutgoingEmailService
     * @param OutlookClient $client
     * @param $mailbox
     * @param $dataEmail
     */
    public function __construct(MailboxOutgoingEmailService $mailboxOutgoingEmailService, OutlookClient $client, $mailbox, $dataEmail)
    {
        $this->client = $client;
        $this->mailbox = $mailbox;
        $this->dataEmail = $dataEmail;
        $this->mailboxOutgoinEmailService = $mailboxOutgoingEmailService;
    }

    /**
     * @param bool $addInlineAttach
     * @return array|bool
     */
    public function send($addInlineAttach = true) {

        $requestCreateMessage = $this->createMessageRequest();

        $createResponse = $this->createMessage($requestCreateMessage);

        if(!$createResponse['success']) {

            return [
                'success' => false,
                'error_message' => 'createMessage() : ' . $createResponse['error_message']
            ];

        }

        if($addInlineAttach) {

            // Przed zapisaniem HTML'a, wstawić inline attachment
            $response = $this->tryAddInlineAttachment($createResponse['messages']);

            if(!$response['success']) {
                return $response;
            }

        }
        else {
            $response = $createResponse;
        }

        $attachResponse = $this->addAttachments($response['messages']);

        $documentId = $this->saveEmailToEml($attachResponse['messages']);

        $response = $this->sendEmail($attachResponse['messages']);

        if($response !== true) {
            return [
                'success' => false,
                'error_message' => 'sendEmail() : ' . $response['error_message']
            ];
        }

        return [
            'success' => true,
            'documentId' => $documentId
        ];

    }

    /**
     * @return array|bool
     */
    public function forward() {

        return $this->send(false);

    }

    /**
     * @param $messages
     * @return array
     */
    private function addAttachments($messages) {

        $firstMessage = $messages[0];

        // ZAŁĄCZNIKI

        if($this->dataEmail['files']) {

            $request = new CreateAttachmentType();
            $request->ParentItemId = new ItemIdType();
            $request->ParentItemId->Id = $firstMessage['id'];
            $request->Attachments = new NonEmptyArrayOfAttachmentsType();

            if(is_array($this->dataEmail['files'])) {

                foreach ($this->dataEmail['files'] as $item) {
                    if($item instanceof Attachment) {

                        $attachment = new FileAttachmentType();
                        $attachment->ContentType = $item->getContentType();
                        $attachment->Name = $item->getFilename();
                        $attachment->Content = $item->getContent();

                        if($item->getContentDisposition() === "inline") {

                            $attachment->IsInline = true;
                            $attachment->ContentId = $item->getFilename();

                        }

                        $request->Attachments->FileAttachment[] = $attachment;

                    }
                }

            }
            else {

                foreach (explode(',', $this->dataEmail['files']) as $item) {

                    $pathInfo = null;
                    $item = trim($item);

                    /** Extra dynamiczny plik jako PDF */
                    if(strpos($item,'{') !== false) {

                        $extraFile = str_replace(['{', '}'], '', $item);

                        // SOURCE::ID|FILENAME::NAME::WITH_PARSE
                        $options = explode('::', $extraFile);

                        $pathInfo = [
                            'name' => (isset($options[2])) ? ($options[2] . '.pdf') : null,
                            'path' => HandlerExtraFiles::getPathDynamicFile($this->mailboxOutgoinEmailService, $options, $this->dataEmail)
                        ];

                    } /** Extra statyczny plik */
                    else if(strpos($item,'[') !== false) {

                        $nameFile = str_replace(['[', ']'], '', $item);

                        $pathInfo = [
                            'name' => null,
                            'path' => HandlerExtraFiles::getPathExtraFile($nameFile)
                        ];

                    }
                    else {

                        $document = $this->em->getRepository('DocumentBundle:Document')->find(intval($item));

                        if($document instanceof Document) {

                            $pathInfo = new ArrayCollection();

                            /** @var File $documentFile */
                            foreach ($document->getFiles() as $documentFile) {

                                $pathInfo->add([
                                    'name' => $documentFile->getName(),
                                    'path' => MailboxOutgoingEmailService::FILES_EML_DIR . $documentFile->getPath()
                                ]);
                            }

                        }

                    }

                    if($pathInfo instanceof ArrayCollection) {

                        foreach ($pathInfo as $pathItem) {

                            if(empty($pathItem['path'])) continue;

                            $file = new \SplFileObject($pathItem['path']);
                            $finfo = finfo_open();

                            $attachment = new FileAttachmentType();
                            $attachment->Content = $file->openFile()->fread($file->getSize());
                            $attachment->Name = (!empty($pathItem['name'])) ? ($pathItem['name']) : $file->getBasename();
                            $attachment->ContentType = finfo_file($finfo, $pathItem['path']);
                            $request->Attachments->FileAttachment[] = $attachment;

                        }

                    }
                    else if($pathInfo !== null && !empty($pathInfo['path'])) {

                        $file = new \SplFileObject($pathInfo['path']);
                        $finfo = finfo_open();

                        $attachment = new FileAttachmentType();
                        $attachment->Content = $file->openFile()->fread($file->getSize());
                        $attachment->Name = (!empty($pathInfo['name'])) ? ($pathInfo['name']) : $file->getBasename();
                        $attachment->ContentType = finfo_file($finfo, $pathInfo['path']);
                        $request->Attachments->FileAttachment[] = $attachment;
                    }

                }

            }

            $response = $this->client->CreateAttachment($request);

            $newId = null;
            $newChangeKey = null;

            $response_messages = $response->ResponseMessages
                ->CreateAttachmentResponseMessage;
            foreach ($response_messages as $response_message) {

                if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                    $code = $response_message->ResponseCode;
                    $message = $response_message->MessageText;

                    return [
                        'success' => false,
                        'error_message' => "addAttachments() - " . $code . ": " . $message
                    ];

                }

                foreach ($response_message->Attachments->FileAttachment as $attachment) {
                    $newId = $attachment->AttachmentId->RootItemId;
                    $newChangeKey = $attachment->AttachmentId->RootItemChangeKey;
                }
            }

            $firstMessage['id'] = $newId;
            $firstMessage['changeKey'] = $newChangeKey;

        }

        return [
            'success' => true,
            'messages' => [
                0 => $firstMessage
            ]
        ];

    }

    private function sendEmail($messages) {

        $requestSendItems = $this->createSendItem($messages);
        $response = $this->client->SendItem($requestSendItems);
        $response_messages = $response->ResponseMessages->SendItemResponseMessage;

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {

                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;

                return [
                    'success' => false,
                    'error_message' => "Message failed to create with " . $code . ": " . $message
                ];

            }
        }

        return true;
    }

    /**
     * @param $messages
     * @return array|mixed|null
     */
    private function saveEmailToEml($messages) {

        $documentId = null;

        // PRZED wysłaniem, jest zapisywany gotowy eml na dysku

        foreach ($messages as $item) {

            $request = new GetItemType();

            $request->ItemShape = new ItemResponseShapeType();
            $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
            $request->ItemShape->IncludeMimeContent = true;

            $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();
            $request->ItemIds->ItemId = new ItemIdType();
            $request->ItemIds->ItemId->Id = $item['id'];

            $response = $this->client->GetItem($request);

            foreach ($response->ResponseMessages->GetItemResponseMessage as $response_message) {

                if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {

                    $code = $response_message->ResponseCode;
                    $message = $response_message->MessageText;

                    return [
                        'success' => false,
                        'error_message' => "Message failed to create with " . $code . ": " . $message
                    ];

                }

                /** @var \jamesiarmes\PhpEws\Type\MessageType $item */
                foreach ($response_message->Items->Message as $item) {

                    $document = $this->saveFile(base64_decode($item->MimeContent->_));
                    $documentId = $document->getId();

                }
            }

        }

        return $documentId;

    }

    /**
     * @param $fileContent
     * @return Document
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function saveFile($fileContent) {

        $nameFile = uniqid().'.eml';
        $pathFile = MailboxOutgoingEmailService::EMAILS_CACHE_DIR . '/' . $nameFile;

        /** Zapisanie e-maila do cached_email */
        file_put_contents($pathFile, $fileContent);

        $originalName = 'email_body_out_'. $this->mailbox->getId(). '_'.$this->dataEmail['id'].'.eml';

        $document = new Document();
        $document->setName($this->mailbox->getHost() . '_'.date('Ymd_His'));
        $this->em->persist($document);

        /** Przeniesienie e-maila do files */
        $mailFile = $this->uploader->upload(new MailContainerFile($pathFile, $originalName));
        $mailFile->setDocument($document);

        $this->em->persist($mailFile);
        $this->em->flush();

        return $document;

    }

    /**
     * @param $requestCreateMessage
     * @return array
     */
    private function createMessage($requestCreateMessage) {

        $response = $this->client->CreateItem($requestCreateMessage);

        $response_messages = $response->ResponseMessages->CreateItemResponseMessage;

        $messagesForSend = [];

        // ZAKŁADAM, ŻE WYSYŁA 1 E-maila !
        foreach ($response_messages as $response_message) {

            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {

                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;

                return [
                    'success' => false,
                    'error_message' => "Message failed to create with " . $code . ": " . $message
                ];

            }

            foreach ($response_message->Items->Message as $item) {

                $messagesForSend[] = [
                    'id' => $item->ItemId->Id,
                    'changeKey' => $item->ItemId->ChangeKey
                ];

            }
        }

        return [
            'success' => true,
            'messages' => $messagesForSend
        ];

    }

    /**
     * @return CreateItemType
     */
    private function  createMessageRequest() {

        // Stworzenie Requesta
        $request = new CreateItemType();
        $request->Items = new NonEmptyArrayOfAllItemsType();
        $request->MessageDisposition = MessageDispositionType::SAVE_ONLY;

        // Katalog gdzie ma się zapisać - w wysłanych
        $send_folder = new TargetFolderIdType();
        $send_folder->DistinguishedFolderId = new DistinguishedFolderIdType();
        $send_folder->DistinguishedFolderId->Id = DistinguishedFolderIdNameType::SENT;
        $request->SavedItemFolderId = $send_folder;

        // Tworzenie wiadomości
        $message = new MessageType();

        $message->IsRead = false;
        $message->Subject = $this->dataEmail['subject'];

        $message->From = new SingleRecipientType();
        $message->From->Mailbox = new EmailAddressType();
        $message->From->Mailbox->EmailAddress = $this->dataEmail['from'];

        $message->ToRecipients = new ArrayOfRecipientsType();

        // Domyślnie e-maile były oddzielane przecinkami... średnikami też powinny, także mały fix:
        $this->dataEmail['to'] = str_replace(';', ',', $this->dataEmail['to']);

        // Odbiorcy
        foreach (explode(',', $this->dataEmail['to']) as $receiver) {

            if(!$receiver) continue;

            $recipient = new EmailAddressType();
            $recipient->EmailAddress = trim($receiver);
            $message->ToRecipients->Mailbox[] = $recipient;

        }

        if($this->dataEmail['dw']) {
            $message->CcRecipients = new ArrayOfRecipientsType();

            $this->dataEmail['dw'] = str_replace(';', ',', $this->dataEmail['dw']);

            // DW
            foreach (explode(',', $this->dataEmail['dw']) as $receiver) {

                if(!$receiver) continue;

                $recipient = new EmailAddressType();
                $recipient->EmailAddress = trim($receiver);
                $message->CcRecipients->Mailbox[] = $recipient;

            }
        }

        if($this->dataEmail['udw']) {
            $message->BccRecipients = new ArrayOfRecipientsType();

            $this->dataEmail['udw'] = str_replace(';', ',', $this->dataEmail['udw']);

            // DW
            foreach (explode(',', $this->dataEmail['udw']) as $receiver) {

                if(!$receiver) continue;

                $recipient = new EmailAddressType();
                $recipient->EmailAddress = trim($receiver);
                $message->BccRecipients->Mailbox[] = $recipient;

            }
        }

        // Zawartość e-maila

        $message->Body = new BodyType();
        $message->Body->BodyType = BodyTypeType::HTML;
        $message->Body->_ = $this->dataEmail['body'];

        $request->Items->Message[] = $message;

        return $request;
    }

    /**
     * @param $id
     * @param $changedKey
     * @param $html
     * @return ItemIdType
     */
    private function updateBody($id, $changedKey, $html) {

        $request = new UpdateItemType();
        $request->ConflictResolution = ConflictResolutionType::ALWAYS_OVERWRITE;
        $request->MessageDisposition = MessageDispositionType::SAVE_ONLY;
        $request->ItemChanges = array();

        $change = new ItemChangeType();
        $change->ItemId = new ItemIdType();
        $change->ItemId->Id = $id;
        $change->ItemId->ChangeKey = $changedKey;
        $change->Updates = new NonEmptyArrayOfItemChangeDescriptionsType();
        $change->Updates->SetItemField = array();

        $field = new SetItemFieldType();
        $field->Item = new ItemType();
        $field->FieldURI = new PathToUnindexedFieldType();
        $field->FieldURI->FieldURI = UnindexedFieldURIType::ITEM_BODY;

        $body = new BodyType();
        $body->BodyType = BodyTypeType::HTML;
        $body->_ = $html;

        $field->Item->Body = $body;
        $change->Updates->SetItemField[] = $field;
        $request->ItemChanges[] = $change;

        $response = $this->client->UpdateItem($request);
        $response_messages = $response->ResponseMessages->UpdateItemResponseMessage;

        foreach ($response_messages as $response_message) {

            foreach ($response_message->Items->Message as $item) {

                return $item->ItemId;

            }

        }

        return null;

    }


    /**
     * @param $messages
     * @param int $count
     * @return array
     */
    private function tryAddInlineAttachment($messages, &$count = 0) {

        $kernelPath = $this->mailboxOutgoinEmailService->container->getParameter('kernel.project_dir');

        $fs = new Filesystem();

        /** @var MessageType $firstMessage */
        $firstMessage = $messages[0];

        $tempBody = $this->dataEmail['body'];

        $re = '/\<[img].+ src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/m';

        $messagesResult = [];

        try {

            preg_match_all($re, $tempBody, $matches, PREG_SET_ORDER, 0);

            $currentInlineAttachment = [];

            $count = count($matches);

            if($count == 0) {
                return [
                    'success' => true,
                    'messages' => $messages
                ];
            }

            $request = new CreateAttachmentType();
            $request->ParentItemId = new ItemIdType();
            $request->ParentItemId->Id = $firstMessage['id'];
            $request->Attachments = new NonEmptyArrayOfAttachmentsType();

            foreach ($matches as $match) {

                $source = strtok($match[1], '?');

                if(!TempFileHandler::strposa($source, ['http','https'])) {

                    // Wiadomo, że jest to attachment z naszego dysku  - potem chyba można wywalić
                    if(
                        strpos($source, 'email_attachments') !== FALSE
                    ) {
                        $source = $kernelPath . '/web' . (str_replace('..', "", $source));
                    }
                    // Prawdopodobnie jest to plik z naszego dysku
                    elseif(substr($source, 0,3) === "../") {

                        $source = $kernelPath . '/web' . (str_replace('..', "", $source));

                        if(!$fs->exists([$source])) {
                            continue;
                        }

                    }
                    else {
                        continue;
                    }

                }

                $tempFile = TempFileHandler::fromUrl($source);

                if(!key_exists($tempFile->fileName, $currentInlineAttachment)) {

                    $request->Attachments->FileAttachment[] = $this->createAttachmentInline($tempFile);
                    $currentInlineAttachment[$tempFile->fileName] = $tempFile;

                }

                $tempBody = str_replace($match[1],"cid:" . $tempFile->fileName, $tempBody);

            }

            /**
             * Sprawdzenie, czy jakiekolwiek pliki udało sie dodać inlinegit
             */
            if(empty($request->Attachments->FileAttachment)) {
                return [
                    'success' => true,
                    'messages' => $messages
                ];
            }

            $response = $this->client->CreateAttachment($request);

            $response_messages = $response->ResponseMessages
                ->CreateAttachmentResponseMessage;

            foreach ($response_messages as $response_message) {

                if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                    $code = $response_message->ResponseCode;
                    $message = $response_message->MessageText;

                    return [
                        'success' => false,
                        'error_message' => "tryAddInlineAttachment() - " . $code . ": " . $message
                    ];

                }

                foreach ($response_message->Attachments->FileAttachment as $attachment) {

                    $messagesResult[] = [
                        'id' => $attachment->AttachmentId->RootItemId,
                        'changeKey' => $attachment->AttachmentId->RootItemChangeKey
                    ];

                }
            }

            $first = $messagesResult[0];

            $messagesResult = [];

            $itemType = $this->updateBody($first['id'], $first['changeKey'], $tempBody);

            $messagesResult[] = [
                'id' => $itemType->Id,
                'changeKey' => $itemType->ChangeKey
            ];

        }
        catch (\Exception $exception) {

            return [
                'success' => false,
                'error_message' => "tryAddInlineAttachment() - " . $exception->getCode() . ": " . $exception->getMessage() . ", " . $exception->getFile() . " " . $exception->getLine()
            ];

        }

        return [
            'success' => true,
            'messages' => $messagesResult
        ];

    }

    private function createAttachmentInline(TempFileHandler $tempFile) {

        // Build the file attachment.
        $attachment = new FileAttachmentType();
        $attachment->IsInline = true;
        $attachment->Content = $tempFile->getContent();
        $attachment->Name = $tempFile->fileName;
        $attachment->ContentType = $tempFile->getContentType();
        $attachment->ContentId = $tempFile->fileName;

        return $attachment;

    }

    private function createSendItem($messages) {

        $request = new SendItemType();
        $request->SaveItemToFolder = true;
        $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();

        foreach ($messages as $message) {
            $item = new ItemIdType();
            $item->Id = $message['id'];
            $item->ChangeKey = $message['changeKey'];
            $request->ItemIds->ItemId[] = $item;
        }

        $send_folder = new TargetFolderIdType();
        $send_folder->DistinguishedFolderId = new DistinguishedFolderIdType();
        $send_folder->DistinguishedFolderId->Id = DistinguishedFolderIdNameType::SENT;

        $request->SavedItemFolderId = $send_folder;

        return $request;

    }

    /**
     * @return OutlookClient
     */
    public function getClient(): OutlookClient
    {
        return $this->client;
    }

    /**
     * @param OutlookClient $client
     */
    public function setClient(OutlookClient $client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getDataEmail()
    {
        return $this->dataEmail;
    }

    /**
     * @param mixed $dataEmail
     */
    public function setDataEmail($dataEmail)
    {
        $this->dataEmail = $dataEmail;
    }

    /**
     * @param mixed $uploader
     */
    public function setUploader($uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * @param EntityManager $em
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;
    }


}
