<?php

namespace MailboxBundle\Service;


use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use DocumentBundle\Uploader\FileUploader;
use Elastica\Response;
use jamesiarmes\PhpEws\Type\MessageType;
use MailboxBundle\Entity\ConnectedMailbox;
use MailboxBundle\Entity\MailContainer;
use PhpImap\IncomingMail;
use PhpImap\Mailbox;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use ToolBundle\Utils\ToolQueryManager;

class MailboxManagementService
{

    const CONFIG_FILE = __DIR__ . '/../../../app/Resources/cron/incoming_email.yml';
    const ATTACHMENT_OF_EMAILS_PATH = '/../web/mailbox_attachments';
    const EMAILS_ATTACHMENT_PATH = '/../web/files/email_attachments';
    const EMAILS_CACHE_DIR = '/../web/cached_emails';
    const FILES_EML_DIR = '/../web/files';
    const REGEX_NUMBER_CASE_PZ = '/U[0-9]{4,10}/';
    const REGEX_NUMBER_CASE_ATLAS = '/A[0-9]{8,10}/';

    const PATH_EMAILS_CACHE_DIR = __DIR__ . '/../..' . self::EMAILS_CACHE_DIR;

    private $attachmentOfEmailsPath;
    private $cachedEmailsPath;

    /** @var  EntityManager */
    private $em;

    /** @var  ContainerInterface $container */
    private $container;

    /** @var  MailboxResponse[] $mailboxResponseList */
    private $mailboxResponseList;

    /** @var  TwigEngine $templating */
    private $templating;

    /** @var  FileUploader */
    private $fileUploader;

    /** @var  ToolQueryManager */
    private $toolQueryManager;

    /** @var \DateTime $currentDateTime */
    private $currentDateTime;

    /**
     * MailboxManagementService constructor.
     * @param ContainerInterface $container
     * @param EntityManager $entityManager
     * @param TwigEngine $templating
     * @param ToolQueryManager $toolQueryManager
     */
    public function __construct(ContainerInterface $container, EntityManager $entityManager, TwigEngine $templating, ToolQueryManager $toolQueryManager)
    {
        $this->currentDateTime = new \DateTime();

        $this->container = $container;
        $this->em = $entityManager;
        $this->templating = $templating;
        $this->toolQueryManager = $toolQueryManager;

//        $this->attachmentOfEmailsPath = $this->container->get('kernel')->getRootDir() . self::ATTACHMENT_OF_EMAILS_PATH;
        $this->attachmentOfEmailsPath = null;

        $this->cachedEmailsPath = $this->container->get('kernel')->getRootDir() . self::EMAILS_CACHE_DIR;
        $this->mailboxResponseList = [];

        $this->fileUploader = $this->container->get('document.uploader');
    }

    private function blockQueue() {

        try {

            $config = Yaml::parse(file_get_contents(self::CONFIG_FILE));

            /** kolejka jest już zablokowana - jakiś inny cron pracuje */
            if ($config['queue_block'] == "1") return false;

            $config['queue_block'] = 1;
            $yaml = Yaml::dump($config);
            file_put_contents(self::CONFIG_FILE, $yaml);

            return true;

        }
        catch (ParseException $e) {
            throw new InvalidConfigurationException(printf("Unable to parse the YAML string: %s", $e->getMessage()));
        }

    }

    private function unblockQueue() {
        try {

            $config = Yaml::parse(file_get_contents(self::CONFIG_FILE));
            $config['queue_block'] = 0;
            $yaml = Yaml::dump($config);
            file_put_contents(self::CONFIG_FILE, $yaml);
            return true;

        }
        catch (ParseException $e) {
            throw new InvalidConfigurationException(printf("Unable to parse the YAML string: %s", $e->getMessage()));
        }
    }

    public function processLastUnseenEmailInMailbox($mailboxName) {

        $mailbox = $this->em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => $mailboxName
        ]);

        if($mailbox->getHost() === 'mail.starter24.pl') {

            $client = new OutlookProvider($mailbox, $this->fileUploader, $this->em, $this->toolQueryManager, $this->container);

            $unseenEmails = $client->getIdsOfUnseenEmails();

            $firstEmail = $client->getFullMessageType($unseenEmails[0]);

            $client->processEmail($firstEmail);

        }

    }

    public function startCron($render = false, $consoleWrite = false)
    {

        if(!$this->blockQueue()) return false;

        try {

            set_time_limit(36000);

            $mailboxes = $this->findMailbox();
            $response = '';

            if ($mailboxes) {

                $this->loopMailbox($mailboxes);

            } else {
                if($render)
                {
                    $response = $this->templating->render('@Mailbox/Admin/Mailbox/cron-result.html.twig', [
                        'mailboxList' => $this->mailboxResponseList
                    ]);
                }
                else {
                    $response = $this->container->get('translator')->trans('Nie posiadasz żadnej skrzynki pocztowej.');
                }
            }

            if($render)
            {

                $response = $this->templating->render('@Mailbox/Admin/Mailbox/cron-result.html.twig', [
                    'mailboxList' => $this->mailboxResponseList
                ]);

            }
            elseif($consoleWrite)
            {
                $consoleText = '';

                try {

                    $mask = "| %-30.30s |%15s |%30s  |\n";

                    $consoleText .= sprintf($mask, 'Nazwa skrzynki', 'Liczba e-maili', 'Błędy');
                    $consoleText .= sprintf($mask, '', '', '');

                    foreach ($this->mailboxResponseList as $item) {

                        $errors = ($item->getError()) ? $item->getError()->getMessage() : '-';
                        $consoleText .= sprintf($mask, $item->getMailbox()->getName(), $item->getAmount(), $errors);
                    }

                }
                catch(\Exception $e)
                {
                    $consoleText = 'Done.';
                }

                $response = $consoleText;
            }
            else {
                $response = $this->container->get('translator')->trans('Skrzynki pocztowe zostały pomyślnie sprawdzone.');
            }

        }
        catch (\Exception $e) {

            $response = 'Error.' . $e->getMessage();

        }
        finally {
            $this->unblockQueue();
            return $response;
        }


    }

    /**
     * @param ConnectedMailbox $connectedMailbox
     * @return MailContainer
     */
    public function getLastEmailFromMailbox(ConnectedMailbox $connectedMailbox) {


        $client = new OutlookProvider($connectedMailbox, $this->fileUploader, $this->em, $this->toolQueryManager, $this->container);

        $unseenEmails = $client->getIdsOfUnseenEmails();

        $email = $client->getFullMessageType($unseenEmails[0]);

        $emailContainer = $client->saveMessageOnDisc($email);

//        $client->markAsRead($email);

        return $emailContainer;

    }


    private function findMailbox()
    {
        $availableMailbox = $this->em->getRepository('MailboxBundle:ConnectedMailbox')->findBy(['enabled' => 1]);
        return ($availableMailbox) ? $availableMailbox : null;
    }

    /**
     * @param ConnectedMailbox[] $mailboxes
     */

    private function loopMailbox($mailboxes)
    {
        /** @var ConnectedMailbox $mailbox */
        foreach ($mailboxes as $mailbox) {
            if($mailbox->getLastRun()->modify('+'.$mailbox->getIntervalCron().' seconds') < $this->currentDateTime)
            {


                $mailboxResponse = $this->runMailbox($mailbox);

//                $mailboxResponse = $this->processMailbox($mailbox, $mailbox->getLastRun());

                $this->updateMailboxEntity($mailboxResponse, $this->currentDateTime);
                $this->mailboxResponseList[] = $mailboxResponse;
                $this->em->flush();
            }
        }

        $this->em->flush();
    }

    private function runMailbox(ConnectedMailbox $mailbox) {


        $host = $mailbox->getHost();

        if($host === 'mail.starter24.pl') {

            $client = new OutlookProvider($mailbox, $this->fileUploader, $this->em, $this->toolQueryManager, $this->container);
            return $client->runMailbox();

        }
        else {

            return $this->processMailbox($mailbox, $mailbox->getLastRun());

        }

//        $this->errorUpdateNote($email);
//        $this->log("Not found MAIL CLIENT (like outlook) for host: " . $mailbox->getHost(), $this->decorateEmailArray($dataEmail, $email), 'ERROR', true);


    }

    /**
     * @param ConnectedMailbox $mailbox
     * @return MailboxResponse|null
     */

    public function processMailbox(ConnectedMailbox $mailbox, $lastRunDate = null)
    {

        $mailboxString = $this->prepareMailboxString($mailbox);
        $mailboxResponse = new MailboxResponse($mailbox);

        /** @var MailContainer[] $lastEmail */
        $lastEmail = $this->em->getRepository('MailboxBundle:MailContainer')->findBy(['mailbox' => $mailbox], ['id' => 'DESC'], 1);

        if($lastEmail && is_array($lastEmail)) {
            $lastIdEmail = $lastEmail[0]->getEmailId();
        }
        else {
            $lastIdEmail = 0;
        }

        /** Ustawienie danych do połączenia */
        $phpImapMailbox = new Mailbox($mailboxString,
            $mailbox->getLogin(),
            $mailbox->getPassword(),
            $this->attachmentOfEmailsPath);

        if(!$phpImapMailbox) return null;

        /** Próba pobrania nieodczytanych e-maili */
        try {
            $unreadEmailsIds = $this->getUnseenEmailsIds($phpImapMailbox, $mailbox);

            //TODO  - dodać jeszcze opcje z UNSEEN

            $unreadEmailsIds = array_filter($unreadEmailsIds, function ($x) use ($lastIdEmail) {
                return $x > $lastIdEmail; }
            );

            /** Pętla po nieodczytanych e-mailach */

            foreach ($unreadEmailsIds as $key => $id) {

                $mail = $phpImapMailbox->getMail($id);

                if(!$mail->textPlain) {
                    $mail->textPlain = $this->getPlainText($phpImapMailbox, $mail);
                }

                $this->processEmail($mail, $mailbox, $phpImapMailbox);
                $mailboxResponse->increment();

//                $mail = $phpImapMailbox->getMail($unreadEmailsIds[$key]);
//                $mailDate = new \DateTime($mail->date);
//
//                if (!$lastRunDate || ($lastRunDate && ($lastRunDate < $mailDate))) {
//                    $this->processEmail($mail, $mailbox, $phpImapMailbox);
//                    $mailboxResponse->increment();
//                }
//                else {
//                    /** Leci w odwrócej kolejności, więc reszta e-maili jest już stara */
//                    break;
//                }
            }
        }
        catch (\Exception $e)
        {
            $mailboxResponse->setError($e);
        }

        return $mailboxResponse;

    }
    private function getPlainText(MailBox $phpImapMailbox, $mail) {

        $mailStructure = imap_fetchstructure($phpImapMailbox->getImapStream(), $mail->id, FT_UID);

        $data = "";

        if(empty($mailStructure->parts)) {
            $data = $this->getEmailBody($mail, $mailStructure, 0, true, $phpImapMailbox->getImapStream());
        }
        else {
            foreach($mailStructure->parts as $partNum => $partStructure) {
                $data .= $this->getEmailBody($mail, $partStructure, $partNum + 1, true, $phpImapMailbox->getImapStream());
            }
        }

        return $data;
    }

    private function getEmailBody(IncomingMail $mail, $partStructure, $partNum, $markAsSeen = true, $imapStream) {

        $options = FT_UID;
        if(!$markAsSeen) {
            $options |= FT_PEEK;
        }

        $data = $partNum ? imap_fetchbody($imapStream, $mail->id, $partNum, $options) : imap_body($imapStream, $mail->id, $options);

        if($partStructure->encoding == 1) {
            $data = imap_utf8($data);
        }
        elseif($partStructure->encoding == 2) {
            $data = imap_binary($data);
        }
        elseif($partStructure->encoding == 3) {
            $data = preg_replace('~[^a-zA-Z0-9+=/]+~s', '', $data); // https://github.com/barbushin/php-imap/issues/88
            $data = imap_base64($data);
        }
        elseif($partStructure->encoding == 4) {
            $data = quoted_printable_decode($data);
        }

        $crawler = new Crawler($data);
        return ($crawler->count()) ? $crawler->text() : '';

    }

    private function prepareMailboxString(ConnectedMailbox $mailbox)
    {
        $mailboxString = '{'.$mailbox->getHost().':'.$mailbox->getPort().'/'.$mailbox->getProtocol();

        if ($mailbox->isSsl()) {
            $mailboxString .= '/ssl';
        }

        if (!$mailbox->isNovalidateCert()) {
            $mailboxString .= '/novalidate-cert';
        }

        $mailboxString .= '}INBOX';

        return $mailboxString;
    }

    /**
     * @param Mailbox $phpImapMailbox
     * @param ConnectedMailbox $mailbox
     * @return array
     * @throws \Exception
     */

    public function getUnseenEmailsIds(Mailbox $phpImapMailbox, ConnectedMailbox $mailbox)
    {
        $mailsIds = [];
        if($mailbox->getProtocol() == ConnectedMailbox::PROTOCOL_IMAP)
        {
//            $mailsIds = imap_search($phpImapMailbox->getImapStream(), 'ALL', SE_FREE, 'UTF-8');
            $mailsIds = $phpImapMailbox->searchMailbox('ALL');
            //TODO  - dodać jeszcze opcje z UNSEEN
        }
        elseif($mailbox->getProtocol() == ConnectedMailbox::PROTOCOL_POP3)
        {
            throw new \Exception('POP3 is not supported!');
        }

        return $mailsIds;
    }

    private function processEmail(IncomingMail $mail, ConnectedMailbox $mailbox, Mailbox $phpImapMailbox)
    {

        $resultProcedure = 0;

        if($mail instanceof IncomingMail)
        {
            /** Save E-mail in database and as File */

            /** Zapisanie e-maila do cached_email */
            $emailTempPath = $this->saveTempEmail($mail->id, $phpImapMailbox);
            $originalName = 'email_body_'.$mailbox->getId().'_'.$mail->id.'.eml';

            $mailContainer = new MailContainer();
            $mailContainer->setSender($mail->fromAddress);
            $mailContainer->setSubject($mail->subject);
            $mailContainer->setMailbox($mailbox);
            $mailContainer->setEmailId($mail->id);

            $document = new Document();
            $document->setName($mailbox->getHost() . '_'.date('Ymd_His'));
            $this->em->persist($document);

            /** Przeniesienie e-maila do files */
            $mailFile = $this->fileUploader->upload(new MailContainerFile($emailTempPath, $originalName));
            $mailFile->setDocument($document);

            $mailContainer->setMailFile($mailFile);

            $this->em->persist($mailFile);
            $this->em->persist($mailContainer);

            $this->em->flush();

            $parsedQuery = '';

            try {
                $resultProcedure = $this->runProcedure($mail, $mailbox, $document->getId(), $parsedQuery);
            }
            catch (\Exception $e) {
                $parsedQuery = 'Error in execute procedure';
            }

            $mailContainer->setProcedureQuery($parsedQuery);

            /** Zapisanie wszystkiego w bazie */
            $this->em->persist($mailContainer);
            $this->em->flush();

        }

        if($resultProcedure == 1) {
            /** Jeżeli wynik procedury 1 to usuwa e-maila ze skrzynki */
            $phpImapMailbox->deleteMail($mail->id);
        }
        elseif($resultProcedure == 0)
        {
            /** Jeżeli 0 to nie usuwa. */
        }
    }

    private function saveTempEmail($mailId, Mailbox $phpImapMailbox)
    {
        $nameFile = uniqid().'.eml';
        $pathFile = $this->cachedEmailsPath.'/'.$nameFile;

        $phpImapMailbox->saveMail($mailId, $pathFile);

        return $pathFile;
    }

    private function runProcedure($mail, ConnectedMailbox $mailbox, $documentId, &$parsedQuery = null)
    {

        $query = $mailbox->getProcedureText();

        $resp = $this->toolQueryManager->executeCustomQuery($query, false, $mailbox->getConnectionString(), true, $this->parseEmail($mail, $documentId), null, $parsedQuery);

        if(is_array($resp) && isset($resp['error'])) {
            if($resp['error'] == 1 || $resp['error'] == "1") {
                return 1;
            }
        }

        return 0;

    }

    private function parseEmail(IncomingMail $mail, $documentId = null)
    {

        /**
         * Dostępne indexy:
         *
         * -- id
         * date
         * subject
         * fromName
         * fromAddress
         * to (array)
         * -- toString
         * -- cc (array)
         * -- replyTo (array)
         * -- messageId
         * textPlain
         * -- textHtml
         * -- attachments (array)
         */

        if(is_array($mail->to) && !empty($mail->to)) {
            $receivers = [];
            foreach ($mail->to as $key => $item) {
                $receivers[] = $key;
            }
            $to = implode(',', $receivers);
        }
        else {
            $to = $mail->toString;
        }

        $parsedMail = [
            'date' => $mail->date,
            'subject' => $mail->subject,
            'fromName' => $mail->fromAddress,
            'fromAddress' => $mail->fromAddress,
            'toString' => $to,
            'textPlain' => $mail->textPlain,
            'documentId' => $documentId,
            'caseNumber' => self::tryGetCaseNumber($mail->subject)
        ];

        return $parsedMail;
    }

    static function tryGetCaseNumber($subject) {


        /** Atlas number case */
        preg_match_all(self::REGEX_NUMBER_CASE_ATLAS, $subject, $matches, PREG_SET_ORDER, 0);

        if(!empty($matches) && !empty($matches[0])) {
            return $matches[0][0];
        }

        /** PZ number case */
        preg_match_all(self::REGEX_NUMBER_CASE_PZ, $subject, $matches, PREG_SET_ORDER, 0);

        if(!empty($matches) && !empty($matches[0])) {
            return $matches[0][0];
        }

        return null;

    }

    private function updateMailboxEntity(MailboxResponse $mailboxResponse, \DateTime $dateTime)
    {
        $mailbox = $mailboxResponse->getMailbox();
        $mailbox->setLastRun($dateTime);

        if ($mailboxResponse->getLastEmailDate())
        {
            $mailbox->setLastProcessed($mailboxResponse->getLastEmailDate());
        }

        $mailbox->setAmountProcessedEmails($mailbox->getAmountProcessedEmails() + $mailboxResponse->getAmount());
        $this->em->persist($mailbox);
    }

    /**
     * Funkcja, do testowania połączenia z mailbox
     *
     * @param ConnectedMailbox $mailbox
     * @return bool|\Exception
     */
    public function testConnect(ConnectedMailbox $mailbox)
    {
        $mailboxString = $this->prepareMailboxString($mailbox);

        $this->checkAndRepairDirs();

        $phpImapMailbox = new Mailbox($mailboxString,
            $mailbox->getLogin(),
            $mailbox->getPassword(),
            $this->attachmentOfEmailsPath);

        try {
            $phpImapMailbox->statusMailbox();
        }
        catch (\Exception $e)
        {
            return $e;
        }

        return true;
    }

    private function checkAndRepairDirs()
    {
        $fs = new Filesystem();
        $pathDirForAttachment = $this->container->get('kernel')->getRootDir() . MailboxManagementService::ATTACHMENT_OF_EMAILS_PATH;
        $pathDirEmailsCache = $this->container->get('kernel')->getRootDir() . MailboxManagementService::EMAILS_CACHE_DIR;

        try {

            if(!$fs->exists($pathDirForAttachment))
            {
                $fs->mkdir($pathDirForAttachment, 0777);
            }

            if(!$fs->exists($pathDirEmailsCache))
            {
                $fs->mkdir($pathDirEmailsCache, 0777);
            }

        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at ".$e->getPath();
            echo $e->getMessage();
        }

    }
}

class MailboxResponse {

    private $amount;
    /** @var  ConnectedMailbox $mailbox */
    private $mailbox;

    /** @var  \DateTime $lastEmailDate */
    private $lastEmailDate;

    /** @var \Exception */
    private $error;

    /**
     * MailboxResponse constructor.
     * @param ConnectedMailbox $mailbox
     */
    public function __construct(ConnectedMailbox $mailbox)
    {
        $this->mailbox = $mailbox;
        $this->amount = 0;
        $this->error = null;
    }

    public function increment() {
        $this->amount++;
        $this->lastEmailDate = new \DateTime();
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return ConnectedMailbox
     */
    public function getMailbox()
    {
        return $this->mailbox;
    }

    /**
     * @return \DateTime
     */
    public function getLastEmailDate()
    {
        return $this->lastEmailDate;
    }

    /**
     * @return \Exception
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param \Exception $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

}