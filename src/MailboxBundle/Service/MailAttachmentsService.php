<?php

namespace MailboxBundle\Service;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use Exception;
use PhpMimeMailParser\Attachment;
use PhpMimeMailParser\Parser;
use SplFileObject;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;

class MailAttachmentsService
{

    /**
     * @var Filesystem
     */
    public $fs;

    /** @var string */
    public $rootDir;

    public $em;

    /**
     * MailAttachmentsService constructor.
     * @param $rootDir
     * @param EntityManager $entityManager
     */
    public function __construct($rootDir, EntityManager $entityManager)
    {
        $this->fs = new Filesystem();
        $this->rootDir = $rootDir;
        $this->em = $entityManager;
    }

    public function getAttachmentsOfDocument($documentId) {

        /** @var Document $document */
        $document = $this->em->getRepository('DocumentBundle:Document')->find(intval($documentId));

        if(empty($document)) {
            return new JsonResponse([
                'success' => false,
                'error' => 'Email nie istnieje. Prawdopodobnie został usunięty.'
            ]);
        }

        /** @var PersistentCollection $files */
        $files = $document->getFiles();

        /** @var File|null $firstEmail */
        $firstEmail = $files->first();

        if(!$this->existsEmail($firstEmail)) {
            return [];
        }

        try {
            return $this->prepareAttachments(new Parser(), $document, $firstEmail);
        } catch (Exception $e) {
            return [];
        }

    }

    /**
     * @param Document $document
     * @param File $firstEmail
     * @return string
     */
    public function getUniquePathToAttachment(Document $document, File $firstEmail) {

        $emailsAttachmentDir = $this->rootDir . MailboxManagementService::EMAILS_ATTACHMENT_PATH;

        if(!$this->fs->exists($emailsAttachmentDir)) {
            $this->fs->mkdir($emailsAttachmentDir);
        }

        return $emailsAttachmentDir . '/' . $document->getId() . '/' . $firstEmail->getId() . '/';

    }

    public function existsEmail(File $firstEmail) {
        return ($this->fs->exists($this->rootDir . MailboxManagementService::FILES_EML_DIR . $firstEmail->getPath()));
    }

    /**
     * @param Parser $Parser
     * @param Document $document
     * @param File $firstEmail
     * @return array|JsonResponse
     * @throws Exception
     */
    public function prepareAttachments(Parser $Parser, Document $document, File $firstEmail) {

        $attachments = [];
        $uniquePathToAttachment = $this->getUniquePathToAttachment($document, $firstEmail);
        $emailsDir = $this->rootDir . MailboxManagementService::FILES_EML_DIR;

        if(!$this->fs->exists($uniquePathToAttachment)) {
            $this->fs->mkdir($uniquePathToAttachment);
        }

        $Parser->setPath($emailsDir . $firstEmail->getPath());

        $this->saveAttachments($Parser, $uniquePathToAttachment, $document, $firstEmail, $attachments);

        return $attachments;

    }

    /**
     * @param Parser $Parser
     * @param Filesystem $fs
     * @param $uniquePathToAttachment
     * @param Document $document
     * @param File $firstEmail
     * @param $attachments
     * @throws Exception
     */
    public function saveAttachments(Parser $Parser, $uniquePathToAttachment, Document $document, File $firstEmail, &$attachments) {

        /** @var Attachment $attachment */
        foreach ($Parser->getAttachments() as $key => $attachment) {

            $attachment_assets = '';

            // When is bad added attachment. Attachment is Inline but do not have ContentID
            $forceAsAttachment = ($attachment->getContentDisposition() === 'inline' && empty($attachment->getContentID()));

            if($attachment->getContentDisposition() === 'inline' && !empty($attachment->getContentID())) {
                $attachment_path = $uniquePathToAttachment . 'inline/' . $attachment->getContentID();
                if(!$this->fs->exists($uniquePathToAttachment . 'inline')) {
                    $this->fs->mkdir($uniquePathToAttachment . 'inline');
                }
            }
            else {
                $attachment_path = $uniquePathToAttachment . $attachment->getFilename();
                $attachment_assets =  'files/email_attachments/' . $document->getId() . '/' . $firstEmail->getId() . '/' . $attachment->getFilename();
            }

            if(false === $this->fs->exists($attachment_path)) {
                /** @var resource $fp */
                if ($fp = fopen($attachment_path, 'w')) {
                    while ($bytes = $attachment->read()) {
                        fwrite($fp, $bytes);
                    }
                    fclose($fp);
                } else {
                    throw new Exception('Could not write attachments. Your directory may be unwritable by PHP.');
                }
            }

            if($forceAsAttachment || $attachment->getContentDisposition() === 'attachment') {

                $file = new SplFileObject($attachment_path);

                $attachments[] = [
                    'path' => $attachment_assets,
                    'name' => $attachment->getFilename(),
                    'size' => $this->human_filesize($file->getSize()),
                    'extension' => $file->getExtension()
                ];

            }

        }

    }

    function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }
}
