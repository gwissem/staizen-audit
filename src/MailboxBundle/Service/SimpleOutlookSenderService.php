<?php

namespace MailboxBundle\Service;

use MailboxBundle\Entity\ConnectedMailbox;
use jamesiarmes\PhpEws\Client as OutlookClient;


use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use DocumentBundle\Uploader\FileUploader;
use DocumentBundle\Utils\HandlerExtraFiles;
use jamesiarmes\PhpEws\ArrayType\ArrayOfRecipientsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAllItemsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAttachmentsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseItemIdsType;
use jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use jamesiarmes\PhpEws\Enumeration\MessageDispositionType;
use jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use jamesiarmes\PhpEws\Request\CreateAttachmentType;
use jamesiarmes\PhpEws\Request\CreateItemType;
use jamesiarmes\PhpEws\Request\GetItemType;
use jamesiarmes\PhpEws\Request\SendItemType;
use jamesiarmes\PhpEws\Type\BodyType;
use jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use jamesiarmes\PhpEws\Type\EmailAddressType;
use jamesiarmes\PhpEws\Type\FileAttachmentType;
use jamesiarmes\PhpEws\Type\ItemIdType;
use jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use jamesiarmes\PhpEws\Type\MessageType;
use jamesiarmes\PhpEws\Type\SingleRecipientType;
use jamesiarmes\PhpEws\Type\TargetFolderIdType;


class SimpleOutlookSenderService
{

    /** @var OutlookClient  */
    private $client;

    /** @var  ConnectedMailbox */
    private $mailbox;

    /**
     * MailboxOutlook constructor.
     * @param ConnectedMailbox $mailbox
     */
    public function __construct(ConnectedMailbox $mailbox = null)
    {

        if($mailbox) {
            $host = $mailbox->getHost();

            if($host === 'mail.starter24.pl') {

                $username = $mailbox->getLogin();
                $password = $mailbox->getPassword();
                $version = OutlookClient::VERSION_2010;

                $this->client = new OutlookClient($host, $username, $password, $version);

            }

            $this->mailbox = $mailbox;
        }

    }

    /**
     * Potem warto dodać jakieś załączniki
     *
     * @param $to
     * @param $subject
     * @param $body
     * @param null $cc
     * @param null $bcc
     * @return array|bool
     */
    public function send($to, $subject, $body, $cc = null, $bcc = null) {

        $requestCreateMessage = $this->createMessageRequest($to, $subject, $body, $cc, $bcc);

        $createResponse = $this->createMessage($requestCreateMessage);

        if(!$createResponse['success']) {

            return [
                'success' => false,
                'error_message' => 'createMessage() : ' . $createResponse['error_message']
            ];

        }

        $response = $this->sendEmail($createResponse['messages']);

        if($response !== true) {
            return [
                'success' => false,
                'error_message' => 'sendEmail() : ' . $response['error_message']
            ];
        }

        return true;

    }

    /**
     * @param $to
     * @param $subject
     * @param $body
     * @param null $cc
     * @param null $bcc
     * @return CreateItemType
     */
    private function  createMessageRequest($to, $subject, $body, $cc = null, $bcc = null) {

        // Stworzenie Requesta
        $request = new CreateItemType();
        $request->Items = new NonEmptyArrayOfAllItemsType();
        $request->MessageDisposition = MessageDispositionType::SAVE_ONLY;

        // Katalog gdzie ma się zapisać - w wysłanych
        $send_folder = new TargetFolderIdType();
        $send_folder->DistinguishedFolderId = new DistinguishedFolderIdType();
        $send_folder->DistinguishedFolderId->Id = DistinguishedFolderIdNameType::SENT;
        $request->SavedItemFolderId = $send_folder;

        // Tworzenie wiadomości
        $message = new MessageType();

        $message->IsRead = false;
        $message->Subject = $subject;

        $message->From = new SingleRecipientType();
        $message->From->Mailbox = new EmailAddressType();
        $message->From->Mailbox->EmailAddress = $this->mailbox->getName();

        $message->ToRecipients = new ArrayOfRecipientsType();

        $to = str_replace(';', ',', $to);

        // Odbiorcy
        foreach (explode(',', $to) as $receiver) {

            if(!$receiver) continue;

            $recipient = new EmailAddressType();
            $recipient->EmailAddress = trim($receiver);
            $message->ToRecipients->Mailbox[] = $recipient;

        }

        if(!empty($cc)) {

            $message->CcRecipients = new ArrayOfRecipientsType();

            $cc = str_replace(';', ',', $cc);

            // DW
            foreach (explode(',', $cc) as $receiver) {

                if(!$receiver) continue;

                $recipient = new EmailAddressType();
                $recipient->EmailAddress = trim($receiver);
                $message->CcRecipients->Mailbox[] = $recipient;

            }
        }

        if(!empty($bcc)) {
            $message->BccRecipients = new ArrayOfRecipientsType();

            $bcc = str_replace(';', ',', $bcc);

            // DW
            foreach (explode(',', $bcc) as $receiver) {

                if(!$receiver) continue;

                $recipient = new EmailAddressType();
                $recipient->EmailAddress = trim($receiver);
                $message->BccRecipients->Mailbox[] = $recipient;

            }
        }

        // Zawartość e-maila

        $message->Body = new BodyType();
        $message->Body->BodyType = BodyTypeType::HTML;
        $message->Body->_ = $body;

        $request->Items->Message[] = $message;

        return $request;
    }

    private function createMessage($requestCreateMessage) {

        $response = $this->client->CreateItem($requestCreateMessage);

        $response_messages = $response->ResponseMessages->CreateItemResponseMessage;

        $messagesForSend = [];

        // ZAKŁADAM, ŻE WYSYŁA 1 E-maila !
        foreach ($response_messages as $response_message) {

            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {

                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;

                return [
                    'success' => false,
                    'error_message' => "Message failed to create with " . $code . ": " . $message
                ];

            }

            foreach ($response_message->Items->Message as $item) {

                $messagesForSend[] = [
                    'id' => $item->ItemId->Id,
                    'changeKey' => $item->ItemId->ChangeKey
                ];

            }
        }

        return [
            'success' => true,
            'messages' => $messagesForSend
        ];

    }

    private function sendEmail($messages) {

        $requestSendItems = $this->createSendItem($messages);
        $response = $this->client->SendItem($requestSendItems);
        $response_messages = $response->ResponseMessages->SendItemResponseMessage;

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {

                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;

                return [
                    'success' => false,
                    'error_message' => "Message failed to create with " . $code . ": " . $message
                ];

            }
        }

        return true;
    }


    private function createSendItem($messages) {

        $request = new SendItemType();
        $request->SaveItemToFolder = true;
        $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();

        foreach ($messages as $message) {
            $item = new ItemIdType();
            $item->Id = $message['id'];
            $item->ChangeKey = $message['changeKey'];
            $request->ItemIds->ItemId[] = $item;
        }

        $send_folder = new TargetFolderIdType();
        $send_folder->DistinguishedFolderId = new DistinguishedFolderIdType();
        $send_folder->DistinguishedFolderId->Id = DistinguishedFolderIdNameType::SENT;

        $request->SavedItemFolderId = $send_folder;

        return $request;

    }


}
