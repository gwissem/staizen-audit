<?php

namespace MailboxBundle\Service;

use AppBundle\Entity\Log;
use AppBundle\Service\ElasticSearchLogService;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\Step;
use CaseBundle\Repository\AttributeValueRepository;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Service\CaseFormGenerator;
use CaseBundle\Service\ProcessHandler;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\File;
use DocumentBundle\Listener\DocumentUploadListener;
use Knp\Snappy\Pdf;
use MailboxBundle\Entity\ConnectedMailbox;
use PhpMimeMailParser\Parser;
use SocketBundle\Service\RedisListListenerService;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use ToolBundle\Utils\ToolQueryManager;
use \jamesiarmes\PhpEws\Client;

class MailboxOutgoingEmailService
{

    const ATTACHMENT_OF_EMAILS_PATH = '/../web/mailbox_attachments';
    const EMAILS_ATTACHMENT_PATH = '/../web/files/email_attachments';

    const CONFIG_FILE = __DIR__ . '/../../../app/Resources/cron/outgoing_email.yml';
    const EMAILS_CACHE_DIR = __DIR__ . '/../../../web/cached_emails';
    const FILES_EML_DIR = __DIR__ . '/../../../web/files';

    /** @var  EntityManager */
    private $em;

    /** @var  ContainerInterface $container */
    public $container;

    /** @var \Predis\Client */
    private $redis;

    /** @var  MailboxResponse[] $mailboxResponseList */
    private $mailboxResponseList;

    /** @var  TwigEngine $templating */
    private $templating;

    /** @var  ToolQueryManager */
    private $toolQueryManager;

    /** @var  ProcessHandler */
    private $processHandler;

    /** @var  AttributeValueRepository */
    private $attributeValueRepository;

    /** @var  AttributeParserService $attributeParser */
    private $attributeParser;

    /** @var Pdf $pdfConverter */
    private $pdfConverter;

    /** @var  Logger */
    private $logger;

    /** @var  SymfonyStyle */
    private $io;

    private $onlyShowMessagesToSend = false;

    /** @var int - Limit ile e-maili wysłać za jednym razem */
    private $limit = 20;

    /**
     * @var int
     */
    private $amountSentEmails = 0;

    /**
     * ID notatek, które zostaną wysłane, reszta pominięta
     *
     * @var array
     */
    private $whiteList = [];

    private $displayEmailArray = [];

    private $isElasticLogging;

    /**
     * MailboxManagementService constructor.
     * @param ContainerInterface $container
     * @param EntityManager $entityManager
     * @param TwigEngine $templating
     * @param ToolQueryManager $toolQueryManager
     */
    public function __construct(ContainerInterface $container, EntityManager $entityManager, TwigEngine $templating, ToolQueryManager $toolQueryManager)
    {

        $this->container = $container;
        $this->templating = $templating;
        $this->processHandler = $container->get('case.process_handler');
        $this->logger = $this->container->get('monolog.logger.sent_email');
        $this->redis = $container->get('snc_redis.default');
        $this->isElasticLogging = filter_var($this->container->getParameter('elastica_logging'), FILTER_VALIDATE_BOOLEAN);

        $this->mailboxResponseList = [];

        // nowe
        $this->em = $entityManager;
        $this->toolQueryManager = $toolQueryManager;
        $this->attributeValueRepository = $this->em->getRepository('CaseBundle:AttributeValue');
    }

    private function blockQueue()
    {

        try {

            $config = Yaml::parse(file_get_contents(self::CONFIG_FILE));

            /** kolejka jest już zablokowana - jakiś inny cron pracuje */
            if ($config['queue_block'] == "1") return false;

            $config['queue_block'] = 1;
            $yaml = Yaml::dump($config);
            file_put_contents(self::CONFIG_FILE, $yaml);

            return true;

        } catch (ParseException $e) {
            throw new InvalidConfigurationException(printf("Unable to parse the YAML string: %s", $e->getMessage()));
        }

    }

    private function unblockQueue()
    {
        try {

            $config = Yaml::parse(file_get_contents(self::CONFIG_FILE));
            $config['queue_block'] = 0;
            $yaml = Yaml::dump($config);
            file_put_contents(self::CONFIG_FILE, $yaml);
            return true;

        } catch (ParseException $e) {
            throw new InvalidConfigurationException(printf("Unable to parse the YAML string: %s", $e->getMessage()));
        }
    }

    /**
     * @param bool $render
     * @param SymfonyStyle|null $io
     * @param bool $onlyShow
     * @return bool|string
     * @internal param bool $consoleWrite
     */
    public function startSending($render = false, SymfonyStyle $io = null, $onlyShow = false)
    {

        /**
         * TODO - zamiast tego blokowania zaimplementować: https://github.com/symfony/lock/blob/master/Store/RedisStore.php
         * A najlepiej przenieść na JOB JMS, albo rabbitMq
         */
        if (!$this->blockQueue()) return false;

        $this->onlyShowMessagesToSend = $onlyShow;
        $this->io = $io;

        try {

            set_time_limit(36000);

            $emailToSend = $this->findNewNotesWithEmail();

            if ($emailToSend) {

                if ($this->io !== null) {
                    $this->io->section('E-mails to send: ' . count($emailToSend) . ' (limit: ' . $this->limit . ')');
                }

                $startTime = new \DateTime();

                $this->loopEmails($emailToSend);

                if ($this->io !== null) {

                    if (!$this->onlyShowMessagesToSend) {
                        $this->io->section('Sent ' . count($this->displayEmailArray) . ' emails');
                    }

                    $this->io->table(
                        array('Id', 'From', 'To', 'Subject'),
                        $this->displayEmailArray
                    );
                }

                $context = [
                    'amount' => $this->amountSentEmails,
                    'time' => (new \DateTime())->getTimestamp() - $startTime->getTimestamp(),
                    'memory' => $this->getUsageMemory()
                ];

                $this->log('Finish emails loop.', $context);

            } else {

                $io->note('Brak e-maili do wysłania.');

            }

        } catch (\Exception $e) {

            $io->error($e->getMessage());

        } finally {
            $this->unblockQueue();
        }

        return true;

    }

    /**
     * @param $emailNoteId
     * @return File|object|null
     */
    public function findEmlByNoteId($emailNoteId)
    {

        /** @var AttributeValue $attributeEmlFile */
        $attributeEmlFile = $this->em->getRepository('CaseBundle:AttributeValue')->findOneBy([
            'attributePath' => '406,226,228',
            'parentAttributeValue' => $emailNoteId
        ]);

        $emlDocumentId = $attributeEmlFile->getValueInt();

        return $this->em->getRepository('DocumentBundle:File')->findOneBy([
            'document' => $emlDocumentId
        ]);

    }

    private function getUsageMemory()
    {
        return number_format((memory_get_usage() / 1024 / 1024), 3);
    }

    /**
     * @return array
     */
    private function findNewNotesWithEmail()
    {
        /**
         * Sprawdzenie listy WhiteList - wiadomo, można by wrzucic do zapytania...
         */
        if (!empty($this->whiteList)) {
            return array_filter($this->attributeValueRepository->findAllNotesWithNewEmail($this->limit), function ($obj) {
                if (isset($obj['id']) && in_array($obj['id'], $this->whiteList)) {
                    return true;
                }
                return false;
            });
        } else {
            return $this->attributeValueRepository->findAllNotesWithNewEmail($this->limit);
        }


    }

    /**
     * @param array $emails
     */

    private function loopEmails($emails)
    {

        foreach ($emails as $email) {

            $this->processEmail($email);

        }

        $this->em->flush();
    }

    private function getMailbox($login)
    {

        $loginMailbox = substr($login, 0, strpos($login, '@'));

        return $this->em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'login' => $loginMailbox
        ]);

    }

    /**
     * @param ConnectedMailbox $mailbox
     * @return Client|null
     */

    private function getMailClient(ConnectedMailbox $mailbox)
    {

        $host = $mailbox->getHost();

        if ($host === 'mail.starter24.pl') {

            $username = $mailbox->getLogin();
            $password = $mailbox->getPassword();
            $version = Client::VERSION_2010;

            return new Client($host, $username, $password, $version);

        }

        return null;

    }

    /**
     * @param $emailNoteId
     * @param $from
     * @param $to
     * @param string $cc
     * @param string $bcc
     * @param string $extraBodyContent
     */
    public function forwardEmailNote($emailNoteId, $from, $to, $cc = '', $bcc = '', $extraBodyContent = '')
    {

        die('Dont USE! In Tests...');

        /** @var AttributeValue $emailAttributeValue */
        $emailAttributeValue = $this->em->getRepository('CaseBundle:AttributeValue')->find($emailNoteId);
        $file = $this->findEmlByNoteId($emailNoteId);

        $mailbox = $this->em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => $from
        ]);

        if (empty($mailbox)) {
            throw new NotFoundResourceException('Mailbox not exists.');
        }

        $Parser = new Parser();

        if (!file_exists(self::FILES_EML_DIR . $file->getPath())) {
            throw new NotFoundResourceException('File EML not exists.');
        }

        $Parser->setPath(self::FILES_EML_DIR . $file->getPath());
        $client = $this->getMailClient($mailbox);

        $dataEmail = [
            'id' => '',
            'to' => $to,
            'dw' => $cc,
            'udw' => $bcc,
            'subject' => "FW: " . $Parser->getHeader('subject'),
            'from' => $mailbox->getName(),
            'groupProcessId' => $emailAttributeValue->getGroupProcessInstance()->getId(),
            'body' => $this->getParserBody($Parser, $extraBodyContent),
            'files' => $Parser->getAttachments()
        ];

        list($newNoteId, $structure) = $this->addNote($emailAttributeValue, $dataEmail);

        $mailboxOutlook = new MailboxOutlook($this, $client, $mailbox, $dataEmail);
        $mailboxOutlook->setUploader($this->container->get('document.uploader'));
        $mailboxOutlook->setEm($this->em);

        $response = $mailboxOutlook->forward();

        $emailExtraData = [
            'id' => $newNoteId,
            'groupProcessId' => $emailAttributeValue->getRootProcess()->getId(),
            'documentId' => $structure[CaseFormGenerator::NOTE_ATTACHMENT]['id'],
            'statusId' => $structure[CaseFormGenerator::NOTE_STATUS]['id'],
        ];

        if ($response['success'] === true) {

            /** Akutalizacja id dokumentu w notatce (.eml) */
            $this->processHandler->editAttributeValueNew(
                ['406,226,228' => $structure[CaseFormGenerator::NOTE_ATTACHMENT]['id']], $response['documentId'],
                'int', 'xxx', $emailAttributeValue->getRootProcess()->getId(), 1, 1);

            /** Ustawienie statusu na "sent" */
            $this->processHandler->editAttributeValueNew(
                ['406,226,734' => $structure[CaseFormGenerator::NOTE_STATUS]['id']], "sent",
                'string', 'xxx', $emailAttributeValue->getRootProcess()->getId(), 1, 1);

            $this->log('Sent e-mail.', $this->decorateEmailArray($dataEmail, $emailExtraData), 'INFO', true);

        } else if ($response['success'] === false) {
//            $this->errorUpdateNote($emailExtraData);
            $this->log($response['error_message'], $this->decorateEmailArray($dataEmail, $emailExtraData), 'ERROR', true);
        }

    }

    private function addNote(AttributeValue $emailAttributeValue, &$dataEmail)
    {

        list($newNoteId, $structure) = $this->container->get('note.handler')->copyNote($emailAttributeValue->getId());

        $userInfo = $this->container->get('user.info');
        $user = $userInfo->getUser();

        $structure = $this->container->get('note.handler')->copyNoteData($emailAttributeValue->getId(), $newNoteId, $emailAttributeValue->getRootProcess()->getId(), $user->getId());

        $dataEmail['id'] = $newNoteId;

        $mapData = [
            ['path' => CaseFormGenerator::NOTE_EMAIL, 'type' => AttributeValue::VALUE_STRING, 'value' => $dataEmail['to']], /** Odbiorca - email */
            ['path' => CaseFormGenerator::NOTE_EMAIL_SENDER, 'type' => AttributeValue::VALUE_STRING, 'value' => $dataEmail['from']], /** sender - email */
            ['path' => CaseFormGenerator::NOTE_SUBJECT, 'type' => AttributeValue::VALUE_STRING, 'value' => $dataEmail['subject']], /** subject */
            ['path' => CaseFormGenerator::NOTE_DIRECTION, 'type' => AttributeValue::VALUE_INT, 'value' => 1], /** Kierunek */
            ['path' => CaseFormGenerator::NOTE_ATTACHMENT, 'type' => AttributeValue::VALUE_INT, 'value' => NULL] /** zerowanie id dokumentu w notatce (.eml) */
        ];

        foreach ($mapData as $mapDatum) {

            $this->processHandler->editAttributeValueNew(
                [$mapDatum['path'] => $structure[$mapDatum['path']]['id']],
                $mapDatum['value'],
                $mapDatum['type'],
                Step::ALWAYS_ACTIVE,
                $emailAttributeValue->getRootProcess()->getId(),
                $user->getId(),
                $user->getId());

        }

        return [$newNoteId, $structure];
    }

    /**
     * @param Parser $parser
     * @param string $extraBodyContent
     * @return string
     */
    private function getParserBody(Parser $parser, $extraBodyContent = '')
    {

        $body = $parser->getMessageBody('html');

        if (empty($body)) {
            $body = $parser->getMessageBody('text');
        }

        if (!empty($extraBodyContent)) {

            $body = $extraBodyContent . "<hr>" . $body;
        }

        return $body;

    }

    public function saveAndSendEmailByOutlook(Client $client, $mailbox, $dataEmail)
    {

        $mailboxOutlook = new MailboxOutlook($this, $client, $mailbox, $dataEmail);
        $mailboxOutlook->setUploader($this->container->get('document.uploader'));
        $mailboxOutlook->setEm($this->em);

        return $mailboxOutlook->send();

    }

    private function successUpdateNote($note, $documentId)
    {

        if ($note['documentId']) {
            /** Akutalizacja id dokumentu w notatce (.eml) */
            $this->processHandler->setAttributeValue(['406,226,228' => $note['documentId']], $documentId, 'int', 'xxx', $note['groupProcessId']);
        }

        if ($note['statusId']) {
            /** Ustawienie statusu na "sent" */
            $this->processHandler->setAttributeValue(['406,226,734' => $note['statusId']], "sent", 'string', 'xxx', $note['groupProcessId']);
        }

    }

    private function errorUpdateNote($note)
    {
        if ($note['statusId']) {
            $this->processHandler->setAttributeValue(['406,226,734' => $note['statusId']], "error", 'string', 'xxx', $note['groupProcessId']);
        }

    }

    /**
     * @param $email - id notatki z e-mailem
     * @return null
     */
    public function processEmail($email)
    {

        $dataEmail = null;

        try {

            /** Pobranie informacji o e-mailu z notatki */
            $dataEmail = $this->getNoteInfo($email);

            if ($this->onlyShowMessagesToSend) {
                $this->displayEmailToSend($dataEmail, $email);
                return true;
            }

            /** Pobranie skrzynki z której zostanie wysłany e-mail */
            $mailbox = $this->getMailbox($dataEmail['from']);

            if ($mailbox) {

                $response = null;

                $client = $this->getMailClient($mailbox);

                if ($client instanceof Client) {

                    try {
                        $response = $this->saveAndSendEmailByOutlook($client, $mailbox, $dataEmail);
                    } catch (\Exception $e) {

                        $message = $e->getMessage();

                        if ($e->getPrevious()) {
                            $message .= ". " . $e->getPrevious()->getMessage();
                        }

                        $this->errorUpdateNote($email);
                        $this->log('EXCEPTION IN saveAndSendEmailByOutlook(). ' . $message, $this->decorateEmailArray($dataEmail, $email), 'CRITICAL', true);
                        $response = null;

                    }

                } else {

                    $this->errorUpdateNote($email);
                    $this->log("Not found MAIL CLIENT (like outlook) for host: " . $mailbox->getHost(), $this->decorateEmailArray($dataEmail, $email), 'ERROR', true);

                }

                if ($response !== null) {

                    /** Udało się wysłać! */
                    if ($response['success'] === true) {
                        $this->successUpdateNote($email, $response['documentId']);
                        $this->displayEmailArray[] = $this->decorateEmailArray($dataEmail, $email);
                        $this->log('Sent e-mail.', $this->decorateEmailArray($dataEmail, $email), 'INFO', true);
                        $this->amountSentEmails++;
                    } else if ($response['success'] === false) {
                        $this->errorUpdateNote($email);
                        $this->log($response['error_message'], $this->decorateEmailArray($dataEmail, $email), 'ERROR', true);
                    }

                }

            } else {

                $this->errorUpdateNote($email);
                $this->log("Not found mailbox for email: " . $dataEmail['from'], $this->decorateEmailArray($dataEmail, $email), 'ERROR', true);

            }

            return true;

        } catch (\Exception $e) {

            $this->errorUpdateNote($email);
            $this->log('EXCEPTION IN processEmail(). ' . $e->getMessage(), $this->decorateEmailArray($dataEmail, $email), 'CRITICAL', true);

            return false;
        }

    }

    public function getEmailBodyFromAttribute($attributeValueId)
    {

        $attributeValue = $this->attributeValueRepository->find($attributeValueId);

        if (!$attributeValue) {
            return null;
        }

        return $attributeValue->getValueText();

    }

    public function getEmailBodyFromFile($twigName, $options)
    {

        return $this->templating->render('@Mailbox/EmailTemplate/' . $twigName . '.html.twig', $options);

    }

    public function parseEmailBody($body, $groupProcessInstanceId)
    {

        if ($this->attributeParser === null) {
            $this->attributeParser = $this->container->get('case.attribute_parser.service');
        }

        $this->attributeParser->setGroupProcessInstanceId($groupProcessInstanceId);

        return $this->attributeParser->parseString($body);

    }

    public function convertHtmlToPdf($html)
    {

        if ($this->pdfConverter === null) {
            $this->pdfConverter = $this->container->get('knp_snappy.pdf');
        }

        $directory = $this->container->getParameter('kernel.root_dir') . DocumentUploadListener::PATH_TEMP_FILES;

        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        $name = uniqid();
        $fullPath = $directory . '/' . $name . '.pdf';

        $this->pdfConverter->setTimeout(60);
        $this->pdfConverter->setOption('zoom', null);
        $this->pdfConverter->setOption('window-status', '');
        $this->pdfConverter->setOption('viewport-size', null);
        $this->pdfConverter->setOption('enable-javascript', true);

        $this->pdfConverter->generateFromHtml($html, $fullPath);

        return $fullPath;

    }

    public function convertProcessInstanceFormToPdf($processInstanceId)
    {

        if ($this->pdfConverter === null) {
            $this->pdfConverter = $this->container->get('knp_snappy.pdf');
        }

        $directory = $this->container->getParameter('kernel.root_dir') . DocumentUploadListener::PATH_TEMP_FILES;

        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        $name = uniqid();
        $fullPath = $directory . '/' . $name . '.pdf';

        $tempOptions = $this->pdfConverter->getOptions();

        $this->pdfConverter->setTimeout(60);
        $this->pdfConverter->setOption('zoom', 0.7);
        $this->pdfConverter->setOption('viewport-size', '1920x1080');
        $this->pdfConverter->setOption('enable-javascript', true);
        $this->pdfConverter->setOption('no-stop-slow-scripts', true);
        $this->pdfConverter->setOption('window-status', 'form-loaded');
        $this->pdfConverter->setOption('debug-javascript', true);

        $host = $this->container->getParameter('server_host');

        $url = $host . $this->container->get('router')->generate('public_operational_dashboard_print', [
                'processInstanceId' => $processInstanceId
            ]);

        $this->pdfConverter->generate($url, $fullPath, [], true);

        // Resetowanie ustawień
        $this->pdfConverter->setOptions($tempOptions);

        return $fullPath;

    }

    public function log($message, $context, $level = 'INFO', $sqlLog = false)
    {

        switch ($level) {
            case 'INFO' :
            {
                $this->logger->info($message, $context);
                break;
            }
            case 'WARNING' :
            {
                $this->logger->warning($message, $context);
                break;
            }
            case 'ERROR' :
            {
                $this->logger->error($message, $context);
                break;
            }
            case 'CRITICAL' :
            {
                $this->logger->critical($message, $context);
                break;
            }
        }

        if ($sqlLog) {

            try {

                if ($this->isElasticLogging) {

                    $log = [
                        'name' => 'mailbox_monitor',
                        'data' => [
                            'mailbox_monitor.type_monitor' => 'sent_email',
                            'mailbox_monitor.level' => $level,
                            'mailbox_monitor.message' => $message,
                            'mailbox_monitor.id' => $context['id'],
                            'mailbox_monitor.from' => $context['from']
                        ]
                    ];

                    $this->redis->rpush(
                        RedisListListenerService::ELASTICA_LOG,
                        [
                            json_encode($log)
                        ]
                    );

                }

            }
            catch (\Exception $exception) {

            }

            $log = new Log();
            $log->setName('EMAIL_' . $level);

            $log->setContent(\GuzzleHttp\json_encode([
                'message' => $message,
                'context' => $context
            ]));

            if (isset($context['id'])) {
                $log->setParam1($context['id']);
            }

            if (isset($context['from'])) {
                $log->setParam2($context['from']);
            }

            if (isset($context['to'])) {
                $log->setParam3($context['to']);
            }

            $this->em->persist($log);
            $this->em->flush();

        }

    }

    private function displayEmailToSend($emailData, $emailNote)
    {

        $this->displayEmailArray[] = $this->decorateEmailArray($emailData, $emailNote);

    }

    /**
     * @param $emailData
     * @param $emailNote
     * @return array
     *
     * list($id,$from,$to,$subject)
     */
    private function decorateEmailArray($emailData, $emailNote)
    {
        try {
            return [
                'id' => (isset($emailNote['id'])) ? $emailNote['id'] : null,
                'from' => ($emailData === null) ?: $emailData['from'],
                'to' => ($emailData === null) ?: $emailData['to'],
                'subject' => ($emailData === null) ?: $emailData['subject']
            ];
        } catch (\Exception $exception) {
            return [
                'exception' => 'Exception in decorateEmailArray(). ' . $exception->getMessage()
            ];
        }
    }

    private function getNoteInfo($email)
    {

        $emailData = [];

        $attributesValue = $this->attributeValueRepository->findBy([
            'parentAttributeValue' => $email['id']
        ]);

        /** @var AttributeValue $attributeValue */
        foreach ($attributesValue as $attributeValue) {
            $emailData[$attributeValue->getAttributePath()] = $attributeValue->getValueByType($attributeValue->getAttribute()->getValueType());
        }

        $this->changeKeyName($emailData);

        $emailData['id'] = $email['id'];
//        $emailData['groupProcessId'] = $email['groupProcessId'];

        return $emailData;

    }

    private function changeKeyName(&$array)
    {

        $pathToName = [
            '406,226,198' => 'to',
            '406,226,227' => 'shortContent',
            '406,226,228' => 'eml',
            '406,226,407' => 'direction',
            '406,226,409' => 'subject',
            '406,226,410' => 'dw',
            '406,226,733' => 'udw',
            '406,226,734' => 'status',
            '406,226,735' => 'from',
            '406,226,736' => 'files',
            '406,226,737' => 'body',
            '406,226,615' => 'groupProcessId'
        ];

        foreach ($array as $key => $value) {

            if (isset($pathToName[$key])) {
                $array[$pathToName[$key]] = $value;
            }

            unset($array[$key]);

        }

    }

    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    public function setWhiteList(array $whiteList)
    {
        $this->whiteList = $whiteList;
    }

    /**
     * @param $noteId
     * @return string
     */
    public function sendNote($noteId)
    {
        $email = $this->attributeValueRepository->findNoteEmail($noteId);

        if (empty($email)) {
            return 'Mail not found';
        }

        $process = $this->processEmail($email[0]);

        return $process ? 'Mail sent' : 'Mail not sent';
    }

}

// Wysyłanie zwykłego e-maila // TODO - trzeba to do jakiegoś DOCa dać

//$mailService = $this->get('atlas.mailbox.outgoing_email.service');
//
//$dataEmail = [
//    'id' => uniqid(),
//    'to' => 'dylesiu@gmail.com',
//    'shortContent' => 'Test',
//    'eml' => '',
//    'direction',
//    'subject' => 'TITLE 123',
//    'dw' => '',
//    'udw' => '',
//    'status' => '',
//    'from' => 'doatlasa@mail.starter24.pl',
//    'files' => '',
//    'body' => "TEST BODY",
//    'groupProcessId' => ''
//];
//
//$mailbox = $this->getDoctrine()->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
//    'login' => 'doatlasa'
//]);
//
//$username = $mailbox->getLogin();
//$password = $mailbox->getPassword();
//$version = \jamesiarmes\PhpEws\Client::VERSION_2010;
//
//$client = new \jamesiarmes\PhpEws\Client('mail.starter24.pl', $username, $password, $version);
//
//$mailService->saveAndSendEmailByOutlook($client, $mailbox, $dataEmail);
