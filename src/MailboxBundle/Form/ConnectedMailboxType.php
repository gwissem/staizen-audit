<?php

namespace MailboxBundle\Form;

use MailboxBundle\Entity\ConnectedMailbox;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConnectedMailboxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa skrzynki'
            ))
            ->add('host', TextType::class, array(
                'label' => 'Adres serwera poczty',
                'attr' => [
                    'placeholder' => 'example: imap.gmail.com'
                ]
            ))
            ->add('port', IntegerType::class, array(
                'label' => 'Port'
            ))
            ->add('ssl', CheckboxType::class, array(
                'label' => 'Bezpieczne połączenie (SSL)',
                'required' => false
            ))
            ->add('novalidateCert', CheckboxType::class, array(
                'label' => 'Walidacja certyfikatu',
                'required' => false
            ))
            ->add('connectionString', TextType::class, array(
                'label' => 'Parametry połączenia',
                'required' => false,
                'attr' => [
                    'placeholder' => 'sql;host;port;dbname;user;password'
                ]
            ))
            ->add('protocol', TextType::class, array(
                'label' => 'Protokół',
                'required' => true,
                'data' => ConnectedMailbox::PROTOCOL_IMAP,
                'disabled' => true
//                'choices' => ConnectedMailbox::getProtocolArray(),
//                'attr' => [
//                    'class' => 'select2'
//                ]
            ))
            ->add('mailbox_login', TextType::class, array(
                'label' => 'Login',
                'property_path' => 'login',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ))
//            ->add('mailbox_password', PasswordType::class, array(
            ->add('mailbox_password', TextType::class, array(
                'label' => 'Hasło',
                'property_path' => 'password',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ))
            ->add('intervalCron', ChoiceType::class, array(
                'label' => 'Częstotliwość sprawdzanie skrzynki',
                'required' => true,
                'choices' => ConnectedMailbox::getAvailableIntervalArray(),
                'attr' => [
                    'class' => 'select2'
                ]
            ))
            ->add('procedureText', TextareaType::class, array(
                'label' => 'Procedura',
                'required' => false,
                'attr' => [
                    'class' => 'sql-codemirror',
                    'placeholder' => "np: CALL `inc_figury`('{@subject@}', '{@fromName@}', '{@textPlain@}', @wynik);"
                ]
            ))
            ->add('enabled', CheckboxType::class, array(
                'label' => 'Aktywna',
                'required' => false
            ))
            ->add('filterSubject', CheckboxType::class, array(
                'label' => 'Czy filtrować temat',
                'required' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'MailboxBundle\Entity\ConnectedMailbox',
            )
        );
    }

    public function getName()
    {
        return 'atlas_connected_mailbox';
    }
}
