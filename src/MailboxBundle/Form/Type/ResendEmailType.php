<?php

namespace MailboxBundle\Form\Type;

use MailboxBundle\Entity\ConnectedMailbox;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResendEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('receiver', EmailType::class, array(
                'label' => 'Odbiorca',
                'attr' => [
                    'placeholder' => 'E-mail'
                ]
            ))
            ->add('sender', TextType::class, array(
                'label' => 'Nadawca',
                'disabled' => true
            ))
            ->add('subject', TextareaType::class, array(
                'label' => 'Temat e-maila',
                'disabled' => true,
                'attr' => [
                    'rows' => 3
                ]
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => null,
                'csrf_protection' => false
            )
        );
    }

}
