<?php

namespace MailboxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CronController extends Controller
{
    /**
     * @Route("/start-cron", name="mailbox_start_cron")
     */

    public function startCronAction()
    {

        $mailboxManagement = $this->get('atlas.mailbox.management.service');
        $response = $mailboxManagement->startCron(true);

        return $this->render('@Mailbox/Admin/Mailbox/blank.html.twig', [
            'htmlResponse' => $response
        ]);

    }
}
