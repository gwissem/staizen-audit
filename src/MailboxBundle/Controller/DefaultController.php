<?php

namespace MailboxBundle\Controller;

use Doctrine\ORM\PersistentCollection;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use MailboxBundle\Entity\ConnectedMailbox;
use MailboxBundle\Form\ConnectedMailboxType;
use MailboxBundle\Service\MailboxManagementService;
use PhpMimeMailParser\Attachment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="admin_mailbox_index")
     * @return Response
     */
    public function indexAction()
    {
        return new RedirectResponse($this->generateUrl('admin_mailbox_list', ['page' => 1]));
    }

    /**
     * @Route("/list/{page}", name="admin_mailbox_list")
     * @param int $page
     * @return Response
     */
    public function listAction($page = 1)
    {

        $em = $this->getDoctrine()->getManager();

        $summaryQuery = $em->getRepository('MailboxBundle:ConnectedMailbox')->getAllAsQuery();
        $perPage = 20;
        $pagination = $this->get('knp_paginator')->paginate($summaryQuery, $page, $perPage);


        return $this->render('@Mailbox/Admin/Mailbox/index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/update-active", name="admin_mailbox_update_active")
     * @param Request $request
     * @return Response
     */

    public function changeActiveStatus(Request $request) {

        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MailboxBundle:ConnectedMailbox')->find($data['pk']);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono skrzynki pocztowej'));
        }

        $response = [
            'status' => true
        ];

        $entity->setEnabled((bool)$data['value']);
        $em->persist($entity);
        $em->flush();

        return new JsonResponse($response);

    }

    /**
     * @Route("/edit/{id}", name="admin_mailbox_edit")
     * @param Request $request
     * @param int $id
     * @return Response
     */

    public function editAction(Request $request, $id = null)
    {

        $em = $this->getDoctrine()->getManager();

        if ($id) {
            $entity = $em->getRepository('MailboxBundle:ConnectedMailbox')->find($id);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('atlas_mailbox.errors.not_found'));
            }
        } else {
            $entity = new ConnectedMailbox();
        }

        $form = $this->createForm(ConnectedMailboxType::class, $entity);
        $form->setData($entity);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($entity);
            $em->flush();

            if($id)
            {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans(
                        'Skrzynka pocztowa "%name%" została pomyślnie edytowana.',
                        ['%name%' => $entity->getName()]
                    )
                );
            }
            else {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans(
                        'Skrzynka pocztowa "%name%" została pomyślnie utworzona.',
                        ['%name%' => $entity->getName()]
                    )
                );
            }


            return $this->redirect($this->generateUrl('admin_mailbox_list'));
        }

        return $this->render('@Mailbox/Admin/Mailbox/edit.html.twig', [
            'form' => $form->createView(),
            'new' => ($id) ? false : true
        ]);
    }

    /**
     * @Route("/remove/{id}", name="admin_mailbox_remove")
     * @param int $id
     * @return Response
     */

    public function removeAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MailboxBundle:ConnectedMailbox')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono skrzynki pocztowej'));
        }

        $name = $entity->getName();

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Skrzynka pocztowa "%name%" została pomyślnie usunięta.',
                ['%name%' => $name]
            )
        );

        return $this->redirect($this->generateUrl('admin_mailbox_list'));

    }

    /**
     * @Route("/test-connection/{id}", name="admin_mailbox_text_connection")
     * @param int $id
     * @return Response
     */

    public function testConnectionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MailboxBundle:ConnectedMailbox')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono skrzynki pocztowej'));
        }

        $data = [
            'success' => true,
            'msg' => ''
        ];

        $mailboxService = $this->get('atlas.mailbox.management.service');
        $test = $mailboxService->testConnect($entity);

        if($test instanceof \Exception){
            $data['success'] = false;
            $data['msg'] = $test->getMessage();
        };

        return new JsonResponse($data);
    }

}
