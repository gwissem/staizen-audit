<?php

namespace MailboxBundle\Controller;

use AppBundle\Entity\Job;
use CaseBundle\Controller\AjaxController;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Repository\AttributeValueRepository;
use CaseBundle\Utils\TempFileHandler;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\PersistentCollection;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use Elastica\Exception\NotFoundException;
use Exception;
use jamesiarmes\PhpEws\Type\EmailAddressType;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use MailboxBundle\Entity\MailContainer;
use MailboxBundle\Form\Type\ForwardEmailType;
use MailboxBundle\Form\Type\ResendEmailType;
use MailboxBundle\Service\CustomContainerFile;
use MailboxBundle\Service\MailboxManagementService;
use MailboxBundle\Service\MailContainerFile;
use PDO;
use PhpMimeMailParser\Attachment;
use PhpMimeMailParser\Parser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SplFileObject;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;
use UserBundle\Entity\User;

class MailboxController extends Controller
{

    const EMAIL_STATUS_PENDING = 'pending';
    const EMAIL_STATUS_SENT = 'sent';
    const EMAIL_STATUS_ERROR = 'error';


    /**
     * @Route("/get-road-report/{groupProcessInstanceId}", name="get_road_report" )
     * @param Request $request
     * @param $name
     * @return Response
     * @Method("GET")
     */
public function getRoadReport( $groupProcessInstanceId, Request $request)
{
    $html = $this->get('twig')->render('@Mailbox/EmailTemplate/road_report.html.twig', [
        'groupProcessInstanceId' => $groupProcessInstanceId
    ]);
    if($request->get('pdf') == 1 ){
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'gop-preview.pdf',
            'application/pdf',
            'inline'
        );
    }else{
        return new Response($html);
    }
}

    /**
     * @Route("/check-email-status/{id}", name="mailboxCheckEmailStatus", options={"expose"=true})
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws Exception
     */

    public function checkEmailStatusAction(Request $request, $id)
    {

        $note = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find(intval($id));

        if(!$note) {
            throw new NotFoundResourceException();
        }

        $processHandler = $this->get('case.process_handler');

        $status = $processHandler->getAttributeValue('406,226,734', $note->getGroupProcessInstance()->getId(),
            'string', NULL, $note->getId());

        $documentId = null;

        if($status === self::EMAIL_STATUS_SENT) {
            $documentId = $processHandler->getAttributeValue('406,226,228', $note->getGroupProcessInstance()->getId(),
                'int', NULL, $note->getId());
        }

        $data = [
            'id' => $id,
            'status' => $status,
            'attachment' => $documentId
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/save-short-content/{noteId}", name="mailboxSaveShortContent", options={"expose"=true})
     * @param Request $request
     * @Method({"POST"})
     * @param $noteId
     * @return Response
     */

    public function saveShortContentAction(Request $request, $noteId)
    {

        $note = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find(intval($noteId));
        $content = $request->request->get('content', '');



        if(!$note) {
            return new JsonResponse([
                'msg' => 'Błąd zapisywania notatki',

            ], 500);
        }


        $processHandler = $this->get('case.process_handler');

        $noteText = $processHandler->getAttributeValue('406,226,227', $note->getGroupProcessInstance()->getId(),
            null, NULL, $note->getId());

        if(!empty($noteText[0])) {
            $userInfo = $this->get('user.info')->getInfo();

            $processHandler->setAttributeValue(['406,226,227' => $noteText[0]['id']], $content, 'text', 'xxx',
                $note->getGroupProcessInstance()->getId(),
                $userInfo['userId'],
                $userInfo['originalUserId']
            );
        }



        $data = [
            'status' => true,
            'content' => $noteText
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/get-email-template/{name}", name="mailboxGetEmailTemplate" )
     * @param Request $request
     * @param $name
     * @return Response
     * @Method("GET")
     */

    public function getEmailTemplateAction(Request $request, $name)
    {

//        return $this->render('@Mailbox/EmailTemplate/modern_email.html.twig', $request->query->all());
        return $this->render('@Mailbox/EmailTemplate/'.$name.'.html.twig', $request->query->all());

    }


    /**
     * @Route("/get-email-test", name="mailboxGetEmailTest", options={"expose"=true})
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws Exception
     */

    public function testEmailAction(Request $request)
    {

        $mailboxManager = $this->get('atlas.mailbox.management.service');

        $mailbox = $this->getDoctrine()->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => 'atlas_test@starter24.pl'
        ]);

        $emailContainer = $mailboxManager->getLastEmailFromMailbox($mailbox);

        return new Response('<ul><li>dokumentId: <span>'.$emailContainer->getMailFile()->getDocument()->getId().'</span></li></ul>');

    }

    /**
     * @Route("/mail-editor/forward-mail", name="mailboxForwardMail", options={"expose"=true})
     * @param Request $request
     * @param $noteId
     * @return Response
     * @Security("is_granted('ROLE_OPERATIONAL_DASHBOARD')")
     */

    public function forwardMailAction(Request $request)
    {

        $data = $request->request->get('forward-email');


    }

    private function findNoteAttribute($noteId) {

        return $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($noteId);

    }

    /**
     * @Route("/mail-editor", name="mailboxEditor", options={"expose"=true})
     * @param Request $request
     * @Security("is_granted('ROLE_OPERATIONAL_DASHBOARD')")
     * @return Response
     * @throws Exception
     */

    public function publicMailEditorAction(Request $request)
    {

        $mailContainerId = $request->query->get('mail-container', null);

        $noteId = $request->query->get('note-id', null);

        /** @var Document $document */
        $document = null;

        /** @var AttributeValue $rootNote */
        $rootNote = null;

        if($noteId !== null) {

            $noteAttachment = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->findOneBy([
                'attributePath' => '406,226,228',
                'parentAttributeValue' => $noteId
            ]);

            $document = $this->getDoctrine()->getRepository('DocumentBundle:Document')->find($noteAttachment->getValueInt());

            /** @var AttributeValue $parent */
            $parent = $noteAttachment->getParentAttributeValue();
            $rootNote = $parent->getParentAttributeValue();

        }
        else if($mailContainerId !== null) {

            /** @var MailContainer $mailContainer */
            $mailContainer = $this->getDoctrine()->getRepository('MailboxBundle:MailContainer')->find($mailContainerId);
            $document = $mailContainer->getMailFile()->getDocument();

        }

        if(!$document) {
            return new Response('',404);
        }

        return $this->render('@Mailbox/Default/public_form_editor.html.twig', [
            'documentId' => $document->getId(),
            'rootNoteId' => $rootNote->getId() ?? null
        ]);

    }

    /**
     * @Route("/get-email-content/{fileId}", name="mailboxGetEmailContent", options={"expose"=true})
     * @param Request $request
     * @param $fileId
     * @return Response
     */

    public function getEmailInIframeAction(Request $request, $fileId)
    {

        list($html, $parser, $isHtml) = $this->getEmailFileHtml($fileId);

        return $this->render('@Mailbox/Admin/Mailbox/html_blank.html.twig', [
            'html' => $html,
            'isHtml' => $isHtml
        ]);

    }

    private function getAttachmentsOfEmail() {



    }

    /**
     * @param null $fileId
     * @param null $uuid
     * @return mixed|string
     */
    private function getEmailFileHtml($fileId = null, $uuid = null) {

        list($emailFile, $document) = $this->getDocumentAndEmailFile($fileId, $uuid);

        $Parser = new Parser();
        $fs = new Filesystem();

        $rootDir = $this->get('kernel')->getRootDir();
        $emailsDir = $rootDir . MailboxManagementService::FILES_EML_DIR;

        if(!$fs->exists($emailsDir . $emailFile->getPath())) {
            throw new NotFoundException( 'E-mail prawdopodobnie został usunięty.');
        }

        $Parser->setPath($emailsDir . $emailFile->getPath());

        $assetsUrl =  '/files/email_attachments/' . $document->getId() . '/' . $emailFile->getId() . '/inline/';

        $html = $Parser->getMessageBody('html');
        $isHtml = true;

        if(empty($html)) {
            $html = $Parser->getMessageBody('text');
            $isHtml = false;
        }

        $html = str_replace('src="cid:', 'data-cid-src="1" src="' . $assetsUrl, $html);

        /** Usunięcie tagu <head> z każdego HTML'a */
        $html = $this->removeHeadTag($html);

        return [$html, $Parser, $isHtml];

    }

    private function getDocumentAndEmailFile($fileId = null, $uuid = null) {

        $emailFile = null;

        if($fileId) {
            $emailFile = $this->getDoctrine()->getRepository('DocumentBundle:File')->find($fileId);
        }
        elseif($uuid) {
            $emailFile = $this->getDoctrine()->getRepository('DocumentBundle:File')->findOneBy([
                'uuid' => $uuid
            ]);
        }

        if(!$emailFile) {
            throw new NotFoundException();
        }

        /** @var Document $document */
        $document = $emailFile->getDocument();

        return [$emailFile, $document];

    }

    /**
     * @Route("/get-email-preview/{id}/{type}", name="mailboxGetEmailPreview", options={"expose"=true}, defaults={"type"="json"})
     * @param Request $request
     * @param int $id
     * @param $type
     * @return Response
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     * @throws Exception
     */

    public function getEmailAction(Request $request, $id, $type)
    {
        $em = $this->getDoctrine()->getManager();

//        $iframe = $request->query->get('iframe', null);
        $disableReply = $request->query->get('disable-reply', false);

        $iframe = true;

        /** @var Document $document */
        $document = $em->getRepository('DocumentBundle:Document')->find(intval($id));

        if(empty($document)) {
            return new JsonResponse([
                'success' => false,
                'error' => 'Email nie istnieje. Prawdopodobnie został usunięty.'
            ]);
        }

        /** @var PersistentCollection $files */
        $files = $document->getFiles();

        /** @var File|null $firstEmail */
        $firstEmail = $files->first();

        // Zakładam, że zawsze max 1 email
        if($firstEmail) {

            $Parser = new Parser();

            if(!$this->get('mailbox.mail_attachment.service')->existsEmail($firstEmail)) {
                return new JsonResponse([
                    'success' => false,
                    'error' => 'E-mail prawdopodobnie został usunięty.'
                ]);
            }

            $attachments = $this->get('mailbox.mail_attachment.service')->prepareAttachments($Parser, $document, $firstEmail);

            $iframeUrl = null;
            $text = '';
            $html = '';

            if($iframe !== null) {
                $iframeUrl = $this->generateUrl('mailboxGetEmailContent', [
                    'fileId' => $firstEmail->getId()
                ]);
            }
            else {

                $text = $Parser->getMessageBody('text');

                $assets =  '/files/email_attachments/' . $document->getId() . '/' . $firstEmail->getId() . '/inline/';
                //  ((src="cid:)+((?!").)+"+)
                $html = str_replace('src="cid:', 'src="' . $assets, $Parser->getMessageBody('html'));

                /** Usunięcie tagu <head> z każdego HTML'a */
                $html = $this->removeHeadTag($html);

            }

            /** Dane do JSON */
            $fromAddress = [];

            foreach ($Parser->getAddresses('from') as $address) {
                $fromAddress[] = $address['address'];
            }

            $toAddress = [];

            foreach ($Parser->getAddresses('to') as $address) {
                $toAddress[] = $address['address'];
            }

            $dataJson = [
                'from' => $fromAddress,
                'to' => $toAddress,
                'subject' => $Parser->getHeader('subject'),
                'file_uuid' => $firstEmail->getUuid()
            ];

            /** KONIEC */

            $emailsData = [
                'subject' => $Parser->getHeader('subject'),
                'from' => $Parser->getHeader('from'),
                'jsonEmail' => \GuzzleHttp\json_encode($dataJson),
                'to' => $Parser->getHeader('to'),
                'date' => $Parser->getHeader('date'),
                'text' => $text,
                'html' => $html,
                'iframeUrl' => $iframeUrl,
                'attachments' => $attachments,
                'disableReply' => $disableReply,
                'downloadEmailUrl' => $this->get('router')->generate('mailboxDownloadEmail', ['uuid' => $firstEmail->getUuid()])
            ];

            $this->tryGetAddress($emailsData);

            if($type === "html") {
                return $this->render('@Case/FormTemplate/templates/widget-email-preview-content.html.twig', $emailsData);
            }
            else {
                $emailContent = $this->get('twig')->render('@Case/FormTemplate/templates/widget-email-preview-content.html.twig', $emailsData);
            }

        }
        else {
            return new JsonResponse([
                'success' => false,
                'error' => 'Dokument nie posiada żadnego e-maila.'
            ]);
        }

        $data = [
            'success' => true,
            'emailContent' => $emailContent,
        ];

        return new JsonResponse($data);
    }


    /**
     * @Route("/download-email/{uuid}", name="mailboxDownloadEmail", options={"expose"=true}, defaults={"type"="json"})
     * @param Request $request
     * @param $uuid
     * @return Response
     */

    public function downloadEmailAction($uuid)
    {

        // TODO - jakieś uprawnienia do pobierania?

        $em = $this->getDoctrine()->getManager();

        /** @var File $file */
        $file = $em->getRepository('DocumentBundle:File')->findOneBy([
            'uuid' => $uuid
        ]);

        if($file === null) {
            return new Response('', 404);
        }

        $fileContent = file_get_contents($this->getParameter('files_directory').$file->getPath());

        if(!$fileContent) {
            throw new NotFoundHttpException();
        }

        $response = new Response();
        $response->headers->set('Content-Type', $file->getMimeType());
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$file->getName());
        $response->setStatusCode(200);
        $response->setContent($fileContent);

        return $response;

    }

        /**
     * @param $html
     * @return string
     */
    private function removeHeadTag($html) {

        if(strpos($html, '<html') !== FALSE) {

            $crawler = new Crawler();
            $crawler->addHTMLContent($html, 'UTF-8');

            if($crawler->nodeName() === "html") {

                $crawler->filter('head')->each(function (Crawler $crawler) {
                    $node = $crawler->getNode(0);
                    $node->parentNode->removeChild($node);
                });

                $crawler->filter('base')->each(function (Crawler $crawler) {
                    $node = $crawler->getNode(0);
                    $node->parentNode->removeChild($node);
                });

                return $crawler->html();

            }

        }

        return $html;

    }

    /**
     * @Route("/inbox-editor/save-email/{rootNoteId}", name="mailboxSaveEmailInboxEditor", options={"expose"=true})
     * @Method({"POST"})
     * @param Request $request
     * @param $rootNoteId
     * @return Response
     */

    public function saveEmailInboxEditorAction(Request $request, $rootNoteId)
    {

        $form = $request->request->all();
        $controls = $form['controls'];
        $isForward = $request->request->get('isForward', false);
        $fileUuid = $form['fileUuid'];

        try {
            if(empty($controls['406,226,198']['value']) || empty($controls['406,226,737']['value']) || empty($controls['406,226,409']['value'])) {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Nie wszystkie wymagane pola zostały uzupełnione.'
                ]);
            }
        }
        catch (Exception $e) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Niepoprawny formularz.'
            ]);
        }

        if(!$this->validEmails($controls['406,226,198']['value'])) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Adres odbiorcy jest niepoprawny.'
            ]);
        }

        if($isForward) {
            $files = $this->saveAttachmentsFromOtherEmails($fileUuid);
        }
        else {
            $files = $form['files'];
        }

        $note = $this->saveNoteEmail($controls, $files);

        $data = [
            'success' => true,
            'notes' => [$note]
        ];

        return new JsonResponse($data);
    }

    /**
     * @param $fileUuid
     * @return array
     */
    private function saveAttachmentsFromOtherEmails($fileUuid) {

        /** @var Document $document */
        list($emailFile, $document) = $this->getDocumentAndEmailFile(null, $fileUuid);

        $em = $this->getDoctrine()->getManager();

        $pathToAttachments = $this->get('mailbox.mail_attachment.service')->getUniquePathToAttachment($document,$emailFile);
        $attachments = $this->get('mailbox.mail_attachment.service')->getAttachmentsOfDocument($document->getId());

        $attachUuid = [];

        foreach ($attachments as $attachment) {

            /** @var TempFileHandler $tempFile */
            $tempFile = TempFileHandler::fromFile($pathToAttachments . $attachment['name']);

            $file = new CustomContainerFile($tempFile->path , $attachment['name']);

            $mailFile = $this->get('document.uploader')->upload($file);

            $em->persist($mailFile);
            $attachUuid[] = $mailFile->getUuid();

        }

        $em->flush();

        return $attachUuid;

    }

    private function validEmails($emails) {

        try{

            foreach (explode(',', $emails) as $email) {

                if(!$email) continue;

                if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    return false;
                }


            }

        }
        catch (\Exception $exception) {
            return false;
        }

        return true;

    }

    /**
     * @Route("/inbox-editor/get-content/{rootNoteId}", name="mailboxGetInboxEditor", options={"expose"=true})
     * @param Request $request
     * @param $rootNoteId
     * @return Response
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */

    public function getInboxEditorAction(Request $request, $rootNoteId)
    {

        $rootNote = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find(intval($rootNoteId));

        $data = $request->query->all();
        $isForward = $request->query->get('isForward', false);


        $noteHandler = $this->get('note.handler');

        /** @var ProcessInstance $rootProcess */
        $rootProcess = $rootNote->getRootProcess();

        $rootId = ($rootProcess) ? $rootProcess->getId() : null;

        $mailboxes = $this->getDoctrine()->getRepository('MailboxBundle:ConnectedMailbox')->findAll();
        $senderBook = [];

        foreach ($mailboxes as $item) {
            $senderBook[] = $item->getLogin() . "@starter24.pl";
        }
        $firstNote = $noteHandler->getNoteStructure($rootNote);

        
//        Incoming emails
        if(isset($firstNote['direction']) && $firstNote['direction'] ==1 )
        {
            if(isset($firstNote['value'])&& $firstNote['value'] !=='')
            {
                $data['from'] = $firstNote['value'];
            }
        }


        $structure = $noteHandler->createNoteStructure($rootNote);
        $noteStructure = [];

        foreach ($structure as $value) {
            $noteStructure[str_replace('406,226,', '', $value['attribute_path'])] = $value;
        }

        $emailContent = '';

            if (isset($data['file_uuid']) && !empty($data['file_uuid'])) {

                /** @var Parser $parser */
                list($emailContent, $parser) = $this->getEmailFileHtml(null, $data['file_uuid']);

                $separator = $this->getReplySeparator($parser, $isForward);
                $emailContent = '<br><br>' . $separator . $emailContent;

            }

        $fromNote =[];

        if($isForward) {

            /** @var Document $document */
            list($emailFile, $document) = $this->getDocumentAndEmailFile(null, $data['file_uuid']);

            $attachments = $this->get('mailbox.mail_attachment.service')->getAttachmentsOfDocument($document->getId());

            $data['to'] = '';
        }
        else {
//            So it's reply
            $fromNote = $data['to'];
            $data['to'] = $data['from'];

            $attachments = [];
        }


        if(!empty($fromNote))
        {
            if(in_array($fromNote[0],$senderBook)!== false){
                //            Wyciągamy maila z naszej listy i wrzucamy go na górę
                unset($senderBook[in_array($fromNote[0],$senderBook)]);
                array_unshift($senderBook,$fromNote[0]);
            }

        }

        $inboxEditor = $this->get('twig')->render('@Case/FormTemplate/templates/inbox-editor.html.twig', [
            'subject' => (isset($data['subject'])) ? $data['subject'] : '',
            'to' => (isset($data['to'])) ? $data['to'] : null,
            'from' => (isset($data['from'])) ? $data['from'] : 'doatlasa@starter24.pl',
            'note' => $noteStructure,
            'senderBook' => $senderBook,
            'rootId' => $rootNote->getId(),
            'emailContent' => $emailContent,
            'signature' => $this->generateSignature(),
            'isForward' => $isForward,
            'attachments' => $attachments,
            'emailFileUuid' => (isset($data['file_uuid']) ? $data['file_uuid'] : '')
        ]);

        $addressBook = [];


        if($rootId) {

            $parameters = [
                [
                    'key' => 'rootId',
                    'value' => (int)$rootId,
                    'type' => PDO::PARAM_INT
                ]
            ];

            $query = 'EXEC dbo.p_case_contact @rootId = :rootId, @email = 1';

            $addressBookArray = $this->get('app.query_manager')->executeProcedure($query, $parameters);

            foreach ($addressBookArray as $item) {
                $addressBook[] = [
                    "id" => $item['value'],
                    "label" => $item['label']
                ];
            }

        }

        $data = [
            'addressBook' => $addressBook,
            'inboxEditor' => $inboxEditor
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/inbox-editor/resend-email/{noteId}", name="mailboxSaveAndSendEmail", options={"expose"=true})
     * @Method({"POST"})
     * @param Request $request
     * @param $noteId
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    public function saveAndSendEmailAction(Request $request, $noteId)
    {

        $data = $request->request->get('resend_email');

        $noteAttribute = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($noteId);

        if(empty($noteAttribute)) {
            return new JsonResponse(['success' => false, 'msg' => 'Nie znaleziono notatki.'], 404);
        }

        $noteHandler = $this->get('note.handler');

        $status = $noteHandler->setNoteValue($noteAttribute, '406,226,198', $data['receiver'], AttributeValue::VALUE_STRING);

        if($status) {
            $status = $noteHandler->resendEmail($noteAttribute);
        }

        $note = $noteHandler->getNoteStructure($noteAttribute);

        $data = [
            'success' => ($status) ? true : false,
            'note' => $note
        ];

        return new JsonResponse($data);

    }

    /**
     * @Route("/inbox-editor/preview-email-with-form/{noteId}", name="mailboxRenderPreviewForResendEmail", options={"expose"=true})
     * @Method({"GET"})
     * @param Request $request
     * @param $noteId
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    public function renderPreviewForResendEmailAction(Request $request, $noteId)
    {

        $noteAttribute = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($noteId);

        if(empty($noteAttribute)) {
            return new JsonResponse(['success' => false, 'msg' => 'Nie znaleziono notatki.'], 404);
        }

        $noteHandler = $this->get('note.handler');

        $note = $noteHandler->getNoteStructure($noteAttribute);

        $data = [
            'receiver' => $note['value'],
            'sender' => $note['sender'],
            'subject' => $note['subject']
        ];

        $form = $this->createForm(ResendEmailType::class, $data , []);

        $formView = $this->renderView('@Mailbox/Default/resend-email-form.html.twig', [
            'form' => $form->createView(),
            'noteId' => $noteAttribute->getId()
        ]);

        return new JsonResponse([
            'success' => true,
            'note' => $note,
            'html' => $formView
        ]);

    }

    /**
     *
     * @Route("/inbox-editor/preview-note-content-email/{noteId}", name="mailboxPreviewNoteContentEmail", options={"expose"=true})
     * @Method({"GET"})
     * @param Request $request
     * @param $noteId
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    public function previewContentRootEmailAction(Request $request, $noteId)
    {

        $noteAttribute = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($noteId);

        if(empty($noteAttribute)) {
            $this->createNotFoundException('Nie znaleziono e-maila.');
        }

        $emailContent = $this->get('note.handler')->getEmailContent($noteAttribute);

        if(empty($emailContent)) {
            $emailContent = '';
        }

        return $this->render('@Mailbox/Admin/Mailbox/html_blank.html.twig', [
            'html' => $emailContent,
            'isHtml' => true
        ]);

    }

    private function generateSignature() {

        /** @var User $user */
        $user = $this->getUser();

        $worker = $this->get('user.info')->getWorker($user->getId(), $this->get('app.query_manager'));

        $position = ($worker && isset($worker['position'])) ? $worker['position'] : null;

        return $this->renderView('@Mailbox/Default/inbox-signature.html.twig', [
            'username' => ($user) ? $user->getName() : null,
            'position' => $position
        ]);

    }

    private function getReplySeparator(Parser $parser, $isForward = false) {

        $formatter = new \IntlDateFormatter($this->getParameter('locale'), \IntlDateFormatter::FULL, \IntlDateFormatter::MEDIUM);
        $date = $formatter->format((new \DateTime($parser->getHeader('date'))));
        $subject = $parser->getHeader('subject');

        $tempArray = [];

        foreach ($parser->getAddresses('from') as $address) {
            $tempArray[] = $address['display'] . " [<a href=\"mailto:" . $address['address'] . "\" target=\"_blank\">" . $address['address'] . "</a>" . "]";
        }

        $from = implode(", ", $tempArray);

        $tempArray = [];

        foreach ($parser->getAddresses('to') as $address) {
            $tempArray[] = $address['display'] . " [" . $address['address'] . "]";
        }

        $to = implode(", ", $tempArray);

        $separator = ($isForward) ? "__________ Forwarded message __________<br>\n" :  "_______________________________________<br>\n";
        $separator .= "Od: " . $from . "<br>\n";
        $separator .= "Wysłano: " . $date . "<br>\n";
        $separator .= "Do: " . $to . "<br>\n";
        $separator .= "Temat: " . $subject . "<br>\n";
        $separator .= "<br>\n";

        return $separator;

    }

    private function saveFilesInNote($files, AttributeValueRepository $repository, $groupProcessInstanceId, AttributeValue $parentNode, ObjectManager $em) {

        // usunięcie pustych plików
        foreach ($files as $key => $uuid) {
            if(empty($uuid)) {
                unset($files[$key]);
            }
        }

        if(count($files)) {

            $fileRepository = $this->getDoctrine()->getRepository('DocumentBundle:File');
            /** @var User $user */
            $user = $this->getUser();

            $document = new Document();
            $document->setName('email_attachments_'.date('Ymd_His'));
            if($user->getCompany()) {
               $document->addCompany($user->getCompany());
            }
            $document->setCreatedBy($user);

            foreach ($files as $uuid) {

                //                $fileEntity = $fileRepository->find(intval($file));
                /** @var File $fileEntity */
                $fileEntity = $fileRepository->findOneBy([
                    'uuid' => $uuid
                ]);

                if($fileEntity) {
                    $fileEntity->setDocument($document);
                    $document->addFile($fileEntity);
                    $em->persist($fileEntity);
                }
            }

            if(count($document->getFiles())) {
                $em->persist($document);
                $em->flush();

                /** @var AttributeValue $noteAttachments */
                $noteAttachments = $repository->findOneBy([
                    'attributePath' => '406,226,736',
                    'groupProcessInstance' => $groupProcessInstanceId,
                    'parentAttributeValue' => $parentNode->getId()
                ]);
                $noteAttachments->setValueString($document->getId());
                $em->persist($noteAttachments);
                $em->flush();

            }

        }


    }

    public function getNoteStructure($rootNoteId, $groupProcessInstanceId) {

        $structure = [];
        $finallyStructure = [];
        $emptyNoteId = null;

        $processHandler = $this->get('case.process_handler');
        $instanceId = $processHandler->getActiveInstanceForRoot($groupProcessInstanceId);

        if($instanceId) {
            $formControls = $processHandler->formControls($instanceId);

            $keys = array_keys(array_column($formControls, 'attribute_path'), AjaxController::NOTE_CONTENT);

            foreach ($keys as $key) {
                if ($key && $formControls[$key]['value_text'] === null) {
                    $emptyNoteId = intval($formControls[$key]['parent_attribute_value_id']);
                    break;
                }
            }

            if ($emptyNoteId) {

                $structure = array_filter($formControls, function ($v, $k) use ($formControls, $emptyNoteId) {
                    if ($v['parent_attribute_value_id'] == $emptyNoteId) {
                        $formControls[$k]['id'] = $v['attribute_value_id'];
                        return true;
                    }
                    return false;
                }, ARRAY_FILTER_USE_BOTH);

            } else {
                $structure = $this->get('case.process_handler')->attributeAddStructure($rootNoteId, AjaxController::NOTE_ID);
            }

        }

        foreach ($structure as $value) {
            $finallyStructure[str_replace('406,226,', '', $value['attribute_path'])] = $value;
        }

        return $finallyStructure;

    }

    private function saveNoteEmail($data, $files) {

        $processHandler = $this->get('case.process_handler');
        $noteHandler = $this->get('note.handler');

        $repository = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue');
        $em = $this->getDoctrine()->getManager();

        $first = reset($data);

        /** @var AttributeValue $parentNode */
        $parentNode = $repository->find(intval($first['parent_attribute_value_id']));
        $groupProcessInstanceId = $parentNode->getGroupProcessInstance()->getId();

        $this->saveFilesInNote($files, $repository, $groupProcessInstanceId, $parentNode, $em);

        /** @var User $user */
        $userInfo = $this->get('user.info')->getInfo();

        foreach ($data as $path => $attributeValueArray) {
            /** @var AttributeValue $attributeValue */
            $attributeValue = $repository->find(intval($attributeValueArray['id']));
            $processHandler->setAttributeValue(
                [$path => $attributeValueArray['id']],
                true === isset($attributeValueArray['value']) ? $attributeValueArray['value'] : '',
                $attributeValue->getAttribute()->getNameValueType(),
                'xxx',
                $attributeValue->getGroupProcessInstance()->getId(),
                $userInfo['userId'],
                $userInfo['originalUserId']
            );

        }

        $this->saveAdditionalDataOfNote($repository, $groupProcessInstanceId, $parentNode, $em, $data['406,226,737']['value']);

        $job =  new Job('atlas:mailbox:note-send', ['noteId' => $parentNode->getId()]);
        $em->persist($job);
        $em->flush();

        return $noteHandler->getNoteStructure($parentNode);

    }

    private function saveAdditionalDataOfNote(AttributeValueRepository $repository, $groupProcessInstanceId,
                                              AttributeValue $parentNode, ObjectManager $em, $emailContent) {

        $rootProcessInstance = $parentNode->getRootProcess();

        /** @var AttributeValue $direction - Kierunek wiadomości: wychodzący */
        $direction = $repository->findOneBy([
            'attributePath' => '406,226,407',
            'rootProcess' => $rootProcessInstance,
            'parentAttributeValue' => $parentNode->getId()
        ]);

        $direction->setValueInt('1');
        $em->persist($direction);

        /** @var AttributeValue $noteContent - Treść notatki: skrót emaila */
        $noteContent = $repository->findOneBy([
            'attributePath' => '406,226,227',
            'rootProcess' => $rootProcessInstance,
            'parentAttributeValue' => $parentNode->getId()
        ]);

        $parsedContent = strip_tags($emailContent);
        $parsedContent = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $parsedContent);
        $shortContent = (mb_strlen($parsedContent) > 200) ? (mb_substr($parsedContent, 0, 200) . '...') : $parsedContent;

        $noteContent->setValueText($shortContent);
        $em->persist($noteContent);

        /** @var AttributeValue $type - Typ notatki: email */
        $type = $repository->findOneBy([
            'attributePath' => '406,226,229',
            'rootProcess' => $rootProcessInstance,
            'parentAttributeValue' => $parentNode->getId()
        ]);
        $type->setValueString('email');
        $em->persist($type);


        /** @var AttributeValue $status - Status notatki: pending */
        $status = $repository->findOneBy([
            'attributePath' => '406,226,734',
            'rootProcess' => $rootProcessInstance,
            'parentAttributeValue' => $parentNode->getId()
        ]);
        $status->setValueString('pending');
        $em->persist($status);

        $em->flush();
    }


    private function tryGetAddress(&$emailsData) {
        preg_match_all('/([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6})/i', $emailsData['from'], $matches, PREG_PATTERN_ORDER);
        if(!empty($matches) && !empty($matches[1])) {
            $emailsData['from_address'] = $matches[1][0];
        }
        else {
            $emailsData['from_address'] = '';
        }
    }

    function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

}
