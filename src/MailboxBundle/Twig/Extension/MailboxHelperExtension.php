<?php

namespace MailboxBundle\Twig\Extension;

use MailboxBundle\Entity\ConnectedMailbox;

class MailboxHelperExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('intervalCron', array($this, 'parseIntervalCron')),
        );
    }

    public function parseIntervalCron($seconds)
    {
        $availableInterval = ConnectedMailbox::getAvailableIntervalArray();

        foreach ($availableInterval as $key => $item) {
            if($item == $seconds)
            {
                return $key;
            }
        }

        return $seconds;

    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'mailbox_helper';
    }
}
