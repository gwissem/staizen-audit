var cwic =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var api = __webpack_require__(1);
	var cwicState = __webpack_require__(9);
	var $ = __webpack_require__(6);
	var triggerError = __webpack_require__(10);
	var log = __webpack_require__(3);
	var setGlobalHandlers = __webpack_require__(99);
	
	var multimediaControllerModule = __webpack_require__(100);
	var telephonyControllerModule  = __webpack_require__(108);
	var loginControllerModule = __webpack_require__(118);
	var SystemControllerModule = __webpack_require__(120);
	var WindowControllerModule = __webpack_require__(126);
	var CertificateControllerModule = __webpack_require__(136);
	var PreviewVideoWindow     = __webpack_require__(135);
	
	// setup cwic "global" state
	cwicState.resetGlobals();
	
	setGlobalHandlers();
	
	// the jQuery plugin
	/**
	 * @description
	 * CWIC is a jQuery plug-in to access the Cisco Web Communicator<br>
	 * Audio and Video media require the Cisco Web Communicator add-on to be installed <br>
	 * <h3>Fields overview</h3>
	 * <h3>Methods overview</h3>
	 * All cwic methods are called in the following manner<br>
	 * <pre class="code">$('#selector').cwic('method',parameters)</pre><br>
	 * <h3>Events overview</h3>
	 * All events are part of the cwic namespace.  For example:
	 * <ul>
	 * <li>conversationStart.cwic</li>
	 * <li>system.cwic</li>
	 * <li>error.cwic</li>
	 * </ul>
	 * <h4>Example conversation events:</h4>
	 * These are conversation-related events that can be triggered by the SDK.<br>
	 * The event handlers are passed the conversation properties as a single object. For example:<br>
	 * @example
	 * // start an audio conversation with phone a number and bind to conversation events
	 * jQuery('#conversation')
	 *   .cwic('startConversation', '+1 234 567')  // container defaults to $(this)
	 *   .bind('conversationStart.cwic', function(event, conversation, container) {
	    *      console.log('conversation has just started');
	    *      // container is jQuery('#conversation')
	    *    })
	 *    .bind('conversationUpdate.cwic', function(event, conversation) {
	    *      console.log('conversation has just been updated');
	    *    })
	 *    .bind('conversationEnd.cwic', function(event, conversation) {
	    *      console.log('conversation has just ended');
	    *    });
	 * @example
	 * // listen for incoming conversation
	 * jQuery('#phone')
	 *   .bind('conversationIncoming.cwic', function(event, conversation, container) {
	    *     console.log('incoming conversation with id ' + conversation.id);
	    *     // attach the 'toast' container to the DOM and bind to events
	    *     container
	    *       .appendTo('#phone')
	    *       .bind('conversationUpdate.cwic', function(event, conversation) {
	    *         // update on incoming conversation
	    *       })
	    *       .bind('conversationEnd.cwic', function(event, conversation) {
	    *         // incoming conversation has ended
	    *         container.remove();
	    *       });
	    *     // suppose UI has a button with id 'answer'
	    *     jQuery('#answer').click(function() {
	    *       // answer the incoming conversation
	    *       // conversation has an id property, so startConversation accepts it
	    *       // use element #conversation as container
	    *       jQuery('#conversation').cwic('startConversation', conversation);
	    *       // remove incoming container
	    *       container.remove();
	    *     });
	    *   });
	 * @class
	 * @static
	 * @param {String} method The name of the method to call
	 * @param {Variable} arguments trailing arguments are passed to the specific call see methods below
	 */
	$.fn.cwic = function(method) {
	  log(false, 'Calling API function: ' + method);

	  try {
	    // Method calling logic
	    if (api[method]) {
	      return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
	
	    } else if (typeof method === 'object' || !method) {
	      return api.init.apply(this, arguments);
	
	    } else {
	      throw method + ': no such method on jQuery.cwic';
	    }
	
	  } catch (e) {
	    if (typeof console !== 'undefined') {
	      if (console.trace) {
	        console.trace();
	      }
	
	      if (console.log && e.message) {
	        console.log('Exception occured in $.fn.cwic() ' + e.message);
	      }
	    }
	
	    triggerError(this, e);
	  }
	
	};
	
	//function CwicLibrary()
	//{
	//    this.MultimediaController = multimediaControllerModule.MultimediaController;
	//}
	
	module.exports.MultimediaController  = multimediaControllerModule.MultimediaController;
	module.exports.TelephonyController   = telephonyControllerModule.TelephonyController;
	module.exports.LoginController       = loginControllerModule.LoginController;
	module.exports.SystemController      = SystemControllerModule.SystemController;
	module.exports.WindowController      = WindowControllerModule.WindowController;
	module.exports.CertificateController = CertificateControllerModule.CertificateController;
	// module.exports.PreviewVideoWIndow   = PreviewVideoWindow.PreviewVideoWindow;


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var externalWindow = __webpack_require__(2);
	var inBrowserVideo = __webpack_require__(14);
	
	// a map with all exposed methods
	module.exports = {
	  about: __webpack_require__(15),
	  init: __webpack_require__(16),
	  shutdown: __webpack_require__(41),
	  rebootIfBroken: __webpack_require__(74),
	
	  registerPhone: __webpack_require__(75),
	  manualSignIn: __webpack_require__(75),
	  switchPhoneMode: __webpack_require__(77),
	  unregisterPhone: __webpack_require__(27),
	  startDiscovery: __webpack_require__(78),
	  cancelSSO: __webpack_require__(79),
	  resetData: __webpack_require__(80),
	  signOut: __webpack_require__(26),
	
	  startConversation: __webpack_require__(81),
	  updateConversation: __webpack_require__(82),
	  endConversation: __webpack_require__(83),
	
	  createVideoWindow: inBrowserVideo.createVideoWindow,
	  addPreviewWindow: inBrowserVideo.addPreviewWindow,
	  removePreviewWindow: inBrowserVideo.removePreviewWindow,
	
	  sendDTMF: __webpack_require__(84),
	  getInstanceId: __webpack_require__(85),
	  getMultimediaDevices: __webpack_require__(86),
	  setRecordingDevice: __webpack_require__(87),
	  setPlayoutDevice: __webpack_require__(88),
	  setCaptureDevice: __webpack_require__(89),
	  setRingerDevice: __webpack_require__(90),
	  getUserAuthStatus: __webpack_require__(91),
	  showUserAuthorization: __webpack_require__(44),
	
	  showCallInExternalWindow: externalWindow.showCallInExternalWindow,
	  hideExternalWindow: externalWindow.hideExternalWindow,
	  showPreviewInExternalWindow: externalWindow.showPreviewInExternalWindow,
	  setExternalWindowAlwaysOnTop: externalWindow.setExternalWindowAlwaysOnTop,
	  setExternalWindowShowSelfViewPip: externalWindow.setExternalWindowShowSelfViewPip,
	  setExternalWindowShowControls: externalWindow.setExternalWindowShowControls,
	  setExternalWindowTitle: externalWindow.setExternalWindowTitle,
	  getExternalWindowState: externalWindow.getExternalWindowState,
	  setExternalWindowSelfViewPipPosition: externalWindow.setExternalWindowSelfViewPipPosition,
	  setExternalWindowShowSelfViewPipBorder: externalWindow.setExternalWindowShowSelfViewPipBorder,
	
	  dock: __webpack_require__(64),
	  undock: __webpack_require__(92),
	
	  setSpeakerVolume: __webpack_require__(93),
	  setRingerVolume: __webpack_require__(94),
	  setMicrophoneVolume: __webpack_require__(95),
	  setRingtone: __webpack_require__(96),
	  setPlayRingerOnAllDevices: __webpack_require__(97),
	  getMultimediaDeviceVolume : __webpack_require__(98)
	};

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	var dockGlobals = __webpack_require__(12);
	var cwicState = __webpack_require__(9);
	var utils = __webpack_require__(13);
	
	
	
	/**
	 * Shows the call in an external video window.  If an external video window already exists,
	 * the current contents will be replaced by the video stream for the selected call.  Otherwise, a new external window will be created.
	 * To detect changes in the window state, for example the user closes the window, use {@link $.fn.cwic#event:externalWindowEvent}.
	 * <br>
	 * By default the external video window will have always on top property and will include a picture-in-picture preview (self-view).
	 * This can be changed using {@link $.fn.cwic-setExternalWindowAlwaysOnTop} and {@link $.fn.cwic-setExternalWindowShowSelfViewPip}, respectively.
	 * <br>
	 * If the user closes an external video window that contains a video call, the call will be ended.
	 * Use {@link $.fn.cwic-hideExternalWindow} to remove the window without interupting the call.
	 * @since 3.1.0
	 * @memberof $.fn.cwic
	 * @param {String|Object} [id] A {String} conversation identifier or an {Object} containing an id property.
	 */
	function showCallInExternalWindow() {
	  log(true, 'showCallInExternalWindow');
	
	  var $this = this;
	  var conversation = $this.data('cwic');
	  var conversationId = conversation ? conversation.id : null;
	
	  // inspect arguments
	  if (arguments.length > 0) {
	    if (typeof arguments[0] === 'object') {
	      conversation = arguments[0];
	      conversationId = conversation.id;
	
	    } else if (typeof arguments[0] === 'string') {
	      conversationId = arguments[0];
	    }
	
	  }
	
	  if (!conversationId) {
	    return triggerError($this, errorMap.InvalidArguments, 'wrong arguments (showCallInExternalWindow)', arguments);
	  }
	
	  dockGlobals.isVideoBeingReceived = true;
	  sendClientRequest('showCallInExternalWindow', {
	    callId: conversationId
	  });
	}
	
	/**
	 * Shows preview (self-view) in an external video window.  If an external video window already exists,
	 * the current contents will be replaced by the preview.  Otherwise, a new external window will be created.
	 * To detect changes in the window state, for example the user closes the window, use {@link $.fn.cwic#event:externalWindowEvent}.
	 * <br>
	 * By default the external video window will have always on top property.  This can be changed using {@link $.fn.cwic-setExternalWindowAlwaysOnTop}.
	 * <br>
	 * If preview in picture-in-picture is enabled (see {@link $.fn.cwic-setExternalWindowShowSelfViewPip})
	 * it will not be visible while the preview is in the full window.
	 * <br>
	 * Use {@link $.fn.cwic-hideExternalWindow} to remove the window.
	 * @since 3.1.0
	 * @memberof $.fn.cwic
	 */
	function showPreviewInExternalWindow() {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'showPreviewInExternalWindow');
	
	    dockGlobals.isVideoBeingReceived = true;
	    sendClientRequest('showPreviewInExternalWindow', {
	      windowType: 'remote'
	    });
	
	  } else {
	    log(false, 'returning from showPreviewInExternalWindow ... not supported in the current state');
	  }
	}
	
	/**
	 * Triggers an {@link $.fn.cwic#event:externalWindowEvent} to be sent to the application with the current state of the external window.
	 * @since 3.1.0
	 * @memberof $.fn.cwic
	 */
	function getExternalWindowState() {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'getExternalWindowState');
	
	    sendClientRequest('getExternalWindowState', {
	      windowType: 'remote'
	    });
	
	  } else {
	    log(false, 'returning from getExternalWindowState ... not supported in the current state');
	  }
	}
	
	/**
	 * Hides an external video window created by {@link $.fn.cwic-showPreviewInExternalWindow} or {@link $.fn.cwic-showCallInExternalWindow}.
	 * @since 3.1.0
	 * @memberof $.fn.cwic
	 */
	function hideExternalWindow() {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'hideExternalWindow');
	
	    dockGlobals.isVideoBeingReceived = false;
	    sendClientRequest('hideExternalWindow', {
	      windowType: 'remote'
	    });
	
	  } else {
	    log(false, 'returning from hideExternalWindow ... not supported in the current state');
	  }
	}
	
	/**
	 * Controls whether external video windows created by {@link $.fn.cwic-showPreviewInExternalWindow} or
	 * {@link $.fn.cwic-showCallInExternalWindow} are shown always on top (default) or not.
	 * @since 3.1.0
	 * @param {Boolean} isAlwaysOnTop Set to false to remove the always on top property.  Set to true to restore default behavior.
	 * @memberof $.fn.cwic 
	 */
	function setExternalWindowAlwaysOnTop() {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'setExternalWindowAlwaysOnTop');
	
	    if (typeof arguments[0] === 'boolean') {
	      sendClientRequest('setExternalWindowAlwaysOnTop', { 
	        alwaysOnTop: arguments[0],
	        windowType: 'remote' 
	      });
	    }
	
	  } else {
	    log(false, 'returning from setExternalWindowAlwaysOnTop ... not supported in the current state');
	  }
	}
	
	/**
	 * Controls whether a picture-in-picture preview (self-view) is shown when {@link $.fn.cwic-showCallInExternalWindow} is used to put a call in external video window.
	 * @since 3.1.0
	 * @param {Boolean} showPipSelfView Set to false to turn off the picture-in-picture.  Set to true to restore default behavior.
	 * @memberof $.fn.cwic
	 */
	function setExternalWindowShowSelfViewPip() {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'setExternalWindowShowSelfViewPip');
	
	    if (typeof arguments[0] === 'boolean') {
	      sendClientRequest('setExternalWindowShowSelfViewPip', { showSelfViewPip: arguments[0] });
	    }
	
	  } else {
	    log(false, 'returning from setExternalWindowShowSelfViewPip ... not supported in the current state');
	  }
	}
	
	/**
	 * Controls whether a overlaid controls are shown in external video window created by {@link $.fn.cwic-showCallInExternalWindow} or {@link $.fn.cwic-showPreviewInExternalWindow}.
	 * @since 4.0.0
	 * @param {Boolean} showControls Set to false to turn off the overlaid call controls. Set to true to restore default behavior.
	 * @memberof $.fn.cwic
	 */
	function setExternalWindowShowControls(showControls) {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'setExternalWindowShowControls');
	
	    if (typeof showControls === 'boolean') {
	      sendClientRequest('setExternalWindowShowControls', {
	        showControls: showControls,
	        windowType: 'remote'
	      });
	    }
	
	  } else {
	    log(false, 'returning from setExternalWindowShowControls ... not supported in the current state');
	  }
	}
	
	/**
	 * Sets the position of self view window relative to parent external video window created by {@link $.fn.cwic-showCallInExternalWindow}.
	 * @since 4.0.0
	 * @param {Object} coordinatesInPercentage
	 * @param {Number|String} coordinatesInPercentage.topLeftX Distance of top-left corner of self view window from the left edge of parent window (in percentage - 0 to 100).
	 * @param {Number|String} coordinatesInPercentage.topLeftY Distance of top-left corner of self view window from the top edge of parent window (in percentage - 0 to 100).
	 * @param {Number|String} coordinatesInPercentage.bottomRightX Distance of bottom-right corner of self view window from the left edge of parent window (in percentage - 0 to 100).
	 * @param {Number|String} coordinatesInPercentage.bottomRightY Distance of bottom-right corner of self view window from the top edge of parent window (in percentage - 0 to 100).
	 * @memberof $.fn.cwic
	 */
	function setExternalWindowSelfViewPipPosition(coordinatesInPercentage) {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'setExternalWindowSelfViewPipPosition');
	
	    sendSelfViewCoordinates(coordinatesInPercentage);
	
	  } else {
	    log(false, 'returning from setExternalWindowShowControls ... not supported in the current state');
	  }
	}
	
	function sendSelfViewCoordinates(coordinatesInPercentage) {
	  if (!utils.isObject(coordinatesInPercentage)) {
	    log(false, 'setExternalWindowSelfViewPipPosition expects object as an argument');
	
	    return;
	  }
	
	  var left = parseInt(coordinatesInPercentage.topLeftX, 10),
	    top = parseInt(coordinatesInPercentage.topLeftY, 10),
	    right = parseInt(coordinatesInPercentage.bottomRightX, 10),
	    bottom = parseInt(coordinatesInPercentage.bottomRightY, 10);
	
	
	  if (
	    !utils.isNumeric(left) ||
	    !utils.isNumeric(top) ||
	    !utils.isNumeric(right) ||
	    !utils.isNumeric(bottom)
	  ) {
	    log(false, 'setExternalWindowSelfViewPipPosition invalid arguments ... expecting numbers from 0 to 100');
	
	    return;
	  }
	
	  sendClientRequest('setExternalWindowSelfViewPipPosition', {pipLeft: left, pipTop: top, pipRight: right, pipBottom: bottom});
	}
	
	/**
	 * Sets the window title used in external video windows created by {@link $.fn.cwic-showPreviewInExternalWindow} or {@link $.fn.cwic-showCallInExternalWindow}.
	 * @since 3.1.0
	 * @param {String} title A string value to be used as the window title for the exernal video window.
	 * @memberof $.fn.cwic
	 */
	function setExternalWindowTitle() {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'setExternalWindowTitle');
	
	    if (typeof arguments[0] === 'string') {
	      sendClientRequest('setExternalWindowTitle', { 
	        title: arguments[0],
	        windowType: 'remote' 
	      });
	    }
	
	  } else {
	    log(false, 'returning from setExternalWindowTitle ... not supported in the current state');
	  }
	}
	
	/**
	 * Show/hide the border of self view in external video window created by {@link $.fn.cwic-showCallInExternalWindow}.
	 * @since 11.0.1
	 * @param {Boolean} showSelfViewPipBorder
	 * @memberof $.fn.cwic
	 */
	function setExternalWindowShowSelfViewPipBorder(showSelfViewPipBorder) {
		if (cwicState.isMultimediaStarted) {
			log(true, 'setExternalWindowShowSelfViewPipBorder');
			
			if (typeof showSelfViewPipBorder === 'boolean') {
				sendClientRequest('setExternalWindowShowSelfViewPipBorder', { showSelfViewPipBorder: showSelfViewPipBorder });
			}
			
		} else {
			log(false, 'returning from setExternalWindowShowSelfViewPipBorder ... not supported in the current state');
		}
	}
	
	exports.showCallInExternalWindow = showCallInExternalWindow;
	exports.showPreviewInExternalWindow = showPreviewInExternalWindow;
	exports.getExternalWindowState = getExternalWindowState;
	exports.hideExternalWindow = hideExternalWindow;
	exports.setExternalWindowAlwaysOnTop = setExternalWindowAlwaysOnTop;
	exports.setExternalWindowShowSelfViewPip = setExternalWindowShowSelfViewPip;
	exports.setExternalWindowShowControls = setExternalWindowShowControls;
	exports.setExternalWindowSelfViewPipPosition = setExternalWindowSelfViewPipPosition;
	exports.setExternalWindowTitle = setExternalWindowTitle;
	exports.setExternalWindowShowSelfViewPipBorder = setExternalWindowShowSelfViewPipBorder;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var initSettings = __webpack_require__(4).settings;
	var $ = __webpack_require__(6);
	
	/**
	 * an internal function to log messages
	 * @param {Boolean} [isVerbose] indicates if msg should be logged in verbose mode only (configurable by the application). true - show this log only in verbose mode, false - always show this log  <br>
	 * @param {String} msg the message to be logged (to console.log by default, configurable by the application)  <br>
	 * @param {Object} [context] a context to be logged <br>
	 */
	module.exports = function _log() {
	  var isVerbose = typeof arguments[0] === 'boolean' ? arguments[0] : false;
	  var msg = typeof arguments[0] === 'string' ? arguments[0] : arguments[1];
	  var context = typeof arguments[1] === 'object' ? arguments[1] : arguments[2];
	
	  if ((!isVerbose || (isVerbose && initSettings.verbose)) && $.isFunction(initSettings.log)) {
	    try {
	      var current = new Date();
	      var timelog = current.getDate() + '/' +
	        ('0' + (current.getMonth() + 1)).slice(-2) + '/' +
	        current.getFullYear() + ' ' +
	        ('0' + current.getHours()).slice(-2) + ':' +
	        ('0' + current.getMinutes()).slice(-2) + ':' +
	        ('0' + current.getSeconds()).slice(-2) + '.' +
	        ('00' + current.getMilliseconds()).slice(-3) + ' ';
	
	      initSettings.log('[cwic] ' + timelog + msg, context);
	
	    } catch (e) {
	      // Exceptions in application-define log functions can't really be logged
	    }
	  }
	}

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	var errorMap = __webpack_require__(5).errorMap;
	var $ = __webpack_require__(6);
	
	/** cwic global settings, they can be overridden by passing options to init
	 * @class
	 * @name init-settings
	 */
	var settings = {};
	
	/** The handler to be called when the API is ready and authorized.<br>
	 * The values in the defaults parameter can be used when invoking registerPhone.<br>
	 * The API is ready when:<ul>
	 *      <li>The document (DOM) is ready.</li>
	 *      <li>The Cisco Web Communicator add-on was found and could be loaded.</li>
	 *      <li>User authorization status is "UserAuthorized" (since 3.0.1).</li></ul>
	 * @type Function=null
	 * @memberof init-settings
	 * @param {Object} defaults An object containing default values retrieved from URL query parameters user and/or cucm <i>e.g: http://myserver/phone?user=foo&cucm=1.2.3.4 </i><br>
	 * @param {Boolean} registered Phone registration status - true if the phone is already registered (can be used when using SDK in multiple browser tabs), false otherwise
	 * @param {String} mode The phone's current call control mode - "SoftPhone" or "DeskPhone"
	 */
	settings.ready = null;
	
	/** Device prefix to use for default softphone device prediction algorithm. If not set, default prefix is 'ecp'. See also {@link init-settings.predictDevice}.
	 * @type String='ecp'
	 * @memberof init-settings
	 */
	settings.devicePrefix = 'ecp';
	
	/** Callback function to predict softphone device name<br>
	 * Device prediction algorithm is used to predict softphone device name in switchPhoneMode API function. If device name is not provided in the form of non-empty string, predictDevice function is used to predict device name. If custom predictDevice is not provided, default implementation is to concatenate settings.devicePrefix + options.username, where options.username is the name of the currently logged-in user.
	 * @type Function
	 * @memberof init-settings
	 * @name predictDevice
	 * @param {Object} options
	 * @param {String} options.username
	 */
	
	/** A flag to indicate to cwic that it should log more messages.
	 * @type Boolean=false
	 *  @memberof init-settings
	 */
	settings.verbose = true;
	
	/** Handler to be called when cwic needs to log information.<br>
	 * Default is to use console.log if available, otherwise do nothing.
	 * @memberof init-settings
	 * @param {String} msg the message
	 * @param {Object} [context] the context of the message
	 * @type Function
	 */
	settings.log = function (msg, context) {
	  if (typeof console !== 'undefined' && console.log) {
	    console.log(msg);
	    if (context) {
	      console.log(context);
	    }
	  }
	};
	
	/** The handler to be called if the API could not be initialized.<br>
	 * The basic properties of the error object are listed, however more may be added based on the context of the error.<br>
	 * If the triggered error originated from a caught exception, the original error properties are included in the error parameter.<br>
	 * An error with code 1 (PluginNotAvailable) can have an extra 'pluginDisabled' property set to true.<br>
	 * @type Function
	 * @memberof init-settings
	 * @param {Object} error see {@link $.fn.cwic-errorMap}
	 * @param {String} [error.message] message associated with the error.
	 * @param {Number} error.code code associated with the error
	 */
	settings.error = function (error) {
	  this.log('Error: ', error);
	};
	
	/**
	 * Allows the application to extend the default error map.<br>
	 * This parameter is a map of error id to {@link $.fn.cwic-errorMapEntry}
	 * It may also be a map of error id to String
	 * By default error messages (String) are associated to error codes (map keys, Numbers).<br>
	 * The application can define new error codes, or associate a different message/object to a pre-defined error code. <br>
	 *   Default error map: {@link $.fn.cwic-errorMap}<br>
	 * @type $.fn.cwic-errorMapEntry
	 * @memberof init-settings
	 */
	settings.errorMap = {};
	
	/**
	 * A callback used to indicate that CWIC must show the user authorization dialog before the application can use
	 * the CWIC API.  Can be used to display instructions to the user, etc. before the user authorization dialog is
	 * displayed to the user.  If implemented, the application must call the {@link $.fn.cwic.showUserAuthorization} API to show the
	 * user authorization dialog and obtain authorization from the user before using the CWIC API.
	 * If null, the user authorization dialog will be displayed when the application calls CWIC 'init', unless
	 * the domain has been previously authorized through the authorization dialog by the user selecting the "Always
	 * Allow" button, or the applications domain has been allowed by administrative whitelist.
	 * @since 3.0.1
	 * @type Function=null
	 * @function
	 * @memberof init-settings
	 */
	settings.delayedUserAuth = null;
	
	/**
	 * A flag to indicate if service discovery based sign in is active.
	 * If discovery lifecycle callbacks are not implemented, set this value to false.
	 * @type Boolean=true
	 * @memberof init-settings
	 */
	settings.serviceDiscovery = true;
	
	/**
	 * OAuth2 redirect_uri parameter. An URL to which an OAuth token is sent. Required for SSO sign in scenario.
	 * @memberof init-settings
	 * @type String
	 */
	settings.redirectUri = '';
	
	/**
	 * Discovery lifecycle callback for "User Profile Required" lifecycle event. Only happens on first use.
	 * After first use, plugin caches user email address/domain and callback won't be triggered again.
	 * Call resetData API to change email address.
	 * @param {Function} setEmail          Callback to call to continue with sign-in.
	 * @param {string}   cachedEmail       Cached previous value of email which could be used to populate input field on UI.
	 * @type Function
	 * @memberof init-settings
	 */
	settings.emailRequired = function (setEmail, cachedEmail) {
	  this.log('emailRequired callback not implemented, cannot proceed with sign in');
	  throw {
	    name: errorMap.ServiceDiscoveryMissingOrInvalidCallback.message,
	    message: "emailRequired not implemented",
	    toString: function () {
	      return this.name + ": " + this.message;
	    }
	  };
	};
	
	/**
	 * Discovery lifecycle callback for "Credentials Required" lifecycle event.
	 * @param {Function} setCredentials    Callback to call to set credentials and to continue with sign-in.
	 * @param {string}   cachedUser        Cached username value which could be used to populate input field on UI.
	 * @type Function
	 * @memberof init-settings
	 */
	settings.credentialsRequired = function (setCredentials, cachedUser) {
	  this.log('credentialsRequired callback not implemented, cannot proceed with sign in');
	  throw {
	    name: errorMap.ServiceDiscoveryMissingOrInvalidCallback.message,
	    message: "credentialsRequired not implemented",
	    toString: function () {
	      return this.name + ": " + this.message;
	    }
	  };
	};
	
	/**
	 * Discovery lifecycle callback for "SSO Signed In" lifecycle state. Only happens when the app has successfully been authenticated.
	 * This does not mean that phone is ready to use, telephony device connection flow is initiated afterwards. Implementation of signedIn callback is optional.
	 * @type Function
	 * @memberof init-settings
	 */
	settings.signedIn = function () {
	  this.log('default signedIn callback called...');
	};
	
	
	var defaultSettings = $.extend({}, settings);
	
	exports.settings = settings;
	exports.reset = function resetInitSettings() {
	  $.extend(settings, defaultSettings);
	}

/***/ },
/* 5 */
/***/ function(module, exports) {

	// jsdoc does not seem to like enumerating properties (fields) of objects it already considers as properties (fields).
	/** @description
	 * cwic error object
	 * @name $.fn.cwic-errorMapEntry
	 * @class
	 * @property {String} code a unique error identifier
	 * @property {String} message the message associated with the error
	 * @property {Any} [propertyName] Additional properties that will be passed back when an error is raised.
	 */
	
	/**
	 * @description
	 * The error map used to build errors triggered by cwic. <br>
	 * Keys are error codes (strings), values objects associated to codes. <br>
	 * By default the error object contains a single 'message' property. <br>
	 * The error map can be customized via the init function. <br>
	 * @name $.fn.cwic-errorMap
	 * @class
	 * @static
	 */
	var errorMap = {}; // this kind of structure is dictated by JSDoc (for some reason it does not document object members!?)
	
	/** Unknown: unknown error or exception
	 * @memberof $.fn.cwic-errorMap
	 * @type {$.fn.cwic-errorMapEntry}
	 */
	errorMap.Unknown = {
	  code: 'Unknown',
	  message: 'Unknown error'
	};
	
	/** PluginNotAvailable: plugin not available (not installed, not enabled or unable to load)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.PluginNotAvailable = {
	  code: 'PluginNotAvailable',
	  message: 'Plugin not available'
	};
	
	/** BrowserNotSupported: browser not supported
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.BrowserNotSupported = {
	  code: 'BrowserNotSupported',
	  message: 'Browser not supported'
	};
	
	/** InvalidArguments: invalid arguments
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.InvalidArguments = {
	  code: 'InvalidArguments',
	  message: 'Invalid arguments'
	};
	
	/** InvalidState: invalid state for operation (e.g. startconversation when phone is not registered)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.InvalidState = {
	  code: 'InvalidState',
	  message: 'Invalid State'
	};
	
	/** NativePluginError: plugin returned an error
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.NativePluginError = {
	  code: 'NativePluginError',
	  message: 'Native plugin error'
	};
	
	/** OperationNotSupported: operation not supported
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.OperationNotSupported = {
	  code: 'OperationNotSupported',
	  message: 'Operation not supported'
	};
	
	/** InvalidTFTPServer: The configured TFTP server is incorrect
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.InvalidTFTPServer = {
	  code: 'InvalidTFTPServer',
	  message: 'The configured TFTP server is incorrect'
	};
	
	/** InvalidCCMCIPServer: The configured CCMCIP server is incorrect
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.InvalidCCMCIPServer = {
	  code: 'InvalidCCMCIPServer',
	  message: 'The configured CCMCIP server is incorrect'
	};
	
	/** InvalidCTIServer: The configured CTI server is incorrect
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.InvalidCTIServer = {
	  code: 'InvalidCTIServer',
	  message: 'The configured CTI server is incorrect'
	};
	
	/** ReleaseMismatch: release mismatch
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.ReleaseMismatch = {
	  code: 'ReleaseMismatch',
	  message: 'Release mismatch'
	};
	
	/** NoDevicesFound: no devices found for supplied credentials
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.NoDevicesFound = {
	  code: 'NoDevicesFound',
	  message: 'No devices found'
	};
	
	/** TooManyPluginInstances: already logged in in another process (browser or window in internet explorer)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.TooManyPluginInstances = {
	  code: 'TooManyPluginInstances',
	  message: 'Too many plug-in instances'
	};
	
	/** AuthenticationFailure: authentication failed - invalid or missing credentials/token or incorrect server parameters
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.AuthenticationFailure = {
	  code: 'AuthenticationFailure',
	  message: 'Authentication failed'
	};
	
	/** SignInError: other sign-in error
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.SignInError = {
	  code: 'SignInError',
	  message: 'Sign-in Error'
	};
	
	/** CallControlError: error performing call control operation
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.CallControlError = {
	  code: 'CallControlError',
	  message: 'Call control error'
	};
	
	/** PhoneConfigGenError: other phone configuration error
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.PhoneConfigGenError = {
	  code: 'PhoneConfigGenError',
	  message: 'Phone configuration error'
	};
	
	/** CreateCallError: error creating a new call. Possible causes:<br>
	 * - the device is not available anymore<br>
	 * - the maximum number of active calls configured on the user's line was reached<br>
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.CreateCallError = {
	  code: 'CreateCallError',
	  message: 'Cannot create call'
	};
	
	/** NetworkError: No network connection or SSL/TLS connection error
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.NetworkError = {
	  code: 'NetworkError',
	  message: 'Network error'
	};
	
	/** VideoWindowError: error modifying video association (e.g. removing non-attached window or adding non-existing window)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.VideoWindowError = {
	  code: 'VideoWindowError',
	  message: 'Video window error'
	};
	
	/** CapabilityMissing: Capability missing (e.g. no capability to merge call to conference or to transfer a call)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.CapabilityMissing = {
	  code: 'CapabilityMissing',
	  message: 'Capability Missing'
	};
	
	/** NotUserAuthorized: user did not authorize the add-on to run
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.NotUserAuthorized = {
	  code: 'NotUserAuthorized',
	  message: 'User did not authorize access'
	};
	
	/** OperationFailed: System not started or fully operational
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.OperationFailed = {
	  code: 'OperationFailed',
	  message: 'Operation Failed'
	};
	
	/** ExtensionNotAvailable: browser extension not available (not installed, not enabled or unable to load)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.ExtensionNotAvailable = {
	  code: 'ExtensionNotAvailable',
	  message: 'Browser extension not available'
	};
	
	/** DockingCapabilitiesNotAvailable: External video docking capabilities not available
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.DockingCapabilitiesNotAvailable = {
	  code: 'DockingCapabilitiesNotAvailable',
	  message: 'External video docking capabilities not available'
	};
	
	/** DockArgumentNotHTMLElement: Dock needs to be called on an HTMLElement
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.DockArgumentNotHTMLElement = {
	  code: 'DockArgumentNotHTMLElement',
	  message: 'Dock needs to be called on an HTMLElement'
	};
	
	/** ServiceDiscoveryMissingOrInvalidCallback: Service discovery lifecycle error (no callback defined or exception occured within callback for some of lifecycle states)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.ServiceDiscoveryMissingOrInvalidCallback = {
	  code: 'ServiceDiscoveryMissingOrInvalidCallback',
	  message: 'Service Discovery Error - Callback not implemented or exception occured'
	};
	
	/** SSOMissingOrInvalidRedirectURI: Single Sign On redirect uri is missing or invalid (OAuth2)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.SSOMissingOrInvalidRedirectURI = {
	  code: 'SSOMissingOrInvalidRedirectURI',
	  message: 'Redirect URI missing or invalid'
	};
	
	/** InvalidUserInput: User input invalid (email, password, redirectUri, etc.)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.InvalidUserInput = {
	  code: 'InvalidUserInput',
	  message: 'Invalid user input'
	};
	
	/** CertificateError: Cannot start a new session due to a certificate problem.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.CertificateError = {
	  code: 'CertificateError',
	  message: 'Certificate error'
	};
	
	/** InvalidURLFragment: Invalid SSO URL fragment received from child window (popup or iFrame)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.InvalidURLFragment = {
	  code: 'InvalidURLFragment',
	  message: 'Invalid URL fragment received'
	};
	
	/** ErrorReadingConfig: Attempt to load startup handler config failed. Check if StartupHandlerConfig.xml file is present in installation directory.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.ErrorReadingConfig = {
	  code: 'ErrorReadingConfig',
	  message: 'Error reading config'
	};
	
	/** UnexpectedLifecycleState: Unexpected application lifecycle state.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.UnexpectedLifecycleState = {
	  code: 'UnexpectedLifecycleState',
	  message: 'Unexpected application lifecycle state'
	};
	
	/** SSOStartSessionError: SSO start session error, probably because of missing token.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.SSOStartSessionError = {
	  code: 'SSOStartSessionError',
	  message: 'SSO start session error'
	};
	
	/** SSOCanceled: SSO canceled.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.SSOCanceled = {
	  code: 'SSOCanceled',
	  message: 'SSO canceled'
	};
	
	/** SSOInvalidUserSwitch: You have attempted to sign in as a different user. To switch user you must call resetData API.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.SSOInvalidUserSwitch = {
	  code: 'SSOInvalidUserSwitch',
	  message: 'Invalid user switch'
	};
	
	/** SSOSessionExpired: SSO session has expired.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.SSOSessionExpired = {
	  code: 'SSOSessionExpired',
	  message: 'SSO session expired'
	};
	
	/** ServiceDiscoveryFailure: Cannot find services automatically. Try to set up server addresses manually.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.ServiceDiscoveryFailure = {
	  code: 'ServiceDiscoveryFailure',
	  message: 'Cannot find services automatically'
	};
	
	/** CannotConnectToServer: Cannot communicate with the server.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.CannotConnectToServer = {
	  code: 'CannotConnectToServer',
	  message: 'Cannot connect to CUCM server'
	};
	
	/** SelectDeviceFailure: Connecting to phone device failed.
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.SelectDeviceFailure = {
	  code: 'SelectDeviceFailure',
	  message: 'Connecting to phone device failed'
	};
	
	/** NoError: no error (success)
	 * @memberof $.fn.cwic-errorMap
	 * @type $.fn.cwic-errorMapEntry
	 */
	errorMap.NoError = {
	  code: 'NoError',
	  message: 'No error'
	};
	
	
	var errorMapAlias = {
	  //origin in plugin - ApiReturnCodeEnum
	  eCreateCallFailed: 'CreateCallError',
	  eCallOperationFailed: 'CallControlError',
	  eNoActiveDevice: 'PhoneConfigGenError',
	  eLoggedInLock: 'TooManyPluginInstances',
	  eLogoutFailed: 'SignInError',
	  eNoWindowExists: 'VideoWindowError',
	  eInvalidWindowIdOrObject: 'VideoWindowError', // for addPreviewWindow and removePreviewWindow (CSCuo00772 and CSCuo00654)
	  eWindowAlreadyExists: 'VideoWindowError',
	  eNoPhoneMode: 'InvalidArgument',
	  eInvalidArgument: 'InvalidArguments',
	  eOperationNotSupported: 'OperationNotSupported',
	  eCapabilityMissing: 'CapabilityMissing',
	  eNotUserAuthorized: 'NotUserAuthorized',
	  eSyntaxError: 'NativePluginError',
	  eOperationFailed: 'OperationFailed',
	  eInvalidCallId: 'InvalidArguments',
	  eInvalidState: 'InvalidState',
	  eNoError: 'NoError',
	  eUnknownServiceEvent: 'Unknown', // default for service events
	
	  Ok: 'NoError',
	
	  // Telephony service event codes
	  Unknown: 'Unknown',
	  DeviceRegNoDevicesFound: 'NoDevicesFound',
	  NoCredentialsConfiguredServerHealth: 'AuthenticationFailure',
	  InvalidCredential: 'AuthenticationFailure',
	  InvalidCredentialServerHealth: 'AuthenticationFailure',
	  InvalidCCMCIPServer: 'InvalidCCMCIPServer',
	  InvalidTFTPServer: 'InvalidTFTPServer',
	  InvalidCTIServer: 'InvalidCTIServer',
	  NoNetwork: 'NetworkError',
	  TLSFailure: 'NetworkError',
	  SSLConnectError: 'NetworkError',
	  ServerConnectionFailure: 'CannotConnectToServer',
	  ServerAuthenticationFailure: 'AuthenticationFailure',
	  SelectDeviceFailure: 'SelectDeviceFailure',
	  InValidConfig: 'AuthenticationFailure', //The last attempt at authentication with CUCM failed because of invalid configuration. The CCMIP server, port etc was incorrect.
	  ServerCertificateRejected: 'CertificateError',
	  InvalidToken: 'AuthenticationFailure',
	  InvalidAuthorisationTokenServerHealth: 'AuthenticationFailure',
	
	  // System service event codes
	  ErrorReadingConfig: 'ErrorReadingConfig',
	  InvalidStartupHandlerState: 'UnexpectedLifecycleState',
	  InvalidLifeCycleState: 'UnexpectedLifecycleState', // The Lifecycle was requested to Start or Stop while in an invalid state.
	  InvalidCertRejected: 'CertificateError',
	  SSOPageLoadError: 'UnexpectedLifecycleState', // wrong input provided to "OnNavigationCompleted"
	  SSOStartSessionError: 'SSOStartSessionError',
	  SSOUnknownError: 'UnexpectedLifecycleState', // wrong input provided to "OnNavigationCompleted"
	  SSOCancelled: 'SSOCancelled',
	  SSOCertificateError: 'CertificateError',
	  SSOInvalidUserSwitch: 'SSOInvalidUserSwitch',
	  SSOWhoAmIFailure: 'SSOStartSessionError',
	  SSOSessionExpired: 'SSOSessionExpired',
	  ServiceDiscoveryFailure: 'ServiceDiscoveryFailure', // Cannot find your services automatically. Click advanced settings to set up manually.
	  ServiceDiscoveryAuthenticationFailure: 'AuthenticationFailure',
	  ServiceDiscoveryCannotConnectToCucmServer: 'CannotConnectToServer',
	  ServiceDiscoveryNoCucmConfiguration: 'ServiceDiscoveryFailure',
	  ServiceDiscoveryNoSRVRecordsFound: 'ServiceDiscoveryFailure',
	  ServiceDiscoveryCannotConnectToEdge: 'CannotConnectToServer',
	  ServiceDiscoveryNoNetworkConnectivity: 'NetworkError',
	  ServiceDiscoveryUntrustedCertificate: 'CertificateError'
	
	  // Connection failure codes
	  // if something breaks connection during the session
	  // a lot of connection failure codes have the same name as above codes, so it's not repeated here.
	};
	
	
	exports.errorMap = errorMap;
	exports.errorMapAlias = errorMapAlias;
	
	exports.getError = function (key, backupkey) {
	  var errorMapKey = 'Unknown';
	
	  if (errorMapAlias[key]) {
	    errorMapKey = errorMapAlias[key];
	  } else if (errorMap[key]) {
	    errorMapKey = key;
	  } else if (backupkey && errorMapAlias[backupkey]) {
	    errorMapKey = errorMapAlias[backupkey];
	  } else if (backupkey && errorMap[backupkey]) {
	    errorMapKey = backupkey;
	  }
	
	  return errorMap[errorMapKey];
	};

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = jQuery;

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var clientRequestCallbacks = __webpack_require__(8);
	var log = __webpack_require__(3);
	var $ = __webpack_require__(6);
	var cwicState = __webpack_require__(9);
	var errors = __webpack_require__(5);
	var triggerError = __webpack_require__(10);
	var generateUniqueId = __webpack_require__(11);
	
	/**
	 * sends clientRequest message to browser plugin/extension
	 * @param {String} name
	 * @param {Object|String} content Object or string to be passed as arguments for the named request
	 * @param {Function} [successCb(result)] Function to be called upon receiving success reply to the request.
	 *                    replyCb should take object as argument the return result from native function.
	 * @param {Function} [errorCb(errorMapAlias)] Function to be called upon receiving error reply to the request.
	 *                    errorCb should take object as argument an errorMapAlias string.  If no errorCb is provided
	 *                    then a generic error.cwic event will be triggered instead.
	 * @private
	 */
	function sendClientRequest() {
	  var plugin = cwicState.plugin;
	
	  if (!plugin || !plugin.api) {
	    log('sendClientRequest no plugin available');
	    return false;
	  }
	
	  // When message is being sent from new API through CompatibilityPlugin and MessageSender.
	  if($.isPlainObject(arguments[0]))
	  {
	    sendMessageToClient(arguments[0]);
	    return;
	  }
	
	  var name = arguments[0];
	  var content = null;
	  var successCb = null;
	  var $this = plugin.scope || this;
	
	  if ($.isFunction(arguments[1])) {
	    successCb = arguments[1];
	  } else {
	    content = arguments[1];
	  }
	
	  // create a default error callback
	  var errorCb = function (errorMapAlias) {
	    triggerError($this, errors.getError(errorMapAlias), { 'nativeError': errorMapAlias, 'nativeRequest': name, 'nativeArgs': content }, 'error returned from native plugin');
	  };
	
	  if ($.isFunction(arguments[2])) {
	    if (successCb === null) {
	      successCb = arguments[2];
	    } else {
	      errorCb = arguments[2];
	    }
	  }
	
	  if ($.isFunction(arguments[3])) {
	    errorCb = arguments[3];
	  }
	
	  // if nothing else, default to no-op
	  successCb = successCb || $.noop;
	
	  if (name !== 'encryptCucmPassword') {
	    log(true, 'sendClientRequest: ' + name, { 'successCb': successCb, 'errorCb': errorCb, 'content': content });
	  } else {
	    log(true, 'sendClientRequest [encryptCucmPassword]');
	  }
	
	  // Good-enough unique ID generation:
	  // Just needs to be unique for this channel across restarts of this client,
	  // just in case there's an old server reply in the pipeline.
	  var uid = (name === 'init') ? '0000' : generateUniqueId();
	
	  var clientMsg = {
	    ciscoSDKClientMessage: {
	      'messageId': uid,
	      'name': name,
	      'content': content || {}
	    }
	  };
	
	  if (name !== 'encryptCucmPassword') {
	    log(true, 'send ciscoSDKClientMessage: ' + name, clientMsg.ciscoSDKClientMessage);
	  } else {
	    log(true, 'sendClientRequest [encryptCucmPassword]. Message id:', clientMsg.ciscoSDKClientMessage.messageId);
	  }
	
	  //send message to Chrome
	  if (plugin.api.sendRequest) {
	    plugin.api.sendRequest(clientMsg);
	
	  } else if (plugin.api.postMessage) {
	    cwicState.clientId = cwicState.clientId || generateUniqueId();
	
	    var clientInfoJSON = {
	      'id': cwicState.clientId,
	      'url': window.location.href,
	      'hostname': window.location.hostname,
	      'name': window.document.title
	    };
	
	    //*****JSON message changed, so it fits NPAPI expectations
	    var NPAPI_Msg = {'ciscoChannelMessage': $.extend(clientMsg, {'client': clientInfoJSON})};
	
	    //send message to browsers that support NPAPI
	    plugin.api.postMessage(JSON.stringify(NPAPI_Msg));
	  }
	
	  // asynchronous backend so we store the callback
	  clientRequestCallbacks.addCallback({
	    id: uid,
	    successCb: successCb,
	    errorCb: errorCb
	  });
	}
	
	// Helper function for sending message througn new API.
	function sendMessageToClient(message)
	{
	  var plugin = cwicState.plugin;
	
	  var clientMsg = message;
	
	  //send message to Chrome
	  if (plugin.api.sendRequest)
	  {
	    plugin.api.sendRequest(clientMsg);
	
	  } else if (plugin.api.postMessage)
	  {
	    cwicState.clientId = cwicState.clientId || generateUniqueId();
	
	    var clientInfoJSON = {
	      'id': cwicState.clientId,
	      'url': window.location.href,
	      'hostname': window.location.hostname,
	      'name': window.document.title
	    };
	
	    //*****JSON message changed, so it fits NPAPI expectations
	    var NPAPI_Msg = {'ciscoChannelMessage': $.extend(clientMsg, {'client': clientInfoJSON})};
	
	    //send message to browsers that support NPAPI
	    plugin.api.postMessage(JSON.stringify(NPAPI_Msg));
	  }
	}
	
	module.exports = sendClientRequest;
	


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var $ = __webpack_require__(6);
	
	var clientRequestCallbacks = {
	  // _callbacks[messageId] = { 'successCb' : successCb, 'errorCb' : errorCb }
	  _callbacks: {},
	
	  addCallback: function(callbackData) {
	    this._callbacks[callbackData.id] = { 'successCb': callbackData.successCb, 'errorCb': callbackData.errorCb }
	  },
	
	  hasCallback: function (msgId) {
	    return Boolean(msgId && this._callbacks[msgId]);
	  },
	
	  callCallback: function (msgId, error, nativeResult) {
	    if (!this.hasCallback(msgId)) {
	      return;
	    }
	
	    var callback, arg;
	
	    if (error && error !== 'eNoError') {
	      callback = this._callbacks[msgId].errorCb;
	      arg = error;
	    } else {
	      callback = this._callbacks[msgId].successCb;
	      arg = nativeResult;
	    }
	
	    if ($.isFunction(callback)) {
	      log(true, 'clientRequestCallbacks calling result callback for msgId: ' + msgId);
	
	      try {
	        callback(arg);
	      } catch (e) {
	        log('Exception occurred in clientRequestCallbacks callback', e);
	      }
	    }
	
	    delete this._callbacks[msgId];
	  },
	
	  purge: function () {
	    this._callbacks = {};
	  }
	};
	
	
	module.exports = clientRequestCallbacks;

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var log = __webpack_require__(3);
	var $ = __webpack_require__(6);
	
	/** @scope $.fn.cwic */
	
	module.exports = {
	  // a global reference to the CWC native plugin API
	  plugin: null,
	
	  setPlugin: function (val) {
	    this.plugin = val;
	  },
	
	  setPropsToPlugin: function (props) {
	    if (!this.plugin || typeof this.plugin !== 'object' || typeof props !== 'object') {
	      throw new Error('Cannot extend plugin object...');
	    }
	
	    $.extend(this.plugin, props);
	
	  },
	
	  getInstanceId: function () {
	    var ret = 'N/A';
	
	    if (this.plugin && this.plugin.instanceId) {
	      ret = this.plugin.instanceId;
	    }
	
	    return ret;
	  },
	
	  initContext: null, // context, aka $this, set by init API
	
	  // client id to differentiate between cwic instances in separate tabs (generated only once per cwic instance)
	  clientId: null,
	
	  // we have to keep the reference to one of the video objects for active conversation.
	  // This is neccessary for calling "removeWindowFromCall" after conversation is ended.
	  activeConversation: {
	    videoObject: null, // first video object added to conversation
	    window: null,
	    lastId: -1 // see addWindowToCall() for explanation
	  },
	
	  emailForManualSignIn: 'jabbersdk@any.domain',
	  localCacheKey: '_cwic_cache_email',
	
	  // 21px width - this works for surface pro device on Chrome 44
	  // see http://stackoverflow.com/questions/986937/how-can-i-get-the-browsers-scrollbar-sizes
	  scrollBarWidth: 21,
	
	  isMultimediaStarted: false,
	
	  pendingConnect: null,
	
	  registration: null,
	
	  regGlobals: {},
	
	  transferCallGlobals: {
	    endTransfer: function () {
	      this.inProgress = false;
	      this.completeBtn = null;
	      this.callId = null;
	    },
	
	    inProgress: false,
	    completeBtn: null,
	    callId: null
	  },
	
	  SSOGlobals: {
	    inProgress: false,
	    canCancel: false
	  },
	
	  resetGlobals: function () {
	    log(true, 'resetGlobals: reseting ...');
	
	    var user = this.regGlobals.user || '',
	      email = this.regGlobals.email || '',
	      manual = this.regGlobals.manual,
	      unregisterCb = this.regGlobals.unregisterCb || null, // cannot overwrite because it waits for 'SIGNEDOUT' state to occur!
	      errorState = this.regGlobals.errorState || '', // after error, logout is called. We must save this values for the next sign in attempt
	      lastAuthenticatorId = this.regGlobals.lastAuthenticatorId || null;
	
	    this.SSOGlobals = {
	      inProgress: false,
	      canCancel: false
	    };
	
	    this.transferCallGlobals.endTransfer();

	    this.regGlobals = {
	      registeringPhone: false,
	      manual: manual,
	      signingOut: false,
	      endingCallForId: -1,
	      switchingMode: false,
	      telephonyDevicesSet: false,
	      lastConnectedDevice: '',
	      errorState: errorState,
	      lastAuthenticatorId: lastAuthenticatorId,
	      successCb: null,
	      errorCb: null,
	      CUCM: [],
	      user: user,
	      password: '',
	      email: email,
	      unregisterCb: unregisterCb,
	      authenticatedCallback: null,
	      credentialsRequiredCalled: false,
	      emailRequiredCalled: false
	    };
	
	    this.registration = {
	      devices: {} // map of available devices (key is device name)
	    };
	  }
	};
	


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	var errorMap = __webpack_require__(5).errorMap;
	var $ = __webpack_require__(6);
	var log = __webpack_require__(3);
	
	/**
	 * _triggerError(target, [callback], [code], [data]) <br>
	 * <br>
	 * - target (Object): a jQuery selection where to trigger the event error from <br>
	 * - callback (Function): an optional callback to be called call with the error. if specifed, prevents the generic error event to be triggered <br>
	 * - code (Number): an optional cwic error code (defaults to 0 - Unknown) <br>
	 * - data (String, Object): some optional error data, if String, used as error message. if Object, used to extend the error. <br>
	 * <br>
	 * cwic builds an error object with the following properties: <br>
	 *  code: a pre-defined error code <br>
	 *  message: the error message (optional) <br>
	 *  any other data passed to _triggerError or set to errorMap (see the init function) <br>
	 *  <br>
	 * When an error event is triggered, the event object is extended with the error properties. <br>
	 * <br>
	 */
	function triggerError() {
	  var $this = arguments[0]; // target (first mandatory argument)
	  var errorCb = null;
	
	  // the default error
	  var error = $.extend({details: []}, errorMap.Unknown);
	
	  // extend error from arguments
	  for (var i = 1; i < arguments.length; i++) {
	    var arg = arguments[i];
	
	    // is the argument a specific error callback ?
	    if ($.isFunction(arg)) {
	      errorCb = arg;
	    } else if (typeof arg === 'string') {
	      error.details.push(arg);
	    } else if (typeof arg === 'object') {
	      $.extend(error, arg);
	    }
	
	  }
	
	  log(error.message, error);
	
	  // if specific error callback, call it
	  if (errorCb) {
	    try {
	      errorCb(error);
	    } catch (errorException) {
	      log('Exception occurred in application error callback', errorException);
	      if (typeof console !== 'undefined' && console.trace) {
	        console.trace();
	      }
	    }
	
	  } else {
	    // if no specific error callback, raise generic error event
	    var event = $.Event('error.cwic');
	    $.extend(event, error);
	    $this.trigger(event);
	  }
	
	  return $this;
	}
	
	module.exports = triggerError;

/***/ },
/* 11 */
/***/ function(module, exports) {

	// Good-enough unique ID generation:
	// used for clienId (generated only once per cwic instance) and message ids (generated for each message)
	// Just needs to be unique for this channel across restarts of this client,
	// just in case there's an old server reply in the pipeline.
	function generateUniqueId() {
	  return (new Date()).valueOf().toString() + Math.floor((Math.random() * 10000) + 1).toString();
	}
	
	module.exports = generateUniqueId;

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var sendClientRequest = __webpack_require__(7);
	var $ = __webpack_require__(6);
	
	var dockGlobals = {
	  _about: null,
	
	  hasDockingCapabilities: function () {
	    var caps;
	    try {
	      caps = dockGlobals._about.capabilities.externalWindowDocking;
	    } catch (e) {
	      caps = null;
	    }
	
	    return caps;
	  },
	
	  isVideoBeingReceived: false,
	
	  isDocked: false,
	
	  timeOfPreviousDocking: 0,
	
	  minimalTimeBeforeChangingPosition: 10, // TODO: find optimal value to avoid lag, but also to avoid too many dockUpdate messges
	
	  targetDiv: null,
	
	  targetDivStyle: {
	    'background-color': 'magenta',
	    'width': '8px',
	    'height': '8px',
	    'position': 'fixed',
	    'border-right': '8px solid black',
	    'z-index': '2147483647'
	  },
	
	  frame: window,
	
	  element: null,
	
	  position: {},
	
	  move: function () {
	    if (dockGlobals.isDocked) {
	
	      // (new Date()).getTime() returns the current time in milliseconds
	      var millisecondsSinceLastMove = (new Date()).getTime() - dockGlobals.timeOfPreviousDocking;
	      var rect = dockGlobals.updateOffsets(dockGlobals.element.getBoundingClientRect());
	      var dockedWindowMoved = (rect.top === dockGlobals.position.top &&
	      rect.left === dockGlobals.position.left &&
	      rect.height === dockGlobals.position.height &&
	      rect.width === dockGlobals.position.width &&
	      rect.cropTop === dockGlobals.position.cropTop &&
	      rect.cropLeft === dockGlobals.position.cropLeft &&
	      rect.cropHeight === dockGlobals.position.cropHeight &&
	      rect.cropWidth === dockGlobals.position.cropWidth) ?
	        false : true;
	
	      if (dockedWindowMoved && millisecondsSinceLastMove > dockGlobals.minimalTimeBeforeChangingPosition) {
	        dockGlobals.timeOfPreviousDocking = (new Date()).getTime();
	        $.extend(dockGlobals.position, rect);
	        dockGlobals.sendMessageToAddOn("dockUpdate", dockGlobals.position, 'remote'); //todo: implement logic for updating other docked windows
	      }
	
	      dockGlobals.frame.requestAnimationFrame(dockGlobals.move);
	    }
	  },
	
	  updateOffsets: function (positionWithinInnermostFrame) {
	    var currentFrame = dockGlobals.frame,
	      currentFrameHeight = $(currentFrame).height(),
	      currentFrameWidth = $(currentFrame).width(),
	      currentFrameRect,
	      parentFrameWidth,
	      parentFrameHeight,
	      position = $.extend({
	        cropTop: 0,
	        cropLeft: 0,
	        cropBottom: 0,
	        cropRight: 0,
	        cropWidth: 0,
	        cropHeight: 0
	      }, positionWithinInnermostFrame),
	      frameBorderOffset = 0,
	      borderTopOffset = 0,
	      borderLeftOffset = 0,
	      paddingTopOffset = 0,
	      paddingLeftOffset = 0;
	
	    // we need to take into account the devicePixelRatio and the CSS zoom property
	    // it won't work if css zoom is set on some of parent elements
	    var scaleCoefficient = currentFrame.devicePixelRatio * $(dockGlobals.element).css('zoom');
	
	    var scrollX = 0;
	    var scrollY = 0;
	
	    if (('ontouchstart' in window) &&
	      (navigator.maxTouchPoints > 1)
	    ) {
	
	      //running on touch-capable device
	      var inner = currentFrame.innerWidth;
	      var hasScrollbar = inner - currentFrame.document.documentElement.clientWidth;
	      dockGlobals.lastZoomFactor = dockGlobals.lastZoomFactor || 1;
	
	      // scrollbar width changes when zooming, we need to calculate it for each scale level
	      // on pinch zoom, hasScrollbar very quickly goes below zero, and we should skip that case (no scrollbars on pinch-zoom)
	      // ...basically just an aproximation which works currentlly
	      if (hasScrollbar > 0) {
	        inner -= hasScrollbar;
	      } else {
	        inner -= (cwicState.scrollBarWidth / dockGlobals.lastZoomFactor);
	      }
	
	      var pinchZoomCoefficient = currentFrame.document.documentElement.clientWidth / inner;
	
	      scaleCoefficient *= pinchZoomCoefficient;
	      dockGlobals.lastZoomFactor = scaleCoefficient;
	
	      var scrollSizeX = $(currentFrame.document).width() - $(currentFrame).width();
	      var scrollSizeY = $(currentFrame.document).height() - $(currentFrame).height();
	
	      if (pinchZoomCoefficient > 1.01) {
	        scrollX = currentFrame.scrollX;
	        scrollY = currentFrame.scrollY;
	
	        // this is complex logic dealing with case when the page (pinchZoom=0) has scrollbars
	        // In that case, when the page is pinche-zoomed, scrollX and scrollY are no longer accurate in providing position of docking container
	        var diffY = scrollSizeY - scrollY;
	
	        // while diff is >0, position.top represent accurate top position
	        // when it goes below 0 (meaning that "original" scrollbar hit the end), position.top gets stuck and then diff value represent how much container is moved
	        if (diffY >= 0) {
	          scrollY = 0;
	        } else {
	          scrollY = Math.abs(diffY);
	        }
	
	        var diffX = scrollSizeX - scrollX;
	
	        if (diffX >= 0) {
	          scrollX = 0;
	        } else {
	          scrollX = Math.abs(diffX);
	        }
	
	        if (diffY >= 0) {
	          dockGlobals.initPosY = position.top + currentFrame.scrollY;
	        }
	
	        if (
	          position.top > dockGlobals.initPosY - scrollSizeY &&
	          diffY < 0
	        ) {
	          position.top = dockGlobals.initPosY - scrollSizeY;
	        }
	
	        if (diffX >= 0) {
	          dockGlobals.initPosX = position.left + currentFrame.scrollX;
	        }
	
	        if (
	          position.left > dockGlobals.initPosX - scrollSizeX &&
	          diffX < 0
	        ) {
	          position.left = dockGlobals.initPosX - scrollSizeX;
	        }
	      }
	
	    }
	
	    position.left -= scrollX;
	    position.top -= scrollY;
	
	    // console.log('SCALE TOP: ', position.top);
	    // console.log('SCALE SCROLL: ', scrollY);
	    // console.log('SCALE SCROLL ACTUAL: ', window.scrollY);
	    // console.log('SCALE SCROLL SIZE: ', scrollSizeY);
	    // console.log('SCALE SCROLL DIFF: ', diffY);
	    // console.log('SCALE NORMAL: ', scaleCoefficient);
	    // console.log('SCALE PINCH: ', pinchZoomCoefficient);
	    // console.log('SCALE INIT POS: ', dockGlobals.initPos);
	
	    // calculating crop values for innermost iframe
	    position.cropTop = (position.top < 0) ?
	      Math.abs(position.top) : 0;
	
	    position.cropLeft = (position.left < 0) ?
	      Math.abs(position.left) : 0;
	
	    position.cropBottom = Math.max(position.bottom - currentFrameHeight, 0);
	    position.cropRight = Math.max(position.right - currentFrameWidth, 0);
	
	    while (currentFrame != currentFrame.top) {
	      currentFrameRect = currentFrame.frameElement.getBoundingClientRect();
	      parentFrameWidth = $(currentFrame.parent).width();
	      parentFrameHeight = $(currentFrame.parent).height();
	
	      // !! converts to boolean: 0 and NaN map to false, the rest of the numbers map to true
	      if (currentFrame.frameElement.frameBorder === "" || !!parseInt(currentFrame.frameElement.frameBorder, 10)) {
	        // after testing on Chrome, whenever a frameBorder is present, it's size is 2px
	        frameBorderOffset = 2;
	      } else {
	        frameBorderOffset = 0;
	      }
	
	      if (currentFrame.frameElement.style.borderTopWidth === "") {
	        borderTopOffset = frameBorderOffset;
	      } else {
	        borderTopOffset = parseInt(currentFrame.frameElement.style.borderTopWidth || 0, 10);
	      }
	
	      paddingTopOffset = parseInt(currentFrame.frameElement.style.paddingTop || 0, 10);
	
	      if (currentFrameRect.top < 0) {
	        if (position.top + position.cropTop < 0) {
	          position.cropTop += Math.abs(currentFrameRect.top);
	        } else if (Math.abs(currentFrameRect.top) - (position.top + position.cropTop + borderTopOffset + paddingTopOffset) > 0) {
	          position.cropTop += Math.abs(Math.abs(currentFrameRect.top) - (position.top + position.cropTop + borderTopOffset + paddingTopOffset));
	        }
	      }
	
	      if (currentFrameRect.top + borderTopOffset + paddingTopOffset + position.top + position.height - position.cropBottom > parentFrameHeight) {
	        position.cropBottom = currentFrameRect.top + borderTopOffset + paddingTopOffset + position.top + position.height - parentFrameHeight;
	      }
	
	      position.top += currentFrameRect.top + borderTopOffset + paddingTopOffset;
	
	      if (currentFrame.frameElement.style.borderLeftWidth === "") {
	        borderLeftOffset = frameBorderOffset;
	      } else {
	        borderLeftOffset = parseInt(currentFrame.frameElement.style.borderLeftWidth || 0, 10);
	      }
	
	      paddingLeftOffset = parseInt(currentFrame.frameElement.style.paddingLeft || 0, 10);
	
	      if (currentFrameRect.left < 0) {
	        if (position.left + position.cropLeft < 0) {
	          position.cropLeft += Math.abs(currentFrameRect.left);
	        } else if (Math.abs(currentFrameRect.left) - (position.left + position.cropLeft + borderLeftOffset + paddingLeftOffset) > 0) {
	          position.cropLeft += Math.abs(Math.abs(currentFrameRect.left) - (position.left + position.cropLeft + borderLeftOffset + paddingLeftOffset));
	        }
	      }
	
	      if (currentFrameRect.left + borderLeftOffset + paddingLeftOffset + position.left + position.width - position.cropRight > parentFrameWidth) {
	        position.cropRight = currentFrameRect.left + borderLeftOffset + paddingLeftOffset + position.left + position.width - parentFrameWidth;
	      }
	
	      position.left += currentFrameRect.left + borderLeftOffset + paddingLeftOffset;
	      currentFrame = currentFrame.parent;
	    }
	
	    position.cropHeight = Math.max(position.height - (position.cropTop + position.cropBottom), 0);
	    position.cropTop = (position.height > position.cropTop) ? position.cropTop : 0;
	    position.cropWidth = Math.max(position.width - (position.cropLeft + position.cropRight), 0);
	    position.cropLeft = (position.width > position.cropLeft) ? position.cropLeft : 0;
	
	    // set target before scaling (Mac) - target is HTML element, it is "immune" to scaling
	    // 8 and 16 because of minimal width of magenta target
	    if (dockGlobals.isVideoBeingReceived && position.cropHeight > 8 && position.cropWidth > 16 && dockGlobals._about.capabilities.showingDockingTarget) {
	      dockGlobals.setTarget(position);
	    } else {
	      $(dockGlobals.targetDiv).css({"display": "none"});
	    }
	
	    // include scale coefficient
	    position.left = Math.round(scaleCoefficient * position.left);
	    position.top = Math.round(scaleCoefficient * position.top);
	    position.width = Math.ceil(scaleCoefficient * position.width);
	    position.height = Math.ceil(scaleCoefficient * position.height);
	    position.cropLeft = Math.round(scaleCoefficient * position.cropLeft) || 0;
	    position.cropTop = Math.round(scaleCoefficient * position.cropTop) || 0;
	    position.cropWidth = Math.floor(scaleCoefficient * position.cropWidth) || 0;
	    position.cropHeight = Math.floor(scaleCoefficient * position.cropHeight) || 0;
	    position.scaleCoefficient = scaleCoefficient;
	
	    return position;
	  },
	
	  sendMessageToAddOn: function (msgName, position, windowType) {
	    var addOnMessageContent = {
	      offsetX: position.left,
	      offsetY: position.top,
	      width: position.width,
	      height: position.height,
	      cropOffsetX: position.cropLeft,
	      cropOffsetY: position.cropTop,
	      cropWidth: position.cropWidth,
	      cropHeight: position.cropHeight,
	      windowType: windowType
	    };
	
	    var scaleCoefficient = position.scaleCoefficient;
	
	    if (msgName === "dockExternalWindow") {
	      addOnMessageContent.title = dockGlobals.frame.top.document.title ||
	        (dockGlobals.frame.top.location.host + dockGlobals.frame.top.location.pathname + dockGlobals.frame.top.location.search);
	
	    } else {
	      addOnMessageContent.pageWidth = Math.floor($(dockGlobals.frame.top).width() * scaleCoefficient);
	      addOnMessageContent.pageHeight = Math.floor($(dockGlobals.frame.top).height() * scaleCoefficient);
	
	    }
	
	    sendClientRequest(msgName, addOnMessageContent); // TODO: couple of ms could be saved if we call _plugin.api.sendRequest directly.
	  },
	
	  resetPosition: function () {
	    if (dockGlobals.targetDiv) {
	      $(dockGlobals.targetDiv).css('display', 'none');
	    }
	
	    dockGlobals.position = {};
	    dockGlobals.timeOfPreviousDocking = 0;
	  },
	
	  setTarget: function (position) {
	    // if the overlay DOM Element is only partially visible, the target should be at the top left of the visible part of the DOM Element
	    var targetTop = (position.top + position.cropTop > 0 || position.bottom <= /* magenta target height */ 8) ?
	      Math.ceil(position.top + position.cropTop) : 0;
	    var targetLeft = (position.left + position.cropLeft > 0 || position.right <= /* magenta target (+ border) width */ 16) ?
	      Math.ceil(position.left + position.cropLeft) : 0;
	
	    dockGlobals.createTargetDivIfNeeded();
	    $(dockGlobals.targetDiv).css({"display": "block", "top": targetTop, "left": targetLeft});
	  },
	
	  createTargetDivIfNeeded: function () {
	    if (!dockGlobals.targetDiv) {
	      dockGlobals.targetDiv = dockGlobals.frame.top.document.createElement("div");
	      dockGlobals.frame.top.document.body.appendChild(dockGlobals.targetDiv);
	      $(dockGlobals.targetDiv).css(dockGlobals.targetDivStyle);
	    }
	  }
	
	};
	
	module.exports = dockGlobals;

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	var sendClientRequest = __webpack_require__(7);
	var $ = __webpack_require__(6);
	
	/**
	 * Helper function. Wraps a function of arity n into another anonymous function of arity 0. Used for passing a function with bound(pre-set) arguments as properties on event objects.
	 * @param   {Function} f function to wrap
	 * @param   {Any} arg f's argument
	 * @private
	 */
	exports.wrap = function wrap(f) {
	  var fn = f,
	    args = [].slice.call(arguments, 1);
	  return function () {
	    return fn.apply(this, args);
	  };
	};
	
	exports.debounce = function debounce(fn, t) {
	  var timeout = null;
	
	  return function () {
	    var args = arguments,
	      self = this;
	
	    if (timeout) {
	      clearTimeout(timeout);
	    }
	    timeout = setTimeout(function () {
	      timeout = null;
	      fn.apply(self, args);
	    }, t);
	  };
	};
	
	/**
	 * Function decorator which negates the result of passed function
	 * @param   {Function} fn function to decorate
	 * @returns {Function} negated passed-in function
	 * @private
	 */
	exports.not = function not(fn) {
	  return function () {
	    return !fn.apply(this, [].slice.call(arguments));
	  };
	}
	
	/**
	 * Check if some value is an object. Needed because of JS quirk "typeof null" returns "object"
	 * @param   {Any} o value to check
	 * @returns {Boolean} result of check
	 * @private
	 */
	exports.isObject = function isObject(o) {
	  return typeof o === 'object' && o !== null;
	}
	
	
	exports.isNumeric = function isNumeric(obj) {
	  return !$.isArray(obj) && (obj - parseFloat(obj) + 1) >= 0;
	}
	
	exports.isMac = function isMac() {
	  return navigator.platform && navigator.platform.indexOf('Mac') !== -1;
	}
	
	/**
	 * send string to plugin for encryption
	 * @param {String} str
	 * @param {Function} cb(error, result)  callback to be called with result/error
	 * @private
	 */
	exports.encrypt = function encrypt(str, cb) {
	  function encryptCb(res) {
	    if (res) {
	      cb(null, res);
	    } else {
	      cb('Empty response from plugin');
	    }
	  }
	
	  if (str && typeof str === 'string') {
	    sendClientRequest('encryptCucmPassword', {password: str}, encryptCb);
	  } else {
	    cb('Invalid input string');
	  }
	}
	
	/**
	 * Enables button wrapped in jQuery object
	 * @param   {jQuery} $el selected button object
	 * @returns {jQuery} button passed-in
	 * @private
	 */
	exports.enable = function enable($el) {
	  if (!($el instanceof jQuery)) {
	    throw new TypeError('enable function accepts only jQuerys');
	  }
	
	  $el.attr('disabled', false);
	
	  return $el;
	}
	
	/**
	 * Disables button wrapped in jQuery object
	 * @param   {jQuery} $el selected button object
	 * @returns {jQuery} button passed-in
	 * @private
	 */
	exports.disable = function disable($el) {
	  if (!($el instanceof jQuery)) {
	    throw new TypeError('disable function accepts only jQuery objects');
	  }
	
	  $el.attr('disabled', true);
	
	  return $el;
	}
	
	// shallow object comparison
	exports.objEqual = function (o1, o2) {
	  var equal = true;
	
	  if (o1 === o2) {
	    return true;
	  }
	
	  $.each(o1, function (key, value) {
	    if(o2[key] !== value) equal = false;
	  });
	
	  return equal;
	}
	


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var about = __webpack_require__(15);
	
	var log = __webpack_require__(3);
	var generateUniqueId = __webpack_require__(11);
	
	var cwicState = __webpack_require__(9);
	var errors = __webpack_require__(5);
	var getError = errors.getError;
	
	var $ = __webpack_require__(6);
	
	
	
	
	var videoWindowLoadedCallbacks = {
	
	  windowobjects: [],
	
	  getWindowId: function (args) {
	    var win = args.window;
	    var windowid = this.windowobjects.indexOf(win);
	
	    if (windowid === -1 && !args.readOnly) {
	      this.windowobjects.push(win);
	      windowid = this.windowobjects.indexOf(win);
	      this.callbacks[windowid] = {};
	
	    }
	
	    return windowid;
	  },
	
	  // callbacks[windowId] = {pluginId: {callback: <function>, wascalled: <bool> }}
	  callbacks: [],
	
	  callCallback: function (win, pluginIdIn) {
	    var windowId = this.getWindowId({window: win, readOnly: true});
	    var pluginId;
	
	    function callbackInner($this, windowId, pluginId) {
	
	      if ($this.callbacks[windowId].hasOwnProperty(pluginId)) {
	        var onloaded = $this.callbacks[windowId][pluginId];
	
	        if (!onloaded.wascalled && onloaded.callback) {
	          try {
	            onloaded.callback(pluginId);
	          } catch (videoLoadedException) {
	            log('Exception occurred in application videoLoaded callback', videoLoadedException);
	
	            if (typeof console !== 'undefined' && console.trace) {
	              console.trace();
	            }
	          }
	
	          onloaded.wascalled = true;
	        }
	      }
	    }
	
	    if (pluginIdIn) {
	
	      // Correct plugin id provided by v3 MR2 or later plugin, just call the correct one.
	      callbackInner(this, windowId, pluginIdIn);
	
	    } else {
	
	      // Fallback to the old buggy way where thh id was not available in the onload callback.
	      for (pluginId in this.callbacks[windowId]) {
	
	        if (this.callbacks[windowId].hasOwnProperty(pluginId)) {
	          callbackInner(this, windowId, pluginId);
	        }
	      }
	    }
	  }
	};
	
	var videoPluginId = 1;
	
	// we should call configure (video object) method only once, when the first video object is created...
	var configureCalled = false;
	
	/**
	 * Creates an object that can be passed to startConversation, addPreviewWindow, or updateConversation('addRemoteVideoWindow').
	 * The object is inserted into the element defined by the jQuery context - e.g. jQuery('#placeholder').cwic('createVideoWindow')
	 * inserts the videowindow under jQuery('#placeholder')
	 * <br>
	 * <br>NOTE: This function will just 'do nothing' and the success() callback will never be called if either of the following are true:
	 * <ul>
	 * <li>video is not supported on the platform, see {@link aboutObject#capabilities:video}</li>
	 * <li>video plugin objects are not supported in the browser, see {@link aboutObject#capabilities:videoPluginObject}</li>
	 * </ul>
	 * NOTE: System resources used when video windows are created cannot be reliably released on all platforms.  The application should reuse the
	 * video objects returned by createVideoWindow, rather than creating new windows for each call to avoid performance problems on some client platforms.
	 * @memberof $.fn.cwic
	 * @example $('#videocontainer').cwic('createVideoWindow', {
	    *      id: 'videoplugin',
	    *      success: function(pluginid) {$('#conversation').cwic('updateConversation',{'addRemoteVideoWindow': pluginid});}
	    * });
	 * @param {Object} [settings] Settings to use when creating the video render object
	 * @param {String} [settings.id = generated] The DOM ID of the element to be created
	 * @param {Function} [settings.success] Called when the object is loaded and ready for use plug-in ID is passed as a parameter
	 * @param {String} [settings.onload] Not recommended for video windows created in the same window as the main phone plug-in.
	 * <br>Mandatory in popup windows or iframes. The string must be the name of a function in the global scope, and the function
	 * must call parent or opener {@link window._cwic_onPopupVideoPluginLoaded}.  This function will be called in the onload handler
	 * of the video object.
	 * <br>Single parameter is the videoplugin object that must be passed to the parent handler.
	 */
	function createVideoWindow(settings) {
	  var $this = this;
	  var ab = about();
	
	  if (ab.capabilities.video && ab.capabilities.videoPluginObject) {
	    settings = settings || {};
	    settings.window = settings.window || window;
	
	    var mimetype = 'application/x-cisco-cwc-videocall';
	    var onload = settings.onload || '_cwic_onVideoPluginLoaded';
	    var callback = settings.success;
	    var id = settings.id || '_cwic_vw' + videoPluginId;
	
	    videoPluginId++;
	
	    var windowid = videoWindowLoadedCallbacks.getWindowId({window: settings.window});
	    videoWindowLoadedCallbacks.callbacks[windowid][id] = {callback: callback, wascalled: false};
	
	    var elemtext = '<object type="' + mimetype + '" id="' + id + '"><param name="loadid" value="' + id + '"></param><param name="onload" value="' + onload + '"></param></object>';
	    $($this).append(elemtext);
	
	  }
	
	  return $this;
	}
	
	// generalization for executing video object operations.
	// Validates window object passed in
	function execVideoObjectOperation(args) {
	  log(true, 'execVideoObjectOperation called with arguments', args);
	
	  var winObj = args.window || window;
	  var videoObject = args.videoObject;
	  var methodName = args.methodName;
	
	  if (typeof videoObject === 'string') {
	    try {
	      videoObject = winObj.document.getElementById(videoObject);
	    } catch (e) {
	      log(true, 'execVideoObjectOperation: invalid window object');
	      // handle wrong window object
	      if (args.error) {
	        args.error(getError('InvalidArguments'));
	      }
	
	      return;
	    }
	  }
	
	  // validates video object and calls methodName on that object
	  callMethodOnVideoObj({
	    obj: videoObject,
	    methodName: methodName,
	    error: args.error || null,
	    success: args.success || null,
	    callId: args.callId || null
	  });
	}
	
	/**
	 * Assign a video window object to preview (self-view).
	 * @memberof $.fn.cwic
	 * @example
	 * $('#phone').cwic('addPreviewWindow',{previewWindow: 'previewVideoObjectID'});
	 * $('#phone').cwic('addPreviewWindow',{previewWindow: previewVideoObject, window: iFramePinPWindow});
	 * $('#phone').cwic('addPreviewWindow',{previewWindow: 'previewVideoObjectID', error: function(err){console.log(err)}});
	 * @param {Object} args arguments object
	 * @param {DOMWindow} [args.window] DOM Window that contains the plug-in Object defaults to current window
	 * @param {String|Object} args.previewWindow ID or DOM element of preview window
	 * @param {Function} [args.error] Called when arguments object is malformed, i.e. args.previewWindow ID or DOM element is non-existent or malformed
	 */
	function addPreviewWindow(args) {
	  var $this = this;
	  var ab = about();
	
	  if (ab.capabilities.videoPluginObject === false) {
	    log(false, 'addPreviewWindow called from unsupported browser');
	
	    return;
	  }
	
	  args.methodName = 'addPreviewWindow';
	  args.videoObject = args.previewWindow;
	
	  execVideoObjectOperation(args);
	
	  return $this;
	}
	
	/**
	 * Remove a video window object from preview (self-view)
	 * @memberof $.fn.cwic
	 * @example
	 * $('#phone').cwic('removePreviewWindow', {
	     *   previewWindow: 'previewVideoObjectID'
	     * });
	 *
	 * $('#phone').cwic('removePreviewWindow', {
	     *   previewWindow: previewVideoObject,
	     *   window: iFramePinPWindow
	     * });
	 *
	 * $('#phone').cwic('removePreviewWindow', {
	     *   previewWindow: 'previewVideoObjectID',
	     *   error: function (err) {
	     *     console.log(err)
	     *   }
	     * });
	 * @param {Object} args arguments object
	 * @param {DOMWindow} [args.window] DOM Window that contains the plug-in Object defaults to current window
	 * @param {String|Object} args.previewWindow id or DOM element of preview window
	 * @param {Function} [args.error] Called when arguments object is malformed, i.e. args.previewWindow ID or DOM element is non-existent or malformed
	 */
	function removePreviewWindow(args) {
	  var $this = this;
	  var ab = about();
	
	  if (ab.capabilities.videoPluginObject === false) {
	    log(false, 'removePreviewWindow called from unsupported browser');
	
	    return;
	  }
	
	  args.methodName = 'removePreviewWindow';
	  args.videoObject = args.previewWindow;
	
	  execVideoObjectOperation(args);
	
	  return $this;
	}
	
	/**
	 * Called from "startConversation" or "updateConversation"
	 * @param {Object} args
	 * @param {String} args.callId
	 * @param {DOMWindow} args.window
	 * @param {String|Object} args.remoteVideoWindow
	 * @private
	 */
	function addWindowToCall(args) {
	  var $this = this;
	  var scb = args.success;
	
	  // flag to distinguish between cases when "startRemoteVideo" or "addWindowToCall" methods should be called
	  // We should call startRemoteVideo when:
	  //     - new conversation is started (from startConversation API)
	  //     - conversation is started without video and later video window is added through updateConversation
	  //     - when swithing between 2 conversations (hold/resume)
	  // We should call addWindowToCall when:
	  //     - new video window is added (updateConversation.addWindowToCall) for the current conversation which already have at least 1 video window
	  // All those calls are covered with activeConversation.lastId flag.
	  // This flag is set when new video window is added (not when new conversation is started!) keeping the last callId value when video window was added last time
	  var newCall = false;
	  var ab = about();
	
	  if (ab.capabilities.videoPluginObject === false) {
	    log(false, 'addWindowToCall called from unsupported browser');
	
	    return;
	  }
	
	  if (cwicState.activeConversation.lastId !== args.callId) {
	    newCall = true;
	  }
	
	  log(true, 'addWindowToCall() called with arguments: ', args);
	  log(true, 'addWindowToCall(): activeConversation object: ', cwicState.activeConversation);
	
	  args.methodName = newCall ? 'startRemoteVideo' : 'addWindowToCall';
	
	  log(true, 'addWindowToCall() calling ' + args.methodName);
	
	  args.videoObject = args.remoteVideoWindow;
	  args.success = function (videoObj) {
	    if (newCall) {
	      log(true, 'addWindowToCall(): setting activeConversation object');
	
	      cwicState.activeConversation.videoObject = videoObj;
	      cwicState.activeConversation.window = args.window;
	      cwicState.activeConversation.lastId = args.callId;
	    }
	
	    if ($.isFunction(scb)) { // if successCb was set, call it
	      scb();
	    }
	  };
	
	  execVideoObjectOperation(args);
	
	  return $this;
	}
	
	/**
	 * Called from "updateConversation"
	 * @param {Object} args
	 * @param {String} args.callId
	 * @param {DOMWindow} args.window
	 * @param {String|Object} args.remoteVideoWindow
	 * @param {Boolean} args.endCall
	 * @private
	 */
	function removeWindowFromCall(args) {
	  var $this = this;
	  var endCall = args.endCall; // flag to distinguish between conversation end and updateConv.removeWindowFromCall
	  var ab = about();
	
	  if (ab.capabilities.videoPluginObject === false) {
	    log(false, 'removeWindowFromCall called from unsupported browser');
	
	    return;
	  }
	
	  if (cwicState.activeConversation.lastId !== args.callId) {
	    log('cannot call removeWindowFromCall for callId ' + args.callId + '. Last call id was: ' + cwicState.activeConversation.lastId);
	
	    return;
	  }
	
	  log(true, 'removeWindowFromCall() called with arguments: ', args);
	  log(true, 'removeWindowFromCall(): activeConversation object: ', cwicState.activeConversation);
	
	  args.methodName = endCall ? 'stopRemoteVideo' : 'removeWindowFromCall';
	
	  log(true, 'removeWindowFromCall() calling method ' + args.methodName);
	
	  args.videoObject = args.remoteVideoWindow;
	  execVideoObjectOperation(args);
	
	  return $this;
	}
	
	/**
	 * Util function
	 * @param {Object} args
	 * @param {Object|String} args.obj Video plugin object
	 * @param {String} args.methodName Method name on video object
	 * @param {Function} [args.error]
	 * @param {Function} [args.success]
	 * @param {String} [args.callId]
	 * @private
	 */
	function callMethodOnVideoObj(args) {
	  var obj = args.obj,
	    methodName = args.methodName,
	    successCb = args.success,
	    errorCb = args.error,
	    msgParams;
	
	  msgParams = generateMessageParamsForIBVideo(args.callId);
	
	  if (obj && (typeof obj[methodName] === 'function' || typeof obj[methodName] === 'object')) { // on IE, NPAPI function is type of 'object'!
	    log(true, 'preparing to call "' + methodName + '" method on video object, with parameters', msgParams);
	
	    obj[methodName].apply(this, msgParams);
	
	    if (successCb) {
	      successCb(obj);
	    }
	
	  } else {
	    log(true, 'calling "' + methodName + '" method on video object failed!');
	
	    if (errorCb) {
	      errorCb(getError('InvalidArguments'));
	    }
	  }
	}
	
	function generateMessageParamsForIBVideo(callId) {
	  var cid = cwicState.clientId || generateUniqueId(),
	    mid = generateUniqueId(),
	    url = window.location.href,
	    hostname = window.location.hostname,
	    name = window.document.title,
	    ret,
	    callIdInt;
	
	  ret = [mid, cid, url, hostname, name];
	
	  if (callId) {
	    try {
	      callIdInt = parseInt(callId, 10);
	    } catch (e) {
	      log(true, 'generateMessageParamsForIBVideo: invalid callId received...');
	      callIdInt = -1;
	    }
	
	    ret.unshift(callIdInt);
	  }
	
	  return ret;
	}
	
	function _cwic_onVideoPluginLoaded(videoPluginObject, win) {
	  log('_cwic_onVideoPluginLoaded called');
	
	  var winObj = win || window;
	
	  if (!configureCalled) {
	    callMethodOnVideoObj({
	      obj: videoPluginObject,
	      methodName: 'configure',
	      success: function successCb() {
	        configureCalled = true;
	      }
	    });
	  }
	
	  // For backward compatibility with existing apps, call the createVideoWindow success callback
	  videoWindowLoadedCallbacks.callCallback(winObj, videoPluginObject.loadid);
	}
	
	
	
	exports._cwic_onVideoPluginLoaded = _cwic_onVideoPluginLoaded; // todo: extract from api layer
	
	// exposed through updateConversation API
	exports.addWindowToCall = addWindowToCall;
	exports.removeWindowFromCall = removeWindowFromCall;
	
	// exposed directly in API
	exports.addPreviewWindow = addPreviewWindow;
	exports.removePreviewWindow = removePreviewWindow;
	exports.createVideoWindow = createVideoWindow;

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var $ = __webpack_require__(6);
	var cwicState = __webpack_require__(9);
	
	/**
	 * Versions, states and capabilities.
	 * @memberof $.fn.cwic
	 * @returns {aboutObject}
	 */
	function about() {
	  log(true, 'about', arguments);
	
	  /*
	   * Versioning scheme: Release.Major.Minor.Revision
	   * Release should be for major feature releases (such as video)
	   * Major for an API-breaking ship within a release (or additional APIs that won't work without error checking on previous plug-ins).
	   * Minor for non API-breaking builds, such as bug fix releases that strongly recommend updating the plug-in
	   * Revision for unique build tracking.
	   */
	
	  var ab = {
	    javascript: {
	      version: '11.7.0.243287',
	      system_release: 'Cisco Unified Communications System Release 11.7'
	    },
	
	    jquery: {
	      version: $.fn.jquery
	    },
	
	    channel: null,    // chrome extension, if any
	
	    plugin: null,       // either NPAPI plugin or native host app
	
	    states: {
	      system: 'unknown',
	      device: {
	        exists: false,
	        inService: false,
	        lineDNs: [],
	        modelDescription: '',
	        name: ''
	      }
	    },
	
	    capabilities: {},
	
	    upgrade: {}
	  };
	
	  // get local cwic javascript version
	  var m = ab.javascript.version.match(/(\d+)\.(\d+)\.(\d+)\.(\d+)/);
	
	  if (m) {
	    ab.javascript.release = m[1];
	    ab.javascript.major = m[2];
	    ab.javascript.minor = m[3];
	    ab.javascript.revision = m[4];
	  }
	
	  // get channel extension version, if any
	  if (typeof cwic_plugin !== 'undefined') {
	    ab.channel = cwic_plugin.about();
	    // at some point, validation of compatibility of Chrome Extension might be needed, but not for now.
	  }
	
	  var plugin = cwicState.plugin;
	
	  // get plugin (either NPAPI or native host) version and validate compatibility
	  if (plugin && plugin.version) {
	    ab.plugin = {
	      version: plugin.version
	    };
	
	  } else {
	    var version = getNpapiPluginVersion();
	
	    if (version === 0) {
	      // something is installed but we can't identify it
	      ab.upgrade.plugin = 'mandatory';
	    } else if (version) {
	      // we extracted a version
	      ab.plugin = {
	        version: {
	          plugin : version
	        }
	      };
	    }
	  }
	
	  if (ab.plugin) {
	    m = ab.plugin.version.plugin.match(/(\d+)\.(\d+)\.(\d+)\.(\d+)/);
	
	    if (m) {
	      ab.plugin.release = m[1];
	      ab.plugin.major = m[2];
	      ab.plugin.minor = m[3];
	      ab.plugin.revision = m[4];
	    }
	
	    // compare javascript and plugin versions to advise about upgrade
	    if (ab.javascript.release > ab.plugin.release) {
	      // release mismatch, upgrade plugin
	      ab.upgrade.plugin = 'mandatory';
	
	    } else if (ab.javascript.release < ab.plugin.release) {
	      // release mismatch, upgrade javascript
	      ab.upgrade.javascript = 'mandatory';
	
	    } else if (ab.javascript.release === ab.plugin.release) {
	      // same release, compare major
	      if (ab.javascript.major > ab.plugin.major) {
	        // newer javascript should always require new plugin
	        ab.upgrade.plugin = 'mandatory';
	
	      } else if (ab.javascript.major < ab.plugin.major) {
	        // newer plugin should generally be backward compatible
	        ab.upgrade.javascript = 'recommended';
	
	      } else if (ab.javascript.major === ab.plugin.major) {
	        // same release.major, compare minor
	        if (ab.javascript.minor > ab.plugin.minor) {
	          ab.upgrade.plugin = 'recommended';
	
	        } else if (ab.javascript.minor < ab.plugin.minor) {
	          ab.upgrade.javascript = 'recommended';
	
	        }
	      }
	    }
	
	  } else {
	    ab.upgrade.plugin = 'unknown';
	
	  }
	
	  if (plugin) {
	    // _plugin.connectionStatus gets set/updated along the way.  If not, keep default from above.
	    ab.states.system = plugin.connectionStatus || ab.states.system;
	    // registration.device gets set in _updateRegistration and registerPhone.  If not, use default device from above.
	    ab.states.device = cwicState.registration.device || ab.states.device;
	    // _plugin.capabilities gets set by _cwic_onFBPluginLoaded.  If not, keep default from above.
	    ab.capabilities = plugin.capabilities || ab.capabilities;
	
	  }
	
	  return ab;
	}
	
	// Returns null if plugin is not installed, 0 if the plugin is installed, but
	// we can't tell what version it is, or the version of the plugin.
	function getNpapiPluginVersion() {
	  // regexp to get the version of the plugin.
	  var r2 = /(\d+\.\d+\.\d+\.\d+)/;
	
	  var match, version = null;
	  var pluginMimeType = navigator.mimeTypes['application/x-ciscowebcommunicator'];
	  var cwcPlugin = pluginMimeType ? pluginMimeType.enabledPlugin : undefined;
	
	  if (cwcPlugin) {
	    // plugin is enabled
	    version = 0;
	
	    if (cwcPlugin.version) {
	      // use explicit version if provided by browser
	      version = cwcPlugin.version;
	
	    } else {
	      // extract version from description
	      match = r2.exec(cwcPlugin.description);
	
	      if (match && match[0]) {
	        version = match[0];
	      }
	    }
	  }
	
	  return version;
	}
	
	module.exports = about;


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	
	var triggerError = __webpack_require__(10);
	
	var initSettings = __webpack_require__(4).settings;
	var cwicState = __webpack_require__(9);
	var handlePluginMessage = __webpack_require__(17);
	
	var NPAPI = __webpack_require__(42);
	
	var $ = __webpack_require__(6);
	
	// SystemModules
	var CompatibilityPluginModule = __webpack_require__(65);
	
	var cwic = {
	  CompatibilityPlugin : CompatibilityPluginModule.CompatibilityPlugin
	};
	
	
	/**
	 * Wait for the document to be ready, and try to load the Cisco Web Communicator add-on.<br>
	 * If cwic was successfully initialized, call the options.ready handler, <br>
	 * passing some stored properties (possibly empty), <br>
	 * otherwise call the options.error handler<br>
	 * @memberof $.fn.cwic
	 * @param {Object} options Is a set of key/value pairs to configure the phone registration.  See {@link $.fn.cwic-settings} for options.
	 * @example
	 * jQuery('#phone').cwic('init', {
	    *   ready: function(defaults) {
	    *     console.log('sdk is ready');
	    *   },
	    *   error: function(error) {
	    *     console.log('sdk cannot be initialized : ' + error.message);
	    *   },
	    *   log: function(msg, exception) {
	    *     console.log(msg); if (exception) { console.log(exception); }
	    *   },
	    *   errorMap: {
	    *     // localized message for error code 'AuthenticationFailure'
	    *     AuthenticationFailure : { message: 'Nom d'utilisateur ou mot de passe incorrect' }
	    *   },
	    *   predictDevice: function(args) {
	    *       return settings.devicePrefix + args.username;
	    *   }
	    *});
	 */
	function init(options) {
	
	  var $this = this;
	  var paramsCheck;
	
	  cwicState.initContext = $this;
	
	  log('init', arguments);
	
	  extendDefaultSettingsObject(options);
	
	  // check sd params after extending settings object because user defined error callback should be copied to settings object first
	  paramsCheck = validateDiscoveryParams(options);
	
	  if (!paramsCheck.result) {
	    return triggerError($this, initSettings.error, errorMap.InvalidArguments, paramsCheck.reason.join(', '));
	  }
	
	  setupGlobalHandlers($this);
	
	  initAddonOnDocumentReady($this);
	  cwic.CompatibilityPlugin.initialize();
	
	  return $this;
	}
	
	function initialize()
	{
	
	}
	
	function extendDefaultSettingsObject(options) {
	  // replace or extend the default error map
	  if (typeof options.errorMap !== 'undefined') {
	    // extend the default errorMap
	    $.each(options.errorMap, function (key, info) {
	      if (typeof info === 'string') {
	        errorMap[key] = $.extend({}, errorMap[key], {message: info});
	
	      } else if (typeof info === 'object') {
	        errorMap[key] = $.extend({}, errorMap[key], info);
	
	      } else {
	        log('ignoring invalid custom error [key=' + key + ']', info);
	
	      }
	    });
	  }
	
	  // extend the default settings
	  $.extend(initSettings, options);
	}
	
	function validateDiscoveryParams(options) {
	  var ret = {result: true, reason: []};
	
	  // check service discovery related properties if necessary
	  if (initSettings.serviceDiscovery) {
	    if (typeof options.credentialsRequired !== 'function') {
	      ret.reason.push('No credentialRequired callback provided');
	      ret.result = false;
	    }
	
	    if (typeof options.emailRequired !== 'function') {
	      ret.reason.push('No emailRequired callback provided');
	      ret.result = false;
	    }
	
	    if (!options.redirectUri || typeof options.redirectUri !== 'string') {
	      ret.reason.push('No redirectUri parameter provided');
	      ret.result = false;
	    }
	  }
	
	  return ret;
	}
	
	function setupGlobalHandlers($this) {
	  window._cwic_onFBPluginLoaded = NPAPI.makeOnFBPluginLoaded($this);
	}
	
	
	function initAddonOnDocumentReady($this) {
	  $(document.body).ready(function () {
	    if(document.hidden)
	    {
	      function onDocumentShown()
	      {
	        if(!document.hidden)
	        {
	          initAddon($this);
	          document.removeEventListener('visibilitychange', onDocumentShown);
	        }
	      }
	
	      document.addEventListener('visibilitychange', onDocumentShown);
	    }
	    else
	    {
	      initAddon($this);
	    }
	
	  });
	}
	
	function initAddon($this) {
	  if (cwicState.plugin !== null) {
	    return;
	  }
	
	  try {
	    discoverPluginTypeAndInit($this);
	
	  } catch (e) {
	    cwicState.setPlugin(null);
	    triggerError($this, initSettings.error, e);
	  }
	}
	
	// discover addon type: FB plugin(NPAPI/ActiveX) or Chrome Native Host
	function discoverPluginTypeAndInit($this) {
	  var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	
	  if (is_chrome === true) {
	
	    // try release extId first
	    var extSettings = {
	      cwicExtId: 'ppbllmlcmhfnfflbkbinnhacecaankdh',
	      verbose: initSettings.verbose
	    };
	
	    var createScript = function (extSettings) {
	      var s = document.createElement('script');
	
	      s.id = extSettings.cwicExtId;
	
	      s.onload = function () {
	        cwic_plugin.init(handlePluginMessage, extSettings);
	      };
	      s.onerror = function () {
	        triggerError($this, initSettings.error, errorMap.ExtensionNotAvailable, 'Chrome requires Cisco Web Communicator extension');
	      };
	
	      s.src = 'chrome-extension://' + s.id + '/cwic_plugin.js';
	
	      return s;
	    };
	
	    if (typeof cwic_plugin === 'undefined') {
	      var script = createScript(extSettings);
	
	      script.onerror = function () {
	
	        // remove our first attempt
	        try {
	          document.head.removeChild(document.getElementById(extSettings.cwicExtId));
	        } catch (e) {
	          log('Cannot remove release extension DOM element...');
	        }
	
	        // try or dev extId second
	        log('Failed loading release version of Chrome extension.  Attempting to load dev version next.');
	
	        extSettings.cwicExtId = 'kekllijkldgcokjdjphahkijinjhlapf';
	        script = createScript(extSettings);
	        document.head.appendChild(script);
	      };
	
	      document.head.appendChild(script);
	
	    } else {
	      log(true, 'calling init on previously loaded cwic_plugin script');
	
	      extSettings.cwicExtId = cwic_plugin.about().cwicExtId;
	      cwic_plugin.init(handlePluginMessage, extSettings);
	    }
	
	    return;
	  }
	
	  var npapiPlugin = NPAPI.isNpapi();
	
	  if (npapiPlugin) {
	    // TODO: add _cwic_onFBPluginLoaded('/comm/npapiInterface.js') to global scope (in main or init???)
	    $(document.body).append('<object id="cwc-plugin" width="1" height="1" type="application/x-ciscowebcommunicator"><param name="onload" value="_cwic_onFBPluginLoaded"></param></object>');
	
	  } else {
	    throw errors.getError('PluginNotAvailable');
	  }
	}
	
	module.exports = init;

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var triggerError = __webpack_require__(10);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var getError = errors.getError;
	var initSettings = __webpack_require__(4).settings;
	
	var clientRequestCallbacks = __webpack_require__(8);
	var eventHandlers = __webpack_require__(18);
	
	var log = __webpack_require__(3);
	
	var $ = __webpack_require__(6);
	
	// SystemModules
	var CompatibilityPluginModule = __webpack_require__(65);
	
	var cwic = {
	  CompatibilityPlugin : CompatibilityPluginModule.CompatibilityPlugin
	};
	
	/**
	 *
	 * @param msg
	 * @param msg.ciscoChannelServerMessage
	 * @param msg.ciscoSDKServerMessage
	 */
	function handlePluginMessage(msg) {
	  var $this = cwicState.plugin ? cwicState.plugin.scope : this;
	
	  cwic.CompatibilityPlugin.onMessageReceived(msg);
	
	  if (msg.ciscoChannelServerMessage) {
	
	    if (msg.ciscoChannelServerMessage.name === 'ChannelDisconnect') {
	      log('Extension channel disconnected', msg.ciscoChannelServerMessage);
	
	      cwicState.setPlugin(null);
	      clientRequestCallbacks.purge();
	      triggerError($this, initSettings.error, errorMap.ExtensionNotAvailable, 'Lost connection to browser extension');
	
	    } else if (msg.ciscoChannelServerMessage.name === 'HostDisconnect') {
	      log('Host application disconnected', msg.ciscoChannelServerMessage);
	
	      cwicState.setPlugin(null);
	      clientRequestCallbacks.purge();
	      triggerError($this, initSettings.error, errorMap.PluginNotAvailable, 'Lost connection to plugin');
	
	    } else {
	      log('ciscoChannelServerMessage unknown name: ' + msg.ciscoChannelServerMessage.name);
	
	    }
	
	  } else if (msg.ciscoSDKServerMessage) {
	    var content = msg.ciscoSDKServerMessage.content;
	    var error = msg.ciscoSDKServerMessage.error;
	    var msgId = msg.ciscoSDKServerMessage.replyToMessageId;
	    var name = msg.ciscoSDKServerMessage.name;
	
	    log(true, '_handlePluginMessage: ' + name, msg.ciscoSDKServerMessage);
	
	    // *************************************************************
	    // Lifecycle errors customizations BEGIN
	    // *************************************************************
	    // plugin sends an array of error codes. We never seen more then one error in that array,
	    // so we will take only the first error and send it as a string to handlers instead of array.
	    // We will log other errors if present
	
	    // content.errors is like [{error: 'err1'}, {error:'err2'}]
	    // simplify it to array of strings.
	    if (content && content.errors) {
	      content.errors = $.map(content.errors, function (errorObj) {
	        return errorObj.error !== '' ? errorObj.error : null;
	      });
	
	      log(true, 'Lifecycle error list contains ' + content.errors.length + ' errors');
	
	      for (var i = 0; i < content.errors.length; i++) {
	        log(true, 'Lifecycle error from the list', content.errors[i]);
	      }
	
	      content.error = content.errors.length ?
	        getError(content.errors[0]) :
	        null;
	    }
	
	    // empty string is passed when there is no error is JCF
	    if (content && content.errors === '') {
	      log(true, 'Received empty string instead of lifecycle error list');
	
	      content.errors = [];
	      content.error = null;
	
	    }
	
	    // *************************************************************
	    // Lifecycle errors customizations END
	    // *************************************************************
	
	
	    // first check if we have a callback waiting
	    if (msgId) {
	      clientRequestCallbacks.callCallback(msgId, error, content);
	    }
	
	    switch (name) {
	      case 'init':
	        eventHandlers.initHandler(content);
	        break;
	      case 'userauthorized':
	        eventHandlers.userAuthorizedHandler(content);
	        break;
	      case 'connectionstatuschange':
	        eventHandlers.connectionStatusChangeHandler($this, content);
	        break;
	      case 'multimediacapabilitiesstarted':
	        eventHandlers.mmCapabilitiesStartedHandler($this, true);
	        break;
	      case 'multimediacapabilitiesstopped':
	        eventHandlers.mmCapabilitiesStartedHandler($this, false);
	        break;
	      case 'ringtonechanged':
	        eventHandlers.currentRingtoneChangedHandler($this, content);
	        break;
	      case 'attendedtransferstatechange':
	        eventHandlers.attendedTransferStateChangeHandler($this, content);
	        break;
	      case 'multimediadevicechange':
	        //multimedia devices changed, now go get the new list
	        eventHandlers.mmDeviceChangeHandler($this, content);
	        break;
	      case 'connectionfailure':
	        // all errors should be handled through lifecycle states
	        eventHandlers.connectionFailureHandler($this, content);
	        break;
	      case 'authenticationresult':
	        eventHandlers.authResultAndStatusHandler($this, content);
	        break;
	      case 'telephonydeviceschange':
	        eventHandlers.telephonyDevicesChangeDebouncedHandler($this, content);
	        break;
	      case 'callstatechange':
	        eventHandlers.callStateChangeHandler($this, content);
	        break;
	      case 'externalwindowevent':
	        eventHandlers.externalWindowEventHandler($this, content);
	        break;
	      case 'videoresolutionchange':
	        eventHandlers.videoResolutionChangeHandler($this, content);
	        break;
	      case 'ssonavigateto':
	        eventHandlers.ssoNavigateToHandler($this, content);
	        break;
	      case 'userprofilecredentialsrequired':
	        // content.errors - [string], content.error - string, content.authenticatorId - number
	        eventHandlers.credentialsRequiredHandler($this, content);
	        break;
	      case 'ssosigninrequired':
	        // content.errors - [string], content.error - string
	        eventHandlers.ssoSignInRequiredHandler($this, content);
	        break;
	      case 'userprofileemailaddressrequired':
	        // content.errors - [string], content.error - string
	        eventHandlers.emailRequiredHandler($this, content);
	        break;
	      case 'loggedin':
	        eventHandlers.signedInHandler($this);
	        break;
	      case 'invokeresetdata':
	        eventHandlers.invokeResetDataHandler();
	        break;
	      case 'lifecyclestatechanged':
	        eventHandlers.lifecycleStateChangedHandler($this, content);
	        break;
	      case 'lifecyclessosessionchanged':
	        eventHandlers.lifecycleSSOSessionChanged($this, content);
	        break;
	      case 'cancancelsinglesignonchanged':
	        eventHandlers.canCancelSSOChangedHandler($this, content);
	        break;
	      case 'invalidcertificate':
	        eventHandlers.invalidCertificateHandler($this, content);
	        break;
	      case 'dockexternalwindowneeded':
	        eventHandlers.dockExternalWindowNeededHandler($this, content);
	        break;
	      default:
	       //log(true, 'Unhandled "'+ name + '" message received from plugin...', msg);
	    }
	  } else {
	    log(true, 'Unknown msg from plugin: ', msg);
	  }
	}
	
	module.exports = handlePluginMessage;

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	exports.attendedTransferStateChangeHandler = __webpack_require__(19);
	exports.authResultAndStatusHandler = __webpack_require__(21);
	exports.callStateChangeHandler = __webpack_require__(22);
	exports.canCancelSSOChangedHandler = __webpack_require__(24);
	exports.connectionFailureHandler = __webpack_require__(25);
	exports.connectionStatusChangeHandler = __webpack_require__(29);
	exports.credentialsRequiredHandler = __webpack_require__(33);
	exports.currentRingtoneChangedHandler = __webpack_require__(29);
	exports.emailRequiredHandler = __webpack_require__(36);
	exports.externalWindowEventHandler = __webpack_require__(37);
	exports.initHandler = __webpack_require__(39);
	exports.invalidCertificateHandler = __webpack_require__(46);
	exports.invokeResetDataHandler = __webpack_require__(48);
	exports.lifecycleSSOSessionChanged = __webpack_require__(49);
	exports.lifecycleStateChangedHandler = __webpack_require__(50);
	exports.mmCapabilitiesStartedHandler = __webpack_require__(51);
	exports.mmDeviceChangeHandler = __webpack_require__(55);
	exports.signedInHandler = __webpack_require__(56);
	exports.ssoNavigateToHandler = __webpack_require__(58);
	exports.ssoSignInRequiredHandler = __webpack_require__(60);
	exports.telephonyDevicesChangeDebouncedHandler = __webpack_require__(61);
	exports.userAuthorizedHandler = __webpack_require__(45);
	exports.videoResolutionChangeHandler = __webpack_require__(62);
	exports.dockExternalWindowNeededHandler = __webpack_require__(63);


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(6);
	var log = __webpack_require__(3);
	var utils = __webpack_require__(13);
	var sendClientRequest = __webpack_require__(7);
	var cwicState = __webpack_require__(9);
	var triggerError = __webpack_require__(10);
	var errors = __webpack_require__(5);
	var triggerCallTransferInProgressEvent = __webpack_require__(20);
	
	
	function attendedTransferStateChangeHandler($this, content) {
	  var transferCallGlobals = cwicState.transferCallGlobals;
	  var callId = content.callId
	  var $completeBtn = transferCallGlobals.completeBtn;
	
	  transferCallGlobals.callId = callId;
	
	
	  function finishCallTransfer(event) {
	    completeTransfer($this, callId);
	
	    if ($completeBtn) {
	      utils.disable($completeBtn).unbind();
	    }
	  }
	
	  if (!callId) {
	    log('attendedTransferStateChangeHandler: No callId received from the plugin. Returning from the function...');
	
	    return;
	  }
	
	  log(true, 'attendedTransferStateChangeHandler: Received callId: ' + callId);
	
	  // check if complete and cancel buttons are passed in
	  if (typeof $completeBtn === 'string') {
	    $completeBtn = $('#' + $completeBtn);
	  }
	
	  // duck typing to check if passed-in buttons are jQuery objects that we need
	  if ($completeBtn && (typeof $completeBtn.one === 'function') && $completeBtn.attr) {
	    utils.enable($completeBtn).unbind().one('click', finishCallTransfer);
	
	    return;
	  } else {
	    triggerCallTransferInProgressEvent($this, utils.wrap(completeTransfer, $this, callId));
	
	    return;
	  }
	}
	
	/**
	 * Completes ongoing call transfer
	 * @param {Number} callId conversation id of active conversation
	 * @private
	 */
	function completeTransfer($this, callId) {
	  var transferCallGlobals = cwicState.transferCallGlobals;
	
	  if (transferCallGlobals.inProgress) {
	    sendClientRequest('completeTransfer', {
	      callId: callId
	    }, $.noop, function errorCb(error) {
	      triggerError($this, errors.getError(error, 'NativePluginError'), 'completeTransfer', error);
	    });
	
	    transferCallGlobals.endTransfer();
	
	    return true;
	
	  } else {
	    log(true, 'completeTransfer: transfer not in progress, returning from function...');
	
	    return false;
	  }
	}
	
	
	module.exports = attendedTransferStateChangeHandler;

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(6);
	
	function triggerCallTransferInProgressEvent($this, completeTransfer) {
	  //Emit public event
	  var event = $.Event('callTransferInProgress.cwic');
	
	  // add wrapped function to event object
	  event.completeTransfer = completeTransfer;
	  $this.trigger(event);
	
	}
	
	module.exports = triggerCallTransferInProgressEvent;

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var cwicState = __webpack_require__(9);
	
	/**
	 * Authentication result handler. Only logs received event. Sign in logic is moved to lifecycle event handlers.
	 * @private
	 */
	function authResultAndStatusHandler($this, resultAndStatus) {
	  var regGlobals = cwicState.regGlobals;
	  var result = resultAndStatus.result;
	  var status = resultAndStatus.status;
	
	  log(true, 'authentication result: ' + result);
	  log(true, 'authentication status: ' + status);
	
	  regGlobals.lastAuthStatus = status;
	}
	
	module.exports = authResultAndStatusHandler;

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	var triggerConversationEvent = __webpack_require__(23);
	
	function callStateChangeHandler($this, conversation){
	  triggerConversationEvent($this, conversation, 'state');
	}
	
	module.exports = callStateChangeHandler;

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var regGlobals = cwicState.regGlobals;
	var transferCallGlobals = cwicState.transferCallGlobals;
	var activeConversation = cwicState.activeConversation;
	var log = __webpack_require__(3);
	var utils = __webpack_require__(13);
	var removeWindowFromCall = __webpack_require__(14).removeWindowFromCall;
	var $ = __webpack_require__(6);
	
	var lastConversationMap = {};
	
	/**
	 * IMPORTANT: if new property is added to call object, this function have to be updated !!!!
	 * @param c1 conversation obj
	 * @param c2 conversation obj
	 * @returns {boolean}
	 */
	function compareConversationObjects(c1, c2) {
	
	  if (
	    c1.audioMuted !== c2.audioMuted ||
	    c1.callType !== c2.callType ||
	    c1.isConference !== c2.isConference ||
	    c1.exists !== c2.exists ||
	    c1.videoDirection !== c2.videoDirection ||
	    c1.videoMuted !== c2.videoMuted
	
	  ) {
	    return false;
	  }
	
	  if (
	    !utils.objEqual(c1.capabilities, c2.capabilities) ||
	    !utils.objEqual(c1.localParticipant, c2.localParticipant) ||
	    !utils.objEqual(c1.participant, c2.participant) ||
	    c1.participants.length !== c2.participants.length || // comparing just the length, might be necessary to extend it and check the array content also!
	    !utils.objEqual(c1.videoResolution, c2.videoResolution)
	  ) {
	    return false;
	  }
	
	  return true;
	}
	
	/**
	 *
	 * @param $this
	 * @param conversation
	 * @param topic "state" or "render"
	 */
	function triggerConversationEvent($this, conversation, topic) {
	  var conversationId = conversation.callId;
	  var conversationState = conversation.callState;
	  var lastConversation = lastConversationMap[conversationId];
	
	  // bypass JCF bug that emits "Connected" state after conversation is ended - this can cause various bugs, i.e. calling showCallInExternalWindow after conversation is destroyed
	  // it works for locally ended calls, but not for calls ended by the other side
	  if (conversationId === regGlobals.endingCallForId && conversationState === 'Connected') {
	    log(true, 'Ignoring "connect" state!', conversation);
	    return;
	  }
	
	  // ... so, additionally filter all duplicated 'Connect' state notifications
	  lastConversationMap[conversationId] = conversation;
	  if (
	    lastConversation && lastConversation.callState === 'Connected' && conversationState  === 'Connected' &&
	    compareConversationObjects(conversation, lastConversation)
	  ) {
	    _log(true, 'Ignoring "connect" state!', conversation);
	    return;
	  }
	
	  // prevent call transfer while another one is in progress
	  if (transferCallGlobals.inProgress) {
	    conversation.capabilities.canDirectTransfer = false;
	  }
	
	  // determine first participant name and number (remote participant)
	  // CSCug19119: we no longer use conversation.calledPartyName and conversation.calledPartyNumber since those are depreciated from ECC
	  // instead, just grab the first entry from participants list, if available
	  var participant = (conversation.participants && conversation.participants.length > 0) ?
	    conversation.participants[0] :
	    {};
	
	  var number = (participant.directoryNumber && participant.directoryNumber !== '') ?
	    participant.directoryNumber :
	    participant.number;
	
	  participant = $.extend({}, participant, { recipient: number });
	
	  // select the conversation container with class cwic-conversation-{conversationId}
	  var container = $('.cwic-conversation-' + conversationId);
	
	  // if no container, select the outgoing conversation (see startConversation)
	  if (container.length === 0) {
	    container = $('.cwic-conversation-outgoing');
	
	  }
	
	  // at this point container may be empty, which means the conversation is incoming
	
	  var data = container.data('cwic') || {};
	
	  log(true, 'conversation id=' + conversationId + ' state=' + conversation.callState || data.state, conversation);
	
	  // extend conversation
	  conversation = $.extend({}, data, conversation, {
	    id: conversationId,
	    state: conversationState || data.state,
	    participant: $.extend(data.participant, participant)
	  });
	
	  /* ECC call states and old skittles/webphone states
	   OnHook : Disconnected
	   OffHook : Created
	   Ringout : RemotePartyAlerting
	   Ringin : Alerting
	   Proceed : Ringin on Deskphone while on a call amongst others
	   Connected : Connected
	   Hold : Held
	   RemHold : "Passive Held"
	   Resume : ?
	   Busy : n/a (connected)
	   Reorder : Failed
	   Conference : n/a
	   Dialing : Dialing
	   RemInUse : "Passive not held" ("RemInUse" should indicate 'Remote In-Use' state, i.e. the line is a shared-line, and another device is actively using the shared line.)
	   HoldRevert : n/a
	   Whisper : n/a
	   Parked : n/a
	   ParkRevert : n/a
	   ParkRetrieved : n/a
	   Preservation : n/a
	   WaitingForDigits : na/ ? Overlapdial capability ?
	   Spoof_Ringout : n/a
	   */
	  // check for an incoming call - based on condition:
	  // * Empty container and conversation state 'Ringin'
	  if (conversation.state === 'Ringin' && container.length === 0) {
	    // new container for incoming call, application is supposed to attach it to the DOM
	    container = $('<div>').addClass('cwic-data cwic-conversation cwic-conversation-' + conversationId).data('cwic', conversation);
	    $this.trigger('conversationIncoming.cwic', [conversation, container]);
	
	    return;
	
	  } else if ((conversation.state === 'OnHook' && !conversation.capabilities.canOriginateCall) || !conversation.exists) {
	
	    // If we can originate a call, onHook does not mean the call has ended - it means it's just about to start
	    removeWindowFromCall({
	      callId: conversationId,
	      remoteVideoWindow: activeConversation.videoObject,
	      window: activeConversation.window,
	      endCall: true
	    });
	
	    if (container.length === 0) {
	      log('warning: no container for ended conversation ' + conversationId);
	      $this.trigger('conversationEnd.cwic', [conversation]);
	
	      return;
	
	    }
	
	    container.removeData('cwic')
	      .removeClass('cwic-data cwic-conversation cwic-conversation-' + conversation.id)
	      .trigger('conversationEnd.cwic', [conversation]);
	
	    return;
	
	  } else {
	    if (conversation.state === 'OffHook' || conversation.state === 'Connected') {
	
	      // store media connection time
	      if (typeof conversation.connect === 'undefined' && conversation.state === 'Connected') {
	
	        if (container.length === 0) {
	          container = $('<div>').addClass('cwic-conversation cwic-conversation-' + conversationId);
	        }
	
	        $.extend(conversation, { connect: new Date() });
	        container.data('cwic', conversation);
	      }
	
	      // store start time and trigger start event only once
	      if (typeof conversation.start === 'undefined') {
	
	        if (container.length === 0) {
	          container = $('<div>');
	        }
	
	        $.extend(conversation, { start: new Date() });
	        container.data('cwic', conversation);
	
	        container
	          .removeClass('cwic-conversation-outgoing')
	          .addClass('cwic-conversation cwic-conversation-' + conversationId)
	          .data('cwic', conversation);
	
	        $this.trigger('conversationStart.cwic', [conversation, container]);
	
	        return;
	
	      }
	    }
	
	    if (container.length === 0) {
	      // if we've just switched to deskphone mode and there's already a call, create a container div
	      // or if we've just opened a new tab, we also need to trigger a conversation start for an ongoing call
	      container = $('<div>').data('cwic', conversation).addClass('cwic-conversation cwic-conversation-' + conversationId);
	
	      log('warning: no container for updated conversation ' + conversationId);
	
	      if (conversation.exists) {
	        $this.trigger('conversationStart.cwic', [conversation, container]);
	
	        return;
	
	      } else {
	        $this.trigger('conversationUpdate.cwic', [conversation, container]); // trigger update event
	
	        return;
	
	      }
	
	    } else {
	      container.data('cwic', conversation);
	    }
	
	    container.trigger('conversationUpdate.cwic', [conversation, container]); // trigger update event
	  }
	
	}
	
	module.exports = triggerConversationEvent;

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var log = __webpack_require__(3);
	
	/**
	 * Handler for cancancelssochanged event. Signals if it is possible to cancel ongoing SSO login.
	 * @param {Object}   content
	 * @param {Boolean}   content.cancancel
	 * @private
	 */
	function canCancelSSOChangedHandler($this, content) {
	  log(true, 'SSO: CanCancel property changed. New state: ' + content.cancancel);
	
	  cwicState.SSOGlobals.canCancel = content.cancancel;
	}
	
	module.exports = canCancelSSOChangedHandler;

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	var triggerError = __webpack_require__(10);
	var getError = __webpack_require__(5).getError;
	var log = __webpack_require__(3);
	var cwicState = __webpack_require__(9);
	var signOut = __webpack_require__(26);
	
	function connectionFailureHandler($this, failureReason){
	  var regGlobals = cwicState.regGlobals;
	  var registration = cwicState.registration;
	
	  log(true, 'Connection Failure event received with reason: ', failureReason);
	
	  if (failureReason === 'None') {
	    log(false, 'Received "None", returning...');
	
	    return;
	  }
	
	  var errorKey = getError(failureReason, 'ServerConnectionFailure');
	
	  triggerError($this, regGlobals.errorCb, errorKey, failureReason, { registration: registration });
	  if (cwicState.regGlobals.switchingMode) {
	    // signOut to return to clean state
	    signOut();
	  }
	
	}
	
	module.exports = connectionFailureHandler;


/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	var unregisterPhone = __webpack_require__(27);
	var isObject = __webpack_require__(13).isObject;
	var log = __webpack_require__(3);
	
	
	/** <br>
	 * Signs out a registered device from CUCM:<ul>
	 * <li>Ends any active call.</li>
	 * <li>In softphone mode, SIP unregisters, in deskphone mode, closes the CTI connection.</li>
	 * <li>Calls the optional complete callback</li></ul>
	 * If specified, the function is called only in the case where the phone was already registered. <br>
	 * <b>Note:</b> This API is a preferred alternative to unregisterPhone API.<br>
	 * <br>
	 * signOut examples: <br>
	 * @example
	 * // *************************************
	 * // signOut device
	 * jQuery('#phone')
	 *     .cwic('signOut', {
	    *         complete: function() {
	    *             console.log('device is signed out');
	    *         }
	    * });
	 *
	 * @param [args] Is a set of key/value pairs to configure the phone signOut.
	 * @param {Function} [args.complete] Callback called when sign out is successfully completed.<br>
	 * @memberof $.fn.cwic
	 *
	 */
	function signOut(args) {
	  var completeCb;
	
	  log(true, 'signOut', arguments);
	
	  if (isObject(arguments[0]) && typeof arguments[0].complete === 'function') {
	    // call complete callback
	    completeCb = arguments[0].complete;
	  }
	
	  unregisterPhone({forceLogout: true, complete: completeCb});
	}
	
	module.exports = signOut;

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var sendClientRequest = __webpack_require__(7)
	var hideExternalWindow = __webpack_require__(28)
	var isObject = __webpack_require__(13).isObject;
	var log = __webpack_require__(3);
	var $ = __webpack_require__(6);
	
	
	/** <br>
	 * Unregisters a phone from CUCM:<ul>
	 * <li>Ends any active call if this is the last instance or forceLogout is set to true.</li>
	 * <li>In softphone mode, SIP unregisters, in deskphone mode, closes the CTI connection.</li>
	 * <li>Calls the optional complete handler (always called).</li></ul>
	 * @param args Is a set of key/value pairs to configure the phone unregistration.
	 * @param {Function} [args.complete] Callback called when unregistration is successfully completed.<br>
	 * If specified, the handler is called only in the case where the phone was already registered.
	 * @param {Boolean} args.forceLogout: If true, end the phone session even if registered in other instances.
	 * unregisterPhone examples
	 * @example
	 * // *************************************
	 * // unregister phone
	 * jQuery('#phone')
	 *     .unbind('.cwic')             // optionally unbind cwic events (it's done in shutdown API automatically)
	 *     .cwic('unregisterPhone', {
	    *         forceLogout: true,
	    *         complete: function() {
	    *             console.log('phone is unregistered');
	    *         }
	    * });
	 * @memberof $.fn.cwic
	 */
	
	function unregisterPhone() {
	  var $this = this;
	
	  log(true, 'unregisterPhone', arguments);
	
	  // have to close it here because of ECC bug, which cause plugin to crash during signing out while call in progress (preview + remote video)
	  hideExternalWindow();
	
	  // should we remove forceLogout parameter at all? What is the purpose of it? If it is false, nothing happens
	  // leave it only for backward compatibility
	  if (isObject(arguments[0]) && arguments[0].forceLogout === true) {
	    sendClientRequest('logout');
	
	    // reset global registration object
	    cwicState.registration = {
	      devices: {}
	    };
	  }
	
	  if (isObject(arguments[0]) && typeof arguments[0].complete === 'function') {
	    // call complete callback
	    var complete = arguments[0].complete;
	
	    cwicState.regGlobals.unregisterCb = function () {
	      log(true, 'Calling unregisterCb...');
	
	      try {
	        complete();
	
	      } catch (completeException) {
	        log('Exception occurred in application unregister complete callback', completeException);
	        if (typeof console !== 'undefined' && console.trace) {
	          console.trace();
	        }
	      }
	
	      cwicState.regGlobals.unregisterCb = null;
	    };
	  }
	
	  _reset();
	
	  return $this;
	}
	
	/**
	 * Should clear all resources occupied during the signIn/registerPhone process and during regular usage of
	 * registered device.
	 * @private
	 */
	function _reset() {
	  log(true, '_reset: reseting regGlobals...');
	
	  // clear all cwic data - data attached with $('.cwic-data').data('cwic', somedata);
	  $('.cwic-data').removeData('cwic');
	  cwicState.resetGlobals();
	}
	
	module.exports = unregisterPhone;

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var dockGlobals = __webpack_require__(12);
	var sendClientRequest = __webpack_require__(7);
	var log = __webpack_require__(3);
	
	/**
	 * Hides an external video window created by {@link $.fn.cwic-showPreviewInExternalWindow} or {@link $.fn.cwic-showCallInExternalWindow}.
	 * @memberof $.fn.cwic
	 * @since 3.1.0
	 */
	function hideExternalWindow() {
	  if (cwicState.isMultimediaStarted) {
	    log(true, 'hideExternalWindow');
	
	    dockGlobals.isVideoBeingReceived = false;
	    sendClientRequest('hideExternalWindow');
	
	  } else {
	    log(false, 'returning from hideExternalWindow ... not supported in the current state');
	  }
	}
	
	module.exports = hideExternalWindow;

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	var triggerProviderEvent = __webpack_require__(30);
	
	module.exports = function($this, state) {
	  triggerProviderEvent($this, state);
	};

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(6);
	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var cwicState = __webpack_require__(9);
	var triggerConversationEvent = __webpack_require__(23);
	var updateRegistration = __webpack_require__(31);
	
	
	
	function triggerProviderEvent($this, state) {
	  // state = connection status, not call state!
	  log(true, 'providerState ' + state);
	
	  var event = $.Event('system.cwic');
	  event.phone = { status: state, ready: false };
	
	  // _updateRegistration provides a devices list to the callback but we don't use it here
	  var updateRegCb = function () {
	    var regGlobals = cwicState.regGlobals;
	    var registration = cwicState.registration;
	
	    // add global registration to the system event
	    event.phone.registration = registration;
	
	    // otherwise, state is our connectionStatus
	    cwicState.setPropsToPlugin({connectionStatus: state});
	
	    if (state === 'eReady') {
	
	      // call success callback only if registering phone
	      if (regGlobals.registeringPhone || regGlobals.switchingMode) {
	        regGlobals.registeringPhone = false;
	        regGlobals.switchingMode = false;
	
	        // finish registering
	        if (regGlobals.successCb) {
	
	          // extend a local copy of registration to be passed to client's callback
	          var localRegistration = $.extend({}, registration, {
	            cucm: $.makeArray(regGlobals.CUCM),
	            password: regGlobals.passphrase,
	            passphrase: regGlobals.passphrase,
	            mode: null
	          });
	
	          var getPropsCb = function (res) {
	            log(true, 'getPropsCb res: ' + JSON.stringify(res));
	
	            $.extend(localRegistration, { mode: res.mode });
	
	            try {
	              regGlobals.successCb(localRegistration);
	
	            } catch (successException) {
	              log('Exception occurred in application success callback', successException);
	
	              if (typeof console !== 'undefined' && console.trace) {
	                console.trace();
	              }
	            }
	          };
	
	          sendClientRequest('getProperty', {property: 'mode'}, getPropsCb);
	
	        } else {
	          log('warning: no registerPhone success callback');
	
	        }
	      }
	
	      event.phone.ready = true;
	      $this.trigger(event);
	
	      var callsCb = function (result) {
	        $.each($.makeArray(result.calls), function (i, call) {
	          triggerConversationEvent($this, call, 'state');
	        });
	      };
	
	      sendClientRequest('getCalls', callsCb);
	
	    } else if (state === 'eIdle') { // state = connection status, not a call state!
	      if (typeof regGlobals.unregisterCb === 'function') {
	        regGlobals.unregisterCb();
	      }
	
	      $this.trigger(event);
	
	    } else {
	      $this.trigger(event);
	
	    }
	  };
	
	  // update global registration
	  updateRegistration(state, updateRegCb);
	}
	
	module.exports = triggerProviderEvent;

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var sendClientRequest = __webpack_require__(7);
	var getAvailableDevices = __webpack_require__(32);
	
	var $ = __webpack_require__(6);
	
	/**
	 * Update the global registration object with information from the native plug-in
	 */
	function updateRegistration(state, updateRegCb) {
	  var props = {};
	
	  function getDevicesCb(res) {
	    var devices;
	    var registration = cwicState.registration;
	
	    if (res.devices) {
	      props.devices = res.devices;
	    }
	
	    if (res.device) {
	      props.device = res.device;
	      registration.device = $.extend({}, res.device);
	    }
	
	    if (res.line) {
	      props.line = res.line;
	      registration.line = $.extend(registration.line, res.line);
	    }
	
	    if ((state === 'eIdle' && props.devices) || (props.devices && props.device && props.line)) {
	      devices = $.makeArray(props.devices);
	      // merge device information returned by the plug-in
	      $.each(devices, function (i, device) {
	        if (device.name) {
	          var deviceName = $.trim(device.name);
	          registration.devices[deviceName] = $.extend({}, registration.devices[deviceName], device);
	        }
	      });
	
	      if ($.isFunction(updateRegCb)) {
	        // devicesAvailableCb needs raw devices array, not what was put in registration.devices
	        /*
	         * name
	         * description
	         * model
	         * modelDescription
	         * isSoftPhone
	         * isDeskPhone
	         * lineDNs[]
	         * serviceState
	         */
	        updateRegCb(devices);
	      }
	    }
	  }
	
	  //
	  //--------------------------------------------
	
	  // add device and line info except during logout
	  if (state !== 'eIdle') { // state = connection status, not a call state!
	    sendClientRequest('getProperty', {property: 'device'}, getDevicesCb);
	    sendClientRequest('getProperty', {property: 'line'}, getDevicesCb);
	  }
	
	  getAvailableDevices(getDevicesCb);
	}
	
	module.exports = updateRegistration;

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	var sendClientRequest = __webpack_require__(7);
	
	function getAvailableDevices(getDevicesCb) {
	  sendClientRequest('getAvailableDevices', getDevicesCb);
	}
	
	module.exports = getAvailableDevices;

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var validators = __webpack_require__(34);
	
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	var initSettings = __webpack_require__(4).settings;
	var stopSignIn = __webpack_require__(35);
	
	var sendClientRequest = __webpack_require__(7);
	var log = __webpack_require__(3);
	var utils = __webpack_require__(13);
	
	/**
	 * Handler for credentialsrequired event (Service discovery found home cluster without SSO enabled). Only happens when:
	 *        1. Non SSO sign out
	 *        2. Non SSO credentials not already set
	 *        3. Non SSO authentication error
	 * @param   {Object}   content event arguments object
	 * @param   {string[]}   content.errors List of status or error messages
	 * @param   {errorMapEntry}   content.error First error from the list converted to errorMapEntry object
	 * @param   {Number}   content.authenticatorId Authenticator ID for which credentials are required
	 * @private
	 */
	function credentialsRequiredHandler($this, content) {
	  var regGlobals = cwicState.regGlobals;
	
	  if (!regGlobals.registeringPhone) {
	    return;
	  }
	
	  var error = content.error,
	    credentialsValidator = validators.get('credentials'),
	    authId = content.authenticatorId,
	    cachedUser = '';
	
	  //
	  // --------------------------------------------
	  function provideManualCredentials($this) {
	    if (regGlobals.user && regGlobals.passphrase) {
	      return setCredentials(regGlobals.user, regGlobals.passphrase);
	    } else {
	      // should never happened, because credentials are checked in the beginning of registerPhone API
	      // ... actually it happens after sign out, if the following credentialsRequired event is not ignored.
	      stopSignInFromCR($this, errors.getError('AuthenticationFailure'), authId);
	      return;
	    }
	  }
	
	  // CR for Credentials Required
	  function stopSignInFromCR($this, error, authId) {
	    regGlobals.errorState = 'credentialsRequired';
	    regGlobals.lastAuthenticatorId = authId;
	
	    stopSignIn($this, error);
	  }
	
	  function getUserFromCache() {
	    if (regGlobals.user) {
	      return regGlobals.user;
	    } else if (regGlobals.email && typeof regGlobals.email === 'string') {
	      return regGlobals.email.split('@')[0];
	    } else {
	      return '';
	    }
	  }
	
	  function callCredentialsRequiredCb($this) {
	    if (error && regGlobals.credentialsRequiredCalled) {
	      return stopSignInFromCR($this, error);
	    }
	
	    regGlobals.credentialsRequiredCalled = true;
	
	    try {
	      initSettings.credentialsRequired(setCredentials, cachedUser);
	    } catch (e) {
	      // not implemented or exception occurred
	      return triggerError($this, initSettings.error, errorMap.ServiceDiscoveryMissingOrInvalidCallback, e);
	    }
	  }
	
	  function setCredentials(username, passphrase) {
	    var isEncrypted = false;
	
	    function encryptCb(error, result) {
	      if (error) {
	        return triggerError($this, initSettings.error, errors.getError('NativePluginError'), error);
	      }
	
	      passphrase = result;
	
	      sendClientRequest('setUserProfileCredentials', {
	        username: username,
	        password: passphrase,
	        authenticator: authId
	      });
	    }
	
	    if (passphrase && passphrase.encrypted) {
	      passphrase = passphrase.encrypted;
	      isEncrypted = true;
	    }
	
	    if (credentialsValidator.isNotValid(username, passphrase)) {
	      return credentialsRequiredHandler($this, {
	        errors: ['InvalidCredentials'],
	        error: errors.getError('AuthenticationFailure'),
	        authenticatorId: authId
	      });
	    }
	
	    cwicState.registration.user = regGlobals.user = username;
	
	    if (isEncrypted === false) {
	      utils.encrypt(passphrase, encryptCb);
	    } else {
	      sendClientRequest('setUserProfileCredentials', {
	        username: username,
	        password: passphrase,
	        authenticator: authId
	      });
	    }
	  }
	  //
	  //-------------------------------------------------------------
	
	  // if manual login, skip credentialsRequired callback calling and immediately set credentials with user provided values
	  // when logged in in manual mode, then signout, credentialsRequired event will be triggered, but we need to ignore it
	  if (regGlobals.manual) {
	    if (error) {
	      log(true, 'returning from credentialsRequired in manual mode, because of error...');
	      stopSignInFromCR($this, error, authId);
	      return;
	    }
	
	    provideManualCredentials($this);
	  } else {
	    cachedUser = getUserFromCache();
	
	    callCredentialsRequiredCb($this);
	  }
	
	  return;
	}
	
	module.exports = credentialsRequiredHandler;


/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	var not = __webpack_require__(13).not;
	var $ = __webpack_require__(6);
	
	function validateEmail(email) {
	  // FUTURE
	  return true;
	}
	function validateSSOUrl(url) {
	  // FUTURE
	  return true;
	}
	
	function validateUrl(url) {
	  // FUTURE
	  return true;
	}
	
	function validateUsername(username) {
	  // FUTURE (how to validate encrypted string!?)
	
	  return true;
	}
	
	function validatePassphrase(passphrase) {
	  return true;
	}
	
	function validateCredentials(usr, pass) {
	  return true;
	}
	
	function validateSSOToken(urlFragmentWithToken) {
	  // FUTURE
	  return true;
	}
	
	var validatorsMap = {
	  email: {
	    validate: validateEmail,
	    isValid: validateEmail,
	    isNotValid: not(validateEmail)
	  },
	  ssourl: {
	    validate: validateSSOUrl,
	    isValid: validateSSOUrl,
	    isNotValid: not(validateSSOUrl)
	  },
	  credentials: {
	    validate: validateCredentials,
	    isValid: validateCredentials,
	    isNotValid: not(validateCredentials)
	  },
	  url: {
	    validate: validateUrl,
	    isValid: validateUrl,
	    isNotValid: not(validateUrl)
	  },
	  passphrase: {
	    validate: validatePassphrase,
	    isValid: validatePassphrase,
	    isNotValid: not(validatePassphrase)
	  },
	  username: {
	    validate: validateUsername,
	    isValid: validateUsername,
	    isNotValid: not(validateUsername)
	  },
	  ssotoken: {
	    validate: validateSSOToken,
	    isValid: validateSSOToken,
	    isNotValid: not(validateSSOToken)
	  }
	};
	
	module.exports = {
	  '@@unittest/validatorsMap': validatorsMap,
	
	  get: function getValidator(validatorName) {
	    var validator;
	
	    if (typeof validatorName !== 'string') {
	      throw new TypeError(validator + ' is not a string');
	    }
	
	    validator = validatorsMap[$.trim(validatorName)];
	
	    if (validator) {
	      return validator;
	    } else {
	      throw new Error(validatorName + ' is not defined');
	    }
	
	  }
	};


/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	
	
	function stopSignIn($this, error) {
	  triggerError($this, cwicState.regGlobals.errorCb, errorMap.AuthenticationFailure, error, {registration: cwicState.registration});
	
	  cwicState.resetGlobals(); // will delete errorCb, don't call it before
	}
	
	
	module.exports = stopSignIn;


/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var validators = __webpack_require__(34);
	
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	
	var triggerError = __webpack_require__(10);
	var initSettings = __webpack_require__(4).settings;
	var stopSignIn = __webpack_require__(35);
	
	var sendClientRequest = __webpack_require__(7);
	var log = __webpack_require__(3);
	
	
	
	/**
	 * Only happens:
	 *    1. On first use if bootstrapped
	 *    2. Provisioned keys have  not been set to auto generate user profile (for Jabber)
	 *    3. Discovery error occurs
	 * @param   {jQuery} $this  jquery object on which SDK is initiated
	 * @param   {string[]}  content.errors List of errors
	 * @param   {errorMapEntry}   content.error First error from the list converted to errorMapEntry object
	 * @private
	 */
	function emailRequiredHandler($this, content) {
	  var regGlobals = cwicState.regGlobals;
	
	  if (!regGlobals.registeringPhone) {
	    return;
	  }
	
	  var error = content.error,
	    emailValidator = validators.get('email'),
	    cachedEmail = '',
	    emailFromLocalStorage;
	
	  function setEmailAddress(email) {
	    log(true, 'setEmailAddress: ' + email);
	
	    if (emailValidator.isNotValid(email)) {
	      emailRequiredHandler($this, {
	        error: errors.getError('InvalidUserInput'),
	        errors: ['Invalid email submited']
	      });
	
	      return;
	    }
	
	    regGlobals.email = email;
	
	    if (email !== cwicState.emailForManualSignIn) {
	      cwicState.registration.email = email;
	    }
	
	    if (localStorage) {
	      if (email !== cwicState.emailForManualSignIn && email !== localStorage.getItem(cwicState.localCacheKey)) {
	        log(true, 'saving email to localStorage');
	        localStorage.setItem('_cwic_cache_email', email);
	      }
	
	    } else {
	      log(false, 'localStorage not available, check IE mode the app is running in ...');
	    }
	
	    sendClientRequest('setUserProfileEmailAddress', {email: email});
	  }
	
	  // if manual login, skip emailRequired callback calling and immediately set email with arbitrary value, it will be ignored anyway
	  // if error is present, call error callback, defined in registerPhone args, and optionally stop lifecycle
	  if (regGlobals.manual) {
	    if (error) {
	      stopSignInFromER($this, error);
	      return;
	    }
	    return setEmailAddress(cwicState.emailForManualSignIn);
	  }
	
	  if (regGlobals.emailReguiredCalled) {
	    if (!error) {
	      error = 'ServiceDiscoveryFailure';
	    }
	
	    return stopSignInFromER($this, error);
	  }
	
	  regGlobals.emailReguiredCalled = true;
	
	  if (localStorage) {
	    emailFromLocalStorage = localStorage.getItem('_cwic_cache_email');
	
	    if (emailFromLocalStorage) {
	      setEmailAddress(emailFromLocalStorage);
	      return;
	    }
	  } else {
	    log(true, 'localStorage not available, check IE mode the app is running in ...');
	  }
	
	  if (regGlobals.email && typeof regGlobals.email === 'string' && regGlobals.email !== cwicState.emailForManualSignIn) {
	    cachedEmail = regGlobals.email;
	  } else if (regGlobals.user && typeof regGlobals.user === 'string') {
	    cachedEmail = regGlobals.user + '@';
	  }
	
	  try {
	    initSettings.emailRequired(setEmailAddress, cachedEmail);
	  } catch (e) {
	    // not implemented
	    return triggerError($this, initSettings.error, errorMap.ServiceDiscoveryMissingOrInvalidCallback, e);
	  }
	
	}
	
	// ER for Email Required
	function stopSignInFromER($this, error) {
	  cwicState.regGlobals.errorState = 'emailRequired';
	  localStorage && localStorage.removeItem(cwicState.localCacheKey);
	
	  stopSignIn($this, error);
	}
	
	module.exports = emailRequiredHandler;


/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	var triggerExternalWindowEvent = __webpack_require__(38);
	var dockGlobals = __webpack_require__(12);
	
	function externalWindowEventHandler($this, content){
	  triggerExternalWindowEvent($this, content);
	  //for docking to work we need to know when video is being received
	  dockGlobals.isVideoBeingReceived = content.showing;
	}
	
	module.exports = externalWindowEventHandler;


/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(6);
	var log = __webpack_require__(3);
	
	
	function triggerExternalWindowEvent($this, state) {
	  log(true, 'externalWindowEvent', state);
	
	  var event = $.Event('externalWindowEvent.cwic');
	  event.externalWindowState = state;
	  $this.trigger(event);
	}
	
	module.exports = triggerExternalWindowEvent;

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var initSettings = __webpack_require__(4).settings;
	var errors = __webpack_require__(5);
	var getError = errors.getError;
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	
	var log = __webpack_require__(3);
	var assert = __webpack_require__(40);
	
	var about = __webpack_require__(15);
	var shutdown = __webpack_require__(41);
	var showUserAuthorization = __webpack_require__(44);
	var userAuthorizedHandler = __webpack_require__(45);
	
	var $ = __webpack_require__(6);
	
	
	/**
	 * Handler for init reply message received from plugin.
	 * @param {object} [content] Data payload of the init reply message when called by handlePluginMessage.
	 * @param {object} content.version Version details for the loaded plug-in.
	 * @param {object} content.instanceId
	 * @param {object} content.userauthstatus
	 * @param {object} content.capabilities
	 * @returns undefined
	 * @private
	 */
	function initHandler(content) {
	  assert(cwicState.initContext, 'initContext not set by init API');
	
	  var error;
	  var $this = cwicState.initContext;
	
	  if (typeof cwic_plugin !== 'undefined' && cwicState.plugin !== null) {
	    // what to do if _cwic_onPluginLoaded called twice without first unloading?
	    log('plugin is already loaded.');
	    return;
	
	  }
	
	  try {
	
	    if (typeof cwic_plugin !== 'undefined') {
	      //for Chrome
	      // plug-in is available, update global reference
	
	      cwicState.setPlugin({
	        scope: $this,
	        api: cwic_plugin
	      });
	
	    }
	
	    if (cwicState.plugin.api) {
	      cwicState.setPropsToPlugin({
	        instanceId: content.instanceId,
	        version: content.version,
	        userAuthStatus: content.userauthstatus,
	        capabilities: content.capabilities
	      });
	
	    } else {
	      throw getError('PluginNotAvailable');
	
	    }
	
	    log('initialized ' + cwicState.plugin.userAuthStatus + ' plugin', cwicState.plugin.version);
	
	    var ab = about();
	    if (ab.upgrade.plugin === 'mandatory') {
	      triggerError($this, initSettings.error, errorMap.PluginNotAvailable, 'Cisco Web Communicator cannot be used when plugin upgrade is "mandatory"');
	      return;
	
	    }
	
	    $(window).unload(function () {
	      shutdown.call($this, true);
	
	    });
	
	    if (cwicState.plugin.userAuthStatus === 'MustShowAuth') {
	
	      //  MustShowAuth implies we either do delayed Auth or we pop the dialog now
	      if ($.isFunction(initSettings.delayedUserAuth)) {
	        initSettings.delayedUserAuth();
	
	      } else {
	
	        // No additional deniedCb is needed.  _cwic_userAuthHandler will trigger NotUserAuthorized error.
	        showUserAuthorization({ denied : $.noop });
	      }
	    } else if (cwicState.plugin.userAuthStatus === 'UserAuthorized') {
	
	      // domain whitelisting can give us immediate authorization
	      userAuthorizedHandler(true);
	    }
	
	    return;
	
	  } catch (e) {
	    if (typeof console !== 'undefined') {
	      if (console.trace) {
	        console.trace();
	
	      }
	
	      if (console.log && e.message) {
	        console.log('Exception occured in initHandler() ' + e.message);
	
	      }
	    }
	
	    cwicState.setPlugin(null);
	    error = $.extend({}, errorMap.PluginNotAvailable, e);
	
	  }
	
	  return triggerError($this, initSettings.error, 'Cannot Initialize Cisco Web Communicator', error);
	};
	
	module.exports = initHandler;

/***/ },
/* 40 */
/***/ function(module, exports) {

	module.exports = function (val, msg) {
	  if (!val) {
	    throw new Error('Assertion failed: ' + msg);
	  }
	}

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	var initObj = __webpack_require__(4);
	var sendClientRequest = __webpack_require__(7);
	var signOut = __webpack_require__(26);
	var NPAPI = __webpack_require__(42);
	var cwicState = __webpack_require__(9);
	var clientRequestCallbacks = __webpack_require__(8);
	var log = __webpack_require__(3);
	
	/**
	 * Shuts down the API<br>
	 * <ul><li>Unregisters the phone</li>
	 * <li>Unbinds all cwic events handlers</li>
	 * <li>Clears all cwic data</li>
	 * <li>Releases the Cisco Web Communicator add-on instance</li></ul>
	 * @memberof $.fn.cwic
	 * @example
	 *  jQuery(window).unload(function() { <br>
	    *      // not necessary, it is already done in cwic.js by default
	    *      // jQuery('#phone').cwic('shutdown'); <br>
	    *  }); <br>
	 * @example
	 * jQuery('#shutdown').click(function() { <br>
	    *      jQuery('#phone').cwic('shutdown'); <br>
	    *  }); <br>
	 */
	function shutdown(auto) {
	  var $this = this;
	
	  log(true, 'shutdown', arguments);
	
	  initObj.reset();
	
	  // auto means that shutdown is called from onunload event and not by calling API
	  if (auto) {
	    // _sendClientRequest('autologout'); TODO: implement if needed
	  } else {
	    signOut();
	  }
	
	  _resetSSO();
	
	  // unbind all cwic events handlers
	  $this.unbind('.cwic');
	
	  // unbind NPAPI events handlers
	  NPAPI.unregisterNpapiCallbacks();
	  sendClientRequest('releaseInstance');
	  clientRequestCallbacks.purge();
	
	  cwicState.setPlugin(null);
	}
	
	/**
	 * internal function to release all SSO related resources on
	 * (window.unload event) and on invokeResetData
	 * To be implemented if needed
	 * @private
	 */
	function _resetSSO() {
	
	}
	
	module.exports = shutdown;

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	
	var cwicState = __webpack_require__(9);
	var initSettings = __webpack_require__(4).settings;
	
	var triggerError = __webpack_require__(10);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var getError = errors.getError;
	
	var sendClientRequest = __webpack_require__(7);
	
	var eventListeners = __webpack_require__(43);
	var $ = __webpack_require__(6);
	
	
	
	
	var addListener = eventListeners.addListener;
	var removeListener = eventListeners.removeListener;
	
	function msgHandler(result) {
	  var msg;
	  var handlePluginMessage = __webpack_require__(17); // TODO: why it's not working when put outside this fn!!!???
	
	
	
	  try {
	    msg = JSON.parse(result);
	  } catch (e) {
	    log(false, 'Invalid JSON message from plugin', e);
	    throw new Error(errorMap.NativePluginError.code);
	  }
	
	  if (msg.ciscoChannelMessage && msg.ciscoChannelMessage.ciscoSDKServerMessage) {
	    if (msg.ciscoChannelMessage.client && msg.ciscoChannelMessage.client.id && msg.ciscoChannelMessage.client.id !== cwicState.clientId) {
	      return;
	    }
	    msg = msg.ciscoChannelMessage;
	
	  }
	
	  handlePluginMessage(msg);
	}
	
	function registerNpapiCallbacks() {
	  if (!cwicState.plugin) {
	    throw new Error('Cannot register NPAPI callbacks before plugin is loaded!');
	  }
	
	  log(true, 'adding npapi listener for "addonmessage"');
	  addListener(cwicState.plugin.api, 'addonmessage', msgHandler);
	}
	
	// remove registered NPAPI event handlers. Called in 'shutdown' to prevent multiplication of registered events in case when 'init' API function is called multiple times during one session.
	function unregisterNpapiCallbacks() {
	  log(true, 'removing npapi listener for "addonmessage" event.');
	  removeListener(cwicState.plugin.api, 'addonmessage', msgHandler);
	}
	
	/**
	 * Phone Object onLoad handler. This function is called as the onload callback for the NPAPI plugin.
	 * @returns function
	 * @private
	 */
	function makeOnFBPluginLoaded($this) {
	  return function () {
	    var error;
	
	    if (cwicState.plugin !== null) {
	      // what to do if _cwic_onFBPluginLoaded called twice without first unloading?
	      log('plugin is already loaded.');
	      return;
	    }
	
	    try {
	      // look for npapi object
	      var cwcObject = document.getElementById('cwc-plugin'); // don't use jQuery to get NPAPI objects, it doesn't work for jQuery 1.5+
	
	      if (cwcObject) {
	        cwicState.setPlugin({scope: $this, api: cwcObject});
	        registerNpapiCallbacks();
	        sendClientRequest('init');
	
	      } else {
	        throw getError('PluginNotAvailable');
	      }
	
	      return;
	    } catch (e) {
	      if (typeof console !== 'undefined') {
	        if (console.trace) {
	          console.trace();
	        }
	
	        if (console.log && e.message) {
	          console.log('Exception occured in _cwic_onFBPluginLoaded() ' + e.message);
	        }
	      }
	
	      cwicState.setPlugin(null);
	      error = $.extend({}, getError('PluginNotAvailable'), e); //todo: test getError
	    }
	
	    return triggerError($this, initSettings.error, 'Cannot Initialize Cisco Web Communicator', error);
	  }
	}
	
	// ...is NPAPI or ActiveX plugin
	function isNpapi() {
	  var npapiPlugin = false;
	  var pluginMimeType = navigator.mimeTypes['application/x-ciscowebcommunicator'];
	
	  if ('ActiveXObject' in window) {
	    // IE - try to load the ActiveX/NPAPI plug-in, throw an error if it fails
	    try {
	      var dummyAXObj1 = new ActiveXObject('CiscoSystems.CWCVideoCall');
	
	      // no exception, plug-in is available
	      // how to check plug-in is enabled in IE ?
	      npapiPlugin = true;
	
	    } catch (e1) {
	      log(true, 'ActiveXObject("CiscoSystems.CWCVideoCall") exception: ' + e1.message);
	      // check if previous release is installed
	      try {
	        var dummyAXObj2 = new ActiveXObject('ActivexPlugin.WebPhonePlugin.1');
	        // no exception. previous plug-in is available
	        throw getError('ReleaseMismatch');
	
	      } catch (e2) {
	        log(true, 'ActiveXObject("ActivexPlugin.WebPhonePlugin.1") exception: ' + e2.message);
	        throw getError('PluginNotAvailable');
	
	      }
	    }
	  } else if (typeof pluginMimeType !== 'undefined') {
	
	    // Firefox or Safari with our plugin
	    npapiPlugin = true;
	  } else {
	
	    // plug-in not available, check if any previous release is installed
	    pluginMimeType = navigator.mimeTypes['application/x-ciscowebphone'];
	
	    if (typeof pluginMimeType !== 'undefined') {
	      // previous plug-in is available
	      throw getError('ReleaseMismatch');
	    }
	
	  }
	
	  return npapiPlugin;
	}
	
	exports.makeOnFBPluginLoaded = makeOnFBPluginLoaded;
	exports.registerNpapiCallbacks = registerNpapiCallbacks;
	exports.unregisterNpapiCallbacks = unregisterNpapiCallbacks;
	exports.isNpapi = isNpapi;


/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	//TODO: replace this with regular jQuery event handling API
	var log = __webpack_require__(3);
	
	// support unit tests for IE
	// should be more transparent than this, but tell that to internet explorer - you can't override attachEvent...
	exports.addListener = function (obj, type, handler) {
	  try {
	    // if the object has a method called _addListener, then we're running a unit test. todo: check and clean
	    if (obj._addListener) {
	      obj._addListener(type, handler, false);
	    } else if (obj.attachEvent) {
	      obj.attachEvent('on' + type, handler);
	    } else {
	      obj.addEventListener(type, handler, false);
	    }
	  } catch (e) {
	    log('_addListener error: ', e);
	  }
	};
	
	exports.removeListener = function (obj, type, handler) {
	  try {
	    // if the object has a method called _addListener, then we're running a unit test.
	    if (obj._addListener) {
	      return;
	      //obj._removeListener(type,handler,false);
	    } else if (obj.attachEvent) {
	      obj.detachEvent('on' + type, handler);
	    } else {
	      obj.removeEventListener(type, handler, false);
	    }
	  } catch (e) {
	    log('_removeListener error: ', e);
	  }
	};

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var eventListeners = __webpack_require__(43);
	var addListener = eventListeners.addListener;
	var removeListener = eventListeners.removeListener;
	
	var sendClientRequest = __webpack_require__(7);
	var errorMap = __webpack_require__(5).errorMap;
	var triggerError = __webpack_require__(10);
	
	var cwicState = __webpack_require__(9);
	
	var $ = __webpack_require__(6);
	
	
	
	/** @description Shows the user authorization dialog.  This API must only be called if the application has provided a delayedUserAuth callback
	 * in the settings object provided to the init function, and the status returned by {@link $.fn.cwic-getUserAuthStatus} is "MustShowAuth"  If the application
	 * receives the {@link $.fn.cwic-settings.delayedUserAuth} callback, the user authorization state will always be "MustShowAuth" so the application can safely call
	 * showUserAuthorization from within the delayedUserAuth callback without checking getUserAuthStatus.
	 * @since 3.0.1
	 * @memberof $.fn.cwic
	 * @param {Function} denied A callback that will be called if the user selects "deny" from the user authorization dialog.  If the user
	 * selects allow or always allow, the settings.ready callback will be called.
	 * @param {Boolean} [force=false] Since 3.1.0 <br>
	 * Set <tt>true</tt> to force the dialog to display even if the page is currently hidden.
	 * Setting this may cause the dialog to appear when the page is not yet accessible to the user.
	 */
	function showUserAuthorization(args) {
	  if (!args || !args.denied || !$.isFunction(args.denied)) {
	    return triggerError(this, errorMap.InvalidArguments, 'showUserAuthorization: wrong arguments');
	  }
	
	  // if page is not visible, then wait for visibilitychange event and retry showUserAuthorization
	  if (document.hidden && !args.force) {
	    log('showUserAuthorization deferred with visibilityState: ' + document.visibilityState);
	
	    addListener(document, 'visibilitychange', function handleVisibilityChange() {
	      if(!document.hidden) {
	
	        // show deferred dialog and remove listener
	        log('showUserAuthorization detected visibilitychange from hidden to ' + document.visibilityState);
	
	        showUserAuthorization(args);
	        removeListener(document, 'visibilitychange', handleVisibilityChange);
	      }
	
	      // else continue listening for 'visibilitychange' until not hidden
	
	    });
	
	    return;
	  }
	
	  sendClientRequest('showUserAuthorization', function() {
	    cwicState.setPropsToPlugin({
	      deniedCb: args.denied,
	      userAuthStatus: 'UserAuthPending'
	    });
	
	  });
	}
	
	module.exports = showUserAuthorization;


/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var initSettings = __webpack_require__(4).settings;
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	
	var sendClientRequest = __webpack_require__(7);
	
	var triggerProviderEvent = __webpack_require__(30);
	var triggerConversationEvent = __webpack_require__(23);
	
	var log = __webpack_require__(3);
	var $ = __webpack_require__(6);
	
	
	
	module.exports = function (result) {
	  var plugin = cwicState.plugin;
	  var userAuthStatus = result ? 'UserAuthorized' : 'UserDenied';
	  var $this = plugin.scope;
	
	  cwicState.setPropsToPlugin({userAuthStatus: userAuthStatus});
	
	  log('userAuthorizedHandler result: ' + plugin.userAuthStatus);
	
	  if (result === true) {
	    //_getAvailableRingtones($this); // todo: mediadevices service: move after login and revert when JCF fix is ready
	
	    sendClientRequest('getProperty', {property: 'connectionStatus'}, function (result) {
	       cwicState.setPropsToPlugin({connectionStatus: result.connectionStatus});
	      onPluginReady($this);
	    });
	
	  } else {
	    triggerError($this, initSettings.error, 'Cannot Initialize Cisco Web Communicator', errorMap.NotUserAuthorized);
	
	    if (typeof plugin.deniedCb === 'function') {
	      plugin.deniedCb();
	      cwicState.setPropsToPlugin({deniedCb: null});
	    }
	
	  }
	}
	
	// Called by userAuthorizedHandler when user authorization status is "UserAuthorized".
	// This occurs either directly from "initHandler" in the whitelisted case,
	// or when userauthorized event is received in showUserAuthorization case.
	function onPluginReady($this) {
	  var plugin = cwicState.plugin;
	  var error;
	
	  try {
	    var defaults = {};
	    var phoneRegistered = false;
	
	    // current connectionStatus was cached in _plugin before calling _cwic_onPluginReady
	    var currState = plugin.connectionStatus;
	
	    if (currState === 'eReady') { // state = connection status, not a call state!
	      phoneRegistered = true;
	
	    }
	
	    // fire and forget requests
	
	    // Get initial mm device list.  If web calls cwic getMultimediaDevices before this returns, they'll get no devices.
	    // That's ok because the success callback here is _triggerMMDeviceEvent, which tells the webapp to refresh its list.
	    // todo: mediadevices service: move after login and revert
	    //_sendClientRequest('getMultimediaDevices', function mmDevicesCb(content) {
	    //    _triggerMMDeviceEvent($this, content);
	    //});
	
	    // wait for reply
	    var modeCb = function (result) {
	      if ($.isFunction(initSettings.ready)) {
	        try {
	          initSettings.ready(defaults, phoneRegistered, result.mode);
	
	        } catch (readyException) {
	          log('Exception occurred in application ready callback', readyException);
	
	          if (typeof console !== 'undefined' && console.trace) {
	            console.trace();
	          }
	
	        }
	      }
	    };
	
	    sendClientRequest('getProperty', {property: 'mode'}, modeCb);
	
	    if (phoneRegistered) {
	      var callsCb = function (result) {
	        $.each($.makeArray(result.calls), function (i, call) {
	          triggerConversationEvent($this, call, 'state');
	        });
	
	      };
	
	      sendClientRequest('getCalls', callsCb);
	
	    } else {
	      // CSCue51645 ensure app is in sync with initial plug-in state
	      triggerProviderEvent($this, currState);
	
	    }
	
	    return;
	
	  } catch (e) {
	    if (typeof console !== 'undefined') {
	      if (console.trace) {
	        console.trace();
	
	      }
	
	      if (console.log && e.message) {
	        console.log('Exception occured in _cwic_onPluginReady() ' + e.message);
	
	      }
	
	    }
	
	    cwicState.setPlugin(null);
	    error = $.extend({}, errorMap.PluginNotAvailable, e);
	
	    triggerError($this, initSettings.error, 'Cannot Initialize Cisco Web Communicator', error);
	  }
	
	}


/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	var triggerInvalidCertificateEvent = __webpack_require__(47);
	var $ = __webpack_require__(6);
	
	// Inalid certificate event handler
	function invalidCertificateHandler($this, content) {
	  /*
	   content properties:
	   - certFingerprint
	   - identifierToDisplay
	   - certSubjectCN
	   - referenceId
	   - invalidReasons
	   - subjectCertificateData
	   - intermediateCACertificateData
	   - allowUserToAccept
	   - persistAcceptedDecision
	   */
	  if ($.isArray(content.invalidReasons)) {
	    content.invalidReasons = $.map(content.invalidReasons, function (elem) {
	      return elem.invalidReason;
	    });
	
	  } else {
	    content.invalidReasons = [];
	  }
	
	  //Emit public event
	  triggerInvalidCertificateEvent($this, content);
	}
	
	module.exports = invalidCertificateHandler;

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(6);
	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	
	
	// callback which submits user's choice to accept or reject the certificate
	// fp - string, accept - boolean
	function handleInvalidCert(fp, accept) {
	  if (fp && (typeof fp === 'string') && (typeof accept === 'boolean')) {
	    log(true, 'handleInvalidCertificate sending response: ' + accept + ' (for fingerprint - ' + fp);
	    sendClientRequest('handleInvalidCertificate', {certFingerprint: fp, accept: accept});
	
	  } else {
	    throw new TypeError('handleInvalidCert: Wrong arguments!');
	  }
	}
	
	module.exports = function ($this, certInfo) {
	  var event = $.Event('invalidCertificate.cwic');
	
	  event.info = certInfo;
	  event.respond = handleInvalidCert;
	
	  $this.trigger(event);
	}
	
	


/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	
	/**
	 * Handler for invokeresetdata event. It should clear all cached information by browser.
	 * In reality it can just clear/reset to default SSOGlobals object.
	 * Candidate for removal.
	 * @private
	 */
	function invokeResetDataHandler() {
	  log(true, 'SSO: Invoke Reset Data Triggered.');
	}
	
	module.exports = invokeResetDataHandler;

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	
	/**
	 * Handler for lifecyclessosessionchanged event. Used only for debugging.
	 * @param {string} content New lifecycle session state
	 * Candidate for removal.
	 * @private
	 */
	function lifecycleSSOSessionChanged($this, content) {
	  log(true, 'SSO: Lifecycle session changed. New state: ' + content);
	}
	
	module.exports = lifecycleSSOSessionChanged;

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var log = __webpack_require__(3);
	var triggerProviderEvent = __webpack_require__(30);
	
	/**
	 * Handler for lifecyclestatechanged event.
	 * @param {string} content New lifecycle state (CSFUnified::LifeCycleStateEnum::LifeCycleState)
	 * Candidate for removal.
	 * @private
	 */
	function lifecycleStateChangedHandler($this, state) {
	  var regGlobals = cwicState.regGlobals;
	
	  log(true, 'System lifecycle state changed. New state: ' + state);
	
	  if (regGlobals.signingOut && state === 'SIGNEDOUT') {
	    regGlobals.signingOut = false;
	    // emulating eIdle connection state to force calling of 'unregisterCb'
	    // because SIGNEDOUT lifecycle state comes a lot earlier than connection state update.
	    // Registered phone is unavailable in between anyway.
	    triggerProviderEvent($this, 'eIdle');
	
	  }
	
	  if (state === 'SIGNINGOUT') {
	    regGlobals.signingOut = true;
	  }
	
	  // calling reset in the middle of discovery sign in lifecycle, causes Authentication Failure, if emailrequired callback is already called
	  // ...so during reset we reset emailReguiredCalled flag to handle this situation...
	  if (state === 'RESETTING') {
	    regGlobals.emailReguiredCalled = false;
	  }
	
	}
	
	module.exports = lifecycleStateChangedHandler;

/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	var sendClientRequest = __webpack_require__(7);
	var triggerMMDeviceEvent = __webpack_require__(52);
	var triggerMMCapabilitiesEvent = __webpack_require__(53);
	var getAvailableRingtones = __webpack_require__(54);
	var cwicState = __webpack_require__(9);
	var log = __webpack_require__(3);
	
	function mmCapabilitiesStartedHandler($this, isMultimediaCapabilityStarted) {
	  cwicState.isMultimediaStarted = isMultimediaCapabilityStarted;
	
	  triggerMMCapabilitiesEvent($this, isMultimediaCapabilityStarted);
	
	  if (isMultimediaCapabilityStarted) {
	    getAvailableRingtones($this);
	
	    sendClientRequest('getMultimediaDevices', function mmDevicesCb(content) {
	      triggerMMDeviceEvent($this, content);
	    });
	
	    if (typeof cwicState.pendingConnect === 'function') {
	      log(true, 'Sending delayed "connect" message');
	      cwicState.pendingConnect();
	      cwicState.pendingConnect = null;
	    }
	  }
	}
	
	module.exports = mmCapabilitiesStartedHandler;

/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(6);
	var cwicState = __webpack_require__(9);
	var log = __webpack_require__(3);
	
	
	
	function triggerMMDeviceEvent($this, result) {
	  log(true, 'mmDeviceChange', result);
	
	  if (result) {
	    // store the updated device list and notify the web app
	    cwicState.setPropsToPlugin({
	      multimediadevices: result.multimediadevices
	    });
	  }
	
	  var event = $.Event('mmDeviceChange.cwic');
	  $this.trigger(event);
	}
	
	module.exports = triggerMMDeviceEvent;


/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(6);
	var log = __webpack_require__(3);
	
	function triggerMMCapabilitiesEvent($this, started) {
	  log(true, 'mmCapabilitiesEvent', started);
	
	  var event = $.Event('multimediaCapabilities.cwic');
	  event.multimediaCapability = started;
	  $this.trigger(event);
	
	}
	
	module.exports = triggerMMCapabilitiesEvent;


/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

	var sendClientRequest = __webpack_require__(7);
	var utils = __webpack_require__(13);
	var $ = __webpack_require__(6);
	
	function getAvailableRingtones($this) {
	  if (utils.isMac()) {
	    return;
	  }
	
	  sendClientRequest('getAvailableRingtones', function (ringtones) {
	    handleRingtonesAvailable($this, ringtones);
	  });
	}
	
	function handleRingtonesAvailable($this, ringtonesList) {
	  var event = $.Event('ringtonesListAvailable.cwic');
	  event.ringtones = ringtonesList.ringtones;
	  $this.trigger(event);
	}
	
	module.exports = getAvailableRingtones;

/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

	var sendClientRequest = __webpack_require__(7);
	var cwicState = __webpack_require__(9);
	var triggerMMDeviceEvent = __webpack_require__(52);
	
	
	function mmDeviceChangeHandler($this, result) {
	  if (cwicState.isMultimediaStarted) {
	    sendClientRequest('getMultimediaDevices', function mmDevicesCb(content) {
	      triggerMMDeviceEvent($this, content);
	    });
	  }
	}
	
	module.exports = mmDeviceChangeHandler;

/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	
	var initSettings = __webpack_require__(4).settings;
	var proceedWithDeviceSelection = __webpack_require__(57);
	
	var log = __webpack_require__(3);
	
	
	/**
	 * Only happens when the app has successfully been authenticated with the primary authenticator. Calls signedIn callback.
	 * SignedIn state means that Lifecycle state is changed to 'SIGNEDIN'. Device connection is performed after this event is triggered.
	 * @param {JQuery} $this
	 * @private
	 */
	function signedInHandler($this) {
	  var regGlobals = cwicState.regGlobals;
	
	  cwicState.SSOGlobals.inProgress = false;
	
	  log(true, '_triggerSignedIn called');
	  log(true, '_triggerSignedIn: authenticatedCB present: ', regGlobals.authenticatedCallback ? true : false);
	  log(true, '_triggerSignedIn: telephonyDevicesSet: ', regGlobals.telephonyDevicesSet);
	
	  if (regGlobals.authenticatedCallback && regGlobals.telephonyDevicesSet) {
	    // regGlobals.telephonyDevicesSet = false; // it will be reset on sign out
	    log(true, 'Proceeding with device selection immediately after SignedIn event');
	    proceedWithDeviceSelection();
	  } else {
	
	    // device selection will be done after devices are received... see _triggerTelephonyDevicesChange
	    log(true, '_triggerSignedIn: Cannot proceed with device selection until devices are ready...');
	  }
	
	  try {
	    initSettings.signedIn();
	  } catch (e) {
	    // signedIn callback is optional
	    log(true, 'No signedIn callback defined...');
	  }
	}
	
	module.exports = signedInHandler;

/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var cwicState = __webpack_require__(9);
	var updateRegistration = __webpack_require__(31);
	
	function proceedWithDeviceSelection() {
	  var regGlobals = cwicState.regGlobals;
	
	  log(true, 'proceedWithDeviceSelection called');
	
	  updateRegistration(regGlobals.currState, regGlobals.authenticatedCallback);
	}
	
	module.exports = proceedWithDeviceSelection;

/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	var initSettings = __webpack_require__(4).settings;
	var triggerSSONavigateToEvent = __webpack_require__(59);
	var log = __webpack_require__(3);
	var $ = __webpack_require__(6);
	
	
	
	/**
	 * Handler for 'ssonavigateto' event. Constructs valid sso url and triggers public event 'ssoNavigateTo.cwic'.
	 * Developers have to implement logic for redirecting to passed url (popup or iframe).
	 * @param   {jQuery} $this plugin
	 * @param   {String}   url   base url passed from JCF
	 * @fires 'ssoNavigateTo.cwic'
	 * @private
	 */
	function ssoNavigateToHandler($this, url) {
	  if (!initSettings.redirectUri) {
	    return triggerError($this, errorMap.SSOMissingOrInvalidRedirectURI);
	  }
	
	  var newUrl,
	    params,
	    paramsObj = {},
	    urlParts = url.split('?'),
	    server = urlParts[0];
	
	  log(true, 'Plugin sent URL: ' + url);
	
	  try {
	    params = urlParts[1].split('&'); // split url to form ['param1=value', 'param2=value']
	
	    // convert array to object
	    $.each(params, function (index, pair) {
	      var param = pair.split('=');
	      paramsObj[param[0]] = param[1];
	    });
	
	  } catch (e) {
	    log(true, 'Invalid SSO URL received', e);
	    throw new Error('SSO authorization service URL invalid format');
	  }
	
	  paramsObj.client_id = 'C69908c4f345729af0a23cdfff1d255272de942193e7d39171ddd307bc488d7a1';
	  paramsObj.redirect_uri = initSettings.redirectUri; // encodeURIComponent(settings.redirectUri); - not needed, $ does it in param method
	
	  newUrl = server + '?' + $.param(paramsObj);
	
	  log(true, 'New URL generated: ' + newUrl);
	
	  triggerSSONavigateToEvent($this, newUrl);
	}
	
	module.exports = ssoNavigateToHandler;


/***/ },
/* 59 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(6);
	
	function triggerSSONavigateToEvent($this, url) {
	  var event = $.Event('ssoNavigateTo.cwic');
	
	  event.url = url;
	  $this.trigger(event);
	}
	
	module.exports = triggerSSONavigateToEvent;
	


/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var stopSignIn = __webpack_require__(35);
	
	function ssoSignInRequiredHandler($this, content) {
	  if (!cwicState.regGlobals.registeringPhone) {
	    return;
	  }
	
	  var error = content.error;
	
	  cwicState.SSOGlobals.inProgress = false;
	  stopSignIn($this, error);
	}
	
	module.exports = ssoSignInRequiredHandler;

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var cwicState = __webpack_require__(9);
	var proceedWithDeviceSelection = __webpack_require__(57);
	var debounce = __webpack_require__(13).debounce;
	
	/**
	 * Handler for telephonydeviceschange event. Triggered every time devices are changed.
	 * When user is switched, this event will also triger with empty device list. That event should be filtered.
	 * @param {Array}   content.devices array of available devices
	 * @private
	 */
	function telephonyDevicesChangeHandler($this, content) {
	  var regGlobals = cwicState.regGlobals;
	
	  log(true, '_triggerTelephonyDevicesChange called with following data: ', content);
	  log(true, '_triggerTelephonyDevicesChange: regGlobals: ', regGlobals);
	
	  if (content.devices && content.devices.length > 0 && regGlobals.registeringPhone) {
	    // TelephonyDevicesChange event is triggered for each device in the list. We don't know when the last
	    // device is discovered. We "debounced" TelephonyDevicesChange event, so this callback will
	    // be called only after timeout of 20ms expires between successive events.
	    // So, "telephonyDevicesChange" event is used only for detecting when the list of available devices is fully populated in JCF,
	    // and after that "getAvailableDevices" function is used to get that list
	    regGlobals.telephonyDevicesSet = true;
	
	    // We now want to call devices available cb for every change event during registration.
	    log(true, 'Proceeding with device selection after waiting for TelephonyDevicesChange event.');
	    proceedWithDeviceSelection();
	  }
	}
	
	module.exports = debounce(telephonyDevicesChangeHandler, 20);

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var triggerConversationEvent = __webpack_require__(23);
	
	function videoResolutionChangeHandler($this, content){
	  log(true, 'video resolution change detected for call ' + content.callId +
	    '. Height: ' + content.height +
	    ', width: ' + content.width, content);
	  // trigger a conversation event with a 'videoResolution' property
	  triggerConversationEvent($this, {
	    callId: content.callId,
	    videoResolution: {
	      width: content.width,
	      height: content.height
	    }
	  }, 'render');
	}
	
	module.exports = videoResolutionChangeHandler;


/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var dock = __webpack_require__(64);
	var dockGlobals = __webpack_require__(12);
	
	function dockExternalWindowNeededHandler($this, content) {
	  var dockArgs = {window: dockGlobals.frame, element: dockGlobals.element};
	  var windowType = content.windowType;
	
	  log(true, 'Sending new "dock" message with parameters: ', dockArgs);
	
	  dock(dockArgs, windowType);
	}
	
	module.exports = dockExternalWindowNeededHandler;

/***/ },
/* 64 */
/***/ function(module, exports, __webpack_require__) {

	var dockGlobals = __webpack_require__(12);
	var about = __webpack_require__(15);
	var log = __webpack_require__(3);
	var triggerError = __webpack_require__(10);
	var errorMap = __webpack_require__(5).errorMap;
	
	/**
	 * Docks an external video window created by {@link $.fn.cwic-showPreviewInExternalWindow} or {@link $.fn.cwic-showCallInExternalWindow}.<br>
	 * Example use:
	 * @example
	 * // simple case where the target video container is on the same HTML page as cwic.js file
	 * $('#videocontainer').cwic('dock');
	 * // or an extended form where the window object along with target element could be specified.
	 * // Window object could be from an iFrame or a popup with the same origin as "parent" page
	 * $().cwic('dock', {window: windowElementOfSomeHtmlDocument, element: targetElementForVideoOverlay});
	 * @param {Object} [args] Information about target element for video overlay
	 * @param {DOMWindow} args.window Window object in which the target element is located
	 * @param {DOMElement} args.element Target element for video overlay
	 * @memberof $.fn.cwic
	 * @since 3.1.2
	 */
	function dock(args, windowType) {
	  var $this = this;
	  var frame = args ? args.window : window;
	  var element = args ? args.element : $this[0];
	  windowType = windowType || 'remote';
	
	  dockGlobals._about = about();
	
	  if (dockGlobals.hasDockingCapabilities() && (element instanceof frame.HTMLElement)) {
	    dockGlobals.isDocked = true;
	
	    log(true, 'dock', arguments);
	
	    dockGlobals.resetPosition();
	    dockGlobals.frame = frame;
	    dockGlobals.element = element;
	
	    var position = dockGlobals.updateOffsets(dockGlobals.element.getBoundingClientRect());
	    dockGlobals.sendMessageToAddOn("dockExternalWindow", position, windowType);
	    dockGlobals.frame.requestAnimationFrame(dockGlobals.move);
	
	  } else if (!dockGlobals.hasDockingCapabilities()) {
	    triggerError($this, errorMap.DockingCapabilitiesNotAvailable);
	
	  } else if (!(element instanceof frame.HTMLElement)) {
	    triggerError($this, errorMap.DockArgumentNotHTMLElement);
	
	  }
	
	  return $this;
	}
	
	module.exports = dock;

/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var ExtensionModule = __webpack_require__(66);
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule = __webpack_require__(72);
	
	// Old API Modules
	var SendClientMessage = __webpack_require__(7);
	
	var cwic = {
	    Plugin : ExtensionModule.Plugin,
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	    MessageSender : MessageSenderModule.MessageSender
	};
	
	function CompatibilityPlugin(cwicID)
	{
	    this.type = "CompabilityPlugin";
	    this.cwicID = cwicID;
	}
	
	CompatibilityPlugin.prototype = Object.create(cwic.Plugin.prototype);
	CompatibilityPlugin.prototype.constructor = CompatibilityPlugin;
	
	CompatibilityPlugin.prototype.initialize = function()
	{
	    cwic.MessageSender.plugin = this;
	    cwic.MessageSender.pluginType = "CompatibilityPlugin";
	};
	
	CompatibilityPlugin.prototype.sendMessage = function(message)
	{
	    SendClientMessage(message);
	};
	
	CompatibilityPlugin.prototype.onMessageReceived = function(message)
	{
	    cwic.MessageReceiver.onMessageReceived(message);
	};
	
	module.exports.CompatibilityPlugin = new CompatibilityPlugin();
	


/***/ },
/* 66 */
/***/ function(module, exports) {

	function Plugin()
	{
	}
	
	Plugin.prototype.version = null;
	Plugin.prototype.sendMessage = null;
	Plugin.prototype.initialize = null;
	Plugin.prototype.type = "Unknown";
	Plugin.prototype.onInitializeError = null;
	
	
	module.exports.Plugin = Plugin;


/***/ },
/* 67 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageResponseHandlerModule = __webpack_require__(68);
	var LoggerModule                 = __webpack_require__(69);
	var SystemErrorsModule           = __webpack_require__(71);
	
	var cwic = {
	    MessageResponseHandler : MessageResponseHandlerModule.MessageResponseHandler,
	    Logger                 : LoggerModule.Logger,
	    SystemErrors           : SystemErrorsModule.SystemErrors
	};
	
	
	function MessageReceiver()
	{
	    var m_MessageHandlers = {};
	
	    this.addMessageHandler = function(messageType, handlerFunction)
	    {
	        m_MessageHandlers[messageType] = handlerFunction;
	    };
	
	    this.removeMessageHandler = function(messageType)
	    {
	        delete m_MessageHandlers[messageType];
	    };
	
	    this.onMessageReceived = function(message)
	    {
	        if (message.ciscoSDKServerMessage)
	        {
	            this.onServerMessageReceived(message.ciscoSDKServerMessage);
	        }
	        else if(message.ciscoChannelServerMessage)
	        {
	            this.onChannelMessage(message.ciscoChannelServerMessage);
	        }
	        else
	        {
	            cwic.Logger.error('Unknown message from plugin: ', message);
	        }
	    };
	
	    this.onServerMessageReceived = function(serverMessage)
	    {
	        var content   = serverMessage.content;
	        var error     = serverMessage.error;
	        var messageId = serverMessage.replyToMessageId;
	        var name      = serverMessage.name;
	
	        cwic.Logger.debug('Received Message: ' + name, serverMessage);
	
	        if(error)
	        {
	            cwic.Logger.debug('Error response for Message: ' + name, serverMessage);
	
	            if(cwic.MessageResponseHandler.errorHandlerExists(messageId))
	            {
	                var errorHandler = cwic.MessageResponseHandler.getErrorHandler(messageId);
	                errorHandler(cwic.SystemErrors[error]);
	                cwic.MessageResponseHandler.removeErrorHandler(messageId);
	            }
	
	            return;
	        }
	
	        if(cwic.MessageResponseHandler.successHandlerExist(messageId))
	        {
	            cwic.Logger.debug('Success response for Message: ' + name, serverMessage);
	
	            var successHandler = cwic.MessageResponseHandler.getSuccessHandler(messageId);
	            successHandler(content);
	        }
	
	        if(m_MessageHandlers[name])
	        {
	            var messageHandler = m_MessageHandlers[name];
	            messageHandler(content);
	        }
	    };
	
	    this.onChannelMessage = function(channelMessage)
	    {
	        if (channelMessage.name === 'ChannelDisconnect' || channelMessage.name === 'HostDisconnect')
	        {
	            cwic.Logger.error("Connection with add-on has been lost", "");
	            var messageHandler = m_MessageHandlers['addonConnectionLost'];
	            if(messageHandler)
	            {
	                messageHandler();
	            }
	        }
	        else
	        {
	            cwic.Logger.debug("Unknown channel message: " + channelMessage.name);
	        }
	    };
	}
	
	module.exports.MessageReceiver = new MessageReceiver();

/***/ },
/* 68 */
/***/ function(module, exports) {

	function MessageResponseHandler()
	{
	    var m_ErrorMessageHandlers   = {};
	    var m_SuccessMessageHandlers = {};
	
	    this.addErrorHandler = function(messageID, handler)
	    {
	        m_ErrorMessageHandlers[messageID] = handler;
	    };
	
	    this.addSuccessHandler = function(messageID, handler)
	    {
	        m_SuccessMessageHandlers[messageID] = handler;
	    };
	
	    this.removeErrorHandler = function(messageID)
	    {
	        delete m_ErrorMessageHandlers[messageID];
	    };
	
	    this.removeSuccessHandler = function(messageID)
	    {
	        delete m_SuccessMessageHandlers[messageID];
	    };
	
	    this.errorHandlerExists = function(messageID)
	    {
	        return m_ErrorMessageHandlers[messageID] ? true : false;
	    };
	
	    this.successHandlerExist = function(messageID)
	    {
	        return m_SuccessMessageHandlers[messageID] ? true : false;
	    };
	
	    this.getErrorHandler = function(messageID)
	    {
	        return m_ErrorMessageHandlers[messageID];
	    };
	
	    this.getSuccessHandler = function (messageID)
	    {
	        return m_SuccessMessageHandlers[messageID];
	    }
	}
	
	
	module.exports.MessageResponseHandler = new MessageResponseHandler();


/***/ },
/* 69 */
/***/ function(module, exports, __webpack_require__) {

	var UtilitiesModule = __webpack_require__(70);
	
	var cwic = {
	    Utilities : UtilitiesModule.Utilities
	};
	
	var LogLevels =
	{
	    debug   : 0,
	    info    : 1,
	    warning : 2,
	    error   : 3
	};
	
	function Logger()
	{
	}
	
	Logger.prototype.logLevel = 0;
	
	Logger.prototype.debug = function(message, context)
	{
	    context = context ? context : "";
	    if(this.logLevel <= LogLevels.debug)
	    {
	        var currentTime = new Date();
	        var timeStamp   =
	            ('0' + currentTime.getHours()).slice(-2) + ':' +
	            ('0' + currentTime.getMinutes()).slice(-2) + ':' +
	            ('0' + currentTime.getSeconds()).slice(-2) + '.' +
	            ('00' + currentTime.getMilliseconds()).slice(-3);
	
	        if(cwic.Utilities.getBrowserType() === "InternetExplorer")
	        {
	            console.log('[cwic][DEBUG][' + timeStamp + '] ' +  message, context);
	        }
	        else
	        {
	            console.log('%c[cwic][DEBUG][' + timeStamp + '] ' +  message, 'color: green', context);
	        }
	    }
	};
	
	Logger.prototype.info = function(message, context)
	{
	    context = context ? context : "";
	    if(this.logLevel <= LogLevels.info)
	    {
	        var currentTime = new Date();
	        var timeStamp   =
	            ('0' + currentTime.getHours()).slice(-2) + ':' +
	            ('0' + currentTime.getMinutes()).slice(-2) + ':' +
	            ('0' + currentTime.getSeconds()).slice(-2) + '.' +
	            ('00' + currentTime.getMilliseconds()).slice(-3);
	
	        if(cwic.Utilities.getBrowserType() === "InternetExplorer")
	        {
	            console.log('[cwic][INFO ][' + timeStamp + '] ' + message, context);
	        }
	        else
	        {
	            console.log('%c[cwic][INFO ][' + timeStamp + '] ' + message, 'color: black', context);
	        }
	    }
	};
	
	
	Logger.prototype.warning = function(message, context)
	{
	    context = context ? context : "";
	    if(this.logLevel <= LogLevels.warning)
	    {
	        var currentTime = new Date();
	        var timeStamp   =
	            ('0' + currentTime.getHours()).slice(-2) + ':' +
	            ('0' + currentTime.getMinutes()).slice(-2) + ':' +
	            ('0' + currentTime.getSeconds()).slice(-2) + '.' +
	            ('00' + currentTime.getMilliseconds()).slice(-3);
	
	        if(cwic.Utilities.getBrowserType() === "InternetExplorer")
	        {
	            console.log('[cwic][WARNI][' + timeStamp + '] ' +  message, context);
	        }
	        else
	        {
	            console.log('%c[cwic][WARNI][' + timeStamp + '] ' + message, 'color: #FF8000', context);
	        }
	    }
	};
	
	
	Logger.prototype.error = function(message, context)
	{
	    context = context ? context : "";
	    if(this.logLevel <= LogLevels.error)
	    {
	        var currentTime = new Date();
	        var timeStamp   =
	            ('0' + currentTime.getHours()).slice(-2) + ':' +
	            ('0' + currentTime.getMinutes()).slice(-2) + ':' +
	            ('0' + currentTime.getSeconds()).slice(-2) + '.' +
	            ('00' + currentTime.getMilliseconds()).slice(-3);
	
	        if(cwic.Utilities.getBrowserType() === "InternetExplorer")
	        {
	            console.log('[cwic][ERROR][' + timeStamp + '] ' +  message, context);
	        }
	        else
	        {
	            console.log('%c[cwic][ERROR][' + timeStamp + '] ' + message, 'color: red', context);
	        }
	    }
	};
	
	module.exports.Logger = new Logger();


/***/ },
/* 70 */
/***/ function(module, exports) {

	
	function Utilities()
	{
	}
	
	Utilities.prototype.getBrowserType = function()
	{
	    var type = 'Unknown';
	
	    if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 )
	    {
	        type = 'Opera';
	    }
	    else if(navigator.userAgent.indexOf("Chrome") != -1 )
	    {
	        type = 'Chrome';
	    }
	    else if(navigator.userAgent.indexOf("Safari") != -1)
	    {
	        type = 'Safari';
	    }
	    else if(navigator.userAgent.indexOf("Firefox") != -1 )
	    {
	        type = 'Firefox';
	    }
	    else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
	    {
	        type = 'InternetExplorer';
	    }
	
	    return type;
	};
	
	Utilities.prototype.getOSType = function()
	{
	    var OSName = "Unknown";
	    if (window.navigator.userAgent.indexOf("Windows NT 10.0")!= -1) OSName="Windows 10";
	    if (window.navigator.userAgent.indexOf("Windows NT 6.2") != -1) OSName="Windows 8";
	    if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1) OSName="Windows 7";
	    if (window.navigator.userAgent.indexOf("Windows NT 6.0") != -1) OSName="Windows Vista";
	    if (window.navigator.userAgent.indexOf("Windows NT 5.1") != -1) OSName="Windows XP";
	    if (window.navigator.userAgent.indexOf("Windows NT 5.0") != -1) OSName="Windows 2000";
	    if (window.navigator.userAgent.indexOf("Mac")!=-1) OSName="Mac";
	
	    return OSName;
	};
	
	module.exports.Utilities = new Utilities();


/***/ },
/* 71 */
/***/ function(module, exports) {

	/**
	 * @enum SystemError {Enum}
	 * @description
	 * These errors are passed in API error handler functions.
	 *
	 * @since 11.7.0
	 */
	var SystemError = {
	    /**
	     * Error occurred in add-on.
	     * @type {String}
	     */
	    NativeError : "Error",
	
	    /**
	     * System not started or fully operational.
	     * @type {String}
	     */
	    OperationFailed : "OperationFailed",
	
	    /**
	     * User has not authorized add-on through Access Control Dialogue.
	     * @type {String}
	     */
	    UserNotAuthorized : "UserNotAuthorized",
	
	    /**
	     * Invalid arguments passed.
	     * @type {String}
	     */
	    InvalidArguments : "InvalidArguments",
	
	    /**
	     * System has no capabilities to execute API call.
	     * @type {String}
	     */
	    CapabilityMissing : "CapabilityMissing",
	
	    /**
	     * More then one instance of Add-on is running.
	     * @type {String}
	     */
	    MoreThenOneInstanceRunning : "MoreThenOneInstanceRunning"
	};
	
	var SystemErrorAliasMap = {
	    eSyntaxError       : "NativeError",
	    eOperationFailed   : "OperationFailed",
	    eNotUserAuthorized : "UserNotAuthorized",
	    eInvalidArgument   : "InvalidArguments",
	    eCapabilityMissing : "CapabilityMissing",
	    eLoggedInLock      : "MoreThenOneInstanceRunning"
	};
	
	module.exports.SystemErrors = SystemErrorAliasMap;

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

	var generateUniqueId = __webpack_require__(73);
	
	// System Modules
	var MessageResponseHandlerModule = __webpack_require__(68);
	var LogModule                    = __webpack_require__(69);
	
	var cwic = {
	    MessageResponseHandler : MessageResponseHandlerModule.MessageResponseHandler,
	    Logger                 : LogModule.Logger
	};
	
	function SystemException(message)
	{
	    this.message = message;
	}
	
	SystemException.prototype = Object.create(Error.prototype);
	SystemException.prototype.name = "cwic.SystemException";
	SystemException.prototype.constructor = SystemException;
	
	
	function MessageSender()
	{
	    var m_EventHandlers = [];
	}
	
	MessageSender.prototype.sendMessage = function(messageType, messageData, errorHandler, successHandler)
	{
	    if(this.plugin == null)
	    {
	        throw new SystemException();
	    }
	
	    var messageID = generateUniqueId();
	
	    if(typeof errorHandler === 'function')
	    {
	        cwic.MessageResponseHandler.addErrorHandler(messageID, errorHandler);
	    }
	
	    if(typeof successHandler === 'function')
	    {
	        cwic.MessageResponseHandler.addSuccessHandler(messageID, successHandler);
	    }
	
	    var message = {
	        ciscoSDKClientMessage: {
	            'messageId': messageID,
	            'name': messageType,
	            'content': messageData || {}
	        }
	    };

	    this.plugin.sendMessage(message);
	};
	
	MessageSender.prototype.addEventHandler = function(eventName, eventHandler)
	{
	    this.eventHandlers[eventName] = eventHandler;
	};
	
	MessageSender.prototype.plugin = null;
	MessageSender.prototype.eventHandlers = [];
	
	module.exports.MessageSender = new MessageSender();

/***/ },
/* 73 */
/***/ function(module, exports) {

	// Good-enough unique ID generation:
	// used for clienId (generated only once per cwic instance) and message ids (generated for each message)
	// Just needs to be unique for this channel across restarts of this client,
	// just in case there's an old server reply in the pipeline.
	function generateUniqueId() {
	  return (new Date()).valueOf().toString() + Math.floor((Math.random() * 10000) + 1).toString();
	}
	
	module.exports = generateUniqueId;

/***/ },
/* 74 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var cwicState = __webpack_require__(9);
	
	function rebootIfBroken(rebootCb) {
	
	  var pluginExists = doesPluginExist();
	
	  if (!pluginExists) {
	    log('Plugin does not exist. Restarting....');
	
	    rebootCb();
	  }
	
	  return pluginExists;
	}
	
	// Helper function to check if plug-in is still available.
	// Related to DE3975. The CK advanced editor causes the overflow CSS attribute to change, which in turn
	// removes and replaces the plug-in during the reflow losing all state.
	
	var doesPluginExist = function () {
	  var ret = true;
	  var plugin = cwicState.plugin;
	
	  if (!plugin || !plugin.api) {
	    log(true, '_doesPluginExist failed basic existence check');
	
	    ret = false;
	
	  } else if (typeof plugin.api.sendRequest === 'undefined' && typeof plugin.api.postMessage === 'undefined') {
	    log(true, '_doesPluginExist failed sendRequest/postMessage method check');
	    ret = false;
	  }
	
	  return ret;
	};
	
	module.exports = rebootIfBroken;

/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var triggerProviderEvent = __webpack_require__(30);
	var credentialsRequiredHandler = __webpack_require__(33);
	
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	
	var triggerError = __webpack_require__(10);
	
	var cwicState = __webpack_require__(9);
	var getAuthenticatedCb = __webpack_require__(76);
	
	var validators = __webpack_require__(34);
	var utils = __webpack_require__(13);
	
	var $ = __webpack_require__(6);
	
	
	/**
	 * Register phone to CUCM (SIP register). Used for manual type of registration, in which connection parameters are manually configured (tftp, ccmcip, cti).
	 * @memberof $.fn.cwic
	 * @param args A map with:
	 * @param {String} args.user The CUCM end user name (required)
	 * @param {String|Object} args.password String - clear password. Object - {encrypted: encoded password, cipher:'cucm'}
	 * @param {String|Object|Array} args.cucm The list of CUCM(s) to attempt to register with (required).<br>
	 * If String, it will be used as a TFTP, CCMCIP and CTI address.<br>
	 * If Array, a list of String or Object as described above.
	 * Three is the maximum number of addresses per server (TFTP, CCMCIP, CTI).
	 * @param {String[]} [args.cucm.tftp] TFTP addresses. Maximum three values.
	 * @param {String[]} [args.cucm.ccmcip] CCMCIP addresses (will use tftp values if not present). Maximum three values.
	 * @param {String[]} [args.cucm.cti]  Since: 2.1.1 <br>
	 * CTI addresses (will use tftp values if not present). Maximum three values.
	 * @param {String} [args.mode]  Register the phone in this mode. Available modes are "SoftPhone" or "DeskPhone". Default of intelligent guess is applied after a device is selected.<br>
	 * @param {Function} [args.devicesAvailable(devices, phoneMode, callback)] Callback called after successful authentication. Might be called multiple times.
	 * If this callback is not specified, cwic applies the default device selection algorithm.  An array of {@link device} objects is passed so the application can select the device.<br>
	 * To complete the device registration, call the callback function that is passed in to devicesAvailable as the third parameter.
	 * The callback function is defined in the API, but it must be called by the function that is specified as the devicesAvailable parameter.
	 * @param {Function} [args.error(err)] Callback called if the registration fails.  {@link $.fn.cwic-errorMapEntry} is passed as parameter.
	 * @param {Boolean} [args.forceRegistration] Specifies whether to forcibly unregister other softphone instances with CUCM. Default is false. See GracefulRegistration doc for more info.
	 * @param {Function} [args.success(registration)] Callback called when registration succeeds. A {@link registration} object is passed to the callback:
	 * registerPhone examples <br>
	 * @example
	 * // *************************************
	 * // register with lab CUCM in default mode (SoftPhone)
	 * jQuery('#phone').cwic('registerPhone', {
	    *     user: 'fbar',
	    *     password: 'secret', // clear password
	    *     cucm: '1.2.3.4',
	    *     success: function (registration) {
	    *         console.log('registered in mode ' + registration.mode);
	    *         console.log('registered with device ' + registration.device.name);
	    *     }
	    * });
	 * @example
	 * // *************************************
	 * // register with Alpha CUCM in DeskPhone mode with encrypted password
	 * jQuery('#phone').cwic('registerPhone', {
	    *     user: 'fbar',
	    *     password: {
	    *         encoded: 'GJH$&*"@$%$^BLKJ==',
	    *         cipher: 'cucm'
	    *     },
	    *     mode: 'DeskPhone',
	    *     cucm: '1.2.3.4',
	    *     success: function (registration) {
	    *         console.log('registered in mode ' + registration.mode);
	    *         console.log('registered with device ' + registration.device.name);
	    *     }
	    * );
	 * @example
	 * // *************************************
	 * // register with Alpha CUCM in SoftPhone mode, select ECP{user} device
	 * jQuery('#phone').cwic('registerPhone', {
	    *     user: 'fbar',
	    *     password: {
	    *         encoded: 'GJH$&*"@$%$^BLKJ==',
	    *         cipher: 'cucm'
	    *     },
	    *     mode: 'SoftPhone',
	    *     cucm: {
	    *         ccmcip: ['1.2.3.4'],
	    *         tftp: ['1.2.3.5']
	    *     },
	    *     devicesAvailable: function (devices, phoneMode, callback) {
	    *         for (var i = 0; i &lt; devices.length; i++) {
	    *             var device = devices[i];
	    *             if (device.name.match(/^ECP/i)) {
	    *                 callback(phoneMode, device);
	    *             } // starts with 'ECP'
	    *         }
	    *         return; // stop registration if no ECP{user} device found
	    *     },
	    *     success: function (registration) {
	    *         console.log('registered in mode ' + registration.mode);
	    *         console.log('registered with device ' + registration.device.name);
	    *     },
	    *     error: function (err) {
	    *         console.log('cannot register phone: ' + err.message);
	    *     }
	    * );
	    */
	function registerPhone(args) {

	  var $this = this,
	    devicesAvailableCb,
	    tftp = [],
	    ccmcip = [],
	    cti = [],
	    result,
	    props = {},
	    passphrase;
	
	  // function definitions (callbacks)
	  //
	  function encryptCb(err, res) {
	    if (err) {
	      log(false, 'Password encryption failed', err);
	      return;
	    }
	
	    if (res) {
	
	      // update passphrase with encrypted result
	      passphrase = {
	        cipher: 'cucm',
	        encrypted: res
	      };
	    }
	    // continue with get and set for various props...
	    sendClientRequest('setProperty', {
	      'TftpAddressList': tftp
	    }, createCb('TftpAddressList'));
	
	    sendClientRequest('setProperty', {
	      'CtiAddressList': cti
	    }, createCb('CtiAddressList'));
	
	    sendClientRequest('setProperty', {
	      'CcmcipAddressList': ccmcip
	    }, createCb('CcmcipAddressList'));
	
	    sendClientRequest('getProperty', {property: 'connectionStatus'}, createCb('connectionStatus'));
	  }
	
	  // first need to set/get a bunch of props before moving on to authenticateAndConnect
	  function createCb(name) {
	    return function (res) {
	      log(true, name + ' callback received');
	
	      props[name] = res[name];
	
	      if (props.hasOwnProperty('TftpAddressList') &&
	        props.hasOwnProperty('CtiAddressList') &&
	        props.hasOwnProperty('CcmcipAddressList') &&
	        props.hasOwnProperty('connectionStatus')
	      ) {
	        // all callbacks returned
	        log(true, 'All prop callbacks received.  Continuing toward authenticateAndConnect', props);
	
	        cwicState.setPropsToPlugin({connectionStatus: props.connectionStatus})
	
	        // now move on to authenticateAndConnect
	        authenticateAndConnect();
	      }
	    };
	  }
	
	  // authenticateAndConnect gets called after all the property set and get callbacks return
	  function authenticateAndConnect() {
	    var currState = cwicState.plugin.connectionStatus;
	    var regGlobals = cwicState.regGlobals;

	    // is the plugin already ready ?
	    if (currState === 'eReady') { // state = connection status, not a call state!
	      triggerProviderEvent($this, currState);
	    }
	
	    regGlobals.passphrase = passphrase;
	
	    if (currState !== 'eReady') { // state = connection status, not a call state!
	      regGlobals.currState = currState;
	
	      // authenticatedCallback is called affter receiving authenticationresult event with positive outcome
	      regGlobals.authenticatedCallback = getAuthenticatedCb(devicesAvailableCb, $this);
	
	      // in this step, passphrase must be in encrypted format
	      if (!passphrase.encrypted || (passphrase.cipher !== 'cucm')) {
	        return triggerError($this, regGlobals.errorCb, errorMap.InvalidArguments, 'authenticateAndConnect: invalid passphrase (type ' + typeof passphrase + ')', {
	          registration: cwicState.registration
	        });
	      }
	
	      // plugin is saving credentials, so, after first login attempt with wrong credentials, credentialsRequired is triggered with "wrongCredentials" error.
	      // When next time lifecycle is started, the same message will be again attached to credentialsRequired event, which will cause _triggerError call...
	      // So, instead of starting lifecycle every time registerPhone is called, only encrypt new credentials and submit.
	      if (regGlobals.errorState === 'credentialsRequired') {
	        credentialsRequiredHandler($this, {
	          errors: [],
	          error: '',
	          authenticatorId: regGlobals.lastAuthenticatorId
	        });
	
	      } else {
	        sendClientRequest('startSignIn', {
	            manualSettings: true
	          }, $.noop,
	          function errorCb(error) {
	            triggerError($this, regGlobals.errorCb, errors.getError(error), error, {
	              registration: cwicState.registration
	            });
	          }
	        );
	      }
	    }
	  }
	
	  ////////////////////////////////
	  // registerPhone main logic
	  ////////////////////////////////
	  logArgsWithMaskedPassphrase(args);
	
	  // M for manual
	  setRegGlobalsM(args);
	
	  try {
	    result = parseAndCheckArgs(args, $this);
	    devicesAvailableCb = result.devicesAvailableCb;
	    tftp = result.tftp;
	    ccmcip = result.ccmcip;
	    cti = result.cti;
	    passphrase = result.passphrase;
	  } catch (e) {
	    // parseAndCheckArgs triggers error callback
	    return $this;
	  }
	
	  if (typeof passphrase === 'string') {
	    // clear passphrase, encrypt it
	    utils.encrypt(passphrase, encryptCb);
	  } else {
	    // passphrase valid and already encrypted
	    encryptCb();
	  }
	
	  return $this;
	}
	
	// for registerPhone
	function logArgsWithMaskedPassphrase(args) {
	  var argsForLog = $.extend({}, args);
	
	  if (argsForLog.passphrase) {
	    argsForLog.passphrase = '*****';
	  }
	
	  if (argsForLog.password) {
	    argsForLog.password = '*****';
	  }
	
	  log(true, 'manualSignIn', argsForLog);
	}
	
	// for registerPhone
	// M for Manual
	function setRegGlobalsM(args) {
	  var regGlobals = cwicState.regGlobals;
	
	  // flag to indicate cwic is in the process of registering a phone in manual mode
	  regGlobals.registeringPhone = true;
	  regGlobals.manual = true;
	
	  // reset global registration object
	  cwicState.registration = {
	    user: args.user,
	    mode: args.mode || 'SoftPhone',
	    devices: {},
	    forceRegistration: args.forceRegistration || false
	  };
	
	  regGlobals.successCb = $.isFunction(args.success) ? args.success : null;
	  regGlobals.errorCb = $.isFunction(args.error) ? args.error : null;
	  regGlobals.CUCM = args.cucm;
	  regGlobals.user = args.user;
	
	  log(true, 'setRegGlobalsM: regGlobals set: ', regGlobals);
	  log(true, 'setRegGlobalsM: registration set: ', cwicState.registration);
	}
	
	// for registerPhone
	function parseAndCheckArgs(args, $this) {
	  var passphraseValidator = validators.get('passphrase');
	  var result = {
	    devicesAvailableCb: null,
	    tftp: [],
	    ccmcip: [],
	    cti: [],
	    passphrase: ''
	  };
	  var regGlobals = cwicState.regGlobals;
	  var registration = cwicState.registration;
	
	  result.devicesAvailableCb = $.isFunction(args.devicesAvailable) ? args.devicesAvailable : null;
	
	  // check plugin state also!
	  if (!cwicState.plugin) {
	    triggerError($this, regGlobals.errorCb, errorMap.PluginNotAvailable, 'Plug-in is not available or has not been initialized', {
	      registration: registration
	    });
	
	    throw new Error('Break manual login');
	  }
	
	  // parse CUCM argument into tftp, ccmcip and cti arrays (list of String addresses)
	  // from the 11.0 release and on, tftp, ccmcip and cti are limited to 3 addresses only
	
	  // args.cucm can be a String, an Object or an Array of both
	  $.each($.makeArray(args.cucm), function (i, elem) {
	    if (typeof elem === 'string') {
	
	      // cucm string can be 'lab call manager 1.2.3.4'
	      var a = elem.split(' ').pop();
	
	      result.tftp.push(a);
	      result.ccmcip.push(a);
	      result.cti.push(a);
	
	    } else if (typeof elem === 'object') {
	      var tftpElem = []; // the tftp array of the current elem
	      var hasOneProperty = false; // just to log a warning
	
	      if ($.isArray(elem.tftp)) {
	        result.tftp = result.tftp.concat(elem.tftp);
	        tftpElem = elem.tftp;
	        hasOneProperty = true;
	      }
	
	      if ($.isArray(elem.ccmcip)) {
	        result.ccmcip = result.ccmcip.concat(elem.ccmcip);
	        hasOneProperty = true;
	      } else {
	        // ccmcip defaults to tftp (backward compatibility)
	        result.ccmcip = result.ccmcip.concat(tftpElem);
	      }
	
	      if ($.isArray(elem.cti)) {
	        result.cti = result.cti.concat(elem.cti);
	        hasOneProperty = true;
	      } else {
	        // cti defaults to tftp (backward compatibility)
	        result.cti = result.cti.concat(tftpElem);
	      }
	
	      if (!hasOneProperty) {
	        log('registerPhone: no ccmcip/tftp/cti properties for cucm element');
	        log(true, elem);
	      }
	    } else {
	      log('registerPhone: ignoring cucm argument of type ' + typeof elem);
	    }
	  });
	
	  log('registerPhone: ' + result.tftp.length + ' cucm TFTP address(es)');
	  log(true, result.tftp);
	  log('registerPhone: ' + result.ccmcip.length + ' cucm CCMCIP address(es)');
	  log(true, result.ccmcip);
	  log('registerPhone: ' + result.cti.length + ' cucm CTI address(es)');
	  log(true, result.cti);
	
	  if (result.tftp.length > 3 || result.ccmcip.length > 3 || result.cti.length > 3) {
	    log('registerPhone: Server address(es) are limited to 3 values. Only first 3 values will be kept.');
	    result.tftp = result.tftp.splice(0, 3);
	    result.ccmcip = result.ccmcip.splice(0, 3);
	    result.cti = result.cti.splice(0, 3);
	  }
	
	  log('registerPhone of user=' + registration.user + ' in mode="' + registration.mode + '"');
	
	  if (!registration.user) {
	    triggerError($this, regGlobals.errorCb, errorMap.InvalidArguments, 'Missing user name', {
	      registration: registration
	    });
	
	    throw new Error('Break manual login');
	  }
	
	  if (!$.isArray(result.tftp) || result.tftp.length < 1) {
	    triggerError($this, regGlobals.errorCb, errorMap.InvalidTFTPServer, 'Missing CUCM address', {
	      registration: registration
	    });
	
	    throw new Error('Break manual login');
	  }
	
	  if (!registration.mode.match(/^(SoftPhone|DeskPhone)$/)) {
	    triggerError($this, regGlobals.errorCb, errorMap.InvalidArguments, 'Invalid phone mode "' + registration.mode + '"', {
	      registration: registration
	    });
	
	    throw new Error('Break manual login');
	  }
	
	  // validate password
	  result.passphrase = args.passphrase || args.password;
	
	  if (typeof result.passphrase === 'string') {
	    if (passphraseValidator.isNotValid(result.passphrase)) {
	      triggerError($this, regGlobals.errorCb, errorMap.InvalidArguments, 'invalid passphrase', {
	        registration: registration
	      });
	      throw new Error('Break manual login');
	    }
	
	  } else if (typeof result.passphrase !== 'object' || (result.passphrase.cipher !== 'cucm')) {
	    triggerError($this, regGlobals.errorCb, errorMap.InvalidArguments, 'invalid passphrase (type ' + typeof result.passphrase + ')', {
	      registration: registration
	    });
	
	    throw new Error('Break manual login');
	
	  } else {
	    if (passphraseValidator.isNotValid(result.passphrase.encrypted)) {
	      triggerError($this, regGlobals.errorCb, errorMap.InvalidArguments, 'invalid passphrase', {
	        registration: registration
	      });
	
	      throw new Error('Break manual login');
	    }
	  }
	
	  return result;
	}
	
	module.exports = registerPhone;

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var utils = __webpack_require__(13);
	var sendClientRequest = __webpack_require__(7);
	
	var errors = __webpack_require__(5);
	
	var triggerError = __webpack_require__(10);
	
	var cwicState = __webpack_require__(9);
	
	var $ = __webpack_require__(6);
	
	// authenticatedCallback is called after receiving authenticationresult event with positive outcome
	// required both for manual and sd based sign in
	function getAuthenticatedCb(devicesAvailableCb, $this) {
	
	  function sendConnectMsg(deviceName, lineDN) {
	    // cannot send connect msg before MM has started
	    var canConnect = cwicState.isMultimediaStarted;
	
	    if (!canConnect) {
	      log(true, 'cannot send "connect" now ... waiting for multimedia start!');
	      cwicState.pendingConnect = utils.wrap(sendConnectMsg, deviceName, lineDN);
	
	      return;
	    }
	
	    var regGlobals = cwicState.regGlobals;
	    var registration = cwicState.registration;
	
	    // because of stream like behavior of available devices list, availableDevicesCB could be triggered multiple times,
	    // so we need to guard against calling connect multiple times  for the same device
	    // on failure, regGlobals.lastConnectedDevice will be reset, so it should not block connecting to device again after failure
	    if (!deviceName || deviceName === regGlobals.lastConnectedDevice) {
	      log(true, 'authenticatedCb: device name empty or already sent "connect" message for this device, returning...', deviceName);
	      return;
	    }
	
	    lineDN = lineDN || '';
	    regGlobals.lastConnectedDevice = deviceName;
	
	    sendClientRequest('connect',
	      { phoneMode: registration.mode, deviceName: deviceName, lineDN: lineDN, forceRegistration: registration.forceRegistration }, $.noop,
	      function errorCb(error) {
	        triggerError($this, regGlobals.errorCb, errors.getError(error), error, {registration: registration});
	      }
	    );
	  }
	
	  return function (_devices) {
	    var defaultDevice;
	    var i;
	    var registration = cwicState.registration;
	
	    log(true, 'Entering authenticatedCallback');
	
	    if (_devices.length === 0) {
	      log(true, 'authenticatedCallback: devices list of zero length received');
	    }
	
	    if (devicesAvailableCb) {
	      try {
	        devicesAvailableCb(_devices, registration.mode, function (phoneMode, deviceName, lineDN) {
	
	          // in softphone mode, lineDN is not allowed
	          if (phoneMode === 'SoftPhone' || !lineDN) {
	            lineDN = '';
	          }
	
	          log(true, 'connecting to user selected device: ', deviceName);
	
	          sendConnectMsg(deviceName, lineDN);
	        });
	
	      } catch (devicesAvailableException) {
	        log('Exception occurred in application devicesAvailable callback', devicesAvailableException);
	
	        if (typeof console !== 'undefined' && console.trace) {
	          console.trace();
	        }
	      }
	
	    } else {
	      defaultDevice = {name: ''};
	      //...use the first available one
	      for (i = 0; i < _devices.length; i++) {
	        if (registration.mode === 'SoftPhone' && _devices[i].isSoftPhone) {
	          // Note device objects retrieved from ECC will have device model description in device.modelDescription
	          // This differs from csf node.js module 'phoneconfig' implementation which puts it in device.model - this is also removed from 11.0
	          // device.model is removed from 11.0 release
	          if (_devices[i].modelDescription.match(/^\s*Cisco\s+Unified\s+Client\s+Services\s+Framework\s*$/i)) { // On CUCM Product Type: Cisco Unified Client Services Framework
	            defaultDevice = _devices[i];
	            break;
	          }
	        }
	
	        if (registration.mode === 'DeskPhone' && _devices[i].isDeskPhone) {
	          defaultDevice = _devices[i];
	          break;
	        }
	
	      }
	
	      log(true, 'connecting to discovered device: ', defaultDevice);
	
	      sendConnectMsg(defaultDevice.name);
	    }
	  };
	}
	
	module.exports = getAuthenticatedCb;

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var cwicState = __webpack_require__(9);
	var $ = __webpack_require__(6);
	var errors = __webpack_require__(5);
	var initSettings = __webpack_require__(4).settings;
	var triggerError = __webpack_require__(10);
	var getAvailableDevices = __webpack_require__(32);
	
	
	/**
	 * Switch mode on a session that is already authorized. Can also be used for switching to different device in the
	 * same mode<br>
	 * @memberof $.fn.cwic
	 * @example
	 * $('#phone').cwic('switchPhoneMode',{
	    *     success: function(registration) { console.log('Phone is in '+registration.mode+' mode'); },
	    *     error: function(err) { console.log('Error: '+error.messag+' while switching mode'); },
	    *     mode: 'DeskPhone',
	    *     device: 'SEP01234567'
	    * });
	 * @param options
	 * @param {Function} [options.progress] A handler called when the mode switch has passed pre-conditions.<br>If
	 *     specified, the handler is called when the switchMode operation starts.
	 * @param {Function} [options.success(registration)] A handler called when mode switch complete with registration as
	 *     a parameter
	 * @param {Function} [options.error(err)] A handler called when the mode switch fails on pre-conditions.  {@link
	  *     $.fn.cwic-errorMapEntry} is passed as parameter.
	 * @param {string} [options.mode] The new mode 'SoftPhone'/'DeskPhone'.  Defaults to SoftPhone.  If you want to
	 *     change a property on a desk phone, such as the line, you must explicitly set this parameter to 'DeskPhone'.
	 * @param {string} [options.device] Name of the device (e.g. SEP012345678, ECPUSER) to control. If not specified and
	 *     switching from SoftPhone to DeskPhone mode, it defaults to picking first available. If not specified and
	 *     switching from DeskPhone to SoftPhone mode, it defaults to output of predictDevice function (see {@link
	  *     $.fn.cwic-settings.predictDevice}). If 'first available' is desired result in this case, output of custom
	 *     predictDevice function should be empty string (''). If device name is invalid, it fallbacks to default device
	 *     selection algorithm.
	 * @param {string} [options.line] Phone number of a line valid for the specified device (e.g. '0000'). defaults to
	 *     picking first available
	 * @param {Boolean} [options.forceRegistration] Specifies whether to forcibly unregister other softphone instances
	 *     with CUCM. Default is false. See GracefulRegistration doc for more info.
	 */
	function switchPhoneMode(options) {
	  var $this = this;
	  var regGlobals = cwicState.regGlobals;
	
	  log(true, 'switchPhoneMode started with arguments: ', options);
	
	  if (typeof options !== 'object') {
	    log(true, 'switchPhoneMode: no arguments provided, stoping execution!');
	    return $this;
	  }
	
	  regGlobals.successCb = $.isFunction(options.success) ? options.success : null;
	  regGlobals.errorCb = $.isFunction(options.error) ? options.error : null;
	  regGlobals.switchingMode = true;
	
	  var switchModeArgs = {
	    phoneMode: options.mode || 'SoftPhone',
	    deviceName: options.device || (options.mode === 'SoftPhone' ? predictDevice({username: cwicState.registration.user}) : ''),
	    lineDN: options.line || '',
	    forceRegistration: options.forceRegistration || false
	  };
	
	  function getDevicesCb(res) {
	    var devices = res.devices || [],
	      chooseDefault = true,
	      filteredDevices,
	      filteredDevicesNames,
	      selectedDeviceName,
	      isDeviceNameValid,
	      mode = switchModeArgs.phoneMode,
	      name = switchModeArgs.deviceName,
	      lineDN = switchModeArgs.lineDN,
	      force = switchModeArgs.forceRegistration;
	
	    filteredDevices = $.grep(devices, function (elem) {
	      return (elem.isDeskPhone && mode === 'DeskPhone' ||
	      elem.isSoftPhone && mode === 'SoftPhone');
	    });
	
	    if (filteredDevices.length === 0) {
	      log(true, 'switchPhoneMode: filtered device list is empty!');
	
	      if ($.isFunction(options.error)) {
	        triggerError($this, options.error, 'no device found for mode: ' + '"' + mode + '"');
	      }
	
	      return $this;
	    }
	
	    filteredDevicesNames = $.map(filteredDevices, function (device) {
	      return device.name;
	    });
	
	    isDeviceNameValid = (name && $.inArray(name, filteredDevicesNames) > -1);
	
	    if (name && isDeviceNameValid) {
	      chooseDefault = false;
	    }
	
	    if (!isDeviceNameValid) {
	      log(true, 'switchPhoneMode: Device name not set or invalid, proceeding with default device selection');
	    }
	
	    if (chooseDefault) {
	      lineDN = ''; // ignore line if device name not given or not valid
	      selectedDeviceName = filteredDevices[0] && filteredDevices[0].name;
	    } else {
	      selectedDeviceName = name;
	    }
	
	    log(true, 'switchPhoneMode: selected device: ', selectedDeviceName);
	    log(true, 'switchPhoneMode: switching to mode: ', mode);
	    log(true, 'switchPhoneMode: switching to line: ', lineDN);
	
	    sendClientRequest('connect', {
	        phoneMode: mode,
	        deviceName: selectedDeviceName,
	        lineDN: lineDN,
	        forceRegistration: force
	      },
	      function successCb() {
	        onProgress({message: 'Switch mode operation started'});
	      },
	      function errorCb(error) {
	        if ($.isFunction(options.error)) {
	          triggerError($this, options.error, errors.getError(error), {message: error});
	        }
	      }
	    );
	  }
	
	  getAvailableDevices(getDevicesCb);
	
	  function onProgress(msg) {
	    if ($.isFunction(options.progress)) {
	      try {
	        options.progress(msg);
	      } catch (progressException) {
	        log('Exception occurred in application switchPhoneMode progress callback', progressException);
	        if (typeof console !== 'undefined' && console.trace) {
	          console.trace();
	        }
	      }
	    }
	  }
	
	  return this;
	}
	
	
	/**
	 * predict a device based on username
	 * @param {Object} options
	 * @type {String}
	 */
	function predictDevice(options) {
	  if ($.isFunction(initSettings.predictDevice)) {
	    try {
	      return initSettings.predictDevice(options);
	
	    } catch (predictDeviceException) {
	      log('Exception occurred in application predictDevice callback', predictDeviceException);
	
	      if (typeof console !== 'undefined' && console.trace) {
	        console.trace();
	      }
	    }
	
	  } else {
	    return (options.username) ? initSettings.devicePrefix + options.username : '';
	  }
	}
	
	module.exports = switchPhoneMode;

/***/ },
/* 78 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	var cwicState = __webpack_require__(9);
	var getAuthenticatedCb = __webpack_require__(76);
	var $ = __webpack_require__(6);
	
	/**
	 * Initiates service discovery based sign-in. Before using this API, following callbacks must be defined in init API:
	 * <ul>
	 * <li>emailRequired</li>
	 * <li>credentialsRequired</li>
	 * <li>signedIn</li>
	 * </ul>
	 * After successfull service discovery either SSO or credentials based authentication occurs depending on UCM configuration.
	 * @memberof $.fn.cwic
	 * @param {Object} args A map with:
	 * @param {String} [args.mode]  Register the phone in this mode. Available modes are "SoftPhone" or "DeskPhone". Default of intelligent guess is applied after a device is selected.<br>
	 * @param {Function} [args.devicesAvailable(devices, phoneMode, selectDeviceCb)] Callback called after successful authentication. Might be called multiple times.
	 * If this callback is not specified, cwic applies the default device selection algorithm.  An array of {@link device} objects is passed so the application can select the device.<br>
	 * To complete the device registration, call the selectDeviceCb(phoneMode, deviceName, lineDN) function that is passed in to devicesAvailable as the third parameter.
	 * lineDN argument is optional and it is valid only in deskphone mode. It is ignored in softphone mode.
	 * @param {Boolean} [args.forceRegistration] Specifies whether to forcibly unregister other softphone instances with CUCM. Default is false. See GracefulRegistration doc for more info.
	 * @param {Function} [args.success(registration)] Callback called when registration succeeds. A {@link registration} object is passed to the callback
	 * @param {Function} [args.error(errorMapEntry)] Callback called if the registration fails.
	 */
	function startDiscovery(args) {
	  var $this = this;
	
	  if (!args) {
	    log(true, 'startDiscovery: empty arguments received');
	    args = {};
	  }
	
	  log(true, 'startDiscovery called with arguments: ', args);
	
	  setRegGlobalsD(args, $this);
	
	  if (canProceed(args, $this) === false) {
	    return $this;
	  }
	
	  // start service discovery
	  cwicState.SSOGlobals.inProgress = true;
	  sendClientRequest('startSignIn', {
	    manualSettings: false
	  });
	
	  return $this;
	}
	
	// D for Discovery
	function setRegGlobalsD(args, $this) {
	  var devicesAvailableCb = $.isFunction(args.devicesAvailable) ? args.devicesAvailable : null;
	  var regGlobals = cwicState.regGlobals;
	
	  regGlobals.registeringPhone = true;
	  regGlobals.manual = false;
	  regGlobals.successCb = $.isFunction(args.success) ? args.success : null;
	  regGlobals.errorCb = $.isFunction(args.error) ? args.error : null;
	  regGlobals.CUCM = 'discovery based address';
	
	  regGlobals.authenticatedCallback = getAuthenticatedCb(devicesAvailableCb, $this);
	
	  // reset global registration object
	  cwicState.registration = {
	    mode: args.mode || 'SoftPhone',
	    devices: {},
	    forceRegistration: args.forceRegistration || false
	  };
	
	  log(true, 'setRegGlobalsD: regGlobals set: ', regGlobals);
	  log(true, 'setRegGlobalsD: registration set: ', cwicState.registration);
	}
	
	// also triggers error as a side effect. (_triggerError does not stop the execution of current function, so it must be done manually in top function)
	function canProceed(args, $this) {
	  var regGlobals = cwicState.regGlobals;
	  var registration = cwicState.registration;
	
	  if (!cwicState.plugin) {
	    triggerError($this, regGlobals.errorCb, errorMap.PluginNotAvailable, 'Plug-in is not available or has not been initialized', {registration: registration});
	
	    return false;
	  }
	
	  if (args.mode && typeof args.mode === 'string' && !args.mode.match(/^(SoftPhone|DeskPhone)$/)) {
	    triggerError($this, regGlobals.errorCb, errorMap.InvalidArguments, 'Invalid phone mode "' + registration.mode + '"', {registration: registration});
	    return false;
	  }
	
	  return true;
	}
	
	module.exports = startDiscovery;


/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var cwicState = __webpack_require__(9);
	
	/**
	 * Cancels ongoing Single Sign On procedure if internal 'canCancelSingleSignOn' capability is enabled.
	 * 'canCancelSingleSignOn' is enabled immediately before 'ssoNavigateTo.cwic' event is triggered. It is disabled
	 * again after the token is acquired or after 'cancelSSO' call.
	 * @since: 4.0.0 <br>
	 * @memberof $.fn.cwic
	 */
	function cancelSSO() {
	  var $this = this;
	
	  if (cwicState.SSOGlobals.canCancel) {
	    // set to false early to prevent multiple calls to plugin. (It would be set to false anyway by plugin when
	    // Lifecycle state changes)
	    cwicState.SSOGlobals.canCancel = false;
	    sendClientRequest('cancelSingleSignOn', {});
	
	  } else {
	    log(false, 'SSO(cancelSSO): Not possible to cancel SSO in the current state');
	
	  }
	
	  return $this;
	}
	
	module.exports = cancelSSO;

/***/ },
/* 80 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var cwicState = __webpack_require__(9);
	
	/**
	 * Clears cached user and system data. Must be called before manual sign in if previous sign in type was discovery
	 * based. Can only be called in signed out state.
	 * @memberof $.fn.cwic
	 */
	function resetData() {
	  function resetCompleted() {
	    // check if success cb means that reset is finished? Maybe call this on InvokeResetData event?
	  }
	
	  // it's possible to call this function only in state LifeCycleStateEnum::SIGNEDOUT
	  sendClientRequest('resetData', resetCompleted);
	
	  if (localStorage) {
	    log(true, 'Removing data from localStorage...');
	
	    localStorage.removeItem(cwicState.localCacheKey);
	  }
	}
	
	module.exports = resetData;

/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	
	var sendClientRequest = __webpack_require__(7);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var getError = errors.getError;
	var triggerError = __webpack_require__(10);
	
	var addWindowToCall = __webpack_require__(14).addWindowToCall;
	
	/**
	 * @param {Object|call} conversation Can be a new object to start a new conversation or an existing {@link call} which you wish to answer.
	 * @param {Number} conversation.id Unique identifier of the conversation.  Required when referencing an exising call.
	 * @param {participant} conversation.participant First remote participant of the call.
	 * @param {String} conversation.participant.recipient The phone number of the participant.  Required when placing a new outbound call.  This will be the dialed number for the call.
	 * @param {String} [conversation.participant.name] The participant name.
	 * @param {String} [conversation.participant.photosrc] A suitable value for the src attribute of an &lt;img&gt; element.
	 * @param {String} [conversation.state] Current state of the conversation. Can be OffHook, Ringing, Connected, OnHook, Reorder.
	 * @param {Date} [conversation.start] Start time. Defined on resolution update only.
	 * @param {Date} [conversation.connect] Media connection time. Defined on resolution update only.
	 * @param {Object} [conversation.videoResolution] Resolution of the video conversation, contains width and height properties. Defined on resolution update only.
	 * @param {String|Object} [conversation.container] The HTML element which contains the conversation. Conversation events are triggered on this element.
	 * If String, specifies a jQuery selector If Object, specifies a jQuery wrapper of matched elements(s).
	 * By default container is $(this), that is the first element of the matched set startConversation is called on.
	 * @param {String} [conversation.subject] The subject of the conversation to start.
	 * @param {Function} [conversation.error(err)] A function to be called if the conversation cannot be started.  {@link $.fn.cwic-errorMapEntry} is passed as parameter.
	 * @param {String} [conversation.videoDirection] The video media direction: 'Inactive' or undefined (audio only by default), 'SendOnly', 'RecvOnly' or 'SendRecv'.
	 * @param {Object} [conversation.remoteVideoWindow] The video object (must be of mime type application/x-cisco-cwc-videocall).
	 * @param {DOMWindow} [conversation.window] DOM window that contains the remoteVideoWindow (default to this DOM window) required if specifying a video object on another window (popup/iframe).
	 * @memberof $.fn.cwic
	 * @description Start a conversation with a participant.
	 * <br>If conversation contain both an ID and a state property, cwic assumes you want to answer that incoming conversation, in this case starting the passed conversation means accepting(answering) it.
	 * @example
	 * // start an audio conversation with element #foo as container
	 * jQuery('#phone').cwic('startConversation', {
	    *   participant: {
	    *     recipient: '1234'
	    *   },
	    *   container: '#foo'
	    * });
	 * // start an audio conversation with a contact (call work phone number)
	 * jQuery('#conversation').cwic('startConversation', {
	    *   participant: {
	    *     recipient: '1234',
	    *     displayName: 'Foo Bar',
	    *     screenName: ' fbar',
	    *     phoneNumbers: {
	    *       work: '1234',
	    *       mobile: '5678'
	    *     }
	    *   }
	    * });
	 * // answer an incoming conversation (input has an id property)
	 * // see another example about the conversationIncoming event
	 * jQuery('#conversation').cwic('startConversation', {
	    *   participant: {
	    *     recipient: '1234'
	    *   },
	    *   id: '612',
	    *   state: 'Ringin'
	    * });
	 * // answer an incoming conversation with video
	 * jQuery('#conversation').cwic('startConversation',
	 *   jQuery.extend(conversation,{
	    *   videoDirection: (sendingVideo ? 'SendRecv':''),
	    *   remoteVideoWindow: 'remoteVideoWindow',  // pass id
	    *   id: callId
	    * }));
	 * // answer an incoming conversation with video object hosted in popoutwindow
	 * jQuery('#conversation').cwic('startConversation',
	 *   jQuery.extend(conversation,{
	    *   videoDirection: (sendingVideo ? 'SendRecv':''),
	    *   remoteVideoWindow: $('#remoteVideoWindow', popoutwindow.document)[0] // pass object setting jQuery context to popoutwindow document
	    *   window: popoutwindow,
	    *   id: callId
	    * }));
	 * // answer an incoming conversation without video
	 * jQuery('#callcontainer').cwic('startConversation', conversation);
	 */
	function startConversation() {
	  log(true, 'startConversation', arguments);
	
	  var $this = this;
	  var callSettings = arguments[0] || $this.data('cwic') || {};
	  var windowhandle, videoDirection;
	
	  if ($this.length === 0) {
	    return triggerError($this, callSettings.error, errorMap.InvalidArguments, 'cannot start conversation with empty selection');
	  }
	
	  // container is the jQuery wrapper of the video container
	  var container = $this;
	
	  if (typeof callSettings.container === 'string') {
	    container = $(callSettings.container);
	
	  } else if (typeof callSettings.container === 'object') {
	    container = callSettings.container;
	
	  }
	
	  container = container.first();
	
	  if (typeof callSettings.id !== 'undefined') {
	    // start an incoming conversation
	    container.addClass('cwic-data cwic-conversation cwic-conversation-' + callSettings.id).data('cwic', callSettings);
	
	    if (arguments.length >= 1) {
	      videoDirection = callSettings.videoDirection;
	
	      if (callSettings.remoteVideoWindow) {
	        addWindowToCall({
	          callId: callSettings.id,
	          remoteVideoWindow: callSettings.remoteVideoWindow
	        });
	
	        if (callSettings.remoteVideoWindow.windowhandle) {
	          windowhandle = callSettings.remoteVideoWindow.windowhandle;
	        }
	      }
	    } else {
	      videoDirection = '';
	
	    }
	
	    var answerObject = {
	      callId: callSettings.id,
	      videoDirection: videoDirection
	    };
	
	    if (windowhandle) {
	      answerObject.windowhandle = windowhandle;
	    }
	
	    sendClientRequest('answer', answerObject);
	
	  } else {
	
	    // start an outgoing conversation
	    var participant = callSettings.participant || {};
	
	    if (typeof participant === 'string') {
	      participant = { recipient: participant };
	    }
	
	    if (typeof participant.recipient === 'undefined') {
	      return triggerError($this, callSettings.error, errorMap.InvalidArguments, 'cannot start conversation: undefined or empty recipient');
	    }
	
	    container.addClass('cwic-data cwic-conversation cwic-conversation-outgoing').data('cwic', { participant: participant });
	
	    if (container.is(':hidden')) {
	      log(true, 'startConversation - warning: container is hidden');
	    }
	
	    var originateObject = {
	      recipient: participant.recipient,
	      videoDirection: callSettings.videoDirection
	    };
	
	    if (callSettings.remoteVideoWindow && callSettings.remoteVideoWindow.windowhandle) {
	      originateObject.windowhandle = callSettings.remoteVideoWindow.windowhandle;
	    }
	
	    sendClientRequest('originate', originateObject,
	      function originateCb(res) {
	        if (res.callId && res.callId >= 1) {
	          if (callSettings.remoteVideoWindow) {
	            callSettings.window = callSettings.window || window;
	
	            addWindowToCall({
	              callId: res.callId,
	              remoteVideoWindow: callSettings.remoteVideoWindow,
	              window: callSettings.window
	            });
	
	          }
	        }
	      },
	      function errorCb(error) {
	        if (error) {
	          log(true, 'originate error', error);
	          triggerError($this, getError(error), error, 'cannot start conversation');
	        }
	      }
	    );
	  }
	
	  return $this;
	}
	
	module.exports = startConversation;

/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	
	var sendClientRequest = __webpack_require__(7);
	
	var cwicState = __webpack_require__(9);
	var transferCallGlobals = cwicState.transferCallGlobals;
	
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var getError = errors.getError;
	
	var triggerError = __webpack_require__(10);
	
	var inBrowserVideo = __webpack_require__(14);
	var addWindowToCall = inBrowserVideo.addWindowToCall;
	var removeWindowFromCall = inBrowserVideo.removeWindowFromCall;
	
	var $ = __webpack_require__(6);
	
	
	/**
	 * @description Updates an existing conversation.<br>
	 * This function controls the call allowing the following operations<ul>
	 * <li>hold call</li>
	 * <li>resume call</li>
	 * <li>mute call</li>
	 * <li>unmute call</li>
	 * <li>mute audio only</li>
	 * <li>mute video only</li>
	 * <li>unmute audio only</li>
	 * <li>unmute video only</li>
	 * <li>add video window for remote sender</li>
	 * <li>remove video window for remote sender</li>
	 * <li>update video preference on a call video escalate/de-escalate</li>
	 * <li>conference two calls together</li>
	 * <li>transfer a call</li>
	 * </ul>
	 * Transfer call flow:
	 * <ol>
	 * <li>establish a conversation between clients A and B.</li>
	 * <li>call "transferCall" API to transfer a conversation to the number of client C. A conversation between clients A and C is established.
	 * <li>Now, client A have an option to complete an ongoing call transfer. If the call transfer is completed,
	 * conversation between clients B and C is established and client A is put out from both conversations. To cancel call transfer, endConversation should be called.</li>
	 * </ol>
	 * There are two ways to implement the final step of call transfer flow. The first one is to pass a "complete" button,
	 * either its id or jQuery object, to the "transferCall" API and library will automatically attach/detach handler and enable/disable the button when it's appropriate.
	 * If more specific behavior is desired comparing to this higher level API, then 'callTransferInProgress.cwic' event could be implemented, see {@link $.fn.cwic#event:callTransferInProgress}
	 * @param {String|Object} update Update a started conversation. update can be: <br>
	 * A String: hold, resume, mute, unmute, muteAudio, muteVideo, unmuteAudio, unmuteVideo.<br>
	 * An Object: contains one or more writable conversation properties to update e.g. videoDirection.<br>
	 * Triggers a conversationUpdate event.
	 * @param {String|Object} id A conversation identifier (String) or Object containing an id property <br>
	 * @example
	 * // typeof input is string HOLD/RESUME
	 * jQuery('#phone').cwic('updateConversation', 'hold', '1234')
	 * jQuery('body').cwic('updateConversation', 'hold', conversation.id);
	 * jQuery('#myid').cwic('updateConversation', 'hold', conversation);
	 *   // typeof input is object
	 * jQuery('#conversation').cwic('updateConversation', 'hold');
	 *   // resume the same conversation,
	 *   // let cwic find the conversation data attached to #conversation
	 * jQuery('#conversation').cwic('updateConversation', 'resume');
	 *   // MUTE/UNMUTE
	 *   // typeof input is string
	 * jQuery('#phone').cwic('updateConversation', 'mute', '1234');
	 * jQuery('body').cwic('updateConversation', 'mute', conversation.id);
	 * jQuery('#myid').cwic('updateConversation', 'mute', conversation);
	 *   // typeof input is object <br>
	 * jQuery('#conversation').cwic('updateConversation', 'mute');
	 *   // unmute the same conversation,
	 *   // let cwic find the conversation data attached to #conversation
	 * jQuery('#conversation').cwic('updateConversation', 'unmute');
	 *
	 *  // add/remove video object in this (default) DOMWindow
	 * jQuery('#conversation').cwic('updateConversation',
	 *               { 'addRemoteVideoWindow':videoObject });
	 * jQuery('#conversation').cwic('updateConversation',
	 *               { 'removeRemoteVideoWindow':videoObject });
	 * // add/remove video object from another DOMWindow
	 * jQuery('#conversation').cwic('updateConversation',
	 *               { 'addRemoteVideoWindow':videoObject, window:popupWindow });
	 * jQuery('#conversation').cwic('updateConversation',
	 *               { 'removeRemoteVideoWindow':videoObject, window:popupWindow });
	 *
	 * // Escalate to video
	 * jQuery('#conversation').cwic('updateConversation', {'videoDirection': 'SendRecv'}); // implied source call is call associated with conversation div
	 * jQuery('#phone').cwic('updateConversation', {'videoDirection': 'SendRecv'}, conversation.id}); // source call id passed
	 * jQuery('#phone').cwic('updateConversation', {'videoDirection': 'SendRecv'}, conversation}); // source call passed
	 * // De-escalate from video
	 * jQuery('#conversation').cwic('updateConversation', {'videoDirection': 'Inactive'}); // implied source call is call associated with conversation div
	 * jQuery('#phone').cwic('updateConversation', {'videoDirection': 'Inactive'}, conversation.id}); // source call id passed
	 * jQuery('#phone').cwic('updateConversation', {'videoDirection': 'Inactive'}, conversation}); // source call passed
	 *
	 * // Transfer call to target number
	 * jQuery('#conversation').cwic('updateConversation', {'transferCall': number, completeButton: 'completebtn'}); // implied source call is call associated with conversation div
	 * jQuery('#phone').cwic('updateConversation', {'transferCall': number}, conversation.id}); // source call id passed. Bind to {@link $.fn.cwic#event:callTransferInProgress} to handle transfer complete
	 * jQuery('#phone').cwic('updateConversation', {'transferCall': number}, conversation}); // source call passed
	 *
	 * // Join target callId to source call
	 * jQuery('#conversation').cwic('updateConversation', {'joinCall':callId}); // implied source call is call associated with conversation div
	 * jQuery('#phone').cwic('updateConversation', {'joinCall':callId}, conversation.id}); // source call id passed
	 * jQuery('#phone').cwic('updateConversation', {'joinCall':callId}, conversation}); // source call passed
	 * @memberof $.fn.cwic
	 */
	function updateConversation() {
	  log(true, 'updateConversation', arguments);
	
	  var $this = this;
	
	  if ($this.length === 0) {
	    return $this;
	  }
	
	  // mandatory first argument
	  var update = arguments[0];
	
	  // find conversation information
	  var conversation = null;
	  var conversationId = null;
	
	  if (typeof arguments[1] === 'object') {
	    conversation = arguments[1];
	    conversationId = conversation.id;
	
	  } else if (typeof arguments[1] === 'undefined') {
	    conversation = $this.data('cwic'); // attached conversation object
	
	    if (typeof conversation === 'object') {
	      conversationId = conversation.id;
	    }
	
	  } else {
	    conversationId = arguments[1];
	    conversation = $('.cwic-conversation-' + conversationId).data('cwic') || $this.data('cwic');
	
	  }
	
	  if (!conversationId || !conversation) {
	    return triggerError($this, errorMap.InvalidArguments, 'cannot update conversation: undefined or empty conversation id');
	  }
	
	  if (typeof update === 'string') {
	    var request = null, content = null;
	
	    if (update.match(/^hold$/i)) {
	      request = 'hold';
	      content = {
	        callId: conversationId
	      };
	
	    } else if (update.match(/^resume$/i)) {
	      request = 'resume';
	      content = {
	        callId: conversationId
	      };
	
	    } else if (update.match(/^mute$/i)) {
	      request = 'mute';
	      content = {
	        callId: conversationId
	      };
	
	    } else if (update.match(/^unmute$/i)) {
	      request = 'unmute';
	      content = {
	        callId: conversationId
	      };
	
	    } else if (update.match(/^muteAudio$/i)) {
	      request = 'mute';
	      content = {
	        callId: conversationId,
	        muteAudio: true
	      };
	
	    } else if (update.match(/^muteVideo$/i)) {
	      request = 'mute';
	      content = {
	        callId: conversationId,
	        muteVideo: true
	      };
	
	    } else if (update.match(/^unmuteAudio$/i)) {
	      request = 'unmute';
	      content = {
	        callId: conversationId,
	        unmuteAudio: true
	      };
	
	    } else if (update.match(/^unmuteVideo$/i)) {
	      request = 'unmute';
	      content = {
	        callId: conversationId,
	        unmuteVideo: true
	      };
	
	    } else {
	      return triggerError($this, errorMap.InvalidArguments, 'wrong arguments (update conversation) - ' + update, arguments);
	    }
	
	    sendClientRequest(request,
	      content,
	      $.noop,
	      function errorCb(error) {
	        triggerError($this, getError(error), error);
	      }
	    );
	
	  } else if (typeof update === 'object') {
	    var foundWritable = false;
	
	    if (update.transferCall) {
	      if (transferCallGlobals.inProgress) {
	        log(true, 'Call transfer already in progress, canceling...');
	
	        return $this;
	
	      }
	
	      foundWritable = true;
	
	      transferCallGlobals.completeBtn = update.completeButton;
	      transferCallGlobals.inProgress = true;
	
	      sendClientRequest('transferCall', {
	          callId: conversationId,
	          transferToNumber: update.transferCall
	        },
	        $.noop,
	        function errorCb(error) {
	          triggerError($this, getError(error, 'NativePluginError'), 'transferCall', error);
	        }
	      );
	    }
	
	    if (update.joinCall) {
	      foundWritable = true;
	
	      sendClientRequest('joinCalls', {
	          joinCallId: conversationId,
	          callId: update.joinCall
	        },
	        $.noop,
	        function errorCb(error) {
	          triggerError($this, getError(error, 'NativePluginError'), 'joinCall', error);
	        }
	      );
	    }
	
	    if (update.videoDirection) {
	      foundWritable = true;
	      sendClientRequest('setVideoDirection', {
	          callId: conversationId,
	          videoDirection: update.videoDirection
	        },
	        $.noop,
	        function errorCb(error) {
	          triggerError($this, getError(error, 'NativePluginError'), 'videoDirection', error);
	        }
	      );
	    }
	
	    if (update.addRemoteVideoWindow) {
	      foundWritable = true;
	
	      log('updateConversation() calling addWindowToCall() for conversationId: ' + conversationId);
	
	      addWindowToCall({
	        callId: conversationId,
	        remoteVideoWindow: update.addRemoteVideoWindow,
	        window: update.window
	      });
	    }
	
	    if (update.removeRemoteVideoWindow) {
	      foundWritable = true;
	
	      log('updateConversation() calling removeWindowFromCall() for conversationId: ' + conversationId);
	
	      removeWindowFromCall({
	        callId: conversationId,
	        remoteVideoWindow:  update.removeRemoteVideoWindow,
	        window: update.window,
	        endCall: false
	      });
	    }
	
	    if (!foundWritable) {
	      triggerError($this, errorMap.InvalidArguments, 'wrong arguments (update conversation)', arguments);
	    }
	
	  } else {
	    triggerError($this, errorMap.InvalidArguments, 'wrong arguments (update conversation)', arguments);
	  }
	
	  return $this;
	}
	
	module.exports = updateConversation;


/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	
	var sendClientRequest = __webpack_require__(7);
	
	var cwicState = __webpack_require__(9);
	var transferCallGlobals = cwicState.transferCallGlobals;
	var regGlobals = cwicState.regGlobals;
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	
	var triggerError = __webpack_require__(10);
	
	
	
	/**
	 * @description Ends a conversation. Triggers a conversationEnd event.
	 * @memberof $.fn.cwic
	 * @param {boolean} iDivert If true, redirects the call to voice mail. See UCM documentation on the Immediate Divert (iDivert) feature for details. The call can be iDiverted only if {@link call#capabilities} contains 'canImmediateDivert',
	 * @param {String|Object} id A conversation identifier (String) or an Object containing an id property.
	 * @example
	 *  // typeof input is string
	 * jQuery('#phone').cwic('endConversation', '1234');
	 *  // or
	 * jQuery('#phone').cwic('endConversation', conversation.id);
	 *  // typeof input is object
	 * jQuery('#phone').cwic('endConversation', conversation);
	 *  // let cwic find the conversation data attached to #conversation
	 * jQuery('#conversation').cwic('endConversation');
	 *  // iDivert the conversation
	 * jQuery('#myconversation').cwic('endConversation', true);
	 *  // iDivert and specify conversation id as a string
	 * jQuery('#phone').cwic('endConversation', true, '1234');
	 *
	 *
	 */
	function endConversation() {
	  log(true, 'endConversation', arguments);
	
	  var $this = this;
	
	  if ($this.length === 0) {
	    return $this;
	  }
	
	  var iDivert = null;
	  var conversation = null;
	  var conversationId = null;
	
	  if (arguments.length === 0) {
	    conversation = $this.data('cwic');
	
	    if (!conversation) {
	      return triggerError($this, 'cannot end conversation: no conversation exists for this element');
	    }
	
	    conversationId = conversation.id;
	
	  } else if (arguments.length === 1) {
	    iDivert = typeof arguments[0] === 'boolean' ? arguments[0] : null;
	    conversation = typeof arguments[0] === 'object' ? arguments[0] : $this.data('cwic');
	    conversationId = typeof arguments[0] === 'string' ? arguments[0] : conversation.id;
	
	  } else if (arguments.length === 2) {
	    iDivert = typeof arguments[0] === 'boolean' ? arguments[0] : null;
	    conversation = typeof arguments[1] === 'object' ? arguments[1] : $this.data('cwic');
	    conversationId = typeof arguments[1] === 'string' ? arguments[1] : conversation.id;
	
	  }
	
	  if (!conversationId) {
	    return triggerError($this, errorMap.InvalidArguments, 'cannot end conversation: undefined or empty conversation id');
	  }
	
	  if (transferCallGlobals.callId === conversationId) {
	    transferCallGlobals.endTransfer();
	  }
	
	  if (iDivert) {
	    // need to check capabilities first
	    conversation = conversation || $('.cwic-conversation-' + conversationId).data('cwic');
	
	    if (!conversation) {
	      return triggerError($this, 'cannot iDivert - undefined conversation');
	    }
	
	    if (!conversation.capabilities || !conversation.capabilities.canImmediateDivert) {
	      return triggerError($this, errorMap.CapabilityMissing, 'cannot iDivert - missing capability', { conversation: conversation });
	    }
	
	    log(true, 'iDivert conversation', conversation);
	
	    sendClientRequest('iDivert', {
	      callId: conversationId
	    });
	
	  } else {
	    log(true, 'end conversation', conversation);
	
	    regGlobals.endingCallForId = conversationId;
	
	    sendClientRequest('endCall', {
	      callId: conversationId
	    });
	  }
	
	  return $this;
	}
	
	module.exports = endConversation;


/***/ },
/* 84 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	
	var triggerError = __webpack_require__(10);
	var sendClientRequest = __webpack_require__(7);
	
	
	/**
	 * Sends digit (String) as Dual-Tone Multi-Frequency (DTMF)
	 * @memberof $.fn.cwic
	 * @example
	 *  // SEND DTMF EXAMPLE
	 * jQuery('#phone').cwic('sendDTMF', '5', '1234');
	 * jQuery('#mydiv').cwic('sendDTMF', '3', conversation.id);
	 * jQuery('body').cwic('sendDTMF', '7', conversation);
	 * jQuery('#conversation').cwic('sendDTMF', '1');
	 * @param {String} digit Dual-Tone Multi-Frequency (DTMF) digit to send.  Does not trigger any event.
	 * @param {String|Object} [id] a {String} conversation identifier or an {Object} containing an id property
	 */
	function sendDTMF() {
	  log(true, 'sendDTMF'); // don't send dtmf digits to logger
	
	  var $this = this;
	  var digit = null;
	  var conversation = $this.data('cwic');
	  var conversationId = conversation ? conversation.id : null;
	  var allowedDigits = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '#', '*', 'A', 'B', 'C', 'D', 'a', 'b', 'c', 'd'];
	
	  // inspect arguments
	  if (arguments.length > 0) {
	    digit = typeof arguments[0] === 'string' ? arguments[0] : null;
	
	    if (arguments.length > 1) {
	      if (typeof arguments[1] === 'object') {
	        conversation = arguments[1];
	        conversationId = conversation.id;
	
	      } else if (typeof arguments[1] === 'string') {
	        conversationId = arguments[1];
	      }
	    }
	  }
	
	  if (typeof digit !== 'string' || !conversationId) {
	    return triggerError($this, errorMap.InvalidArguments, 'wrong arguments (sendDTMF)', arguments);
	  }
	
	  if (allowedDigits.indexOf(digit) === -1) {
	    return triggerError($this, errorMap.InvalidArguments, 'invalid DTMF digit (sendDTMF)', arguments);
	  }
	
	  sendClientRequest('sendDTMF', {
	    callId: conversationId,
	    digit: digit
	  });
	
	  return $this;
	}
	
	module.exports = sendDTMF;

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var cwicState = __webpack_require__(9);
	/**
	 * @memberof $.fn.cwic
	 * @returns {Object}
	 */
	function getInstanceId() {
	  log(true, 'getInstanceId');
	  return cwicState.getInstanceId();
	}
	
	module.exports = getInstanceId;

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

	var cwicState = __webpack_require__(9);
	var log = __webpack_require__(3);
	
	
	/**
	 * Gets a list of objects describing the multimedia devices installed on a system.
	 * @since 3.0.0
	 * @memberof $.fn.cwic
	 * @returns
	 * a list of objects describing the multimedia devices with the following properties:<ul>
	 *   <li>deviceID: unique device ID</li>
	 *   <li>deviceName: {string} human readable device name</li>
	 *   <li>vendorID: {string} unique vendor ID</li>
	 *   <li>productID: {string} vendor product ID</li>
	 *   <li>hardwareID: {string} hardware dependent ID</li>
	 *   <li>canRecord: {boolean} indicates whether this object can be used as an audio recording device</li>
	 *   <li>canPlayout: {boolean} indicates whether this object can be used as an audio playout device</li>
	 *   <li>canCapture: {boolean} indicates whether this object can be used as a video capture device</li>
	 *   <li>canRing: {boolean} indicates whether this object can be used as a ringer device</li>
	 *   <li>isDefault:  {boolean} indicates whether this object represents the default device of the type indicated by the canRecord, canPlayout, and canCapture flags</li>
	 *   <li>recordingName: {string} human readable name for the audio recording function of this device</li>
	 *   <li>playoutName: {string} human readable name for the audio playout function of this device</li>
	 *   <li>captureName: {string} human readable name for the video capture function of this device</li>
	 *   <li>recordingID: {string} ID for the audio recording function of this device</li>
	 *   <li>playoutID: {string} ID for the audio playout function of this device</li>
	 *   <li>captureID: {string} ID for the video capture function of this device</li>
	 *   <li>clientRecordingID: {string} the ID to pass to setRecordingDevice to select this device as the audio recording device</li>
	 *   <li>clientPlayoutID: {string} the ID to pass to setPlayoutDevice to select this device as the audio playout device</li>
	 *   <li>clientCaptureID: {string} the ID to pass to setCaptureDevice to select this device as the video capture device</li>
	 *   <li>isSelectedRecordingDevice: {boolean} indicates whether this is the currently selected audio recording device</li>
	 *   <li>isSelectedPlayoutDevice: {boolean} indicates whether this is the currently selected audio playout device</li>
	 *   <li>isSelectedCaptureDevice: {boolean} indicates whether this is the currently selected video capture device</li>
	 *   </ul>
	 *   In order to use the list, the client should check the canXXXX fields to determine if a device can be passed as a particular function, then pass the clientXXXID
	 *   to the correct setXXXXDevice function.
	 *
	 *   Depending on the platform, devices with multiple functions may show up as a single entry with multiple IDs, or multiple times with similar or different IDs.
	 *
	 * @example
	 *  see sample.html
	 */
	function getMultimediaDevices() {
	  // new messaging interface passes mmDevices list in the change event
	  // so we just return the data from the most recent event
	  var devices = { 'multimediadevices': cwicState.plugin.multimediadevices };
	
	  log(true, 'getMultimediaDevices returning:', devices);
	
	  return devices;
	
	}
	
	module.exports = getMultimediaDevices;

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	
	var triggerError = __webpack_require__(10);
	var sendClientRequest = __webpack_require__(7);
	var triggerMMDeviceEvent = __webpack_require__(52);
	
	
	
	/**
	 * Sets the audio recording device used by the Cisco Web Communicator.  To set a device, pass the clientRecordingID from a device with the canRecord flag set to true.
	 * @memberof $.fn.cwic
	 * @since 3.0.0
	 * @param {String} clientRecordingID: clientRecordingID retrieved from getMultimediaDevices()
	 */
	function setRecordingDevice() {
	  log(true, 'setRecordingDevice', arguments);
	  var $this = this;
	
	  var clientRecordingIDIn = arguments[0];
	
	  if (typeof clientRecordingIDIn !== 'string' || clientRecordingIDIn.length === 0) {
	    return triggerError($this, errorMap.InvalidArguments, 'wrong arguments (setRecordingDevice)', arguments);
	  }
	
	  sendClientRequest('setRecordingDevice', {
	    'clientRecordingID': clientRecordingIDIn
	  });
	
	  // after setting device, we need to refresh our cache
	  sendClientRequest('getMultimediaDevices', function mmDevicesCb(content) {
	    triggerMMDeviceEvent($this, content);
	  });
	
	}
	
	module.exports = setRecordingDevice;


/***/ },
/* 88 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	var triggerMMDeviceEvent = __webpack_require__(52);
	
	
	/**
	 * Sets the audio playout device used by the Cisco Web Communicator.  To set a device, pass the clientPlayoutID from a device with the canPlayout flag set to true.
	 * @memberof $.fn.cwic
	 * @since 3.0.0
	 * @param {String} clientPlayoutID: clientPlayoutID retrieved from getMultimediaDevices()
	 */
	function setPlayoutDevice() {
	  log(true, 'setPlayoutDevice', arguments);
	
	  var $this = this;
	  var clientPlayoutIDIn = arguments[0];
	
	  if (typeof clientPlayoutIDIn !== 'string' || clientPlayoutIDIn.length === 0) {
	    return triggerError($this, errorMap.InvalidArguments, 'wrong arguments (setPlayoutDevice)', arguments);
	  }
	
	  sendClientRequest('setPlayoutDevice', {
	    'clientPlayoutID': clientPlayoutIDIn
	  });
	
	  // after setting device, we need to refresh our cache
	  sendClientRequest('getMultimediaDevices', function mmDevicesCb(content) {
	    triggerMMDeviceEvent($this, content);
	  });
	}
	
	module.exports = setPlayoutDevice;


/***/ },
/* 89 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var sendClientRequest = __webpack_require__(7);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	var triggerError = __webpack_require__(10);
	var triggerMMDeviceEvent = __webpack_require__(52);
	
	
	/**
	 * Sets the video capture device used by the Cisco Web Communicator.  To set a device, pass the clientCaptureID from a device with the canCapture flag set to true.
	 * @since 3.0.0
	 * @memberof $.fn.cwic
	 * @param {String} clientCaptureID: clientCaptureID retrieved from getMultimediaDevices()
	 */
	function setCaptureDevice() {
	  log(true, 'setCaptureDevice', arguments);
	
	  var $this = this;
	  var clientCaptureIDIn = arguments[0];
	
	  if (typeof clientCaptureIDIn !== 'string' || clientCaptureIDIn.length === 0) {
	    return triggerError($this, errorMap.InvalidArguments, 'wrong arguments (setCaptureDevice)', arguments);
	  }
	
	  sendClientRequest('setCaptureDevice', {
	    'clientCaptureID': clientCaptureIDIn
	  });
	
	  // after setting device, we need to refresh our cache
	  sendClientRequest('getMultimediaDevices', function mmDevicesCb(content) {
	    triggerMMDeviceEvent($this, content);
	  });
	}
	
	module.exports = setCaptureDevice;


/***/ },
/* 90 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var errors = __webpack_require__(5);
	var errorMap = errors.errorMap;
	
	var triggerError = __webpack_require__(10);
	var sendClientRequest = __webpack_require__(7);
	var triggerMMDeviceEvent = __webpack_require__(52);
	
	
	
	
	/**
	 * Sets the ringer device used by the Cisco Web Communicator. To set a device, pass the clientRingerID from a device with the canRing flag set to true.
	 * @memberof $.fn.cwic
	 * @since 4.0.0
	 * @param {String} clientRingerID: clientRingerID retrieved from getMultimediaDevices()
	 */
	function setRingerDevice() {
	  log(true, 'setRingerDevice', arguments);
	
	  var $this = this;
	  var clientRingerIDIn = arguments[0];
	
	  if (typeof clientRingerIDIn !== 'string' || clientRingerIDIn.length === 0) {
	    return triggerError($this, errorMap.InvalidArguments, 'wrong arguments (setRingerDevice)', arguments);
	  }
	
	  sendClientRequest('setRingerDevice', {
	    'clientRingerID': clientRingerIDIn
	  });
	
	  // after setting device, we need to refresh our cache
	  sendClientRequest('getMultimediaDevices', function mmDevicesCb(content) {
	    triggerMMDeviceEvent($this, content);
	  });
	}
	
	module.exports = setRingerDevice;

/***/ },
/* 91 */
/***/ function(module, exports, __webpack_require__) {

	var about = __webpack_require__(15);
	var triggerError = __webpack_require__(10);
	var errorMap = __webpack_require__(5).errorMap;
	var cwicState = __webpack_require__(9);
	
	
	
	/** @description Gets the current user authorization status.
	 * @since 3.0.1
	 * @memberof $.fn.cwic
	 * @returns {String} a value indicating the current user authorization status.
	 * <ul>
	 * <li>"UserAuthorized" indicates the user has authorized the Cisco Web Communicator add-on and it is ready to use.</li>
	 * <li>"MustShowAuth" indicates the application must call {@link $.fn.cwic-showUserAuthorization} to show the user authorization dialog.</li>
	 * <li>"UserDenied" indicates the user has denied the application access to the Cisco Web Communicator add-on.</li>
	 * <li>"UserAuthPending" indicates the dialog box is currently displayed and the user has not yet selected "allow", "deny", or "always allow".</li>
	 * <li>"Unknown" indicates status cannot be determined because delay authorization feature is not supported by the current Cisco Web Communicator add-on.
	 * This case will trigger {@link $.fn.cwic-errorMap.OperationNotSupported} as well.</li>
	 * </ul>
	 */
	function getUserAuthStatus() {
	  var ab = about();
	
	  if (!ab.capabilities.delayedUserAuth) {
	    triggerError(this, errorMap.OperationNotSupported, 'Check cwic("about").capabilities.delayedUserAuth');
	
	    return 'Unknown';
	  }
	
	  return cwicState.plugin && cwicState.plugin.userAuthStatus || null;
	}
	
	module.exports = getUserAuthStatus;

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

	var dockGlobals = __webpack_require__(12);
	var sendClientRequest = __webpack_require__(7);
	var log = __webpack_require__(3);
	var triggerError = __webpack_require__(10);
	var errorMap = __webpack_require__(5).errorMap;
	
	/**
	 * Undocks an external video window previously docked by {@link $.fn.cwic-dock}<br>
	 * Example use:
	 * @example
	 * $('#videocontainer').cwic('undock');
	 * // or:
	 * $(document).cwic('undock');
	 * // or:
	 * $().cwic('undock');
	 * @since 3.1.2
	 * @memberof $.fn.cwic
	 */
	function undock() {
	  var $this = this;
	
	  log(false, 'Undocking ...');
	
	  if (dockGlobals.hasDockingCapabilities() && dockGlobals.isDocked) {
	    log(true, 'undock', arguments);
	    log(true, 'Setting isDocked to false and resetting docking position...');
	
	    dockGlobals.resetPosition();
	    dockGlobals.isDocked = false;
	    sendClientRequest('undockExternalWindow', {
	      windowType: 'remote'
	    });
	
	  } else if (!dockGlobals.hasDockingCapabilities()) {
	    triggerError($this, errorMap.DockingCapabilitiesNotAvailable);
	  }
	
	  return $this;
	}
	
	module.exports = undock;

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var isMac = __webpack_require__(13).isMac;
	var sendClientRequest = __webpack_require__(7);
	var $ = __webpack_require__(6);
	
	
	
	/**
	 * Sets speaker volume. Works on Windows platform only.
	 * @memberof $.fn.cwic
	 * @since 4.0.0
	 * @param {String|Number} args.volume Volume to set (0 to 100)
	 * @param {Function} args.success Success callback
	 * @param {Function} args.error Error callback
	 */
	function setSpeakerVolume(content) {
	  if (isMac()) {
	    log(false, 'setPlayRingerOnAllDevices works only on Windows platform for now...');
	
	    return;
	  }
	
	  var volume = parseInt(content.speakerVolume, 10);
	  var success = $.isFunction(content.success) ? content.success : $.noop;
	  var error = $.isFunction(content.error) ? content.error : $.noop;
	
	  sendClientRequest('setCurrentSpeakerVolume', {
	    volume: volume
	  }, success, error);
	
	}
	
	module.exports = setSpeakerVolume;


/***/ },
/* 94 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var isMac = __webpack_require__(13).isMac;
	var sendClientRequest = __webpack_require__(7);
	var $ = __webpack_require__(6);
	
	
	
	
	/**
	 * Sets ringer volume. Works on Windows platform only.
	 * @memberof $.fn.cwic
	 * @since 4.0.0
	 * @param {String|Number} args.volume Volume to set (0 to 100)
	 * @param {Function} args.success Success callback
	 * @param {Function} args.error Error callback
	 */
	function setRingerVolume(content) {
	  if (isMac()) {
	    log(false, 'setRingerVolume works only on Windows platform for now...');
	
	    return;
	  }
	
	  var volume = parseInt(content.ringerVolume, 10);
	  var success = $.isFunction(content.success) ? content.success : $.noop;
	  var error = $.isFunction(content.error) ? content.error : $.noop;
	
	  sendClientRequest('setCurrentRingerVolume', {
	    volume: volume
	  }, success, error);
	}
	
	module.exports = setRingerVolume;


/***/ },
/* 95 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var isMac = __webpack_require__(13).isMac;
	var sendClientRequest = __webpack_require__(7);
	var $ = __webpack_require__(6);
	
	
	
	
	/**
	 * Sets microphone volume. Works on Windows platform only.
	 * @memberof $.fn.cwic
	 * @since 4.0.0
	 * @param {String|Number} args.volume Volume to set (0 to 100)
	 * @param {Function} args.success Success callback
	 * @param {Function} args.error Error callback
	 */
	function setMicrophoneVolume(content) {
	  if (isMac()) {
	    log(false, 'setMicrophoneVolume works only on Windows platform for now...');
	
	    return;
	  }
	
	  var volume = parseInt(content.microphoneVolume, 10);
	  var success = $.isFunction(content.success) ? content.success : $.noop;
	  var error = $.isFunction(content.error) ? content.error : $.noop;
	
	  sendClientRequest('setCurrentMicrophoneVolume', {
	    volume: volume
	  }, success, error);
	
	}
	
	module.exports = setMicrophoneVolume;


/***/ },
/* 96 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var isMac = __webpack_require__(13).isMac;
	var sendClientRequest = __webpack_require__(7);
	
	
	
	
	/**
	 * Sets ringtone. Will trigger ringtoneChange.cwic event. Works on Windows platform only.
	 * @memberof $.fn.cwic
	 * @since 4.0.0
	 * @param {String} ringtone ringtone name
	 */
	function setRingtone(ringtone) {
	  if (isMac()) {
	    log(false, 'setRingtone works only on Windows platform for now...');
	
	    return;
	  }
	
	  sendClientRequest('setCurrentRingtone', {
	    ringtone: ringtone
	  });
	
	}
	
	module.exports = setRingtone;


/***/ },
/* 97 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var isMac = __webpack_require__(13).isMac;
	var sendClientRequest = __webpack_require__(7);
	
	
	
	
	/**
	 * Sets all capable devices as ringers. Works on Windows platform only.
	 * @memberof $.fn.cwic
	 * @since 4.0.0
	 */
	function setPlayRingerOnAllDevices() {
	  if (isMac()) {
	    log(false, 'setPlayRingerOnAllDevices works only on Windows platform for now...');
	    return;
	  }
	
	  sendClientRequest('setPlayRingerOnAllDevices');
	}
	
	module.exports = setPlayRingerOnAllDevices;


/***/ },
/* 98 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var isMac = __webpack_require__(13).isMac;
	var cwicState = __webpack_require__(9);
	var sendClientRequest = __webpack_require__(7);
	
	
	
	
	/**
	 * Gets the current volume value for particular device. Async function.
	 * Works on Windows platform only.
	 * @since 4.0.0
	 * @memberof $.fn.cwic
	 * @param {String} device Type of device. One of: "Speaker", "Microphone", "Ringer"
	 * @param {Function} callback(volume) Callback to be called with volume value
	 */
	function getMultimediaDeviceVolume(device, handleVolumeChangeCallback) {
	  if (isMac()) {
	    log(false, 'getMultimediaDeviceVolume works only on Windows platform for now...');
	
	    return;
	  }
	
	  if (device !== 'Speaker' && device !== 'Microphone' && device !== 'Ringer') {
	    log(false, 'getMultimediaDeviceVolume received wrong "device" parameter. Must be one of ("Speaker", "Microphone", "Ringer") and received: ' + device);
	  }
	
	  if (cwicState.isMultimediaStarted) {
	    sendClientRequest('getMultimediaDeviceVolume', {
	      device: device
	    }, handleVolumeChangeCallback);
	
	  } else {
	    log(true, 'getMultimediaDeviceVolume: Mutimedia services not started, returning...');
	  }
	}
	
	module.exports = getMultimediaDeviceVolume;


/***/ },
/* 99 */
/***/ function(module, exports, __webpack_require__) {

	var log = __webpack_require__(3);
	var validators = __webpack_require__(34);
	var _cwic_onVideoPluginLoaded = __webpack_require__(14)._cwic_onVideoPluginLoaded; // todo: extract from api layer
	var errorMap = __webpack_require__(5).errorMap;
	var sendClientRequest = __webpack_require__(7);
	
	function setGlobalHandlers() {
	  /**@description
	   * Global window namespace
	   * @namespace
	   * @name window
	   */
	
	  /**
	   * global(window) level function object to handle video plug-in object onLoad
	   * @type function
	   * @memberof window
	   * @param  {Object} videopluginobject video plug-in object (DOM Element)
	   * @param {Object} win Optional window object. Used only if it's called from _cwic_onPopupVideoPluginLoaded
	   * @returns undefined
	   * @private
	   */
	  window._cwic_onVideoPluginLoaded = _cwic_onVideoPluginLoaded;
	
	  /**
	   * global(window) level function to handle video plug-ins loaded in iframe/popup window.
	   * Should be called from video object onLoad handler {@link $.fn.cwic.createVideoWindow}.<br>
	   * Example onLoad function in iframe.html
	   * @example
	   * function onvideoPluginLoaded(obj) {
	     *     window.parent._cwic_onPopupVideoPluginLoaded(obj, window);
	     * }
	   * @returns undefined
	   * @memberof window
	   * @param  {JSAPI} videopluginobject video plug-in object (DOM Element)
	   * @param  {DOMWindow} win iframe or popup window
	   * @public
	   */
	  window._cwic_onPopupVideoPluginLoaded = function (videopluginobject, win) {
	    log('_cwic_onPopupVideoPluginLoaded called');
	
	    _cwic_onVideoPluginLoaded(videopluginobject, win);
	  };
	
	  /**
	   * global(window) level function object to handle SSO token receiving from child window (popup or iFrame).
	   * Should be called after the token is returned in URL of the target HTML page configured via 'redirect_uri' parameter.
	   * @type function
	   * @memberof window
	   * @param  {Object} msg     message received from popup/iFrame
	   * @param {string} msg.url URL with parameters: <ul><li>access_token - returned token</li><li>token_type - Always "Bearer"</li><li>expires_in - Number of seconds the returned token is valid</li></ul>
	   * @example
	   * //As soon as the page loads, call SSO token handler on the parent window (parent page is a page with cwic.js loaded)
	   * window.onload = function(e) {
	    *     // The SSO token information is available in a URL fragment. Pass a whole URL string to SSO token handler
	    *     window.opener._cwic_onSSOTokenReceived(location.hash);
	    *     window.close()
	    * };
	   * @public
	   */
	  window._cwic_onSSOTokenReceived = function onSSOTokenReceived(msg) {
	    var url = msg.url,
	      ssoTokenValidator = validators.get('ssotoken');
	
	    if (ssoTokenValidator.isValid(url)) {
	      sendClientRequest('ssoNavigationCompleted', {result: 200, url: url, document: ''});
	
	    } else {
	      throw new Error(errorMap.InvalidURLFragment.code);
	    }
	  };
	}
	
	module.exports = setGlobalHandlers;

/***/ },
/* 100 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule   = __webpack_require__(72);
	
	// Multimedia Modules
	var SpeakerModule    = __webpack_require__(101);
	var MicrophoneModule = __webpack_require__(103);
	var CameraModule     = __webpack_require__(104);
	var RingerModule     = __webpack_require__(105);
	var RingtoneModule   = __webpack_require__(106);
	var MonitorModule    = __webpack_require__(107);
	
	
	var cwic = {
	    Speaker    : SpeakerModule.Speaker,
	    Microphone : MicrophoneModule.Microphone,
	    Camera     : CameraModule.Camera,
	    Ringer     : RingerModule.Ringer,
	    Ringtone   : RingtoneModule.Ringtone,
	    Monitor    : MonitorModule.Monitor,
	
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	    MessageSender   : MessageSenderModule.MessageSender
	};
	
	/**
	 * @class MultimediaController
	 *
	 * @classdesc
	 * Multimedia controller is responsible for managing [Media Devices]{@link MediaDevice}, [Ringtones]{@link Ringtone}
	 * and [Monitors]{@link Monitor} that are used for screen share. <br>
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 *
	 */
	function MultimediaController()
	{
	    this.speakerList    = [];
	    this.microphoneList = [];
	    this.ringerList     = [];
	    this.cameraList     = [];
	    this.ringtoneList   = [];
	    var m_EventHandlers  = {};
	
	    cwic.MessageReceiver.addMessageHandler('multimediacapabilitiesstarted', onCapabilitiesStarted.bind(this));
	    cwic.MessageReceiver.addMessageHandler('multimediacapabilitiesstopped', onCapabilitiesStopped.bind(this));
	
	    function onCapabilitiesStarted()
	    {
	        cwic.MessageReceiver.addMessageHandler('multimediadevicechange', mediaDeviceListChanged.bind(this));
	        cwic.MessageReceiver.addMessageHandler('ringtonechanged', onRingtoneChanged.bind(this));
	        this.refreshMediaDeviceList();
	        this.refreshRingtoneList();
	    }
	
	    function onCapabilitiesStopped()
	    {
	        cwic.MessageReceiver.removeMessageHandler('multimediadevicechange');
	
	        this.speakerList.length = 0;
	        this.cameraList.length = 0;
	        this.microphoneList.length = 0;
	        this.ringerList.length = 0;
	
	        var eventHandler = m_EventHandlers['onMediaDeviceListChanged'];
	
	        if(eventHandler)
	        {
	            eventHandler();
	        }
	    }
	
	    function mediaDeviceListChanged()
	    {
	        this.refreshMediaDeviceList();
	    }
	
	    function onMediaDeviceListChanged(content)
	    {
	        var deviceList = content.multimediadevices;
	
	        this.speakerList.length = 0;
	        this.cameraList.length = 0;
	        this.microphoneList.length = 0;
	        this.ringerList.length = 0;
	
	        for (var index = 0; index < deviceList.length; ++index)
	        {
	            var device = deviceList[index];
	
	            if (device.canPlayout === true)
	            {
	                var speaker = new cwic.Speaker(device);
	                this.speakerList.push(speaker);
	            }
	
	            if(device.canRecord === true)
	            {
	                var microphone = new cwic.Microphone(device);
	                this.microphoneList.push(microphone);
	            }
	
	            if(device.canCapture === true)
	            {
	                var camera = new cwic.Camera(device);
	                this.cameraList.push(camera);
	            }
	
	            if(device.canRing === true)
	            {
	                var ringer = new cwic.Ringer(device);
	                this.ringerList.push(ringer);
	            }
	        }
	
	        var eventHandler = m_EventHandlers['onMediaDeviceListChanged'];
	
	        if(eventHandler)
	        {
	            eventHandler();
	        }
	    }
	
	    function onRingtoneListChanged(content)
	    {
	        var ringtoneNames = content.ringtones;
	        this.ringtoneList = [];
	
	        for(var index=0; index<ringtoneNames.length; index++)
	        {
	            var ringtone = new cwic.Ringtone(ringtoneNames[index].name);
	            this.ringtoneList.push(ringtone);
	        }
	
	        var eventHandler = m_EventHandlers['onRingtoneListChanged'];
	        if(eventHandler)
	        {
	            eventHandler();
	        }
	    }
	
	    function onRingtoneChanged(content)
	    {
	        var ringtone = new cwic.Ringtone(content.ringtone);
	        var eventHandler = m_EventHandlers['onRingtoneChanged'];
	        if(eventHandler)
	        {
	            eventHandler(ringtone);
	        }
	    }
	
	    function onMonitorListChanged(content)
	    {
	        var monitorList = content.monitors;
	        var monitors = [];
	
	        for(var index=0; index < monitorList.length; ++index)
	        {
	            var monitor = new cwic.Monitor(monitorList[index]);
	            monitors.push(monitor);
	        }
	
	        var eventHandler = m_EventHandlers['onMonitorListChanged'];
	
	        if(eventHandler)
	        {
	            eventHandler(monitors);
	        }
	    }
	
	    /**
	     * @memberof MultimediaController
	     * @method refreshMediaDeviceList
	     * @description
	     * Refresh the list of available media devices:
	     *  - Speakers
	     *  - Microphones
	     *  - Ringers
	     *  - Cameras
	     *
	     * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	     * @since 11.7.0
	     */
	    this.refreshMediaDeviceList = function(errorHandler)
	    {
	        var messageName    = "getMultimediaDevices";
	        var messageData    = {};
	        var successHandler = onMediaDeviceListChanged.bind(this);
	
	        cwic.MessageSender.sendMessage(messageName, messageData, errorHandler, successHandler);
	    };
	
	    /**
	     * @memberof MultimediaController
	     * @method refreshRingtoneList
	     * @description
	     * Refresh the list of available ringtones.
	     *
	     * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	     * @since 11.7.0
	     */
	    this.refreshRingtoneList = function(errorHandler)
	    {
	        var messageType = "getAvailableRingtones";
	        var messageData = {};
	        var successHandler = onRingtoneListChanged.bind(this);
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler, successHandler);
	    };
	
	    /**
	     * @memberof MultimediaController
	     * @method refreshMonitorList
	     * @description
	     * Refresh the list of connected monitors.
	     *
	     * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	     * @since 11.7.0
	     */
	    this.refreshMonitorList = function(errorHandler)
	    {
	        var messageType    = "getMonitorList";
	        var messageData    = {};
	        var successHandler = onMonitorListChanged.bind(this);
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler, successHandler);
	    };
	
	    /**
	     * @memberof MultimediaController
	     * @method addEventHandler
	     * @description
	     * Add handler function for multimedia controller's events.
	     *
	     * @param eventName {String} - Name of the event.
	     * @param eventHandler {Function} - Function that will be called when event is fired.
	     *
	     * @since 11.7.0
	     */
	    this.addEventHandler = function(eventName, eventHandler)
	    {
	        m_EventHandlers[eventName] = eventHandler;
	    };
	
	    /**
	     * @memberof MultimediaController
	     * @method removeEventHandler
	     * @description
	     * Remove handler function for multimedia controller's event.
	     *
	     * @param eventName {String} - Name of the event.
	     *
	     * @since 11.7.0
	     */
	    this.removeEventHandler = function(eventName)
	    {
	        delete m_EventHandlers[eventName];
	    };
	}
	
	/**
	 * @memberof MultimediaController
	 * @member speakerList
	 * @description
	 * List of available speakers.
	 *
	 * @type {Speaker[]}
	 * @since 11.7.0
	 */
	MultimediaController.prototype.speakerList = [];
	
	/**
	 * @memberof MultimediaController
	 * @member ringerList
	 * @description
	 * List of available ringers.
	 *
	 * @type {Ringer[]}
	 * @since 11.7.0
	 */
	MultimediaController.prototype.ringerList = [];
	
	/**
	 * @memberof MultimediaController
	 * @member microphoneList
	 * @description
	 * List of available microphones.
	 *
	 * @type {Microphone[]}
	 * @since 11.7.0
	 */
	MultimediaController.prototype.microphoneList = [];
	
	/**
	 * @memberof MultimediaController
	 * @member cameraList
	 * @description
	 * List of available cameras.
	 *
	 * @type {Camera[]}
	 * @since 11.7.0
	 */
	MultimediaController.prototype.cameraList = [];
	
	/**
	 * @memberof MultimediaController
	 * @member ringtoneList
	 * @description
	 * List of available ringtones.
	 *
	 * @type {Array}
	 * @since 11.7.0
	 */
	MultimediaController.prototype.ringtoneList = [];
	
	/**
	 * @memberof MultimediaController
	 * @method selectSpeaker
	 * @description
	 * Select new active speaker.
	 *
	 * @param speaker {Speaker} - Speaker that will be set as active.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if speaker is not instance of {@link Speaker}.
	 *
	 * @since 11.7.0
	 */
	MultimediaController.prototype.selectSpeaker = function(speaker, errorHandler)
	{
	    if(!(speaker instanceof cwic.Speaker))
	    {
	        throw Error("Invalid Object");
	    }
	
	    var messageType = 'setPlayoutDevice';
	    var messageData = {
	        'clientPlayoutID' : speaker.deviceID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method selectMicrophone
	 * @description
	 * Select new active microphone.
	 *
	 * @param microphone {Microphone} - Microphone that will be set as active.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if microphone is not instance of {@link Microphone}.
	 *
	 * @since 11.7.0
	 */
	MultimediaController.prototype.selectMicrophone = function(microphone, errorHandler)
	{
	    if(!(microphone instanceof cwic.Microphone))
	    {
	        throw Error("Invalid Object");
	    }
	
	    var messageType = "setRecordingDevice";
	    var messageData = {
	        'clientRecordingID' : microphone.deviceID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method selectCamera
	 * @description
	 * Select new active camera.
	 *
	 * @param camera {Camera} - Camera that will be set as active.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if camera is not instance of {@link Camera}.
	 *
	 * @since 11.7.0
	 */
	MultimediaController.prototype.selectCamera = function(camera, errorHandler)
	{
	    if(!(camera instanceof cwic.Camera))
	    {
	        throw Error("Invalid Object");
	    }
	
	    var messageType = "setCaptureDevice";
	    var messageData = {
	        'clientCaptureID' : camera.deviceID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method selectRinger
	 * @description
	 * Select new active camera.
	 *
	 * @param ringer {Ringer} - Ringer that will be set as active.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if ringer is not instance of {@link Ringer}.
	 *
	 * @since 11.7.0
	 */
	MultimediaController.prototype.selectRinger = function(ringer, errorHandler)
	{
	    if(!(ringer instanceof cwic.Ringer))
	    {
	        throw Error("Invalid Object")
	    }
	
	    var messageType = "setRingerDevice";
	    var messageData = {
	        'clientRingerID' : ringer.deviceID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method playRingtoneOnAllRingers
	 * @description
	 * On incoming call, ringtone will be played on all ringer devices. Calling this method will ignore any ringer device
	 * previously set as active. Once a new ringer device is set as active ringtone will then only play on active ringer
	 * device.
	 *
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 */
	MultimediaController.prototype.playRingtoneOnAllRingers = function(errorHandler)
	{
	    var messageType = "setPlayRingerOnAllDevices";
	    var messageData = {};
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method selectRingtone
	 * @description
	 * Select new active ringtone for incoming calls.
	 *
	 * @param ringtone {Ringtone} - Ringtone that will be set as active.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if ringtone is not instance of {@link Ringtone}.
	 *
	 * @since 11.7.0
	 */
	MultimediaController.prototype.selectRingtone = function (ringtone, errorHandler)
	{
	    if (!(ringtone instanceof cwic.Ringtone))
	    {
	        throw Error("Invalid Object");
	    }
	
	    var messageType = "setCurrentRingtone";
	    var messageData = {
	        'ringtone' : ringtone.name
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method selectMonitor
	 * @description
	 * Select new active monitor that will be used for screen sharing.
	 *
	 * @param monitor {Monitor} - Monitor that will be set as active.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if monitor is not instance of {@link Monitor}.
	 *
	 * @since 11.7.0
	 */
	MultimediaController.prototype.selectMonitor = function(monitor, errorHandler)
	{
	    if(!(monitor instanceof cwic.Monitor))
	    {
	        throw Error("Invalid Object");
	    }
	
	    var messageType = "selectMonitor";
	    var messageData = {
	        monitorID : monitor.id
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method highlightMonitor
	 * @description
	 * Highlights monitor by displaying border around its edges.
	 *
	 * @param monitor {Monitor} - Monitor that will be highlighted.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if monitor is not instance of {@link Monitor}.
	 *
	 * @since 11.7.0
	 */
	MultimediaController.prototype.highlightMonitor = function (monitor, errorHandler)
	{
	    if(!(monitor instanceof cwic.Monitor))
	    {
	        throw Error("Invalid Object");
	    }
	
	    var messageType = "showMonitorBorderIndicator";
	    var messageData = {
	        monitorID : monitor.id
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method unHighlightMonitor
	 * @description
	 * UnHighlights current highlighted monitor by hiding border around its edges.
	 *
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	MultimediaController.prototype.unHighlightMonitor = function(errorHandler)
	{
	    var messageType = "hideMonitorBorderIndicator";
	    var messageData = {};
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof MultimediaController
	 * @method setMonitorHighlightColor
	 * @description
	 * Set new monitor highlight color.
	 *
	 * @param red - Red color value.
	 * @param green - Green color value.
	 * @param blue - blue color value.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	MultimediaController.prototype.setMonitorHighlightColor = function(red, green, blue, errorHandler)
	{
	    var messageType = "setMonitorBorderIndicatorColor";
	    var messageData = {
	        redValue : red,
	        greenValue : green,
	        blueValue : blue
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	
	
	module.exports.MultimediaController = new MultimediaController();

/***/ },
/* 101 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageSenderModule = __webpack_require__(72);
	
	// Multimedia Modules
	var DeviceModule = __webpack_require__(102);
	
	var cwic = {
	    MessageSender : MessageSenderModule.MessageSender,
	    MediaDevice : DeviceModule.MediaDevice
	};
	
	/**
	 * @class Speaker
	 * @classdesc Speaker device plugged into PC.
	 * @extends {MediaDevice}
	 * @constructor
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function Speaker(genericDevice)
	{
	    var deviceInfo = {
	        deviceID : genericDevice.clientPlayoutID,
	        hardwareID : genericDevice.hardwareID,
	        isSelected : genericDevice.isSelectedPlayoutDevice,
	        name : genericDevice.playoutName,
	        vendorID : genericDevice.vendorID
	    };
	
	    cwic.MediaDevice.call(this, deviceInfo);
	
	    this.volume = genericDevice.volume;
	}
	
	Speaker.prototype = Object.create(cwic.MediaDevice.prototype);
	Speaker.prototype.constructor = Speaker;
	
	/**
	 * @memberof Speaker
	 * @member volume
	 * @description
	 * Speaker's output volume level.
	 *
	 * @type {number}
	 * @since 11.7.0
	 */
	Speaker.prototype.volume = 0;
	
	/**
	 * @memberof Speaker
	 * @method setVolume
	 * @description
	 * Sets a new output volume level of the ringer.
	 *
	 * @param volume {Number} - New volume level that will be set. Allowed values are 0 - 100.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 *
	 * @throw Invalid Volume Value - Thrown if volume level value is less then 0 or more then 100.
	 */
	Speaker.prototype.setVolume = function(volume, errorHandler)
	{
	    if(volume < 0 || volume > 100)
	    {
	        throw Error("Invalid Volume Value");
	    }
	
	    this.volume = volume;
	
	    var messageType = "setCurrentSpeakerVolume";
	    var messageContent = {
	        volume : volume
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	module.exports.Speaker = Speaker;
	
	


/***/ },
/* 102 */
/***/ function(module, exports) {

	/**
	 * @class MediaDevice
	 *
	 * @description
	 * This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function MediaDevice(deviceInfo)
	{
	    this.deviceID   = deviceInfo.deviceID;
	    this.hardwareID = deviceInfo.hardwareID;
	    this.isSelected = deviceInfo.isSelected;
	    this.name       = deviceInfo.name;
	    this.vendorID   = deviceInfo.vendorID;
	}
	
	/**
	 * @memberof MediaDevice
	 * @memeber deviceID
	 * @description
	 * Unique device ID given by operating system.
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	MediaDevice.prototype.deviceID = null;
	
	/**
	 * @memberof MediaDevice
	 * @memeber hardwareID
	 * @description
	 * Unique hardwareID of device.
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	MediaDevice.prototype.hardwareID = null;
	
	/**
	 * @memberof MediaDevice
	 * @memeber isSelected
	 * @description
	 * Indicates whether this device is selected or not.
	 *
	 * @type {Boolean}
	 * @since 11.7.0
	 */
	MediaDevice.prototype.isSelected = null;
	
	/**
	 * @memberof MediaDevice
	 * @memeber name
	 * @description
	 * Name of the device.
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	MediaDevice.prototype.name = null;
	
	/**
	 * @memberof MediaDevice
	 * @memeber vendorID
	 * @description
	 * Unique ID for device given by vendor.
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	MediaDevice.prototype.vendorID = null;
	
	module.exports.MediaDevice = MediaDevice;


/***/ },
/* 103 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageSenderModule = __webpack_require__(72);
	
	// Multimedia Modules
	var DeviceModule = __webpack_require__(102);
	
	var cwic = {
	    MessageSender : MessageSenderModule.MessageSender,
	    MediaDevice : DeviceModule.MediaDevice
	};
	
	/**
	 * @class Microphone
	 * @classdesc Microphone device plugged into PC.
	 * @extends {MediaDevice}
	 * @constructor
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function Microphone(genericDevice)
	{
	    var deviceInfo = {
	        deviceID : genericDevice.recordingID,
	        hardwareID : genericDevice.hardwareID,
	        isSelected : genericDevice.isSelectedRecordingDevice,
	        name : genericDevice.recordingName,
	        vendorID : genericDevice.vendorID
	    };
	
	    cwic.MediaDevice.call(this, deviceInfo);
	
	    this.volume = genericDevice.volume;
	}
	
	Microphone.prototype = Object.create(cwic.MediaDevice.prototype);
	Microphone.prototype.constructor = Microphone;
	
	/**
	 * @memberof Microphone
	 * @member volume
	 * @description
	 * Microphone's input volume level.
	 *
	 * @type {number}
	 * @since 11.7.0
	 */
	Microphone.prototype.volume = 0;
	
	/**
	 * @memberof Microphone
	 * @method setVolume
	 * @description
	 * Sets a new input volume level of the microphone.
	 *
	 * @param volume {Number} - New volume level that will be set. Allowed values are 0 - 100.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Volume Value - Thrown if volume level value is less then 0 or more then 100.
	 *
	 * @since 11.7.0
	 */
	Microphone.prototype.setVolume = function(volume, errorHandler)
	{
	    if(volume < 0 || volume > 100)
	    {
	        throw Error("Invalid Volume Value");
	    }
	
	    this.volume = volume;
	
	    var messageType = "setCurrentMicrophoneVolume";
	    var messageContent = {
	        volume : volume
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	
	module.exports.Microphone = Microphone;


/***/ },
/* 104 */
/***/ function(module, exports, __webpack_require__) {

	var DeviceModule = __webpack_require__(102);
	
	var cwic = {
	    MediaDevice : DeviceModule.MediaDevice
	};
	
	/**
	 * @class Camera
	 * @classdesc Camera device plugged into PC.
	 * @extends {MediaDevice}
	 * @constructor
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function Camera(genericDevice)
	{
	    var deviceInfo = {
	        deviceID : genericDevice.captureID,
	        hardwareID : genericDevice.hardwareID,
	        isSelected : genericDevice.isSelectedCaptureDevice,
	        name : genericDevice.captureName,
	        vendorID : genericDevice.vendorID
	    };
	
	    cwic.MediaDevice.call(this, deviceInfo);
	}
	
	Camera.prototype = Object.create(cwic.MediaDevice.prototype);
	Camera.prototype.constructor = Camera;
	
	module.exports.Camera = Camera;


/***/ },
/* 105 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageSenderModule = __webpack_require__(72);
	
	// Multimedia Modules
	var DeviceModule = __webpack_require__(102);
	
	var cwic = {
	    MessageSender : MessageSenderModule.MessageSender,
	    MediaDevice : DeviceModule.MediaDevice
	};
	
	/**
	 * @class Ringer
	 * @classdesc Ringer device plugged into PC. Any speaker device can also be a ringer device.
	 * @extends {MediaDevice}
	 * @constructor
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function Ringer(genericDevice)
	{
	    var deviceInfo = {
	        deviceID : genericDevice.ringerID,
	        hardwareID : genericDevice.hardwareID,
	        isSelected : genericDevice.isSelectedRingerDevice,
	        name : genericDevice.ringerName,
	        vendorID : genericDevice.vendorID
	    };
	
	    cwic.MediaDevice.call(this, deviceInfo);
	
	    this.volume = genericDevice.volume;
	}
	
	Ringer.prototype = Object.create(cwic.MediaDevice.prototype);
	Ringer.prototype.constructor = Ringer;
	
	/**
	 * @memberof Ringer
	 * @member volume
	 * @description
	 * Ringer's output volume level.
	 *
	 * @type {number}
	 * @since 11.7.0
	 */
	Ringer.prototype.volume = 0;
	
	/**
	 * @memberof Ringer
	 * @method setVolume
	 * @description
	 * Sets a new output volume level of the ringer.
	 *
	 * @param volume {Number} - New volume level that will be set. Allowed values are 0 - 100.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Volume Value - Thrown if volume level value is less then 0 or more then 100.
	 *
	 * @since 11.7.0
	 */
	Ringer.prototype.setVolume = function(volume, errorHandler)
	{
	    if(volume < 0 || volume > 100)
	    {
	        throw Error("Invalid Volume Value");
	    }
	
	    this.volume = volume;
	
	    var messageType = "setCurrentRingerVolume";
	    var messageContent = {
	        volume : volume
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	module.exports.Ringer = Ringer;

/***/ },
/* 106 */
/***/ function(module, exports) {

	// Required Modules
	
	/**
	 * @class Ringtone
	 * @classdesc Sound that will be played on incoming telephony conversation.
	 * @constructor
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function Ringtone(name)
	{
	    this.name = name;
	}
	
	/**
	 * @memberof Ringtone
	 * @member name
	 * @description Name of the ringtone.
	 * @type {string}
	 * @since 11.7.0
	 */
	Ringtone.prototype.name = "unknown";
	
	module.exports.Ringtone = Ringtone;

/***/ },
/* 107 */
/***/ function(module, exports) {

	/**
	 * @class Monitor
	 * @classdesc
	 * Monitor currently connected to PC.
	 * @constructor
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function Monitor(monitorInfo)
	{
	    this.id = monitorInfo.ID;
	    this.name = monitorInfo.name;
	}
	
	/**
	 * @memberof Monitor
	 * @meber id
	 * @description
	 * Unique monitor ID.
	 * @type {string}
	 * @since 11.7.0
	 */
	Monitor.prototype.id = "Unknown";
	
	/**
	 * @memberof Monitor
	 * @member name
	 * @description
	 * Name of the monitor.
	 * @type {string}
	 * @since 11.7.0
	 */
	Monitor.prototype.name = "Unknown";
	
	module.exports.Monitor = Monitor;


/***/ },
/* 108 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule   = __webpack_require__(72);
	
	// Telephony Modules.
	var TelephonyControllerCapabilitiesModule = __webpack_require__(109);
	var TelephonyConnectionStateModule = __webpack_require__(110);
	var TelephonyConversationModule = __webpack_require__(111);
	var TelephonyDeviceModule       = __webpack_require__(114);
	var SoftPhoneModule             = __webpack_require__(115);
	var DeskPhoneModule             = __webpack_require__(116);
	var RemotePhoneModule           = __webpack_require__(117);
	
	// Utils Modules
	var LoggerModule = __webpack_require__(69);
	
	var cwic = {
	    TelephonyControllerCapabilities : TelephonyControllerCapabilitiesModule.TelephonyControllerCapabilities,
	    TelephonyConnectionStateMap : TelephonyConnectionStateModule.TelephonyConnectionStateMap,
	    TelephonyConversation : TelephonyConversationModule.TelephonyConversation,
	    TelephonyDevice       : TelephonyDeviceModule.TelephonyDevice,
	    SoftPhone             : SoftPhoneModule.SoftPhone,
	    DeskPhone             : DeskPhoneModule.Deskphone,
	    RemotePhone           : RemotePhoneModule.RemotePhone,
	
	    MessageSender         : MessageSenderModule.MessageSender,
	    MessageReceiver       : MessageReceiverModule.MessageReceiver,
	    Logger                : LoggerModule.Logger
	};
	
	/**
	 * @class TelephonyController
	 * @constructor
	 */
	function TelephonyController()
	{
	    var m_EventHandlers = {};
	
	    cwic.MessageReceiver.addMessageHandler('telephonydeviceschange', onTelephonyDeviceListChanged.bind(this));
	    cwic.MessageReceiver.addMessageHandler('connectionstatuschange', onConnectionStateChanged.bind(this));
	    cwic.MessageReceiver.addMessageHandler('connectionfailure', onConnectionFailure.bind(this));
	    cwic.MessageReceiver.addMessageHandler('conversationCallStateChanged', onConversationCallStateChanged.bind(this));
	    cwic.MessageReceiver.addMessageHandler('conversationStateChanged', onConversationStateChanged.bind(this));
	    cwic.MessageReceiver.addMessageHandler('audioCallPickupNotification', onAudioCallPickupNotification.bind(this));
	    cwic.MessageReceiver.addMessageHandler('visualCallPickupNotification', onVisualCallPickupNotification.bind(this));
	    cwic.MessageReceiver.addMessageHandler('callPickupError', onCallPickupError.bind(this));
	    cwic.MessageReceiver.addMessageHandler('telephonyservicechange', onCapabilitiesChanged.bind(this));
	
	    function onTelephonyDeviceListChanged(content)
	    {
	        var index;
	        var telephonyDevices = content.devices;
	
	        this.telephonyDevices.length = 0;
	
	        for(index = 0; index < telephonyDevices.length; ++index)
	        {
	            var device = telephonyDevices[index];
	            var controlMode = device.controlMode;
	
	            switch(controlMode)
	            {
	                case "Deskphone":
	                {
	                    var deskPhone = new cwic.DeskPhone(telephonyDevices[index]);
	                    this.telephonyDevices.push(deskPhone);
	                    break;
	                }
	                case "Softphone":
	                {
	                    var softPhone = new cwic.SoftPhone(telephonyDevices[index]);
	                    this.telephonyDevices.push(softPhone);
	                    break;
	                }
	                case "ExtendConnect":
	                {
	                    var remotePhone = new cwic.RemotePhone(telephonyDevices[index]);
	                    this.telephonyDevices.push(remotePhone);
	                    break;
	                }
	            }
	
	            var eventHandler = m_EventHandlers['onTelephonyDeviceListChanged'];
	            if(eventHandler)
	            {
	                eventHandler();
	            }
	        }
	    }
	
	    function onConnectionStateChanged(state)
	    {
	        this.connectionState = cwic.TelephonyConnectionStateMap[state];
	
	        var eventHandler = m_EventHandlers['onConnectionStateChanged'];
	        if(eventHandler)
	        {
	            eventHandler(this.connectionState);
	        }
	    }
	
	    function onConnectionFailure(content)
	    {
	        var connectionFailure = content;
	        cwic.Logger.error("Failed to connect with CUCM or connection has been broken. Reason: " + connectionFailure);
	
	        var eventHandler = m_EventHandlers['onConnectionFailure'];
	        if(eventHandler)
	        {
	            eventHandler(connectionFailure);
	        }
	    }
	
	    function onConversationStateChanged(content)
	    {
	        var eventHandler = m_EventHandlers['onConversationUpdated'];
	
	        if(eventHandler)
	        {
	            var telephonyConversation = new cwic.TelephonyConversation(content);
	            eventHandler(telephonyConversation);
	        }
	    }
	
	    function onConversationCallStateChanged(content)
	    {
	        var telephonyConversation = new cwic.TelephonyConversation(content);
	        var eventHandler;
	
	        switch(telephonyConversation.callState)
	        {
	            case 'OffHook':
	                eventHandler = m_EventHandlers['onConversationOutgoing'];
	                break;
	            case 'Ringin':
	                eventHandler = m_EventHandlers['onConversationIncoming'];
	                break;
	            case 'OnHook':
	                eventHandler = m_EventHandlers['onConversationEnded'];
	                break;
	            case 'Connected':
	                eventHandler = m_EventHandlers['onConversationStarted'];
	                break;
	            default:
	                eventHandler = m_EventHandlers['onConversationUpdated'];
	        }
	
	        if(eventHandler)
	        {
	            eventHandler(telephonyConversation);
	        }
	    }
	
	    function onVisualCallPickupNotification(content)
	    {
	        var notificationInfo = content.notificationInfo;
	
	        var eventHandler = m_EventHandlers['onVisualCallPickupNotification'];
	
	        if(eventHandler)
	        {
	            eventHandler(notificationInfo);
	        }
	    }
	
	    function onAudioCallPickupNotification(content)
	    {
	        var isAudioEnabled = content.isAudioEnabled;
	
	        var eventHandler = m_EventHandlers['onAudioCallPickupNotification'];
	
	        if(eventHandler)
	        {
	            eventHandler(isAudioEnabled);
	        }
	    }
	
	    function onCallPickupError(content)
	    {
	
	    }
	
	    function onCapabilitiesChanged(content)
	    {
	        var telephonyCapabilities = new cwic.TelephonyControllerCapabilities(content);
	        this.capabilities = telephonyCapabilities;
	
	        this.recentGroupCallPickupNumbers = content.recentGroupCallPickupNumbers ? content.recentGroupCallPickupNumbers : [];
	    }
	
	    /**
	     * @memberof TelephonyController
	     * @method addEventHandler
	     * @description Add handler function for Telephony Controller's event.
	     *
	     * @param eventName {String} - Name of the event.
	     * @param handler {Function} - Function that will be called when event is fired.
	     *
	     * @since 11.7.0
	     */
	    this.addEventHandler = function(eventName, handler)
	    {
	        m_EventHandlers[eventName] = handler;
	    };
	
	    /**
	     * @memberof TelephonyController
	     * @method removeEventHandler
	     *
	     * @description Remove handler function for TelephonyController's event.
	     *
	     * @param eventName {String} - Name of the event.
	     *
	     * @since 11.7.0
	     */
	    this.removeEventHandler = function(eventName)
	    {
	        delete m_EventHandlers[eventName];
	    };
	
	    /**
	     * @memberof TelephonyController
	     * @method refreshTelephonyDeviceList
	     *
	     * @description Send request to client to refresh telephony device list.
	     *
	     * @since 11.7.0
	     */
	    this.refreshTelephonyDeviceList = function()
	    {
	        var messageType    = "getAvailableDevices";
	        var messageData    = {};
	        var errorHandler   = null;
	        var successHandler = onTelephonyDeviceListChanged.bind(this);
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler, successHandler);
	    };
	}
	
	/**
	 * @memberof TelephonyController
	 * @property telephonyDevices
	 *
	 * @description
	 * List of available telephony devices.
	 * @type {TelephonyDevice[]}
	 * @since 11.7.0
	 */
	TelephonyController.prototype.telephonyDevices = [];
	
	/**
	 * @memberof TelephonyController
	 * @member connectionState
	 *
	 * @description
	 * Current connection state with CUCM.
	 * @type {TelephonyConnectionState}
	 * @since 11.7.0
	 */
	TelephonyController.prototype.connectionState = "Disconnected";
	
	/**
	 * @memberof TelephonyController
	 * @member capabilities
	 * @description
	 * List of telephony controller capabilities.
	 * @type {TelephonyControllerCapabilities}
	 * @since 11.7.0
	 */
	TelephonyController.prototype.capabilities = null;
	
	/**
	 * @memberof TelephonyController
	 * @member recentGroupCallPickupNumbers
	 * @description
	 * List of recently dialed group pickup numbers
	 * @type {String[]}
	 * @since 11.7.0
	 */
	TelephonyController.prototype.recentGroupCallPickupNumbers = [];
	
	/**
	 * @memberof TelephonyController
	 * @method TelephonyController#getConnectedTelephonyDevice
	 * @description Retrieves telephony device that is currently connected with CUCM.
	 *
	 * @returns {TelephonyDevice} Currently connected telephony device.
	 * @since 11.7.0
	 */
	TelephonyController.prototype.getConnectedTelephonyDevice = function()
	{
	    var telephonyDevice = null;
	
	    for(var index in this.telephonyDevices)
	    {
	        if(this.telephonyDevices[index].isSelected)
	        {
	            telephonyDevice = this.telephonyDevices[index];
	            break;
	        }
	    }
	    return telephonyDevice;
	};
	
	/**
	 * @memberof TelephonyController
	 * @method startAudioConversation
	 *
	 * @description
	 * Starts new audio conversation. Before calling this function it is necessary to add 'onConversationStarted' event
	 * handler through addEventHandler in order to receive TelephonyConversation object.
	 *
	 * @param number {String} - Number that will be called to start a conversation.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyController.prototype.startAudioConversation = function(number, errorHandler)
	{
	    var messageType = "originate";
	    var messageData = {
	        recipient : number,
	        videoDirection : "RecvOnly"
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof TelephonyController
	 * @method startVideoConversation
	 *
	 * @description
	 * Starts new video conversation. Before calling this function it is necessary add to 'onConversationStarted' event
	 * handler through addEventHandler in order to receive TelephonyConversation object.
	 *
	 * @param number {String} - Number that will be called to start a conversation.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyController.prototype.startVideoConversation = function(number, errorHandler)
	{
	    var messageType = "originate";
	    var messageData = {
	        recipient : number,
	        videoDirection : "sendRecv"
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof TelephonyController
	 * @method callPickup
	 * @description
	 * Pick up incoming call in call pickup group in which currently connected telephony device's line is associated with.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyController.prototype.callPickup = function(errorHandler)
	{
	    var messageType = "callPickup";
	    var messageData = {};
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof TelephonyController
	 * @method groupCallPickup
	 * @description
	 * Can be used for both direct call pickup and group call pickup. Group call pickup, picks up incoming call in another
	 * call pickup group. Group number is specified passed as parameter. Direct call pickup, picks up incoming call on the
	 * specified directory number. Directory number is specified as parameter.
	 *
	 * @param number - Group number if used for group call pickup. Directory number if used for direct call pick up.
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyController.prototype.groupCallPickup = function(number, errorHandler)
	{
	    var messageType = "groupCallPickup";
	    var messageData = {
	        pickupNumber: number
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof TelephonyController
	 * @method otherGroupPickup
	 * @description
	 * Pick up incoming call, in a group that is associated with group in which currently connected telephony device's line
	 * is associated with.
	 *
	 * @param [errorHandler] - Called if error has occurred in add-on, passes {@link SystemError}.
	 * @since 11.7.0
	 */
	TelephonyController.prototype.otherGroupPickup = function(errorHandler)
	{
	    var messageType = "otherGroupPickup";
	    var messageData = {};
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	module.exports.TelephonyController = new TelephonyController();

/***/ },
/* 109 */
/***/ function(module, exports) {

	/**
	 * @class TelephonyControllerCapabilities
	 * @classdesc
	 * List of telephony capabilities.
	 *
	 * @description This class cannot be instantiated.
	 * @since 11.7.0
	 */
	function TelephonyControllerCapabilities(capabilites)
	{
	    this.isHuntGroupEnabled = capabilites.isHuntGroupEnabled;
	    this.isCallPickupEnabled = capabilites.isCallPickupEnabled;
	    this.isGroupCallPickupEnabled = capabilites.isGroupCallPickupEnabled;
	    this.isOtherGroupPickupEnabled = capabilites.isOtherGroupPickupEnabled;
	}
	
	/**
	 * @memberof TelephonyControllerCapabilities
	 * @member isHuntGroupEnabled
	 * @description Indicates whether hunt group feature is enabled or not.
	 * @type {boolean}
	 *
	 * @since 11.7.0
	 */
	TelephonyControllerCapabilities.prototype.isHuntGroupEnabled = false;
	
	/**
	 * @memberof TelephonyControllerCapabilities
	 * @member isCallPickupEnabled
	 * @description Indicates whether call pickup feature is enabled or not.
	 * @type {boolean}
	 *
	 * @since 11.7.0
	 */
	TelephonyControllerCapabilities.prototype.isCallPickupEnabled = false;
	
	/**
	 * @memberof TelephonyControllerCapabilities
	 * @member isGroupCallPickupEnabled
	 * @description Indicates whether group call pickup feature is enabled or not.
	 * @type {boolean}
	 *
	 * @since 11.7.0
	 */
	TelephonyControllerCapabilities.prototype.isGroupCallPickupEnabled = false;
	
	/**
	 * @memberof TelephonyControllerCapabilities
	 * @member isOtherGroupPickupEnabled
	 * @description Indicates whether other group pickup feature is enabled or not.
	 * @type {boolean}
	 *
	 * @since 11.7.0
	 */
	TelephonyControllerCapabilities.prototype.isOtherGroupPickupEnabled = false;
	
	module.exports.TelephonyControllerCapabilities = TelephonyControllerCapabilities;

/***/ },
/* 110 */
/***/ function(module, exports) {

	/**
	 * @enum TelephonyConnectionState {Enum}
	 * @description
	 * Represents connection state between CUCM and telephony device.
	 *
	 * @since 11.7.0
	 */
	var TelephonyConnectionState = {
	    /**
	     * Connection is established with CUCM.
	     * @type {String}
	     */
	    Connected : "Connected",
	
	    /**
	     * Establishing connection with CUCM.
	     * @type {String}
	     */
	    Connecting : "Connecting",
	
	    /**
	     * Disconnected from CUCM.
	     * @type {String}
	     */
	    Disconnected : "Disconnected"
	};
	
	var TelephonyConnectionStateMap = {
	    "eReady"                : "Connected",
	    "eIdle"                 : "Disconnected",
	    "eRegistering"          : "Connecting",
	    "eConnectionTerminated" : "Disconnected"
	};
	
	module.exports.TelephonyConnectionStateMap = TelephonyConnectionStateMap;

/***/ },
/* 111 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule   = __webpack_require__(72);
	
	// TelephonyModules
	var TelephonyConversationStatesModule = __webpack_require__(112);
	var TelephonyConversationCapabilitiesModule = __webpack_require__(113);
	
	var cwic = {
	    MessageSender   : MessageSenderModule.MessageSender,
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	
	    TelephonyConversationState : TelephonyConversationStatesModule.TelephonyConversationStates,
	    TelephonyConversationCapabilities : TelephonyConversationCapabilitiesModule.TelephonyConversationCapabilities
	};
	
	/**
	 * @class TelephonyConversation
	 *
	 * @static
	 * @since 11.7.0
	 *
	 * @classdesc
	 * Telephony conversation class can be used to manipulate incoming, ongoing and outcoming conversations. It is the actual
	 * representation of one telephony call.
	 *
	 */
	function TelephonyConversation(conversation)
	{
	    this.ID = conversation.callId;
	
	    // Mapping of video direction to canStartVideo capability
	    // TODO: Do this in addon
	    var canStartVideo = {
	        Inactive : true,
	        SendRecv : false,
	        SendOnly : false,
	        RecvOnly : true
	    };
	
	    // Mapping of video direction to canStopVideo capability
	    // TODO: Do this in addon
	    var canStopVideo = {
	        Inactive : false,
	        SendRecv : true,
	        SendOnly : true,
	        RecvOnly : false
	    };
	
	    this.capabilities = new cwic.TelephonyConversationCapabilities(conversation.capabilities);
	    this.capabilities.canStartVideo = canStartVideo[conversation.videoDirection];
	    this.capabilities.canStopVideo = canStopVideo[conversation.videoDirection];
	
	    this.callState = conversation.callState;
	    this.states = new cwic.TelephonyConversationState(conversation);
	
	    // Participant objects
	    this.localParticipant = conversation.localParticipant;
	    this.maxParticipants  = conversation.maxParticipants;
	    this.participants     = conversation.participants;
	
	
	
	    this.videoDirection   = conversation.videoDirection;
	}
	
	/**
	 * @memberof TelephonyConversation
	 * @member states
	 * @description
	 * List of different active/inactive conversation states.
	 *
	 * @type {TelephonyConversationStates}
	 */
	TelephonyConversation.prototype.states = null;
	
	/**
	 * @memberof TelephonyConversation
	 * @member capabilities
	 * @description
	 * List of capable actions that telephony conversation can perform.
	 *
	 * @type {TelephonyConversationCapabilities}
	 */
	TelephonyConversation.prototype.capabilities = null;
	
	/**
	 * @memberof TelephonyConversation
	 * @member callState
	 * @description
	 * Current call state of telephony conversation.
	 *
	 * @type {String}
	 */
	TelephonyConversation.prototype.callState = null;
	
	/**
	 * @memberof TelephonyConversation
	 * @method answerAudio
	 * @description
	 * Answers incoming telephony conversation with audio.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.answerAudio = function(errorHandler)
	{
	    var messageType = "answer";
	    var messageContent = {
	        callId : this.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method answerVideo
	 * @description
	 * Answers incoming telephony conversation with video.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.answerVideo = function(errorHandler)
	{
	    var messageType = "answer";
	    var messageContent = {
	        callId : this.ID,
	        videoDirection : 'SendRecv'
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method reject
	 * @description
	 * Rejects(Immediate Divert) incoming telephony conversation.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.reject = function(errorHandler)
	{
	    var messageType = "iDivert";
	    var messageContent = {
	        callId : this.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method end
	 * @description
	 * Ends telephony conversation/
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.end = function(errorHandler)
	{
	    var messageType = "endCall";
	    var messageContent = {
	        callId : this.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method hold
	 * @description
	 * Puts telephony conversation on hold.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.hold = function(errorHandler)
	{
	    var messageType = "hold";
	    var messageContent = {
	        callId : this.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method resume
	 * @description
	 * Resumes telephony conversation that was previously been put on hold.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.resume = function(errorHandler)
	{
	    var messageType = "resume";
	    var messageContent = {
	        callId : this.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method startVideo
	 * @description
	 * Start sending video to other participants.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.startVideo = function(errorHandler)
	{
	    var messageType = "setVideoDirection";
	    var messageContent = {
	        callId: this.ID,
	        videoDirection: "SendRecv"
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method stopVideo
	 * @description
	 * Stop sending video to to other participants.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.stopVideo = function(errorHandler)
	{
	    var messageType = "setVideoDirection";
	    var messageContent = {
	        callId: this.ID,
	        videoDirection: "RecvOnly"
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method muteAudio
	 * @description
	 * Mute audio on the telephony conversation.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.muteAudio = function(errorHandler)
	{
	    var messageType = "mute";
	    var messageContent = {
	        callId : this.ID,
	        muteVideo: false,
	        muteAudio: true
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method unmuteAudio
	 * @description
	 * Unmute audio on the telephony conversation.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.unmuteAudio = function(errorHandler)
	{
	    var messageType = "unmute";
	    var messageContent = {
	        callId : this.ID,
	        unmuteAudio: true
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method muteVideo
	 * @description
	 * Mute video on the telephony conversation. When video is muted remote side will still receive video but it will
	 * only see 'frozen' frame.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.muteVideo = function(errorHandler)
	{
	    var messageType = "mute";
	    var messageContent = {
	        callId : this.ID,
	        muteVideo: true,
	        muteAudio: false
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method unmuteVideo
	 * @description
	 * Unmute video on the telephony conversation.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.unmuteVideo = function(errorHandler)
	{
	    var messageType = "unmute";
	    var messageContent = {
	        callId : this.ID,
	        unmuteVideo: true
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method transferConversation
	 * @description
	 * Initiates conversation transfer to another directory number. In order for transfer to be complete, successive call to
	 * completeTransfer() must be made.
	 *
	 * @param number {Number} - Number to witch conversation will be transferred.
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 *
	 * @see completeTransfer
	 */
	TelephonyConversation.prototype.transferConversation = function(number, errorHandler)
	{
	    var messageType = "transferCall";
	    var messageContent = {
	        callId : this.ID,
	        transferToNumber : number
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method completeTransfer
	 * @description
	 * Completes telephony conversation transfer that was previously initiated.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 *
	 * @see transferConversation
	 */
	TelephonyConversation.prototype.completeTransfer = function(errorHandler)
	{
	    var messageType = "completeTransfer";
	    var messageContent = {
	        callId : this.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method merge
	 * @description
	 * Merge two telephony conversations in conference conversation.
	 *
	 * @param conversation {TelephonyConversation} - Conversation that will be merged.
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if conversation is not instance of {@link TelephonyConversation}.
	 *
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.merge = function(conversation, errorHandler)
	{
	    if(!(conversation instanceof TelephonyConversation))
	    {
	        throw Error("Invalid Object");
	    }
	
	    var messageType = "joinCalls";
	    var messageContent = {
	        callId : this.ID,
	        joinCallId : conversation.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method startScreenShare
	 * @description
	 * Starts sharing of the screen. If this function is called and no screen has been selected for sharing through
	 * MultimediaController.selectMonitor(), default screen will be selected for sharing. Defaulted screen is the first
	 * screen retrieved when monitors are being enumerated during plugin initialization or the previously set monitor
	 * through MultimediaController.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 *
	 * @see MultimedaiController
	 */
	TelephonyConversation.prototype.startScreenShare = function(errorHandler)
	{
	    var messageType = "requestDesktopShare";
	    var messageContent = {
	        callId: this.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method stopScreenShare
	 * @description
	 * Stops sharing of the screen.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.stopScreenShare = function(errorHandler)
	{
	    var messageType = "releaseDesktopShare";
	    var messageContent = {
	        callId: this.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method sendDTMF
	 * @description
	 * Send one or more DTMF characters.
	 *
	 * @param dtmfDigit{String} - DTMF characters that will be sent.
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.sendDTMF = function(dtmfDigit, errorHandler)
	{
	    var messageType = "sendDTMF";
	    var messageContent = {
	        callId: this.ID,
	        digit: dtmfDigit
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method startRemoteCameraAction
	 * @description
	 * Starts remote camera action.
	 * Once this method is called remote camera will continuously perform specified action. To stop specified
	 * action {@link TelephonyConversation.stopRemoteCameraAction} needs to be called.
	 *
	 * @param action{String} - Action to will be started. For supported camera actions please see {@link RemoteCameraAction}
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.startRemoteCameraAction = function (action, errorHandler)
	{
	    var messageType = "startFECCAction";
	    var messageContent = {
	        callId: this.ID,
	        action: action
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent,errorHandler);
	};
	
	/**
	 * @memberof TelephonyConversation
	 * @method stopRemoteCameraAction
	 * @description
	 * Stops remote camera action previously started with {@link TelephonyConversation.startRemoteCameraAction}.
	 *
	 * @param action{String} - Action to will be stopped. For supported camera actions please see {@link RemoteCameraAction}
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyConversation.prototype.stopRemoteCameraAction = function (action, errorHandler)
	{
	    var messageType = "stopFECCAction";
	    var messageContent = {
	        callId: this.ID,
	        action: action
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageContent, errorHandler);
	};
	
	exports.TelephonyConversation = TelephonyConversation;

/***/ },
/* 112 */
/***/ function(module, exports) {

	/**
	 * @class TelephonyConversationStates
	 * @static
	 *
	 * @description
	 * Represents different conversation states that can be present or not.
	 *
	 * @since 11.7.0
	 */
	function TelephonyConversationStates(conversation)
	{
	    this.isAudioMuted = conversation.audioMuted;
	    this.isVideoMuted = conversation.videoMuted;
	    this.isLocalSharing = conversation.isLocalSharing;
	    this.isRemoteSharing = conversation.isRemoteSharing;
	}
	
	/**
	 * @property isAudioMuted
	 * @description
	 * Indicates if audio is muted or not.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationStates.prototype.isAudioMuted = false;
	
	/**
	 * @property isVideoMuted
	 * @description
	 * Indicates if video is muted or not.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationStates.prototype.isVideoMuted = false;
	
	/**
	 * @property isLocalSharing
	 * @description
	 * Indicates if screen is shared or not.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationStates.prototype.isLocalSharing = false;
	
	/**
	 * @property isRemoteSharing
	 * @description
	 * Indicates if screen is shared by remote participant or not.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationStates.prototype.isRemoteSharing = false;
	
	
	module.exports.TelephonyConversationStates = TelephonyConversationStates;

/***/ },
/* 113 */
/***/ function(module, exports) {

	/**
	 * @class TelephonyConversationCapabilities
	 * @static
	 * @description
	 * List of capabilities that telephony conversation can perform.
	 *
	 * @since 11.7.0
	 */
	function TelephonyConversationCapabilities(capabilities)
	{
	    this.canAnswer              = capabilities.canAnswerCall;
	    this.canEnd                 = capabilities.canEndCall;
	    this.canReject              = capabilities.canRejectCall;
	    this.canHold                = capabilities.canHold;
	    this.canResume              = capabilities.canResume;
	    this.canMerge               = capabilities.canJoinAcrossLine;
	    this.canTransfer            = capabilities.canDirectTransfer;
	    this.canMuteAudio           = capabilities.canMuteAudio;
	    this.canUnmuteAudio         = capabilities.canUnmuteAudio;
	    this.canMuteVideo           = capabilities.canMuteVideo;
	    this.canUnmuteVideo         = capabilities.canUnmuteVideo;
	    this.canStartScreenShare    = capabilities.canRequestShare;
	    this.canStopScreenShare     = capabilities.canReleaseShare;
	    this.canSendDTMF            = capabilities.canSendDigit;
	    this.canControlRemoteCamera = capabilities.canFarEndCameraControl;
	    this.canCameraTiltUp        = capabilities.canTiltUp;
	    this.canCameraTiltDown      = capabilities.canTiltDown;
	    this.canCameraPanLeft       = capabilities.canPanLeft;
	    this.canCameraPanRight      = capabilities.canPanRight;
	    this.canCameraZoomIn        = capabilities.canZoomIn;
	    this.canCameraZoomOut       = capabilities.canZoomOut;
	    this.canUpdateVideo = capabilities.canUpdateVideoCapability;
	}
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canAnswer
	 * @description
	 * If true telephony conversation can be answered;
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canAnswer = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canEnd
	 * @description
	 * If true telephony conversation can be ended.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canEnd = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canReject
	 * @description
	 * If true telephony conversation can be rejected (Immediate Divert).
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canReject = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canHold
	 * @description
	 * If true telephony conversation can be put on hold.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canHold = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canResume
	 * @description
	 * If true telephony conversation previously put on hold can be resumed.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canResume = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canMerge
	 * @description
	 * If true telephony conversation can be merged with another one and form a conference conversation.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canMerge = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canTransfer
	 * @description
	 * If true telephony conversation can be transferred to another number.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canTransfer = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canMuteAudio
	 * @description
	 * If true telephony conversation's audio can be muted.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canMuteAudio = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canUnmuteAudio
	 * @description
	 * If true telephony conversation's audio can be unmuted.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canUnmuteAudio = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canMuteVideo
	 * @description
	 * If true telephony conversation's video can be muted.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canMuteVideo = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canUnmuteVideo
	 * @description
	 * If true telephony conversation's video can be unmuted.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canUnmuteVideo = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canUpdateVideo
	 * @description
	 * If true telephony conversation's video can be started or stopped. This capability is
	 * used in combination with [canStartVideo]{@link TelephonyConversationCapabilities#canStartVideo} and
	 * [canStopVideo]{@link TelephonyConversationCapabilities#canStopVideo} capabilities
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canUpdateVideo = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canStartVideo
	 * @description
	 * If true telephony conversation's video can be started.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canStartVideo = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canStopVideo
	 * @description
	 * If true telephony conversation's video can be stopped.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canStopVideo = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canStopScreenShare
	 * @description
	 * If true telephony conversation can stop screen share.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canStopScreenShare = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canStartScreenShare
	 * @description
	 * If true telephony conversation can start screen share.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canStartScreenShare = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canSendDTMF
	 * @description
	 * If true telephony conversation can send DTMF digits.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canSendDTMF = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canControlRemoteCamera
	 * @description
	 * If true remote camera can be controlled.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canControlRemoteCamera = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canCameraTiltUp
	 * @description
	 * If true remote camera can be tilted up.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canCameraTiltUp = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canCameraTiltDown
	 * @description
	 * If true remote camera can be tilted down.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canCameraTiltDown = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canCameraPanLeft
	 * @description
	 * If true remote camera can be panned left.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canCameraPanLeft = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canCameraPanRight
	 * @description
	 * If true remote camera can be panned right.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canCameraPanRight = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canCameraZoomIn
	 * @description
	 * If true remote camera can zoom in.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canCameraZoomIn = false;
	
	/**
	 * @memberof TelephonyConversationCapabilities
	 * @member canCameraZoomOut
	 * @description
	 * If true remote camera can zoom out.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	TelephonyConversationCapabilities.prototype.canCameraZoomOut = false;
	
	module.exports.TelephonyConversationCapabilities = TelephonyConversationCapabilities;

/***/ },
/* 114 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageSenderModule = __webpack_require__(72);
	
	var cwic = {
	    MessageSender : MessageSenderModule.MessageSender
	};
	
	/**
	 * @class TelephonyDevice
	 * @classdesc
	 * Telephony Device represents abstraction of actual phone device.
	 *
	 * @description
	 * This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function TelephonyDevice(device)
	{
	    this.name                 = device.name;
	    this.controlMode          = device.controlMode;
	    this.description          = device.description;
	    this.type                 = device.modelDescription;
	    this.lineDirectoryNumbers = device.lineDNs;
	    this.activeLine           = device.activeLineNumber;
	    this.isSelected           = device.isSelected;
	    this.huntGroupState       = device.huntGroupState;
	    this.guid                 = device.guid;
	}
	
	/**
	 * @memberof TelephonyDevice
	 * @member name
	 * @description
	 * Name of telephony device. Reflects the value set on CUCM.
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.name = null;
	
	/**
	 * @memberof TelephonyDevice
	 * @member controlMode
	 * @description
	 * Mod in which telephony device is controlled: SoftPhone, DeskPhone, RemotePhone
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.controlMode = null;
	
	/**
	 * @memberof TelephonyDevice
	 * @member description
	 * @description
	 * Telephony device's description. Reflects the value set on CUCM.
	 * @type {String}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.description = null;
	
	/**
	 * @memberof TelephonyDevice
	 * @member type
	 * @description
	 * Telephony device's type. Reflects the value set on CUCM.
	 * @type {String}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.type = null;
	
	/**
	 * @memberof TelephonyDevice
	 * @member lineDirectoryNumbers
	 * @description
	 * Line directory numbers associated with this device. This property is set only when device is connected to CUCM.
	 * List of available lines reflects the ones set up on CUCM.
	 *
	 * @type {Array}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.lineDirectoryNumbers = null;
	
	/**
	 * @memberof TelephonyDevice
	 * @member activeLine
	 * @description
	 * Line that is used by telephony device when it is connected to CUCM.
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.activeLine = null;
	
	/**
	 * @memberof TelephonyDevice
	 * @member isSelected
	 * @description
	 * Tells whether telephony device is connected to CUCM or not.
	 *
	 * @type {Boolean}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.isSelected = false;
	
	/**
	 * @memberof TelephonyDevice
	 * @member huntGroupState
	 * @description
	 * Telephony device's current hunt group state.
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.huntGroupState = null;
	
	/**
	 * @memberof TelephonyDevice
	 * @member guid
	 * @description
	 * Telephony device's GUID.
	 *
	 * @type {String}
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.guid = null;
	
	/**
	 * @memberof TelephonyDevice
	 * @method connect
	 * @description
	 * Connect telephony device to CUCM.
	 *
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.connect = function(){};
	
	/**
	 * @memberof TelephonyDevice
	 * @method huntGroupLogin
	 * @description
	 * Login telephony device to hunt group.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.huntGroupLogin = function(errorHandler)
	{
	    if(this.huntGroupState === "LoggedOut")
	    {
	        var messageType = "toggleHuntGroupLogin";
	        var messageData = {
	            deviceGuid : this.guid
	        };
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    }
	};
	
	/**
	 * @memberof TelephonyDevice
	 * @method huntGroupLogout
	 * @description
	 * Logout telephony device from hunt group.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	TelephonyDevice.prototype.huntGroupLogout = function(errorHandler)
	{
	    if(this.huntGroupState === "LoggedIn")
	    {
	        var messageType = "toggleHuntGroupLogin";
	        var messageData = {
	            deviceGuid : this.guid
	        };
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    }
	};
	
	module.exports.TelephonyDevice = TelephonyDevice;


/***/ },
/* 115 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var TelephonyDeviceModule = __webpack_require__(114);
	
	// System Modules
	var MessageSenderModule = __webpack_require__(72);
	
	var cwic = {
	    TelephonyDevice : TelephonyDeviceModule.TelephonyDevice,
	    MessageSender   : MessageSenderModule.MessageSender
	};
	
	/**
	 * @class SoftPhone
	 * @extends {TelephonyDevice}
	 * @classdesc SoftPhone represents software telephony device.
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function SoftPhone(device)
	{
	    cwic.TelephonyDevice.call(this, device)
	}
	
	SoftPhone.prototype = Object.create(cwic.TelephonyDevice.prototype);
	SoftPhone.prototype.constructor = SoftPhone;
	
	
	/**
	 * @memberof SoftPhone
	 * @method connect
	 * @description
	 * Connect to and register softphone device with CUCM. If force registration is set to true, and this device is
	 * registered elsewhere, device will first be disconnected then connected again.
	 *
	 * @param isForceRegistration {Boolean} - Flag that tells whether its a force registration or not.
	 * @throw Invalid Number Of Arguments - Thrown if isForceRegistration is not specified.
	 *
	 * @since 11.7.0
	 */
	SoftPhone.prototype.connect = function(isForceRegistration)
	{
	    if(arguments.length !== 1)
	    {
	        throw Error("Invalid Number of Arguments");
	    }

	    var messageType = "connect";
	    var messageData = {
	        phoneMode: "Softphone",
	        deviceName: this.name,
	        lineDN: "",
	        forceRegistration: isForceRegistration
	    };

	    cwic.MessageSender.sendMessage(messageType, messageData);
	};
	
	// Module Exports
	module.exports.SoftPhone = SoftPhone;

/***/ },
/* 116 */
/***/ function(module, exports, __webpack_require__) {

	// Telephony Modules
	var TelephonyDeviceModule = __webpack_require__(114);
	
	// System Modules
	var MessageSenderModule = __webpack_require__(72);
	
	
	var cwic = {
	    TelephonyDevice : TelephonyDeviceModule.TelephonyDevice,
	    MessageSender   : MessageSenderModule.MessageSender
	};
	
	
	/**
	 * @class DeskPhone
	 * @extends {TelephonyDevice}
	 * @classdesc
	 * Deskphone represents physical telephony device. Along with all capabilities of telephony device it can also change
	 * active line.
	 *
	 * @description This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function DeskPhone(device)
	{
	    cwic.TelephonyDevice.call(this, device)
	}
	
	DeskPhone.prototype = Object.create(cwic.TelephonyDevice.prototype);
	DeskPhone.prototype.constructor = DeskPhone;
	
	/**
	 * @memberof DeskPhone
	 * @method selectLine
	 * @description
	 * Change active line to a new one.
	 *
	 * @param line {String} - A line that will be selected.
	 *
	 * @since 11.7.0
	 */
	DeskPhone.prototype.selectLine = function(line)
	{
	    var messageType = "connect";
	    var messageData = {
	        phoneMode: "Softphone",
	        deviceName: this.name,
	        lineDN: line,
	        forceRegistration: false
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData);
	};
	
	/**
	 * @memberof DeskPhone
	 * @method connect
	 * @description
	 * Connect to and register deskphone device with CUCM. If force registration is set to true, and this device is
	 * registered elsewhere, device will first be disconnected then connected again.
	 *
	 * @param isForceRegistration {Boolean} - Flag that tells whether its a force registration or not.
	 *
	 * @throw Invalid Number Of Arguments - Thrown if isForceRegistration is not specified.
	 *
	 * @since 11.7.0
	 */
	DeskPhone.prototype.connect = function(isForceRegistration)
	{
	    if(arguments.length !== 1)
	    {
	        throw Error("Invalid number of arguments");
	    }
	
	    var messageType = "connect";
	    var messageData = {
	        phoneMode: "Softphone",
	        deviceName: this.name,
	        lineDN: "",
	        forceRegistration: isForceRegistration
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData);
	};
	
	// Module Exports
	module.exports.Deskphone = DeskPhone;


/***/ },
/* 117 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var telephonyDeviceModule = __webpack_require__(114);
	
	// System Modules
	var MessageSenderModule = __webpack_require__(72);
	
	var cwic = {
	    TelephonyDevice     : telephonyDeviceModule.TelephonyDevice,
	    MessageSender       : MessageSenderModule.MessageSender
	};
	
	RemotePhone.prototype = Object.create(cwic.TelephonyDevice.prototype);
	RemotePhone.prototype.constructor = RemotePhone;
	
	/**
	 * @class RemotePhone
	 * @extends {TelephonyDevice}
	 * @classdesc
	 * RemotePhone represents remote device that can be connected to CUCM through "Extened&Connect" feature.
	 *
	 * @description
	 * This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function RemotePhone(device)
	{
	    cwic.TelephonyDevice.call(this, device);
	
	    var m_Number = "";
	
	    this.getNumber = function()
	    {
	        return m_Number;
	    };
	
	    this.setNumber = function(number)
	    {
	        m_Number = number;
	    };
	}
	
	/**
	 * @memberof RemotePhone
	 * @method connect
	 * @description
	 * Connect to and register deskphone device with CUCM. If force registration is set to true, and this device is
	 * registered elsewhere, device will first be disconnected then connected again.
	 *
	 * @param number (String} - Number that will be associated with this device (Represents remoteDestinationNumber on CUCM).
	 * @param isForceRegistration {Boolean} - Flag that tells whether its a force registration or not.
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Number Of Arguments - Thrown if isForceRegistration is not specified.
	 *
	 * @since 11.7.0
	 */
	RemotePhone.prototype.connect = function(number, isForceRegistration, errorHandler)
	{
	    if(arguments.length !== 2)
	    {
	        throw Error("Invalid Number of Arguments");
	    }
	
	    var messageType = "selectRemoteDestination";
	    var messageData = {
	        deviceName : this.name,
	        remoteDestinationNumber : number,
	        forceRegistration : isForceRegistration
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof RemotePhone
	 * @method deleteNumber
	 * @description
	 * Delete remote number used for remote phone. Doing this will also disconnect from and unregister remote phone from
	 * the CUCM.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	RemotePhone.prototype.deleteNumber = function(errorHandler)
	{
	    var messageType = "deleteRemoteDestination";
	    var messageData = {};
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	
	// Module Exports
	module.exports.RemotePhone = RemotePhone;


/***/ },
/* 118 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule   = __webpack_require__(72);
	var LoggerModule          = __webpack_require__(69);
	var AuthenticationEnumModule = __webpack_require__(119);
	
	
	var cwic = {
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	    MessageSender   : MessageSenderModule.MessageSender,
	    Logger          : LoggerModule.Logger,
	    AuthenticationState : AuthenticationEnumModule.AuthenticationState,
	    AuthenticationError : AuthenticationEnumModule.AuthenticationError,
	    AuthenticationStateMap : AuthenticationEnumModule.AuthenticationStateMap,
	    AuthenticationErrorMap : AuthenticationEnumModule.AuthenticationErrorMap
	};
	
	
	/**
	 * @class LoginController
	 * @constructor
	 */
	function LoginController()
	{
	    var m_EventHandlers = {};
	    var m_CTIServers    = [];
	    var m_CCMCIPServers = [];
	    var m_TFTPServers   = [];
	    var m_SSORedirectURL = "";
	    this.signInType    = "Unknown";
	
	    cwic.MessageReceiver.addMessageHandler('lifecyclestatechanged', onLoginStateChanged.bind(this));
	    cwic.MessageReceiver.addMessageHandler('userprofilecredentialsrequired', onCredentialsRequired.bind(this));
	    cwic.MessageReceiver.addMessageHandler('userprofileemailaddressrequired', onEmailRequired.bind(this));
	    cwic.MessageReceiver.addMessageHandler('loggedin', onLoggedIn.bind(this));
	    cwic.MessageReceiver.addMessageHandler('authenticationresult', onAuthenticationResults.bind(this));
	    cwic.MessageReceiver.addMessageHandler('ssonavigateto', onSSONavigateRequired);
	
	    function onEmailRequired()
	    {
	        if(this.signInType == "Manual")
	        {
	            this.setEmail('dummy@jsdk.com');
	        }
	        else
	        {
	            cwic.Logger.info("E-mail address is required.");
	            var eventHandler = m_EventHandlers['onEmailRequired'];
	            if(eventHandler)
	            {
	                // TODO Add additional logic here
	                eventHandler();
	            }
	        }
	    }
	
	    function onCredentialsRequired()
	    {
	        cwic.Logger.info("Credentials are required.");
	        var eventHandler = m_EventHandlers['onCredentialsRequired'];
	        if(eventHandler)
	        {
	            eventHandler();
	        }
	    }
	
	    function onLoginStateChanged(content)
	    {
	        var lifecycleState = content;
	        var eventHandler   = null;
	        var loginState     = "";
	
	        switch(lifecycleState)
	        {
	            case "SIGNEDOUT" :
	            {
	                eventHandler = m_EventHandlers['onSignedOut'];
	                loginState = "Signed Out";
	                break;
	            }
	            case "SIGNINGOUT" :
	            {
	                eventHandler = m_EventHandlers['onSigningOut'];
	                loginState = "Signing Out";
	                break;
	            }
	            case "SIGNEDIN" :
	            {
	                eventHandler = m_EventHandlers['onSignedIn'];
	                loginState = "Signed In";
	                break;
	            }
	            case "SIGNINGIN" :
	            {
	                eventHandler = m_EventHandlers['onSigningIn'];
	                loginState = "Signing In";
	                break;
	            }
	            case "DISCOVERING" :
	            {
	                eventHandler = m_EventHandlers['onServiceDiscovering'];
	                loginState = "Discovering Services";
	                break;
	            }
	            case "RESETTING" :
	            {
	                eventHandler = m_EventHandlers['onDataResetting'];
	                loginState = "Resetting User Data";
	                break;
	            }
	        }
	
	        cwic.Logger.info("Login state has changed to: " + loginState + ".");
	
	        if(eventHandler)
	        {
	            eventHandler();
	        }
	    }
	
	    function onAuthenticationResults(content)
	    {
	        var authenticationResult = content.result;
	
	        var eventParam  = "";
	        var eventHandler = null;
	
	        switch(authenticationResult)
	        {
	            case "":
	            case "eNoError":
	                eventHandler = m_EventHandlers["onAuthenticationStateChanged"];
	                eventParam = cwic.AuthenticationStateMap[content.status];
	                cwic.Logger.info("Authentication state has changed to: " + eventParam);
	                break;
	            case "eFailed":
	                eventHandler = m_EventHandlers["onAuthenticationFailed"];
	                eventParam = cwic.AuthenticationErrorMap[content.status];
	                cwic.Logger.warning("Authentication failed! Reason: " + eventParam);
	                break;
	        }
	
	        if(eventHandler)
	        {
	            eventHandler(eventParam);
	        }
	    }
	
	    function onLoggedIn()
	    {
	        var eventHandler = m_EventHandlers['onLoggedIn'];
	        if(eventHandler)
	        {
	            eventHandler();
	        }
	    }
	
	    function onSSONavigateRequired(content)
	    {
	        cwic.Logger.info("SSO Navigation required.");
	        var eventHandler = m_EventHandlers['onSSONavigationRequired'];
	        if(eventHandler)
	        {
	            // Not a valid URL for sdk client that is received from JCF.
	            // We need to replace client_id parameter in SSOUrl with a valid client ID.
	            var SSOUrl = content;
	
	            // Regex for finding client id.
	            var regex = new RegExp("client_id=[A-Za-z0-9]*");
	
	            // Valid client id for SDK client.
	            var SSOClientID = 'C69908c4f345729af0a23cdfff1d255272de942193e7d39171ddd307bc488d7a1';
	
	            var url = SSOUrl.replace(regex, "client_id=" + SSOClientID);
	
	            if(m_SSORedirectURL !== "")
	            {
	                url += "&redirect_uri=" + encodeURIComponent(m_SSORedirectURL);
	            }
	
	            eventHandler(url);
	        }
	    }
	
	    /**
	     * @memberof LoginController
	     * @method addEventHandler
	     * @description Add handler function for Login Controller's event.
	     *
	     * @param eventName {String} - Name of the event.
	     * @param eventHandler {Function} - Function that will be called when event is fired.
	     *
	     * @since 11.7.0
	     */
	    this.addEventHandler = function(eventName, eventHandler)
	    {
	        m_EventHandlers[eventName] = eventHandler;
	    };
	
	    /**
	     * @memberof LoginController
	     * @method removeEventHandler
	     * @description Remove handler function for Login Controller's event.
	     *
	     * @param eventName {String} - Name of the event for which handler will be removed.
	     *
	     * @since 11.7.0
	     */
	    this.removeEventHandler = function(eventName)
	    {
	        delete m_EventHandlers[eventName];
	    };
	
	    /**
	     * @memberof LoginController
	     * @method setSSORedirectURL
	     * @description
	     * Set URL that will be used as a redirection once a successful authentication has been performed with
	     * identity provider.
	     *
	     * @param redirectURL {String} - URL that will be set.
	     * @since 11.7.0
	     */
	    this.setSSORedirectURL = function(redirectURL)
	    {
	        m_SSORedirectURL = redirectURL;
	    };
	
	    /**
	     * @memberof LoginController
	     * @method setCTIServers
	     * @description
	     * Set up to a maximum of 3 CTI servers.
	     *
	     * @param CTIServers {Array} - List of CTI servers to be set
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     *
	     * @throw Invalid Object - Thrown if CTIServers is not an array.
	     *
	     * @since 11.7.0
	     */
	    this.setCTIServers = function(CTIServers, errorHandler)
	    {
	        if(!(CTIServers instanceof Array))
	        {
	            throw Error("Invaild Object");
	        }
	        if(CTIServers.length > 3)
	        {
	            throw "Server List Size Exceeds Maximum Value."
	        }
	
	        m_CTIServers = CTIServers;
	
	        var messageType = "setProperty";
	        var messageData = {CtiAddressList : CTIServers};
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    };
	
	    /**
	     * @memberof LoginController
	     * @method setTFTPServers
	     * @description
	     * Set up to a maximum of 3 TFTP servers.
	     *
	     * @param TFTPServers {Array} - List of TFTP servers to be set
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     *
	     * @throw Invalid Object - Thrown if TFTPServers is not an array.
	     *
	     * @since 11.7.0
	     */
	    this.setTFTPServers = function(TFTPServers, errorHandler)
	    {
	        if(!(TFTPServers instanceof Array))
	        {
	            throw Error("Invaild Object");
	        }
	        if(TFTPServers.length > 3)
	        {
	            throw "Server List Size Exceeds Maximum Value."
	        }
	
	        m_TFTPServers = TFTPServers;
	        var messageType = "setProperty";
	        var messageData = {TftpAddressList : TFTPServers};
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    };
	
	    /**
	     * @memberof LoginController
	     * @method setCUCMPServers
	     * @description
	     * Set up to a maximum of 3 CUCM servers.
	     *
	     * @param CUCMServers {Array} - List of CUCM servers to be set
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     *
	     * @throw Invalid Object - Thrown if CUCMServers is not an array.
	     *
	     * @since 11.7.0
	     */
	    this.setCUCMServers = function (CUCMServers, errorHandler)
	    {
	        if(!(CUCMServers instanceof Array))
	        {
	            throw Error("Invaild Object");
	        }
	        if(CUCMServers.length > 3)
	        {
	            throw "Server List Size Exceeds Maximum Value."
	        }
	
	        m_CCMCIPServers = CUCMServers;
	        var messageType = "setProperty";
	        var messageData = {CcmcipAddressList : CUCMServers};
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    };
	}
	
	/**
	 * @memberof LoginController
	 * @method signIn
	 * @description
	 * Performs manual sign in. In order for sign in to be successful credentials and servers needs to be setup properly.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	LoginController.prototype.signIn = function(errorHandler)
	{
	    this.signInType = "Manual";
	
	    var messageType = "startSignIn";
	    var messageData = {
	        manualSettings : true
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof LoginController
	 * @method signOut
	 * @description
	 * Perform sign out.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	LoginController.prototype.signOut = function(errorHandler)
	{
	    var messageType = "logout";
	    var messageData = {};
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof LoginController
	 * @method startDiscovery
	 * @description
	 * Start service discovery lifecycle.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	LoginController.prototype.startDiscovery = function(errorHandler)
	{
	    this.signInType = "Discovery";
	    var messageType = "startSignIn";
	    var messageData = {
	        manualSettings : false
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof LoginController
	 * @method setEmail
	 * @description
	 * Set email address that will be used for service discovery. Should be called when "onEmailRequired" event is fired.
	 *
	 * @param email {String} - E-mail address to be set.
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	LoginController.prototype.setEmail = function(email, errorHandler)
	{
	    var messageType = "setUserProfileEmailAddress";
	    var messageData = {
	        email : email
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof LoginController
	 * @method setCredentials
	 * @description
	 * Set credentials that will be used in login process. Should be called when "onCredentialsRequired" event is fired.
	 *
	 * @param username {String} - Username to be set.
	 * @param password {String} - Password to be set.
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	LoginController.prototype.setCredentials = function(username, password, errorHandler)
	{
	    var messageType = "setUserCredentials";
	    var messageData = {
	        username: username,
	        password: password
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof LoginController
	 * @method resetUserData
	 * @description
	 * Erases all user specific data that was cached in add-on.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	LoginController.prototype.resetUserData = function(errorHandler)
	{
	    var messageType = "resetData";
	    var messageData = {};
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof LoginController
	 * @method setSSOTokenUri
	 * @description
	 * Set SSO token uri that was retrieved from identity provided.
	 *
	 * @param uri {String} - Token uri that will be set.
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	LoginController.prototype.setSSOTokenURI = function(uri, errorHandler)
	{
	    var messageType = "ssoNavigationCompleted";
	    var messageData = {
	        result: 200,
	        url: uri,
	        document: ''
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof LoginController
	 * @method cancelSSO
	 * @description
	 * Cancel ongoing SSO procedure.
	 *
	 * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	 * @since 11.7.0
	 */
	LoginController.prototype.cancelSSO = function(errorHandler)
	{
	    var messageType = "cancelSingleSignOn";
	    var messageData = {};
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	module.exports.LoginController = new LoginController();


/***/ },
/* 119 */
/***/ function(module, exports) {

	/**
	 * @enum AuthenticationState {Enum}
	 * @description
	 * Different authentication states that can occur when trying to connect to CUCM.
	 *
	 * @since 11.7.0
	 */
	var AuthenticationState = {
	    /**
	     * Initial state before authentication attempt has occurred.
	     * @type {String}
	     */
	    NotAuthenticated : "NotAuthenticated",
	
	    /**
	     * Authentication with CUCM is in progress.
	     * @type {String}
	     */
	    InProgress       : "InProgress",
	
	    /**
	     * Successfully authenticated with CUCM.
	     * @type {String}
	     */
	    Authenticated    : "Authenticated"
	};
	
	/**
	 * @enum AuthenticationError {Enum}
	 * @description
	 * Different authentication errors that can occur during authentication process with CUCM.
	 *
	 * @since 11.7.0
	 */
	var AuthenticationError = {
	    /**
	     *  The last attempt at authentication with CUCM failed because of invalid configuration. The CCMIP server
	     *  was incorrect.
	     *  @type {String}
	     */
	    InvalidConfig             : "InvalidConfiguration",
	
	    /**
	     * The last attempt at authentication with CUCM failed because the users credentials are incorrect.
	     * @type {String}
	     */
	    InvalidCredentials        : "InvalidCredentials",
	
	    /**
	     * The last attempt at authentication with CUCM failed because the authentication token was invalid.
	     * @type {String}
	     */
	    InvalidToken              : "InvalidToken",
	
	    /**
	     * The last attempt at authentication with CUCM failed because the user rejected an invalid server certificate.
	     * @type {String}
	     */
	    ServerCertificateRejected : "ServerCertificateRejected",
	
	    /**
	     * The last attempt at authentication with CUCM failed because of an error with the client's certificate.
	     * @type {String}
	     */
	    ClientCertificateError    : "ClientCertificateError",
	
	    /**
	     * The last attempt at authentication with CUCM failed because no credentials are configured.
	     * @type {String}
	     */
	    NoCredentialsConfigured   : "NoCredentialsConfigured",
	
	    /**
	     * Unable to connect to CUCM.
	     * @type {String}
	     */
	    CouldNotConnect           : "CouldNotConnect",
	
	    /**
	     * The last attempt at authentication failed.
	     * @type {String}
	     */
	    Failed                    : "Failed",
	
	    /**
	     * TLS/SSL Connection Error.
	     * @type {String}
	     */
	    SSLConnectError           : "SSLConnectionError",
	
	    /**
	     * Unknown authentication failure.
	     * @type {String}
	     */
	    Unknown                   : "Unknown"
	};
	
	var AuthenticationStateMap  = {
	    eAuthenticated   : "Authenticated",
	    eInProgress      : "InProgress",
	    eNotAuthenticated: "NotAuthenticated"
	};
	
	var AuthenticationErrorMap = {
	    eNoServersConfigured       : "InvalidConfiguration",
	    eCredentialsRejected       : "InvalidCredentials",
	    eInvalidToken              : "InvalidToken",
	    eServerCertificateRejected : "ServerCertificateRejected",
	    eClientCertificateError    : "ClientCertificateError",
	    eNoCredentialsConfigured   : "NoCredentialsConfigured",
	    eCouldNotConnect           : "CouldNotConnect",
	    eFailed                    : "Failed",
	    eSSLConnectError           : "SSLConnectionError",
	    ""                         : "Unknown"
	};
	
	module.exports.AuthenticationState = AuthenticationState;
	module.exports.AuthenticationError = AuthenticationError;
	module.exports.AuthenticationStateMap = AuthenticationStateMap;
	module.exports.AuthenticationErrorMap = AuthenticationErrorMap;


/***/ },
/* 120 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule   = __webpack_require__(72);
	var ChromeExtensionModule = __webpack_require__(121);
	var NPAPIPluginModule     = __webpack_require__(122);
	var ActiveXControlModule  = __webpack_require__(123);
	var VersionModule         = __webpack_require__(124);
	var LoggerModule          = __webpack_require__(69);
	var UtilitesModule        = __webpack_require__(70);
	var SystemControllerCapabilitiesModule = __webpack_require__(125);
	
	// Utilities Modules
	var generateUniqueId = __webpack_require__(73);
	
	var cwic = {
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	    MessageSender   : MessageSenderModule.MessageSender,
	
	    ChromeExtension : ChromeExtensionModule.ChromeExtension,
	    NPAPIPlugin     : NPAPIPluginModule.NPAPIPlugin,
	    ActiveXControl  : ActiveXControlModule.ActiveXControl,
	
	    Logger  : LoggerModule.Logger,
	    Utilities : UtilitesModule.Utilities,
	    Version : VersionModule.Version,
	    version : VersionModule.cwicVersion,
	
	    SystemControllerCapabilities : SystemControllerCapabilitiesModule.SystemControllerCapabilities
	};
	
	/**
	 * @class SystemController
	 * @classdesc
	 * Controller responsible for initializing CWIC, and notifying of various system events. CWIC global capabilities can
	 * also be retrieved once the CWIC has been initialized.
	 *
	 * @since 11.7.0
	 */
	function SystemController()
	{
	    var m_EventHandlers = {};
	    var m_Capabilities  = {};
	    var m_ClientID      = generateUniqueId();
	    var m_Version       = {};
	    var m_Plugin        = null;
	    this.cwicVersion = "11.7.0.243287";
	    this.addonVersion = "Unknown";
	    this.systemRelease = "Unknown";

	    cwic.MessageReceiver.addMessageHandler('init', onCwicInitialized.bind(this));
	    cwic.MessageReceiver.addMessageHandler('userauthorized', onUserAuthorized.bind(this));
	    cwic.MessageReceiver.addMessageHandler('addonConnectionLost', onAddonConnectionLost.bind(this));
	
	    function onCwicInitialized(content)
	    {
	        m_Capabilities = new cwic.SystemControllerCapabilities(content.capabilities);
	        this.addonVersion = content.version.plugin;
	        this.systemRelease = content.version.system_release;
	
	        if(this.addonVersion !== this.cwicVersion)
	        {
	            cwic.Logger.warning("Cwic version and add-on version miss-match");
	        }
	
	        cwic.Logger.info("System Initialized.");
	
	        if(m_EventHandlers['onInitialized'])
	        {
	            var onInitializedCallback = m_EventHandlers['onInitialized'];
	            onInitializedCallback();
	        }
	    }
	
	    function onUserAuthorized(content)
	    {
	        var eventName;
	        if(content === true)
	        {
	            eventName = 'onUserAuthorized';
	        }
	        else
	        {
	            eventName = 'onUserAuthorizationRejected';
	        }
	
	        if(m_EventHandlers['onUserAuthorized'])
	        {
	
	            var onUserAuthorizedCallback = m_EventHandlers['onUserAuthorized'];
	            onUserAuthorizedCallback();
	        }
	    }
	
	    function onAddonConnectionLost()
	    {
	        var eventHandler = m_EventHandlers['onAddonConnectionLost'];
	        m_Plugin.uninitialize();
	        m_Plugin = null;
	
	        if(eventHandler)
	        {
	            eventHandler();
	        }
	    }
	
	    function initialize(errorHandler)
	    {
	        cwic.Logger.info("System Initializing.");
	
	        var browserType = cwic.Utilities.getBrowserType();
	
	        switch(browserType)
	        {
	            case "Chrome":
	                m_Plugin = new cwic.ChromeExtension();
	                break;
	            case "Firefox":
	            case "Safari":
	                m_Plugin = new cwic.NPAPIPlugin(m_ClientID);
	                break;
	            case "InternetExplorer":
	                m_Plugin = new cwic.ActiveXControl(m_ClientID);
	                break;
	            default:
	                throw Error("Unsupported Browser");
	        }
	        cwic.Logger.info("Detected " + browserType + " browser.");
	
	        m_Plugin.onInitializeError = errorHandler;
	        m_Plugin.initialize();
	    }
	
	    /**
	     * @memberof SystemController
	     * @method getInstanceID
	     * @description
	     * Retrieve unique instance ID of CWIC library.
	     * @returns {String} Unique instance id.
	     * @since 11.7.0
	     */
	    this.getInstanceID = function ()
	    {
	        return m_ClientID;
	    };
	
	    /**
	     * @memberof SystemController
	     * @method getCapabilities
	     * @description
	     * Retrieve CWIC capability list.
	     * @returns {SystemControllerCapabilities}.
	     * @since 11.7.0
	     */
	    this.getCapabilities = function()
	    {
	        return m_Capabilities;
	    };
	
	    /**
	     * @memberof SystemController
	     * @method getVersion
	     * @description
	     * Retrieve CWIC library version.
	     *
	     * @returns {String}.
	     * @since 11.7.0
	     */
	    this.getVersion = function()
	    {
	        return m_Version;
	    };
	
	    /**
	     * @memberof SystemController
	     * @method setLoggingLevel
	     * @description
	     * Set new logging level. Possible Values:
	     * <br>
	     * <ul>
	     *     <li> 0 - Debug, this logging level is used for debugging purposes of JSDK team.
	     *     <li> 1 - Info, default value for log level. Logs main CWIC events.
	     *     <li> 2 - Warning, logs errors that are not critical (the ones that could be expected of improper use of API)
	     *     <li> 3 - Error, logs critical errors
	     * </ul>
	     *
	     * @param level {Number} - Logging level that will be set.
	     * @since 11.7.0
	     */
	    this.setLoggingLevel = function(level)
	    {
	        cwic.Logger.logLevel = level;
	    };
	
	    /**
	     * @memberof SystemController
	     * @method addEventHandler
	     * @description Add handler function for System Controller's event.
	     *
	     * @param eventName {String} - Name of the event.
	     * @param handler {Function} - Function that will be called when event is fired.
	     *
	     * @since 11.7.0
	     */
	    this.addEventHandler = function(eventName, handlerFunction)
	    {
	        m_EventHandlers[eventName] = handlerFunction;
	    };
	
	    /**
	     * @memberof SystemController
	     * @method removeEventHandler
	     *
	     * @description Remove handler function for System Controller's event.
	     *
	     * @param eventName {String} - Name of the event.
	     *
	     * @since 11.7.0
	     */
	    this.removeEventHandler = function(eventName)
	    {
	        delete m_EventHandlers[eventName];
	    };
	
	    /**
	     * @memberof SystemController
	     * @method initialize
	     *
	     * @description
	     * Initializes CWIC library. Initializes NPAPI Plugin (Internet Explorer, Safara, Mozzila Firefox) or Chrome Plugin
	     * (Google Chrome). In order to use any other API call from library CWIC needs to be initialized first.
	     *
	     * @throw Unsupported Browser - Thrown if initialize is called in unsupported browser.
	     *
	     * @since 11.7.0
	     */
	    this.initialize = function ()
	    {
	        if(document.hidden)
	        {
	            function onDocumentShown()
	            {
	                if(!document.hidden)
	                {
	                    initialize();
	                    document.removeEventListener('visibilitychange', onDocumentShown);
	                }
	            }
	
	            document.addEventListener('visibilitychange', onDocumentShown);
	        }
	        else
	        {
	            initialize(m_EventHandlers['onInitializationError']);
	        }
	    };
	}
	
	module.exports.SystemController = new SystemController();
	


/***/ },
/* 121 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var ExtensionModule = __webpack_require__(66);
	var MessageSenderModule = __webpack_require__(72);
	var MessageReceiverModule = __webpack_require__(67);
	var LoggerModule = __webpack_require__(69);
	
	var cwic = {
	    Plugin : ExtensionModule.Plugin,
	    MessageSender : MessageSenderModule.MessageSender,
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	    Logger : LoggerModule.Logger
	};
	
	function ChromeExtension()
	{
	    this.type = "ChromeExtension";
	}
	
	ChromeExtension.prototype = Object.create(cwic.Plugin.prototype);
	ChromeExtension.prototype.constructor = ChromeExtension;
	
	ChromeExtension.prototype.initialize = function()
	{
	    cwic.Logger.info('Initializing Chrome extension.');
	    var extension;
	    var extensionScript = document.createElement('script');
	    var extensionID     = "ppbllmlcmhfnfflbkbinnhacecaankdh";
	
	    extensionScript.id      = extensionID;
	    extensionScript.src     = 'chrome-extension://' + extensionID + '/cwic_plugin.js';
	    extensionScript.onload  = onExtensionLoaded.bind(this);
	    extensionScript.onerror = onExtensionError.bind(this);
	
	    document.head.appendChild(extensionScript);
	
	    function onExtensionLoaded()
	    {
	        cwic.Logger.info('Chrome extension has been initialized.');
	        var extension = cwic_plugin;
	
	        this.version = extension.version;
	        this.sendMessage = extension.sendRequest;
	
	        var settings = {
	            cwicExtId: 'ppbllmlcmhfnfflbkbinnhacecaankdh',
	            verbose: true
	        };
	
	        cwic.MessageSender.plugin = this;
	        extension.init(this.onMessageReceived.bind(this), settings);
	    }
	
	    function onExtensionError()
	    {
	        cwic.Logger.error('Failed to load Chrome extension! Extension is not installed!');
	
	        if(this.onInitializeError)
	        {
	            var url = 'https://chrome.google.com/webstore/detail/cisco-web-communicator/ppbllmlcmhfnfflbkbinnhacecaankdh';
	
	            var errorInfo = {
	                errorType : "ChromeExtension",
	                errorData : {
	                    reason : "ExtensionNotInstalled",
	                    extensionURL : url
	                }
	            };
	
	            this.onInitializeError(errorInfo);
	        }
	    }
	};
	
	ChromeExtension.prototype.uninitialize = function()
	{
	    var extensionID     = "ppbllmlcmhfnfflbkbinnhacecaankdh";
	    var extensionScript = document.getElementById(extensionID);
	
	    extensionScript.parentNode.removeChild(extensionScript);
	    cwic.MessageSender.plugin = null;
	    cwic_plugin = null;
	};
	
	ChromeExtension.prototype.sendMessage = function(message)
	{
	    var extension = cwic_plugin;
	
	    cwic.Logger("Sending message: ", message);
	
	    extension.sendRequest(message);
	};
	
	ChromeExtension.prototype.onMessageReceived = function(message)
	{
	    cwic.MessageReceiver.onMessageReceived(message);
	};
	
	module.exports.ChromeExtension = ChromeExtension;


/***/ },
/* 122 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var ExtensionModule = __webpack_require__(66);
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule = __webpack_require__(72);
	var LoggerModule = __webpack_require__(69);
	
	var cwic = {
	    Plugin : ExtensionModule.Plugin,
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	    MessageSender : MessageSenderModule.MessageSender,
	    Logger : LoggerModule.Logger
	};
	
	function NPAPIPlugin(cwicID)
	{
	    this.type = "NPAPIPlugin";
	    this.cwicID = cwicID;
	}
	
	NPAPIPlugin.prototype = Object.create(cwic.Plugin.prototype);
	NPAPIPlugin.prototype.constructor = NPAPIPlugin;
	
	NPAPIPlugin.prototype.initialize = function()
	{
	    cwic.Logger.info('Initializing NPAPI plugin.');
	    var pluginName = 'application/x-ciscowebcommunicator';
	    var pluginMimeType = navigator.mimeTypes[pluginName];
	
	    if(!pluginMimeType)
	    {
	        cwic.Logger.error("Failed to load NPAPI Plugin.");
	
	        if(this.onInitializeError)
	        {
	            var errorInfo = {
	                errorType : "NPAPIPlugin",
	                errorData : {
	                    reason : "AddonNotInstalled"
	                }
	            };
	
	            this.onInitializeError(errorInfo);
	        }
	    }
	    else
	    {
	        cwic.Logger.info('NPAPI plugin has been initialized.');
	        var plugin = document.createElement('object');
	
	
	        plugin.id   = "cwic-plugin";
	        plugin.type = "application/x-ciscowebcommunicator";
	
	        document.body.appendChild(plugin);
	
	        plugin.addEventListener('addonmessage', this.onMessageReceived.bind(this), false);
	
	        plugin.setClientId(this.cwicID);
	        plugin.setUrl(window.location.href);
	        plugin.setHostName(window.location.hostname ? window.location.hostname : "file://");
	        plugin.setAppName(window.document.title);
	
	        cwic.MessageSender.plugin = this;
	        cwic.MessageSender.sendMessage('init', {});
	    }
	};
	
	NPAPIPlugin.prototype.uninitialize = function()
	{
	    var pluginID = "cwic-plugin";
	    var plugin   = document.getElementById(pluginID);
	
	    document.body.removeChild(plugin)
	    //plugin.parentNode.removeChild(plugin);
	    cwic.MessageSender.plugin = null;
	};
	
	NPAPIPlugin.prototype.sendMessage = function(message)
	{
	    var npapiPlugin = document.getElementById('cwic-plugin');
	
	    message.client = {
	        'id': this.cwicID,
	        'url': window.location.href,
	        'hostname': window.location.hostname ? window.location.hostname : "file://",
	        'name': window.document.title
	    };
	
	    var npapiPluginMessage = {
	        ciscoChannelMessage: message
	    };
	
	    npapiPlugin.postMessage(JSON.stringify(npapiPluginMessage));
	};
	
	NPAPIPlugin.prototype.onMessageReceived = function(message)
	{
	    try
	    {
	        var clientMessage = JSON.parse(message);
	    }
	    catch (exception)
	    {
	        cwic.Logger.error("Failed to parse message received from Add-on!");
	        return;
	    }
	    cwic.MessageReceiver.onMessageReceived(clientMessage.ciscoChannelMessage);
	};
	
	module.exports.NPAPIPlugin = NPAPIPlugin;


/***/ },
/* 123 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var ExtensionModule = __webpack_require__(66);
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule = __webpack_require__(72);
	var LoggerModule = __webpack_require__(69);
	
	var cwic = {
	    Plugin : ExtensionModule.Plugin,
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	    MessageSender : MessageSenderModule.MessageSender,
	    Logger : LoggerModule.Logger
	};
	
	function ActiveXControl(cwicID)
	{
	    this.type = "ActiveXControl";
	    this.cwicID = cwicID;
	}
	
	ActiveXControl.prototype = Object.create(cwic.Plugin.prototype);
	ActiveXControl.prototype.constructor = ActiveXControl;
	
	ActiveXControl.prototype.initialize = function()
	{
	    if ('ActiveXObject' in window)
	    {
	        cwic.Logger.info('Initializing ActiveX control.');
	        try
	        {
	            // Try to load the ActiveX Control, throws exception if it fails.
	            var dummyActiveXControl = new ActiveXObject('CiscoSystems.CWCVideoCall');
	
	            cwic.Logger.info('ActiveX control initialized.');
	
	            var plugin = document.createElement('object');
	
	            plugin.id   = "cwic-plugin";
	            plugin.type = "application/x-ciscowebcommunicator";
	
	            document.body.appendChild(plugin);
	
	            plugin.attachEvent('onaddonmessage', this.onMessageReceived.bind(this));
	
	            plugin.setClientId(this.cwicID);
	            plugin.setUrl(window.location.href);
	            plugin.setHostName(window.location.hostname ? window.location.hostname : "file://");
	            plugin.setAppName(window.document.title);
	
	            cwic.MessageSender.plugin = this;
	            cwic.MessageSender.pluginType = "ActiveXControl";
	            cwic.MessageSender.sendMessage('init', {});
	        }
	        catch (exception)
	        {
	            cwic.Logger.error("Failed to load ActiveX control");
	
	            if(this.onInitializeError)
	            {
	                var errorInfo = {
	                    errorType : "ActiveXControl",
	                    errorData : {
	                        reason : "AddonNotInstalled"
	                    }
	                };
	
	                this.onInitializeError(errorInfo);
	            }
	        }
	    }
	};
	
	ActiveXControl.prototype.uninitialize = function()
	{
	    var pluginID = "cwic-plugin";
	    var plugin   = document.getElementById(pluginID);
	
	    plugin.parentNode.removeChild(plugin);
	    cwic.MessageSender.plugin = null;
	};
	
	ActiveXControl.prototype.sendMessage = function(message)
	{
	    var activeXControl = document.getElementById('cwic-plugin');
	
	    message.client = {
	        'id': this.cwicID,
	        'url': window.location.href,
	        'hostname': window.location.hostname ? window.location.hostname : "file://",
	        'name': window.document.title
	    };
	
	    var activeXControlMessage = {
	        ciscoChannelMessage: message
	    };
	
	    activeXControl.postMessage(JSON.stringify(activeXControlMessage));
	};
	
	ActiveXControl.prototype.onMessageReceived = function(message)
	{
	    try
	    {
	        var clientMessage = JSON.parse(message);
	        cwic.MessageReceiver.onMessageReceived(clientMessage.ciscoChannelMessage);
	    }
	    catch (exception)
	    {
	        //Add error handlig logic here or log message.
	    }
	};
	
	module.exports.ActiveXControl = ActiveXControl;


/***/ },
/* 124 */
/***/ function(module, exports) {

	var version = "11.7.0.243287";
	
	function Version(versionString)
	{
	    var versionNumbers = versionString.split('.');
	
	    this.release = versionNumbers[0];
	    this.major = versionNumbers[1];
	    this.minor = versionNumbers[2];
	    this.build = versionNumbers[3];
	}
	
	Version.prototype.release = "";
	Version.prototype.major = "";
	Version.prototype.minor = "";
	Version.prototype.build = "";
	
	module.exports.cwicVersion = new Version(version);
	module.exports.Version = Version;
	


/***/ },
/* 125 */
/***/ function(module, exports) {

	/**
	 * @class SystemController
	 * @constructor
	 * @description
	 * This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function SystemControllerCapabilities(capabilites)
	{
	    this.inBrowserVideoSupport            = capabilites.videoPluginObject;
	    this.ringtoneOnAllRingersSupport      = capabilites.ringOnAllDevices;
	    this.ringtoneSupport                  = capabilites.ringtoneSelection;
	    this.nativeWindowDockingSupport       = capabilites.externalWindowDocking;
	    this.nativeWindowDockingTargetSupport = capabilites.showingDockingTarget;
	}
	
	/**
	 * @memberof SystemControllerCapabilities
	 * @member inBrowserVideoSupport
	 * @description
	 * Indicates whether in-browser video window support exists or not.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	SystemControllerCapabilities.prototype.inBrowserVideoSupport = false;
	
	/**
	 * @memberof SystemControllerCapabilities
	 * @member ringtoneOnAllRingersSupport
	 * @description
	 * Indicates whether ringtone can played on all ringtones or not.
	 *
	 * @since 11.7.0
	 */
	SystemControllerCapabilities.prototype.ringtoneOnAllRingersSupport = false;
	
	/**
	 * @memberof SystemControllerCapabilities
	 * @member ringtoneSupport
	 * @description
	 * Indicates whether ringtone support exists or not.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	SystemControllerCapabilities.prototype.ringtoneSupport = false;
	
	/**
	 * @memberof SystemControllerCapabilities
	 * @member nativeWindowDockingSupport
	 * @description
	 * Indicates whether native window can be docked or not.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	SystemControllerCapabilities.prototype.nativeWindowDockingSupport = false;
	
	/**
	 * @memberof SystemControllerCapabilities
	 * @member nativeWindowDockingTargetSupport
	 * @description
	 * Indicates whether docking target for native window exists or not.
	 *
	 * @type {boolean}
	 * @since 11.7.0
	 */
	SystemControllerCapabilities.prototype.nativeWindowDockingTargetSupport = false;
	
	module.exports.SystemControllerCapabilities = SystemControllerCapabilities;

/***/ },
/* 126 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var SystemControllerModule = __webpack_require__(120);
	var MessageSenderModule    = __webpack_require__(72);
	var MessageReceiverModule  = __webpack_require__(67);
	
	// Window Modules
	var NativePreviewWindowModule = __webpack_require__(127);
	var NativeScreenShareWindowModule = __webpack_require__(130);
	var NativeConversationWindowModule = __webpack_require__(131);
	var ScreenShareWindowModule = __webpack_require__(132);
	var ConversationWindowModule = __webpack_require__(134);
	var PreviewWindowModule = __webpack_require__(135);
	
	var cwic = {
	    MessageSender    : MessageSenderModule.MessageSender,
	    MessageReceiver  : MessageReceiverModule.MessageReceiver,
	    SystemController : SystemControllerModule.SystemController,
	
	    NativeScreenShareWindow  : NativeScreenShareWindowModule.NativeScreenShareWindow,
	    NativePreviewWindow : NativePreviewWindowModule.NativePreviewWindow,
	    NativeConversationWindow : NativeConversationWindowModule.NativeConversationWindow,
	
	    ScreenShareWindow  : ScreenShareWindowModule.ScreenShareWindow,
	    ConversationWindow : ConversationWindowModule.ConversationWindow,
	    PreviewWindow      : PreviewWindowModule.PreviewWindow
	
	};
	
	/**
	 * @class WindowController
	 * @classdesc
	 * WindowController is responsible for managing native and in-browser video windows.
	 *
	 * @description
	 * This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function WindowController()
	{
	    var m_NativePreviewWindow = cwic.NativePreviewWindow;
	    var m_NativeScreenShareWindow  = cwic.NativeScreenShareWindow;
	    var m_NativeConversationWindow = cwic.NativeConversationWindow;
	
	    /**
	     * @memberof WindowController
	     * @method getNativePreviewWindow
	     * @description
	     * Retrieve native window that renders local camera feed.
	     *
	     * @returns {NativePreviewWindow}
	     * @since 11.7.0
	     */
	    this.getNativePreviewWindow = function()
	    {
	        return m_NativePreviewWindow;
	    };
	
	    /**
	     * @memberof WindowController
	     * @method getNativeScreenShareWindow
	     * @description
	     * Retrieve native window that renders screen share video.
	     *
	     * @returns {NativeScreenShareWindow}
	     * @since 11.7.0
	     */
	    this.getNativeScreenShareWindow = function()
	    {
	        return m_NativeScreenShareWindow;
	    };
	
	    /**
	     * @memberof WindowController
	     * @method getNativeConversationWindow
	     * @description
	     * Retrieve native window that renders conversation video.
	     *
	     * @returns {NativeConversationWindow}
	     * @since 11.7.0
	     */
	    this.getNativeConversationWindow = function()
	    {
	        return m_NativeConversationWindow;
	    };
	
	    /**
	     * @memberof WindowController
	     * @method createVideoWindow
	     * @description
	     * Create video window object. In order to create a video window object, HTML element that will
	     * serve as container must be specified. This HTML element can be a part of the same DOMWindow in which CWIC is
	     * initialized or it can be a separate child window or IFrame.
	     *
	     * @param windowType {WindowType} - Type of video window it will be created.
	     * @param htmlElement {HTMLElement} - HTML element that will represent window container.
	     * @param [DOMWindow] {Window} - DOM window that owns HTML element
	     * @returns {ScreenShareWindow | PreviewWindow | ConversationWindow}
	     *
	     * @since 11.7.0
	     *
	     * @throw Invalud Window Type - Thrown if windowType parameter is invalid.
	     */
	    this.createVideoWindow = function(windowType, htmlElement, DOMWindow)
	    {
	        var videoWindow;
	
	        switch(windowType)
	        {
	            case 'ScreenShare':
	                videoWindow = new cwic.ScreenShareWindow(htmlElement, DOMWindow);
	                break;
	            case 'Preview':
	                videoWindow = new cwic.PreviewWindow(htmlElement, DOMWindow);
	                break;
	            case 'Conversation':
	                videoWindow = new cwic.ConversationWindow(htmlElement, DOMWindow);
	                break;
	            default:
	                throw "Invalid Window Type";
	        }
	
	        return videoWindow;
	    }
	}
	
	module.exports.WindowController = new WindowController();

/***/ },
/* 127 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var NativeVideoWindowModule = __webpack_require__(128);
	
	var cwic = {
	    NativeVideoWindow : NativeVideoWindowModule.NativeVideoWindow
	};
	
	/**
	 * @class NativePreviewWindow
	 * @extends  NativeVideoWindow
	 * @classdesc
	 * Represents native video window in which camera's feed video is rendered.
	 *
	 * @description
	 * This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function NativePreviewWindow()
	{
	    cwic.NativeVideoWindow.call(this, 'preview');
	}
	
	NativePreviewWindow.prototype = Object.create(cwic.NativeVideoWindow.prototype);
	NativePreviewWindow.prototype.constructor = NativePreviewWindow();
	
	
	module.exports.NativePreviewWindow = new NativePreviewWindow();


/***/ },
/* 128 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageSenderModule    = __webpack_require__(72);
	var SystemControllerModule = __webpack_require__(120);
	
	// Window Modules
	var DockingWindowModule = __webpack_require__(129);
	
	var cwic = {
	    MessageSender    : MessageSenderModule.MessageSender,
	    SystemController : SystemControllerModule.SystemController,
	
	    PreviewDockingWindow     : DockingWindowModule.PreviewDockingWindow,
	    RemoteDockingWindow      : DockingWindowModule.RemoteDockingWindow,
	    ScreenShareDockingWindow : DockingWindowModule.ScreenShareDockingWindow
	};
	
	/**
	 * @class NativeVideoWindow
	 * @classdesc
	 * Native video window represents, native window in which video is rendered. Depending on the browser and operating
	 * system (OS) this window will have different capabilities.
	 * On Google Chrome NPAPI support has been dropped so communication between Cisco Web Communicator and CWIC API is being
	 * handled through Chromes's extensions. Chrome extension doesn't provide a way to render video from native application
	 * directly into browser's web application like NPAPI does. In order to overcome this issue Native Video Window can be
	 * docked/undocked to/from a specified HTML element that will act as a docking target in web browser application. This
	 * way we simulate behaviour as if native video window is part of a HTML document. There are specific limitations with
	 * this approach. Since native window acts as an overlay over specified HTML element, no child HTML elements will be
	 * visible.
	 *
	 * <br>
	 * @description
	 * On Mozzila Firefox, Internet Explorer and Safari u can use regular VideoWindow as NPAPI support for these brwosers
	 * still exits.
	 *
	 * @since 11.7.0
	 */
	function NativeVideoWindow(windowType)
	{
	    var m_Type = windowType;
	
	    function getDockingWindow()
	    {
	        var dockingWindow = null;
	        switch(m_Type)
	        {
	            case "preview":
	                dockingWindow = cwic.PreviewDockingWindow;
	                break;
	            case "remote":
	                dockingWindow = cwic.RemoteDockingWindow;
	                break;
	            case "share":
	                dockingWindow = cwic.ScreenShareDockingWindow;
	                break;
	        }
	
	        return dockingWindow;
	    }
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method hide
	     * @description
	     * Hide native video window.
	     *
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     * @since 11.7.0
	     */
	    this.hide = function(errorHandler)
	    {
	        var messageType = 'hideExternalWindow';
	        var messageData = {
	            windowType : m_Type
	        };
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    };
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method show
	     * @description
	     * Show native video window.
	     *
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     * @since 11.7.0
	     */
	    this.show = function(errorHandler)
	    {
	        var messageType;
	
	        if(m_Type === 'preview' || m_Type === 'share')
	        {
	            messageType = 'showPreviewInExternalWindow';
	        }
	        else
	        {
	            messageType = "showExternalWindow";
	        }
	
	        var messageData = {
	            windowType : m_Type
	        };
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    };
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method showAlwaysOnTop
	     * @description
	     * Set native video window to be shown always on top of Z-order.
	     *
	     * @param isAlwaysOnTop {Boolean}
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     * @since 11.7.0
	     */
	    this.showAlwaysOnTop = function(isAlwaysOnTop, errorHandler)
	    {
	        var messageType = 'setExternalWindowAlwaysOnTop';
	        var messageData = {
	            alwaysOnTop: isAlwaysOnTop,
	            windowType: m_Type
	        };
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    };
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method setTitle
	     * @description
	     * Set native video window's title.
	     *
	     * @param title {String} - Title that will be set
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     * @since 11.7.0
	     */
	    this.setTitle = function(title, errorHandler)
	    {
	        var messageType = 'setExternalWindowTitle';
	        var messageData = {
	            title : title,
	            windowType : m_Type
	        };
	
	        cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	    };
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method setDockTargetColor
	     * @description
	     * Set the color of docking target. MAC OS only.
	     *
	     * @param red {Number} - Value of the red color 0-255
	     * @param green {Number} - Value of the green color 0-255
	     * @param blue {Number} - Value of the blue color 0-255
	     *
	     * @since 11.7.0
	     *
	     * @throw No Capability - Thrown if there is no support for docking targer color.
	     */
	    this.setDockTargetColor = function(red, green, blue)
	    {
	        if (!cwic.SystemController.getCapabilities().nativeWindowDockingSupport ||
	            !cwic.SystemController.getCapabilities().nativeWindowDockingTargetSupport)
	        {
	            throw Error("No capability");
	        }
	
	        var dockingWindow = getDockingWindow();
	        dockingWindow.setDockColor(red, green, blue);
	
	        var messageType = "setDockTargetColor";
	        var messageData = {
	            redValue: red,
	            greenValue: green,
	            blueValue: blue,
	            windowType: m_Type
	        };
	
	        cwic.MessageSender.sendMessage(messageType, messageData);
	    };
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method dock
	     * @description
	     * Dock native video window to an HTML element of the specified DOM window.
	     *
	     * @param htmlElement {HTMLElement} - HTML element to which native video window will be docked
	     * @param DOMWindow {Window} - DOM window to which HTML Element belongs. If this parameter isn't specified
	     * current DOM window is used.
	     *
	     * @throws No Capability - Thrown if docking isn't supported.
	     * @throws Invalid HTML Element - Thrown if htmlElement parameter is not instance of HTMLElement.
	     *
	     * @since 11.7.0
	     */
	    this.dock = function(htmlElement, DOMWindow)
	    {
	        if (!cwic.SystemController.getCapabilities().nativeWindowDockingSupport)
	        {
	            throw Error("No capability");
	        }
	
	        var frame = DOMWindow ? DOMWindow : window;
	
	        if(!(htmlElement instanceof frame.HTMLElement))
	        {
	            throw Error("Invalid HTML Element");
	        }
	
	        var dockingWindow = getDockingWindow();
	
	
	        dockingWindow.isDocked = true;
	
	        dockingWindow.externalWindowDocking = cwic.SystemController.getCapabilities().nativeWindowDockingSupport;
	        dockingWindow.showingDockingTarget = cwic.SystemController.getCapabilities().nativeWindowDockingTargetSupport;
	
	        dockingWindow.resetPosition();
	        dockingWindow.frame = frame;
	        dockingWindow.element = htmlElement;
	
	        var position = dockingWindow.updateOffsets(dockingWindow.element.getBoundingClientRect());
	        dockingWindow.sendMessageToAddOn("dockExternalWindow", position, windowType);
	        dockingWindow.updatePosition();
	    };
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method undock
	     * @description
	     * Undock native video window from HTML element.
	     *
	     * @throws No Capability - Thrown if docking isn't supported.
	     * @since 11.7.0
	     *
	     * @throw No Capability - Thrown if docking isn't supported.
	     */
	    this.undock = function()
	    {
	        if (!cwic.SystemController.getCapabilities().nativeWindowDockingSupport)
	        {
	            throw Error("No Capability");
	        }
	
	        var dockingWindow = getDockingWindow();
	
	        dockingWindow.hideDockingTarget();
	        dockingWindow.isDocked = false;
	
	        var messageType = 'undockExternalWindow';
	        var messageData = {
	            windowType: windowType
	        };
	
	        cwic.MessageSender.sendMessage(messageType, messageData);
	    };
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method showControls
	     * @description
	     * Shows native video window controls.
	     *
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     */
	    this.showControls = function(errorHandler)
	    {
	        var messageType = 'setExternalWindowShowControls';
	        var messageData = {
	            showControls : true,
	            windowType : m_Type
	        };
	
	        cwic.MessageSender.sendMessage(messageType,messageData, errorHandler);
	    };
	
	    /**
	     * @memberof NativeVideoWindow
	     * @method hideControls
	     * @description
	     * Hides native video window controls.
	     *
	     * @param [errorHandler]{Function} - Called if error has occurred in add-on.
	     */
	    this.hideControls = function(errorHandler)
	    {
	        var messageType = 'setExternalWindowShowControls';
	        var messageData = {
	            showControls : false,
	            windowType : m_Type
	        };
	
	        cwic.MessageSender.sendMessage(messageType,messageData, errorHandler);
	    };
	}
	
	module.exports.NativeVideoWindow = NativeVideoWindow;


/***/ },
/* 129 */
/***/ function(module, exports, __webpack_require__) {

	//var cwicState = require('./cwicState.js');
	//var about = require('../api/about.js');
	//var sendClientRequest = require('../communication/sendClientRequest.js');
	var $ = __webpack_require__(6);
	//var log = require('../utils/log.js');
	
	// System Modules
	var MessageSenderModule = __webpack_require__(72)
	var SystemControllerModule = __webpack_require__(120);
	
	var cwic = {
	    MessageSender : MessageSenderModule.MessageSender,
	    SystemController : SystemControllerModule.SystemController
	}
	
	
	function WindowPosition()
	{
	    this.top        = 0;
	    this.left       = 0;
	    this.height     = 0;
	    this.width      = 0;
	    this.cropTop    = 0;
	    this.cropLeft   = 0;
	    this.cropHeight = 0;
	    this.cropWidth  = 0;
	}
	
	function Color(red, green, blue)
	{
	    if(arguments.length !== 3)
	    {
	        throw "Invalid number of arguments for color object";
	    }
	
	    for(var i=0; i<arguments.length; arguments++)
	    {
	        var colorValue = arguments[i];
	        if(colorValue < 0 || colorValue > 255)
	        {
	            throw "Invalid color value.";
	        }
	    }
	
	    this.red   = red;
	    this.green = green;
	    this.blue  = blue;
	
	    this.toString = function()
	    {
	        var stringColor = "rgb(" + this.red + "," + this.green + "," + this.blue + ")";
	        return stringColor;
	    }
	}
	
	
	function DockingWindow(windowType)
	{
	    this.type     = windowType;
	    //this._about   = about();
	    this.isDocked = true;
	    this.isVideoBeingReceived = false;
	    this.frame    = window;
	    this.element  = null;
	    this.position = new WindowPosition();
	    this.targetDiv = null;
	    this.dockingColor = new Color(255,0,255);
	    this.externalWindowDocking = false;
	    this.showingDockingTarget   = false;
	
	
	    this.targetDivStyle = {
	        //'background-color': 'magenta',
	        'width': '8px',
	        'height': '8px',
	        'position': 'fixed',
	        'border-right': '8px solid black',
	        'z-index': '2147483647'
	    };
	
	    this.setDockColor = function(red, green, blue)
	    {
	        if(arguments.length !== 3)
	        {
	            throw "Invalid number of arguments for color object";
	        }
	
	        this.dockingColor = new Color(red, green, blue);
	        $(this.targetDiv).css("background-color", this.dockingColor.toString());
	    };
	
	
	    this.isPositionChanged = function(newPosition)
	    {
	        var positionChanged = !(
	            this.position.top        === newPosition.top &&
	            this.position.left       === newPosition.left &&
	            this.position.left       === newPosition.left &&
	            this.position.height     === newPosition.height &&
	            this.position.width      === newPosition.width &&
	            this.position.cropTop    === newPosition.cropTop &&
	            this.position.cropLeft   === newPosition.cropLeft &&
	            this.position.cropHeight === newPosition.cropHeight &&
	            this.position.cropWidth  === newPosition.cropWidth
	        );
	
	        return positionChanged;
	    }
	
	
	    this.updatePosition = function ()
	    {
	        if (this.isDocked)
	        {
	            var newPosition = this.updateOffsets(this.element.getBoundingClientRect());
	
	            if(this.isPositionChanged(newPosition))
	            {
	                this.resetPosition();
	                this.sendMessageToAddOn("dockUpdate", newPosition, this.type);
	                $.extend(this.position, newPosition);
	            }
	
	            window.setTimeout(this.updatePosition.bind(this), 10);
	        }
	    };
	
	    this.updateOffsets = function (positionWithinInnermostFrame)
	    {
	        var currentFrame = this.frame,
	            currentFrameHeight = $(currentFrame).height(),
	            currentFrameWidth = $(currentFrame).width(),
	            currentFrameRect,
	            parentFrameWidth,
	            parentFrameHeight,
	            position = $.extend({
	                cropTop: 0,
	                cropLeft: 0,
	                cropBottom: 0,
	                cropRight: 0,
	                cropWidth: 0,
	                cropHeight: 0
	            }, positionWithinInnermostFrame),
	            frameBorderOffset = 0,
	            borderTopOffset = 0,
	            borderLeftOffset = 0,
	            paddingTopOffset = 0,
	            paddingLeftOffset = 0;
	
	        // we need to take into account the devicePixelRatio and the CSS zoom property
	        // it won't work if css zoom is set on some of parent elements
	        var scaleCoefficient = currentFrame.devicePixelRatio * $(this.element).css('zoom');
	
	        var scrollX = 0;
	        var scrollY = 0;
	
	        if (('ontouchstart' in window) && (navigator.maxTouchPoints > 1))
	        {
	
	            //running on touch-capable device
	            var inner = currentFrame.innerWidth;
	            var hasScrollbar = inner - currentFrame.document.documentElement.clientWidth;
	            this.lastZoomFactor = this.lastZoomFactor || 1;
	
	            // scrollbar width changes when zooming, we need to calculate it for each scale level
	            // on pinch zoom, hasScrollbar very quickly goes below zero, and we should skip that case (no scrollbars on pinch-zoom)
	            // ...basically just an aproximation which works currentlly
	            if (hasScrollbar > 0) {
	                inner -= hasScrollbar;
	            } else {
	                var scrollBarWidth = 21;
	                inner -= (scrollBarWidth / this.lastZoomFactor);
	            }
	
	            var pinchZoomCoefficient = currentFrame.document.documentElement.clientWidth / inner;
	
	            scaleCoefficient *= pinchZoomCoefficient;
	            this.lastZoomFactor = scaleCoefficient;
	
	            var scrollSizeX = $(currentFrame.document).width() - $(currentFrame).width();
	            var scrollSizeY = $(currentFrame.document).height() - $(currentFrame).height();
	
	            if (pinchZoomCoefficient > 1.01) {
	                scrollX = currentFrame.scrollX;
	                scrollY = currentFrame.scrollY;
	
	                // this is complex logic dealing with case when the page (pinchZoom=0) has scrollbars
	                // In that case, when the page is pinche-zoomed, scrollX and scrollY are no longer accurate in providing position of docking container
	                var diffY = scrollSizeY - scrollY;
	
	                // while diff is >0, position.top represent accurate top position
	                // when it goes below 0 (meaning that "original" scrollbar hit the end), position.top gets stuck and then diff value represent how much container is moved
	                if (diffY >= 0) {
	                    scrollY = 0;
	                } else {
	                    scrollY = Math.abs(diffY);
	                }
	
	                var diffX = scrollSizeX - scrollX;
	
	                if (diffX >= 0) {
	                    scrollX = 0;
	                } else {
	                    scrollX = Math.abs(diffX);
	                }
	
	                if (diffY >= 0) {
	                    this.initPosY = position.top + currentFrame.scrollY;
	                }
	
	                if (
	                    position.top > this.initPosY - scrollSizeY &&
	                    diffY < 0
	                ) {
	                    position.top = this.initPosY - scrollSizeY;
	                }
	
	                if (diffX >= 0) {
	                    this.initPosX = position.left + currentFrame.scrollX;
	                }
	
	                if (
	                    position.left > this.initPosX - scrollSizeX &&
	                    diffX < 0
	                ) {
	                    position.left = this.initPosX - scrollSizeX;
	                }
	            }
	
	        }
	
	        position.left -= scrollX;
	        position.top -= scrollY;
	
	        // calculating crop values for innermost iframe
	        position.cropTop = (position.top < 0) ?
	            Math.abs(position.top) : 0;
	
	        position.cropLeft = (position.left < 0) ?
	            Math.abs(position.left) : 0;
	
	        position.cropBottom = Math.max(position.bottom - currentFrameHeight, 0);
	        position.cropRight = Math.max(position.right - currentFrameWidth, 0);
	
	        while (currentFrame != currentFrame.top)
	        {
	            currentFrameRect = currentFrame.frameElement.getBoundingClientRect();
	            parentFrameWidth = $(currentFrame.parent).width();
	            parentFrameHeight = $(currentFrame.parent).height();
	
	            // !! converts to boolean: 0 and NaN map to false, the rest of the numbers map to true
	            if (currentFrame.frameElement.frameBorder === "" || !!parseInt(currentFrame.frameElement.frameBorder, 10))
	            {
	                // after testing on Chrome, whenever a frameBorder is present, it's size is 2px
	                frameBorderOffset = 2;
	            }
	            else
	            {
	                frameBorderOffset = 0;
	            }
	
	            if (currentFrame.frameElement.style.borderTopWidth === "")
	            {
	                borderTopOffset = frameBorderOffset;
	            }
	            else
	            {
	                borderTopOffset = parseInt(currentFrame.frameElement.style.borderTopWidth || 0, 10);
	            }
	
	            paddingTopOffset = parseInt(currentFrame.frameElement.style.paddingTop || 0, 10);
	
	            if (currentFrameRect.top < 0)
	            {
	                if (position.top + position.cropTop < 0)
	                {
	                    position.cropTop += Math.abs(currentFrameRect.top);
	                }
	                else if (Math.abs(currentFrameRect.top) - (position.top + position.cropTop + borderTopOffset + paddingTopOffset) > 0)
	                {
	                    position.cropTop += Math.abs(Math.abs(currentFrameRect.top) - (position.top + position.cropTop + borderTopOffset + paddingTopOffset));
	                }
	            }
	
	            if (currentFrameRect.top + borderTopOffset + paddingTopOffset + position.top + position.height - position.cropBottom > parentFrameHeight)
	            {
	                position.cropBottom = currentFrameRect.top + borderTopOffset + paddingTopOffset + position.top + position.height - parentFrameHeight;
	            }
	
	            position.top += currentFrameRect.top + borderTopOffset + paddingTopOffset;
	
	            if (currentFrame.frameElement.style.borderLeftWidth === "")
	            {
	                borderLeftOffset = frameBorderOffset;
	            }
	            else
	            {
	                borderLeftOffset = parseInt(currentFrame.frameElement.style.borderLeftWidth || 0, 10);
	            }
	
	            paddingLeftOffset = parseInt(currentFrame.frameElement.style.paddingLeft || 0, 10);
	
	            if (currentFrameRect.left < 0)
	            {
	                if (position.left + position.cropLeft < 0)
	                {
	                    position.cropLeft += Math.abs(currentFrameRect.left);
	                }
	                else if (Math.abs(currentFrameRect.left) - (position.left + position.cropLeft + borderLeftOffset + paddingLeftOffset) > 0)
	                {
	                    position.cropLeft += Math.abs(Math.abs(currentFrameRect.left) - (position.left + position.cropLeft + borderLeftOffset + paddingLeftOffset));
	                }
	            }
	
	            if (currentFrameRect.left + borderLeftOffset + paddingLeftOffset + position.left + position.width - position.cropRight > parentFrameWidth)
	            {
	                position.cropRight = currentFrameRect.left + borderLeftOffset + paddingLeftOffset + position.left + position.width - parentFrameWidth;
	            }
	
	            position.left += currentFrameRect.left + borderLeftOffset + paddingLeftOffset;
	            currentFrame = currentFrame.parent;
	        }
	
	        position.cropHeight = Math.max(position.height - (position.cropTop + position.cropBottom), 0);
	        position.cropTop = (position.height > position.cropTop) ? position.cropTop : 0;
	        position.cropWidth = Math.max(position.width - (position.cropLeft + position.cropRight), 0);
	        position.cropLeft = (position.width > position.cropLeft) ? position.cropLeft : 0;
	
	        //this._about = about();
	
	        // set target before scaling (Mac) - target is HTML element, it is "immune" to scaling
	        // 8 and 16 because of minimal width of magenta target
	        if (position.cropHeight > 8 && position.cropWidth > 16 && cwic.SystemController.getCapabilities().nativeWindowDockingTargetSupport)
	        //if (this.isVideoBeingReceived && position.cropHeight > 8 && position.cropWidth > 16 && this._about.capabilities.showingDockingTarget)
	        {
	            this.setTarget(position);
	        }
	        else
	        {
	            $(this.targetDiv).css({"display": "none"});
	        }
	
	        // include scale coefficient
	        position.left = Math.round(scaleCoefficient * position.left);
	        position.top = Math.round(scaleCoefficient * position.top);
	        position.width = Math.ceil(scaleCoefficient * position.width);
	        position.height = Math.ceil(scaleCoefficient * position.height);
	        position.cropLeft = Math.round(scaleCoefficient * position.cropLeft) || 0;
	        position.cropTop = Math.round(scaleCoefficient * position.cropTop) || 0;
	        position.cropWidth = Math.floor(scaleCoefficient * position.cropWidth) || 0;
	        position.cropHeight = Math.floor(scaleCoefficient * position.cropHeight) || 0;
	        position.scaleCoefficient = scaleCoefficient;
	
	        return position;
	    };
	
	    this.sendMessageToAddOn = function (msgName, position, windowType)
	    {
	        var addOnMessageContent =
	        {
	            offsetX: position.left,
	            offsetY: position.top,
	            width: position.width,
	            height: position.height,
	            cropOffsetX: position.cropLeft,
	            cropOffsetY: position.cropTop,
	            cropWidth: position.cropWidth,
	            cropHeight: position.cropHeight,
	            windowType: windowType
	        };
	
	        var scaleCoefficient = position.scaleCoefficient;
	
	        if (msgName === "dockExternalWindow")
	        {
	            addOnMessageContent.title = this.frame.top.document.title ||
	                (this.frame.top.location.host + this.frame.top.location.pathname + this.frame.top.location.search);
	        }
	        else
	        {
	            addOnMessageContent.pageWidth = Math.floor($(this.frame.top).width() * scaleCoefficient);
	            addOnMessageContent.pageHeight = Math.floor($(this.frame.top).height() * scaleCoefficient);
	        }
	
	        cwic.MessageSender.sendMessage(msgName, addOnMessageContent); // TODO: couple of ms could be saved if we call _plugin.api.sendRequest directly.
	    };
	
	    this.resetPosition = function ()
	    {
	        if (this.targetDiv)
	        {
	            //$(this.targetDiv).css('display', 'none');
	        }
	
	    };
	
	    this.setTarget = function (position) {
	        // if the overlay DOM Element is only partially visible, the target should be at the top left of the visible part of the DOM Element
	        var targetTop = (position.top + position.cropTop > 0 || position.bottom <= /* magenta target height */ 8) ?
	            Math.ceil(position.top + position.cropTop) : 0;
	        var targetLeft = (position.left + position.cropLeft > 0 || position.right <= /* magenta target (+ border) width */ 16) ?
	            Math.ceil(position.left + position.cropLeft) : 0;
	
	        this.createTargetDivIfNeeded();
	        $(this.targetDiv).css({"display": "block", "top": targetTop, "left": targetLeft});
	    };
	
	    this.createTargetDivIfNeeded = function ()
	    {
	        if (!this.targetDiv)
	        {
	            this.targetDiv = this.frame.document.createElement("div");
	            this.element.appendChild(this.targetDiv);
	            //this.targetDiv = this.frame.top.document.createElement("div");
	            //this.frame.top.document.body.appendChild(this.targetDiv);
	            $(this.targetDiv).css(this.targetDivStyle);
	            $(this.targetDiv).css('background-color', this.dockingColor.toString());
	        }
	    };
	
	    this.hasDockingCapabilities = function ()
	    {
	        var caps;
	        try
	        {
	            caps = cwic.SystemController.getCapabilities().nativeWindowDockingSupport;
	        }
	        catch (e)
	        {
	            caps = null;
	        }
	
	        return caps;
	    };
	
	    this.hideDockingTarget = function()
	    {
	        $(this.targetDiv).css({"display": "none"});
	    }
	}
	
	module.exports.PreviewDockingWindow      = new DockingWindow("preview");
	module.exports.RemoteDockingWindow       = new DockingWindow("remote");
	module.exports.ScreenShareDockingWindow  = new DockingWindow("share");


/***/ },
/* 130 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var NativeVideoWindowModule = __webpack_require__(128);
	
	var cwic = {
	    NativeVideoWindow : NativeVideoWindowModule.NativeVideoWindow
	};
	
	/**
	 * @class NativeScreenShareWindow
	 * @extends  NativeVideoWindow
	 * @classdesc
	 * Represents native video window in which screen share video is rendered.
	 *
	 * @description
	 * This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function NativeScreenShareWindow()
	{
	    cwic.NativeVideoWindow.call(this, 'share');
	}
	
	NativeScreenShareWindow.prototype = Object.create(cwic.NativeVideoWindow.prototype);
	NativeScreenShareWindow.prototype.constructor = NativeScreenShareWindow();
	
	
	module.exports.NativeScreenShareWindow = new NativeScreenShareWindow();


/***/ },
/* 131 */
/***/ function(module, exports, __webpack_require__) {

	// Required Modules
	var NativeVideoWindowModule = __webpack_require__(128);
	var MessageSenderModule     = __webpack_require__(72);
	
	var cwic = {
	    NativeVideoWindow : NativeVideoWindowModule.NativeVideoWindow,
	    MessageSender     : MessageSenderModule.MessageSender
	};
	
	/**
	 * @class NativeConversationWindow
	 * @extends  NativeVideoWindow
	 * @classdesc
	 * Represents native video window in which telephony conversation video is rendered.
	 *
	 * @description
	 * This class cannot be instantiated.
	 *
	 * @since 11.7.0
	 */
	function NativeConversationWindow()
	{
	    cwic.NativeVideoWindow.call(this, 'remote');
	}
	
	NativeConversationWindow.prototype = Object.create(cwic.NativeVideoWindow.prototype);
	NativeConversationWindow.prototype.constructor = new NativeConversationWindow();
	
	
	/**
	 * @memberof NativeConversationWindow
	 * @method showVideoForConversation
	 * @description
	 * Show video for given telephony conversation.
	 *
	 * @param conversation {TelephonyConversation} - Telephony conversation for which video will be rendered.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	NativeConversationWindow.prototype.showVideoForConversation = function(conversation, errorHandler)
	{
	    var messageType = 'showCallInExternalWindow';
	    var messageData = {
	        callId: conversation.ID
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof NativeConversationWindow
	 * @method showSelfView
	 * @description
	 * Show self-view video.
	 *
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	NativeConversationWindow.prototype.showSelfView = function(errorHandler)
	{
	    var messageType = 'setExternalWindowShowSelfViewPip';
	    var messageData = {
	        showSelfViewPip: 'true'
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof NativeConversationWindow
	 * @method hideSelfView
	 * @description
	 * Hide self-view video.
	 *
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	NativeConversationWindow.prototype.hideSelfView = function(errorHandler)
	{
	    var messageType = 'setExternalWindowShowSelfViewPip';
	    var messageData = {
	        showSelfViewPip: false
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof NativeConversationWindow
	 * @method showSelfViewBorder
	 * @description
	 * Show border for self-view video.
	 *
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	NativeConversationWindow.prototype.showSelfViewBorder = function(errorHandler)
	{
	    var messageType = 'setExternalWindowShowSelfViewPipBorder';
	    var messageData = {
	        showSelfViewPipBorder: true
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof NativeConversationWindow
	 * @method hideSelfViewBorder
	 * @description
	 * Hide border for self-view video.
	 *
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	NativeConversationWindow.prototype.hideSelfViewBorder = function(errorHandler)
	{
	    var messageType = 'setExternalWindowShowSelfViewPipBorder';
	    var messageData = {
	        showSelfViewPipBorder: false
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof NativeConversationWindow
	 * @method setSelfViewPosition
	 * @description
	 * Set position of self-view video relative to conversation window.
	 *
	 * @param left {Number} - Distance from left edge of window in percentages.
	 * @param top {Number} - Distance from top edge of window in percentages.
	 * @param right {Number} - Distance from right edge of window in percentages.
	 * @param bottom {Number} - Distance from bottom edge of window in percentages.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @since 11.7.0
	 */
	NativeConversationWindow.prototype.setSelfViewPosition = function(left, top, right, bottom, errorHandler)
	{
	    var messageType = 'setExternalWindowSelfViewPipPosition';
	    var messageData = {
	        pipLeft  : left,
	        pipTop   : top,
	        pipRight : right,
	        pipBottom: bottom
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	module.exports.NativeConversationWindow = new NativeConversationWindow();


/***/ },
/* 132 */
/***/ function(module, exports, __webpack_require__) {

	// Window Modules
	var VideoWindowModule = __webpack_require__(133);
	
	// Telephony Modules
	var TelephonyConversationModule = __webpack_require__(111);
	
	
	var cwic = {
	    VideoWindow : VideoWindowModule.VideoWindow,
	    TelephonyConversation : TelephonyConversationModule.TelephonyConversation
	};
	
	/**
	 * @class ScreenShareWindow
	 * @classdesc
	 * In-browser video window in which incoming screen share for specified {@link TelephonyConversation} is rendered.
	 *
	 * @description
	 * It is instantiated through [WindowController.createVideoWindow()]{@link WindowController.createVideoWindow}.
	 * @since 11.7.0
	 */
	function ScreenShareWindow(htmlElement, DOMWindow)
	{
	    var m_VideoWindow = new cwic.VideoWindow(htmlElement, DOMWindow);
	
	    /**
	     * @memberof ScreenShareWindow
	     * @method showForConversation
	     * @description
	     * Start rendering screen share video for specified conversation.
	     *
	     * @param conversation {TelephonyConversation} - Telephony conversation for which video rendering will start.
	     * @since 11.7.0
	     */
	    this.showForConversation = function(conversation)
	    {
	        var methodName = 'addShareWindow';
	
	        m_VideoWindow.element.style.width  = "100%";
	        m_VideoWindow.element.style.height = "100%";
	        m_VideoWindow.executeMethod(methodName, conversation.ID);
	    };
	
	    /**
	     * @memberof ScreenShareWindow
	     * @method hidForConversation
	     * @description
	     * Stop rendering screen share video for specified conversation.
	     *
	     * @param conversation {TelephonyConversation} - Telephony conversation for which video rendering will stop.
	     * @since 11.7.0
	     */
	    this.hideForConversation = function(conversation)
	    {
	        var methodName = 'removeShareWindow';
	
	        m_VideoWindow.element.style.width  = "0%";
	        m_VideoWindow.element.style.height = "0%";
	        m_VideoWindow.executeMethod(methodName, conversation.ID);
	    };
	}
	
	module.exports.ScreenShareWindow = ScreenShareWindow;


/***/ },
/* 133 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var SystemControllerModule = __webpack_require__(120);
	
	// Utilities Modules
	var generateUniqueID = __webpack_require__(73);
	
	var cwic = {
	    SystemController : SystemControllerModule.SystemController
	};
	
	/**
	 * @class VideoWindow
	 * @classdesc
	 * Video window represents window in which video will be rendered. This class can only be used in following Browsers:
	 * <br>
	 * <ul>
	 *     <li> Mozzile Firefox
	 *     <li> Internet Explorer
	 *     <li> Safari
	 * </ul>
	 * <br>
	 *
	 * @param htmlElement {HTMLElement} - Element in which video will be rendered.
	 * @param [DOMWindow] {Window} - DOM window that is a parent of HTML element.
	 *
	 * @throw Invalid HTML Type - Thrown if HTML element is invalid.
	 * @throw Element Requires ID - Thrown if HTML element doesn't have ID.
	 */
	function VideoWindow(htmlElement, DOMWindow)
	{
	    var domWindow = DOMWindow ? DOMWindow : window;
	
	    if(!(htmlElement instanceof domWindow.HTMLElement))
	    {
	        throw Error("Invalid HTML Type");
	    }
	
	    if(!htmlElement.id)
	    {
	        throw Error("Element Requires ID");
	    }
	
	    var videoWindow = domWindow.document.createElement("object");
	    videoWindow.type = 'application/x-cisco-cwc-videocall';
	    videoWindow.id   = 'videoWindow-' + htmlElement.id + generateUniqueID();
	    videoWindow.style.width  = "0%";
	    videoWindow.style.height = "0%";
	
	    htmlElement.appendChild(videoWindow);
	    
	    this.element = videoWindow;
	
	    this.executeMethod = function(methodName, callID)
	    {
	        var clientID  = cwic.SystemController.getInstanceID();
	        var messageID = generateUniqueID();
	        var url       = window.location.href;
	        var hostname  = window.location.hostname;
	        var name      = window.document.title;
	
	        var messageParameters = [messageID, clientID, url, hostname, name];
	
	        if(callID)
	        {
	            messageParameters.unshift(callID);
	        }
	
	        videoWindow[methodName].apply(this, messageParameters);
	    };
	
	    this.executeMethod('configure');
	}
	
	module.exports.VideoWindow = VideoWindow;
	
	


/***/ },
/* 134 */
/***/ function(module, exports, __webpack_require__) {

	// Window Modules
	var VideoWindowModule = __webpack_require__(133);
	
	// Telephony Modules
	var TelephonyConversationModule = __webpack_require__(111);
	
	
	var cwic = {
	    VideoWindow : VideoWindowModule.VideoWindow,
	    TelephonyConversation : TelephonyConversationModule.TelephonyConversation
	};
	
	/**
	 * @class ConversationWindow
	 * @classdesc
	 * In-browser video window in which [Telephony Conversation's]{@link TelephonyConversation} conversation video
	 * (incoming video from remote participant) is rendered.
	 *
	 * @description
	 * It is instantiated through [WindowController.createVideoWindow()]{@link WindowController.createVideoWindow}.
	 * @since 11.7.0
	 */
	function ConversationWindow(htmlElement, DOMWindow)
	{
	    var m_VideoWindow = new cwic.VideoWindow(htmlElement, DOMWindow);
	
	    /**
	     * @memberof ConversationWindow
	     * @method showForConversation
	     * @description
	     * Start rendering video for specified conversation.
	     *
	     * @param conversation {TelephonyConversation} - Telephony conversation for which video rendering will start.
	     * @since 11.7.0
	     */
	    this.showForConversation = function(conversation)
	    {
	        var methodName = 'addWindowToCall';
	
	        m_VideoWindow.element.style.width  = "100%";
	        m_VideoWindow.element.style.height = "100%";
	        m_VideoWindow.executeMethod(methodName, conversation.ID);
	
	        var methodName = 'startRemoteVideo';
	        
	        m_VideoWindow.executeMethod(methodName, conversation.ID);
	    };
	
	    /**
	     * @memberof ConversationWindow
	     * @method hideForConversation
	     * @description
	     * Stop rendering video for specified conversation.
	     *
	     * @param conversation {TelephonyConversation} - Telephony conversation for which video rendering will stop.
	     * @since 11.7.0
	     */
	    this.hideForConversation = function(conversation)
	    {
	        var methodName = 'removeWindowFromCall';
	
	        m_VideoWindow.element.style.width  = "0%";
	        m_VideoWindow.element.style.height = "0%";
	        m_VideoWindow.executeMethod(methodName, conversation.ID);
	    };
	}
	
	module.exports.ConversationWindow = ConversationWindow;


/***/ },
/* 135 */
/***/ function(module, exports, __webpack_require__) {

	// Window Modules
	var VideoWindowModule = __webpack_require__(133);
	
	var cwic = {
	    VideoWindow: VideoWindowModule.VideoWindow
	};
	
	/**
	 * @class PreviewWindow
	 * @classdesc
	 * In-browser video window in which local camera feed is rendered.
	 *
	 * @description
	 * It is instantiated through [WindowController.createVideoWindow()]{@link WindowController.createVideoWindow}.
	 * @since 11.7.0
	 */
	function PreviewWindow(htmlElement, DOMWindow)
	{
	    var m_VideoWindow = new cwic.VideoWindow(htmlElement, DOMWindow);
	
	    /**
	     * @memberof PreviewWindow
	     * @method show
	     * @description
	     * Show preview video window. Once shown, local camera feed will start being rendered.
	     *
	     * @since 11.7.0
	     */
	    this.show = function()
	    {
	        var methodName = 'addPreviewWindow';
	
	        m_VideoWindow.element.style.width  = "100%";
	        m_VideoWindow.element.style.height = "100%";
	        m_VideoWindow.executeMethod(methodName);
	    };
	
	    /**
	     * @memberof PreviewWindow
	     * @method hide
	     * @description
	     * Hide preview video window. Once hidden, local camera feed will stop being rendered.
	     *
	     * @since 11.7.0
	     */
	    this.hide = function()
	    {
	        var methodName = 'removePreviewWindow';
	
	        m_VideoWindow.element.style.width  = "0%";
	        m_VideoWindow.element.style.height = "0%";
	        m_VideoWindow.executeMethod(methodName);
	    };
	}
	
	module.exports.PreviewWindow = PreviewWindow;

/***/ },
/* 136 */
/***/ function(module, exports, __webpack_require__) {

	// System Modules
	var MessageReceiverModule = __webpack_require__(67);
	var MessageSenderModule   = __webpack_require__(72);
	
	// Certificate Modules
	var InvalidCertificateModule = __webpack_require__(137);
	
	var cwic = {
	    MessageReceiver : MessageReceiverModule.MessageReceiver,
	    MessageSender   : MessageSenderModule.MessageSender,
	    InvalidCertificate : InvalidCertificateModule.InvalidCertificate
	};
	
	/**
	 * @class CertificateController
	 * @classdesc
	 * Certificate controller is responsible for handling invalid certificates.
	 *
	 * @description This class cannot be instantiated.
	 * @since 11.7.0
	 */
	function CertificateController()
	{
	    var m_EventHandlers = {};
	
	    cwic.MessageReceiver.addMessageHandler('invalidcertificate', onInvalidCertificate)
	
	    function onInvalidCertificate(content)
	    {
	        var eventHandler = m_EventHandlers['onInvalidCertificate'];
	        if(eventHandler)
	        {
	            var invalidCertificate = new cwic.InvalidCertificate(content);
	
	            var reasons = [];
	            for(var index = 0; index<content.invalidReasons.length; index++)
	            {
	                reasons.push(content.invalidReasons[index].invalidReason);
	            }
	
	            eventHandler(invalidCertificate, reasons, content.allowUserToAccept);
	        }
	    }
	
	    /**
	     * @memberof CertificateController
	     * @method addEventHandler
	     * @description Add handler function for Certificate Controller's event.
	     *
	     * @param eventName {String} - Name of the event.
	     * @param handler {Function} - Function that will be called when event is fired.
	     *
	     * @since 11.7.0
	     */
	    this.addEventHandler = function(eventName, eventHandler)
	    {
	        m_EventHandlers[eventName] = eventHandler;
	    };
	
	    /**
	     * @memberof CertificateController
	     * @method removeEventHandler
	     * @description Remove handler function for Certificate Controller's event.
	     *
	     * @param eventName {String} - Name of the event.
	     *
	     * @since 11.7.0
	     */
	    this.removeEventHandler = function(eventName)
	    {
	        delete m_EventHandlers[eventName];
	    };
	}
	
	/**
	 * @memberof CertificateController
	 * @method acceptInvalidCertificate
	 * @description
	 * Accepts invalid certificate. Invalid certificate can be obtained through "onInvalidCertificate" event.
	 *
	 * @param certificate {InvalidCertificate} - Invalid certificate to be accepted.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if certificate is not instance of {@link InvalidCertificate}.
	 *
	 * @since 11.7.0
	 */
	CertificateController.prototype.acceptInvalidCertificate = function (certificate, errorHandler)
	{
	    if(!(certificate instanceof cwic.InvalidCertificate))
	    {
	        throw Error("Invalid Object");
	    }
	
	    var messageType = 'handleInvalidCertificate';
	    var messageData = {
	        certFingerprint: certificate.fingerprint,
	        accept : true
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	/**
	 * @memberof CertificateController
	 * @method rejectInvalidCertificate
	 * @description
	 * Rejects invalid certificate. Invalid certificate can be obtained through "onInvalidCertificate" event.
	 *
	 * @param certificate {InvalidCertificate} - Invalid certificate to be accepted.
	 * @param [errorHandler] {Function} - Called if error has occurred in add-on.
	 *
	 * @throw Invalid Object - Thrown if certificate is not instance of {@link InvalidCertificate}.
	 *
	 * @since 11.7.0
	 */
	CertificateController.prototype.rejectInvalidCertificate = function (certificate, errorHandler)
	{
	    if(!(certificate instanceof cwic.InvalidCertificate))
	    {
	        throw Error("Invalid Type");
	    }
	
	    var messageType = 'handleInvalidCertificate';
	    var messageData = {
	        certFingerprint: certificate.fingerprint,
	        accept : false
	    };
	
	    cwic.MessageSender.sendMessage(messageType, messageData, errorHandler);
	};
	
	module.exports.CertificateController = new CertificateController();
	
	


/***/ },
/* 137 */
/***/ function(module, exports) {

	/**
	 * @class InvalidCertificate
	 *
	 * Invalid Certificate
	 */
	function InvalidCertificate(data)
	{
	    this.subjectCN   = data.certSubjectCN;
	    this.fingerprint = data.certFingerprint;
	    this.identifier  = data.identifierToDisplay;
	    this.referenceID = data.referenceId;
	}
	
	/**
	 * @memberof InvalidCertificate
	 * @member subjectCN
	 * @description
	 * Certificate Name
	 *
	 * @type {string}
	 * @since 11.7.0
	 */
	InvalidCertificate.prototype.subjectCN = "";
	
	/**
	 * @memberof InvalidCertificate
	 * @member fingerprint
	 * @description
	 * Certificate fingerprint
	 *
	 * @type {string}
	 * @since 11.7.0
	 */
	InvalidCertificate.prototype.fingerprint = "";
	
	/**
	 * @memberof InvalidCertificate
	 * @member identifier
	 * @description
	 * Identity that provides the certificate.
	 *
	 * @type {string}
	 * @since 11.7.0
	 */
	InvalidCertificate.prototype.identifier = "";
	
	/**
	 * @memberof InvalidCertificate
	 * @member referenceID
	 * @description
	 * Identity of the certificate provider matching the certificate owner.
	 *
	 * @type {string}
	 * @since 11.7.0
	 */
	InvalidCertificate.prototype.referenceID = "";
	
	module.exports.InvalidCertificate = InvalidCertificate;


/***/ }
/******/ ]);
//# sourceMappingURL=cwic-debug.js.map