/**
 * @fileOverview supplementaldocs.js
 * documentation of implicit objects/events that don't fit inline
 */
//
// Extra documentation for cwic.js that can't be inlined
// Put into a separate file to prevent clutter in cwic.js

// OBJECTS

// using virtual namespace for documentation purposes
// useful for objects that can are re-used and need to be cross-referenced
// also jsdoc support documenting nested anonymous virtual objects as return objects

/**
 * Object returned from about method
 * @name aboutObject
 * @namespace
 * @property {Object} javascript JavaScript properties
 * @property {String} javascript:version JavaScript version
 * @property {String} javascript:system_release Since: 2.1.1 <br>
 * JavaScript system release information.
 * @property {Object} jquery jQuery properties
 * @property {String} jquery:version jQuery version
 * @property {Object|null} channel Since: 3.1.0 <br> Properties of channel extension to add-on.  Null if add-on is loaded directly (NPAPI or ActiveX plug-in).
 * @property {String} channel:cwicExtId ID of the browser extension, if applicable.
 * @property {String} channel:objectId ID of html element that was appended to the web page.  If present, this element is used and required by the channel extension.
 * @property {String} channel:version CWIC version of the Cisco Chrome extension channel.
 * @property {String} channel:system_release System release information for the channel.
 * @property {Object|null} plugin Cisco Web Communicator add-on properties.  Null if add-on could not be loaded.
 * @property {Object} plugin:version Version details for the loaded add-on.
 * @property {String} plugin:version:plugin CWIC version of the add-on.
 * @property {String} plugin:version:system_release Since: 2.1.1 <br>
 * System release information for the add-on.
 * @property {Object} states cwic states
 * @property {String} states:system Current system state (e.g. "eReady")
 * @property {Object} states:device Currently registered device if any
 * @property {Object} capabilities Capabilities
 * @property {Boolean} capabilities:video Support for video calls.
 * @property {Boolean} capabilities:videoPluginObject Since: 3.1.0 <br>
 * Support for video plug-in objects in the browser, see {@link $.fn.cwic.createVideoWindow}.
 * If <tt>false</tt>, video can only be shown in an external video window.
 * @property {Boolean} capabilities:delayedUserAuth Since: 3.0.1 <br>
 * Support for delayed user authorization dialog.
 * @property {Boolean} capabilities:certValidation Since: 3.0.4 <br>
 * Support for certificate validation.  If <tt>true</tt>, certificate validation will be used unless explicitly disabled by 
 * calling {@link $.fn.cwic.disableCertValidation}.  If <tt>false</tt>, certificate validation will not be available.
 * @property {Boolean} capabilities:externalWindowDocking Since: 3.1.2 <br>
 * Support for external video window docking, see {@link $.fn.cwic.dock} and {@link $.fn.cwic.undock}.
 * @property {Object} upgrade Advice about possible upgrade.
 * @property {String} upgrade:javascript Relevant if add-on version is later than the cwic.js JavaScript. Can be one of<ul>
 * <li>"mandatory" The add-on and cwic.js versions are not compatible.</li>
 * <li>"recommended" The add-on may have features or bug fixes that require upgrade to cwic.js.</li>
 * <li>undefined if not relevant.</li></ul>
 * @property {String} upgrade:plugin Relevant if the cwic.js JavaScript version is later than the add-on. Can be one of<ul>
 * <li>"mandatory" The add-on and cwic.js versions are not compatible.</li>
 * <li>"recommended" The add-on may not provide all features or bug fixes available.</li>
 * <li>"unknown" Unable to determine version of the add-on.</li>
 * <li>undefined if not relevant.</li></ul>
 */
/**
 * @name device
 * @namespace
 * @property {String} name
 * @property {String} description
 * @property {String} modelDescription
 * @property {Boolean} isSoftPhone <tt>true</tt> if the device is a software phone
 * @property {Boolean} isDeskPhone <tt>true</tt> if the device is a hardware (desk) phone
 * @property {String[]} lineDNs Array of line DNs associated with this device (may be blank until connected to the device)
 * @property {Object} line Object containing info about currently active line
 * @property {String} serviceState One of <ul>
 * <li>"eUnknown"</li>
 * <li>"eInService"</li>
 * <li>"eOutOfService"</li></ul>
 * @property {String} [inService] Service state
 * @property {String} [serviceCause] Describes cause for current service state
 * @property {Boolean} [isDND] DND state
 * @property {String} [DNDType] DND type
 * @property {Boolean} exists The device exists and can be used
 */

/**
 * @name line
 * @namespace
 *
 * @property {String} directoryNumber DN of the line as represented in the directory
 * @property {String} name
 * @property {Boolean} exists <tt>true</tt> if the line exists
 */

/**
 * @name call
 * @namespace
 * @property {Number} callId Unique identifier of the call.
 * @property {Boolean} exists <tt>true</tt> if the call exists.
 * @property {Boolean} audioMuted Audio is muted.
 * @property {Boolean} videoMuted Video is muted.
 * @property {String} callState Current state of the call.  May be one of the following:<ul>
 * <li>"OffHook" - receiver lifted in CTI control mode, or preparing to make a call in either mode.</li>
 * <li>"OnHook" - call is ended or about to go OffHook. If !capabilities.canOriginateCall, then call is ended.</li>
 * <li>"Ringout" - remote party is ringing.</li>
 * <li>"Ringin" - incoming call.</li>
 * <li>"Proceed" - if already on a call in CTI mode and there's an incoming call it may be in "Proceed" state.</li>
 * <li>"Connected" - call is connected.</li>
 * <li>"Hold" - call is held.</li>
 * <li>"RemHold" - call is held on a shared-line device.</li>
 * <li>"Resume" - n/a.</li>
 * <li>"Busy" - remote line busy.</li>
 * <li>"Reorder" - call failed.</li>
 * <li>"Conference"</li>
 * <li>"Dialing" - dialing number.</li>
 * <li>"RemInUse" - call on a shared-line device.</li>
 * <li>"HoldRevert"</li>
 * <li>"Whisper"</li>
 * <li>"Parked"</li>
 * <li>"ParkRevert"</li>
 * <li>"ParkRetrieved"</li>
 * <li>"Preservation" - call is in preservation mode.</li>
 * <li>"WaitingForDigits"</li
 * ><li>"Spoof_Ringout"</li></ul>
 * @property {String} callType One of the following: <ul><li>"Placed"</li><li>"Received"</li><li>"Missed"</li>
 * @property {Date} start The <tt>new Date()</tt> when the conversationStart.cwic event was fired for the call.
 * @property {String} videoDirection One of the following: <ul><li>"Inactive"</li><li>"SendRecv"</li><li>"SendOnly"</li><li>"RecvOnly"</li></ul>
 * @property {Object} videoResolution Contains video resolution width and height values (in pixels). Included in the Call object any time the resolution changes.
 * @property {Boolean} isConference <tt>true</tt> if the call is a conference call.
 * @property {callcapability[]} capabilities A map that has capability name as the key and <tt>true</tt> as the value. 
 * Call capabilities indicate operations you can perform on the call, and they should be used to control enabling/disabling/state/access to parts of the UI. Important capabilities are:
 * <ul>
 * <li>canOriginateCall - can create new call</li>
 * <li>canAnswerCall - call can be answered</li>
 * <li>canHold - call can be Held</li>
 * <li>canResume - call can be Resumed</li>
 * <li>canEndCall - call can be Ended</li>
 * <li>canSendDigit - can send a DTMF digit to the call</li>
 * <li>canDirectTransfer - can tranfer call</li>
 * <li>canJoinAcrossLine - can join this call with another call on the same line with this capability also set</li>
 * <li>canImmediateDivert - can iDivert (e.g. to voicemail)</li>
 * <li>canUpdateVideoCapability - can update the videoDirection on the call. See {@link $.fn.cwic.updateConversation} examples.</li>
 * <li>canMuteAudio - can mute audio on the call</li>
 * <li>canUnmuteAudio - can unmute audio on the call</li>
 * <li>canMuteVideo - can mute video on the call</li>
 * <li>canUnmuteVideo - can unmute video on the call</li>
 * </ul>
 * @property {participant[]} participants Array of all remote participants on the call.
 * @property {participant} participant Remote participant (first entry in participants list).
 * @property {participant} localParticipant Local participant (your line info).
 */

/**
 * @name participant
 * @namespace
 * @property {} name
 * @property {} [number] Untranslated number for other participant
 * @property {} [translatedNumber] Number for other participant as translated using available Cisco Unified Communications Manager dial rules
 * @property {} [directoryNumber] Local line number as represented in Cisco Unified Communications Manager directory
 */

/**
 * @name registration
 * @namespace
 * @property {String} user The Cisco Unified Communications Manager end user name
 * @property {String} mode One of <ul><li>"SoftPhone"</li><li>"DeskPhone"</li></ul>
 * @property {device{}} devices Map keyed by device name of all devices available to authenticated user
 * @property {device} device The selected device
 * @property {line} line The selected line
 * @property {String|Object} password Cleartext password or encrypted password Object
 * @property {String} [password.encrpyted] Encrypted password
 * @property {String} [password.cipher] Cipher used to encrypt password
 * @property {String|Object} passphrase Alias for password
 * @property {String|Array|Object} cucm Original cucm parameter
 * @property {String|Array} [cucm.ccmcip] ccmcip addresses to attempt connection with
 * @property {String|Array} [cucm.tftp] tftp addresses to attempt connection with
 * Object representing Cisco Unified Communications Manager registration.  Many properties are set in the initial call to {@link $.fn.cwic.registerPhone}
 */

// EVENTS

/**
 * A conversation has just started. Note the conversation may not be connected yet, for example it is in the Alerting state.
 * The conversation container is passed as an extra event data.
 * @name $.fn.cwic#conversationStart
 * @event
 * @param {Object} event
 * @param {call} conversation
 * @param {DomNode} container
 */
/**
 * One or more properties of a conversation have changed, such as state or participants.
 * @name $.fn.cwic#conversationUpdate
 * @event
 * @param {Object} event
 * @param {call} conversation
 * @param {jQueryNodeArray} container
 */
/**
 * A conversation has ended.  The conversation was terminated by the local user, all participants left the conversation, or an error occurred (media failure for example).
 * @name $.fn.cwic#conversationEnd
 * @event
 * @param {Object} event
 * @param {call} conversation
 */
/**
 * A new conversation is being received, but has not yet started.  The newly created conversation container is passed as an extra event data.<br>
 * The application is responsible for attaching the container to the DOM and for managing the container lifetime.<br>
 * In the case the new conversation is accepted by calling startConversation, a conversationStart event is triggered afterwards.<br>
 * @name $.fn.cwic#conversationIncoming
 * @event
 * @param {Object} event
 * @param {call} conversation
 * @param {jQueryNodeArray} container
 */
/**
 * Called when the phone changes state.
 * @name $.fn.cwic#system
 * @event
 * @param {Object} event
 * @param {Object} event.phone
 * @param {String} event.phone.status One of <ul>
 * <li>"eConnectionTerminated"</li>
 * <li>"eIdle"</li>
 * <li>"eRegistering"</li>
 * <li>"eReady"</li></ul>
 * @param {Boolean} event.phone.ready <tt>true</tt> if phone is ready (status "eReady"), <tt>false</tt> if it is logged out or in the process of registering or recovering
 * @param {registration} event.phone.registration An updated copy of the registration object passed to {@link $.fn.cwic.registerPhone}
 */
/**
 * @name $.fn.cwic#error
 * @event
 * @param {Object|$.fn.cwic-errorMapEntry} event An arbitrary error event or pre-defined cwic error object.
 * @param {Object} event.code 
 * @param {String} [event.message] 
 * @param {String[]} [event.details] An array of additional information.
 */
/**
 * A change to the list of local multimedia devices has occurred.
 * The application should use {@link $.fn.cwic.getMultimediaDevices} to acquire the updated list of devices.
 * @name $.fn.cwic#mmDeviceChange
 * @since 3.0.0
 * @event
 * @param {Object} event
 */
/**
 * A property has changed on the external video window created by {@link $.fn.cwic.showPreviewInExternalWindow} or {@link $.fn.cwic.showCallInExternalWindow}.
 * @name $.fn.cwic#externalWindowEvent
 * @since 3.1.0
 * @event
 * @param {Object} event
 * @param {Object} event.externalWindowState
 * @param {Number} event.externalWindowState.callId The {@link call#callId} of a call associated with the external video window. -1 if no call is associated.
 *  See also {@link $.fn.cwic.showCallInExternalWindow}.
 * @param {Boolean} event.externalWindowState.showing When <tt>true</tt>, the external video window is displayed, possibly minimized or behind other windows.
 *  See also {@link $.fn.cwic.hideExternalWindow}.
 * @param {Boolean} event.externalWindowState.selfViewPip When <tt>true</tt>, the external video window will display a picture-in-picture preview (self-view).
 *  Note the picture-in-picture preview will not be visible if preview is also in the full window.  See also {@link $.fn.cwic.setExternalWindowShowSelfViewPip}.
 * @param {Boolean} event.externalWindowState.alwaysOnTop When <tt>true</tt>, the external video window will be set to always display on top of other windows.
 *  See also {@link $.fn.cwic.setExternalWindowAlwaysOnTop}.
 * @param {Boolean} event.externalWindowState.preview When <tt>true</tt>, the external video window is showing preview (self-view) in the full window.
 *  See also {@link $.fn.cwic.showPreviewInExternalWindow}.
 * @param {Boolean} event.externalWindowState.docking When <tt>true</tt>, the external video window will be in docked state.
 *  See also {@link $.fn.cwic.dock}.
 * @param {String} event.externalWindowState.title The text used for the external video window's title bar.
 *  See also {@link $.fn.cwic.setExternalWindowTitle}
 */
 
/**
 * A call transfer is in progress. It signals that an ongoing call transfer can be completed or canceled.
 * The application should implement this event to attach "completeTransfer" handler, if the higher level API does not fit specific needs. 
 * <b>Important note:</b> This event could be triggered multiple times for one call transfer. (be careful not to create memory leaks with event handlers)
 * @name $.fn.cwic#callTransferInProgress
 * @since 4.0.0
 * @event
 * @param {Object} event
 * @param {Function} event.completeTransfer Function to be called to complete ongoing call transfer       
 */
 
 /**
 * An invalid certificate detected.
 * @name $.fn.cwic#invalidCertificate
 * @since 4.0.0
 * @event
 * @param {Object} event
 * @param {Function} event.respond(fingerprint,accept) Function to be called to reject or accept advertised invalid certificate.
 * @param {Object} event.info Additional info about advertised invalid certificate   
 * @param {String} event.info.certFingerprint Certificate fingerprint
 * @param {String} event.info.certSubjectCN Certificate Subject Common Name
 * @param {String} event.info.referenceId 
 * @param {Array} event.info.invalidReasons Array of invalid reasons
 * @param {String} event.info.subjectCertificateData 
 * @param {String} event.info.intermediateCACertificateData 
 * @param {Boolean} event.info.allowUserToAccept If true, certificate can be accepted with repond function
 */

/**
 * Signals when authentication with Identity Provider is required during the SSO process. 
 * @name $.fn.cwic#ssoNavigateTo
 * @since 4.0.0
 * @event
 * @param {Object} event
 * @param {String} event.url URL to navigate to from popup/iFrame to continue with SSO process.      
 */
 
 /**
 * Ringtone changed
 * @name $.fn.cwic#ringtoneChange
 * @since 4.0.0
 * @event
 * @param {Object} event
 * @param {String} event.ringtone New ringtone name      
 */
 
 /**
 * List of ringtones is available
 * @name $.fn.cwic#ringtonesListAvailable
 * @since 4.0.0
 * @event
 * @param {Object} event
 * @param {Array} event.ringtones List of available ringtones     
 */
 
/**
 * Signals weather the multimedia capability is enabled or not. Multimedia services are enabled only in authenticated state.
 * @name $.fn.cwic#multimediaCapabilities
 * @since 4.0.0
 * @event
 * @param {Object} event
 * @param {Boolean} event.multimediaCapability    
 */