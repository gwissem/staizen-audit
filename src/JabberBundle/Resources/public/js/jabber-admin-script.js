var JabberAdminScript = null;

var jabberAdminScript = (initJabberAdminScript);

/**
 * @param {Object} _ref
 * @param {Object} _ref.moduleCallingOptions
 * @param {Object} _ref.moduleHistoryCall
 * @param {Object} _ref.modulePhoneStatus
 * @param {Object} _ref.modulePhoneTransfer
 * @param {Object} _ref.moduleCallPanel
 * @param {Object} _ref.modulePhoneSetting
 * @param {Object} _ref.moduleConversations
 * @param {Object} _ref.moduleDTMF
 * @param {Object} _ref.webSocket
 * @param {function} _ref.sendEventToIframe
 * @param options
 * @returns {{}}
 */
function initJabberAdminScript(_ref, options) {


    function _init() {


    }

    function _getRef(name) {

        if(typeof _ref[name] !== "undefined") {
            return _ref[name];
        }

        return 'NOT EXISTS!';

    }

    function _conversationEnded(status, ringingNumber) {

        status = (typeof status === "undefined") ? "FINISHED" : status;
        ringingNumber = (typeof ringingNumber === "undefined") ? 123123123 : ringingNumber;

        _ref.moduleConversations.notifyAboutConversationEnded(status, {
            ringingNumber: ringingNumber,
            platformNumber: 721
        }, {})

    }

    function _addNoteOfOutgoingCall(number) {

        number = (typeof number === "undefined") ? 123123123 : number;

        var dataEvent = {
            detail: {
                number: number
            }
        };

        console.log(dataEvent);

        var cEvent = new CustomEvent("out-coming-connection", dataEvent);
        _ref.sendEventToIframe(cEvent);

    }

    _init();

    return {
        simulationConversationEnded: _conversationEnded,
        simulationAddNoteOfOutgoingCall: _addNoteOfOutgoingCall,
        _ref: _getRef
    }

}