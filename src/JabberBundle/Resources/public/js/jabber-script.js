var getConversations;

String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return hours + ':' + minutes + ':' + seconds;
};

String.prototype.toMMSS = function () {
    var sec_num = parseInt(this, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return minutes + ':' + seconds;
};

// ucs-2 string to base64 encoded ascii
function utoa(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

// base64 encoded ascii to ucs-2 string
function atou(str) {
    return decodeURIComponent(escape(window.atob(str)));
}

var jabberChannel = null;

(function ($, window, document) {

    var CONST_READY = 'READY',
        CONST_NOT_READY = 'NOT_READY',
        CONST_WRAP_UP = 'WRAP_UP';

    var jabberInstance,
        $phoneTopPanel,
        webSocket,
        atlasExtension,
        telephonySession,
        eventListener;

    var timing = 0,
        timerInterval,
        startDateTimeOfConversation;

    var helpLines = [],
        eCallPlatformId;

    /** Vue Variables */

    var vueVars = {
        duringConversation: {
            isDuring: false,
            start: function () {
                this.isDuring = true;
            },
            stop: function () {
                this.isDuring = false;
            }
        },
        leaveOnTopPanel: null,
        leaveOnTopPanelInterval: null
    };

    /** Vue Modules */
    var VueModule,
        moduleCallingOptions,
        moduleHistoryCall,
        modulePhoneStatus,
        modulePhoneTransfer,
        moduleCallPanel,
        modulePhoneSetting,
        moduleConversations,
        moduleDTMF;

    /** DuelJs */

        // var jabberChannel = null,
    var jChannelName = 'atlas_jabber';

    /** Notifications */

    var jabberNotification,
        atlasExtNotification,
        statusToastr = null,
        statusToastrTimer = null;

    var $atlasIframe,
        $phonePanel,
        $logoutBtn,
        conversationEvent = null;

    initSocketHandle();
    initAtlasChromeExtension();

    $(function () {

        // Disable Jabber on mobile
        if (isMobile.any) {
            $('.jabber-voice-top-bar').remove();
            return false;
        }

        $phoneTopPanel = $('#phone-top-panel');
        $atlasIframe = $('#atlas-iframe');
        $phonePanel = $('#phone-panel');
        $logoutBtn = $('#logout');

        loadSettings();

        var _u = JSON.parse(atou(_j));

        /**
         * Muszą być dane logowania, żeby połączyć się z telefonią
         */
        if(_u.username && _u.password) {

            jabberInstance = new JabberVoice(_u.username, _u.password, {
                loggingLevel: 3,
                conversationInComing: _conversationInComing,
                conversationOutComing: _conversationOutComing,
                conversationStarted: _conversationStarted,
                endConversation: _endConversation,
                conversationUpdate: _conversationUpdate,
                jabberIsConnected: _onConnected,
                onCwicError: _onCwicError,
                mediaListChanged: _mediaListChanged,
                setVolumeOfMedia: _setVolumeOfMedia,
                debugMode: false
            });

            jabberInstance.Init();

        }
        else {

            setInterval(function () {
                toastr.error('Wystąpił problem połączenia z telefonią (empty l&p). Proszę się wylogować i zalogować do Atlasa ponownie.', 'Proszę się ponownie zalogować.', {
                    timeOut: 30000
                })
            }, 31000);

            toastr.error('Wystąpił problem połączenia z telefonią (empty l&p). Proszę się wylogować i zalogować do Atlasa ponownie.', 'Proszę się ponownie zalogować.', {
                timeOut: 30000
            })

        }

        if (jabberChannel === null) jabberChannel = duel.channel(jChannelName);

        initEventListener();
        initVue();

        if (!("Notification" in window)) {
            alert("Ta przeglądarka nie obsługuje powiadomień");
        } else if (Notification.permission !== "granted" || Notification.permission === "denied") {
            Notification.requestPermission();
        }

        if(typeof jabberAdminScript !== "undefined" ) {

            JabberAdminScript = new jabberAdminScript(
                {
                    moduleCallingOptions: moduleCallingOptions,
                    moduleHistoryCall: moduleHistoryCall,
                    modulePhoneStatus: modulePhoneStatus,
                    modulePhoneTransfer: modulePhoneTransfer,
                    moduleCallPanel: moduleCallPanel,
                    modulePhoneSetting: modulePhoneSetting,
                    moduleConversations: moduleConversations,
                    moduleDTMF: moduleDTMF,
                    webSocket: webSocket,
                    jabberChannel: jabberChannel,
                    sendEventToIframe: sendEventToIframe
                }
            );

        }

        // if (!("Notification" in window)) {
        //     alert("Ta przeglądarka nie obsługuje powiadomień");
        // }
        // else if (Notification.permission === "denied") {
        //     Notification.requestPermission();
        // }

    });

    function initEventListener() {

        document.addEventListener("telephony-call-number", function (e) {

            // if (!vueVars.duringConversation.isDuring && e.detail.number && jabberChannel) {
            if (e.detail.number && jabberChannel) {

                moduleCallPanel.callNumber = e.detail.number;
                moduleCallPanel.$moduleContent.slideDown(100);
                moduleCallPanel._$el.addClass('active');
                moduleCallPanel.startOutCalling();

            }
            else {
                console.error('Nie może nawiązać połaczenia.');
            }
        });

    }

    function initAtlasChromeExtension() {

        /**
         * @type {chrome.runtime.Port} _port
         */
        var _port = atlasChromeExtension.getPort(),
            lastNotificationId = null;

        function hideNotification() {
            if(lastNotificationId !== null) {
                _port.postMessage({action: "clear-notification", id: lastNotificationId}, function() {

                });

                lastNotificationId = null;
                atlasExtNotification = false;
            }
        }

        function showChromeNotification(number, welcome) {

            hideNotification();

            welcome = welcome || ' ';
            number = number || ' ';

            lastNotificationId = guid();
            atlasExtNotification = true;

            var opts = {
                id: lastNotificationId,
                type: "image",
                // iconUrl: 'http://atlas.starter24.dev/bundles/web/images/incoming-call-icon-mini.png',
                // imageUrl: 'http://atlas.starter24.dev/bundles/web/images/incoming-call-icon.png',
                title: 'Przychodzące połączenie - Atlas',
                message: 'Numer: ' + number,
                contextMessage: welcome,
                buttons: [
                    {
                        title: 'Odbierz',
                        iconUrl: window.location.origin + '/bundles/web/images/incoming-call-icon-mini.png'
                    }
                ]
            };

            _port.postMessage({action: "notification", focusWindow: true, options: opts});

        }

        if(_port) {

            _port.onMessage.addListener(function(msg, sender) {

                if(msg.type === "buttonClicked") {
                    // msg.buttonIndex

                    jabberInstance.pickUp();

                    if(lastNotificationId === msg.notificationId) {
                        lastNotificationId = null;
                    }
                }
                else if(msg.type === "openTask") {

                    var isOperationDashboard = $atlasIframe.contents().find('body').hasClass('operational-page');

                    /** Jeżeli w iframe nie jest załadowany dashboard to robi przekierowanie do operation dashboard */

                    if (isOperationDashboard) {

                        var telephonyEvent = new CustomEvent("telephony-pick-up", {'detail': msg.detail});
                        sendEventToIframe(telephonyEvent);

                    }
                    else {

                        var message = {
                            type: 'telephony-pick-up-with-reload',
                            data: msg.detail
                        };

                        $atlasIframe[0].contentWindow.postMessage(message, Routing.generate('index', [], true));

                    }

                }

            });

            atlasExtension = {

                getPort: function () {
                    return _port;
                },
                showNotification: showChromeNotification,
                hideNotification: hideNotification

            }
        }
        else {
            atlasExtension = null;
        }

    }

    function initSocketHandle() {


        if (typeof WS === "undefined" || !document.WSConnectingString) {
            console.error('WebSocket is not loaded.');
            return false;
        }

        webSocket = webSocket || WS.connect(document.WSConnectingString);

        webSocket.on("socket/connect", function (session) {

            telephonySession = session;

            telephonySession.subscribe("atlas/telephony", function (uri, payload) {


                if(payload.action === "force-create") {

                    payload.task.isForce = true;
                    payload.task.processInstances = payload.task.id;

                    if(atlasExtension) {
                        atlasExtension.getPort().postMessage({action: 'focus-first-dashboard', detail: payload.task});
                    }
                    else {
                        var isOperationDashboard = $atlasIframe.contents().find('body').hasClass('operational-page');

                        /** Jeżeli w iframe nie jest załadowany dashboard to robi przekierowanie do operation dashboard */

                        if (isOperationDashboard) {

                            var telephonyEvent = new CustomEvent("telephony-pick-up", {'detail': payload.task});
                            sendEventToIframe(telephonyEvent);

                        }
                        else {

                            var message = {
                                type: 'telephony-pick-up-with-reload',
                                data: payload.task
                            };

                            $atlasIframe[0].contentWindow.postMessage(message, Routing.generate('index', [], true));

                        }
                    }

                }
                else if(payload.action === "refresh-statuses") {
                    modulePhoneStatus.getSupportedStatus(function (response) {

                        if(response.forceReady) {
                            if(response.forceReady) {
                                modulePhoneStatus.setReadyIfNot();
                            }
                        }

                    });
                }

            });

        });

    }

    function _onCwicError(errorInfo) {

        var title = '';

        if (errorInfo.errorType && errorInfo.errorData && errorInfo.errorData.reason) {
            title = errorInfo.errorType + ': ' + errorInfo.errorData.reason;
        }

        $.ajax({
            url: Routing.generate('js_logger'),
            type: "POST",
            data: {
                on_cwic_error: errorInfo,
                browser_url: window.location.href
            }
        });

        if($phoneTopPanel && $phoneTopPanel.length) {
            var $connection = $phoneTopPanel.find('.connecting');
            $connection.addClass('error').html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>');
            $connection.attr('title', title);
        }


    }

    function onClickLogout(e) {
        e.preventDefault();
    }

    function enableLogout() {
        $logoutBtn.removeClass('disabled').attr('title', "").off('click', onClickLogout);
    }

    function disableLogout() {
        $logoutBtn.addClass('disabled').attr('title', "W czasie połączenia nie można się wylogować.").on('click', onClickLogout);
    }

    /** Nastąpiło połączenie z Jabberem */

    function _onConnected(data) {

        $phoneTopPanel.find('.connecting').addClass('done');

        moduleHistoryCall.refreshHistory();
        moduleConversations.handleJabberChannelBroadcast();

        // _refreshTelephonyLayout();

        /** Logowanie do Finesse, jeżeli należy do grupy CallCenter */

        if (CONST_CALL_CENTER_MODE) {
            if (data.authLogin === true) {

                /** Log in to Finesse */

                sendAjax({}, Routing.generate('jabberVoiceAuthLogin'), 'PUT', function (response) {

                    if (response.status) {
                        afterLoginToFinesse();
                    }

                }, function (err) {

                    modulePhoneStatus.errorLogin = true;

                    toastr.error('Nie można się zalogować do systemu Cisco Finesse.', 'Błąd logowania', {timeOut: 5000});

                });
            }
            else {
                afterLoginToFinesse();
            }
        }
    }

    function afterLoginToFinesse() {

        holdSession();
        listeningSupportedStatus();

        modulePhoneStatus.getSupportedStatus(function (response) {
            modulePhoneStatus.updateCurrentStatus().done(function () {

                if(response.forceReady) {
                    modulePhoneStatus.setReadyIfNot();
                }

            })
        })

    }

    function _mediaListChanged() {
        cLog('Zmiana media');
        // modulePhoneSetting.updateFromDefaultMedia();
    }

    function _setVolumeOfMedia(mediaName, volume) {

        modulePhoneSetting.updateMediaVolume(mediaName, volume);

    }

    function _conversationUpdate(telephonyConversation) {

        moduleConversations.updateConversationObj(telephonyConversation);

        // if(telephonyConversation.callState == "Busy") {
        //     if(moduleConversations.conversations[telephonyConversation.ID].conversationId) {
        //         moduleConversations.updateConversationStatus(moduleConversations.conversations[telephonyConversation.ID].conversationId, 'BUSY');
        //     }
        // }

        if (telephonyConversation.callState === "Ringout") {
            moduleConversations.callState = "Ringout";
        }
        else if (telephonyConversation.callState === "Ringin") {
            moduleConversations.callState = "Ringin";
        }
        else if (telephonyConversation.callState === "Reorder") {
            /** Błąd numeru */
        }
    }

    function _conversationInComing(telephonyConversation) {

        cLog('Połączenie przychodzące...');

        disableLogout();
        moduleConversations.incomingConversation(telephonyConversation);

    }

    function _conversationOutComing(telephonyConversation) {

        cLog('Połączenie wychodzące...');

        disableLogout();
        moduleConversations.outcomingConversation(telephonyConversation);

    }

    function _conversationStarted(telephonyConversation) {

        cLog('Połączenie rozpoczęte');

        // Placed
        // Received

        moduleConversations.updateConversationObj(telephonyConversation);
        moduleConversations.startConversation(telephonyConversation);

    }

    function _endConversation(telephonyConversation) {

        cLog('Połączenie zakończone');

        moduleConversations.conversationEnded(telephonyConversation);

    }

    function _refreshTelephonyLayout() {

        /** Sprawdza czy obecnie istnieje połączenie */

        var conversation = jabberChannel.getItem('currentConversation');

        if (conversation) {

            moduleConversations.conversations[conversation.content.ID] = conversation;
            moduleConversations.currentConversation = conversation;

            moduleCallPanel.lockTab();

            moduleConversations.ringingNumber = moduleConversations.getNumberFromConversation(conversation.content);
            moduleConversations.ringingName = moduleConversations.getNameFromConversation(conversation.content);

            moduleCallingOptions.start(moduleConversations.ringingNumber, moduleConversations.ringingName);

        }

    }

    /**
     * Co 30 sekund sprawdza dostępne statusy (jeżeli zmieniła się godzina ostatniej aktualizacji - wysyła request)
     */

    var lastCheckingStatuses = null,
        intervalCheckingStatuses = null;

    function listeningSupportedStatus() {

        if(intervalCheckingStatuses) {
            clearInterval(intervalCheckingStatuses);
            intervalCheckingStatuses = null;
        }

        intervalCheckingStatuses = setInterval(function () {

            if(lastCheckingStatuses === null) return;

            if(moment().format("HH") !== moment(lastCheckingStatuses).format("HH")) {
                modulePhoneStatus.loadingStatuses = true;
                modulePhoneStatus.getSupportedStatus(function (response) {
                    if(response.forceReady) {
                        modulePhoneStatus.setReadyIfNot();
                    }
                });
            }

        }, 30000);

    }

    /** Podtrzymanie sesji przy zalogowaniu się do finesse */

    function holdSession() {

        /** Co 10 sekund sprawdzany jest status użytkownika, więc to już nie jest potrzebne */
        //
        // setInterval(function () {
        //     sendAjax({}, Routing.generate('jabberVoiceRefreshUserSession'), 'GET', function (result) {
        //
        //         if (result.status === true) {
        //             /** Użytkownik jest zalogownay */
        //         }
        //         else {
        //
        //             /** Użytkownik się wylogowal, trzeba przeladowac strone */
        //
        //             window.postMessage({
        //                 type: 'force-redirect',
        //                 delay: 1000
        //             }, Routing.generate('index', [], true));
        //
        //         }
        //
        //     }, function (error) {
        //
        //         window.postMessage({
        //             type: 'force-redirect',
        //             delay: 1000
        //         }, Routing.generate('index', [], true));
        //
        //     });
        //
        // }, 55 * 1000);

        var holdStatusInterval;

        if (CONST_CALL_CENTER_MODE) {

            var _url = Routing.generate('jabberVoiceGetCurrentStatus');

            holdStatusInterval = setInterval(function () {

                if (!vueVars.duringConversation.isDuring && modulePhoneStatus.statuses.length !== 0 && telephonySession && telephonySession._websocket_connected) {

                    sendAjax({}, _url, 'GET', function (response) {

                        if (!vueVars.duringConversation.isDuring) {
                            if (response.status) {

                                // Sprawdzenie czy użytkownik został wylogowany
                                if(response.message === "LOGOUT") {
                                    if (modulePhoneStatus.currentStatus) {
                                        modulePhoneStatus.currentStatus.isActive = false;
                                    }
                                    modulePhoneStatus.currentStatus = null;
                                    modulePhoneStatus.errorLogin = true;
                                    toggleJabberStatusToastr('Zostałeś wylogowany z telefonii.');
                                    clearInterval(holdStatusInterval);

                                }
                                else {
                                    modulePhoneStatus.redrawCurrentStatus(response);
                                }

                            }
                            else {

                                if (modulePhoneStatus.currentStatus) {
                                    modulePhoneStatus.currentStatus.isActive = false;
                                }

                                modulePhoneStatus.currentStatus = null;
                                toggleJabberStatusToastr('Wystąpił problem ze zmianą statusu. Prosze się przelogowac.');

                            }
                        }

                    }, function (err) {
                        console.error(err);
                    });

                    // telephonySession.call("atlas/telephony/rpc/check_status/" + _locale, {}).then(
                    //     function (response) {
                    //
                    //         if (!vueVars.duringConversation.isDuring) {
                    //             if (response.status) {
                    //
                    //                 // Sprawdzenie czy użytkownik został wylogowany
                    //                 if(response.message === "LOGOUT") {
                    //                     if (modulePhoneStatus.currentStatus) {
                    //                         modulePhoneStatus.currentStatus.isActive = false;
                    //                     }
                    //                     modulePhoneStatus.currentStatus = null;
                    //                     modulePhoneStatus.errorLogin = true;
                    //                     toggleJabberStatusToastr('Zostałeś wylogowany z telefonii.');
                    //                     clearInterval(holdStatusInterval);
                    //
                    //                 }
                    //                 else {
                    //                     modulePhoneStatus.redrawCurrentStatus(response);
                    //                 }
                    //
                    //             }
                    //             else {
                    //
                    //                 if (modulePhoneStatus.currentStatus) {
                    //                     modulePhoneStatus.currentStatus.isActive = false;
                    //                 }
                    //
                    //                 modulePhoneStatus.currentStatus = null;
                    //                 toggleJabberStatusToastr('Wystąpił problem ze zmianą statusu. Prosze się przelogowac.');
                    //
                    //             }
                    //         }
                    //
                    //     },
                    //     function (error, desc) {
                    //         // toastr.error("Opis: " + error.desc, "Błąd", {timeOut: 7000});
                    //     }
                    // );
                }
            }, 10000)
        }
        else {
            console.error('Brak połączenia z socketem.');
        }
    }

    function sendAjax(objToSend, url, type, success, error, complete) {

        return $.ajax({
            type: type,
            dataType: 'json',
            url: url,
            data: objToSend,
            success: function (response) {
                if (typeof success === 'function') {
                    success(response);
                }
            },
            complete: function (response) {
                if (typeof complete === 'function') {
                    complete(response);
                }
            },
            error: function (err) {
                if (typeof error === 'function') {
                    error(err);
                }
            }
        });
    }

    function startTiming(startDateTime) {
        var timer = $phoneTopPanel.find('.timing');

        // startDateTime

        startDateTimeOfConversation = startDateTime || new Date();

        if (!startDateTime) {
            // moduleConversations.currentConversation.startDateTime  = startDateTimeOfConversation.getTime();
            // jabberChannel.setItem('currentConversation', moduleConversations.currentConversation);
        }

        timing = 0;

        timerInterval = setInterval(function () {

            timing = ((new Date()).getTime() - startDateTimeOfConversation.getTime()) / 1000;
            updateTime(timer);

        }, 1000);
    }

    function updateTime(timer) {

        timer.text((timing.toString()).toMMSS());
    }

    function changeRingtone(number) {

        var platform = helpLines.filter(function (el) {
            return (number == el.number);
        });

        if (platform.length && platform[0].ringtone) {
            modulePhoneSetting.setRingtone(platform[0].ringtone);
        }

    }

    function loadSettings() {

        eCallPlatformId = $phonePanel.attr('data-e-call-platform-id');

        /** Załadowanie listy infolini z przywitaniami */

        sendAjax({}, Routing.generate('jabberVoiceGetHelpLineList'), 'GET', function (response) {
            if (response.status) {

                helpLines = response.list;

            }
            else {
                console.error('Help lines are empty!')
            }
        }, function (err) {
            console.error(err);
        });

    }

    /**
     * Odpala event z parametrami
     *
     * Przykładowy Event:
     *
     * var event = new CustomEvent("custom-event", {'detail': {
     *      custom_info: 10,
     *      custom_property: 'importantData'
     * }});
     *
     * Użycie w iframe :
     *
     * document.addEventListener("custom-event", function(e) {
     *      console.log("custom-event", e.detail);
     * });
     *
     */

    function sendEventToIframe(cEvent) {

        if (!$atlasIframe.length) return false;

        $atlasIframe[0].contentWindow.document.dispatchEvent(cEvent);

    }

    function initVue() {

        /**
         * --------------
         * MODULES
         * --------------
         */

        var myMixin = {
            created: function () {
                // this.hello()
            },
            methods: {
                hello: function () {
                    console.log('hello from mixin!')
                }
            }
        };

        var AdvancedVue = Vue.extend({
            delimiters: ['${', '}']
        });

        VueModule = AdvancedVue.extend({
            mixins: [myMixin],
            mounted: function () {
                var that = this;
                this._$el = $(this.$el);
                this.$moduleContent = this._$el.find('.phone-module-content');

                $(this.$el).on('mouseenter', '.show-on-hover', this.actionOnHoverTabPanel);

                $(this.$el).on('click', '.toggle-btn', function (e) {

                    if ($(this).attr('disabled') == "disabled") return false;

                    if (that._$el.hasClass('active')) {
                        that.$moduleContent.hide();
                        $('.phone-module').removeClass('active');

                        if(typeof that.onCloseTab === "function") {
                            that.onCloseTab();
                        }
                    }
                    else {
                        $('.phone-module-content').hide();
                        that.$moduleContent.slideDown(100);
                        $('.phone-module').removeClass('active');
                        that._$el.addClass('active');

                        if (!vueVars.leaveOnTopPanel) {
                            vueVars.leaveOnTopPanel = true;
                            that.eventOnTopPanel();
                        }

                        if(typeof that.onOpenTab === "function") {
                            that.onOpenTab();
                        }

                    }

                });

            },
            methods: {
                actionOnHoverTabPanel: function (e) {

                    var that = this;

                    if ($(e.currentTarget).attr('disabled') == "disabled") return false;

                    if (that._$el.hasClass('active')) return false;

                    $('.phone-module-content').hide();
                    that.$moduleContent.slideDown(100);
                    $('.phone-module').removeClass('active');
                    that._$el.addClass('active');

                    if (!vueVars.leaveOnTopPanel) {
                        vueVars.leaveOnTopPanel = true;
                        that.eventOnTopPanel();
                    }

                    if(typeof this.onOpenTab === "function") {
                        this.onOpenTab();
                    }

                    //
                    // if (that._$el.hasClass('active')) {
                    //     that.$moduleContent.hide();
                    //     $('.phone-module').removeClass('active');
                    // }
                    // else {
                    //     $('.phone-module-content').hide();
                    //     that.$moduleContent.slideDown(100);
                    //     $('.phone-module').removeClass('active');
                    //     that._$el.addClass('active');
                    //
                    //     if (!vueVars.leaveOnTopPanel) {
                    //         vueVars.leaveOnTopPanel = true;
                    //         that.eventOnTopPanel();
                    //     }
                    //
                    // }
                },
                hideContent: function () {
                    this.$moduleContent.hide();
                    this._$el.removeClass('active');
                },
                lockTab: function () {
                    this._$el.find('.toggle-btn').attr('disabled', "disabled");
                    this.hideContent();
                },
                unLockTab: function () {
                    this._$el.find('.toggle-btn').removeAttr('disabled');
                },
                eventOnTopPanel: function () {
                    $phoneTopPanel.on('mouseleave', this.eventOnLeave).on('mouseenter', this.eventOnEnter);
                },
                delayTime: function () {
                    return (moduleCallPanel._$el.hasClass('active')) ? 4000 : 2000
                },
                eventOnLeave: function () {

                    var _this = this;

                    vueVars.leaveOnTopPanelInterval = setTimeout(function () {

                        $('.phone-module-content').hide();
                        $('.phone-module').removeClass('active');
                        $phoneTopPanel.off('mouseleave', this.eventOnLeave);
                        $phoneTopPanel.off('mouseenter', this.eventOnEnter);
                        vueVars.leaveOnTopPanel = false;

                        if(typeof this.onCloseTab === "function") {
                            this.onCloseTab();
                        }

                    }.bind(this), this.delayTime());
                },
                eventOnEnter: function () {
                    if (vueVars.leaveOnTopPanelInterval) {
                        clearInterval(vueVars.leaveOnTopPanelInterval);
                        vueVars.leaveOnTopPanelInterval = null;
                    }
                }
            }
        });

        /** Connecting Module */

        // moduleConnecting = new VueModule({
        //     el: '#calling-options-module',
        //     data: {
        //         duringConversation: vueVars.duringConversation
        //     }
        // });


        /** DTMF-module */

        moduleDTMF = new VueModule({
            el: '#DTMF-module',
            mounted: function () {
                var self = this;

                this.btnNumber = $('#jabberNumberForDTMF');
                this.number = '';

                this._$el.find('.footer button').on('click', function (e) {
                    self.pressKey(e.target.getAttribute('data-key-value'));
                });

                this.btnNumber.on('keypress', function (e) {
                    jabberInstance.pressedDTMFDigit(e);
                    self.disableForMoment();
                    return true;
                });
            },
            data: {
                duringConversation: vueVars.duringConversation,
                btnNumber: null,
                number: null
            },
            methods: {
                pressKey: function (key) {
                    if (key && !this.btnNumber.is(':disabled')) {
                        this.number += key;
                        jabberInstance.pressedDTMFDigit(key);
                        this.disableForMoment();
                    }
                },
                clear: function () {
                    this.number = '';
                },
                disableForMoment: function () {
                    setTimeout(function () {
                        this.btnNumber.prop('disabled', true);
                        setTimeout(function () {
                            this.btnNumber.prop('disabled', false);
                        }.bind(this), 1000);
                    }.bind(this), 50)
                }
            }
        });

        /** Calling Options Module */

        moduleCallingOptions = new VueModule({
            el: '#calling-options-module',
            mounted: function () {
                // mute-volume

                $('#end-call').on('click', function () {

                    jabberInstance.endConversation();

                });

                $('#mute-volume').on('click', function (e) {
                    e.preventDefault();
                    $(this).toggleClass('is-mute');
                    jabberInstance.muteAudio();
                });

                this._$el.find('.live-conversations').on('click', 'li button', this.onClickBtnLiveConversation);

            },
            data: {
                duringConversation: vueVars.duringConversation,
                phoneNumber: null,
                liveConversations: [],
                participantName: null,
                timing: false
            },
            methods: {
                addLiveConversation: function (conversation) {

                    var numbers = (conversation.details.fromAddress) ? [conversation.details.fromAddress] : moduleConversations.getNumbersFromConversation(conversation.content);

                    this.liveConversations.push({
                        numbers: numbers,
                        ID: parseInt(conversation.content.ID),
                        _conversation: conversation,
                        isActive: (conversation.content.callState === "Connected"),
                        canEnd: conversation.content.capabilities.canEnd,
                        canHold: conversation.content.capabilities.canHold,
                        canResume: conversation.content.capabilities.canResume,
                        displayNumbers: function () {
                            return (Array.isArray(this.numbers) && this.numbers.length > 0) ? this.numbers.join(', ') : '';
                        },
                        canMerge: false
                    });

                    this.updateMergeOfLiveConversations();

                },
                findLiveConversation: function (ID) {

                    var result = this.liveConversations.filter(function (obj) {
                        return (obj.ID === ID);
                    });

                    if (result.length === 1) {
                        return result[0];
                    }

                    return null;

                },
                updateLiveConversation: function (conversation) {

                    var liveConversation = this.findLiveConversation(conversation.content.ID);

                    if (liveConversation) {

                        var numbers = moduleConversations.getNumbersFromConversation(conversation.content);

                        if (numbers.length === 0 || numbers.length === 1) {
                            numbers = (conversation.details.fromAddress) ? [conversation.details.fromAddress] : [moduleConversations.getNumberFromConversation(conversation.content)];
                        }

                        liveConversation.canEnd = conversation.content.capabilities.canEnd;
                        liveConversation.canHold = conversation.content.capabilities.canHold;
                        liveConversation.canResume = conversation.content.capabilities.canResume;
                        liveConversation.canMerge = this.conversationCanMerge(liveConversation);
                        liveConversation.isActive = (conversation.content.callState === "Connected");
                        liveConversation.numbers = numbers;

                    }

                    this.updateMergeOfLiveConversations();

                },
                removeLiveConversation: function (ID) {

                    var _this = this;

                    $.each(this.liveConversations, function (i) {
                        if (_this.liveConversations[i].ID === ID) {
                            _this.liveConversations.splice(i, 1);
                            return false;
                        }
                    });

                    this.updateMergeOfLiveConversations();

                },

                updateMergeOfLiveConversations: function () {

                    var _this = this;

                    $.each(this.liveConversations, function (i, ele) {
                        _this.liveConversations[i].canMerge = _this.conversationCanMerge(ele);
                    });

                },
                conversationCanMerge: function (ele) {
                    return (moduleConversations.currentConversation.content.callState === "Connected" && moduleConversations.currentConversation.content.ID !== ele.ID && this.liveConversations.length > 1)
                },
                onClickBtnLiveConversation: function (e) {

                    var $t = $(e.currentTarget),
                        role = $t.attr('data-role'),
                        ID = parseInt($t.closest('li[data-id-conversation]').attr('data-id-conversation')),
                        conversationToAction = this.findLiveConversation(ID);

                    if (conversationToAction === null) return false;

                    switch (role) {
                        case 'merge': {

                            if (moduleConversations.currentConversation) {

                                if (moduleConversations.currentConversation.content.capabilities.canMerge) {
                                    try {

                                        moduleConversations.currentConversation.content.merge(conversationToAction._conversation.content);

                                    }
                                    catch (exception) {
                                        console.log(exception.message);
                                    }
                                }
                            }

                            break;
                        }
                        case 'cancel': {
                            cLog('ON CANCEL');
                            if (conversationToAction._conversation.content.capabilities.canEnd) {
                                conversationToAction._conversation.content.end();
                            }

                            break;
                        }
                        case 'hold': {
                            cLog('ON HOLD');
                            if (conversationToAction._conversation.content.capabilities.canHold) {
                                conversationToAction._conversation.content.hold(function (error) {
                                    console.error(error);
                                });
                            }

                            break;
                        }
                        case 'resume': {

                            if (conversationToAction._conversation.content.capabilities.canResume) {
                                conversationToAction._conversation.content.resume();
                            }

                            break;
                        }
                    }

                    this.updateMergeOfLiveConversations();

                },
                start: function (number, name, startDateTime) {
                    if (!timing) {
                        timing = true;
                        this.phoneNumber = number;
                        this.participantName = name;
                        vueVars.duringConversation.start();
                        startTiming(startDateTime);
                    }
                },
                stop: function () {
                    this.phoneNumber = null;
                }
            }
        });

        /** History Call Module */

        moduleHistoryCall = new VueModule({
            el: '#history-call-module',
            data: {
                callHistory: [],
                duringConversation: vueVars.duringConversation,
                historyLoading: true,
                isEmpty: true,
                notPickUpAmount: 0
            },
            mounted: function () {

                this._$el.find('ul').slimScroll({
                    height: '250px'
                });

                this._$el.on('click', '.toggle-btn', function () {

                    if (this.notPickUpAmount) this.resetNotPickUp();

                }.bind(this));

                this.$moduleContent.on('click', '.re-call', function (e) {

                    // if (!vueVars.duringConversation.isDuring) {

                    if (moduleCallPanel.status === 'ready' && !moduleCallPanel.jabberNumberToCall.is(":disabled")) {
                        moduleHistoryCall.hideContent();
                        moduleCallPanel.callNumber = $(this).attr('data-number-element');
                        moduleCallPanel.$moduleContent.slideDown(100);
                        moduleCallPanel._$el.addClass('active');
                        moduleCallPanel.startOutCalling();
                    }
                    else {

                        toastr.warning('Jesteś w trakcie nawiązywania połączenia.', 'Nie można dzwonic.', {timeOut: 3000});
                    }

                });

            },
            methods: {
                incrementNotPickUpCalls: function () {

                    if (this.callHistory[0] && this.callHistory[0].status == "START_RINGING") {
                        this.callHistory[0].status = "NOT_PICK_UP";
                    }

                    this.notPickUpAmount++;
                },
                resetNotPickUp: function () {
                    this.notPickUpAmount = 0;
                },
                createHistoryElement: function (data, callback) {

                    /** data = { number, type, uri, name } */

                    sendAjax({date: (new Date()).getTime()}, Routing.generate('jabberVoicePrepareDateTime'), 'POST', function (response) {

                        var ele = {
                            type: data.type,
                            time: '---',
                            date: '---',
                            number: data.number,
                            status: 'START_RINGING',
                            name: data.name,
                            uri: data.uri
                        };

                        if (response.status) {
                            ele.time = response.dateTime.date;
                            ele.date = response.dateTime.time;
                        }

                        if (typeof callback === 'function') {
                            callback(ele);
                        }

                    }, function () {
                        callback(false);
                    });

                },
                refreshHistory: function () {

                    this.historyLoading = true;

                    sendAjax({}, Routing.generate('jabberVoiceGetHistoryConversation'), 'GET', function (response) {
                        if (response.status) {
                            this.callHistory = response.historyConversation;
                        }
                        else {
                            this.callHistory = [];
                        }
                    }.bind(this), function (err) {
                        console.error(err);
                    }, function () {
                        this.historyLoading = false;
                    }.bind(this));

                },
                appendToHistory: function (participant, type, callback) {

                    if (jabberChannel.currentWindowIsMaster()) {

                        this.createHistoryElement({
                                type: type,
                                name: participant.name,
                                uri: participant.uri,
                                number: participant.number
                            },
                            function (historyElement) {

                                if (historyElement === false) {
                                    callback(false);
                                }
                                else {
                                    this.callHistory.unshift(historyElement);
                                    callback(historyElement);
                                }

                            }.bind(this));

                    }
                    else {
                        if (typeof callback === "function") {
                            callback(false);
                        }
                    }

                    // if(typeof numberOrObject == "object") {
                    //     this.callHistory.unshift(numberOrObject);
                    // }
                    // else {
                    //
                    // }
                }
            }
        });

        /** Phone Status Module */

        modulePhoneStatus = new VueModule({
            el: '#phone-options-module',
            data: {
                duringConversation: vueVars.duringConversation,
                statuses: [],
                loadingStatuses: true,
                errorLogin: false,
                requireStatus: CONST_CALL_CENTER_MODE,
                currentStatus: null,
                spinnerRunning: true,
                title: '-',
                currentStatusType: CONST_NOT_READY,
                canWrapUp: false
            },
            mounted: function () {

                if (!this.requireStatus) {
                    $(this.$el).addClass('importantNone');
                    return false;
                }

                var _this = this;

                $('#status-list').on('click', '.status-label', function (e) {
                    e.preventDefault();

                    var $t = $(this);
                    if ($t.hasClass('active') || $t.hasClass('disabled')) return false;

                    _this.hideContent();

                    var status = _this.statuses[$t.attr('data-index')];

                    _this.setStatus(status);

                });

            },
            methods: {

                reLoginToFinesse: function (e) {
                    e.preventDefault();

                    modulePhoneStatus.errorLogin = false;

                    sendAjax({}, Routing.generate('jabberVoiceAuthLogin'), 'PUT', function (response) {

                        if (response.status) {
                            afterLoginToFinesse();
                        }

                    }, function () {

                        modulePhoneStatus.errorLogin = true;
                        toastr.error('Nie można się zalogować do systemu Cisco Finesse.', 'Błąd logowania', {timeOut: 5000});

                    });

                },

                getSupportedStatus: function (callback) {

                    var _this = this;

                    sendAjax({}, Routing.generate('jabberVoiceGetSupportedStatus'), 'GET', function (response) {

                        lastCheckingStatuses = new Date();

                        var oldCurrentStatus = _this.currentStatus;

                        if (response.statuses) {
                            _this.statuses = response.statuses;
                            _this.loadingStatuses = false;

                            if(oldCurrentStatus) {

                                var result = $.grep(_this.statuses, function (e) {
                                    return (e.type === oldCurrentStatus.type && e.idCode === oldCurrentStatus.idCode);
                                });

                                if (result.length === 1) {
                                    _this.currentStatus = result[0];
                                    _this.currentStatus.isActive = true;
                                }

                            }

                        }
                        else {
                            console.error('Supported Statuses are empty!')
                        }

                        if (typeof callback === "function") {
                            callback(response);
                        }

                    }, function (err) {
                        handleAjaxResponseError(err, true);
                    });

                },
                refreshTitle: function () {
                    if (this.currentStatus && this.currentStatus.label) {
                        this.title = this.currentStatus.label;
                    }
                },
                setReadyIfNot: function() {

                    if(this.currentStatus === null) {
                        this.setReady();
                    }
                    else if(this.currentStatus) {

                        // status typu NOT_READY i idCode inny od 1 i 2 (przerwy)
                        if(this.currentStatus.type !== CONST_READY && ["1","2"].indexOf(this.currentStatus.idCode) < 0 ) {
                            this.setReady();
                        }

                    }

                },
                setReady: function () {

                    this.canWrapUp = true;

                    var result = $.grep(this.statuses, function (e) {
                        return e.type === CONST_READY;
                    });

                    if (result.length === 1) {
                        if (this.currentStatus) {
                            this.currentStatus.isActive = false;
                        }
                        this.currentStatus = result[0];
                        this.currentStatus.isActive = true;
                        this.setStatus(this.currentStatus);
                    }
                },

                /** Po zakończeniu rozmowy ustawienie statusu Wrap-up i sprawdza od razu status w finesse */

                setWrapUp: function () {

                    if (!CONST_CALL_CENTER_MODE) return false;

                    if (this.canWrapUp) {
                        if (this.currentStatus) this.currentStatus.isActive = false;
                        this.currentStatus = this.getWrapUpStatus();
                    }

                    this.updateCurrentStatus();

                    // setTimeout(function () {
                    //
                    //     modulePhoneStatus.canWrapUp = true;
                    //
                    //     if (modulePhoneStatus.currentStatus.type == CONST_WRAP_UP) {
                    //         modulePhoneStatus.setReady();
                    //     }
                    //
                    // }, 20000);
                },

                getWrapUpStatus: function () {
                    return {
                        type: CONST_WRAP_UP
                    }
                },

                getFailStatus: function () {
                    return {
                        message: "NOT_READY",
                        reasonCodeId: "-1",
                        status: true
                    };
                },

                setStatus: function (status) {
                    var _this = this;

                    this.spinnerRunning = true;

                    var url = (status.type == CONST_READY) ? Routing.generate('jabberVoiceSetStatusReady') : Routing.generate('jabberVoiceSetStatusNotReady'),
                        data = (status.type == CONST_READY) ? {} : {label: CONST_NOT_READY, idCode: status.idCode};

                    sendAjax(data, url, 'PUT', function (response) {

                        if (response.status) {

                            if (_this.currentStatus) {
                                _this.currentStatus.isActive = false;
                            }

                            _this.currentStatus = status;
                            _this.currentStatus.isActive = true;

                            _this.refreshTitle();

                            toggleJabberStatusToastr();
                        }
                        else if(response.message) {
                            toastr.error(response.message);
                        }

                    }, function (err) {

                        if (_this.currentStatus) {
                            _this.currentStatus.isActive = false;
                        }

                        _this.redrawCurrentStatus(_this.getFailStatus());

                        toastr.error("Wystąpił problem przy zmianie statusu. Proszę się przelogowac. Jeżeli problem się powtarza, niezwłocznie zgłoś to do Administratora.", "Błąd przy zmianie statusu.", {timeOut: 15000});

                    }, function () {
                        _this.spinnerRunning = false;
                    });

                },

                updateCurrentStatus: function () {
                    var _this = this;
                    _this.spinnerRunning = true;

                    return sendAjax({}, Routing.generate('jabberVoiceGetCurrentStatus'), 'GET', function (response) {

                        _this.redrawCurrentStatus(response);

                    }, function (err) {
                        console.error(err);
                    }, function () {
                        _this.spinnerRunning = false;
                    });
                },
                redrawCurrentStatus: function (response) {

                    if (response.status) {

                        var currentIdCode = (modulePhoneStatus.currentStatus) ? modulePhoneStatus.currentStatus.idCode : "-1";

                        if (currentIdCode != response.reasonCodeId) {

                            if (response.reasonCodeId == "-1") {
                                if (modulePhoneStatus.currentStatus) modulePhoneStatus.currentStatus.isActive = false;
                                modulePhoneStatus.currentStatus = null;
                            }
                            else {
                                var statusType = response.message,
                                    reasonId = response.reasonCodeId;

                                if (modulePhoneStatus.currentStatus) {
                                    modulePhoneStatus.currentStatus.isActive = false;
                                }

                                $.each(modulePhoneStatus.statuses, function (i, ele) {
                                    if (ele.type == statusType && ele.idCode == reasonId) {
                                        modulePhoneStatus.currentStatus = modulePhoneStatus.statuses[i];
                                        modulePhoneStatus.currentStatus.isActive = true;
                                    }
                                });
                            }

                        }

                        toggleJabberStatusToastr();

                    }

                }

            }
        });

        /**  Phone Transfer Module */

        modulePhoneTransfer = new VueModule({
            el: '#phone-transfer-module',
            data: {
                duringConversation: vueVars.duringConversation,
                jabberNumberForTransfer: null,
                transferring: false,
                transferCanceled: false,
                completeTransferBtn: null,
                completeTransformWithTaskBtn: null,
                loadingTransferBtn: null,
                taskIsActive: false, // czy jest aktywne zadanie na dashboard
                activeTicketId: null,  // id aktywnego zadania
                canTransfer: false  // Odebrano połączenie i można przekierowac
            },
            mounted: function () {

                var self = this;

                // this.updateContactList();

                this.loadingTransferBtn = Ladda.create(this._$el.find('#transferConversation')[0]);
                // this.completeTransformWithTaskBtn = Ladda.create(this._$el.find('#completeTransformWithTask')[0]);
                this.completeTransferBtn = Ladda.create(this._$el.find('#completeTransfer')[0]);


                this._$el.on('click', '#transferConversation', function (e) {
                    e.preventDefault();

                    self.taskIsActive = false;
                    self.activeTicketId = null;

                    this.transferNumber();

                    setTimeout(function () {
                        // self.completeTransformWithTaskBtn.start();
                        self.completeTransferBtn.start();
                    },10);

                    /** Nasłuchiwanie na rozpoczeciu połączenia wychodzącego */
                    document.addEventListener("out-call-is-connected", self._onStartOutCall);

                }.bind(this));

                this._$el.on('click', '#completeTransformWithTask', function (e) {

                    if (telephonySession && telephonySession._websocket_connected) {

                        telephonySession.call("atlas/telephony/rpc/redirect_task/" + _locale, {
                            ticketId: self.activeTicketId,
                            agentNumber: self.jabberNumberForTransfer
                        }).then(
                            function (result) {
                                console.log(result);
                            },
                            function (error, desc) {
                                toastr.error("Opis: " + error.desc, "Błąd", {timeOut: 7000});
                            }
                        );

                    }

                    self._completeTransfer(e);

                });

                this._$el.on('click', '#completeTransfer', this._completeTransfer);
                this._$el.on('click', '#cancelTransfer', this.cancelTransfer);

                document.addEventListener("on-active-task", function (e) {

                   if(e.detail && e.detail.activeTicketId)  {

                       if(e.detail.activeTicketId === null) {
                           self.taskIsActive = false;
                       }
                       else if(e.detail.activeTicketId){
                           self.taskIsActive = true;
                       }

                       self.activeTicketId = e.detail.activeTicketId;
                       self.taskIsActive = (self.activeTicketId !== null) ? self.activeTicketId : null;

                   }
                });


                var url = Routing.generate('jabberVoiceGetContacts'),
                    XMLHttpRequest = null;

                this._$el.find('.telephony-contact-searcher').typeahead({
                        minLength: 1,
                        highlight: true,
                        classNames: {
                            menu: 'contact-searcher-menu',
                            wrapper: 'telephony-contact-searcher-wrapper'
                        }
                    },
                    {
                        name: 'my-dataset',
                        async: true,
                        limit: 10,
                        source: function (query, processSync, processAsync) {

                            delayedCallback(function () {

                                if (XMLHttpRequest && XMLHttpRequest.status !== 200) {
                                    XMLHttpRequest.abort();
                                    XMLHttpRequest = null;
                                }

                                XMLHttpRequest = $.get(url, {query: query}, function (data) {
                                    return processAsync(data.list);
                                });

                                return XMLHttpRequest;

                            }, 400);

                            return XMLHttpRequest;

                        },
                        display: function (data) {
                            return data.phoneNumber;
                        },
                        templates: {
                            empty: [
                                '<div class="empty-message">',
                                'Brak wyników',
                                '</div>'
                            ].join('\n'),
                            suggestion: function (data) {
                                return '<p><strong>' + data.username + '</strong><span>' + data.phoneNumber + '</span></p>';
                            }
                        }
                    }).on('typeahead:asyncrequest', function () {

                    self.loadingTransferBtn.start();

                }).on('typeahead:asyncreceive', function () {
                    self.loadingTransferBtn.stop();
                }).on('typeahead:select', function (e, data) {
                    self.jabberNumberForTransfer = data.phoneNumber;
                });

            },
            methods: {

                _onStartOutCall: function (e) {


                    var self = this;

                    if(e.detail && e.detail.data)  {


                        // if(String(self.jabberNumberForTransfer).length === 3) {
                        //     self._checkExistsTask();
                        // }

                        self._checkExistsTask();

                        // self.completeTransformWithTaskBtn.stop();
                        self.completeTransferBtn.stop();

                        // var number = moduleConversations.getNumberFromConversation(moduleConversations.conversations[e.detail.data.id].content);
                        // console.log(number);

                    }

                },

                transferNumber: function () {

                    if (this.jabberNumberForTransfer && moduleConversations.ringingNumber) {
                        jabberInstance.transferConversation(this.jabberNumberForTransfer);
                        this.transferring = true;
                    }
                },

                cancelTransfer: function () {

                    document.removeEventListener("out-call-is-connected", self._onStartOutCall);

                    this.canTransfer = false;
                    this.transferring = false;
                    this.transferCanceled = true;
                    jabberInstance.cancelTransfer();

                },

                _completeTransfer: function () {

                    document.removeEventListener("out-call-is-connected", self._onStartOutCall);

                    this.canTransfer = false;
                    this.transferring = false;
                    jabberInstance.completeTransfer();

                },

                _checkExistsTask: function () {

                    var activeTicketEvent = new CustomEvent("check-active-task", {'detail': {extraData: '1'}});
                    sendEventToIframe(activeTicketEvent);

                },

                updateContactList: function () {

                    var $jabberContactList = $('#jabberContactList'),
                        contactArr = [
                            {
                                index: 123,
                                name: 'Stasiu'
                            },
                            {
                                index: 234,
                                name: 'Mirek'
                            }
                        ];

                    $.each(contactArr, function (i, ele) {
                        var $contactItem = $("<option></option>");
                        $contactItem.val(ele.index);
                        $contactItem.text(ele.name);
                        $jabberContactList.append($contactItem);
                    });
                }
            }
        });

        /**  Phone Transfer Module */

        getConversations = function (create) {

            return [moduleConversations.conversations, moduleConversations.currentConversation, moduleCallingOptions.liveConversations];

        };

        moduleCallPanel = new VueModule({
            el: '#phone-call-panel-module',
            data: {
                callBtn: null,
                callNumber: null,
                jabberNumberToCall: null,
                status: 'ready'     /** ready, ringing, call */
            },
            mounted: function () {
                var self = this;

                this.callBtn = Ladda.create($('#start-call')[0]);
                this.jabberNumberToCall = $('#jabberNumberToCall');

                this._$el.find('.toggle-phone-call-panel').on('click', function (e) {
                    setTimeout(function () {
                        this.jabberNumberToCall[0].focus();
                    }.bind(this), 300);
                }.bind(this));

                this._$el.find('#start-call').on('click', function (e) {

                    e.preventDefault();
                    this.startOutCalling();

                }.bind(this));

                this._$el.find('#stop-call').on('click', function (e) {

                    e.preventDefault();
                    this.stopOutCalling();

                }.bind(this));


                var url = Routing.generate('jabberVoiceGetContacts'),
                    XMLHttpRequest = null;

                this._$el.find('.telephony-contact-searcher').typeahead({
                        minLength: 1,
                        highlight: true,
                        classNames: {
                            menu: 'contact-searcher-menu',
                            wrapper: 'telephony-contact-searcher-wrapper'
                        }
                    },
                    {
                        name: 'my-dataset',
                        async: true,
                        limit: 10,
                        source: function (query, processSync, processAsync) {

                            delayedCallback(function () {

                                if (XMLHttpRequest && XMLHttpRequest.status !== 200) {
                                    XMLHttpRequest.abort();
                                    XMLHttpRequest = null;
                                }

                                XMLHttpRequest = $.get(url, {query: query}, function (data) {
                                    return processAsync(data.list);
                                });

                                return XMLHttpRequest;

                            }, 400);

                            return XMLHttpRequest;

                        },
                        display: function (data) {
                            return data.phoneNumber;
                        },
                        templates: {
                            empty: [
                                '<div class="empty-message">',
                                'Brak wyników',
                                '</div>'
                            ].join('\n'),
                            suggestion: function (data) {
                                return '<p><strong>' + data.username + '</strong><span>' + data.phoneNumber + '</span></p>';
                            }
                        }
                    }).on('typeahead:asyncrequest', function () {
                    self.callBtn.start();
                }).on('typeahead:asyncreceive typeahead:asynccancel', function () {
                    self.callBtn.stop();
                }).on('typeahead:select', function (e, data) {
                    self.callNumber = data.phoneNumber;
                });

                this._$el.find('.phone-module-form').on('submit', function (e) {
                    e.preventDefault();
                    self.startOutCalling();
                });

            },
            methods: {
                parseNumber: function (number) {

                    var parsedNumber = number.replace(/\s/g, '');
                    // prefixNumber = parsedNumber.substring(0, 3);

                    /** Polski prefix */
                    if (parsedNumber.substring(0, 4) == "0048") {
                        return "0" + parsedNumber.slice(4, parsedNumber.length);
                    }

                    /** Polski prefix */
                    if (parsedNumber.substring(0, 3) == "+48") {
                        return "0" + parsedNumber.slice(3, parsedNumber.length);
                    }

                    if (parsedNumber.substring(0, 1) == "0") {
                        return parsedNumber;
                    }

                    if (parsedNumber.substring(0, 1) == "+") {
                        parsedNumber = "000" + parsedNumber.slice(1, parsedNumber.length);
                        return parsedNumber;
                    }

                    if (parsedNumber.length >= 13 && parsedNumber.substring(0, 1) != 0) {
                        parsedNumber = "000" + parsedNumber;
                    }
                    else if (parsedNumber.length < 13 && parsedNumber.length > 4 && parsedNumber.substring(0, 1) != 0) {
                        parsedNumber = "0" + parsedNumber;
                    }

                    return parsedNumber;
                },

                startOutCalling: function () {

                    var _this = this;

                    if (this.callNumber) {
                        this.callBtn.start();
                        this.jabberNumberToCall.prop('disabled', true);

                        var _timer = setTimeout(function () {
                            this.status = 'ringing';
                            this.callBtn.stop();
                        }.bind(this), 2000);

                        jabberInstance.startCall(this.parseNumber(this.callNumber), function (e) {

                            if(_timer) {
                                clearTimeout(_timer);
                            }

                            try {

                                var message = 'Error in startAudioConversation. Call number: ' + _this.callNumber + ". ";

                                if(typeof e === "object") {
                                    if(e.message) {
                                        message += 'Message: ' + e.message;
                                    }
                                }
                                else if(typeof e === "string") {
                                    message += 'Message: ' + e;
                                }

                                $.ajax({
                                    url: window.location.origin + "/js-logger",
                                    type: "POST",
                                    data: {
                                        msg: message,
                                        browser_url: window.location.href,
                                        browser_version: navigator.sayswho
                                    }
                                });

                            }
                            catch(e) {
                                console.error('Error when sending error.');

                            }

                            _this.callBtn.stop();
                            _this.stopOutCalling();
                            toastr.error("Wystąpił problem podczas wykonywania połączenia.", "Błąd telefonii", {timeOut: 7000});

                        });

                    }
                    else {
                        // this.jabberNumberToCall.attr('placeholder', 'Proszę podać numer');
                    }

                },

                stopOutCalling: function () {

                    this.callNumber = null;
                    this.status = 'ready';
                    this.jabberNumberToCall.prop('disabled', false);

                    if (moduleConversations.currentConversation && moduleConversations.currentConversation.content) {
                        if (moduleConversations.conversations[moduleConversations.currentConversation.content.ID]) {
                            moduleConversations.conversations[moduleConversations.currentConversation.content.ID].extraAction = 'CANCELED';
                            jabberInstance.endConversation();
                        }
                    }
                }

            }
        });

        /**  Phone Settings Module */

        modulePhoneSetting = new VueModule({
            el: '#phone-setting-module',
            mounted: function () {

                var _this = this;

                $('.main-media-device').on('change', function () {
                    var textOption = $(this).find(':selected').text();
                    _this.setDefaultDevice($(this).data('media-name'), textOption);
                });

                this.speakerVolumeSlider = $('#speakervolumecontrol-range-slider')[0];
                this.ringerVolumeSlider = $('#ringervolumecontrol-range-slider')[0];
                this.microphoneVolumeSlider = $('#microphonevolumecontrol-range-slider')[0];
                this.$ringtonesSelect = $('#ringtonesSelect');

                noUiSlider.create(this.speakerVolumeSlider,
                    {
                        start: 0,
                        range: {min: 0, max: 100}
                    }
                ).on("change", function (n, t) {
                    _this.speakerVolumeValue = parseInt(n[t]);
                    _this.onChangeMediaVolume('speaker', _this.speakerVolumeValue);
                });

                noUiSlider.create(this.ringerVolumeSlider,
                    {
                        start: 0,
                        range: {min: 0, max: 100}
                    }
                ).on("change", function (n, t) {
                    _this.ringerVolumeValue = parseInt(n[t]);
                    _this.onChangeMediaVolume('ringer', _this.ringerVolumeValue);
                });

                noUiSlider.create(this.microphoneVolumeSlider,
                    {
                        start: 0,
                        range: {min: 0, max: 100}
                    }
                ).on("change", function (n, t) {
                    _this.microphoneVolumeValue = parseInt(n[t]);
                    _this.onChangeMediaVolume('microphone', _this.microphoneVolumeValue);
                });

                this._$el.find('.toggle-phone-setting').on('click', function () {

                    if (!_this._$el.hasClass('active')) {
                        // _this.updateFromDefaultMedia();
                    }
                });

                $('.telephony-audio-test').on('click', function (e) {
                    e.preventDefault();

                    var $t = $(this),
                        _audioTestControl = document.getElementById($t.attr('data-target'));

                    $t.toggleClass('active');

                    if($t.hasClass('active')) {
                        _audioTestControl.pause();
                        _audioTestControl.currentTime = 0;
                        _audioTestControl.play();
                    }
                    else {
                        _audioTestControl.pause();
                    }

                });

                $('#microphoneList').on('change', function() {

                    _this.stopMicrophoneMeter();
                    _this.createMicrophoneMeter($(this).find('option:selected').text());

                });

            },
            data: {
                mediaNameList: ['microphone', 'speaker', 'ringer'],
                mediaUpdated: false,
                speakerVolumeSlider: null,
                speakerVolumeValue: 0,
                ringerVolumeSlider: null,
                ringerVolumeValue: 0,
                microphoneVolumeSlider: null,
                microphoneVolumeValue: 0,
                $ringtonesSelect: null,
                prevRingtone: null,
                drawerMicrophone: null,
                audio_stream: null
            },
            methods: {
                onOpenTab: function () {
                    this.createMicrophoneMeter();
                },
                onCloseTab: function () {
                    this.stopMicrophoneMeter();
                },
                stopMicrophoneMeter: function() {

                    if(this.drawerMicrophone !== null) {
                        this.drawerMicrophone.stop();
                        this.drawerMicrophone = null;

                        if(this.audio_stream) {
                            var audioTrack = this.audio_stream.getAudioTracks();
                            if(audioTrack.length) {
                                audioTrack[0].stop();
                            }
                        }
                    }

                },
                createMicrophoneMeter: function (labelDevice) {

                    labelDevice = (typeof labelDevice === "undefined") ? $('#microphoneList option:selected').text() : labelDevice;

                    var audioContext = null,
                        meter = null,
                        canvasContext = null,
                        _this = this;

                    this.stopMicrophoneMeter();

                    canvasContext = document.getElementById( "microphone-meter" ).getContext("2d");
                    window.AudioContext = window.AudioContext || window.webkitAudioContext;
                    audioContext = new AudioContext();

                    // canvasContext.canvas.width = $('#microphone-meter').closest('.control').width();
                    canvasContext.canvas.width = 250;
                    canvasContext.canvas.height = 10;

                    try {

                        requestMedia(labelDevice, 'audioinput', function(device) {

                            var audioSource = device.deviceId;

                            var constraints = {
                                audio: {deviceId: audioSource ? {exact: audioSource} : undefined}
                            };

                            navigator.mediaDevices.getUserMedia(constraints).then(gotStream);

                        });

                    } catch (e) {
                        console.error('getUserMedia threw exception :' + e);
                    }


                    var mediaStreamSource = null;

                    function gotStream(stream) {

                        _this.audio_stream = stream;
                        mediaStreamSource = audioContext.createMediaStreamSource(stream);
                        meter = createAudioMeter(audioContext);

                        mediaStreamSource.connect(meter);
                        _this.drawerMicrophone = drawMicrophoneMeter(meter, canvasContext);
                        _this.drawerMicrophone.start();
                    }

                },
                setRingtone: function (nameRingtone) {

                    var _this = this;

                    $.each(this.$ringtonesSelect.find('option'), function (i2, ele2) {
                        if (ele2.text == nameRingtone) {
                            _this.prevRingtone = _this.$ringtonesSelect.val();
                            _this.$ringtonesSelect.val(ele2.value).trigger('change');
                        }
                    });

                },
                rollBackRingtone: function () {
                    if (this.prevRingtone !== null) {
                        this.$ringtonesSelect.val(this.prevRingtone).trigger('change');
                        this.prevRingtone = null;
                    }
                },
                updateFromDefaultMedia: function (force) {

                    if (this.mediaUpdated && force !== true) return false;

                    this.mediaUpdated = true;

                    var _this = this;

                    var defaultDevices = Cookies.getJSON('atlas_default_devices');

                    if (defaultDevices) {

                        $.each(_this.mediaNameList, function (i, ele) {
                            var _device = defaultDevices[ele];
                            if (_device) {
                                $.each(_this._$el.find('select[data-media-name="' + ele + '"] option'), function (i2, ele2) {
                                    if (ele2.text == _device) {
                                        _this._$el.find('select[data-media-name="' + ele + '"]').val(ele2.value).trigger('change');
                                    }
                                });
                            }
                        });

                        setTimeout(function () {
                            this.mediaUpdated = false;
                        }.bind(this), 5000);
                    }
                },
                setDefaultDevice: function (deviceName, deviceValue) {

                    var defaultDevices = this.getDefaultDeviceFromCookie();
                    defaultDevices[deviceName] = deviceValue;
                    Cookies.set('atlas_default_devices', defaultDevices, {expires: 30});

                },
                getDefaultDeviceFromCookie: function () {

                    var defaultDevices = Cookies.getJSON('atlas_default_devices');
                    return ((defaultDevices) ? defaultDevices : {});

                },
                onChangeMediaVolume: function (mediaName, volume) {
                    jabberInstance.changeVolumeOfMedia(mediaName, volume);
                },
                updateMediaVolume: function (mediaName, volume) {

                    if (this[mediaName + 'VolumeSlider']) {
                        this[mediaName + 'VolumeSlider'].noUiSlider.set(volume);
                        this[mediaName + 'VolumeValue'] = volume;
                    }

                }
            }
        });

        /**  Phone Settings Module */

        moduleConversations = new VueModule({
            el: '#incoming-call-box',
            data: {
                currentConversation: null,
                conversations: {},
                ringingNumber: null,
                platformNumber: null,
                ringingName: null,
                openInNewTab: false,
                clientInfo: '',
                loadingClient: true,
                welcoming: 'Dzień dobry!',
                callState: null
            },
            mounted: function () {

                this._$el = $(this.$el);

                if (!CONST_CALL_CENTER_MODE) {
                    this._$el.addClass('simple-version')
                }

                $('#phone-pick-up').on('click', function () {

                    moduleConversations.openInNewTab = false;

                    if (jabberNotification) {
                        setTimeout(function () {
                            jabberNotification.close();
                        }, 100);
                    }

                    if(atlasExtNotification) {
                        atlasExtension.hideNotification();
                    }

                    jabberInstance.pickUp();

                });

                $('#phone-pick-up-in-new-tab').on('click', this.onPickUpNewTab);

                //
                // $(window).on('keypress', function(e){
                //     if(e.keyCode == 121 ) {
                //         console.log(_this.conversations);
                //     }
                // });
            },
            methods: {

                onPickUpNewTab: function(e) {
                    e.preventDefault();

                    moduleConversations.openInNewTab = true;

                    if (jabberNotification) {
                        setTimeout(function () {
                            jabberNotification.close();
                        }, 100);
                    }

                    if(atlasExtNotification) {
                        atlasExtension.hideNotification();
                    }

                    jabberInstance.pickUp();

                },
                handleJabberChannelBroadcast: function () {

                    var _this = this;

                    jabberChannel.on('updateCurrentConversationUniqueId', function (obj) {

                        if (jabberChannel.currentWindowIsMaster()) return;

                        if (_this.currentConversation) {
                            _this.conversations[_this.currentConversation.content.ID].uniqueRandom = obj.uniqueRandom;
                        }
                        else {
                            console.error('Brak konwersacji!');
                        }
                    });
                },
                updateWelcome: function (number) {

                    var welcome = "Dzień dobry.",
                        name = null,
                        idPlatform = null;

                    var helpLine = helpLines.filter(function (obj) {
                        return obj.number == number;
                    });

                    if (helpLine) {
                        if (Array.isArray(helpLine)) helpLine = helpLine[0];
                        if (helpLine) {
                            welcome = helpLine.welcome;
                            name = helpLine.platform.name;
                            idPlatform = helpLine.platform.id;
                        }
                    }

                    if (name) this.ringingName = name;
                    this.welcoming = this.parseWelcoming(welcome);

                    return idPlatform;
                },
                parseWelcoming: function (welcome) {

                    // Dzień dobry, [Platforma] Assistance. [Imię i nazwisko konsultanta], w czym mogę pomóc?

                    var helloMsg = ((new Date()).getHours() >= 20 || (new Date()).getHours() <= 4 ) ? "Dobry wieczór" : "Dzień dobry";

                    return welcome.parseParamsInString({
                        hello: helloMsg,
                        username: _uInfo._username
                    }, '@', '@');

                },
                getPlatformId: function (number) {

                    number = this.platformNumber | number;

                    var helpLine = helpLines.filter(function (obj) {
                        return obj.number == number;
                    });

                    if (helpLine) {
                        return helpLine.platform.id;
                    }

                    return null;

                },
                sizeOfConversations: function () {
                    var size = 0, key, _this = this;
                    for (key in _this.conversations) {
                        if (_this.conversations.hasOwnProperty(key)) size++;
                    }
                    return size;
                },
                getNameFromConversation: function (telephonyConversation) {
                    if (telephonyConversation &&
                        telephonyConversation.participants &&
                        telephonyConversation.participants[0] &&
                        telephonyConversation.participants[0].name) {
                        return telephonyConversation.participants[0].name;
                    }
                    else {
                        return null;
                    }
                },
                getNumbersFromConversation: function (telephonyConversation) {
                    if (telephonyConversation &&
                        telephonyConversation.participants) {

                        var numbers = [];

                        $.each(telephonyConversation.participants, function (i, ele) {
                            numbers.push(ele.number);
                        });

                        return numbers;
                    }
                    else {
                        return [];
                    }
                },
                getNumberFromConversation: function (telephonyConversation) {
                    if (telephonyConversation &&
                        telephonyConversation.participants &&
                        telephonyConversation.participants[0] &&
                        telephonyConversation.participants[0].number) {
                        return telephonyConversation.participants[0].number;
                    }
                    else {
                        return null;
                    }
                },
                getParticipant: function (telephonyConversation) {
                    if (telephonyConversation &&
                        telephonyConversation.participants &&
                        telephonyConversation.participants[0]) {
                        return {
                            number: telephonyConversation.participants[0].number,
                            name: telephonyConversation.participants[0].name,
                            uri: telephonyConversation.participants[0].uri
                        }
                    }
                    else {
                        return null;
                    }
                },
                outcomingConversation: function (telephonyConversation) {

                    var isNewConnection = (!this.conversations[telephonyConversation.ID]);

                    cLog('isNewConnection: ' + isNewConnection);

                    if (isNewConnection) {

                        var _number = this.getNumberFromConversation(telephonyConversation);

                        /** Wysłanie Eventu do iframe, że jest nowe połączenie wychodzące */
                        if(CONST_CALL_CENTER_MODE && jabberChannel.currentWindowIsMaster()) {

                            var dataEvent = {
                                detail: {
                                    number: _number
                                }
                            };

                            var event = new CustomEvent("out-coming-connection", dataEvent);
                            sendEventToIframe(event);

                        }

                        this.createConversationObj(telephonyConversation, 'OUT');

                        toggleJabberStatusToastr();

                        this.ringingNumber = _number;
                        this.ringingName = this.getNameFromConversation(telephonyConversation);
                    }

                },
                incomingConversation: function (telephonyConversation) {

                    var _this = this;

                    var promiseCreateConversation = this.createConversationObj(telephonyConversation, 'IN');

                    if (CONST_CALL_CENTER_MODE) {
                        if (!_this.conversations[telephonyConversation.ID].details.idDialog) {

                            sendAjax({}, Routing.generate('jabberVoiceCurrentDialog'), 'GET', function (response) {

                                changeRingtone(response.details.toAddress);

                                // response.details:
                                // departamentCode:"750"
                                // fromAddress:"0721xxxxxx"
                                // id:158
                                // idDialog:"23494701"
                                // status:"ALERTING"
                                // toAddress:"721"
                                // type:"ACD_IN"

                                if (response.status) {

                                    /** Dodawanie nowej rozmowy do JabberVoice z Cisco Finesse  */

                                    if (jabberChannel.currentWindowIsMaster()) {
                                        sendAjax({}, Routing.generate('jabberVoiceProcessDialog', {idDialog: response.details.idDialog}), 'POST', function (response2) {

                                            if (_this.currentConversation && response2.conversation) {
                                                _this.currentConversation.details.id = response2.conversation.id;
                                            }

                                        }, function (err) {
                                            handleAjaxResponseError(err, true);
                                        });
                                    }

                                    /** Tylko odpalany dla WindowMaster */

                                    if (promiseCreateConversation) {
                                        $.when(promiseCreateConversation).then(
                                            function (status) {
                                                // Success

                                                if (status.ele && status.id) {
                                                    status.ele.number = response.details.fromAddress;

                                                    sendAjax({}, Routing.generate('jabberVoiceUpdateNumberConversation', {
                                                        uniqueId: status.id,
                                                        number: response.details.fromAddress
                                                    }), 'PUT', function () {

                                                    }, function () {

                                                    })
                                                }
                                            },
                                            function (status) {
                                                // Error
                                                console.error('From promise', status);
                                            }
                                        )
                                    }

                                    _this.ringingNumber = response.details.fromAddress;
                                    _this.platformNumber = response.details.toAddress;
                                    _this.ringingName = "";

                                    var idPlatform = _this.updateWelcome(_this.platformNumber);

                                    // var idPlatform = _this.getPlatformId();
                                    if (idPlatform === null) {
                                        // toastr.error("Numer '" + _this.platformNumber + "' nie jest przypisany do żadnej platformy.", "Nie znaleziono platformy.", {timeOut: 7000});

                                        /** Dodatkowe info o dzwoniącym */
                                        _this.getInfoAboutClient();
                                    }
                                    else {

                                        /** Jeżeli jest połączenie WebSocket i okno jest Master, to zakłada nową sprawę */

                                        if(jabberChannel.currentWindowIsMaster()) {

                                            $.ajax({
                                                url: Routing.generate('operationalDashboardInterfaceCreateNewTask'),
                                                type: "POST",
                                                data: {
                                                    numberAddress: _this.ringingNumber,
                                                    platformNumber: _this.platformNumber,
                                                    userId: _uInfo._id,
                                                    originalUserId: _uInfo._oId,
                                                    idPlatform: idPlatform,
                                                    idDialog: response.details.idDialog
                                                },
                                                success: function (result) {

                                                    /** Ten Event jest wysyłany do Iframe, że odebrano telefon. (sendSignalPickUpToIframe) */
                                                    conversationEvent = result;

                                                    /** Dodatkowe info o dzwoniącym - VIP? */
                                                    _this.getInfoAboutClient(result.processDescription.groupProcessId);

                                                    if (promiseCreateConversation) {
                                                        $.when(promiseCreateConversation).then(
                                                            function (status) {

                                                                /** Aktualizacja ConversationTelephony o IdDialog i ProcessInstanceId */

                                                                if (status.id) {

                                                                    var _data = {
                                                                        idDialog: response.details.idDialog,
                                                                        processInstanceId: result.processInstances
                                                                    };

                                                                    sendAjax(_data, Routing.generate('jabberVoiceUpdateIdDialogAndProcessId', {
                                                                        uniqueId: status.id
                                                                    }), 'PUT', function () {

                                                                    }, function () {

                                                                    })
                                                                }
                                                            },
                                                            function (status) {
                                                                // Error
                                                                console.error('From promise', status);
                                                            }
                                                        )
                                                    }

                                                },
                                                error: function (error) {
                                                    toastr.error("", "Błąd stworzenia sprawy", {timeOut: 7000});
                                                    console.info("RPC Error", error);
                                                }
                                            });

                                            // if (telephonySession) {
                                            //     telephonySession.call("atlas/telephony/rpc/generate_task/" + _locale , {
                                            //         numberAddress: _this.ringingNumber,
                                            //         platformNumber: _this.platformNumber,
                                            //         userId: _uInfo._id,
                                            //         originalUserId: _uInfo._oId,
                                            //         idPlatform: idPlatform,
                                            //         idDialog: response.details.idDialog
                                            //     }).then(
                                            //         function (result) {
                                            //
                                            //             /** Ten Event jest wysyłany do Iframe, że odebrano telefon. (sendSignalPickUpToIframe) */
                                            //             conversationEvent = result;
                                            //
                                            //             /** Dodatkowe info o dzwoniącym - VIP? */
                                            //             _this.getInfoAboutClient(result.processDescription.groupProcessId);
                                            //
                                            //             if (promiseCreateConversation) {
                                            //                 $.when(promiseCreateConversation).then(
                                            //                     function (status) {
                                            //
                                            //                         /** Aktualizacja ConversationTelephony o IdDialog i ProcessInstanceId */
                                            //
                                            //                         if (status.id) {
                                            //
                                            //                             var _data = {
                                            //                                 idDialog: response.details.idDialog,
                                            //                                 processInstanceId: result.processInstances
                                            //                             };
                                            //
                                            //                             sendAjax(_data, Routing.generate('jabberVoiceUpdateIdDialogAndProcessId', {
                                            //                                 uniqueId: status.id
                                            //                             }), 'PUT', function () {
                                            //
                                            //                             }, function () {
                                            //
                                            //                             })
                                            //                         }
                                            //                     },
                                            //                     function (status) {
                                            //                         // Error
                                            //                         console.error('From promise', status);
                                            //                     }
                                            //                 )
                                            //             }
                                            //
                                            //         },
                                            //         function (error, desc) {
                                            //             toastr.error("Opis: " + error.desc, "Błąd stworzenia sprawy", {timeOut: 7000});
                                            //             console.info("RPC Error", error, desc);
                                            //         }
                                            //     );
                                            // }
                                            // else {
                                            //     console.error('WebSocket Session nie istnieje!');
                                            // }
                                        }

                                    }

                                    //TODO dać update na osobę która dzwoni
                                    // this.updateConversationStatus(this.conversations[telephonyConversation.ID].uniqueRandom, 'PICK_UP');
                                    // this.conversations[telephonyConversation.ID].uniqueRandom

                                    if(_this.conversations[telephonyConversation.ID]) {
                                        _this.conversations[telephonyConversation.ID].details = response.details;
                                    }

                                }
                            }, function (err) {
                                handleAjaxResponseError(err, true);
                            }, function () {
                                if (_this.conversations[telephonyConversation.ID]) {
                                    _this.showNotifyAndCallBox();
                                }
                            });
                        }
                    }
                    else {

                        this.ringingNumber = this.getNumberFromConversation(telephonyConversation);
                        this.ringingName = this.getNameFromConversation(telephonyConversation);

                        this.showNotifyAndCallBox();
                    }

                    /** Blokownie panelu w czasie połączenia */

                    // if (telephonyConversation.callState == "Ringin") {
                    //     moduleCallPanel.lockTab();
                    // }

                },

                showNotifyAndCallBox: function () {
                    /** Wyświetenie powiadomienia (Browser Notification) */

                    if (jabberChannel.currentWindowIsMaster() && !document.hasFocus()) {

                        if(atlasExtension) {
                            atlasExtension.showNotification(this.ringingNumber, this.ringingName);
                        }
                        else {
                            showJabberNotify(this.ringingNumber, this.ringingName, function () {
                                jabberInstance.pickUp();
                            });
                        }

                    }

                    this.showBox();
                },

                getInfoAboutClient: function (processGroupId) {

                    if (!CONST_CALL_CENTER_MODE) return false;

                    var that = this;

                    this.loadingClient = true;

                    /** Sprawdzanie, czy dzwoni VIP. */

                    var data;

                    if (processGroupId) {
                        data = {
                            type: 'process',
                            data: processGroupId
                        }
                    }
                    else {
                        data = {
                            type: 'number',
                            data: that.ringingNumber
                        }
                    }

                    sendAjax({}, Routing.generate('jabberVoiceGetClientInfo', data), 'GET', function (response) {

                        if (response.welcome) {
                            that.clientInfo = response.welcome;
                        }
                        else {
                            that.clientInfo = "-";
                        }

                    }, function (err) {
                        console.error(err);
                    }, function () {
                        that.loadingClient = false;
                    });

                },

                startConversation: function (telephonyConversation) {

                    var _this = this;

                    if (telephonyConversation.callState == "Connected" && this.conversations[telephonyConversation.ID] && !this.conversations[telephonyConversation.ID].wasStarted) {
                        /** Jest połączenie */

                        this.conversations[telephonyConversation.ID].wasStarted = true;

                        if (this.conversations[telephonyConversation.ID].uniqueRandom) {
                            this.updateConversationStatus(this.conversations[telephonyConversation.ID].uniqueRandom, 'PICK_UP');
                        }
                        else {
                            console.error('Konwersacja nie posiada jeszcze uniqueRandom - coś jest nie tak.');
                        }

                        /** Jeżeli jest tryb Konsultanta na centrum */

                        if (CONST_CALL_CENTER_MODE) {

                            modulePhoneStatus.canWrapUp = true;

                            /** Wysyła sygnał do iframe o odebraniu połączenia (po stworzeniu sprawy na bazie) */

                            if (jabberChannel.currentWindowIsMaster()) {

                                _this.sendSignalPickUpToIframe();

                                /** Zaktualizowanie statusu rozmowy w CC */

                                if (_this.conversations[telephonyConversation.ID].details.idDialog) {

                                    if (jabberChannel.currentWindowIsMaster()) {
                                        sendAjax({},
                                            Routing.generate('jabberVoiceProcessDialog', {idDialog: _this.conversations[telephonyConversation.ID].details.idDialog}),
                                            'POST',
                                            function (response2) {

                                            },
                                            function (err) {
                                                console.error(err);
                                            }
                                        );
                                    }
                                }

                            }
                        }

                        moduleCallingOptions.addLiveConversation(this.conversations[telephonyConversation.ID]);


                        /** Gdy połączenie wychodzące się rozpocznie to odblokowac input i przycisk w moduleCallPanel */

                        if (this.conversations[telephonyConversation.ID].type === "OUT") {
                            moduleCallPanel.callNumber = null;
                            moduleCallPanel.status = 'ready';
                            moduleCallPanel.jabberNumberToCall.prop('disabled', false);

                            var cEvent = new CustomEvent("out-call-is-connected", {'detail': {data: {id: telephonyConversation.ID}}});
                            document.dispatchEvent(cEvent);

                        }

                        /** Blokowanie taba z dzwonieniem */

                        if (this.callState == "Ringout") {
                            // moduleCallPanel.lockTab();
                            moduleCallPanel.callBtn.stop();
                            moduleCallPanel.callNumber = null;
                            // moduleCallPanel.jabberNumberToCall.prop('disabled', false);
                        }

                        this.hideBox(true);
                        moduleCallingOptions.start(this.ringingNumber, this.ringingName);

                    }
                    else if (telephonyConversation.callState == "OffHook") {        /** Połączenie wychodzące - Ringing */

                    }
                    else if (telephonyConversation.callState == "Ringin") {        /** Połączenie przychodzące - Ringing */


                    }

                    toggleJabberStatusToastr();

                },

                sendSignalPickUpToIframe: function () {

                    var i = 0;
                    var eventIframe = setInterval(function () {

                        if (conversationEvent) {

                            conversationEvent.isForce = (conversationEvent.idPlatform == eCallPlatformId);

                            try {

                                /** Jeżeli odebrano połączenie poprzez przycisk "Odbierz i otwórz sprawę w nowym oknie" */

                                if(moduleConversations.openInNewTab) {

                                    clearInterval(eventIframe);

                                    var url = Routing.generate('operational_dashboard_index', {
                                        action: 'telephony-pick-up',
                                        process_instance: conversationEvent.processInstances,
                                        isforce: true,
                                        disable_phone_module: 1
                                    }, true);

                                    var win = window.open(url, '_blank');

                                    win.focus();

                                    moduleConversations.openInNewTab = false;
                                    conversationEvent = null;

                                }
                                else {

                                    if(atlasExtension) {

                                        atlasExtension.getPort().postMessage({action: 'focus-first-dashboard', detail: conversationEvent});
                                        conversationEvent = null;
                                        clearInterval(eventIframe);

                                    }
                                    else {

                                        var isOperationDashboard = $atlasIframe.contents().find('body').hasClass('operational-page');

                                        /** Jeżeli w iframe nie jest załadowany dashboard to robi przekierowanie do operation dashboard */

                                        if (isOperationDashboard) {

                                            var telephonyEvent = new CustomEvent("telephony-pick-up", {'detail': conversationEvent});
                                            sendEventToIframe(telephonyEvent);

                                        }
                                        else {

                                            var message = {
                                                type: 'telephony-pick-up-with-reload',
                                                data: conversationEvent
                                            };

                                            $atlasIframe[0].contentWindow.postMessage(message, Routing.generate('index', [], true));

                                        }

                                        conversationEvent = null;
                                        clearInterval(eventIframe);
                                    }

                                }

                            } catch (Exception) {
                                i++;
                                console.error(Exception);
                            }

                        }
                        else {
                            i++;
                        }
                        /** Po 10 próbach (3 sek) anuluje informacje o odebraniu połaczenia */

                        if (i === 10) {
                            clearInterval(eventIframe);
                            moduleConversations.openInNewTab = false;
                        }

                    }, 300);
                },
                notifyAboutConversationEnded: function(status, data, telephonyConversation) {

                    if (jabberChannel.currentWindowIsMaster()) {

                        if(typeof atlasTabChannel === "undefined" || !atlasTabChannel) return;

                        atlasTabChannel.setCurrentWindowAsMaster();

                        atlasTabChannel.broadcast('conversationEnded', {
                            'status' : status,
                            'data' : data
                        });

                        var message = {
                            data: {
                                'status' : status,
                                'data' : data
                            },
                            type: 'conversationEnded'
                        };

                        // sendEventToIframe(message);

                        $atlasIframe[0].contentWindow.postMessage(message, Routing.generate('index', [], true));

                        // $atlasIframe[0].contentWindow.postMessage(message, Routing.generate('index', [], true));

                    }

                },
                conversationEnded: function (telephonyConversation) {

                    var _this = this;

                    if (vueVars.duringConversation.isDuring) {

                        cLog('Removing conversation...');

                        if(this.conversations[telephonyConversation.ID]) {

                            this.updateConversationStatus(this.conversations[telephonyConversation.ID].uniqueRandom, 'FINISHED');

                            /** Zaktualizowanie statusu rozmowy dla CC */
                            if (_this.conversations[telephonyConversation.ID].details.idDialog) {
                                if (CONST_CALL_CENTER_MODE) {
                                    if (jabberChannel.currentWindowIsMaster()) {
                                        sendAjax({}, Routing.generate('jabberVoiceFinishDialog', {idDialog: _this.conversations[telephonyConversation.ID].details.idDialog}),
                                            'POST',
                                            function (response) {

                                            },
                                            function (err) {
                                                console.error(err);
                                            }
                                        );
                                    }
                                }
                            }

                        }

                        this.removeConversation(telephonyConversation);

                        if (this.sizeOfConversations() === 0) {

                            enableLogout();

                            vueVars.duringConversation.stop();

                            moduleCallingOptions.timing = false;
                            clearInterval(timerInterval);
                            timing = 0;
                            updateTime($phoneTopPanel.find('.timing'));

                            /** Powiadomienie IFRAME, o zakończeniu połącznia */
                            this.notifyAboutConversationEnded("FINISHED", {
                                ringingNumber: this.ringingNumber,
                                platformNumber: this.platformNumber
                            }, telephonyConversation);

                            this.ringingNumber = null;
                            this.platformNumber = null;
                            this.clientInfo = null;
                            this.loadingClient = true;
                            moduleCallPanel.callNumber = null;
                            moduleCallPanel.status = 'ready';
                            moduleCallPanel.jabberNumberToCall.prop('disabled', false);

                            $('#mute-volume').removeClass('is-mute');

                            this.currentConversation = null;
                        }
                        else {

                            /** Sprawdza czy transfer został anulowany i próba połączenia z poprzednim klientem */
                            if (modulePhoneTransfer.transferCanceled) {
                                modulePhoneTransfer.transferCanceled = false;

                                $.each(this.conversations, function (i, ele) {
                                    if (ele.content && ele.content.callState == "Hold") {
                                        ele.content.resume();
                                    }
                                })
                            }
                        }

                    } else {

                        if (!this.conversations[telephonyConversation.ID]) return;

                        /** Ktoś dzwonił ale nie odebrano */

                        if (this.conversations[telephonyConversation.ID].content.callState == "Ringin") {

                            if (conversationEvent) conversationEvent = null;

                            if (this.conversations[telephonyConversation.ID].uniqueRandom) {

                                moduleHistoryCall.incrementNotPickUpCalls();
                                this.updateConversationStatus(this.conversations[telephonyConversation.ID].uniqueRandom, 'NOT_PICK_UP');

                            }
                            else {
                                console.error('Konwersacja nie posiada jeszcze uniqueRandom - coś nie tak.');
                            }

                            if (jabberNotification) {
                                setTimeout(function () {
                                    jabberNotification.close();
                                }, 100);

                                if(atlasExtNotification) {
                                    atlasExtension.hideNotification();
                                }
                            }
                        }

                        /** Przerwanie rozmowy wychodzącej*/

                        if (this.conversations[telephonyConversation.ID].extraAction == 'CANCELED') {

                            /** Powiadomienie IFRAME, o zakończeniu połącznia */
                            this.notifyAboutConversationEnded("CANCELED", {
                                ringingNumber: this.ringingNumber
                            }, telephonyConversation);

                            this.updateConversationStatus(this.conversations[telephonyConversation.ID].uniqueRandom, 'CANCELED');

                        }

                        this.hideBox();

                        this.ringingNumber = null;
                        this.platformNumber = null;
                        this.clientInfo = null;
                        this.loadingClient = true;

                        this.removeConversation(telephonyConversation);
                        this.currentConversation = null;

                    }

                    moduleCallPanel.unLockTab();
                    modulePhoneStatus.setWrapUp();

                },

                showBox: function () {
                    this._$el.removeClass('moved');
                    this._$el.fadeIn(500).addClass('visible');
                },
                hideBox: function (moveBox) {

                    var _this = this;

                    if (moveBox === true && CONST_CALL_CENTER_MODE && moduleConversations.currentConversation.type === "IN") {

                        this._$el.addClass('moved').removeClass('visible');

                        setTimeout(function () {

                            _this._$el.fadeOut(500);

                        }, 5000);

                    }
                    else {
                        modulePhoneSetting.rollBackRingtone();
                        this._$el.fadeOut(500).removeClass('visible');
                    }
                },
                forceEndCurrentConversation: function () {
                    console.log(this.currentConversation);
                },
                createConversationObj: function (telephonyConversation, type) {

                    var _this = this;

                    /** Potrzebne wyżej, żeby zaktualizować numer na liście - gdy telefon z infolini */

                    var dfd = jQuery.Deferred();

                    if (this.conversations[telephonyConversation.ID]) {
                        cLog('Create... conversation exist.');

                        if (this.conversations[telephonyConversation.ID].content) {
                            this.conversations[telephonyConversation.ID].content = telephonyConversation;
                        }
                    }
                    else {
                        cLog('Create!');

                        this.conversations[telephonyConversation.ID] = {
                            details: {},
                            conversationID: null,
                            uniqueRandom: null,
                            extraAction: null,
                            content: telephonyConversation,
                            startDateTime: null,
                            wasStarted: false,
                            type: type
                        };

                        var participant = this.getParticipant(telephonyConversation);

                        if (jabberChannel.currentWindowIsMaster()) {

                            var uniqueRandom = (Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000).toString() + ((new Date()).getTime()).toString();

                            this.conversations[telephonyConversation.ID].uniqueRandom = uniqueRandom;

                            /** Aktualizacja Unique ID konwersacji w każdym tabie */

                            // jabberChannel.setItem('currentConversation', this.conversations[telephonyConversation.ID]);

                            /** Zapisanie nowej konwersacji w bazie */

                            var isOperationDashboard = $atlasIframe.contents().find('body').hasClass('operational-page'),
                                caseRootId = null;

                            if(isOperationDashboard && $atlasIframe[0].contentWindow.currentProcess) {
                                caseRootId = $atlasIframe[0].contentWindow.currentProcess.rootId;
                            }

                            sendAjax({
                                    conversation: {
                                        type: (type) ? type : 'OTHER',
                                        uri: participant.uri,
                                        name: participant.name,
                                        caseRooId: caseRootId,
                                        unique_random: uniqueRandom,
                                        from: (type === "OUT") ? cwic.TelephonyController.getConnectedTelephonyDevice().activeLine : _this.getNumberFromConversation(telephonyConversation),
                                        to: (type === "IN") ? cwic.TelephonyController.getConnectedTelephonyDevice().activeLine : _this.getNumberFromConversation(telephonyConversation)
                                    }
                                },
                                Routing.generate('jabberVoiceAddConversation'), 'POST', function (response) {

                                    if (response.status) {

                                        jabberChannel.broadcast('updateCurrentConversationUniqueId', {'uniqueRandom' : uniqueRandom});

                                        if(!_this.conversations[telephonyConversation.ID]) {
                                            dfd.reject();
                                            return false;
                                        }

                                        _this.conversations[telephonyConversation.ID].conversationID = response.conversation.id;

                                        if (!participant.number) participant.number = response.conversation.participantNumber;
                                        if (!participant.name) {
                                            participant.name = response.conversation.participantName;
                                            _this.ringingName = participant.name;
                                        }

                                        // jabberChannel.setItem('currentConversation', _this.conversations[telephonyConversation.ID]);
                                        // jabberChannel.broadcast('updateCurrentConversation');

                                        moduleHistoryCall.appendToHistory(participant, type, function (historyElement) {
                                            if (historyElement === false) {
                                                dfd.reject();
                                            }
                                            else {
                                                dfd.resolve({ele: historyElement, id: uniqueRandom});
                                            }
                                        });

                                    }

                                }, function (err) {
                                    dfd.reject();
                                    console.error(err);
                                });
                        }

                    }


                    this.currentConversation = this.conversations[telephonyConversation.ID];

                    return dfd.promise();
                },

                updateConversationObj: function (telephonyConversation) {

                    if (this.conversations[telephonyConversation.ID]) {

                        /** SET CURRENT CONVERSATION */

                        this.conversations[telephonyConversation.ID].content = telephonyConversation;

                        if (telephonyConversation.callState === "Connected") {
                            moduleConversations.currentConversation = this.conversations[telephonyConversation.ID];
                        }

                        moduleCallingOptions.updateLiveConversation(this.conversations[telephonyConversation.ID]);
                    }

                },

                removeConversation: function (telephonyConversation) {

                    if (jabberChannel.currentWindowIsMaster()) {
                        // jabberChannel.setItem('currentConversation', null);
                    }

                    moduleCallingOptions.removeLiveConversation(telephonyConversation.ID);

                    delete this.conversations[telephonyConversation.ID];
                },

                updateConversationStatus: function (ConversationTelephonyUniqueRandom, status) {

                    if (jabberChannel.currentWindowIsMaster()) {

                        if (!ConversationTelephonyUniqueRandom) {
                            console.error("TelephonyUniueId empty. Transaction required?");
                            return false;
                        }
                        sendAjax({},
                            Routing.generate('jabberVoiceUpdateConversation', {
                                idConversation: ConversationTelephonyUniqueRandom,
                                status: status
                            }), 'PUT', function (response) {

                            }, function (err) {
                                console.error(err);
                            });

                    }
                }
            }
        });
    }

    function getSecondConversation(id) {

        var secondConversation = null;

        $.each(moduleConversations.conversations, function (_id, ele) {
            if (_id !== id) {
                secondConversation = ele;
                return false;
            }
        });

        return secondConversation;
    }

    function toggleJabberStatusToastr(msg) {
        if (!vueVars.duringConversation.isDuring && CONST_CALL_CENTER_MODE && (modulePhoneStatus.currentStatus === null || (modulePhoneStatus.currentStatus.type === CONST_NOT_READY && (modulePhoneStatus.currentStatus.idCode == "1" || modulePhoneStatus.currentStatus.idCode == 2) ))) {

            var label = (modulePhoneStatus.currentStatus === null) ? 'Niedostępny' : modulePhoneStatus.currentStatus.label;
            if (statusToastr !== null && typeof statusToastr !== "undefined") {
                statusToastr.find('.toast-title').text('Status: ' + label);
            }
            else if (modulePhoneStatus.currentStatus) {
                var statusToastr = toastr.warning('00:00:00', 'Status: ' + label, {
                    timeOut: 0,
                    extendedTimeOut: 0,
                    closeDuration: 100,
                    tapToDismiss: false,
                    preventDuplicates: true,
                    toastClass: 'jabber-toast'
                });

                $.ajax({
                    url: Routing.generate('ajax_telephony_status_state_change_date'),
                    type: "POST",
                    success: function (response) {
                        var $statusToastr = $('.jabber-toast.toast-warning');
                        $statusToastr.find('.toast-message').text(response.time);
                        $statusToastr.find('.toast-message').attr('data-start-time',response.timestamp);

                        statusToastrTimer = setInterval(function () {
                            var myTime = $statusToastr.find('.toast-message').attr('data-start-time');
                            var epoch = (parseInt(myTime)*1000);//+ (new Date().getTimezoneOffset() * -1);
                            var msec = Date.now() - epoch;

                            var hh = Math.floor(msec / 1000 / 60 / 60);
                            msec -= hh * 1000 * 60 * 60;
                            var mm = Math.floor(msec / 1000 / 60);
                            msec -= mm * 1000 * 60;
                            var ss = Math.floor(msec / 1000);
                            msec -= ss * 1000;

                            hh = hh.toString().length === 1 ? "0"+hh : hh;
                            mm = mm.toString().length === 1 ? "0"+mm : mm;
                            ss = ss.toString().length === 1 ? "0"+ss : ss;

                            $statusToastr.find('.toast-message').html(hh+":"+mm+":"+ss);

                        }.bind(this), 1000);

                    },
                });
                $('.jabber-toast.toast-warning').find('.toast-title').text('Status: ' + label);
            }
        }
        else {
            if (statusToastr !== null) {
                if(statusToastrTimer !== null && typeof statusToastrTimer !== "undefined"){
                    clearInterval(statusToastrTimer);
                }
                toastr.clear(statusToastr);
                statusToastr = null;
            }
        }
    }

    function cLog(msg) {
        if (JabberVoice._options.debugMode) {
            if (typeof msg === "object") {
                console.log(msg);
            }
            else {
                console.log('%c' + msg, 'background:#91cef3;color:#000;padding:5px');
            }
        }
    }

    function showJabberNotify(number, name, onClick) {

        if (!("Notification" in window)) {
            console.info("This browser does not support desktop notification");
            return false;
        }

        if (Notification.permission === "granted") {
            var body = (name) ? name : number;
            body = "Dzwoni: " + body;

            spawnNotification(body, 'http://www.trucks.com.pl/wp-content/uploads/2015/05/Starter-logo.jpg', 'Atlas - połączenie', function () {
                if (typeof onClick == "function") {
                    onClick();
                }
            });
        }
        else {
            Notification.requestPermission();
        }

    }

    function spawnNotification(theBody, theIcon, theTitle, onClick) {
        var options = {
            body: theBody,
            icon: theIcon
        };

        jabberNotification = new Notification(theTitle, options);

        jabberNotification.onclick = function (event) {
            event.preventDefault();
            parent.focus();
            window.focus();

            if (typeof onClick == "function") {
                onClick();
            }

            this.close();
        }
    }


}(window.jQuery, window, document));