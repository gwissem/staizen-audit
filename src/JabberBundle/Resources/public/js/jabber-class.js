/**
 * Documentation:
 * https://developer.cisco.com/site/jabber-websdk/develop-and-test/voice-and-video/api/
 */

var ciscobase = {};
ciscobase.$ = jQuery.noConflict(true);

if (typeof(window.jQuery) == "undefined") {
    window.jQuery = ciscobase.$;
}

if (typeof(window.$) == "undefined") {
    window.$ = ciscobase.$;
}

window.ciscobase = ciscobase;

var delayedCallback1 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

var delayedCallback2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

var JabberVoice = (function () {

    var mediaInited = false,
        ringtoneInited = false;

    // Constructor
    function JabberVoice(username, password, options) {

        if (typeof cwic === 'undefined') {
            console.error('Cwic library not defined!');
        }

        JabberVoice._options = $.extend({}, getDefaultOptions(), options || {});

        JabberVoice._username = username;
        JabberVoice._password = password;

    }

    function initPrivateVariable() {

        JabberVoice._serverList = [];

        JabberVoice._telephonyDevices = [];

        JabberVoice._ringtoneList = [];

        JabberVoice._selectedConversationID = null;
        JabberVoice._telephonyConversations = {};
        JabberVoice._isMuted = false;

        JabberVoice._media =
            {
                Speakers: [],
                Microphones: [],
                Ringers: [],
                Cameras: []
            };

        JabberVoice._numberOfTryLogin = 0;

        JabberVoice._connected = false;
        JabberVoice._connecting = false;

    }

    /**
     * Dodanie nasłuchiwanie zdarzeń na Cwic
     */

    function addEventHandlerOnCwic() {
        initializeSystemHandlers();
        initializeInvalidCertificateEventHandlers();
    }

    /**
     * Ustawianie parametrów w Cwic
     */

    function setConfigOnCwic() {

        cwic.SystemController.setLoggingLevel(JabberVoice._options.loggingLevel);

    }

    function initializeSystemHandlers() {

        cwic.SystemController.addEventHandler('onInitialized', JabberVoice._options.onInitialized);

        cwic.SystemController.addEventHandler('onInitializationError', JabberVoice._options.onCwicError);

        cwic.SystemController.addEventHandler('onUserAuthorized', JabberVoice._options.onUserAuthorized);
        cwic.SystemController.addEventHandler('onAuthorizationRejected', function (e) {
            console.log('onAuthorizationRejected/...');
        });

        cwic.SystemController.addEventHandler('onAddonConnectionLost', function (e) {

            var errorInfo = {
                errorType: "Add-on",
                errorData: {
                    reason: "Connection with add-on has been lost"
                }
            };

            JabberVoice._options.onCwicError(errorInfo);

            console.error("Connection with add-on has been lost");

        });

    }

    function onUserAuthorized(e, b) {

        cLog('onUserAuthorized...');

        initializeLoginHandlers();

        initializeTelephonyDeviceHandlers();

        initializeTelephonyConversationHandlers();

        initializeMediaDeviceEventHandlers();

        initializeMediaDeviceUI();

        initializeRingtoneEventHandlers();

        initializeCallPickupEventHandlers();

        setTimeout(function () {
            if (!JabberVoice._connected) {
                cwic.TelephonyController.refreshTelephonyDeviceList();
                cwic.MultimediaController.refreshMediaDeviceList();
                cwic.MultimediaController.refreshRingtoneList();
            }
        }, 5000);

    }

    function initializeLoginHandlers() {

        cwic.LoginController.addEventHandler('onAuthenticationStateChanged', onAuthenticationStateChanged);
        cwic.LoginController.addEventHandler('onAuthenticationError', function (e) {
            console.log(e);
        });

        cwic.LoginController.addEventHandler('onSignedIn', JabberVoice._options.onSignedIn);

        cwic.LoginController.addEventHandler('onSigningIn', onSigningIn);

        cwic.LoginController.addEventHandler("onEmailRequired", function () {
            console.error('Email required!');
        });

        cwic.LoginController.addEventHandler("onSSONavigationRequired", function () {
            console.error('SSONavigation required!');
        });

        cwic.LoginController.addEventHandler("onCredentialsRequired", JabberVoice._options.onCredentialsRequired);
        cwic.LoginController.addEventHandler("onAuthenticationFailed", function (content) {

            var errorInfo;

            if (content == "InvalidCredentials") {

                errorInfo = {
                    errorType: "AuthenticationFailed",
                    errorData: {
                        reason: "InvalidCredentials"
                    }
                };

                JabberVoice._options.onCwicError(errorInfo);

                console.error('InvalidCredentials');

            }
            else if (content == "CouldNotConnect") {

                errorInfo = {
                    errorType: "AuthenticationFailed",
                    errorData: {
                        reason: "CouldNotConnect"
                    }
                };

                JabberVoice._options.onCwicError(errorInfo);
            }
            else {

                console.error('OTHER onAuthenticationFailed ');
                console.error(content);

            }
        });

        // cwic.MessageReceiver.addMessageHandler('conversationCallStateChanged', onConversationCallStateChanged.bind(this));
        // cwic.MessageReceiver.addMessageHandler('conversationStateChanged', onConversationStateChanged.bind(this));

    }

    function initializeTelephonyDeviceHandlers() {

        cwic.TelephonyController.addEventHandler('onTelephonyDeviceListChanged', onTelephonyDeviceListChanged);
        cwic.TelephonyController.addEventHandler('onConnectionStateChanged', JabberVoice._options.onConnectionStateChanged);
        cwic.TelephonyController.addEventHandler('onConnectionFailure', JabberVoice._options.onConnectionFailure);
        // telephonyservicechange

    }

    function initializeTelephonyConversationHandlers() {

        /** OffHook */
        cwic.TelephonyController.addEventHandler('onConversationOutgoing', onConversationStartedOutComing);
        /** Ringin */
        cwic.TelephonyController.addEventHandler('onConversationIncoming', onConversationStartedInComing);
        /** OnHook */
        cwic.TelephonyController.addEventHandler('onConversationEnded', onConversationEnded);
        /** Connected */
        cwic.TelephonyController.addEventHandler('onConversationStarted', onConversationStarted);
        /** Other */
        cwic.TelephonyController.addEventHandler('onConversationUpdated', onConversationUpdate);
    }


    function initializeCallPickupEventHandlers() {
        // cwic.TelephonyController.addEventHandler('onVisualCallPickupNotification', onVisualCallPickupNotification);
        cwic.TelephonyController.addEventHandler('onAudioCallPickupNotification', JabberVoice._options.onAudioCallPickupNotification)
    }

    function initializeMediaDeviceEventHandlers() {
        cwic.MultimediaController.addEventHandler("onMediaDeviceListChanged", JabberVoice._options.onMediaDeviceListChanged);

    }

    function initializeRingtoneEventHandlers() {
        cwic.MultimediaController.addEventHandler("onRingtoneListChanged", onRingtoneListChanged);
    }

    function initializeInvalidCertificateEventHandlers() {
        cwic.CertificateController.addEventHandler('onInvalidCertificate', onInvalidCertificate);
    }

    /**
     * ---------------------------
     * DEFINING DEFAULT METHODS
     * --------------------------
     */

    function onInitialized() {
        cLog('onInitialized...');

        signIn();
    }

    function onCwicError(errorInfo) {
        console.error('onCwicError...');
        log(errorInfo);
    }

    function signIn() {

        JabberVoice._serverList.push(JabberVoice._options.cucm);
        JabberVoice._serverList.push(JabberVoice._options.cucm2);

        cwic.LoginController.setCTIServers(JabberVoice._serverList, onServersError);
        cwic.LoginController.setTFTPServers(JabberVoice._serverList, onServersError);
        cwic.LoginController.setCUCMServers(JabberVoice._serverList, onServersError);

        cwic.LoginController.signIn(function (err) {
            console.error('Error on SignIn');
            console.error(err);
        });

    }

    function onServersError(err) {
        console.error('onServersError');
        console.error(err);
    }

    function onAuthenticationStateChanged(authenticationState) {

        cLog(authenticationState);

        switch (authenticationState) {
            case "NotAuthenticated":
                // Handle authentication state here...
                break;
            case "InProgress":
                // Handle authentication state here...
                break;
            case "Authenticated":
                // Handle authentication state here...
                break;
        }
    }

    function signInWithCredentials() {

        if (JabberVoice._options.limitTryLogin > JabberVoice._numberOfTryLogin) {

            cLog('onCredentialsRequired...');

            cwic.LoginController.setCredentials(JabberVoice._username, JabberVoice._password, function (e) {
                console.error('Error in setCredentials');
                console.error(e);
            });

            JabberVoice._numberOfTryLogin++;
        }

        // cwic.LoginController.signIn();

    }

    function onSigningIn() {
        cLog('onSigningIn...');
    }

    function onSignedIn() {
        cLog('onSignedIn....');
    }

    function onTelephonyDeviceListChanged() {
        cLog('onTelephonyDeviceListChanged...');

        JabberVoice._telephonyDevices = cwic.TelephonyController.telephonyDevices;

        var $DeviceList = $('#devices');
        $DeviceList.empty();

        var index;

        for (index = 0; index < JabberVoice._telephonyDevices.length; ++index) {
            var device = JabberVoice._telephonyDevices[index];
            var $Device = $('<option>');

            $Device.val(index);
            $Device.text(device.name);
            $Device.attr('title', device.description);

            $DeviceList.append($Device);

            if (device.isSelected) {
                $DeviceList.val(index);

                var $ControlModeInfo = $('#controlMode');
                var $DeviceNameInfo = $('#deviceName');
                var $ActiveLineInfo = $('#activeLine');
                var $HuntGroupStateInfo = $('#huntGroupState');

                $ControlModeInfo.text(device.controlMode);
                $DeviceNameInfo.text(device.name);
                $ActiveLineInfo.text(device.activeLine);
                $HuntGroupStateInfo.text(device.huntGroupState);
            }
        }

        if (JabberVoice._telephonyDevices.length > 0) {
            connectToTelephonyDevice();
        }
    }

    function onConnectionStateChanged(state) {

        cLog('onConnectionStateChanged... [' + state + ']');

        if (state === "Connected") {
            JabberVoice._connected = true;
            JabberVoice._connecting = false;
            JabberVoice._options.jabberIsConnected({
                authLogin: true
            });
        }
        else if(state === "Connecting") {
            JabberVoice._connected = false;
            JabberVoice._connecting = true;
        }

        // to remove

        // var $ConnectionStateInfo = $('#connectionState');
        // $ConnectionStateInfo.text(state);
        //
        // if(state === "Connected")
        // {
        //     $('#startAudioConversationButton').attr('disabled', false);
        //     $('#startVideoConversationButton').attr('disabled', false);
        //     $('#selectedDeviceInfo').show();
        //     cwic.TelephonyController.refreshTelephonyDeviceList();
        // }
    }

    function manualConnected() {
        JabberVoice._connecting = false;
        JabberVoice._connected = true;
        JabberVoice._options.jabberIsConnected({
            authLogin: false
        });
    }

    function onConnectionFailure(e) {
        // log('onConnectionFailure...');
        console.error(e);

        JabberVoice._connecting = false;
    }

    function connectToTelephonyDevice() {

        if (!JabberVoice._connected && !JabberVoice._connecting) {

            JabberVoice._connecting = true;

            cLog('connectToTelephonyDevice...');

            var $SelectedTelephonyDevice = $('#devices option:selected');
            var selectedDeviceIndex = $SelectedTelephonyDevice.val();

            var telephonyDevice = JabberVoice._telephonyDevices[selectedDeviceIndex];

            if (telephonyDevice.isSelected) {
                manualConnected();
            }
            else {
                var isForceRegistration = true;

                if (telephonyDevice.controlMode === "ExtendConnect") {
                    var number = $("#remotePhoneNumberTextField").val();
                    telephonyDevice.connect(number, isForceRegistration);
                }
                else {
                    telephonyDevice.connect(isForceRegistration);
                }
            }

        }

    }

    function onAudioCallPickupNotification(isAudioEnabled) {
        log('onAudioCallPickupNotification...');
    }

    function onMediaDeviceListChanged() {

        cLog('onMediaDeviceListChanged...');

        // try {
        var cameraList = cwic.MultimediaController.cameraList;
        var speakerList = cwic.MultimediaController.speakerList;
        var microphoneList = cwic.MultimediaController.microphoneList;
        var ringerList = cwic.MultimediaController.ringerList;

        JabberVoice._media.Speakers = speakerList;
        JabberVoice._media.Microphones = microphoneList;
        JabberVoice._media.Ringers = ringerList;
        JabberVoice._media.Cameras = cameraList;

        var $MicrophoneList = $('#microphoneList');
        var $CameraList = $('#cameraList');
        var $SpeakerList = $('#speakerList');
        var $RingerList = $('#ringerList');

        var $RingerVolume = $('#ringervolumecontrol');
        var $SpeakerVolume = $('#speakervolumecontrol');
        var $MicrophoneVolume = $('#microphonevolumecontrol');

        $MicrophoneList.empty();
        $CameraList.empty();
        $SpeakerList.empty();
        $RingerList.empty();

        var index;

        for (index = 0; index < speakerList.length; ++index) {
            var speaker = speakerList[index];

            var $SpeakerListItem = $("<option></option>");
            $SpeakerListItem.val(index);
            $SpeakerListItem.text(speaker.name);
            $SpeakerList.append($SpeakerListItem);

            if (speaker.isSelected) {
                $SpeakerList.val(index);
                $SpeakerVolume.val(speaker.volume);
                JabberVoice._options.setVolumeOfMedia('speaker', speaker.volume);
            }
        }

        for (index = 0; index < cameraList.length; ++index) {
            var camera = cameraList[index];

            var $CameraListItem = $("<option></option>");
            $CameraListItem.val(index);
            $CameraListItem.text(camera.name);
            $CameraList.append($CameraListItem);

            if (camera.isSelected) {
                $CameraList.val(index);
            }
        }

        for (index = 0; index < microphoneList.length; ++index) {
            var microphone = microphoneList[index];

            var $MicrophoneListItem = $("<option></option>");
            $MicrophoneListItem.val(index);
            $MicrophoneListItem.text(microphone.name);
            $MicrophoneList.append($MicrophoneListItem);

            if (microphone.isSelected) {
                $MicrophoneList.val(index);
                $MicrophoneVolume.val(microphone.volume);
                JabberVoice._options.setVolumeOfMedia('microphone', microphone.volume);
            }
        }

        for (index = 0; index < ringerList.length; ++index) {
            var ringer = ringerList[index];

            var $RingerListItem = $("<option></option>");
            $RingerListItem.val(index);
            $RingerListItem.text(ringer.name);
            $RingerList.append($RingerListItem);

            if (ringer.isSelected) {
                $RingerList.val(index);
                $RingerVolume.val(ringer.volume);
                JabberVoice._options.setVolumeOfMedia('ringer', ringer.volume);
            }
        }

        if (ringerList.length > 0 && microphoneList.length && speakerList.length) {

            delayedCallback1(function () {

                if (!mediaInited) {

                    mediaInited = true;

                    var defaultDevices = Cookies.getJSON('atlas_default_devices');

                    if (defaultDevices) {
                        selectMediaFromCookie(defaultDevices);
                    }

                }

            }, 1000);

        }

        JabberVoice._options.mediaListChanged();
        // }
        // catch (error) {
        //     console.log(error);
        // }
    }

    function selectMediaFromCookie(defaultDevices) {

        var mediaNameList = ['microphone', 'speaker', 'ringer'];

        $.each(mediaNameList, function (i, ele) {
            var _device = defaultDevices[ele];
            if (_device) {

                var $mediaDeviceSelection = $('#mediaDeviceSelectionWindow');

                $.each($mediaDeviceSelection.find('select[data-media-name="' + ele + '"] option'), function (i2, ele2) {
                    if (ele2.text == _device) {
                        $mediaDeviceSelection.find('select[data-media-name="' + ele + '"]').val(ele2.value).trigger('change');
                    }
                });
            }
        });

    }

    function onInvalidCertificate(certificate, reasons, canUserAccept) {
        var $invalidcertcontainer = $('#invalidcertcontainer'),
            $responsebuttons = $('#responsebuttons'),
            $certdetailstable = $('#certdetailstable');


        $invalidcertcontainer.show();

        $certdetailstable.find('.identifier').text(certificate.identifier);
        $certdetailstable.find('.subjectCN').text(certificate.subjectCN);
        $certdetailstable.find('.reference').text(certificate.referenceID);
        $certdetailstable.find('.reason').text(reasons.join(', '));

        if (canUserAccept) {
            $responsebuttons.find('.accept').off().on('click', function () {
                cwic.CertificateController.acceptInvalidCertificate(certificate);
                $invalidcertcontainer.hide();
            });

            $responsebuttons.find('.reject').off().on('click', function () {
                cwic.CertificateController.rejectInvalidCertificate(certificate);
                $invalidcertcontainer.hide();
            });

            $responsebuttons.show();
        }
        else {
            $responsebuttons.hide();
        }
    }

    function onRingtoneListChanged() {
        cLog('onRingtoneListChanged...');

        var ringtones = cwic.MultimediaController.ringtoneList;

        var $RingtoneList = $('#ringtonesSelect');
        $RingtoneList.empty();
        JabberVoice._ringtoneList.length = 0;

        for (var index = 0; index < ringtones.length; index++) {
            var ringtone = ringtones[index];
            JabberVoice._ringtoneList.push(ringtone);

            var $RingtoneListItem = $("<option></option>");
            $RingtoneListItem.val(index);
            $RingtoneListItem.text(ringtone.name);
            $RingtoneList.append($RingtoneListItem);
        }


        if (ringtones.length > 0) {

            delayedCallback2(function () {

                if (!ringtoneInited) {

                    ringtoneInited = true;

                    var defaultDevices = Cookies.getJSON('atlas_default_devices');

                    if (defaultDevices) {

                        var defaultRingtones = defaultDevices['ringtones'];

                        if (defaultRingtones) {

                            var $mediaDeviceSelection = $('#mediaDeviceSelectionWindow');

                            $.each($mediaDeviceSelection.find('select[data-media-name="ringtones"] option'), function (i2, ele2) {
                                if (ele2.text == defaultRingtones) {
                                    $mediaDeviceSelection.find('select[data-media-name="ringtones"]').val(ele2.value).trigger('change');
                                }
                            });


                        }
                    }

                }


            }, 1000);

        }

        JabberVoice._options.mediaListChanged();

    }

    function refreshMediaDeviceList() {
        cwic.MultimediaController.refreshMediaDeviceList();
    }

    function initializeMediaDeviceUI() {
        var $RefreshDeviceListButton = $('#refreshMediaDeviceListButton');
        $RefreshDeviceListButton.click(refreshMediaDeviceList);

        var $CameraList = $('#cameraList');
        var $MicrophoneList = $('#microphoneList');
        var $SpeakerList = $('#speakerList');
        var $RingerList = $('#ringerList');
        var $RingtonesSelect = $('#ringtonesSelect');

        $CameraList.change(selectCamera);
        $MicrophoneList.change(selectMicrophone);
        $SpeakerList.change(selectSpeaker);
        $RingerList.change(selectRinger);
        $RingtonesSelect.change(selectRingtone);

        // var $SpeakerVolumeButton    = $('#speakervolumebtn');
        // var $RingerVolumeButton     = $('#ringervolumebtn');
        // var $MicrophoneVolumeButton = $('#micvolumebtn');
        //
        // $SpeakerVolumeButton.click(speakerVolumeButtonPressed);
        // $RingerVolumeButton.click(ringerVolumeButtonPressed);
        // $MicrophoneVolumeButton.click(microphoneVolumeButtonPressed);
    }

    function selectCamera() {
        var $SelectedCameraItem = $('#cameraList option:selected');
        var selectedCameraIndex = $SelectedCameraItem.val();

        var camera = JabberVoice._media.Cameras[selectedCameraIndex];
        cwic.MultimediaController.selectCamera(camera);
    }

    function selectMicrophone() {
        var $SelectedMicrophoneItem = $('#microphoneList option:selected');
        var selectedMicrophoneIndex = $SelectedMicrophoneItem.val();

        var microphone = JabberVoice._media.Microphones[selectedMicrophoneIndex];
        cwic.MultimediaController.selectMicrophone(microphone, function (e) {
            // console.log(e);
        });

        var $MicrophoneVolume = $('#microphonevolumecontrol');
        var volume = microphone.volume;
        $MicrophoneVolume.val(volume);
    }

    function selectSpeaker() {
        var $SelectedSpeakerItem = $('#speakerList option:selected');
        var selectedSpeakerIndex = $SelectedSpeakerItem.val();

        var speaker = JabberVoice._media.Speakers[selectedSpeakerIndex];

        cwic.MultimediaController.selectSpeaker(speaker);

        var $SpeakerVolume = $('#speakervolumecontrol');
        var volume = speaker.volume;
        $SpeakerVolume.val(volume);

        JabberVoice._options.setVolumeOfMedia('speaker', speaker.volume);

        var AudioTestControl = document.getElementById('audio-test-control');

        if(AudioTestControl) {
            requestMedia(speaker.name, 'audiooutput', function(device) {
                AudioTestControl.setSinkId(device.deviceId);
                AudioTestControl.volume = (speaker.volume / 100);
            });
        }

    }

    function selectRinger() {
        var $SelectedRingerItem = $('#ringerList option:selected');
        var selectedRingerIndex = $SelectedRingerItem.val();

        var ringer = JabberVoice._media.Ringers[selectedRingerIndex];
        cwic.MultimediaController.selectRinger(ringer);

        var $RingerVolume = $('#ringervolumecontrol');
        var volume = ringer.volume;
        $RingerVolume.val(volume);

        var AudioTestControl = document.getElementById('audio-test-control-2');

        if(AudioTestControl) {
            requestMedia(ringer.name, 'audiooutput', function(device) {
                AudioTestControl.setSinkId(device.deviceId);
                AudioTestControl.volume = (ringer.volume / 100);
            });
        }
    }

    function selectRingtone() {
        var $RingtoneList = $('#ringtonesSelect');
        var $SelectedRingtoneItem = $('#ringtonesSelect option:selected');
        var selectedRingtoneIndex = $SelectedRingtoneItem.val();

        var ringtone = JabberVoice._ringtoneList[selectedRingtoneIndex];
        $RingtoneList.val(selectedRingtoneIndex);
        cwic.MultimediaController.selectRingtone(ringtone);
    }

    /**
     * - - - - - - - - - - - - - - - -
     *          Conversation
     * - - - - - - - - - - - - - - - -
     */

    function onConversationStartedInComing(telephonyConversation) {

        JabberVoice._options.conversationInComing(telephonyConversation);

        var ID = telephonyConversation.ID;

        if (!JabberVoice._telephonyConversations[ID]) {
            addConversationToList(telephonyConversation);
        }

        JabberVoice._telephonyConversations[ID] = telephonyConversation;

        // onConversationStarted(telephonyConversation);
    }

    function onConversationStarted(telephonyConversation) {

        JabberVoice._options.conversationStarted(telephonyConversation);

        updateConversationInfo();
    }

    function onConversationUpdate(telephonyConversation) {
        cLog('Połączenie UPDATE');

        JabberVoice._options.conversationUpdate(telephonyConversation);

        var ID = telephonyConversation.ID;

        if (JabberVoice._telephonyConversations[ID]) {
            JabberVoice._telephonyConversations[ID] = telephonyConversation;
            updateConversationInfo();
        }
    }

    function onConversationEnded(telephonyConversation) {
        var ID = telephonyConversation.ID;

        if (JabberVoice._telephonyConversations[ID]) {
            delete JabberVoice._telephonyConversations[ID];
            removeConversationFromList(telephonyConversation);
        }

        JabberVoice._options.endConversation(telephonyConversation);
    }


    function onConversationStartedOutComing(telephonyConversation) {

        JabberVoice._options.conversationOutComing(telephonyConversation);

        var ID = telephonyConversation.ID;

        if (!JabberVoice._telephonyConversations[ID]) {
            addConversationToList(telephonyConversation);
        }

        JabberVoice._telephonyConversations[ID] = telephonyConversation;

        // onConversationStarted(telephonyConversation);
    }

    /**
     * PUBLIC METHODS
     */

    JabberVoice.prototype.muteAudio = function () {
        muteAudio();
    };

    JabberVoice.prototype.pickUp = function () {
        pickUpAudio();
    };

    JabberVoice.prototype.endConversation = function () {
        endCurrentCall();
    };


    JabberVoice.prototype.transferConversation = function (number) {
        transferCurrentConversation(number);
    };

    JabberVoice.prototype.startCall = function (number, errorHandler) {
        startAudioConversation(number, errorHandler);
    };

    JabberVoice.prototype.completeTransfer = function () {
        completeTransfer();
    };

    JabberVoice.prototype.cancelTransfer = function () {
        endCurrentCall();
    };

    JabberVoice.prototype.changeVolumeOfMedia = function (mediaName, volume) {

        var AudioTestControl = null;

        if (mediaName === 'speaker') {

            var speakerOption = JabberVoice._media.Speakers[$('#speakerList').val()];

            if(speakerOption) {
                speakerOption.setVolume(volume);
            }

            AudioTestControl = document.getElementById('audio-test-control');

            if(AudioTestControl) {
                AudioTestControl.volume = (volume / 100);
            }

        }
        else if (mediaName === 'ringer') {

            var ringerOption = JabberVoice._media.Ringers[$('#ringerList').val()];

            if(ringerOption) {
                ringerOption.setVolume(volume);
            }

            AudioTestControl = document.getElementById('audio-test-control-2');

            if(AudioTestControl) {
                AudioTestControl.volume = (volume / 100);
            }

        }
        else if (mediaName === 'microphone') {

            var microphoneOption = JabberVoice._media.Microphones[$('#microphoneList').val()];

            if(microphoneOption) {
                microphoneOption.setVolume(volume);
            }

        }

    };

    JabberVoice.prototype.pressedDTMFDigit = function (e) {
        onDTMFDigitEntered(e)
    };

    /**
     * END PUBLIC METHODS
     */

    function muteAudio() {
        var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];

        if (JabberVoice._isMuted) {
            conversation.unmuteAudio();
            JabberVoice._isMuted = false;
        }
        else {
            conversation.muteAudio();
            JabberVoice._isMuted = true;
        }
    }

    function pickUpAudio() {
        var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
        if (conversation) {
            conversation.answerAudio();
        }
    }

    function endCurrentCall() {
        var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];

        if (conversation) {
            conversation.end();
        }
    }

    function transferCurrentConversation(number) {
        var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
        conversation.transferConversation(number);

    }


    function startAudioConversation(number, errorHandler) {
        cwic.TelephonyController.startAudioConversation(number, function (e) {

            console.error(e);
            console.log(e);

            try{
                console.log(cwic.TelephonyController.connectionState);
            }
            catch (e) {

            }

            if(typeof errorHandler === "function"){
                errorHandler(e);
            }

        });
    }

    function completeTransfer() {
        var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
        conversation.completeTransfer();
    }

    function cancelTransfer() {
        // var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
        // conversation.cancelTransfer();
    }

    function onDTMFDigitEntered(input) {
        /** Jeżeli event to wyciągam key */
        if (typeof input === "object") {
            var charCode = input.which;
            var dtmf = String.fromCharCode(charCode);
        }
        else {
            dtmf = input;
        }

        var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
        conversation.sendDTMF(dtmf);
    }

    /**
     * OTHERS
     */

    function log(text) {
        if (JabberVoice._options.debugMode) {
            console.log(text);
        }
    }

    function cLog(msg) {
        if (JabberVoice._options.debugMode) {
            if (typeof msg === "object") {
                console.log(msg);
            }
            else {
                console.log('%c' + msg, 'background:#91cef3;color:#000;padding:5px');
            }
        }
    }

    // functionconsole.error(msg) {
    //     if(JabberVoice._options.debugMode) {
    //         console.log('%c' + msg,'background:red;color:#fff;padding:5px');
    //     }
    // }

    /**
     * Domyśle parametry Jabbera
     */

    function getDefaultOptions() {
        return {
            loggingLevel: 1,
            cucm: '10.10.77.100',
            cucm2: '10.10.77.106',
            serverList: [],

            debugMode: true,

            limitTryLogin: 3,

            onInitialized: onInitialized,
            onUserAuthorized: onUserAuthorized,
            onCwicError: onCwicError,
            onSignedIn: onSignedIn,
            onCredentialsRequired: signInWithCredentials,

            onTelephonyDeviceListChanged: onTelephonyDeviceListChanged,
            onConnectionStateChanged: onConnectionStateChanged,
            onConnectionFailure: onConnectionFailure,
            onMediaDeviceListChanged: onMediaDeviceListChanged,
            onRingtoneListChanged: onRingtoneListChanged,
            onAudioCallPickupNotification: onAudioCallPickupNotification,


            /** METHODS FOR jabber-script */
            conversationInComing: function (telephonyConversation) {
            },
            conversationOutComing: function (telephonyConversation) {
            },
            endConversation: function (telephonyConversation) {
            },
            conversationStarted: function (telephonyConversation) {
            },
            conversationUpdate: function (telephonyConversation) {
            },
            mediaListChanged: function () {
            },
            jabberIsConnected: function () {
            },
            setVolumeOfMedia: function () {
            },
            setRingtone: function (nameRingtone) {

            }
        }
    }


    JabberVoice.prototype.Init = function () {

        initPrivateVariable.call(this);

        setConfigOnCwic.call(this);
        addEventHandlerOnCwic.call(this);

        cwic.SystemController.initialize();
    };

    return JabberVoice;
})();

function requestMedia(label, inputKind, callback) {

    navigator.mediaDevices.enumerateDevices().then(function(deviceInfos) {

        var device = null;

        for (var i = 0; i !== deviceInfos.length; ++i) {

            var deviceInfo = deviceInfos[i];

            if (deviceInfo.kind === inputKind) {
                if(label.indexOf(deviceInfo.label) !== -1 || deviceInfo.label.indexOf(label) !== -1) {
                    device = deviceInfo;
                    break;
                }
            }
        }

        callback(device);

    });

}

function onVisualCallPickupNotification(notificationInfo) {
    var $CallPickupNotificationWindow = $('#callPickupNotification');
    var $NotificationInfoBody = $('#callPickupNotificationInfo');

    $NotificationInfoBody.text(notificationInfo);
    $CallPickupNotificationWindow.show();
}

function onIgnoreButtonPressed() {
    var $CallPickupNotificationWindow = $('#callPickupNotification');
    $CallPickupNotificationWindow.hide();
}

function onPickupButtonPressed() {
    var errorHandler = function () {
        console.log("error");
    };

    cwic.TelephonyController.callPickup(errorHandler);
    var $CallPickupNotificationWindow = $('#callPickupNotification');
    $CallPickupNotificationWindow.hide();
}

function onCallPickupButtonPressed() {
    cwic.TelephonyController.callPickup();
}

function onGroupCallPickuButtonPressed() {
    var $GroupCallPickupNumberFiled = $('#groupCallPickupNumber');
    var number = $GroupCallPickupNumberFiled.val();
    cwic.TelephonyController.groupCallPickup(number);
}

function onOtherGroupPickupButtonPressed() {
    cwic.TelephonyController.otherGroupPickup();
}


function onConversationOutgoing(telephonyConversation) {
    var ID = telephonyConversation.ID;

    if (!JabberVoice._telephonyConversations[ID]) {
        addConversationToList(telephonyConversation);
    }
    else {

    }

    JabberVoice._telephonyConversations[ID] = telephonyConversation;
}

function onConversationIncoming(telephonyConversation) {
    var ID = telephonyConversation.ID;

    if (!JabberVoice._telephonyConversations[ID]) {
        addConversationToList(telephonyConversation);
    }
    else {

    }

    JabberVoice._telephonyConversations[ID] = telephonyConversation;
    updateConversationInfo();
}

function onConversationSelected(e) {
    if ((e.target.id !== "calllist")) {
        var el = $(e.target);
        while (el.length && el[0].id.indexOf("conversation")) {
            el = el.parent();
        }
        if (el.length) {
            var $CurrentConversation = $('#conversation' + JabberVoice._selectedConversationID);
            $CurrentConversation.removeClass('selected');

            JabberVoice._selectedConversationID = el.find(".ConversationID")[0].id;

            var $SelectedConversation = $(el);
            $SelectedConversation.addClass('selected');

            updateConversationInfo();
        }
    }
}

function addConversationToList(conversation) {
    JabberVoice._selectedConversationID = conversation.ID;
}

function removeConversationFromList(conversation) {

    if (Object.keys(JabberVoice._telephonyConversations).length === 0) {

    }
    else {
        for (var property in JabberVoice._telephonyConversations) {
            if (JabberVoice._telephonyConversations.hasOwnProperty(property)) {
                var telephonyConversation = JabberVoice._telephonyConversations[property];
                JabberVoice._selectedConversationID = telephonyConversation.ID;

                updateConversationInfo();
            }
        }
    }
}

function iDivert() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
    conversation.reject();
};

function answerAudio() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
    conversation.answerAudio();
};

function answerVideo() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
    conversation.answerVideo();
};

function endCall() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
    conversation.end();
}

function hold() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];

    if ($("#holdResumeButton").text() === "Hold") {
        conversation.hold();
    }
    else {
        conversation.resume();
    }
}

function muteAudio() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];

    if ($('#muteAudioButton').hasClass('muted')) {
        conversation.unmuteAudio();
    }
    else {
        conversation.muteAudio();
    }
}

function mergeConversations() {
    try {
        var conversationID = $("#conferencelist").val();
        var conversationToMerge = JabberVoice._telephonyConversations[conversationID];
        var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
        conversation.merge(conversationToMerge);
    }
    catch (exception) {
        console.log(exception.message);
    }
}

function updateConversationInfo() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];

    var $EndButton = $('#endCallButton');
    var $AnswerAudioButton = $('#answerAudioButton');
    var $AnswerVideoButton = $('#answerVideoButton');
    var $IDivertButton = $('#iDevertButton');
    var holdResumeButton = $('#holdResumeButton');
    var muteAudioButton = $('#muteAudioButton');
    var muteVideoButton = $('#muteVideoButton');
    var sendVideoButton = $('#sendVideoButton');
    var $StartScreenShareButton = $('#screenShareButton');
    var $TransferButton = $('#transferbtn');
    var $StartConferenceButton = $('#conferencebtn');

    var $CameraTurnLeftButton = $("#cameraTurnLeft");
    var $CameraTurnRightButton = $("#cameraTurnRight");
    var $CameraTurnUpButton = $("#cameraTurnUp");
    var $CameraTurnDownButton = $("#cameraTurnDown");
    var $CameraZoomInButton = $("#cameraZoomIn");
    var $CameraZoomOutButton = $("#cameraZoomOut");

    $AnswerAudioButton.attr('disabled', !conversation.capabilities.canAnswer);
    $AnswerVideoButton.attr('disabled', !conversation.capabilities.canAnswer);
    $EndButton.attr('disabled', !conversation.capabilities.canEnd);
    $IDivertButton.attr('disabled', !conversation.capabilities.canReject);

    // Update Hold Call Controls.
    if (conversation.callState === "Hold") {
        holdResumeButton.attr('disabled', !conversation.capabilities.canResume);
        holdResumeButton.text('Resume');
    }
    else if (conversation.callState === "Connected") {
        holdResumeButton.attr('disabled', !conversation.capabilities.canHold);
        holdResumeButton.text('Hold');
    }

    // Update Mute Audio Controls.
    if (conversation.states.isAudioMuted) {
        muteAudioButton.attr('disabled', !conversation.capabilities.canUnmuteAudio);
        muteAudioButton.text('Unmute Audio').addClass('muted');
    }
    else {
        muteAudioButton.attr('disabled', !conversation.capabilities.canMuteAudio);
        muteAudioButton.text('Mute Audio').removeClass('muted');
    }

    // Update Mute Video Controls.
    if (conversation.states.isVideoMuted) {
        muteVideoButton.attr('disabled', !conversation.capabilities.canUnmuteVideo);
        muteVideoButton.text('Unmute Video').addClass('muted');
    }
    else {
        muteVideoButton.attr('disabled', !conversation.capabilities.canMuteVideo);
        muteVideoButton.text('Mute Video').removeClass('muted');
    }

    // Update Video Sending Controls
    if (conversation.capabilities.canStartVideo) {
        sendVideoButton.text('Start Video');
        sendVideoButton.attr('disabled', !conversation.capabilities.canUpdateVideo);
    }

    if (conversation.capabilities.canStopVideo) {
        sendVideoButton.text('Stop Video');
        sendVideoButton.attr('disabled', !conversation.capabilities.canUpdateVideo);
    }

    if (conversation.states.isLocalSharing) {
        $StartScreenShareButton.text('Stop Share');
        $StartScreenShareButton.attr('disabled', !conversation.capabilities.canStopScreenShare);
    }
    else {
        $StartScreenShareButton.text('Start Share');
        $StartScreenShareButton.attr('disabled', !conversation.capabilities.canStartScreenShare);
    }


    if (conversation.capabilities.canControlRemoteCamera) {
        $CameraTurnLeftButton.attr('disabled', !conversation.capabilities.canCameraPanLeft);
        $CameraTurnRightButton.attr('disabled', !conversation.capabilities.canCameraPanRight);
        $CameraTurnUpButton.attr('disabled', !conversation.capabilities.canCameraTiltUp);
        $CameraTurnDownButton.attr('disabled', !conversation.capabilities.canCameraTiltDown);
        $CameraZoomInButton.attr('disabled', !conversation.capabilities.canCameraZoomIn);
        $CameraZoomOutButton.attr('disabled', !conversation.capabilities.canCameraZoomOut);
    }
    else {
        $CameraTurnLeftButton.attr('disabled', true);
        $CameraTurnRightButton.attr('disabled', true);
        $CameraTurnUpButton.attr('disabled', true);
        $CameraTurnDownButton.attr('disabled', true);
        $CameraZoomInButton.attr('disabled', true);
        $CameraZoomOutButton.attr('disabled', true);
    }


    $TransferButton.attr('disabled', !conversation.capabilities.canTransfer);
    $StartConferenceButton.attr('disabled', !conversation.capabilities.canMerge);

}

function showConversationVideoWindow() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
    videoWindow.showForConversation(conversation);
}

function hideConversationVideoWindow() {
    var conversation = JabberVoice._telephonyConversations[JabberVoice._selectedConversationID];
    videoWindow.hideForConversation(conversation);
}
