<?php

namespace JabberBundle\Repository;

class ConversationRepository extends \Doctrine\ORM\EntityRepository implements ConversationRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function findConversationByIdDialog($idDialog)
    {
        $result = $this->findOneByIdDialog($idDialog);

        return $result;
    }
}