<?php

namespace JabberBundle\Repository;

interface CUCUMServerRepositoryInterface
{
    /**
     * Get available server list
     *
     * @return \JabberBundle\Entity\CUCUMServerInterface[]
     */
    public function getServerList();
}