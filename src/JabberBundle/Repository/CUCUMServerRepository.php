<?php

namespace JabberBundle\Repository;

class CUCUMServerRepository extends \Doctrine\ORM\EntityRepository implements CUCUMServerRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getServerList()
    {
        $qb = $this
            ->createQueryBuilder('s')
            ->orderBy('s.priority', 'DESC')
            ->getQuery();

        $result = $qb->getResult();

        return $result;
    }
}