<?php

namespace JabberBundle\Repository;

interface ConversationRepositoryInterface
{
    /**
     * @param int $idDialog API dialog identifier
     * @return null|\JabberBundle\Entity\ConversationInterface
     */
    public function findConversationByIdDialog($idDialog);
}