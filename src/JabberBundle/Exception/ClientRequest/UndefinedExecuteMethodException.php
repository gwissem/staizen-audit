<?php

namespace JabberBundle\Exception\ClientRequest;

use LogicException;

/**
 * Thrown when user set undefined method in execute process
 */
class UndefinedExecuteMethodException extends LogicException {}