<?php

namespace JabberBundle\Exception\ClientRequest;

use RuntimeException;

/**
 * Thrown when request execute an exception that
 * is different than already defined exceptions
 */
class NotSpecifiedException extends RuntimeException {}