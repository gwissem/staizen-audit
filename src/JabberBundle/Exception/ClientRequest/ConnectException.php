<?php

namespace JabberBundle\Exception\ClientRequest;

use RuntimeException;

/**
 * Thrown when request timeout
 */
class ConnectException extends RuntimeException {}