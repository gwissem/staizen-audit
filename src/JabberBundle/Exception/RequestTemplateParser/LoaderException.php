<?php

namespace JabberBundle\Exception\RequestTemplateParser;

use RuntimeException;

/**
 * Thrown when request template not found
 */
class LoaderException extends RuntimeException {}