<?php

namespace JabberBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use JabberBundle\Entity\CUCUMServer;

class LoadCUCUMServerList implements FixtureInterface
{
    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getMapping() as $data)
        {
            $server = new CUCUMServer();
            $server->setAddress($data['address']);
            $server->setPriority($data['priority']);

            $manager->persist($server);
            $manager->flush();
        }
    }

    /**
     * Get CUCUM server list
     *
     * @return array
     */
    private function getMapping()
    {
        $list = [];
        array_push($list,
            ['address' => '10.10.77.100', 'priority' => 2],
            ['address' => '10.10.77.106', 'priority' => 1]
        );

        return $list;
    }
}