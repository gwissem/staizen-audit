<?php

namespace JabberBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExceptionListener
{
    /**
     * Define exception classes that will return
     * JSON response with error message
     *
     * @var array
     */
    private $allowedExceptionClasses = [
        \JabberBundle\Exception\ClientRequest\ConnectException::class,
        \JabberBundle\Exception\ClientRequest\NotSpecifiedException::class
    ];

    /**
     * @inheritdoc
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (in_array(get_class($exception), $this->allowedExceptionClasses))
        {
            $response = new JsonResponse([
                'status' => false,
                'message' => 'PROCESS_EXCEPTION',
                'exception' => [
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode()
                ]
            ]);

            // Send response
            $event->setResponse($response);
        }
    }
}