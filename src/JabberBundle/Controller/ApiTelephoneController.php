<?php

namespace JabberBundle\Controller;

use DateTime;
use JabberBundle\Entity\ConversationTelephony;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Get;
use JabberBundle\Entity\Conversation;
use JabberBundle\Entity\ConversationMember;
use Symfony\Component\Validator\Constraints\Date;
use UserBundle\Entity\User;

class ApiTelephoneController extends ApiBaseController
{

    private $today;
    /** @var  Translator translator */
    private $translator;
    /** @var  \IntlDateFormatter $formatter */
    private $formatter;

    private function getUserId($returnEntity = false) {

        $user = $this->get('user.info')->getOriginalUser();

        if($user instanceof User) {
            return ($returnEntity) ? $user : $user->getId();
        }

        return null;
    }

    /**
     * Update STATUS Conversation
     *
     * @Put("/api/jabber_voice/conversation_telephone/{idConversation}/{status}", name="jabberVoiceUpdateConversation", options = { "expose" = true })
     * @param Request $request
     * @param $idConversation
     * @param $status
     * @return Response
     */
    public function putUpdateConversationAction(Request $request, $idConversation, $status)
    {

        /** $idConversation to tak naprawdę ConversationTelephony.uniqueRandom */

        $conversation = $this
            ->getDoctrine()
            ->getRepository('JabberBundle:ConversationTelephony')
            ->findOneBy(['uniqueRandom' => $idConversation]);

        if($conversation && $status) {

            $conversation = $this->updateConversation($conversation, $status);

            $view = $this->view([
                'status' => true,
                'conversation' => [
                    'id' => $conversation->getId()
                ]
            ]);

        }
        else {
            $view = $this->view([
                'status' => false
            ]);
        }

        return $this->handleView($view);
    }

    /**
     * Create new Conversation with ACTIVE status
     *
     * @Post("/api/jabber_voice/conversation_telephone", name="jabberVoiceAddConversation", options = { "expose" = true })
     * @param Request $request
     * @return Response
     */
    public function postAddConversationAction(Request $request)
    {

        $conversationData = $request->request->get('conversation');

        if($conversationData) {

            $conversation = $this->createConversation($conversationData);

            $view = $this->view([
                'status' => true,
                'conversation' => [
                    'id' => $conversation->getId(),
                    'participantName' => $conversation->getParticipantName(),
                    'participantNumber' => ($conversation->getType() == 'IN') ? $conversation->getFromAddress() : $conversation->getToAddress()
                ]
            ]);
        }
        else {
            $view = $this->view([
                'status' => false
            ]);
        }

        return $this->handleView($view);
    }

    /**
     * Update number of Conversation
     *
     * @Put("/api/jabber_voice/conversation_telephone_update_number/{uniqueId}/{number}", name="jabberVoiceUpdateNumberConversation", options = { "expose" = true })
     * @param Request $request
     * @param null $uniqueId
     * @param null $number
     * @return Response
     */
    public function putNumberConversationAction(Request $request, $uniqueId = null, $number = null)
    {

        $view = $this->view([
            'status' => false
        ]);

        if($uniqueId && $number) {

            $conversation = $this
                ->getDoctrine()
                ->getRepository('JabberBundle:ConversationTelephony')
                ->findOneBy(['uniqueRandom' => $uniqueId]);

            if($conversation) {

                $em = $this->getDoctrine()->getManager();

                $conversation->setFromAddress($number);

                $em->persist($conversation);
                $em->flush();

                $view = $this->view([
                    'status' => true,
                    'conversation' => [
                        'id' => $conversation->getId()
                    ]
                ]);

            }
        }

        return $this->handleView($view);
    }

    /**
     * Update idDialog and ProcessInstanceId of Conversation
     *
     * @Put("/api/jabber_voice/conversation_telephone_update_id_dialog/{uniqueId}", name="jabberVoiceUpdateIdDialogAndProcessId", options = { "expose" = true })
     * @param Request $request
     * @param null $uniqueId
     * @return Response
     */
    public function putUpdateIdDialogAndProcessIdAction(Request $request, $uniqueId = null)
    {

        $view = $this->view([
            'status' => false
        ]);

        $data = $request->request->all();

        if($uniqueId && !empty($data)) {

            $conversation = $this
                ->getDoctrine()
                ->getRepository('JabberBundle:ConversationTelephony')
                ->findOneBy(['uniqueRandom' => $uniqueId]);

            if($conversation) {

                $em = $this->getDoctrine()->getManager();

                $processInstance = $em->getRepository('CaseBundle:ProcessInstance')->find(intval($data['processInstanceId']));

                $conversationWithProcessInstance = $em->getRepository('JabberBundle:ConversationTelephony')->findOneBy([
                    'processInstance' => $processInstance
                ]);

                if(!$conversationWithProcessInstance) {
                    $conversation->setProcessInstance($processInstance);
                }

                $conversation->setIdDialog($data['idDialog']);

                $em->persist($conversation);
                $em->flush();

                $view = $this->view([
                    'status' => true,
                    'conversation' => [
                        'id' => $conversation->getId()
                    ]
                ]);

            }
        }

        return $this->handleView($view);
    }

    private function createConversation($conversationData) {

        $em = $this->getDoctrine()->getManager();

        $conversation = new ConversationTelephony();

        $conversation->setUniqueRandom($conversationData['unique_random']);

        $conversation->setType($conversationData['type']);

        $direction = ($conversation->getType() == "IN") ? 'from' : 'to';

        /** Check if Participant number exist, if not: search in fos_user */

        if(empty($conversationData[$direction] && $conversationData['uri']) ) {

            /** @var User $participant */
            $participant = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(['email' => $conversationData['uri']]);

            if($participant && $participant->getCucumExtension()) {
                $conversationData[$direction] = $participant->getCucumExtension();
            }
        }

        /** Check if Participant name exist, if not: search in fos_user */

        if(empty($conversationData['name'] && $conversationData[$direction]) ) {

            /** @var User $participant */
            $participant = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy([ 'cucumExtension' => $conversationData[$direction]]);

            if($participant) {
                $conversationData['name'] = $participant->getName();
            }
        }

        $conversation->setFromAddress($conversationData['from']);
        $conversation->setToAddress($conversationData['to']);
        $conversation->setParticipantName($conversationData['name']);

        if(!empty($conversationData['caseRooId'])) {
            $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($conversationData['caseRooId']));
            $conversation->setProcessInstance($processInstance);
        }

        if($id = $this->getUserId()) {
            $conversation->setAuthor($id);
        }

        $em->persist($conversation);
        $em->flush();

        return $conversation;

    }

    private function updateConversation(ConversationTelephony $conversation, $status) {

        $em = $this->getDoctrine()->getManager();

        $conversation->setStatus($status);

        if($status == ConversationTelephony::STATUS_PICK_UP) {
            $conversation->setConversationStartTime(new \DateTime());
        }

        if($status == ConversationTelephony::STATUS_FINISHED) {
            $conversation->setConversationFinishTime(new \DateTime());
        }

        if($status == ConversationTelephony::STATUS_CANCELED) {

        }

        if($status == ConversationTelephony::STATUS_NOT_PICK_UP) {

        }

        $conversation->setUpdatedAt(new \DateTime());

        $em->persist($conversation);
        $em->flush();

        return $conversation;

    }

    /**
     * Get history of users
     *
     * @Post("/api/jabber_voice/conversation_telephone/prepare_date_time", name="jabberVoicePrepareDateTime", options = { "expose" = true })
     * @param Request $request
     * @return Response
     */

    public function postPrepareDateTime(Request $request)
    {
        $date = new DateTime();
        $date->setTimestamp($request->request->get('date') / 1000);

        if($date) {

            $this->today = new DateTime();
            $this->today->setTime(0,0,0);
            $this->translator = $this->get('translator');
            $this->formatter = new \IntlDateFormatter($request->getLocale(), \IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT);
            $this->formatter->setPattern('d/M,E');

            $view = $this->view([
                'status' => true,
                'dateTime' => [
                    'time' => $date->format('H:i'),
                    'date' => $this->prepareDayName($date),
                ]
            ]);
        }
        else {
            $view = $this->view([
                'status' => false
            ]);
        }

        return $this->handleView($view);
    }

    /**
     * Get history of users
     *
     * @Get("/api/jabber_voice/conversation_telephone/history", name="jabberVoiceGetHistoryConversation", options = { "expose" = true })
     * @param Request $request
     * @return Response
     */

    public function getHistoryConversations(Request $request)
    {

        $historyConversation = $this->getDoctrine()->getRepository('JabberBundle:ConversationTelephony')->findBy(
            ['author' => $this->getUserId()],
            ['createdAt' => 'DESC'],
            10
        );

//        $historyConversation = $this->getDoctrine()
//            ->getRepository('JabberBundle:ConversationTelephony')
//            ->getLastConversationsByUser($this->getUserId(), 10);

//            ->findBy(
//            ['author' => $this->getUserId()],
//            ['createdAt' => 'DESC'],
//            10
//        );

        if($historyConversation) {

            $historyConversation = $this->prepareHistory($request, $historyConversation);

            $view = $this->view([
                'status' => true,
                'historyConversation' => $historyConversation
            ]);
        }
        else {
            $view = $this->view([
                'status' => false
            ]);
        }

        return $this->handleView($view);
    }

    private function prepareHistory(Request $request, $historyConversation) {

        $historyList = [];
        $this->today = new DateTime();
        $this->today->setTime(0,0,0);
        $this->translator = $this->get('translator');
        $this->formatter = new \IntlDateFormatter($request->getLocale(), \IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT);
        $this->formatter->setPattern('d/M,E');


        /** @var ConversationTelephony[] $historyConversation */
        foreach ($historyConversation as $item) {
            $dateTime = ($item->getConversationStartTime()) ? $item->getConversationStartTime() : $item->getCreatedAt();

            $historyList[] = [
                'type' => $item->getType(),
                'time' => $dateTime->format('H:i'),
                'date' => $this->prepareDayName($dateTime),
                'number' => ($item->getType() === ConversationTelephony::TYPE_IN) ? $item->getFromAddress() : $item->getToAddress(),
                'name' => $item->getParticipantName(),
                'status' => $item->getStatus(),
                'randomUnique' => $item->getUniqueRandom()
            ];
        }

        return $historyList;
    }

    private function prepareDayName(DateTime $date) {

        $copyDate = clone $date;
        $copyDate->setTime(0,0,0);

        $returnDateName = $this->formatter->format($copyDate);

        $diff = $copyDate->diff($this->today);

        if($diff->days === 0) {
            $returnDateName = $this->translator->trans('history_call_today');
        }
        else if($diff->days === 1) {
            $returnDateName = $this->translator->trans('history_call_yesterday');
        }

        return $returnDateName;
    }

    /**
     * Search contact of user by phrase
     *
     * @Get("/api/jabber_voice/contact-list", name="jabberVoiceGetContacts", options = { "expose" = true })
     * @param Request $request
     * @return Response
     */

    public function getTelephonyContacts(Request $request)
    {

        $phrase = $request->query->get('query', null);
        $list = [];

        $contactList = $this->getDoctrine()->getRepository('UserBundle:User')->getContactList($phrase);

        if(count($contactList) > 0) {
            $list = array_map(function ($a){
                if($a instanceof User) {
                    return [
                        'username' => $a->getName(),
                        'phoneNumber' => $a->getCucumExtension()
                    ];
                }
                return [];
            }, $contactList);
        }

        $view = $this->view([
            'list' => $list
        ]);

        return $this->handleView($view);
    }


    /**
     * Close conversation entity.
     * I have not found API with user call history.
     *
     * @Post("/api/jabber_voice/telephone/{idDialog}/process_finish_conversation", name="jabberVoiceFinishDialog", options = { "expose" = true })
     */
    public function postProcessFinishConversationAction($idDialog)
    {
        $conversation = $this
            ->getDoctrine()
            ->getRepository('JabberBundle:Conversation')
            ->findConversationByIdDialog($idDialog);

        /** @var $conversation Conversation */
        if ($conversation)
        {
            $conversation->setStatus($conversation::STATUS_FINISHED);
            $conversation->setConversationFinishTime(new DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($conversation);
            $em->flush();

            $view = $this->view([
                'status' => true,
                'conversation' => [
                    'id' => $conversation->getId()
                ]
            ]);
        }
        else
        {
            $view = $this->view([
                'status' => false
            ]);
        }

        // Build output
        return $this->handleView($view);
    }

    /**
     * Process conversation entity (create or update) by response dialog API status.
     *
     * @Post("/api/jabber_voice/telephone/{idDialog}/process_conversation", requirements={"idDialog": "\d+"}, name="jabberVoiceProcessDialog", options = { "expose" = true })
     */
    public function postProcessConversationAction($idDialog)
    {
        // Send request
        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('Dialog/%d', $idDialog))
            ->execute('get');

        if ($client->getStatusCode() === Response::HTTP_OK)
        {
            $conversation = $this
                ->getDoctrine()
                ->getRepository('JabberBundle:Conversation')
                ->findConversationByIdDialog($idDialog);

            // Parse request content
            $crawler = $this->getXMLCrawler();
            $crawler->addXmlContent($client->getResponse());

            $result = $this->processConversationEntity($crawler, $conversation);
            $view = $this->view([
                'status' => true,
                'conversation' => [
                    'id' => $result->getId()
                ]
            ]);
        }
        else
        {
            $view = $this->view([
                'status' => false
            ]);
        }

        // Build output
        return $this->handleView($view);
    }

    /**
     * Process conversation entity by dialog API response.
     * Notice: conversation members support only two callers (CLIENT, SUPPORT).
     * But we can append new member from transfer option. I don't apply
     * right now that function because i don't know if they event wanna this.
     *
     * @param Crawler $crawler Loaded crawler with XML document
     * @param Conversation|null $conversation Conversation entity
     * @return Conversation
     */
    private function processConversationEntity(Crawler $crawler, Conversation $conversation = null)
    {
        $em = $this->getDoctrine()->getManager();
        $createEntity = ($conversation === null);
        $conversation = ($conversation ?: new Conversation());

        $conversation->setType($crawler->filter('Dialog > mediaProperties > callType')->first()->text());
        $conversation->setStatus($crawler->filter('Dialog > state')->first()->text());
        $conversation->setIdDialog($crawler->filter('Dialog > id')->first()->text());
        $conversation->setDepartamentCode($crawler->filter('Dialog > mediaProperties > DNIS')->first()->text());
        $conversation->setFromAddress($crawler->filter('Dialog > fromAddress')->first()->text());
        $conversation->setToAddress($crawler->filter('Dialog > toAddress')->first()->text());

        if (true === $createEntity)
        {
            $conversation->setConversationStartTime(
                new DateTime(
                    $crawler->filter('participants > Participant > startTime')->first()->text()
                )
            );

            // Add support member
            $supportMember = new ConversationMember();
            $supportMember->setIsConversationMaster(true);
            $supportMember->setRole(ConversationMember::ROLE_SUPPORT);
            $supportMember->setAddress(
                ($conversation->getType() == 'OUT') ? $conversation->getFromAddress() : $conversation->getToAddress()
            );

            // Add client member
            $clientMember = new ConversationMember();
            $clientMember->setIsConversationMaster(true);
            $clientMember->setRole(ConversationMember::ROLE_CLIENT);
            $clientMember->setAddress(
                ($conversation->getType() == 'OUT') ? $conversation->getToAddress() : $conversation->getFromAddress()
            );

            if($this->getUser()) {
                $supportMember->setIdAuthor($this->getUser()->getId());
                $clientMember->setIdAuthor($this->getUser()->getId());
            }

            $conversation->addMember($supportMember);
            $conversation->addMember($clientMember);
        }

        $em->persist($conversation);
        $em->flush();

        return $conversation;
    }
}