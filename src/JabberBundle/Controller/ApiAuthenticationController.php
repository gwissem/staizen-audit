<?php

namespace JabberBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ApiAuthenticationController extends ApiBaseController
{

    const PREFIX_KEY = 'username_jabber_';

    /**
     * Login to Cisco Finesse API.
     * Please do not confuse with the connection to the telephone exchange
     *
     * @Put("/api/jabber_voice/authentication/login", name="jabberVoiceAuthLogin", options = { "expose" = true } )
     */
    public function putLoginAction(Request $request)
    {
        // Build request body
        $templateBody = $this
            ->getRequestTemplate()
            ->loadTemplate('authentication/login.xml.twig')
            ->render([
                'cucumExtension' => $this->getCUCUMUserEntty()->getExtension()
            ]);

        // Send request
        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('User/%s', $this->getCUCUMUserEntty()->getLogin()))
            ->setOptions([
                'body' => $templateBody,
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('put');

        // Build output
        return $this->handleView(
            $this->view([
                'status' => ($client->getStatusCode() === Response::HTTP_ACCEPTED)
            ])
        );
    }

    /**
     * Logout from Cisco Finesse API.
     * Please do not confuse with the connection to the telephone exchange
     *
     * @Put("/api/jabber_voice/authentication/logout")
     */
    public function putLogoutAction()
    {
        // Build request body
        $templateBody = $this
            ->getRequestTemplate()
            ->loadTemplate('authentication/logout.xml.twig')
            ->render();

        // Send request
        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('User/%s', $this->getCUCUMUserEntty()->getLogin()))
            ->setOptions([
                'body' => $templateBody,
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('put');

        // Build output
        return $this->handleView(
            $this->view([
                'status' => ($client->getStatusCode() === Response::HTTP_ACCEPTED)
            ])
        );
    }

    /**
     * Keep session still alive. When session will expire
     * all application may crash, because we need authorization to get
     * user data
     *
     * @Get("/api/jabber_voice/authentication/refresh_service_user_session", name="jabberVoiceRefreshUserSession", options = { "expose" = true })
     */
    public function getRefreshServiceUserSessionAction()
    {
        // Build output
        return $this->handleView(
            $this->view([
                'status' => true
            ])
        );
    }
}