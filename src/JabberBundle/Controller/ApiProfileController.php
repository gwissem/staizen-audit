<?php

namespace JabberBundle\Controller;

use Elastica\Processor\Json;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use JabberBundle\Communication\ClientRequest;
use JabberBundle\Communication\Entity\CUCUMUser;
use ManagementBundle\Utils\ScheduleHour;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JabberBundle\Form\Profile\Status\NotReadyType as FormNotReadyType;
use JabberBundle\Entity\Individual\ProfileStatus;
use UserBundle\Entity\TelephonyAgentStatus;
use UserBundle\Entity\User;

class ApiProfileController extends ApiBaseController
{

    const READ_ICON = 'fa-check-circle';
    const NOT_READ_ICON = 'fa-times-circle';

    const STATUS_READY = 'READY';
    const STATUS_NOT_READY = "NOT_READY";

    public $allowedStatuses = [1,2,5];

    /**
     * @Get("/api/jabber_voice/profile/get_supported_status", name="jabberVoiceGetSupportedStatus", options = { "expose" = true } )
     */

    public function getSupportedStatus()
    {

        // Send request
        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('User/%s/ReasonCodes?category=NOT_READY', $this->getCUCUMUserEntty()->getLogin()))
            ->execute('get');

        $status = ($client->getStatusCode() === Response::HTTP_OK);
        $codes = false;

        if ($status) {
            $crawler = $this->getXMLCrawler();
            $crawler->addXmlContent($client->getResponse());
            $codes = $crawler->filter('ReasonCode');
        }

        $statuses = [];
        $translator = $this->get('translator');

        $isScheduleTelephonyStatus = $this->get('management.schedule_telephony_status.service')->hasTerm($this->getUser());

        if($codes) {

            $statuses[] = [
                'type' => self::STATUS_READY,
                'label' => $translator->trans('Ready'),
                'icon' => self::READ_ICON,
                'idCode' => 0,
                'isActive' => false,
                'disabled' => false
            ];

//            $statuses[] = [
//                'type' => self::STATUS_NOT_READY,
//                'label' =>  $translator->trans("Not ready"),
//                'icon' => self::NOT_READ_ICON,
//                'idCode' => -1,
//                'isActive' => false
//            ];

            /** @var \DOMElement $item */
            foreach ($codes as $item) {

                $newStatus = [];
                $newStatus['type'] = $item->getElementsByTagName('category')->item(0)->nodeValue;
                $newStatus['label'] =  $translator->trans("Not ready - %label%", ['%label%' => $item->getElementsByTagName('label')->item(0)->nodeValue]);
                $newStatus['icon'] = ($newStatus['type'] == self::STATUS_NOT_READY) ? self::NOT_READ_ICON : self::READ_ICON;
                $newStatus['idCode'] = $item->getElementsByTagName('code')->item(0)->nodeValue;
                $newStatus['isActive'] = false;
                $newStatus['disabled'] = true;

                if($isScheduleTelephonyStatus || in_array($newStatus['idCode'], $this->allowedStatuses)) {
                    $newStatus['disabled'] = false;
                }

                $statuses[] = $newStatus;
            }
        }

//
//        $statuses = [
//            [
//                'type' => self::STATUS_READY,
//                'label' => $translator->trans('Ready'),
//                'icon' => self::READ_ICON,
//                'idCode' => 0
//            ],
//            [
//                'type' => self::STATUS_NOT_READY,
//                'label' => $translator->trans('Not Ready - Long Break 15 min'),
//                'icon' => self::NOT_READ_ICON,
//                'idCode' => 2
//            ],
//            [
//                'type' => self::STATUS_NOT_READY,
//                'label' => $translator->trans('Not Ready - Short Break 5 min'),
//                'icon' => self::NOT_READ_ICON,
//                'idCode' => 1
//            ],
//            [
//                'type' => self::STATUS_NOT_READY,
//                'label' => $translator->trans('Not Ready - Praca w Aplikacji'),
//                'icon' => self::NOT_READ_ICON,
//                'idCode' => 5
//            ],
//            [
//                'type' => self::STATUS_NOT_READY,
//                'label' => $translator->trans('Not Ready - Zadanie Specjalne'),
//                'icon' => self::NOT_READ_ICON,
//                'idCode' => 3
//            ],
//            [
//                'type' => self::STATUS_NOT_READY,
//                'label' => $translator->trans('Not Ready - Spotkanie poza CC'),
//                'icon' => self::NOT_READ_ICON,
//                'idCode' => 4
//            ]
//        ];

        return $this->handleView(
            $this->view([
                'statuses' => $statuses,
                'forceReady' => !$isScheduleTelephonyStatus
            ])
        );
    }


    /**
     * Get CUCUM User current status on Cisco Finesse API.
     * Please do not confuse with the connection to the telephone exchange
     *
     * @Get("/api/jabber_voice/profile/current_status", name="jabberVoiceGetCurrentStatus", options = { "expose" = true })
     */
    public function getCurrentStatusAction()
    {
        // Send request
        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('User/%s', $this->getCUCUMUserEntty()->getLogin()))
            ->execute('get');

        $status = ($client->getStatusCode() === Response::HTTP_OK);
        $value = false;
        $reasonCodeId = false;

        if ($status) {
            $crawler = $this->getXMLCrawler();
            $crawler->addXmlContent($client->getResponse());
            $value = $crawler->filter('User > state')->first()->text();
            if ($value == self::STATUS_NOT_READY) {
                $reasonCodeId = $crawler->filter('User > reasonCodeId')->first()->text();
            }
        }

        return $this->handleView(
            $this->view([
                'status' => $status,
                'message' => $value,
                'reasonCodeId' => $reasonCodeId
            ])
        );
    }

    /**
     * Set CUCUM User status to "READY" on Cisco Finesse API.
     * Please do not confuse with the connection to the telephone exchange
     *
     * @Put("/api/jabber_voice/profile/set_status/ready", name="jabberVoiceSetStatusReady", options = { "expose" = true } )
     * @return Response
     */
    public function putSetStatusReadyAction()
    {
        $username = $this->getCUCUMUserEntty()->getLogin();

        // Build request body
        $templateBody = $this
            ->getRequestTemplate()
            ->loadTemplate('profile/status/ready.xml.twig')
            ->render();

        // Send request
        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('User/%s', $username))
            ->setOptions([
                'body' => $templateBody,
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('put');

        if($client->getStatusCode() === Response::HTTP_ACCEPTED){
            /** @var TelephonyAgentStatus $telephonyAgentStatus */
            $telephonyAgentStatus = $this->getDoctrine()->getRepository(TelephonyAgentStatus::class)->findOneByUsername($username);
            if($telephonyAgentStatus){
                $em = $this->getDoctrine()->getManager();
                $now = new \DateTime();
                $telephonyAgentStatus->setState(self::STATUS_READY);
                $telephonyAgentStatus->setStateChangeTime($now);
                $telephonyAgentStatus->setCodeId(NULL);
                $em->persist($telephonyAgentStatus);
                $em->flush();

            }
        }

        // Build output
        return $this->handleView(
            $this->view([
                'status' => ($client->getStatusCode() === Response::HTTP_ACCEPTED)
            ])
        );
    }

    /**
     * Set CUCUM User status to "READY" on Cisco Finesse API.
     * Please do not confuse with the connection to the telephone exchange
     *
     * @Put("/api/jobs/jabber_voice/profile/set_status/ready/{username}", name="jabberVoiceSetStatusReadyUser", options = { "expose" = true } )
     * @param $username
     * @return Response
     */
    public function putSetStatusReadyUserAction($username)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneByUsername($username);

        // Build request body
        $templateBody = $this
            ->getRequestTemplate()
            ->loadTemplate('profile/status/ready.xml.twig')
            ->render();

        $data = $this->container->get('snc_redis.default')->hgetall(DefaultController::INDEX_JABBER_REDIS . $user->getId());
        $data['id'] = $user->getId();
        $data['username'] = $user->getUsername();

        $cucmUser = new CUCUMUser(null,null, $data);
        $client = new ClientRequest($cucmUser);

        $client->setActionUrl(sprintf('User/%s', $username))
            ->setOptions([
                'body' => $templateBody,
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('put');

        if($client->getStatusCode() === Response::HTTP_ACCEPTED){
            /** @var TelephonyAgentStatus $telephonyAgentStatus */
            $telephonyAgentStatus = $this->getDoctrine()->getRepository(TelephonyAgentStatus::class)->findOneByUsername($username);
            if($telephonyAgentStatus){
                $em = $this->getDoctrine()->getManager();
                $now = new \DateTime();
                $telephonyAgentStatus->setState(self::STATUS_READY);
                $telephonyAgentStatus->setStateChangeTime($now);
                $telephonyAgentStatus->setCodeId(NULL);
                $em->persist($telephonyAgentStatus);
                $em->flush();

                $this->get('app.notification')->sendToUser($user->getId(),'Czas na dokończenie zadania upłynął. Zmieniono status telefonii na READY', 'success');
            }
        }

        return new JsonResponse($client->getStatusCode());
    }

    /**
     * Set CUCUM User status to "NOT_READY" on Cisco Finesse API.
     * Notice that we need also send reason id.
     * Please do not confuse with the connection to the telephone exchange
     *
     * @Put("/api/jabber_voice/profile/set_status/not_ready", name="jabberVoiceSetStatusNotReady", options = { "expose" = true })
     */
    public function putSetStatusNotReadyAction(Request $request)
    {
        $current = null;

        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('User/%s', $this->getCUCUMUserEntty()->getLogin()))
            ->execute('get');

        $status = ($client->getStatusCode() === Response::HTTP_OK);

        if ($status) {
            $crawler = $this->getXMLCrawler();
            $crawler->addXmlContent($client->getResponse());
            $current = $crawler->filter('User > state')->first()->text();
        }

        $profile = new ProfileStatus();
        $form = $this
            ->createForm(FormNotReadyType::class, $profile)
            ->submit($request->request->all());


        // Zabezpieczenie czy może zmienić status na NOT READY inny niż przerwa (poza przypadkiem gdy zmieniay na dokończenie zgłoszenie z wrapa)
        if($profile->getIdCode() && (!in_array($profile->getIdCode(), $this->allowedStatuses) || ($current !== 'WORK' && $profile->getIdCode() == 5))) {

            $isScheduleTelephonyStatus = $this->get('management.schedule_telephony_status.service')->hasTerm($this->getUser());

            if(!$isScheduleTelephonyStatus) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'W tej chwili nie masz uprawnień do wybrania tego statusu.'
                ]);
            }

        }

        if ($form->isValid()) {
            // Build request body
            $templateBody = $this
                ->getRequestTemplate()
                ->loadTemplate('profile/status/not_ready.xml.twig')
                ->render([
                    'idCode' => $profile->getIdCode()
                ]);

            // Send request
            $client = $this
                ->getClientRequest()
                ->setActionUrl(sprintf('User/%s', $this->getCUCUMUserEntty()->getLogin()))
                ->setOptions([
                    'body' => $templateBody,
                    'headers' => [
                        'Content-Type' => 'application/xml'
                    ]
                ])
                ->execute('put');

            if($client->getStatusCode() === Response::HTTP_ACCEPTED){
                /** @var TelephonyAgentStatus $telephonyAgentStatus */
                $telephonyAgentStatus = $this->getDoctrine()->getRepository(TelephonyAgentStatus::class)->findOneByUsername($this->getCUCUMUserEntty()->getLogin());
                if($telephonyAgentStatus){
                    $em = $this->getDoctrine()->getManager();
                    $now = new \DateTime();
                    $telephonyAgentStatus->setState(self::STATUS_NOT_READY);
                    $telephonyAgentStatus->setStateChangeTime($now);
                    $telephonyAgentStatus->setCodeId($profile->getIdCode());
                    $em->persist($telephonyAgentStatus);
                    $em->flush();
                }
            }

            $view = $this->view([
                'status' => ($client->getStatusCode() === Response::HTTP_ACCEPTED)
            ], Response::HTTP_OK);

        } else {
            $view = $this->view($form, Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }
}