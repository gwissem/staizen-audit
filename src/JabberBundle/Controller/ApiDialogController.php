<?php

namespace JabberBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class ApiDialogController extends ApiBaseController
{
    /**
     * Get current CUCUM User dialog.
     * Notice: response can return event twice dialogs, but we
     * catch only first one.
     *
     * @Get("/api/jabber_voice/dialog/current", name="jabberVoiceCurrentDialog", options = { "expose" = true } )
*/
    public function getCurrentUserConversationAction()
    {
        // Send request
        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('User/%s/Dialogs', $this->getCUCUMUserEntty()->getLogin()))
            ->setOptions([
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('get');

        // Parse request content
        $crawler = $this->getXMLCrawler();
        $crawler->addXmlContent($client->getResponse());
        $dialogDetails = $this->buildCommunicationDialogOutput($crawler->filter('Dialogs'));

        // Build output
        return $this->handleView(
            $this->view([
                'status' => ($client->getStatusCode() === Response::HTTP_OK),
                'details' => $dialogDetails
            ])
        );
    }

    /**
     * Get specific dialog details
     *
     * @Get("/api/jabber_voice/dialog/{idDialog}/details", requirements={"idDialog": "\d+"})
     */
    public function getConversationDetailAction($idDialog)
    {
        // Send request
        $client = $this
            ->getClientRequest()
            ->setActionUrl(sprintf('Dialog/%d', $idDialog))
            ->execute('get');

        // Parse request content
        $crawler = $this->getXMLCrawler();
        $crawler->addXmlContent($client->getResponse());
        $dialogDetails = $this->buildCommunicationDialogOutput($crawler);

        // Build output
        return $this->handleView(
            $this->view([
                'status' => $client->getStatusCode(),
                'details' => $dialogDetails
            ])
        );
    }

    /**
     * Build communication dialog output
     *
     * @param Crawler $crawler Crawler instance
     * @return array|bool
     */
    private function buildCommunicationDialogOutput(Crawler $crawler)
    {
        $dialogDetails = false;
        $existAnyDialogs = (bool) ($crawler->filter('Dialog')->first()->count());

        if ($existAnyDialogs)
        {

            $csqId = null;

            // Czasami (a może i zawsze) Dialog ma dodatkowe parametry.
            // Gdy połączenie jest przekierowywane przez DTMF , to centralka zwraca do jakiej faktycznie infolini dzwoni klient

            if($crawler->filter('Dialog > mediaProperties > callvariables')->count()) {

                $variables = $crawler->filter('Dialog > mediaProperties > callvariables')->children();

                if($variables->count() > 0) {
                    foreach ($variables as $child) {
                        /** @var \DOMElement $child */
                        $name = $child->getElementsByTagName('name')->item(0)->nodeValue;
                        if($name === 'userCSQid') {
                            $csqId = $child->getElementsByTagName('value')->item(0)->nodeValue;
                        }
                    }
                }
            }

            $toAddress = (!empty($csqId)) ? $csqId : $crawler->filter('Dialog > toAddress')->first()->text();

            $dialogDetails = [
                'fromAddress' => $crawler->filter('Dialog > fromAddress')->first()->text(),
                'toAddress' => $toAddress,
                'idDialog' => $crawler->filter('Dialog > id')->first()->text(),
                'type' => $crawler->filter('Dialog > mediaProperties > callType')->first()->text(),
                'status' => $crawler->filter('Dialog > state')->first()->text(),
                'departamentCode' => $crawler->filter('Dialog > mediaProperties > DNIS')->first()->text()
            ];
        }

        return $dialogDetails;
    }
}