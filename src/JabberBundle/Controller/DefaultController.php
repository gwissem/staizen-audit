<?php

namespace JabberBundle\Controller;

use Symfony\Component\DomCrawler\Crawler;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    const INDEX_JABBER_REDIS = 'jabber_user:';
    const SUFFIX_USERNAME_JABBER = '@starter.local';

    /**
     * @Route("/jabber_voice")
     */
    public function indexAction()
    {
        return $this->render('JabberBundle:Default:index.html.twig');
    }

    /** Przekazywanie username i password do frontendu (do JS) */

    public function renderUserAccountAction() {

        $redis = $this->get('snc_redis.default');

        $originalUser = $this->get('user.info')->getOriginalUser();
        $userInfo = $this->get('user.info')->getInfo();

        $data = $redis->hgetall(self::INDEX_JABBER_REDIS . $originalUser->getId());

        $pass = base64_encode(json_encode($data, JSON_FORCE_OBJECT));

        return $this->render(
            '@Jabber/Components/jabber-account-variable.html.twig',
            [
                'data' => $pass,
                'user' => $userInfo
            ]
        );

    }

}