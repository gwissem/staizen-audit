<?php

namespace JabberBundle\Controller;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\PlatformPhone;
use FOS\RestBundle\Controller\Annotations\Get;
use JabberBundle\Entity\HelpLine;
use SocketBundle\RPC\AtlasTelephonyRpc;
use Symfony\Component\HttpFoundation\Request;

class ApiServerController extends ApiBaseController
{

    /**
     * Get greetings list of helplines
     *
     * @Get("/api/jabber_voice/server/client-info/{type}/{data}", name="jabberVoiceGetClientInfo", options = { "expose" = true } )
     * @param Request $request
     * @param $type
     * @param $data
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getClientInfoAction(Request $request, $type, $data)
    {

        $welcome = null;

        /** Odczytywanie komunikatu powitalnego [403] */

        if ($type == 'process') {
            $processHandler = $this->get('case.process_handler');
            $welcome = $processHandler->getAttributeValue(AtlasTelephonyRpc::PATH_WELCOME, intval($data), AttributeValue::VALUE_TEXT, NULL);
            if (is_array($welcome) && count($welcome) === 0) $welcome = null;
        } elseif ($type == 'number') {

        }

        return $this->handleView(
            $this->view([
                'welcome' => $welcome
            ])
        );
    }

    /**
     * Get greetings list of helplines
     *
     * @Get("/api/jabber_voice/server/helpline-list", name="jabberVoiceGetHelpLineList", options = { "expose" = true } )
     */
    public function getHelpLineListAction()
    {

        $list = $this->getDoctrine()
            ->getRepository('CaseBundle:PlatformPhone')
            ->getAllHelplines();

        $arrayList = [];

        /** @var PlatformPhone $item */
        foreach ($list as $item) {
            $helpline = [
                'id' => $item->getId(),
                'number' => $item->getNumber(),
                'welcome' => $item->getWelcome(),
                'ringtone' => $item->getRingtone(),
                'platform' => [
                    'id' => $item->getPlatform()->getId(),
                    'name' => $item->getPlatform()->getName()
                ]
            ];

            $arrayList[] = $helpline;
        }

        return $this->handleView(
            $this->view([
                'status' => true,
                'list' => $arrayList
            ])
        );
    }

    /**
     * Get CUCUM server list
     *
     * @Get("/api/jabber_voice/server/list")
     */
    public function getServerListAction()
    {
        $list = $this->getDoctrine()
            ->getRepository('JabberBundle:CUCUMServer')
            ->getServerList();


        // Build output
        return $this->handleView(
            $this->view([
                'status' => count($list) > 0,
                'list' => $list
            ])
        );
    }
}