<?php

namespace JabberBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DomCrawler\Crawler;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\TelephonyAgentStatus;

class AjaxController extends Controller
{

    /**
     * @Route("/state-change-date", name="ajax_telephony_status_state_change_date", options = { "expose" = true } )
     * @Method("POST")
     */
    public function telephonyStatusStateChangeAction()
    {

        $username = $this->getUser()->getUsername();
        /** @var TelephonyAgentStatus $stateChange */
        $stateChange = $this->getDoctrine()->getRepository(TelephonyAgentStatus::class)->findOneByUsername($username);
        $response = [
            'time' => '00:00:00',
            'timestamp' => time()
        ];

        if($stateChange){
            $now = new \DateTime();
            $last = $stateChange->getStateChangeTime();
            if($last) {
                $diff = $now->getTimestamp() - $last->getTimestamp();
                $response = [
                    'time' => sprintf('%02d:%02d:%02d', ($diff / 3600), ($diff / 60 % 60), $diff % 60),
                    'timestamp' => $last->getTimestamp()
                ];
            }
        }

        return new JsonResponse($response);
    }


}