<?php

namespace JabberBundle\Controller;

use Symfony\Component\DomCrawler\Crawler;
use FOS\RestBundle\Controller\FOSRestController;

class ApiBaseController extends FOSRestController
{
    /**
     * Get CUCUM User Entity
     *
     * @return \JabberBundle\Communication\Entity\CUCUMUserInterface
     */
    protected function getCUCUMUserEntty()
    {
        /** @var $cucumUser \JabberBundle\Communication\Entity\CUCUMUserInterface */
        $user = $this->get('jabber_voice_conversation.communication.entity.cucum_user');

        return $user;
    }

    /**
     * Get template engine for build request body
     *
     * @return \JabberBundle\Communication\RequestTemplateParserInterface
     */
    protected function getRequestTemplate()
    {
        /** @var $parser \JabberBundle\Communication\RequestTemplateParserInterface */
        $engine = $this->get('jabber_voice_conversation.communication.request_template_parser');

        return $engine;
    }

    /**
     * Get HTTP Client Request instance
     *
     * @return \JabberBundle\Communication\ClientRequestInterface
     */
    protected function getClientRequest()
    {
        /** @var $clientRequest \JabberBundle\Communication\ClientRequestInterface */
        $clientRequest = $this->get('jabber_voice_conversation.communication.client_request');

        return $clientRequest;
    }

    /**
     * Get XML document crawler
     *
     * @return Crawler
     */
    protected function getXMLCrawler()
    {
        $crawler = new Crawler();

        return $crawler;
    }
}