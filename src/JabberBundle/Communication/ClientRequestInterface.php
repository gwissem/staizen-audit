<?php

namespace JabberBundle\Communication;

use JabberBundle\Communication\Entity\CUCUMUserInterface;

interface ClientRequestInterface
{
    /**
     * REST API url
     */
    const API_URL = 'https://10.10.77.101:8445/finesse/api';
    const API_URL_RESERVE = 'https://10.10.77.107:8445/finesse/api';

    /**
     * Define timeout limit to wait when request are done
     */
    const TIMEOUT_LIMIT = 5;

    /**
     * Set CUCUM user entity
     *
     * @param CUCUMUserInterface $user Entity
     * @return $this
     */
    public function setCUCUMUser(CUCUMUserInterface $user);

    /**
     * Get CUCUM user entity
     *
     * @return CUCUMUserInterface
     */
    public function getCUCUMUser();

    /**
     * Set HTTP Client connection options
     *
     * @param array $options Configuration
     * @return $this
     */
    public function setOptions(array $options);

    /**
     * Get HTTP Client configuration options
     *
     * @return array
     */
    public function getOptions();

    /**
     * API address url
     *
     * @param string $baseUrl Base url to API (domain)
     * @return $this
     */
    public function setBaseUrl($baseUrl);

    /**
     * Get API address url
     *
     * @return string
     */
    public function getBaseUrl();

    /**
     * Url to current action in API methods
     *
     * @param string $url Action url
     * @return $this
     */
    public function setActionUrl($url);

    /**
     * Get action URL
     *
     * @return string
     */
    public function getActionUrl();

    /**
     * Execute request
     *
     * @param string $method Request method
     * @return $this
     */
    public function execute($method);

    /**
     * Get raw response from URL
     *
     * @return string
     */
    public function getResponse();

    /**
     * Get HTTP response code
     *
     * @return int
     */
    public function getStatusCode();
}