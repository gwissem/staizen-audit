<?php

namespace JabberBundle\Communication\Entity;

interface CUCUMUserInterface
{
    /**
     * Set user service identifier
     *
     * @param integer $serviceUserId User identifier
     * @return $this
     */
    public function setServiceUserId($serviceUserId);

    /**
     * Get user service identifier
     *
     * @return int
     */
    public function getServiceUserId();

    /**
     * Set CUCUM username/login
     *
     * @param string|null $login Username/login
     * @return $this
     */
    public function setLogin($login);

    /**
     * Get CUCUM username/login
     *
     * @return string|null
     */
    public function getLogin();

    /**
     * Set CUCUM password
     *
     * @param string|null $password Password
     * @return $this
     */
    public function setPassword($password);

    /**
     * Get CUCUM password
     *
     * @return string|null
     */
    public function getPassword();

    /**
     * Set CUCUM phone extension
     *
     * @param string|null $extension Phone extension
     * @return $this
     */
    public function setExtension($extension);

    /**
     * Get CUCUM phone extension
     *
     * @return string|null
     */
    public function getExtension();
}