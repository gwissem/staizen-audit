<?php

namespace JabberBundle\Communication\Entity;

use JabberBundle\Controller\DefaultController;
use Predis\Client;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

class CUCUMUser implements CUCUMUserInterface
{
    /**
     * User service identifier
     *
     * @var int
     */
    private $serviceUserId;

    /**
     * CUCUM username/login
     *
     * @var string|null
     */
    private $login = null;

    /**
     * CUCUM password
     *
     * @var string|null
     */
    private $password = null;

    /**
     * CUCUM phone extension
     *
     * @var string|null
     */
    private $extension = null;

    /**
     * @inheritdoc
     */
    public function __construct(TokenStorage $token = null, Client $redis = null, $data = null)
    {
        if($data !== null ) {
            $this->setServiceUserId($data['id']);
            $this->setLogin($data['username']);
            $this->setPassword($data['password']);
            $this->setExtension($data['extension']);
        }
        elseif ($token AND $user = $token->getToken()->getUser())
        {
            /** Wyciągnięcie jawnego hasła */
            $data = $redis->hgetall(DefaultController::INDEX_JABBER_REDIS . $user->getId());
            $this->setServiceUserId($user->getId());
            $this->setLogin($user->getUsername());
            $this->setPassword($data['password']);
            $this->setExtension($user->getCucumExtension());
        }
    }

    /**
     * @inheritdoc
     */
    public function setServiceUserId($serviceUserId)
    {
        $this->serviceUserId = $serviceUserId;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getServiceUserId()
    {
        return $this->serviceUserId;
    }

    /**
     * @inheritdoc
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @inheritdoc
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getExtension()
    {
        return $this->extension;
    }
}