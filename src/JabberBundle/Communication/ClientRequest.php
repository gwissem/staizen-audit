<?php

namespace JabberBundle\Communication;

use Exception;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ConnectException;
use Psr\Http\Message\ResponseInterface;
use JabberBundle\Communication\Entity\CUCUMUserInterface;
use JabberBundle\Exception\ClientRequest\UndefinedExecuteMethodException;
use JabberBundle\Exception\ClientRequest\ConnectException as ClientConnectException;
use JabberBundle\Exception\ClientRequest\NotSpecifiedException;
use Symfony\Component\HttpFoundation\Response;

class ClientRequest implements ClientRequestInterface
{
    /**
     * CUCUM User Entity
     *
     * @var CUCUMUserInterface
     */
    private $cuCUMUser;

    /**
     * API address url
     *
     * @var string
     */
    private $baseUrl = self::API_URL;

    /**
     * Action url
     *
     * @var string
     */
    private $actionUrl;

    /**
     * HTTP Client configuration options
     *
     * @var array
     */
    private $options = array();

    /**
     * HTTP Client
     *
     * @var HttpClient
     */
    private $httpClient;

    /**
     * HTTP Response
     *
     * @var \GuzzleHttp\Psr7\Response
     */
    private $httpResponse = null;

    /**
     * @inheritdoc
     */
    public function __construct(CUCUMUserInterface $cuCUMUser)
    {
        $this->setCUCUMUser($cuCUMUser);
        $this->configureDefaults([]);
    }

    /**
     * Configure HTTP Client options
     *
     * @param array $options Extra configuration options
     * @return array
     */
    private function configureDefaults(array $options = array())
    {
        // Prepare default options
        $default = [
            'timeout' => self::TIMEOUT_LIMIT,
            'verify' => false,
            'http_errors' => false,
            'auth' => [
                $this->getCUCUMUser()->getLogin(),
                $this->getCUCUMUser()->getPassword()
            ]
        ];

        $options = array_replace_recursive($default, $options);
        $this->options = $options;

        return $options;
    }

    /**
     * @inheritdoc
     */
    public function getCUCUMUser()
    {
        return $this->cuCUMUser;
    }

    /**
     * @inheritdoc
     */
    public function setCUCUMUser(CUCUMUserInterface $user)
    {
        $this->cuCUMUser = $user;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function execute($method)
    {
        if (in_array(strtolower($method), ['get', 'head', 'put', 'post', 'patch', 'delete'])) {
            try {
                $fullUrl = ($this->getBaseUrl() ? rtrim($this->getBaseUrl(), '/') . '/' : null) . $this->getActionUrl();
                $this->httpClient = new HttpClient($this->getOptions());
                $this->httpResponse = call_user_func_array([$this->httpClient, $method], [$fullUrl, []]);

//                if($this->httpResponse->getStatusCode() !== Response::HTTP_ACCEPTED) {
//                    throw new \JabberBundle\Exception\ClientRequest\ConnectException();
//                }

                return $this;

            } catch (ConnectException $e) {

                /** try connect with second API_URL */

                $fullUrl = '';

                try {

                    $fullUrl = (self::API_URL_RESERVE ? rtrim(self::API_URL_RESERVE, '/') . '/' : null) . $this->getActionUrl();
                    $this->httpClient = new HttpClient($this->getOptions());
                    $this->httpResponse = call_user_func_array([$this->httpClient, $method], [$fullUrl, []]);

                    return $this;

                } catch (ConnectException $e) {
                    throw new ClientConnectException(sprintf('Connection timed out for "%s" url.', $fullUrl));
                } catch (Exception $e) {
                    throw new NotSpecifiedException('Request process problem. Exception class: "%s" with message: "%s"', get_class($e), $e->getMessage());
                }

            } catch (Exception $e) {
                throw new NotSpecifiedException('Request process problem. Exception class: "%s" with message: "%s"', get_class($e), $e->getMessage());
            }
        } else {
            throw new UndefinedExecuteMethodException(sprintf('Not supported metchod "%s".', $method));
        }
    }

    /**
     * @inheritdoc
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @inheritdoc
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getActionUrl()
    {
        return $this->actionUrl;
    }

    /**
     * @inheritdoc
     */
    public function setActionUrl($url)
    {
        $this->actionUrl = $url;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @inheritdoc
     */
    public function setOptions(array $options)
    {
        $this->options = $this->configureDefaults($options);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getResponse()
    {
        return $this->httpResponse->getBody()->getContents();
    }

    /**
     * @inheritdoc
     */
    public function getStatusCode()
    {
        return $this->httpResponse->getStatusCode();
    }
}