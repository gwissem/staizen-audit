<?php

namespace JabberBundle\Communication;

use Exception;
use Twig_Environment;
use Twig_Loader_Filesystem;
use Twig_TemplateInterface;
use Twig_Error_Loader;
use JabberBundle\Exception\RequestTemplateParser\LoaderException as TemplateLoaderException;

class RequestTemplateParser implements RequestTemplateParserInterface
{
    /**
     * Template engine
     *
     * @var Twig_Environment
     */
    private $engine;

    /**
     * Template to process
     *
     * @var Twig_TemplateInterface
     */
    private $template;

    /**
     * @param Twig_Environment $templateEngine Framework template engine
     *
     * @inheritdoc
     */
    public function __construct(Twig_Environment $templateEngine)
    {
        $this->engine = $templateEngine;
        $this->engine->setLoader(
            new Twig_Loader_Filesystem(dirname(__DIR__) . '/Resources/requestTemplate')
        );
    }

    /**
     * @inheritdoc
     */
    public function loadTemplate($path)
    {
        try
        {
            $loader = $this->engine->loadTemplate($path);
            $this->template = $loader;
        }
        catch (Twig_Error_Loader $exception) {
            throw new TemplateLoaderException(sprintf('Could not found request template "%s". System was looking at: "%s" directories.', $path, implode(', ', $this->engine->getLoader()->getPaths())));
        }
        catch (Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function render(array $params = array())
    {
        return $this
            ->template
            ->render($params);
    }
}