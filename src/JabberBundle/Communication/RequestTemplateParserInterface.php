<?php

namespace JabberBundle\Communication;

interface RequestTemplateParserInterface
{
    /**
     * Load communication request template
     *
     * @param string $path Template path
     * @return $this
     */
    public function loadTemplate($path);

    /**
     * Render request template
     *
     * @param array $params Parameters to set into template
     * @return string
     */
    public function render(array $params = array());
}