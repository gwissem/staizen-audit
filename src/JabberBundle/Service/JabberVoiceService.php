<?php

namespace JabberBundle\Service;


use Predis\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JabberVoiceService
{

    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @var Client $redis
     */
    protected $redis;

    /**
     * JabberVoiceService constructor.
     * @param ContainerInterface $container
     * @param Client $redis
     */
    public function __construct(ContainerInterface $container, Client $redis)
    {
        $this->container = $container;
        $this->redis = $redis;
    }


}