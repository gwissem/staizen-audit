<?php

namespace JabberBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ExistsStatusIdCode extends Constraint
{
    /**
     * Translation
     *
     * @var string
     */
    public $message = 'jabber_voice_conversation.profile_status.id_code_not_exists';
}