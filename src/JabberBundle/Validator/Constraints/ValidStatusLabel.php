<?php

namespace JabberBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ValidStatusLabel extends Constraint
{
    /**
     * Translation
     *
     * @var string
     */
    public $message = 'jabber_voice_conversation.profile_status.label_not_valid';
}