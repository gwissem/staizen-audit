<?php

namespace JabberBundle\Validator\Constraints;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JabberBundle\Communication\ClientRequestInterface;
use JabberBundle\Communication\Entity\CUCUMUserInterface;

class ExistsStatusIdCodeValidator extends ConstraintValidator
{
    /**
     * Client HTTP Requester
     *
     * @var ClientRequestInterface
     */
    private $clientRequester;

    /**
     * CUCUM User entity
     *
     * @var CUCUMUserInterface
     */
    private $cucumUser;

    /**
     * @inheritdoc
     */
    public function __construct(ClientRequestInterface $clientRequester, CUCUMUserInterface $cucumUser)
    {
        $this->clientRequester = $clientRequester;
        $this->cucumUser = $cucumUser;
    }

    /**
     * Check if idCode of status exists
     *
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var $profileStatus \JabberBundle\Entity\Individual\ProfileStatusInterface */
        $profileStatus = $this->context->getObject();
        $clientRequest = $this
            ->clientRequester
            ->setActionUrl(sprintf('User/%s/ReasonCodes?category=%s', $this->cucumUser->getLogin(), $profileStatus->getLabel()))
            ->setOptions([
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ])
            ->execute('get');

        // Process request body
        {
            $notValid = true;
            if ($clientRequest->getStatusCode() == Response::HTTP_OK)
            {
                $crawler = new Crawler();
                $crawler->addXmlContent($clientRequest->getResponse());
                $list = (array) $crawler
                    ->filter('ReasonCodes > ReasonCode')
                    ->each(function(Crawler $node, $i) use($value) {
                        return $node->filter('code')->first()->text();
                    });

                $notValid = (false === in_array($value, $list));
            }
        }

        if ($notValid)
        {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}