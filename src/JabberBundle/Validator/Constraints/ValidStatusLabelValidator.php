<?php

namespace JabberBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use JabberBundle\Entity\Individual\ProfileStatus;

class ValidStatusLabelValidator extends ConstraintValidator
{
    /**
     * Check if status label is correct
     *
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (false === in_array($value, ProfileStatus::supportedLabels()))
        {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}