<?php

namespace JabberBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HelpLine
 *
 * @ORM\Table(name="help_line")
 * @ORM\Entity(repositoryClass="JabberBundle\Repository\HelpLineRepository")
 */
class HelpLine
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="welcome", type="string", length=255, nullable=true)
     */
    private $welcome;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return HelpLine
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set welcome
     *
     * @param string $welcome
     *
     * @return HelpLine
     */
    public function setWelcome($welcome)
    {
        $this->welcome = $welcome;

        return $this;
    }

    /**
     * Get welcome
     *
     * @return string
     */
    public function getWelcome()
    {
        return $this->welcome;
    }
}

