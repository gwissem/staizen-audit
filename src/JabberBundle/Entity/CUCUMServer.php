<?php

namespace JabberBundle\Entity;

class CUCUMServer implements CUCUMServerInterface
{
    /**
     * Entity identifier
     *
     * @var int
     */
    private $id;

    /**
     * CUCUM server address
     *
     * @var string
     */
    private $address;

    /**
     * Priority
     *
     * @var int
     */
    private $priority;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
       $this->setPriority(0);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @inheritdoc
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        return $this->priority;
    }
}