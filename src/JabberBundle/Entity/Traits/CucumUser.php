<?php

namespace JabberBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait CucumUser
{
    /**
     * @var string $cucmLogin
     *
     * @ORM\Column(name="cucm_login", type="string", length=255, nullable=true)
     */
    protected $cucmLogin;

    /**
     * @var string $cucmPassword
     *
     * @ORM\Column(name="cucm_password", type="string", length=255, nullable=true)
     */
    protected $cucmPassword;

    /**
     * @var string $cucumExtension
     *
     * @ORM\Column(name="cucum_extension", type="string", length=32, nullable=true)
     */

    protected $cucumExtension;

    /**
     * @return string
     */
    public function getCucmLogin()
    {
        return $this->cucmLogin;
    }

    /**
     * @param string $cucmLogin
     */
    public function setCucmLogin($cucmLogin)
    {
        $this->cucmLogin = $cucmLogin;
    }

    /**
     * @return string
     */
    public function getCucmPassword()
    {
        return $this->cucmPassword;
    }

    /**
     * @param string $cucmPassword
     */
    public function setCucmPassword($cucmPassword)
    {
        $this->cucmPassword = $cucmPassword;
    }



    /**
     * @return string
     */
    public function getCucumExtension()
    {
        return $this->cucumExtension;
    }

    /**
     * @param string $cucumExtension
     */
    public function setCucumExtension($cucumExtension)
    {
        $this->cucumExtension = $cucumExtension;
    }


}