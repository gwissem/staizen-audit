<?php

namespace JabberBundle\Entity\Individual;

interface ProfileStatusInterface
{
    /**
     * Set status label
     *
     * @param string $label Status name/label
     * @return $this
     */
    public function setLabel($label);

    /**
     * Get status label
     *
     * @return string
     */
    public function getLabel();

    /**
     * Identifier status for label like "NOT_READY"
     *
     * @param int $idCode Identifier status
     * @return mixed
     */
    public function setIdCode($idCode);

    /**
     * @return int
     */
    public function getIdCode();

    /**
     * List of supported labels
     *
     * @return array
     */
    public static function supportedLabels();
}