<?php

namespace JabberBundle\Entity\Individual;

class ProfileStatus implements ProfileStatusInterface
{
    /**
     * Label name (like: READY, NOT_READY)
     *
     * @var string
     */
    private $label;

    /**
     * Identifier status (for NOT_READY label)
     *
     * @var int
     */
    private $idCode;

    /**
     * @inheritdoc
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @inheritdoc
     */
    public function setIdCode($idCode)
    {
        $this->idCode = $idCode;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getIdCode()
    {
        return $this->idCode;
    }

    /**
     * @inheritdoc
     */
    public static function supportedLabels()
    {
        return [
            'READY',
            'NOT_READY'
        ];
    }
}