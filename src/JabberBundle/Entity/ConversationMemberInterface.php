<?php

namespace JabberBundle\Entity;

interface ConversationMemberInterface
{
    /**
     * Role type
     */
    const ROLE_CLIENT = 'CLIENT';
    const ROLE_SUPPORT = 'SUPPORT';

    /**
     * Get PK for entity
     *
     * @return int
     */
    public function getId();

    /**
     * Set id conversation
     *
     * @param integer $idConversation
     *
     * @return $this
     */
    public function setIdConversation($idConversation);

    /**
     * Get id conversation
     *
     * @return int
     */
    public function getIdConversation();

    /**
     * Set role
     *
     * @param string $role
     *
     * @return $this
     */
    public function setRole($role);

    /**
     * Get role
     *
     * @return int
     */
    public function getRole();

    /**
     * Set is conversation master
     *
     * @param boolean $isConversationMaster
     *
     * @return $this
     */
    public function setIsConversationMaster($isConversationMaster);

    /**
     * Get is conversation master
     *
     * @return bool
     */
    public function getIsConversationMaster();

    /**
     * Set address
     *
     * @param string $address
     *
     * @return $this
     */
    public function setAddress($address);

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress();

    /**
     * Set id author
     *
     * @param integer $idAuthor
     *
     * @return $this
     */
    public function setIdAuthor($idAuthor);

    /**
     * Get id author
     *
     * @return int
     */
    public function getIdAuthor();

    /**
     * Set created at
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get created at
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set updated at
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updated at
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Conversation related entity
     *
     * @return ConversationInterface
     */
    public function getConversation();

    /**
     * Append conversation to member
     *
     * @param ConversationInterface $conversation Conversation entity
     * @return $this
     */
    public function setConversation(ConversationInterface $conversation);
}