<?php

namespace JabberBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Conversation implements ConversationInterface
{
    /**
     * Entity identifier
     *
     * @var int
     */
    private $id;

    /**
     * Incoming or outcoming call
     *
     * @var int
     */
    private $type;

    /**
     * Conversation status
     *
     * @var int
     */
    private $status;

    /**
     * Dialog ID (from API)
     *
     * @var int
     */
    private $idDialog;

    /**
     * Departament channel (like "804" (TEST) or "200" for Starter)
     *
     * @var string
     */
    private $departamentCode;

    /**
     * The phone number of the person who is calling
     *
     * @var string
     */
    private $fromAddress;

    /**
     * The phone number of the person who received
     *
     * @var string
     */
    private $toAddress;

    /**
     * Conversation start time
     *
     * @var \DateTime
     */
    private $conversationStartTime;

    /**
     * Conversation finish time
     *
     * @var \DateTime
     */
    private $conversationFinishTime;

    /**
     * Entity creation time
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Entity update time
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Members assigned to conversation entity
     *
     * @var ConversationMemberInterface[]
     */
    private $members;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @inheritdoc
     */
    public function setIdDialog($idDialog)
    {
        $this->idDialog = $idDialog;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getIdDialog()
    {
        return $this->idDialog;
    }

    /**
     * @inheritdoc
     */
    public function setDepartamentCode($departamentCode)
    {
        $this->departamentCode = $departamentCode;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getDepartamentCode()
    {
        return $this->departamentCode;
    }

    /**
     * @inheritdoc
     */
    public function setFromAddress($fromAddress)
    {
        $this->fromAddress = $fromAddress;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    /**
     * @inheritdoc
     */
    public function setToAddress($toAddress)
    {
        $this->toAddress = $toAddress;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getToAddress()
    {
        return $this->toAddress;
    }

    /**
     * @inheritdoc
     */
    public function setConversationStartTime($conversationStartTime)
    {
        $this->conversationStartTime = $conversationStartTime;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getConversationStartTime()
    {
        return $this->conversationStartTime;
    }

    /**
     * @inheritdoc
     */
    public function setConversationFinishTime($conversationFinishTime)
    {
        $this->conversationFinishTime = $conversationFinishTime;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getConversationFinishTime()
    {
        return $this->conversationFinishTime;
    }

    /**
     * @inheritdoc
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @inheritdoc
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @inheritdoc
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @inheritdoc
     */
    public function addMember(ConversationMemberInterface $member)
    {
        $member->setConversation($this);
        $this->members->add($member);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function removeMember(ConversationMemberInterface $member)
    {
        $this->members->removeElement($member);

        return $this;
    }
}