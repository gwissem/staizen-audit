<?php

namespace JabberBundle\Entity;

use CaseBundle\Entity\ProcessInstance;

class ConversationTelephony
{
    const TYPE_IN = "IN";
    const TYPE_OUT = "OUT";

    const STATUS_RINGING = 'RINGING';
    const STATUS_PICK_UP = 'PICK_UP';
    const STATUS_CANCELED = 'CANCELED';
    const STATUS_NOT_PICK_UP = 'NOT_PICK_UP';
    const STATUS_FINISHED = 'FINISHED';
    const STATUS_FORCED_FINISHED = 'FORCED_FINISHED';
    const STATUS_BUSY = 'BUSY';

    /**
     * Entity identifier
     *
     * @var int
     */
    private $id;

    /**
     * Generate unique value in fronted-page for identification
     *
     * @var string
     */
    protected $uniqueRandom;

    /**
     * Incoming or outcoming call
     *
     * @var int
     */
    private $type;

    /**
     * Conversation status
     *
     * @var int
     */
    private $status;

    /**
     * The phone number of the person who is calling
     *
     * @var string
     */
    private $fromAddress;

    /**
     * The phone number of the person who received
     *
     * @var string
     */
    private $toAddress;


    /**
     * The name of person who calling
     *
     * @var string
     */
    private $participantName;

    /**
     * Conversation start time
     *
     * @var \DateTime
     */
    private $conversationStartTime;

    /**
     * Conversation finish time
     *
     * @var \DateTime
     */
    private $conversationFinishTime;

    /**
     * Entity creation time
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Entity update time
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * User service identifier
     *
     * @var int
     */
    private $author;

    /**
     * Dialog ID (from API)
     *
     * @var int
     */
    private $idDialog;

    /**
     * @var ProcessInstance
     */
    private $processInstance;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->setStatus(self::STATUS_RINGING);
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @inheritdoc
     */
    public function setFromAddress($fromAddress)
    {
        $this->fromAddress = $fromAddress;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    /**
     * @inheritdoc
     */
    public function setToAddress($toAddress)
    {
        $this->toAddress = $toAddress;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getToAddress()
    {
        return $this->toAddress;
    }

    /**
     * @inheritdoc
     */
    public function setConversationStartTime($conversationStartTime)
    {
        $this->conversationStartTime = $conversationStartTime;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getConversationStartTime()
    {
        return $this->conversationStartTime;
    }

    /**
     * @inheritdoc
     */
    public function setConversationFinishTime($conversationFinishTime)
    {
        $this->conversationFinishTime = $conversationFinishTime;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getConversationFinishTime()
    {
        return $this->conversationFinishTime;
    }

    /**
     * @inheritdoc
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @inheritdoc
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param int $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getParticipantName()
    {
        return $this->participantName;
    }

    /**
     * @param string $participantName
     */
    public function setParticipantName($participantName)
    {
        $this->participantName = $participantName;
    }

    /**
     * @return string
     */
    public function getUniqueRandom()
    {
        return $this->uniqueRandom;
    }

    /**
     * @param string $uniqueRandom
     */
    public function setUniqueRandom($uniqueRandom)
    {
        $this->uniqueRandom = $uniqueRandom;
    }

    /**
     * @return mixed
     */
    public function getIdDialog()
    {
        return $this->idDialog;
    }

    /**
     * @param mixed $idDialog
     */
    public function setIdDialog($idDialog)
    {
        $this->idDialog = $idDialog;
    }

    /**
     * @return mixed
     */
    public function getProcessInstance()
    {
        return $this->processInstance;
    }

    /**
     * @param mixed $processInstance
     */
    public function setProcessInstance($processInstance)
    {
        $this->processInstance = $processInstance;
    }

}