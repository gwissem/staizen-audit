<?php

namespace JabberBundle\Entity;

class ConversationMember implements ConversationMemberInterface
{
    /**
     * Entity identifier
     *
     * @var int
     */
    private $id;

    /**
     * Conversation relation id
     *
     * @var int
     */
    private $idConversation;

    /**
     * Role name
     *
     * @var string
     */
    private $role;

    /**
     * Define if user is connected (transferred) to conversation or not.
     *
     * @var bool
     */
    private $isConversationMaster;

    /**
     * Phone number, phone departament number
     *
     * @var string
     */
    private $address;

    /**
     * User service identifier
     *
     * @var int
     */
    private $idAuthor;

    /**
     * Entity creation time
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Entity update time
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Conversation related entity
     *
     * @var ConversationInterface
     */
    private $conversation;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function setIdConversation($idConversation)
    {
        $this->idConversation = $idConversation;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getIdConversation()
    {
        return $this->idConversation;
    }

    /**
     * @inheritdoc
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @inheritdoc
     */
    public function setIsConversationMaster($isConversationMaster)
    {
        $this->isConversationMaster = $isConversationMaster;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getIsConversationMaster()
    {
        return $this->isConversationMaster;
    }

    /**
     * @inheritdoc
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @inheritdoc
     */
    public function setIdAuthor($idAuthor)
    {
        $this->idAuthor = $idAuthor;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getIdAuthor()
    {
        return $this->idAuthor;
    }

    /**
     * @inheritdoc
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @inheritdoc
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @inheritdoc
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * @inheritdoc
     */
    public function setConversation(ConversationInterface $conversation)
    {
        $this->conversation = $conversation;

        return $this;
    }
}