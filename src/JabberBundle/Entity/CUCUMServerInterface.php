<?php

namespace JabberBundle\Entity;

interface CUCUMServerInterface
{
    /**
     * Get PK for entity
     *
     * @return int
     */
    public function getId();

    /**
     * Set CUCUM server address
     *
     * @param string $address Hostname
     *
     * @return $this
     */
    public function setAddress($address);

    /**
     * Get CUCUM server address
     *
     * @return string
     */
    public function getAddress();

    /**
     * Set priority
     *
     * @param $priority
     *
     * @return $this
     */
    public function setPriority($priority);

    /**
     * Get priority
     *
     * @return int
     */
    public function getPriority();
}