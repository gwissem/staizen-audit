<?php

namespace JabberBundle\Entity;

interface ConversationInterface
{
    /**
     * Define types id
     */
    const TYPE_INCOMING = 'ACD_IN';
    const TYPE_OUTCOMING = 'OUT';

    /**
     * Define conversation status
     */
    const STATUS_RINGING = 'ALERTING';
    const STATUS_TALKING = 'ACTIVE';
    const STATUS_FINISHED = 'FINISHED';

    /**
     * Get PK for entity
     *
     * @return int
     */
    public function getId();

    /**
     * Set conversation type
     *
     * @param string $type
     *
     * @return $this
     */
    public function setType($type);

    /**
     * Get conversation type
     *
     * @return string
     */
    public function getType();

    /**
     * Set conversation status
     *
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get conversation status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set idDialog identifier from API
     *
     * @param integer $idDialog
     *
     * @return $this
     */
    public function setIdDialog($idDialog);

    /**
     * Get idDialog
     *
     * @return int
     */
    public function getIdDialog();

    /**
     * Set departament code
     *
     * @param string $departamentCode Phone departament number
     *
     * @return $this
     */
    public function setDepartamentCode($departamentCode);

    /**
     * Get departament code
     *
     * @return string
     */
    public function getDepartamentCode();

    /**
     * Set from address (phone number, phone departament number)
     *
     * @param string $fromAddress
     *
     * @return $this
     */
    public function setFromAddress($fromAddress);

    /**
     * Get from address (phone number, phone departament number)
     *
     * @return string
     */
    public function getFromAddress();

    /**
     * Set to address (phone number, phone departament number)
     *
     * @param string $toAddress
     *
     * @return $this
     */
    public function setToAddress($toAddress);

    /**
     * Get to address (phone number, phone departament number)
     *
     * @return string
     */
    public function getToAddress();

    /**
     * Set conversation start time
     *
     * @param \DateTime $conversationStartTime
     *
     * @return $this
     */
    public function setConversationStartTime($conversationStartTime);

    /**
     * Get conversation start time
     *
     * @return \DateTime
     */
    public function getConversationStartTime();

    /**
     * Set conversation finish time
     *
     * @param \DateTime $conversationFinishTime
     *
     * @return $this
     */
    public function setConversationFinishTime($conversationFinishTime);

    /**
     * Get conversation finish time
     *
     * @return \DateTime
     */
    public function getConversationFinishTime();

    /**
     * Set created at
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set updated at
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updated at
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Members assigned to conversation entity
     *
     * @return ConversationMemberInterface[]
     */
    public function getMembers();

    /**
     * Append member to conversation
     *
     * @param ConversationMemberInterface $member Member entity
     * @return $this
     */
    public function addMember(ConversationMemberInterface $member);

    /**
     * Remove member from conversation
     *
     * @param ConversationMemberInterface $member Member entity
     * @return $this
     */
    public function removeMember(ConversationMemberInterface $member);
}