<?php

namespace ToolBundle\API\Controller;

use DocumentBundle\Entity\Document;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\UserFilterSet;
use ToolBundle\Form\Type\ToolSummaryType;
use ToolBundle\Form\Type\UserFilterSetType;
use ToolBundle\Form\Type\UserFilterType;
use ToolBundle\Form\Type\UserToolDataModifyType;

class DefaultController extends FOSRestController
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Action for displaying the data from the tool",
     *  requirements={
     *     {
     *      "name"="toolId",
     *      "dataType"="integer",
     *      "requirement"="\d+",
     *      "description"="tool id"
     *     }
     *  },
     *  statusCodes={
     *     200="Returned when successful",
     *     403="Returned when the user is not permitted to view the tool result",
     *     404="Returned when tool is not found"
     *  }
     * )
     *
     * @View()
     *
     * @Route("/{toolId}", name="api_tool_show", requirements={"toolId": "\d+"} )
     * @Method({"GET"})
     * @ParamConverter("tool", class="ToolBundle:Tool", options={"mapping":{"toolId"="id"}})
     */

    public function showAction(Request $request, Tool $tool)
    {

        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $this->denyAccessUnlessGranted('read', $tool);
        $requestFilters = $request->query->all();

        $filters = $requestFilters ?: $tool->getFormFilters();

        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool, $filters);

        if ($requestFilters) {
            $toolQueryManager->setFilters($filters);
        }


        $toolQueryManager->initQuery();
        $results = $toolQueryManager->getResults();

        //remove hidden columns from response
        foreach ($results as $i => $result) {
            foreach ($result as $key => $value) {
                foreach ($tool->getStyles() as $style) {
                    if ($style->getColumnName() === $key && $style->getHidden()) {
                        unset($result[$key]);
                        $results[$i] = $result;
                    }
                }
            }
        }

        $errors = $toolQueryManager->getErrors();

        return empty($errors) ? $results : $errors;
    }

    /**
     * @Route("/show-subquery/{toolId}", name="api_tool_show_subquery", requirements={"toolId": "\d+"})
     * @Method({"POST"})
     */

    public function subQueryAction(Request $request, $toolId)
    {
        $em = $this->getDoctrine()->getManager();

        $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $results = false;

        $subQuery = $request->request->get('subquery');
        if ($subQuery) {
            $toolQueryManager = $this->get('tool.query_manager');
            $queryParser = $this->get('tool.query_parser');
            $userInfo = $this->get('user.info');
            $subQuery = $queryParser->parseQuery($subQuery, $userInfo->getInfo(), '{#', '#}');
            $toolQueryManager->setParams($tool);
            $toolQueryManager->initQuery([], $subQuery);
            $results = $toolQueryManager->getResults();
        }

        return new JsonResponse(['results' => $results, 'query' => $subQuery]);

    }


    /**
     * @Route("/user-filter-sets/{toolId}", name="api_user_filter_sets", requirements={"toolId": "\d+"})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     */

    public function userFilterSetsAction(Request $request, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $filterSets = $em->getRepository('ToolBundle:UserFilterSet')->fetchUserFilterSets($toolId, $this->getUser());

        return $this->render(
            'ToolBundle:Default:filter/user-filter-sets.html.twig',
            array(
                'filterSets' => $filterSets,
            )
        );

    }

    /**
     * @Route("/zapisz-zestaw-filtrow/{toolId}", name="api_tool_filter_set_save", requirements={"toolId": "\d+"})
     * @Method("POST")
     */

    public function filterSetSaveAction(Request $request, $toolId = null)
    {

        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $user = $this->getUser();
        $filterSet = new UserFilterSet();


        $filterSetForm = $this->createForm(
            UserFilterSetType::class,
            $filterSet,
            [
                'user' => $user,
            ]
        );

        $filterSetForm->setData($filterSet);
        $filterSetForm->handleRequest($request);

        $filterSet->setTool($tool);
        $filterSet->setUser($user);

        if ($request->isMethod('POST') && $filterSetForm->isValid()) {
            $em->persist($filterSet);
            $em->flush();
        }

        return new JsonResponse('OK');
    }

    /**
     * @Route("/edit-data/{toolId}/{rowId}", name="api_tool_data_editor")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_USER')")
     *
     */
    public function toolDataEditor(Request $request, $toolId, $rowId = null)
    {
        $em = $this->getDoctrine()->getManager();

        $tool = null;
        $info = [];

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $idName = $tool->getEditIdColumn() ?: 'id';
        $redirect = $request->query->get('redirect', null);

        $params = $request->query->get('params', []);

        if (!empty($params)) {
            unset($params['hash']);
        }

        $batch = $request->query->get('batch', false);

        if (!$idName) {
            return new Response('Nie zdefiniowano nazwy kolumny ID do edycji', 200);
        }

        $action = $batch ? 'batchWrite' : 'write';
        $this->denyAccessUnlessGranted($action, $tool);

        $data = [];

        $toolQueryManager = $this->get('tool.query_manager');

        if ($rowId) {
            $rowIds = explode(',', $rowId);

            foreach ($rowIds as $rId) {
                $toolQueryManager->setParams($tool, [$idName => $rId, $idName.'--op' => '='], 1, 0, false, $params);
                $toolQueryManager->initQuery();
                $results[] = $toolQueryManager->getResults();
            }

            if (!empty($results) && $rowId) {
                if ($batch) {
                    $data = [$idName => $rowId];
                } else {

                    $data = !empty($results[0]) ? $results[0][0] : [];
                }
            }
        } else {
            $toolQueryManager->setParams($tool);
        }

        $form = $this->createForm(
            UserToolDataModifyType::class,
            $data,
            [
                'fields' => $tool->getFormFields(),
                'toolQueryManager' => $toolQueryManager,
                'id' => $rowId,
                'batch' => $batch,
                'idName' => $idName,
                'dateParser' => $this->get('tool.date_parser.service'),
                'isDisableTrim' => $tool->isDisableTrim()
            ]
        );

        $form->handleRequest($request);


        if ($request->isMethod('POST') && $form->isValid()) {

            $formData = $form->getData();
            // file upload and document creation

            if ($request->files) {
                foreach ($request->files as $name => $fileInput) {
                    if ($fileInput[0] !== null) {
                        $document = new Document();
                        $documentColumnName = str_replace('__files', '', $name);

                        $document->setName($documentColumnName.'_'.date('Ymd_His'));

                        if ($company = $this->getUser()->getCompany()) {
                            $document->addCompany($company);
                        }

                        foreach ($fileInput as $uploadedFile) {
                            $file = $this->get('document.uploader')->upload($uploadedFile);
                            $file->setDocument($document);
                            $em->persist($file);
                        }

                        $em->persist($document);
                        $em->flush();

                        $formData[$documentColumnName] = $document->getId();
                    }
                }


            }

            $query = $rowId ? $tool->getUpdateQuery() : $tool->getInsertQuery();
            $parser = $this->get('tool.query_parser');


            $data = array_merge($params, $formData);
            $extraData = $form->getExtraData();

            if (array_key_exists($idName, $data) && strpos($data[$idName], ',') !== false) {

                foreach ($results as $result) {
                    $item = $result[0];

                    foreach ($rowIds as $rId) {
                        if ($item[$idName] === $rId) {

                            foreach ($data as $key => $value) {
                                if (array_key_exists('set-'.$key, $extraData)) {
                                    $item[$key] = $data[$key];
                                }
                            }

                            $parsedQuery = $parser->parseQuery($query, $item);
                            $toolQueryManager->executeCustomQuery($parsedQuery);

                            if ($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('ROLE_ANALYST') ) {
                                $this->get('session')->getFlashBag()->add(
                                    'admin-info',
                                    ['info' => $parsedQuery]
                                );
                            }

                            $info[] = $parsedQuery;
                        }
                    }
                }
            } else {
                $parsedQuery = $parser->parseQuery($query, $data);
                $toolQueryManager->executeCustomQuery($parsedQuery);
                $info[] = $parsedQuery;

                if ($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('ROLE_ANALYST') ) {
                    $this->get('session')->getFlashBag()->add(
                        'admin-info',
                        ['info' => $parsedQuery]
                    );
                }

            }

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['info' => $info], 200);
            }

            if ($redirect) {
                return $this->redirect($redirect);
            }
        }


        $template = $request->isXmlHttpRequest() ? 'ToolBundle:Default:data-modify/edit-form.html.twig' :
            'ToolBundle:Default:data-modify/edit.html.twig';

        return $this->render(
            $template,
            [
                'form' => $form->createView(),
                'tool' => $tool,
                'rowId' => $rowId,
                'redirect' => $redirect,
                'batch' => $batch,
                'info' => $info,
                'params' => $params,
            ]
        );

    }

    /**
     * @Route("/tool-data-delete/{toolId}/{rowId}", name="api_tool_data_delete")
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     */
    public function toolDataDeleteAction(Request $request, $toolId, $rowId)
    {
        $tool = $this->getDoctrine()
            ->getRepository('ToolBundle:Tool')
            ->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $this->denyAccessUnlessGranted('write', $tool);

        $params = $request->query->get('params', []);
        $redirect = $request->query->get('redirect', false);

        $deleteQuery = $tool->getDeleteQuery();
        if ($deleteQuery) {
            $toolQueryManager = $this->get('tool.query_manager');
            $toolQueryManager->setParams($tool);

            $parser = $this->get('tool.query_parser');
            $rowIds = explode(',', $rowId);
            foreach ($rowIds as $rowId) {
                $params = array_merge($params, [$tool->getEditIdColumn() => $rowId]);
                $parsedQuery = $parser->parseQuery($deleteQuery, $params);

                $toolQueryManager->executeCustomQuery($parsedQuery);
                $info[] = $parsedQuery;

                if ($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('ROLE_ANALYST') ) {
                    $this->get('session')->getFlashBag()->add(
                        'admin-info',
                        ['info' => $parsedQuery]
                    );
                } else {
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $this->get('translator')->trans(
                            'Rekord o id: %rowId% narzędzia %name% został pomyślnie usunięty',
                            [
                                '%rowId%' => $rowId,
                                '%name%' => $tool->getName(),
                            ]
                        )
                    );
                }

            }
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['info' => $info], 200);
        }

        if ($redirect) {
            return $this->redirect($redirect);
        }


        return $this->redirect($this->generateUrl('tool_data_editor', ['toolId' => $toolId]));

    }

    /**
     * @Route("/tool-action-execute", name="api_tool_action_execute")
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     */
    public function toolActionExecuteAction(Request $request)
    {

        $toolId = $request->query->get('toolId', false);
        $actionId = $request->query->get('actionId', false);
        $rowIds = $request->query->get('rowIds', []);
        $actionName = $request->query->get('actionName', false);
        $query = $request->query->get('actionQuery');
        $tool = $this->getDoctrine()
            ->getRepository('ToolBundle:Tool')
            ->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $action = $this->getDoctrine()
            ->getRepository('ToolBundle:ToolAction')
            ->find($actionId);
        if (!$action) {
            throw $this->createNotFoundException('Nie znaleziono akcji');
        }

        $query = $query != '' ? $query : $action->getQuery();

        $idName = $tool->getEditIdColumn();

        //$this->denyAccessUnlessGranted('write', $tool);

        $params = $request->query->get('params', []);

        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool);

        $parser = $this->get('tool.query_parser');

        if (!empty($rowIds)) {
            foreach ($rowIds as $rowId) {
                $params = array_merge($params, [$idName => $rowId]);
                $parsedQuery = $parser->parseQuery($query, $params);

                $toolQueryManager->executeCustomQuery($parsedQuery);
                $info[] = $parsedQuery;

                if ($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('ROLE_ANALYST') ) {
                    $this->get('session')->getFlashBag()->add(
                        'admin-info',
                        ['info' => $parsedQuery]
                    );
                } else {
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $this->get('translator')->trans('Pomyślnie wykonano akcję').': '.$actionName
                    );
                }

            }
        } else {
            $parsedQuery = $parser->parseQuery($query, $params);

            $toolQueryManager->executeCustomQuery($parsedQuery);
            $info[] = $parsedQuery;

            if ($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('ROLE_ANALYST') ) {
                $this->get('session')->getFlashBag()->add(
                    'admin-info',
                    ['info' => $parsedQuery]
                );
            } else {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans('Pomyślnie wykonano akcję').': '.$actionName
                );
            }
        }

        return new JsonResponse(['info' => $info], 200);

    }
}
