<?php

namespace ToolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TransactionUncommittedController extends Controller
{

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->getDoctrine()->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);
    }

}
