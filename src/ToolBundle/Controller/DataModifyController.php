<?php

namespace ToolBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use ToolBundle\Entity\FormField;
use ToolBundle\Entity\Tool;
use ToolBundle\Form\Type\AdminToolDataModifyType;
use ToolBundle\Form\Type\FormFieldType;
use ToolBundle\Form\Type\ToolAutoGeneratorType;
use ToolBundle\Form\Type\UserToolDataModifyType;

class DataModifyController extends TransactionUncommittedController
{
    /**
     * Uzupełnienie zapytań dla INSERT/UPDATE/DELETE
     * Lista pól formularza edycji danych narzędzia
     *
     * @Route("/{toolId}", name="admin_tool_data_modify", requirements={"toolId": "\d+"})
     * @Method({"GET","POST"})
     */

    public function indexAction(Request $request, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$toolId || !$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $form = $this->createForm(AdminToolDataModifyType::class, $tool);

        $form->setData($tool);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($tool);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Dane narzędzia "%name%" zostały pomyślnie zapisane',
                    ['%name%' => $tool->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_tool_data_modify', array('toolId' => $toolId)));
        }

        $formBlankFields = $this->createForm(
            UserToolDataModifyType::class,
            [],
            [
                'fields' => $tool->getFormFields(),
                'toolQueryManager' => '',
                'id' => '',
                'idName' => $tool->getEditIdColumn(),
                'dateParser' => $this->get('tool.date_parser.service'),
                'isDisableTrim' => $tool->isDisableTrim()
            ]
        );

        list($formAutoGenerator, $notEmpty) = $this->generateFormWithNewColumns($tool);

        return $this->render(
            'ToolBundle:Admin:data-modify/index.html.twig',
            array(
                'tool' => $tool,
                'form' => $form->createView(),
                'formBlankFields' => $formBlankFields->createView(),
                'formAutoGenerator' => $formAutoGenerator->createView(),
                'emptyAutoGenerator' => !$notEmpty
            )
        );
    }


    private function generateFormWithNewColumns(Tool $tool)
    {
        $toolManager = $this->get('tool.query_manager');
        $toolManager->setParams($tool);
        $toolManager->initQuery();

        $columns = $toolManager->getColumns();

        /** @var FormField $formField */
        foreach ($tool->getFormFields() as $formField) {
            if(($key = array_search($formField->getColumnName(), $columns)) !== false) {
                unset($columns[$key]);
            }
        }

        $blankTool = new Tool();


        foreach ($columns as $column) {
            $newFormField = new FormField();
            $newFormField->setColumnName($column);
            $newFormField->translate()->setLabel($column);
            $newFormField->setType('text');

            $blankTool->addFormField($newFormField);
        }

        $formAutoGenerator = $this->createForm(ToolAutoGeneratorType::class, $blankTool);

        return [$formAutoGenerator, (count($columns)) ? true : false];
    }

    /**
     *
     * @Route("/{toolId}/auto-generator", name="admin_tool_auto_generate_form", requirements={"toolId": "\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @param null $toolId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function autoGenerateFormAction(Request $request, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$toolId || !$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        try {
            $autoGeneratorRequest = $request->request->get('atlas_tool_auto_generator', null);

            if($autoGeneratorRequest)
            {
                foreach ($autoGeneratorRequest['formFields'] as $item) {
                    if(isset($item['addIt']) && $item['addIt'])
                    {
                        $newFormField = new FormField();
                        $newFormField->setTool($tool);
                        $newFormField->setColumnName($item['columnName']);
                        $newFormField->setType($item['type']);

                        foreach ($this->getParameter('available_locales') as $language) {
                            $newFormField->translate($language)->setLabel((isset($item['translations'][$language]['label'])) ? $item['translations'][$language]['label'] : '');
                        }

                        if(isset($item['disabled'])) $newFormField->setDisabled(true);
                        if(isset($item['required'])) $newFormField->setRequired(true);
                        if(isset($item['query'])) $newFormField->setQuery($item['query']);

                        $tool->addFormField($newFormField);
                        $em->persist($newFormField);
                        $newFormField->mergeNewTranslations();
                    }
                }
            }

            $em->persist($tool);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('Dodawanie zakończone pomyślnie')
            );
        }
        catch (\Exception $e)
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('Wystąpił problem przy dodawaniu pól')
            );

            return $this->redirect($this->generateUrl('admin_tool_data_modify', array('toolId' => $toolId)));
        }


//        /** Uzupełnia konstruktor formularza */
//        $this->autoCompleteFormBuilder($tool, $em);

        return $this->redirect($this->generateUrl('admin_tool_data_modify', array('toolId' => $toolId)));
    }

    /**
     * @Route("/{toolId}/update-form-builder", options={"expose"=true}, name="admin_tool_form_builder_update", requirements={"toolId": "\d+"})
     * @Method("POST")
     * @param Request $request
     * @param $toolId
     * @return JsonResponse
     * @throws \Exception
     * @internal param $option
     */
    public function updateFormFieldsOfTool(Request $request, $toolId)
    {
        /** Check Ajax Method */
        if($request->isXmlHttpRequest())
        {

            $em = $this->getDoctrine()->getManager();

            if ($toolId) {
                $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
                if (!$tool) {
                    throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
                }
            }
            else {
                return new JsonResponse([], 500);
            }

            $response = new JsonResponse();

            $data = $request->request->get('form_builder');

            $formFields = $tool->getFormFields();

            $found = false;
            /** @var FormField $formField */
            foreach ($formFields as $formField) {

                if($data)
                {
                    foreach($data as $field) {
                        if ($formField->getId() == $field['id']) {
                            $formField->setRowNumber($field['row']);
                            $formField->setColSize($field['col']);
                            $formField->setPositionInRow($field['order']);
                            $formField->setTargetName($field['targetName']);
                            $formField->setTargetValue($field['targetValue']);
                            $formField->setSectionRow($field['sectionRow']);
                            $formField->setCustomClass($field['customClass']);
                            $em->persist($formField);
                            $found = true;
                            break;
                        }
                    }
                }

                if(!$found)
                {
                    $formField->setRowNumber(NULL);
                    $formField->setPositionInRow(NULL);
                    $formField->setSectionRow(NULL);
                    $em->persist($formField);
                }

                $found = false;
            }

            $em->flush();

            $response->setData(['success' => true, 'data' => $data]);
            return $response;
        }

        throw new AccessDeniedException('Only Ajax Method');
    }

    /**
     * @Route("/{toolId}/editor/{id}", name="admin_tool_form_field_editor", requirements={"toolId": "\d+"}, options={"expose"=true})
     * @Method({"GET", "POST"})
     */

    public function formFieldEditorAction(Request $request, $toolId, $id = null)
    {
        $em = $this->getDoctrine()->getManager();

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
            if (!$tool) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
            }
        }

        if ($id) {
            $formField = $em->getRepository('ToolBundle:FormField')->find($id);
            if (!$formField || !$tool->getFormFields()->contains($formField)) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono pola'));
            }
        } else {
            $formField = new FormField();
        }

        $formField->setTool($tool);

        $form = $this->createForm(FormFieldType::class, $formField);

        $form->setData($formField);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($formField);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Pole "%label%" zostało pomyślnie zapisane',
                    ['%label%' => $formField->getLabel()]
                )

            );

            return $this->redirect($this->generateUrl('admin_tool_data_modify', array('toolId' => $toolId)));
        }

        return $this->render(
            'ToolBundle:Admin:data-modify/form-field/editor.html.twig',
            array(
                'form' => $form->createView(),
                'tool' => $tool,
            )
        );

    }

    /**
     * @Route("/delete/{id}", name="admin_tool_form_field_delete")
     * @Method({"GET"})
     */
    public function formFieldDeleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('ToolBundle:FormField')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono pola formularza'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Pole "%label%" zostało pomyślnie usunięte',
                ['%label%' => $entity->getLabel()]
            )
        );

        return $this->redirect($request->headers->get('referer'));

    }

    private function autoCompleteFormBuilder(Tool $tool, ObjectManager $em)
    {
        $nextRow = 1;
        /** @var FormField[] $blankField */
        $blankField = [];

        /** @var FormField $formField */
        foreach ($tool->getFormFields() as $formField) {
            if ($formField->getRowNumber()) {
                $nextRow++;
            } else {
                $blankField[] = $formField;
            }
        }

        foreach ($blankField as $item) {
            $item->setRowNumber($nextRow);
            $item->setPositionInRow(1);
            if (!$item->getColSize()) {
                $item->setColSize(12);
            }
            $em->persist($item);
            $nextRow++;
        }

        $em->flush();
    }

}
