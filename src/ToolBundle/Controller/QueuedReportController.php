<?php

namespace ToolBundle\Controller;


use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use PhpImap\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ToolBundle\Entity\QueuedReport;
use ToolBundle\Entity\Tool;


class QueuedReportController extends TransactionUncommittedController
{

    /**
     * @Rest\Route("/check-report-files/{toolId}", options={"expose"=true}, requirements={"toolId" = "\d+"}, name="checkIfReportGeneratedRecently")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getRecentReportFiles(Request $request, $toolId)
    {

        $recentFound = $this->getDoctrine()->getRepository('ToolBundle:QueuedReport')->findByRecentGeneratedFilesByTool($toolId);


        If (!empty($recentFound)) {

            $queuedReportService = $this->get('tool.queued_report.service');
            $preparedToRender = [];

            /** @var QueuedReport $report */
            foreach ($recentFound as $report) {
                $date = $report->getCreatedAt()->format('Y-m-d H:i');
                $link = $queuedReportService->getFileLink($report);
                $status = $queuedReportService->getStatusText($report);
                $filters = $queuedReportService->getFiltersText($report);
                $type = $queuedReportService->getTypeText($report);
                $preparedToRender[] = ['link' => $link, 'status' => $status,'status_id' =>$report->getStatus(), 'filters' => $filters, 'date' => $date, 'type'=>$type];
            }


            $html = $this->get('twig')->render('@Tool/Default/tool/queued-reports.html.twig',
                [
                    'found' => $preparedToRender
                ]);
            return new JsonResponse(['html' => $html, 'error' => '0'], 200);
        } else {
            $html = 'Nie posiadamy zapisanej wersji tego raportu, wygeneruj nowy';
            return new JsonResponse(['html' => $html, 'error' => '0'], 200);
        }


    }

    /**
     * @Rest\Route("/add-report-to-queue", options={"expose"=true}, name="addReportToQueue")
     * @param Request $request
     * @return JsonResponse
     */
    public function addToQueue(Request $request)
    {

        $toolRepository = $this->getDoctrine()->getRepository('ToolBundle:Tool');

        $toolId = $request->request->get('toolId');
        $filters = $request->request->get('filters') ?? null;

        $tool = $toolRepository->find($toolId);
        $type = intval($request->request->get('type'));

        if ($tool && $type > 0) {
            $service = $this->get('tool.queued_report.service');
            $user = $this->getUser();
//            Add to Queue
            $queuedReport = new QueuedReport();
            $queuedReport->setCreatedBy($user);
            $queuedReport->setTool($tool);
            $queuedReport->setType($type);
            $queuedReport->setFilters($filters);
            $queuedReport->setStatus(1);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($queuedReport);
            $manager->flush();
            $manager->refresh($queuedReport);
            $reportRepository = $this->getDoctrine()->getRepository('ToolBundle:QueuedReport');
            $number = $reportRepository->getNumberInQueue($queuedReport);
            $link = $service->getFileLink($queuedReport);


        }

        return new JsonResponse(['url' => $link, 'number' => $number], 200);
    }

    /**
     * @Rest\Route("/download-ordered-report/{uuid}", options={"expose"=true}, name="download_queued_report")
     * @param $uuid
     * @return  Response
     * @throws NotFoundHttpException
     */
    public function downloadReport($uuid)
    {

        $reportRepository = $this->getDoctrine()->getRepository('ToolBundle:QueuedReport');
        $queuedReport = $reportRepository->findOneBy(['uuid' => $uuid]);

        if (!$queuedReport) {

            throw $this->createNotFoundException();
        } else {
//            Report exist

            if ($queuedReport->getStatus() == 3) {
                return $this->get('tool.queued_report.service')->getFile($queuedReport);
            } else if ($queuedReport->getStatus() == 4) {
                $view = $this->get('twig')->render('@Tool/Default/tool/queued-report-error.html.twig');
                return new Response($view);
//            Error
            } else if ($queuedReport->getStatus() == 1 || $queuedReport->getStatus() == 2) {

                $number = $reportRepository->getNumberInQueue($queuedReport);
                $view = $this->get('twig')->render('@Tool/Default/tool/queued-report-awaiting.html.twig', ['number' => $number]);

                return new Response($view);
            }


        }

    }

}