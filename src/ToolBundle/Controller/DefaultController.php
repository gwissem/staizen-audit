<?php

namespace ToolBundle\Controller;

use AppBundle\Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ToolBundle\Entity\FormField;
use ToolBundle\Entity\Style;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolLog;
use ToolBundle\Entity\UserFilterSet;
use ToolBundle\Form\Type\ToolSummaryType;
use ToolBundle\Form\Type\UserFilterSetType;
use ToolBundle\Form\Type\UserFilterType;
use ToolBundle\Form\Type\UserToolDataModifyType;
use ToolBundle\Repository\FormFieldRepository;
use ToolBundle\Utils\ToolQueryManager;

class DefaultController extends TransactionUncommittedController
{
    const UNLIMITED = 99999999;

    /**
     * @Route("/", name="tool_index")
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     */

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render(
            'ToolBundle:Default:tool/index.html.twig',
            array()
        );

    }

    /**
     * @Route("/{toolId}/{refresh}", name="tool_show", requirements={"toolId": "\d+"}, options={"expose"=true})
     * @Route("/embed/{toolId}/{refresh}", name="tool_show_embed", requirements={"toolId": "\d+"}, options={"expose"=true})
     * @Method({"GET"})
     */

    public function showAction(Request $request, $toolId, $refresh = false)
    {

        $tool = $this->getDoctrine()->getRepository('ToolBundle:Tool')->find($toolId);

        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        // LOGER
        $log = ToolLog::create($this->getUser(), $tool->getId(), 1);
        $startTime = microtime(true);
        // ---

        $actions = $this->getDoctrine()->getRepository('ToolBundle:ToolAction')->fetchUserGroupActions(
            $tool,
            $this->getUser()
        );

        if (!empty($tool->getThirdPartyToolUrl())) {

            return $this->render(
                'ToolBundle:Default:tool/third-party-tool.html.twig',
                [
                    'tool' => $tool,
                ]
            );
        }

        $this->denyAccessUnlessGranted('read', $tool);

        $rowCheckbox = false;
        $showOrdinal = false;
        $authChecker = $this->container->get('security.authorization_checker');
        $totalTableColumns = 0;

        $filtersEncrypter = $this->get('tool.filters_encrypter');
        $requestFilters = $request->query->all();

        if (array_key_exists('tool_title', $requestFilters)) {
            unset($requestFilters['tool_title']);
        }

        $filters = $requestFilters ?: $tool->getFormFilters();

        $hash = $filtersHash = $filtersEncrypter->generateHash($filters);

        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool, $filters);

        $paginationPage = $request->headers->get('Pagination-Page', false);
        $totalRowCount = false;
        $loadedRowCount = 0;
        $totalLoadedRowCount = 0;
        $mainQuery = '';

        /** 257 ms */
        if ($request->isXmlHttpRequest() && $refresh) {

            if (array_key_exists('hash', $requestFilters)) {
                $filtersHash = $requestFilters['hash'];
                unset($requestFilters['hash']);
            }

            $isAdmin = $this->getUser() ? $this->getUser()->hasRole('ROLE_ADMIN') : false;

            if (!$isAdmin && !$filtersEncrypter->isHashCorrect(
                    $requestFilters,
                    $filtersHash
                )
            ) {
                return new Response('', 204);
            }

            if ($requestFilters) {

                /** Wywalenie przekazanych zmiennych w HASHU które nie występują w filtrach i sortowaniu */
                $keysOfFilters = $tool->getKeysOfFilters();
                foreach ($filters as $key => $filter) {
                    if (!in_array($key, $keysOfFilters) && (strpos($key, '--order') === false)) {
                        unset($filters[$key]);
                    }
                }

                $toolQueryManager->setFilters($filters);

            }

            /** 250ms */
//            $toolQueryManager->initQuery()

            $beforeFiltersQuery = $toolQueryManager->getQuery();
            $toolQueryManager->setPagination($paginationPage);
            $table = $toolQueryManager->getTable();

            if ($paginationPage) {
                $mainQuery = $toolQueryManager->getQuery();
                if ($paginationPage == 1) {
                    $totalRowCount = $toolQueryManager->getTotalCount($beforeFiltersQuery);
                }
            }

            $loadedRowCount = count($table['rows']);
            $totalTableColumns = count($table['columns']);
            $totalLoadedRowCount = ($paginationPage - 1) * $toolQueryManager->getLimit() + count($table['rows']);


            $template = 'ToolBundle:Default:tool/partials/tool-table-body.html.twig';

        } else {
            $table = [
//                'columns' => $toolQueryManager->getColumns(true),
                'columns' => $this->getCachedColumns($toolQueryManager),
                'rows' => [],
            ];

            if ($request->get('_route') == 'tool_show_embed') {
                $template = 'ToolBundle:Default:tool/show-embed.html.twig';
            } else {
                $authChecker = $this->container->get('security.authorization_checker');
                if ($authChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                    $template = 'ToolBundle:Default:tool/show.html.twig';
                } else {
                    $template = 'ToolBundle:Default:tool/show-guest.html.twig';
                }
            }

            /** 230 ms */
            $summaryToolForm = $this->createForm(ToolSummaryType::class, ToolSummaryController::createSummaryTool($tool, null, $table));

            $filtersForm = $this->createForm(
                UserFilterType::class,
                null,
                array(
                    'filters' => $tool->getFilters(),
                    'toolQueryManager' => $toolQueryManager,
                    'requestFilters' => array_merge($filters, $requestFilters),
                )
            );

//            $filtersForm->handleRequest($request);

            $filterSetForm = $this->createForm(
                UserFilterSetType::class,
                null,
                [
                    'user' => $this->getUser(),
                ]
            );

        }
        /** --- */

        if ($table['columns']) {
            $rowCheckbox = $tool->getEditIdColumn() && in_array(
                    $tool->getEditIdColumn(),
                    $table['columns']
                ) && ($authChecker->isGranted(
                        'batchWrite',
                        $tool
                    ) || $authChecker->isGranted('useActionRow', $tool));
            $showOrdinal = $tool->getShowOrdinal();
            if ($showOrdinal) {
                $totalTableColumns++;
            }
        }
        if ($rowCheckbox) {
            $totalTableColumns++;
        }
        if ($tool->getSubQuery()) {
            $totalTableColumns++;
        }

        $errors = $toolQueryManager->getErrors();

        if($paginationPage && !empty($mainQuery)) {
            $query = $mainQuery;
        }
        else {
            $query = $toolQueryManager->getQuery();
        }

        $response = $this->renderView(
            $template,
            [
                'tool' => $tool,
                'totalRowCount' => $totalRowCount,
                'table' => $table,
                'errors' => $errors,
                'filtersForm' => (isset($filtersForm)) ? $filtersForm->createView() : null,
                'filterSetForm' => (isset($filterSetForm)) ? $filterSetForm->createView() : null,
                'summaryToolForm' => (isset($summaryToolForm)) ? $summaryToolForm->createView() : null,
                'query' => $query,
                'hash' => $hash,
                'requestFilters' => $requestFilters,
                'actions' => $actions,
                'rowCheckbox' => $rowCheckbox,
                'showOrdinal' => $showOrdinal,
                'ordinalStart' => ($tool->getPaginationLimit()) ? (($paginationPage - 1) * $tool->getPaginationLimit()) : 0,
                'totalTableColumns' => $totalTableColumns,
                'paginationPage' => $paginationPage
            ]
        );

        $this->saveToolLog($log, $startTime);

        if ($paginationPage) {
            return new JsonResponse(
                [
                    'query' => $query,
                    'view' => $response,
                    'loadedRowCount' => $loadedRowCount,
                    'totalLoadedRowCount' => $totalLoadedRowCount,
                    'totalRowCount' => $totalRowCount
                ]
            );
        }

        return new Response($response);
    }

    /**
     * @param ToolQueryManager $toolQueryManager
     * @return array|bool|string
     */
    private function getCachedColumns($toolQueryManager)
    {

        $columns = json_decode($this->get('snc_redis.default')->get('tool_columns_' . $toolQueryManager->getTool()->getId()));

        if (!$columns) {
            $columns = $toolQueryManager->getColumns(true);
            $this->get('snc_redis.default')->set('tool_columns_' . $toolQueryManager->getTool()->getId(), json_encode($columns));
        }

        return $columns;

    }

    /**
     * @Route("/show-subquery/{toolId}", name="tool_show_subquery", requirements={"toolId": "\d+"})
     * @Method({"POST"})
     */

    public function subQueryAction(Request $request, $toolId)
    {
        $em = $this->getDoctrine()->getManager();

        $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);

        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $results = false;
        $subQuery = $tool->getSubQuery();

        if ($subQuery) {

            $params = $request->request->get('params', []);

            $toolQueryManager = $this->get('tool.query_manager');
            $queryParser = $this->get('tool.query_parser');
            $userInfo = $this->get('user.info');

            $subQuery = $queryParser->parseQuery($subQuery, $params);
            $subQuery = $queryParser->parseQuery($subQuery, $userInfo->getInfo(), '{#', '#}');
            $toolQueryManager->setParams($tool);
            $toolQueryManager->initQuery([], $subQuery);

            $results = $toolQueryManager->getResults();

        }

        return new JsonResponse(['results' => $results, 'query' => $subQuery]);

    }

    /**
     * @Route("/user-filter-sets/{toolId}", name="user_filter_sets", requirements={"toolId": "\d+"})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     */

    public function userFilterSetsAction(Request $request, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $filterSets = $em->getRepository('ToolBundle:UserFilterSet')->fetchUserFilterSets($toolId, $this->getUser());

        return $this->render(
            'ToolBundle:Default:filter/user-filter-sets.html.twig',
            array(
                'filterSets' => $filterSets,
            )
        );

    }

    /**
     * @Route("/zapisz-zestaw-filtrow/{toolId}", name="tool_filter_set_save", requirements={"toolId": "\d+"})
     * @Method("POST")
     */

    public function filterSetSaveAction(Request $request, $toolId = null)
    {

        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $user = $this->getUser();
        $filterSet = new UserFilterSet();


        $filterSetForm = $this->createForm(
            UserFilterSetType::class,
            $filterSet,
            [
                'user' => $user,
            ]
        );

        $filterSetForm->setData($filterSet);
        $filterSetForm->handleRequest($request);

        $filterSet->setTool($tool);
        $filterSet->setUser($user);

        if ($request->isMethod('POST') && $filterSetForm->isValid()) {
            $em->persist($filterSet);
            $em->flush();
        }

        return new JsonResponse('OK');
    }

    /**
     * @Route("/edit-data/{toolId}/{rowId}", name="tool_data_editor")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @param $toolId
     * @param null $rowId
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function toolDataEditor(Request $request, $toolId, $rowId = null)
    {
        $em = $this->getDoctrine()->getManager();

        $windowType = $request->query->get('windowType', 'modal');

        /** @var Tool $tool */
        $tool = null;
        $info = [];
        $toolRepository = $em->getRepository('ToolBundle:Tool');

        if ($toolId) {
            $tool = $toolRepository->find($toolId);
        }

        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        // LOGER
        $log = ToolLog::create($this->getUser(), $tool->getId(), 2);
        $startTime = microtime(true);
        // ---

        $formFieldRepository = $em->getRepository('ToolBundle:FormField');
        $idName = $tool->getEditIdColumn() ?: 'id';
        $redirect = $request->query->get('redirect', null);

        $params = $request->query->get('params', []);

        if (!empty($params)) {
            unset($params['hash']);
        }

        $batch = $request->query->get('batch', false);

        if (!$idName) {
            return new Response('Nie zdefiniowano nazwy kolumny ID do edycji', 200);
        }

        $action = $batch ? 'batchWrite' : 'write';
        $this->denyAccessUnlessGranted($action, $tool);

        $data = [];

        $toolQueryManager = $this->get('tool.query_manager');

        if ($rowId) {
            $rowIds = explode(',', $rowId);

            foreach ($rowIds as $rId) {
                $toolQueryManager->setParams($tool, [$idName => $rId, $idName . '--op' => '='], 1, 0, false, $params);
                $toolQueryManager->initQuery();
                $results[] = $toolQueryManager->getResults();
            }

            if (!empty($results) && $rowId) {
                if ($batch) {
                    $data = [$idName => $rowId];
                } else {
                    $data = !empty($results[0]) ? $results[0][0] : [];
                }
            }
        } else {
            $toolQueryManager->setParams($tool);
        }

        $return = [];

        if ($batch === false) {
            $parser = $this->get('tool.query_parser');
            $return['title'] = $parser->parseQuery($tool->getToolEditorTitle(), $data, '{@', '@}', false, "-");
        }

        if ($request->getMethod() == 'POST') {
            $data = $request->request->all();
        }

        if(!empty($rowId) && !$batch && !empty($data)) {
            if(empty($params)) {
                $params = $data;
            }
            else {
                foreach ($data as $key => $datum) {
                    if((!is_array($datum)) &&strlen($datum) > 100) {
                        /** Fix po to, żeby w Action formularza, nie było za wiele dlugich parametrów, bo wtedy URL jest za długi */
                        continue;
                    }
                    $params[$key] = $datum;
                }
            }
        }

        $form = $this->createForm(
            UserToolDataModifyType::class,
            $data,
            [
                'fields' => $formFieldRepository->fetchFormFields($tool),
                'toolQueryManager' => $toolQueryManager,
                'id' => $rowId,
                'wordParser' => $this->get('tool.word_parser'),
                'batch' => $batch,
                'idName' => $idName,
                'dateParser' => $this->get('tool.date_parser.service'),
                'params' => $params,
                'entity_manager' => $this->getDoctrine()->getManager(),
                'query_parser' => $this->get('tool.query_parser'),
                'isDisableTrim' => $tool->isDisableTrim()
            ]
        );

        $form->handleRequest($request);

        $autoSave = $request->query->get('autocomplete-form', 0);
        if ($request->get('relatedField')) {

            $responseParams = [
                'form' => $form->createView(),
                'tool' => $tool,
                'rowId' => $rowId,
                'redirect' => $redirect,
                'batch' => $batch,
                'info' => $info,
                'params' => $params,
            ];
            $return = $this->renderView(
                'ToolBundle:Default:data-modify/edit-form.html.twig',
                $responseParams
            );

            return new Response($return, 200);
        }


        if ($form->isSubmitted()) {
            if ($form->isValid() && ($autoSave || $this->container->get('tool.validator')->isValid($form, $tool))) {
                $formData = $form->getData();
                // file upload and document creation

                if ($request->files) {
                    foreach ($request->files as $name => $fileInput) {

                        if (!empty($fileInput) && $fileInput[0] !== null) {
                            $document = new Document();
                            $document->setCreatedBy($this->getUser());
                            $documentColumnName = str_replace('__files', '', $name);

                            $document->setName($documentColumnName . '_' . date('Ymd_His'));

                            if ($company = $this->getUser()->getCompany()) {
                                $document->addCompany($company);
                            }

                            foreach ($fileInput as $uploadedFile) {
                                $file = $this->get('document.uploader')->upload($uploadedFile);
                                $file->setDocument($document);
                                $em->persist($file);
                            }

                            $em->persist($document);
                            $em->flush();

                            $formData[$documentColumnName] = $document->getId();
                        }
                    }
                }

                $query = $rowId ? $tool->getUpdateQuery() : $tool->getInsertQuery();
                $parser = $this->get('tool.query_parser');

                $data = array_merge($params, $formData);
                $extraData = $form->getExtraData();

                $lastInsertedId = 0;

                if (array_key_exists($idName, $data) && strpos($data[$idName], ',') !== false) {

                    foreach ($results as $result) {
                        $item = $result[0];

                        foreach ($rowIds as $rId) {
                            if ($item[$idName] === $rId) {

                                foreach ($data as $key => $value) {
                                    if (array_key_exists('set-' . $key, $extraData)) {
                                        $item[$key] = $data[$key];
                                    }
                                }

                                $parsedQuery = $parser->parseQuery($query, $item);

                                $execute = $toolQueryManager->executeCustomQuery($parsedQuery);
                                $error = false;
                                if (is_array($execute) && array_key_exists('error', $execute)) {
                                    $error = $execute['error'];
                                }

                                $info[] = $parsedQuery;

                                if ($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('ROLE_ANALYST') && !$error) {
                                    $this->get('session')->getFlashBag()->add(
                                        'admin-info',
                                        ['info' => $parsedQuery]
                                    );
                                }

                            }
                        }
                    }
                } else {

                    if (strpos($query, '@autoSave') !== false) {
                        $data['autoSave'] = $autoSave;
                    }

                    $parsedQuery = $parser->parseQuery($query, $data);

                    $execute = $toolQueryManager->executeCustomQuery($parsedQuery, false, null, false, [], false, $noUse, $lastInsertedId);

                    $error = false;
                    if (is_array($execute) && array_key_exists('error', $execute)) {
                        $error = $execute['error'];
                    }

                    $info[] = $parsedQuery;

                    if ($this->getUser()->hasRole('ROLE_ADMIN') ||  $this->getUser()->hasRole('ROLE_ANALYST') ) {
                        $this->get('session')->getFlashBag()->add(
                            'admin-info',
                            ['info' => $parsedQuery, 'error' => $error]
                        );
                    }

                }

                $this->saveToolLog($log, $startTime);

                if ($request->isXmlHttpRequest()) {
                    return new JsonResponse(['info' => $info, 'error' => $error, 'row_id' => $lastInsertedId, 'tool_id' => $tool->getId()], 200);
                }

                if ($redirect) {
                    return $this->redirect($redirect);
                }
            } else {

                $errors = [];
                foreach ($form->getErrors(true, false) as $error) {
                    $name = $this->get('translator')->trans(/** @Ignore */
                        $error->getForm()->getName());
                    $errors[$name] = $error->current()->getMessage();
                }
                return new JsonResponse(['validation' => $errors], 200);

            }
        }

        $template = $request->isXmlHttpRequest() ? 'ToolBundle:Default:data-modify/edit-form.html.twig' :
            'ToolBundle:Default:data-modify/edit.html.twig';

        $responseParams = [
            'form' => $form->createView(),
            'tool' => $tool,
            'rowId' => $rowId,
            'redirect' => $redirect,
            'batch' => $batch,
            'info' => $info,
            'params' => $params,
        ];

        if ($windowType == 'modal') {
            $return['html'] = $this->renderView(
                $template,
                $responseParams
            );
            return new JsonResponse($return, 200);
        } else {
            return $this->render(
                $template,
                $responseParams
            );
        }
    }

    /**
     * @Route("/tool-data-delete/{toolId}/{rowId}", name="tool_data_delete")
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     */
    public function toolDataDeleteAction(Request $request, $toolId, $rowId)
    {
        $tool = $this->getDoctrine()
            ->getRepository('ToolBundle:Tool')
            ->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $this->denyAccessUnlessGranted('write', $tool);

        $params = $request->query->get('params', []);
        $redirect = $request->query->get('redirect', false);

        $deleteQuery = $tool->getDeleteQuery();
        if ($deleteQuery) {
            $toolQueryManager = $this->get('tool.query_manager');
            $toolQueryManager->setParams($tool);

            $parser = $this->get('tool.query_parser');
            $rowIds = explode(',', $rowId);
            foreach ($rowIds as $rowId) {
                $params = array_merge($params, [$tool->getEditIdColumn() => $rowId]);
                $parsedQuery = $parser->parseQuery($deleteQuery, $params);

                $toolQueryManager->executeCustomQuery($parsedQuery);
                $info[] = $parsedQuery;

                if ($this->getUser()->hasRole('ROLE_ADMIN') ||  $this->getUser()->hasRole('ROLE_ANALYST') ) {
                    $this->get('session')->getFlashBag()->add(
                        'admin-info',
                        ['info' => $parsedQuery]
                    );
                } else {
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $this->get('translator')->trans(
                            'Rekord o id: %rowId% narzędzia %name% został pomyślnie usunięty',
                            [
                                '%rowId%' => $rowId,
                                '%name%' => $tool->getName(),
                            ]
                        )
                    );
                }

            }
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['info' => $info], 200);
        }

        if ($redirect) {
            return $this->redirect($redirect);
        }

        return $this->redirect($this->generateUrl('tool_data_editor', ['toolId' => $toolId]));

    }

    /**
     * @Route("/tool-action-execute", name="tool_action_execute")
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     */
    public function toolActionExecuteAction(Request $request)
    {

        $toolId = $request->query->get('toolId', false);
        $actionId = $request->query->get('actionId', false);
        $rowIds = $request->query->get('rowIds', []);
        $actionName = $request->query->get('actionName', false);
//        $query = $request->query->get('actionQuery');
        $trParams = $request->query->get('trParams', []);
        $params = $request->query->get('params', []);

        if(empty($rowIds) && !empty($trParams)) {
            foreach ($trParams as $key => $trParam) {
                $params[$key] = $trParam;
            }
        }

        $tool = $this->getDoctrine()->getRepository('ToolBundle:Tool')->find($toolId);

        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $action = $this->getDoctrine()->getRepository('ToolBundle:ToolAction')->find($actionId);

        if (!$action) {
            throw $this->createNotFoundException('Nie znaleziono akcji');
        }

        $query = $action->getQuery();

        $idName = $tool->getEditIdColumn();

        //$this->denyAccessUnlessGranted('write', $tool);

        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool);

        $parser = $this->get('tool.query_parser');
        $error = false;

        if (!empty($rowIds)) {
            foreach ($rowIds as $rowId) {
                $params = array_merge($params, [$idName => $rowId]);
                $parsedQuery = $parser->parseQuery($query, $params);

                $info[] = $parsedQuery;
                $execute = $toolQueryManager->executeCustomQuery($parsedQuery);

                if (is_array($execute) && array_key_exists('error', $execute)) {
                    $error = $execute['error'];
                }

                if (!$error) {
                    if ($this->getUser()->hasRole('ROLE_ADMIN')|| $this->getUser()->hasRole('ROLE_ANALYST') ) {
                        $this->get('session')->getFlashBag()->add(
                            'admin-info',
                            ['info' => $parsedQuery]
                        );
                    } else {
                        $this->get('session')->getFlashBag()->add(
                            'success',
                            $this->get('translator')->trans('Pomyślnie wykonano akcję') . ': ' . $actionName
                        );
                    }
                }
            }
        } else {
            $parsedQuery = $parser->parseQuery($query, $params);
            $execute = $toolQueryManager->executeCustomQuery($parsedQuery);

            $info = [$parsedQuery];

            if (is_array($execute) && array_key_exists('error', $execute)) {
                $error = $execute['error'];
            }

            if (!$error) {
                if ($this->getUser()->hasRole('ROLE_ADMIN') ||  $this->getUser()->hasRole('ROLE_ANALYST') ) {
                    $this->get('session')->getFlashBag()->add(
                        'admin-info',
                        ['info' => $parsedQuery]
                    );
                } else {
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $this->get('translator')->trans('Pomyślnie wykonano akcję') . ': ' . $actionName
                    );
                }
            }
        }

        return new JsonResponse(['info' => $info, 'error' => $error], 200);

    }


    /**
     * @Route("/export-data/{toolId}/{type}", options={"expose"=true}, requirements={"toolId" = "\d+"}, name="admin_tools_export_xls")
     * @Method({"GET"})
     * @param Request $request
     * @param $toolId
     * @param $type
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */

    public function toolDataExportAction(Request $request, $toolId, $type = 1 )
    {
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        ini_set('mssql.connect_timeout',-1);
        $em = $this->getDoctrine()->getManager();

        $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        /** Pobieranie odpowiednich danych narzędzia */

        $requestFilters = $request->query->all();
        $filters = $requestFilters ?: $tool->getFormFilters();
        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool, $filters, self::UNLIMITED);
        $table = $toolQueryManager->getTable();

        if($type == 1) {
            return $this->get('tool.data.xlsspout')->generateXlsResponse($tool, $table);
        }else{
            return $this->get('tool.data.xlsspout')->generateCSVResponse($tool, $table);
        }
    }

    /**
     * @Route("/custom-export-get-form/{toolId}", options={"expose"=true}, requirements={"toolId" = "\d+"}, name="tools_custom_export_form")
     * @Method({"GET"})
     * @param Request $request
     * @param $toolId
     * @return Response
     */

    public function toolCustomExportGetFormAction(Request $request, $toolId)
    {

        $em = $this->getDoctrine()->getManager();

        $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        /** Pobieranie odpowiednich danych narzędzia */

        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool, []);
        $columns = $this->getCachedColumns($toolQueryManager);
        $styles = $tool->getStylesArray();

        $availableColumns = [];

        foreach ($columns as $column) {

            $value = $column;
            $label = $column;

            if(array_key_exists($column, $styles)) {

                /** @var Style $firstStyle */
                $firstStyle = $styles[$column][0];

                if($firstStyle->getHidden()) {
                    continue;
                }

                $label = $firstStyle->getAlias();

            }

            $availableColumns[] = [
                'value' => $value,
                'label' => $label
            ];

        }

        return $this->render('@Tool/Default/tool/partials/tool-custom-export-form.html.twig', [
           'available_columns' => $availableColumns
        ]);

    }

    /**
     * @Route("/custom-export-data/{toolId}", options={"expose"=true}, requirements={"toolId" = "\d+"}, name="tools_custom_export")
     * @Method({"POST"})
     * @param Request $request
     * @param $toolId
     * @param $type
     * @return Response|\Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \Box\Spout\Common\Exception\IOException
     */

    public function toolCustomExportDataAction(Request $request, $toolId)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        ini_set('mssql.connect_timeout',-1);
        $em = $this->getDoctrine()->getManager();

        $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        /** Pobieranie odpowiednich danych narzędzia */

        $customExport = $request->request->get('custom_export');

        $availableColumns = $customExport['columns'];
        $format = $customExport['format'];
        $addHeaders = filter_var(((isset($customExport['add_headers'])) ? $customExport['add_headers'] : 0), FILTER_VALIDATE_BOOLEAN);

        $requestFilters = $request->query->all();
        $filters = $requestFilters ?: $tool->getFormFilters();
        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool, $filters, self::UNLIMITED);
        $table = $toolQueryManager->getTable();

        if($format === "txt") {
            return $this->get('tool.data.xlsspout')->generateTxtResponse($tool, $table, $addHeaders, $availableColumns);
        }

        return new Response('');

//        $columns = $request->request->get('columns', 'plain_text');

    }


    /**
     * @Route("/xls/{hash}", options={"expose"=true}, name="tool_xls_view")
     * @Method({"GET"})
     * @param Request $request
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */

    public function toolXlsViewAction(Request $request, $hash)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        ini_set('mssql.connect_timeout',-1);

        $em = $this->getDoctrine()->getManager();

        $toolXlsHash = $em->getRepository('ToolBundle:ToolXlsHash')->find($hash);
        if (!$toolXlsHash) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $tool = $toolXlsHash->getTool();
        $requestFilters = $request->query->all();
        parse_str((string)$toolXlsHash->getFilters(), $filters);

        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool, array_merge($filters, $requestFilters), self::UNLIMITED);

        $table = $toolQueryManager->getTable();
        return $this->get('tool.data.xlsspout')->generateXlsResponse($tool, $table);
    }

    /**
     * @param $log
     * @param $startTime
     */
    private function saveToolLog($log, $startTime) {

        $endTime = microtime(true) - $startTime;
        $log->setTime($endTime);

        $this->getDoctrine()->getManager()->persist($log);
        $this->getDoctrine()->getManager()->flush();

    }

}
