<?php

namespace ToolBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ToolBundle\Entity\Filter;
use ToolBundle\Entity\Tool;
use ToolBundle\Form\Type\AdminFilterType;

class FilterController extends TransactionUncommittedController
{
    /**
     * @Route("/{toolId}", name="admin_tool_filters", requirements={"toolId": "\d+"})
     * @Method({"GET"})
     */

    public function indexAction(Request $request, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$toolId || !$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        return $this->render(
            'ToolBundle:Admin:filter/index.html.twig',
            array(
                'tool' => $tool,
            )
        );

    }

    /**
     * @Route("/{toolId}/editor/{id}", name="admin_tool_filter_editor", requirements={"id": "\d+", "toolId": "\d+"})
     * @Method({"GET", "POST"})
     */

    public function editorAction(Request $request, $id = null, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
            if (!$tool) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
            }
        }

        if ($id) {
            $filter = $em->getRepository('ToolBundle:Filter')->find($id);
            if (!$filter || !$tool->getFilters()->contains($filter)) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono filtru'));
            }
        } else {
            $filter = new Filter();
        }

        $filter->setTool($tool);

        $form = $this->createForm(AdminFilterType::class, $filter);

        $form->setData($filter);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($filter);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Filtr "%name%" został pomyślnie zapisany',
                    ['%name%' => $filter->getLabel()]
                )
            );

            return $this->redirect($this->generateUrl('admin_tool_filters', array('toolId' => $toolId)));
        }

        return $this->render(
            'ToolBundle:Admin:filter/editor.html.twig',
            array(
                'form' => $form->createView(),
                'tool' => $tool,
            )
        );

    }

    /**
     * @Route("/delete/{id}", name="admin_tool_filter_delete")
     * @Method({"GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('ToolBundle:Filter')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono filtru'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Filtr "%name%" został pomyślnie usunięty',
                ['%name%' => $entity->getLabel()]
            )
        );

        return $this->redirect($request->headers->get('referer'));

    }


}
