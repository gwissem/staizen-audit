<?php

namespace ToolBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolSummary;
use ToolBundle\Form\Type\ToolSummaryType;
use ToolBundle\Service\ToolSummaryService;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class ToolSummaryController extends TransactionUncommittedController
{

    /**
     * @Route("/add/{toolId}", name="tool_summary_add", requirements={"toolId": "\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @param null $toolId
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function updateSummaryToolAction(Request $request, $toolId = null)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);

            if (!$tool) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
            }

            $toolSummary = self::createSummaryTool($tool, $this->getUser());

            $form = $this->createForm(ToolSummaryType::class, $toolSummary);
            $form->setData($toolSummary);
            $form->handleRequest($request);

            if ($form->isValid()) {

                $globalSummary = ($form->has('globalSummary')) ? $form->get('globalSummary')->getData() : null;

                $toolSummaryFromDb = null;

                if ($globalSummary && count($form->getData()->getUserGroups()) <= 0) {
                    return new JsonResponse(
                        [
                            'success' => false,
                            'msg' => $this->get('translator')->trans('Przy globalnym podsumowaniu wybierz grupę'),
                        ], 200
                    );
                }

                /** @var ToolSummaryService $toolSummaryService */
                $toolSummaryService = $this->get('tool.summary.service')->setUser($this->getUser());

                if ($globalSummary) {
                    $toolSummaryService->setGroups($toolSummary->getUserGroups());

                    $toolSummary->setUser(null);

                    /** Sprawdzenie czy podsumowanie narzędzia istnieje już w jakiejś grupie */

                    /** @var Group[] $groups */
                    if ($groups = $toolSummaryService->checkExistenceSummaryInGroups($tool, $toolSummary->getFilters(), $toolSummary->getColumnName())) {

                        $groupsName = '';

                        foreach ($groups as $key => $group) {
                            if ($key != 0) $groupsName .= ', ';
                            $groupsName .= $group->getName();
                        }

                        return new JsonResponse(
                            [
                                'success' => false,
                                'msg' => $this->get('translator')->trans('Takie podsumowanie już istnieje w grupach: ') . $groupsName,
                            ], 200
                        );
                    }

                    /** Sprawdza czy identyczne podsumowanie istnieje w bazie, jeżeli tak, to dodaje do niej podaną grupę użytkowników */
                    /** @var ToolSummary $toolSummaryFromDb */
                    $toolSummaryFromDb = $em->getRepository('ToolBundle:ToolSummary')->getSummaryTool(null, $tool, $toolSummary->getFilters(), $toolSummary->getColumnName(), $toolSummary->getDescription());
                } else {
                    /** Sprawdzenie czy podsumowanie narzędzia U USERA już istnieje */
                    if (!$toolSummaryService->checkExistenceSummaryInUser($tool, $toolSummary->getFilters(), $toolSummary->getColumnName())) {
                        return new JsonResponse(
                            [
                                'success' => false,
                                'msg' => $this->get('translator')->trans('Takie podsumowanie już istnieje'),
                            ], 200
                        );
                    }
                }

                $toolQueryManager = $toolSummaryService->generateQuery($tool, $toolSummary->getFilters());

                if ($toolSummaryFromDb && $globalSummary) {

                    foreach ($toolSummary->getUserGroups() as $userGroup) {
                        $toolSummaryFromDb->addUserGroup($userGroup);
                    }

                    /** Przeliczanie wartości dla podsumowania */
                    $toolSummaryService->execToolSummaryValue($toolSummaryFromDb, $toolQueryManager, $toolQueryManager->getResults());

                    /** Zapisanie wartości w Redisie */
                    $toolSummaryService->saveValueInRedis($toolSummaryFromDb);

                    $em->persist($toolSummaryFromDb);
                    $em->flush();

                } else {

                    /** Sprawdzenie czy dana kolumna istnieje w narzędziu */
                    if ($toolSummary->getColumnName() && !in_array($toolSummary->getColumnName(), $toolQueryManager->getColumns())) {
                        return new JsonResponse(
                            ['success' => false, 'msg' => $this->get('translator')->trans('Podana kolumna nie istnieje')],
                            200
                        );
                    }

                    /** Wyliczanie wartości dla podsumowania */
                    $toolSummaryService->execToolSummaryValue($toolSummary, $toolQueryManager, $toolQueryManager->getResults());

                    $toolSummary->setQuery($toolQueryManager->getQuery());
                    $toolSummary->setHash($toolSummary->getTool()->getId() . '_' . md5($toolSummary->getFilters()) . '_' . $toolSummary->getColumnName());

                    /** Zapisanie wartości w Redisie */
                    $toolSummaryService->saveValueInRedis($toolSummary);

                    $em->persist($toolSummary);
                    $em->flush();
                }


                if ($request->isXmlHttpRequest()) {
                    return new JsonResponse(['success' => true], 200);
                }
            }

            return new JsonResponse(['success' => false, 'error' => ($form->getErrors()) ? $form->getErrors()[0]->getMessage() : 'Error'], 500);
        } else {
            throw new AccessDeniedException('Only XmlHttpRequest');
        }
    }

    static function createSummaryTool(Tool $tool = null, User $user = null, $table = null)
    {

        $summaryTool = new ToolSummary();
        $summaryTool->setTool($tool);
        $summaryTool->setUser($user);

        if ($tool && $table) {
            $columns = [];
            $toolStyles = $tool->getStylesArray();
            if (!empty($table['columns'])) {
                foreach ($table['columns'] as $column) {
                    if (isset($toolStyles[$column])) {
                        $columns[$toolStyles[$column][0]->getAlias()] = $column;
                    } else {
                        $columns[$column] = $column;
                    }
                }
            }
            $tool->columnsArray = $columns;
        }

        return $summaryTool;
    }

    /**
     * @Route("/admin/{page}", requirements={"page" = "\d+"}, name="tool_summary_admin_view")
     * @Method({"GET"})
     * @param int $page
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */

    public function adminSummaryViewAction($page = 1)
    {

        $em = $this->getDoctrine()->getManager();

        $summaryQuery = $em->getRepository('ToolBundle:ToolSummary')->getGlobalSummary();

        $perPage = 20;

        $pagination = $this->get('knp_paginator')->paginate($summaryQuery, $page, $perPage);

        return $this->render(
            'ToolBundle:Admin:tool-summary/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );

    }

    /**
     * @Route("/remove/{id}", name="tool_summary_remove", options={"expose"=true}, requirements={"id": "\d+"})
     * @Method({"POST|GET"})
     * @param Request $request
     * @param null $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function removeSummaryToolAction(Request $request, $id = null)
    {
        // SPRAWDZENIE CZY JESTEŚ WŁAŚCICIELEM

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        /** Admin może usunąć każde */
        if ($user->hasRole(User::ROLE_ADMIN)) {
            $toolSummary = $em->getRepository('ToolBundle:ToolSummary')->findOneBy(['id' => $id]);
        } else {
            $toolSummary = $em->getRepository('ToolBundle:ToolSummary')->findOneBy(['id' => $id, 'user' => $this->getUser()]);
        }

        if (!$toolSummary) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono podsumowania lub nie masz praw do niego.'));
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($toolSummary);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => true], 200);
            }
        } catch (\Exception $e) {
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => false, 'msg' => $e->getMessage()], 500);
            } else {
                $url = $this->generateUrl('tool_summary_admin_view');
                return new RedirectResponse($url);
            }
        }

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Podsumowanie zostało pomyślnie usunięte.'
            )
        );

        $url = $this->generateUrl('tool_summary_admin_view');
        return new RedirectResponse($url);
    }

    /**
     * @Route("/get-summary-tools", name="tool_summary_get_view", options={"expose"=true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function ajaxRenderSummaryTools()
    {
        return $this->renderSummaryToolsAction();
    }

    public function renderSummaryToolsAction($user = null)
    {

        $summaryTools = [];
        $enabledSummary = $this->getParameter('enable_summary_tool');

        if($enabledSummary) {
            try {
                $summaryToolService = $this->get('tool.summary.service');
                $summaryToolService->setUser($this->getUser());
                $toolQueryManager = $this->get('tool.query_manager');

                if (!$user) {
                    $user = $this->getUser();
                }

                $summaryTools = $summaryToolService->getSummaryTools($user);

                if ($summaryTools) {
                    $redis = $this->get('snc_redis.default');
                    $em = $this->get('doctrine.orm.entity_manager');

                    /** @var ToolSummary $summaryTool */
                    foreach ($summaryTools as $key => $summaryTool) {

                        try {

                            /** Sprawdzanie czy podsumowanie jest per user */
                            $hash = ($summaryTool->isPerUser()) ? ($user->getId() . '_' . $summaryTool->getHash()) : $summaryTool->getHash();

                            $val = $redis->get($hash);
                            if ($val !== null) {
                                $summaryTool->setValue($val);
                            } else {
                                $summaryToolService->execToolSummaryValue($summaryTool, $toolQueryManager);
                                $summaryToolService->saveValueInRedis($summaryTool, $hash);
                                $em->persist($summaryTool);
                                $em->flush();
                            }

                        }
                        catch (\Exception $e) {

                            unset($summaryTools[$key]);

                        }

                    }

                    $em->flush();
                }
            } catch (\Exception $e) {
                $summaryTools = [];

            }
        }

        $summaryHtml = $this->renderView('@Web/Default/partials/summary-tools.html.twig', array(
            'summaryTools' => $summaryTools
        ));

        return new JsonResponse([
            'status' => true,
            'html' => $summaryHtml
        ]);


//        return $this->render('@Web/Default/partials/summary-tools.html.twig', array(
//            'summaryTools' => $summaryTools
//        ));
    }

}
