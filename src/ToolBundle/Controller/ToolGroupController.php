<?php

namespace ToolBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolGroup;
use ToolBundle\Form\Type\ToolGroupType;

class ToolGroupController extends TransactionUncommittedController
{

    /**
     * @Route("/{page}", requirements={"page" = "\d+"}, name="admin_tool_groups")
     * @Method({"GET"})
     */
    public function indexAction(Request $request, $page = 1)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('ToolBundle:ToolGroup')->fetchUserToolGroupsQuery($request->getLocale());
        $perPage = 20;

        $pagination = $this->get('knp_paginator')->paginate($query, $page, $perPage, [
            'wrap-queries' => true,
            'defaultSortFieldName' => 'tgt.name',
            'defaultSortDirection' => 'asc'
        ]);

        return $this->render(
            'ToolBundle:Admin:tool-group/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @Route("/editor/{id}", name="admin_tool_group_editor", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     */

    public function editorAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();

        if ($id) {
            $entity = $em->getRepository('ToolBundle:ToolGroup')->find($id);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono grupy narzędzi'));
            }
        } else {
            $entity = new ToolGroup();
        }

        $form = $this->createForm(ToolGroupType::class, $entity, [
            'em' => $em,
            'translator' => $this->get('translator')
        ]);

        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Grupa narzędzi "%name%" została pomyślnie utworzona',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_tool_groups'));
        }

        return $this->render(
            'ToolBundle:Admin:tool-group/editor.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }


    /**
     * @Route("/delete/{id}", name="admin_tool_group_delete")
     * @Method({"GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('ToolBundle:ToolGroup')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono grupy narzędzi'));
        }

        /** @var Tool $item */
        foreach ($entity->getTools() as $item) {
            $item->setToolGroup(NULL);
            $em->persist($item);
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Grupa narzędzi "%name%" została pomyślnie usunięta',
                ['%name%' => $entity->getName()]
            )

        );

        return $this->redirect($this->generateUrl('admin_tool_groups'));

    }

}
