<?php

namespace ToolBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ToolBundle\Entity\Style;
use ToolBundle\Entity\Tool;
use ToolBundle\Form\Type\StyleType;

class StyleController extends TransactionUncommittedController
{
    /**
     * @Route("/{toolId}", name="admin_tool_styles", requirements={"toolId": "\d+"})
     * @Method({"GET"})
     */

    public function stylesAction(Request $request, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$toolId || !$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        return $this->render(
            'ToolBundle:Admin:style/index.html.twig',
            array(
                'tool' => $tool,
            )
        );
    }

    /**
     * @Route("/{toolId}/editor/{id}", name="admin_tool_style_editor", requirements={"id": "\d+", "toolId": "\d+"})
     * @Method({"GET", "POST"})
     */

    public function styleEditorAction(Request $request, $id = null, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
            if (!$tool) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
            }
        }

        if ($id) {
            $style = $em->getRepository('ToolBundle:Style')->find($id);
            if (!$style || !$tool->getStyles()->contains($style)) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono stylu'));
            }
        } else {
            $style = new Style();
        }

        $style->setTool($tool);

        $form = $this->createForm(StyleType::class, $style, ['tool' => $tool]);

        $form->setData($style);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($style);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Styl kolumny "%name%" został pomyślnie zapisany',
                    ['%name%' => $style->getColumnName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_tool_styles', array('toolId' => $toolId)));
        }

        return $this->render(
            'ToolBundle:Admin:style/editor.html.twig',
            array(
                'form' => $form->createView(),
                'tool' => $tool,
            )
        );

    }

    /**
     * @Route("/delete/{id}", name="admin_tool_style_delete")
     * @Method({"GET"})
     */
    public function deleteStyleAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('ToolBundle:Style')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono stylu'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Styl kolumny "%name%" został pomyślnie usunięty',
                ['%name%' => $entity->getColumnName()]
            )
        );

        return $this->redirect($request->headers->get('referer'));

    }

}
