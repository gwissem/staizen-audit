<?php

namespace ToolBundle\Controller;

use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use ToolBundle\Entity\Style;
use ToolBundle\Entity\Tool;
use ToolBundle\Form\Type\ToolCopyType;
use ToolBundle\Form\Type\ToolExportType;

class ToolExportController extends TransactionUncommittedController
{

    const COPY = 'copy';
    const EXPORT_JSON = 'export';

    /**
     * @Route("/{toolId}", name="admin_tool_export", requirements={"toolId": "\d+"})
     * @Method({"GET", "POST"})
     */
    public function exportAction(Request $request, $toolId)
    {
        $em = $this->getDoctrine()->getManager();

        if ($toolId) {
            $entity = $em->getRepository('ToolBundle:Tool')->find($toolId);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
            }
        } else {
            $entity = new Tool();
        }

        $form = $this->createForm(ToolExportType::class, null);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $formData = $form->getData();
            $type = $formData['exportType'];

            if ($type == self::COPY) {
                $cloneTool = $this->get('tool.export.service')->cloneTool($entity, $formData);
            } elseif ($type == self::EXPORT_JSON) {

                $em->getFilters()->disable('softdeleteable');

                $serializer = $this->container->get('jms_serializer');
                $serializedTool = $serializer->serialize(
                    $entity,
                    'json',
                    SerializationContext::create()
                        ->enableMaxDepthChecks()
                        //->setSerializeNull(true)
                        ->addExclusionStrategy($this->get('translatable_exclusion_strategy'))
                );

                $em->getFilters()->enable('softdeleteable');

                $response = new Response($serializedTool);

                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    'tool_'.$entity->getUniqueId().'_'.date("Ymd").'.json'
                );

                $response->headers->set('Content-Type', 'application/json; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;

            }

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Eksport zakończony pomyślnie'
                )
            );

            return $this->redirect($this->generateUrl('admin_tool_definition', array('toolId' => $cloneTool->getId())));
        }

        return $this->render(
            'ToolBundle:Admin:tool/export.html.twig',
            array(
                'form' => $form->createView(),
                'tool' => $entity,
            )
        );

    }

    public function exportTest()
    {
//        $answers = $this->entityManager
//            ->getRepository(Answer::class)
//            ->getAnswers($survey, $from, $to);
//        $response = new StreamedResponse();
//        $response->setCallback(function () use ($answers) {
//            $handle = fopen('php://output', 'w+');
//            fputcsv($handle, [
//                $this->translator->trans('dashboard.survey.export.header.survey'),
//                $this->translator->trans('dashboard.survey.export.header.date'),
//                $this->translator->trans('dashboard.survey.export.header.rating'),
//                $this->translator->trans('dashboard.survey.export.header.contexts'),
//                $this->translator->trans('dashboard.survey.export.header.longitude'),
//                $this->translator->trans('dashboard.survey.export.header.latitude'),
//                $this->translator->trans('dashboard.survey.export.header.nps')
//            ], ';');
//            /** @var Answer $answer */
//            foreach ($answers as $answer) {
//                fputcsv($handle, [
//                    $answer->getSurvey()->getName(),
//                    $answer->getCreatedAt()->format('Y-m-d H:i:s'),
//                    $answer->getRating(),
//                    $answer->getStringContexts(),
//                    $answer->getLongitude(),
//                    $answer->getLatitude(),
//                    $answer->getNetPromoterScore()
//                ], ';');
//            }
//            fclose($handle);
//        });
//        $response->setStatusCode(200);
//        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
//        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');
//        return $response;
    }

}
