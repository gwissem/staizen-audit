<?php

namespace ToolBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\DeserializationContext;
use Predis\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use ToolBundle\Entity\Style;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolAction;
use ToolBundle\Entity\ToolPermission;
use ToolBundle\Entity\ToolTranslation;
use ToolBundle\Entity\ToolXlsHash;
use ToolBundle\Form\Type\ToolImportType;
use ToolBundle\Form\Type\ToolType;
use UserBundle\Entity\Group;
use WebBundle\Form\Type\SearcherType;

class ToolController extends TransactionUncommittedController
{
    /**
     * @Route("/{page}", requirements={"page" = "\d+"}, name="admin_tools")
     * @Method({"GET"})
     */
    public function indexAction(Request $request, $page = 1)
    {

        $em = $this->getDoctrine()->getManager();

        $searchForm = $this->createForm(SearcherType::class);
        $searchForm->handleRequest($request);

        $perPage = 20;
        $phrase = $searchForm->get('phrase')->getData();

        $query = $em->getRepository('ToolBundle:Tool')->findAllByPhrase($phrase);
        $pagination = $this->get('knp_paginator')->paginate($query, $page, $perPage);

        $template = ($request->isXmlHttpRequest(
        )) ? 'ToolBundle:Admin:tool/partials/tool-list.html.twig' : 'ToolBundle:Admin:tool/index.html.twig';

        return $this->render(
            $template,
            array(
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView(),
            )
        );

    }

    /**
     * @Route("/show/{toolId}", name="admin_tool_show", requirements={"toolId": "\d+"})
     * @Method({"GET"})
     */

    public function showAction(Request $request, $toolId = null)
    {
        return new RedirectResponse($this->generateUrl('tool_show', ['toolId' => $toolId]));
    }

    /**
     * @Route("/definition/{toolId}", name="admin_tool_definition", requirements={"toolId": "\d+"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param null $toolId
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function editorAction(Request $request, $toolId = null)
    {

        $em = $this->getDoctrine()->getManager();

        if ($toolId) {
            $entity = $em->getRepository('ToolBundle:Tool')->find($toolId);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
            }
        } else {
            $entity = new Tool();
        }

        $form = $this->createForm(ToolType::class, $entity);

        $form->setData($entity);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid() && $this->get('tool.tooltypevalidator')->isValid($form)) {
            if (!empty($entity->getThirdPartyToolUrl())) {
                $entity->setQuery('');
            }
            $em->persist($entity);
            $em->flush();

            $toolId = $entity->getId();

            $usersId = $em->getRepository('UserBundle:User')
                ->findAllUsersIdAccessedToTool($toolId);
            $this->get('web.cached_menu.service')->clearCacheOfUser($usersId);

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Narzędzie "%name%" zostało pomyślnie utworzone',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_tool_definition', array('toolId' => $toolId)));
        }

        return $this->render(
            'ToolBundle:Admin:tool/editor.html.twig',
            array(
                'form' => $form->createView(),
                'tool' => $entity,
            )
        );

    }

    /**
     * @Route("/delete/{id}", name="admin_tool_delete")
     * @Method({"GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('ToolBundle:Tool')
            ->find($id);

        $groups = $entity->getHasGroups();

        foreach ($groups as $group) {
            $em->remove($group);
        }
        $em->flush();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Narzędzie "%name%" zostało pomyślnie usunięte',
                ['%name%' => $entity->getName()]
            )
        );

        $usersId = $em->getRepository('UserBundle:User')
            ->findAllUsersIdAccessedToTool($id);
        $this->get('web.cached_menu.service')->clearCacheOfUser($usersId);

        return $this->redirect($this->generateUrl('admin_tools'));

    }


    /**
     * @Route("/admin_tool_cache_clear/{id}", name="admin_tool_cache_clear",options = { "expose" = true })
     * @Method({"GET"})
     * @param Request $request
     * @param null $id
     * @return JsonResponse
     */
    public function clearCacheAction(Request $request, $id = null){
        /** @var Client $redis */

        if($id)
        {
            $redis = $this->container->get('snc_redis.default');
            $redis->del(['tool_columns_' . $id]);
            return new JsonResponse(
                [
                    'status' => 'ok'
                ]
            );
        }else{
            return new JsonResponse(
                [
                    'status' => 'err'
                ]
            );
        }
    }

    /**
     * @Route("/import-from-json", name="admin_tool_import")
     * @Method({"GET", "POST"})
     */
    public function importAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();


        $form = $this->createForm(ToolImportType::class, null);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $file = $form->get('file')->getData();
            $fileContent = file_get_contents($file->getRealPath());

            $serializer = $this->container->get('jms_serializer');
            $context = new DeserializationContext();
            $context->setAttribute('id', null)
                ->setSerializeNull(true);

            /** @var Tool $importedTool */

            $importedTool = $serializer->deserialize($fileContent, Tool::class, 'json', $context);

            $em->getConnection()->beginTransaction();

            try {

                $tool = $em->getRepository('ToolBundle:Tool')->findOneBy(['uniqueId' => $importedTool->getUniqueId()]);
                $importResult = $this->get('tool.import.service')->import($importedTool, $tool);
                $em->getConnection()->commit();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans(
                        'Import zakończony pomyślnie'
                    )
                );

                return $this->redirect(
                    $this->generateUrl('admin_tool_definition', array('toolId' => $importResult->getId()))
                );

            } catch (\Exception $e) {
                $em->getConnection()->rollBack();
                throw $e;
            }


        }

        return $this->render(
            'ToolBundle:Admin:tool/import.html.twig',
            array(
                'form' => $form->createView(),
            )
        );


    }

    /**
     * @Route("/tool-generate-xls-hash", name="admin_tool_generate_xls_hash", options = { "expose" = true })
     * @Method("POST")
     */
    public function generateXlsHashAction(Request $request)
    {

        if ($request->isXmlHttpRequest()) {
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $url = $request->request->get('url');
            $toolId = $request->request->get('toolId');
            $filters = $request->request->get('filters');

            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);

            if (!$tool) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
            }

            $locale = $request->getLocale();
            $filtersArray = [];
            parse_str($filters, $filtersArray);
            $hash = $this->get('tool.filters_encrypter')->generateHash(array_merge(['toolId' => $toolId, 'locale' => $locale], $filtersArray));
            $toolXlsHash = $em->getRepository('ToolBundle:ToolXlsHash')->find($hash);
            if (!$toolXlsHash) {
                $toolXlsHash = new ToolXlsHash();
                $toolXlsHash->setHash($hash);
                $toolXlsHash->setLocale($locale);
                $toolXlsHash->setUrl($url);
                $toolXlsHash->setTool($tool);
                $toolXlsHash->setFilters($filters);
            }
            $em->persist($toolXlsHash);
            $em->flush();
        }

        return new JsonResponse(
            $this->generateUrl('tool_xls_view', ['hash' => $hash], UrlGeneratorInterface::ABSOLUTE_URL)
        );
    }

}
