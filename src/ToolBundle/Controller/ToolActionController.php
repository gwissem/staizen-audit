<?php

namespace ToolBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolAction;
use ToolBundle\Form\Type\ActionType;

class ToolActionController extends TransactionUncommittedController
{
    /**
     * @Route("/{toolId}", name="admin_tool_actions", requirements={"toolId": "\d+"})
     * @Method({"GET"})
     */

    public function indexToolAction(Request $request, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $tool = null;

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        }
        if (!$toolId || !$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        return $this->render(
            'ToolBundle:Admin:action/index.html.twig',
            array(
                'tool' => $tool,
            )
        );
    }

    /**
     * @Route("/{toolId}/editor/{id}", name="admin_tool_action_editor", requirements={"id": "\d+", "toolId": "\d+"})
     * @Method({"GET", "POST"})
     */

    public function actionEditorToolAction(Request $request, $id = null, $toolId = null)
    {
        $em = $this->getDoctrine()->getManager();

        if ($toolId) {
            $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
            if (!$tool) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
            }
        }

        if ($id) {
            $action = $em->getRepository('ToolBundle:ToolAction')->find($id);
            if (!$action || !$tool->getActions()->contains($action)) {
                throw $this->createNotFoundException('Nie znaleziono akcji');
            }
        } else {
            $action = new ToolAction();
        }

        $action->setTool($tool);

        $form = $this->createForm(ActionType::class, $action);

        $form->setData($action);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($action);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Akcja "%name%" została pomyślnie zapisana',
                    ['%name%' => $action->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_tool_actions', array('toolId' => $toolId)));
        }

        return $this->render(
            'ToolBundle:Admin:action/editor.html.twig',
            array(
                'form' => $form->createView(),
                'tool' => $tool,
                'entity' => $action,
            )
        );

    }

    /**
     * @Route("/delete/{id}", name="admin_tool_action_delete")
     * @Method({"GET"})
     */
    public function deleteActionToolAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('ToolBundle:ToolAction')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono akcji'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Akcja "%name%" została pomyślnie usunięta',
                ['%name%' => $entity->getName()]
            )
        );

        return $this->redirect($request->headers->get('referer'));

    }

}
