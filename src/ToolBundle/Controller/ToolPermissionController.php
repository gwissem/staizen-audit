<?php

namespace ToolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use ToolBundle\Entity\ToolPermission;
use ToolBundle\Entity\ToolUserPermission;
use ToolBundle\Form\Type\ToolPermissionType;
use ToolBundle\Entity\Tool;
use ToolBundle\Repository\ToolUserPermissionRepository;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class ToolPermissionController extends TransactionUncommittedController
{

    /**
     * @Route("{toolId}", name="admin_tool_permissions", requirements={"toolId": "\d+"})
     * @Method({"GET", "POST"})
     */

    public function indexAction(Request $request, $toolId)
    {
        $em = $this->getDoctrine()->getManager();
        $tool = $em->getRepository('ToolBundle:Tool')->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $groupRepository = $em->getRepository('UserBundle:Group');
        $groups = $groupRepository->findAll();

        if ($request->isXmlHttpRequest() && $request->isMethod('POST') && $request->request->get('group')) {

            $toolPermissions = $tool->getPermissions();
            $groups = $request->request->get('group');

            foreach ($groups as $id => $group) {
                $permission = false;

                $read = $group['read'] == 1 ? true : false;
                $write = $group['write'] == 1 ? true : false;
                $batchWrite = $group['batchwrite'] == 1 ? true : false;

                $userGroup = $groupRepository->find($id);
                foreach ($toolPermissions as $toolPermission) {
                    if ($toolPermission->getGroup()->getId() == $id) {
                        $permission = $toolPermission;
                        continue;
                    }
                }
                if (!$permission) {
                    $permission = new ToolPermission();
                    $permission->setTool($tool);
                    $permission->setGroup($userGroup);
                }

                $permission->setCanRead($read);
                $permission->setCanWrite($write);
                $permission->setCanBatchWrite($batchWrite);

                $em->persist($permission);
            }

            $em->flush();

            $usersId = $em->getRepository('UserBundle:User')
                ->findAllUsersIdAccessedToTool($toolId);
            $this->get('web.cached_menu.service')->clearCacheOfUser($usersId);

            return new JsonResponse(
                array('message' => $this->get('translator')->trans('Uprawnienia grupy zostały zaktualizowane'))
            );
        }

        return $this->render(
            'ToolBundle:Admin:tool-permission/index.html.twig',
            array(
                'groups' => $groups,
                'tool' => $tool
            )
        );
    }

    /**
     * @Route("/delete/{id}", name="admin_tool_permission_delete")
     * @Method({"GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getDoctrine()
            ->getRepository('ToolBundle:ToolPermission')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono uprawnień'));
        }

        $toolId = $entity->getTool()->getId();

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('Uprawnienia zostały pomyślnie usunięte')
        );

        return $this->redirect($this->generateUrl('admin_tool_permissions', array('id' => $toolId)));

    }


    /**
     * @Route("/fetchUsersWithToolPermissions/{id}/{type}",
     *     name="fetchUsersWithToolPermissions",
     *     requirements={
     *     "id": "\d+",
     *     "type"="read|write|batch"
     * },
     *   options={"expose":true})
     * @Method({"GET"})
     *
     *
     * @
     */
    public function fetchUsersWithToolPermissions($id, $type)
    {
        $result = $this->getUserToolsPermissionsByType($id, $type);
        $parsedResult = [];

        if ($result) {
            foreach ($result as $userPermission) {
                $parsedResult[] = [
                    'name'=>$userPermission->getUser()->getName(),
                    'id'=>$userPermission->getUser()->getId()
                ];
            }
        }

        $return = new JsonResponse(
            [
                'users' => $parsedResult
            ]
        );
        return $return;
    }

    /**
     * @Route("/save-user-permissions/{id}/{type}", name="saveUserPermissionsForTool",
     *     options={"expose":true}))
     *
     * @Method({"POST"})
     */
    public function saveUserPermissionsForTool(Request $request, $id, $type)
    {

        switch ($type) {
            case 'read':
                $typeSearch = 'CanRead';
                break;
            case 'write':
                $typeSearch = 'CanWrite';
                break;
            case 'batch':
                $typeSearch = 'CanBatchWrite';
                break;
            default:
                $typeSearch = false;
        }

        if ($typeSearch):
            $typeSearch = 'set'.$typeSearch;
            $userList = $request->get('users'); // User array from tool ajax
            $tool = $this->getDoctrine()->getRepository('ToolBundle:Tool')->findOneBy(['id' => $id]);
            $repo = $this->getDoctrine()->getRepository('ToolBundle:ToolUserPermission');
            $em = $this->getDoctrine()->getManager();
            /** @var array $alreadyUsers  - Users in database*/
            $alreadyUsers = $this->getUserToolsPermissionsByType($id, $type);


            /** @var User $alreadyUser */
            foreach ($alreadyUsers as $alreadyUser) { //            Remove Unsetted





                if (is_array($userList)) {
                    if (!in_array($alreadyUser->getId(), $userList)) {


                        $alreadyUser->$typeSearch(0);
                        $em->persist($alreadyUser);
                        $em->flush();

                    }
                } else { //              SET ALL TO 0


                    $alreadyUser->$typeSearch(0);
                    $em->persist($alreadyUser);
                    $em->flush();

                }
            }


            if ($userList) {
                foreach ($userList as $userID) {

//                Find for user and tool
                    $perm = $repo->findOneBy(
                        [
                            'tool' => $tool,
                            'user' => $userID
                        ]
                    );
                    if (!$perm) {//                Create if not exist in perms
                        $perm = new ToolUserPermission();
                        $user = $em->getRepository('UserBundle:User')->find($userID);
                        $perm->setUser($user);
                        $perm->setTool($tool);
                        $perm->setCanBatchWrite(0);
                        $perm->setCanRead(0);
                        $perm->setCanWrite(0);
                    }


                    $perm->$typeSearch(1);
                    $em->persist($perm);
                    $em->flush();
                }
            }

        endif;

        return new JsonResponse(['msg' => 'ok']);
    }


    private function getUserToolsPermissionsByType($id, $type)
    {
        switch ($type) {
            case 'read':
                $typeSearch = 'canRead';
                break;
            case 'write':
                $typeSearch = 'canWrite';
                break;
            case 'batch':
                $typeSearch = 'canBatchWrite';
                break;
            default:
                $typeSearch = false;
        }

        if ($typeSearch) {
            $result = $this->getDoctrine()->getRepository('ToolBundle:ToolUserPermission')->findBy(
                [
                    $typeSearch => 1,
                    'tool' => $id
                ]
            );
        } else {
            $result = [];
        }
        return $result;
    }


}
