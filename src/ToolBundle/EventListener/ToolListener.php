<?php
namespace ToolBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Predis\Client;
use ToolBundle\Entity\Tool;

class ToolListener
{
    private $redis;

    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    public function postUpdate(Tool $tool, LifecycleEventArgs $args)
    {
        $this->redis->del(['tool_columns_' . $tool->getId()]);
    }

}