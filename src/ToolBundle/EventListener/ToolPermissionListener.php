<?php
namespace ToolBundle\EventListener;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use ToolBundle\Entity\ToolPermission;
use WebBundle\Service\CachedMenuService;

class ToolPermissionListener
{
    private $cachedMenuService;

    public function __construct(CachedMenuService $cachedMenuService)
    {
        $this->cachedMenuService = $cachedMenuService;
    }

    public function preUpdate(ToolPermission $permission, PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('canRead')) {
            $this->clearToolMenuCache($permission->getGroup());
        }
    }

    public function clearToolMenuCache($group)
    {

        $userIds = array_unique(
            $group->getUsers()->map(
                function ($entity) {
                    return $entity->getId();
                }
            )->toArray()
        );

        $this->cachedMenuService->clearCacheOfUser($userIds);
    }

}