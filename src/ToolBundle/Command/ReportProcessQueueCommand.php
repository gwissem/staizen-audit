<?php

namespace ToolBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use ToolBundle\Entity\QueuedReport;

class ReportProcessQueueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('report:processQueue')
            ->setDescription('Process Single Report from Queue')->addArgument('uuid',InputArgument::OPTIONAL,'Uuid of report for processing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $uuid =  $input->getArgument('uuid');

        $queryReportService = $this->getContainer()->get('tool.queued_report.service');
        $queueNumber = $queryReportService->getNumberInQueue();
        /** @var $report QueuedReport */

        if($uuid){
            $report = $this->getContainer()->get('doctrine')->getRepository('ToolBundle:QueuedReport')->findOneBy(['uuid' =>$uuid]);
        }  else {
            $report = $queryReportService->getReportForProcessing();
        }
        if ($report) {
            $reportID = $report->getId();
            $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' Odpalono processing raportu z kolejki - ID Raportu: ' . $reportID);


            $status = $queryReportService->processReport($report);

            if($status == 3)
            {
                $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' Wygenerowano raport ' . $reportID);
            }else{
                $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' Status ' . $status);
            }


//            Processing Report

        } else {
            if ($queueNumber > 0) {
                $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' Poprzedni raport w trakcie generowania');
            } else {
                $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' Brak raportów do wygenerowania');
        }

        }


    }

}
