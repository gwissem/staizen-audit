<?php

namespace ToolBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Style
 *
 * @ORM\Table(name="tool_action")
 * @UniqueEntity("uniqueId")
 * @ORM\Entity(repositoryClass="ToolBundle\Repository\ToolActionRepository")
 */
class ToolAction
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    const PLACEMENT_TOP = 'top';
    const PLACEMENT_ROW = 'row';

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\Group", inversedBy="toolActions")
     * @ORM\JoinTable(name="user_group_tool_actions",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $userGroups;
    /**
     * @var integer
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @var integer
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $hasConfirmation;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Serializer\Expose()
     * @var string
     * @ORM\Column(type="string", length=32, unique=true)
     */
    private $uniqueId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    private $query;
    /**
     * @ORM\ManyToOne(targetEntity="Tool", inversedBy="actions", cascade={"persist"})
     * @ORM\JoinColumn(name="tool_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $tool;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default" : true})
     */
    private $forSelectedRows = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="string", nullable=true, options={"default" : "top"})
     */
    private $buttonPlacement = self::PLACEMENT_TOP;

    public function __construct()
    {
        $this->userGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uniqueId = md5(rand().time());
    }

    /**
     * @return boolean
     */
    public function getButtonPlacement()
    {
        return $this->buttonPlacement ?: self::PLACEMENT_TOP;
    }

    /**
     * @param boolean $buttonPlacement
     */
    public function setButtonPlacement($buttonPlacement)
    {
        $this->buttonPlacement = $buttonPlacement ?: self::PLACEMENT_TOP;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->translate()->getName();
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return ToolAction
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return ToolAction
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get query
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set query
     *
     * @param string $query
     *
     * @return ToolAction
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ToolAction
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get tool
     *
     * @return \ToolBundle\Entity\Tool
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * Set tool
     *
     * @param \ToolBundle\Entity\Tool $tool
     *
     * @return ToolAction
     */
    public function setTool(\ToolBundle\Entity\Tool $tool = null)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * Add userGroup
     *
     * @param \UserBundle\Entity\Group $userGroup
     *
     * @return ToolAction
     */
    public function addUserGroup(\UserBundle\Entity\Group $userGroup)
    {
        $this->userGroups[] = $userGroup;

        return $this;
    }

    /**
     * Remove userGroup
     *
     * @param \UserBundle\Entity\Group $userGroup
     */
    public function removeUserGroup(\UserBundle\Entity\Group $userGroup)
    {
        $this->userGroups->removeElement($userGroup);
    }

    /**
     * Get userGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserGroups()
    {
        return $this->userGroups;
    }

    /**
     * Get forSelectedRows
     *
     * @return boolean
     */
    public function getForSelectedRows()
    {
        return $this->forSelectedRows;
    }

    /**
     * Set forSelectedRows
     *
     * @param boolean $forSelectedRows
     *
     * @return ToolAction
     */
    public function setForSelectedRows($forSelectedRows)
    {
        $this->forSelectedRows = $forSelectedRows;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * @param mixed $uniqueId
     * @return ToolAction
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * @return int
     */
    public function getHasConfirmation()
    {
        return $this->hasConfirmation;
    }

    /**
     * @param int $hasConfirmation
     * @return ToolAction
     */
    public function setHasConfirmation($hasConfirmation)
    {
        $this->hasConfirmation = $hasConfirmation;

        return $this;
    }

    /**
     * @return int
     */
    public function getConfirmationDescription()
    {
        return ucfirst($this->translate()->getConfirmationDescription());
    }

    public function __clone()
    {
        $this->id = null;
        $this->uniqueId = md5(rand().time());

        foreach ($this->getTranslations() as $translation) {
            $cloneTranslation = clone $translation;
            $this->translations->add($cloneTranslation);
            $cloneTranslation->setTranslatable($this);
        }
    }
}
