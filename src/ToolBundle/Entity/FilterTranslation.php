<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table(name="filter_translation")
 * @ORM\Entity()
 */
class FilterTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return FilterTranslation
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

}
