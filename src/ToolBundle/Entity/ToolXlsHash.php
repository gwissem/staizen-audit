<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="tool_xls_hash")
 * @ORM\Entity()
 */
class ToolXlsHash
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @var integer
     * @ORM\Column(type="string", length=32)
     */
    protected $hash;

    /**
     * @var integer
     * @ORM\Column(name="url", type="text")
     */
    protected $url;
    /**
     * @var integer
     * @ORM\Column(name="`filters`", type="text", nullable=true)
     */
    protected $filters;
    /**
     * @var integer
     * @ORM\Column(type="string", length=2)
     */
    protected $locale;
    /**
     * @ORM\ManyToOne(targetEntity="Tool")
     * @ORM\JoinColumn(name="tool_id", referencedColumnName="id")
     */
    private $tool;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ToolXlsHash
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param int $url
     * @return ToolXlsHash
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return int
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param int $locale
     * @return ToolXlsHash
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return int
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param int $hash
     * @return ToolXlsHash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * @param mixed $tool
     * @return ToolXlsHash
     */
    public function setTool($tool)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * @return int
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param int $filters
     * @return ToolXlsHash
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;

        return $this;
    }

}
