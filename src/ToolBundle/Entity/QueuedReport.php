<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use DocumentBundle\Entity\File;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use MapBundle\Controller\FindBySmsController;

/**
 * QueuedReport
 *
 * @ORM\Table(name="queued_report")
 * @ORM\Entity(repositoryClass="ToolBundle\Repository\QueuedReportRepository")
 */
class QueuedReport
{
    use ORMBehaviors\Timestampable\Timestampable;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="uuid", type="uniqueidentifier")
     * @var string
     */
    private $uuid;


    /**
     * @ManyToOne(targetEntity="ToolBundle\Entity\Tool")
     * @JoinColumn(name="tool_id", referencedColumnName="id", fieldName="tool_id")
     */
    private $tool;


    /**
     * @ORM\Column(name="type", type="integer")
     * Default XLSX
     */
    private $type = 1;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;


    /**
     * @ORM\ManyToOne(targetEntity="DocumentBundle\Entity\File")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $file;


    /**
     * @var
     * @ORM\Column(name="filters",type="text")
     */
    private $filters;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return QueuedReport
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set tool
     *
     * @param integer $tool
     *
     * @return QueuedReport
     */
    public function setTool($tool)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * Get tool
     *
     * @return int
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }


    /**
     * Set status
     *
     * @param integer $status
     *
     * @return QueuedReport
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param mixed $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }




    /**
     * File constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->uuid = FindBySmsController::gen_uuid();

    }
}

