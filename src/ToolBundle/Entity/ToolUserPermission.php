<?php
namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="ToolBundle\Repository\ToolUserPermissionRepository")
 * @ORM\Table(name="tool_permission_user")
 */
class ToolUserPermission
{

    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $canRead = true;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $canWrite = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $canBatchWrite = false;

    /**
     * @ORM\ManyToOne(targetEntity="ToolBundle\Entity\Tool", inversedBy="permissions")
     * @ORM\JoinColumn(name="tool_id", referencedColumnName="id", nullable=false)
     */
    private $tool;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="toolPermissions", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get canRead
     *
     * @return boolean
     */
    public function getCanRead()
    {
        return $this->canRead;
    }

    /**
     * Set canRead
     *
     * @param boolean $canRead
     *
     * @return ToolPermission
     */
    public function setCanRead($canRead)
    {
        $this->canRead = $canRead;

        return $this;
    }

    /**
     * Get canWrite
     *
     * @return boolean
     */
    public function getCanWrite()
    {
        return $this->canWrite;
    }

    /**
     * Set canWrite
     *
     * @param boolean $canWrite
     *
     * @return ToolPermission
     */
    public function setCanWrite($canWrite)
    {
        $this->canWrite = $canWrite;

        return $this;
    }

    /**
     * Get canBatchWrite
     *
     * @return boolean
     */
    public function getCanBatchWrite()
    {
        return $this->canBatchWrite;
    }

    /**
     * Set canBatchWrite
     *
     * @param boolean $canBatchWrite
     *
     * @return ToolPermission
     */
    public function setCanBatchWrite($canBatchWrite)
    {
        $this->canBatchWrite = $canBatchWrite;

        return $this;
    }

    /**
     * Get tool
     *
     * @return \ToolBundle\Entity\Tool
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * Set tool
     *
     * @param \ToolBundle\Entity\Tool $tool
     *
     * @return ToolPermission
     */
    public function setTool(\ToolBundle\Entity\Tool $tool = null)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \UserBundle\Entity\User|null $user
     * @return $this
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function __clone()
    {
        $this->id = null;
    }
}
