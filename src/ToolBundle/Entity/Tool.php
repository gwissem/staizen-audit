<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table(name="tool")
 * @UniqueEntity("uniqueId")
 * @ORM\Entity(repositoryClass="ToolBundle\Repository\ToolRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Serializer\ExclusionPolicy("none")
 */
class Tool
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    const EDIT_TYPE_MODAL = 'modal';
    const EDIT_TYPE_PAGE = 'page';
    const EDIT_TYPE_NEW_TAB = 'new-tab';

    /**
     * @Serializer\Exclude()
     */
    public $isSelected = false;

    /**
     * @Serializer\Exclude()
     */
    public $columnsArray;
    public $overrideColWidth = [];
    /**
     * @Serializer\Expose()
     * @Serializer\MaxDepth(4)
     * @ORM\OneToMany(targetEntity="ToolBundle\Entity\UserGroupTool", mappedBy="tool",cascade={"persist","remove"} )
     */
    protected $hasGroups;
    /**
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $columnForAutoComplete;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @Serializer\Expose()
     * @var string
     * @ORM\Column(type="string", length=32, unique=true)
     */
    private $uniqueId;
    /**
     * @Serializer\Expose()
     * @var string
     * @ORM\Column(type="text")
     */
    private $query;
    /**
     * @Serializer\Expose()
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $subQuery;
    /**
     * @Serializer\Expose()
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $insertQuery;
    /**
     * @Serializer\Expose()
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $updateQuery;
    /**
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(type="text", nullable=true)
     */
    private $deleteQuery;
    /**
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $connectionString;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $allowCount = false;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $allowSearch = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $liveReport = true;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $allowSummary = false;
    /**
     *
     * @Serializer\MaxDepth(4)
     * @ORM\OneToMany(targetEntity="Style", mappedBy="tool", cascade={"persist"}, orphanRemoval=true)
     */
    private $styles;
    /**
     * @Serializer\Expose()
     * @Serializer\MaxDepth(4)
     * @ORM\OneToMany(targetEntity="Filter", mappedBy="tool", cascade={"persist"}, orphanRemoval=true)
     */
    private $filters;
    /**
     * @Serializer\Expose()
     * @Serializer\MaxDepth(4)
     * @ORM\OneToMany(targetEntity="ToolBundle\Entity\ToolAction", mappedBy="tool", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $actions;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $summaryColumns;
    /**
     * @Serializer\Expose()
     * @Serializer\MaxDepth(4)
     * @ORM\OneToMany(targetEntity="ToolBundle\Entity\ToolPermission", mappedBy="tool", cascade={"persist"})
     */
    private $permissions;
    /**
     * @Serializer\Expose()
     * @Serializer\MaxDepth(4)
     * @ORM\OneToMany(targetEntity="ToolBundle\Entity\ToolUserPermission", mappedBy="tool", cascade={"persist"})
     */
    private $userPermissions;
    /**
     * @Serializer\Expose()
     * @Serializer\MaxDepth(4)
     * @ORM\ManyToOne(targetEntity="ToolBundle\Entity\ToolGroup", inversedBy="tools")
     * @ORM\JoinColumn(name="tool_group_id", referencedColumnName="id")
     */
    private $toolGroup;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $permissionTransferAvailability = true;
    /**
     * @Serializer\Expose()
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;
    /**
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;
    /**
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;
    /**
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ordering;
    /**
     * @Serializer\MaxDepth(4)
     * @ORM\OneToMany(targetEntity="FormField", mappedBy="tool", cascade={"persist"}, orphanRemoval=true)
     */
    private $formFields;
    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $targetBlank = false;
    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allowForGuest = false;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thirdPartyToolUrl;
    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $showOrdinal = false;
    /**
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $editIdColumn;
    /**
     * @var integer
     * @Serializer\Expose()
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paginationLimit;
    /**
     * @var integer
     * @Serializer\Expose()
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $count = false;
    /**
     * @var integer

     * @Serializer\Expose()
     * @ORM\Column(type="integer", nullable=true)
     */
    private $heightOfRow;
    /**
     * @var string
     * @Serializer\Expose()
     * @ORM\Column(type="string", length=255, nullable=true, options={"default" : "modal"})
     */
    private $editType = self::EDIT_TYPE_MODAL;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $toolEditorTitle;

    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @ORM\Column(type="boolean", nullable=true, options={"default" : false})
     */
    private $disableTrim = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->styles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->filters = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hasGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->formFields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uniqueId = md5(rand().time());
        $this->columnsArray = array();
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    public function getName()
    {
        return $this->translate()->getName();
    }

    public function getDefaultColor()
    {
        return '#67809F';
    }

    public function getDefaultIcon()
    {
        return  '<i class="fa fa-cog"></i>';
    }

    public function setOverrideColWidth($arr) {

        $this->overrideColWidth = $arr;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Add style
     *
     * @param \ToolBundle\Entity\Style $style
     *
     * @return Tool
     */
    public function addStyle(\ToolBundle\Entity\Style $style)
    {
        $this->styles[] = $style;
        $style->setTool($this);
        return $this;
    }

    /**
     * Remove style
     *
     * @param \ToolBundle\Entity\Style $style
     */
    public function removeStyle(\ToolBundle\Entity\Style $style)
    {
        $this->styles->removeElement($style);
    }

    /**
     * Get styles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStyles()
    {
        return $this->styles;
    }

    /**
     * Get styles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStylesArray()
    {
        $result = [];
        foreach ($this->styles as $style) {
            $result[$style->getColumnName()][] = $style;
        }

        return $result;
    }

    /**
     * Add filter
     *
     * @param \ToolBundle\Entity\Filter $filter
     *
     * @return Tool
     */
    public function addFilter(\ToolBundle\Entity\Filter $filter)
    {
        $this->filters[] = $filter;
        $filter->setTool($this);
        return $this;
    }

    /**
     * Remove filter
     *
     * @param \ToolBundle\Entity\Filter $filter
     */
    public function removeFilter(\ToolBundle\Entity\Filter $filter)
    {
        $this->filters->removeElement($filter);
    }

    /**
     * Add permission
     *
     * @param \ToolBundle\Entity\ToolPermission $permission
     *
     * @return Tool
     */
    public function addPermission(\ToolBundle\Entity\ToolPermission $permission)
    {
        $this->permissions[] = $permission;
        $permission->setTool($this);
        return $this;
    }

    /**
     * Remove permission
     *
     * @param \ToolBundle\Entity\ToolPermission $permission
     */
    public function removePermission(\ToolBundle\Entity\ToolPermission $permission)
    {
        $this->permissions->removeElement($permission);
    }

    /**
     * Get permissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
    /**
     * Add permission
     *
     * @param \ToolBundle\Entity\ToolUserPermission $permission
     *
     * @return Tool
     */
    public function addUserPermission(\ToolBundle\Entity\ToolUserPermission $permission)
    {
        $this->permissions[] = $permission;
        $permission->setTool($this);
        return $this;
    }

    /**
     * Remove permission
     *
     * @param \ToolBundle\Entity\ToolUserPermission $permission
     */
    public function removeUserPermission(\ToolBundle\Entity\ToolUserPermission $permission)
    {
        $this->permissions->removeElement($permission);
    }

    /**
     * Get permissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserPermissions()
    {
        return $this->userPermissions;
    }



    /**
     * Get toolGroup
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getToolGroup()
    {
        return $this->toolGroup;
    }

    /**
     * Set toolGroup
     *
     * @param \ToolBundle\Entity\ToolGroup $toolGroup
     *
     * @return Tool
     */
    public function setToolGroup(\ToolBundle\Entity\ToolGroup $toolGroup = null)
    {
        $this->toolGroup = $toolGroup;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Tool
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasGroups()
    {
        return $this->hasGroups;
    }

    /**
     * @param mixed $hasGroups
     */
    public function setHasGroups($hasGroups)
    {
        $this->hasGroups = $hasGroups;
    }

    public function getFormFilters()
    {
        $result = [];
        foreach ($this->getFilters() as $filter) {
            $defaultValue = $filter->getDefaultValue();
            $result[$filter->getColumnName().'--op'] = $filter->getDefaultOperator();
            if ($filter->getType() == Filter::TYPE_MULTISELECT) {
                $defaultValue = explode(',', preg_replace('/\s*([\s,])\s*/', '\1', $filter->getDefaultValue()));
            }
            $result[$filter->getColumnName()] = $defaultValue;
            if ($filter->getType() == 'dates') {
                $result[$filter->getColumnName().'--to--op'] = $filter->getSecondDefaultOperator();
                $result[$filter->getColumnName().'--to'] = $filter->getSecondDefaultValue();
            }
        }

        return $result;
    }

    /**
     * Get filters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilters()
    {
        return $this->filters;
    }

    public function getKeysOfFilters() {

        $result = [];

        foreach ($this->getFilters() as $filter) {

            $columnName = $filter->getColumnName();

            $result[] = $columnName;
            $result[] = $columnName . '--op';
            $result[] = $columnName . '--order';

            if ($filter->getType() == 'dates') {
                $result[] = $columnName . '--to--op';
                $result[] = $columnName . '--to';
            }

        }

        return $result;
    }

    public function getFiltersOfColumn($columnName) {

        $filters = $this->getFilters()->filter(function ($item) use ($columnName) {
             if($item instanceof Filter) {
                 return $item->getColumnName() === $columnName;
             }
             return false;
        });

        return $filters;
    }

    public function getFiltersOfHiddenColumn($columns) {

        $filters = [];

        $hiddenStyles = $this->getStyles()->filter(function ($style) use ($columns) {
            if($style instanceof Style) {
                return ($style->getHidden() && in_array($style->getColumnName(), $columns));
            }
            return false;
        });

        if(!empty($hiddenStyles)) {

            $columnsName = [];

            /** @var Style $style */
            foreach ($hiddenStyles as $style) {
                $columnsName[] = $style->getColumnName();
            }

            $filters = $this->getFilters()->filter(function ($item) use ($columnsName) {
                if($item instanceof Filter) {
                    return (in_array($item->getColumnName(), $columnsName));
                }
                return false;
            });

        }

        return $filters;

    }

    /**
     * Add hasGroup
     *
     * @param \ToolBundle\Entity\UserGroupTool $hasGroup
     *
     * @return Tool
     */
    public function addHasGroup(\ToolBundle\Entity\UserGroupTool $hasGroup)
    {
        $this->hasGroups[] = $hasGroup;
        $hasGroup->setTool($this);
        return $this;
    }

    /**
     * Remove hasGroup
     *
     * @param \ToolBundle\Entity\UserGroupTool $hasGroup
     */
    public function removeHasGroup(\ToolBundle\Entity\UserGroupTool $hasGroup)
    {
        $this->hasGroups->removeElement($hasGroup);
    }

    /**
     * Add formField
     *
     * @param \ToolBundle\Entity\FormField $formField
     *
     * @return Tool
     */
    public function addFormField(\ToolBundle\Entity\FormField $formField)
    {
        $this->formFields[] = $formField;
        $formField->setTool($this);
        return $this;
    }

    /**
     * Remove formField
     *
     * @param \ToolBundle\Entity\FormField $formField
     */
    public function removeFormField(\ToolBundle\Entity\FormField $formField)
    {
        $this->formFields->removeElement($formField);
    }

    /**
     * Get formFields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFormFields()
    {
        return $this->formFields;
    }

    /**
     * @return boolean
     */
    public function isTargetBlank()
    {
        return $this->targetBlank;
    }

    /**
     * Get targetBlank
     *
     * @return boolean
     */
    public function getTargetBlank()
    {
        return $this->targetBlank;
    }

    /**
     * @param boolean $targetBlank
     */
    public function setTargetBlank($targetBlank)
    {
        $this->targetBlank = $targetBlank;
    }

    /**
     * @return bool
     */
    public function isLiveReport()
    {
        return $this->liveReport;
    }

    /**
     * @param bool $liveReport
     */
    public function setLiveReport($liveReport)
    {
        $this->liveReport = $liveReport;
    }



    /**
     * Add action
     *
     * @param \ToolBundle\Entity\ToolAction $action
     *
     * @return Tool
     */
    public function addAction(\ToolBundle\Entity\ToolAction $action)
    {
        $this->actions[] = $action;
        $action->setTool($this);
        return $this;
    }

    /**
     * Remove action
     *
     * @param \ToolBundle\Entity\ToolAction $action
     */
    public function removeAction(\ToolBundle\Entity\ToolAction $action)
    {
        $this->actions->removeElement($action);
    }

    /**
     * Get actions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActions()
    {
        return $this->actions;
    }

    public function getDescription()
    {
        return $this->translate()->getDescription();
    }

    public function getSubQueryDescription()
    {
        return $this->translate()->getSubQueryDescription();
    }

    /**
     * @return string
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * @param string $uniqueId
     * @return Tool
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    public function updateValues(Tool $importedTool)
    {
        $this->setAllowCount($importedTool->getAllowCount());
        $this->setAllowForGuest($importedTool->getAllowForGuest());
        $this->setShowOrdinal($importedTool->getShowOrdinal());
        $this->setAllowSearch($importedTool->getAllowSearch());
        $this->setAllowSummary($importedTool->getAllowSummary());
        $this->setQuery($importedTool->getQuery());
        $this->setSubQuery($importedTool->getSubQuery());
        $this->setInsertQuery($importedTool->getInsertQuery());
        $this->setUpdateQuery($importedTool->getUpdateQuery());
        $this->setDeleteQuery($importedTool->getDeleteQuery());
        $this->setConnectionString($importedTool->getConnectionString());
        $this->setSummaryColumns($importedTool->getSummaryColumns());
        $this->setPermissionTransferAvailability($importedTool->getPermissionTransferAvailability());
        $this->setColor($importedTool->getColor());
        $this->setIcon($importedTool->getIcon());
        $this->setOrdering($importedTool->getOrdering());
        $this->setOrdering($importedTool->getOrdering());
        $this->setTargetBlank($importedTool->getTargetBlank());
        $this->setEditIdColumn($importedTool->getEditIdColumn());
        $this->setEditType($importedTool->getEditType());

    }

    /**
     * Get allowCount
     *
     * @return boolean
     */
    public function getAllowCount()
    {
        return $this->allowCount;
    }

    /**
     * Set allowCount
     *
     * @param boolean $allowCount
     *
     * @return Tool
     */
    public function setAllowCount($allowCount)
    {
        $this->allowCount = $allowCount;

        return $this;
    }

    /**
     * Get allowForGuest
     *
     * @return boolean
     */
    public function getAllowForGuest()
    {
        return $this->allowForGuest;
    }

    /**
     * Set allowForGuest
     *
     * @param boolean $allowForGuest
     *
     * @return Tool
     */
    public function setAllowForGuest($allowForGuest)
    {
        $this->allowForGuest = $allowForGuest;

        return $this;
    }

    /**
     * @return string
     */
    public function getThirdPartyToolUrl()
    {
        return $this->thirdPartyToolUrl;
    }

    /**
     * @param string $thirdPartyToolUrl
     */
    public function setThirdPartyToolUrl(string $thirdPartyToolUrl)
    {
        $this->thirdPartyToolUrl = $thirdPartyToolUrl;
    }

    /**
     * @return bool
     */
    public function getShowOrdinal()
    {
        return $this->showOrdinal;
    }

    /**
     * @param bool $showOrdinal
     */
    public function setShowOrdinal($showOrdinal)
    {
        $this->showOrdinal = $showOrdinal;

        return $this;
    }


    /**
     * Get allowSearch
     *
     * @return boolean
     */
    public function getAllowSearch()
    {
        return $this->allowSearch;
    }

    /**
     * Set allowSearch
     *
     * @param boolean $allowSearch
     *
     * @return Tool
     */
    public function setAllowSearch($allowSearch)
    {
        $this->allowSearch = $allowSearch;

        return $this;
    }

    /**
     * Get allowSummary
     *
     * @return boolean
     */
    public function getAllowSummary()
    {
        return $this->allowSummary;
    }

    /**
     * Set allowSummary
     *
     * @param boolean $allowSummary
     *
     * @return Tool
     */
    public function setAllowSummary($allowSummary)
    {
        $this->allowSummary = $allowSummary;

        return $this;
    }

    /**
     * Get query
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set query
     *
     * @param string $query
     *
     * @return Tool
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get subQuery
     *
     * @return string
     */
    public function getSubQuery()
    {
        return $this->subQuery;
    }

    /**
     * Set subQuery
     *
     * @param string $subQuery
     *
     * @return Tool
     */
    public function setSubQuery($subQuery)
    {
        $this->subQuery = $subQuery;

        return $this;
    }

    /**
     * Get insertQuery
     *
     * @return string
     */
    public function getInsertQuery()
    {
        return $this->insertQuery;
    }

    /**
     * Set insertQuery
     *
     * @param string $insertQuery
     *
     * @return Tool
     */
    public function setInsertQuery($insertQuery)
    {
        $this->insertQuery = $insertQuery;

        return $this;
    }

    /**
     * Get updateQuery
     *
     * @return string
     */
    public function getUpdateQuery()
    {
        return $this->updateQuery;
    }

    /**
     * Set updateQuery
     *
     * @param string $updateQuery
     *
     * @return Tool
     */
    public function setUpdateQuery($updateQuery)
    {
        $this->updateQuery = $updateQuery;

        return $this;
    }

    /**
     * Get deleteQuery
     *
     * @return string
     */
    public function getDeleteQuery()
    {
        return $this->deleteQuery;
    }

    /**
     * Set deleteQuery
     *
     * @param string $deleteQuery
     *
     * @return Tool
     */
    public function setDeleteQuery($deleteQuery)
    {
        $this->deleteQuery = $deleteQuery;

        return $this;
    }

    /**
     * Get connectionString
     *
     * @return string
     */
    public function getConnectionString()
    {
        return $this->connectionString;
    }

    /**
     * Set connectionString
     *
     * @param string $connectionString
     *
     * @return Tool
     */
    public function setConnectionString($connectionString)
    {
        $this->connectionString = $connectionString;

        return $this;
    }

    /**
     * Get summaryColumns
     *
     * @return string
     */
    public function getSummaryColumns()
    {
        return $this->summaryColumns;
    }

    /**
     * Set summaryColumns
     *
     * @param string $summaryColumns
     *
     * @return Tool
     */
    public function setSummaryColumns($summaryColumns)
    {
        $this->summaryColumns = $summaryColumns;

        return $this;
    }

    /**
     * Get permissionTransferAvailability
     *
     * @return boolean
     */
    public function getPermissionTransferAvailability()
    {
        return $this->permissionTransferAvailability;
    }

    /**
     * Set permissionTransferAvailability
     *
     * @param boolean $permissionTransferAvailability
     *
     * @return Tool
     */
    public function setPermissionTransferAvailability($permissionTransferAvailability)
    {
        $this->permissionTransferAvailability = $permissionTransferAvailability;

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * Get ordering
     *
     * @return string
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set ordering
     *
     * @param string $ordering
     *
     * @return Tool
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get editIdColumn
     *
     * @return string
     */
    public function getEditIdColumn()
    {
        return $this->editIdColumn;
    }

    /**
     * Set editIdColumn
     *
     * @param string $editIdColumn
     *
     * @return Tool
     */
    public function setEditIdColumn($editIdColumn)
    {
        $this->editIdColumn = $editIdColumn;

        return $this;
    }

    /**
     * Get editType
     *
     * @return string
     */
    public function getEditType()
    {
        return $this->editType ?: self::EDIT_TYPE_MODAL;
    }

    /**
     * Set editType
     *
     * @param string $editType
     *
     * @return Tool
     */
    public function setEditType($editType)
    {
        $this->editType = $editType;

        return $this;
    }

    public function removeElements($propertyName)
    {
        foreach ($this->$propertyName as $element) {
            $this->$propertyName->removeElement($element);
        }
    }

    public function __clone()
    {
        $this->id = null;
        $this->uniqueId = md5(rand().time());

        foreach ($this->getTranslations() as $translation) {
            $cloneTranslation = clone $translation;
            $this->translations->add($cloneTranslation);
            $cloneTranslation->setTranslatable($this);
            $cloneTranslation->setName($this->getName().' copy '.date('Y-m-d H:i:s'));
        }

    }

    /**
     * @return int
     */
    public function getHeightOfRow()
    {
        return $this->heightOfRow;
    }

    /**
     * @param int $heightOfRow
     */
    public function setHeightOfRow($heightOfRow)
    {
        $this->heightOfRow = $heightOfRow;
    }

    /**
     * @return string
     */
    public function getColumnForAutoComplete()
    {
        return $this->columnForAutoComplete;
    }

    /**
     * @param string $columnForAutoComplete
     */
    public function setColumnForAutoComplete($columnForAutoComplete)
    {
        $this->columnForAutoComplete = $columnForAutoComplete;
    }

    public function getPaginationLimit()
    {
        return $this->paginationLimit;
    }

    public function setPaginationLimit($paginationLimit)
    {
        $this->paginationLimit = $paginationLimit;
        return $this;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return string
     */
    public function getToolEditorTitle()
    {
        return $this->toolEditorTitle;
    }

    /**
     * @param string $toolEditorTitle
     */
    public function setToolEditorTitle($toolEditorTitle)
    {
        $this->toolEditorTitle = $toolEditorTitle;
    }

    /**
     * @return bool
     */
    public function isDisableTrim()
    {
        return $this->disableTrim;
    }

    /**
     * @param bool $disableTrim
     */
    public function setDisableTrim($disableTrim)
    {
        $this->disableTrim = $disableTrim;
    }
}
