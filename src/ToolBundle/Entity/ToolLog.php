<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * ToolLog
 *
 * @ORM\Table(name="tool_log")
 * @ORM\Entity()
 */
class ToolLog
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $user;

    /**
     * @ORM\Column(name="tool_id", type="integer")
     */
    private $tool;

    /**
     * @ORM\Column(name="type_log", type="integer")
     */
    private $typeLog;

    /**
     * @var float
     * @ORM\Column(name="time", type="decimal", precision=12, scale=6)
     */
    private $time;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
    }

    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * @param mixed $tool
     */
    public function setTool($tool)
    {
        $this->tool = $tool;
    }

    /**
     * @return float
     */
    public function getTime(): float
    {
        return $this->time;
    }

    /**
     * @param float $time
     */
    public function setTime(float $time)
    {
        $this->time = $time;
    }

    /**
     * @param $user
     * @param $toolId
     * @param $type
     * @return ToolLog
     */
    static function create($user, $toolId, $type) {

        if($user instanceof User) {
            $userId = $user->getId();
        }
        else {
            $userId = null;
        }

        $log = new ToolLog();
        $log->setTool($toolId);
        $log->setUser($userId);
        $log->setTypeLog($type);

        return $log;

    }

    /**
     * @return mixed
     */
    public function getTypeLog()
    {
        return $this->typeLog;
    }

    /**
     * @param mixed $typeLog
     */
    public function setTypeLog($typeLog)
    {
        $this->typeLog = $typeLog;
    }

}
