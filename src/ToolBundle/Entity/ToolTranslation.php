<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table(name="tool_translation")
 * @ORM\Entity()
 */
class ToolTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Sluggable\Sluggable,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="`description`", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $subQueryDescription;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tool
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Tool
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubQueryDescription()
    {
        return $this->subQueryDescription;
    }

    /**
     * @param string $subQueryDescription
     * @return Tool
     */
    public function setSubQueryDescription($subQueryDescription)
    {
        $this->subQueryDescription = $subQueryDescription;

        return $this;
    }

    public function getSluggableFields()
    {
        return ['name', 'id'];
    }

    public function generateSlugValue($values)
    {
        $string = count($values) > 1 ? implode('-', $values) : $values[0];

        $slug = preg_replace("/[^a-zA-Z0-9-]/", '', $string);
        $slug = strtolower(trim($slug, ''));

        return $slug;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
