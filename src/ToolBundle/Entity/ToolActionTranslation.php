<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table(name="tool_action_translation")
 * @ORM\Entity()
 */
class ToolActionTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name = null;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $confirmationDescription;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return StyleTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmationDescription()
    {
        return $this->confirmationDescription;
    }

    /**
     * @param string $confirmationDescription
     */
    public function setConfirmationDescription($confirmationDescription)
    {
        $this->confirmationDescription = $confirmationDescription;
    }



}
