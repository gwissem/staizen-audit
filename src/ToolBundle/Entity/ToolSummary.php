<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use ToolBundle\Service\ToolSummaryService;

/**
 * Tool
 *
 * @ORM\Table(name="tool_summary")
 * @ORM\Entity(repositoryClass="ToolBundle\Repository\ToolSummaryRepository")
 */
class ToolSummary
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="summaryTools")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\Group")
     * @ORM\JoinTable(name="tool_summary_user_group",
     *      joinColumns={@ORM\JoinColumn(name="summary_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $userGroups;

    /**
     * @ORM\ManyToOne(targetEntity="ToolBundle\Entity\Tool")
     * @ORM\JoinColumn(name="tool", referencedColumnName="id")
     */
    protected $tool;
    
    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $filters = '';

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $query = '';
    
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $value = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $columnName;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */

    protected $perUser = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $hash;

    public function __construct()
    {
        $this->userGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getParsedFilters()
    {
        if(!$this->filters) {
            return $this->getTool()->getFormFilters();
        } else {
            return ToolSummaryService::parseFilters($this->filters);
        }
    }
    
    /**
     * @return Tool
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * @param mixed $tool
     */
    public function setTool($tool)
    {
        $this->tool = $tool;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param string $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return mixed
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * @param mixed $columnName
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->translate()->getDescription();
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * Add userGroup
     *
     * @param \UserBundle\Entity\Group $userGroup
     *
     * @return ToolSummary
     */
    public function addUserGroup(\UserBundle\Entity\Group $userGroup)
    {
        $this->userGroups[] = $userGroup;

        return $this;
    }

    /**
     * Remove userGroup
     *
     * @param \UserBundle\Entity\Group $userGroup
     */
    public function removeUserGroup(\UserBundle\Entity\Group $userGroup)
    {
        $this->userGroups->removeElement($userGroup);
    }

    /**
     * Get userGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserGroups()
    {
        return $this->userGroups;
    }

    /**
     * Has userGroups
     *
     * @return boolean
     */
    public function hasUserGroups()
    {
        return (!$this->userGroups->isEmpty());
    }

    /**
     * @return bool
     */
    public function isPerUser()
    {
        return $this->perUser;
    }

    /**
     * @param bool $perUser
     */
    public function setPerUser(bool $perUser)
    {
        $this->perUser = $perUser;
    }

}
