<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Filter
 *
 * @ORM\Table(name="filter")
 * @ORM\Entity(repositoryClass="ToolBundle\Repository\FilterRepository")
 */
class Filter
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    const TYPE_DATE = 'date';
    const TYPE_DATERANGE = 'dates';
    const TYPE_TEXT = 'text';
    const TYPE_NUMBER = 'number';
    const TYPE_SELECT = 'select';
    const TYPE_MULTISELECT = 'multiselect';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Tool", inversedBy="filters")
     * @ORM\JoinColumn(name="tool_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $tool;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $type = self::TYPE_TEXT;

    /**
     * @var string
     *
     * @ORM\Column(name="operator1", type="string", length=255)
     */
    private $defaultOperator;

    /**
     * @var string
     *
     * @ORM\Column(name="operator2", type="string", length=255, nullable = true)
     */
    private $secondDefaultOperator;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="columnName", type="string", length=255)
     */
    private $columnName;

    /**
     * @var string
     *
     * @ORM\Column(name="value1", type="string", length=255, nullable=true)
     */
    private $defaultValue;

    /**
     * @var string
     *
     * @ORM\Column(name="value2", type="string", length=255, nullable=true)
     */
    private $secondDefaultValue;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $query;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pointedColumnName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display", type="boolean", options={"default" : true}, nullable=true)
     */
    private $display = true;

    /**
     * @return string
     */
    public function getPointedColumnName()
    {
        return $this->pointedColumnName;
    }

    /**
     * @param string $pointedColumnName
     * @return Filter
     */
    public function setPointedColumnName($pointedColumnName)
    {
        $this->pointedColumnName = $pointedColumnName;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get defaultOperator
     *
     * @return string
     */
    public function getDefaultOperator()
    {
        return $this->defaultOperator;
    }

    /**
     * Set defaultOperator
     *
     * @param string $defaultOperator
     *
     * @return Filter
     */
    public function setDefaultOperator($defaultOperator)
    {
        $this->defaultOperator = $defaultOperator;

        return $this;
    }

    /**
     * Get secondDefaultOperator
     *
     * @return string
     */
    public function getSecondDefaultOperator()
    {
        return $this->secondDefaultOperator;
    }

    /**
     * Set secondDefaultOperator
     *
     * @param string $secondDefaultOperator
     *
     * @return Filter
     */
    public function setSecondDefaultOperator($secondDefaultOperator)
    {
        $this->secondDefaultOperator = $secondDefaultOperator;

        return $this;
    }

    /**
     * Get columnName
     *
     * @return string
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * Set columnName
     *
     * @param string $columnName
     *
     * @return Filter
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;

        return $this;
    }

    /**
     * uwzględnienie parsowania tablicy elementów oddzielonych przcinkami dla multiselect'a
     *
     * @return string
     */
    public function getParsedDefaultValue()
    {
        if ($this->getType() == self::TYPE_MULTISELECT) {
            return explode(',', preg_replace('/\s*([\s,])\s*/', '\1', $this->defaultValue));
        }

        return $this->defaultValue;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Filter
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set defaultValue
     *
     * @param string $defaultValue
     *
     * @return Filter
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get secondDefaultValue
     *
     * @return string
     */
    public function getSecondDefaultValue()
    {
        return $this->secondDefaultValue;
    }

    /**
     * Set secondDefaultValue
     *
     * @param string $secondDefaultValue
     *
     * @return Filter
     */
    public function setSecondDefaultValue($secondDefaultValue)
    {
        $this->secondDefaultValue = $secondDefaultValue;

        return $this;
    }

    /**
     * Get query
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set query
     *
     * @param string $query
     *
     * @return Filter
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get tool
     *
     * @return \ToolBundle\Entity\Tool
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * Set tool
     *
     * @param \ToolBundle\Entity\Tool $tool
     *
     * @return Filter
     */
    public function setTool(\ToolBundle\Entity\Tool $tool = null)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->translate()->getLabel();
    }

    public function __clone()
    {
        $this->id = null;

        foreach ($this->getTranslations() as $translation) {
            $cloneTranslation = clone $translation;
            $this->translations->add($cloneTranslation);
            $cloneTranslation->setTranslatable($this);
        }

    }

    /**
     * @return bool
     */
    public function isDisplay()
    {
        return $this->display;
    }

    /**
     * @param bool $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

}
