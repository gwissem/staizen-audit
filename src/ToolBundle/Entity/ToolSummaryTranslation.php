<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table(name="tool_summary_translation")
 * @ORM\Entity()
 */
class ToolSummaryTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Column(name="`description`", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return ToolSummaryTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }


}
