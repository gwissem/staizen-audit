<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Style
 *
 * @ORM\Table(name="style")
 * @ORM\Entity(repositoryClass="ToolBundle\Repository\StyleRepository")
 */
class Style
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    const HREF_TYPE_LINK = 'link';
    const HREF_TYPE_UPDATE = 'update';
    const HREF_TYPE_ACTION = 'action';
    const HREF_TYPE_DOCUMENT = 'document';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $columnName;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    private $hidden = false;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $css;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $tooltip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $linkParams;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $conditionQuery;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $rowHighlight = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $displaySummary = false;

    /**
     * @ORM\ManyToOne(targetEntity="Tool")
     * @ORM\JoinColumn(name="tool_id", referencedColumnName="id")
     */
    private $tool;

    /**
     * @ORM\Column(name="edit_row_id", type="string", length=255, nullable=true)
     */
    private $editRowId;

    /**
     * @ORM\ManyToOne(targetEntity="Tool")
     * @ORM\JoinColumn(name="edit_tool_id", referencedColumnName="id")
     */
    private $editTool;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $linkTarget = true;

    /**
     * @ORM\ManyToOne(targetEntity="ToolBundle\Entity\ToolAction", cascade={"persist"})
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $action;

    /**
     * @ORM\Column(name="document_id", type="string", length=255, nullable=true)
     */
    private $document;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maxWidth;

    /**
     * @var integer
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $textOverflowHidden = false;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get columnName
     *
     * @return string
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * Set columnName
     *
     * @param string $columnName
     *
     * @return Style
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;

        return $this;
    }

    /**
     * Get css
     *
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set css
     *
     * @param string $css
     *
     * @return Style
     */
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * @return string
     */
    public function getTooltip()
    {
        return $this->tooltip;
    }

    /**
     * @param string $tooltip
     */
    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return Style
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Style
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get displaySummary
     *
     * @return bool
     */
    public function getDisplaySummary()
    {
        return $this->displaySummary;
    }

    /**
     * Set displaySummary
     *
     * @param boolean $displaySummary
     *
     * @return Style
     */
    public function setDisplaySummary($displaySummary)
    {
        $this->displaySummary = $displaySummary;

        return $this;
    }

    /**
     * Get tool
     *
     * @return \ToolBundle\Entity\Tool
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * Set tool
     *
     * @param \ToolBundle\Entity\Tool $tool
     *
     * @return Style
     */
    public function setTool(\ToolBundle\Entity\Tool $tool = null)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * Get conditionQuery
     *
     * @return string
     */
    public function getConditionQuery()
    {
        return $this->conditionQuery;
    }

    /**
     * Set conditionQuery
     *
     * @param string $conditionQuery
     *
     * @return Style
     */
    public function setConditionQuery($conditionQuery)
    {
        $this->conditionQuery = $conditionQuery;

        return $this;
    }

    /**
     * Get rowHighlight
     *
     * @return boolean
     */
    public function getRowHighlight()
    {
        return $this->rowHighlight;
    }

    /**
     * Set rowHighlight
     *
     * @param boolean $rowHighlight
     *
     * @return Style
     */
    public function setRowHighlight($rowHighlight)
    {
        $this->rowHighlight = $rowHighlight;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->translate()->getAlias();
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set hidden
     *
     * @param boolean $hidden
     *
     * @return Style
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get linkParams
     *
     * @return string
     */
    public function getLinkParams()
    {
        return $this->linkParams;
    }

    /**
     * Set linkParams
     *
     * @param string $linkParams
     *
     * @return Style
     */
    public function setLinkParams($linkParams)
    {
        $this->linkParams = $linkParams;

        return $this;
    }

    /**
     * Get linkType
     *
     * @return string
     */
    public function getLinkType()
    {
        return $this->linkType;
    }

    /**
     * Set linkType
     *
     * @param string $linkType
     *
     * @return Style
     */
    public function setLinkType($linkType)
    {
        $this->linkType = $linkType;

        return $this;
    }

    public function getLinkInfo()
    {
        $output = 'brak';
        if ($this->linkType == self::HREF_TYPE_UPDATE && $this->getEditTool()) {
            $output = 'edycja: "'.$this->getEditTool()->getName().'", dla id='.$this->getEditRowId();
        } elseif ($this->linkType == self::HREF_TYPE_LINK) {
            $output = $this->link.$this->linkParams;
        } elseif ($this->linkType == self::HREF_TYPE_ACTION && $this->getAction()) {
            $output = 'akcja: '.$this->getAction()->getName();
        } elseif ($this->linkType == self::HREF_TYPE_DOCUMENT) {
            $output = 'dokument: '.$this->getDocument();
        }

        return $output;
    }

    /**
     * Get editTool
     *
     * @return \ToolBundle\Entity\Tool
     */
    public function getEditTool()
    {
        return $this->editTool;
    }

    /**
     * Set editTool
     *
     * @param \ToolBundle\Entity\Tool $editTool
     *
     * @return Style
     */
    public function setEditTool(\ToolBundle\Entity\Tool $editTool = null)
    {
        $this->editTool = $editTool;

        return $this;
    }

    /**
     * Get editRowId
     *
     * @return integer
     */
    public function getEditRowId()
    {
        return $this->editRowId;
    }

    /**
     * Set editRowId
     *
     * @param integer $editRowId
     *
     * @return Style
     */
    public function setEditRowId($editRowId)
    {
        $this->editRowId = $editRowId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return Style
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param mixed $document
     */
    public function setDocument($document)

    {
        $this->document = $document;
    }

    /**
     * @return boolean
     */
    public function isLinkTarget()
    {
        return $this->linkTarget;
    }

    /**
     * @param boolean $linkTarget
     */
    public function setLinkTarget($linkTarget)
    {
        $this->linkTarget = $linkTarget;
    }

    public function __clone()
    {
        $this->id = null;

        foreach ($this->getTranslations() as $translation) {
            $cloneTranslation = clone $translation;
            $this->translations->add($cloneTranslation);
            $cloneTranslation->setTranslatable($this);
        }

    }

    /**
     * @return int
     */
    public function getMaxWidth()
    {
        return $this->maxWidth;
    }

    /**
     * @param int $maxWidth
     */
    public function setMaxWidth($maxWidth)
    {
        $this->maxWidth = $maxWidth;
    }

    /**
     * @return int
     */
    public function getTextOverflowHidden()
    {
        return $this->textOverflowHidden;
    }

    /**
     * @param int $textOverflowHidden
     */
    public function setTextOverflowHidden($textOverflowHidden)
    {
        $this->textOverflowHidden = $textOverflowHidden;
    }
}
