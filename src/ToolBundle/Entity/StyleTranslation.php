<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table(name="style_translation")
 * @ORM\Entity()
 */
class StyleTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return StyleTranslation
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

}
