<?php

namespace ToolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Filter
 *
 * @ORM\Table(name="tool_form_field")
 * @ORM\Entity(repositoryClass="ToolBundle\Repository\FormFieldRepository")
 */
class FormField
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    const TYPE_DATE = 'date';
    const TYPE_DATETIME = 'datetime';
    const TYPE_TEXT = 'text';
    const TYPE_NUMBER = 'number';
    const TYPE_SELECT = 'select';
    const TYPE_MULTISELECT = 'multiselect';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_HIDDEN = 'hidden';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_DOCUMENT = 'document';
    const TYPE_WORD = 'word';
    const TYPE_RELATION = 'related';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Tool", inversedBy="formFields")
     * @ORM\JoinColumn(name="tool_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $tool;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $type = self::TYPE_TEXT;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true, options={"default" : false})
     */
    private $required = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true, options={"default" : false})
     */
    private $disabled = false;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="columnName", type="string", length=255)
     */
    private $columnName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $query;

    /**
     * @ORM\Column(name="col_size", type="integer", nullable=true)
     */
    private $colSize;

    /**
     * @ORM\Column(name="row_number", type="integer", nullable=true)
     */
    private $rowNumber;

    /**
     * @ORM\Column(name="position_in_row", type="integer", nullable=true)
     */
    private $positionInRow;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */

    private $sectionRow;

    /**
     * @ORM\Column(name="target_name", type="text", nullable=true)
     */
    private $targetName;

    /**
     * @ORM\Column(name="target_value", type="text", nullable=true)
     */
    private $targetValue;

    /**
     * @ORM\Column(name="word_template", type="text", nullable=true)
     */
    private $wordTemplate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $defaultValue;



    /**
     * @var string
     * @ORM\Column(name="custom_class", type="string", length=255, nullable=true)
     */
    protected $customClass;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return FormField
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get columnName
     *
     * @return string
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * Set columnName
     *
     * @param string $columnName
     *
     * @return FormField
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;

        return $this;
    }

    /**
     * Get query
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set query
     *
     * @param string $query
     *
     * @return FormField
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get tool
     *
     * @return \ToolBundle\Entity\Tool
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * Set tool
     *
     * @param \ToolBundle\Entity\Tool $tool
     *
     * @return FormField
     */
    public function setTool(\ToolBundle\Entity\Tool $tool = null)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * Get required
     *
     * @return boolean
     */
    public function getRequired()
    {
        return (bool)$this->required;
    }

    /**
     * Set required
     *
     * @param boolean $required
     *
     * @return FormField
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getColSize()
    {
        return $this->colSize;
    }

    /**
     * @param mixed $colSize
     */
    public function setColSize($colSize)
    {
        $this->colSize = $colSize;
    }

    /**
     * @return mixed
     */
    public function getRowNumber()
    {
        return $this->rowNumber;
    }

    /**
     * @param mixed $rowNumber
     */
    public function setRowNumber($rowNumber)
    {
        $this->rowNumber = $rowNumber;
    }

    /**
     * @return mixed
     */
    public function getPositionInRow()
    {
        return $this->positionInRow;
    }

    /**
     * @param mixed $positionInRow
     */
    public function setPositionInRow($positionInRow)
    {
        $this->positionInRow = $positionInRow;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     *
     * @return FormField
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTargetName()
    {
        return $this->targetName;
    }

    /**
     * @param mixed $targetName
     */
    public function setTargetName($targetName)
    {
        $this->targetName = $targetName;
    }

    /**
     * @return mixed
     */
    public function getTargetValue()
    {
        return $this->targetValue;
    }

    /**
     * @param mixed $targetValue
     */
    public function setTargetValue($targetValue)
    {
        $this->targetValue = $targetValue;
    }

    /**
     * @return string
     */
    public function getSectionRow()
    {
        if(empty($this->sectionRow)) return $this->sectionRow;

        try {
            $section = explode('||', $this->sectionRow);

            return array(
                'tag' => $section[0],
                'name' => $section[1]
            );
        }
        catch (\Exception $e)
        {
            return $this->sectionRow;
        }

    }

    /**
     * @param string $sectionRow
     */
    public function setSectionRow($sectionRow)
    {
        $this->sectionRow = $sectionRow;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->translate()->getLabel();
    }

    public function __clone()
    {
        $this->id = null;

        foreach ($this->getTranslations() as $translation) {
            $cloneTranslation = clone $translation;
            $this->translations->add($cloneTranslation);
            $cloneTranslation->setTranslatable($this);
        }

    }

    /**
     * @return mixed
     */
    public function getWordTemplate()
    {
        return $this->wordTemplate;
    }

    /**
     * @param mixed $wordTemplate
     */
    public function setWordTemplate($wordTemplate)
    {
        $this->wordTemplate = $wordTemplate;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param mixed $defaultValue
     * @return FormField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomClass()
    {
        return $this->customClass;
    }

    /**
     * @param string $customClass
     */
    public function setCustomClass($customClass)
    {
        $this->customClass = $customClass;
    }


}
