<?php

namespace ToolBundle\Utils;

use UserBundle\Service\UserInfoService;

class WordParser
{

    protected $userInfo;

    public function __construct(UserInfoService $userInfo)
    {
        $this->userInfo = $userInfo;
    }

    public function parseWord($html, $data, $prefix = '{@', $affix = '@}', $useApostrophe = false)
    {
        $query = $this->parseQueryWithRequestParams($html, $data, $prefix, $affix, $useApostrophe);

        $info = $this->userInfo->getInfo();
        if (!empty($info)) {
            $query = $this->parseQueryWithRequestParams($query, $info, '{#', '#}');
        }

        $query = $this->parseCustomNullValues($query, $data);

        return $query;
    }

    public function parseQueryWIthRequestParams($query, $data, $prefix = '{@', $affix = '@}', $useApostrophe = false)
    {
        $params = $this->getStringBetween($prefix, $affix, $query);

        foreach ($params as $param) {
            if (array_key_exists($param, $data)) {
                $value = $useApostrophe ? "'".$data[$param]."'" : $data[$param];
                if ($value === false) {
                    $value = 0;
                }
                $query = str_replace($prefix.$param.$affix, $value, $query);
            }
        }

        return $query;
    }

    public function getStringBetween($start, $end, $str)
    {
        if ($start === $end) {
            $string = explode($start, $str, 3);

            return isset($string[1]) ? $string[1] : '';
        }

        $matches = [];
        $regex = "/$start([a-zA-Z0-9_*]*)$end/";
        preg_match_all($regex, $str, $matches);

        return $matches[1];
    }

    public function parseCustomNullValues($query, $data)
    {
        return str_replace("###NULL###", "NULL", str_replace("'###NULL###'", "NULL", $query));
    }
}