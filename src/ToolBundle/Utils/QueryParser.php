<?php

namespace ToolBundle\Utils;

use ToolBundle\Entity\Tool;
use UserBundle\Service\UserInfoService;

class QueryParser
{

    protected $userInfo;

    /** @var Tool */
    public $currentTool;

    public function __construct(UserInfoService $userInfo)
    {
        $this->userInfo = $userInfo;
    }

    public function parseQuery($query, $data, $prefix = '{@', $affix = '@}', $useApostrophe = false, $emptyChar = "0")
    {
        $query = $this->parseQueryWithRequestParams($query, $data, $prefix, $affix, $useApostrophe, $emptyChar);

        $info = $this->userInfo->getInfo();
        if (!empty($info)) {
            $query = $this->parseQueryWithRequestParams($query, $info, '{#', '#}');
        }

        $query = $this->parseCustomNullValues($query, $data);

        $query = preg_replace('/,$/', '', rtrim($query));

        $disableTrim = ($this->currentTool) ? $this->currentTool->isDisableTrim() : false;

        if(!$disableTrim) {
            /** Po to , żeby wyczyścić białe znaki z zapytania - \t\n\space */
            $query = str_replace(',,', ',', preg_replace('/\s*([\s,])/', '\1', $query));
        }

        return $query;

    }

    public function parseQueryWIthRequestParams($query, $data, $prefix = '{@', $affix = '@}', $useApostrophe = false, $emptyChar = "0")
    {

        $params = $this->getStringBetween($prefix, $affix, $query);
        if (empty($params)) {
            $params = array_keys($data);
        }

        foreach ($params as $param) {
            if (array_key_exists($param, $data)) {

                $value = $data[$param];

                if($value === false ) {
                    $value = 0;
                }
                elseif (is_array($value)) {

                    $items = '';

                    foreach ($value as $itemKey => $item) {
                        if ($useApostrophe) {
                            $item = "'" . $this->ms_escape_string($item) . "'";
                        }
                        $itemPrefix = $itemKey != 0 ? ',' : ' ';
                        $items .= $itemPrefix . $item;
                    }

                    $value = $items;

                }
                else {
                    if($useApostrophe) {
                        $value = "'" . $this->ms_escape_string($value) . "'";
                    }
                }

                $query = str_replace($prefix.$param.$affix, $this->ms_escape_string($value), $query);

            }
            else {
                /** Zabezpieczenie, jak nie przyjdą wymagane dane, do uzupełniania Query */
                $query = str_replace($prefix.$param.$affix, $emptyChar, $query);
            }
        }

        return $query;
    }

    private function getStringBetween($start, $end, $str)
    {
        if ($start === $end) {
            $string = explode($start, $str, 3);

            return isset($string[1]) ? $string[1] : '';
        }

        $matches = [];
        $regex = "/$start([a-zA-Z0-9_-]*)$end/";
        preg_match_all($regex, $str, $matches);

        return $matches[1];
    }

    public function ms_escape_string($data)
    {
        if (is_numeric($data)) {
            return $data;
        }

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ($non_displayables as $regex) {
            $data = preg_replace($regex, '', $data);
        }
        $data = str_replace("'", "''", $data);

        return $data;
    }

    public function parseCustomNullValues($query, $data)
    {
        return str_replace("###NULL###", "NULL", str_replace("'###NULL###'", "NULL", $query));
    }


    public function getParameters($query, $prefix = '{@', $affix = '@}')
    {

        $params = $this->getStringBetween($prefix, $affix, $query);
        return $params;
    }

}