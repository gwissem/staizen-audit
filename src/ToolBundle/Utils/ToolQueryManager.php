<?php

namespace ToolBundle\Utils;

use MediaMonks\MssqlBundle\PDO\PDO as MssqlPDO;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ToolBundle\Entity\Tool;
use ToolBundle\Form\Type\FilterType;
use UserBundle\Service\UserInfoService;

class ToolQueryManager
{
    /** @var Tool $tool */
    private $tool;
    private $container;
    private $offset;
    private $limit;
    private $results;
    private $customQuery;
    private $query;
    private $errors = [];
    private $filters = [];
    private $columns = [];
    private $userInfo;
    private $parser;
    private $extraParams;
    private $translator;

    /** @var \ToolBundle\Service\DateParserService $dateParser */
    private $dateParser;

    public function __construct(
        ContainerInterface $container,
        UserInfoService $userInfo,
        QueryParser $parser,
        $translator
    )
    {
        $this->container = $container;
        $this->userInfo = $userInfo;
        $this->parser = $parser;
        $this->dateParser = $container->get('tool.date_parser.service');
        $this->translator = $translator;
    }

    public function setParams(
        Tool $tool,
        $filters = array(),
        $limit = 1000,
        $offset = 0,
        $customQuery = false,
        $extraParams = []
    )
    {
        $this->tool = $tool;
        $this->setParsedFilters($filters);
        $this->parser->currentTool = $tool;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->customQuery = $customQuery;
        $this->query = $tool->getQuery();
        $this->extraParams = $extraParams;
    }

    private function setParsedFilters($filters)
    {
        foreach ($filters as $key => $filter) {
            $filters[$key] = $this->dateParser->parseDate($filter);
        }

        $this->filters = $filters;
    }

    /**
     * @return Tool mixed
     */
    public function getTool()
    {
        return $this->tool;
    }

    public function getTable($subQuery = false)
    {
        try {
            $this->initQuery([], $subQuery);
        } catch (\PDOException $e) {
            $this->errors[$this->translator->trans('Błąd połączenia')] = $e->getMessage();

            return false;
        }

        return [
            'columns' => $this->getColumns(),
            'rows' => $this->getResults(),
        ];
    }

    public function initQuery($options = [], $subQuery = false, $nestedQuery = false)
    {
        $sql = $subQuery ? $subQuery : $this->tool->getQuery();

        $config = $this->getConnectionParams($options);
        $pdo = $this->setupPDO($config);

        $query = $this->generateQuery($sql, $config['driver'], $this->limit, $this->offset, $subQuery);

        if ($nestedQuery) {
            $query = str_replace('##QUERY##', $query, $nestedQuery);
        }

        $this->query = $query;

        $statement = $pdo->prepare($query);

        $statement->execute();

        /**
         * PRZECHWYCENIE BŁĘDU SQL W PDO DBLIB
         */
        if ($statement->errorCode() !== "00000") {
            $this->errors[$this->translator->trans('Błąd zapytania')] = $statement->errorInfo()[2];
        }

        $total_column = $statement->columnCount();

        $columns = [];

        for ($counter = 0; $counter < $total_column; $counter++) {
            $meta = $statement->getColumnMeta($counter);
            $columns[] = $meta['name'];
        }

        $this->columns = $columns;
        $this->results = $statement->fetchAll(\PDO::FETCH_ASSOC);

    }

    private function getConnectionParams($options = [], $connectionString = null)
    {

        $driver = 'mssql';

        if (!$connectionString && $this->tool) {
            $connectionString = $this->tool->getConnectionString();
        }

        if ($connectionString) {
            $port = '1433';
            $connectionParams = explode(';', $connectionString);
            switch (count($connectionParams)) {
                case 6:
                    $driver = $connectionParams[0];
                    $host = $connectionParams[1];
                    $port = $connectionParams[2];
                    $dbname = $connectionParams[3];
                    $user = $connectionParams[4];
                    $password = $connectionParams[5];
                    break;
                case 5:
                    $driver = $connectionParams[0] == 'mysql' ? 'mysql' : 'mssql';
                    $port = $driver == 'mysql' ? '3306' : '1433';

                    $host = $connectionParams[1];
                    $dbname = $connectionParams[2];
                    $user = $connectionParams[3];
                    $password = $connectionParams[4];
                    break;
                case 4:
                    $host = $connectionParams[0];
                    $dbname = $connectionParams[1];
                    $user = $connectionParams[2];
                    $password = $connectionParams[3];
                    break;
                default:
                    throw new Exception(
                        $this->translator->trans(
                            'Connection string wymaga podania 6 (driver;host;port;dbname;user;password), 5 (domyślny port), 4 (mssql + domyślnym portem) parametrów'
                        )
                    );
                    break;
            }
        } else {
            $host = $this->container->getParameter('mssql_database_host');
            $port = $this->container->getParameter('mssql_database_port');
            $dbname = $this->container->getParameter('mssql_database_name');
            $user = $this->container->getParameter('mssql_database_user');
            $password = $this->container->getParameter('mssql_database_password');
        }

        if (empty($options)) {
            if ($driver == 'mysql') {
                $options = [
                    \PDO::ATTR_PERSISTENT => true,
                ];
                $options[1002] = "SET NAMES 'UTF8' COLLATE 'utf8_unicode_ci'";
            }
        }

        $config = [
            'driver' => $driver,
            'params' => [
                'host' => $host,
                'port' => $port,
                'dbname' => $dbname,
                'user' => $user,
                'password' => $password,
            ],
            'options' => $options,
        ];

        return $config;
    }

    protected function setupPDO($config)
    {

        if ($config['driver'] == 'mysql') {
            $pdo = new \PDO(
                "mysql:host=" . $config['params']['host'] . ";port=" . $config['params']['port'] . ";dbname=" . $config['params']['dbname'],
                $config['params']['user'],
                $config['params']['password'],
                $config['options']
            );
        } else {
            $pdo = new \PDO(
                "sqlsrv:server=" . $config['params']['host'] . "," . $config['params']['port'] . "; Database=" . $config['params']['dbname'],
                $config['params']['user'],
                $config['params']['password'],
                $config['options']
            );
        }

        return $pdo;
    }

    public function generateQuery($query, $driver, $limit, $offset, $subQuery = false)
    {

        // DODANIE PARAMETRÓW FILTROWANIA
        if (!empty($this->extraParams)) {
            $query = $this->parser->parseQuery($query, $this->extraParams);
        }

        $query = $this->updateQueryWithUserParams($query, $this->filters);

        // DOMYŚLNA PAGINACJA

        if (!$subQuery) {
            $query = $this->appendOrder($query, $this->filters, $this->tool->getOrdering());
            if ($driver == 'mssql') {
                if (strpos(strtolower($query), 'order by') === false) {
                    $query .= ' ORDER BY (SELECT(1))';
                }
                if (strpos(strtolower($query), 'offset') === false) {
                    $query .= ' OFFSET ' . $offset . ' ROWS FETCH NEXT ' . $limit . ' ROWS ONLY';
                }
            } else {
                if (strpos(strtolower($query), 'limit') === false) {
                    $query .= ' LIMIT ' . $offset . ',' . $limit;
                }
            }
        }

        $this->query = $query;

        return $query;
    }

    public function updateQueryWithUserParams($query, $filters)
    {
        if ($filters) {
            $query = $this->appendFilters($query, $filters);
        }
        $appParamParsed = $this->parseLocalVariables($query);
        $returnQuery = $this->parser->parseQuery($appParamParsed, $filters);
        $this->query = $returnQuery;

        return $returnQuery;
    }

    /**
     * Dodanie warunków WHERE
     *
     * @param $query
     * @param $filters
     * @return string
     */
    protected function appendFilters($query, $filters)
    {

        if (array_key_exists('hash', $filters)) {
            unset($filters['hash']);
        }
        if (array_key_exists('search', $filters)) {
            unset($filters['search']);
        }

        if (empty($filters)) {
            return $query;
        }
        $where = ' WHERE ';
        $filteredFields = 0;

        foreach ($filters as $key => $value) {

            if (strpos($key, '--pointed') !== false) {
                $sourceColumnName = str_replace('--pointed', '', $key);

                $items = '';

                if (array_key_exists($sourceColumnName, $filters)) {

                    /** Chyba poprawione - by dylo */
                    if(is_array($filters[$sourceColumnName])) {
                        foreach ($filters[$sourceColumnName] as $itemKey => $item) {
                            $itemPrefix = $itemKey != 0 ? ' OR ' : '';
                            $items .= $itemPrefix . "','+" . $value . "+',' like '%," . $item . ",%'";

                        }
                    }

                    if ($items != '') {
                        $where .= '(' . $items . ')';
                        $filteredFields++;
                    }

                    unset($filters[$sourceColumnName]);
                    unset($filters[$sourceColumnName . '--op']);
                }
            }

            if (strpos($key, '--order') === false && strpos($key, '--op') === false && $key != 'hash' && strpos(
                    $key,
                    '--pointed'
                ) === false
            ) {
                $operator = isset($filters[$key . '--op']) ? $filters[$key . '--op'] : '=';
                if(strpos($key, '--to') !== false && !empty($value)){
                    $dateTimeTo = new \DateTime($value);
                    $value = $dateTimeTo->format('Y-m-d H:i:59');
                }
                $key = str_replace('--to', '', $key);

                if (in_array(
                        $operator,
                        array(
                            FilterType::FILTER_EMPTY,
                            FilterType::FILTER_NOT_EMPTY,
                        )
                    )
                    || $value != ''
                ) {

                    if ($filteredFields > 0) {
                        $where .= ' AND ';
                    }

                    if (is_array($value)) {
                        $items = '';
                        foreach ($value as $itemKey => $item) {
                            if (!is_numeric($item)) {
                                $item = "'" . $this->parser->ms_escape_string($item) . "'";
                            }
                            $itemPrefix = $itemKey != 0 ? ',' : ' ';
                            $items .= $itemPrefix . $item;
                        }

                        $where .= $key . ' IN (' . $items . ')';
                    } else {
                        if (!is_numeric($value)) {
                            $value = "'" . $this->parser->ms_escape_string($value) . "'";
                        }

                        switch ($operator) {
                            case FilterType::FILTER_LIKE_PARSED:
                                $where .= $key . " LIKE '%" . str_replace("'", "", $value) . "%' ";
                                break;
                            case FilterType::FILTER_NOT_LIKE_PARSED:
                                $where .= $key . " NOT LIKE '%" . str_replace("'", "", $value) . "%' ";
                                break;
                            case FilterType::FILTER_EMPTY:
                                $where .= "(" . $key . " = '' OR " . $key . " IS NULL)";
                                break;
                            case FilterType::FILTER_NOT_EMPTY:
                                $where .= "(" . $key . " != '' OR " . $key . " IS NOT NULL)";
                                break;
                            default:
                                $where .= $key . ' ' . $operator . ' ' . $value . ' ';
                        }
                    }
                    $filteredFields++;

                }
            }
        }

        $query = 'SELECT * FROM (' . $query . ') toolQuery ';

        if ($filteredFields > 0) {
            $query .= $where;
        }

        $this->query = $query;

        return $query;
    }

    /**
     * Parse for app variables stored in UserInfoService
     *
     * @param $query
     * @return mixed
     */
    public function parseLocalVariables($query)
    {

        $info = $this->userInfo->getInfo();
        if (!empty($info)) {
            $query = $this->parser->parseQuery($query, $info, '{#', '#}');
        }


        return $query;
    }

    /**
     * Dodanie sortowania użytkownika, oraz zdefiniowanego przez admina
     *
     * @param $query
     * @param $filters
     * @param $defaultOrder
     * @return string
     */
    protected function appendOrder($query, $filters, $defaultOrder)
    {

        $orderCount = 0;
        $order = '';
        $defaultOrder = preg_replace('/\s*([\s,])/', '\1', $defaultOrder);

        foreach ($filters as $key => $value) {
            if (strpos($key, '--order') !== false) {
                $column = str_replace('--order', '', $key);

                if ($pos = strpos($defaultOrder, $column) !== false) {

                    $toRemove = array('[', ']', $column . " asc", $column . " desc", $column);
                    $defaultOrder = str_replace($toRemove, array("", "", "", "", ""), $defaultOrder);
                }
                if ($orderCount > 0) {
                    $order .= ', ';
                }

                $order .= $column . ' ' . $value;
                $orderCount++;
            }
        }

        if (!empty($order) || !empty($defaultOrder)) {
            if (!empty($order) && !empty($defaultOrder)) {
                $defaultOrder = ', ' . $defaultOrder;
            }

            $query = $query . ' ORDER BY ' . $order . ' ' . $defaultOrder;
        }

        // usunięcie przecinka z końca zapytania jeśli istnieje
        $query = preg_replace('/,$/', '', rtrim($query));
        $query = str_replace(',,', ',', preg_replace('/\s*([\s,])/', '\1', $query));


        return $query;
    }

    public function getColumns($initQuery = false)
    {
        if ($initQuery) {
            try {
                $this->initQuery();
            } catch (\PDOException $e) {
                $this->errors[$this->translator->trans('Błąd połączenia')] = $e->getMessage();

                return false;
            }
        }

        return $this->columns;
    }

    public function getResults()
    {
        return $this->results;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function setFilters($filters)
    {
        $this->setParsedFilters($filters);
    }

    public function fetchColumnsForCustomQuery($query, $connectionString)
    {

        $config = $this->getConnectionParams([], $connectionString);
        $pdo = $this->setupPDO($config);
        $statement = $pdo->prepare($query);
        $execute = $statement->execute();

        $total_column = $statement->columnCount();
        $columns = [];

        for ($counter = 0; $counter < $total_column; $counter++) {
            $meta = $statement->getColumnMeta($counter);
            $columns[] = $meta['name'];
        }

        return $columns;
    }

    public function rowCount($query, $connectionString)
    {
        $config = $this->getConnectionParams([], $connectionString);
        $pdo = $this->setupPDO($config);
        $statement = $pdo->prepare($query);
        $execute = $statement->execute();

        $rowCount = $statement->rowCount();
        if ($rowCount < 0) {
            $statement = $pdo->prepare('SELECT COUNT(*) FROM (' . $query . ') customQuery');
            $execute = $statement->execute();
            $rowCount = $statement->fetch()[0];
        }

        return $rowCount;
    }

    public function appendLimit($query, $offset = 0, $limit, $connectionString = null)
    {
        $config = $this->getConnectionParams([], $connectionString);

        if ($config['driver'] == 'mssql') {
            if (strpos(strtolower($query), 'order by') === false) {
                $query .= ' ORDER BY (SELECT(1))';
            }
            if (strpos(strtolower($query), 'offset') === false) {
                $query .= ' OFFSET ' . $offset . ' ROWS FETCH NEXT ' . $limit . ' ROWS ONLY';
            }
        } else {
            if (strpos(strtolower($query), 'limit') === false) {
                $query .= ' LIMIT ' . $offset . ',' . $limit;
            }
        }

        return $query;
    }

    public function setPagination($page)
    {
        $paginationLimit = $this->tool->getPaginationLimit();
        if ($page && $paginationLimit) {
            $this->limit = $limit = $paginationLimit;
            $this->offset = ($page - 1) * $limit;
        }
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getTotalCount($query)
    {
        if ($this->tool->getCount()) {
            $results = $this->executeCustomQuery('SELECT COUNT (*) totalRows FROM (' . $this->updateQueryWithUserParams($query, $this->filters) . ') totalCount', true);
            return $results[0]['totalRows'];
        }
        return false;
    }

    public function executeCustomQuery(
        $query,
        $fetch = false,
        $connectionString = null,
        $procedure = false,
        $parserData = [],
        $nextRowSet = false,
        &$parsedQ = null,
        &$lastInsertId = null
    )
    {
        $config = $this->getConnectionParams([], $connectionString);
        $pdo = $this->setupPDO($config);
        $parsedQuery = $this->parser->parseQuery($query, $parserData);

        $execProcedure = strpos(strtolower($parsedQuery), 'exec') === 0 ||
            strpos(strtolower($parsedQuery), 'call') === 0 ||
            strpos(strtolower($parsedQuery), 'execute') === 0;

        if ($execProcedure) {
            $parsedQuery = $this->appendDeclareAndSelectToQuery($parsedQuery);
        }

        if ($parsedQ !== null) {
            $parsedQ = $parsedQuery;
        }

        $statement = $pdo->prepare($parsedQuery);

        if($statement === false) {
            return false;
        }

        $execute = $statement->execute();

        if ($lastInsertId !== null) $lastInsertId = $pdo->lastInsertId();

        // check if stored procedure
        if ($execProcedure && $execute) {

            do {
                $result = $statement->fetchAll(\PDO::FETCH_ASSOC);

                if (!empty($result)) {

                    /** Funkcjonalność do zwracania ID ostatniego INSERTU wykonanego przez procedure */

                    if (count($result[0]) > 0 && (array_key_exists('id', $result[0]) || array_key_exists('Id', $result[0]))) {

                        $lastId = array_values($result[0])[0];
                        if ($lastId && $lastInsertId !== null) $lastInsertId = $lastId;

                    }
                    /** --- */

                    if (count($result[0]) > 0 && array_key_exists('@err', $result[0])) {

                        if ($result[0]['@err'] != 0) {

                            $msg = 'PROCEDURE ERROR';
                            if (array_key_exists('@message', $result[0])) {
                                $msg = $result[0]['@message'];
                            }

                            return ['execute' => $execute, 'error' => $msg];
                        }

                    }
                }

            } while ($statement->nextRowset());

            return $execute;
        }

        if ($fetch) {
            return $statement->fetchAll(\PDO::FETCH_ASSOC);
        }

        return $execute;
    }

    private function appendDeclareAndSelectToQuery($query)
    {

        $query = 'DECLARE @err int, @message nvarchar(100) ' . $query;
        $query .= " SELECT @err as N'@err', @message as N'@message'";

        return $query;
    }


}