<?php

namespace ToolBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use ToolBundle\Entity\Tool;
use UserBundle\Entity\User;
use UserBundle\Service\UserPersonalizationService;

class MenuBuilder
{
    private $factory;

    /** @var  EntityManager */
    private $em;

    /** @var RequestStack $requestStack */
    private $requestStack;

    /** @var  ContainerInterface */
    private $container;

    private $translator;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     * @param $em
     * @param $requestStack
     * @param ContainerInterface $container
     * @param $translator
     */
    public function __construct(
        FactoryInterface $factory,
        $em,
        $requestStack,
        ContainerInterface $container,
        $translator
    )
    {
        $this->factory = $factory;
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->container = $container;
        $this->translator = $translator;
    }

    public function createAdminToolsMenu(array $options)
    {
        $menu = $this->factory->createItem(
            'root',
            array(
                'childrenAttributes' => array(
                    'class' => 'dropdown-menu dropdown-menu-fw',
                    'id' => 'admin-starter-menu',
                ),
            )
        );

        $tools = $menu->addChild(
            $this->translator->trans('Narzędzia'),
            array(
                'uri' => 'javascript:;',
                'attributes' => array(
                    'class' => 'dropdown more-dropdown-sub',
                ),
                'childrenAttributes' => array(
                    'class' => 'dropdown-menu',
                ),
            )
        );
        $tools->addChild(
            $this->translator->trans('Dodaj narzędzie'),
            array(
                'route' => 'admin_tool_definition',
            )
        );
        $tools->addChild(
            $this->translator->trans('Lista narzędzi'),
            array(
                'route' => 'admin_tools',
            )
        );


        $toolGroups = $menu->addChild(
            $this->translator->trans('Grupy narzędzi'),
            array(
                'uri' => 'javascript:;',
                'attributes' => array(
                    'class' => 'dropdown more-dropdown-sub',
                ),
                'childrenAttributes' => array(
                    'class' => 'dropdown-menu',
                ),
            )
        );
        $toolGroups->addChild(
            $this->translator->trans('Dodaj grupę'),
            array(
                'route' => 'admin_tool_group_editor',
            )
        );
        $toolGroups->addChild(
            $this->translator->trans('Lista grup'),
            array(
                'route' => 'admin_tool_groups',
            )
        );

        return $menu;
    }


    public function createUserToolsMenu(array $options)
    {
        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $locale = ($user->getLocale()) ?: $this->requestStack->getMasterRequest()->getLocale();

        if ($user->isAdmin()) {
            $menuItems = $this->em->getRepository('ToolBundle:Tool')->fetchUserTools($locale);
        } else {
            $menuItems = $this->em->getRepository('ToolBundle:Tool')->fetchUserAvailableTools($user, $locale);
        }

        $menu = $this->createRootNode();

        $ids = $user->getPersonalizationJson()[UserPersonalizationService::SIDEBAR_NAV_ITEMS] ?? [];

        if($ids === '') {
            $ids = [];
        }

        foreach ($menuItems as $key => $group) {


            $idGroup = (count($group) && $group[0]->getToolGroup()) ? $group[0]->getToolGroup()->getId() : 'other';

            $openClass = (in_array($idGroup, $ids)) ? 'open' : '';

            $menuGroup = $this->addGroupToolsNode($menu, $key, $idGroup, $openClass);

            foreach ($group as $tool) {
                $this->addToolNode($menuGroup, $tool);
            }
        }

        return $menu;
    }

    private function createRootNode()
    {
        return $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'class' => 'page-sidebar-menu',
                    'id' => 'user-tools-menu',
                    'data-keep-expanded' => 'true',
                    'data-auto-scroll' => 'true',
                    'data-slide-speed' => '200',
                ],
            ]
        );
    }

    private function addGroupToolsNode($menu, $key, $idGroup, $openClass)
    {
        return $menu->addChild(
            $key,
            [
                'uri' => 'javascript:;',
                'attributes' => [
                    'class' => $idGroup === $this->container->getParameter(
                        'tool_hidden_menu_id'
                    ) ? 'hidden' : ' nav-item ' . $openClass,
                    'span' => '<span class="arrow ' . $openClass . '"></span>',
                    'data-id' => $idGroup
                ],
                'linkAttributes' => [
                    'class' => 'nav-link nav-toggle',
                ],
                'childrenAttributes' => [
                    'style' => ($openClass == "open") ? 'display: block;' : ''
                ],
            ]
        );
    }

    private function addToolNode($menuGroup, $tool)
    {
        $menuGroup->addChild(
            $tool->getName().'-'.$tool->getId(),
            [
                'label' => $tool->getName(),
                'route' => 'tool_show',
                'routeParameters' => [
                    'toolId' => $tool->getId(),
                ],
                'attributes' => [
                    'class' => 'nav-item',
                ],
                'linkAttributes' => [
                    'class' => 'nav-link',
                ],
            ]
        );
    }

    public function createAdminToolEditorMenu(array $options)
    {
        $currentRoute = $this->requestStack->getCurrentRequest()->get('_route');
        $toolId = $this->requestStack->getCurrentRequest()->get('toolId');

        $menu = $this->factory->createItem(
            'toolActionBar',
            array(
                'childrenAttributes' => array(
                    'class' => 'nav nav-tabs',
                    'id' => 'tool-action-bar',
                ),
            )
        );

        $menu->addChild(
            $this->translator->trans('Definicja'),
            array(
                'route' => 'admin_tool_definition',
                'routeParameters' => array(
                    'toolId' => $toolId,
                ),
                'attributes' => array(
                    'class' => 'nav-item',
                ),
                'linkAttributes' => array(
                    'class' => 'nav-link p-x-3',
                ),
            )
        );
        if ($toolId) {
            $filters = $menu->addChild(
                $this->translator->trans('Filtry'),
                array(
                    'route' => 'admin_tool_filters',
                    'routeParameters' => array(
                        'toolId' => $toolId,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );

            $attachedRoutes = array(
                'admin_tool_filter_editor',
            );

            if (in_array($currentRoute, $attachedRoutes)) {
                $filters->setCurrent(true);
            }

            $styles = $menu->addChild(
                $this->translator->trans('Style'),
                array(
                    'route' => 'admin_tool_styles',
                    'routeParameters' => array(
                        'toolId' => $toolId,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );

            $attachedRoutes = array(
                'admin_tool_style_editor',
            );

            if (in_array($currentRoute, $attachedRoutes)) {
                $styles->setCurrent(true);
            }

            // EDYCJA DANYCH

            $modify = $menu->addChild(
                $this->translator->trans('Edycja'),
                array(
                    'route' => 'admin_tool_data_modify',
                    'routeParameters' => array(
                        'toolId' => $toolId,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );

            $attachedRoutes = array(
                'admin_tool_form_field_editor',
            );

            if (in_array($currentRoute, $attachedRoutes)) {
                $modify->setCurrent(true);
            }

            $actions = $menu->addChild(
                $this->translator->trans('Akcje'),
                array(
                    'route' => 'admin_tool_actions',
                    'routeParameters' => array(
                        'toolId' => $toolId,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );

            $attachedRoutes = array(
                'admin_tool_action_editor',
            );

            if (in_array($currentRoute, $attachedRoutes)) {
                $actions->setCurrent(true);
            }


            // UPRAWNIENIA DLA GRUP

            $permissions = $menu->addChild(
                $this->translator->trans('Uprawnienia'),
                array(
                    'route' => 'admin_tool_permissions',
                    'routeCollection' => array(
                        'admin_tool_permission_editor',
                    ),
                    'routeParameters' => array(
                        'toolId' => $toolId,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );

            $attachedRoutes = array(
                'admin_tool_permission_editor',
            );

            if (in_array($currentRoute, $attachedRoutes)) {
                $permissions->setCurrent(true);
            }

            // UPRAWNIENIA DLA GRUP

            $permissions = $menu->addChild(
                $this->translator->trans('Kopia/Eksport'),
                array(
                    'route' => 'admin_tool_export',
                    'routeParameters' => array(
                        'toolId' => $toolId,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );

            $attachedRoutes = array(
                'admin_tool_import_json',
                'admin_tool_export_json',
                'admin_tool_clone',
            );

            if (in_array($currentRoute, $attachedRoutes)) {
                $permissions->setCurrent(true);
            }

            $menu->addChild(
                $this->translator->trans('Powrót'),
                array(
                    'route' => 'admin_tool_show',
                    'routeParameters' => array(
                        'toolId' => $toolId,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item pull-right back-to-tool',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );


        } else {
            $this->addDisabledTab($menu, $this->translator->trans('Filtry'));
            $this->addDisabledTab($menu, $this->translator->trans('Style'));
            $this->addDisabledTab($menu, $this->translator->trans('Akcje'));
            $this->addDisabledTab($menu, $this->translator->trans('Uprawnienia'));
            $this->addDisabledTab($menu, $this->translator->trans('Powrót'), 'pull-right');
        }
        return $menu;
    }

    private function addDisabledTab($menu, $name, $class = "")
    {
        $menu->addChild(
            $name,
            array(
                'uri' => '#',
                'attributes' => array(
                    'class' => 'nav-item disabled '.$class,
                ),
                'linkAttributes' => array(
                    'class' => 'nav-link p-x-3',
                ),
            )
        );
    }
}