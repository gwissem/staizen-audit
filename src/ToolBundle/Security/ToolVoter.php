<?php

namespace ToolBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolAction;
use UserBundle\Entity\User;

class ToolVoter extends Voter
{
    const READ = 'read';
    const WRITE = 'write';
    const BATCHWRITE = 'batchWrite';
    const USE_ACTION_ROW = 'useActionRow';
    const USE_ACTION_INLINE = 'useActionInline';
    private $decisionManager;
    private $em;

    public function __construct(AccessDecisionManagerInterface $decisionManager, EntityManager $em)
    {
        $this->em = $em;
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array(
            $attribute,
            array(self::READ, self::WRITE, self::BATCHWRITE, self::USE_ACTION_ROW, self::USE_ACTION_INLINE)
        )
        ) {
            return false;
        }

        // only vote on Tool objects inside this voter
        if (!$subject instanceof Tool) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        // you know $subject is a Tool object, thanks to supports
        /** @var Tool $Tool */
        $tool = $subject;

        switch ($attribute) {
            case self::READ:
                return $this->canRead($tool, $user);
            case self::WRITE:
                if (!$user instanceof User) {
                    return false;
                }

                return $this->canWrite($tool, $user);
            case self::BATCHWRITE:
                if (!$user instanceof User) {
                    return false;
                }
                return $this->canBatchWrite($tool, $user);
            case self::USE_ACTION_ROW:
                if (!$user instanceof User) {
                    return false;
                }

                return $this->canUseActionRow($tool, $user);
            case self::USE_ACTION_INLINE:
                if (!$user instanceof User) {
                    return false;
                }

                return $this->canUseActionInline($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canRead(Tool $tool, $user)
    {
        if ($tool->getAllowForGuest()) {
            return true;
        }

        if (!$user instanceof User) {
            return false;
        }

        return $this->canModifyTool($tool, $user, 'read');
    }

    public function canModifyTool($tool, $user, $action)
    {
        $actionMethod = 'getCan'.ucwords($action);
        $userPermissions = $this->em->getRepository('ToolBundle:ToolUserPermission')->findOneBy(['user'=> $user,
            'tool'=>$tool]);
        if($userPermissions){
            if($userPermissions->$actionMethod()===true){
                return true;
            }
        }
        foreach ($user->getGroups() as $group) {
            foreach ($tool->getPermissions() as $toolPermission) {
                if ($toolPermission->getGroup()->getId() === $group->getId() && $toolPermission->$actionMethod(
                    ) === true
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    private function canWrite(Tool $tool, User $user)
    {
        // if can batch-write, can write singles also
        if ($this->canBatchWrite($tool, $user)) {
            return true;
        }

        return $this->canModifyTool($tool, $user, self::WRITE);
    }

    private function canBatchWrite(Tool $tool, User $user)
    {
        return $this->canModifyTool($tool, $user, self::BATCHWRITE);
    }

    private function canUseActionRow(Tool $tool, User $user)
    {
        foreach ($tool->getActions() as $action) {
            foreach ($action->getUserGroups() as $userGroup) {
                if ($user->getGroups()->contains($userGroup)) {
                    return true;
                }
            }
        }

        return false;
    }

    private function canUseActionInline(ToolAction $action, User $user)
    {
        foreach ($action->getUserGroups() as $userGroup) {
            if ($user->getGroups()->contains($userGroup)) {
                return true;
            }
        }

        return false;
    }
}