<?php

namespace ToolBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolAction;
use UserBundle\Entity\User;

class ToolActionVoter extends Voter
{
    const EXECUTE = 'execute';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::EXECUTE))) {
            return false;
        }

        // only vote on Tool objects inside this voter
        if (!$subject instanceof ToolAction) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        // you know $subject is a Tool object, thanks to supports
        /** @var ToolAction $action */
        $action = $subject;

        switch ($attribute) {
            case self::EXECUTE:
                if (!$user instanceof User) {
                    return false;
                }

                return $this->canExecute($action, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }


    private function canExecute(ToolAction $action, User $user)
    {
        foreach ($action->getUserGroups() as $userGroup) {
            if ($user->getGroups()->contains($userGroup)) {
                return true;
            }
        }

        return false;
    }
}