<?php

namespace ToolBundle\Repository;
use Doctrine\ORM\EntityRepository;

class GroupHasToolRepository extends EntityRepository
{
    public function fetchUserDefaultTools($user)
    {
        $query = $this->createQueryBuilder('ugt')
            ->select('ugt, ug, t, u, tt')
            ->innerJoin('ugt.group', 'ug')
            ->innerJoin('ugt.tool', 't')
            ->innerJoin('ug.users', 'u')
            ->leftJoin('t.translations', 'tt')
            ->where('u.id = :userId')
            ->groupBy('ugt.tool')
            ->setParameter('userId', $user->getId())
            ->getQuery();

        return $query->getResult();

    }
}
