<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use ToolBundle\Entity\ToolAction;

class ActionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [

                    'fields' => [
                        'name' => [
                            'field_type' => TextType::class,
                            'label' => 'Nazwa',
                            'required' => true,
                            'constraints' => [new NotBlank()],
                        ],
                        'confirmationDescription' => [
                            'field_type' => TextareaType::class,
                            'label' => 'Tekst potwierdzenia',
                            'required' => false,
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                    //'excluded_fields' => ['slug', 'subqueryQescription', 'createdAt', 'updatedAt'],
                ]
            )
            ->add(
                'query',
                TextareaType::class,
                [
                    'label' => 'Kwerenda',
                    'attr' => array(
                        'rows' => 3,
                    ),
                ]
            )
            ->add(
                'hasConfirmation',
                CheckboxType::class,
                [
                    'label' => 'Wyświetl potwierdzenie',
                    'required' => false,
                ]
            )
            ->add(
                'forSelectedRows',
                CheckboxType::class,
                [
                    'label' => 'Dla zaznaczonych wierszy',
                    'required' => false,
                ]
            )
            ->add(
                'buttonPlacement',
                ChoiceType::class,
                [
                    'label' => 'Położenie przycisku',
                    'choices' => [
                        'Nad tabelą' => ToolAction::PLACEMENT_TOP,
                        'W wierszu tabeli' => ToolAction::PLACEMENT_ROW,
                    ],
                ]
            )
            ->add(
                'color',
                TextType::class,
                array(
                    'required' => false,
                    'label' => 'Kolor',
                )
            )
            ->add(
                'userGroups',
                null,
                array(
                    'required' => false,
                    'label' => 'Grupy użytkowników',
                    'attr' => ['class' => 'select2-deselect'],
                    'multiple' => true,
                )
            )
            ->add(
                'icon',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'Ikona',
                ]
            )
            ->add(
                'position',
                ChoiceType::class,
                array(
                    'required' => true,
                    'choices' => $this->getPositions(),
                    'label' => 'Pozycja',
                )
            );

    }

    private function getPositions()
    {
        $arr = [];
        for ($i = 1; $i <= 10; $i++) {
            $arr[$i] = $i;
        }

        return $arr;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\ToolAction',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_action';
    }

}