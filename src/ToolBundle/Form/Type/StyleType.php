<?php

namespace ToolBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use ToolBundle\Entity\Style;
use ToolBundle\Repository\ToolActionRepository;
use ToolBundle\Repository\ToolRepository;

class StyleType extends AbstractType
{

    protected $tool;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->tool = $options['tool'];

        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [
                    'fields' => [
                        'alias' => [
                            'field_type' => TextType::class,
                            'label' => 'Nagłówek tabeli',
                            'required' => false,
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                    //'excluded_fields' => ['slug', 'subqueryQescription', 'createdAt', 'updatedAt'],
                ]
            )
            ->add(
                'columnName',
                TextType::class,
                [
                    'label' => 'Nazwa kolumny',
                ]
            )
            ->add(
                'hidden',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Ukryta kolumna',
                ]
            )
            ->add(
                'css',
                TextareaType::class,
                [
                    'label' => 'CSS',
                    'required' => false,
                    'attr' => array(
                        'rows' => 5,
                    ),
                ]
            )
            ->add(
                'tooltip',
                TextareaType::class,
                [
                    'label' => 'Tooltip info',
                    'required' => false,
                    'attr' => array(
                        'rows' => 2,
                    ),
                ]
            )
            ->add(
                'conditionQuery',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'Warunek',
                    'attr' => array(
                        'rows' => 3,
                    ),
                ]
            )
            ->add(
                'icon',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'Ikona',
                ]
            )
            ->add(
                'maxWidth',
                IntegerType::class,
                [
                    'required' => false,
                    'label' => 'Szerokość kolumny (px)',
                ]
            )
            ->add(
                'textOverflowHidden',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Ucinanie tekstu',
                ]
            )
            ->add(
                'linkType',
                ChoiceType::class,
                [
                    'label' => 'Typ podlinkowania',
                    'required' => false,
                    'choices' => [
                        'Link do strony' => Style::HREF_TYPE_LINK,
                        'Edycja danych' => Style::HREF_TYPE_UPDATE,
                        'Wywołanie akcji' => Style::HREF_TYPE_ACTION,
                        'Dokument plików' => Style::HREF_TYPE_DOCUMENT,
                    ],
                    'placeholder' => 'Bez podlinkowania',
                ]
            );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetDataEvent'));
        $builder->addEventListener(FormEvents::SUBMIT, array($this, 'onPostSubmitData'));

        $builder->get('linkType')->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onTypePreSubmitDataEvent'));
    }

    public function onPostSubmitData(FormEvent $event)
    {
        $data = $event->getData();
        $linkType = $data->getLinkType();

        if ($linkType == Style::HREF_TYPE_UPDATE && !$data->getEditRowId()) {
            $event->getForm()->get('editRowId')->addError(new FormError('Wybierz edytowane narzędzie'));
        }
        if ($linkType == Style::HREF_TYPE_UPDATE && !$data->getEditTool()) {
            $event->getForm()->get('editTool')->addError(new FormError('Wskaż kolumnę id edytowanego elementu'));
        }
        if ($linkType == Style::HREF_TYPE_LINK && !$data->getLink()) {
            $event->getForm()->get('link')->addError(new FormError('Wprowadź adres URL'));
        }
    }

    function onPreSetDataEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $linkType = $data->getLinkType();
        $this->updateFormElements($form, $linkType, 'preSetData', $data);
    }

    protected function updateFormElements($builder, $linkType, $eventType = 'preSetData')
    {

        if ($linkType == Style::HREF_TYPE_LINK) {
            $builder->add(
                'link',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'URL',
                ]
            )
                ->add(
                    'linkParams',
                    TextType::class,
                    [
                        'required' => false,
                        'label' => 'Parametry linku',
                        'attr' => [
                            'placeholder' => 'np: #id--op=%3D&id={@id@}',
                        ],
                    ]
                )
                ->add(
                    'linkTarget', CheckboxType::class, [
                        'required' => false,
                        'label' => "W nowym oknie"
                    ]
                )
                ->remove('document')
                ->remove('editTool')
                ->remove('editRowId');
        } elseif ($linkType == Style::HREF_TYPE_UPDATE) {
            $builder->add(
                'editTool',
                EntityType::class,
                [
                    'label' => 'Narzędzie',
                    'class' => 'ToolBundle:Tool',
                    'query_builder' => function (ToolRepository $er) {
                        return $er->createQueryBuilder('s')
                            ->join('s.translations', 'st')
                            ->orderBy('st.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Wybierz narzędzie',
                    'attr' => ['class' => 'select2'],
                ]
            )
                ->add(
                    'editRowId',
                    TextType::class,
                    [
                        'label' => 'Wskaż kolumnę id',
                        'attr' => [
                            'placeholder' => 'np: {@id@}',
                        ],
                    ]
                )
                ->remove('document')
                ->remove('link')
                ->remove('action')
                ->remove('linkTarget')
                ->remove('linkParams');
        } elseif ($linkType == Style::HREF_TYPE_ACTION) {

            $tool = $this->tool;

            $builder->add(
                'action',
                EntityType::class,
                [
                    'label' => 'Akcja',
                    'class' => 'ToolBundle:ToolAction',
                    'query_builder' => function (ToolActionRepository $er) use ($tool) {
                        $result = $er->createQueryBuilder('a')
                            ->orderBy('a.position', 'ASC');

                        if ($tool) {
                            $result->andWhere('a.tool = :tool')
                                ->setParameter('tool', $tool);
                        }

                        return $result;
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Wybierz akcję',
                    'attr' => ['class' => 'select2'],
                ]
            )
                ->remove('document')
                ->remove('editRowId')
                ->remove('editTool')
                ->remove('link')
                ->remove('linkTarget')
                ->remove('linkParams');
        } elseif ($linkType == Style::HREF_TYPE_DOCUMENT) {
            $builder
                ->remove('editTool')
                ->remove('editRowId')
                ->remove('link')
                ->remove('action')
                ->remove('linkTarget')
                ->remove('linkParams')
                ->add(
                    'document',
                    TextType::class,
                    [
                        'required' => false,
                        'label' => 'Id dokumentu',
                    ]
                );
        } else {
            $builder
                ->remove('document')
                ->remove('editTool')
                ->remove('editRowId')
                ->remove('link')
                ->remove('action')
                ->remove('linkTarget')
                ->remove('linkParams');
        }
    }

    function onTypePreSubmitDataEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $this->updateFormElements($form->getParent(), $data, 'preSubmitData');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\Style',
                'tool' => null,
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_style';
    }

}