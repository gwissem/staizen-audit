<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ToolBundle\Entity\FormField;
use ToolBundle\Utils\WordParser;

class UserToolDataModifyType extends FilterType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $data = $builder->getData();

        $dateParser = $options['dateParser'];

        $builder->setMethod('POST');
        $fields = $options['fields'];
        $params = $options['params'];
        $toolQueryManager = $options['toolQueryManager'];
        $id = $options['id'];
        $idName = $options['idName'] ?: 'id';
        $idDefined = false;
        $isDisableTrim = $options['isDisableTrim'];

        /** @var WordParser $wordParser */
        $wordParser = $options['wordParser'] ? $options['wordParser'] : null;

        /**
         * @var FormField $field
         */

        foreach ($fields as $field) {
            if ($field->getColumnName() == $idName) {
                $idDefined = true;
            }

            $fieldType = $field->getType();

            switch ($fieldType) {
                case 'datetime' :
                    $widgetClass = 'date-time-picker';
                    break;
                case 'date' :
                    $widgetClass = 'date-picker';
                    break;
                default :
                    $widgetClass = '';
            }

            switch ($fieldType) {

                case FormField::TYPE_DATETIME:

                    $builder->add(
                        $field->getColumnName(),
                        TextType::class,
                        [
                            'label' => $field->getLabel(),
                            'required' => $field->getRequired(),
                            'disabled' => $field->getDisabled(),
                            'attr' => ['class' => 'date-time-picker'],
                            'data' => $this->getDefaultValue($data, $field),
                        ]
                    );

                    $builder->get($field->getColumnName())
                        ->addModelTransformer(
                            new CallbackTransformer(
                                function ($value) use ($dateParser) {
                                    return $dateParser->parseDate($value);
                                },
                                function ($value) use ($field) {
                                    if ($value == '') {
//                                        return '###NULL###';
                                        return $field->getRequired() ? '' : '###NULL###';
                                    }

                                    return $value;
                                }
                            )
                        );

                    break;
                case FormField::TYPE_DATE:

                    $builder->add(
                        $field->getColumnName(),
                        TextType::class,
                        [
                            'label' => $field->getLabel(),
                            'required' => $field->getRequired(),
                            'disabled' => $field->getDisabled(),
                            'attr' => ['class' => 'date-picker'],
                            'data' => $this->getDefaultValue($data, $field),
                        ]
                    );

                    $builder->get($field->getColumnName())
                        ->addModelTransformer(
                            new CallbackTransformer(
                                function ($value) use ($dateParser) {
                                    return $dateParser->parseDate($value);
                                },
                                function ($value) use ($field) {
                                    if ($value == '') {
//                                        return '###NULL###';
                                        return $field->getRequired() ? '' : '###NULL###';
                                    }

                                    return $value;
                                }
                            )
                        );

                    break;
                case FormField::TYPE_NUMBER:
                    $builder->add(
                        $field->getColumnName(),
                        TextType::class,
                        [
                            'label' => $field->getLabel(),
                            'required' => $field->getRequired(),
                            'disabled' => $field->getDisabled(),
                            'data' => $this->getDefaultValue($data, $field),
                            'trim' => !$isDisableTrim
                        ]
                    );

                    $builder->get($field->getColumnName())
                        ->addModelTransformer(
                            new CallbackTransformer(
                                function ($number) {
                                    return str_replace(',', '.', $number);
                                },
                                function ($value) {

                                    if ($value == '') {
                                        return '###NULL###';
                                    } else {
                                        return str_replace(',', '.', $value);
                                    }
                                    return $value;
                                }
                            )
                        );

                    break;
                case FormField::TYPE_MULTISELECT:
                case FormField::TYPE_SELECT:
                case FormField::TYPE_RELATION:

                    $builder->add(
                        $field->getColumnName(),
                        ChoiceType::class,
                        [
                            'label' => $field->getLabel(),
                            'required' => $field->getRequired(),
                            'disabled' => $field->getDisabled(),
                            'attr' => [
                                'class' => 'select2-deselect ' . ($fieldType == FormField::TYPE_RELATION ? ' dontlook ' : '')
                            ],
                            'multiple' => $fieldType == FormField::TYPE_MULTISELECT,
                            'choices' => $this->selectQueryToArray($field->getQuery(), $toolQueryManager, $params),
                            'data' => $this->getDefaultValue($data, $field),
                        ]
                    );
                    if ($fieldType == FormField::TYPE_RELATION) {

                        $builder->get($field->getColumnName())
                            ->addModelTransformer(
                                new CallbackTransformer(
                                    function ($number) {
                                        return (int)$number;
                                    },
                                    function ($value) {

                                        return $value;
                                    }
                                )
                            )
                            ->addEventListener(FormEvents::PRE_SET_DATA, function ($event) use ($options) {
                                $this->onPreSubmitEventWithRelatedField($event, $options);
                            });
                    }
                    if ($fieldType == FormField::TYPE_MULTISELECT) {
                        $builder->get($field->getColumnName())
                            ->addModelTransformer(
                                new CallbackTransformer(
                                    function ($array) {
                                        if(is_string($array)) {
                                            return explode(',', $array);
                                        }
                                    },
                                    function ($array) {
                                        if(is_array($array)) {
                                            return implode(',', $array);
                                        }
                                    }
                                )
                            );
                    }
                    break;

                case FormField::TYPE_CHECKBOX:

                    $builder->add(
                        $field->getColumnName(),
                        CheckboxType::class,
                        [
                            'label' => $field->getLabel(),
                            'required' => false,
                            'disabled' => $field->getDisabled(),
                            'data' => $this->getDefaultValue($data, $field),
                        ]
                    );

                    $builder->get($field->getColumnName())
                        ->addModelTransformer(
                            new CallbackTransformer(
                                function ($value) {
                                    return (boolean)$value;
                                },
                                function ($value) {
                                    return (boolean)$value;
                                }
                            )
                        );
                    break;

                case FormField::TYPE_HIDDEN:

                    $builder->add(
                        $field->getColumnName(),
                        HiddenType::class,
                        [
                            'label' => false,
                            'required' => $field->getRequired(),
                            'disabled' => $field->getDisabled(),
                            'data' => $this->getDefaultValue($data, $field),
                            'trim' => !$isDisableTrim
                        ]
                    );

                    $builder->get($field->getColumnName())
                        ->addModelTransformer(
                            new CallbackTransformer(
                                function ($value) {
                                    return $value;
                                },
                                function ($value) {
                                    if ($value == '') {
                                        return '###NULL###';
                                    }

                                    return $value;
                                }
                            )
                        );

                    break;

                case FormField::TYPE_DOCUMENT:

                    $builder->add(
                        $field->getColumnName() . '__files',
                        FileType::class,
                        [
                            'multiple' => true,
                            'label' => $field->getLabel(),
                            'required' => $field->getRequired(),
                            'disabled' => $field->getDisabled(),
                            'data' => $this->getDefaultValue($data, $field),
                            'attr' => [
                                'class' => 'advanced-upload'
                            ],
                        ]
                    )
                        ->add(
                            $field->getColumnName(),
                            HiddenType::class,
                            [
                                'label' => false,
                                'required' => false,
                                'trim' => !$isDisableTrim
                            ]
                        );

                    $builder->get($field->getColumnName())
                        ->addModelTransformer(
                            new CallbackTransformer(
                                function ($value) {
                                    return $value;
                                },
                                function ($value) {
                                    if ($value == '') {
                                        return '###NULL###';
                                    }

                                    return $value;
                                }
                            )
                        );

                    break;

                case FormField::TYPE_WORD:

                    $template = '';
                    $wordTemplate = $field->getWordTemplate();

                    if ($data && isset($data[$field->getColumnName()])) {
                        $template = $wordParser->parseWord($data[$field->getColumnName()], $data);
                    } else if ($wordTemplate) {

                        if ($wordParser) {
                            if ($data) {
                                $template = $wordParser->parseWord($wordTemplate, $data);
                            } else {
                                $template = $wordTemplate;
                            }

                            if ($params) {
                                $template = $wordParser->parseWord($template, $params);
                            }
                        } else {
                            $template = $wordTemplate;
                        }

                        if ($data && isset($data[$field->getColumnName()])) {
                            if (!$data[$field->getColumnName()]) {
                                $data[$field->getColumnName()] = $template;
                                $builder->setData($data);
                            }
                        }

                    }

                    $builder->add(
                        $field->getColumnName(),
                        HiddenType::class,
                        [
                            'label' => $field->getLabel(),
                            'required' => $field->getRequired(),
                            'disabled' => $field->getDisabled(),
                            'trim' => !$isDisableTrim,
                            'attr' => [
                                'class' => 'word-input'
                            ],
                            'data' => ($template) ? $template : $this->getDefaultValue($data, $field),
                        ]
                    );

                    break;

                default:

                    $typeClassName = ucfirst($field->getType()) . 'Type';

                    $typeClass = '\\Symfony\\Component\\Form\\Extension\\Core\\Type\\' . $typeClassName;

                    $builder->add(
                        $field->getColumnName(),
                        $typeClass,
                        [
                            'label' => $field->getLabel(),
                            'required' => $field->getRequired(),
                            'disabled' => $field->getDisabled(),
                            'attr' => ['class' => $widgetClass],
                            'data' => $this->getDefaultValue($data, $field),
                            'trim' => !$isDisableTrim
                        ]
                    );
            }


            if ($options['batch']) {

                $builder->add(
                    'set-' . $field->getColumnName(),
                    CheckboxType::class,
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['class' => 'batch-edit-checkbox'],
                    ]
                );
            }
        }

        if (!$idDefined) {

            $builder->add(
                $idName,
                HiddenType::class,
                [
                    'label' => false,
                    'required' => false,
                    'data' => $id,
                ]
            );

        }

//        $builder->addEventListener(FormEvents::PRE_SUBMIT, array(
//            $this, 'onPreSubmitDataEvent')
//        );

    }

    protected function getDefaultValue($data, FormField $field)
    {
        $fieldData = $field->getDefaultValue();

        if (isset($data) && array_key_exists($field->getColumnName(), $data)) {
            $fieldData = $data[$field->getColumnName()];
        }

        return $fieldData;
    }

    protected function selectQueryToArray($select, $toolQueryManager, $params = [])
    {
        $choices = [];
        if (strpos($select, '|') === false && $toolQueryManager) {

            try {
                $results = $toolQueryManager->executeCustomQuery($select, true, NULL, false, $params);


                if ($results) {
                    $choices[''] = '';
                    foreach ($results as $result) {
                        if (is_array($result)) {
                            $keys = array_keys($result);
                            $value = $keys[0];
                            if (count($result) > 1) {
                                $label = $keys[1];
                            } else {
                                $label = $value;
                            }

                            $choices[$result[$label]] = $result[$value];
                        }
                    }
                }
            } catch (\PDOException $e) {
                $choices = [];
            }


        } else {
            $options = explode('|', $select);
            $choices[''] = '';
            foreach ($options as $option) {
                $choices[$option] = $option;
            }
        }

        return $choices;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'fields' => null,
                'id' => null,
                'idName' => 'id',
                'batch' => false,
                'toolQueryManager' => null,
                'wordParser' => null,
                'csrf_protection' => false,
                'allow_extra_fields' => true,
                'dateParser' => null,
                'params' => [],
                'entity_manager' => false,
                'query_parser' => false,
                'isDisableTrim' => false
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function onPreSubmitEventWithRelatedField(FormEvent $event, array $options)
    {

        $em = $options['entity_manager'];
        $form = $event->getForm()->getParent();
        $fieldsAll = $form->getData();
        $fieldsAllEntity = $options['fields'];
        $relatedFieldsList = [];
        /** @var FormField $field */
        foreach ($fieldsAllEntity as $field) {
            if ($field->getType() == FormField::TYPE_RELATION) {
                $relatedFieldsList[] = $field;
            }
        }

        $parser = $options['query_parser'];

        if($parser) {
            foreach ($relatedFieldsList as $field) {

                $query = $field->getQuery(); //query

                $params = $parser->getParameters($query);

                foreach ($params as $param) {
                    if (in_array($param, array_keys($fieldsAll))) {
                        if (empty($fieldsAll[$param])) {
                            $fieldsAll[$param] = false;
                        }
                    } else {
                        $fieldsAll[$param] = false;
                    }
                }

                $query = $parser->parseQuery($query, $fieldsAll);

                $connection = $em->getConnection();
                $stmt = $connection->prepare($query);
                $stmt->execute();
                $choices = $stmt->fetchAll();
                $choicesAdapted = [];
                foreach ($choices as $option) {
                    $choicesAdapted[$option['name']] = $option['id'];
                }

                $form->remove($field->getColumnName());
                $form->add($field->getColumnName(), ChoiceType::class,
                    [
                        'label' => $field->getLabel(),
                        'required' => $field->getRequired(),
                        'disabled' => $field->getDisabled(),
                        'attr' => [
                            'class' => 'select2-deselect dontlook',

                        ],
                        'multiple' => 0,
                        'choices' => $choicesAdapted,
                        'data' => $fieldsAll[$field->getColumnName()] ?? false
                    ]
                );

            }
        }
    }
}