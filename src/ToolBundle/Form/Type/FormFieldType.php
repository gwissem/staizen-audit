<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use ToolBundle\Entity\FormField;

class FormFieldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [

                    'fields' => [
                        'label' => [
                            'field_type' => TextType::class,
                            'label' => 'Opis (label)',
                            'required' => true,
                            'constraints' => [new NotBlank()],
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                    //'excluded_fields' => ['slug', 'subqueryQescription', 'createdAt', 'updatedAt'],
                ]
            )
            ->add(
                'columnName',
                TextType::class,
                array(
                    'label' => 'Kolumna',
                )
            )
            ->add(
                'required',
                CheckboxType::class,
                array(
                    'label' => 'Czy wymagane?',
                    'required' => false,
                )
            )
            ->add(
                'disabled',
                CheckboxType::class,
                array(
                    'label' => 'Czy zablokowane do edycji?',
                    'required' => false,
                )
            )
            ->add(
                'type',
                ChoiceType::class,
                array(
                    'label' => 'Typ wartości',
                    'choices' => [
                        'Tekst' => FormField::TYPE_TEXT,
                        'Liczba' => FormField::TYPE_NUMBER,
                        'Data' => FormField::TYPE_DATE,
                        'Data i Godzina' => FormField::TYPE_DATETIME,
                        'Select' => FormField::TYPE_SELECT,
                        'Multiselect' => FormField::TYPE_MULTISELECT,
                        'Textarea' => FormField::TYPE_TEXTAREA,
                        'Dokument' => FormField::TYPE_DOCUMENT,
                        'Word' => FormField::TYPE_WORD,
                        'Checkbox' => FormField::TYPE_CHECKBOX,
                        'Ukryte' => FormField::TYPE_HIDDEN,
                        'Zależne' => FormField::TYPE_RELATION,
                    ],
                    'attr' => array(
                        'id' => 'fitler_type',
                    ),
                )
            )
            ->add(
                'defaultValue',
                TextType::class,
                array(
                    'label' => 'default_value',
                    'required' => false,
                )
            );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPresetDataEvent'));
        $builder->get('type')->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmitEvent'));
    }

    function onPresetDataEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $type = $data->getType();
        $this->updateFormElements($form, $type, 'preSetData');
    }

    protected function updateFormElements($builder, $type, $eventType = 'preSetData')
    {

        if ($eventType == 'postSubmit') {
            $builder->remove('query');
            $builder->remove('wordTemplate');
        }


        if ($type == FormField::TYPE_SELECT || $type == FormField::TYPE_MULTISELECT) {
            $queryPlaceholder = '';

            $builder
                ->add(
                    'query',
                    TextareaType::class,
                    [
                        'label' => 'Opcje wyboru',
                        'required' => false,
                        'attr' => ['placeholder' => 'Wartości oddzielone znakiem | , lub zapytanie do bazy zdefiniowanej w narzędziu'],
                    ]
                );
        }
        elseif ($type == FormField::TYPE_WORD)
        {
            $builder->add(
                'wordTemplate',
                TextareaType::class,
                [
                    'label' => "Szablon dokumentu",
                    'required' => false,
                    'attr' => [
                        'class' => "tinymce-word",
                        "data-theme" => "atlas_theme"
                    ]
                ]
            );
        }elseif($type == FormField::TYPE_RELATION)
        {
            $builder
                ->add(
                'query',
                TextareaType::class,
                [
                    'label' => 'Kwerenda',
                    'required' => false,
                    'attr' => ['placeholder' => 'Wpisz zapytanie do bazy, mające pobrać wartości dla tego pola'],
                ]
            );
        }
    }

    function onPostSubmitEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $form->getData();

        $this->updateFormElements($form->getParent(), $data, 'postSubmit');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\FormField',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_form_field';
    }

}