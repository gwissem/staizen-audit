<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ToolBundle\Entity\Filter;
use ToolBundle\Entity\FormField;

class FormFieldExtendType extends FormFieldType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);

        $builder
            ->add(
                'addIt',
                CheckboxType::class,
                array(
                    'label' => 'Wygenerować?',
                    'mapped' => false,
                    'required' => false,
                    'data' => true
                )
            )
            ->remove('columnName')
            ->add(
                'columnName',
                TextType::class,
                array(
                    'label' => 'Kolumna',
                    'attr' => [
                        'readonly' => 'readonly'
                    ]
                )
            )
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\FormField',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_form_field_extend';
    }

}