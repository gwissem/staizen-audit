<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class FilterType extends AbstractType
{

    const FILTER_LIKE_PARSED = '%...%';
    const FILTER_NOT_LIKE_PARSED = '~%...%';
    const FILTER_EMPTY = "=''";
    const FILTER_NOT_EMPTY = "~=''";

    public function comaSeparatedToArray($string)
    {
        if(is_array($string)) {
            return $string;
        }
        return explode(',', preg_replace('/\s*([\s,])\s*/', '\1', $string));

    }

    protected function getOperatorChoices($filterType = 'text')
    {
        if ($filterType == 'date' || $filterType == 'number') {
            $operators = [
                '=' => '=',
                '!=' => '!=',
                '>' => '>',
                '>=' => '>=',
                '<' => '<',
                '<=' => '<=',
            ];
        } else if ($filterType == 'date1') {
            $operators = [
                '>' => '>',
                '>=' => '>=',
            ];
        } else if ($filterType == 'date2') {
            $operators = [
                '<' => '<',
                '<=' => '<=',
            ];
//        } else if ($filterType == 'select' || $filterType == 'multiselect') {
//            $operators = [
//                '=' => '=',
//            ];
        } else {
            $operators = [
                self::FILTER_LIKE_PARSED => self::FILTER_LIKE_PARSED,
                self::FILTER_NOT_LIKE_PARSED => self::FILTER_NOT_LIKE_PARSED,
                '=' => '=',
                '!=' => '!=',
                self::FILTER_EMPTY => self::FILTER_EMPTY,
                self::FILTER_NOT_EMPTY => self::FILTER_NOT_EMPTY,
            ];
        }

        return $operators;
    }

}