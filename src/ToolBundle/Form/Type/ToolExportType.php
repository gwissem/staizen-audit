<?php

namespace ToolBundle\Form\Type;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use ToolBundle\Entity\ToolGroup;
use WebBundle\Form\Type\TranslationFormType;

class ToolExportType extends TranslationFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add(
                'exportType',
                ChoiceType::class,
                [
                    'choices' => [
                        'kopiowanie w aplikacji' => 'copy',
                        'eksport do JSON' => 'export',
                    ],
                    'required' => true,
                ]
            )
            ->add(
                'definition',
                CheckboxType::class,
                [
                    'label' => 'Definicja',
                    'required' => true,
                    'data' => true,
                    'attr' => [
                        'disabled' => 'disabled',
                    ],
                ]
            )
            ->add(
                'filter',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Filtry',
                    'data' => true,
                ]
            )
            ->add(
                'style',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Style',
                    'data' => true,
                ]
            )
            ->add(
                'edit',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Edycja',
                    'data' => true,
                ]
            )
            ->add(
                'action',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Akcje',
                    'data' => true,
                ]
            )
            ->add(
                'permission',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Uprawnienia',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

}