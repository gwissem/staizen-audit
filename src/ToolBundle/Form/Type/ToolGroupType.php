<?php

namespace ToolBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Validator\Constraints\NotBlank;
use ToolBundle\Entity\ToolGroupTranslation;

class ToolGroupType extends AbstractType
{

    /** @var EntityManager $em */
    protected $em;

    /** @var  DataCollectorTranslator $translator */
    protected $translator;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->em = $options['em'];
        $this->translator = $options['translator'];

        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [

                    'fields' => [
                        'name' => [
                            'field_type' => TextType::class,
                            'label' => 'Nazwa grupy',
                            'required' => true,
                            'constraints' => [new NotBlank()],
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                    //'excluded_fields' => ['slug', 'subqueryQescription', 'createdAt', 'updatedAt'],
                ]
            );

        $builder->get('translations')->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmitTranslationsEvent'));
    }


    /**
     * Sprawdza czy istnieją grupy o tych samych nazwach
     * @param FormEvent $event
     */

    public function onPostSubmitTranslationsEvent(FormEvent $event)
    {
        if($this->em) {

            $form = $event->getForm();
            $arrayTranslations = $form->getData();

            $repo = $this->em->getRepository('ToolBundle:ToolGroupTranslation');

            if(!empty($arrayTranslations)) {

                /** @var ToolGroupTranslation $arrayTranslation */
                foreach ($arrayTranslations as $arrayTranslation) {

                    $tgt = $repo->findBy(['name' => $arrayTranslation->getName(), 'locale' => $arrayTranslation->getLocale()]);

                    if(!empty($tgt)) {

                        $msg = $this->translator->trans('tool_groups.name_already_exists', [
                                '%name%' => $arrayTranslation->getName()
                            ], 'messages'
                        );

                        $form->addError(new FormError($msg));
                    }
                }
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\ToolGroup',
                'translator' => null,
                'em' => null
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_group';
    }

}