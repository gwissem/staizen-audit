<?php

namespace ToolBundle\Form\Type;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use ToolBundle\Entity\ToolGroup;
use WebBundle\Form\Type\TranslationFormType;

class ToolType extends TranslationFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [

                    'fields' => [
                        'name' => [
                            'field_type' => TextType::class,
                            'label' => 'Nazwa narzędzia',
                            'required' => true,
                            'constraints' => [new NotBlank()],
                        ],
                        'description' => [
                            'field_type' => TextareaType::class,
                            'label' => 'Opis',
                            'required' => false,
                            'attr' => [
                                'class' => "tinymce-word",
                                "data-theme" => "atlas_simple",
                            ],
                        ],
                        'subQueryDescription' => ['display' => false],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                        'slug' => ['display' => false],
                    ],
                    'label' => false,
                    //'excluded_fields' => ['slug', 'subqueryQescription', 'createdAt', 'updatedAt'],
                ]
            )
            /*->add(
                'name',
                TextType::class,
                array(
                    'label' => 'Nazwa narzędzia',
                )
            )*/
            ->add(
                'toolGroup',
                EntityType::class,
                array(
                    'label' => 'Grupa narzędzi',
                    'required' => false,
                    'class' => ToolGroup::class,
                    'placeholder' => 'Wybierz z listy',
                    'attr' => array(
                        'class' => 'select2',
                    ),
                )
            )
            ->add(
                'connectionString',
                TextType::class,
                array(
                    'required' => false,
                    'label' => 'Parametry połączenia',
                    'attr' => [
                        'placeholder' => 'sql;host;port;dbname;user;password',
                    ],
                )
            )
            ->add(
                'query',
                TextareaType::class,
                array(
                    'label' => 'Treść zapytania',
                    'required' => false,
                    'attr' => array(
                        'rows' => 6,
                        'class' => 'sql-codemirror'
                    ),
                )
            )
            ->add(
                'ordering',
                TextType::class,
                array(
                    'label' => 'Sortowanie',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'Np: id ASC',
                    ),
                )
            )
            ->add(
                'subQuery',
                TextareaType::class,
                array(
                    'label' => 'Opcjonalne podzapytanie',
                    'required' => false,
                    'attr' => array(
                        'rows' => 6,
                        'class' => 'sql-codemirror'
                    ),
                )
            )
            ->add(
                'icon',
                TextType::class,
                array(
                    'required' => false,
                    'label' => 'Ikona',
                )
            )
            ->add(
                'color',
                TextType::class,
                array(
                    'required' => false,
                    'label' => 'Kolor',
                )
            )
            ->add(
                'heightOfRow',
                IntegerType::class,
                array(
                    'required' => false,
                    'label' => 'Wysokość wiersza (px)',
                )
            )
            ->add('liveReport',
                CheckboxType::class,
                array(
                    'required' =>false,
                    'label' => 'Raport Live'
                ))
            ->
            add(
                'allowForGuest',
                CheckboxType::class,
                array(
                    'required' => false,
                    'label' => 'Udostępnienie dla gości',
                )
            )
            ->add(
                'paginationLimit',
                TextType::class,
                array(
                    'required' => false,
                    'label' => 'Liczba paginowanych wierszy',
                )
            )
            ->add(
                'count',
                CheckboxType::class,
                array(
                    'required' => false,
                    'label' => 'Zliczanie wierszy',
                )
            )
            ->add(
                'showOrdinal',
                CheckboxType::class,
                array(
                    'required' => false,
                    'label' => 'Liczba porządkowa',
                )
            )
            ->add(
                'thirdPartyToolUrl',
                TextType::class,
                array(
                    'required' => false,
                    'label' => 'Url zewnętrznego narzędzia',
                    'constraints' => [new Url()]
                )
            )
            ->add(
                'disableTrim',
                CheckboxType::class,
                array(
                    'required' => false,
                    'label' => 'Czy wyłączyć usuwanie białych znaków przy zapisywaniu danych?'
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\Tool',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool';
    }

}