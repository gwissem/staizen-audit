<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ToolBundle\Entity\Filter;

class UserFilterType extends FilterType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->setMethod('GET');

        $filters = $options['filters'];
        $requestFilters = $options['requestFilters'];
        $toolQueryManager = $options['toolQueryManager'];

        if ($filters) {
            /** @var Filter $filter */
            foreach ($filters as $filter) {

//                if(!$filter->isDisplay()) continue;

                $filterType = $filter->getType();
                $widgetClass = ($filterType === 'dates' || $filterType === 'date') ? 'date-time-picker' : '';
                $readonly = false;

                if (!empty($requestFilters)) {
                    if (array_key_exists($filter->getColumnName().'--ro', $requestFilters)) {
                        $readonly = 'readonly';
                        $widgetClass .= ' readonly';
                    }
                }

                switch ($filterType) {
                    case 'dates':
                        $builder->add(
                            $filter->getColumnName().'--op',
                            HiddenType::class,
                            [
                                'label' => false,
                                'data' => $filter->getDefaultOperator(),
                                'required' => true,
                            ]
                        );
                        $builder->add(
                            $filter->getColumnName(),
                            TextType::class,
                            [
                                'label' => false,
                                'required' => false,
                                'attr' => [
                                    'placeholder' => "od",
                                    'class' => $widgetClass,
                                    'readonly' => $readonly,
                                ],
                                'data' => $this->getControlValue($filter, $options['requestFilters']),
                            ]
                        );
                        $builder->add(
                            $filter->getColumnName().'--to--op',
                            HiddenType::class,
                            [
                                'label' => false,
                                'data' => $filter->getSecondDefaultOperator(),
                                'required' => true,
                            ]
                        );
                        $builder->add(
                            $filter->getColumnName().'--to',
                            TextType::class,
                            [
                                'label' => false,
                                'required' => false,
                                'attr' => [
                                    'placeholder' => "do",
                                    'class' => $widgetClass,
                                    'readonly' => $readonly,
                                ],
                                'data' => $filter->getSecondDefaultValue(),
                            ]
                        );
                        break;
                    case 'select':
                    case 'multiselect':
                        if ($filterType == 'multiselect') {
                            $data = $this->comaSeparatedToArray($this->getControlValue($filter, $options['requestFilters']));
                        } else {
                            $data = $this->getControlValue($filter, $options['requestFilters']);
                        }

                        $builder->add(
                            $filter->getColumnName().'--op',
                            HiddenType::class,
                            [
                                'label' => false,
                                'data' => $filter->getDefaultOperator(),
                                'required' => true,
                            ]
                        );
                        $builder->add(
                            $filter->getColumnName(),
                            ChoiceType::class,
                            [
                                'label' => false,
                                'required' => false,
                                'attr' => [
                                    'class' => $widgetClass.' select2-deselect',
                                    'readonly' => $readonly,
                                ],
                                'choices' => $this->selectQueryToArray($filter->getQuery(), $toolQueryManager),
                                'data' => $data,
                                'multiple' => $filter->getType() == 'multiselect',
                            ]
                        );

                    if ($filter->getPointedColumnName()) {
                        $builder->add(
                            $filter->getColumnName().'--pointed',
                            HiddenType::class,
                            [
                                'label' => false,
                                'data' => $filter->getPointedColumnName(),
                            ]
                        );
                    }

                        break;
                    default:
                        $builder->add(
                            $filter->getColumnName().'--op',
                            ChoiceType::class,
                            [
                                'choices' => $this->getOperatorChoices($filter->getType()),
                                'label' => false,
                                'required' => false,
                                'attr' => [
//                                    'placeholder' => $filter->getLabel()
                                ],
                                'data' => $filter->getDefaultOperator(),
                                'required' => true,
                            ]
                        );
                        
                        $builder->add(
                            $filter->getColumnName(),
                            TextType::class,
                            [
                                'label' => false,
                                'required' => false,
                                'attr' => [
//                                    'placeholder' => $filter->getLabel(),
                                    'class' => $widgetClass,
                                    'readonly' => $readonly,
                                ],
                                'data' => $this->getControlValue($filter, $options['requestFilters']),
                            ]
                        );
                        break;
                }
            }
        }
    }

    protected function getControlValue(Filter $filter, $requestFilters){
        return array_key_exists($filter->getColumnName(), $requestFilters) ? $requestFilters[$filter->getColumnName()] : $filter->getDefaultValue();
    }

    protected function selectQueryToArray($select, $toolQueryManager)
    {
        $choices = [];
        if (strpos($select, '|') === false && $toolQueryManager) {

            try {
                $results = $toolQueryManager->executeCustomQuery($select, true);

                if ($results) {
                    foreach ($results as $result) {
                        if (is_array($result)) {
                            $keys = array_keys($result);
                            $value = $keys[0];
                            if (count($result) > 1) {
                                $label = $keys[1];
                            } else {
                                $label = $value;
                            }
                            $choices[$result[$label]] = $result[$value];
                        }
                    }
                }
            } catch (\PDOException $e) {
                $choices = [];
            }


        } else {
            $options = explode('|', $select);
            foreach ($options as $option) {
                $choices[$option] = $option;
            }
        }

        return $choices;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'filters' => null,
                'requestFilters' => null,
                'toolQueryManager' => null,
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}