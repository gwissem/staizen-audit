<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFilterSetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user = $options['user'];

        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Nazwa zestawu filtrów',
                    ),
                )
            )
            ->add(
                'tool',
                HiddenType::class,
                array(
                    'required' => true,
                    'label' => false,
                )
            )
            ->
            add(
                'filtersString',
                HiddenType::class,
                array(
                    'required' => false,
                    'label' => false,
                )
            );
        if ($user && $user->hasRole('ROLE_ADMIN')) {
            $builder->add(
                'isPublic',
                CheckboxType::class,
                array(
                    'required' => false,
                    'label' => 'Oznacz jako publiczny',
                )
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\UserFilterSet',
                'user' => null,
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_user_filter_set';
    }

}