<?php

namespace ToolBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\Group;

class ToolPermissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'group',
                EntityType::class,
                array(
                    'label' => 'Grupa użytkowników',
                    'class' => Group::class,
                    'placeholder' => 'Wybierz z listy',
                    'attr' => array(
                        'class' => 'select2',
                    ),
                )
            )
            ->add(
                'canRead',
                CheckboxType::class,
                array(
                    'required' => false,
                    'label' => 'READ',
                )
            )
            ->add(
                'canWrite',
                CheckboxType::class,
                array(
                    'required' => false,
                    'label' => 'WRITE',
                )
            )
            ->add(
                'canBatchWrite',
                CheckboxType::class,
                array(
                    'required' => false,
                    'label' => 'BATCH-WRITE',
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\ToolPermission',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_permission';
    }

}