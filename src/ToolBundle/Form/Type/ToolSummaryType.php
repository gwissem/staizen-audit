<?php

namespace ToolBundle\Form\Type;

use Doctrine\DBAL\Types\BooleanType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolGroup;
use ToolBundle\Entity\ToolSummary;
use UserBundle\Entity\User;
use UserBundle\UserBundle;

class ToolSummaryType extends AbstractType
{
    const COUNT_TYPE = 'count';
    const SUM_TYPE = 'sum';

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * ToolSummaryType constructor.
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $entity = $builder->getData();
        $columns = (($entity instanceof ToolSummary) && $entity->getTool() && $entity->getTool()) ? $entity->getTool()->columnsArray : [] ;

        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [
                    'fields' => [
                        'description' => [
                            'field_type' => TextType::class,
                            'label' => 'Opis',
                            'required' => false,
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                ]
            )
            ->add('summaryType', ChoiceType::class, array(
                'label' => 'Typ podsumowania',
                'mapped' => false,
                'choices' => [
                    'Zliczenie wierszy' => self::COUNT_TYPE,
                    'Suma kolumny' => self::SUM_TYPE,
                ],
            ));

            /** Żeby po stronie ToolSummaryController nie pobierać znowu nazw kolumn */
            if($columns)
            {
                $builder->add('columnName', ChoiceType::class, array(
                    'label' => 'Nazwa kolumny',
                    'required' => false,
                    'choices' => $columns,
                    'attr' => [
                        'disabled' => 'disabled'
                    ]
                ));
            }
            else {
                $builder->add('columnName', TextType::class, array(
                    'label' => 'Nazwa kolumny',
                    'required' => false,
                    'attr' => [
                        'disabled' => 'disabled'
                    ]
                ));
            }

//
            $builder->add('filters', HiddenType::class, array(

            ))
        ;

        /** @var User $user */
        $user = $this->getUser();
        if ($user instanceof User && $user->hasRole(User::ROLE_ADMIN))
        {
            $builder
                ->add('globalSummary', CheckboxType::class, array(
                    'label' => 'Globalne podsumowanie',
                    'mapped' => false,
                    'required' => false
                ))
                ->add('userGroups', EntityType::class, array(
                    'label' => 'Grupy użytkowników',
                    'multiple' => true,
                    'class' => 'UserBundle\Entity\Group',
                    'required' => false,
                    'attr' => [
                        'class' => 'select2-deselect'
                    ]
                ))
                ->add('perUser', null, [
                    'label' => 'Indywidualne dla każdego użytkownika',
                    'required' => false
                ])
            ;
        }

        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    private function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    public function onPreSubmit(FormEvent $event)
    {
        $toolSummary = $event->getData();
        
        if($toolSummary['summaryType'] == self::COUNT_TYPE) $toolSummary['columnName'] = NULL;
        if(!$toolSummary['filters']) $toolSummary['filters'] = '';

        $event->setData($toolSummary);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\ToolSummary',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_summary';
    }

}