<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use WebBundle\Form\Type\TranslationFormType;

class ToolImportType extends TranslationFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'file',
                FileType::class,
                [
                    'constraints' => [
                        new File(
                            [
                                'mimeTypes' => [
                                    'application/json',
                                    'application/octet-stream',
                                    'text/plain',
                                ],
                            ]
                        ),
                    ],
                    'label' => 'Plik JSON',
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

}