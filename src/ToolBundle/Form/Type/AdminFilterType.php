<?php

namespace ToolBundle\Form\Type;

use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use ToolBundle\Entity\Filter;

class AdminFilterType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [

                    'fields' => [
                        'label' => [
                            'field_type' => TextType::class,
                            'label' => 'Opis',
                            'required' => true,
                            'constraints' => [new NotBlank()],
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                ]
            )
            ->add(
                'columnName',
                TextType::class,
                array(
                    'label' => 'Kolumna',
                )
            )
            ->add(
                'display',
                null,
                array(
                    'label' => 'Czy wyświetlać?',
                )
            )
            ->add(
                'type',
                ChoiceType::class,
                array(
                    'label' => 'Typ wartości',
                    'choices' => [
                        'Tekst' => Filter::TYPE_TEXT,
                        'Liczba' => Filter::TYPE_NUMBER,
                        'Data' => Filter::TYPE_DATE,
                        'Zakres dat' => Filter::TYPE_DATERANGE,
                        'Select' => Filter::TYPE_SELECT,
                        'Multiselect' => Filter::TYPE_MULTISELECT,
                    ],
                    'attr' => array(
                        'id' => 'fitler_type',
                    ),
                )
            )
            ->add(
                'defaultOperator',
                ChoiceType::class,
                array(
                    'label' => 'Dom. operator',
                    'choices' => $this->getOperatorChoices('number'),
                )
            )
            ->add(
                'defaultValue',
                TextType::class,
                array(
                    'label' => 'Dom. wartość',
                    'required' => false,
                    'attr' => array(
                        'class' => '',
                    ),
                )
            );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPresetDataEvent'));
        $builder->get('type')->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmitEvent'));
    }

    function onPresetDataEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $type = $data->getType();
        $this->updateFormElements($form, $type, 'preSetData');
    }

    protected function updateFormElements($builder, $type, $eventType = 'preSetData')
    {

        if ($eventType == 'postSubmit') {
            $builder->remove('query');
            $builder->remove('secondDefaultOperator');
            $builder->remove('secondDefaultValue');
            $builder->remove('pointedColumnName');
        }

        if ($type == Filter::TYPE_NUMBER) {
            $builder->add(
                'defaultOperator',
                ChoiceType::class,
                array(
                    'label' => 'Dom. operator',
                    'choices' => $this->getOperatorChoices('number'),
                )
            );
        } else if ($type == Filter::TYPE_TEXT) {
            $builder->add(
                'defaultOperator',
                ChoiceType::class,
                array(
                    'label' => 'Dom. operator',
                    'choices' => $this->getOperatorChoices(),
                )
            );
        } else if ($type == Filter::TYPE_DATE) {
            $builder
                ->add(
                    'defaultOperator',
                    ChoiceType::class,
                    array(
                        'label' => 'Dom. operator i wartość',
                        'choices' => $this->getOperatorChoices('date'),
                    )
                )
                ->add(
                'defaultValue',
                TextType::class,
                array(
                    'label' => 'Dom. wartość',
                    'required' => false,
                    'attr' => array(
                        'class' => 'date-time-picker',
                    ),
                )
            );
        } else if ($type == Filter::TYPE_DATERANGE) {
                $builder
                    ->add(
                        'defaultOperator',
                        ChoiceType::class,
                        array(
                            'label' => 'Dom. operator',
                            'choices' => $this->getOperatorChoices('date1'),
                        )
                    )
                    ->add(
                        'defaultValue',
                        TextType::class,
                        array(
                            'label' => 'Dom. wartość',
                            'required' => false,
                            'attr' => array(
                                'class' => 'date-time-picker',
                            ),
                        )
                    )
                    ->add(
                        'secondDefaultOperator',
                        ChoiceType::class,
                        array(
                            'label' => 'Dom. operator 2',
                            'choices' => $this->getOperatorChoices('date2'),
                        )
                    )
                    ->add(
                        'secondDefaultValue',
                        TextType::class,
                        array(
                            'label' => 'Dom. wartość 2',
                            'required' => false,
                            'attr' => array(
                                'class' => 'date-time-picker',
                            ),
                        )
                    );
        } elseif ($type == Filter::TYPE_SELECT || $type == Filter::TYPE_MULTISELECT) {
            $queryPlaceholder = 'Wartości oddzielone znakiem | , lub zapytanie do bazy zdefiniowanej w narzędziu';
            $defaultValuePlaceholder = '';
            if ($type == Filter::TYPE_MULTISELECT) {
                $defaultValuePlaceholder = 'Wartości oddzielone przecinkami';
                $builder
                    ->add(
                        'pointedColumnName',
                        TextType::class,
                        array(
                            'label' => 'Kolumna dla wartości',
                            'required' => false
                        )
                    );
            }
            $builder
                ->add(
                    'defaultOperator',
                    ChoiceType::class,
                    array(
                        'label' => 'Dom. operator',
                        'choices' => $this->getOperatorChoices('select'),
                    )
                )
                ->add(
                    'defaultValue',
                    TextType::class,
                    array(
                        'label' => 'Dom. wartość',
                        'required' => false,
                        'attr' => ['placeholder' => $defaultValuePlaceholder],
                    )
                )
                ->add(
                    'query',
                    TextareaType::class,
                    array(
                        'label' => 'Opcje wyboru',
                        'required' => false,
                        'attr' => ['placeholder' => $queryPlaceholder],
                    )
                );
            }
    }

    function onPostSubmitEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $form->getData();

        $this->updateFormElements($form->getParent(), $data, 'postSubmit');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\Filter',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_filter';
    }

    private function getTypeChoices()
    {
        return array();
    }

}