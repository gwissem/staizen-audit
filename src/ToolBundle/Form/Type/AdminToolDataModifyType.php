<?php

namespace ToolBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ToolBundle\Entity\FormField;
use ToolBundle\Entity\Tool;

class AdminToolDataModifyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Tool $self */
        $self = $builder->getData();

        $formFields = [
            '' => ''
        ];

        /** @var FormField $formField */
        foreach ($self->getFormFields() as $formField) {
            $formFields[$formField->getColumnName()] = $formField->getColumnName();
        }

        $builder
            ->add(
                'editIdColumn',
                TextType::class,
                array(
                    'label' => 'nazwa kolumny id narzędzia',
                    'required' => true,
                    'attr' => array(
                        'placeholder' => 'np: id',
                    ),
                )
            )
            ->add(
                'insertQuery',
                TextareaType::class,
                array(
                    'label' => 'zapytanie INSERT',
                    'required' => false,
                    'attr' => array(
                        'rows' => 4,
                        'class' => 'sql-codemirror'
                    ),
                )
            )
            ->add(
                'updateQuery',
                TextareaType::class,
                array(
                    'label' => 'zapytanie UPDATE',
                    'required' => false,
                    'attr' => array(
                        'rows' => 4,
                        'class' => 'sql-codemirror'
                    ),
                )
            )
            ->add(
                'deleteQuery',
                TextareaType::class,
                array(
                    'label' => 'zapytanie DELETE',
                    'required' => false,
                    'attr' => array(
                        'rows' => 4,
                        'class' => 'sql-codemirror'
                    ),
                )
            )
            ->add(
                'editType',
                ChoiceType::class,
                array(
                    'choices' => [
                        'Okno modalne' => Tool::EDIT_TYPE_MODAL,
                        'W tej samej karcie' => Tool::EDIT_TYPE_PAGE,
                        'W nowej karcie' => Tool::EDIT_TYPE_NEW_TAB,
                    ],
                    'label' => 'tryb edycji',
                    'choice_label' => function ($value, $key, $index) {
                        return $key;
                    },
                    'choice_translation_domain' => 'messages',
                )
            )
            ->add(
                'columnForAutoComplete',
                ChoiceType::class,
                array(
                    'choices' => $formFields,
                    'label' => 'Pole do autouzupełniania formularza'
                )

            )
            ->add(
                'toolEditorTitle',
                TextType::class,
                [
                    'label' => 'tytuł edycji wiersza narzędzia',
                    'attr' => [
                        'placeholder' => 'e.g. {@XXX,YYY,ZZZ@}',
                    ],
                    'required' => false,
                ]
            )
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ToolBundle\Entity\Tool',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_tool_data_modify';
    }

}