<?php
namespace ToolBundle\Service;

use PHPExcel_Writer_CSV;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ToolDataXlsService
{

    const LIMIT_XLS_CELLROW = 100000000;

    /** @var  ContainerInterface */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function generateXlsResponse($tool, $table)
    {

        set_time_limit(0);
        ini_set('memory_limit', '2G');

        $largeFile = count($table['columns']) * count($table['rows']) > self::LIMIT_XLS_CELLROW;
        $fileType = $largeFile ? 'CSV' : 'Excel5';

        /** @var Style[] $styles */
        $styles = $tool->getStyles();

        foreach ($styles as $style) {
            foreach ($table['columns'] as $key => $column) {

                if ($column == $style->getColumnName()) {
                    if ($style->getHidden()) {
                        unset($table['columns'][$key]);
                        foreach ($table['rows'] as $key2 => $row) {
                            unset($table['rows'][$key2][$column]);
                        }
                        break;
                    }

                    if ($style->getAlias()) {
                        $table['columns'][$key] = $style->getAlias();
                    }

                    break;
                }
            }
        }

        /** Tworzenie XLS */

        $phpExcel = $this->container->get('phpexcel');
        $phpExcelObject = $phpExcel->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Atlas")
            ->setLastModifiedBy("Atlas")
            ->setTitle("Atlas - narzędzie")
            ->setSubject("Atlas - narzędzie")
            ->setDescription($tool->getDescription())
            ->setKeywords("narzędzie atlas")
            ->setCategory("- CATEGORY -");

        $phpExcelObject->setActiveSheetIndex(0);
        $activeSheet = $phpExcelObject->getActiveSheet();
        $activeSheet->getDefaultRowDimension()->setRowHeight(-1);
        $currentColumn = 0;
        $currentRow = 1;
        $columns = [];

        /** Ustawienie nazw kolumn */
        foreach ($table['columns'] as $column) {
            $newCol = $this->getLetterFromNumber($currentColumn);
            $columns[] = $newCol;
            $activeSheet->setCellValue($newCol.$currentRow, $column);
            if (!$largeFile) {
                $activeSheet->getColumnDimension($newCol)->setAutoSize(true);
                $activeSheet->getStyle($newCol.$currentRow)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '3B3F51'),
                        ),
                        'font' => array(
                            'color' => array('rgb' => 'FFFFFF'),
                        ),
                        'borders' => array(
                            'allborders' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => 'e7ecf1'),
                            ),
                        ),
                    )
                );
            }
            $currentColumn++;
        }

        /** Wstawienie danych */
        foreach ($table['rows'] as $key => $row) {
            $currentColumn = 0;
            $currentRow++;
            foreach ($row as $cell) {
                $activeSheet->setCellValue(
                    $columns[$currentColumn].$currentRow,
                    strip_tags(preg_replace('#<br\s*/?>#i', "\n", $cell))
                );
                $activeSheet->getStyle($columns[$currentColumn].$currentRow)->getAlignment()->setWrapText(true);
                if (!$largeFile) {
                    $color = ($currentRow % 2) ? 'fbfcfd' : 'ffffff';
                    $activeSheet->getStyle($columns[$currentColumn].$currentRow)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => $color),
                            ),
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array('rgb' => 'e7ecf1'),
                                ),
                            ),
                        )
                    );
                }
                $currentColumn++;
            }
        }

        $phpExcelObject->getActiveSheet()->setTitle(mb_substr($tool->getName(), 0, 30));
        $phpExcelObject->setActiveSheetIndex(0);
        /** @var PHPExcel_Writer_CSV $writer */
        $writer = $this->container->get('phpexcel')->createWriter($phpExcelObject, $fileType);

        if ($fileType == 'CSV') {
            $mimeType = 'text/csv';
            $extension = 'csv';
            $writer->setDelimiter(';');
        } else {
            $mimeType = 'text/vnd.ms-excel';
            $extension = 'xls';
        }

        $response = $this->container->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'tool_'.$tool->getId().'_'.date("Ymd").'.'.$extension
        );

        $response->headers->set('Content-Type', $mimeType.'; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        if ($fileType == 'CSV') {
            echo "\xEF\xBB\xBF"; // ADD BOM
        }

        return $response;
    }

    protected function getLetterFromNumber($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getLetterFromNumber($num2 - 1).$letter;
        } else {
            return $letter;
        }
    }
}
