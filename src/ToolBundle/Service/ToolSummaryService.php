<?php
namespace ToolBundle\Service;

use Doctrine\ORM\EntityManager;
use Predis\Client;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Stopwatch\Stopwatch;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolSummary;
use ToolBundle\Utils\ToolQueryManager;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class ToolSummaryService
{
    
    const SIDEBAR_NAV_ITEMS = 'sidebar-nav-items';

    /** @var  ContainerInterface */
    protected $container;

    /** @var  EntityManager */
    protected $em;

    /** @var  Request */
    protected $request;

    /** @var  User */
    protected $user;

    /** @var  Group[] */
    protected $userGroups;

    /** @var Logger */
    protected $logger;

    /**
     * UserPersonalizationService constructor.
     * @param ContainerInterface $container
     * @param EntityManager $entityManager
     * @param RequestStack $requestStack
     */
    public function __construct(ContainerInterface $container, EntityManager $entityManager, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->em = $entityManager;
        $this->request = $requestStack->getCurrentRequest();
        $this->logger = $this->container->get('monolog.logger.stopwatch');
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setGroups($groups)
    {
        $this->userGroups = $groups;
        return $this;
    }

    /**
     * @param Tool $tool
     * @param null $filters
     * @return \ToolBundle\Utils\ToolQueryManager
     */
    public function generateQuery(Tool $tool, $filters = null)
    {
        if(!$filters) $filters = $tool->getFormFilters();
        else $filters = $this->parseFilters($filters);
        
        $toolQueryManager = $this->container->get('tool.query_manager');
        $toolQueryManager->setParams($tool, $filters);
        $toolQueryManager->initQuery();
        
        return $toolQueryManager;
    }

    public function checkExistenceSummaryInUser(Tool $tool, $filters, $columnName){
        $toolSummary = $this->em->getRepository('ToolBundle:ToolSummary')->getSummaryTool($this->user, $tool, $filters, $columnName);
        
        return ($toolSummary) ? false : true;
    }

    public function checkExistenceSummaryInGroups(Tool $tool, $filters, $columnName){

        return $this->em->getRepository('ToolBundle:ToolSummary')->getSummaryToolAsGroup($this->userGroups, $tool, $filters, $columnName);

    }
    
    public function getSummaryTools($user)
    {
        if(!$user) $user = $this->user;

        $toolsSummary = $this->em->getRepository('ToolBundle:ToolSummary')->getSummaryToolOfUser($user);
        
        return ($toolsSummary) ? $toolsSummary : [];
    }

    public function execToolSummaryValue(ToolSummary $toolSummary, ToolQueryManager $toolQueryManager, $result = [])
    {
        $columnName = $toolSummary->getColumnName();

        if(!$result)
        {
            $watcher = new Stopwatch();
            $watcher->start('summaryTool');
            $unique = uniqid();

            $this->logger->info('WATCHER_START_EXEC_SUMMARY_TOOL',
                [
                    'uniqueId' => $unique,
                    'summaryToolId' => $toolSummary->getId()
                ]
            );

            try {

                $toolQueryManager->setParams($toolSummary->getTool(), $toolSummary->getParsedFilters());

                if($columnName) {
                    $subQuery = 'SELECT SUM(sq.'.$columnName.') AS result FROM (##QUERY##) AS sq';
                }
                else {
                    $subQuery = 'SELECT COUNT(*) as result FROM (##QUERY##) AS sq';
                }

                $toolQueryManager->initQuery([], false, $subQuery);

                $this->logSummaryWatcher($watcher, $toolSummary, $unique);

            }
            catch (\Exception $exception) {

                $this->logger->info('WATCHER_FAIL_EXEC_SUMMARY_TOOL',
                    [
                        'uniqueId' => $unique,
                        'summaryToolId' => $toolSummary->getId()
                    ]
                );

                throw $exception;
            }

            $result = $toolQueryManager->getResults();

        }

        if($result){

            $result = (is_array($result)) ? $result[0] : $result;

            if(isset($result['result'])) {
                $toolSummary->setValue($result['result']);
            }
            else {
                $toolSummary->setValue(0);
            }
        }
        else {
            $toolSummary->setValue(0);
        }
    }
    
    public function saveValueInRedis(ToolSummary $toolSummary, $hash = null)
    {
        $hash = $hash ?: $toolSummary->getHash();
        $this->container->get('snc_redis.default')->setex($hash, $this->container->getParameter('redis_ttl'), $toolSummary->getValue());
    }
    
    static function parseFilters($filters)
    {
        parse_str(Request::normalizeQueryString($filters), $filters);
        return $filters;
    }

    private function logSummaryWatcher(Stopwatch $stopwatch, ToolSummary $toolSummary, $unique) {

        $event = $stopwatch->stop('summaryTool');

        $data = [
            'uniqueId' => $unique,
            'summaryToolId' => $toolSummary->getId(),
            'all' => $event->getDuration(),
            'toolId' => $toolSummary->getTool()->getId(),
            'userId' => ($this->user) ? $this->user->getId() : null
        ];

        $this->logger->info('WATCHER_END_EXEC_SUMMARY_TOOL', $data);

    }
}