<?php

namespace ToolBundle\Service;

use Symfony\Component\Form\Form as Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use ToolBundle\Entity\FormField;
use ToolBundle\Entity\Tool;

class ValidatorService
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function validRequired(Form $form, Tool $tool) : bool
    {
        $valid = true;
        $formFields = $tool->getFormFields();

        foreach($formFields as $field) {
            if ($this->isTargetedRequired($form, $field)) {
                $form->get($field->getColumnName())->addError(
                    new FormError($this->translator->trans(/** @Ignore */(new NotBlank())->message, [], 'validators')
                ));
                $valid = false;
            }
            if ($this->isRequired($form, $field)) {
                $form->get($field->getColumnName())->addError(
                    new FormError($this->translator->trans(/** @Ignore */(new NotBlank())->message, [], 'validators')
                ));
                $valid = false;
            }
        }

        return $valid;
    }

    public function isValid(Form $form, Tool $tool) : bool
    {
        if (!$this->validRequired($form, $tool)) {
            return false;
        }

        return true;
    }

    private function isTargetedRequired(Form $form, FormField $field) : bool
    {
        return ($field->getRequired()
            && ($form->get($field->getColumnName())->getData() === null
                || $form->get($field->getColumnName())->getData() === '')
            && (
                !empty($field->getTargetName()
                && $form->get($field->getTargetName())->getData() == $field->getTargetValue())
            ));
    }

    private function isRequired(Form $form, FormField $field) : bool
    {
        return empty($field->getTargetName()) && $field->getRequired()
            && ($form->get($field->getColumnName())->getData() === null
                || $form->get($field->getColumnName())->getData() === '');
    }
}