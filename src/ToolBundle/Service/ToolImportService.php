<?php
namespace ToolBundle\Service;

use Doctrine\ORM\EntityManager;
use ToolBundle\Entity\Filter;
use ToolBundle\Entity\Style;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolAction;
use ToolBundle\Entity\ToolTranslation;

class ToolImportService
{
    /** @var  EntityManager */
    protected $em;
    protected $translator;
    protected $locales;

    /**
     * UserPersonalizationService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, $translator, $locales)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->locales = $locales;
    }

    public function import(Tool $importedTool, $tool = null)
    {

        $em = $this->em;

        $userGroupRepository = $em->getRepository('UserBundle:Group');
        $toolGroupRepository = $em->getRepository('ToolBundle:ToolGroup');
        $actionRepository = $em->getRepository('ToolBundle:ToolAction');
        $toolRepository = $em->getRepository('ToolBundle:Tool');

        if (!$tool) {
            $tool = new Tool();
            $tool->setUniqueId($importedTool->getUniqueId());
        } else {
            $tool->removeElements('translations');
            $tool->removeElements('actions');
            $tool->removeElements('styles');
            $tool->removeElements('filters');
            $tool->removeElements('hasGroups');
            $tool->removeElements('permissions');

            $em->flush();
            $em->refresh($tool);
        }
        foreach ($importedTool->getTranslations() as $translation) {
            /** @var ToolTranslation $translation */
            $translation->setTranslatable($tool);
            $tool->addTranslation($translation);
        }

        foreach ($importedTool->getStyles() as $style) {
            /** @var Style $style */
            $linkedAction = $style->getAction();
            $editTool = $style->getEditTool();
            if ($linkedAction) {
                $linkedActionExists = $actionRepository->findOneBy(
                    ['uniqueId' => $linkedAction->getUniqueId()]
                );
                $style->setAction($linkedActionExists);
            }
            if ($editTool) {
                $editToolExists = $toolRepository->findOneBy(
                    ['uniqueId' => $editTool->getUniqueId()]
                );
                $style->setTool($editToolExists);
            }

            foreach ($style->getTranslations() as $translation) {
                $translation->setTranslatable($style);
            }

            $tool->addStyle($style);
        }

        if ($importedTool->getToolGroup()) {
            $toolGroup = $toolGroupRepository->findOneBy(
                ['uniqueId' => $importedTool->getToolGroup()->getUniqueId()]
            );

            foreach ($toolGroup->getTranslations() as $translation) {
                $translation->setTranslatable($toolGroup);
            }

            $tool->setToolGroup($toolGroup);
        }

        foreach ($importedTool->getHasGroups() as $importedUserGroupTool) {
            $userGroup = $userGroupRepository->findOneBy(
                ['uniqueId' => $importedUserGroupTool->getGroup()->getUniqueId()]
            );
            if ($userGroup) {
                $importedUserGroupTool->setGroup($userGroup);
                $tool->addHasGroup($importedUserGroupTool);
            } else {
                $tool->removeHasGroup($importedUserGroupTool);
            }
        }

        foreach ($importedTool->getPermissions() as $importedPermission) {
            /** @var ToolPermission $importedPermission */
            if ($importedPermission->getCanRead() || $importedPermission->getCanWrite()
                || $importedPermission->getCanBatchWrite()
            ) {
                $userGroup = $em->getRepository('UserBundle:Group')->findOneBy(
                    ['uniqueId' => $importedPermission->getGroup()->getUniqueId()]
                );
                if ($userGroup) {
                    $importedPermission->setGroup($userGroup);
                    $tool->addPermission($importedPermission);
                }
            }
        }

        foreach ($importedTool->getActions() as $action) {

            $actionExists = $actionRepository->findOneBy(
                ['uniqueId' => $action->getUniqueId()]
            );
            if (!$actionExists) {
                /** @var ToolAction $action */
                foreach ($action->getUserGroups() as $actionUserGroup) {
                    $userGroup = $em->getRepository('UserBundle:Group')->findOneBy(
                        ['uniqueId' => $actionUserGroup->getUniqueId()]
                    );
                    if ($userGroup) {
                        /** @var Group $actionUserGroup */
                        $action->addUserGroup($userGroup);
                    }
                    $action->removeUserGroup($actionUserGroup);
                }

                foreach ($action->getTranslations() as $translation) {
                    $translation->setTranslatable($action);
                }
                $tool->addAction($action);
            }
        }

        foreach ($importedTool->getFilters() as $filter) {

            /** @var Filter $filter */
            foreach ($filter->getTranslations() as $translation) {
                $translation->setTranslatable($filter);
            }
            $tool->addFilter($filter);
        }

        foreach ($importedTool->getFormFields() as $formField) {

            /** @var Filter $filter */
            foreach ($formField->getTranslations() as $translation) {
                $translation->setTranslatable($formField);
            }
            $tool->addFormField($formField);
        }

        $tool->updateValues($importedTool);

        $tool->setDeletedAt(null);

        $em->persist($tool);
        $em->flush();

        return $tool;
    }

}