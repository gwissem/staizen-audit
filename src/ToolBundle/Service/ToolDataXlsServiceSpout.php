<?php

namespace ToolBundle\Service;

use Box\Spout\Common\Type;
use Box\Spout\Writer\CSV\Writer;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;
use Box\Spout\Writer\Style\StyleBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Internal\Hydration\ObjectHydrator;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use ToolBundle\Entity\Style;
use ToolBundle\Entity\Tool;
use Utils\AtlasUtils;

class ToolDataXlsServiceSpout
{

    const LIMIT_XLS_CELLROW = 10000000;

    /** @var  ContainerInterface */
    protected $container;
    protected $em;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @param Tool $tool
     * @param $table
     * @return null|\Symfony\Component\HttpFoundation\Response
     * @throws \Box\Spout\Common\Exception\IOException
     */
    public function generateXlsResponse($tool, $table)
    {

        set_time_limit(0);
        ini_set('memory_limit', '2G');

        $largeFile = count($table['columns']) * count($table['rows']) > self::LIMIT_XLS_CELLROW;
        $fileType = $largeFile ? Type::CSV : Type::XLSX;

        /** @var Style[] $styles */
        $styles = $tool->getStyles();

        foreach ($styles as $style) {
            foreach ($table['columns'] as $key => $column) {

                if ($column == $style->getColumnName()) {
                    if ($style->getHidden()) {
                        unset($table['columns'][$key]);
                        foreach ($table['rows'] as $key2 => $row) {
                            unset($table['rows'][$key2][$column]);
                        }
                        break;
                    }

                    if ($style->getAlias()) {
                        $table['columns'][$key] = $style->getAlias();
                    }

                    break;
                }
            }
        }


        /** Tworzenie XLS */
//        mb_internal_encoding('UTF-8');
        $spout = $this->container->get('stev_spout.box_spout');

        $spoutWriter = $spout->initWriter($fileType);
        $spoutWriter->setShouldUseInlineStrings(true);
        $headerBorder = (new BorderBuilder())
            ->setBorderBottom('e7ecf1', Border::WIDTH_THIN, Border::STYLE_SOLID)
            ->setBorderLeft('e7ecf1', Border::WIDTH_THIN, Border::STYLE_SOLID)
            ->setBorderRight('e7ecf1', Border::WIDTH_THIN, Border::STYLE_SOLID)
            ->build();

        $styleHeaderColumns = (new StyleBuilder())
            ->setFontBold()
            ->setFontColor('FFFFFF')
            ->setBackgroundColor('3B3F51')
            ->setBorder($headerBorder)
            ->build();

        $showOrdinal = $tool->getShowOrdinal();

        if ($showOrdinal == 1) {
            $ordinalCount = 1;
            foreach ($table['rows'] as $keyRow => $row) {
                $row = ['LP' => strval($ordinalCount)] + $row;
                $table['rows'][$keyRow] = $row;
                $ordinalCount++;

            }
            array_unshift($table['columns'], 'LP');

        }


        $spoutWriter->openToBrowser($tool->getName() . '.' . $fileType);

        try {

            $spoutWriter->addRowWithStyle($table['columns'], $styleHeaderColumns);

            foreach ($table['rows'] as $row):
                foreach ($row as $key => $cell):
                    $row[$key] = strip_tags(preg_replace('#<br\s*/?>#i', "\n", $cell));
                endforeach;

                $spoutWriter->addRow($row);

            endforeach;

            $spoutWriter->close();
            exit();

        } catch (\Exception $exception) {
            $spoutWriter->close();
            return null;
        }

        $response = new \Symfony\Component\HttpFoundation\Response('', 200);
        return $response;
    }


    /**
     * @param Tool $tool
     * @param $table
     * @return null|\Symfony\Component\HttpFoundation\Response
     * @throws \Box\Spout\Common\Exception\IOException
     */
    public function generateCSVResponse($tool, $table)
    {

        set_time_limit(0);
        ini_set('memory_limit', '2G');

        $fileType = Type::CSV;

        /** @var Style[] $styles */
        $styles = $tool->getStyles();

        foreach ($styles as $style) {
            foreach ($table['columns'] as $key => $column) {

                if ($column == $style->getColumnName()) {
                    if ($style->getHidden()) {
                        unset($table['columns'][$key]);
                        foreach ($table['rows'] as $key2 => $row) {
                            unset($table['rows'][$key2][$column]);
                        }
                        break;
                    }

                    if ($style->getAlias()) {
                        $table['columns'][$key] = $style->getAlias();
                    }

                    break;
                }
            }
        }


        /** Tworzenie csv */
//        mb_internal_encoding('UTF-8');
        $spout = $this->container->get('stev_spout.box_spout');

        /** @var Writer $spoutWriter */
        $spoutWriter = $spout->initWriter($fileType);

        $spoutWriter->setFieldDelimiter(';');

        $spoutWriter->openToBrowser($tool->getName() . '.' . $fileType);


        try {

            $spoutWriter->addRow($table['columns']);

            foreach ($table['rows'] as $row) {
                foreach ($row as $key => $cell) {
                    $row[$key] = strip_tags(preg_replace('#<br\s*/?>#i', "\n", $cell));
                }

                $spoutWriter->addRow($row);

            }
            $spoutWriter->close();
            exit();

        }catch (\Exception $exception) {
            $spoutWriter->close();
            return null;
        }

        $response = new \Symfony\Component\HttpFoundation\Response('', 200);
        return $response;
    }

    /**
     * @param Tool $tool
     * @param $table
     * @param bool $withHeaders
     * @param array $availableColumns
     * @return null|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function generateTxtResponse($tool, $table, $withHeaders = true, $availableColumns = [])
    {

        set_time_limit(0);
        ini_set('memory_limit', '2G');

        $contentType = 'text/plain';
        $filename = $tool->getName() . ' ' . (new \DateTime('now'))->format('Y-m-d H:i:s') . '.txt';
        $filename = AtlasUtils::slugify($filename);

        if($withHeaders) {

            /** @var Style[] $styles */
            $styles = $tool->getStyles();

            foreach ($styles as $style) {
                foreach ($table['columns'] as $key => $column) {

                    if(!in_array($column, $availableColumns)) {

                        unset($table['columns'][$key]);
                        continue;

                    }

                    if ($column == $style->getColumnName()) {
                        if ($style->getHidden()) {
                            unset($table['columns'][$key]);
                            foreach ($table['rows'] as $key2 => $row) {
                                unset($table['rows'][$key2][$column]);
                            }
                            break;
                        }

                        if ($style->getAlias()) {
                            $table['columns'][$key] = $style->getAlias();
                        }

                        break;
                    }
                }
            }

        }

        $response = new StreamedResponse();

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', $contentType);

        $response->setCallback(function () use ($table, $availableColumns, $withHeaders) {

            if($withHeaders) {

                $values = [];

                foreach ($table['columns'] as $columnName => $value) {
                    $values[] = $value;
                }

                echo implode(';', $values) . "\n";

                ob_flush();
                flush();

            }

            foreach ($table['rows'] as $row) {

                $values = [];

                foreach ($row as $columnName => $value) {

                    if(in_array($columnName, $availableColumns)) {
                        $values[] = str_replace("\n", " ", $value);
                    }

                }

                echo implode(';', $values) . "\n";

            }

        });

        return $response;

    }

    /**
     * Generate Report from tool to the file. with tool name and date in name
     * @param Tool $tool
     * @param array $table
     * @return string - uuid of file
     * @throws \Box\Spout\Common\Exception\IOException
     */
    public function generateXlsResponseToFile(Tool $tool, $table): string
    {

        set_time_limit(0);
        ini_set('memory_limit', '2G');

        $largeFile = count($table['columns']) * count($table['rows']) > self::LIMIT_XLS_CELLROW;
        $fileType = $largeFile ? Type::CSV : Type::XLSX;

        /** @var Style[] $styles */
        $styles = $tool->getStyles();


        foreach ($styles as $style) {
            foreach ($table['columns'] as $key => $column) {

                if ($column == $style->getColumnName()) {
                    if ($style->getHidden()) {
                        unset($table['columns'][$key]);
                        foreach ($table['rows'] as $key2 => $row) {
                            unset($table['rows'][$key2][$column]);
                        }
                        break;
                    }

                    if ($style->getAlias()) {
                        $table['columns'][$key] = $style->getAlias();
                    }

                    break;
                }
            }
        }


        /** Tworzenie XLS */
//        mb_internal_encoding('UTF-8');
        $spout = $this->container->get('stev_spout.box_spout');

        $spoutWriter = $spout->initWriter($fileType);
        $spoutWriter->setShouldUseInlineStrings(true);
        $headerBorder = (new BorderBuilder())
            ->setBorderBottom('e7ecf1', Border::WIDTH_THIN, Border::STYLE_SOLID)
            ->setBorderLeft('e7ecf1', Border::WIDTH_THIN, Border::STYLE_SOLID)
            ->setBorderRight('e7ecf1', Border::WIDTH_THIN, Border::STYLE_SOLID)
            ->build();

        $styleHeaderColumns = (new StyleBuilder())
            ->setFontBold()
            ->setFontColor('FFFFFF')
            ->setBackgroundColor('3B3F51')
            ->setBorder($headerBorder)
            ->build();


        $directory = $this->container->getParameter('report_directory') . '/' . $tool->getId();

        $filesystem = new Filesystem();
        if (!file_exists($directory)) {
            $filesystem->mkdir($directory, 0777);
        }


        $user = $this->em->getRepository('UserBundle:User')->find(1);

        $date = new \DateTime();
        $shortName = $tool->getName() . '_' . $date->format('d-M-Y H:i:s') . '.' . $fileType;
        $shortName = str_replace(' ', '_', $shortName);
        $fullName = $directory . '/' . $shortName;
        $shortPath = '/data/reports/' . $tool->getId() . '/' . $shortName;
        $spoutWriter->openToFile($fullName);

        try {
            $spoutWriter->addRowWithStyle($table['columns'], $styleHeaderColumns);

            foreach ($table['rows'] as $row):
                foreach ($row as $key => $cell):
                    $row[$key] = strip_tags(preg_replace('#<br\s*/?>#i', "\n", $cell));
                endforeach;

                $spoutWriter->addRow($row);

            endforeach;

            $spoutWriter->close();


            $document = new Document();
            $document->setName('email_attachments_' . date('Ymd_His'));
            $document->setCreatedBy($user);

            $fileSize = (filesize($fullName));
            $file = new File();
            $file->setCreatedBy($user);
            $file->setName($shortName);
            $file->setDocument($fileType);
            $file->setPath($shortPath);
            $file->setFileSize($fileSize);
            $document->addFile($file);


            $file->setDocument($document);
            $file->setMimeType('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $document->addFile($file);
            $this->em->persist($file);
            $this->em->persist($document);
            $this->em->flush();
            return $file->getUuid();


        } catch (\Exception $exception) {
            $spoutWriter->close();


            return '';
        }


    }


    /**
     * Generate Report from tool to the file. with tool name and date in name
     * @param Tool $tool
     * @param array $table
     * @return string - uuid of file
     * @throws \Box\Spout\Common\Exception\IOException
     */
    public function generateCSVResponseToFile(Tool $tool, $table)
    {

        set_time_limit(0);
        ini_set('memory_limit', '2G');

        $fileType = Type::CSV;


        /** Tworzenie XLS */
        mb_internal_encoding('UTF-8');
        $spout = $this->container->get('stev_spout.box_spout');


        $directory = $this->container->getParameter('report_directory') . '/' . $tool->getId();

        $filesystem = new Filesystem();
        if (!file_exists($directory)) {
            $filesystem->mkdir($directory, 0777);
        }


        $user = $this->em->getRepository('UserBundle:User')->find(1);

        $date = new \DateTime();
        $shortName = $tool->getName() . '_' . $date->format('d-M-Y H:i:s') . '.' . $fileType;
        $shortName = str_replace(' ', '_', $shortName);
        $fullName = $directory . '/' . $shortName;
        $shortPath = '/data/reports/' . $tool->getId() . '/' . $shortName;


        $styles = $tool->getStyles();
        foreach ($styles as $style) {
            foreach ($table['columns'] as $key => $column) {

                if ($column == $style->getColumnName()) {
                    if ($style->getHidden()) {
                        unset($table['columns'][$key]);
                        foreach ($table['rows'] as $key2 => $row) {
                            unset($table['rows'][$key2][$column]);
                        }
                        break;
                    }

                    if ($style->getAlias()) {
                        $table['columns'][$key] = $style->getAlias();
                    }

                    break;
                }
            }
        }


        $spoutWriter = $spout->initWriter($fileType);


        $spoutWriter->setFieldDelimiter(';');
        $spoutWriter->openToFile($fullName);
        /** @var Style[] $styles */


        try {


            $spoutWriter->addRow($table['columns']);
            foreach ($table['rows'] as $row):
                foreach ($row as $key => $cell):
                    $row[$key] = strip_tags(preg_replace('#<br\s*/?>#i', "\n", $cell));
                endforeach;

                $spoutWriter->addRow($row);

            endforeach;

            $spoutWriter->close();


            $document = new Document();
            $document->setName('email_attachments_' . date('Ymd_His'));
            $document->setCreatedBy($user);

            $fileSize = (filesize($fullName));
            $file = new File();
            $file->setCreatedBy($user);
            $file->setName($shortName);
            $file->setDocument($fileType);
            $file->setPath($shortPath);
            $file->setFileSize($fileSize);
            $document->addFile($file);


            $file->setDocument($document);
            $file->setMimeType('text/csv');
            $document->addFile($file);
            $this->em->persist($file);
            $this->em->persist($document);
            $this->em->flush();
            return $file->getUuid();


        } catch (\Exception $exception) {
            $spoutWriter->close();


            return '';
        }


    }


}
