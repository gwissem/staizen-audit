<?php
namespace ToolBundle\Service;

use CaseBundle\Service\ParserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DateParserService implements ParserInterface
{
    const PREFIX = '{$';
    const PREFIX_ESC = '{\$';
    const SUFFIX = '$}';
    const SUFFIX_ESC = '\$}';
    const PREFIX_LEN = 2;
    const SUFFIX_LEN = 2;
    const FORMAT = 'Y-m-d H:i:s';

    /** @var  ContainerInterface */
    protected $container;

    /**
     * UserPersonalizationService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function parse(string $input): string
    {
        return $this->parseDate($input);
    }

    public function parseDate($input, \DateTime $date = null)
    {
        if ($this->isRelativeDate($input)) {
            try{
                /** Ucięcie prefixu i suffixu i białych znaków */
                $copyInput = trim(substr($input, self::PREFIX_LEN, strlen($input) - (self::SUFFIX_LEN + 2)));

                if(!$date) $date = new \DateTime();

                foreach (explode(".", $copyInput) as $item) {
                    $this->doParse($item, $date);
                }

                return $date->format(self::FORMAT);
            } catch (\Exception $e) {
            }
        }

        return $input;
    }

    private function isRelativeDate($input)
    {
        if(!is_string($input)) return false;

        if(substr($input, 0, self::PREFIX_LEN) != self::PREFIX || substr($input, -self::SUFFIX_LEN) != self::SUFFIX) return false;

        return true;
    }

    private function doParse($str, \DateTime $date)
    {
        return (substr($str, 0, 1) == '!') ? $this->modifySpecialDate($str, $date) : $this->modifyNormalDate($str, $date);
    }

    private function modifySpecialDate($str, \DateTime $date)
    {
        switch ($str) {
            case '!today' : {
                $date->modify('today');
                break;
            }
            case '!now' : {
                $date->modify('now');
                break;
            }
            case '!last-d' : {
                $date->modify('last day of');
                break;
            }
            case '!first-d' : {
                $date->modify('first day of');
            }
        }
        return $date;
    }

    private function modifyNormalDate($str, \DateTime $date)
    {
        $modifyStr = '';
        $mark = substr($str, 1, 1);
        $typeMark = substr($str, 0, 1);
        $type = $this->getTypeDate($typeMark);

        if(in_array($mark, ['-','+'])){
            if($typeMark == 'M') {
                $this->modifyMonth($str, $mark, $date);
            } else {
                $modifyStr .= $mark;
                $modifyStr .= substr($str, 2);
                $modifyStr .= " ".$type;
                $date->modify($modifyStr);
            }
        } else {
            $val = substr($str, 1);
            switch ($typeMark) {
                case 'I' : {
                    $date->setTime($date->format('H'), $val, 0);
                    break;
                }
                case 'H' : {
                    $date->setTime($val, $date->format('i'), 0);
                    break;
                }
                case 'D' : {
                    $date->setDate($date->format('Y'), $date->format('m'), $val);
                    break;
                }
                case 'M' : {
                    $this->setMonth($str, $date);
                    break;
                }
                case 'Y' : {
                    $date->setDate($val, $date->format('m'), $date->format('d'));
                    break;
                }
            }
        }

        return $date;
    }

    private function getTypeDate($str)
    {
        switch ($str) {
            case 'I' : return 'minutes';
            case 'H' : return 'hours';
            case 'D' : return 'days';
            case 'W' : return 'week';
            case 'M' : return 'month';
            case 'Y' : return 'year';
        }
        return '';
    }

    private function modifyMonth($str, $mark, \DateTime $date)
    {
        $modifyBy = intval(substr($str, 2));
        $currentMonth = intval($date->format('m'));
        $checkMonth = $currentMonth;
        $year = intval($date->format('Y'));

        if ($mark == '-') {
            $checkMonth = $currentMonth - $modifyBy;
            if($checkMonth <= 0) {
                $checkMonth = 12 - $checkMonth;
                $year -= 1;
            }
        } elseif ($mark == '+') {
            $checkMonth = $currentMonth + $modifyBy;
            if($checkMonth > 12) {
                $checkMonth = $checkMonth - 12;
                $year += 1;
            }
        }

        if (!checkdate($checkMonth, $date->format('d'), $year)) {
            $date->modify('last day of '.$mark.$modifyBy.' month');
        } else {
            $date->modify($mark.$modifyBy.' month');
        }
    }

    private function setMonth($str, \DateTime $date)
    {
        $checkMonth = intval(substr($str, 1));


        if (!checkdate($checkMonth, $date->format('d'), $date->format('Y'))) {
            $dateObj   = \DateTime::createFromFormat('!m', $checkMonth);
            $monthName = $dateObj->format('F');

            $date->modify('first day of');
            $date->modify($monthName);
            $date->modify('last day of');
        } else {
            $date->setDate($date->format('Y'), $checkMonth, $date->format('d'));
        }
    }

    public function getSchemaRegex(): string
    {
        return '/' . self::PREFIX_ESC
            . '.+' . self::SUFFIX_ESC . '/';
    }
}