<?php
namespace ToolBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class FilterEncrypter
{

    /** @var  ContainerInterface */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function isHashCorrect($filters, $hash)
    {

        if ($this->generateHash($filters) === $hash) {
            return true;
        }


        return false;
    }

    public function generateHash($filters)
    {

        if (is_array($filters)) {
            if(array_key_exists('hash', $filters)){
                unset($filters['hash']);
            }

            $filters = http_build_query($filters);
        }

        $salt = $this->container->getParameter('tool_filters_salt');
        $hash = md5($filters.$salt);

        return $hash;
    }
}