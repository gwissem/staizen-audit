<?php

namespace ToolBundle\Service;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Translation\TranslatorInterface;
use ToolBundle\Entity\Tool;

class ToolTypeValidatorService
{
    private $requiredConnection = [
        ['query', 'thirdPartyToolUrl']
    ];
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function isValid(Form $form) : bool
    {
        if (!$this->validRequired($form)) {
            return false;
        }

        return true;
    }

    public function validRequired(Form $form) : bool
    {
        $valid = true;
        $valid = !(empty($form->get($this->requiredConnection[0][0])->getData())
            && empty($form->get($this->requiredConnection[0][1])->getData()));

        if (!$valid) {
            $form->get($this->requiredConnection[0][0])->addError(
                new FormError($this->translator->trans(
                    'one of this cannot be blankedede (%first% or %second%)',
                    ['%first%' => $this->requiredConnection[0][0], '%second%' => $this->requiredConnection[0][1]]
                ))
            );

            $form->get($this->requiredConnection[0][1])->addError(
                new FormError($this->translator->trans(
                    'one of this cannot be blankedede (%first% or %second%)',
                    ['%first%' => $this->requiredConnection[0][0], '%second%' => $this->requiredConnection[0][1]]
                ))
            );
        }

        return $valid;
    }
}