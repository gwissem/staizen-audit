<?php

namespace ToolBundle\Service;

use AppBundle\Entity\Log;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\File;
use Symfony\Component\HttpFoundation\Response;
use Psr\Container\ContainerInterface;
use ToolBundle\Entity\QueuedReport;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use ToolBundle\Entity\Tool;

class QueuedReportService
{

    private $em;
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(Container $container, EntityManager $em)
    {
        $this->em = $em;
        $this->container = $container;
    }


    public function getTypeText(QueuedReport $report)
    {
        switch ($report->getType()) {
            case 1:
                $return = 'xlsx';
                break;
            case 2:
                $return = 'csv';
                break;
            default:
                $return = 'xlsx';

        }
        return $return;
    }

    public function getFiltersText(QueuedReport $report)
    {

        return $report->getFilters()??'-';
    }


    public function getStatusText(QueuedReport $report)
    {


        switch ($report->getStatus()) {
            case 1:
                $return = 'Oczekuje';
                break;
            case 2:
                $return = 'W trakcie działania';
                break;
            case 3:
                $return = 'Gotowy do pobrania';
                break;
            case 4:
                $return = 'BŁĄD';
                break;
            default:
                $return = 'Oczekuje';

        }
        return $return;

    }

    public function getFileLink(QueuedReport $report)
    {

        $url = $this->container->getParameter('server_host') . $this->container->get('router')->generate('download_queued_report', [
                'uuid' => $report->getUuid()]);

        return $url;

    }

    public function getFile(QueuedReport $report)
    {

        /** @var File $file */
        $file = $report->getFile();


       $fileContent = file_get_contents($this->container->getParameter('kernel.root_dir') . '/../' . $file->getPath());


        $response = new Response();
        $response->headers->set('Content-Type', $file->getMimeType());
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $file->getName());
        $response->setStatusCode(200);
        $response->setContent($fileContent);

        return $response;


    }


    public function getNumberInQueue()
    {
        return $this->em->getRepository('ToolBundle:QueuedReport')->getQueuedReportsNumber() ?? 0;
    }

    public function getReportForProcessing()
    {
        return $this->em->getRepository('ToolBundle:QueuedReport')->getNextReportInQueue();
    }


    /**
     * @param QueuedReport $report
     * @return int Status
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function processReport(QueuedReport $report)
    {
        $report->setStatus(2);
        $this->em->persist($report);
        $this->em->flush();

        /** @var Tool $tool */
        $tool = $report->getTool();
        $type = $report->getType();
        $filters = $report->getFilters() ? \GuzzleHttp\Psr7\parse_query($report->getFilters()) : $tool->getFormFilters();
        try {
            $uuid = $this->container->get('report.sending')->generateSingleToolFile($tool, $type, $filters);
            $file = $this->em->getRepository('DocumentBundle:File')->findOneBy(['uuid' => $uuid]);
            $report->setStatus(3);
            $report->setFile($file);
            $this->em->persist($report);
            $this->em->flush();
            return 3;

        } catch (\Exception $e) {
            $report->setStatus(4);
            $this->em->persist($report);
            $this->em->flush();

            $log = new Log();
            $log->setName('Processing Queued Report Error');
            $log->setContent($e->getMessage());
            $log->setParam1($report->getId());
            $this->em->persist($log);
            $this->em->flush();

            return 4;
        }
    }
}