<?php
namespace ToolBundle\Service;

use Doctrine\ORM\EntityManager;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\ToolAction;

class ToolExportService
{
    /** @var  EntityManager */
    protected $em;
    protected $translator;
    protected $locales;

    /**
     * UserPersonalizationService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, $translator, $locales)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->locales = $locales;
    }

    public function cloneTool($tool, $collectionsArray, $compareUnique = false)
    {

        /** @var Tool $cloneTool */
        $cloneTool = clone $tool;
        if (array_key_exists('filter', $collectionsArray) && $collectionsArray['filter'] === true) {
            foreach ($cloneTool->getFilters() as $filter) {
                $cloneFilter = clone $filter;
                $cloneTool->addFilter($cloneFilter);
                $cloneFilter->setTool($cloneTool);
            }
        }

        if (array_key_exists('style', $collectionsArray) && $collectionsArray['style'] === true) {
            foreach ($cloneTool->getStyles() as $style) {

                $cloneStyle = clone $style;

                if ($cloneStyle->getEditTool() == $tool) {
                    $cloneStyle->setEditTool($cloneTool);
                }
                $cloneStyle->setAction(null);

                $cloneTool->addStyle($cloneStyle);
                $cloneStyle->setTool($cloneTool);
            }
        }
        if (array_key_exists('action', $collectionsArray) && $collectionsArray['action'] === true) {
            foreach ($cloneTool->getActions() as $action) {
                $cloneAction = clone $action;
                $cloneTool->addAction($cloneAction);
                $cloneAction->setTool($cloneTool);
            }
        }

        if (array_key_exists('edit', $collectionsArray) && $collectionsArray['edit'] === true) {
            foreach ($cloneTool->getFormFields() as $formField) {
                $cloneFormField = clone $formField;
                $cloneTool->addFormField($cloneFormField);
                $cloneFormField->setTool($cloneTool);
            }
        } else {
            $cloneTool->setEditIdColumn(null);
            $cloneTool->setInsertQuery(null);
            $cloneTool->setUpdateQuery(null);
            $cloneTool->setDeleteQuery(null);
        }

        if (array_key_exists('permission', $collectionsArray) && $collectionsArray['permission'] === true) {
            foreach ($cloneTool->getPermissions() as $permission) {
                $clonePermission = clone $permission;
                $cloneTool->addPermission($clonePermission);
                $clonePermission->setTool($cloneTool);
            }
        }

        $this->em->persist($cloneTool);
        $this->em->flush();

        return $cloneTool;
    }

}