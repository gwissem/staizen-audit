<?php

namespace MapBundle\Controller;


use CaseBundle\Service\CaseHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MapTowingMonitorController extends Controller
{

    /**
     * @Route("/map/order/{number}",
     *     name="map_towing_monitor",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @param $number
     * @return Response
     */
    public function towingMonitor(Request $request, $number)
    {

        $orderId = $this->get('case.handler')->getIdOrderByNumber($number);
        $order = $this->get('map.monitor.service')->getOrder($orderId);
        $platform = $this->get('case.process_handler')->getRootPlatform(CaseHandler::parseCaseNumber($order['number']));

        return $this->render('@Map/Monitoring/index.html.twig', [
            'number' => $number,
            'brandUrl' => (!empty($platform)) ? $platform->getIconPath() : ''
        ]);

    }

    /**
     * @Route("/towing-monitor/get-coordinates",
     *     name="map_towing_monitor_get_coords",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @param $rootId
     * @return Response
     */
    public function getCoordinates(Request $request)
    {

        $orderNumber = $request->query->get('orderNumber');
        $orderId = $this->get('case.handler')->getIdOrderByNumber($orderNumber);

        return new JsonResponse($this->get('map.monitor.service')->getDataForMapMonitor($orderId));

    }


    /**
     * @Route("/towing-monitor/get-other-orders",
     *     name="map_towing_monitor_get_orders",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @param $rootId
     * @return Response
     */
    public function getOtherOrders(Request $request)
    {

        $orders = array_merge($this->get('map.monitor.service')->getOrders(), $this->get('map.monitor.service')->getOrders(6));

        return new JsonResponse($orders);

    }

    /**
     * @Route("/towing-monitor/refresh-map",
     *     name="map_towing_monitor_refresh",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @param $rootId
     * @return Response
     */
    public function refreshMapMonitor(Request $request)
    {

        $orderNumber = $request->query->get('orderNumber');
        $orderId = $this->get('case.handler')->getIdOrderByNumber($orderNumber);
//        $orderId = $request->query->get('orderId');
        $lastCoordinateId = $request->query->get('lastCoordinateId');
        $orderStatus = $request->query->get('orderStatus');
        $saveAt = $request->query->get('saveAt');

        return new JsonResponse($this->get('map.monitor.service')->getDataForMapRefresh($orderId, $lastCoordinateId, $orderStatus, $saveAt));

    }

}
