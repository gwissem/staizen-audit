<?php

namespace MapBundle\Controller;

use CaseBundle\Entity\ProcessInstance;
use MapBundle\Entity\Poi;
use MapBundle\Entity\PositionRequest;
use SocketBundle\Topic\TaskManagerTopic;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FindBySmsController extends Controller
{

    /**
     * @Route("/client/location/{instanceId}",
     *     name="client_location_map",
     *     schemes={"https"},
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @param $instanceId
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function clientLocationAction(Request $request, $instanceId) {

        /** @var PositionRequest $positionRequest */
        $positionRequest = $this->getDoctrine()->getRepository('MapBundle:PositionRequest')->findOneBy(['uniqueId' => $instanceId]);

        $now = new \DateTime();

        $id = null;

        if($positionRequest) {

            if($positionRequest->getUpdatedAt() !== NULL) {

                $diff = $now->getTimestamp() - $positionRequest->getUpdatedAt()->getTimestamp();

                /** Sms ważny max 15 min */
                if(($diff/60) <= 15) {
                    $id = $positionRequest->getUniqueId();
                }
            }
            else {
                $id = $positionRequest->getUniqueId();
            }

        }

        return $this->render('MapBundle:Sms:index.html.twig', [
            'id' => $id
        ]);

    }

    /**
     * @Route("/client/find-location/{instanceId}",
     *     name="find_client_location",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @param $instanceId
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function findClientLocationAction(Request $request, $instanceId) {

        $data = $request->request->all();

        /** @var PositionRequest $positionRequest */
        $positionRequest = $this->getDoctrine()->getRepository('MapBundle:PositionRequest')->findOneBy(['uniqueId' => $instanceId]);

        if(!$positionRequest && empty($data['coords'])) {
            return new JsonResponse([
                'status' => false
            ]);
        }

        $positionRequest->setLatitude($data['coords']['latitude']);
        $positionRequest->setLongitude($data['coords']['longitude']);
        $positionRequest->setUsed($positionRequest->getUsed() + 1);
        $this->getDoctrine()->getManager()->persist($positionRequest);
        $this->getDoctrine()->getManager()->flush();


        $pusher = $this->container->get('gos_web_socket.zmq.pusher');

        $groupProcessId = $positionRequest->getGroupProcessId();

        /** Wyciągnięcie aktywnych procesów z grupy */

        /** @var ProcessInstance[] $activeProcesses */
        $activeIdProcesses = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findIdOfActiveProcessesInGroup($groupProcessId);

        if(!empty($activeIdProcesses)) {

            $ids = [];

            foreach ($activeIdProcesses as $activeIdProcess) {
                $ids[] = $activeIdProcess['id'];
            }

            $data = [
                'event' => [
                    'processesId' => $ids,
                    'coords' => $data['coords'],
                    'action' => TaskManagerTopic::UPDATE_LOCATION
                ],
            ];

            $pusher->push($data, 'atlas_task_manager');
            $status = true;

        }
        else {
            $status = false;
        }

        return new JsonResponse([
            'status' => $status
        ]);

    }

    /**
     * @Route("/map/create-position-request/{groupProcessId}",
     *     name="create_position_request",
     *     options={"expose"=true},
     *     methods={"post"},
     *     requirements={
     *         "instanceId": "\d+"
     *     }
     * )
     * @param Request $request
     * @param $groupProcessId
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function createPositionRequestAction(Request $request, $groupProcessId) {

        /** @var PositionRequest[] $positionRequest */
        $positionRequest = $this->getDoctrine()->getRepository('MapBundle:PositionRequest')->findBy(['groupProcessId' => $groupProcessId]);

        if(is_array($positionRequest) && count($positionRequest) > 0) {
            $positionRequest = $positionRequest[0];
        }

        if($positionRequest) {

            $positionRequest->setUpdatedAt(new \DateTime());

        }
        else {
            $positionRequest = new PositionRequest();
            $positionRequest->setUniqueId(self::gen_uuid());
            $positionRequest->setGroupProcessId($groupProcessId);

        }

        $positionRequestUniqueId = $positionRequest->getUniqueId();
        $this->getDoctrine()->getManager()->persist($positionRequest);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'uniqueId' => $positionRequestUniqueId
        ]);

    }

    static function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

}
