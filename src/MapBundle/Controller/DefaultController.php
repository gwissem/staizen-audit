<?php

namespace MapBundle\Controller;

use AppBundle\Entity\Config;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\ProcessInstance;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use MapBundle\Entity\Poi;
use MapBundle\Service\MapCenterClientPlugin;
use Predis\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    const SESSION_COMMENT = 'Atlas Session';
    const QUERY_RADIUS = 100;
    const MAP_ELEMENTS_CATEGORY = 'PointNumeration';
    const COUNTRY_CODES = 'country_codes';
    const REDIS_MAP_SESSION_KEY = 'map_session_key';

    const LIMIT_ANON_SESSION = 20;

    private $currentProjections;
    private $currentDeGeocodeLayers;
    private $currentAnonSessionKey;

    private $cachedCountry = [];

    private function getPaths() {

        return [
            'city' => '87',
            'street' => '94',
            'streetNumber' => '95',
            'objectNumber' => '96',
            'voivodeship' => '88',
            'county' => '91',
            'commune' => '90',
            'country' => '86',
            'areaName1' => '88',
            'areaName2' => '91',
            'areaName3' => '90',
            'lat' => '93',
            'lng' => '92',
            'zip' => '89',
            'typeLoc' => '257',
            'desc' => '63',
            'location' => '85',
            'road' => '509',
            'inCity' => '541'
        ];

    }

    public function __destruct()
    {
        if($this->currentAnonSessionKey) {
            $this->unlockSessionKey();
        }
    }

    /**
     * @Route("/atlas-map-secret123")
     */
    public function indexAction()
    {
        return $this->render('MapBundle:Default:index.html.twig');
    }

    /**
     * @Route("/exact_geocode",
     *     name="map_exact_geocode",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function searchOneLocation(Request $request)
    {

        $data = $request->request->all();

        if(empty($data)) {
            return new JsonResponse([
                'result' => []
            ]);
        }

        $result = $this->searchLocationByAddress($data);

        return new JsonResponse([
            'result' => $result
        ]);

    }

    /**
     * @param $datum
     * @return array|JsonResponse
     * @throws \Exception
     */
    public function searchLocationByAddress($datum) {

        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'country' => null,
            'city' => null,
            'zipCode' => null,
            'voivodeship' => null,
            'street' => null,
            'streetNumber' => null,
            'countryId' => null
        ]);

        $data = $resolver->resolve($datum);

        if(empty($data['countryId'])) {
            $data['countryId'] = $this->getCountryIdFromConfig();
        }

        $result = [];

        $mapCenterPlugin = $this->get('map.center.client.plugin');

        $sessionId = $this->getSession($mapCenterPlugin);

        try {

            if(!$sessionId) {
                return new JsonResponse(['error' => true]);
            }

            $mapCenterPlugin->ClearMapSelection($sessionId);

            $selectedCities = $mapCenterPlugin->SearchSelectCities($sessionId, intval($data['countryId']), $data['city'],  $data['zipCode'], $data['voivodeship'], '', '');

            $cities = $mapCenterPlugin->SearchGetCityListEx($sessionId, 0, $selectedCities['ResultCount']);

            $cityDetails = $this->handleCityDetails($cities, $mapCenterPlugin, $sessionId, true);

            if(isset($data['street']) && !empty($data['street'])) {

                $_street = '';
                $_streetNumber = '';
                $_longitude = '';
                $_latitude = '';

                // Szukanie drogi
                $itemKinds = $mapCenterPlugin->SearchGetItemKindList($sessionId);
                $setResult = $mapCenterPlugin->SearchSetItemsFilter(
                    $sessionId,
                    [array_search("roads", $itemKinds['ItemKindNames'])]
                );

                if ($setResult['Result'] == 1) {

                    /** Jeżeli adres ZIP przekazany, to ustawienie (prawdopodobnie dokładniejszy) */

                    if(!empty($data['zipCode'])) {
                        $cityDetails[0]['zipCode'] = $data['zipCode'];
                    }

                    $resultSelect = $mapCenterPlugin->SearchSelectItems($sessionId, 0, 0, $data['street']);

                    if ($resultSelect['Result'] != 1 || $resultSelect['ResultCount'] == 0) {

                    }
                    else {

                        $streets = $mapCenterPlugin->SearchGetItemsList($sessionId, 0, $resultSelect['ResultCount']);

                        if (count($streets['ItemNames']) == 0 || $streets['Result'] != 1) {

                        }
                        else {

                            $_street = $streets['ItemNames'][0];

                            if(isset($data['streetNumber']) && !empty($data['streetNumber'])) {

                                $point = $mapCenterPlugin->SearchAddStreetWithNumToSelection(
                                    $sessionId,
                                    0,
                                    $data['streetNumber']
                                );

                                if ($point['Result'] == 1 && !empty($point['MidPoint']['Longitude'])) {

                                    $_streetNumber = $data['streetNumber'];
                                    $_longitude = $point['MidPoint']['Longitude'];
                                    $_latitude = $point['MidPoint']['Latitude'];

                                }

                            }
                            else {

                                /**
                                 * Żeby szybciej było to szukanie drogi z numerem 1
                                 */
//                                $point = $mapCenterPlugin->SearchAddStreetWithNumToSelection(
//                                    $sessionId,
//                                    0,
//                                    1
//                                );

//                                if(!empty($point) && $point['Result'] == 1) {
//
//                                    $_latitude = $point['MidPoint']['Latitude'];
//                                    $_longitude = $point['MidPoint']['Longitude'];
//
//                                }
//                                else {

                                    $inputPoints = [[
                                        "City" => $cityDetails[0]['city'],
                                        "Country" => $cityDetails[0]['country'],
                                        "District" => $cityDetails[0]['voivodeship'],
                                        "County" => $cityDetails[0]['county'],
                                        "Commune" => $cityDetails[0]['commune'],
                                        "Zip" => $cityDetails[0]['zipCode'],
                                        "Street" => $_street,
                                        "StreetNumber" => ""
                                    ]];

                                    $deGeocodesResult = $mapCenterPlugin->Geocode($sessionId, 1, $inputPoints);

                                    $_latitude = $deGeocodesResult['Positions'][0]['Latitude'];
                                    $_longitude = $deGeocodesResult['Positions'][0]['Longitude'];

//                                }

                            }
                        }

                        $cityDetails[0]['street'] = $_street;
                        $cityDetails[0]['streetNumber'] = $_streetNumber;
                        $cityDetails[0]['latitude'] = $_latitude;
                        $cityDetails[0]['longitude'] = $_longitude;

                    }

                }

            }

            foreach ($cityDetails as $key => $cityDetail) {
                if(!empty($cityDetail['country'])) {
                    $cityDetails[$key]['country'] = $this->translateCountry($cityDetail['country']);
                }
            }

            $result = $cityDetails;

        }
        catch (\Exception $e) {

        }
        finally {

        }

        return $result;

    }

    /**
     * @return int
     */
    private function getCountryIdFromConfig() {

        $configLocale = $this->getDoctrine()->getRepository('AppBundle:Config')->findOneBy([
            'key' => 'application_locale'
        ]);

        switch (($configLocale instanceof Config) ? $configLocale->getValue() : null) {
            case 'ru' : {
                return 74;
            }
            case 'pl' : {
                return 71;
            }
            default : {
                return 0;
            }
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAvailableCountries() {

        $mapCenterPlugin = $this->get('map.center.client.plugin');
        $sessionId = $this->getSession($mapCenterPlugin);
        $countries = $mapCenterPlugin->SearchGetCountryList($sessionId);

        if(!empty($countries) && isset($countries['CountryNames'])) {
            return $countries['CountryNames'];
        }

        return [];

    }

    /**
     * @param MapCenterClientPlugin $mapCenterPlugin
     * @param $sessionId
     * @param $countryName
     * @return int|mixed
     */
    private function getCountryId(MapCenterClientPlugin $mapCenterPlugin, $sessionId, $countryName){

        $countries = $mapCenterPlugin->SearchGetCountryList($sessionId);

        if(empty($countries) || empty($countries['CountryNames'])) return 0;

        $new_array = array_filter($countries['CountryNames'], function($country) use ($countryName) {
            return ( $country === $countryName);
        });

        if(!empty($new_array)) {
            return key($new_array);
        }

        return 0;

    }

        /**
     * @Route("/geocode",
     *     name="map_geocode_post",
     *     options={"expose"=true}
     * )
     * @Route("/geocode/{expression}",
     *     name="map_geocode",
     *     options={"expose"=true},
     *     requirements={"expression"=".+"}
     * )
     * @param Request $request
     * @param string $expression
     * @return JsonResponse
     * @internal param $expression
     */
    public function searchLocation(Request $request, $expression = '')
    {

        $country = 0;

        if($request->isMethod('POST')) {
            $expression = $request->request->get('expression', '');
            $country = $request->request->get('country', 0);
        }
        elseif($request->isMethod('GET')) {
            $country = $request->query->get('country', 0);
        }

        if(empty($expression)) {
            return new JsonResponse([
                'result' => []
            ]);
        }

        $locationResult = [];
        $expressionParts = explode(',', $expression);

        $mapCenterPlugin = $this->get('map.center.client.plugin');

        $sessionId = $this->getSession($mapCenterPlugin);

        if(!$sessionId) {
            return new JsonResponse(['error' => true]);
        }

        $result = [];

        try {

            $mapCenterPlugin->ClearMapSelection($sessionId);
            $mapCenterPlugin->RoutePlannerRouteClear($sessionId);
            $mapCenterPlugin->RoutePlannerEntriesClear($sessionId);

            $this->currentProjections = $mapCenterPlugin->getProjections();
            $this->currentDeGeocodeLayers = $mapCenterPlugin->getDegeocodeLayers($sessionId);

            if(empty($country)) $country = 0;
            $selectedCities = $mapCenterPlugin->SearchSelectCities($sessionId, intval($country), $expressionParts[0], '', '', '', '');

            $resultCount = (intval($selectedCities['ResultCount']) > 200) ? 200 : $selectedCities['ResultCount'];

            /**
             * Wyszukanie miast - zwraca liczbę wyszukanych
            */
            $cities = $mapCenterPlugin->SearchGetCityListEx($sessionId, 0, $resultCount);

            if ($cities['Result'] != 1 || count($cities['CityNames']) == 0) {
                return new JsonResponse(['result' => []]);
            }

            /**
             * Pobiera wyszukane miasta
             */
            $cityDetails = $this->handleCityDetails($cities, $mapCenterPlugin, $sessionId);

            /**
             * Pobiera możliwe "elementy" do wyszukiwania
             */
            $itemKinds = $mapCenterPlugin->SearchGetItemKindList($sessionId);

            /**
             * Ustawienie, że będzie szukanie po drodze
             */
            $setResult = $mapCenterPlugin->SearchSetItemsFilter(
                $sessionId,
                [array_search("roads", $itemKinds['ItemKindNames'])]
            );

            /**
             * sprawdza czy jest przekazana droga do wyszukiwania i czy jest taka możliwość - jeżeli nie to zwracane są
             * tylko informacje o mieście
             */
            if ($setResult['Result'] != 1 || !isset($expressionParts[1])) {

                $result = $cityDetails;

            }
            else {

                /**
                 * Próba pobrania drogi i numeru
                 */
                $address = $this->getAddress($expressionParts[1]);

                /**
                 * Pętla po wszystkich znalezionych miastach
                 */
                for ($cityIndex = 0; $cityIndex < count($cities['CityNames']); $cityIndex++) {

                    /**
                     * Wyszukiwanie drogi w danym mieście
                     */
                    $resultSelect = $mapCenterPlugin->SearchSelectItems($sessionId, $cityIndex, 0, $address['street']);

                    if ($resultSelect['Result'] != 1 || $resultSelect['ResultCount'] == 0) {
                        continue;
                    }

                    /**
                     * Pobranie informacji o drogach
                     */
                    $streets = $mapCenterPlugin->SearchGetItemsList($sessionId, 0, $resultSelect['ResultCount']);

                    if (count($streets['ItemNames']) == 0 || $streets['Result'] != 1) {
                        continue;
                    }

                    /**
                     * Pętla po znalezionych drogach, żeby pobrać ich ZIP CODE
                     */
                    for ($j = 0; $j < count($streets['ItemNames']); $j++) {

                        $locationResult[$cityIndex][$j] = $cityDetails[$cityIndex];
                        $locationResult[$cityIndex][$j]['street'] = $streets['ItemNames'][$j];

                        /**
                         * Jeżeli podana droga, to dokładniejsze przeszukiwanie
                         */
                        if (!empty($address['number'])) {

                            $point = $mapCenterPlugin->SearchAddStreetWithNumToSelection(
                                $sessionId,
                                0,
                                $address['number']
                            );

                            if ($point['Result'] == 1 && !empty($point['MidPoint']['Longitude'])) {
                                $locationResult[$cityIndex][$j]['streetNumber'] = $address['number'];
                                $locationResult[$cityIndex][$j]['longitude'] = $point['MidPoint']['Longitude'];
                                $locationResult[$cityIndex][$j]['latitude'] = $point['MidPoint']['Latitude'];
                            }

                        } else {

                            /**
                             * Pobranie współrzędnych drogi, tylko poto, żeby wyciągnąć ZIP-CODE
                             */
                            $point = $mapCenterPlugin->SearchAddStreetWithNumToSelection(
                                $sessionId,
                                $j,
                                1
                            );

                            if(!empty($point) && $point['Result'] == 1) {

                                $resultOfStreet = $this->simpleDeGeocode($sessionId, $mapCenterPlugin, $point['MidPoint']['Latitude'], $point['MidPoint']['Longitude']);

                                if(!empty($resultOfStreet['Zip'])) {
                                    $locationResult[$cityIndex][$j]['zipCode'] = $resultOfStreet['Zip'];
                                }

                            }

                        }
                    }
                }

            }

        } catch (\Exception $e) {
//            $mapCenterPlugin->DropSession($sessionId);
        }

        foreach ($locationResult as $item) {
            if(!empty($item['country'])) {
                $item['country'] = $this->translateCountry($item['country']);
            }
            $result = array_merge($result, $item);
        }

        usort($result, static function($a, $b){
            return ($b['country'] === 'Polska');
        });

        return new JsonResponse([
            'result' => $result
        ]);
    }

    private function setLanguageContext(MapCenterClientPlugin $mapCenterPlugin, $sessionId) {

        $configLocale = $this->getDoctrine()->getRepository('AppBundle:Config')->findOneBy([
            'key' => 'application_locale'
        ]);

        $lang = 'POL';

        if($configLocale !== null) {
            $lang = $this->getLanguageInISO($configLocale->getValue());
        }

//        $lang = 'POL';

        $mapCenterPlugin->SetSessionLanguageContext($sessionId, 0, $lang);

    }

    /**
     *  Get Name of Language in ISO 639.2 code (needed for emapi)
     *  https://www.loc.gov/standards/iso639-2/php/code_list.php
     *
     * @param $locale
     * @return string
     */
    private function getLanguageInISO($locale) {

        switch ($locale) {
            case 'pl': {
                return 'POL';
            }
            case 'en':{
                return 'ENG';
            }
            case 'ru': {
                return 'RUS';
            }
            default: {
                return 'POL';
            }
        }

    }

    /**
     * @Route("/get_location/{latitude}/{longitude}",
     *     name="map_degeocode",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @param $latitude
     * @param $longitude
     * @return JsonResponse|Response
     */
    public function getLocation(Request $request, $latitude, $longitude)
    {
        $result = $this->degeocode($latitude, $longitude);

        if(empty($result)) {
            return new JsonResponse(['error' => true]);
        }

        list($streetNumber, $street) = $this->tryGetStreetNumber($result);

        $result = [
            'city' => !empty($result['City']) ? $result['City'][0]['Name'] : '',
            'country' => $this->translateCountry($result['AreaName0']),
            'voivodeship' => $result['AreaName1'],
            'county' => $result['AreaName2'],
            'commune' => $result['AreaName3'],
            'zipCode' => $result['Zip'],
            'street' => $street,
            'inCity' => $result['inCity'],
            'streetNumber' => $streetNumber,
            'road' => $this->getRoad($result)
        ];

        if($request->query->has('no-json')) {

            $str = '';
            $count = count($result);
            $i = 1;
            foreach ($result as $key => $item) {
                $str .= $key .":".$item;
                if($i < $count) $str .= ',';
                $i++;
            }

            return new Response($str);
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/save_location",
     *     name="save_location_in_case",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function saveLocationInCase(Request $request) {

        $processToken = $request->headers->get('Process-Token');
        $groupProcessId = $request->get('groupProcessId');
        $locationPath = $request->get('locationPath');
        $location = $request->get('locationData');

        /** @var ProcessInstance $processInstance */
        $processInstance = null;

        if(empty($groupProcessId) && !empty($processToken)) {
            $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy([
                'token' => $processToken
            ]);
        }
        else {
            $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy([
                'id' => $groupProcessId
            ]);
        }

        if(empty($processInstances)) {
            return new JsonResponse(['message' => 'Process is not exists.'], 404);
        }

        $processInstance = $processInstances[0];

        $paths = $this->getPaths();
        $processHandler = $this->get('case.process_handler');
        $repository = $this->getDoctrine()->getRepository('CaseBundle:Attribute');
        $groupProcessId = $processInstance->getGroupProcess()->getId();

        /** @var User $user */
        $user = $this->container->get('user.info')->getUser();

        if($user === "anon." && !empty($processToken)) {

            $this->setVirtualUser();
            $user = $this->container->get('user.info')->getUser();
        }

        foreach ($location as $key => $value) {

            if(array_key_exists($key, $paths)) {

                /** @var Attribute $attribute */
                $attribute = $repository->find($paths[$key]);
                $processHandler->editAttributeValue($locationPath . ',' . $paths[$key], $groupProcessId, $value, $attribute->getNameValueType(), 'xxx', $user->getId(), $user->getId());

            }

        }

        return new JsonResponse([]);

    }

    /**
     * @Route("/get-routes-points",
     *     name="map_get_routes_points",
     *     options={"expose"=true}
     * )
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function calculateRoutePoints(Request $request)
    {

        $data = $request->query->get('data');
        $isJson = $request->query->get('json', 0);

        $dataArray = explode('|', $data);
        $source = explode(':', array_shift($dataArray));

        $points = [];

        foreach ($dataArray as $item) {
            if(!empty($item)) {
                $point = explode(':', $item);
                $points[] = [
                    'id' => $point[0],
                    'lng' => $point[1],
                    'lat' => $point[2]
                ];
            }
        }

        if(!empty($source) && !empty($points)) {
            $result = $this->calculateRoute([$source[2], $source[1]], $points);
        }
        else {
            $result = [];
        }

        if($isJson) {
            return new JsonResponse($result);
        }
        else {

            $arrResult = [];

            foreach ($result as $item) {
                $arrResult[] = $item['id'] . ':' . (intval($item['TotalRouteLength']) / 1000);
            }

            $strResult = implode('|', $arrResult);

            return new Response($strResult);
        }
    }

    /**
     * @param $source
     * @param $points
     * @param int $method
     * @return array
     */
    public function calculateRoute($source, $points, $method = 1) {

        /** @var MapCenterClientPlugin $mapCenterPlugin */
        $mapCenterPlugin = $this->get('map.center.client.plugin');
        $sessionId = $this->getSession($mapCenterPlugin);

        if(!$sessionId) {
            return [];
        }

        try {

            $mapCenterPlugin->RoutePlannerRouteClear($sessionId);
            $mapCenterPlugin->RoutePlannerEntriesClear($sessionId);
//            $mapCenterPlugin->RouteAttributesClearRouteAttribute($sessionId);
//            $mapCenterPlugin->RouteRepositoryClear($sessionId);

            $sourcePoint = $mapCenterPlugin->createPoint($source[0], $source[1]);

            $results = [];

            $mapCenterPlugin->RoutePlannerEntryAdd($sessionId, $sourcePoint, 0);

//            $this->SetRoutePlannerDriverParams($mapCenterPlugin, $sessionId);
//            $this->SetRoutePlannerVehicleParams($mapCenterPlugin, $sessionId);
//            $this->SetRoutePlannerRoadParams($mapCenterPlugin, $sessionId);

            foreach ($points as $point) {

                $targetPoint = $mapCenterPlugin->createPoint($point['lat'], $point['lng']);

                try {

                    $mapCenterPlugin->RoutePlannerEntryAdd($sessionId, $targetPoint, 0);

                    /** Narazie używam domyślnych ustawień */

        //        - shortest = 0x00
        //        - quickest = 0x01
        //        - cheapest = 0x02

                    $result = $mapCenterPlugin->RoutePlannerCalculateRoute($sessionId,
                        1,
                        0,
                        0,
                        0,
                        0,true);


                    if(!empty($result) && $result['Result'] == 1) {
                        $results[] = [
                            'id' => $point['id'],
                            'TotalRouteLength' => $result['TotalRouteLength']
                        ];
                    }

                }
                catch (\Exception $exception) {

                }
                finally {

                    /** Usuniecie ostatnio dodane waypointa */
                    $mapCenterPlugin->RoutePlannerEntryRemove($sessionId, 1);

                }

            }

            return $results;

//            $result2 = $mapCenterPlugin->RoutePlannerGetRouteItinerary($sessionId);
//            $result3 = $mapCenterPlugin->RoutePlannerGetRouteSummary($sessionId, 1);

        }
        catch (\Exception $exception) {
            return [];

        }


    }

    private function SetRoutePlannerDriverParams(MapCenterClientPlugin $mapCenterPlugin, $sessionId) {

        $driverParams = [
            'JourneyStartTime' => 8 / 24,
            'DayWorkTime' => 8 / 24,
            'ContinuousWorkTime' => 5 / 24,
            'BreakTime' => 0.5 / 24,
            'CostPerKilometer' => 5,
            'CostPerHour'=> 10
        ];

        $mapCenterPlugin->RoutePlannerDriverParamsSet($sessionId, $driverParams);

    }

    private function SetRoutePlannerVehicleParams(MapCenterClientPlugin $mapCenterPlugin, $sessionId) {

        $vehicleParams = [
            'VehicleType' => 0,
            'IgnoreFuel' => true,
            'FixedCost' => 10,
            'CostPerKilometer' => 1,
            'CostPerHour' => 1,
            'TollRoadPerKilometer' => 2,
            'TankCapacity' => 40,
            'FuelCost' => 4,
            'RefuelTime' => 0.5,
            'VehicleWeight' => 0,
            'VehicleLength' => 0,
            'VehicleHeight' => 0,
            'VehicleWidth' => 0,
            'VehicleCapacity' => 0,
            'VehicleLoadCapacity' => 0,
            'ShippingTime' => 2,
            'BorderPassTime' => 5
        ];

        $mapCenterPlugin->RoutePlannerVehicleParamsSet($sessionId, $vehicleParams);

    }

    private function SetRoutePlannerRoadParams(MapCenterClientPlugin $mapCenterPlugin, $sessionId) {

        $params = [
            [
                "RoadType" => 4,
                "Use" => true,
                "Speed" => 50,
                "Combustion" => 10
            ]
        ];

        $mapCenterPlugin->RoutePlannerRoadParamsSet($sessionId, $params);

    }

    /**
     * @param MapCenterClientPlugin $mapCenterPlugin
     * @param bool $forceAnon
     * @return null|string
     * @throws \Exception
     */
    private function getSession(MapCenterClientPlugin $mapCenterPlugin, $forceAnon = false) {

        $redis = $this->get('snc_redis.default');
        $user = $this->getUser();

        if($user instanceof User && $forceAnon === false) {
            $key = self::REDIS_MAP_SESSION_KEY . '_' . $user->getId();
        }
        else {
            /**
             * Specjalny sposób na rezerwację sesji anonimowych
             * Występywały konflikty
             */
            $key = $this->getAnonSessionKey($redis);
//            $key = self::REDIS_MAP_SESSION_KEY . '_anon';
        }

        $sessionId = $redis->get($key);

        if($sessionId) {
            $this->setLanguageContext($mapCenterPlugin, $sessionId);
//            $mapCenterPlugin->setSessionComment($sessionId, self::SESSION_COMMENT);
            $mapCenterPlugin->keepSession($sessionId);
            $redis->expire($key, 1790);

        }
        else {

            $session = $mapCenterPlugin->createSessionID();
            $sessionId = $session['SessionID'];
            $mapCenterPlugin->SearchInitialize($sessionId, 1);
            $this->setLanguageContext($mapCenterPlugin, $sessionId);

            // sprawdzenie, czy udało się stworzyć nową sesje (bo czasami kończy się limit i się nie da) - wtedy próba pobrania ANON SESSION
            if(empty($sessionId)) {
                $redis->del([$key]);

                if($forceAnon === false) {
                    $this->getSession($mapCenterPlugin, true);
                }

                return null;
            }
            else {
                $mapCenterPlugin->setSessionComment($sessionId, self::SESSION_COMMENT . ' ' . $key . '_' . random_int(1,100));
                $mapCenterPlugin->keepSession($sessionId);
                $redis->setex($key, 1790, $sessionId);
            }

        }

        return $sessionId;

    }

    /**
     * @param Client $redis
     * @return string|null
     */
    private function getAnonSessionKey(Client $redis) {

        $sessionPrefix = self::REDIS_MAP_SESSION_KEY . '_anon_';

        for ($i = 0; $i < self::LIMIT_ANON_SESSION; $i++) {

            $existsKey = $redis->exists($sessionPrefix . $i);

            if($existsKey) {

                // sprawdzenie, czy klucz jest zarezerwowany
                $existsKey = $redis->exists($sessionPrefix . $i . '_LOCK');

                // Sesja nie jest zarezerwowana, można użyć
                if(!$existsKey) {

                    $this->currentAnonSessionKey = $sessionPrefix . $i;

                    // rezerwacja klucza na 10 sekund
                    $redis->setex($sessionPrefix . $i . '_LOCK', 10, 1);

                    return $this->currentAnonSessionKey;
                }

            }
            // Taka Sesja jeszcze nie została stworzona, rezerwuje i zwraca klucz
            else {

                $redis->setex($sessionPrefix . $i, 1790, null);
                $redis->setex($sessionPrefix . $i . '_LOCK', 10, 1);
                $this->currentAnonSessionKey = $sessionPrefix . $i;

                return $this->currentAnonSessionKey;

            }

        }

        return null;

    }

    /**
     * Używane jest przy destruktorze. Żeby zwolnić sesje.
     */
    private function unlockSessionKey() {
        $this->get('snc_redis.default')->del([$this->currentAnonSessionKey . '_LOCK']);
    }

    private function getAddress($addressData)
    {
        $address = [
            'number' => '',
            'street' => ''
        ];

        $trimmedAddress = trim($addressData);
        $addressDetails = explode(' ', $trimmedAddress);
        $strNumber = '';

        if (is_numeric($addressDetails[count($addressDetails) - 1])) {
            $address['number'] = $addressDetails[count($addressDetails) - 1];
        }
        else {

            $strNumber = $addressDetails[count($addressDetails) - 1];
            $address['number'] = intval($addressDetails[count($addressDetails) - 1]);

            if($address['number'] == 0) {
                $address['number'] = null;
            }

        }

        if(!empty($strNumber) && $address['number'] != null) {
            $address['street'] = trim(substr($trimmedAddress, 0 , strlen($trimmedAddress) - strlen($strNumber)));
        }
        else {
            $address['street'] = trim(substr($trimmedAddress, 0 , strlen($trimmedAddress) - strlen($address['number'])));
        }

        return $address;
    }

    private function handleCityDetails($cities, MapCenterClientPlugin $mapCenterPlugin, $sessionId, $withDeGeocode = false)
    {
        $address = [];
        for ($i = 0; $i < count($cities['CityNames']); $i++) {

            $address[$i]['city'] = $cities['CityNames'][$i];
            $cityDetails = explode('|', $cities['CityAdmNames'][$i]);
            $address[$i]['country'] = $cityDetails[0];
            $address[$i]['voivodeship'] = $cityDetails[1];
            $address[$i]['county'] = $cityDetails[2];
            $address[$i]['commune'] = $cityDetails[3];
            $address[$i]['zipCode'] = $cityDetails[10];
            $address[$i]['street'] = '';
            $address[$i]['inCity'] = true;
            $address[$i]['road'] = '';
            $address[$i]['streetNumber'] = '';

            if($withDeGeocode) {

                $inputPoints = [[
                    "City" => $cities['CityNames'][$i],
                    "Country" => $cityDetails[0],
                    "District" => $cityDetails[1],
                    "County" => $cityDetails[2],
                    "Commune" => $cityDetails[3],
                    "Zip" => $cityDetails[10],
                    "Street" => "",
                    "StreetNumber" => ""
                ]];

                $deGeocodesResult = $mapCenterPlugin->Geocode($sessionId, 1, $inputPoints);

                $address[$i]['latitude'] = $deGeocodesResult['Positions'][0]['Latitude'];
                $address[$i]['longitude'] = $deGeocodesResult['Positions'][0]['Longitude'];

            }

        }

        return $address;
    }

    /**
     * @param $countryName
     * @return String
     */
    private function translateCountry($countryName) {

        if(!isset($this->cachedCountry[$countryName])) {
            $this->cachedCountry[$countryName] = $this->get('app.query_manager')->translate($countryName);
        }

        return $this->cachedCountry[$countryName];

    }

    private function simpleDeGeocode($sessionId, MapCenterClientPlugin $mapCenterPlugin, $latitude, $longitude)
    {

        $point = $mapCenterPlugin->createPoint($latitude, $longitude);

        $result = $mapCenterPlugin->degeocodeAtPoint(
            $sessionId, $point, 100.0,
            0, 0, $this->currentProjections['Projections'][6],
            '', $point, self::QUERY_RADIUS * 1,
            1, $this->currentDeGeocodeLayers['DegeocodeLayers'], 1,
            1
        );

        return $result;

    }

    private function degeocode($latitude, $longitude)
    {
        /** @var MapCenterClientPlugin $mapCenterPlugin */
        $mapCenterPlugin = $this->get('map.center.client.plugin');
        $sessionId = $this->getSession($mapCenterPlugin);

        if(!$sessionId) {
            return [];
        }

        $result = [];

        try {

            $mapCenterPlugin->ClearMapSelection($sessionId);

            $projections = $mapCenterPlugin->getProjections();
            $degeocodeLayers = $mapCenterPlugin->getDegeocodeLayers($sessionId);
            $point = $mapCenterPlugin->createPoint($latitude, $longitude);

            /** Jeżeli nie znajdzie miejscowości, to podnosimy promień przeszukiwań */

            $tryAmount = 0;
            $multipleRadius = [1, 10, 30, 50];
            $inCity = null;

            while(empty($result['City'])) {

                $result = $mapCenterPlugin->degeocodeAtPoint(
                    $sessionId, $point, 100.0,
                    0, 0, $projections['Projections'][6],
                    '', $point, self::QUERY_RADIUS * $multipleRadius[$tryAmount],
                    20, $degeocodeLayers['DegeocodeLayers'], 1,
                    1
                );

                if($tryAmount === 0) {

                    if (!empty($result['City']) && $result['City'][0]['FoundLength'] == "0") {
                        $inCity = 1;
                    }
                    else {
                        $inCity = 0;
                    }
                }

                if(!empty($result['City']) || ((count($multipleRadius)-1) <= $tryAmount )) {

//                    /** Sprawdzenie czy lokalizacja za granicą, żeby pobrać nazwy miejscowości w innym języku */
//                    if(!empty($result['City']) && $result['AreaName0'] !== 'Polska') {
//
//                        $originalCountry = $result['AreaName0'];
//
//                        $countryCode = $this->getCountryCode($result['AreaName0']);
//
//                        if($countryCode) {
//                            $mapCenterPlugin->SetSessionLanguageContext($sessionId, 0, $countryCode);
//                        }
//                        else {
//                            $mapCenterPlugin->SetSessionLanguageContext($sessionId, 0, 'POL');
//                        }
//
//                        $result = $mapCenterPlugin->degeocodeAtPoint(
//                            $sessionId, $point, 100.0,
//                            0, 0, $projections['Projections'][6],
//                            '', $point, self::QUERY_RADIUS * $multipleRadius[$tryAmount],
//                            20, $degeocodeLayers['DegeocodeLayers'], 1,
//                            1
//                        );
//
//                        $result['AreaName0'] = $originalCountry;
//
//                    }

                    break;
                }

                $tryAmount++;
            }

            $result['inCity'] = $inCity;

        }
        catch (\Exception $e) {

            return [];

        }
        finally {

//            if($sessionId) {
//                $mapCenterPlugin->DropSession($sessionId);
//            }

        }

        return $result;
    }

    /**
     * @param $result
     * @return array
     */
    private function tryGetStreetNumber($result) {
        $streetNumber = '';

        if (($mapElement = $this->getFirstPointNumeration($result)) !== null) {

            $address = $mapElement['Name'];
            $addressElements = explode(' ', $address);

            $streetNumber = array_pop($addressElements);
            $street = implode(" ", $addressElements);

        } else {
            $street = empty($result['Street'][0]['Name']) ? '' : $result['Street'][0]['Name'];
        }

//        if(empty($street)) $street = $this->getBestStreet($result);

        return [$streetNumber, $street];
    }

    private function getRoad($result) {
        if(isset($result['Road'])) {
            if(is_array($result['Road']) && !empty($result['Road'])) {
                if(isset($result['Road'][0]['Name'])) {
                    return $result['Road'][0]['Name'];
                }
            }
        }
        return "";
    }

    private function getFirstPointNumeration($result) {
        if (!empty($result['OtherMapElements'])) {

            foreach ($result['OtherMapElements'] as $key => $otherMapElement) {
                if($otherMapElement['Category'] === self::MAP_ELEMENTS_CATEGORY) {
                    return $otherMapElement;
                }
            }
        }

        return null;
    }

    /**
     * @Route("/get_map_statistic",
     *     name="get_statistic",
     *     options={"expose"=true}
     * )
     * @Security("is_granted('ROLE_ADMIN')")
     * @return JsonResponse
     */
    public function getStatisticAction()
    {

        /** @var MapCenterClientPlugin $mapCenterPlugin */
        $mapCenterPlugin = $this->get('map.center.client.plugin');
        $mapCenterPlugin->setUser('admin', 'admin');

        $result = $this->checkSessions($mapCenterPlugin);

        return new JsonResponse($result);

    }

    /**
     * @Route("/map-center-check-ip",
     *     name="map_center_check_ip",
     *     options={"expose"=true}
     * )
     * @Security("is_granted('ROLE_ADMIN')")
     * @return JsonResponse
     */
    public function checkIpsAction()
    {

        /** @var MapCenterClientPlugin $mapCenterPlugin */
        $mapCenterPlugin = $this->get('map.center.client.plugin');
        $mapCenterPlugin->setUser('admin', 'admin');

        $result = $this->checkIpLive($mapCenterPlugin);

        return new JsonResponse($result);

    }


    /**
     * @Route("/map-center/clean_sessions",
     *     name="map_center_clean_sessions"
     * )
     * @return JsonResponse
     */
    public function cleanSessionsMapCenterAction()
    {

        /** @var MapCenterClientPlugin $mapCenterPlugin */
        $mapCenterPlugin = $this->get('map.center.client.plugin');
        $mapCenterPlugin->setUser('admin', 'admin');

        $result = $this->checkIpLive($mapCenterPlugin);

        $response = [
            'cleanedSessions' => 0,
            'time' => null,
            'ips' => []
        ];

        $start = time();

        foreach ($result['dead'] as $item) {
            try{
                $mapCenterPlugin->AdminDropSession($item['id'], $item['ip'], "");
                $response['ips'][] = $item['ip'];
                $response['cleanedSessions']++;
            }
            catch (\Exception $exception) {
                return new JsonResponse([
                    'error' => true,
                    'message' => $exception->getMessage()
                ]);
            }
        }

        $response['time'] = time() - $start;

        return new JsonResponse($response);

    }


    /**
     * Ubijanie sesji na mapie
     * https://atlas.starter24.pl/drop_session_map?ip=10.10.60.13&id=3441374416
     *
     * @Route("/drop_session_map",
     *     name="drop_session_map",
     *     options={"expose"=true}
     * )
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return JsonResponse
     */
    public function dropSessionAction(Request $request)
    {
        $ip = $request->query->get('ip', null);
        $id = $request->query->get('id', null);

        $response = [
            'success' => false,
            'ip' => $ip,
            'id' => $id
        ];

        try {

            /** @var MapCenterClientPlugin $mapCenterPlugin */
            $mapCenterPlugin = $this->get('map.center.client.plugin');
            $mapCenterPlugin->setUser('admin', 'admin');

            $data = $mapCenterPlugin->AdminDropSession($id, $ip, "");

            $response['success'] = true;
            $response['message'] = $data;

        }
        catch (\Exception $e) {

        }


        return new JsonResponse($response);

    }

    private function pingAddress($ip) {

        $pingresult = exec("/bin/ping -c 1 -W 3 $ip", $outcome, $status);

        if (0 == $status) {
            return 1;
        } else {
            return 0;
        }
    }

    private function checkIpLive(MapCenterClientPlugin $mapCenterPlugin) {

        $live = [];
        $dead = [];

        $sessions = $mapCenterPlugin->GetActiveSessions();

        foreach ($sessions['Sessions'] as $activeSession) {

            if(!$this->pingAddress($activeSession['SessionIP'])) {
                $dead[] = [
                    'ip' => $activeSession['SessionIP'],
                    'id' => $activeSession['SessionID'],
                    'url' => '?ip=' . $activeSession['SessionIP'] . '&id=' . $activeSession['SessionID']
                ];
            }
            else {
                $live[] = $activeSession['SessionIP'];
            }
        }

        return [
            'live' => $live,
            'dead' => $dead
        ];

    }

    private function checkSessions(MapCenterClientPlugin $mapCenterPlugin) {

        $ipsArray = [];
        $duplicateIps = [];
        $atlasIps = '';
        $sessions = $mapCenterPlugin->GetActiveSessions();

        foreach ($sessions['Sessions'] as $activeSession) {

            if(!empty($activeSession['SessionComment'])) {
                $atlasIps .= ', ' . $activeSession['SessionIP'];
            }
            else {
                if(in_array($activeSession['SessionIP'], $ipsArray)) {
                    $duplicateIps[] = $activeSession['SessionIP'];
                }
                $ipsArray[] = $activeSession['SessionIP'];
            }

        }

        return [
            'ServerVersion' => $mapCenterPlugin->GetServerVersion(),
            'CurrentSessionCount' => $mapCenterPlugin->GetCurrentSessionCount(),
            'MaxSessionsCount' => $mapCenterPlugin->GetMaxSessionsCount(),
            'SessionLifetimeSeconds' => $mapCenterPlugin->GetSessionLifetimeSeconds(),
            'ips' => $ipsArray,
            'duplicateIps' => $duplicateIps,
            'atlas_ips' => $atlasIps,
            'sessions' => $sessions
        ];

    }

    /**
     * @Route("/get_object/{name}",
     *     name="map_get_object",
     *     options={"expose"=true}
     * )
     * @param $name
     * @return JsonResponse
     */
    public function getObject($name)
    {

        /** [ street_name [, description [km] ] [, km] */

        $objects = [];
        $description = null;
        $km = null;

        $name = trim($name);

        if(strpos($name, ",") !== false) {
            /** przecinek jako separator */

            $arrValue = explode(",", $name);

            $street = $arrValue[0];
            $descOrKm = explode(" ", trim($arrValue[1]));

            list($km, $description) = $this->parseArrayValue($descOrKm);

        }
        else if(strpos($name, " ") !== false) {

            $arrValue = explode(" ", $name);

            $street = array_shift($arrValue);

            list($km, $description) = $this->parseArrayValue($arrValue);

        }
        else {
            $street = $name;
        }

        $poiArray = $this->getPoiArray($street, $description, $km);

//        /** Stara metoda */
//        $poiArray = $this->getPoiArray_old($street, $km, 200);

        if($poiArray) {

            /** @var Poi[] $poiArray */
            foreach ($poiArray as $poi) {
                $desc = $poi->getStreet() . " " . $poi->getPoiKm() . " km, " . $poi->getPoiDescription();

                $objects[] = [
                    'value' => $desc,
                    'label' => $desc,
                    'description' => '[SŁUPEK: '.$poi->getPoiKm().' km, KIERUNEK: '. $poi->getPoiDescription() .']',
                    'coordinates' => $poi->getCoordinate()
                ];
            }
        }

        return new JsonResponse([
            'objects' => $objects
        ]);
    }

    private function getPoiArray($street, $description, $km) {

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata('MapBundle:Poi', 'p');
        $params = new ArrayCollection();

        $sql = "SELECT TOP 1000 p.* FROM poi p WHERE p.street LIKE :street ";

        if($km) {
            $sql .= "and p.poi_km LIKE :km ";
            $params->add(new Parameter('km', $km . "%"));
        }

        if($description) {
            $sql .= "and p.poi_km between ( select top 1 poi_km from poi where poi_description like :description) - 10  and (select top 1 poi_km from  poi where  poi_description like :description) + 10";
            $params->add(new Parameter('description', "%".$description."%"));
        }

        $params->add(new Parameter('street', '%' . $street . '%'));

        $nativeQuery = $em->createNativeQuery($sql, $rsm);
        $nativeQuery->setParameters($params);

        return $nativeQuery->getResult();

    }

    private function getPoiArray_old($street, $km, $limit = 200) {

        $poiArray = $this->getDoctrine()->getRepository('MapBundle:Poi')->findPoi($street, $km, $limit);

        return $poiArray;

    }

    private function parseArrayValue($descOrKm) {
        $km = null;
        $description = null;

        if(count($descOrKm) == 1 && $descOrKm[0] === "") {
            return [$km, $description];
        }

        if(count($descOrKm) == 1) {
            if(is_numeric($descOrKm[0][0])) {
                $km = $descOrKm[0];
            }
            else {
                $description = $descOrKm[0];
            }
        }
        else if(count($descOrKm) == 2) {

            if(is_numeric($descOrKm[1][0])) {
                /** Jest nazwa drogi i liczba kilometrów */
                list($description, $km) = $descOrKm;
            }
            else {
                $description = implode(" ", $descOrKm);
            }

        }
        else {

            if(is_numeric($descOrKm[count($descOrKm)-1][0])) {
                $km = array_pop($descOrKm);
            }

            $description = implode(" ", $descOrKm);
        }

        return [$km, $description];
    }

    private function getCountryCode($countryName) {

        /** @var Client $redis */
        $redis = $this->container->get('snc_redis.default');

        $this->checkCountryMemory($redis);

        return $redis->hget(self::COUNTRY_CODES, $countryName);
    }

    private function checkCountryMemory(Client $redis) {

        if(!$redis->exists(self::COUNTRY_CODES)) {

            $countries = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'country'
            ]);

            foreach ($countries as $country) {
                $redis->hset(self::COUNTRY_CODES, $country->getText(), $country->getArgument2());
            }

        }

    }

    private function setVirtualUser() {
        $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(['username' => 'virtual_partner']);
        if(!$user instanceof User) {
            throw new NotFoundResourceException('Not found Virtual Partner!');
        }
        $this->get('user.info')->setVirtualUser($user);
    }

//
//        $allObjects = [
//            [
//                'value' => 'MOP na autostradzie A1 w Olszach',
//                'label' => 'MOP na autostradzie A1 w Olszach',
//                'coordinates' => '53.887943|18.630589'
//            ]
//        ];
//        $name = strtolower($name);
//
//        $objects = array_filter($allObjects, function ($ele) use ($name) {
//
//            return (strpos(strtolower($ele['value']), $name) !== false);
//
//        });

}
