<?php

namespace MapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * PositionRequest
 *
 * @ORM\Table(name="position_request")
 * @ORM\Entity(repositoryClass="MapBundle\Repository\PositionRequestRepository")
 */
class PositionRequest
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Przepraszam, że nie ma "foreign key"a  : (
     *
     * @var int
     *
     * @ORM\Column(name="group_process_id", type="integer")
     */
    private $groupProcessId;

    /**
     * @var string
     *
     * @ORM\Column(name="uniqueId", type="string", length=64, unique=true)
     */
    private $uniqueId;

    /**
     * @var int
     *
     * @ORM\Column(name="used", type="integer", options={"defaults" : 0})
     */
    private $used = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $longitude;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniqueId
     *
     * @param string $uniqueId
     *
     * @return PositionRequest
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * Get uniqueId
     *
     * @return string
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set used
     *
     * @param integer $used
     *
     * @return PositionRequest
     */
    public function setUsed($used)
    {
        $this->used = $used;

        return $this;
    }

    /**
     * Get used
     *
     * @return int
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return PositionRequest
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return PositionRequest
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return int
     */
    public function getGroupProcessId()
    {
        return $this->groupProcessId;
    }

    /**
     * @param int $groupProcessId
     */
    public function setGroupProcessId($groupProcessId)
    {
        $this->groupProcessId = $groupProcessId;
    }
}

