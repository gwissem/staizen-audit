<?php

namespace MapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Poi
 *
 * @ORM\Table(name="poi")
 * @ORM\Entity(repositoryClass="MapBundle\Repository\PoiRepository")
 */
class Poi
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=10, scale=7)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=10, scale=7)
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=64)
     */
    private $street;

    /**
     * @var int
     *
     * @ORM\Column(name="poi_km", type="integer")
     */
    private $poiKm;

    /**
     * @var int
     *
     * @ORM\Column(name="order_km", type="integer")
     */
    private $orderKm;

    /**
     * @var string
     *
     * @ORM\Column(name="poi_description", type="string", length=255)
     */
    private $poiDescription;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return Poi
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     *
     * @return Poi
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Poi
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set poiKm
     *
     * @param integer $poiKm
     *
     * @return Poi
     */
    public function setPoiKm($poiKm)
    {
        $this->poiKm = $poiKm;

        return $this;
    }

    /**
     * Get poiKm
     *
     * @return int
     */
    public function getPoiKm()
    {
        return $this->poiKm;
    }

    /**
     * Set orderKm
     *
     * @param integer $orderKm
     *
     * @return Poi
     */
    public function setOrderKm($orderKm)
    {
        $this->orderKm = $orderKm;

        return $this;
    }

    /**
     * Get orderKm
     *
     * @return int
     */
    public function getOrderKm()
    {
        return $this->orderKm;
    }

    /**
     * Set poiDescription
     *
     * @param string $poiDescription
     *
     * @return Poi
     */
    public function setPoiDescription($poiDescription)
    {
        $this->poiDescription = $poiDescription;

        return $this;
    }

    /**
     * Get poiDescription
     *
     * @return string
     */
    public function getPoiDescription()
    {
        return $this->poiDescription;
    }

    public function getCoordinate() {
        return $this->getLat() . "|" . $this->getLng();
    }
}

