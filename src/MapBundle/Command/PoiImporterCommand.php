<?php

namespace MapBundle\Command;

use Doctrine\ORM\EntityManager;
use MapBundle\Entity\Poi;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class PoiImporterCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:map:poi-import')
            ->setDescription('Import POI from file.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln([
            '',
            '<comment>Running...</comment>',
            '===================',
            '',
        ]);

        $path = $this->getContainer()->getParameter('kernel.root_dir') . '/../web/poi_table.csv';
        $fs = $this->getContainer()->get('filesystem');

        if($fs->exists($path)) {

            /** @var EntityManager $em */
            $em = $this->getContainer()->get('doctrine.orm.entity_manager');

            $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
            $data = $serializer->decode(file_get_contents($path), 'csv');

            /** WITH REMOVE ALL */

            $poiRepo = $em->getRepository('MapBundle:Poi');
            $pois = $poiRepo->findAll();

            foreach ($pois as $poi) {
                $em->remove($poi);
            }

            $em->flush();

            $i = 0;

            foreach ($data as $poi) {

                $i++;
                $newPoi = new Poi();
                $newPoi->setLat(floatval(str_replace(",", ".", $poi['lat'])));
                $newPoi->setLng(floatval(str_replace(",", ".", $poi['lng'])));
                $newPoi->setStreet($poi['street']);
                $newPoi->setPoiKm(intval($poi['poi_km']));
                $newPoi->setOrderKm(intval($poi['order_km']));
                $newPoi->setPoiDescription($poi['poi_description']);

                $em->persist($newPoi);
            }

            $em->flush();

            $output->writeln([
                '<info>Result</info>',
                '==================='
            ]);

            $output->writeln([
                '',
                "Wrzucono " . $i . " obiektów.",
                ''
            ]);

        }
        else {
            $output->writeln([
                '',
                '<comment>File do not exists.</comment>',
                '===================',
                '',
            ]);
        }

        $output->writeln(sprintf("%s\n", "<info>End process.</info>"));
    }
}
