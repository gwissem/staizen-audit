<?php
include('XMLDOMClientPlugin.php');
$xml = new XMLDOMClientPlugin;
$xml->url = 'http://10.10.77.93:6090/xml?version=2.0';
$res = $xml->CreateSessionID();
$sesja_id = $res['SessionID'];
echo "Nr sesji = $sesja_id\n";
$xml->SetSessionComment($sesja_id, 'Sesja z PHP');
$xml->KeepSession($sesja_id);
$layers = $xml->GetDefaultLayers($sesja_id);

$projections = $xml->GetProjections();
$DegeocodeLayers = $xml->GetDegeocodeLayers($sesja_id);

$arrayOfPoints = [
    array('Longitude' => '52.4084155', 'Latitude' => '16.901937')
];

$result = $xml->Degeocode($sesja_id,
    array('Longitude' => '52.4084155', 'Latitude' => '16.901937'),
    400.0,
    0,
    0,
    $projections['Projections'][6],
    '',
    $arrayOfPoints,
    400,
    $DegeocodeLayers['DegeocodeLayers'],
    true,
    false
);

//Degeocode($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $MapPoints,
//    $QueryRadius, $DegeocodeLayers, $OnlyNamedEntries, $UseViewVisibility)
//
//
//RenderMapOnImageByPoint($SessionID, $MimeType, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
//                                            $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams)

//$img = $xml->RenderMapOnImageByPoint($sesja_id,
//	'image/png',
//	array('Longitude'=>'51.0', 'Latitude'=>'19.0'),
//	4000.0,
//	0.0,
//	0.0,
//	'Mercator',
//	'',
//	800,
//	600,
//	$layers['MapLayers'],
//	array('RenderWithoutSessionObjects'=>0, 'DPI'=>96, 'Antialiasing'=>1, 'RenderParams'=>''));
////echo $img['BitmapImage'];
//foreach($img['BitmapRightUpCorner'] as $key => $value)
//	echo "$key = $value\n";
//


$xml->DropSession($sesja_id);
?>