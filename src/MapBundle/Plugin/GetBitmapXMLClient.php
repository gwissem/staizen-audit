<?php

include('XMLDOMClientPlugin.php');

$pre_width = '400';
$pre_height = '300';
$pre_long = '15';
$pre_lat = '51';
$pre_alt = '4000';
$pre_mime = 'image/png';

if ($_GET['long'] != '') {
    $pre_long = $_GET['long'];
}
if ($_GET['lat'] != '') {
    $pre_lat = $_GET['lat'];
}
if ($_GET['alt'] != '') {
    $pre_alt = $_GET['alt'];
}
if ($_GET['width'] != '') {
    $pre_width = $_GET['width'];
}
if ($_GET['height'] != '') {
    $pre_height = $_GET['height'];
}
if ($_GET['mime'] != '') {
    $pre_mime = $_GET['mime'];
}

$xml = new XMLDOMClientPlugin;
$sesja_id = $xml->CreateSessionID();
$layers = $xml->GetDefaultLayers($sesja_id['SessionID']);
$img = $xml->RenderMapOnImageByPoint(
    $sesja_id['SessionID'],
    'image/png',
    array('Longitude' => $pre_long, 'Latitude' => $pre_lat),
    $pre_alt,
    0.0,
    0.0,
    'Mercator',
    '',
    $pre_width,
    $pre_height,
    $layers['MapLayers'],
    array('RenderWithoutSessionObjects' => 0, 'DPI' => 96, 'Antialiasing' => 1, 'RenderParams' => ''));

header('Content-Type: ' . $pre_mime);

echo $img['BitmapImage'];

$xml->DropSession($sesja_id['SessionID']);
?>
