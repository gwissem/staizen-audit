<?php

include('XMLDOMClientPlugin.php');

$pre_width = '400';
$pre_height = '300';
$pre_long = '15';
$pre_lat = '51';
$pre_alt = '4000';
$pre_mime = 'image/png';

if ($_GET['long'] != '') {
    $pre_long = $_GET['long'];
}
if ($_GET['lat'] != '') {
    $pre_lat = $_GET['lat'];
}
if ($_GET['alt'] != '') {
    $pre_alt = $_GET['alt'];
}
if ($_GET['width'] != '') {
    $pre_width = $_GET['width'];
}
if ($_GET['height'] != '') {
    $pre_height = $_GET['height'];
}
if ($_GET['mime'] != '') {
    $pre_mime = $_GET['mime'];
}

$xml = new XMLDOMClientPlugin;
$sesja_id = $xml->CreateSessionID();
$layers = $xml->GetDefaultLayers($sesja_id['SessionID']);

$errorcode = $xml->LocalizeObjectAdd(
    $sesja_id['SessionID'],
    1,
    'Object',
    0,
    0,
    0,
    0x00000000,
    0x00FF0000,
    2,
    4,
    0,
    1,
    1,
    array('Name' => 'Arial', 'Size' => 8, 'Color' => 0x00000000, 'Style' => 0, 'Charset' => 1));

$errorcode = $xml->LocalizeObjectPositionAdd(
    $sesja_id['SessionID'],
    1,
    array(),
    array(),
    array('Longitude' => $pre_long - 0.01, 'Latitude' => $pre_lat + 0.01),
    0.0,
    'Position 1',
    1,
    0,
    0,
    0,
    0x000000000,
    array('Name' => 'Arial', 'Size' => 8, 'Color' => 0x00000000, 'Style' => 0, 'Charset' => 1));

$errorcode = $xml->LocalizeObjectPositionAdd(
    $sesja_id['SessionID'],
    1,
    array(),
    array(),
    array('Longitude' => $pre_long + 0.01, 'Latitude' => $pre_lat - 0.01),
    0.0,
    'Position 2',
    1,
    0,
    0,
    0,
    0x00000000,
    array('Name' => 'Arial', 'Size' => 8, 'Color' => 0x00000000, 'Style' => 0, 'Charset' => 1));

$img = $xml->RenderMapOnImageByPoint(
    $sesja_id['SessionID'],
    'image/png',
    array('Longitude' => $pre_long, 'Latitude' => $pre_lat),
    $pre_alt,
    0.0,
    0.0,
    'Mercator',
    '',
    $pre_width,
    $pre_height,
    $layers['MapLayers'],
    array('RenderWithoutSessionObjects' => 0, 'DPI' => 96, 'Antialiasing' => 1, 'RenderParams' => ''));

header('Content-Type: ' . $pre_mime);

echo $img['BitmapImage'];

$xml->DropSession($sesja_id['SessionID']);
?>
