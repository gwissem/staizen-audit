<?php

interface PostIntf
{
    function PostQuery($querydoc);
}

class XMLClientPoster implements PostIntf
{
    function PostQuery($querydoc)
    {
    }
}

class XMLDOMClientPlugin
{
    private $conn_params = array();
    private $curl_conn;

    function __construct()
    {
        $this->conn_params = array('url' => 'http://localhost:6090/xml?version=2.0', 'username' => '', 'password' => '');
        $this->curl_conn = curl_init();
        curl_setopt($this->curl_conn, CURLOPT_URL, $this->conn_params['url']);
        if ($this->conn_params['username'] != '')
            curl_setopt($this->curl_conn, CURLOPT_USERPWD, $this->conn_params['username'] . ':' . $this->conn_params['password']);
    }

    function __destruct()
    {
        curl_close($this->curl_conn);
    }

    function __get($n)
    {
        return $this->conn_params[$n];
    }

    function __set($n, $v)
    {
        if (isset($this->conn_params[$n])) {
            $this->conn_params[$n] = $v;
            if ($n == 'username' || $n == 'password')
                curl_setopt($this->curl_conn, CURLOPT_USERPWD, $this->conn_params['username'] . ':' . $this->conn_params['password']);
            if ($n == 'url')
                curl_setopt($this->curl_conn, CURLOPT_URL, $this->conn_params['url']);
        }
    }

    public function CreateSessionID()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'CreateSessionID');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SessionID = $this->ParseTextValue($xpath, '/ANSWER/SessionID/text()');
        return array('Result' => $RESULT, 'SessionID' => $SessionID);
    }

    private function AddTextValue($querydoc, $root, $name, $value)
    {
        //echo "Adding $name = $value\n";
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        $val_node = $querydoc->createTextNode($value);
        $node->appendChild($val_node);
    }

    private function PostQuery($querydoc)
    {
        curl_setopt($this->curl_conn, CURLOPT_HEADER, 0);
        curl_setopt($this->curl_conn, CURLOPT_POST, 1);
        curl_setopt($this->curl_conn, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl_conn, CURLOPT_HEADER, false);
        curl_setopt($this->curl_conn, CURLOPT_HTTPHEADER, array("Content-Type: application/xml;charset=\"utf-8\""));

        curl_setopt($this->curl_conn, CURLOPT_POSTFIELDS, $querydoc->saveXML());
        $xml_response = curl_exec($this->curl_conn);

        $returndoc = new DOMDocument();
        if (!$returndoc->loadXML($xml_response)) {
            echo "Error while parsing the document\n";
            exit;
        }
        return $returndoc;
    }

    private function ParseTextValue($xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0) {
            echo "No elems found in $path";
            return;
        }
        return $items->item(0)->nodeValue;
    }

    public function KeepSession($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'KeepSession');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function DropSession($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DropSession');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetSessionComment($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetSessionComment');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Comment = $this->ParseTextValue($xpath, '/ANSWER/Comment/text()');
        return array('Result' => $RESULT, 'Comment' => $Comment);
    }

    public function SetSessionComment($SessionID, $Comment)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetSessionComment');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Comment', $Comment);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetSessionLanguageContext($SessionID, $ContextIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetSessionLanguageContext');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ContextIndex', $ContextIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $LanguageName = $this->ParseTextValue($xpath, '/ANSWER/LanguageName/text()');
        return array('Result' => $RESULT, 'LanguageName' => $LanguageName);
    }

    public function SetSessionLanguageContext($SessionID, $ContextIndex, $LanguageName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetSessionLanguageContext');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ContextIndex', $ContextIndex);
        $this->AddTextValue($querydoc, $root, 'LanguageName', $LanguageName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetMapModules()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetMapModules');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Modules = $this->ParseArrayValue($xpath, '/ANSWER/Modules');
        return array('Result' => $RESULT, 'Modules' => $Modules);
    }

    private function ParseArrayValue($xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0) {
            echo "No elems found in $path";
            return;
        }
        $returnval = array();
        $record = $items->item(0);
        for ($i = 0; $i < $record->childNodes->length; $i++) {
            $item = $record->childNodes->item($i);
            $name = $item->nodeName;
            $value = $item->firstChild->nodeValue;
            $returnval[] = $value;
        }
        return $returnval;
    }

    public function GetServerVersion()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetServerVersion');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ServerVersion = $this->ParseTextValue($xpath, '/ANSWER/ServerVersion/text()');
        $DataCompilationDate = $this->ParseArrayValue($xpath, '/ANSWER/DataCompilationDate');
        return array('Result' => $RESULT, 'ServerVersion' => $ServerVersion, 'DataCompilationDate' => $DataCompilationDate);
    }

    public function GetMaxSessionsCount()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetMaxSessionsCount');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SessionCount = $this->ParseTextValue($xpath, '/ANSWER/SessionCount/text()');
        return array('Result' => $RESULT, 'SessionCount' => $SessionCount);
    }

    public function GetCurrentSessionCount()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetCurrentSessionCount');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SessionCount = $this->ParseTextValue($xpath, '/ANSWER/SessionCount/text()');
        return array('Result' => $RESULT, 'SessionCount' => $SessionCount);
    }

    public function GetSessionLifetimeSeconds()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetSessionLifetimeSeconds');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SessionLifetime = $this->ParseTextValue($xpath, '/ANSWER/SessionLifetime/text()');
        return array('Result' => $RESULT, 'SessionLifetime' => $SessionLifetime);
    }

    public function GetActiveSessions()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetActiveSessions');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Sessions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Sessions');
        return array('Result' => $RESULT, 'Sessions' => $Sessions);
    }

    private function ParseArrayOfRecordsValue($xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0) {
            echo "No elems found in $path";
            return;
        }
        $returnval = array();
        $array = $items->item(0);
        for ($i = 0; $i < $array->childNodes->length; $i++) {
            $record = $array->childNodes->item($i);
            $entries = array();
            for ($j = 0; $j < $record->childNodes->length; $j++) {
                $entry = $record->childNodes->item($j);
                $entries[$entry->nodeName] = $entry->nodeValue;
            }
            $returnval[] = $entries;
        }
        return $returnval;
    }

    public function AdminDropSession($SessionID, $SessionIP, $SessionUserName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AdminDropSession');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'SessionIP', $SessionIP);
        $this->AddTextValue($querydoc, $root, 'SessionUserName', $SessionUserName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetProjections()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetProjections');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Projections = $this->ParseArrayValue($xpath, '/ANSWER/Projections');
        return array('Result' => $RESULT, 'Projections' => $Projections);
    }

    public function GetAvailableImageFormats()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetAvailableImageFormats');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ImageFormats = $this->ParseArrayValue($xpath, '/ANSWER/ImageFormats');
        return array('Result' => $RESULT, 'ImageFormats' => $ImageFormats);
    }

    public function GetDefaultLayers($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetDefaultLayers');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MapLayers = $this->ParseArrayValue($xpath, '/ANSWER/MapLayers');
        return array('Result' => $RESULT, 'MapLayers' => $MapLayers);
    }

    public function GetAvailableMapFiles()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetAvailableMapFiles');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MapFiles = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/MapFiles');
        return array('Result' => $RESULT, 'MapFiles' => $MapFiles);
    }

    public function GetLoadedMapsRegion()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetLoadedMapsRegion');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CurrentMapSize = $this->ParseRecordValue($xpath, '/ANSWER/CurrentMapSize');
        return array('Result' => $RESULT, 'CurrentMapSize' => $CurrentMapSize);
    }

    private function ParseRecordValue($xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0) {
            echo "No elems found in $path";
            return;
        }
        $returnval = array();
        $record = $items->item(0);
        for ($i = 0; $i < $record->childNodes->length; $i++) {
            $item = $record->childNodes->item($i);
            $name = $item->nodeName;
            $value = $item->firstChild->nodeValue;
            $returnval[$name] = $value;
        }
        return $returnval;
    }

    public function ReloadMapFiles()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ReloadMapFiles');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function ActivateMapFile($FileName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ActivateMapFile');
        $this->AddTextValue($querydoc, $root, 'FileName', $FileName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function DeactivateMapFile($FileName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DeactivateMapFile');
        $this->AddTextValue($querydoc, $root, 'FileName', $FileName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetViewConfigList($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetViewConfigList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ViewConfigList = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ViewConfigList');
        return array('Result' => $RESULT, 'ViewConfigList' => $ViewConfigList);
    }

    public function GetCurrentViewConfig($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetCurrentViewConfig');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ViewConfigName = $this->ParseTextValue($xpath, '/ANSWER/ViewConfigName/text()');
        return array('Result' => $RESULT, 'ViewConfigName' => $ViewConfigName);
    }

    public function SetCurrentViewConfig($SessionID, $ViewConfigName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetCurrentViewConfig');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ViewConfigName', $ViewConfigName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LoadCurrentViewConfig($SessionID, $ViewConfigBlob)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LoadCurrentViewConfig');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddBinaryValue($querydoc, $root, 'ViewConfigBlob', $ViewConfigBlob);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    private function AddBinaryValue($querydoc, $root, $name, $value)
    {
        //echo "Adding $name = $value\n";
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        $val_node = $querydoc->createCDATASection(base64_encode($value));
        $node->appendChild($val_node);
    }

    public function AuthCreateUser($Username, $Password, $Comment, $AccountEnabled, $Groups)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthCreateUser');
        $this->AddTextValue($querydoc, $root, 'Username', $Username);
        $this->AddTextValue($querydoc, $root, 'Password', $Password);
        $this->AddTextValue($querydoc, $root, 'Comment', $Comment);
        $this->AddTextValue($querydoc, $root, 'AccountEnabled', $AccountEnabled);
        $this->AddArrayValue($querydoc, $root, 'Groups', $Groups);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    private function AddArrayValue($querydoc, $root, $name, $array)
    {
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        foreach ($array as $key => $value) {
            $subnode = $querydoc->createElement('ITEM');
            $node->appendChild($subnode);
            $val_node = $querydoc->createTextNode($value);
            $subnode->appendChild($val_node);
        }
    }

    public function AuthGetUserParams($Username)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthGetUserParams');
        $this->AddTextValue($querydoc, $root, 'Username', $Username);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Comment = $this->ParseTextValue($xpath, '/ANSWER/Comment/text()');
        $AccountEnabled = $this->ParseTextValue($xpath, '/ANSWER/AccountEnabled/text()');
        $Groups = $this->ParseArrayValue($xpath, '/ANSWER/Groups');
        return array('Result' => $RESULT, 'Comment' => $Comment, 'AccountEnabled' => $AccountEnabled, 'Groups' => $Groups);
    }

    public function AuthGetUserParamEx($Username, $ParamName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthGetUserParamEx');
        $this->AddTextValue($querydoc, $root, 'Username', $Username);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamValue = $this->ParseTextValue($xpath, '/ANSWER/ParamValue/text()');
        return array('Result' => $RESULT, 'ParamValue' => $ParamValue);
    }

    public function AuthSetUserParamEx($Username, $ParamName, $ParamValue)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthSetUserParamEx');
        $this->AddTextValue($querydoc, $root, 'Username', $Username);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        $this->AddTextValue($querydoc, $root, 'ParamValue', $ParamValue);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AuthModifyUser($Username, $Comment, $AccountEnabled, $Groups)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthModifyUser');
        $this->AddTextValue($querydoc, $root, 'Username', $Username);
        $this->AddTextValue($querydoc, $root, 'Comment', $Comment);
        $this->AddTextValue($querydoc, $root, 'AccountEnabled', $AccountEnabled);
        $this->AddArrayValue($querydoc, $root, 'Groups', $Groups);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AuthModifyUserPassword($Username, $Password)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthModifyUserPassword');
        $this->AddTextValue($querydoc, $root, 'Username', $Username);
        $this->AddTextValue($querydoc, $root, 'Password', $Password);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AuthDeleteUser($Username)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthDeleteUser');
        $this->AddTextValue($querydoc, $root, 'Username', $Username);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AuthGetUsersList()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthGetUsersList');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $UsersList = $this->ParseArrayValue($xpath, '/ANSWER/UsersList');
        return array('Result' => $RESULT, 'UsersList' => $UsersList);
    }

    public function AuthCreateGroup($GroupName, $GroupComment)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthCreateGroup');
        $this->AddTextValue($querydoc, $root, 'GroupName', $GroupName);
        $this->AddTextValue($querydoc, $root, 'GroupComment', $GroupComment);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AuthGetGroup($GroupName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthGetGroup');
        $this->AddTextValue($querydoc, $root, 'GroupName', $GroupName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GroupComment = $this->ParseTextValue($xpath, '/ANSWER/GroupComment/text()');
        return array('Result' => $RESULT, 'GroupComment' => $GroupComment);
    }

    public function AuthModifyGroup($GroupName, $GroupComment)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthModifyGroup');
        $this->AddTextValue($querydoc, $root, 'GroupName', $GroupName);
        $this->AddTextValue($querydoc, $root, 'GroupComment', $GroupComment);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AuthDeleteGroup($GroupName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthDeleteGroup');
        $this->AddTextValue($querydoc, $root, 'GroupName', $GroupName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AuthGetGroupsList()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthGetGroupsList');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GroupsList = $this->ParseArrayValue($xpath, '/ANSWER/GroupsList');
        return array('Result' => $RESULT, 'GroupsList' => $GroupsList);
    }

    public function ServiceConfigGetServiceParams()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ServiceConfigGetServiceParams');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamNames = $this->ParseArrayValue($xpath, '/ANSWER/ParamNames');
        $ParamValues = $this->ParseArrayValue($xpath, '/ANSWER/ParamValues');
        return array('Result' => $RESULT, 'ParamNames' => $ParamNames, 'ParamValues' => $ParamValues);
    }

    public function ServiceConfigSetServiceParam($ParamName, $ParamValue)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ServiceConfigSetServiceParam');
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        $this->AddTextValue($querydoc, $root, 'ParamValue', $ParamValue);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RenderMapOnImageByPoint($SessionID, $MimeType, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                            $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnImageByPoint');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->ParseBinaryValue($xpath, '/ANSWER/BitmapImage/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        return array('Result' => $RESULT, 'BitmapImage' => $BitmapImage, 'BitmapRightUpCorner' => $BitmapRightUpCorner, 'BitmapRightDownCorner' => $BitmapRightDownCorner,
            'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner);
    }

    private function AddRecordValue($querydoc, $root, $name, $array)
    {
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        foreach ($array as $key => $value) {
            $subnode = $querydoc->createElement($key);
            $node->appendChild($subnode);
            $val_node = $querydoc->createTextNode($value);
            $subnode->appendChild($val_node);
        }
    }

    private function ParseBinaryValue($xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0) {
            echo "No elems found in $path";
            return;
        }
        return base64_decode($items->item(0)->nodeValue);
    }

    public function RenderMapOnImageByRect($SessionID, $MimeType, $LeftUpPoint, $RightBottomPoint, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                           $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnImageByRect');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddRecordValue($querydoc, $root, 'LeftUpPoint', $LeftUpPoint);
        $this->AddRecordValue($querydoc, $root, 'RightBottomPoint', $RightBottomPoint);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->ParseBinaryValue($xpath, '/ANSWER/BitmapImage/text()');
        $MiddlePoint = $this->ParseRecordValue($xpath, '/ANSWER/MiddlePoint');
        $MapAltitude = $this->ParseTextValue($xpath, '/ANSWER/MapAltitude/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        return array('Result' => $RESULT, 'BitmapImage' => $BitmapImage, 'MiddlePoint' => $MiddlePoint, 'MapAltitude' => $MapAltitude, 'BitmapRightUpCorner' => $BitmapRightUpCorner,
            'BitmapRightDownCorner' => $BitmapRightDownCorner, 'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner);
    }

    public function RenderSplitMapOnImageByPoint($SessionID, $MimeType, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                                 $BitmapWidth, $BitmapHeight, $TileWidth, $TileHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderSplitMapOnImageByPoint');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddTextValue($querydoc, $root, 'TileWidth', $TileWidth);
        $this->AddTextValue($querydoc, $root, 'TileHeight', $TileHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->ParseArrayValue($xpath, '/ANSWER/BitmapImage');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        return array('Result' => $RESULT, 'BitmapImage' => $BitmapImage, 'BitmapRightUpCorner' => $BitmapRightUpCorner, 'BitmapRightDownCorner' => $BitmapRightDownCorner,
            'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner);
    }

    public function RenderSplitMapOnImageByRect($SessionID, $MimeType, $LeftUpPoint, $RightBottomPoint, $MapRotation, $MapTilt, $MapProjection,
                                                $MapProjectionParams, $BitmapWidth, $BitmapHeight, $TileWidth, $TileHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderSplitMapOnImageByRect');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddRecordValue($querydoc, $root, 'LeftUpPoint', $LeftUpPoint);
        $this->AddRecordValue($querydoc, $root, 'RightBottomPoint', $RightBottomPoint);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddTextValue($querydoc, $root, 'TileWidth', $TileWidth);
        $this->AddTextValue($querydoc, $root, 'TileHeight', $TileHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->ParseArrayValue($xpath, '/ANSWER/BitmapImage');
        $MiddlePoint = $this->ParseRecordValue($xpath, '/ANSWER/MiddlePoint');
        $MapAltitude = $this->ParseTextValue($xpath, '/ANSWER/MapAltitude/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        return array('Result' => $RESULT, 'BitmapImage' => $BitmapImage, 'MiddlePoint' => $MiddlePoint, 'MapAltitude' => $MapAltitude, 'BitmapRightUpCorner' => $BitmapRightUpCorner,
            'BitmapRightDownCorner' => $BitmapRightDownCorner, 'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner);
    }

    public function RenderMapOnHWNDByPoint($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                           $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams, $HWND)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnHWNDByPoint');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        $this->AddTextValue($querydoc, $root, 'HWND', $HWND);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        return array('Result' => $RESULT, 'BitmapRightUpCorner' => $BitmapRightUpCorner, 'BitmapRightDownCorner' => $BitmapRightDownCorner, 'BitmapLeftUpCorner' => $BitmapLeftUpCorner,
            'BitmapLeftDownCorner' => $BitmapLeftDownCorner);
    }

    public function RenderMapOnHWNDByRect($SessionID, $LeftUpPoint, $RightBottomPoint, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                          $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams, $HWND)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnHWNDByRect');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'LeftUpPoint', $LeftUpPoint);
        $this->AddRecordValue($querydoc, $root, 'RightBottomPoint', $RightBottomPoint);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        $this->AddTextValue($querydoc, $root, 'HWND', $HWND);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MiddlePoint = $this->ParseRecordValue($xpath, '/ANSWER/MiddlePoint');
        $MapAltitude = $this->ParseTextValue($xpath, '/ANSWER/MapAltitude/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        return array('Result' => $RESULT,
            'MiddlePoint' => $MiddlePoint, 'MapAltitude' => $MapAltitude, 'BitmapRightUpCorner' => $BitmapRightUpCorner, 'BitmapRightDownCorner' => $BitmapRightDownCorner,
            'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner);
    }

    public function RenderMapOnImageByPointWithCopyright($SessionID, $MimeType, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection,
                                                         $MapProjectionParams, $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnImageByPointWithCopyright');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->ParseBinaryValue($xpath, '/ANSWER/BitmapImage/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        $Copyrights = $this->ParseArrayValue($xpath, '/ANSWER/Copyrights');
        return array('Result' => $RESULT, 'BitmapImage' => $BitmapImage, 'BitmapRightUpCorner' => $BitmapRightUpCorner, 'BitmapRightDownCorner' => $BitmapRightDownCorner,
            'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner, 'Copyrights' => $Copyrights);
    }

    public function RenderMapOnImageByRectWithCopyright($SessionID, $MimeType, $LeftUpPoint, $RightBottomPoint, $MapRotation, $MapTilt, $MapProjection,
                                                        $MapProjectionParams, $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnImageByRectWithCopyright');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddRecordValue($querydoc, $root, 'LeftUpPoint', $LeftUpPoint);
        $this->AddRecordValue($querydoc, $root, 'RightBottomPoint', $RightBottomPoint);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->ParseBinaryValue($xpath, '/ANSWER/BitmapImage/text()');
        $MiddlePoint = $this->ParseRecordValue($xpath, '/ANSWER/MiddlePoint');
        $MapAltitude = $this->ParseTextValue($xpath, '/ANSWER/MapAltitude/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        $Copyrights = $this->ParseArrayValue($xpath, '/ANSWER/Copyrights');
        return array('Result' => $RESULT, 'BitmapImage' => $BitmapImage, 'MiddlePoint' => $MiddlePoint, 'MapAltitude' => $MapAltitude, 'BitmapRightUpCorner' => $BitmapRightUpCorner,
            'BitmapRightDownCorner' => $BitmapRightDownCorner, 'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner, 'Copyrights' => $Copyrights);
    }

    public function RenderSplitMapOnImageByPointWithCopyright($SessionID, $MimeType, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection,
                                                              $MapProjectionParams, $BitmapWidth, $BitmapHeight, $TileWidth, $TileHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderSplitMapOnImageByPointWithCopyright');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddTextValue($querydoc, $root, 'TileWidth', $TileWidth);
        $this->AddTextValue($querydoc, $root, 'TileHeight', $TileHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->ParseArrayValue($xpath, '/ANSWER/BitmapImage');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        $Copyrights = $this->ParseArrayValue($xpath, '/ANSWER/Copyrights');
        return array('Result' => $RESULT, 'BitmapImage' => $BitmapImage, 'BitmapRightUpCorner' => $BitmapRightUpCorner, 'BitmapRightDownCorner' => $BitmapRightDownCorner,
            'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner, 'Copyrights' => $Copyrights);
    }

    public function RenderSplitMapOnImageByRectWithCopyright($SessionID, $MimeType, $LeftUpPoint, $RightBottomPoint, $MapRotation, $MapTilt, $MapProjection,
                                                             $MapProjectionParams, $BitmapWidth, $BitmapHeight, $TileWidth, $TileHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderSplitMapOnImageByRectWithCopyright');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddRecordValue($querydoc, $root, 'LeftUpPoint', $LeftUpPoint);
        $this->AddRecordValue($querydoc, $root, 'RightBottomPoint', $RightBottomPoint);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddTextValue($querydoc, $root, 'TileWidth', $TileWidth);
        $this->AddTextValue($querydoc, $root, 'TileHeight', $TileHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->ParseArrayValue($xpath, '/ANSWER/BitmapImage');
        $MiddlePoint = $this->ParseRecordValue($xpath, '/ANSWER/MiddlePoint');
        $MapAltitude = $this->ParseTextValue($xpath, '/ANSWER/MapAltitude/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        $Copyrights = $this->ParseArrayValue($xpath, '/ANSWER/Copyrights');
        return array('Result' => $RESULT, 'BitmapImage' => $BitmapImage, 'MiddlePoint' => $MiddlePoint, 'MapAltitude' => $MapAltitude, 'BitmapRightUpCorner' => $BitmapRightUpCorner,
            'BitmapRightDownCorner' => $BitmapRightDownCorner, 'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner, 'Copyrights' => $Copyrights);
    }

    public function RenderMapOnHWNDByPointWithCopyright($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                                        $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams, $HWND)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnHWNDByPointWithCopyright');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        $this->AddTextValue($querydoc, $root, 'HWND', $HWND);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        $Copyrights = $this->ParseArrayValue($xpath, '/ANSWER/Copyrights');
        return array('Result' => $RESULT, 'BitmapRightUpCorner' => $BitmapRightUpCorner, 'BitmapRightDownCorner' => $BitmapRightDownCorner, 'BitmapLeftUpCorner' => $BitmapLeftUpCorner,
            'BitmapLeftDownCorner' => $BitmapLeftDownCorner, 'Copyrights' => $Copyrights);
    }

    public function RenderMapOnHWNDByRectWithCopyright($SessionID, $LeftUpPoint, $RightBottomPoint, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                                       $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams, $HWND)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnHWNDByRectWithCopyright');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'LeftUpPoint', $LeftUpPoint);
        $this->AddRecordValue($querydoc, $root, 'RightBottomPoint', $RightBottomPoint);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        $this->AddTextValue($querydoc, $root, 'HWND', $HWND);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MiddlePoint = $this->ParseRecordValue($xpath, '/ANSWER/MiddlePoint');
        $MapAltitude = $this->ParseTextValue($xpath, '/ANSWER/MapAltitude/text()');
        $BitmapRightUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->ParseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        $Copyrights = $this->ParseArrayValue($xpath, '/ANSWER/Copyrights');
        return array('Result' => $RESULT,
            'MiddlePoint' => $MiddlePoint, 'MapAltitude' => $MapAltitude, 'BitmapRightUpCorner' => $BitmapRightUpCorner, 'BitmapRightDownCorner' => $BitmapRightDownCorner,
            'BitmapLeftUpCorner' => $BitmapLeftUpCorner, 'BitmapLeftDownCorner' => $BitmapLeftDownCorner, 'Copyrights' => $Copyrights);
    }

    public function ClearMapSelection($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ClearMapSelection');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RenderTiledMap($SessionID, $MimeType, $PositionX, $PositionY, $Zoom, $RenderParam, $TiledMapLayers)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderTiledMap');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddTextValue($querydoc, $root, 'PositionX', $PositionX);
        $this->AddTextValue($querydoc, $root, 'PositionY', $PositionY);
        $this->AddTextValue($querydoc, $root, 'Zoom', $Zoom);
        $this->AddRecordValue($querydoc, $root, 'RenderParam', $RenderParam);
        $this->AddArrayValue($querydoc, $root, 'TiledMapLayers', $TiledMapLayers);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImages = $this->ParseArrayValue($xpath, '/ANSWER/BitmapImages');
        $TilesX = $this->ParseTextValue($xpath, '/ANSWER/TilesX/text()');
        $TilesY = $this->ParseTextValue($xpath, '/ANSWER/TilesY/text()');
        return array('Result' => $RESULT, 'BitmapImages' => $BitmapImages, 'TilesX' => $TilesX, 'TilesY' => $TilesY);
    }

    public function RenderTiledMapWithCopyright($SessionID, $MimeType, $PositionX, $PositionY, $Zoom, $RenderParam, $TiledMapLayers)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderTiledMapWithCopyright');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddTextValue($querydoc, $root, 'PositionX', $PositionX);
        $this->AddTextValue($querydoc, $root, 'PositionY', $PositionY);
        $this->AddTextValue($querydoc, $root, 'Zoom', $Zoom);
        $this->AddRecordValue($querydoc, $root, 'RenderParam', $RenderParam);
        $this->AddArrayValue($querydoc, $root, 'TiledMapLayers', $TiledMapLayers);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImages = $this->ParseArrayValue($xpath, '/ANSWER/BitmapImages');
        $TilesX = $this->ParseTextValue($xpath, '/ANSWER/TilesX/text()');
        $TilesY = $this->ParseTextValue($xpath, '/ANSWER/TilesY/text()');
        $Copyrights = $this->ParseArrayValue($xpath, '/ANSWER/Copyrights');
        return array('Result' => $RESULT, 'BitmapImages' => $BitmapImages, 'TilesX' => $TilesX, 'TilesY' => $TilesY, 'Copyrights' => $Copyrights);
    }

    public function RenderTiledMapEx($SessionID, $MimeType, $PositionX, $PositionY, $Zoom, $RenderParam, $TiledMapLayers, $VisibleLayers)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderTiledMapEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->AddTextValue($querydoc, $root, 'PositionX', $PositionX);
        $this->AddTextValue($querydoc, $root, 'PositionY', $PositionY);
        $this->AddTextValue($querydoc, $root, 'Zoom', $Zoom);
        $this->AddRecordValue($querydoc, $root, 'RenderParam', $RenderParam);
        $this->AddArrayValue($querydoc, $root, 'TiledMapLayers', $TiledMapLayers);
        $this->AddArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImagesPresent = $this->ParseArrayValue($xpath, '/ANSWER/BitmapImagesPresent');
        $BitmapImages = $this->ParseArrayValue($xpath, '/ANSWER/BitmapImages');
        $TilesX = $this->ParseTextValue($xpath, '/ANSWER/TilesX/text()');
        $TilesY = $this->ParseTextValue($xpath, '/ANSWER/TilesY/text()');
        $Copyrights = $this->ParseArrayValue($xpath, '/ANSWER/Copyrights');
        return array('Result' => $RESULT, 'BitmapImagesPresent' => $BitmapImagesPresent, 'BitmapImages' => $BitmapImages, 'TilesX' => $TilesX, 'TilesY' => $TilesY,
            'Copyrights' => $Copyrights);
    }

    public function GetTiledMapLayers($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetTiledMapLayers');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TiledMapLayers = $this->ParseArrayValue($xpath, '/ANSWER/TiledMapLayers');
        return array('Result' => $RESULT, 'TiledMapLayers' => $TiledMapLayers);
    }

    public function ConvertMapToScreen($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $BitmapWidth,
                                       $BitmapHeight, $ImageRenderParams, $MapPoints)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ConvertMapToScreen');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'MapPoints', $MapPoints);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ScreenPoints = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ScreenPoints');
        return array('Result' => $RESULT, 'ScreenPoints' => $ScreenPoints);
    }

    private function AddArrayOfRecordsValue($querydoc, $root, $name, $array)
    {
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        foreach ($array as $key => $value)
            $this->AddRecordValue($querydoc, $node, 'ITEM', $value);
    }

    public function ConvertScreenToMap($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $BitmapWidth,
                                       $BitmapHeight, $ImageRenderParams, $ScreenPoints)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ConvertScreenToMap');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ScreenPoints', $ScreenPoints);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MapPoints = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/MapPoints');
        return array('Result' => $RESULT, 'MapPoints' => $MapPoints);
    }

    public function GetMapPointLength($SessionID, $MapPoints)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetMapPointLength');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'MapPoints', $MapPoints);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Distance = $this->ParseTextValue($xpath, '/ANSWER/Distance/text()');
        return array('Result' => $RESULT, 'Distance' => $Distance);
    }

    public function GetScreenPointLength($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $BitmapWidth,
                                         $BitmapHeight, $ImageRenderParams, $ScreenPoints)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetScreenPointLength');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->AddTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->AddRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ScreenPoints', $ScreenPoints);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Distance = $this->ParseTextValue($xpath, '/ANSWER/Distance/text()');
        return array('Result' => $RESULT, 'Distance' => $Distance);
    }

    public function InverseCalc($SessionID, $StartPoint, $StopPoint)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'InverseCalc');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'StartPoint', $StartPoint);
        $this->AddRecordValue($querydoc, $root, 'StopPoint', $StopPoint);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Length = $this->ParseTextValue($xpath, '/ANSWER/Length/text()');
        $Azimuth12 = $this->ParseTextValue($xpath, '/ANSWER/Azimuth12/text()');
        $Azimuth21 = $this->ParseTextValue($xpath, '/ANSWER/Azimuth21/text()');
        return array('Result' => $RESULT, 'Length' => $Length, 'Azimuth12' => $Azimuth12, 'Azimuth21' => $Azimuth21);
    }

    public function ForwardCalc($SessionID, $StartPoint, $Length, $Azimuth)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ForwardCalc');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'StartPoint', $StartPoint);
        $this->AddTextValue($querydoc, $root, 'Length', $Length);
        $this->AddTextValue($querydoc, $root, 'Azimuth', $Azimuth);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $StopPoint = $this->ParseRecordValue($xpath, '/ANSWER/StopPoint');
        return array('Result' => $RESULT, 'StopPoint' => $StopPoint);
    }

    public function GetDegeocodeLayers($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetDegeocodeLayers');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DegeocodeLayers = $this->ParseArrayValue($xpath, '/ANSWER/DegeocodeLayers');
        return array('Result' => $RESULT, 'DegeocodeLayers' => $DegeocodeLayers);
    }

    public function DegeocodeGetParamValue($SessionID, $ParamName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DegeocodeGetParamValue');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamValue = $this->ParseTextValue($xpath, '/ANSWER/ParamValue/text()');
        return array('Result' => $RESULT, 'ParamValue' => $ParamValue);
    }

    public function DegeocodeSetParamValue($SessionID, $ParamName, $ParamValue)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DegeocodeSetParamValue');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        $this->AddTextValue($querydoc, $root, 'ParamValue', $ParamValue);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function Degeocode($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $MapPoints,
                              $QueryRadius, $DegeocodeLayers, $OnlyNamedEntries, $UseViewVisibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'Degeocode');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'MapPoints', $MapPoints);
        $this->AddTextValue($querydoc, $root, 'QueryRadius', $QueryRadius);
        $this->AddArrayValue($querydoc, $root, 'DegeocodeLayers', $DegeocodeLayers);
        $this->AddTextValue($querydoc, $root, 'OnlyNamedEntries', $OnlyNamedEntries);
        $this->AddTextValue($querydoc, $root, 'UseViewVisibility', $UseViewVisibility);
        // Post data

        $returndoc = $this->PostQuery($querydoc);

        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Results = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Results');
        return array('Result' => $RESULT, 'Results' => $Results);
    }

    public function DegeocodeEx($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $MapPoints,
                                $QueryRadius, $DegeocodeLayers, $OnlyNamedEntries, $UseViewVisibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DegeocodeEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'MapPoints', $MapPoints);
        $this->AddTextValue($querydoc, $root, 'QueryRadius', $QueryRadius);
        $this->AddArrayValue($querydoc, $root, 'DegeocodeLayers', $DegeocodeLayers);
        $this->AddTextValue($querydoc, $root, 'OnlyNamedEntries', $OnlyNamedEntries);
        $this->AddTextValue($querydoc, $root, 'UseViewVisibility', $UseViewVisibility);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Results = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Results');
        $OtherElements = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/OtherElements');
        return array('Result' => $RESULT, 'Results' => $Results, 'OtherElements' => $OtherElements);
    }

    public function DegeocodeExWithAttributes($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                              $MapPoints, $MapPointAttributes, $QueryRadius, $DegeocodeLayers, $OnlyNamedEntries, $UseViewVisibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DegeocodeExWithAttributes');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'MapPoints', $MapPoints);
        $this->AddArrayValue($querydoc, $root, 'MapPointAttributes', $MapPointAttributes);
        $this->AddTextValue($querydoc, $root, 'QueryRadius', $QueryRadius);
        $this->AddArrayValue($querydoc, $root, 'DegeocodeLayers', $DegeocodeLayers);
        $this->AddTextValue($querydoc, $root, 'OnlyNamedEntries', $OnlyNamedEntries);
        $this->AddTextValue($querydoc, $root, 'UseViewVisibility', $UseViewVisibility);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Results = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Results');
        $OtherElements = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/OtherElements');
        return array('Result' => $RESULT, 'Results' => $Results, 'OtherElements' => $OtherElements);
    }

    public function DegeocodeAtPoint($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $DegeocodePoint,
                                     $QueryRadius, $MaxElems, $DegeocodeLayers, $OnlyNamedEntries, $UseViewVisibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DegeocodeAtPoint');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->AddTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->AddTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->AddTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->AddTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->AddRecordValue($querydoc, $root, 'DegeocodePoint', $DegeocodePoint);
        $this->AddTextValue($querydoc, $root, 'QueryRadius', $QueryRadius);
        $this->AddTextValue($querydoc, $root, 'MaxElems', $MaxElems);
        $this->AddArrayValue($querydoc, $root, 'DegeocodeLayers', $DegeocodeLayers);
        $this->AddTextValue($querydoc, $root, 'OnlyNamedEntries', $OnlyNamedEntries);
        $this->AddTextValue($querydoc, $root, 'UseViewVisibility', $UseViewVisibility);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $AreaName0 = $this->ParseTextValue($xpath, '/ANSWER/AreaName0/text()');
        $AreaName1 = $this->ParseTextValue($xpath, '/ANSWER/AreaName1/text()');
        $AreaName2 = $this->ParseTextValue($xpath, '/ANSWER/AreaName2/text()');
        $AreaName3 = $this->ParseTextValue($xpath, '/ANSWER/AreaName3/text()');
        $City = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/City');
        $Zip = $this->ParseTextValue($xpath, '/ANSWER/Zip/text()');
        $Road = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Road');
        $InternationalRoad = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/InternationalRoad');
        $Street = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Street');
        $Natural = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Natural');
        $OtherMapElements = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/OtherMapElements');
        $DatabaseElements = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/DatabaseElements');
        return array('Result' => $RESULT, 'AreaName0' => $AreaName0, 'AreaName1' => $AreaName1, 'AreaName2' => $AreaName2, 'AreaName3' => $AreaName3, 'City' => $City,
            'Zip' => $Zip, 'Road' => $Road, 'InternationalRoad' => $InternationalRoad, 'Street' => $Street, 'Natural' => $Natural, 'OtherMapElements' => $OtherMapElements,
            'DatabaseElements' => $DatabaseElements);
    }

    public function MapBaseConnect($SessionID, $DatabaseType, $ConnectionString)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'MapBaseConnect');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        $this->AddTextValue($querydoc, $root, 'ConnectionString', $ConnectionString);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $AccessLevel = $this->ParseTextValue($xpath, '/ANSWER/AccessLevel/text()');
        return array('Result' => $RESULT, 'AccessLevel' => $AccessLevel);
    }

    public function MapBaseDisconnect($SessionID, $DatabaseType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'MapBaseDisconnect');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function MapBaseLoadFromBlob($SessionID, $DatabaseType, $BinaryData)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'MapBaseLoadFromBlob');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        $this->AddBinaryValue($querydoc, $root, 'BinaryData', $BinaryData);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function MapBaseStoreToBlob($SessionID, $DatabaseType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'MapBaseStoreToBlob');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BinaryData = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryData/text()');
        return array('Result' => $RESULT, 'BinaryData' => $BinaryData);
    }

    public function GetDatabaseList($DatabaseType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetDatabaseList');
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DatabaseList = $this->ParseArrayValue($xpath, '/ANSWER/DatabaseList');
        return array('Result' => $RESULT, 'DatabaseList' => $DatabaseList);
    }

    public function GetDatabaseTableList($DatabaseType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetDatabaseTableList');
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DatabaseTableList = $this->ParseArrayValue($xpath, '/ANSWER/DatabaseTableList');
        return array('Result' => $RESULT, 'DatabaseTableList' => $DatabaseTableList);
    }

    public function GetObjectCount($SessionID, $TableName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RecordCount = $this->ParseTextValue($xpath, '/ANSWER/RecordCount/text()');
        return array('Result' => $RESULT, 'RecordCount' => $RecordCount);
    }

    public function GetObjectFields($SessionID, $TableName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectFields');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $FieldNames = $this->ParseArrayValue($xpath, '/ANSWER/FieldNames');
        $FieldTypes = $this->ParseArrayValue($xpath, '/ANSWER/FieldTypes');
        $FieldSize = $this->ParseArrayValue($xpath, '/ANSWER/FieldSize');
        $FieldPrecision = $this->ParseArrayValue($xpath, '/ANSWER/FieldPrecision');
        $FieldInfo = $this->ParseArrayValue($xpath, '/ANSWER/FieldInfo');
        $FieldFunctions = $this->ParseArrayValue($xpath, '/ANSWER/FieldFunctions');
        $GroupIDs = $this->ParseArrayValue($xpath, '/ANSWER/GroupIDs');
        $GroupNames = $this->ParseArrayValue($xpath, '/ANSWER/GroupNames');
        $GroupIcons = $this->ParseArrayValue($xpath, '/ANSWER/GroupIcons');
        $GroupIconsProperties = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/GroupIconsProperties');
        return array('Result' => $RESULT, 'FieldNames' => $FieldNames, 'FieldTypes' => $FieldTypes, 'FieldSize' => $FieldSize, 'FieldPrecision' => $FieldPrecision,
            'FieldInfo' => $FieldInfo,
            'FieldFunctions' => $FieldFunctions, 'GroupIDs' => $GroupIDs, 'GroupNames' => $GroupNames, 'GroupIcons' => $GroupIcons, 'GroupIconsProperties' => $GroupIconsProperties);
    }

    public function SetObjectFields($SessionID, $FieldNames, $FieldTypes, $FieldSize, $FieldPrecision, $FieldInfo, $FieldFunctions, $FieldNewNames)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetObjectFields');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        $this->AddArrayValue($querydoc, $root, 'FieldTypes', $FieldTypes);
        $this->AddArrayValue($querydoc, $root, 'FieldSize', $FieldSize);
        $this->AddArrayValue($querydoc, $root, 'FieldPrecision', $FieldPrecision);
        $this->AddArrayValue($querydoc, $root, 'FieldInfo', $FieldInfo);
        $this->AddArrayValue($querydoc, $root, 'FieldFunctions', $FieldFunctions);
        $this->AddArrayValue($querydoc, $root, 'FieldNewNames', $FieldNewNames);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function CreateObjectSet($SessionID, $AConnectionString, $FieldNames, $FieldTypes, $FieldSize, $FieldPrecision, $FieldInfo, $FieldFunctions,
                                    $SaveToDisk)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'CreateObjectSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AConnectionString', $AConnectionString);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        $this->AddArrayValue($querydoc, $root, 'FieldTypes', $FieldTypes);
        $this->AddArrayValue($querydoc, $root, 'FieldSize', $FieldSize);
        $this->AddArrayValue($querydoc, $root, 'FieldPrecision', $FieldPrecision);
        $this->AddArrayValue($querydoc, $root, 'FieldInfo', $FieldInfo);
        $this->AddArrayValue($querydoc, $root, 'FieldFunctions', $FieldFunctions);
        $this->AddTextValue($querydoc, $root, 'SaveToDisk', $SaveToDisk);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetObject($SessionID, $TableName, $ObjectID, $FieldNames)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObject');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $FieldValues = $this->ParseArrayValue($xpath, '/ANSWER/FieldValues');
        $ObjectPosition = $this->ParseRecordValue($xpath, '/ANSWER/ObjectPosition');
        $CaptionPosition = $this->ParseTextValue($xpath, '/ANSWER/CaptionPosition/text()');
        return array('Result' => $RESULT, 'FieldValues' => $FieldValues, 'ObjectPosition' => $ObjectPosition, 'CaptionPosition' => $CaptionPosition);
    }

    public function EditObject($SessionID, $TableName, $ObjectID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'EditObject');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function PostObject($SessionID, $TableName, $ObjectID, $GroupID, $FieldNames, $FieldValues, $ObjectPosition, $CaptionPosition)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'PostObject');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        $this->AddArrayValue($querydoc, $root, 'FieldValues', $FieldValues);
        $this->AddRecordValue($querydoc, $root, 'ObjectPosition', $ObjectPosition);
        $this->AddTextValue($querydoc, $root, 'CaptionPosition', $CaptionPosition);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function PostObjects($SessionID, $TableName, $ObjectIDs, $GroupIDs, $FieldNames, $FieldValues, $ObjectPosition, $CaptionPosition)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'PostObjects');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'ObjectIDs', $ObjectIDs);
        $this->AddArrayValue($querydoc, $root, 'GroupIDs', $GroupIDs);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        //Niezaimplementowano dla FieldValues
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ObjectPosition', $ObjectPosition);
        $this->AddArrayValue($querydoc, $root, 'CaptionPosition', $CaptionPosition);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $NotPostedIDs = $this->ParseArrayValue($xpath, '/ANSWER/NotPostedIDs');
        return array('Result' => $RESULT, 'NotPostedIDs' => $NotPostedIDs);
    }

    public function AppendObject($SessionID, $TableName, $GroupID, $FieldNames, $FieldValues, $ObjectPosition, $CaptionPosition)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AppendObject');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        $this->AddArrayValue($querydoc, $root, 'FieldValues', $FieldValues);
        $this->AddRecordValue($querydoc, $root, 'ObjectPosition', $ObjectPosition);
        $this->AddTextValue($querydoc, $root, 'CaptionPosition', $CaptionPosition);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $NewObjectID = $this->ParseTextValue($xpath, '/ANSWER/NewObjectID/text()');
        return array('Result' => $RESULT, 'NewObjectID' => $NewObjectID);
    }

    public function AppendObjects($SessionID, $TableName, $GroupIDs, $FieldNames, $FieldValues, $ObjectPosition, $CaptionPosition)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AppendObjects');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'GroupIDs', $GroupIDs);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        //Niezaimplementowano dla FieldValues
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ObjectPosition', $ObjectPosition);
        $this->AddArrayValue($querydoc, $root, 'CaptionPosition', $CaptionPosition);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $NewObjectIDs = $this->ParseArrayValue($xpath, '/ANSWER/NewObjectIDs');
        return array('Result' => $RESULT, 'NewObjectIDs' => $NewObjectIDs);
    }

    public function DeleteObject($SessionID, $TableName, $ObjectID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DeleteObject');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function DeleteObjects($SessionID, $TableName, $ObjectIDs)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DeleteObjects');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'ObjectIDs', $ObjectIDs);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $NotDeletedIDs = $this->ParseArrayValue($xpath, '/ANSWER/NotDeletedIDs');
        return array('Result' => $RESULT, 'NotDeletedIDs' => $NotDeletedIDs);
    }

    public function CancelEditObject($SessionID, $TableName, $ObjectID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'CancelEditObject');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AppendObjectPolygon($SessionID, $TableName, $ObjectID, $MapPoints, $ShapeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AppendObjectPolygon');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'MapPoints', $MapPoints);
        $this->AddTextValue($querydoc, $root, 'ShapeID', $ShapeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetObjectPolygons($SessionID, $TableName, $ObjectID, $PolygonID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectPolygons');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddTextValue($querydoc, $root, 'PolygonID', $PolygonID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        //Niezaimplementowano dla MapPoints
        return array('Result' => $RESULT, 'MapPoints' => $MapPoints);
    }

    public function AppendObjectPolyline($SessionID, $TableName, $ObjectID, $MapPoints, $ShapeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AppendObjectPolyline');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'MapPoints', $MapPoints);
        $this->AddTextValue($querydoc, $root, 'ShapeID', $ShapeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetObjectPolylines($SessionID, $TableName, $ObjectID, $PolylineID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectPolylines');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddTextValue($querydoc, $root, 'PolylineID', $PolylineID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        //Niezaimplementowano dla MapPoints
        return array('Result' => $RESULT, 'MapPoints' => $MapPoints);
    }

    public function AppendObjectEllipse($SessionID, $TableName, $ObjectID, $MapRect, $ShapeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AppendObjectEllipse');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddRecordValue($querydoc, $root, 'MapRect', $MapRect);
        $this->AddTextValue($querydoc, $root, 'ShapeID', $ShapeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetObjectEllipses($SessionID, $TableName, $ObjectID, $EllipseID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectEllipses');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddTextValue($querydoc, $root, 'EllipseID', $EllipseID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MapRects = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/MapRects');
        return array('Result' => $RESULT, 'MapRects' => $MapRects);
    }

    public function AppendObjectRectangle($SessionID, $TableName, $ObjectID, $MapRect, $ShapeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AppendObjectRectangle');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddRecordValue($querydoc, $root, 'MapRect', $MapRect);
        $this->AddTextValue($querydoc, $root, 'ShapeID', $ShapeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetObjectRectangles($SessionID, $TableName, $ObjectID, $RectangleID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectRectangles');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddTextValue($querydoc, $root, 'RectangleID', $RectangleID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MapRects = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/MapRects');
        return array('Result' => $RESULT, 'MapRects' => $MapRects);
    }

    public function AppendObjectCircle($SessionID, $TableName, $ObjectID, $MapPoint, $MapRadius, $ShapeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AppendObjectCircle');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddRecordValue($querydoc, $root, 'MapPoint', $MapPoint);
        $this->AddTextValue($querydoc, $root, 'MapRadius', $MapRadius);
        $this->AddTextValue($querydoc, $root, 'ShapeID', $ShapeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetObjectCircles($SessionID, $TableName, $ObjectID, $CircleID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectCircles');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddTextValue($querydoc, $root, 'CircleID', $CircleID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MapPointArray = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/MapPointArray');
        $MapRadiusArray = $this->ParseArrayValue($xpath, '/ANSWER/MapRadiusArray');
        return array('Result' => $RESULT, 'MapPointArray' => $MapPointArray, 'MapRadiusArray' => $MapRadiusArray);
    }

    public function DeleteObjectShape($SessionID, $TableName, $ObjectID, $ShapeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DeleteObjectShape');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddTextValue($querydoc, $root, 'ShapeID', $ShapeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetObjectBinaryData($SessionID, $TableName, $ObjectID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectBinaryData');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BinaryData = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryData/text()');
        return array('Result' => $RESULT, 'BinaryData' => $BinaryData);
    }

    public function SetObjectBinaryData($SessionID, $TableName, $ObjectID, $BinaryData)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetObjectBinaryData');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddBinaryValue($querydoc, $root, 'BinaryData', $BinaryData);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetGroupBinaryData($SessionID, $TableName, $GroupID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetGroupBinaryData');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BinaryData = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryData/text()');
        return array('Result' => $RESULT, 'BinaryData' => $BinaryData);
    }

    public function SetGroupBinaryData($SessionID, $TableName, $GroupID, $BinaryData)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetGroupBinaryData');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        $this->AddBinaryValue($querydoc, $root, 'BinaryData', $BinaryData);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetGroupSettings($SessionID, $TableName, $GroupID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetGroupSettings');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GroupSettings = $this->ParseRecordValue($xpath, '/ANSWER/GroupSettings');
        $VisibleFields = $this->ParseArrayValue($xpath, '/ANSWER/VisibleFields');
        return array('Result' => $RESULT, 'GroupSettings' => $GroupSettings, 'VisibleFields' => $VisibleFields);
    }

    public function GetGroupShapeSettings($SessionID, $TableName, $GroupID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetGroupShapeSettings');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GroupShapeSettings = $this->ParseRecordValue($xpath, '/ANSWER/GroupShapeSettings');
        return array('Result' => $RESULT, 'GroupShapeSettings' => $GroupShapeSettings);
    }

    public function SetGroupShapeSettings($SessionID, $TableName, $GroupID, $GroupShapeSettings)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetGroupShapeSettings');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        $this->AddRecordValue($querydoc, $root, 'GroupShapeSettings', $GroupShapeSettings);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function SetVisibleGroups($SessionID, $TableName, $GroupIDs)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetVisibleGroups');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'GroupIDs', $GroupIDs);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetVisibleGroups($SessionID, $TableName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetVisibleGroups');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GroupIDs = $this->ParseArrayValue($xpath, '/ANSWER/GroupIDs');
        return array('Result' => $RESULT, 'GroupIDs' => $GroupIDs);
    }

    public function EditGroup($SessionID, $TableName, $GroupID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'EditGroup');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function PostGroup($SessionID, $TableName, $GroupID, $GroupSettings, $VisibleFields)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'PostGroup');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        $this->AddRecordValue($querydoc, $root, 'GroupSettings', $GroupSettings);
        $this->AddArrayValue($querydoc, $root, 'VisibleFields', $VisibleFields);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function AppendGroup($SessionID, $TableName, $GroupSettings, $VisibleFields)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AppendGroup');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddRecordValue($querydoc, $root, 'GroupSettings', $GroupSettings);
        $this->AddArrayValue($querydoc, $root, 'VisibleFields', $VisibleFields);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $NewGroupID = $this->ParseTextValue($xpath, '/ANSWER/NewGroupID/text()');
        return array('Result' => $RESULT, 'NewGroupID' => $NewGroupID);
    }

    public function DeleteGroup($SessionID, $TableName, $GroupID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DeleteGroup');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function CancelEditGroup($SessionID, $TableName, $GroupID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'CancelEditGroup');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetGroupIcon($SessionID, $TableName, $GroupID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetGroupIcon');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'GroupID', $GroupID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GroupIcon = $this->ParseBinaryValue($xpath, '/ANSWER/GroupIcon/text()');
        $GroupIconProperties = $this->ParseRecordValue($xpath, '/ANSWER/GroupIconProperties');
        return array('Result' => $RESULT, 'GroupIcon' => $GroupIcon, 'GroupIconProperties' => $GroupIconProperties);
    }

    public function AddGroupIcon($SessionID, $DatabaseType, $GroupIcon, $GroupIconProperties)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AddGroupIcon');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        $this->AddBinaryValue($querydoc, $root, 'GroupIcon', $GroupIcon);
        $this->AddRecordValue($querydoc, $root, 'GroupIconProperties', $GroupIconProperties);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $NewIconID = $this->ParseTextValue($xpath, '/ANSWER/NewIconID/text()');
        return array('Result' => $RESULT, 'NewIconID' => $NewIconID);
    }

    public function DeleteGroupIcon($SessionID, $DatabaseType, $IconID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DeleteGroupIcon');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        $this->AddTextValue($querydoc, $root, 'IconID', $IconID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetGroupIcons($SessionID, $DatabaseType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetGroupIcons');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'DatabaseType', $DatabaseType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GroupIcons = $this->ParseArrayValue($xpath, '/ANSWER/GroupIcons');
        $GroupIconsProperties = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/GroupIconsProperties');
        return array('Result' => $RESULT, 'GroupIcons' => $GroupIcons, 'GroupIconsProperties' => $GroupIconsProperties);
    }

    public function CheckUpdate($SessionID, $TableName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'CheckUpdate');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ModifiedObjectCount = $this->ParseTextValue($xpath, '/ANSWER/ModifiedObjectCount/text()');
        $RemovedObjectCount = $this->ParseTextValue($xpath, '/ANSWER/RemovedObjectCount/text()');
        $ModifiedGroups = $this->ParseArrayValue($xpath, '/ANSWER/ModifiedGroups');
        $ModifiedIcons = $this->ParseTextValue($xpath, '/ANSWER/ModifiedIcons/text()');
        return array('Result' => $RESULT, 'ModifiedObjectCount' => $ModifiedObjectCount, 'RemovedObjectCount' => $RemovedObjectCount, 'ModifiedGroups' => $ModifiedGroups,
            'ModifiedIcons' => $ModifiedIcons);
    }

    public function GetModifiedFields($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetModifiedFields');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $FieldNames = $this->ParseArrayValue($xpath, '/ANSWER/FieldNames');
        return array('Result' => $RESULT, 'FieldNames' => $FieldNames);
    }

    public function GetRemovedFields($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetRemovedFields');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $FieldNames = $this->ParseArrayValue($xpath, '/ANSWER/FieldNames');
        return array('Result' => $RESULT, 'FieldNames' => $FieldNames);
    }

    public function FilterByText($SessionID, $TableName, $FieldNames, $FieldValues, $Conditions, $Limit)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'FilterByText');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        $this->AddArrayValue($querydoc, $root, 'FieldValues', $FieldValues);
        $this->AddArrayValue($querydoc, $root, 'Conditions', $Conditions);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function FilterByPoint($SessionID, $TableName, $MiddlePoint, $Limit)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'FilterByPoint');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function FilterByRadius($SessionID, $TableName, $MiddlePoint, $Radius, $Limit)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'FilterByRadius');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->AddTextValue($querydoc, $root, 'Radius', $Radius);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function FilterByObjectShape($SessionID, $TableName, $ObjectID, $ShapeID, $Limit)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'FilterByObjectShape');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        $this->AddTextValue($querydoc, $root, 'ShapeID', $ShapeID);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function FilterByShape($SessionID, $TableName, $MapPointArray, $Limit)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'FilterByShape');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'MapPointArray', $MapPointArray);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function GetNextResult_FieldSet($SessionID, $TableName, $ObjectIDs, $FieldName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetNextResult_FieldSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'ObjectIDs', $ObjectIDs);
        $this->AddTextValue($querydoc, $root, 'FieldName', $FieldName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $FieldValues = $this->ParseArrayValue($xpath, '/ANSWER/FieldValues');
        $NotFoundIDs = $this->ParseArrayValue($xpath, '/ANSWER/NotFoundIDs');
        return array('Result' => $RESULT, 'FieldValues' => $FieldValues, 'NotFoundIDs' => $NotFoundIDs);
    }

    public function GetNextResult_IDSet($SessionID, $TableName, $Limit, $FromBeginning)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetNextResult_IDSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        $this->AddTextValue($querydoc, $root, 'FromBeginning', $FromBeginning);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ObjectIDs = $this->ParseArrayValue($xpath, '/ANSWER/ObjectIDs');
        $GroupIDs = $this->ParseArrayValue($xpath, '/ANSWER/GroupIDs');
        return array('Result' => $RESULT, 'ObjectIDs' => $ObjectIDs, 'GroupIDs' => $GroupIDs);
    }

    public function GetNextResult_PositionSet($SessionID, $TableName, $ObjectIDs)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetNextResult_PositionSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'ObjectIDs', $ObjectIDs);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ObjectPositions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ObjectPositions');
        $CaptionPositions = $this->ParseArrayValue($xpath, '/ANSWER/CaptionPositions');
        $NotFoundIDs = $this->ParseArrayValue($xpath, '/ANSWER/NotFoundIDs');
        return array('Result' => $RESULT, 'ObjectPositions' => $ObjectPositions, 'CaptionPositions' => $CaptionPositions, 'NotFoundIDs' => $NotFoundIDs);
    }

    public function GetNextResult_Set($SessionID, $TableName, $Limit, $FieldNames, $FromBeginning)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetNextResult_Set');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        $this->AddTextValue($querydoc, $root, 'FromBeginning', $FromBeginning);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ObjectIDs = $this->ParseArrayValue($xpath, '/ANSWER/ObjectIDs');
        $GroupIDs = $this->ParseArrayValue($xpath, '/ANSWER/GroupIDs');
        $ObjectPositions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ObjectPositions');
        $CaptionPositions = $this->ParseArrayValue($xpath, '/ANSWER/CaptionPositions');
        //Niezaimplementowano dla FieldValuesArray
        return array('Result' => $RESULT, 'ObjectIDs' => $ObjectIDs, 'GroupIDs' => $GroupIDs, 'ObjectPositions' => $ObjectPositions, 'CaptionPositions' => $CaptionPositions,
            'FieldValuesArray' => $FieldValuesArray);
    }

    public function GetNextResult_UpdatedSet($SessionID, $TableName, $Limit, $FieldNames)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetNextResult_UpdatedSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ObjectIDs = $this->ParseArrayValue($xpath, '/ANSWER/ObjectIDs');
        $GroupIDs = $this->ParseArrayValue($xpath, '/ANSWER/GroupIDs');
        $ObjectPositions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ObjectPositions');
        $CaptionPositions = $this->ParseArrayValue($xpath, '/ANSWER/CaptionPositions');
        //Niezaimplementowano dla FieldValuesArray
        return array('Result' => $RESULT, 'ObjectIDs' => $ObjectIDs, 'GroupIDs' => $GroupIDs, 'ObjectPositions' => $ObjectPositions, 'CaptionPositions' => $CaptionPositions,
            'FieldValuesArray' => $FieldValuesArray);
    }

    public function GetNextResult_RemovedSet($SessionID, $TableName, $Limit)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetNextResult_RemovedSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddTextValue($querydoc, $root, 'Limit', $Limit);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ObjectIDs = $this->ParseArrayValue($xpath, '/ANSWER/ObjectIDs');
        return array('Result' => $RESULT, 'ObjectIDs' => $ObjectIDs);
    }

    public function SetObjectIDs($SessionID, $TableName, $ObjectIDs, $AllowSelected)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetObjectIDs');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'ObjectIDs', $ObjectIDs);
        $this->AddTextValue($querydoc, $root, 'AllowSelected', $AllowSelected);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetObjectAggregates($SessionID, $TableName, $FieldNames)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetObjectAggregates');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'FieldNames', $FieldNames);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $FieldSum = $this->ParseArrayValue($xpath, '/ANSWER/FieldSum');
        $FieldMin = $this->ParseArrayValue($xpath, '/ANSWER/FieldMin');
        $FieldMax = $this->ParseArrayValue($xpath, '/ANSWER/FieldMax');
        $FieldAvg = $this->ParseArrayValue($xpath, '/ANSWER/FieldAvg');
        $FieldNull = $this->ParseArrayValue($xpath, '/ANSWER/FieldNull');
        return array('Result' => $RESULT, 'FieldSum' => $FieldSum, 'FieldMin' => $FieldMin, 'FieldMax' => $FieldMax, 'FieldAvg' => $FieldAvg, 'FieldNull' => $FieldNull);
    }

    public function ClearSetFilter($SessionID, $TableName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ClearSetFilter');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function EditObjectFields($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'EditObjectFields');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function CancelEditObjectFields($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'CancelEditObjectFields');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetAggregatedObjectShapes($SessionID, $TableName, $GroupIDs, $ObjectIDs)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetAggregatedObjectShapes');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'GroupIDs', $GroupIDs);
        $this->AddArrayValue($querydoc, $root, 'ObjectIDs', $ObjectIDs);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        //Niezaimplementowano dla AggregatedIDs
        return array('Result' => $RESULT, 'AggregatedIDs' => $AggregatedIDs);
    }

    public function GetAggregatedObjects($SessionID, $TableName, $GroupIDs, $ObjectIDs)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetAggregatedObjects');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        $this->AddArrayValue($querydoc, $root, 'GroupIDs', $GroupIDs);
        $this->AddArrayValue($querydoc, $root, 'ObjectIDs', $ObjectIDs);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        //Niezaimplementowano dla AggregatedIDs
        $Aggregated = $this->ParseArrayValue($xpath, '/ANSWER/Aggregated');
        return array('Result' => $RESULT, 'AggregatedIDs' => $AggregatedIDs, 'Aggregated' => $Aggregated);
    }

    public function SearchInitialize($SessionID, $ASCIISearch)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchInitialize');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ASCIISearch', $ASCIISearch);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function SearchGetCountryList($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetCountryList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CountryNames = $this->ParseArrayValue($xpath, '/ANSWER/CountryNames');
        return array('Result' => $RESULT, 'CountryNames' => $CountryNames);
    }

    public function SearchSelectCities($SessionID, $Country, $Prefix, $ZIP, $Adm1, $Adm2, $Adm3)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSelectCities');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Country', $Country);
        $this->AddTextValue($querydoc, $root, 'Prefix', $Prefix);
        $this->AddTextValue($querydoc, $root, 'ZIP', $ZIP);
        $this->AddTextValue($querydoc, $root, 'Adm1', $Adm1);
        $this->AddTextValue($querydoc, $root, 'Adm2', $Adm2);
        $this->AddTextValue($querydoc, $root, 'Adm3', $Adm3);
        $this->AddTextValue($querydoc, $root, 'Adm3', $Adm3);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ResultCount = $this->ParseTextValue($xpath, '/ANSWER/ResultCount/text()');
        return array('Result' => $RESULT, 'ResultCount' => $ResultCount);
    }

    public function SearchGetCityList($SessionID, $First, $Count)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetCityList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'First', $First);
        $this->AddTextValue($querydoc, $root, 'Count', $Count);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CityNames = $this->ParseArrayValue($xpath, '/ANSWER/CityNames');
        //Niezaimplementowano dla CityAdmAbbrev
        //Niezaimplementowano dla CityAdmNames
        return array('Result' => $RESULT, 'CityNames' => $CityNames, 'CityAdmAbbrev' => $CityAdmAbbrev, 'CityAdmNames' => $CityAdmNames);
    }

    public function SearchGetCityListEx($SessionID, $First, $Count)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetCityListEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'First', $First);
        $this->AddTextValue($querydoc, $root, 'Count', $Count);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CityNames = $this->ParseArrayValue($xpath, '/ANSWER/CityNames');
        $CityAdmAbbrev = $this->ParseArrayValue($xpath, '/ANSWER/CityAdmAbbrev');
        $CityAdmNames = $this->ParseArrayValue($xpath, '/ANSWER/CityAdmNames');
        return array('Result' => $RESULT, 'CityNames' => $CityNames, 'CityAdmAbbrev' => $CityAdmAbbrev, 'CityAdmNames' => $CityAdmNames);
    }

    public function SearchGetItemKindList($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetItemKindList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ItemKindNames = $this->ParseArrayValue($xpath, '/ANSWER/ItemKindNames');
        return array('Result' => $RESULT, 'ItemKindNames' => $ItemKindNames);
    }

    public function SearchSetItemsFilter($SessionID, $ItemKinds)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSetItemsFilter');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayValue($querydoc, $root, 'ItemKinds', $ItemKinds);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function SearchSelectItems($SessionID, $CityIndex, $ItemKindIndex, $Prefix)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSelectItems');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'CityIndex', $CityIndex);
        $this->AddTextValue($querydoc, $root, 'ItemKindIndex', $ItemKindIndex);
        $this->AddTextValue($querydoc, $root, 'Prefix', $Prefix);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ResultCount = $this->ParseTextValue($xpath, '/ANSWER/ResultCount/text()');
        return array('Result' => $RESULT, 'ResultCount' => $ResultCount);
    }

    public function SearchSelectItemsEx($SessionID, $Country, $ItemKindIndex, $Prefix)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSelectItemsEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Country', $Country);
        $this->AddTextValue($querydoc, $root, 'ItemKindIndex', $ItemKindIndex);
        $this->AddTextValue($querydoc, $root, 'Prefix', $Prefix);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ResultCount = $this->ParseTextValue($xpath, '/ANSWER/ResultCount/text()');
        return array('Result' => $RESULT, 'ResultCount' => $ResultCount);
    }

    public function SearchGetItemsList($SessionID, $First, $Count)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetItemsList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'First', $First);
        $this->AddTextValue($querydoc, $root, 'Count', $Count);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ItemNames = $this->ParseArrayValue($xpath, '/ANSWER/ItemNames');
        $ItemTypes = $this->ParseArrayValue($xpath, '/ANSWER/ItemTypes');
        return array('Result' => $RESULT, 'ItemNames' => $ItemNames, 'ItemTypes' => $ItemTypes);
    }

    public function SearchAddCityToSelection($SessionID, $CityNum)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchAddCityToSelection');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'CityNum', $CityNum);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $MidPoint = $this->ParseRecordValue($xpath, '/ANSWER/MidPoint');
        return array('Result' => $RESULT, 'BoundingRect' => $BoundingRect, 'MidPoint' => $MidPoint);
    }

    public function SearchAddObjectToSelection($SessionID, $ObjectNum)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchAddObjectToSelection');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ObjectNum', $ObjectNum);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $MidPoint = $this->ParseRecordValue($xpath, '/ANSWER/MidPoint');
        return array('Result' => $RESULT, 'BoundingRect' => $BoundingRect, 'MidPoint' => $MidPoint);
    }

    public function SearchAddStreetWithNumToSelection($SessionID, $StreetNum, $Numeration)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchAddStreetWithNumToSelection');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'StreetNum', $StreetNum);
        $this->AddTextValue($querydoc, $root, 'Numeration', $Numeration);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MidPoint = $this->ParseRecordValue($xpath, '/ANSWER/MidPoint');
        return array('Result' => $RESULT, 'MidPoint' => $MidPoint);
    }

    public function SearchGetStreetHandle($SessionID, $StreetNum)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetStreetHandle');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'StreetNum', $StreetNum);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $StreetHandle = $this->ParseTextValue($xpath, '/ANSWER/StreetHandle/text()');
        return array('Result' => $RESULT, 'StreetHandle' => $StreetHandle);
    }

    public function SearchAddStreetCrossingToSelection($SessionID, $StreetHandleA, $StreetHandleB)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchAddStreetCrossingToSelection');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'StreetHandleA', $StreetHandleA);
        $this->AddTextValue($querydoc, $root, 'StreetHandleB', $StreetHandleB);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MidPoint = $this->ParseRecordValue($xpath, '/ANSWER/MidPoint');
        return array('Result' => $RESULT, 'MidPoint' => $MidPoint);
    }

    public function Geocode($SessionID, $ASCIISearch, $Points)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'Geocode');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ASCIISearch', $ASCIISearch);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Points', $Points);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GeocodeLevel = $this->ParseArrayValue($xpath, '/ANSWER/GeocodeLevel');
        $Positions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Positions');
        return array('Result' => $RESULT, 'GeocodeLevel' => $GeocodeLevel, 'Positions' => $Positions);
    }

    public function GeocodeEx($SessionID, $ASCIISearch, $Point, $MaxResultCount)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GeocodeEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ASCIISearch', $ASCIISearch);
        $this->AddRecordValue($querydoc, $root, 'Point', $Point);
        $this->AddTextValue($querydoc, $root, 'MaxResultCount', $MaxResultCount);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Points = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Points');
        $GeocodeLevel = $this->ParseArrayValue($xpath, '/ANSWER/GeocodeLevel');
        $Positions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Positions');
        $BoundingRects = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/BoundingRects');
        return array('Result' => $RESULT, 'Points' => $Points, 'GeocodeLevel' => $GeocodeLevel, 'Positions' => $Positions, 'BoundingRects' => $BoundingRects);
    }

    public function MultiGeocodeEx($SessionID, $ASCIISearch, $InputPoints, $MaxResultCount)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'MultiGeocodeEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ASCIISearch', $ASCIISearch);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'InputPoints', $InputPoints);
        $this->AddTextValue($querydoc, $root, 'MaxResultCount', $MaxResultCount);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $OutputPoints = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/OutputPoints');
        return array('Result' => $RESULT, 'OutputPoints' => $OutputPoints);
    }

    public function LocalizeNumFieldsGet($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeNumFieldsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Name = $this->ParseTextValue($xpath, '/ANSWER/Name/text()');
        $ShowOnInfo = $this->ParseTextValue($xpath, '/ANSWER/ShowOnInfo/text()');
        return array('Result' => $RESULT, 'Name' => $Name, 'ShowOnInfo' => $ShowOnInfo);
    }

    public function LocalizeNumFieldsSet($SessionID, $Index, $Name, $ShowOnInfo)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeNumFieldsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        $this->AddTextValue($querydoc, $root, 'Name', $Name);
        $this->AddTextValue($querydoc, $root, 'ShowOnInfo', $ShowOnInfo);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeNumFieldsAdd($SessionID, $Name, $ShowOnInfo)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeNumFieldsAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Name', $Name);
        $this->AddTextValue($querydoc, $root, 'ShowOnInfo', $ShowOnInfo);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeNumFieldsRemove($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeNumFieldsRemove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeNumFieldsClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeNumFieldsClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeNumFieldsGetCount($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeNumFieldsGetCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function LocalizeStrFieldsGet($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeStrFieldsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Name = $this->ParseTextValue($xpath, '/ANSWER/Name/text()');
        $ShowOnInfo = $this->ParseTextValue($xpath, '/ANSWER/ShowOnInfo/text()');
        return array('Result' => $RESULT, 'Name' => $Name, 'ShowOnInfo' => $ShowOnInfo);
    }

    public function LocalizeStrFieldsSet($SessionID, $Index, $Name, $ShowOnInfo)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeStrFieldsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        $this->AddTextValue($querydoc, $root, 'Name', $Name);
        $this->AddTextValue($querydoc, $root, 'ShowOnInfo', $ShowOnInfo);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeStrFieldsAdd($SessionID, $Name, $ShowOnInfo)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeStrFieldsAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Name', $Name);
        $this->AddTextValue($querydoc, $root, 'ShowOnInfo', $ShowOnInfo);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeStrFieldsRemove($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeStrFieldsRemove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeStrFieldsClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeStrFieldsClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeStrFieldsGetCount($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeStrFieldsGetCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function LocalizeIconAdd($SessionID, $IconInfo, $IconData)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeIconAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'IconInfo', $IconInfo);
        $this->AddBinaryValue($querydoc, $root, 'IconData', $IconData);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeIconGet($SessionID, $ItemIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeIconGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $IconInfo = $this->ParseRecordValue($xpath, '/ANSWER/IconInfo');
        $IconData = $this->ParseBinaryValue($xpath, '/ANSWER/IconData/text()');
        return array('Result' => $RESULT, 'IconInfo' => $IconInfo, 'IconData' => $IconData);
    }

    public function LocalizeIconSet($SessionID, $ItemIndex, $IconInfo, $IconData)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeIconSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        $this->AddRecordValue($querydoc, $root, 'IconInfo', $IconInfo);
        $this->AddBinaryValue($querydoc, $root, 'IconData', $IconData);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeIconGetCount($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeIconGetCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function LocalizeIconRemove($SessionID, $ItemIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeIconRemove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeIconClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeIconClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeIconIDToItemIndex($SessionID, $IconId)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeIconIDToItemIndex');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'IconId', $IconId);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ItemIndex = $this->ParseTextValue($xpath, '/ANSWER/ItemIndex/text()');
        return array('Result' => $RESULT, 'ItemIndex' => $ItemIndex);
    }

    public function LocalizeObjectPositionAdd($SessionID, $EntityID, $NumFields, $StrFields, $PositionPoint, $Time, $Description, $ShowDescription,
                                              $ShowName, $IconID, $ShowIcon, $IconColor, $FontParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectPositionAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        $this->AddArrayValue($querydoc, $root, 'NumFields', $NumFields);
        $this->AddArrayValue($querydoc, $root, 'StrFields', $StrFields);
        $this->AddRecordValue($querydoc, $root, 'PositionPoint', $PositionPoint);
        $this->AddTextValue($querydoc, $root, 'Time', $Time);
        $this->AddTextValue($querydoc, $root, 'Description', $Description);
        $this->AddTextValue($querydoc, $root, 'ShowDescription', $ShowDescription);
        $this->AddTextValue($querydoc, $root, 'ShowName', $ShowName);
        $this->AddTextValue($querydoc, $root, 'IconID', $IconID);
        $this->AddTextValue($querydoc, $root, 'ShowIcon', $ShowIcon);
        $this->AddTextValue($querydoc, $root, 'IconColor', $IconColor);
        $this->AddRecordValue($querydoc, $root, 'FontParams', $FontParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeObjectPositionGet($SessionID, $EntityID, $ItemIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectPositionGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $NumFields = $this->ParseArrayValue($xpath, '/ANSWER/NumFields');
        $StrFields = $this->ParseArrayValue($xpath, '/ANSWER/StrFields');
        $PositionPoint = $this->ParseRecordValue($xpath, '/ANSWER/PositionPoint');
        $Time = $this->ParseTextValue($xpath, '/ANSWER/Time/text()');
        $Description = $this->ParseTextValue($xpath, '/ANSWER/Description/text()');
        $ShowDescription = $this->ParseTextValue($xpath, '/ANSWER/ShowDescription/text()');
        $ShowName = $this->ParseTextValue($xpath, '/ANSWER/ShowName/text()');
        $IconID = $this->ParseTextValue($xpath, '/ANSWER/IconID/text()');
        $ShowIcon = $this->ParseTextValue($xpath, '/ANSWER/ShowIcon/text()');
        $IconColor = $this->ParseTextValue($xpath, '/ANSWER/IconColor/text()');
        $FontParams = $this->ParseRecordValue($xpath, '/ANSWER/FontParams');
        return array('Result' => $RESULT, 'NumFields' => $NumFields, 'StrFields' => $StrFields, 'PositionPoint' => $PositionPoint, 'Time' => $Time, 'Description' => $Description,
            'ShowDescription' => $ShowDescription, 'ShowName' => $ShowName, 'IconID' => $IconID, 'ShowIcon' => $ShowIcon, 'IconColor' => $IconColor, 'FontParams' => $FontParams);
    }

    public function LocalizeObjectPositionSet($SessionID, $EntityID, $ItemIndex, $NumFields, $StrFields, $PositionPoint, $Time, $Description, $ShowDescription,
                                              $ShowName, $IconID, $ShowIcon, $IconColor, $FontParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectPositionSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        $this->AddArrayValue($querydoc, $root, 'NumFields', $NumFields);
        $this->AddArrayValue($querydoc, $root, 'StrFields', $StrFields);
        $this->AddRecordValue($querydoc, $root, 'PositionPoint', $PositionPoint);
        $this->AddTextValue($querydoc, $root, 'Time', $Time);
        $this->AddTextValue($querydoc, $root, 'Description', $Description);
        $this->AddTextValue($querydoc, $root, 'ShowDescription', $ShowDescription);
        $this->AddTextValue($querydoc, $root, 'ShowName', $ShowName);
        $this->AddTextValue($querydoc, $root, 'IconID', $IconID);
        $this->AddTextValue($querydoc, $root, 'ShowIcon', $ShowIcon);
        $this->AddTextValue($querydoc, $root, 'IconColor', $IconColor);
        $this->AddRecordValue($querydoc, $root, 'FontParams', $FontParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeObjectPositionRemove($SessionID, $EntityID, $ItemIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectPositionRemove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeObjectPositionClear($SessionID, $EntityID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectPositionClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeObjectPositionGetCount($SessionID, $EntityID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectPositionGetCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function LocalizeObjectAdd($SessionID, $EntityID, $Name, $ShowName, $IconID, $ShowIcon, $IconColor, $PathColor, $PathWidth, $Size, $RemovePreviousPositions,
                                      $PointsConnected, $ShowDirection, $FontParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        $this->AddTextValue($querydoc, $root, 'Name', $Name);
        $this->AddTextValue($querydoc, $root, 'ShowName', $ShowName);
        $this->AddTextValue($querydoc, $root, 'IconID', $IconID);
        $this->AddTextValue($querydoc, $root, 'ShowIcon', $ShowIcon);
        $this->AddTextValue($querydoc, $root, 'IconColor', $IconColor);
        $this->AddTextValue($querydoc, $root, 'PathColor', $PathColor);
        $this->AddTextValue($querydoc, $root, 'PathWidth', $PathWidth);
        $this->AddTextValue($querydoc, $root, 'Size', $Size);
        $this->AddTextValue($querydoc, $root, 'RemovePreviousPositions', $RemovePreviousPositions);
        $this->AddTextValue($querydoc, $root, 'PointsConnected', $PointsConnected);
        $this->AddTextValue($querydoc, $root, 'ShowDirection', $ShowDirection);
        $this->AddRecordValue($querydoc, $root, 'FontParams', $FontParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeObjectGet($SessionID, $ItemIndex, $ShowDirection)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        $this->AddTextValue($querydoc, $root, 'ShowDirection', $ShowDirection);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $EntityID = $this->ParseTextValue($xpath, '/ANSWER/EntityID/text()');
        $Name = $this->ParseTextValue($xpath, '/ANSWER/Name/text()');
        $ShowName = $this->ParseTextValue($xpath, '/ANSWER/ShowName/text()');
        $IconID = $this->ParseTextValue($xpath, '/ANSWER/IconID/text()');
        $ShowIcon = $this->ParseTextValue($xpath, '/ANSWER/ShowIcon/text()');
        $IconColor = $this->ParseTextValue($xpath, '/ANSWER/IconColor/text()');
        $PathColor = $this->ParseTextValue($xpath, '/ANSWER/PathColor/text()');
        $PathWidth = $this->ParseTextValue($xpath, '/ANSWER/PathWidth/text()');
        $Size = $this->ParseTextValue($xpath, '/ANSWER/Size/text()');
        $RemovePreviousPositions = $this->ParseTextValue($xpath, '/ANSWER/RemovePreviousPositions/text()');
        $PointsConnected = $this->ParseTextValue($xpath, '/ANSWER/PointsConnected/text()');
        $FontParams = $this->ParseRecordValue($xpath, '/ANSWER/FontParams');
        return array('Result' => $RESULT, 'EntityID' => $EntityID, 'Name' => $Name, 'ShowName' => $ShowName, 'IconID' => $IconID, 'ShowIcon' => $ShowIcon, 'IconColor' => $IconColor,
            'PathColor' => $PathColor, 'PathWidth' => $PathWidth, 'Size' => $Size, 'RemovePreviousPositions' => $RemovePreviousPositions, 'PointsConnected' => $PointsConnected,
            'FontParams' => $FontParams);
    }

    public function LocalizeObjectSet($SessionID, $ItemIndex, $EntityID, $Name, $ShowName, $IconID, $ShowIcon, $IconColor, $PathColor, $PathWidth,
                                      $Size, $RemovePreviousPositions, $PointsConnected, $ShowDirection, $FontParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        $this->AddTextValue($querydoc, $root, 'Name', $Name);
        $this->AddTextValue($querydoc, $root, 'ShowName', $ShowName);
        $this->AddTextValue($querydoc, $root, 'IconID', $IconID);
        $this->AddTextValue($querydoc, $root, 'ShowIcon', $ShowIcon);
        $this->AddTextValue($querydoc, $root, 'IconColor', $IconColor);
        $this->AddTextValue($querydoc, $root, 'PathColor', $PathColor);
        $this->AddTextValue($querydoc, $root, 'PathWidth', $PathWidth);
        $this->AddTextValue($querydoc, $root, 'Size', $Size);
        $this->AddTextValue($querydoc, $root, 'RemovePreviousPositions', $RemovePreviousPositions);
        $this->AddTextValue($querydoc, $root, 'PointsConnected', $PointsConnected);
        $this->AddTextValue($querydoc, $root, 'ShowDirection', $ShowDirection);
        $this->AddRecordValue($querydoc, $root, 'FontParams', $FontParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeObjectRemove($SessionID, $ItemIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectRemove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ItemIndex', $ItemIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeObjectClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeObjectGetCount($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectGetCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function LocalizeObjectEntityIDToItemIndex($SessionID, $EntityID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectEntityIDToItemIndex');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ItemIndex = $this->ParseTextValue($xpath, '/ANSWER/ItemIndex/text()');
        return array('Result' => $RESULT, 'ItemIndex' => $ItemIndex);
    }

    public function LocalizeObjectAddWithPositions($SessionID, $EntityID, $Name, $ShowName, $IconID, $ShowIcon, $IconColor, $PathColor, $PathWidth,
                                                   $Size, $RemovePreviousPositions, $PointsConnected, $ShowDirection, $FontParams, $ObjectPositions, $StringFields, $FloatFields)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeObjectAddWithPositions');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        $this->AddTextValue($querydoc, $root, 'Name', $Name);
        $this->AddTextValue($querydoc, $root, 'ShowName', $ShowName);
        $this->AddTextValue($querydoc, $root, 'IconID', $IconID);
        $this->AddTextValue($querydoc, $root, 'ShowIcon', $ShowIcon);
        $this->AddTextValue($querydoc, $root, 'IconColor', $IconColor);
        $this->AddTextValue($querydoc, $root, 'PathColor', $PathColor);
        $this->AddTextValue($querydoc, $root, 'PathWidth', $PathWidth);
        $this->AddTextValue($querydoc, $root, 'Size', $Size);
        $this->AddTextValue($querydoc, $root, 'RemovePreviousPositions', $RemovePreviousPositions);
        $this->AddTextValue($querydoc, $root, 'PointsConnected', $PointsConnected);
        $this->AddTextValue($querydoc, $root, 'ShowDirection', $ShowDirection);
        $this->AddRecordValue($querydoc, $root, 'FontParams', $FontParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ObjectPositions', $ObjectPositions);
        //Niezaimplementowano dla StringFields
        //Niezaimplementowano dla FloatFields
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeAddObjectPositions($SessionID, $EntityID, $ObjectPositions, $StringFields, $FloatFields)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeAddObjectPositions');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntityID', $EntityID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ObjectPositions', $ObjectPositions);
        //Niezaimplementowano dla StringFields
        //Niezaimplementowano dla FloatFields
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeSetObjectVisibility($SessionID, $ObjectVisibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeSetObjectVisibility');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayValue($querydoc, $root, 'ObjectVisibility', $ObjectVisibility);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function LocalizeGetObjectVisibility($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeGetObjectVisibility');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ObjectVisibility = $this->ParseArrayValue($xpath, '/ANSWER/ObjectVisibility');
        return array('Result' => $RESULT, 'ObjectVisibility' => $ObjectVisibility);
    }

    public function GetAvailableIconFormats()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetAvailableIconFormats');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $IconFormats = $this->ParseArrayValue($xpath, '/ANSWER/IconFormats');
        return array('Result' => $RESULT, 'IconFormats' => $IconFormats);
    }

    public function RoutePlannerGetAvailableRoadTypes($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetAvailableRoadTypes');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoadTypes = $this->ParseArrayValue($xpath, '/ANSWER/RoadTypes');
        return array('Result' => $RESULT, 'RoadTypes' => $RoadTypes);
    }

    public function RoutePlannerSimpleCalculateRoute($SessionID, $RoutePlanEntries, $VehicleParams, $DriverParams, $RoadParams, $RouteCalculateType,
                                                     $CalculateRoute, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation, $ReturnRouteItinerary, $ReturnRoadResults, $StoreDriverParamsInSession,
                                                     $StoreVehicleParamsInSession, $StoreRoadParamsInSession, $StoreRoutePlanEntriesInSession, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerSimpleCalculateRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'CalculateRoute', $CalculateRoute);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreDriverParamsInSession', $StoreDriverParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreVehicleParamsInSession', $StoreVehicleParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoadParamsInSession', $StoreRoadParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlanEntriesInSession', $StoreRoutePlanEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'RouteRepresentation' => $RouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerSimpleCalculateRoute2($SessionID, $RoutePlanEntries, $VehicleParams, $DriverParams, $RoadParams, $ViaTollParams,
                                                      $RouteCalculateType, $CalculateRoute, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation, $ReturnRouteItinerary, $ReturnRoadResults,
                                                      $ReturnViaTollRoadResults, $StoreDriverParamsInSession, $StoreVehicleParamsInSession, $StoreRoadParamsInSession, $StoreRoutePlanEntriesInSession,
                                                      $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerSimpleCalculateRoute2');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ViaTollParams', $ViaTollParams);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'CalculateRoute', $CalculateRoute);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'ReturnViaTollRoadResults', $ReturnViaTollRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreDriverParamsInSession', $StoreDriverParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreVehicleParamsInSession', $StoreVehicleParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoadParamsInSession', $StoreRoadParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlanEntriesInSession', $StoreRoutePlanEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $TotalViaTollLength = $this->ParseTextValue($xpath, '/ANSWER/TotalViaTollLength/text()');
        $TotalViaTollCost = $this->ParseTextValue($xpath, '/ANSWER/TotalViaTollCost/text()');
        $TotalChargeRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TotalChargeRoadLength/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $ViaTollRoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ViaTollRoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost, 'TotalViaTollLength' => $TotalViaTollLength, 'TotalViaTollCost' => $TotalViaTollCost, 'TotalChargeRoadLength' => $TotalChargeRoadLength,
            'RoadResults' => $RoadResults,
            'ViaTollRoadResults' => $ViaTollRoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'RouteRepresentation' => $RouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerSimpleCalculateRoute3($SessionID, $RoutePlanEntries, $VehicleParams, $DriverParams, $RoadParams, $ViaTollParams,
                                                      $RoutePlannerParams,
                                                      $SubscribeProviders, $DriveThroughEntries, $RouteCalculateType, $CalculateRoute, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation,
                                                      $ReturnRouteItinerary, $ReturnRoadResults, $ReturnViaTollRoadResults, $ReturnLogisticTOLLResults, $StoreDriverParamsInSession, $StoreVehicleParamsInSession,
                                                      $StoreRoadParamsInSession,
                                                      $StoreRoutePlanEntriesInSession, $StoreRoutePlannerParamsInSession, $StoreSubscribeProvidersInSession, $StoreDriveThroughEntriesInSession,
                                                      $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerSimpleCalculateRoute3');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ViaTollParams', $ViaTollParams);
        $this->AddArrayValue($querydoc, $root, 'RoutePlannerParams', $RoutePlannerParams);
        $this->AddArrayValue($querydoc, $root, 'SubscribeProviders', $SubscribeProviders);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DriveThroughEntries', $DriveThroughEntries);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'CalculateRoute', $CalculateRoute);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'ReturnViaTollRoadResults', $ReturnViaTollRoadResults);
        $this->AddTextValue($querydoc, $root, 'ReturnLogisticTOLLResults', $ReturnLogisticTOLLResults);
        $this->AddTextValue($querydoc, $root, 'StoreDriverParamsInSession', $StoreDriverParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreVehicleParamsInSession', $StoreVehicleParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoadParamsInSession', $StoreRoadParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlanEntriesInSession', $StoreRoutePlanEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlannerParamsInSession', $StoreRoutePlannerParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreSubscribeProvidersInSession', $StoreSubscribeProvidersInSession);
        $this->AddTextValue($querydoc, $root, 'StoreDriveThroughEntriesInSession', $StoreDriveThroughEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $TotalViaTollLength = $this->ParseTextValue($xpath, '/ANSWER/TotalViaTollLength/text()');
        $TotalViaTollCost = $this->ParseTextValue($xpath, '/ANSWER/TotalViaTollCost/text()');
        $TotalChargeRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TotalChargeRoadLength/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $ViaTollRoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ViaTollRoadResults');
        $LogisticTOLLResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/LogisticTOLLResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost, 'TotalViaTollLength' => $TotalViaTollLength, 'TotalViaTollCost' => $TotalViaTollCost, 'TotalChargeRoadLength' => $TotalChargeRoadLength,
            'RoadResults' => $RoadResults, 'ViaTollRoadResults' => $ViaTollRoadResults, 'LogisticTOLLResults' => $LogisticTOLLResults, 'TollRoadLength' => $TollRoadLength,
            'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost, 'RouteRepresentation' => $RouteRepresentation, 'RouteItinerary' => $RouteItinerary,
            'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription, 'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerSimpleCalculateAlternativeRoutesAndStore($SessionID, $RoutePlanEntries, $VehicleParams, $DriverParams, $RoadParams,
                                                                         $ViaTollParams, $RoutePlannerParams, $SubscribeProviders, $DriveThroughEntries, $RouteCalculateType, $StoreFirstRouteInSession, $StoreInRepository,
                                                                         $ReplaceRepository,
                                                                         $UseIdentsArray, $IdentsArray, $UseColorArray, $ColorArray, $StoreDriverParamsInSession, $StoreVehicleParamsInSession, $StoreRoadParamsInSession,
                                                                         $StoreRoutePlanEntriesInSession, $StoreRoutePlannerParamsInSession, $StoreSubscribeProvidersInSession, $StoreDriveThroughEntriesInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerSimpleCalculateAlternativeRoutesAndStore');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ViaTollParams', $ViaTollParams);
        $this->AddArrayValue($querydoc, $root, 'RoutePlannerParams', $RoutePlannerParams);
        $this->AddArrayValue($querydoc, $root, 'SubscribeProviders', $SubscribeProviders);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DriveThroughEntries', $DriveThroughEntries);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'StoreFirstRouteInSession', $StoreFirstRouteInSession);
        $this->AddTextValue($querydoc, $root, 'StoreInRepository', $StoreInRepository);
        $this->AddTextValue($querydoc, $root, 'ReplaceRepository', $ReplaceRepository);
        $this->AddTextValue($querydoc, $root, 'UseIdentsArray', $UseIdentsArray);
        $this->AddArrayValue($querydoc, $root, 'IdentsArray', $IdentsArray);
        $this->AddTextValue($querydoc, $root, 'UseColorArray', $UseColorArray);
        $this->AddArrayValue($querydoc, $root, 'ColorArray', $ColorArray);
        $this->AddTextValue($querydoc, $root, 'StoreDriverParamsInSession', $StoreDriverParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreVehicleParamsInSession', $StoreVehicleParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoadParamsInSession', $StoreRoadParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlanEntriesInSession', $StoreRoutePlanEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlannerParamsInSession', $StoreRoutePlannerParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreSubscribeProvidersInSession', $StoreSubscribeProvidersInSession);
        $this->AddTextValue($querydoc, $root, 'StoreDriveThroughEntriesInSession', $StoreDriveThroughEntriesInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ResultCount = $this->ParseTextValue($xpath, '/ANSWER/ResultCount/text()');
        $TotalRouteLengths = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteLengths');
        $TotalRouteTimes = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteTimes');
        $TotalRouteCosts = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteCosts');
        $BoundingRect = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'ResultCount' => $ResultCount, 'TotalRouteLengths' => $TotalRouteLengths, 'TotalRouteTimes' => $TotalRouteTimes,
            'TotalRouteCosts' => $TotalRouteCosts, 'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerCalculateAlternativeRoutesAndStore($SessionID, $RouteCalculateType, $StoreFirstRouteInSession, $StoreInRepository,
                                                                   $ReplaceRepository, $UseIdentsArray, $IdentsArray, $UseColorArray, $ColorArray)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateAlternativeRoutesAndStore');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'StoreFirstRouteInSession', $StoreFirstRouteInSession);
        $this->AddTextValue($querydoc, $root, 'StoreInRepository', $StoreInRepository);
        $this->AddTextValue($querydoc, $root, 'ReplaceRepository', $ReplaceRepository);
        $this->AddTextValue($querydoc, $root, 'UseIdentsArray', $UseIdentsArray);
        $this->AddArrayValue($querydoc, $root, 'IdentsArray', $IdentsArray);
        $this->AddTextValue($querydoc, $root, 'UseColorArray', $UseColorArray);
        $this->AddArrayValue($querydoc, $root, 'ColorArray', $ColorArray);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ResultCount = $this->ParseTextValue($xpath, '/ANSWER/ResultCount/text()');
        $TotalRouteLengths = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteLengths');
        $TotalRouteTimes = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteTimes');
        $TotalRouteCosts = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteCosts');
        $BoundingRect = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'ResultCount' => $ResultCount, 'TotalRouteLengths' => $TotalRouteLengths, 'TotalRouteTimes' => $TotalRouteTimes,
            'TotalRouteCosts' => $TotalRouteCosts, 'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerSimpleCalculateRouteWithBinaryResult($SessionID, $RoutePlanEntries, $VehicleParams, $DriverParams, $RoadParams,
                                                                     $RouteCalculateType, $CalculateRoute, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation, $ReturnRouteItinerary, $ReturnRoadResults,
                                                                     $StoreDriverParamsInSession, $StoreVehicleParamsInSession, $StoreRoadParamsInSession, $StoreRoutePlanEntriesInSession, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerSimpleCalculateRouteWithBinaryResult');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'CalculateRoute', $CalculateRoute);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreDriverParamsInSession', $StoreDriverParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreVehicleParamsInSession', $StoreVehicleParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoadParamsInSession', $StoreRoadParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlanEntriesInSession', $StoreRoutePlanEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $BinaryRouteRepresentation = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryRouteRepresentation/text()');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'BinaryRouteRepresentation' => $BinaryRouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerSimpleCalculateRoute2WithBinaryResult($SessionID, $RoutePlanEntries, $VehicleParams, $DriverParams, $RoadParams,
                                                                      $ViaTollParams, $RouteCalculateType, $CalculateRoute, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation, $ReturnRouteItinerary,
                                                                      $ReturnRoadResults,
                                                                      $ReturnViaTollRoadResults, $StoreDriverParamsInSession, $StoreVehicleParamsInSession, $StoreRoadParamsInSession, $StoreRoutePlanEntriesInSession,
                                                                      $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerSimpleCalculateRoute2WithBinaryResult');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ViaTollParams', $ViaTollParams);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'CalculateRoute', $CalculateRoute);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'ReturnViaTollRoadResults', $ReturnViaTollRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreDriverParamsInSession', $StoreDriverParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreVehicleParamsInSession', $StoreVehicleParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoadParamsInSession', $StoreRoadParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlanEntriesInSession', $StoreRoutePlanEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $TotalViaTollLength = $this->ParseTextValue($xpath, '/ANSWER/TotalViaTollLength/text()');
        $TotalViaTollCost = $this->ParseTextValue($xpath, '/ANSWER/TotalViaTollCost/text()');
        $TotalChargeRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TotalChargeRoadLength/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $ViaTollRoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ViaTollRoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $BinaryRouteRepresentation = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryRouteRepresentation/text()');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost, 'TotalViaTollLength' => $TotalViaTollLength, 'TotalViaTollCost' => $TotalViaTollCost, 'TotalChargeRoadLength' => $TotalChargeRoadLength,
            'RoadResults' => $RoadResults,
            'ViaTollRoadResults' => $ViaTollRoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'BinaryRouteRepresentation' => $BinaryRouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerSimpleCalculateRoute3WithBinaryResult($SessionID, $RoutePlanEntries, $VehicleParams, $DriverParams, $RoadParams,
                                                                      $ViaTollParams, $RoutePlannerParams, $SubscribeProviders, $DriveThroughEntries, $RouteCalculateType, $CalculateRoute, $ReturnRoutePlanEntriesDescription,
                                                                      $ReturnRouteRepresentation, $ReturnRouteItinerary, $ReturnRoadResults, $ReturnViaTollRoadResults, $ReturnLogisticTOLLResults, $StoreDriverParamsInSession,
                                                                      $StoreVehicleParamsInSession, $StoreRoadParamsInSession, $StoreRoutePlanEntriesInSession, $StoreRoutePlannerParamsInSession, $StoreSubscribeProvidersInSession,
                                                                      $StoreDriveThroughEntriesInSession, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerSimpleCalculateRoute3WithBinaryResult');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ViaTollParams', $ViaTollParams);
        $this->AddArrayValue($querydoc, $root, 'RoutePlannerParams', $RoutePlannerParams);
        $this->AddArrayValue($querydoc, $root, 'SubscribeProviders', $SubscribeProviders);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DriveThroughEntries', $DriveThroughEntries);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'CalculateRoute', $CalculateRoute);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'ReturnViaTollRoadResults', $ReturnViaTollRoadResults);
        $this->AddTextValue($querydoc, $root, 'ReturnLogisticTOLLResults', $ReturnLogisticTOLLResults);
        $this->AddTextValue($querydoc, $root, 'StoreDriverParamsInSession', $StoreDriverParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreVehicleParamsInSession', $StoreVehicleParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoadParamsInSession', $StoreRoadParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlanEntriesInSession', $StoreRoutePlanEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlannerParamsInSession', $StoreRoutePlannerParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreSubscribeProvidersInSession', $StoreSubscribeProvidersInSession);
        $this->AddTextValue($querydoc, $root, 'StoreDriveThroughEntriesInSession', $StoreDriveThroughEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $TotalViaTollLength = $this->ParseTextValue($xpath, '/ANSWER/TotalViaTollLength/text()');
        $TotalViaTollCost = $this->ParseTextValue($xpath, '/ANSWER/TotalViaTollCost/text()');
        $TotalChargeRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TotalChargeRoadLength/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $ViaTollRoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ViaTollRoadResults');
        $LogisticTOLLResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/LogisticTOLLResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $BinaryRouteRepresentation = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryRouteRepresentation/text()');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost, 'TotalViaTollLength' => $TotalViaTollLength, 'TotalViaTollCost' => $TotalViaTollCost, 'TotalChargeRoadLength' => $TotalChargeRoadLength,
            'RoadResults' => $RoadResults, 'ViaTollRoadResults' => $ViaTollRoadResults, 'LogisticTOLLResults' => $LogisticTOLLResults, 'TollRoadLength' => $TollRoadLength,
            'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost, 'BinaryRouteRepresentation' => $BinaryRouteRepresentation, 'RouteItinerary' => $RouteItinerary,
            'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription, 'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerCalculateRoute($SessionID, $RouteCalculateType, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation,
                                               $ReturnRouteItinerary, $ReturnRoadResults, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'RouteRepresentation' => $RouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerCalculateRouteWithBinaryResult($SessionID, $RouteCalculateType, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation,
                                                               $ReturnRouteItinerary, $ReturnRoadResults, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRouteWithBinaryResult');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $BinaryRouteRepresentation = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryRouteRepresentation/text()');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'BinaryRouteRepresentation' => $BinaryRouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerGetRouteItinerary($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteItinerary');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        return array('Result' => $RESULT, 'RouteItinerary' => $RouteItinerary);
    }

    public function RoutePlannerGetRouteRepresentation($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteRepresentation');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        return array('Result' => $RESULT, 'RouteRepresentation' => $RouteRepresentation);
    }

    public function RoutePlannerGetRouteRepresentationWithBinaryResult($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteRepresentationWithBinaryResult');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BinaryRouteRepresentation = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryRouteRepresentation/text()');
        return array('Result' => $RESULT, 'BinaryRouteRepresentation' => $BinaryRouteRepresentation);
    }

    public function RoutePlannerGetRouteSummary($SessionID, $ReturnRoadResults)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteSummary');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $RouteCalculateType = $this->ParseTextValue($xpath, '/ANSWER/RouteCalculateType/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'BoundingRect' => $BoundingRect, 'RouteCalculateType' => $RouteCalculateType);
    }

    public function RoutePlannerGetRouteViaTollSummary($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteViaTollSummary');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ViaTollRoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ViaTollRoadResults');
        return array('Result' => $RESULT, 'ViaTollRoadResults' => $ViaTollRoadResults);
    }

    public function RoutePlannerGetRouteLogisticTOLLSummary($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteLogisticTOLLSummary');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $LogisticTOLLRoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/LogisticTOLLRoadResults');
        return array('Result' => $RESULT, 'LogisticTOLLRoadResults' => $LogisticTOLLRoadResults);
    }

    public function RoutePlannerEntriesSet($SessionID, $RoutePlanEntries, $RoutePlanVisitTimes)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddArrayValue($querydoc, $root, 'RoutePlanVisitTimes', $RoutePlanVisitTimes);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        return array('Result' => $RESULT, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription);
    }

    public function RoutePlannerEntriesSetWithDescription($SessionID, $RoutePlanEntries, $RoutePlanVisitTimes, $RoutePlanEntriesDescription)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesSetWithDescription');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddArrayValue($querydoc, $root, 'RoutePlanVisitTimes', $RoutePlanVisitTimes);
        $this->AddArrayValue($querydoc, $root, 'RoutePlanEntriesDescription', $RoutePlanEntriesDescription);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntriesGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntries = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoutePlanEntries');
        $RoutePlanVisitTimes = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanVisitTimes');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        return array('Result' => $RESULT,
            'RoutePlanEntries' => $RoutePlanEntries, 'RoutePlanVisitTimes' => $RoutePlanVisitTimes, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription);
    }

    public function RoutePlannerEntriesGetWithSnapPoints($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesGetWithSnapPoints');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntries = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoutePlanEntries');
        $RoutePlanEntriesSnapPoints = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoutePlanEntriesSnapPoints');
        $RoutePlanVisitTimes = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanVisitTimes');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        return array('Result' => $RESULT,
            'RoutePlanEntries' => $RoutePlanEntries, 'RoutePlanEntriesSnapPoints' => $RoutePlanEntriesSnapPoints, 'RoutePlanVisitTimes' => $RoutePlanVisitTimes,
            'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription);
    }

    public function RoutePlannerEntryAdd($SessionID, $RoutePlanEntry, $RoutePlanVisitTime)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'RoutePlanEntry', $RoutePlanEntry);
        $this->AddTextValue($querydoc, $root, 'RoutePlanVisitTime', $RoutePlanVisitTime);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntryDescription = $this->ParseTextValue($xpath, '/ANSWER/RoutePlanEntryDescription/text()');
        return array('Result' => $RESULT, 'RoutePlanEntryDescription' => $RoutePlanEntryDescription);
    }

    public function RoutePlannerEntryAddWithDescription($SessionID, $RoutePlanEntry, $RoutePlanVisitTime, $RoutePlanEntryDescription)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryAddWithDescription');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'RoutePlanEntry', $RoutePlanEntry);
        $this->AddTextValue($querydoc, $root, 'RoutePlanVisitTime', $RoutePlanVisitTime);
        $this->AddTextValue($querydoc, $root, 'RoutePlanEntryDescription', $RoutePlanEntryDescription);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntriesAdd($SessionID, $RoutePlanEntries, $RoutePlanVisitTimes)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddArrayValue($querydoc, $root, 'RoutePlanVisitTimes', $RoutePlanVisitTimes);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        return array('Result' => $RESULT, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription);
    }

    public function RoutePlannerEntriesAddWithDescription($SessionID, $RoutePlanEntries, $RoutePlanVisitTimes, $RoutePlanEntriesDescription)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesAddWithDescription');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddArrayValue($querydoc, $root, 'RoutePlanVisitTimes', $RoutePlanVisitTimes);
        $this->AddArrayValue($querydoc, $root, 'RoutePlanEntriesDescription', $RoutePlanEntriesDescription);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntryInsert($SessionID, $EntryIndex, $RoutePlanEntry, $RoutePlanVisitTime)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryInsert');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddRecordValue($querydoc, $root, 'RoutePlanEntry', $RoutePlanEntry);
        $this->AddTextValue($querydoc, $root, 'RoutePlanVisitTime', $RoutePlanVisitTime);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntryDescription = $this->ParseTextValue($xpath, '/ANSWER/RoutePlanEntryDescription/text()');
        return array('Result' => $RESULT, 'RoutePlanEntryDescription' => $RoutePlanEntryDescription);
    }

    public function RoutePlannerEntryInsertWithDescription($SessionID, $EntryIndex, $RoutePlanEntry, $RoutePlanVisitTime, $RoutePlanEntryDescription)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryInsertWithDescription');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddRecordValue($querydoc, $root, 'RoutePlanEntry', $RoutePlanEntry);
        $this->AddTextValue($querydoc, $root, 'RoutePlanVisitTime', $RoutePlanVisitTime);
        $this->AddTextValue($querydoc, $root, 'RoutePlanEntryDescription', $RoutePlanEntryDescription);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntryGet($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntry = $this->ParseRecordValue($xpath, '/ANSWER/RoutePlanEntry');
        $RoutePlanVisitTime = $this->ParseTextValue($xpath, '/ANSWER/RoutePlanVisitTime/text()');
        $RoutePlanEntryDescription = $this->ParseTextValue($xpath, '/ANSWER/RoutePlanEntryDescription/text()');
        return array('Result' => $RESULT, 'RoutePlanEntry' => $RoutePlanEntry, 'RoutePlanVisitTime' => $RoutePlanVisitTime, 'RoutePlanEntryDescription' => $RoutePlanEntryDescription);
    }

    public function RoutePlannerEntrySet($SessionID, $EntryIndex, $RoutePlanEntry, $RoutePlanVisitTime)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntrySet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddRecordValue($querydoc, $root, 'RoutePlanEntry', $RoutePlanEntry);
        $this->AddTextValue($querydoc, $root, 'RoutePlanVisitTime', $RoutePlanVisitTime);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntryDescription = $this->ParseTextValue($xpath, '/ANSWER/RoutePlanEntryDescription/text()');
        return array('Result' => $RESULT, 'RoutePlanEntryDescription' => $RoutePlanEntryDescription);
    }

    public function RoutePlannerEntrySetWithDescription($SessionID, $EntryIndex, $RoutePlanEntry, $RoutePlanVisitTime, $RoutePlanEntryDescription)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntrySetWithDescription');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddRecordValue($querydoc, $root, 'RoutePlanEntry', $RoutePlanEntry);
        $this->AddTextValue($querydoc, $root, 'RoutePlanVisitTime', $RoutePlanVisitTime);
        $this->AddTextValue($querydoc, $root, 'RoutePlanEntryDescription', $RoutePlanEntryDescription);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntryMove($SessionID, $OldEntryIndex, $NewEntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryMove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'OldEntryIndex', $OldEntryIndex);
        $this->AddTextValue($querydoc, $root, 'NewEntryIndex', $NewEntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntryComplexMove($SessionID, $OldEntryIndexes, $NewEntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryComplexMove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayValue($querydoc, $root, 'OldEntryIndexes', $OldEntryIndexes);
        $this->AddTextValue($querydoc, $root, 'NewEntryIndex', $NewEntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntryReverse($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryReverse');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntryRemove($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryRemove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntriesClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntriesGetCount($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesGetCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function RoutePlannerEntriesSetAsDriveThrough($SessionID, $Entries)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesSetAsDriveThrough');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Entries', $Entries);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerRouteClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRouteClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerCalculateRoutesWithSingleDepot($SessionID, $DepotEntry, $DestinationEntries, $RouteCalculateType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRoutesWithSingleDepot');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'DepotEntry', $DepotEntry);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DestinationEntries', $DestinationEntries);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLengths = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteLengths');
        $TotalRouteTimes = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteTimes');
        $TotalRouteCosts = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteCosts');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLengths' => $TotalRouteLengths, 'TotalRouteTimes' => $TotalRouteTimes, 'TotalRouteCosts' => $TotalRouteCosts,
            'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerCalculateRoutesWithSingleDepotAndStore($SessionID, $DepotEntry, $DestinationEntries, $RouteCalculateType, $StoreInRepository,
                                                                       $ReplaceRepository, $UseIdentsArray, $IdentsArray, $UseColorArray, $ColorArray)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRoutesWithSingleDepotAndStore');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'DepotEntry', $DepotEntry);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DestinationEntries', $DestinationEntries);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'StoreInRepository', $StoreInRepository);
        $this->AddTextValue($querydoc, $root, 'ReplaceRepository', $ReplaceRepository);
        $this->AddTextValue($querydoc, $root, 'UseIdentsArray', $UseIdentsArray);
        $this->AddArrayValue($querydoc, $root, 'IdentsArray', $IdentsArray);
        $this->AddTextValue($querydoc, $root, 'UseColorArray', $UseColorArray);
        $this->AddArrayValue($querydoc, $root, 'ColorArray', $ColorArray);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLengths = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteLengths');
        $TotalRouteTimes = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteTimes');
        $TotalRouteCosts = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteCosts');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLengths' => $TotalRouteLengths, 'TotalRouteTimes' => $TotalRouteTimes, 'TotalRouteCosts' => $TotalRouteCosts,
            'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerCalculateRoutesWithMultipleDepots($SessionID, $DepotEntries, $DestinationEntries, $RouteCalculateType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRoutesWithMultipleDepots');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DepotEntries', $DepotEntries);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DestinationEntries', $DestinationEntries);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DepotAssigments = $this->ParseArrayValue($xpath, '/ANSWER/DepotAssigments');
        $TotalRouteLengths = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteLengths');
        $TotalRouteTimes = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteTimes');
        $TotalRouteCosts = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteCosts');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'DepotAssigments' => $DepotAssigments, 'TotalRouteLengths' => $TotalRouteLengths, 'TotalRouteTimes' => $TotalRouteTimes,
            'TotalRouteCosts' => $TotalRouteCosts, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerCalculateRoutesWithMultipleDepotsAndStore($SessionID, $DepotEntries, $DestinationEntries, $RouteCalculateType, $StoreInRepository,
                                                                          $ReplaceRepository, $UseIdentsArray, $IdentsArray, $UseColorArray, $ColorArray)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRoutesWithMultipleDepotsAndStore');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DepotEntries', $DepotEntries);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DestinationEntries', $DestinationEntries);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'StoreInRepository', $StoreInRepository);
        $this->AddTextValue($querydoc, $root, 'ReplaceRepository', $ReplaceRepository);
        $this->AddTextValue($querydoc, $root, 'UseIdentsArray', $UseIdentsArray);
        $this->AddArrayValue($querydoc, $root, 'IdentsArray', $IdentsArray);
        $this->AddTextValue($querydoc, $root, 'UseColorArray', $UseColorArray);
        $this->AddArrayValue($querydoc, $root, 'ColorArray', $ColorArray);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DepotAssigments = $this->ParseArrayValue($xpath, '/ANSWER/DepotAssigments');
        $TotalRouteLengths = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteLengths');
        $TotalRouteTimes = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteTimes');
        $TotalRouteCosts = $this->ParseArrayValue($xpath, '/ANSWER/TotalRouteCosts');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'DepotAssigments' => $DepotAssigments, 'TotalRouteLengths' => $TotalRouteLengths, 'TotalRouteTimes' => $TotalRouteTimes,
            'TotalRouteCosts' => $TotalRouteCosts, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerCalculateRouteDistanceMatrix($SessionID, $RoutePlanEntries, $RouteCalculateType, $AssumeSymetricDistance)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRouteDistanceMatrix');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'AssumeSymetricDistance', $AssumeSymetricDistance);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        //Niezaimplementowano dla TotalRouteLengths
        //Niezaimplementowano dla TotalRouteTimes
        //Niezaimplementowano dla TotalRouteCosts
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLengths' => $TotalRouteLengths, 'TotalRouteTimes' => $TotalRouteTimes, 'TotalRouteCosts' => $TotalRouteCosts,
            'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerCalculatePointDistanceFromRoute($SessionID, $MaxLength, $Points)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculatePointDistanceFromRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MaxLength', $MaxLength);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Points', $Points);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DistanceFromRoute = $this->ParseArrayValue($xpath, '/ANSWER/DistanceFromRoute');
        return array('Result' => $RESULT, 'DistanceFromRoute' => $DistanceFromRoute);
    }

    public function RoutePlannerCalculatePointWithinRouteBuffer($SessionID, $BufferRadius, $Points)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculatePointWithinRouteBuffer');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'BufferRadius', $BufferRadius);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Points', $Points);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $PointWithinBuffer = $this->ParseArrayValue($xpath, '/ANSWER/PointWithinBuffer');
        return array('Result' => $RESULT, 'PointWithinBuffer' => $PointWithinBuffer);
    }

    public function RoutePlannerCalculateRouteFromGPSTrack($SessionID, $BinaryFile, $BinaryFileFormat, $ReturnRouteRepresentation, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRouteFromGPSTrack');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddBinaryValue($querydoc, $root, 'BinaryFile', $BinaryFile);
        $this->AddTextValue($querydoc, $root, 'BinaryFileFormat', $BinaryFileFormat);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        return array('Result' => $RESULT, 'RouteRepresentation' => $RouteRepresentation, 'BoundingRect' => $BoundingRect, 'TotalRouteLength' => $TotalRouteLength);
    }

    public function RoutePlannerVehicleParamsGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerVehicleParamsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $VehicleParams = $this->ParseRecordValue($xpath, '/ANSWER/VehicleParams');
        return array('Result' => $RESULT, 'VehicleParams' => $VehicleParams);
    }

    public function RoutePlannerVehicleParamsSet($SessionID, $VehicleParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerVehicleParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerVehicleParamsGet2($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerVehicleParamsGet2');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $VehicleEntry = $this->ParseRecordValue($xpath, '/ANSWER/VehicleEntry');
        return array('Result' => $RESULT, 'VehicleEntry' => $VehicleEntry);
    }

    public function RoutePlannerVehicleParamsSet2($SessionID, $VehicleEntry)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerVehicleParamsSet2');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'VehicleEntry', $VehicleEntry);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerVehicleParamExGet($SessionID, $ParamName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerVehicleParamExGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $PasamValue = $this->ParseTextValue($xpath, '/ANSWER/PasamValue/text()');
        return array('Result' => $RESULT, 'PasamValue' => $PasamValue);
    }

    public function RoutePlannerVehicleParamsExGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerVehicleParamsExGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamNames = $this->ParseArrayValue($xpath, '/ANSWER/ParamNames');
        $PasamValues = $this->ParseArrayValue($xpath, '/ANSWER/PasamValues');
        return array('Result' => $RESULT, 'ParamNames' => $ParamNames, 'PasamValues' => $PasamValues);
    }

    public function RoutePlannerVehicleParamExSet($SessionID, $ParamName, $PasamValue)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerVehicleParamExSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        $this->AddTextValue($querydoc, $root, 'PasamValue', $PasamValue);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerDriverParamsGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerDriverParamsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DriverParams = $this->ParseRecordValue($xpath, '/ANSWER/DriverParams');
        return array('Result' => $RESULT, 'DriverParams' => $DriverParams);
    }

    public function RoutePlannerDriverParamsSet($SessionID, $DriverParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerDriverParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerRoadParamsGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRoadParamsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoadParams = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadParams');
        return array('Result' => $RESULT, 'RoadParams' => $RoadParams);
    }

    public function RoutePlannerRoadParamsSet($SessionID, $RoadParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRoadParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerRoadViaTollParamsGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRoadViaTollParamsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ViaTollRoadParams = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ViaTollRoadParams');
        return array('Result' => $RESULT, 'ViaTollRoadParams' => $ViaTollRoadParams);
    }

    public function RoutePlannerRoadViaTollParamsSet($SessionID, $ViaTollRoadParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRoadViaTollParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ViaTollRoadParams', $ViaTollRoadParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerGetParamValue($SessionID, $ParamName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetParamValue');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamValue = $this->ParseTextValue($xpath, '/ANSWER/ParamValue/text()');
        return array('Result' => $RESULT, 'ParamValue' => $ParamValue);
    }

    public function RoutePlannerGetParamValues($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetParamValues');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamNames = $this->ParseArrayValue($xpath, '/ANSWER/ParamNames');
        $ParamValues = $this->ParseArrayValue($xpath, '/ANSWER/ParamValues');
        return array('Result' => $RESULT, 'ParamNames' => $ParamNames, 'ParamValues' => $ParamValues);
    }

    public function RoutePlannerSetParamValue($SessionID, $ParamName, $ParamValue)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerSetParamValue');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        $this->AddTextValue($querydoc, $root, 'ParamValue', $ParamValue);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerRouteLoadFromBlob($SessionID, $BinaryRepresentation)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRouteLoadFromBlob');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddBinaryValue($querydoc, $root, 'BinaryRepresentation', $BinaryRepresentation);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerRouteStoreToBlob($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRouteStoreToBlob');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BinaryRepresentation = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryRepresentation/text()');
        return array('Result' => $RESULT, 'BinaryRepresentation' => $BinaryRepresentation);
    }

    public function RoutePlannerRouteLoadFromDatabase($SessionID, $ObjectID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRouteLoadFromDatabase');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerRouteStoreToDatabase($SessionID, $ObjectID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRouteStoreToDatabase');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ObjectID', $ObjectID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryGetRoutesCount($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryGetRoutesCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }

    public function RouteRepositoryGetRoutes($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryGetRoutes');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RepositoryRoutes = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RepositoryRoutes');
        return array('Result' => $RESULT, 'RepositoryRoutes' => $RepositoryRoutes);
    }

    public function RouteRepositoryAddRoute($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryAddRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryAddRouteWithIdent($SessionID, $Ident)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryAddRouteWithIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryReplaceRoute($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryReplaceRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryReplaceRouteWithIdent($SessionID, $Ident)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryReplaceRouteWithIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryFetchRoute($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryFetchRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryFetchRouteWithIdent($SessionID, $Ident)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryFetchRouteWithIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryRemove($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryRemove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryRemoveWithIdent($SessionID, $Ident)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryRemoveWithIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositorySetRouteColor($SessionID, $Index, $Color)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositorySetRouteColor');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        $this->AddTextValue($querydoc, $root, 'Color', $Color);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositorySetRouteColorByIdent($SessionID, $Ident, $Color)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositorySetRouteColorByIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        $this->AddTextValue($querydoc, $root, 'Color', $Color);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositorySetRouteVisibility($SessionID, $Index, $Visibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositorySetRouteVisibility');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        $this->AddTextValue($querydoc, $root, 'Visibility', $Visibility);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositorySetRouteVisibilityByIdent($SessionID, $Ident, $Visibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositorySetRouteVisibilityByIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        $this->AddTextValue($querydoc, $root, 'Visibility', $Visibility);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositorySetRouteDescription($SessionID, $Index, $Description)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositorySetRouteDescription');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        $this->AddTextValue($querydoc, $root, 'Description', $Description);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositorySetRouteDescriptionByIdent($SessionID, $Ident, $Description)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositorySetRouteDescriptionByIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        $this->AddTextValue($querydoc, $root, 'Description', $Description);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryGetRouteItinerary($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryGetRouteItinerary');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        return array('Result' => $RESULT, 'RouteItinerary' => $RouteItinerary);
    }

    public function RouteRepositoryGetRouteItineraryByIdent($SessionID, $Ident)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryGetRouteItineraryByIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        return array('Result' => $RESULT, 'RouteItinerary' => $RouteItinerary);
    }

    public function RouteRepositoryGetRouteRepresentation($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryGetRouteRepresentation');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        return array('Result' => $RESULT, 'RouteRepresentation' => $RouteRepresentation);
    }

    public function RouteRepositoryGetRouteRepresentationByIdent($SessionID, $Ident)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryGetRouteRepresentationByIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        return array('Result' => $RESULT, 'RouteRepresentation' => $RouteRepresentation);
    }

    public function RouteRepositoryGetRouteSummary($SessionID, $Index, $ReturnRoadResults)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryGetRouteSummary');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $RouteCalculateType = $this->ParseTextValue($xpath, '/ANSWER/RouteCalculateType/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'BoundingRect' => $BoundingRect, 'RouteCalculateType' => $RouteCalculateType);
    }

    public function RouteRepositoryGetRouteSummaryByIdent($SessionID, $Ident, $ReturnRoadResults)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryGetRouteSummaryByIdent');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Ident', $Ident);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $RouteCalculateType = $this->ParseTextValue($xpath, '/ANSWER/RouteCalculateType/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'BoundingRect' => $BoundingRect, 'RouteCalculateType' => $RouteCalculateType);
    }

    public function RouteRepositoryLoadFromBlob($SessionID, $BinaryRepresentation)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryLoadFromBlob');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddBinaryValue($querydoc, $root, 'BinaryRepresentation', $BinaryRepresentation);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryStoreToBlob($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryStoreToBlob');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BinaryRepresentation = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryRepresentation/text()');
        return array('Result' => $RESULT, 'BinaryRepresentation' => $BinaryRepresentation);
    }

    public function RouteRepositoryCalculatePointDistanceFromRoute($SessionID, $MaxLength, $Points)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryCalculatePointDistanceFromRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'MaxLength', $MaxLength);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Points', $Points);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        //Niezaimplementowano dla DistanceFromRoutes
        $RoutesIdents = $this->ParseArrayValue($xpath, '/ANSWER/RoutesIdents');
        return array('Result' => $RESULT, 'DistanceFromRoutes' => $DistanceFromRoutes, 'RoutesIdents' => $RoutesIdents);
    }

    public function RouteAttributesClearRouteAttribute($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesClearRouteAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesAddPointAttribute($SessionID, $Position, $AttributeParams, $GenerateID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesAddPointAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'Position', $Position);
        $this->AddRecordValue($querydoc, $root, 'AttributeParams', $AttributeParams);
        $this->AddTextValue($querydoc, $root, 'GenerateID', $GenerateID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $AttributeID = $this->ParseTextValue($xpath, '/ANSWER/AttributeID/text()');
        return array('Result' => $RESULT, 'AttributeID' => $AttributeID);
    }

    public function RouteAttributesAddLineAttribute($SessionID, $Line, $AttributeParams, $GenerateID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesAddLineAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Line', $Line);
        $this->AddRecordValue($querydoc, $root, 'AttributeParams', $AttributeParams);
        $this->AddTextValue($querydoc, $root, 'GenerateID', $GenerateID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $AttributeID = $this->ParseTextValue($xpath, '/ANSWER/AttributeID/text()');
        return array('Result' => $RESULT, 'AttributeID' => $AttributeID);
    }

    public function RouteAttributesAddPolygonAttribute($SessionID, $Polygon, $AttributeParams, $GenerateID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesAddPolygonAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Polygon', $Polygon);
        $this->AddRecordValue($querydoc, $root, 'AttributeParams', $AttributeParams);
        $this->AddTextValue($querydoc, $root, 'GenerateID', $GenerateID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $AttributeID = $this->ParseTextValue($xpath, '/ANSWER/AttributeID/text()');
        return array('Result' => $RESULT, 'AttributeID' => $AttributeID);
    }

    public function RouteAttributesAddAlongRouteAttribute($SessionID, $StartPoint, $FinishPoint, $AttributeParams, $GenerateID, $ReturnRepresentation)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesAddAlongRouteAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'StartPoint', $StartPoint);
        $this->AddRecordValue($querydoc, $root, 'FinishPoint', $FinishPoint);
        $this->AddRecordValue($querydoc, $root, 'AttributeParams', $AttributeParams);
        $this->AddTextValue($querydoc, $root, 'GenerateID', $GenerateID);
        $this->AddTextValue($querydoc, $root, 'ReturnRepresentation', $ReturnRepresentation);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $AttributeID = $this->ParseTextValue($xpath, '/ANSWER/AttributeID/text()');
        $Representation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Representation');
        return array('Result' => $RESULT, 'AttributeID' => $AttributeID, 'Representation' => $Representation);
    }

    public function RouteAttributesModifyPointAttribute($SessionID, $AttributeID, $Position)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesModifyPointAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AttributeID', $AttributeID);
        $this->AddRecordValue($querydoc, $root, 'Position', $Position);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesModifyLineAttribute($SessionID, $AttributeID, $Line)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesModifyLineAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AttributeID', $AttributeID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Line', $Line);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesModifyPolygonAttribute($SessionID, $AttributeID, $Polygon)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesModifyPolygonAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AttributeID', $AttributeID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Polygon', $Polygon);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesModifyAlongRouteAttribute($SessionID, $AttributeID, $StartPoint, $FinishPoint, $ReturnRepresentation)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesModifyAlongRouteAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AttributeID', $AttributeID);
        $this->AddRecordValue($querydoc, $root, 'StartPoint', $StartPoint);
        $this->AddRecordValue($querydoc, $root, 'FinishPoint', $FinishPoint);
        $this->AddTextValue($querydoc, $root, 'ReturnRepresentation', $ReturnRepresentation);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Representation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Representation');
        return array('Result' => $RESULT, 'Representation' => $Representation);
    }

    public function RouteAttributesSetRouteAttribute($SessionID, $AttributeID, $AttributeParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesSetRouteAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AttributeID', $AttributeID);
        $this->AddRecordValue($querydoc, $root, 'AttributeParams', $AttributeParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesDeleteRouteAttribute($SessionID, $AttributeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesDeleteRouteAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AttributeID', $AttributeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesGetRouteAttribute($SessionID, $AttributeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesGetRouteAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AttributeID', $AttributeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Attribute = $this->ParseRecordValue($xpath, '/ANSWER/Attribute');
        return array('Result' => $RESULT, 'Attribute' => $Attribute);
    }

    public function RouteAttributesGetRouteAttributeWithGeometry($SessionID, $AttributeID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesGetRouteAttributeWithGeometry');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'AttributeID', $AttributeID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Attribute = $this->ParseRecordValue($xpath, '/ANSWER/Attribute');
        $GeometryKind = $this->ParseTextValue($xpath, '/ANSWER/GeometryKind/text()');
        $Geometry = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Geometry');
        return array('Result' => $RESULT, 'Attribute' => $Attribute, 'GeometryKind' => $GeometryKind, 'Geometry' => $Geometry);
    }

    public function RouteAttributesGetRouteAttributes($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesGetRouteAttributes');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $AttributesList = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/AttributesList');
        return array('Result' => $RESULT, 'AttributesList' => $AttributesList);
    }

    public function RouteAttributesLoadFromBlob($SessionID, $BinaryRepresentation)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesLoadFromBlob');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddBinaryValue($querydoc, $root, 'BinaryRepresentation', $BinaryRepresentation);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesStoreToBlob($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesStoreToBlob');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BinaryRepresentation = $this->ParseBinaryValue($xpath, '/ANSWER/BinaryRepresentation/text()');
        return array('Result' => $RESULT, 'BinaryRepresentation' => $BinaryRepresentation);
    }

    public function RouteAttributesGetAttributesProviderList($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesGetAttributesProviderList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ProvidersList = $this->ParseArrayValue($xpath, '/ANSWER/ProvidersList');
        return array('Result' => $RESULT, 'ProvidersList' => $ProvidersList);
    }

    public function RouteAttributesGetSubscribedProvidersList($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesGetSubscribedProvidersList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SubscribedProvidersList = $this->ParseArrayValue($xpath, '/ANSWER/SubscribedProvidersList');
        return array('Result' => $RESULT, 'SubscribedProvidersList' => $SubscribedProvidersList);
    }

    public function RouteAttributesSubscribeToDataProvider($SessionID, $Provider)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesSubscribeToDataProvider');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Provider', $Provider);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesUnsubscribeFromDataProvider($SessionID, $Provider)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesUnsubscribeFromDataProvider');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Provider', $Provider);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesGetDataProviderParams($SessionID, $Provider)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesGetDataProviderParams');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Provider', $Provider);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamNames = $this->ParseArrayValue($xpath, '/ANSWER/ParamNames');
        $ParamValues = $this->ParseArrayValue($xpath, '/ANSWER/ParamValues');
        return array('Result' => $RESULT, 'ParamNames' => $ParamNames, 'ParamValues' => $ParamValues);
    }

    public function RouteAttributesSetDataProviderParam($SessionID, $Provider, $ParamName, $ParamValue)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesSetDataProviderParam');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Provider', $Provider);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        $this->AddTextValue($querydoc, $root, 'ParamValue', $ParamValue);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesGetDataProviderActions($SessionID, $Provider)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesGetDataProviderActions');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Provider', $Provider);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ActionNames = $this->ParseArrayValue($xpath, '/ANSWER/ActionNames');
        return array('Result' => $RESULT, 'ActionNames' => $ActionNames);
    }

    public function RouteAttributesExecuteDataProviderAction($SessionID, $Provider, $ActionName, $ActionParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesExecuteDataProviderAction');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Provider', $Provider);
        $this->AddTextValue($querydoc, $root, 'ActionName', $ActionName);
        $this->AddArrayValue($querydoc, $root, 'ActionParams', $ActionParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesGetPassedAttributesList($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesGetPassedAttributesList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $VisitedAttributes = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/VisitedAttributes');
        $BlockedAttributes = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/BlockedAttributes');
        return array('Result' => $RESULT, 'VisitedAttributes' => $VisitedAttributes, 'BlockedAttributes' => $BlockedAttributes);
    }

    public function RouteOptimizerSimpleOptimizeRoute($SessionID, $RoutePlanEntries, $VehicleParams, $DriverParams, $RoadParams, $RouteCalculateType,
                                                      $RouteOptimizeType, $CalculateRoute, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation, $ReturnRouteItinerary, $ReturnRoadResults,
                                                      $StoreDriverParamsInSession, $StoreVehicleParamsInSession, $StoreRoadParamsInSession, $StoreRoutePlanEntriesInSession, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerSimpleOptimizeRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoutePlanEntries', $RoutePlanEntries);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'RouteOptimizeType', $RouteOptimizeType);
        $this->AddTextValue($querydoc, $root, 'CalculateRoute', $CalculateRoute);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreDriverParamsInSession', $StoreDriverParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreVehicleParamsInSession', $StoreVehicleParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoadParamsInSession', $StoreRoadParamsInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRoutePlanEntriesInSession', $StoreRoutePlanEntriesInSession);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CyclePoints = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/CyclePoints');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'CyclePoints' => $CyclePoints, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'RouteRepresentation' => $RouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RouteOptimizerOptimizeRoute($SessionID, $RouteCalculateType, $RouteOptimizeType, $CalculateRoute, $ReorderPoints, $ReturnRoutePlanEntriesDescription,
                                                $ReturnRouteRepresentation, $ReturnRouteItinerary, $ReturnRoadResults, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerOptimizeRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'RouteOptimizeType', $RouteOptimizeType);
        $this->AddTextValue($querydoc, $root, 'CalculateRoute', $CalculateRoute);
        $this->AddTextValue($querydoc, $root, 'ReorderPoints', $ReorderPoints);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CyclePoints = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/CyclePoints');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'CyclePoints' => $CyclePoints, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'RouteRepresentation' => $RouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RouteOptimizerAddShipmentType($SessionID, $ShipmentType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerAddShipmentType');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ShipmentType', $ShipmentType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerRemoveShipmentType($SessionID, $ShipmentType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerRemoveShipmentType');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ShipmentType', $ShipmentType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerShipmentTypesGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerShipmentTypesGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ShipmentTypes = $this->ParseArrayValue($xpath, '/ANSWER/ShipmentTypes');
        return array('Result' => $RESULT, 'ShipmentTypes' => $ShipmentTypes);
    }

    public function RouteOptimizerShipmentTypesSet($SessionID, $ShipmentTypes)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerShipmentTypesSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayValue($querydoc, $root, 'ShipmentTypes', $ShipmentTypes);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerShipmentTypesClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerShipmentTypesClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerSetDepotEntry($SessionID, $DepotEntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerSetDepotEntry');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'DepotEntryIndex', $DepotEntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerGetDepotEntry($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerGetDepotEntry');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DepotEntryIndex = $this->ParseTextValue($xpath, '/ANSWER/DepotEntryIndex/text()');
        return array('Result' => $RESULT, 'DepotEntryIndex' => $DepotEntryIndex);
    }

    public function RouteOptimizerGetEntryTimeWindows($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerGetEntryTimeWindows');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TimeWindows = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/TimeWindows');
        return array('Result' => $RESULT, 'TimeWindows' => $TimeWindows);
    }

    public function RouteOptimizerClearEntryTimeWindows($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerClearEntryTimeWindows');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerAddEntryTimeWindow($SessionID, $EntryIndex, $TimeWindow)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerAddEntryTimeWindow');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddRecordValue($querydoc, $root, 'TimeWindow', $TimeWindow);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerAddEntryTimeWindows($SessionID, $EntryIndex, $TimeWindows)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerAddEntryTimeWindows');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'TimeWindows', $TimeWindows);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerSetEntryTimeWindows($SessionID, $EntryIndex, $TimeWindows)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerSetEntryTimeWindows');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'TimeWindows', $TimeWindows);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerGetEntryDeliveryShipments($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerGetEntryDeliveryShipments');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DeliveryShipments = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/DeliveryShipments');
        return array('Result' => $RESULT, 'DeliveryShipments' => $DeliveryShipments);
    }

    public function RouteOptimizerAddEntryDeliveryShipment($SessionID, $EntryIndex, $DeliveryShipment)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerAddEntryDeliveryShipment');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddRecordValue($querydoc, $root, 'DeliveryShipment', $DeliveryShipment);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerAddEntryDeliveryShipments($SessionID, $EntryIndex, $DeliveryShipments)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerAddEntryDeliveryShipments');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DeliveryShipments', $DeliveryShipments);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerClearEntryDeliveryShipments($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerClearEntryDeliveryShipments');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerSetEntryDeliveryShipments($SessionID, $EntryIndex, $DeliveryShipments)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerSetEntryDeliveryShipments');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'DeliveryShipments', $DeliveryShipments);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerGetEntryPickupShipments($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerGetEntryPickupShipments');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $PickupShipments = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/PickupShipments');
        return array('Result' => $RESULT, 'PickupShipments' => $PickupShipments);
    }

    public function RouteOptimizerAddEntryPickupShipment($SessionID, $EntryIndex, $PickupShipment)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerAddEntryPickupShipment');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddRecordValue($querydoc, $root, 'PickupShipment', $PickupShipment);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerAddEntryPickupShipments($SessionID, $EntryIndex, $PickupShipments)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerAddEntryPickupShipments');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'PickupShipments', $PickupShipments);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerClearEntryPickupShipments($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerClearEntryPickupShipments');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerSetEntryPickupShipments($SessionID, $EntryIndex, $PickupShipments)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerSetEntryPickupShipments');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'PickupShipments', $PickupShipments);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerGetEntryShipmentDeliveryPickupTimes($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerGetEntryShipmentDeliveryPickupTimes');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DeliveryTime = $this->ParseTextValue($xpath, '/ANSWER/DeliveryTime/text()');
        $PickupTime = $this->ParseTextValue($xpath, '/ANSWER/PickupTime/text()');
        return array('Result' => $RESULT, 'DeliveryTime' => $DeliveryTime, 'PickupTime' => $PickupTime);
    }

    public function RouteOptimizerSetEntryShipmentDeliveryPickupTimes($SessionID, $EntryIndex, $DeliveryTime, $PickupTime)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerSetEntryShipmentDeliveryPickupTimes');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        $this->AddTextValue($querydoc, $root, 'DeliveryTime', $DeliveryTime);
        $this->AddTextValue($querydoc, $root, 'PickupTime', $PickupTime);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerVehicleCategoryToShipmentTypeIncompatibilityClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerVehicleCategoryToShipmentTypeIncompatibilityClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerVehicleCategoryToShipmentTypeIncompatibilityAdd($SessionID, $VehicleCategory, $ShipmentType)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerVehicleCategoryToShipmentTypeIncompatibilityAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleCategory', $VehicleCategory);
        $this->AddTextValue($querydoc, $root, 'ShipmentType', $ShipmentType);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerVehicleCategoryToShipmentTypeIncompatibilityGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerVehicleCategoryToShipmentTypeIncompatibilityGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $VehicleCategoryToShipmentTypeIncompatibility = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/VehicleCategoryToShipmentTypeIncompatibility');
        return array('Result' => $RESULT, 'VehicleCategoryToShipmentTypeIncompatibility' => $VehicleCategoryToShipmentTypeIncompatibility);
    }

    public function RouteOptimizerVehicleCategoryToShipmentTypeIncompatibilitySet($SessionID, $VehicleCategoryToShipmentTypeIncompatibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerVehicleCategoryToShipmentTypeIncompatibilitySet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'VehicleCategoryToShipmentTypeIncompatibility', $VehicleCategoryToShipmentTypeIncompatibility);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerShipmentTypeToShipmentTypeIncompatibilityClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerShipmentTypeToShipmentTypeIncompatibilityClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerShipmentTypeToShipmentTypeIncompatibilityAdd($SessionID, $ShipmentType1, $ShipmentType2)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerShipmentTypeToShipmentTypeIncompatibilityAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ShipmentType1', $ShipmentType1);
        $this->AddTextValue($querydoc, $root, 'ShipmentType2', $ShipmentType2);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerShipmentTypeToShipmentTypeIncompatibilityGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerShipmentTypeToShipmentTypeIncompatibilityGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ShipmentTypeToShipmentTypeIncompatibility = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ShipmentTypeToShipmentTypeIncompatibility');
        return array('Result' => $RESULT, 'ShipmentTypeToShipmentTypeIncompatibility' => $ShipmentTypeToShipmentTypeIncompatibility);
    }

    public function RouteOptimizerShipmentTypeToShipmentTypeIncompatibilitySet($SessionID, $ShipmentTypeToShipmentTypeIncompatibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerShipmentTypeToShipmentTypeIncompatibilitySet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ShipmentTypeToShipmentTypeIncompatibility', $ShipmentTypeToShipmentTypeIncompatibility);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteOptimizerSolveProblem($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteOptimizerSolveProblem');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SolutionCost = $this->ParseTextValue($xpath, '/ANSWER/SolutionCost/text()');
        $Routes = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Routes');
        $ShipmentMoves = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ShipmentMoves');
        $Entries = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Entries');
        $ShipmentDeliveries = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ShipmentDeliveries');
        $ShipmentPickups = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ShipmentPickups');
        return array('Result' => $RESULT,
            'SolutionCost' => $SolutionCost, 'Routes' => $Routes, 'ShipmentMoves' => $ShipmentMoves, 'Entries' => $Entries, 'ShipmentDeliveries' => $ShipmentDeliveries,
            'ShipmentPickups' => $ShipmentPickups);
    }

    public function VehicleRepositoryAddVehicle($SessionID, $Vehicle)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryAddVehicle');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'Vehicle', $Vehicle);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryRemoveVehicle($SessionID, $VehicleIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryRemoveVehicle');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryVehicleGet($SessionID, $VehicleIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehicleGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Vehicle = $this->ParseRecordValue($xpath, '/ANSWER/Vehicle');
        return array('Result' => $RESULT, 'Vehicle' => $Vehicle);
    }

    public function VehicleRepositoryVehicleSet($SessionID, $VehicleIndex, $Vehicle)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehicleSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        $this->AddRecordValue($querydoc, $root, 'Vehicle', $Vehicle);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryRoadParamsGet($SessionID, $VehicleIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryRoadParamsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoadParams = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadParams');
        return array('Result' => $RESULT, 'RoadParams' => $RoadParams);
    }

    public function VehicleRepositoryRoadParamsSet($SessionID, $VehicleIndex, $RoadParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryRoadParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryVehicleViaTollGet($SessionID, $VehicleIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehicleViaTollGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ViaTollParams = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/ViaTollParams');
        return array('Result' => $RESULT, 'ViaTollParams' => $ViaTollParams);
    }

    public function VehicleRepositoryVehicleViaTollSet($SessionID, $VehicleIndex, $ViaTollParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehicleViaTollSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'ViaTollParams', $ViaTollParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryDriverParamsGet($SessionID, $VehicleIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryDriverParamsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DriverParams = $this->ParseRecordValue($xpath, '/ANSWER/DriverParams');
        return array('Result' => $RESULT, 'DriverParams' => $DriverParams);
    }

    public function VehicleRepositoryDriverParamsSet($SessionID, $VehicleIndex, $DriverParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryDriverParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryVehicleFetch($SessionID, $VehicleIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehicleFetch');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryVehicleReplace($SessionID, $VehicleIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehicleReplace');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'VehicleIndex', $VehicleIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryVehiclesGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehiclesGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Vehicles = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Vehicles');
        return array('Result' => $RESULT, 'Vehicles' => $Vehicles);
    }

    public function VehicleRepositoryVehiclesSet($SessionID, $Vehicles)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehiclesSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Vehicles', $Vehicles);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function VehicleRepositoryVehiclesClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'VehicleRepositoryVehiclesClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function SearchGetParamValue($SessionID, $ParamName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetParamValue');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamValue = $this->ParseTextValue($xpath, '/ANSWER/ParamValue/text()');
        return array('Result' => $RESULT, 'ParamValue' => $ParamValue);
    }

    public function SearchGetParamValues($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetParamValues');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamNames = $this->ParseArrayValue($xpath, '/ANSWER/ParamNames');
        $ParamValues = $this->ParseArrayValue($xpath, '/ANSWER/ParamValues');
        return array('Result' => $RESULT, 'ParamNames' => $ParamNames, 'ParamValues' => $ParamValues);
    }

    public function SearchSetParamValue($SessionID, $ParamName, $ParamValue)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSetParamValue');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        $this->AddTextValue($querydoc, $root, 'ParamValue', $ParamValue);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

}

?>
