/**
 * @class MapMonitoringModule
 */
var MapMonitoringModule = (function(options) {

    var REFRESH_INTERVAL = 10000;

    var _opts = Object.assign({}, getDefaultOptions(), options),
        _map,
        atlasMapProperties = getMapProperties(),
        _routes = {},
        currentStatus,
        eventRoute,
        lastCoordinateId,
        lastCoordinateSaveAt,
        intervalRefresh,
        eventLocation,
        isNear,
        lineLayer = null,
        towingMarker,
        serviceLocation,
        caseInfo = {};

    function getColorStaticWay(status) {

        status = (typeof status === "undefined") ? currentStatus : status;

        if(status === "4") {
            return '#252832';
        }
        else if(status === "6") {
            return 'green';
        }
        else {
            return '#252832';
        }

    }

    function getColorDynamicWay() {
        return 'red';
    }

    function getDefaultOptions() {

        return {
            map_options: {
                mapContainer: '#map-handler',
                centerCoordinates: [16.902084, 52.407854], // Lat , Lng
                zoomMap: 12
            },
            enableRoute: 0
        }

    }

    function getMapProperties() {

        return {
            opts: _opts.map_options,
            provider: new ProviderEmapa('6vihz1Nbw5QC5rQ5')
        };

    }

    function _initMap() {

        var promise = jQuery.Deferred();

        AtlasMapModule.init(atlasMapProperties, function (map) {

            if (!map) {
                console.error('Error loading map');
                promise.reject('Error loading map');
            }

            _map = map;
            // _map._P.router.addEventListener('afterCalculateRoute', _afterCalculateRoute, false);

            promise.resolve(map);

        });

        return promise;

    }

    function addNewRoute(options, wayPoints) {

        wayPoints = (typeof wayPoints === "undefined") ? [] : wayPoints;

        var promise = jQuery.Deferred(),
            uuid = guid();

        var route = new EMAPI.Router( _map._map, {
            preserveViewport: true,
            routeLineWidth: 6
        });

        // for(var i = 0; i < 7; i++) {
        //     console.log(i);
        //     route._vehicles.removeItem(0);
        // }

        // console.log(route._vehicles.getObject());

        route['uuid'] = uuid;

        _routes[route.uuid] = {
            r: route,
            o: options
        };

        route.addEventListener('afterCalculateRoute', function (e) {

            _afterCalculateRoute(e);

            if(promise.state() !== "resolved") {
                promise.resolve(route);
            }

        }, false);

        wayPoints.forEach(function (ele) {
            route.addWaypoint( new EMAPI.LonLat(ele.lon, ele.lat), ele.type);
        });

        return promise;

    }

    function updateRoute(route, wayPoints) {

        route.removeWaypoints();

        wayPoints.forEach(function (ele) {
            route.addWaypoint( new EMAPI.LonLat(ele.lon, ele.lat), ele.type);
        });

    }

    function _afterCalculateRoute(e) {

        var thisRoute = _routes[e.target.uuid],
            _layers = e.target._wayPoints._layer._layers;

        $.each(_layers, function (i, ele) {

            if(ele.hasOwnProperty('_icon')) {

            }
            else {
                ele._leaflet_events = null;
                ele.options.opacity = (typeof thisRoute.o.opacity !== "undefined") ? thisRoute.o.opacity : 1;

                if(thisRoute.o.color) {
                    ele.options.color = thisRoute.o.color;
                }
                ele._initStyle();
            }

        });

        if(typeof thisRoute.o.onCalculation === "function") {
            thisRoute.o.onCalculation(thisRoute.r.routeRepresentation());
        }

        // Wykasowanie eventu na DRAG
        setTimeout(function () {
            $.each(_layers, function (i, ele) {
                if(ele.hasOwnProperty('_icon')) {
                    ele.dragging.disable();
                }
            });
        }, 0);

    }

    function _addServiceLocation(lon, lat) {
        return addMarker('service_location', Translator.trans('ics_map.service_location'), lon, lat, 'repair'); // @desc("Lokalizacja Warsztatu")
    }

    function _addEventLocation(lon, lat) {
        return addMarker('event_location', Translator.trans('ics_map.event_location'), lon, lat, 'caution'); // @desc("Miejsce zdarzenia")
    }

    function _addStartLocation(lon, lat) {
        return addMarker('start_location', Translator.trans('ics_map.start_location'), lon, lat, 'point'); // @desc("Lokalizacja skąd wyruszyła pomoc")
    }

    function _addTowingCar(lon, lat) {
        return addMarker('towing_location', Translator.trans('ics_map.towing_car_location'), lon, lat, 'carrepair'); // @desc("Lokalizacja pomocy drogowej")
    }

    function _updateTowingCar(lon, lat, direction) {

        towingMarker.object.setPosition(new EMAPI.LonLat(lon, lat));
        towingMarker.object.redraw();

        if(typeof direction !== "undefined") {

            _rotateMarker(towingMarker, direction);

        }

    }

    function _rotateMarker(marker, direction) {
        marker.object._feature._icon.style.transform = marker.object._feature._icon.style.transform + " rotate(" + direction + "deg)"
    }

    function addMarker(name, label, lon, lat, icon) {
       return _map.addMarker(name, label, [lat, lon], label, '', icon);
    }


    // -------------------------------------------------------
    //                      PUBLIC
    // -------------------------------------------------------

    /**
     * @param {Object} response
     * @param {Object} response.eventLocation
     * @param {Object} response.order
     * @param {Object} response.towingLocation
     * @param {Object} response.startLocation
     * @param {string} response.order.current_status
     * @param {int} response.lastCoordinateId
     * @param {string} response.lastSaveAt
     * @param {string} response.caseNumber
     * @param {int} response.isNear
     * @param {Array} response.lines
     * @param {Array} response.staticWays
     * @param {Object} response.serviceLocation
     * @private
     */
    function _handleRequestLoadMap(response) {

        currentStatus = response.order.current_status;
        lastCoordinateId = response.lastCoordinateId;
        lastCoordinateSaveAt = response.lastSaveAt;
        isNear = response.isNear;

        eventLocation = {
            lon: response.eventLocation.lon,
            lat: response.eventLocation.lat
        };

        if(response.eventLocation) {

            _addEventLocation(response.eventLocation.lon, response.eventLocation.lat);

            if(response.towingLocation) {
                towingMarker = _addTowingCar(response.towingLocation.lon, response.towingLocation.lat);
                // _rotateMarker(towingMarker, response.direction);
                _map._map.setCenterPos(new EMAPI.LonLat(response.towingLocation.lon, response.towingLocation.lat));
            }
            else {
                _map._map.setCenterPos(new EMAPI.LonLat(response.eventLocation.lon, response.eventLocation.lat));
            }

        }

        if(response.startLocation) {
            mapMonitoringModule.addStartLocation(response.startLocation.lon, response.startLocation.lat);
        }
        else {

            if(["4","5","7"].indexOf(currentStatus) !== -1) {

                /** 4 === Kierowca jeszcze nie wyruszył */

                $('.swal-overlay.swal-overlay--show-modal').removeClass('init-hidden');

            }

        }

        if(["5","7","9"].indexOf(currentStatus) !== -1) {

            $('.swal-overlay.swal-overlay--show-modal').removeClass('init-hidden');

            var msg = "";

            if(currentStatus === "5") {

                msg = Translator.trans('ics_map.help_probably_in_place'); // @desc("Pomoc drogowa powinna być już na miejscu.")

            } else if(currentStatus === "7" || currentStatus === "9") {

                msg = Translator.trans('ics_map.service_finished'); // @desc("Usługa została zakończona.")

            }

            swal({
                title: "",
                text: msg,
                icon: "success"
            });

        }

        // dumpCoordinates(response.coordinates);

        if(response.serviceLocation) {
            serviceLocation = response.serviceLocation;
        }

        if(_opts.enableRoute) {
            if(response.lines) {

                $.each(response.lines, function (i, ele) {
                    addLines(ele.array, ele.status);
                })

            }

            if(response.staticWays) {

                $.each(response.staticWays, function (i, ele) {
                    addStaticWays(ele.array, ele.status);
                });

            }
        }

        if(currentStatus === "4") {

            if(response.towingLocation && response.eventLocation && !isNear) {
                addWayToEventLocation({
                    lon: response.towingLocation.lon,
                    lat: response.towingLocation.lat,
                    type: 0
                }, {
                    lon: response.eventLocation.lon,
                    lat: response.eventLocation.lat,
                    type: 2
                });
            }

        }
        else if(currentStatus === "6") {

            if(response.serviceLocation && response.serviceLocation.lon && response.towingLocation && response.towingLocation.lon) {

                _addServiceLocation(response.serviceLocation.lon, response.serviceLocation.lat);

                addWayToEventLocation({
                    lon: response.towingLocation.lon,
                    lat: response.towingLocation.lat,
                    type: 0
                }, {
                    lon: response.serviceLocation.lon,
                    lat: response.serviceLocation.lat,
                    type: 2
                });
            }

        }
        else if(currentStatus === "7" && response.serviceLocation && response.serviceLocation.lon ) {
            _addServiceLocation(response.serviceLocation.lon, response.serviceLocation.lat);
        }

        if(currentStatus !== "7") {
            _startIntervalRefresh();
        }

        if(response.orderDetails) {
            setCaseInfo(response.orderDetails);
        }

        if(_opts._debugger) {
            _debugger.setData(response);
        }

    }

    function setCaseInfo(values) {

        $.each(values, function (i, ele) {
            setValueOfCase(ele.name, ele.value, ele.label);
        });

        updateCaseInfo();

    }

    function setValueOfCase(name, value, label) {

        if(caseInfo.hasOwnProperty(name)) {
            caseInfo[name].value = value;
            if(typeof label !== "undefined") {
                caseInfo[name].label = label;
            }
        }
        else {
            caseInfo[name] = {
                value: value,
                label: label
            }
        }

    }

    function updateCaseInfo() {

        var $tbody = $('.info-map-wrapper table tbody');

        $tbody.children().remove();

        $.each(caseInfo, function (i, ele) {

            $tbody.append(
                '<tr><td>' + ele.label + '</td><td>' + ele.value + '</td></tr>'
            )

        })

    }

    /**
     * @param {Object} response
     * @param {Object} response.eventLocation
     * @param {Object} response.order
     * @param {Object} response.towingLocation
     * @param {Object} response.startLocation
     * @param {string} response.order.current_status
     * @param {int} response.lastCoordinateId
     * @param {string} response.lastSaveAt
     * @param {int} response.isNear
     * @param {Array} response.lines
     * @param {Array} response.staticWays
     * @private
     */

    var _onSuccessRefresh = function(response) {

        if(_opts._debugger) {

        }

        if(response.order) {
            setValueOfCase('status', response.order.status_name);
            updateCaseInfo();
        }

        /**
         * Nastąpiła zmiana statusu zlecenia
         */
        if(response.order.current_status !== currentStatus) {

            if(currentStatus === "4") {

                if (response.order.current_status === "5") {

                    $('.swal-overlay.swal-overlay--show-modal').removeClass('init-hidden');

                    swal({
                        title: "",
                        text:  Translator.trans('ics_map.help_probably_in_place'),
                        icon: "success"
                    });

                }
                else if (response.order.current_status === "6") {

                }
            }

            if(currentStatus !== "6" && response.order.current_status === "6" && serviceLocation && serviceLocation.lon) {
                _addServiceLocation(serviceLocation.lon, serviceLocation.lat);
            }

            if(currentStatus !== "7" && response.order.current_status === "7" && serviceLocation && serviceLocation.lon) {

                $('.swal-overlay.swal-overlay--show-modal').removeClass('init-hidden');

                swal({
                    title: "",
                    text: Translator.trans('ics_map.car_towing'), // @desc("Samochód został odholowany do warsztatu."),
                    icon: "success"
                });

            }

            currentStatus = response.order.current_status;

            if(["4","6"].indexOf(currentStatus) !== 1) {
                if(eventRoute) eventRoute.removeWaypoints();
            }

            if(_opts._debugger) {
                _opts._debugger.set('currentStatus', response.order.current_status);
                _opts._debugger.update();
            }

        }

        /** Usługa zakończona, nie trzeba odświeżać */
        if(currentStatus === "7") {
            clearInterval(intervalRefresh);
        }

        if(response.isNear && !isNear) {
            if(eventRoute) eventRoute.removeWaypoints();
            isNear = true;
        }

        if(response.lastSaveAt) {
            lastCoordinateSaveAt = response.lastSaveAt;
        }

        if(response.lastCoordinateId && lastCoordinateId !== response.lastCoordinateId) {
            lastCoordinateId = response.lastCoordinateId;
        }

        if(response.towingLocation) {

            _updateTowingCar(response.towingLocation.lon, response.towingLocation.lat);
            _map._map.setCenterPos(new EMAPI.LonLat(response.towingLocation.lon, response.towingLocation.lat));

            if(!isNear) {

                var wayPoints = [];

                if(currentStatus === "4") {

                    wayPoints = [
                        {
                            lon: response.towingLocation.lon,
                            lat: response.towingLocation.lat,
                            type: 0
                        }, {
                            lon: eventLocation.lon,
                            lat: eventLocation.lat,
                            type: 2
                        }
                    ];

                }
                else if(currentStatus === "6") {

                    wayPoints = [
                        {
                            lon: response.towingLocation.lon,
                            lat: response.towingLocation.lat,
                            type: 0
                        }, {
                            lon: serviceLocation.lon,
                            lat: serviceLocation.lat,
                            type: 2
                        }
                    ];

                }

                if(wayPoints.length) {
                    updateRoute(eventRoute, wayPoints);
                }

            }

        }

        if(_opts.enableRoute) {
            if(response.lines) {
                addLines(response.lines);
            }

            if(response.staticWays) {
                addStaticWays(response.staticWays);
            }
        }


        // if(response.towingLocation && response.eventLocation) {
        //     addWayToEventLocation({
        //         lon: response.towingLocation.lon,
        //         lat: response.towingLocation.lat,
        //         type: 0
        //     }, {
        //         lon: response.eventLocation.lon,
        //         lat: response.eventLocation.lat,
        //         type: 2
        //     });
        // }


        // if(response.towing_location) {
        //     towingMarker.setPosition(new EMAPI.LonLat(response.towing_location.longitude, response.towing_location.latitude));
        //     MAP._map.setCenterPos(new EMAPI.LonLat(response.towing_location.longitude, response.towing_location.latitude));
        // }

    };

    function addWayToEventLocation(carLocation, eventLocation) {

        addNewRoute({
            color: getColorDynamicWay(),
            onCalculation: function (routeRepresentation) {

                setValueOfCase('estimatedTime', (routeRepresentation.totalRouteTime !== "0min") ? routeRepresentation.totalRouteTime.replace("min", " min") : "1 min");

                if(currentStatus === "4" || currentStatus === "6") {
                    setValueOfCase('remainingDistance', routeRepresentation.totalRouteLength + " km");
                } else if(currentStatus === "5" || currentStatus === "9" ){
                    setValueOfCase('remainingDistance', "Na miejscu");
                }
                else {
                    setValueOfCase('remainingDistance', "-");
                }

                updateCaseInfo();

            }
        }, [carLocation, eventLocation]).then(function (route) {
            eventRoute = route;
        });

    }

    function addStaticWays(ways, forceStatus) {

        ways.forEach(function (ele) {

            var ways = [
                {
                    lon: ele.start.lon, lat: ele.start.lat, type: 0
                },
                {
                    lon: ele.end.lon, lat: ele.end.lat, type: 2
                }
            ];

            addNewRoute({
                color: getColorStaticWay(forceStatus),
                opacity: 1
            }, ways);

        });

    }

    function addLines(lines, forceStatus) {

        var _coordinates = [],
            way;

        if(lineLayer === null) {
            lineLayer = new EMAPI.VectorLayer('Way');
        }

        _map._map.addLayer(lineLayer);

        lines.forEach(function (line, i) {

            line.forEach(function (e, j) {
                _coordinates.push((new EMAPI.LonLat(e.lon, e.lat)));
            });

            way = new EMAPI.Object.PolylineObject({
                name:'Way_' + i,
                labelVisible: false,
                strokeWidth:'5',
                fontColor:getColorStaticWay(forceStatus),
                strokeColor:getColorStaticWay(forceStatus),
                lonLat: _coordinates
            });

            lineLayer.addObjects(way);

            // _map.addMarker('coordinate_' + Math.floor(Math.random() * 10000), 'poit', [_coordinates[0].lat, _coordinates[0].lon], 'point', '', 'point');
            // _map.addMarker('coordinate_' + Math.floor(Math.random() * 10000), 'poit', [_coordinates[_coordinates.length-1].lat, _coordinates[_coordinates.length-1].lon], 'point', '', 'point');

            _coordinates = [];

        });

    }

    function _startIntervalRefresh() {
        intervalRefresh = setInterval(onIntervalRefresh, REFRESH_INTERVAL);
    }

    var onIntervalRefresh = function() {

        $.ajax({
            url: Routing.generate('map_towing_monitor_refresh', {lastCoordinateId: lastCoordinateId, orderNumber: orderNumber, orderStatus: currentStatus, saveAt: lastCoordinateSaveAt}),
            type: "GET",
            success: _onSuccessRefresh
        });

    };

    // -------------------------------------------------------
    //                      END PUBLIC
    // -------------------------------------------------------

    function dumpCoordinates(coordinates) {

        if(coordinates) {
            coordinates.forEach(function (e, i) {
                // setTimeout(function () {
                _map.addMarker('coordinate_' + e.id, 'poit', [e.lat, e.lon], 'point', '', 'point');
                // }, tim);
                // tim += 500;
            });
        }

    }

    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }


    function eventsHandle() {


    }

    function init() {

        eventsHandle();

    }

    init();

    return {
        initMap: _initMap,
        addEventLocation: _addEventLocation,
        addStartLocation: _addStartLocation,
        addTowingCar: _addTowingCar,
        updateTowingCar: _updateTowingCar,
        addRoute: addNewRoute,
        handleRequestLoadMap: _handleRequestLoadMap

    }

});



// function addStaticWays(ways) {
//
//     var routesPromise = [];
//
//     ways.forEach(function (ele, i) {
//
//         routesPromise.push(mapMonitoringModule.addRoute({
//             color: '#252832',
//             opacity: 0
//         }, ways));
//
//     });
//
//     $.when.apply($, routesPromise).then( function () {
//
//         $.each(arguments, function (i, route) {
//
//             $.each(route._wayPoints._layer._layers, function (i, ele) {
//                 if(!ele.hasOwnProperty('_icon')) {
//                     ele.options.opacity = 1;
//                     ele._initStyle();
//                 }
//             });
//
//         });
//
//
//     });
//
// }