var _debugMapMonitor = function () {

    var dataSet = {
        caseNumber: '',
        orderId: '',
        currentStatus: '',
        countCoordinates: 0,
        countLines: 0,
        countStaticWays: 0,
        location_mode: '',
        updatedAt: ''
    };

    var _init = function () {

        $('.debugger-box').removeClass('init-hidden');

        getOtherOrders();

    };

    var getOtherOrders = function () {

        $.ajax({
            url: Routing.generate('map_towing_monitor_get_orders'),
            type: "GET",
            success: function (response) {

                var $tableBody = $('#orders-table tbody'),
                    trProto = '<tr><td><a href="__LINK__">__NUMBER__</a></td><td>__STATUS__</td></tr>'

                $.each(response, function (i, order) {

                    var tr = trProto.replace('__NUMBER__', order.number).replace('__STATUS__', order.current_status)
                        .replace('__LINK__', Routing.generate('map_towing_monitor', {'number': order.number}));

                    $tableBody.append(tr);

                });


            }
        });

    };

    var update = function() {

        $('#case-number').text(dataSet.caseNumber);
        $('#order-id').text(dataSet.orderId);
        $('#current-status').text(dataSet.currentStatus);
        $('#count-coordinates').text(dataSet.countCoordinates);
        $('#count-lines').text(dataSet.countLines);
        $('#count-static-ways').text(dataSet.countStaticWays);
        $('#location-mode').text(dataSet.location_mode);
        $('#updated-at').text(dataSet.updatedAt);

    };

    var set = function (name, value) {

        if(dataSet.hasOwnProperty(name)) {
            dataSet[name] = value;
        }
    };

    var setData = function(data) {

        dataSet.countCoordinates = data.coordinates.length;
        dataSet.countStaticWays = data.staticWays.length;

        var lines = 0;

        $.each(data.lines, function (i, ele) {
             lines += ele.array.length;
        });

        dataSet.countLines = lines;

        dataSet.orderId = data.order.id;
        dataSet.caseNumber = data.order.number;
        dataSet.currentStatus  = data.order.current_status + ' (' + data.order.status_name + ')';
        dataSet.location_mode = data.order.location_mode;
        dataSet.updatedAt = data.order.updated_at;

        update();

    };

    _init();

    return {
        update: update,
        setData: setData,
        set: set
    }

};