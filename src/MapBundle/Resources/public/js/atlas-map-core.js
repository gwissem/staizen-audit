// var AtlasMapInterface;

/**
 * initialize ATLAS MAP
 */

var AtlasMapModule = {
    currentProvider: 'EMAPA',
    isReady: false,
    callbackOnReady: [],
    ShowMap: null,
    init: function (properties, callback) {

        if (!AtlasMapModule.isReady) {
            AtlasMapModule.callbackOnReady.push({
                'callback': callback,
                'properties': properties
            });
        }
        else {
            AtlasMapModule.ShowMap(properties, callback);
        }
    }
};

(function ($, window, document) {

    $(function () {

        var timer = 30;

        var loadingProvider = setInterval(function () {

            switch (AtlasMapModule.currentProvider) {
                case 'EMAPA': {

                    if (typeof EMAPI != "undefined") {
                        initMap();
                        clearInterval(loadingProvider);
                    }

                    break;
                }
                default : {
                    clearInterval(loadingProvider);
                    break;
                }
            }

            if (timer === 0) {
                clearInterval(loadingProvider);
            }

            timer--;

        }, 1000);

        loadCountries();

    });

    function loadCountries() {


    }

    function initMap() {

        AtlasMapModule.ShowMap = initializeAtlasMap;
        AtlasMapModule.isReady = true;
        AtlasMapModule.callbackOnReady.forEach(function (ele, i) {
            if (ele.properties && typeof ele.callback == "function") {
                initializeAtlasMap(ele.properties, ele.callback);
            }
            else {
                console.error('Błąd ładowania mapy!');
            }
        });

        AtlasMapModule.callbackOnReady = [];

    }

    function initializeAtlasMap(properties, callback) {

        var AtlasMapInterface = null;

        try {
            AtlasMapInterface = new AtlasMap(properties.provider, properties.opts);
            AtlasMapInterface.InitMap();
            AtlasMapInterface.RunMap();
        }
        catch (e) {
            AtlasMapInterface = null;
            console.error(e);
        }

        if (typeof callback == "function") {
            callback(AtlasMapInterface);
        }

    }

}(window.jQuery, window, document));


/** class AtlasMap */

var AtlasMap = (function () {

    var DEF_LOCATION = [52.406631469, 16.919521331];

    function AtlasMap(Provider, options) {

        /** Valid a provider */
        testProvider(Provider);

        this.data = {
            markers: [],
            clientLocation: {},
            wayPoints: {
                source: {},
                target: {}
            }
        };

        this._opts = $.extend(defaultOptions(), options);

        this._P = Provider;

        /** Save and call init() of provider  */

        this._P.init(this, this.data);

        // AtlasMap.prototype.publicMethod = {
        //     _log : logMap
        // };

    }

    /**
     * --------------------------------------------------------------------------------------------
     * PUBLIC METHODS
     * --------------------------------------------------------------------------------------------
     */

    AtlasMap.prototype.version = '1.0';

    AtlasMap.prototype.reDraw = function () {
        this._P.reDraw();
    };

    /** Init a map (load configurations, init layout and handle events) */

    AtlasMap.prototype.InitMap = function () {
        loadConfigurations.call(this);
        initLayout.call(this);
        handleEvents.call(this);
    };

    /** Load a map in layout */

    AtlasMap.prototype.RunMap = function () {
        // AtlasMap._P.hello();

        if(this._opts.centerCoordinates.length === 0) this._opts.centerCoordinates = DEF_LOCATION;

        if (!$(this._opts.mapContainer).length) {
            throw "Brak kontenera mapy";
        }
        else {
            this._map = this._P.initMap(
                $(this._opts.mapContainer),
                new AtlasLoc(this._opts.centerCoordinates[0], this._opts.centerCoordinates[1]),
                this._opts.zoomMap
            );
        }

    };

    /**
     *
     */

    AtlasMap.prototype.getCurrentZoom = function () {

        return this._P.getZoom();

    };

    /**
     *
     * @param {number[]=} LatLng
     * @param {function=} callback
     */
    AtlasMap.prototype.searchByCoordinates = function (LatLng, callback) {

        var latLng = LatLng || this._opts.centerCoordinates;

        /** Tutaj cały system parsowania z różnych formatów */

        parseCoordinates(latLng);

        searchClientLocationByCoordinates.call(this, latLng, callback);

    };

    function parseCoordinates(latLng) {

        $.each(latLng, function (i, ele) {

           /** Czy są stopnie, minuty, sekundy */

            if(typeof ele === "string") {
                ele = ele.trim();
            }

            if((/^-?[\d°]{1,4}[ ][\d']{1,3}[ ][\d,.']+$/.test(ele))) {

                ele = ele.replace('°', '').replace(/\'/g, '').replace(',', '.');

                var values = ele.split(" ");

                if(values.length === 3) {
                    latLng[i] = (parseInt(values[0]) + (parseInt(values[1])/60) + (parseFloat(values[2])/3600));
                }

            }

            if(typeof ele === "string") {
                if(ele.indexOf(',') !== -1) {
                    latLng[i] = ele.replace(',', '.');
                }
            }

        });

    }

    /**
     *
     * @param {string} address
     * @param {function=} callback
     */
    AtlasMap.prototype.searchByAddress = function (address, callback) {

        searchClientLocationByAddress.call(this, address, callback);

    };

    /**
     *
     * @param {InfoLocation} infoLocation
     * @param {function=} callback
     */
    AtlasMap.prototype.geocodeFullAddress = function (infoLocation, callback) {

        // var self = this;

        this._P.geocodeFullAddress(infoLocation, function (result) {

            // returnResultLocation.call(self, result, 'address');

            if (typeof callback === 'function') {
                callback(result);
            }

        });

    };

    /**
     *
     * @param {AtlasLoc} from
     * @param {AtlasLoc} to
     * @param {function=} callback
     */
    AtlasMap.prototype.drawRouteWay = function (from, to, callback) {

        $.when(drawWay.call(this, from, to)).then(

            /** @param {InfoRoute} InfoRoute */
            function (InfoRoute) {
                if(InfoRoute){

                    callback(InfoRoute);

                    // AtlasMap._opts.infoContent.$_routeTotalLength.text(InfoRoute.totalRouteLength);
                    // infoContentVisible(true);
                }
                else {
                    callback(null);
                    console.log("Nie znaleziono.");
                }
            }
        );

    };


    /**
     * -- Dodawanie markera do mapy --
     *
     * Dostępne ikony: ambulance, car, caraccident, carrepair, caution, repair.
     *
     * @param {string} name
     * @param {string} label
     * @param {number[]} latLng
     * @param {string=} content
     * @param {string=} type
     * @param {string=} icon
     * @param {Object=} extraData
     *
     */
    AtlasMap.prototype.addMarker = function (name, label, latLng, content, type, icon, extraData) {

        if (!name || !label || !latLng) {
            console.error('[Dodawanie markera] - brak wymaganych parametrów');
            return false;
        }

        var iconMarker = icon || 'caution';

        var marker = new AtlasMarker(
            name,
            label,
            content || '',
            type || 'crash',
            new AtlasLoc(latLng[0], latLng[1]),
            '/images/map/' + iconMarker + '.png',
            extraData || {}
        );

        marker['object'] = setMarkerOnMap.call(this, marker);

        return marker;
    };


    /**
     * -- Usuwa marker z mapy --
     * @param {AtlasMarker} marker
     */

    AtlasMap.prototype.removeMarker = function (marker) {
        this._P.removeMarker(marker);
    };

    /**
     * -- Centering a map --
     *
     * @param {AtlasLoc=} LatLng
     * @param {number=} zoom
     *
     */

    AtlasMap.prototype.centerMap = function (LatLng, zoom) {

        var latLng = LatLng || this._opts.centerCoordinates;
        centerMap.call(this, new AtlasLoc(latLng[0], latLng[1]), zoom || null);

    };

    AtlasMap.prototype.centerDefault = function () {

        var atlasLoc = new AtlasLoc(this._opts.centerCoordinates[0], this._opts.centerCoordinates[1]);

        centerMap(atlasLoc, this._opts.zoomMap);

        AtlasMap.prototype.addMarker('nazwa', 'label', [AtlasMap._opts.centerCoordinates[0], AtlasMap._opts.centerCoordinates[1]]);

        searchClientLocationByCoordinates.call(this, atlasLoc.toArray(), function (data) {
            // console.log(data);
        })
    };

    /**
     * --------------------------------------------------------------------------------------------
     * PRIVATE METHODS - MAIN
     * --------------------------------------------------------------------------------------------
     */

    function loadConfigurations() {

    }

    function initLayout() {

    }

    function handleEvents() {

        var self = this;

        if (this._opts.$_inputListener !== null && this._opts.$_inputListener.length) {

            this._opts.$_inputListener.on('keyup', function (event) {
                var $t = $(this);

                /** Odpalenie wyszukiwania na Enter */
                if (event.which === 13 || event.keyCode === 13) {
                    if ($t.val()) {
                        searchClientLocationByAddress.call(self, $t.val());
                    }
                    return false;
                }
                return true;

                // delayFun(function () {
                //     if($t.val()) {
                //         searchClientLocationByAddress.call(self, $t.val());
                //     }
                // }, 2000);
            });

            /** Rysowanie drogi */

            // AtlasMap._opts.$_buttonDraw_way.on('click', function (e) {
            //     e.preventDefault();
            //
            //     var point1 = AtlasMap._opts.$_inputWayStart.val() || AtlasMap._opts.$_inputListener.val(),
            //         point2 = AtlasMap._opts.$_inputWayEnd.val();
            //
            //     if(point1 && point2)
            //     {
            //         $.when(getCoordinatesFromAddress(point1), getCoordinatesFromAddress(point2)).then(
            //             function (loc1, loc2) {
            //                 if(loc1 && loc2)
            //                 {
            //
            //                     $.when(drawWay(loc1.coordinates, loc2.coordinates)).then(
            //
            //                         /** @param {InfoRoute} InfoRoute */
            //                         function (InfoRoute) {
            //                             if(InfoRoute){
            //
            //                                 AtlasMap._opts.infoContent.$_routeTotalLength.text(InfoRoute.totalRouteLength);
            //                                 infoContentVisible(true);
            //                             }
            //                             else {
            //                                 console.log("Nie znaleziono.");
            //                             }
            //                         }
            //                     );
            //                 }
            //                 else {
            //                     /** NOT VALID */
            //                     return false;
            //                 }
            //             }
            //         );
            //     }
            //     else {
            //         /** NOT VALID */
            //         return false;
            //     }
            // })
        }

    }

    /**
     * -- Default options of AtlasMap --
     *
     * @returns {{mapContainer: string, centerCoordinates: number[], zoomMap: number}}
     */

    function defaultOptions() {
        return {
            mapContainer: '#atlas-map',
            centerCoordinates: DEF_LOCATION,
            zoomMap: 6,
            // $_inputListener: $('#client_target'),
            $_inputListener: null,
            // $_inputResult: [],
            $_inputResult: null,
            inputResultIfEmpty: true,
            // onFindAddress: function (resultData) {
            // },
            // onDragEndMarker: function (marker) {
            // },

            onFindAddress: null,
            onDragEndMarker: null,
            onMapChange: null,
            onPutMarker: null,

            centerAfterFind: false,

            $_inputFoundLocalization: $('#find_localization'),
            $_inputWayStart: $('#way_start'),
            $_inputWayEnd: $('#way_end'),
            $_buttonDraw_way: $('#draw_way'),
            infoContent: {
                $box: $('.atlas-map-info-content'),
                $_routeTotalLength: $('#routeTotalLength')
            }
        }
    }

    /**
     * -- Check if require methods and properties exist in Map Provider --
     *
     * @param Provider
     */

    function testProvider(Provider) {

        if (typeof Provider === 'undefined') throw "Map Provider Not Found";

        var methods = ['hello', 'initMap', 'init', 'getCoordinatesFromAddress'],
            variables = ['nameProvider', 'map', '_AtlasMap'];

        $.each(methods, function (i, ele) {
            if (typeof Provider[ele] !== 'function') throw "AtlasMap error: method \"" + ele + "()\" don't exists in Map Provider.";
        });

        $.each(variables, function (i, ele) {
            if (typeof Provider.get(ele) === 'undefined') throw "AtlasMap error: properties \"" + ele + "\" don't exists in Map Provider.";
        });
    }

    function infoContentVisible(visible) {
        if (!visible) {
            AtlasMap._opts.infoContent.$box.removeClass('active');
        }
        else {
            AtlasMap._opts.infoContent.$box.addClass('active');
        }
    }

    /**
     * --------------------------------------------------------------------------------------------
     * PRIVATE METHODS - OTHERS
     * --------------------------------------------------------------------------------------------
     */


    /**
     * Type - coordinates or address
     *
     * @param {InfoLocation} data
     * @param {string} type
     */
    function returnResultLocation(data, type) {

        eventFindAddress.call(this, data);

        if (data) {

            if (this._opts.centerAfterFind) {
                centerMap.call(this, data[0].coordinates);
            }

            if (this._opts.$_inputResult && this._opts.$_inputResult.length) {

                if (!this._opts.$_inputResult.is(":focus")) {
                    this._opts.$_inputResult.val(data[0].toInput())
                }

                // if(!this._opts.inputResultIfEmpty) {
                //     this._opts.$_inputResult.val(data.toInput())
                // }
                // else if(!Boolean(this._opts.$_inputResult.val())) {
                //     this._opts.$_inputResult.val(data.toInput())
                // }

            }
        }

    }

    function eventFindAddress(resultData) {

        if (typeof this._opts.onFindAddress == "function") {
            this._opts.onFindAddress(resultData);
        }

    }

    /**
     * @param {number[]} coordinates
     * @param {function=} callback
     */

    function searchClientLocationByCoordinates(coordinates, callback) {

        var atlasLoc = new AtlasLoc(coordinates[0], coordinates[1]);

        var self = this;

        this._P.getAddressFromCoordinates(atlasLoc, function (data) {

            returnResultLocation.call(self, data, 'coordinate');

            if (typeof callback === 'function') {

                callback(data);
            }

        });

    }

    /**
     * @param {string} inputAddress
     */
    function searchClientLocationByAddress(inputAddress, callback) {

        var self = this;

        this._P.getCoordinatesFromAddress(inputAddress, function (result) {

            returnResultLocation.call(self, result, 'address');

            if (typeof callback === 'function') {

                callback(result);
            }

        });

    }

    /**
     *
     * @param {AtlasLoc} point1
     * @param {AtlasLoc} point2
     */

    function drawWay(point1, point2) {

        var dfd = jQuery.Deferred();

        this._P.drawRouteWay(point1, point2, function (result) {
            dfd.resolve(result);
        });

        return dfd.promise();
    }

    /**
     * -- Centering a map --
     *
     * @param {AtlasLoc} LatLng
     * @param {number=} zoom
     */

    function centerMap(LatLng, zoom) {
        zoom = zoom || this._opts.zoomMap;

        this._P.centerMap(LatLng, zoom);
    }

    /**
     * @param {AtlasMarker} markerData
     * @description
     *      Add marker to map
     */

    function setMarkerOnMap(markerData) {
        return this._P.addMarker(markerData);
    }

    /**
     * @param {string} msg
     */

    function logMap(msg) {
        console.info(msg);
    }

    return AtlasMap;

}());

/**
 * --------------------------------------------------------------------------------------------
 * HELPERS
 * --------------------------------------------------------------------------------------------
 */

var _MapMethods = function () {

    var paths = {
        city: '87',
        street: '94',
        streetNumber: '95',
        objectNumber: '96',
        voivodeship: '88',
        county: '91',
        commune: '90',
        country: '86',
        areaName1: '88',
        areaName2: '91',
        areaName3: '90',
        lat: '93',
        lng: '92',
        zip: '89',
        typeLoc: '257',
        desc: '63',
        location: '85',
        road: '509',
        inCity: '541'
    };

    var $resultForm = null;

    return {
        setResultForm: function (resultForm) {
            $resultForm = resultForm;
        },
        getResultForm: function () {
            return $resultForm;
        },
        getPath: function (nameInput, prefix, replace) {

            if(paths.hasOwnProperty(nameInput)) {
                var path = ( (typeof prefix !== "undefined") ? (prefix + ',' + paths[nameInput]) : paths[nameInput] );
                return (replace === true) ? ( path.replace(/,/g, '-') ) : path;
            }

            console.error('Nie ma takiego typu lokalizacji: ' + nameInput);
            return '';

        },
        searchInputs: function ($resultForm, prefixPath) {

            /** Wyszukiwanie kontrolek na formularzu */

            var inputs = {};

            Object.keys(paths).forEach(function(key) {
                inputs[key] = $resultForm.find('[data-attribute-path="' + prefixPath + ',' + paths[key] + '"]');
            });

            return inputs;

        }
    }
}();

/**
 *
 * @param name
 * @param label
 * @param content
 * @param type
 * @param LatLng
 * @param icon
 * @param extraData : { moveable, clickable }
 * @constructor
 */

var AtlasMarker = function (name, label, content, type, LatLng, icon, extraData) {
    this.name = name;
    this.label = label;
    this.content = content;
    this.type = type;
    this.latLng = LatLng;
    this.icon = icon;
    this.extraData = extraData;

    this.isMoveable = function () {
        return (extraData.moveable);
    };

    this.isClickable = function () {
        return (extraData.moveable);
    }
};

/**
 * @param {number} lat
 * @param {number} lng
 */

var AtlasLoc = function (lat, lng) {
    this.lat = lat;
    this.lng = lng;
    this.lon = lng;
    this.toArray = function (inverse) {
        if (inverse === true) return [this.lng, this.lat];
        else return [this.lat, this.lng];
    };
    this.toString = function (inverse) {
        if (inverse === true) return this.lng + "|" + this.lat;
        else return this.lat + "|" + this.lng;
    }
};

/**
 * e-mapa format: (miejscowość|kod)[,ulica [numer]][;kod|adm1[,adm2[,adm3]])]
 *
 * @param {AtlasLoc} AtlasLoc
 * @param {string} city
 * @param {string} zip (xx-yyy)
 * @param {string} street
 * @param {string} streetNumber
 * @param {string} areaName1 - voivodeship (województwo)
 * @param {string} areaName2 - county (powiat)
 * @param {string} areaName3 - commune (gmina)
 * @param {string} country
 * @param road
 * @param inCity
 */

var InfoLocation = function (AtlasLoc, city, zip, street, streetNumber, areaName1, areaName2, areaName3, country, road, inCity) {
    this.coordinates = AtlasLoc || null;
    this.city = city || '';
    this.zip = zip || '';
    this.street = street || '';
    this.streetNumber = streetNumber || '';
    this.areaName1 = areaName1 || '';
    this.areaName2 = areaName2 || '';
    this.areaName3 = areaName3 || '';
    this.country = country || '';
    this.road = road || '';
    this.inCity = inCity || 0;

    this.toString = function () {

        var string = this.city + ' (' + this.zip + ') ';
        if (this.street) {
            string += ", " + this.street + '' + this.streetNumber;
        }

        string += ' [' + this.areaName1 + ', ' + this.areaName2 + ', ' + this.areaName3 + ']';

        return string;
    };

    this.toObjRequest = function () {

        return {
            city: this.city,
            country: this.country,
            voivodeship: this.areaName1,
            zipCode: this.zip,
            street: this.street,
            streetNumber: this.streetNumber
        };

    };

    /** For autocomplete */
    this.value = InfoLocationMethods.toInput.call(this, true);
    this.string = InfoLocationMethods.toValue.call(this);
    this.label = InfoLocationMethods.toInput.call(this);
    this.paragraphInfo = InfoLocationMethods.paragraphInfo.call(this);

    this.getIterator = function () {
        return [
            'coordinates',
            'city',
            'zip',
            'street',
            'streetNumber',
            'areaName1',
            'areaName2',
            'areaName3',
            'country',
            'road',
            'inCity'
        ]
    }
};

var InfoLocationMethods = {
    toInput: function (useReplace) {

        var string = this.city || this.zip;

        if (this.street) {
            string += ", " + this.street;
            if (this.streetNumber) string += " " + this.streetNumber;
        }

        if (this.zip || this.areaName1) {
            string += "; " + (this.zip || this.areaName3);
        }

        if (this.areaName2) string += ", " + this.areaName2;
        if (this.areaName1) string += ", " + this.areaName1;

        return (useReplace) ? string.replaceSpecialChars() : string;
    },
    toValue: function (useReplace) {
        var string = this.city || this.zip;

        if (this.street) {
            string += ", " + this.street;
            if (this.streetNumber) string += " " + this.streetNumber;
        }

        return (useReplace) ? string.replaceSpecialChars() : string;
    },
    paragraphInfo: function (inputs) {
        var address = "";

        if(inputs) {

            address += Translator.trans('map.address') + ": <strong>" + inputs.city.val() + ', ' + inputs.street.val() + ' ' +  // @desc("Adres")
                inputs.streetNumber.val() + '; ' + inputs.zip .val() + '</strong>';

            var _inCity = (inputs.inCity.val() == "1") ? Translator.trans('global.yes') : Translator.trans('global.no');
            address += "<br>" + Translator.trans('map.is_in_city') + ": <strong>" + _inCity + '</strong>'; // @desc("Czy w miejscowości")

            if(inputs.road.val()) {
                address += "<br>" + Translator.trans('map.way') + ": <strong>" + inputs.road.val() + "</strong>"; // @desc("Droga")
            }
            address += "<br>" + Translator.trans('map.commune') + ": <strong>" + // @desc("Gmina")
                inputs.areaName3.val() + "</strong><br>" + Translator.trans('map.county') + ": <strong>" + // @desc("Powiat")
                inputs.areaName2.val() + "</strong><br>" + Translator.trans('map.voivodeship') + ": <strong>" + // @desc("Województwo")
                inputs.areaName1.val() + "</strong><br>" + Translator.trans('map.country') + ": <strong>" + // @desc("Kraj")
                inputs.country.val() + "</strong>";
        }
        else {

            address += Translator.trans('map.address') + ": <strong>" + this.city + ', ' + this.street + ' ' + this.streetNumber + '; ' + this.zip + '</strong>';

            var _inCity = (this.inCity == "1") ? Translator.trans('global.yes') : Translator.trans('global.no');
            address += "<br>" + Translator.trans('map.is_in_city') + ": <strong>" + _inCity + '</strong>';

            if(this.road) {
                address += "<br>" + Translator.trans('map.way') + ": <strong>" + this.road + "</strong>";
            }
            address += "<br>" + Translator.trans('map.commune') + ": <strong>" +
                this.areaName3 + "</strong><br>" + Translator.trans('map.county') + ": <strong>" +
                this.areaName2 + "</strong><br>" + Translator.trans('map.voivodeship') + ": <strong>" +
                this.areaName1 + "</strong><br>" + Translator.trans('map.country') + ": <strong>" +
                this.country + "</strong>";
        }

        return address;
    }
};


/**
 *
 * @param {number} totalRouteLength
 */

var InfoRoute = function (totalRouteLength, data) {
    this.totalRouteLength = totalRouteLength || 0;
    this.unit = 'km';
    this.data = data || null;

    this.toString = function () {
        return this.totalRouteLength + ' ' + this.unit;
    };

    this.getNameRoute = function () {

        if(this.data !== null && this.data.routeFrom && this.data.routeTo) {
            return (this.data.routeFrom + " --> " + this.data.routeTo );
        }

        return '';
    };

    this.getRouteItems = function () {

        if(this.data !== null && this.data.routeItems) {
            return this.data.routeItems;
        }

        return null;
    }
};

var delayFun = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function asyncScript(u, c) {
    var d = document, t = 'script',
        o = d.createElement(t),
        s = d.getElementsByTagName(t)[0];
    console.log(u);
    o.src = u;
    console.log(o);
    if (c) {
        o.addEventListener('load', function (e) {
            c(null, e);
        }, false);
    }
    s.parentNode.insertBefore(o, s);
}
