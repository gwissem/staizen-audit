var ProviderEmapa = (function () {

    /**
     * Constructor
     */

    function ProviderEmapa(userId) {
        this.nameProvider = "EMAPA" + Math.random();
        this.map = null;
        this._AtlasMap = null;
        this.url = 'http://emapi.pl/LoadApi.aspx?userId=6vihz1Nbw5QC5rQ5&ver=2';

        this.userID = userId;
        this.finder = null;
        this.layer = null;
        this.router = null;
    }

    ProviderEmapa.prototype.get = function(name)
    {
        return this[name];
    };

    ProviderEmapa.prototype.hello = function () {

        console.info('Hello, I\'m ' + this.nameProvider);

    };

    /**
     * --------------------------------------------------------------------------------------------
     * MAIN METHODS
     * --------------------------------------------------------------------------------------------
     */

    ProviderEmapa.prototype.init = function (_AtlasMap, _AtlasMapData) {
        this._AtlasMap = _AtlasMap;
        this._markers = _AtlasMapData.markers;
    };

    ProviderEmapa.prototype.initMap = function ($mapContainer, LatLng, Zoom) {

        var self = this;

        this.map = new EMAPI.Map($mapContainer.attr('id'), this.userID);

        var lng = LatLng.lng || 52.4070216;
        var lat = LatLng.lat || 16.9174668;

        this.map.setCenterPos(new EMAPI.LonLat(lng, lat), Zoom);

        this.finder = new EMAPI.Finder(this.map);
        this.router = new EMAPI.Router(this.map, {
                // reportPointsHandlerPointsName: 'reportPointsHandlerPointsName',
                // reportResultHandlerName: 'reportResultHandlerName',
                // forceCalc: true
            }
        );

        resetActionContext.call(this);

        this.map.addMapAction(0, 'Postaw pinezke', 'http://emapi.pl/css/u/img/context/context_permlink.png', function (e) {
            if(typeof self._AtlasMap._opts.onPutMarker === "function") {
                self._AtlasMap._opts.onPutMarker(e);
            }
        });

        this.map.addMapAction(1, 'Pokaż w Google Maps', 'http://emapi.pl/css/u/img/context/context_defaultview.png', function (e) {
            window.open("https://www.google.com/maps?z=12&t=m&q=loc:+"+e.lat+"+"+e.lng+"+");
        });

        // this.map.addMapAction(0, 'Center', null, function (e) {

            // console.log(self._AtlasMap.markers);
        // });

        // var actionMapClick = new EMAPI.Actions.MapClick('putMarkerAction', {
        //     description: "Postaw pinezkę",
        //     position: 'topleft',
        //     img: 'http://emapi.pl/css/u/img/context/context_permlink.png',
        //     cursor: 'coordinate',
        //     action: function (e) {
        //         if(typeof self._AtlasMap._opts.onPutMarker === "function") {
        //             self._AtlasMap._opts.onPutMarker(e);
        //         }
        //     }
        // });

        // this.map.addMapAction(0, actionMapClick);

        this.map.setCallback('objDragStart', function (eventName, marker) {

            // ProviderEmapa.finder.degeocode(cos.lonLat, 17, function (result) {
            //     console.log(result);
            // });

        });

        var withCallback = false;

        if(typeof self._AtlasMap._opts.onDragEndMarker == "function") {
            withCallback = true;
        }

        this.map.setCallback('mapChange', function (a, b) {
            /** Dorzucić obsługę tego eventu. Żeby przeliczać ponownie koordynaty.*/

        });

        this.map.setCallback('objDragEnd', function (eventName, marker) {

            if(withCallback) {
                self._AtlasMap._opts.onDragEndMarker(marker);
            }

        });


        // var cos = new EMAPI.Actions.MapClick('testowo', {
        //     description: '0000000',
        //     action: function (e) {
        //         console.log(e);
        //     }
        // });

        // ProviderEmapa.map.removeMapAction(1);

        return this.map;
    };

    function resetActionContext() {
        this.map.removeMapAction(0);
        this.map.removeMapAction(0);
        this.map.removeMapAction(0);
        // this.map.removeMapAction(0);
        // this.map.removeMapAction(1);
    }

    /**
     * --------------------------------------------------------------------------------------------
     * REQUIRES PUBLIC METHODS
     * --------------------------------------------------------------------------------------------
     */

    ProviderEmapa.prototype.reDraw = function () {
        this.map.updateSize();
    };


    /**
     * 
     * @param {AtlasLoc} coordinates
     * @param {function} callback
     */
    ProviderEmapa.prototype.getAddressFromCoordinates = function (coordinates, callback) {

        var _this = this;

        // _this.finder.degeocode(new EMAPI.LonLat(coordinates.lng, coordinates.lat), 15, function (result) {
        //
        //     var city = (result.cities.length) ? result.cities[0].name : '';
        //     var street =(result.streets.length) ? result.streets[0].name : '';
        //     var road =(result.roads.length) ? result.roads[0].name : '';
        //
        //     var infoLocation = new InfoLocation (
        //         coordinates,
        //         city,
        //         result.zip,
        //         street,
        //         '',
        //         result.areaName1,
        //         result.areaName2 || '',
        //         result.areaName3 || '',
        //         result.areaName0,
        //         road,
        //         (city) ? 1 : 0
        //     );
        //
        //     callback(infoLocation);
        //
        // });


        $.ajax({
            url: Routing.generate('map_degeocode', {latitude: coordinates.lat, longitude: coordinates.lng}),
            type: "GET",
            success: function (data) {

                if(data['error'] && data['error'] === true) {

                    /** Jeżeli problem z MapCenter - Old method - by emapi JS */

                    _this.finder.degeocode(new EMAPI.LonLat(coordinates.lng, coordinates.lat), 15, function (result) {

                        var city = (result.cities.length) ? result.cities[0].name : '';
                        var street =(result.streets.length) ? result.streets[0].name : '';
                        var road =(result.roads.length) ? result.roads[0].name : '';

                        var infoLocation = new InfoLocation (
                            coordinates,
                            city,
                            result.zip,
                            street,
                            '',
                            result.areaName1,
                            result.areaName2 || '',
                            result.areaName3 || '',
                            result.areaName0,
                            road,
                            (city) ? 1 : 0
                        );

                        callback(infoLocation);

                    });

                }
                else {
                    var infoLocation = new InfoLocation (
                        coordinates,
                        data['city'],
                        data['zipCode'],
                        data['street'],
                        data['streetNumber'],
                        data['voivodeship'],
                        data['county'],
                        data['commune'],
                        data['country'],
                        data['road'],
                        data['inCity']
                    );

                    callback(infoLocation);
                }

            }
        });

    };

    /**
     * @param {string} inputAddress
     * @param {function} callback
     *
     * format adresu: (miejscowość|kod)[,ulica [numer]][;kod|adm1[,adm2[,adm3]])]
     */

    ProviderEmapa.prototype.getCoordinatesFromAddress = function (inputAddress, callback) {

        /** Usuwanie słowa "ul." */
        inputAddress = inputAddress.replace(/(ul\.)/g, '');

        /** By MapCenter*/

        searchLocationFromAddress.call(this, inputAddress, function (result) {
            callback(result);
        });

        /** By Emapi API */
        // searchLocationPointsFromAddress.call(this, inputAddress, function (result) {
        //     callback(result);
        // });

    };

    /**
     * @param {InfoLocation} infoLocation
     * @param {function} callback
     */

    ProviderEmapa.prototype.geocodeFullAddress = function (infoLocation, callback) {

        var _data = infoLocation.toObjRequest();
        _data['countryId'] = $('#map-widget-countries').val();

        $.ajax({
            url: Routing.generate('map_exact_geocode'),
            type: "POST",
            data: _data,
            success: function (response) {

                var item = response.result[0];

                if(item) {
                    var r = new InfoLocation (
                        new AtlasLoc(item.latitude, item.longitude),
                        item['city'],
                        item['zipCode'],
                        item['street'],
                        item['streetNumber'],
                        item['voivodeship'],
                        item['county'],
                        item['commune'],
                        item['country'],
                        item['road'],
                        (item['inCity']) ? 1 : 0
                    );

                    callback(r);
                }
                else {
                    callback(null);
                }

            },
            error: function (response) {
                callback(null);
            }
        });

        // this.finder.geocode(infoLocation.country, infoLocation.city, infoLocation.zip, infoLocation.street, infoLocation.streetNumber, function(result) {
        //
        //         if(result.result === 0 || result.result === 2)
        //         {
        //
        //             if(result.geocodeInfoArray && result.geocodeInfoArray.length > 0)
        //             {
        //
        //                 var item = result.geocodeInfoArray[0];
        //
        //                 var r = new InfoLocation (
        //                         new AtlasLoc(item.middlePoint.Latitude, item.middlePoint.Longitude),
        //                         item.city,
        //                         item.zip,
        //                         item.street,
        //                         item.streetNumber,
        //                         item.areaName1,
        //                         item.areaName2,
        //                         item.areaName3,
        //                         item.areaName0,
        //                         '',
        //                         1
        //                 );
        //
        //                 callback(r);
        //             }
        //             else {
        //                 callback(null);
        //             }
        //         }
        //         else if(result.result === 1)
        //         {
        //             callback(null);
        //         }
        //         else {
        //             callback(null);
        //         }
        //
        // });

    };

    ProviderEmapa.prototype.onMarkerDragEnd = function (marker) {

    };

    ProviderEmapa.prototype.addMarker = function (markerData) {
        return addMarker.call(this, markerData);
    };

    ProviderEmapa.prototype.removeMarker = function (markerData) {
        removeMarker.call(this, markerData);
    };

    ProviderEmapa.prototype.centerMap = function (LatLng, zoom){

        if(zoom){
            this.map.setCenterPos(new EMAPI.LonLat(LatLng.lng, LatLng.lat), zoom);
        }
        else {
            this.map.setCenterPos(new EMAPI.LonLat(LatLng.lng, LatLng.lat));
        }

    };

    ProviderEmapa.prototype.getZoom = function () {

        return this.map.getZoom();

    };

    ProviderEmapa.prototype.drawRouteWay = function (start, end, callback){

        drawRouteWay.call(this, start, end, callback);
        // drawRouteWay(start, end, function (response) {
        //     callback(response);
        // });
        

    };

    /**
     * --------------------------------------------------------------------------------------------
     * Private methods
     * --------------------------------------------------------------------------------------------
     */

    function afterCalculateRoute(callback, result) {

        var route = this.router.routeRepresentation();
        callback(new InfoRoute(route.totalRouteLength, route));
        this.router.removeEventListener('afterCalculateRoute');

    }

    function drawRouteWay(start, end, callback) {

        this.router.removeWaypoints(); //usunięcie punktów trasy z tablicy obiektów
        resetRoute.call(this);
        this.router.addWaypoint(new EMAPI.LonLat(start.lng, start.lat), 0, null);
        this.router.addWaypoint(new EMAPI.LonLat(end.lng, end.lat), 2, null);

        this.router.addEventListener('afterCalculateRoute', afterCalculateRoute.bind(this, callback), false);

        this.router.calculateRoute(
            0, 0,
            0, //obliczanie trasy najkrótszej
            {
                useGroundRoad:true,
                useFerry:true,
                useTollRoad:true,
                useVignetteRoads:true,
                useDifficulties:true,
                routeOptions: true
            },
            function (pos) {
                console.log('CALLBACK WORK');
            }, true
        );

        // var route = this.router.routeRepresentation(),
        //     i = 0;
        //
        // /** Sprawdzanie czy już policzył trase i zwrócenie InfoRoute - głupie rozwiązanie */
        //
        // var interval = setInterval(function (e) {
        //
        //     try {
        //         if(route && route.totalRouteLength)
        //         {
        //             callback(new InfoRoute(route.totalRouteLength, route));
        //             clearInterval(interval);
        //         }
        //     }
        //     catch(exception) {
        //
        //     }
        //     finally {
        //         i++;
        //     }
        //
        //     /** po 3 sek rezygnuje ze sprawdzania */
        //
        //     if(i > 5){
        //         callback(null);
        //         clearInterval(interval);
        //     }
        //
        // }, 500);

    }

    function resetRoute() {
        // this.router = new EMAPI.Router(this.map, {
        //         reportPointsHandlerPointsName: 'route'
        //     }
        // );
    }

    function addWayPoint(point) {
        // router.addWaypoint(new EMAPI.LonLat(19.405975341022, 51.85613979201), 0, null);  //dodanie punktu startowego
        // router.addWaypoint(new EMAPI.LonLat(19.952545165219, 52.117469879906), 1, null); //dodanie punktu
        // router.addWaypoint(new EMAPI.LonLat(20.673522948394, 52.418336289777), 2, null); //dodanie punktu końcowego
    }


    /** Szukanie po MapCenter */

    function searchLocationFromAddress(inputAddress, callback) {

        var _this = this;

        $.ajax({
            url: Routing.generate('map_geocode'),
            type: "POST",
            data: {expression: inputAddress, country: $('#map-widget-countries').val() },
            success: function (response) {

                if(response.error) {

                    searchLocationPointsFromAddress.call(_this, inputAddress, function (result) {
                        callback(result);
                    });

                }
                else {
                    if(response.result.length > 0) {

                        var results = [];

                        response.result.forEach(function (item, i) {

                            results.push( new InfoLocation (
                                new AtlasLoc(item.latitude, item.longitude),
                                item['city'],
                                item['zipCode'],
                                item['street'],
                                item['streetNumber'],
                                item['voivodeship'],
                                item['county'],
                                item['commune'],
                                item['country'],
                                item['road'],
                                (item['inCity']) ? 1 : 0
                            ));

                        });

                        callback(results);

                    }
                    else {
                        callback(null);
                    }
                }

            }
        });

    }

    function searchLocationPointsFromAddress(inputAddress, callback) {

        this.finder.search(inputAddress, function (result) {

            if(result.result == 0 || result.result == 2)
            {

                if(result.geocodeInfoArray)
                {

                    var results = [];

                    result.geocodeInfoArray.forEach(function (item, i) {

                        results.push( new InfoLocation (
                            new AtlasLoc(item.middlePoint.Latitude, item.middlePoint.Longitude),
                            item.city,
                            item.zip,
                            item.street,
                            item.streetNumber,
                            item.areaName1,
                            item.areaName2,
                            item.areaName3,
                            item.areaName0,
                            '',
                            1)
                        );

                    });

                    callback(results);
                }
                else {
                    callback(null);
                }
            }
            else if(result.result == 1)
            {
                callback(null);
            }

        });
    }

    function getCoordinates(callback) {

        this.finder.getCoordinates(0, function (response) {
            if (response.result == 0)
            {
                var coordinates = new AtlasLoc(response.middlePoint.Latitude, response.middlePoint.Longitude);
                callback(coordinates);
            }
        });

    }

    function removeMarker(markerData) {

        var indexMarker = null;
        var foundMarker = this._AtlasMap.markers.find(function (marker, index) {
            if(marker.name == markerData.name) {
                indexMarker = index;
                return true;
            }
            else {
                return false;
            }
        });

        if(foundMarker) {
            this.layer.removeObjects([this._AtlasMap.markers[indexMarker]]);
            this._AtlasMap.markers[indexMarker].destroy();
            this._AtlasMap.markers.splice( indexMarker, 1 );
        }

    }

    function updateMarker(markerData) {
        this._markers.forEach(function (ele) {

            if(ele.name === markerData.name) {

                ele.setPosition(new EMAPI.LonLat(markerData.lon, markerData.lat));
                ele.redraw();

            }

        });
    }

    function addMarker(markerData) {

        if(!this.layer)
        {
            this.layer = new EMAPI.VectorLayer('points layer');
            this.map.addLayer(this.layer);
        }

        var marker, found = 0;
        this._markers.forEach(function (ele, i, markers) {

            if(ele.name == markerData.name) {

                ele.label = markerData.label;
                ele.setPosition(new EMAPI.LonLat(markerData.latLng.lng, markerData.latLng.lat));
                ele.redraw();

                marker = markers[i];
                found++;
            }
        });

        if(!found) {
            var newMarker = new EMAPI.Object.PointObject({
                moveable: markerData.isMoveable(),
                clickable: markerData.isClickable(),
                name: markerData.name,
                label: markerData.label,
                // labelVisible: true,
                lonLat: new EMAPI.LonLat(markerData.latLng.lng, markerData.latLng.lat),
                externalGraphic: markerData.icon,
                graphicWidth:32,
                graphicHeight:37,
                graphicXOffset: -16,
                graphicYOffset: -38,
                labelXOffset: -32,
                labelYOffset: 37,
                fontSize: 16,
                fontFamily: 'arial'
            });

            this._markers.push(newMarker);
            this.layer.addObjects([this._markers[this._markers.length-1]]);
            marker = newMarker;
        }

        return marker;
    }

    function getInstanceOfRouter() {
        return new EMAPI.Router({
                map: this.map,
                reportPointsHandlerPointsName: 'punkty'
            }
        );
    }

    // function AtlasMapDo(method, data) {
    //     var methodToRun = ProviderEmapa._AtlasMap.publicMethod[method];
    //
    //     if(typeof methodToRun === 'function')
    //     {
    //         methodToRun(data);
    //     }
    // }

    return ProviderEmapa;
}());