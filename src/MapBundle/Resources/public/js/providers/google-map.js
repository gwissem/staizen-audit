var ProviderGoogleMap = (function () {

    // Constructor
    function ProviderGoogleMap() {
        ProviderGoogleMap.nameProvider = "Google Map";
        ProviderGoogleMap.map = null;
        ProviderGoogleMap._AtlasMap = null;

        ProviderGoogleMap.geocoder = null;
    }

    // PUBLIC METHODS - IMPORTANT

    ProviderGoogleMap.prototype.get = function(name)
    {
        return ProviderGoogleMap[name];
    };

    ProviderGoogleMap.prototype.hello = function () {
        console.log('Hello, I\'m ' + ProviderGoogleMap.nameProvider);
    };

    ProviderGoogleMap.prototype.init = function (_AtlasMap) {
        ProviderGoogleMap._AtlasMap = _AtlasMap;
    };

    ProviderGoogleMap.prototype.getCordsFromAddress = function (inputAddress, callback) {

        getCords(inputAddress, function (result) {
            callback(result);
        });

    };

    ProviderGoogleMap.prototype.initMap = function ($mapContainer, LatLng, Zoom) {

        ProviderGoogleMap.map = new google.maps.Map($mapContainer[0], {
            center: {lat: LatLng.lat, lng: LatLng.lng},
            zoom: Zoom
        });

        ProviderGoogleMap.geocoder = new google.maps.Geocoder();

        AtlasMapDo('_log', "Mapa załadowana!");

        return ProviderGoogleMap.map;
    };

    ProviderGoogleMap.prototype.addMarker = function (markerData) {
        var marker = new google.maps.Marker({
            position: {lat: markerData.latLng.lat, lng: markerData.latLng.lng},
            title: markerData.label
        });

        marker.setIcon(markerData.icon);
        marker.setMap(ProviderGoogleMap.map);
    };

    ProviderGoogleMap.prototype.centerMap = function (LatLng, zoom){

        if(zoom){
            ProviderGoogleMap.map.setZoom(zoom);
        }

        ProviderGoogleMap.map.setCenter(new google.maps.LatLng(LatLng.lat, LatLng.lng));

    };

    ProviderGoogleMap.prototype.drawRouteWay = function (start, end, callback){


    };

    // PRIVATE METHODS

    function getCords(inputAddress, callback) {
        ProviderGoogleMap.geocoder.geocode({
            "address": inputAddress
        }, function(results) {

            var location = new AtlasLoc(results[0].geometry.location.lat(), results[0].geometry.location.lng());
            callback(location);
        });
    }

    function AtlasMapDo(method, data) {
        var methodToRun = ProviderGoogleMap._AtlasMap.publicMethod[method];

        if(typeof methodToRun === 'function')
        {
            methodToRun(data);
        }
    }

    return ProviderGoogleMap;
})();