<?php

namespace MapBundle\Service;

use CaseBundle\Service\CaseHandler;
use CaseBundle\Utils\ParserMethods;

class MapMonitorService
{

    /**
     * Określa jaka może być maksymalna przerwa pomiędzy współrzędnymi.
     * Odcinki te są zbierane i wstawiana jest droga.
     */
    const MAX_BLANK_COORDINATES = 500;

    /** @var CaseHandler */
    protected $caseHandler;

    /**
     * MapMonitorService constructor.
     * @param CaseHandler $caseHandler
     */
    public function __construct(CaseHandler $caseHandler)
    {
        $this->caseHandler = $caseHandler;
    }

    /**
     * @param $orderId
     * @param int $status
     * @return array
     */
    public function getCoordinatesByOrder($orderId, $status = 4) {

        return array_map(
            array($this,'mapCoordinate'),
            $this->caseHandler->getOrderCoordinates($orderId, NULL, $status)
        );

    }

    /**
     * @param $orderId
     * @param $lastSaveAt
     * @param int $status
     * @return array
     */
    public function getNewCoordinatesOfOrder($orderId, $lastSaveAt, $status = 4) {

        return array_map(
            array($this,'mapCoordinate'),
            $this->caseHandler->getOrderCoordinates($orderId, $lastSaveAt, $status)
        );

    }

    /**
     * @param int $status
     * @return array
     */
    public function getOrders($status = 4)
    {
        return $this->caseHandler->getOrders($status);
    }

    /**
     * @param $coordinate
     * @return array
     */
    static function mapCoordinate($coordinate) {
        return [
            'lat' => $coordinate['latitude'],
            'lon' => $coordinate['longitude'],
            'date' => $coordinate['saved_at'],
            'id' => $coordinate['id'],
        ];
    }

    public function getDataForMapMonitor($orderId) {

        $order = $this->getOrder($orderId);
        $rootId = CaseHandler::parseCaseNumber($order['number']);
        list($inWayCoordinates, $towingCoordinates) = [$this->getCoordinatesByOrder($orderId), []];

        if(intval($order['current_status']) >= 6) {
            $towingCoordinates = $this->getCoordinatesByOrder($orderId, '6');
        }

        $eventLocation = [
            'lat' => $order['latitude'],
            'lon' => $order['longitude']
        ];

        $first = (!empty($inWayCoordinates)) ? $inWayCoordinates[0] : null;

        if(!empty($towingCoordinates)) {
            $last = $towingCoordinates[count($towingCoordinates) - 1];
        }
        elseif (!empty($inWayCoordinates)) {
            $last = $inWayCoordinates[count($inWayCoordinates) - 1];
        }
        else {
            $last = null;
        }

        list($lines_, $staticWays_) = $this->prepareWays($inWayCoordinates);

        $lines = [
            [
                'status' => '4',
                'array' => $lines_
            ]
        ];

        $staticWays = [
           [
               'status' => '4',
               'array' => $staticWays_
           ]
        ];

        if(!empty($towingCoordinates)) {

            list($lines_, $staticWays_) = $this->prepareWays($towingCoordinates);

            $lines[] = [
                'status' => '6',
                'array' => $lines_
            ];

            $staticWays[] = [
                'status' => '6',
                'array' => $staticWays_
            ];
        }

        $coordinates = array_merge($inWayCoordinates, $towingCoordinates);

        $orderDetails = $this->caseHandler->getICSOrderDetails($orderId);

//        $direction = $this->countDirection($coordinates[count($coordinates)-5], $coordinates[count($coordinates) -1]);

        return [
            'eventLocation' => [
                'lat' => $order['latitude'],
                'lon' => $order['longitude']
            ],
            'towingLocation' => $last,
            'startLocation' => $first,
            'coordinates' => $coordinates,
//            'direction' => $direction,
            'lines' => $lines,
            'caseNumber' => ParserMethods::formattingCaseNumber($rootId),
            'lastCoordinateId' => (!empty($last)) ? $last['id'] : null,
            'lastSaveAt' => (!empty($last)) ? $last['date'] : null,
            'staticWays' => $staticWays,
            'order' => $order,
            'isNear' => $this->isNear($last, $eventLocation),
            'serviceLocation' => $this->getCoordinateOfService($rootId),
            'orderDetails' => $orderDetails
        ];
    }

    /**
     * @param $coordinate1
     * @param $coordinate2
     * @return float
     */
    private function countDirection($coordinate1, $coordinate2) {

        return atan(($coordinate1['lon'] - $coordinate2['lon']) / ($coordinate1['lat'] - $coordinate2['lat'])) * 180;

    }

    /**
     * @param $orderId
     * @param $lastCoordinateId
     * @param $orderStatus
     * @param $lastSaveAt
     * @return array
     */
    public function getDataForMapRefresh($orderId, $lastCoordinateId, $orderStatus, $lastSaveAt) {

        $order = $this->getOrder($orderId);
        $coordinates = $this->getNewCoordinatesOfOrder($orderId, $lastSaveAt, $orderStatus);
//        $direction = null;

        if(!count($coordinates)) {
            return [
                'order' => $order
            ];
        }

        $eventLocation = [
            'lat' => $order['latitude'],
            'lon' => $order['longitude']
        ];

        $last = $coordinates[count($coordinates)-1];

        $isNewData = $last['id'] !== $lastCoordinateId;

        list($lines, $staticWays) = [null, [], []];

        if($isNewData) {
            list($lines, $staticWays) = $this->prepareWays($coordinates);
        }

//        if(count($coordinates) >= 5) {
//            $direction = $this->countDirection($coordinates[count($coordinates)-5], $coordinates[count($coordinates) -1]);
//        }

        return [
            'towingLocation' => ($isNewData) ? $last : null,
            'lines' => $lines,
            'lastCoordinateId' => (!empty($last) && $isNewData) ? $last['id'] : null,
            'lastSaveAt' => (!empty($last)) ? $last['date'] : null,
            'staticWays' => $staticWays,
//            'direction' => $direction,
            'order' => $order,
            'isNear' => $this->isNear($last, $eventLocation)
        ];

    }

    /**
     * @param $coordinate1
     * @param $coordinate2
     * @param int $distance
     * @return bool
     */
    private function isNear($coordinate1, $coordinate2, $distance = 50): bool
    {

        if(empty($coordinate1) || empty($coordinate2)) {
            return false;
        }

        return $this->circleDistance($coordinate1['lat'], $coordinate1['lon'], $coordinate2['lat'], $coordinate2['lon']) <= $distance;

    }

    /**
     * @param $coordinates
     * @return array
     */
    private function prepareWays($coordinates) {

        $staticWays = [];
        $lines = [];
        $currentLinesPoints = [];
        $wasStaticWays = false;

        $prevCoordinate = array_shift($coordinates);

        foreach ($coordinates as $coordinate) {

            if($this->isTooFar($coordinate, $prevCoordinate)) {

                if($wasStaticWays) {
                    $staticWays[count($staticWays)-1]['end'] = $coordinate;
                }
                else {
                    $staticWays[] = [
                        'start' => $prevCoordinate,
                        'end' => $coordinate
                    ];
                }

                if(count($currentLinesPoints) >= 2) {
                    $lines[] = $currentLinesPoints;
                }

                $currentLinesPoints = [];
                $wasStaticWays = true;

            }
            else {
                $currentLinesPoints[] = $prevCoordinate;
                $wasStaticWays = false;
            }

            $prevCoordinate = $coordinate;

        }

        if(!$wasStaticWays) {
            $currentLinesPoints[] = $prevCoordinate;
        }

        if(count($currentLinesPoints) >= 2) {
            $lines[] = $currentLinesPoints;
        }

        return [$lines,$staticWays];

    }

    /**
     * @param $coordinate1
     * @param $coordinate2
     * @return bool
     */
    private function isTooFar($coordinate1, $coordinate2) {

        return ($this->circleDistance($coordinate1['lat'], $coordinate1['lon'], $coordinate2['lat'], $coordinate2['lon']) >= self::MAX_BLANK_COORDINATES);

    }

    /**
     * @param $rootId
     * @return array
     */
    public function getCoordinateOfService($rootId): array
    {
        return $this->caseHandler->getCoordinatesOfService($rootId);
    }

    /**
     * @param $orderId
     * @return array
     */
    public function getOrder($orderId) {
        return $this->caseHandler->getInfoOrderInTeka($orderId);
    }

    /**
     * @param $latitudeFrom
     * @param $longitudeFrom
     * @param $latitudeTo
     * @param $longitudeTo
     * @param int $earthRadius
     * @return float|int
     */
    private  function circleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $earthRadius;
    }

}

