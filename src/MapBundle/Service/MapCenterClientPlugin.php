<?php

namespace MapBundle\Service;

use DOMDocument;
use DOMElement;
use DOMXPath;

class MapCenterClientPlugin
{	
	private $conn_params = array();
	private $curl_conn;

//	const SERVER_URL = 'http://10.10.77.93:6090/xml?version=2.0';
    const SERVER_URL = 'http://10.10.77.131:6090/xml?version=2.0';

	function __construct()
	{
		$this->conn_params = array('url'=> self::SERVER_URL, 'username' => '', 'password' => '');
		$this->curl_conn = curl_init();
		curl_setopt($this->curl_conn, CURLOPT_URL, $this->conn_params['url']);

		if ($this->conn_params['username'] != '')
			curl_setopt($this->curl_conn, CURLOPT_USERPWD, $this->conn_params['username'].':'.$this->conn_params['password']);
	}

	function setUser($username = '', $password = '') {

        $this->conn_params = array('url'=> self::SERVER_URL, 'username' => $username, 'password' => $password);
        $this->curl_conn = curl_init();
        curl_setopt($this->curl_conn, CURLOPT_URL, $this->conn_params['url']);

        if ($this->conn_params['username'] != '')
            curl_setopt($this->curl_conn, CURLOPT_USERPWD, $this->conn_params['username'].':'.$this->conn_params['password']);

    }


	function __destruct()
	{
		curl_close ($this->curl_conn);
	}
	
	function __get($n)
	{		
		return $this->conn_params[$n];
	}
	
	function __set($n, $v)
	{
		if (isset($this->conn_params[$n]))
		{
			$this->conn_params[$n] = $v;
			if ($n == 'username' || $n == 'password')
				curl_setopt($this->curl_conn, CURLOPT_USERPWD, $this->conn_params['username'].':'.$this->conn_params['password']);
			if ($n == 'url')
				curl_setopt($this->curl_conn, CURLOPT_URL, $this->conn_params['url']);
		}
	}


    public function createSessionID()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'CreateSessionID');
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SessionID = $this->parseTextValue($xpath, '/ANSWER/SessionID/text()');
        return array('Result'=>$RESULT, 'SessionID'=>$SessionID);
    }

    public function keepSession($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'KeepSession');
        $this->addTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result'=>$RESULT);
    }

    public function dropSession($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DropSession');
        $this->addTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result'=>$RESULT);
    }

    public function setSessionComment($SessionID, $Comment)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetSessionComment');
        $this->addTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->addTextValue($querydoc, $root, 'Comment', $Comment);
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result'=>$RESULT);
    }

    public function GetDefaultLayers($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetDefaultLayers');
        $this->addTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MapLayers = $this->parseArrayValue($xpath, '/ANSWER/MapLayers');
        return array('Result'=>$RESULT, 'MapLayers'=>$MapLayers);
    }

    public function getProjections()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetProjections');
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Projections = $this->parseArrayValue($xpath, '/ANSWER/Projections');
        return array('Result'=>$RESULT, 'Projections'=>$Projections);
    }

    public function getDegeocodeLayers($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetDegeocodeLayers');
        $this->addTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        $DegeocodeLayers = $this->parseArrayValue($xpath, '/ANSWER/DegeocodeLayers');
        return array('Result'=>$RESULT, 'DegeocodeLayers'=>$DegeocodeLayers);
    }

    public function Geocode($SessionID, $ASCIISearch, $Points)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'Geocode');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ASCIISearch', $ASCIISearch);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'Points', $Points);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $GeocodeLevel = $this->ParseArrayValue($xpath, '/ANSWER/GeocodeLevel');
        $Positions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Positions');
        return array('Result' => $RESULT, 'GeocodeLevel' => $GeocodeLevel, 'Positions' => $Positions);
    }

    public function GeocodeEx($SessionID, $ASCIISearch, $Point, $MaxResultCount)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GeocodeEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ASCIISearch', $ASCIISearch);
        $this->AddRecordValue($querydoc, $root, 'Point', $Point);
        $this->AddTextValue($querydoc, $root, 'MaxResultCount', $MaxResultCount);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Points = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Points');
        $GeocodeLevel = $this->ParseArrayValue($xpath, '/ANSWER/GeocodeLevel');
        $Positions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Positions');
        $BoundingRects = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/BoundingRects');
        return array('Result' => $RESULT, 'Points' => $Points, 'GeocodeLevel' => $GeocodeLevel, 'Positions' => $Positions, 'BoundingRects' => $BoundingRects);
    }

    public function degeocode($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $MapPoints,
                              $QueryRadius, $DegeocodeLayers, $OnlyNamedEntries, $UseViewVisibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'Degeocode');
        $this->addTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->addRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->addTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->addTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->addTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->addTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->addTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->addArrayOfRecordsValue($querydoc, $root, 'MapPoints', $MapPoints);
        $this->addTextValue($querydoc, $root, 'QueryRadius', $QueryRadius);
        $this->addArrayValue($querydoc, $root, 'DegeocodeLayers', $DegeocodeLayers);
        $this->addTextValue($querydoc, $root, 'OnlyNamedEntries', $OnlyNamedEntries);
        $this->addTextValue($querydoc, $root, 'UseViewVisibility', $UseViewVisibility);
        // Post data

        $returndoc = $this->postQuery($querydoc);

        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Results = $this->parseArrayOfRecordsValue($xpath, '/ANSWER/Results');
        return array('Result'=>$RESULT, 'Results'=>$Results);
    }

    public function degeocodeAtPoint($SessionID, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams, $DegeocodePoint,
                                     $QueryRadius, $MaxElems, $DegeocodeLayers, $OnlyNamedEntries, $UseViewVisibility)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DegeocodeAtPoint');
        $this->addTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->addRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->addTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->addTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->addTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->addTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->addTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->addRecordValue($querydoc, $root, 'DegeocodePoint', $DegeocodePoint);
        $this->addTextValue($querydoc, $root, 'QueryRadius', $QueryRadius);
        $this->addTextValue($querydoc, $root, 'MaxElems', $MaxElems);
        $this->addArrayValue($querydoc, $root, 'DegeocodeLayers', $DegeocodeLayers);
        $this->addTextValue($querydoc, $root, 'OnlyNamedEntries', $OnlyNamedEntries);
        $this->addTextValue($querydoc, $root, 'UseViewVisibility', $UseViewVisibility);
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        $AreaName0 = $this->parseTextValue($xpath, '/ANSWER/AreaName0/text()');
        $AreaName1 = $this->parseTextValue($xpath, '/ANSWER/AreaName1/text()');
        $AreaName2 = $this->parseTextValue($xpath, '/ANSWER/AreaName2/text()');
        $AreaName3 = $this->parseTextValue($xpath, '/ANSWER/AreaName3/text()');
        $City = $this->parseArrayOfRecordsValue($xpath, '/ANSWER/City');
        $Zip = $this->parseTextValue($xpath, '/ANSWER/Zip/text()');
        $Road = $this->parseArrayOfRecordsValue($xpath, '/ANSWER/Road');
        $InternationalRoad = $this->parseArrayOfRecordsValue($xpath, '/ANSWER/InternationalRoad');
        $Street = $this->parseArrayOfRecordsValue($xpath, '/ANSWER/Street');
        $Natural = $this->parseArrayOfRecordsValue($xpath, '/ANSWER/Natural');
        $OtherMapElements = $this->parseArrayOfRecordsValue($xpath, '/ANSWER/OtherMapElements');
        $DatabaseElements = $this->parseArrayOfRecordsValue($xpath, '/ANSWER/DatabaseElements');
        return array('Result'=>$RESULT, 'AreaName0'=>$AreaName0, 'AreaName1'=>$AreaName1, 'AreaName2'=>$AreaName2, 'AreaName3'=>$AreaName3, 'City'=>$City,
            'Zip'=>$Zip, 'Road'=>$Road, 'InternationalRoad'=>$InternationalRoad, 'Street'=>$Street, 'Natural'=>$Natural, 'OtherMapElements'=>$OtherMapElements,
            'DatabaseElements'=>$DatabaseElements);
    }

    public function renderMapOnImageByPoint($SessionID, $MimeType, $MiddlePoint, $MapAltitude, $MapRotation, $MapTilt, $MapProjection, $MapProjectionParams,
                                            $BitmapWidth, $BitmapHeight, $VisibleLayers, $ImageRenderParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->addTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RenderMapOnImageByPoint');
        $this->addTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->addTextValue($querydoc, $root, 'MimeType', $MimeType);
        $this->addRecordValue($querydoc, $root, 'MiddlePoint', $MiddlePoint);
        $this->addTextValue($querydoc, $root, 'MapAltitude', $MapAltitude);
        $this->addTextValue($querydoc, $root, 'MapRotation', $MapRotation);
        $this->addTextValue($querydoc, $root, 'MapTilt', $MapTilt);
        $this->addTextValue($querydoc, $root, 'MapProjection', $MapProjection);
        $this->addTextValue($querydoc, $root, 'MapProjectionParams', $MapProjectionParams);
        $this->addTextValue($querydoc, $root, 'BitmapWidth', $BitmapWidth);
        $this->addTextValue($querydoc, $root, 'BitmapHeight', $BitmapHeight);
        $this->addArrayValue($querydoc, $root, 'VisibleLayers', $VisibleLayers);
        $this->addRecordValue($querydoc, $root, 'ImageRenderParams', $ImageRenderParams);
        // Post data
        $returndoc = $this->postQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->parseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BitmapImage = $this->parseBinaryValue($xpath, '/ANSWER/BitmapImage/text()');
        $BitmapRightUpCorner = $this->parseRecordValue($xpath, '/ANSWER/BitmapRightUpCorner');
        $BitmapRightDownCorner = $this->parseRecordValue($xpath, '/ANSWER/BitmapRightDownCorner');
        $BitmapLeftUpCorner = $this->parseRecordValue($xpath, '/ANSWER/BitmapLeftUpCorner');
        $BitmapLeftDownCorner = $this->parseRecordValue($xpath, '/ANSWER/BitmapLeftDownCorner');
        return array('Result'=>$RESULT, 'BitmapImage'=>$BitmapImage, 'BitmapRightUpCorner'=>$BitmapRightUpCorner, 'BitmapRightDownCorner'=>$BitmapRightDownCorner,
            'BitmapLeftUpCorner'=>$BitmapLeftUpCorner, 'BitmapLeftDownCorner'=>$BitmapLeftDownCorner);
    }

//  ---------------  PRIVATE --------------------

    private function addTextValue(DOMDocument $querydoc, DOMElement $root, $name, $value)
    {
        //echo "Adding $name = $value\n";
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        $val_node = $querydoc->createTextNode($value);
        $node->appendChild($val_node);
    }

    private function parseRecordValue(DOMXPath $xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0)
        {
            return null;
        }
        $returnval = array();
        $record = $items->item(0);
        for($i = 0; $i < $record->childNodes->length; $i++)
        {
            $item = $record->childNodes->item($i);
            $name = $item->nodeName;
            $value = $item->firstChild->nodeValue;
            $returnval[$name] = $value;
        }
        return $returnval;
    }

    private function parseTextValue(DOMXPath $xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0)
        {
            return NULL;
        }
        return $items->item(0)->nodeValue;
    }

    private function parseArrayValue(DOMXPath $xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0)
        {
            return NULL;
        }
        $returnval = array();
        $record = $items->item(0);
        for($i = 0; $i < $record->childNodes->length; $i++)
        {
            $item = $record->childNodes->item($i);
            $name = $item->nodeName;
            if (!$item->firstChild) {
                $returnval[] = '';
                continue;
            }
            $value = $item->firstChild->nodeValue;
            $returnval[] = $value;
        }
        return $returnval;
    }

    private function addRecordValue(DOMDocument $querydoc, DOMElement $root, $name, $array)
    {
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        foreach($array as $key => $value)
        {
            $subnode = $querydoc->createElement($key);
            $node->appendChild($subnode);
            $val_node = $querydoc->createTextNode($value);
            $subnode->appendChild($val_node);
        }
    }

    private function parseBinaryValue(DOMXPath $xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0)
        {
            return null;
        }
        return base64_decode($items->item(0)->nodeValue);
    }

    private function addArrayValue(DOMDocument $querydoc, DOMElement $root, $name, $array)
    {
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        foreach($array as $key => $value)
        {
            $subnode = $querydoc->createElement('ITEM');
            $node->appendChild($subnode);
            $val_node = $querydoc->createTextNode($value);
            $subnode->appendChild($val_node);
        }
    }

    private function addArrayOfRecordsValue(DOMDocument $querydoc, DOMElement $root, $name, $array)
    {
        $node = $querydoc->createElement($name);
        $root->appendChild($node);
        foreach($array as $key => $value)
            $this->addRecordValue($querydoc, $node, 'ITEM', $value);
    }

    private function parseArrayOfRecordsValue(DOMXPath $xpath, $path)
    {
        $items = $xpath->query($path);
        if ($items->length == 0)
        {
            return NULL;
        }
        $returnval = array();
        $array = $items->item(0);
        for($i = 0; $i < $array->childNodes->length; $i++)
        {
            $record = $array->childNodes->item($i);
            $entries = array();
            for($j = 0; $j < $record->childNodes->length; $j++)
            {
                $entry = $record->childNodes->item($j);
                $entries[$entry->nodeName] = $entry->nodeValue;
            }
            $returnval[] = $entries;
        }
        return $returnval;
    }

    private function postQuery(DOMDocument $querydoc, $SessionID = null, $debug = 0)
    {
        curl_setopt($this->curl_conn, CURLOPT_HEADER, 0);
        curl_setopt($this->curl_conn, CURLOPT_POST, 1);
        curl_setopt($this->curl_conn, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl_conn, CURLOPT_HEADER, false);
        curl_setopt($this->curl_conn, CURLOPT_HTTPHEADER, array("Content-Type: application/xml;charset=\"utf-8\""));

        curl_setopt ($this->curl_conn, CURLOPT_POSTFIELDS, $querydoc->saveXML());
        $xml_response = curl_exec ($this->curl_conn);

        $returndoc = new DOMDocument();

        if(!$returndoc->loadXML($xml_response))
        {
            echo "Error while parsing the document\n";
            exit;
        }
        return $returndoc;
    }

    public function createPoint($latitude, $longitude) : array
    {
        return ['Longitude' => $longitude, 'Latitude' => $latitude];
    }

    public function SearchGetParamValues($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetParamValues');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamNames = $this->ParseArrayValue($xpath, '/ANSWER/ParamNames');
        $ParamValues = $this->ParseArrayValue($xpath, '/ANSWER/ParamValues');
        return array('Result' => $RESULT, 'ParamNames' => $ParamNames, 'ParamValues' => $ParamValues);
    }

    public function LocalizeNumFieldsGetCount($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeNumFieldsGetCount');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Count = $this->ParseTextValue($xpath, '/ANSWER/Count/text()');
        return array('Result' => $RESULT, 'Count' => $Count);
    }


    public function LocalizeNumFieldsGet($SessionID, $Index)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'LocalizeNumFieldsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Index', $Index);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Name = $this->ParseTextValue($xpath, '/ANSWER/Name/text()');
        $ShowOnInfo = $this->ParseTextValue($xpath, '/ANSWER/ShowOnInfo/text()');
        return array('Result' => $RESULT, 'Name' => $Name, 'ShowOnInfo' => $ShowOnInfo);
    }

    public function DegeocodeGetParamValue($SessionID, $ParamName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'DegeocodeGetParamValue');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ParamName', $ParamName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ParamValue = $this->ParseTextValue($xpath, '/ANSWER/ParamValue/text()');
        return array('Result' => $RESULT, 'ParamValue' => $ParamValue);
    }


    public function SearchInitialize($SessionID, $ASCIISearch)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchInitialize');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ASCIISearch', $ASCIISearch);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetSessionLifetimeSeconds()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetSessionLifetimeSeconds');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SessionLifetime = $this->ParseTextValue($xpath, '/ANSWER/SessionLifetime/text()');
        return array('Result' => $RESULT, 'SessionLifetime' => $SessionLifetime);
    }

    public function AdminDropSession($SessionID, $SessionIP, $SessionUserName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AdminDropSession');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'SessionIP', $SessionIP);
        $this->AddTextValue($querydoc, $root, 'SessionUserName', $SessionUserName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetActiveSessions()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetActiveSessions');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $Sessions = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/Sessions');
        return array('Result' => $RESULT, 'Sessions' => $Sessions);
    }

    public function SearchGetCountryList($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetCountryList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CountryNames = $this->ParseArrayValue($xpath, '/ANSWER/CountryNames');
        return array('Result' => $RESULT, 'CountryNames' => $CountryNames);
    }

    public function SearchGetItemKindList($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetItemKindList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ItemKindNames = $this->ParseArrayValue($xpath, '/ANSWER/ItemKindNames');
        return array('Result' => $RESULT, 'ItemKindNames' => $ItemKindNames);
    }

    public function SearchSelectCities($SessionID, $Country, $Prefix, $ZIP, $Adm1, $Adm2, $Adm3)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSelectCities');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Country', $Country);
        $this->AddTextValue($querydoc, $root, 'Prefix', $Prefix);
        $this->AddTextValue($querydoc, $root, 'ZIP', $ZIP);
        $this->AddTextValue($querydoc, $root, 'Adm1', $Adm1);
        $this->AddTextValue($querydoc, $root, 'Adm2', $Adm2);
        $this->AddTextValue($querydoc, $root, 'Adm3', $Adm3);
        // Post data

        $returndoc = $this->PostQuery($querydoc, $SessionID);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ResultCount = $this->ParseTextValue($xpath, '/ANSWER/ResultCount/text()');
        return array('Result' => $RESULT, 'ResultCount' => $ResultCount);
    }


    public function SearchGetCityListEx($SessionID, $First, $Count)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetCityListEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'First', $First);
        $this->AddTextValue($querydoc, $root, 'Count', $Count);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CityNames = $this->ParseArrayValue($xpath, '/ANSWER/CityNames');
        $CityAdmAbbrev = $this->ParseArrayValue($xpath, '/ANSWER/CityAdmAbbrev');
        $CityAdmNames = $this->ParseArrayValue($xpath, '/ANSWER/CityAdmNames');
        return array('Result' => $RESULT, 'CityNames' => $CityNames, 'CityAdmAbbrev' => $CityAdmAbbrev, 'CityAdmNames' => $CityAdmNames);
    }

    public function SearchAddStreetWithNumToSelection($SessionID, $StreetNum, $Numeration)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchAddStreetWithNumToSelection');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'StreetNum', $StreetNum);
        $this->AddTextValue($querydoc, $root, 'Numeration', $Numeration);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $MidPoint = $this->ParseRecordValue($xpath, '/ANSWER/MidPoint');
        return array('Result' => $RESULT, 'MidPoint' => $MidPoint);
    }

    public function SearchSelectItems($SessionID, $CityIndex, $ItemKindIndex, $Prefix)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSelectItems');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'CityIndex', $CityIndex);
        $this->AddTextValue($querydoc, $root, 'ItemKindIndex', $ItemKindIndex);
        $this->AddTextValue($querydoc, $root, 'Prefix', $Prefix);
        // Post data
        $returndoc = $this->PostQuery($querydoc, null, 1);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ResultCount = $this->ParseTextValue($xpath, '/ANSWER/ResultCount/text()');
        return array('Result' => $RESULT, 'ResultCount' => $ResultCount);
    }

    public function SearchSelectItemsEx($SessionID, $Country, $ItemKindIndex, $Prefix)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSelectItemsEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'Country', $Country);
        $this->AddTextValue($querydoc, $root, 'ItemKindIndex', $ItemKindIndex);
        $this->AddTextValue($querydoc, $root, 'Prefix', $Prefix);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ResultCount = $this->ParseTextValue($xpath, '/ANSWER/ResultCount/text()');
        return array('Result' => $RESULT, 'ResultCount' => $ResultCount);
    }

    public function SearchGetItemsList($SessionID, $First, $Count)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetItemsList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'First', $First);
        $this->AddTextValue($querydoc, $root, 'Count', $Count);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ItemNames = $this->ParseArrayValue($xpath, '/ANSWER/ItemNames');
        $ItemTypes = $this->ParseArrayValue($xpath, '/ANSWER/ItemTypes');
        return array('Result' => $RESULT, 'ItemNames' => $ItemNames, 'ItemTypes' => $ItemTypes);
    }

    public function SearchAddCityToSelection($SessionID, $CityNum)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchAddCityToSelection');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'CityNum', $CityNum);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $MidPoint = $this->ParseRecordValue($xpath, '/ANSWER/MidPoint');
        return array('Result' => $RESULT, 'BoundingRect' => $BoundingRect, 'MidPoint' => $MidPoint);
    }
    public function SearchSetItemsFilter($SessionID, $ItemKinds)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchSetItemsFilter');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayValue($querydoc, $root, 'ItemKinds', $ItemKinds);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function SearchGetCityList($SessionID, $First, $Count)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SearchGetCityList');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'First', $First);//co to jest?
        $this->AddTextValue($querydoc, $root, 'Count', $Count);//i to
        // Post data

        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $CityNames = $this->ParseArrayValue($xpath, '/ANSWER/CityNames');
        //Niezaimplementowano dla CityAdmAbbrev
        //Niezaimplementowano dla CityAdmNames
        return array('Result' => $RESULT, 'CityNames' => $CityNames);
    }

    public function MultiGeocodeEx($SessionID, $ASCIISearch, $InputPoints, $MaxResultCount)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'MultiGeocodeEx');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ASCIISearch', $ASCIISearch);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'InputPoints', $InputPoints);
        $this->AddTextValue($querydoc, $root, 'MaxResultCount', $MaxResultCount);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $OutputPoints = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/OutputPoints');
        return array('Result' => $RESULT, 'OutputPoints' => $OutputPoints);
    }

    public function DoCitySearch($city)
    {
//        timerCity.Enabled = false;

        $cityCount = $this->SearchSelectCities(comboCountries.SelectedIndex, $city, "", "", "", "");

        $cities = [];
        $adm = [];
        $result = $this->SearchGetCityList(0, $cities, $adm);

//        listCities.Items.Clear();
//        $s = '';
//        for ($i = 0; $i < cities.length; $i++) {
//            $s = cities[$i];
//            foreach (string a in adm[i])
//            if (a != "") $s += " (" + a + ")";
//            listcities.items.add(s);
//        }
    }

    public function GetCurrentViewConfig($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetCurrentViewConfig');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ViewConfigName = $this->ParseTextValue($xpath, '/ANSWER/ViewConfigName/text()');
        return array('Result' => $RESULT, 'ViewConfigName' => $ViewConfigName);
    }

    public function AuthGetUsersList()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'AuthGetUsersList');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $UsersList = $this->ParseArrayValue($xpath, '/ANSWER/UsersList');
        return array('Result' => $RESULT, 'UsersList' => $UsersList);
    }
    public function GetServerVersion()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetServerVersion');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $ServerVersion = $this->ParseTextValue($xpath, '/ANSWER/ServerVersion/text()');
        $DataCompilationDate = $this->ParseArrayValue($xpath, '/ANSWER/DataCompilationDate');
        return array('Result' => $RESULT, 'ServerVersion' => $ServerVersion, 'DataCompilationDate' => $DataCompilationDate);
    }

    public function GetMaxSessionsCount()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetMaxSessionsCount');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SessionCount = $this->ParseTextValue($xpath, '/ANSWER/SessionCount/text()');
        return array('Result' => $RESULT, 'SessionCount' => $SessionCount);
    }

    public function GetCurrentSessionCount()
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetCurrentSessionCount');
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $SessionCount = $this->ParseTextValue($xpath, '/ANSWER/SessionCount/text()');
        return array('Result' => $RESULT, 'SessionCount' => $SessionCount);
    }

    public function SetSessionLanguageContext($SessionID, $ContextIndex, $LanguageName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'SetSessionLanguageContext');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ContextIndex', $ContextIndex);
        $this->AddTextValue($querydoc, $root, 'LanguageName', $LanguageName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function GetSessionLanguageContext($SessionID, $ContextIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'GetSessionLanguageContext');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ContextIndex', $ContextIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $LanguageName = $this->ParseTextValue($xpath, '/ANSWER/LanguageName/text()');
        return array('Result' => $RESULT, 'LanguageName' => $LanguageName);
    }


    public function ClearMapSelection($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ClearMapSelection');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function ClearSetFilter($SessionID, $TableName)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'ClearSetFilter');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'TableName', $TableName);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }


    /** ROUTING  */

    public function RoutePlannerEntryAdd($SessionID, $RoutePlanEntry, $RoutePlanVisitTime)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryAdd');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'RoutePlanEntry', $RoutePlanEntry);
        $this->AddTextValue($querydoc, $root, 'RoutePlanVisitTime', $RoutePlanVisitTime);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoutePlanEntryDescription = $this->ParseTextValue($xpath, '/ANSWER/RoutePlanEntryDescription/text()');
        return array('Result' => $RESULT, 'RoutePlanEntryDescription' => $RoutePlanEntryDescription);
    }

    public function RoutePlannerCalculateRoute($SessionID, $RouteCalculateType, $ReturnRoutePlanEntriesDescription, $ReturnRouteRepresentation,
                                               $ReturnRouteItinerary, $ReturnRoadResults, $StoreRouteInSession)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerCalculateRoute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'RouteCalculateType', $RouteCalculateType);
        $this->AddTextValue($querydoc, $root, 'ReturnRoutePlanEntriesDescription', $ReturnRoutePlanEntriesDescription);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteRepresentation', $ReturnRouteRepresentation);
        $this->AddTextValue($querydoc, $root, 'ReturnRouteItinerary', $ReturnRouteItinerary);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        $this->AddTextValue($querydoc, $root, 'StoreRouteInSession', $StoreRouteInSession);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        $RoutePlanEntriesDescription = $this->ParseArrayValue($xpath, '/ANSWER/RoutePlanEntriesDescription');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $UnreachableEntry = $this->ParseTextValue($xpath, '/ANSWER/UnreachableEntry/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'RouteRepresentation' => $RouteRepresentation, 'RouteItinerary' => $RouteItinerary, 'RoutePlanEntriesDescription' => $RoutePlanEntriesDescription,
            'BoundingRect' => $BoundingRect, 'UnreachableEntry' => $UnreachableEntry);
    }

    public function RoutePlannerDriverParamsSet($SessionID, $DriverParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerDriverParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'DriverParams', $DriverParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerRoadParamsGet($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRoadParamsGet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoadParams = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadParams');
        return array('Result' => $RESULT, 'RoadParams' => $RoadParams);
    }

    public function RoutePlannerVehicleParamsSet($SessionID, $VehicleParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerVehicleParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'VehicleParams', $VehicleParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerRoadParamsSet($SessionID, $RoadParams)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRoadParamsSet');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddArrayOfRecordsValue($querydoc, $root, 'RoadParams', $RoadParams);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerGetAvailableRoadTypes($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetAvailableRoadTypes');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RoadTypes = $this->ParseArrayValue($xpath, '/ANSWER/RoadTypes');
        return array('Result' => $RESULT, 'RoadTypes' => $RoadTypes);
    }

    public function RoutePlannerGetRouteItinerary($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteItinerary');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteItinerary = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteItinerary');
        return array('Result' => $RESULT, 'RouteItinerary' => $RouteItinerary);
    }

    public function RoutePlannerRouteClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerRouteClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerEntriesClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntriesClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteRepositoryClear($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteRepositoryClear');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RouteAttributesClearRouteAttribute($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RouteAttributesClearRouteAttribute');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerGetRouteSummary($SessionID, $ReturnRoadResults)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteSummary');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'ReturnRoadResults', $ReturnRoadResults);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $TotalRouteLength = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteLength/text()');
        $TotalRouteTime = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteTime/text()');
        $TotalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/TotalRouteCost/text()');
        $TotalFuelCost = $this->ParseTextValue($xpath, '/ANSWER/TotalFuelCost/text()');
        $RoadResults = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RoadResults');
        $TollRoadLength = $this->ParseTextValue($xpath, '/ANSWER/TollRoadLength/text()');
        $TollRoadTime = $this->ParseTextValue($xpath, '/ANSWER/TollRoadTime/text()');
        $AdditionalRouteCost = $this->ParseTextValue($xpath, '/ANSWER/AdditionalRouteCost/text()');
        $BoundingRect = $this->ParseRecordValue($xpath, '/ANSWER/BoundingRect');
        $RouteCalculateType = $this->ParseTextValue($xpath, '/ANSWER/RouteCalculateType/text()');
        return array('Result' => $RESULT, 'TotalRouteLength' => $TotalRouteLength, 'TotalRouteTime' => $TotalRouteTime, 'TotalRouteCost' => $TotalRouteCost,
            'TotalFuelCost' => $TotalFuelCost,
            'RoadResults' => $RoadResults, 'TollRoadLength' => $TollRoadLength, 'TollRoadTime' => $TollRoadTime, 'AdditionalRouteCost' => $AdditionalRouteCost,
            'BoundingRect' => $BoundingRect, 'RouteCalculateType' => $RouteCalculateType);
    }

    public function RoutePlannerEntryRemove($SessionID, $EntryIndex)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryRemove');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddTextValue($querydoc, $root, 'EntryIndex', $EntryIndex);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

    public function RoutePlannerGetRouteRepresentation($SessionID)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerGetRouteRepresentation');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        $RouteRepresentation = $this->ParseArrayOfRecordsValue($xpath, '/ANSWER/RouteRepresentation');
        return array('Result' => $RESULT, 'RouteRepresentation' => $RouteRepresentation);
    }

    public function RoutePlannerEntryAddWithDescription($SessionID, $RoutePlanEntry, $RoutePlanVisitTime, $RoutePlanEntryDescription)
    {
        $querydoc = new DOMDocument('1.0', 'utf-8');
        $root = $querydoc->createElement('QUERY');
        $querydoc->appendChild($root);
        $this->AddTextValue($querydoc, $root, 'MC_QUERY_NAME', 'RoutePlannerEntryAddWithDescription');
        $this->AddTextValue($querydoc, $root, 'SessionID', $SessionID);
        $this->AddRecordValue($querydoc, $root, 'RoutePlanEntry', $RoutePlanEntry);
        $this->AddTextValue($querydoc, $root, 'RoutePlanVisitTime', $RoutePlanVisitTime);
        $this->AddTextValue($querydoc, $root, 'RoutePlanEntryDescription', $RoutePlanEntryDescription);
        // Post data
        $returndoc = $this->PostQuery($querydoc);
        $xpath = new DOMXPath($returndoc);
        $RESULT = $this->ParseTextValue($xpath, '/ANSWER/RESULT/text()');
        return array('Result' => $RESULT);
    }

}
?>
