<?php

namespace TelephonyStatsBundle\Controller;

use Elastica\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use MssqlBundle\PDO\PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {


        return $this->render('TelephonyStatsBundle:Default:index.html.twig');
    }


    /**
     * @Rest\Route("/ajax", name="telephony-public-refresh", options={"expose":true})
     */
    public function ajaxTelephonyStatsRefresh()
    {
        $cfm = $this->getStats('CFM');
        $front = $this->getStats('FRONT');
        $adac = $this->getStats('ADAC');

        $params = [
            'cfm' =>
                $cfm,
            'front' => $front,
            'adac' => $adac,
            'update' => 1
        ];
        $forceCatShow = false;
        $catUrl= $this->getDoctrine()->getRepository('AppBundle:CustomContent')->findOneBy([
            'key' => 'cat_url'
        ]);
        if(is_null($catUrl))
        {
            $catUrl = 'https://static.wixstatic.com/media/2cd43b_baec20d09b1f4d41bd84fcf014019f20~mv2.png/v1/fill/w_320,h_381,fp_0.50_0.50/2cd43b_baec20d09b1f4d41bd84fcf014019f20~mv2.png';
        }else{
            $forceCatShow = $catUrl->getValue()??false;
            $catUrl = $catUrl->getWebUrl();
        }

        $params['cat'] = $catUrl;
        $params['forceCatShow'] = $forceCatShow;

        $tabContent = $this->get('twig')->render('TelephonyStatsBundle:Default:tab_ajax.html.twig', $params);
        $response = new JsonResponse(['html' => $tabContent]);
        return $response;
    }

    /**
     * @Rest\Route("/ajax-breaks", name="telephony-breaks-public-refresh", options={"expose":true})
     */
    public function ajaxBreakStatsRefresh()
    {
        $pzuStats = $this->getPZUStats();
        $nhStats = $this->getNHStats();
        $breakStats =$this->getStatsOnBreak();


        $tabContent = $this->get('twig')->render('TelephonyStatsBundle:Default:tab_bottom_ajax.html.twig',array_merge(['pzu'=>$pzuStats, 'nhs'=>$nhStats],$breakStats));
        return new JsonResponse(['html' => $tabContent]);
    }


    private function getStats($tabName)
    {


        $result = $this->get('app.query_manager')
            ->executeProcedure("SELECT statPersons.name, lower(replace(statPersons.name,' ','_')) slug,value,type, iif(panelConfig.condition is not null , value+panelConfig.condition ,null) cond
from statpersons join panelConfig on statpersons.tabName = panelConfig.tabname  and statpersons.name = panelConfig.keyName
where  panelConfig.tabName = :tabname
ORDER BY panelConfig.orderBy asc",
                [
                    [
                        'key' => 'tabname',
                        'value' => $tabName,
                        'type' => PDO::PARAM_STR
                    ]
                ]
            );
        $finalResults = [];
        foreach ($result as $singleResult) {
            if (!is_null($singleResult['cond'])) {


                $cond = preg_replace("/([^0-9,\-,<,>,=])/", "", $singleResult['cond']);

                if (!is_null($cond) && strlen($cond) > 0) {
                    $singleResult['cond'] = eval('return ' . $cond . ';');

                } else {
                    $singleResult['cond'] = false;
                }

            } else {
                $singleResult['cond'] = false;
            }


            $finalResults[] = $singleResult;
        }

        return $finalResults;
    }

    private function getPZUStats()
    {
        $activeTasksCount = $this->get('app.query_manager')
            ->executeProcedure("SELECT count(id) active from process_instance with (nolock) where step_id = '1011.102' and active = 1");

        $longestAwaitingTask = $this->get('app.query_manager')
            ->executeProcedure("SELECT top 1 datediff(second , created_at, GETDATE()) second_diff from process_instance  with (nolock) where step_id = '1011.102' and active = 1 order by id asc");


        if ($activeTasksCount && is_array($activeTasksCount)) {
            $activeTasksCount = $activeTasksCount[0]['active'];
        } else {
            $activeTasksCount = 0;
        }


        if ($longestAwaitingTask && is_array($longestAwaitingTask)) {
            $longestAwaitingTask = $longestAwaitingTask[0]['second_diff'];
        } else {
            $longestAwaitingTask = 0;
        }

        if( $longestAwaitingTask > 0 ){
            $formattedString= intval($longestAwaitingTask/60) .':'.strval($longestAwaitingTask%60);
        }else{
            $formattedString = '' ;
        }

        return [
            'active_count' => $activeTasksCount,
            'longest_awaiting_task' => $longestAwaitingTask,
            'longest_awaiting_task_formatted' => $formattedString
        ];


    }

    /**
     * @return array
     */
    public function getNHStats()
    {
        try {
            $sr = $this->get('app.query_manager')
                ->executeProcedure("SELECT 'SR - BACK' name, lower('sr_srednia') slug,left(avg(cast(value as decimal(5,2))),4) value ,1 type  from statPersons with (nolock )
where  statPersons.tabName = 'BACK' and (statPersons.name in ('SR Wysyłka', 'SR Back','SR Zamknięcia'))"
                );

            $result = $this->get('app.query_manager')
                ->executeProcedure("select name, value from statPersons where tabName = 'back' and name in (
    'Oferowane', 'Odebrane'
    ) order by name asc");
//                ->executeProcedure("SELECT presented, handled from statQueues with (nolock )
//where  statQueues.tabName = 'BACK' and (statQueues.csqname in ('BACK1_WYSYLKA'))"
//                );




            $final = [];
            if(count($result) >0) {

                foreach ($result as $single) {

                    $final[] = [
                        'name' => $single['name'],
                        'slug' => $single['name'],
                        'value' => $single['value'],
                        'type' => 2
                    ];
                }
            }
            $final = array_merge($sr, $final);


        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
            $final = [];
        }
        return $final;
    }


    /** Break Stats */
    private function getStatsOnBreak()
    {

        $result = $this->get('app.query_manager')
            ->executeProcedure("SELECT count(distinct name) as count  from statOperators with (nolock) where
    name  like '%vv%' and statOperators.reasoncode like '%break%'");
        $vv = $result[0]['count'];

        $result = $this->get('app.query_manager')
            ->executeProcedure("SELECT count(distinct name) as count from statOperators with (nolock) where
    name not  like '%vv%' and statOperators.reasoncode like '%break%'");
        $cz = $result[0]['count'];
        return
            [
                'cz' => $cz,
                'vv' => $vv
            ];
    }
}
