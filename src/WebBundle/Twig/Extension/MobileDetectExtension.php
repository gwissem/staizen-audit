<?php

namespace WebBundle\Twig\Extension;

use Bes\Twig\Extension\MobileDetectExtension as MobileDetectExtensionBase;

class MobileDetectExtension extends MobileDetectExtensionBase
{

    /**
     * Pass through calls of undefined methods to the mobile detect library
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (substr($name, 0, 4) == 'isis') {
            $name = substr($name, 2);
        }

        return parent::__call($name, $arguments);

    }

}


