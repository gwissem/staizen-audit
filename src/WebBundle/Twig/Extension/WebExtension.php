<?php
namespace WebBundle\Twig\Extension;

use AppBundle\Entity\Config;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\AttributeParserService;
use DocumentBundle\Entity\File;
use MapBundle\Controller\DefaultController;
use stdClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Stopwatch\Stopwatch;
use ToolBundle\Entity\FormField;
use ToolBundle\Entity\Style;
use Twig_Environment;
use Twig_Function_Method;
use Twig_SimpleFunction;
use UserBundle\Entity\User;
use UserBundle\Service\UserInfoService;
use WebBundle\Service\CachedMenuService;

class WebExtension extends \Twig_Extension
{

    /** @var UserInfoService  */
    protected $userInfo;

    /** @var  CachedMenuService */
    protected $cachedMenuService;

    /** @var \Doctrine\ORM\EntityManager  */
    private $em;

    /** @var ContainerInterface  */
    private $container;

    private $isWindow;

    /** @var AttributeParserService */
    private $attributeParser;

    public function __construct(\Doctrine\ORM\EntityManager $em, UserInfoService $userInfo, CachedMenuService $cachedMenuService, ContainerInterface $container)
    {
        $this->em = $em;
        $this->userInfo = $userInfo;
        $this->cachedMenuService = $cachedMenuService;
        $this->container = $container;

        $this->isWindow = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? true: false;

    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('orderFormFieldsOfTool', array($this, 'orderFormFieldsOfTool')),
            new \Twig_SimpleFilter('gmdate', [$this, 'gmdate']),
            new \Twig_SimpleFilter('incrementByOne', [$this, 'incrementByOne']),
            new \Twig_SimpleFilter('removeAppDev', [$this, 'removeAppDev']),
            new \Twig_SimpleFilter('removeDoubleApostrophe', [$this, 'removeDoubleApostrophe']),
            new \Twig_SimpleFilter('removeNewLine', [$this, 'removeNewLine']),
            new \Twig_SimpleFilter('parseToolSrc', [$this, 'parseToolSrc']),
            new \Twig_SimpleFilter('dateOnly', [$this, 'dateOnly']),
            new \Twig_SimpleFilter('dateTimeOnly', [$this, 'dateTimeOnly']),
            new \Twig_SimpleFilter('parseIframeSrcParameters', [$this, 'parseIframeSrcParameters']),
            new \Twig_SimpleFilter('humanTime', [$this, 'humanTime']),
            new \Twig_SimpleFilter('dimensions_format', [$this, 'dimensionsFormat']),
            new \Twig_SimpleFilter('iconFromExtension', [$this, 'displayIconFromExtension']),
            new \Twig_SimpleFilter('json_decode', [$this, 'jsonDecode'])
        ];
    }

    public function displayIconFromExtension($extension) {

        switch ($extension) {
            case 'doc':
            case 'docx': return 'file-word-o';
            case 'pdf': return 'file-pdf-o';
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'png': return 'file-image-o';
            case 'xlsx':
            case 'xls': return 'file-excel-o';
            case 'txt': return 'file-text-o';
            case 'rar':
            case 'zip': return 'file-zip-o';
            default: return 'file-o';
        }

//         Example of file-archive-o
//         Example of file-audio-o
//         Example of file-code-o
//         Example of file-movie-o (alias)
//         Example of file-powerpoint-o
//         Example of file-sound-o (alias)
//         Example of file-video-o


    }


    public function dimensionsFormat($amount) {
        return ((float)$amount + 0) . 'm';
    }

    public function drawAgentState($state, $label = null) {

        $color = 'warning';

        switch ($state) {
            case 'NOT_READY': {
                $color = 'danger';
                break;
            }
            case 'TALKING': {
                $color = 'primary';
                break;
            }
            case 'READY': {
                $color = 'success';
                break;
            }
            case 'LOGOUT': {
                $color = 'default';
                break;
            }
        }

        $text = ($label !== null) ? $label : $state;

        $html = '<span class="label label-'.$color.'">'.$text.'</span>';

        return $html;
    }

    public function humanTime($seconds) {

        $seconds = intval($seconds);

        if(floor($seconds/60) == 0) {
            return (($seconds % 60) . "s");
        }

        if(floor($seconds/3600) == 0) {
            return (floor($seconds / 60) . "min " . ($seconds % 60) . "s");
        }

        return (floor($seconds/3600) . "h " . floor($seconds / 60) % 60 . "min " . ($seconds % 60) . "s");

    }

    public function removeNewLine($text) {
        return str_replace(["\r\n", "\r", "\n"], "<br/>", $text);
    }

    /**
     * Parse mixed date and return only date part
     *
     * @param $date
     * @return string
     */
    public function dateOnly($date)
    {

        if(!$date){
            return NULL;
        }
        elseif(!($date instanceof \DateTime)){
            $date = new \DateTime($date);
        }

        return $date->format('Y-m-d');
    }

    /**
     * Parse mixed dateTime and return only dateTime part
     *
     * @param $dateTime
     * @return string
     */
    public function dateTimeOnly($dateTime)
    {

        if(!$dateTime){
            return NULL;
        }
        elseif(!($dateTime instanceof \DateTime)){
            $dateTime = new \DateTime($dateTime);
        }

        return $dateTime->format('Y-m-d H:i');
    }

    public function parseToolSrc($text, $groupProcessId = NULL, $decode = false)
    {
        $parsed = ($decode) ? base64_decode($text) : $text;
        if ($groupProcessId) {
            $attributeParser = $this->container->get('case.attribute_parser.service');
            $attributeParser->setGroupProcessInstanceId($groupProcessId);
            $parsed = $attributeParser->parseString($parsed);
        }

        return $parsed;
    }

    public function parseAttribute($string, $groupProcessId, $suffix = "") {

        if($this->attributeParser === null) {
            $this->attributeParser = $this->container->get('case.attribute_parser.service');
        }

        $this->attributeParser->setGroupProcessInstanceId($groupProcessId);
        $parsed = $this->attributeParser->parseString($string, false);

        if($parsed === '""') {
            return null;
        }

        return $parsed . $suffix;

    }

    public function getOrganisationRequestNotes($groupProcessInstanceId, $serviceId){
        return $this->container->get('case.handler')->getCrossBorderGopRemarks($groupProcessInstanceId, $serviceId);
    }

    public function parseIframeSrcParameters($text, $groupProcessId) {
        $iFrameSrcParser = $this->container->get('case.iframe_src_parser.service');

        return $iFrameSrcParser->setGroupProcessInstanceId($groupProcessId)
            ->parse($text);
    }

    public function removeDoubleApostrophe($text) {

        return str_replace("\"", "'", $text);

    }

    public function incrementByOne($value, $secure = false) {
        if(!$secure) return $value;
        return (intval($value) + 1);
    }

    public function removeAppDev($url) {

        if ($this->isWindow) {
            return str_replace('app_dev.php/', '', $url);
        }

        return $url;
    }

    public function getFunctions()
    {
        return [
            'fetchUserTools' => new \Twig_SimpleFunction('fetchUserTools', [$this, 'fetchUserTools']),
            'isStylable' => new \Twig_SimpleFunction('isStylable', [$this, 'isStylable']),
            'parseLinkForColumnParams' => new Twig_SimpleFunction('parseLinkForColumnParams', [$this, 'parseLinkForColumnParams']),
            'parseForColumnParam' => new \Twig_SimpleFunction('parseForColumnParam', [$this, 'parseForColumnParam']),
            'availablePermissionTransfers' => new \Twig_SimpleFunction('availablePermissionTransfers', [$this, 'availablePermissionTransfers']),
            'impersonateComeback' => new \Twig_SimpleFunction('impersonateComeback', [$this, 'impersonateComeback']),
            'getColumnName' => new \Twig_SimpleFunction('getColumnName', [$this, 'getColumnName']),
            'renderCachedMenu' => new \Twig_SimpleFunction('renderCachedMenu', [$this, 'renderCachedMenu']),
            'displayScreenShoot' => new \Twig_SimpleFunction('displayScreenShoot', [$this, 'displayScreenShoot']),
            'projectVersion' => new \Twig_SimpleFunction('projectVersion', [$this, 'projectVersion']),
            'getConfigScenarios' => new \Twig_SimpleFunction('getConfigScenarios', [$this, 'getConfigScenarios']),
            'isImage' => new \Twig_SimpleFunction('isImage', [$this, 'isImage']),
            'getEmailAvatar' => new \Twig_SimpleFunction('getEmailAvatar', [$this, 'getEmailAvatar']),
            'parseAttribute' => new \Twig_SimpleFunction('parseAttribute', [$this, 'parseAttribute']),
            'getFilesOfDocument' => new \Twig_SimpleFunction('getFilesOfDocument', [$this, 'getFilesOfDocument']),
            'getAmountUsersInCase' => new \Twig_SimpleFunction('getAmountUsersInCase', [$this, 'getAmountUsersInCase']),
            'getStaticProperty' => new \Twig_SimpleFunction('getStaticProperty', [$this, 'getStaticProperty']),
            'callStaticMethod' => new \Twig_SimpleFunction('callStaticMethod', [$this, 'callStaticMethod']),
            'drawAgentState' => new \Twig_SimpleFunction('drawAgentState', [$this, 'drawAgentState']),
            'getOrganisationRequestNotes' => new \Twig_SimpleFunction('getOrganisationRequestNotes', [$this, 'getOrganisationRequestNotes']),
            'getBusinessConfig' => new \Twig_SimpleFunction('getBusinessConfig', [$this, 'getBusinessConfig']),
            'getPartnerLogo' => new \Twig_SimpleFunction('getPartnerLogo', [$this, 'getPartnerLogo']),
            'getFormEditUrl' => new \Twig_SimpleFunction('getFormEditUrl', [$this, 'getFormEditUrl']),
            'getPlatformId' => new \Twig_SimpleFunction('getPlatformId', [$this, 'getPlatformId']),
            'noAutocomplete' => new \Twig_SimpleFunction('noAutocomplete', [$this, 'noAutocomplete']),
            'getCountryForMap' => new \Twig_SimpleFunction('getCountryForMap', [$this, 'getCountryForMap']),
            'addPhonePrefix' => new \Twig_SimpleFunction('addPhonePrefix', [$this, 'addPhonePrefix'])
        ];
    }

    /**
     * Trzeba to poprawić... pobierać nazwy krajów z EMAPI wykorzystująć ->SearchGetCountryList();
     */
    public function getCountryForMap() {

        // Przez to, że kod mapy nie jest w serwisie, to takie brzydkie rozwiązanie...
//      // Ale i tak zły IDki zwraca
//        $defaultMapController = new DefaultController();
//        $defaultMapController->setContainer($this->container);


        $countries = [
            "0" => "Wszystkie",
            "1" => "Afganistan",
            "2" => "Albania",
            "3" => "Andora",
            "4" => "Arabia Saudyjska",
            "5" => "Armenia",
            "6" => "Austria",
            "7" => "Azerbejdżan",
            "8" => "Bahrajn",
            "9" => "Bangladesz",
            "10" => "Belgia",
            "11" => "Bhutan",
            "12" => "Białoruś",
            "13" => "Birma",
            "14" => "Bośnia i Hercegowina",
            "15" => "Brunei",
            "16" => "Bułgaria",
            "17" => "Chiny",
            "18" => "Chorwacja",
            "19" => "Cypr",
            "20" => "Czarnogóra",
            "21" => "Czechy",
            "22" => "Dania",
            "23" => "Estonia",
            "24" => "Filipiny",
            "25" => "Finlandia",
            "26" => "Francja",
            "27" => "Gibraltar",
            "28" => "Grecja",
            "29" => "Gruzja",
            "30" => "Guam",
            "31" => "Hiszpania",
            "32" => "Holandia",
            "33" => "Hong Kong",
            "34" => "Indie",
            "35" => "Indonezja",
            "36" => "Irak",
            "37" => "Iran",
            "38" => "Irlandia",
            "39" => "Islandia",
            "40" => "Izrael",
            "41" => "Japonia",
            "42" => "Jemen",
            "43" => "Jordania",
            "44" => "Kambodża",
            "45" => "Katar",
            "46" => "Kazachstan",
            "47" => "Kirgistan",
            "48" => "Korea Południowa",
            "49" => "Korea Północna",
            "50" => "Kosowo",
            "51" => "Kuwejt",
            "52" => "Laos",
            "53" => "Liban",
            "54" => "Liechtenstein",
            "55" => "Litwa",
            "56" => "Luksemburg",
            "57" => "Łotwa",
            "58" => "Macedonia",
            "59" => "Makau",
            "60" => "Malezja",
            "61" => "Malta",
            "62" => "Mikronezja",
            "63" => "Mołdawia",
            "64" => "Monako",
            "65" => "Mongolia",
            "66" => "Nepal",
            "67" => "Niemcy",
            "68" => "Norwegia",
            "69" => "Oman",
            "70" => "Pakistan",
            "71" => "Palau",
            "72" => "Polska",
            "73" => "Portugalia",
            "74" => "Północne Mariany",
            "75" => "Rosja",
            "76" => "Rumunia",
            "77" => "San Marino",
            "78" => "Serbia",
            "79" => "Singapur",
            "80" => "Słowacja",
            "81" => "Słowenia",
            "82" => "Sri Lanka",
            "83" => "Syria",
            "84" => "Szwajcaria",
            "85" => "Szwecja",
            "86" => "Tadżykistan",
            "87" => "Tajlandia",
            "88" => "Tajwan",
            "89" => "Turcja",
            "90" => "Turkmenistan",
            "91" => "Ukraina",
            "92" => "Uzbekistan",
            "93" => "Watykan",
            "94" => "Węgry",
            "95" => "Wielka Brytania",
            "96" => "Wietnam",
            "97" => "Włochy",
            "98" => "Wyspa Bożego Narodzenia",
            "99" => "Zjednoczone Emiraty Arabskie",

        ];

        $firstCountryId = $this->getFirstCountry();

        $qm = $this->container->get('app.query_manager');

        foreach ($countries as $key => $country) {
            $countries[$key] = $qm->translate($country);
        }

        $first = null;
        if(isset($countries[$firstCountryId])) {
            $first = [
                'value' => $firstCountryId,
                'name' => $countries[$firstCountryId]
            ];
            unset($countries[$firstCountryId]);
        }

        return [
            'first' => $first,
            'countries' => $countries
            ];

    }

    /**
     * @return int
     */
    private function getFirstCountry() {

        /** @var Config $configLocale */
        $configLocale = $this->container->get('doctrine')->getRepository('AppBundle:Config')->findOneBy([
            'key' => 'application_locale'
        ]);

        $idCountry = 0;

        if($configLocale) {

            switch ($configLocale->getValue()) {
                case 'ru': {
                    $idCountry = 75;
                    break;
                }
                case 'pl' : {
                    $idCountry = 72;
                }
            }

        }

        return $idCountry;

    }

    public function callStaticMethod($class, $method, array $args = [])
    {
        $refl = new \reflectionClass($class);

        // Check that method is static AND public
        if ($refl->hasMethod($method) && $refl->getMethod($method)->isStatic() && $refl->getMethod($method)->isPublic()) {
            return call_user_func_array($class.'::'.$method, $args);
        }

        throw new \RuntimeException(sprintf('Invalid static method call for class %s and method %s', $class, $method));
    }

    public function getStaticProperty($class, $property)
    {
        $refl = new \reflectionClass($class);

        // Check that property is static AND public
        if ($refl->hasProperty($property) && $refl->getProperty($property)->isStatic() && $refl->getProperty($property)->isPublic()) {
            return $refl->getProperty($property)->getValue();
        }

        throw new \RuntimeException(sprintf('Invalid static property get for class %s and property %s', $class, $property));
    }

    public function getAmountUsersInCase($rootId) {

        return $this->container->get('api_task.service')->amountUsersInCase($rootId);

    }

    public function getFilesOfDocument($documentId) {
        if(empty($documentId)) return '';

        $document = $this->em->getRepository('DocumentBundle:Document')->find(intval($documentId));

        if($document) {
            $files = [];

            /** @var File $file */
            foreach ($document->getFiles() as $file) {
                $files[] = [
                    'id' => $file->getId(),
                    'uuid' => $file->getUuid(),
                    'name' => $file->getName(),
                    'size' => $file->getFileSize()
                ];
            }

            if(count($files) > 0 ) {
                return \GuzzleHttp\json_encode($files);
            }

        }

        return "";
    }

    private $allowedExtensions = array("gif", "jpeg", "jpg", "png");

    public function getEmailAvatar($address) {

        if(!empty($address)) {

            $finder = new Finder();
            $parsedAddress = preg_replace('/[@\.]/', '_', $address);
            $avatarsDir = $this->container->get('kernel')->getRootDir() . '/../web/images/email_avatar';
            $finder->files()->in($avatarsDir)->name('*'.$parsedAddress.'*');

            if($finder->count()) {
                return 'images/email_avatar/' . $finder->getIterator()->getBasename();
            }
        }

        return 'cometchat_lib/images/noavatar.png';

    }

    public function isImage($extension) {
        return (in_array($extension, $this->allowedExtensions));
    }

    public function getConfigScenarios() {

        $rootDir = $this->container->get('kernel')->getRootDir();
        $rootToScenarios = $rootDir . '/Resources/scenarios';

        $finder = new Finder();
        $finder->files()->in($rootToScenarios);

        $result = [];

        foreach ($finder as $file) {
            $result[] = $file->getBasename('.' . $file->getExtension());
        }

        return $result;

    }

    public function projectVersion() {
        $result = $this->container->get('app.query_manager')->executeProcedure(
            "SELECT
           max(r.restore_date) date
           FROM master.sys.databases d
           LEFT OUTER JOIN msdb.dbo.restorehistory r ON r.destination_database_name = d.Name
            where d.name='" . $this->container->getParameter('mssql_database_name') . "'");

        $dbRealeaseDate = date('Y-m-d H:i', strtotime($result[0]['date']));

        $parametersPath = $this->container->getParameter('kernel.root_dir') . '/config/parameters.yml';
        if (is_link($parametersPath)) {
            $parametersPath = readLink($parametersPath);
        }
        $codeReaseDate = date('Y-m-d H:i', filemtime($parametersPath));

        return $this->container->get('translator')->trans('release db: %dbRealeaseDate%, code: %codeReleaseDate%', [
            '%dbRealeaseDate%' => $dbRealeaseDate,
            '%codeReleaseDate%' => $codeReaseDate
        ]);
    }

    public function displayScreenShoot($path) {
        return '/files' . $path;
    }

    public function renderCachedMenu($name, $options) {

        return $this->cachedMenuService->renderMenu($name, $options);
    }

    public function getColumnName(FormField $field)
    {
        $column = $field->getColumnName();
        $translateColumn = $column ;

        $stylesOfColumn = $field->getTool()->getStyles()->filter(/**
         * @param Style $entry
         */
            function($entry) use ($column) {
                return $entry->getColumnName() == $column;
            })->first();

        if($stylesOfColumn instanceof Style) {
            if($stylesOfColumn->getAlias()) $translateColumn = $stylesOfColumn->getAlias();
        }

        return $translateColumn;
    }

    public function impersonateComeback()
    {
        $info = $this->userInfo->getInfo();
        if (!empty($info) && $info['userId'] !== $info['originalUserId']) {
            return $this->userInfo->getOriginalUser();
        }

        return null;
    }

    /**
     * @param User $user
     * @return array
     */
    public function availablePermissionTransfers($user)
    {
        return $this->em->getRepository('UserBundle:PermissionTransfer')->availablePermissionTransfers($user);
    }

    public function gmdate($seconds, $format)
    {
        return gmdate($format, $seconds);
    }

    /**
     * @param FormField[] $fields
     * @param bool $displayRowZero
     * @param bool $orderBySection
     * @return array
     */
    public function orderFormFieldsOfTool($fields, $displayRowZero = true, $orderBySection = false)
    {
        $rows = [];

        foreach ($fields as $field) {

            if(!$field->getRowNumber()){

                if($displayRowZero) {
                    if(!isset($rows[0])){
                        $rows[0] = [];
                    }
                    $rows[0][] = $field;
                }
                continue;
            }

            if(!isset($rows[$field->getRowNumber()])){
                $rows[$field->getRowNumber()] = [];
            }

            $rows[$field->getRowNumber()][] = $field;
        }

        ksort($rows);

        foreach ($rows as $key => $row) {
            usort($rows[$key], array($this, "usortFormFields"));
        }

        if($orderBySection) {

            $sections = [];
            $sections[0] = [
                'tag' => 'other-tools',
                'name' => '',
                'rows' => []
            ];

            foreach ($rows as $key => $row) {

                if($displayRowZero && $key == 0) {
                    $sections[0]['rows'] = array($row);
                    continue;
                }

                $sectionRow = $row[0]->getSectionRow();

                if($sectionRow) {
                    $index = $this->searchIndexOfSection($sections, $sectionRow['tag']);

                    if($index !== false) {
                        $sections[$index]['rows'][] = $row;
                    } else {
                        $sections[] = [
                            'tag' => $sectionRow['tag'],
                            'name' => $sectionRow['name'],
                            'rows' => array($row)
                        ];
                    }

                } else {
                    $sections[] = [
                        'tag' => '',
                        'name' => '',
                        'rows' => array($row)
                    ];
                }
            }

            return $sections;
        }

        return $rows;
    }

    private function searchIndexOfSection($sections, $tag)
    {
        $key = array_search($tag, array_column($sections, 'tag'));
        return $key;
    }

    function parseForColumnParam($string, $resultRow, $returnString = false)
    {
        $params = $this->getStringBetween('{@', '@}', $string);

        $column = empty($params) ? '' : $params[0];
        if (!empty($column)) {

            if (array_key_exists($column, $resultRow)) {
                return $resultRow[$column];
            }
        }

        if ($returnString) {
            return $string;
        }

        return '';
    }

    private function getStringBetween($start, $end, $str)
    {
        if ($start === $end) {
            $string = explode($start, $str, 3);

            return isset($string[1]) ? $string[1] : '';
        }

        $matches = [];
        $regex = "/$start([a-zA-Z0-9_]*)$end/";
        preg_match_all($regex, $str, $matches);

        return $matches[1];
    }

    function parseLinkForColumnParams($link, $resultRow, $env = 'prod')
    {
        $params = $this->getStringBetween('{@', '@}', $link);
        foreach ($params as $param) {
            if (array_key_exists($param, $resultRow)) {
                $link = str_replace('{@'.$param.'@}', $resultRow[$param], $link);
            }
        }

        if (!$link) {
            $link = '#';
        }

        return ($env === "dev") ? '/app_dev.php' . $link : $link;

    }

    function fetchUserTools($user)
    {
        $em = $this->em;

        return $em->getRepository('Web:ToolBundle')->fetchUserTools($user);
    }


    /**
     * Get specified admin form by processinstnaceid
     */
     function getFormEditUrl($processInstanceId, $type=  'step')
    {


        $result = '';
        $em= $this->em;


        $processInstance = $em->getRepository('CaseBundle:ProcessInstance')->find($processInstanceId);
        /** @var  ProcessInstance $processInstance  */


        if($processInstance)
        {
            $router =$this->container->get('router');


            $step = $processInstance->getStep();
            $stepId = $step->getId();

            $processDefinitionId = $processInstance->getDefinitionId();
            if($stepId)
            {

                if ($type == 'process'){
                    $route =  $route= $router->generate('admin_process_design_chart',['id' =>$processDefinitionId]);
                }else{
                    $route = $router->generate('admin_process_design_chart',['id' =>$processDefinitionId]) . '?stepId='.$stepId;
                }


                $result= $route;
            }
        }else{
            $result ='#';
        }

        return $result;
    }

    /**
     * Funkcja sprawdzająca warunek stylu
     *
     * @return boolean
     */
    function isStylable($column, $style, $row)
    {

        $language = new ExpressionLanguage();

        /**
         * zamiana ' na "" i like na LIKE
         */
        $condition = str_replace(
            ["'", ' like ', ' and ', ' AND ', ' or ', ' OR '],
            ['"', ' LIKE ', ' && ', ' && ', ' || ', ' || '],
            trim($style->getConditionQuery())
        );

        if (!$condition) {
            return true;
        }

        /**
         * Sprawdzenie wszystkich warunków LIKE i zamiana warunku na true/false
         */
        $likeMatches = [];
        preg_match_all('(LIKE)', $condition, $likeMatches);
        foreach ($likeMatches[0] as $key => $operator) {

            $result = 'true';
            $compared = explode($operator, $condition);

            $columns = $this->getStringBetween('{@', '@}', $compared[0]);
            $value = $this->getStringBetween('"', '"', $compared[1]);

            if (!empty($columns) && $value) {

                $column = $columns[count($columns) - 1];

                $rowData = $row[$column];

                $value = str_replace('"', '', $value);

                /**
                 * Sprawdzenie 3 wariantów użycia znaku %
                 */
                if (substr_count($value, '%') == 2) {
                    $result = strpos($rowData, str_replace('%', '', $value)) !== false ? 'true' : 'false';
                } elseif (substr_count($value, '%') == 1) {

                    $pos = strpos($value, '%');
                    if ($pos !== false) {
                        $rawValue = str_replace('%', '', $value);
                        if ($pos != 0) {
                            $result = strpos($rowData, $rawValue) === 0 ? 'true' : 'false';
                        } else {

                            $end = strlen($rowData) - strlen($rawValue);
                            $result = strpos($rowData, $rawValue) === $end ? 'true' : 'false';
                        }
                    }
                }
            }
            $condition = str_replace('{@'.$column.'@} LIKE "'.$value.'"', $result, $condition);

        }
        /**
         * sprawdzenie warunku dla pozostałych kolumn
         */
        $columns = $this->getStringBetween('{@', '@}', $condition);
        foreach ($columns as $column) {
            if (array_key_exists($column, $row)) {
                $rowData = $row[$column];
                if (!is_numeric($rowData) && strpos($rowData, '"') === false) {
                    $rowData = '"'.$rowData.'"';
                }

                $condition = str_replace('{@'.$column.'@}', $rowData, $condition);
            }
        }

        try {
            return $language->evaluate($condition);
        } catch (\Exception $e) {
            return false;
        }
    }

    function getName()
    {
        return 'web_extension';
    }

    /**
     * @param FormField $a
     * @param FormField $b
     * @return int
     */

    private function usortFormFields($a, $b)
    {
        return $a->getPositionInRow() > $b->getPositionInRow();
    }

    private function validateDate($date)
    {
        $d = DateTime::createFromFormat('Y-m-d H:i:s', $date);

        return $d && $d->format('Y-m-d H:i:s') === $date;
    }

    public function getBusinessConfig($key, $groupProcessInstanceId, $platformId = null, $programId = null, $showDescription = false){
        return $this->container->get('case.process_handler')->getBusinessConfig($key, $groupProcessInstanceId, $platformId, $programId, $showDescription);
    }

    public function getPlatformId($groupProcessInstanceId){

        return $this->container->get('case.process_handler')->getAttributeValue('253',$groupProcessInstanceId,'int');

    }

    public function getPartnerLogo($groupProcessInstanceId){

        $platform = null;
        $platformId = $this->container->get('case.process_handler')->getAttributeValue('253',$groupProcessInstanceId,'int');
        if($platformId) {
            $platform = $this->em->getRepository(Platform::class)->find($platformId);
        }

        if(!$platform){
            return '/images/email_assets/ARC.png';
        }

        return $platform->getIconPath();
    }

    public function noAutocomplete() {
        return ' autocomplete=' . mt_rand() . '-' . mt_rand();
    }

    public function addPhonePrefix($number)
    {
        $phonePrefix = $this->em->getRepository('AppBundle:Config')->findOneBy([
            'key' => 'home_country_phone_prefix'
        ]);

        if(!$phonePrefix){
            return $number;
        }

        $phonePrefix = $phonePrefix->getValue();

        $hasPrefix = strpos($number, '+48');

        if (false === $hasPrefix && false === empty($phonePrefix)) {
            $number = '+'. $phonePrefix . ltrim($number);
        }

        return $number;
    }

    public function jsonDecode($json)
    {
        return json_decode($json);
    }
}


