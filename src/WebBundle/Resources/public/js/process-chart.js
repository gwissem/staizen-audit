var $spinner = null;
var myDiagram = null;

function initChart(divId, chartData, processType) {

    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    var diagramConfig = {
        // start everything in the middle of the viewport
        initialContentAlignment: go.Spot.Center,
        // have mouse wheel events zoom in and out instead of scroll up and down
        "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
        "toolManager.hoverDelay": 50,
        // support double-click in background creating a new node
        "clickCreatingTool.archetypeNodeData": {name: ""},
        // enable undo & redo
        "undoManager.isEnabled": false,
        "animationManager.isEnabled": false,
        "isReadOnly": isReadOnly,
        scale: 1,
        padding: 10
    };

    if(processType == 2){
        diagramConfig = {
            // start everything in the middle of the viewport
            initialContentAlignment: go.Spot.Left,
            // create a TreeLayout for the decision tree
            layout: $(go.TreeLayout, {nodeSpacing: 150}),
            // have mouse wheel events zoom in and out instead of scroll up and down
            "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
            // support double-click in background creating a new node
            "clickCreatingTool.archetypeNodeData": {name: ""},
            // enable undo & redo
            "undoManager.isEnabled": false,
            "animationManager.isEnabled": false,
            "isReadOnly": isReadOnly,
            scale: 1,
            padding: 10
        };
    }

    myDiagram = $(go.Diagram, divId, diagramConfig);
    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener("Modified", function (e) {
        var button = document.getElementById("SaveButton");
        if (button) button.disabled = !myDiagram.isModified;
        var idx = document.title.indexOf("*");
        if (myDiagram.isModified) {
            if (idx < 0) document.title += "*";
            if (window.goSamples) goSamples();
        } else {
            if (idx >= 0) document.title = document.title.substr(0, idx);
        }
    });

    var $stepSelect = jQuery('#stepSelect');
    var $dataToSelect = chartData.nodeDataArray;

    for (var iter = 0; iter < $dataToSelect.length; iter += 1)
    {
        var arrayStep = $dataToSelect[iter];
        var newOption = new Option((arrayStep.id + ' ' + arrayStep.name), arrayStep.graphId, false, false);
        $stepSelect.append(newOption)
    }

    $stepSelect.select2();
    $stepSelect.on('select2:select', function (e) {
        var data = e.params.data;
        var node =myDiagram.findNodeForKey(data.id);
        myDiagram.centerRect(node.actualBounds);
        myDiagram.select(node);

    });
    // define the Node template
    myDiagram.nodeTemplate =
        $(go.Node, "Auto",
            {
                desiredSize: new go.Size(184, 60)
            },
            {
                doubleClick: function (e, node) {
                    openNodeFormModal(myDiagram, node.data, isReadOnly);
                }
            },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            // define the node's outer shape, which will surround the TextBlock
            $(go.Shape, "RoundedRectangle",
                {
                    parameter1: 5,  // the corner has a large radius,
                    fill: "#fafafa",
                    portId: "",  // this Shape is the Node's port, not the whole Node
                    fromLinkable: true,
                    fromLinkableSelfNode: true,
                    fromLinkableDuplicates: true,
                    toLinkable: true,
                    toLinkableSelfNode: true,
                    toLinkableDuplicates: true,
                    cursor: "pointer",
                },
                new go.Binding("stroke", "isTechnical", function (value) {
                    return value ? "#e43a45" : "#000"
                })
            ),
            $(go.Panel, "Table",
                {
                    defaultAlignment: go.Spot.Center,
                    defaultRowSeparatorStrokeWidth: 0,
                    defaultColumnSeparatorStrokeWidth: 0
                },
                $(go.RowColumnDefinition, {column: 0, width: 170}),
                $(go.Panel, "Auto",
                    {row: 0, column: 0, stretch: go.GraphObject.Horizontal},
                    $(go.Shape, "RoundedRectangle", {
                        fill: "transparent",
                        stroke: "transparent",
                        strokeWidth: 0,
                        margin: new go.Margin(5, 0, 0, 0),
                        height: 17
                    }),
                    $(go.TextBlock,
                        {font: "bold 8pt Open Sans, bold helvetica", stroke: "#1a1a1a"},
                        new go.Binding("text", "id"))
                ),
                $(go.Panel, "Auto",
                    {row: 1, column: 0, stretch: go.GraphObject.Horizontal},
                    $(go.Shape, "RoundedRectangle", {
                        fill: "transparent",
                        stroke: "transparent",
                        strokeWidth: 0,
                        margin: new go.Margin(-3, 0, -3, 0),
                        height: 19
                    }),
                    $(go.TextBlock,
                        {font: "bold 8.5pt Open Sans, bold helvetica", margin: 0, stroke: "#111", maxLines: 1},
                        new go.Binding("text", "name"))
                ),
                $(go.Panel, "Auto",
                    {row: 2, column: 0, stretch: go.GraphObject.Horizontal},
                    $(go.Shape, "RoundedRectangle", {
                        fill: "transparent",
                        stroke: "transparent",
                        strokeWidth: 0,
                        margin: new go.Margin(0, 0, 5, 0),
                        height: 17
                    }),
                    $(go.TextBlock,
                        {font: "8pt Open Sans", stroke: "#1a1a1a"},
                        new go.Binding("text", "ownerName"))
                )
            )
        );

    // unlike the normal selection Adornment, this one includes a Button
    myDiagram.nodeTemplate.selectionAdornmentTemplate =
        $(go.Adornment, "Spot",
            $(go.Panel, "Auto",
                $(go.Shape, {fill: null, stroke: "#32c5d2", strokeWidth: 2}),
                $(go.Placeholder)  // a Placeholder sizes itself to the selected Node
            ),
            // the button to create a "next" node, at the top-right corner
            $("Button",
                {
                    alignment: go.Spot.TopRight,
                    click: addNodeAndLink  // this function is defined below
                },
                $(go.Shape, "PlusLine", {width: 6, height: 6})
            ) // end button
        ); // end Adornment

    // clicking the button inserts a new node to the right of the selected node,
    // and adds a link to that new node
    function addNodeAndLink(e, obj) {
        var adornment = obj.part;
        var diagram = e.diagram;
        diagram.startTransaction("Add State");

        // get the node data for which the user clicked the button
        var fromNode = adornment.adornedPart;
        var fromData = fromNode.data;
        // create a new "State" data object, positioned off to the right of the adorned Node
        var toData = {name: ""};
        var p = fromNode.location.copy();
        p.x = 300;
        toData.loc = go.Point.stringify(p);  // the "loc" property is a string, not a Point object
        // add the new node data to the model
        var model = diagram.model;
        model.addNodeData(toData);

        // create a link data from the old node data to the new node data
        var linkdata = {
            from: model.getKeyForNodeData(fromData),  // or just: fromData.id
            to: model.getKeyForNodeData(toData),
            variant: getNextLinkNumber(model.getKeyForNodeData(fromData), model.linkDataArray) + 1
        };
        // and add the link data to the model
        model.addLinkData(linkdata);

        // select the new Node
        var newnode = diagram.findNodeForData(toData);
        diagram.select(newnode);

        diagram.commitTransaction("Add State");

        // if the new node is off-screen, scroll the diagram to show the new node
        diagram.scrollToRect(newnode.actualBounds);
    }

    var linkConfig = {
        reshapable: true,
        relinkableFrom: true,
        relinkableTo: true,
        curve: go.Link.Bezier,
        adjusting: go.Link.Stretch,
        smoothness: 1,
        toShortLength: 10,
        doubleClick: function (e, link) {
            openLinkFormModal(myDiagram, link, isReadOnly);
        }
    };

    if(processType == 2){
            linkConfig = {
            reshapable: true,
            relinkableFrom: true,
            relinkableTo: true,
            routing: go.Link.AvoidsNodes,
            smoothness: 1,
            toShortLength: 10,
            doubleClick: function (e, link) {
                openLinkFormModal(myDiagram, link, isReadOnly);
            }
        };
    }

    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
        $(go.Link,  // the whole link panel
            linkConfig,
            {
                selectionAdornmentTemplate: $(go.Adornment,
                    $(go.Shape,
                        {isPanelMain: true, stroke: "#32c5d2", strokeWidth: 2}),
                    $(go.Shape,
                        {toArrow: "Standard", fill: "#32c5d2", stroke: null})
                )  // end Adornment
            },
            {
                toolTip:  // define a tooltip for each node that displays the color as text
                    $(go.Adornment, "Auto",
                        // new go.Binding("visible", "description", function(d) { return (d !== null && d !== '') }),
                        $(go.Shape, { fill: "#f0f0f0" }),
                        $(go.TextBlock, { margin: 4 , textAlign: "center"},
                            new go.Binding("text", "description",function(d) { return (d !== null && d !== '') ? d : '-' })
                        )
                    )  // end of Adornment
            },
            new go.Binding("points").makeTwoWay(),
            new go.Binding("curviness").makeTwoWay(),
            $(go.Shape,  // the link shape
                { isPanelMain: true, strokeWidth: 1 },
                new go.Binding("stroke", "isPrimaryPath", function (value) {
                    return value ? "#000" : "#aaa"
                })),
            $(go.Shape,  // a thick transparent link shape
                { isPanelMain: true, strokeWidth: 25, stroke: "transparent" }),
            $(go.Shape,  // the arrowhead
                {toArrow: "standard", stroke: null}),
            $(go.Panel, "Auto",
                $(go.Shape,  // the label background, which becomes transparent around the edges
                    {
                        isPanelMain: true,
                        fill: "#fff",
                        stroke: null
                    }),
                $(go.TextBlock,
                    new go.Binding("text", "variant"),  // the label text
                    {
                        textAlign: "center",
                        font: "9pt helvetica, arial, sans-serif",
                        margin: new go.Margin(9, 9)
                    },
                    // editing the text automatically updates the model data
                    new go.Binding("text", "variantDesc").makeTwoWay())


            )
        );

    myDiagram.addDiagramListener("LinkDrawn", function (e) {
        var link = e.subject;
        var model = e.diagram.model;
        var linkDataArray = model.linkDataArray;

        myDiagram.model.setDataProperty(link.data, "variant", getNextLinkNumber(link.data.from, linkDataArray));
    });


    calculateDiagramHeight(myDiagram);

    myDiagram.model = go.Model.fromJson(chartData);

    handleChartSaving(myDiagram);

    $spinner.trigger('spinner-off');
    var queryDict = {};
    location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]});
    if(queryDict.stepId){
        openStepModalByStepId(queryDict.stepId, myDiagram);
    }
}

function getNextLinkNumber(fromNode, linkDataArray)
{
    var findLink = linkDataArray.filter(function (e) {
        return e.from == fromNode;
    });

    return findLink.length;
}

function openNodeFormModal(diagram, node, isReadOnly) {

    var nodeName = node.name !== null ? node.name : '';

    if(node.id) {
        nodeName += ' (' + node.id + ')';
    }

    $spinner.trigger('spinner-on');

    $('#step-modal').removeClass('formGenerate').removeClass('overflow-hidden');

    $('#step-attributes-list').html('');
    $.get(Routing.generate('admin_case_process_step_editor', {
        'processId': $('#process-chart').attr('data-process-chart-id'),
        'id': node.id
    }), node, function (response) {

        var nodeId = node.id;
        $('#step-modal #definition-content').html(response);

        if (node.id !== undefined) {
            $('#step-modal').attr('data-id', node.id);
        }
        else {
            $('#step-modal').attr('data-id', '');
        }
        changeStepView('definition');
        $('#step-modal').modal({
            backdrop: 'static', // we don't want to dismiss Modal when Modal or backdrop is the click event target
            keyboard: false // we don't want to dismiss Modal on pressing Esc key
        });
        initSelect2();
        handleStepSaving(diagram, node);
        $('#step-modal .step-name').text(nodeName);

        if (isReadOnly) {
            $("#step-modal #edit-confirm").hide();
            $('#step-edit-form input, #step-edit-form select, #step-edit-form textarea').prop('disabled',true);
        }

        return false;
    });

}

function changeStepView(value) {
    $('.step-editor-content').addClass('hidden');
    $('#' + value + '-content').removeClass('hidden');

    $('#step-modal .btn-step-view').removeClass('active');
    $('#step-modal #btn-step-' + value).addClass('active');
}

function stepAttributesView(stepId, openModal) {
    var openModal = openModal || false;

    if (stepId !== undefined) {
        $.ajax({
            url: Routing.generate('admin_step_attribute_chart', {
                'id': stepId,
                'processId': $('#process-chart').attr('data-process-chart-id')
            }),
            method: 'GET',
            beforeSend: function () {
                $spinner.trigger('spinner-on');
            },
            success: function (response) {
                if(openModal){
                    $('#step-modal').modal();
                }
                $('#step-modal #attributes-content').html(response);
                attributeDiagram.model = go.Model.fromJson(currentAttributeModel);
                changeStepView('attributes');
                $spinner.trigger('spinner-off');
            }
        });
    }
}

function openLinkFormModal(diagram, link, isReadOnly) {

    if(isReadOnly){
        return false;
    }

    $spinner.trigger('spinner-on');
    $.get(Routing.generate('admin_case_process_flow_editor', {
        'processId': $('#process-chart').attr('data-process-chart-id'),
        'id': link.data.id
    }), function (response) {
        $('#flow-modal .modal-body').html(response);
        initSelect2();
        $('#flow-modal').modal();
        handleFlowSaving(diagram, link);
        $spinner.trigger('spinner-off');
        return false;
    });
}

function initSelect2() {
    $('.select2-deselect').select2({
        allowClear: true,
        placeholder: ''
    });
}

function handleChartSaving(diagram) {
    $spinner.trigger('spinner-on');
    $('#submit-process-chart').on('click', function () {
        l = Ladda.create($(this)[0]);
        var processDefinitionId = $('#process-chart').attr('data-process-chart-id');
        $.ajax({
                method: 'POST',
                url: Routing.generate('admin_process_chart_save', {'id': processDefinitionId}),
                data: {'diagramData': diagram.model.toJson()},
                beforeSend: function () {
                    l.start();
                },
                success: function (response) {
                    delayedCallback(function () {
                        l.stop();
                    }, 1000);

                    var $modal = $('#add-version-log-modal');
                    $modal.find('.modal-content').html('');

                    $.ajax({
                        method: 'GET',
                        url: Routing.generate('management_version_log_for_chart_design'),
                        success: function (response) {

                            if(response.form) {

                                $modal.find('.modal-content').html(response.form);
                                $modal.modal({
                                    show: 'true'
                                });

                            }

                        }
                    });

                }
            }
        );
        return false;
    });

}

function handleStepSaving(diagram, node) {

    $('#step-modal .btn-step-view').unbind('click').on('click', function () {

        var stepId = $('#step-modal').attr('data-id');
        var value = $(this).attr('id').replace('btn-step-', '');

        $('#step-modal').removeClass('formGenerate').removeClass('overflow-hidden');

        if (value == 'attributes') {
            $('#btn-step-attributes').addClass('clicked');
            if (stepId === '' || stepId === undefined) {
                $('#step-edit-form').trigger('submit');
                if ($('#step-edit-form #form-errors').text().length == 0) {
                    stepAttributesView(stepId);
                    $('#btn-step-attributes').removeClass('clicked');
                }
                else {
                    changeStepView('definition');
                }
            }
            else {
                stepAttributesView(stepId);
            }
        }
        else {
            changeStepView(value);
        }

    });

    $('#step-edit-form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: function () {
                    $spinner.trigger('spinner-on');
                },
                success: function (response) {
                    $('#error-list').remove();
                    if (response.errors) {
                        $('#form-errors').append('<ul id="error-list" class="list-unstyled"></ul>');
                        $.each(response.errors, function (key, value) {
                            var errorMsg = "<li><span class='fa fa-exclamation-circle'></span><strong>" + $('label[for=step_' + key + ']').text() + "</strong>: " + value + "</li>";
                            $('#step-edit-form #error-list').append(errorMsg);
                        });
                    }
                    else {
                        var inputId = "step_translations_" + $('body').attr('data-locale') + "_name";
                        var data = {
                            "name": $("#" + inputId).val(),
                            "id": $('#step_idPrefix').val() + '.' + $('#step_idSuffix').val(),
                            'ownerName': $('#step_owner').find("option:selected").text(),
                            "isTechnical": $('#step_isTechnical').prop('checked')
                        };
                        updateNodeWithData(diagram, node, data);
                        $('#step-modal').attr('data-id', data.id);
                        if (!$('#btn-step-attributes').hasClass('clicked')) {
                            $("#step-modal").modal('hide');
                        }
                        else {
                            stepAttributesView(data.id);
                        }
                    }
                    $spinner.trigger('spinner-off');
                }
            }
        );
        return false;
    });
}

function handleFlowSaving(diagram, link) {
    $('#flow-edit-form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: function () {
                    $spinner.trigger('spinner-on');
                },
                success: function (response) {
                    $('#flow-edit-form #error-list').remove();
                    if (response.errors) {
                        $('#flow-edit-form #form-errors').append('<ul id="error-list" class="list-unstyled"></ul>');
                        $.each(response.errors, function (key, value) {
                            var errorMsg = "<li><span class='fa fa-exclamation-circle'></span><strong>" + $('label[for=process_flow_' + key + ']').text() + "</strong>: " + value + "</li>";
                            $('#flow-edit-form #error-list').append(errorMsg);
                        });
                    }
                    else {
                        var desc = $('#process_flow_translations_' + _locale + '_description').val();
                        var variantDesc = $('#process_flow_variant').val();

                        if (_processType == 2) {
                            if (desc != '') {
                                variantDesc += ' (' + truncate(desc, 50) + ')';
                            }
                        }
                        var data = {
                            "variant": $('#process_flow_variant').val(),
                            "variantDesc": variantDesc,
                            "isPrimaryPath": $('#process_flow_isPrimaryPath').prop('checked'),
                            "id": response.id,
                            "description": desc
                        };
                        updateLinkWithData(diagram, link, data);
                        $("#flow-modal").modal('hide');
                    }
                    $spinner.trigger('spinner-off');
                }
            }
        );
        return false;
    });
}

function updateNodeWithData(diagram, node, newData) {
    diagram.startTransaction('update node');
    var data = diagram.model.findNodeDataForKey(node.graphId);
    if (data !== null) {
        $.each(newData, function (key, value) {
            diagram.model.setDataProperty(data, key, value);
        });
    }
    diagram.commitTransaction('update node');
}


function updateLinkWithData(diagram, data, newData) {
    diagram.startTransaction('update node');
    $.each(newData, function (key, value) {
        diagram.model.setDataProperty(data.data, key, value);
    });
    diagram.commitTransaction('update node');
}

function spinnerHandle() {

    $spinner = $('#bottom-spinner');

    $spinner.on('spinner-off', function (e) {
        $spinner.fadeOut(300);
    });

    $spinner.on('spinner-on', function (e) {
        $spinner.fadeIn(300);
    });
}

function calculateDiagramHeight(diagram, reload) {
    var reload = reload || false;
    var offset = $('.page-header').outerHeight() + $('#tool-action-bar').outerHeight() + $('#flash-messages .alert').outerHeight(true) + 110;
    var calculatedWidth = window.innerHeight - offset;
    diagram.div.style.height = calculatedWidth + 'px';
    if (reload) {
        diagram.requestUpdate();
    }
}


function openStepModalByStepId(id, diagram) {
    var node = null;
    var itr = diagram.nodes;
    while (itr.next())
    {
        var nodeTest = itr.value;
        if(nodeTest.data.id == id)
        {
            node = nodeTest;
            break;
        }
    }
   if(node)
   {
       return openNodeFormModal(myDiagram, node.data,0);
   }
}