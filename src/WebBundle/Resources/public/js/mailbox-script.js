(function($, window, document) {

    $(function() {

        var l, $smallModal = $("#small");

        $('.test-connection').on('click', function (e) {

            var $t = $(this);
            l =  Ladda.create($t[0]);
            l.start();

            checkConn($t.data('url')).done(function(data) {
                if(data.success) {
                    $smallModal.find('.modal-body .alert-success').show().next().hide();
                }
                else {
                    $smallModal.find('.modal-body .alert-danger').show().prev().hide().next().find('.msg').html(data.msg);
                }

                $('#small').modal();

            }).fail(function (data) {
                console.error('Błąd przy testowaniu połączenia. Msg: ' +data.statusText);
            }).always(function (data) {
                l.stop();
            });
        });
    });

    function checkConn(url) {
        return $.ajax({
            url: url,
            type: "GET"
        });
    }

}(window.jQuery, window, document));