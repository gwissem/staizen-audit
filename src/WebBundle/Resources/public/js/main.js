var iconsPaginationLimit = 24;


/** For storage variable for main.js */

var mainJs = {
    $body: null
};

var fileAdvancedUpload;

$(function () {

    var modalDialog = $('.modal-dialog');
    if (!modalDialog.parents().hasClass('fullscreen') && !modalDialog.hasClass('js-form-constructor')) {
        modalDialog.draggable({
            handle: ".modal-header"
        });
    }

    initializeVariables();
    checkIfMobile();
    fixPageContent();
    recoverHamburgerMenu();

    if ($('.a2lix_translations').length > 0) {
        var defaultLocale = $('.a2lix_translations').attr('data-default-locale');

        if ($('.a2lix_translationsFields .has-error').length > 0) {
            var errorLocale = $('.a2lix_translationsFields .has-error').closest('.tab-pane').attr('data-locale');
            var errorTab = $('.a2lix_translationsLocales.nav-tabs:first li[data-locale=' + errorLocale + ']');
            errorTab.find('a').trigger('click');
            errorTab.addClass('has-error');
        }
        else {
            $('.a2lix_translations input, .a2lix_translations textarea').each(function () {
                var inputLocale = $(this).closest('.tab-pane').attr('data-locale');
                if (defaultLocale != inputLocale && $(this).val() == '') {
                    var defaultLocaleinputId = $(this).attr('id').replace('translations_' + inputLocale, 'translations_' + defaultLocale);
                    $(this).val($('#' + defaultLocaleinputId).val());
                }
            });
        }
    }

    $('.tooltip-wrapper').tooltip({position: "bottom"});

    $('.select2').select2();

    $('[data-toggle="confirmation"]').confirmation();

    $('.select2-deselect').select2({
        allowClear: true,
        placeholder: ''
    });

    moment().locale("pl").format('LLL');

    mainJs.$body.on('click', '.btn-delete', function () {
        $('#removal-modal .modal-body b').html($(this).parent().siblings('.entity-name').html());
        $("#removal-modal #confirm-modal").attr('href', $(this).attr('data-url'));
        $('#removal-modal').modal();
        return false;
    });

    $('#removal-modal #confirm-modal').click(function () {
        window.location = $(this).attr('href');
    });

    $('#removal-modal #cancel-modal').click(function () {
        $('#list-modal').modal('hide');
    });

    var $selectbox = $('.floating-label').select2({
        placeholder: "",
        allowClear: true,
    })
        .on('select2:select', function () {
            // $('label[for="country"]').addClass('filled active');
            $(this).siblings('label').addClass('filled active');
        })
        .on('select2:unselect', function () {
            // $('label[for="country"]').removeClass('filled');
            $(this).siblings('label').removeClass('filled active');
        });

    $selectbox.on('blur', function () {
        // $('label[for="country"]').removeClass('active');
        $(this).siblings('label').removeClass('active');
    });
    $selectbox.val("").trigger("change");

    $('#user-tools-menu li li.active').parents('.current_ancestor').addClass('active open').find('span.arrow').addClass('open');


    /** Inicjalizacja DatePickerów */

    $('.date-picker:not(.from-now, .to-now)').datepicker(datePickerOptions);
    $('.date-time-picker:not(.from-now, .to-now)').datepicker(dateTimePickerOptions);

    var fromNowOptions = $.extend({}, datePickerOptions);
    var toNowOptions = $.extend({}, datePickerOptions);

    var fromNowTimeOptions = $.extend({}, dateTimePickerOptions);
    var toNowTimeOptions = $.extend({}, dateTimePickerOptions);

    fromNowTimeOptions.minDate = new Date();
    toNowTimeOptions.maxDate = new Date();
    fromNowOptions.minDate = new Date();
    toNowOptions.maxDate = new Date();

    $('.date-time-picker.from-now').datepicker(fromNowTimeOptions);
    $('.date-time-picker.to-now').datepicker(toNowTimeOptions);

    $('.date-picker.from-now').datepicker(fromNowOptions);
    $('.date-picker.to-now').datepicker(toNowOptions);

    var date;
    globalVar.dateTimeDisableRefresh = true;
    $('.date-picker, .date-time-picker').not(':disabled').each(function (i, ele) {
        if(ele.value)
        {
            date = new Date(ele.value);

            if(isNaN( date.getTime()))
            {
            }
            else{
                $(ele).data('datepicker').selectDate(new Date(ele.value));
            }
        }
    });
    globalVar.dateTimeDisableRefresh = false;

    /** KONIEC */

    dependentInputsHandle();

    disableRefresh();

    /** --------- TE CHYBA DO WYWALENIA STĄD ------------ */

    $('#icons-search').hideseek(
        {
            hidden_mode: true
        }
    );

    $('#icons-search').on('_after', function () {
        if ($(this).val() == '') {
            $('#icons .fa-item').hide();
            $('#icons .fa-item').slice(0, iconsPaginationLimit).show();
            $('.icon-pagination').show();
        }
        else {
            $('.icon-pagination').hide();
        }
    });

    $("#icons-search").bind('cut', function () {
        $('#icons .fa-item').hide();
        $('#icons .fa-item').slice(0, iconsPaginationLimit).show();
        $('.icon-pagination').show();
    });

    $('#icons').twbsPagination({
        totalPages: $('#icons').children().length / iconsPaginationLimit + 1,
        visiblePages: 4,
        onPageClick: function (event, page) {
            var page = page - 1;
            var from = page * iconsPaginationLimit;
            var to = page * iconsPaginationLimit + iconsPaginationLimit;
            $('#icons .fa-item').hide();
            $('#icons .fa-item').slice(from, to).show();
        },
        paginationClass: 'icon-pagination pagination'
    });

    fileAreaHandle();

    colorPickerHandle(); /** Picker do kolorów */

    editableInputsHandle(); /** Jak na razie wykorzystane tylko przy Mailbox */

    codeMirrorHandle(); /** tylko do edycji narzędzia i mailboxu */

    /** ---------- KONIEC ----------- */

    /** scripts fired only in content Atlas - not in Header Atlas */

    if(typeof HeaderAtlas === 'undefined') {
        if (typeof inIframe === 'function') {
            if (inIframe()) {

                messagesHandle(); /** Do odbierania wiadomości od Parenta (Header Atlas) */

                toolRefreshHandle(); /** Odświeżanie wyników w narzędziu */

                toolSummaryModalHandle(); /** Obsługa dodawania powiadomień */

            }
        }

        /*funResizeHandle.addFunction('FixPageContent', fixPageContent, true);*/

        $(window).on('focus', function() {
            sendToParentWindow({
                type: 'on-focus'
            });
        });

        sendToParentWindow({
            type: 'title-change',
            data: document.title
        });

        sideMenuHandle();
    }
    else {
        /** Funkcje odpalane tylko w Header Atlas */


    }
    /** Resize handle */

    windowResizeHandle();
});

function fileAreaHandle() {

    fileAdvancedUpload = new FileAdvancedUpload();
    fileAdvancedUpload.Run();

}


function initializeVariables() {
    mainJs.$body = $('body');
}

function checkIfMobile() {
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    if( isMobile ) {
       $('body').attr('id', 'mobileView');
    }
}

function recoverHamburgerMenu() {

    var message = {
        type: 'dashboard-not-viewed'
    };

    window.parent.postMessage(message, Routing.generate('index', [], true));
}

function fixPageContent() {
    var pageContent = $('.page-content .portlet');
    var sidebarMenuContainer = $('#side-menu-container');
    var deviceHeight = $(window).height();
    var sidebarMenu = $('.page-sidebar-menu');
    var mobileSideBarHeight = 172;
    var isMobile = false;
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
        isMobile = true;

    if ( isMobile ) {
        if ( pageContent.height() > sidebarMenu.height() ) {
        sidebarMenuContainer.height(deviceHeight - ( 45 + mobileSideBarHeight));
        sidebarMenu.height(deviceHeight - ( 45 + mobileSideBarHeight));
        }
    } else {
        sidebarMenuContainer.height(deviceHeight - 114);
        sidebarMenu.height(deviceHeight - 114);
    }

}

function codeMirrorHandle() {

    if($('.sql-codemirror').size()) {

        $('.sql-codemirror').each(function (i, ele) {

            CodeMirror.fromTextArea(ele, {
                mode: 'text/x-mssql',
                theme: 'neat',
                indentWithTabs: true,
                smartIndent: true,
                lineNumbers: true,
                matchBrackets : true,
                extraKeys: {
                    "F11": function(cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                    },
                    "Esc": function(cm) {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    }
                    // "Ctrl-Space": "autocomplete"
                }
            });
        })
    }

}

function codeMirrorHandleJson() {

    if($('.json-codemirror').size()) {

        $('.json-codemirror').each(function (i, ele) {

            var editor  = CodeMirror.fromTextArea(ele, {
                mode: 'application/json',
                theme: 'neat',
                indentWithTabs: true,
                smartIndent: true,
                lineNumbers: true,
                matchBrackets : true,
                extraKeys: {
                    "F11": function(cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                    },
                    "Esc": function(cm) {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    }
                    // "Ctrl-Space": "autocomplete"
                },
                onChange: function(){editor.save()},

            });
            editor.on('change ',function () {
                editor.save()
            })
        })
    }

}

function editableInputsHandle() {

    $('.editable-boolean').editable({
        type: 'select',
        placement: 'right',
        title: 'Option 1',
        inputclass: '',
        display: function(value) {
            if(value == 0) {
                this.innerHTML = this.getAttribute('data-msg-false');
            }
            else if(value == 1) {
                this.innerHTML = this.getAttribute('data-msg-true');
            }
        }
    });

    $('.dynamic-editable').editable({
        placement: 'right'
    });

}

function disableRefresh() {
    $(document).on("keydown", disableF5);
}

function disableF5(e) {
    if ((e.which || e.keyCode) == 116 || ((e.which || e.keyCode) == 82 && e.ctrlKey))
        e.preventDefault();
}

function messagesHandle() {

    window.addEventListener("message", receiveMessage, false);

    function receiveMessage(event)
    {

        /** Miało byc zabezpieczenie, zeby nie wysylac wiadomosci z innych stron */
        // var origin = event.origin || event.originalEvent.origin;
        // if ((origin + '/') !== Routing.generate('index', [], true).replace('app_dev.php/', ''))
        //     return;

        executeMessage(event.data);

    }

    $('.users-table').on('click', '.impersonate-user-btn', function () {

        sendToParentWindow({
            type: 'force-redirect',
            delay: 2000
        });

    });

}

function sendToParentWindow(message) {
    window.parent.postMessage(message, Routing.generate('index', [], true));
}

function executeMessage(message) {

    var url = '';

    if(message.type === "sidebar-toggle") {

        Cookies.set('sidebar_closed', message.data);

        if(message.data) {
            mainJs.$body.addClass('page-sidebar-closed');
        }
        else {
            mainJs.$body.removeClass('page-sidebar-closed');
        }
    }
    else if(message.type === "telephony-pick-up-with-reload") {

        url = Routing.generate('operational_dashboard_index', {
            action: 'telephony-pick-up',
            process_instance: message.data.processInstances,
            isforce: message.data.isForce
        }, true);

        forceReload(false, url);

    }
    else if(message.type === 'mobile-sidebar-toggle') {

        $('.page-sidebar-wrapper').toggleClass('active');
        $('.overlay').toggleClass('visible');
        $('body').toggleClass('scrollDisabled');

    }
    else if(message.type === 'force-reload') {
        forceReload(false, message.link);
    }
    else if(message.type === 'task-overview') {

        var isOperationDashboard = $('body').hasClass('operational-page');

        if(isOperationDashboard) {
            taskOverview(message.id);
        }
        else {
            url = Routing.generate('operational_dashboard_index', {
                action: 'task-overview',
                process_instance: message.id
            }, true);
            forceReload(false, url);
        }
    }
    else if (message.type === "conversationEnded") {

        var cEvent = new CustomEvent("conversationEnded", {'detail': message.data});
        window.document.dispatchEvent(cEvent);

    }

}

function tinyMCEHandle() {

    var $tinyMceModal = $('#tinymce-modal'),
        textAreaTinyMce;

    if($tinyMceModal.size())
    {
        textAreaTinyMce = $tinyMceModal.find('.tinymce-word');

        mainJs.$body.on('click', '.init-word-edit', function (e) {
            e.preventDefault();

            var $targetWord = $(this).parent().next();
            textAreaTinyMce.val($targetWord.val());
            $tinyMceModal.attr('data-target-word', $targetWord.attr('id'));
            $tinyMceModal.modal();
        });

        $('#confirm-word-edit').on('click', function (e) {
            e.preventDefault();

            var $targetWord = $('#' + $tinyMceModal.attr('data-target-word'));
            $targetWord.val(textAreaTinyMce.val());
            $tinyMceModal.modal('hide');
        });
    }
}

function colorPickerHandle() {
    $('.init-colorpicker').minicolors({
        theme: 'bootstrap'
    });
}

function dashboardClock() {
    var hands = [];
    hands.push(document.querySelector('#secondhand > *'));
    hands.push(document.querySelector('#minutehand > *'));
    hands.push(document.querySelector('#hourhand > *'));

    var cx = 100;
    var cy = 100;

    function shifter(val) {
        return [val, cx, cy].join(' ');
    }

    var date = new Date();
    var hoursAngle = 360 * date.getHours() / 12 + date.getMinutes() / 2;
    var minuteAngle = 360 * date.getMinutes() / 60;
    var secAngle = 360 * date.getSeconds() / 60;

    hands[0].setAttribute('from', shifter(secAngle));
    hands[0].setAttribute('to', shifter(secAngle + 360));
    hands[1].setAttribute('from', shifter(minuteAngle));
    hands[1].setAttribute('to', shifter(minuteAngle + 360));
    hands[2].setAttribute('from', shifter(hoursAngle));
    hands[2].setAttribute('to', shifter(hoursAngle + 360));

    for(var i = 1; i <= 12; i++) {
        var el = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        el.setAttribute('x1', '100');
        el.setAttribute('y1', '30');
        el.setAttribute('x2', '100');
        el.setAttribute('y2', '40');
        el.setAttribute('transform', 'rotate(' + (i*360/12) + ' 100 100)');
        el.setAttribute('style', 'stroke: #ffffff;');
        document.querySelector('svg').appendChild(el);
    }

    /*var formattedClockDate = date.getHours() + ":" + date.getMinutes() + " " + date.getFullYear()*/

    function leadingZero(i) {
        return (i < 10)? '0'+i : i;
    }

    function showTextTime() {
        var currentDate = new Date();
        var days = ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"];
        var textDate = leadingZero(currentDate.getDate()) + "." + leadingZero((currentDate.getMonth()+1)) + "." + currentDate.getFullYear();
        var textTime = leadingZero(currentDate.getHours()) + ":" + leadingZero(currentDate.getMinutes()) + ":" + leadingZero(currentDate.getSeconds());


        document.querySelector('#date').innerHTML = textDate;
        document.querySelector('#time').innerHTML = textTime;

        setTimeout(function() {
            showTextTime()
        }, 1000);
    }

    showTextTime();

}


function sideMenuHandle() {

    mainJs['sideMenuContainer'] = $('#side-menu-container');
    mainJs['userToolsMenu'] = $('#user-tools-menu');
    mainJs['toolExist'] = ($('#tool-table-container').length !== 0);

    var tagElements = mainJs.sideMenuContainer.find('li');

    $('#side-menu-search-input').on('keyup', function () {

        if($(this).val()) {
            tagElements.hide();

            var isValid = true;

            try {
                new RegExp($(this).val().toLowerCase());
            } catch(e) {
                isValid = false;
            }

            if(isValid) {
                var myWord = new RegExp($(this).val().toLowerCase());

                for (var i = tagElements.length; i >= 0; i--) {
                    var tag = tagElements.eq(i);

                    var $tag = $(tag);

                    if (myWord.test($tag.text().toLowerCase())) {
                        $tag.show();
                        if($tag.find('li:visible').size() == 0) {
                            $tag.find('li').show();
                            if ( !$tag.hasClass('open') && $tag.children('ul').size()) {
                                $tag.children('ul').show();
                            }
                        }
                    }
                }
            }

        }
        else {
            tagElements.show();
            mainJs['userToolsMenu'].find('> li.nav-item:not(.open)').find('ul').hide();
        }


    });

    mainJs['dashboardSearch'] = $('.tickets-wrapper');
    var searchResults = mainJs.dashboardSearch.find('.single-ticket');

    $('.tickets-wrapper #side-menu-search input').on('keyup', function () {
        searchResults.hide();
        searchResults.parent().parent().hide();
        if($(this).val()) {
            var myWord = new RegExp($(this).val().toLowerCase());

            for (var i = searchResults.length; i >= 0; i--) {
                var tag = searchResults.eq(i);

                var $tag = $(tag);

                if (myWord.test($tag.text().toLowerCase())) {
                    $tag.show();
                    $tag.parent().parent().show();
                    if($tag.find('li:visible').size() == 0) {
                        $tag.find('li').show();
                        if ( !$tag.hasClass('open') && $tag.children('ul').size()) {
                            $tag.children('ul').show();
                        }
                    }
                }
            }
        }
        else {
            searchResults.show();
            searchResults.parent().parent().show();
        }


    });

    funResizeHandle.addFunction('rebuildSideMenuScroll', rebuildSideMenuScroll, true);

}

function rebuildSideMenuScroll() {

    /*if(mainJs.sideMenuContainer.next().hasClass('slimScrollBar')) {
     mainJs.sideMenuContainer.slimScroll({destroy: true});
     }

     var windowHeight = (mainJs.toolExist) ? window.innerHeight : Math.max(document.body.clientHeight, window.innerHeight);
     var height = window.innerHeight - ($('#side-menu-search').outerHeight(true) + parseInt($('.page-container').css('padding-top')) + 172);

     mainJs.sideMenuContainer.slimScroll({
     height: height + 'px'
     });*/

}

function toolRefreshHandle() {

    $('#tool-refresh-trigger').on('click', function () {
        refreshView();
    })
}

function forceReload($btn, link) {
    var href = ($btn === false) ? link : $btn.attr('href');

    window.location.href = href;
    if(window.location.pathname == href.substr(0, href.indexOf('#'))){
        window.location.reload();
    }
}

function toolSummaryModalHandle(){

    var prefixFormName = '#atlas_tool_summary_',
        $form = $(prefixFormName + 'form'),
        toolModal = $('#tool-summary-modal');

    $form.on('submit', function (e) {
        e.preventDefault();

        var l = Ladda.create($(this).find('.bind-spinner')[0]);
        l.start();

        var form = $(this);

        $.ajax({
            url: form.attr('action'),
            type: "POST",
            data: form.serialize()
        }).done(function(data) {

            if(data.success){
                $form.find('.alert').hide();
                toolModal.modal('hide');

                $form[0].reset();
                $form.find(prefixFormName + 'userGroups').select2("val", "");
                $form.find(prefixFormName + 'globalSummary').trigger('change');

            }
            else {
                var alert = $form.find('.alert');
                alert.find('.msg').html(data.msg);
                alert.show();
            }
        }).fail(function (data) {
            console.error('Błąd. Msg: ' +data.statusText);
            var alert = $form.find('.alert');
            alert.find('.msg').html('Nieznany błąd...');
            alert.show();
        }).always(function(){

            if (typeof inIframe === 'function') {
                if (inIframe()) {

                    sendToParentWindow({
                        type: 'refresh-summary'
                    });
                }
            }

            l.stop();
        });
    });

    $(prefixFormName + 'summaryType').on("change", function () {
        if($(this).val() == "sum") {
            $(prefixFormName + 'columnName').prop('disabled', false);
        }
        else {
            $(prefixFormName + 'columnName').val('').prop('disabled', true);
        }
    });

    $('#tool-summary-modal-trigger').on('click', function (e) {
        e.preventDefault();
        toolModal.modal();
        toolModal.find(prefixFormName + 'filters').val((removeURLParameter('?'+ window.location.hash.replace('#',''), 'hash')).replace("?", ''));
    });

    toggleSummaryGroupSelect(toolModal, prefixFormName);
}

function toggleSummaryGroupSelect(toolModal, prefixFormName) {

    var userGroupSelect = toolModal.find(prefixFormName + 'userGroups');
    var perUserCheckbox = toolModal.find(prefixFormName + 'perUser');

    toolModal.find(prefixFormName + 'globalSummary').on('change', function () {
        if($(this).is(':checked'))
        {
            userGroupSelect.select2("val", "");
            userGroupSelect.closest('.form-group').show();
            perUserCheckbox.prop('checked', false);
            perUserCheckbox.closest('.form-group').show();
        }
        else {
            userGroupSelect.select2("val", "");
            userGroupSelect.closest('.form-group').hide();
            perUserCheckbox.prop('checked', false);
            perUserCheckbox.closest('.form-group').hide();
        }
    }).trigger('change');
}

function removeURLParameter(url, parameter) {
    var urlparts= url.split('?');
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        for (var i= pars.length; i-- > 0;) {
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        return url;
    } else {
        return url;
    }
}


/**
 * @class FileAdvancedUpload
 */
var FileAdvancedUpload = (function () {

    var excludeExtension = [
        'exe', 'bat', 'cmd', 'js', 'vbs', 'dll', 'scr', 'ocx'
    ];

    function FileAdvancedUpload () {

        FileAdvancedUpload.isAdvancedUpload = function() {
            var div = document.createElement('div');
            return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
        }();

        FileAdvancedUpload.filesSelector = '.advanced-upload';
        FileAdvancedUpload.droppedFiles = {};
        FileAdvancedUpload.prototypeFileBox = "<div class='file-box' data-uuid='__UUID__' data-index='__INDEX__'><div class='close'></div>" +
                "<div class='icon __ICON__-icon'></div>" +
                "<div class='name'>__NAME__</div>" +
                "<div class='size'>__SIZE__</div>" +
            "</div>";
    }

    function _handleEvents()
    {

        $.each($(FileAdvancedUpload.filesSelector), function (i, ele) {
            _handleDragAndDrop($(ele));
        });

        $(document).on('click', '.file-box .close', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var $fileBox = $(this).parent();

            _removeFile($fileBox.attr('data-index'), $fileBox.attr('data-uuid'));

        })
    }

    function _handleDragAndDrop($ele) {

        var uuid = guid();
        $ele.addClass('file-area-ready');

        $ele.on('clearFiles', function () {
            var $fileBox = $(this).parent();
            _clearFiles($fileBox.attr('data-uuid'));
        });

        var $fileArea = $ele.wrap(function() {
            return "<div data-uuid='"+uuid+"' class='advanced-upload-box'>" + "</div>";
        }).parent();

        $fileArea.append("<div class='text'>" +
                "<svg xmlns='http://www.w3.org/2000/svg' width='40' height='46' viewBox='0 0 50 43'>" +
                    "<path d='M48.4 26.5c-.9 0-1.7.7-1.7 1.7v11.6h-43.3v-11.6c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v13.2c0 .9.7 1.7 1.7 1.7h46.7c.9 0 1.7-.7 1.7-1.7v-13.2c0-1-.7-1.7-1.7-1.7zm-24.5 6.1c.3.3.8.5 1.2.5.4 0 .9-.2 1.2-.5l10-11.6c.7-.7.7-1.7 0-2.4s-1.7-.7-2.4 0l-7.1 8.3v-25.3c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v25.3l-7.1-8.3c-.7-.7-1.7-.7-2.4 0s-.7 1.7 0 2.4l10 11.6z'></path>" +
                "</svg>" +
                "<p><strong>" + Translator.trans('file_upload.choose_file') + "</sn> " + Translator.trans('file_upload.or_drop_here') + "</span></p>" +
            "</div>" +
            "<div class='list-files'></div>");

        FileAdvancedUpload.droppedFiles[uuid] = {
            files: [],
            multiple: $ele.is('[multiple]')
        };

        $ele.on('change', function (e) {

            if(this.files.length > 0) {
                FileAdvancedUpload.droppedFiles[uuid].files = [];

                var error = null;

                if(FileAdvancedUpload.droppedFiles[uuid].multiple) {
                    $.each(this.files, function (i, file) {

                        error = validFile(file);
                        if(!error) {
                            _appendFile(uuid, file);
                        }

                    });
                }
                else {

                    error = validFile(this.files.item(0));
                    if(!error) {
                        _appendFile(uuid, this.files.item(0));
                    }

                }

                _refreshDisplayingFiles(uuid);

                if(error) {
                    alert(error.msg);
                    return false;
                }

            }
        });


        $fileArea.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
            })
            .on('dragover dragenter', function() {
                $fileArea.addClass('is-dragover');
            })
            .on('dragleave dragend drop', function() {
                $fileArea.removeClass('is-dragover');
            })
            .on('click', function (e) {

                var wasClicked = false;

                if($(e.target).closest('.file-box').size() <= 0) {
                    var label = $('label[for="' + $ele.attr('id') + '"]');
                    if (label.length) {
                        label.trigger('click');
                        wasClicked = true;
                    }
                }

                if ($(e.target).closest('.form-block-upload-file').length && wasClicked === false) {
                    $('.advanced-upload-box[data-uuid="' + uuid + '"] input[type="file"]')[0].click();
                }

            })
            .on('drop', function(e) {

                var safeExtention = [
                    'png', 'jpeg', 'jpg', 'gif', 'svg', 'tif', '',
                    'text', 'docx', 'eml', 'pdf', 'pptx', 'rar', 'xlsx', 'xls', 'doc', 'zip'
                ];

                var error = null;

                $.each(e.originalEvent.dataTransfer.files, function (i, file) {

                    error = validFile(file);

                });

                if(error) {
                    alert(error.msg);
                    return false;
                }

                $ele[0].files = e.originalEvent.dataTransfer.files;

                if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
                    alert('Drag & Drop działa tylko na przeglądarce Google Chrome. Proszę kliknąć i wybrać plik ręcznie.');
                    $ele.trigger('click');
                }
                else {
                    $ele.trigger('change');
                }

            });

    }

    function validFile(file) {
        var error = null;

        if(file.size > 26214400) {
            error = {
                msg: file.name + ' - ' + Translator.trans('file_upload.file_too_large')+ '.'
            };
        }

        var ext = _getFileExtension(file.name);

        if(typeof ext === "undefined" || excludeExtension.indexOf(ext) > -1) {
            error = {
                msg: file.name + ' - ' + Translator.trans('file_upload.file_bad_extension')+ '.'
            };
        }

        return error;
    }
    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    FileAdvancedUpload.prototype.initFileArea = function($ele) {
        _handleDragAndDrop($ele)
    };

    FileAdvancedUpload.prototype.refreshInit = function() {
        _refreshInit();
    };

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _appendFile(uuid, file) {

        var index = null;

        $.each(FileAdvancedUpload.droppedFiles[uuid].files, function (i, ele) {
            if(ele.name == file.name) {
                index = i;
            }
        });

        if(index !== null) {
            FileAdvancedUpload.droppedFiles[uuid].files.splice(index, 1);
        }

        // if(file.size > LIMIT ) {
        //     showError();
        //     return false;
        // }

        FileAdvancedUpload.droppedFiles[uuid].files.push(file);

    }

    function _refreshInit() {
        $.each($(FileAdvancedUpload.filesSelector).not('.file-area-ready'), function (i, ele) {
            _handleDragAndDrop($(ele));
        });
    }

    function _refreshDisplayingFiles(uuid) {

        var $fileArea = $('.advanced-upload-box[data-uuid="' + uuid + '"]');

        if(FileAdvancedUpload.droppedFiles[uuid].files.length) {

            var $filesBox = $fileArea.find('.list-files');
            $filesBox.html('');

            $.each(FileAdvancedUpload.droppedFiles[uuid].files, function (i, ele) {
                $filesBox.append(_renderFile(ele, i, uuid));
            });

            $fileArea.addClass('is-uploading');
        }
        else {
            $fileArea.removeClass('is-uploading');
        }

    }

    function _renderFile(file, i, uuid) {

        var fileBox = FileAdvancedUpload.prototypeFileBox;

        var ext = _getFileExtension(file.name);

        fileBox = fileBox.replace('__NAME__', file.name);
        fileBox = fileBox.replace('__SIZE__', '(' + humanFileSize(file.size, true) + ')');
        fileBox = fileBox.replace('__ICON__', ext);
        fileBox = fileBox.replace('__INDEX__', i);
        fileBox = fileBox.replace('__UUID__', uuid);

        if(['png', 'jpeg', 'jpg', 'gif', 'svg', 'tif'].indexOf(ext) != -1)
        {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.file-box[data-uuid="'+uuid+'"][data-index="'+i+'"] .icon').css({
                    'background-image': 'url("'+e.target.result+'")'
                });
            };

            reader.readAsDataURL(file);
        }

        return fileBox;
    }

    function _removeFile(i, uuid) {
        FileAdvancedUpload.droppedFiles[uuid].files.splice(i, 1);
        _refreshDisplayingFiles(uuid);
    }

    function _clearFiles(uuid) {
        FileAdvancedUpload.droppedFiles[uuid].files.splice(0, FileAdvancedUpload.droppedFiles[uuid].files.length);
        _refreshDisplayingFiles(uuid);
    }

    function _getFileExtension(filename) {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : undefined;
    }

    FileAdvancedUpload.prototype.Run = function () {

        if (FileAdvancedUpload.isAdvancedUpload) {

            _handleEvents.call(this);

        }

    };

    return FileAdvancedUpload;
})();

var decodeEntities = (function () {
    // this prevents any overhead from creating the object each time
    var element = document.createElement('div');

    function decodeHTMLEntities(str) {
        if (str && typeof str === 'string') {
            // strip script/html tags
            str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
            str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
            element.innerHTML = str;
            str = element.textContent;
            element.textContent = '';
        }

        return str;
    }

    return decodeHTMLEntities;
})();

function truncate(str, maxLength) {
    if (str.length > maxLength) {
        return str.substr(0, maxLength) + '...';
    }
    return str;
}