var $spinner = null;
var attributeSearchValue = '';
var isAttributeEditor = true;
var currentAttributeModel = {'nodeDataArray': [], 'linkDataArray': [], 'nodeKeyProperty': 'id'};

function initAttributeChart(divId, chartData) {
    handleDragDrop();
    handleBtnGroup();
    $spinner.trigger('spinner-on');
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    // Now we can initialize a Diagram that looks at the visual tree that constitutes the Diagram above.
    attributeDiagram =
        $(go.Diagram, divId,
            {
                initialContentAlignment: go.Spot.Center,
                // have mouse wheel events zoom in and out instead of scroll up and down
                "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
                "undoManager.isEnabled": true,
                "animationManager.isEnabled": false,
                scale: 1,
                padding: 10,
                layout: $(go.TreeLayout, {nodeSpacing: 10})  // automatically laid out as a tree
            });

    attributeDiagram.nodeTemplate =
        $(go.Node, "Auto",
            {
                doubleClick: function (e, node) {
                    if (isAttributeEditor) {
                        openAttributeFormModal(attributeDiagram, node.data);
                    }
                    else {
                        addStepAttribute(node.data);
                    }
                }
            },
            $(go.Shape, {
                fill: "#fafafa", stroke: "#111", strokeWidth: 0.75, portId: "",  // this Shape is the Node's port, not the whole Node
                fromLinkable: true, fromLinkableSelfNode: false, fromLinkableDuplicates: false,
                toLinkable: true, toLinkableSelfNode: false, toLinkableDuplicates: false,
                cursor: "pointer"
            }),
            $(go.TextBlock,
                {
                    font: "10pt Open Sans, bold helvetica",
                    stroke: "#111",
                    margin: new go.Margin(8, 12)
                },
                // bind the text to the Diagram/Layer/Part/GraphObject converted to a string
                new go.Binding("text", "name")
            )
        );
    attributeDiagram.linkTemplate =
        $(go.Link,
            {
                routing: go.Link.AvoidsNodes,
                corner: 2
            },
            $(go.Shape, {stroke: "#111", strokeWidth: 0.75}),
            $(go.Shape,  // the arrowhead
                {toArrow: "standard", stroke: null})
        );

    attributeDiagram.addDiagramListener("SelectionDeleting",
        function (e) {
            for (var it = e.diagram.selection.iterator; it.next();) {
                var part = it.value;  // part is now a Node or a Group or a Link or maybe a simple Part
                if (part instanceof go.Node) {
                    removeFromChart(part);
                }
            }
        });

    if (!isAttributeEditor) {
        attributeDiagram.isReadOnly = true;
    }

    attributeDiagram.model = go.Model.fromJson(chartData);
    attributeDiagram.validCycle = go.Diagram.CycleNotDirected;

    handleAttributeChartSaving(attributeDiagram);

    $spinner.trigger('spinner-off');
    calculateAttributeDiagramHeight(attributeDiagram);
}


function spinnerHandle() {

    $spinner = $('#bottom-spinner');

    $spinner.on('spinner-off', function (e) {
        $spinner.fadeOut(300);
    });

    $spinner.on('spinner-on', function (e) {
        $spinner.fadeIn(300);
    });
}

function calculateAttributeDiagramHeight(diagram, reload, modal) {
    var modal = modal || false;
    var reload = reload || false;
    var offset = modal ? $('#step-modal .modal-header').outerHeight() + 50 : $('.page-header').outerHeight() + 80;
    var btnOffset = modal ? 9 : 60;
    var calculatedHeight = window.innerHeight - offset;
    diagram.div.style.height = calculatedHeight + 'px';
    if (reload || modal) {
        diagram.requestUpdate();
    }
    $('#attribute-list').height(calculatedHeight - btnOffset + 'px');
    if (modal) {
        var stepListHeight = calculatedHeight - 5;
        $('#step-attributes-list').height(stepListHeight + 'px');
        $('#attribute-list').height(calculatedHeight - 50 + 'px');
    }
}

function handleDragDrop() {

    var dragged = null;
    var attributeList = document.getElementById("attribute-list");
    attributeList.addEventListener("dragstart", function (event) {
        if (event.target.className !== "draggable") return;
        // Some data must be set to allow drag
        event.dataTransfer.setData("text", "");

        // store a reference to the dragged element
        dragged = event.target;
        // Objects during drag will have a red border
        event.target.style.background = "#f2784b";
        event.target.style.color = "#fff";
    }, false);

// This event resets styles after a drag has completed (successfully or not)
    attributeList.addEventListener("dragend", function (event) {
        event.target.style.background = "#fff";
        event.target.style.color = "#337ab7";
    }, false);

// Next, events intended for the drop target - the Diagram div

    var div = document.getElementById("attribute-chart");
    div.addEventListener("dragenter", function (event) {
        // Here you could also set effects on the Diagram,
        // such as changing the background color to indicate an acceptable drop zone

        // Requirement in some browsers, such as Internet Explorer
        event.preventDefault();
    }, false);

    div.addEventListener("dragover", function (event) {
        // We call preventDefault to allow a drop
        // But on divs that already contain an element,
        // we want to disallow dropping

        if (event.target.className === "dropzone") {
            // Disallow a drop by returning before a call to preventDefault:
            return;
        }

        // Allow a drop on everything else
        event.preventDefault();
    }, false);

    div.addEventListener("dragleave", function (event) {
        // reset background of potential drop target
        if (event.target.className == "dropzone") {
            event.target.style.background = "";
        }
    }, false);

    div.addEventListener("drop", function (event) {
        // prevent default action
        // (open as link for some elements in some browsers)
        event.preventDefault();

        // Dragging onto a Diagram
        if (this === attributeDiagram.div) {
            attributeDiagram.startTransaction('new node');
            //
            // attributeDiagram.model.addNodeData({
            //     id: dragged.getAttribute('data-id'),
            //     name: dragged.textContent
            // });


            $.ajax({
                    method: 'POST',
                    url: Routing.generate('admin_attribute_diagram_model', {id: dragged.getAttribute('data-id')}),
                data: {'diagramData': attributeDiagram.model.toJson()},
                beforeSend: function () {
                    $spinner.trigger('spinner-on');
                },
                    success: function (response) {
                        attributeDiagram.model = go.Model.fromJson(response);
                        currentAttributeModel = response;
                        // if (!isAttributeEditor) {
                        //     addStepAttribute({'id': dragged.getAttribute('data-id'), 'name': dragged.textContent});
                        // }
                        //dragged.remove();
                        $spinner.trigger('spinner-off');
                    }
                }
            );

        }

        attributeDiagram.commitTransaction('new node');
        // If we were using drag data, we could get it here, ie:
        // var data = event.dataTransfer.getData('text');
    }, false);
}

function attributeAction(id, addToStep) {

    var addToStep = addToStep || false;
    $('#attribute-chart').attr('data-id', id);
    $.ajax({
            method: 'POST',
            url: Routing.generate('admin_attribute_diagram_model', {id: id}),
            beforeSend: function () {
                $spinner.trigger('spinner-on');
            },
            success: function (response) {
                currentAttributeModel = response;
                attributeDiagram.model = go.Model.fromJson(response);
                $spinner.trigger('spinner-off');
            },
            always: function () {
                $spinner.trigger('spinner-off');
            }
        }
    );
    return false;
}

function removeFromChart(node) {
    removeChildNodes(node);
    //addNodeToList(node);
}

function removeChildNodes(node) {
    var childNodes = node.findTreeChildrenNodes();
    for (var it = childNodes.iterator; it.next();) {
        var part = it.value;
        if (part.findLinksInto().count < 2) {
            //addNodeToList(part);
            removeChildNodes(part);
            attributeDiagram.remove(part);
        }
    }
}

function addNodeToList(node) {
    var html = '<li class="last-created">' +
        '<div ondblclick="attributeAction(' + node.data.id + ')" data-id="' + node.data.id + '" class="draggable" draggable="true">' + node.data.name + '</div>' +
        '</li>';
    $('#attribute-list').prepend(html);
}
function handleAttributeChartSaving(diagram) {
    $('#submit-attribute-chart').on('click', function () {
        l = Ladda.create($(this)[0]);
        $.ajax({
                method: 'POST',
                url: Routing.generate('admin_attribute_chart_save'),
                data: {'diagramData': diagram.model.toJson()},
            beforeSend: function () {
                l.start();
            }, success: function (response) {
                delayedCallback(function () {
                    l.stop();
                }, 1000);
                }
            }
        );
        return false;
    });
}

function openAttributeFormModal(diagram, node, isNew) {
    var isNew = isNew || false;
    var node = node || null;
    var $attributeModal = $('#attribute-modal');
    var nodeId = null;
    if (node) {
        nodeId = node.id;
    }

    $spinner.trigger('spinner-on');

    changeAttributeView($('#btn-attr-definition'));

    $('#step-modal').removeClass('formGenerate overflow-hidden');

    $.get(Routing.generate('admin_attribute_editor', {
        'id': nodeId
    }), node, function (response) {
        $('#attribute-modal #editor-content').html(response);
        initSelect2();
        $attributeModal.modal();
        $attributeModal.attr('data-id', nodeId);

        if (isNew) {
            $('#submit-and-drag').removeClass('hidden');
        }

        $('#attribute-edit-form #submit-and-drag').on('click', function () {
            handleAttributeSaving(diagram, node, true);
        });

        $('#attribute-edit-form #submit').on('click', function () {
            handleAttributeSaving(diagram, node);
        });
        handleGoToParent();
        return false;
    });
}

function handleBtnGroup() {

    $('#attribute-modal').find('.btn-step-view').on('click', function () {

        changeAttributeView($(this));

    })
}

function changeAttributeView($t) {

    var $attributeModal = $('#attribute-modal');

    $attributeModal.find('.btn-step-view').removeClass('active');
    $t.addClass('active');

    $attributeModal.find('.step-editor-content').addClass('hidden');
    $attributeModal.find('#' + $t.attr('data-content') + '-content').removeClass('hidden');
}

function handleGoToParent() {
    $('body').on('click', '.go-to-parent', function(){
        var attributeId = $(this).attr('data-id');
        $('#attribute-modal').modal('hide');
        attributeAction(attributeId);
        return false;
    });
}

function initSelect2() {
    $('.select2-deselect').select2({
        allowClear: true,
        placeholder: ''
    });
}

function handleAttributeSaving(diagram, node, drag) {

    var drag = drag || false;
    $.ajax({
            method: 'POST',
            url: $('#attribute-edit-form').attr('action'),
            data: $('#attribute-edit-form').serialize(),
            success: function (response) {
                $('#error-list').remove();
                if (response.errors) {
                    $('#form-errors').append('<ul id="error-list" class="list-unstyled"></ul>');
                    $.each(response.errors, function (key, value) {
                        var errorMsg = "<li><span class='fa fa-exclamation-circle'></span>" +
                            (key != '' ? "<strong>" + $('label[for=attribute_' + key + ']').text() + "</strong>: " : "") + value + "</li>";
                        $('#attribute-edit-form #error-list').append(errorMsg);
                    });
                }
                else {
                    var id = response.id;
                    var inputId = "attribute_translations_" + $('body').attr('data-locale') + "_name";
                    var data = {
                        "name": $("#" + inputId).val()
                    };

                    if (node) {
                        updateAttrubuteNodeWithData(diagram, node, data);
                    }

                    $('#attribute-list').find('div[data-id=' + id + ']').text(data.name);
                    $("#attribute-modal").modal('hide');

                    if (drag) {
                        attributeDiagram.startTransaction('new node');
                        attributeDiagram.model.addNodeData({
                            id: id,
                            name: '(' + id + ') ' + data.name
                        });
                        attributeDiagram.commitTransaction('new node');
                    }

                }
            }
        }
    );
    return false;
}

function updateAttrubuteNodeWithData(diagram, node, newData) {
    diagram.startTransaction('update node');
    var data = attributeDiagram.model.findNodeDataForKey(node.id);
    if (data !== null) {
        $.each(newData, function (key, value) {
            attributeDiagram.model.setDataProperty(data, key, '('+data.id+') '+value);
        });
    }
    diagram.commitTransaction('update node');
}

$('#attribute-search-input').on('focus', function (e) {
    attributeSearchValue = $(this).val();
});

function attributeSearchHandle(addToStep) {
    var addToStep = addToStep || false;
    var currentRequest = null;
    $('#attribute-search-input').on('keyup', function (e) {
        var value = $(this).val();
        if (value !== '') {
            if (value == jQuery.data(this, "lastvalue")) {
                return false;
            }
            jQuery.data(this, "lastvalue", $(this).val());

            delayedCallback(function () {
                $spinner.trigger('spinner-on');
                currentRequest = $.ajax({
                        method: 'GET',
                    url: Routing.generate('admin_attribute_search', {
                        value: value,
                        addToStep: addToStep
                    }),
                        beforeSend: function () {
                            if (currentRequest != null) {
                                currentRequest.abort();
                            }
                        },
                        success: function (response) {
                            $('.attribute-search-result').remove();
                            $('.attribute-root').addClass('hidden');
                            $('#attribute-list').prepend(response);
                            $spinner.trigger('spinner-off');
                        }
                    }
                )
            }, 500);

        }
        else {
            $('.attribute-search-result').remove();
            $('.attribute-root').removeClass('hidden');
        }


    });

    // $('#attribute-search-input').on('change', function () {
    //     if ($(this).val() == '') {
    //         $('.attribute-root').removeClass('hidden');
    //         $('.attribute-search-result').remove();
    //     }
    // });
}