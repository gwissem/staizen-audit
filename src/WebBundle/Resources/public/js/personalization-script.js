(function($, window, document) {

    $(function() {
        var $menuElements = $('.page-sidebar-menu > li'),
            ids = [];

        $menuElements.on('click', function () {

            // Jest delay, ponieważ menu od razu nie dodaje klasy open
            setTimeout(function () {
                ids = [];
                $.each($menuElements, function () {
                    if($(this).hasClass('open')){
                        ids.push($(this).data('id'));
                    }
                });
                
                updateOption('sidebar-nav-items', ids).done(function(data) {

                }).fail(function (data) {
                    console.error('Błąd przy personalizacji. Msg: ' +data.statusText);
                })
            }, 200);

        });

        handleFilterToggle();
        handleToolColWidth();
    });

    function handleToolColWidth() {
        $(document).on('toolColResize', function (e, data) {

            var idTool = $('#tool-table').attr('data-tool-id');

            var obj = {};

            obj[data.getAttribute('data-column-name')] = data.offsetWidth;

            updateOption('$tool_col_width_' + idTool, obj).done(function(data) {

            }).fail(function (data) {
                console.error('Błąd przy personalizacji. Msg: ' +data.statusText);
            })

        });

    }

    function handleFilterToggle() {

        $('.filter-toggle').on('click', function (e) {
            e.preventDefault();

            var $t = $(this);

            setTimeout(function (e) {
                var idTool = $('#tool-table').attr('data-tool-id');

                updateOption('$filter_toggle_tool_' + idTool, $t.hasClass('active') ).done(function(data) {

                }).fail(function (data) {
                    console.error('Błąd przy personalizacji. Msg: ' +data.statusText);
                })

            }, 200);
        });
    }

    function updateOption(optionName, optionData) {
        return $.ajax({
            url: Routing.generate('user_personalization_update', {option : optionName}),
            type: "POST",
            data: {personalization: optionData}
        });
    }

}(window.jQuery, window, document));