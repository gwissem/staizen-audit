var setAttributeValue = function() {},
    getAttributeValue,
    copyAttributeStructure,
    nextStep,
    openModal,
    focusPopup,
    addNote,
    taskOverview,
    changeServiceStatus,
    reloadCurrentForm = function () {};

var reserveAndGetTask = function () {
    // Override...
};

// var shortFunction;

var __debug = function() {};

var SUPER_USER = {
    reserveForce: false
};

$.extend($.ui.autocomplete, {
    escapeRegex: function (value) {
        return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&").replaceSpecialChars();
    },
    filter: function (array, term, useFilter) {

        useFilter = useFilter || false;

        if(useFilter === true) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");

            return $.grep(array, function (value) {
                return (matcher.test(value.value) || matcher.test(value.label) );
            });
        }
        else {
            /** przepuszczam wszystko co zwróci EMAPI */
            return array;
        }

    }

});


var _globalSubscriberEvents = {
    addScript: function (uId, callback) {

        var $ele = $('[data-uid="' + uId + '"]');

        if ($ele.length) {
            if (typeof callback === "function") callback($ele);
        }
    }
};

var _f = {
    toggleVisible: function (ids) {
        if (!Array.isArray(ids)) ids = [ids];

        $.each(ids, function (i, id) {
            $('.grid-stack-item[data-id="' + id + '"]').toggle();
        })
    },
    updateGridItems: function () {
        gridstackItemsModule.updateGridItemsPosition();
    },
    hide: function (ids) {
        if (!Array.isArray(ids)) ids = [ids];

        if(ids.length) {
            var $items = null;
            $.each(ids, function (i, id) {

                $items = $('.grid-stack-item[data-id="' + id + '"]');

                $items.each(function(i, ele) {
                    var _id = $(ele).attr('data-unique-id');
                    gridstackItemsModule.hideItem(_id);
                });

                $items.attr('data-is-visible', "false");

            });

            tabkeyModule.setTabIndex();

            gridstackItemsModule.refreshVisibleInVirtualForm();
            gridstackItemsModule.updateLocationOfItems();

        }
    },
    show: function (ids) {
        if (!Array.isArray(ids)) ids = [ids];

        if(ids.length) {
            var $items = null;
            $.each(ids, function (i, id) {

                $items = $('.grid-stack-item[data-id="' + id + '"]');

                $items.each(function(i, ele) {
                    var _id = $(ele).attr('data-unique-id');
                    gridstackItemsModule.showItem(_id);
                });

                $items.attr('data-is-visible', "true");

            });

            tabkeyModule.setTabIndex();

            gridstackItemsModule.refreshVisibleInVirtualForm();
            gridstackItemsModule.updateLocationOfItems();
        }

    },
    wait: function (time, fun) {
        time = time || 100;
        setTimeout(function () {
            fun();
        }, time);
    },
    disable: function (ids, disabled) {
        if (!Array.isArray(ids)) ids = [ids];
        if (typeof disabled === "undefined") disabled = true;
        $.each(ids, function (i, id) {
            $('.grid-stack-item[data-id="' + id + '"]').find('button,input,textarea').prop('disabled', disabled);
        })
    },
    getValueByPath: function(path) {

        var $el = $('#form-generator-box').find('[data-attribute-path="' + path + '"]');

        if($el.length) {
            var _type = $el[0].type;

            if(_type === "radio" || _type === "checkbox") {
                return _f.checkedValue($el.parent());
            }
            else {
                return $el.val();
            }
        }

        return false;
    },
    checkedValue: function ($t) {
        var $el = $t.find('input[type="radio"]:checked');
        if ($el.length === 0) {
            $el = $t.find('input[type="checkbox"]');
            if ($el.length) {
                if(!$el.is(":checked")) {
                    return 0;
                }
            }
            else {
                return null;
            }
        }
        return $el.val();
    },
    openModal: function (message, confirm, cancel, onlyInfo, asyncContent, options) {

        var defaultOptions = {
            'buttonConfirm' : {'html' : '<i class="fa fa-check"></i> Tak'},
            'buttonCancel' : {'html' : '<i class="fa fa-times"></i> Nie'},
            'hideAfterConfirm': true
        };

        var _opts = $.extend(defaultOptions, options);

        var $customProcessModal = $('#custom-process-modal');
        $customProcessModal.find('.modal-dialog').removeClass('modal-lg');

        if(typeof onlyInfo === "undefined") onlyInfo = false;

        message = message || "";

        if(typeof message === "object") {
            $customProcessModal.find('.modal-header .modal-label').html(message.header);
            $customProcessModal.find('.modal-body').html(message.body);

            if(message.bigSize) {
                $customProcessModal.find('.modal-dialog').addClass('modal-lg');
            }

        }
        else {
            $customProcessModal.find('.modal-header .modal-label').html('Komunikat');
            $customProcessModal.find('.modal-body').html(message);
        }

        if(typeof asyncContent !== "undefined") {
            if(confirm){
                $customProcessModal.find('.btn').prop('disabled',true);
            }
            $customProcessModal.find('.modal-body').html(miniLoaderTemplate);

            $.when(asyncContent.promise()).then(function (result) {
                $customProcessModal.find('.modal-body').html(result);
                if(confirm){
                    $customProcessModal.find('.btn').prop('disabled',false);
                }
            });

        }

        $customProcessModal.removeClass('only-info');
        $customProcessModal.find('#custom-btn-confirm').html(_opts.buttonConfirm.html);
        $customProcessModal.find('#custom-btn-cancel').html(_opts.buttonCancel.html);

        if(onlyInfo) {
            $customProcessModal.addClass('only-info');

            $customProcessModal.find('#custom-btn-ok').off('click').on('click', function (e) {
                e.preventDefault();
                $customProcessModal.modal('hide');

                if(typeof confirm === "function") {
                    confirm($customProcessModal);
                }

            });

        }
        else {

            if(typeof confirm === "function") {
                $customProcessModal.find('#custom-btn-confirm').off('click').on('click', function (e) {

                    e.preventDefault();

                    if(_opts.hideAfterConfirm) {
                        $customProcessModal.modal('hide');
                    }

                    confirm($customProcessModal, $(this));

                });
            }

            if(typeof cancel === "function") {

                $customProcessModal.find('#custom-btn-cancel').off('click').on('click', function (e) {
                    e.preventDefault();
                    $customProcessModal.modal('hide');
                    $customProcessModal.off('hidden.bs.modal');
                    cancel($customProcessModal, $(this));
                });

                $customProcessModal.off('hidden.bs.modal').on('hidden.bs.modal', function () {
                    cancel($customProcessModal, $(this));
                })

            }

        }

        $customProcessModal.modal('show');

        return $customProcessModal;
    },
    refreshMap: function() {
        $('#atlas-map-step').trigger('refresh-map');
    },
    addAttributes: function (_this, attributes) {

        /**
         * attributes =>  [{ 'key' : 'value'}, ... ]
         */

        $.each(attributes, function (key, value) {
            _this.attr(key, value);
        });

    },
    updateAttributeByFilter: function (_this, data) {

        if(_this) {
            if(typeof(_this.data('filterMapping')) !== "undefined") return false;

            _this.data('filterMapping', data);
            /**
             * Na przykład: (nazwa kolumny, attr_path)
             * data: {
             *    firstname: '81,342,64',
             *    lastname: '81,342,66'
             * }
             * */
        }
    },
    refreshEmailPreview: function () {
        if(typeof CustomEvent !== 'undefined') {
            window.document.body.dispatchEvent(new CustomEvent("refresh-preview-email"));
        }
    },
    disableNext: function() {
        $('.generating-form-submit').addClass('force-disabled').prop('disabled', true);
    },
    enableNext: function() {
        $('.generating-form-submit').removeClass('force-disabled').prop('disabled', false);
    },
    formTooltip: function (_this, _title, opts) { /** _f.formTooltip(_this, 'Test Title', {placement: 'left'}); */

        _this.tooltip({
            container: "#page-main-content",
            html: true,
            title: _title,
            placement: (opts && opts.placement) ? opts.placement : "top",
            template: '<div class="tooltip timeline-tooltip new-custom-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
        });

    },
    saveLocationInCase: function (locationPath, locationData) {

        var id = null;

        if(typeof _dashboardService !== "undefined") {
            id = _dashboardService.getCurrentTicket('id');
        }

        return $.ajax({
            url: Routing.generate('save_location_in_case'),
            type: "POST",
            data: {
                locationPath: locationPath,
                locationData: locationData,
                groupProcessId: (id) ? _dashboardService.getCurrentTicket('groupProcessId') : null
            },
            beforeSend: function(xhr){
                if(typeof _processToken !== "undefined" && _processToken) {
                    xhr.setRequestHeader('Process-Token', _processToken);
                }
            },
            error: function (error) {
                handleAjaxResponseError(error);
            }
        });

    }
};

$.fn._change = function (fun) {
    if (this[0].tagName === "DIV") {
        this.find('input').on('change', fun);
    }
    else {
        this.on('change', fun);
    }
};

$.fn._click = function (fun) {
    if (this[0].tagName === "DIV") {
        var i = this.find('input');
        if(i.length) {
            i.on('click', fun);
        }
        else {
            this.on('click', fun);
        }
    }
    else {
        this.on('click', fun);
    }

};

$.fn._keyup = function (fun) {
    this.on('keyup', fun);
};

(function ($, window, document) {

        if(checkIfIE()) return false;

        var sessionDashboard,
            webSocket = null,
            heartBeat,
            /**
             * @type {DashboardSocket|null}
             */
            _socket = null,
            _MAP = null;

        var $window,
            $body,
            $mainContent,
            ticketContainer,
            casePanelContainer,
            historyTicketContainer,
            activeTicketId = null,
            $formPlacement = null,
            $caseInfo = null,
            $addCaseInfo = null,
            $formPlacementDrop = null,
            $currentTaskContainer,
            $caseSearchWrapper,
            $caseSearchButton,
            $caseSearchButtonFilter,
            caseSearchLadda,
            $caseSearchInput,
            $notificationsContainer,
            $notificationModuleWrapper,
            $abandonProcessModal,
            laddaNotification,
            $reservedPopup,
            $kickUserPopup,
            ticketListManager,
            timeLineManager,
            $modeSwitchContainer,
            $chatBar,
            $timelineContainer,
            $updatePositionModal,
            $notificationsTypeContainer,
            intervalsToClean = [],
            $caseNote,
            $rightWidget,
            emailsForCheck = [],
            xhrFilterTasksInput = null,
            promiseRefresh = null,
            historyValueViewer = null,
            $blogPage,
            $efficiencyRanking,
            $medicalCaseModal;

        var _status,
            /**
             * @type DashboardService
             */
            _dashboardService,
            isSimulation = false,
            isSimulationStarted = false,
            isScenarioRun = false,
            isPublicDashboard = false,
            isPartnerInterface = false,
            isCaseEditorPanel = false,
            wasReloaded,
            reloadingTimeout,
            multiTemplates = {},
            _refreshTaskModule,
            copyingOfAttribute = false;

        /** MODULES */

        var serviceSummaryTooltipModule = ServiceSummaryTooltipModule({
            nextStep: onNextStep,
            getTask: tryGetTask
        });

        var dynamicEditAttributeModule = DynamicEditAttributeModule(),
            parserInputsModule = ParserInputsModule(),
            publicDashboardModule = null,
            emailPreviewModule = null,
            caseNoteModule = null,
            partnerInterfaceModule = null,
            inboxEditorModule = null,
            dynamicalWidgetModule = null,
            postponeProcessModule = null,
            adminApiModule = null,
            /**
             * @type CaseTodoManagement
             */
            caseTodoManagement = null;

        /** END MODULES */

        _f['nextStep'] = function (variant, omitValidation, skipStepProcedure, skipErrors, skipPostpone) {

            variant = variant || null;
            onNextStep(variant, omitValidation, skipStepProcedure, skipErrors, skipPostpone);

        };

        _f['call'] = callNumber;

        _f['execProcedure'] = _execProcedureFromFront;

        function _execProcedureFromFront(query, onSuccess, onError, onComplete) {
            _socket.execProcedure(query, onSuccess, onError, onComplete);
        }

        /** Reset options of global datepicker - variable datePickerOptions is in main.js */

        datePickerOptions.onSelect = function (d1, d2, event) {
            if(event.$el[0].disableTrigger) return false;

            delayedCallbackAttribute(function () {
                event.$el.trigger('change');
            }, 100);
        };

        function addApi(name, module) {

            if(typeof dashboardApi !== "undefined") {
                dashboardApi[name] = module;
            }

        }

        $(function () {

            /** Init variaibles */
            initVariables();

            /** Init Dashboard Layout */
            initLayout();

            /** Handle events */
            mainEventHandle();

            /** Ładowanie bloga */
            loadPosts();

            /** Efficiency rank loading */
            loadEfficiencyRank();


            webSocketHandle(function () {

                if(isPublicDashboard) {
                    handlePublicDashboard();
                }

                if(isPartnerInterface) {
                    PartnerInterfaceTabModule(_dashboardService, function (_id) {
                        /** Ładowanie formularza */
                        _socket.getTask(_id, function (result) {
                            createForm(result);
                        }, null, false);
                    });
                }

                if(isCaseEditorPanel) {

                    CaseEditorPanelModule(_socket, _dashboardService, function (_id, finishGetTask) {
                        /** Ładowanie formularza */
                        _socket.getTask({id: _id, previewMode: true}, function (result) {
                            createForm(result);
                            finishGetTask();
                        }, null, false);
                    });

                }

                addApi('socket', _socket);

            });

            if(!isPublicDashboard && !isCaseEditorPanel) {
                handleATPTestCases();
                vinHeadersInfoModal();
                ticketsListHandle();
                timelineHandle();
                tasksFromTelephonyHandle();
                adminTaskBarHandle();
                initHistoricalTasksOnTabClick();
                programCardsInfoModal();

                if(!isPartnerInterface) {
                    searchCaseInput();
                }

                setInterval(function () {
                    reportUserAtInstance()
                }, 45000);
            }

            handleWindowHeightChange();
            windowResizeHandle();

            addApi('dashboardService', _dashboardService);
            addApi('adminApi', adminApiModule);

        });

        taskOverview = function(id){
            loadTaskOverview(id);
        };

        /** Zainicjowanie zmiennych */

        function initVariables() {

            $window = $(window);
            $body = $('body');
            isPublicDashboard = $body.hasClass('public-mode');
            isPartnerInterface = $body.hasClass('partner-interface-body');
            isCaseEditorPanel = $body.hasClass('case-editor');

            ticketContainer = $('#ticket-groups, #user-');
            casePanelContainer = $('#case-panel-management');
            historyTicketContainer = $('#history-ticket-groups');
            $formPlacement = $('.form-placement');
            $formPlacementDrop = $formPlacement.find('.drop');
            $currentTaskContainer = $('.category.currentTask');
            $caseSearchWrapper = $('.case-search-wrapper');
            $caseSearchButton = $('#case-search-button');
            $caseSearchButtonFilter = $('#toggle-searcher-filters');
            caseSearchLadda = ($caseSearchButton.length) ? Ladda.create($caseSearchButton[0]) : null;
            $caseSearchInput = $('#case-search');
            $notificationsContainer = $('#customAccordion');
            $notificationModuleWrapper = $('.notification-module .notifications-outer-wrapper');
            $notificationsTypeContainer = $('.notification-type-wrapper');
            $reservedPopup = $('.reserved-popup');
            $kickUserPopup = $('.kick-user-popup');
            $modeSwitchContainer = $('.mode-switch-container');
            $chatBar = $(window.parent.document.getElementById('cometchat'));
            $timelineContainer = $('.timeline-container');
            $abandonProcessModal = $('#abandon-process-modal');
            $caseInfo = $('#case-info');
            $addCaseInfo = $('#additional-case-info');
            $updatePositionModal = $('#update-position-map-modal');
            $caseNote = $('#case-notes');
            $mainContent = $('#page-main-content');
            $rightWidget = $('#right-widget');
            $blogPage = $('.blog-page');
            $efficiencyRanking = $('#efficiency-ranks-section');
            $medicalCaseModal = $('#medical-case-modal');

            /**
             * @typedef {Object} DashboardService
             * @property {Function} isPushCaseMode
             * @property {Function} isPreviewStep
             * @property {Function} setPreviewStep
             * @property {Function} isPublicDashboard
             * @property {Function} setPublicDashboard
             * @property {Function} isReadOnlyDashboard
             * @property {Function} setReadOnlyDashboard
             * @property {Function} getCurrentTicket
             * @property {Function} setCurrentTicket
             * @property {Function} setCurrentTicketId
             * @property {Function} isCurrentTicket
             * @property {Function} dispatchDashboardEvent
             * @property {Function} setNextStepLadda
             * @property {Function} startRequest
             * @property {Function} stopRequest
             * @property {Function} blockNextStep
             * @property {Function} unBlockNextStep
             * @property {Function} canNextStep
             */

            _dashboardService = function () {

                var _isPreview = false,
                    _currentTicket = null,   // id, stepId, groupProcessId, rootId
                    _isPublicDashboard = false,
                    _isReadOnly = false,
                    _nextStep = null,
                    _nextStepLadda = null,
                    _countRequests = 0,
                    _countNotValidAttributes = 0;

                __debug = function() {
                    console.log(_nextStep);
                    console.log(_nextStepLadda);
                    console.log('_countRequests');
                    console.log(_countRequests);
                    console.log('_countNotValidAttributes');
                    console.log(_countNotValidAttributes);
                };

                var _getCurrentTicket = function (prop) {
                    if(typeof prop !== 'undefined' && _currentTicket !== null && _currentTicket.hasOwnProperty(prop)) {
                        return _currentTicket[prop];
                    }
                    return _currentTicket;
                };

                function toggleSpinnerNextStep() {
                    if(_nextStepLadda) {
                        if(_countRequests === 0) {
                            _nextStepLadda.stop();
                            checkBlockNextStep();
                        }
                        else if(!_nextStepLadda.isLoading()) {
                            _nextStepLadda.start();
                        }
                    }
                }

                function checkBlockNextStep() {

                    if(!_nextStep || _nextStep.length <= 0) return;

                    if(_countNotValidAttributes === 0 && !_nextStep.hasClass('force-disabled')) {

                        if(_nextStepLadda) {
                            if(!_nextStepLadda.isLoading()) {
                                _nextStep.prop('disabled', false);
                            }
                        }
                        else {
                            _nextStep.prop('disabled', false);
                        }

                    }
                    else {
                        _nextStep.prop('disabled', true);
                    }
                }

                _f['getCurrentTicket'] = _getCurrentTicket;

                return {
                    dispatchDashboardEvent: function (name, data) {
                        if(name === 'setAttributeValue') {
                            if(typeof CustomEvent !== 'undefined') {
                                document.dispatchEvent(new CustomEvent("setAttributeValue", { 'detail': data }));
                            }
                        }
                    },
                    isPushCaseMode: function () {
                        return ($('body.operational-page').hasClass('push-case-mode'));
                    },
                    isPreviewStep: function () {
                        return _isPreview;
                    },
                    setPreviewStep: function (status) {
                        _isPreview = status;
                    },
                    isPublicDashboard: function () {
                        return _isPublicDashboard;
                    },
                    setPublicDashboard: function (mode) {
                        _isPublicDashboard = mode;
                    },
                    isReadOnlyDashboard: function () {
                        return _isReadOnly;
                    },
                    setReadOnlyDashboard: function (mode) {
                        _isReadOnly = mode;
                    },
                    getCurrentTicket: _getCurrentTicket,
                    setCurrentTicket: function (_ticket) {
                        _currentTicket = _ticket;
                    },
                    setCurrentTicketId: function (_id) {
                        if(_currentTicket !== null) {
                            _currentTicket.id = _id;
                        }
                    },
                    setNextStepLadda: function($btn) {

                        if($btn === null) {
                            _nextStepLadda = null;
                        }
                        else {
                            if($btn.length) {
                                _nextStep = $('.generating-form-submit.next-button');
                                _nextStepLadda = Ladda.create(_nextStep[0]);
                            }
                            else {
                                _nextStep = $btn;
                                _nextStepLadda = Ladda.create($btn[0]);
                            }
                        }

                    },
                    canNextStep: function() {
                        return (!_nextStep.is(":disabled"));
                    },
                    blockNextStep: function( ) {
                        _countNotValidAttributes++;
                        checkBlockNextStep();
                    },
                    unBlockNextStep: function(all) {

                        all = (typeof all === "undefined") ? false : all;

                        if(all) {
                            _countNotValidAttributes = 0;
                        }
                        else {
                            _countNotValidAttributes--;
                        }

                        if(_countNotValidAttributes < 0) {
                            _countNotValidAttributes = 0;
                        }

                        checkBlockNextStep();

                    },
                    isCurrentTicket: function () {
                        return (_currentTicket !== null);
                    },
                    startRequest: function () {
                        _countRequests++;
                        toggleSpinnerNextStep();
                    },
                    stopRequest: function () {
                        _countRequests--;

                        if(_countRequests < 0) {
                            _countRequests = 0;
                        }

                        toggleSpinnerNextStep();
                    }
                }

            }();

            /** INIT MODULES **/

            validatorModule.setDashboardService(_dashboardService);

            serviceSummaryTooltipModule.setDashboardService(_dashboardService);

            ticketListManager = ticketManager({
                setHeightOfTicketGroups: setHeightOfTicketGroups,
                loadTaskOverview: loadTaskOverview
            });

            timeLineManager = TimeLineManager();

            dynamicalWidgetModule = DynamicalWidgetModule(_dashboardService);

            inboxEditorModule = EmailInboxEditorModule({
                _addEmailNote: function (note, position, id, opts) {
                    caseNoteModule.appendNote(note, position, id, opts);
                },
                appendEmail: function(id) {
                    emailsForCheck.push(id);
                }
            });

            caseNoteModule = CaseNoteModule(inboxEditorModule, _dashboardService, {
                canResendEmail: true,
                canReplyChat: true
            });

            emailPreviewModule = EmailPreviewModule(inboxEditorModule);

            postponeProcessModule = PostponeProcessModule(_dashboardService, function(message){

                finishForm(
                    'Odroczenie zadania',
                    message,
                    'fa-clock-o',
                    false,
                    true
                );

                activeTicketId = null;

                if(_dashboardService.isPublicDashboard()) {
                    return;
                }

                ticketListManager.remove(_dashboardService.getCurrentTicket('id'), true);
                ticketListManager.finishTask();
                _dashboardService.setCurrentTicket(null);
                _socket.cancelReserveCurrentTask();

                if(caseTodoManagement) {
                    caseTodoManagement.loadTask();
                }

            });

            /** END INIT MODULES */

            _status = function () {

                var loading = false,
                    loaderWrapper = $('.form-placement-wrapper .loader-wrapper'),
                    dropForm = $('.form-placement-wrapper .drop'),
                    countDown = $('.center-countdown');

                function onStartLoading() {
                    loaderWrapper.stop().fadeIn(500);
                    // dropForm.css('opacity', 0.5);
                    if ($('.finish-popup').length > 0) {
                        $('.finish-popup').fadeOut();
                    }
                }

                function onEndLoading() {
                    loaderWrapper.stop().fadeOut(500);
                    dropForm.css('opacity', 1);
                    $('#customAccordion').show();
                }

                return {
                    isLoading: function () {
                        return (loading)
                    },
                    startLoading: function () {
                        onStartLoading();
                        loading = true;
                    },
                    stopLoading: function () {
                        onEndLoading();
                        loading = false;
                    },
                    startCounting: function () {
                        countDown.fadeIn(200);
                    },
                    stopCounting: function () {
                        countDown.fadeOut(200);
                    }
                }
            }();

            reloadCurrentForm = function () {

                _socket.getTask(_dashboardService.getCurrentTicket('id'), function (result) {
                    createForm(result);
                });

            };

            adminApiModule = (function () {

                return {
                    reActiveProcessInstance: function (processInstanceId) {

                        $.ajax({
                            url: Routing.generate('operation_dashboard_active_process_instance', {processInstanceId: processInstanceId}),
                            type: "GET",
                            success: function () {

                                _socket.getTask(processInstanceId, function (result) {
                                    createForm(result);
                                });

                            },
                            error: function (error) {

                                if(error.statusText !== "abort") {
                                    handleAjaxResponseError(error);
                                }

                            }

                        })

                    }
                }

            })();

        }

        /** ------------------------------------- HANDLERS ---------------------------------- */


        function  loadEfficiencyRank() {
                if($('#rank-holder-recent').length){
                    $.get(Routing.generate('user-efficiency-rank-recent')).then(function (response){
                        $('#rank-holder-recent').html(response.html);
                    });
                }
            if($('#rank-holder-historical').length){
                $.get(Routing.generate('user-efficiency-rank-month')).then(function (response){
                    $('#rank-holder-historical').html(response.html);
                });
            }
            if($('#statistic-holder-dashboard').length){
                $.get(Routing.generate('user-efficiency-stats-global')).then(function(response){
                    $('#statistic-holder-dashboard').html(response.html);
                });
            }
        }
        function loadPosts() {
            $('.operational-page').on('click', "ul.pagination a", function (e) {
                $('#bottom-spinner').show();
                e.preventDefault();
                $.get($(this).attr('href'), function (html) {
                    $('.blog-page').replaceWith(
                        $(html).find('.blog-page')
                    );
                });
            });
        }

        function handlePublicDashboard() {
            "use strict";

            publicDashboardModule = PublicDashboardModule(_socket, _dashboardService);

            if(typeof _processToken === "undefined" || !_processToken) {
                if(typeof _processInstanceId === "undefined" || !_processInstanceId) {
                    toastr.error("Nie znaleziono Token'a.", "Błąd", {timeOut: 7000});
                }
                else {

                    _dashboardService.setPublicDashboard(true);
                    _dashboardService.setReadOnlyDashboard(true);

                    _processToken = _subToken;

                    publicDashboardModule.getCase(_processToken, function (result) {

                        var processInfo = result.processInfo;

                        /** Ten ticket jest aktywny */
                        if (processInfo.id === activeTicketId) return false;

                        activeTicketId = processInfo.id;

                        _dashboardService.setCurrentTicket({
                            'id': processInfo.id,
                            'stepId': processInfo.stepId,
                            'groupProcessId': processInfo.groupProcessId,
                            'rootId': processInfo.rootId
                        });

                        createForm(result);

                    }, function(error) {

                    });
                    //
                    // /** Ładowanie formularza */
                    // _socket.getTask(_processInstanceId, function (result) {
                    //
                    //     console.log(result);
                    //
                    //     createForm(result);
                    //
                    // }, function(error){
                    //
                    //     if(error.responseJSON) {
                    //         customDashboardPopup.open('Brak zadania', error.responseJSON.message, false, false);
                    //     }
                    //     else {
                    //         customDashboardPopup.open('Brak zadania', error.responseText);
                    //     }
                    //
                    // }, false);

                }

                return false;
            }

            _dashboardService.setPublicDashboard(true);

            publicDashboardModule.getCase(_processToken, function (result) {

                var processInfo = result.processInfo;

                /** Ten ticket jest aktywny */
                if (processInfo.id === activeTicketId) return false;

                activeTicketId = processInfo.id;

                _dashboardService.setCurrentTicket({
                    'id': processInfo.id,
                    'stepId': processInfo.stepId,
                    'groupProcessId': processInfo.groupProcessId,
                    'rootId': processInfo.rootId
                });

                createForm(result);

            }, function(error) {

                if(error.responseJSON) {
                    customDashboardPopup.open('Brak zadania', error.responseJSON.message, false, false);
                }
                else {
                    customDashboardPopup.open('Brak zadania', error.responseText);
                }

            });

        }

        function tryGetTask(options, afterReserveCallback, afterGetTaskCallback) {

            var data = {id: options.id, force: false};

            if(_dashboardService.getCurrentTicket('id') == data.id) {

                _socket.getTask(data.id, function (result) {

                    createForm(result);
                    reportUserAtInstance();

                    if(typeof afterGetTaskCallback === "function") afterGetTaskCallback();
                });

            }
            else {
                reserveTask(data, function () {

                    if(typeof afterReserveCallback === "function") afterReserveCallback();

                    _socket.getTask(data.id, function (result) {

                        if(result.processInfo) {
                            _dashboardService.setCurrentTicket(result.processInfo);

                            if(options.displayCase) {

                                $('.button-timeline').find('.active-task-id').html(ticketListManager.displayCaseNumber(
                                    result.processInfo.rootId,
                                    result.processInfo.stepId
                                ));

                            }

                        }
                        activeTicketId = data.id;

                        if(options.activeTimeline) {
                            timelineOnTaskClick();
                            dashboardModeSwitch();
                        }

                        if(options.hideNotes) {
                            caseNoteModule.hideNoteForm(true);
                        }

                        createForm(result);
                        reportUserAtInstance();
                        refreshCurrentTaskId();

                        if(typeof afterGetTaskCallback === "function") afterGetTaskCallback();
                    }, function () {
                        _socket.cancelReserveCurrentTask();
                    });

                }, {
                    notificationMode: options.notificationMode
                });
            }

        }

        reserveAndGetTask = function (options, afterGetTaskCallback) {

            var data = {id: options.id, force: false};

            reserveTask(data, function () {

                _socket.getTask(data.id, function (result) {

                    if(result.processInfo) {
                        _dashboardService.setCurrentTicket(result.processInfo);
                    }


                    if(!$('.button-timeline').hasClass('active-task')) {
                        $('.button-timeline').find('.active-task-id').html(ticketListManager.displayCaseNumber(result.processInfo.rootId, result.processInfo.stepId));
                        timelineOnTaskClick();
                    }

                    activeTicketId = data.id;

                    createForm(result);
                    reportUserAtInstance();
                    refreshCurrentTaskId();

                    if(typeof afterGetTaskCallback === "function") afterGetTaskCallback();

                }, function () {
                    _socket.cancelReserveCurrentTask();
                });

            }, {
                notificationMode: options.notificationMode
            });
        };

        /**
         * Obsługa odświeżania listy zadań
         * */

        var getRefreshTaskModule = (function(){

            var _intervalTime = 180000, // 3min
                _intervalRefresh,
                _canRefresh = false,
                _isFocus = false;

            function _handleEvents() {

                _isFocus = document.hasFocus() || parent.document.hasFocus();

                $(window).add($(parent.window)).on('focus', function () {

                    _setFocus();
                    _tryRefresh();

                }).on('blur', _setFocus);
            }

            function _setFocus() {

                if(window.parent) {
                    _isFocus = document.hasFocus() || window.parent.document.hasFocus();
                }
                else {
                    _isFocus = document.hasFocus();
                }

            }

            function _resetInterval() {

                if(_intervalRefresh) {
                    clearInterval(_intervalRefresh);
                }

                _intervalRefresh = setInterval(_intervalFun, _intervalTime);

            }

            function _intervalFun() {
                _canRefresh = true;
                _tryRefresh();
            }

            function _init() {

                _handleEvents();
                _intervalRefresh = setInterval(_intervalFun, _intervalTime);

            }

            function _tryRefresh(force) {

                force = force || false;

                if((_isFocus && _canRefresh) || force) {
                    _canRefresh = false;
                    _resetInterval();
                    _refresh();
                }

            }

            function _refresh() {

                var filter = $caseSearchInput.val();

                var data = {
                    currentTask: _dashboardService.getCurrentTicket('id'),
                    filter: (filter) ? filter : null
                };

                refreshTasks(data);

            }

            _init();

            return {
                refresh: _refresh
            };

        });


        /** Połączenie z WebSocketem i subskrypcja kanałów */

        function webSocketHandle(socketConnectedCallback) {

            setAttributeValue = _setAttributeValue;
            changeServiceStatus = _changeServiceStatus;
            getAttributeValue = _getAttributeValue;
            copyAttributeStructure = _copyAttributeStructure();
            addNote = _addNote;
            nextStep = _nextStep;
            previousStep = _previousStep;
            openModal = _openModal;
            focusPopup = _focusPopup;

            if(isPublicDashboard) {

                // W publicznym dashboardzie nie używamy socketu
                _socket = DashboardSocketModule(null, _status, timeLineManager, _dashboardService);
                if(typeof socketConnectedCallback === "function") {
                    socketConnectedCallback();
                }
                return false;
            }

            if (typeof WS === "undefined" || !document.WSConnectingString) {
                console.error('WebSocket is not loaded.');
                return false;
            }

            webSocket = webSocket || WS.connect(document.WSConnectingString);

            var wasDisconnect = false,
                firstConnection = true;

            /** Połączenie z WebSoscketem */
            webSocket.on("socket/connect", function (session) {

                if (wasDisconnect) {
                    toastr.success("", "Połączono z serwerem (WebSocket)", {timeOut: 5000});
                }

                sessionDashboard = session;

                if(!isPublicDashboard && !isPartnerInterface && !isCaseEditorPanel) {

                    /** Kanał przez który otrzymuje się nowe zadania */
                    session.subscribe("atlas/dashboard/case", function (uri, payload) {
                        createTicket(payload);
                    });

                    /** Kanał do zarządzania zadaniami (reserved tasks, refresh) */
                    session.subscribe("atlas/task/manager", function (uri, payload) {
                        handleSocketTaskManager(payload);
                    });

                    heartBeat = setInterval(function () {
                        session.publish("atlas/task/manager", casePingTimer.getPingCounter());
                    }, 25000);

                }

                if (firstConnection) {

                    firstConnection = false;

                    _socket = DashboardSocketModule(sessionDashboard, _status, timeLineManager, _dashboardService);

                    if(!isPublicDashboard && !isPartnerInterface && !isCaseEditorPanel) {
                        _socket.enableCaseTimer();
                    }

                    afterFirstConnection(_socket);

                    if(typeof socketConnectedCallback === "function") {
                        socketConnectedCallback();
                    }
                }
                else {
                    if(_socket) {
                        _socket.setSessionDashboard(sessionDashboard);
                    }
                }

            });

            webSocket.on("socket/disconnect", function (error) {

                sessionDashboard = null;
                if(_socket !== null) {
                    _socket.setSessionDashboard(null);
                }

                if(heartBeat) {
                    clearInterval(heartBeat);
                }

                wasDisconnect = true;
                switch (error.code) {
                    case 6:
                    {
                        toastr.error("Połączenie z serwerem zostało zerwane.", "Błąd połączenia (WebSocket)", {timeOut: 7000});
                        break;
                    }
                    case 3:
                    {

                        if(isPublicDashboard) {
                            customDashboardPopup.open('Błąd połączenia', "Wystąpił błąd w aplikacji.", false, true, false);
                        }
                        else {
                            toastr.error("Nie można nawiązać połączenia z serwerem.", "Błąd połączenia (WebSocket)", {timeOut: 7000});
                        }

                        break;
                    }
                }

                console.log("Disconnected for " + error.reason + " with code " + error.code);
            });

            initSimulations();
        }

        function afterFirstConnection(_socket) {

            $body.addClass('websocket-connected');
            // console.log('%cWebsocket Connected', 'background:#91cef3;color:#000;padding:5px');

            if(!isPublicDashboard && !isPartnerInterface && !isCaseEditorPanel) {

                /** Załadowanie modułu do wystawiania zadań */
                caseTodoManagement = CaseTodoManagement(tryGetTask, _dashboardService, _socket);

                /** Po połączeniu sprawdza, czy jest zadanie do odpalenia */

                if(typeof telephonyPickUpAction !== 'undefined' ) {
                    if(typeof CustomEvent !== 'undefined') {
                        document.dispatchEvent(new CustomEvent("telephony-pick-up", { 'detail': telephonyPickUpAction }));
                    }
                }

                if($caseSearchInput.hasClass('empty-search')) {
                    var filter = $caseSearchInput.val();

                    var data = {
                        currentTask: _dashboardService.getCurrentTicket('id'),
                        filter: (filter) ? filter : null,
                        options: getSearcherAdvancedFilters()
                    };

                    if(xhrFilterTasksInput !== null) {
                        xhrFilterTasksInput.abort();
                    }

                    caseSearchLadda.start();

                    xhrFilterTasksInput = $.ajax({
                        url: Routing.generate('operationalDashboardInterfaceRefreshTasks'),
                        type: "GET",
                        data: data,
                        success: function (result) {
                            ticketListManager.setList(result.tasks);
                        },
                        error: function (error) {
                            if(error.statusText !== "abort") {
                                handleAjaxResponseError(error);
                            }
                        },
                        complete: function () {
                            caseSearchLadda.stop();
                        }

                    });
                }

                /** Odpalenie cyklicznego odświeżania zadań - gdy nie ma PushCaseMode  */

                if(!_dashboardService.isPushCaseMode()) {
                    _refreshTaskModule = getRefreshTaskModule();
                }

            }
        }

        function initHistoricalTasksOnTabClick() {
            $('.button-history').on('click', function() {

                $('#history-ticket-groups li').remove();
                ticketListManager.getHistoricalTasks();
                setHeightOfTicketGroups();
            })
        }

        function TimeLineManager() {

            var $timeLineWrapper = $("#timeline-wrapper"),
                $topServicePanel = $(".top-service-panel"),
                prototypeFilter = $timeLineWrapper.attr('data-prototype-filter'),
                prototypeTaskBlock = $timeLineWrapper.attr('data-prototype-task-block'),
                $firstSection = $timeLineWrapper.find('#filter-1'),
                $timeLineLoader = $('#timeline-loader'),
                $tabTimeLine = $('#tab-timeline');

            function getSection() {
                return $firstSection;
            }

            function _getElement() {
                return $timeLineWrapper;
            }

            function _clearTimeLine() {
                var $section = getSection();
                $section.html('');
            }

            function _refreshTimeLine(processStepId) {

                $timeLineLoader.stop().fadeIn(500);

                var $section = getSection();
                $section.html('');
                $topServicePanel.removeClass('active');

                _socket.getTimeLine(processStepId,
                    /**
                     * @param {{timeLine: Object}} result
                     */
                    function (result) {

                    if (result.timeLine) {

                        var $html = '',
                            number = 0,
                            lastId = null,
                            activeIndex = 0,
                            lastServicePanel = null;

                        $.each(result.timeLine, function (i, task) {

                            if (task.id !== null && lastId === task.id) return true;
                            if (task.active === "1") activeIndex = number;

                            number++;

                            var taskBlock = prototypeTaskBlock;
                            taskBlock = taskBlock.replace(/__STATE__/g, (getState(task.active)))
                                .replace(/__NUMBER__/g, number)
                                .replace(/__DESC__/g, (task.stepName))
                                .replace(/__INSTANCE_ID__/g, (task.id || ''))
                                .replace(/__TITLE__/g, (task.instanceDescription || ''))
                                .replace(/__STEP_ID__/g, (task.stepId));

                            lastId = task.id;
                            $html += taskBlock;

                            if(task.stepId === "1011.010" && task.id) {
                                lastServicePanel = task;
                                $topServicePanel.find('button').attr('data-process-instance-id', lastServicePanel.id);
                                $topServicePanel.addClass('active');
                            }

                        });

                        if ($html !== "") $html += '<div class="line"></div>';
                        $section.html($html);

                        $section.find('.line').height($section.find('.timeline-block').outerHeight(true) * (number - 1));

                        _resizeTimeLineHeight();

                        $('.timeline-block').removeClass('current');
                        $('.timeline-block[data-process-instance-id="'+processStepId+'"]').addClass('current');
                        if($section.find('.timeline-block.current').length > 0){
                            activeIndex = $section.find('.timeline-block.current').index();
                        }
                        $section.scrollTop(($section.find('.timeline-block').outerHeight(true) * activeIndex) - 4);
                        $timeLineLoader.stop().fadeOut(500);

                    }

                }, function () {
                    $timeLineLoader.stop().fadeOut(500);
                });

            }

            function getState(active) {
                switch (active) {
                    case "0" : {
                        return "previous disabled";
                    }
                    case "999" : {
                        return "previous disabled";
                    }
                    case "-1" :
                    {
                        return "next disabled";
                    }
                    case "1" : {
                        return 'current';
                    }
                }
            }

            function _resizeTimeLineHeight() {

                var $timeLineContainer = $timeLineWrapper.find('.timeline-container');
                if ($timeLineContainer.length) {
                    $('#tab-timeline .filter-tabs').height($(window).height() - ($('#timeline-tab .filters-wrapper').height() + 140));
                }
            }

            function _init() {

                $tabTimeLine.find('.btn-service-panel').on('click', function(e){

                    if (_status.isLoading()) return false;
                    var $t = $(this),
                        _id = $t.attr('data-process-instance-id');

                    if(_id) {
                        tryGetTask({
                            id: parseInt(_id),
                            notificationMode: true
                        });
                    }

                });

            }

            _init();

            return {
                getElement: _getElement,
                refreshTimeLine: _refreshTimeLine,
                resizeTimeLineHeight: _resizeTimeLineHeight,
                clearTimeLine: _clearTimeLine
            }

        }

        function refreshTasks(params, callback) {

            params = typeof params !== 'undefined' ? params : {};

            if(xhrFilterTasksInput !== null) {
                xhrFilterTasksInput.abort();
            }

            if(promiseRefresh) {
                promiseRefresh.reject('manually');
            }

            if(!$caseSearchInput.hasClass('empty-search') && !params.filter) {

                if(!params.filter) {
                    ticketListManager.clear();
                }

                return false;
            }

            caseSearchLadda.start();

            if(params.filter) {
                params.filter = params.filter.trim();
            }

            params.options = getSearcherAdvancedFilters();

            xhrFilterTasksInput = $.ajax({
                url: Routing.generate('operationalDashboardInterfaceRefreshTasks'),
                type: "GET",
                data: params,
                success: function (result) {

                    promiseRefresh = ticketListManager.updateTasks(result.tasks);

                    $.when(promiseRefresh).then(function () {

                        caseSearchLadda.stop();

                        if(typeof callback === "function") {
                            callback();
                        }

                        promiseRefresh = null;

                    }, function (status) {

                        if(status !== 'manually') {

                            console.error('ERROR!' + status);

                            caseSearchLadda.stop();

                            if(typeof callback === "function") {
                                callback();
                            }

                            promiseRefresh = null;
                        }

                    });

                },
                error: function (error) {
                    caseSearchLadda.stop();
                    if(error.statusText !== "abort") {
                        handleAjaxResponseError(error);
                    }
                },
                complete: function () {

                }
            });

        }

        function handleSocketTaskManager(payload) {

            if (!payload.action) return;

            if (payload.action === "reserve_task" && payload.processInstanceId) {

                ticketListManager.remove(payload.processInstanceId, true);

                sendToParentWindow({
                    type: 'reserve-task',
                    id: payload.processInstanceId
                });

                caseTodoManagement.incomingReserveTask(payload);

            }
            else if(payload.action === "update_location_on_map") {

                if(_MAP === null) return false;

                $updatePositionModal.find('.action-confirm').attr('data-position', payload.coords.latitude + "|" + payload.coords.longitude);
                $updatePositionModal.modal('show');

            }
            else if (payload.action === "kick_user") {

                var currentTicket = _dashboardService.getCurrentTicket('id');

                resetFormView();

                ticketListManager.remove( currentTicket, true);
                ticketListManager.finishTask();
                _dashboardService.setCurrentTicket(null);
                activeTicketId = null;

                if(payload.reason && payload.reason === "timeout") {
                    toastr.info('Minął maksymalny czas jaki można spędzić na zadaniu (1h)', 'Formularz został zamknięty automatycznie', {
                        extendedTimeOut: 0,
                        timeOut: 0
                    })
                }
                else {
                    var nameOfUserWhoKicked = (payload.whoKicked) ? payload.whoKicked : "NONAME";
                    $kickUserPopup.find('[data-username]').attr('data-username', nameOfUserWhoKicked);
                    $kickUserPopup.addClass('fadeInDown').show();
                }

            }
            else if (payload.action === "refresh_tasks") {

                /** Deprecated - zadania są już odświeżane przez frontend - getRefreshTaskModule */
                //
                // var filter = $caseSearchInput.val();
                //
                // var data = {
                //     currentTask: _dashboardService.getCurrentTicket('id'),
                //     filter: (filter) ? filter : null
                // };
                //
                // refreshTasks(data);

            }

        }

        function initSimulations() {

            /**
             * eCall: document.simulationIncomingCall("0999666333", 985, 1);
             * Default: document.simulationIncomingCall("123123123001", 721, 1426, 11);
             * */

            document.simulationIncomingCall = function (number, platformNumber, user_id, platformId) {

                isSimulationStarted = true;

                return $.ajax({
                    url: Routing.generate('operationalDashboardInterfaceCreateNewTask'),
                    type: "POST",
                    data: {
                        numberAddress: number || "",
                        platformNumber: platformNumber || 721,
                        userId: user_id || 1426,
                        originalUserId: user_id || 1426,
                        idPlatform: platformId || 11,
                        idDialog: 0
                    },
                    success: function (result) {

                        var conversationEvent = result;

                        conversationEvent.isForce = (parseInt(result.idPlatform) === 69);
                        var telephonyEvent = new CustomEvent("telephony-pick-up", {'detail': conversationEvent});

                        window.document.dispatchEvent(telephonyEvent);

                    }
                });

            };

            // console.log('%csimulationIncomingCall inited', 'background:#91cef3;color:#000;padding:5px');

            /**
             * Default: document.simulationFollowUp("123123123001", 1426);
             * */

            document.simulationFollowUp = function (number, user_id) {

                isSimulationStarted = true;

                if (!sessionDashboard) throw "No connection with socket";

                $.ajax({
                    url: Routing.generate('operationalDashboardInterfaceCreateNewTask'),
                    type: "POST",
                    data: {
                        numberAddress: number || "",
                        platformNumber: 721,
                        userId: user_id || 1426,
                        originalUserId: user_id || 1426,
                        idPlatform: 11,
                        idDialog: 0,
                        stepId: '1006.001'
                    },
                    success: function (result) {

                        var conversationEvent = result;

                        conversationEvent.isForce = (parseInt(result.idPlatform) === 69);
                        var telephonyEvent = new CustomEvent("telephony-pick-up", {'detail': conversationEvent});

                        window.document.dispatchEvent(telephonyEvent);

                    }
                });

            };

        }

        /** Obsłużenie panelu administratora na dashboard */

        function adminTaskBarHandle() {

            if ($('#admin-panel').length && !isPartnerInterface) {
                var _adminPanel = initAdminPanel();
                _adminPanel.init();
            }

        }

        function initAdminPanel() {

            var $dom = $('#admin-panel'),
                $simulationForm = $('#new-task-simulation-form'),
                btnSimulationForm = ($simulationForm.length) ? Ladda.create($simulationForm.find('button')[0]) : null,
                $previewStepForm = $('#preview-step-form'),
                btnPreviewStepForm = ($previewStepForm.length) ? Ladda.create($previewStepForm.find('button')[0]) : null,
                $buttonPartnerList = $('#get-partner-list'),
                btnPartnerList = ($buttonPartnerList.length) ? Ladda.create($buttonPartnerList[0]) : null,
                $creatorPartnerForm = $('#creator-partner-form'),
                btnCreatorPartnerForm = ($creatorPartnerForm.length) ? Ladda.create($creatorPartnerForm.find('button')[0]) : null,
                $buttonAddPartner = $("#add-new-partner"),
                btnAddPartner = ($buttonAddPartner.length) ? Ladda.create($buttonAddPartner[0]) : null,
                _currentProcess = null,
                loaded = false;

            function _getProcessDefinition() {

                if (loaded) return;
                loaded = true;

                if($simulationForm.length) {
                    btnSimulationForm.start();

                    var $taskSelect = $('#task-id');

                    $.ajax({
                        url: Routing.generate('admin_process_definition_index'),
                        type: "GET",
                        success: function (data) {

                            $taskSelect.select2({
                                data: data.results
                            }).on('change', function () {
                                if ($(this).val()) {
                                    Cookies.set('DO_admin_process', $(this).val());
                                    _getStepsOfProcess($(this).val());
                                }
                            });

                            if (Cookies.get('DO_admin_process')) {
                                $taskSelect.val(Cookies.get('DO_admin_process'));
                                $taskSelect.trigger('change');
                            }
                            else {
                                _getStepsOfProcess(data.results[0].id);
                            }

                            btnSimulationForm.stop();
                        }
                    });
                }

            }

            function _getStepsOfProcess(id) {

                if (!id) return;

                btnPreviewStepForm.start();

                _currentProcess = id;
                var $stepSelect = $('#step-id');

                $.ajax({
                    url: Routing.generate('operation_dashboard_get_steps', {id: id}),
                    type: "GET",
                    success: function (data) {

                        $stepSelect.html('').select2({
                            data: data.results
                        });

                        if (Cookies.get('DO_admin_step_' + id)) {
                            $stepSelect.val(Cookies.get('DO_admin_step_' + id));
                            $stepSelect.trigger('change');
                        }

                    },
                    error: function (e) {
                        handleAjaxResponseError(e);
                    },
                    complete: function (data) {
                        btnPreviewStepForm.stop();
                    }
                });
            }

            function _renderStep($form) {

                _status.startLoading();
                btnPreviewStepForm.start();

                var id = $form.find('#step-id').val();

                $.ajax({
                    url: Routing.generate('operation_dashboard_render_step', {id: id}),
                    type: "GET",
                    success: function (data) {
                        createForm(data, true);
                    },
                    error: function (e) {
                        $formPlacementDrop.html('');
                        handleAjaxResponseError(e);
                    },
                    complete: function () {
                        _status.stopLoading();
                        btnPreviewStepForm.stop();
                    }
                });

            }

            function _handleEvents() {

                $buttonPartnerList.on('click', function (e) {
                    e.preventDefault();

                    btnPartnerList.start();

                    $.ajax({
                        url: Routing.generate('operation_dashboard_get_partners_list'),
                        type: "GET",
                        success: function (data) {

                            $buttonPartnerList.hide();
                            $creatorPartnerForm.show();

                            $('#partner-id').html('').select2({
                                data: data.partners
                            });

                        },
                        error: function (e) {
                            handleAjaxResponseError(e);
                        },
                        complete: function () {
                            btnPartnerList.stop();
                        }
                    });

                });

                $buttonAddPartner.on('click', function (e) {
                    e.preventDefault();

                    btnAddPartner.start();

                    $.ajax({
                        url: Routing.generate('operation_dashboard_add_partner'),
                        type: "POST",
                        success: function (data) {

                        },
                        error: function (e) {
                            handleAjaxResponseError(e);
                        },
                        complete: function () {
                            btnAddPartner.stop();
                        }
                    });

                });

                $creatorPartnerForm.on('submit', function (e) {
                    e.preventDefault();

                    btnCreatorPartnerForm.start();

                    activeTicketId = $('#partner-id').val();
                    _dashboardService.setCurrentTicket({
                        'id': activeTicketId,
                        'stepId': '1091.011',
                        'groupProcessId': null,
                        'rootId': null
                    });

                    /** Ładowanie formularza */

                    _socket.getTask(activeTicketId, function (result) {
                        btnCreatorPartnerForm.stop();
                        timelineOnTaskClick();
                        createForm(result);
                        caseNoteModule.hideNoteForm(true);
                    }, function(){
                        btnCreatorPartnerForm.stop();
                    }, false);

                });

                $simulationForm.on('submit', function (e) {
                    e.preventDefault();

                    var $t = $(this);
                    btnSimulationForm.start();

                    $.ajax({
                        url: Routing.generate('operation_dashboard_simulation_task'),
                        type: "POST",
                        data: $t.serialize(),
                        success: function (data) {
                            btnSimulationForm.stop();
                            toastr.success(data.msg, data.title, {timeOut: 5000});
                        },
                        error: function (e) {
                            handleAjaxResponseError(e);
                        }
                    });
                });

                if(!isPartnerInterface) {
                    if ( Cookies("dashboard-admin-panel") == "true" ) {
                        $('.admin-panel-button').removeClass('inactive');
                        $('.admin-operation-row').show();
                    } else {
                        $('.admin-panel-button').addClass('inactive');
                        $('.admin-operation-row').hide();
                    }
                }

                $('.admin-panel-button').click(function (e) {
                    e.preventDefault();

                    var $t = $(this);

                    $('.button-activities').trigger('click');
                    if ($t.hasClass('inactive')) {
                        $t.removeClass('inactive');
                        $('.admin-operation-row').show();
                        _getProcessDefinition();
                        Cookies.set('dashboard-admin-panel', true);
                    }
                    else {
                        $t.addClass('inactive');
                        $('.admin-operation-row').hide();
                        Cookies.set('dashboard-admin-panel', false);
                    }

                    ticketListManager.refresh();

                });

                var $stepSelect = $('#step-id');

                $stepSelect.select2().on('change', function () {
                    if ($(this).val() && _currentProcess) {
                        Cookies.set('DO_admin_step_' + _currentProcess, $(this).val());
                    }
                });

                $previewStepForm.on('submit', function (e) {

                    e.preventDefault();
                    _renderStep($(this));

                });
            }

            function _init() {

                if (Cookies.getJSON('dashboard-admin-panel')) {
                    $('.admin-operation-row .toggle-open-panel').removeClass('closed');
                    ticketListManager.refresh();
                    _getProcessDefinition();
                }

            }

            _handleEvents();

            return {
                // getProcessDefinition: _getProcessDefinition,
                init: _init
            }

        }

        /** Obsługa ticketów na liście */

        function ticketsListHandle() {

            if (casePanelContainer.length) {

                historyTicketContainer.on('click', '.js-single-ticket', onClickTicket);
                casePanelContainer.on('click', '.js-single-ticket, #user-next-task', onClickTicket);

            }

            /** TimeLine */
            timelineTooltipsInit();
        }

        function onClickTicket(e) {

            e.preventDefault();

            $blogPage.remove();
           $efficiencyRanking.addClass('hidden');

            if (_status.isLoading()) return false;

            var $t = $(this),
                _id = $t.attr('data-attribute-value-id'),
                _rootId = $t.attr('data-root-id'),
                _stepId = $t.attr('data-attribute-value-step-id'),
                _group = $t.attr('data-attribute-value-group-process-id'),
                _isForce = $t.attr('data-is-force');

            var isHistoricalTask = $t.hasClass('historical-task');

            /** Ten ticket jest aktywny */
            if (_id === activeTicketId) {
                toastr.info("Jesteś obecnie na tym zadaniu.");
                return false;
            }

            /** Próba zarezerwowania zadania */

            var data = {id: _id, force: false};

            if(SUPER_USER.reserveForce || _isForce === "1") {
                data.force = true;
            }

            reserveTask(data, function () {

                /** Zadanie zostało zarezerwowane */

                activeTicketId = _id;

                displayNotifications();

                wasReloaded = false;
                if (reloadingTimeout) clearTimeout(reloadingTimeout);

                if(!isHistoricalTask) {

                    _dashboardService.setCurrentTicket({
                        'id': _id,
                        'stepId': _stepId,
                        'groupProcessId': _group,
                        'rootId': _rootId
                    });

                    $('.button-timeline').find('.active-task-id').html(ticketListManager.displayCaseNumber(_rootId, _stepId));

                }

                /** Ładowanie formularza */

                _socket.getTask(_id, function (result) {

                    if(isHistoricalTask) {

                        _dashboardService.setCurrentTicket({
                            'id': _id,
                            'stepId': result.processInfo.stepId,
                            'groupProcessId': result.processInfo.groupProcessId,
                            'rootId': result.processInfo.rootId
                        });

                        $('.button-timeline').find('.active-task-id').html(ticketListManager.displayCaseNumber(result.processInfo.rootId, result.processInfo.stepId));

                    }

                    // ticketListManager.setActive($t);
                    timelineOnTaskClick();
                    createForm(result);
                    dashboardModeSwitch();
                    reportUserAtInstance();
                    caseNoteModule.hideNoteForm(true);

                }, function () {
                    _socket.cancelReserveCurrentTask();
                });

            },{
                notificationMode: ($t.is('#user-next-task'))
            });

        }

        /** steps on timeline */

        function timelineHandle() {

            $timelineContainer.on('click', '.timeline-block', function (e) {

                e.preventDefault();
                if ($(this).hasClass('next')) {
                    return false;
                }
                else {

                    $('.notification-module').show();
                   $efficiencyRanking.addClass('hidden');
                    if (_status.isLoading()) return false;

                    var $t = $(this),
                        _id = $t.attr('data-process-instance-id');

                    wasReloaded = false;
                    if (reloadingTimeout) clearTimeout(reloadingTimeout);

                    if(!$t.hasClass('disabled')) {

                        tryGetTask({
                            id: parseInt(_id),
                            notificationMode: true
                        });

                    }
                    else {
                        _socket.getTask(_id, function (result) {
                            createForm(result);
                            reportUserAtInstance();
                            dashboardModeSwitch(_id);
                        });
                    }

                }

            });


            /** TimeLine */
            timelineTooltipsInit();
        }

        function reserveTask(data, callback, options) {

            if(!_socket) {

                toastr.error("Proszę odświeżyć panel operacyjny.", "Nie można nawiązać połączenia z serwerem.", {timeOut: 5000});

            }
            else {

                _socket.tryReserveTask(data, function (result) {

                    if (result.isReserved) {

                        if(options.notificationMode) {

                            if(result.isSameUser) {
                                toastr.warning('Zadanie jest używane przez Ciebie w innym oknie.');
                            } else {
                                toastr.warning('Zadanie jest zajęte przez ' + result.whoReserved);
                            }

                        }
                        else {
                            resetFormView();

                            ticketListManager.setActive(null);

                            var nameOfUserReserved = (result.whoReserved) ? result.whoReserved : 'NONAME';

                            $reservedPopup.find('[data-username]').attr('data-username', nameOfUserReserved);
                            $reservedPopup.addClass('fadeInDown').show();
                        }

                    }
                    else {
                        if (typeof callback === "function") callback();
                    }

                });

            }

        }

        function setBordertoYesNoButton() {
            var $form = $('#form-generator-box'),
                input = $form.find('.custom-radio-button input');

            input.each(function() {
                var $t = $(this);
                $t.closest('.custom-radio-button').addClass('auto-filled');
            })
        }

        function displayNotifications() {


            $notificationModuleWrapper.fadeIn(500);
            $notificationModuleWrapper.parent().addClass('active');
            $notificationsTypeContainer.fadeIn(500);

            setTimeout(function () {

         /*       var navBarHeight = $('.page-header.navbar').height();
                var chatBarHeight = $('#cometchat').height();*/
                var notificationTitle = $('.notification-container .title');

                $('.right-side .statBox').addClass('shrinked');
              /*  $notificationModule.height($window.height() - 90 - (navBarHeight + chatBarHeight));*/
                notificationTitle.slice(0, 4).attr('aria-selected', 'true').next().show();
            }, 100);

            displayAdditionalCaseInfo();

        }

        function activeAndLoadPreviousStep(instanceId) {

            _status.startLoading();

            $.ajax({
                url: Routing.generate('case_ajax_reactivate_prev_step'),
                type: "POST",
                beforeSend: function(xhr){
                    if(_dashboardService.isPublicDashboard()) {
                        xhr.setRequestHeader('Process-Token', _processToken);
                    }
                },
                data: {
                    'id': _dashboardService.getCurrentTicket('id'),
                    'previousId': instanceId
                },
                success: function (response) {

                    _status.stopLoading();

                    if(response.processInfo) {

                        _dashboardService.setCurrentTicket({
                            'id': response.processInfo.id,
                            'stepId': response.processInfo.stepId,
                            'groupProcessId': response.processInfo.groupProcessId,
                            'rootId': response.processInfo.rootId
                        });

                        getPublicCase();
                    }
                },
                error: function(err) {
                    _status.stopLoading();
                    handleAjaxResponseError(err)
                }

            });

        }

        function onPrevStep(instanceId) {

            _status.startLoading();

            $.ajax({
                url: Routing.generate('case_ajax_reactivate_process'),
                type: "POST",
                beforeSend: function(xhr){
                    if(_dashboardService.isPublicDashboard()) {
                        xhr.setRequestHeader('Process-Token', _processToken);
                    }
                },
                data: {
                    'id': instanceId,
                    'previousId': _dashboardService.getCurrentTicket('id')
                },
                success: function (newId) {
                    _status.stopLoading();

                    if(newId) {

                        _dashboardService.setCurrentTicketId(newId);
                        getPublicCase();

                    }
                }
            });
        }

        function getPublicCase() {

            publicDashboardModule.getCase(_processToken, function (result) {

                var processInfo = result.processInfo;

                activeTicketId = processInfo.id;

                _dashboardService.setCurrentTicket({
                    'id': processInfo.id,
                    'stepId': processInfo.stepId,
                    'groupProcessId': processInfo.groupProcessId,
                    'rootId': processInfo.rootId
                });

                createForm(result);

            }, function(error) {

                $('.form-placement form').remove();

                if(error.responseJSON) {
                    customDashboardPopup.open('Błąd', error.responseJSON.message, true, false, false);
                }
                else {
                    customDashboardPopup.open('Błąd', "Wystąpił problem z otworzeniem formularza.", false);
                }

            });

        }

        /**
         * @typedef {Object} OnNextResult
         *      @property {int} error
         *      @property {String} desc
         *      @property {Array} semirequireds
         */

        function onNextStep(variant, omitValidation, skipStepProcedure, skipErrors, skipPostpone) {

            skipErrors = typeof skipErrors !== 'undefined' ? skipErrors : false;
            omitValidation = typeof omitValidation !== 'undefined' ? omitValidation : false;
            skipStepProcedure = typeof skipStepProcedure !== 'undefined' ? skipStepProcedure : false;
            skipPostpone = typeof skipPostpone !== 'undefined' ? skipPostpone : false;

            // if(reloadingTimeout) clearTimeout(reloadingTimeout);
            // if(!sessionDashboard) return false;

            if (!_dashboardService.isCurrentTicket()) {
                toastr.error("Brak ID obecnego kroku.", "Błąd!", {timeOut: 5000});
                return;
            }

            var forceVariant = variant || null;
            var $quiz = $('.operation-dashboard-form #quiz');

            if ($quiz.length > 0 && variant !== 98 && variant !== 92) {
                forceVariant = $quiz.find('input[name="answer"]:checked').val();
                if ($quiz.find('input[name="answer"]').length > 0 && typeof forceVariant == 'undefined') {
                    $('#errorValidationMsg').text('Wybierz odpowiedź, aby przejść dalej');
                    $('#validation-error-modal').modal('show');
                    return false;
                }
            }

            _socket.getNextStep({
                id: _dashboardService.getCurrentTicket('id'),
                description: true,
                variant: forceVariant,
                omitValidation: omitValidation,
                skipStepProcedure : skipStepProcedure,
                skipErrors: skipErrors,
                skipPostpone: skipPostpone
            },
                /**
                 * @param {OnNextResult} result
                 */
                function (result) {

                if (result.error !== 0 && skipErrors == false) {
                    if (result.error === 2) {

                        displaySemiRequireModal(result);

                    } else if (result.error === 3) {
                        var currentHtml = '';
                        for (var x in result.eCallControls) {
                            currentHtml += '</br>' + result.desc + ': ' + $(result.eCallControls[x]).siblings('label').text();
                        }
                        $('#errorValidationMsg').html(currentHtml);

                        $('#validation-error-modal').modal('show');
                    }
                    else if (result.error < 0) {

                        if(result.error < -100) {
                            _f.openModal(
                                result.message,
                                function (){},
                                function(){},
                                true
                            );
                        }
                        else {
                            _f.openModal(
                                result.message,
                                function ($modal) {
                                    onNextStep(variant, omitValidation, skipStepProcedure, true)
                                },
                                function(){}
                            );
                        }

                    }
                    else {

                        displayValidation(result, null);

                    }
                } else {

                    if(typeof scenarioRecorder !== "undefined" && scenarioRecorder) {

                        scenarioRecorder.successNextStep({
                            stepId: _dashboardService.getCurrentTicket('stepId'),
                            variant: forceVariant,
                            isQuiz: ($quiz.length > 0)
                        });

                    }

                    afterSuccessNextStep(result);
                }

            }, function () {

                if(_socket) {
                    _socket.tryStopTimer();
                }

                $('.reload-current-form-wrapper').show();

            });

        }

        /**
         *
         * @param {OnNextResult} result
         */
        function displaySemiRequireModal(result) {

            var $confirmModal = $('#semirequired-confirmation-modal');

            var semiRequiredInfo = '<ul>';

            for (var control in result.semirequireds) {
                semiRequiredInfo += '<li>' + $('div[data-id=' + result.semirequireds[control] + '] label:first').text() + '</li>';
            }
            semiRequiredInfo += '</ul>';

            $('#semirequireds').html(semiRequiredInfo);
            $confirmModal.modal('show');

            confirmationReasonInit();

            $confirmModal.find('#action-confirm').off('click', onConfirmConfirmationModal).on('click', {result: result}, onConfirmConfirmationModal);

        }

        function onConfirmConfirmationModal(event) {

            var result = event.data.result;

            var $confirmModal = $('#semirequired-confirmation-modal'),
                $textarea = $confirmModal.find("textarea");

            if ($textarea.val().trim() !== '') {

                var inputsAttributes = [];

                for (var semirequired in result.semirequireds) {
                    inputsAttributes.push($('div[data-id=' + result.semirequireds[semirequired] + '] input').attr('data-attribute-value-id'));
                }

                _socket.getNextStep({
                    id: _dashboardService.getCurrentTicket('id'),
                    description: true,
                    omitValidation: true,
                    omitedInputs: inputsAttributes,
                    omitReason: $textarea.val()
                }, function (result) {

                    $confirmModal.modal('hide');
                    $textarea.val('');

                    $('#semirequiredOmitReason').parent('div').removeClass('has-error');
                    afterSuccessNextStep(result);

                });
            } else {
                $textarea.val('');
                $('#semirequiredOmitReason').parent('div').addClass('has-error');
            }

        }

        function confirmationReasonInit() {
            var textarea = $('#semirequiredOmitReason'),
                availableReasons = [
                    {
                        "label": "//Rozłączyło rozmowę i nie można się dodzwonic do klienta",
                        "value": "Rozłączyło rozmowę i nie można się dodzwonic do klienta"
                    },
                    {
                        "label": "//Błędy uniemożliwiają przejście dalej",
                        "value": "Błędy uniemożliwiają przejście dalej"
                    },
                    {
                        "label": "//Zasłabłem i spadłem pod krzesło",
                        "value": "Zasłabłem i spadłem pod krzesło"
                    }
                ];

            textarea.autocomplete({
                autoFocus: true,
                minLength: 0,
                source: availableReasons,
                appendTo: "#semirequired-confirmation-modal .form-group"
            });

            textarea.autocomplete("disable");

            textarea.keyup(function() {
                var value = textarea.val();
                var first = value.substr(0, 2);
                if ( first == "//") {
                    textarea.autocomplete("enable");
                    textarea.autocomplete("search");
                } else {
                    textarea.autocomplete("disable");
                }
            });
        }

        function afterSuccessNextStep(result) {

            if (result.id === null) {

                finishForm("Informacja", result.message, 'fa-exclamation', result.err, false);
                activeTicketId = null;
                if(_dashboardService.isPublicDashboard()) {
                    return;
                }
                ticketListManager.remove(_dashboardService.getCurrentTicket('id'), true);
                ticketListManager.finishTask();
                _dashboardService.setCurrentTicket(null);
                _socket.cancelReserveCurrentTask();
                caseTodoManagement.loadTask();
                casePingTimer.stopTimer();

            }
            else {

                if(_dashboardService.isPublicDashboard()) {

                    if(result.token && result.token !== _processToken) {
                        _processToken = result.token;

                        var newUrl = Routing.generate('public_operational_dashboard_index', {
                            'processToken': _processToken
                        });

                        window.history.pushState("", "", newUrl);
                    }

                    getPublicCase();
                    return;
                }

                if (result.sparxUrl) {
                    toastr.options.onclick = function() { window.open(result.sparxUrl); };
                    toastr.info("", result.sparxUrl, {timeOut: 15000});
                }

                if(_dashboardService.getCurrentTicket('id') == result.id) {
                    _socket.getTask(result.id, function (result) {
                        createForm(result);
                        dashboardModeSwitch();
                    });
                }
                else {

                    try {
                        ticketListManager.remove(_dashboardService.getCurrentTicket('id'), true, true);
                    }
                    catch (e) {}

                    var data = {id: result.id, force: false};
                    reserveTask(data, function () {

                        // ticketListManager.replace(result, _dashboardService.getCurrentTicket('id'));
                        var _rootId = (result.description) ? result.description.rootId : result.groupProcessId;

                        $('.button-timeline').find('.active-task-id').html( ticketListManager.displayCaseNumber(_rootId, result.stepId));

                        _dashboardService.setCurrentTicket({
                            'id': result.id,
                            'stepId': result.stepId,
                            'groupProcessId': result.groupProcessId,
                            'rootId': result.description.rootId
                        });

                        activeTicketId = result.id;

                        _socket.getTask(result.id, function (result) {

                            caseTodoManagement.refreshTask();
                            createForm(result);
                            dashboardModeSwitch();

                        }, function () {
                            _socket.cancelReserveCurrentTask();
                        });

                    });

                }

            }

        }

        /** Obsługa do otwierania nowej sprawy gdy dzwoni telefon */

        function tasksFromTelephonyHandle() {

            document.addEventListener("telephony-pick-up", function (e) {

                if (e.detail && e.detail.processInstances) {

                    var newProcess = e.detail,
                        data = {id: newProcess.processInstances, force: (typeof newProcess.force !== "undefined") ? newProcess.force : true};

                    $blogPage.remove();

                    reserveTask(data, function () {

                        caseTodoManagement.refreshTask(data.id);

                        displayNotifications();

                        _socket.getTask(data.id, function (result) {

                            if(result.processInfo) {
                                _dashboardService.setCurrentTicket(result.processInfo);

                                $('.button-timeline').find('.active-task-id').html(ticketListManager.displayCaseNumber(
                                    result.processInfo.rootId,
                                    result.processInfo.stepId
                                ));
                            }

                            activeTicketId = data.id;

                            timelineOnTaskClick();
                            dashboardModeSwitch();
                            caseNoteModule.hideNoteForm(true);
                            refreshCurrentTaskId();
                            createForm(result);
                            reportUserAtInstance();

                        }, function () {
                            _socket.cancelReserveCurrentTask();
                        });

                    }, {
                        notificationMode: true
                    });

                }

            });

            document.addEventListener("check-active-task", function (e) {

                if (e.detail){

                    if(typeof CustomEvent !== 'undefined') {
                        var _event = new CustomEvent("on-active-task", {'detail': {activeTicketId: _dashboardService.getCurrentTicket('id')}});
                        window.parent.document.dispatchEvent(_event);
                    }
                }

            });

        }

        // function createAndClickTicket(processDescription) {
        //     if(!processDescription) return false;
        //
        //     ticketListManager.add(processDescription, true, function ($el) {
        //         $el.attr('data-is-force', "1");
        //         $el[0].click();
        //     });
        // }

        /** Inicjalizacja widoku dashboardu */

        function initLayout() {

            if(isPublicDashboard && isMobile.any) {
                disableScaleWindow();
            }

            // for printing

            if(typeof CustomEvent !== 'undefined') {
                new WOW({
                    // scrollContainer: '.tickets-sidebar[data-category=steps]',
                    live: true
                }).init();
            }
            else {
                $('.form-placement').css('visibility', 'visible');
            }

            if(isPartnerInterface || isCaseEditorPanel) {
                // PartnerInterfaceTabModule(_dashboardService, function (_id) {
                //     /** Ładowanie formularza */
                //     _socket.getTask(_id, function (result) {
                //         createForm(result);
                //     });
                // });
            }
            else {

                hideHamburgerMenu();
                ticketSidebarTabs();
                dashboardNotifications();
                caseNoteModule.initNoteForm();
                foldExpandNotifications();
                showTimelinePopup();
                dashboardCounter();
                setTicketColor();
            }



        }

        /**
         * @function delayedWindowCallback
         */
        var delayedWindowCallback = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        /** Odpalenie głównym eventów */

        function mainEventHandle() {

            var openedEmailNotes = {};

            /** Tymczasowe wywalenie timeline*/
            $('.timeline-custom').remove();

            /** Ukrycie Accordion*/
            /*$('#customAccordion').hide();*/

            resizeFormPlacmenet();

            $('.close-this-popup').on('click', function (e) {

                e.preventDefault();
                $(this).closest('.custom-info-modal').fadeOut();

            });

            $('.finish-popup').on('click', '.back-to-service-panel', function (e) {

                e.preventDefault();

                var id = parseInt($(this).attr('data-service-panel-id'));

                if(id) {

                    tryGetTask({
                        id: id,
                        force: false,
                        displayCase: true,
                        activeTimeline: true
                    });
                }

            });

            $updatePositionModal.find('.action-confirm').on('click', function (e) {

                e.preventDefault();

                if(_MAP !== null) {
                    var position = $(this).attr('data-position').split('|');
                    _MAP.updatePositionOnMap(position[0], position[1]);
                }

                $updatePositionModal.modal('hide');

            });

            $abandonProcessModal.find('#abandon-confirm').on('click', function () {

                $abandonProcessModal.modal('hide');
               $efficiencyRanking.removeClass('hidden');
                $('.notification-module').addClass('hidden');
                resetFormView();
                resizeFormPlacmenet();

                _socket.cancelReserveCurrentTask();
                ticketListManager.finishTask(true);

                _dashboardService.setCurrentTicket(null);
                activeTicketId = null;

                window.currentProcessId = null;
                window.currentProcess = null;

                sendToParentWindow({
                    type: 'onGetTask'
                });

            });

            $medicalCaseModal.find('#medical-case-confirm').on('click', function () {
                var $input = $('#medical-case-input');
                var $inputIsChecked = $input.is(':checked') === true ? 1 : 0;
                var $attributeId = $input.attr('data-attribute-id');
                $medicalCaseModal.modal('hide');

                $.ajax({
                    url: Routing.generate('operationalDashboardInterfaceSetMedicalDataCase'),
                    type: 'POST',
                    data: {
                        'groupProcessId': _dashboardService.getCurrentTicket('rootId'),
                        'valueId': $attributeId,
                        'value': $inputIsChecked,
                    },
                    success: function (data) {
                        toastr.success('', 'Oznaczenie zostało zapisane.', 3000);
                    },
                    error: function(err) {
                        $input.prop('checked', false);
                        handleAjaxResponseError(err);
                    }
                });

            });

            $medicalCaseModal.find('#medical-case-cancel').on('click', function(){
                $('#medical-case-input').prop('checked', false);
            });

            if($caseNote.length) {
                $caseNote.on('click', '.preview-email', function () {

                    var $note = $(this).closest('.case-note'),
                        noteId = $note.attr('data-id');

                    if(typeof openedEmailNotes[noteId] !== "undefined" && openedEmailNotes[noteId] !== null && !openedEmailNotes[noteId].closed ) {
                        openedEmailNotes[noteId].focus();
                    }
                    else {

                        var url = Routing.generate('mailboxEditor', {'note-id': $note.attr('data-id'), 'fullscreen': 1 }),
                            windowHeight = window.innerHeight,
                            windowWidth = window.innerWidth;

                        var offset = 100,
                            height = windowHeight - (offset * 2),
                            width = windowWidth - (offset * 4);

                        openedEmailNotes[noteId] = window.open(url, $note.attr('data-original-title'),
                            'toolbar=no,location=no,statusbar=no,width=' + width + ',height=' + height + ',top=' + (offset + 100) + ',left=' + offset * 2);

                        if(openedEmailNotes[noteId]) {

                            /**
                             * Jeżeli okno z e-mailem jest czytane, to aktywuje timer, że ktoś jest na tym zadaniu gdzie otworzył e-maila
                             * */
                            openedEmailNotes[noteId].addEventListener("focus", function(e){

                                delayedWindowCallback(function () {

                                    if(!openedEmailNotes[noteId]) return;
                                    openedEmailNotes[noteId].opener.postMessage({action: 'focus-email', noteId: noteId}, Routing.generate('index', [], true));

                                }, 200);

                            }, false);

                            openedEmailNotes[noteId].addEventListener("blur", function(e){

                                openedEmailNotes[noteId].opener.postMessage({action: 'blur-email', noteId: noteId}, Routing.generate('index', [], true));

                            }, false);

                            openedEmailNotes[noteId].addEventListener("beforeunload", function(e){

                                openedEmailNotes[noteId].opener.postMessage({action: 'blur-email', noteId: noteId}, Routing.generate('index', [], true));
                                delete openedEmailNotes[noteId];

                            }, false);

                        }

                    }

                    // $('.note-inbox').attr('data-document-id', this.getAttribute('data-attachment-id'));
                    // if(typeof CustomEvent !== 'undefined') {
                    //     window.document.body.dispatchEvent(new CustomEvent("refresh-note-inbox"));
                    // }

                });

                $caseNote.on('click', '.edit-email-note', function () {

                    var $t = $(this),
                        $noteContent = $t.closest('.note-content'),
                        $boxContent = $noteContent.find('.box-content'),
                        noteId = this.getAttribute('data-note-id'),
                        text = $boxContent.text(),
                        $textarea = $('<textarea rows="4" class="dynamic-note-content-edit"></textarea>'),
                        $submit = $('<a class="save-note btn btn-xs btn-success">Zapisz notatkę</a>');

                    $textarea.val(text);
                    $noteContent.addClass('editing');

                    $submit.on('click', function(e) {

                        $.ajax({
                            url: Routing.generate('mailboxSaveShortContent', {
                                'noteId': noteId
                            }),
                            data: {
                               'content': $textarea.val()
                            },
                            type: "POST",
                            success: function (data) {
                                toastr.success('', 'Notatka została zapisana.', 3000);
                            },
                            error: function(err) {
                                handleAjaxResponseError(err)
                            }
                        });

                        $noteContent.removeClass('editing');
                        $boxContent.empty().html($textarea.val());

                    });

                    $boxContent.html('').append($textarea).append($submit);

                });

                $caseNote.on('click', '.chat-reply', function () {

                    if(!$notificationsTypeContainer.hasClass('enable-chat')) {
                        return false;
                    }

                    var dataChatSender = $(this).attr('data-chat-sender');
                    $('#chat-note-btn').trigger('click');
                    $('#addNotification #406-226-198').val(dataChatSender).trigger('change');

                });
            }

            setInterval(function () {

                emailsForCheck.forEach(function (item, i, object) {

                    if(!item || item === "0") return;

                    $.ajax({
                        url: Routing.generate('mailboxCheckEmailStatus', {
                            'id': item
                        }),
                        type: "GET",
                        success: function (response) {

                            var $note = $('#case-notes .case-note[data-id="'+response.id+'"]');

                            if($note.length) {
                                var extraContent = caseNoteModule.getNoteExtraContent(response.attachment, response.status, response.id);
                                $note.find('.extra-content-note').replaceWith(extraContent);
                            }

                            if(response.status !== "pending" || $note.length === 0) {
                                var index = emailsForCheck.indexOf(item);
                                if (index > -1) {
                                    emailsForCheck.splice(index, 1);
                                }
                            }

                        },
                        fail: function () {
                            var index = emailsForCheck.indexOf(item);
                            if (index > -1) {
                                emailsForCheck.splice(index, 1);
                            }
                        }
                    });

                });

            }, 60000)
        }

        /** ------------------------------------------ FUNCTIONS --------------------------------------- */

        function refreshCurrentTaskId() {
            $('.currentTicketContainer > li').attr('data-attribute-value-id', _dashboardService.getCurrentTicket('id'));
        }

        function timelineTooltipsInit() {
            $('#tab-timeline').find('.timeline-content').each(function () {
                $(this).tooltip({
                    container: "body",
                    html: true,
                    placement: "right",
                    template: '<div class="tooltip timeline-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                });
            })
        }

        function ticketSidebarTabs() {

            $('.dashboard-mode-switcher').tabs({
                disabled: [1]
            });

            $('.filter-tabs').tabs({
                disabled: [1, 2, 3]
            });

        }

        function showTimelinePopup() {
            $('.mode-switch-container').on('click', 'li.ui-state-disabled', function () {
                if ($('.timeline-message:not(.visible)'))
                    $(this).find('.timeline-message').addClass('visible');
                setTimeout(function () {
                    $('.mode-switch-container .timeline-message').removeClass('visible');
                }, 3500);
            })
        }

        function timelineOnTaskClick() {

            var currentTaskIsActive = $('#tab-activities .category.currentTask.active'),
                timelineButton = $('.button-timeline');

            if (currentTaskIsActive) {

                $(".dashboard-mode-switcher").tabs("enable", 1);
                timelineButton.trigger('click').addClass('active-task');
            }

            $('.button-activities').on('click', function() {
                setHeightOfTicketGroups();
            })


        }

        function addNotification() {
            var addNotificationContainer = $('#addNotification');
            addNotificationContainer.on('click', '.js-submit-button', function () {
                e.preventDefault();
                var ifUrgent = $('#addNotification input.urgent').is(':checked');
                /*var notificationHeader = $('#addNotification .js-notification-header').val();*/
                var notificationMessage = $('#addNotification .js-notification-message').val();
                var notification = $("<h4 class='title'><i class='fa fa-envelope fa-lg'></i><span class='headerText'>" + notificationMessage + "</span></h4><div><p class='description'>" + notificationMessage + "</p></div>");
                var notificationUrgent = $("<h4 class='title urgent'><i class='fa fa-warning fa-lg'></i><i class='fa fa-envelope fa-lg'></i><span class='headerText'>" + notificationMessage + "</span></h4><div><p class='description'>" + notificationMessage + "</p></div>");
                var notificationProper = ifUrgent ? notificationUrgent : notification;
                notificationProper.prependTo(".notification-container");
                /*.slideDown(300);*/
                $(".notification-container").accordion("refresh");
                $('#addNotification .js-notification-message').val('');
            });

            var functionSwitchSingle = false;
            $(".notification-container").on('click', '.title', function () {
                if (!functionSwitchSingle) {
                    $(this).attr({
                        "aria-selected": "false",
                        "aria-expanded": "false"
                    }).next().attr("aria-hidden", "true").slideUp(300);
                    functionSwitchSingle = true;
                }
                else {
                    $(this).attr({
                        "aria-selected": "true",
                        "aria-expanded": "true"
                    }).next().attr("aria-hidden", "false").slideDown(300);
                    functionSwitchSingle = false;
                }
            });
        }

        function foldExpandNotifications() {

            var foldButton = $('.accordion-expand-all');
            foldButton.on('click', function () {

                if ($(this).hasClass('active')) {
                    $('.notification-container').find('.title').removeClass('absolute-header').attr({
                        "aria-selected": "false",
                        "aria-expanded": "false"
                    }).next().attr("aria-hidden", "true").hide().next().next().hide();
                } else {
                    $('.notification-container').find('.title').addClass('absolute-header').attr({
                        "aria-selected": "true",
                        "aria-expanded": "true"
                    }).next().attr("aria-hidden", "false").show().next().next().show();
                }

                $(this).toggleClass('active');

                caseNoteModule.recalculateHeight();
            });
        }

        function checkRequiredFields() {
            var filteredElements = 'button, input, select, textarea',
                $generatorForm = $('#form-generator-box');

            $generatorForm.find(filteredElements).on("change", function() {

                var $t = $(this);

                if ( !$t.val() && $t.attr('required') ) {
                    $t.addClass('required');
                } else {
                    $t.removeClass('required');
                }

                if ( $t.val() && $t.attr('auto-field') == "true" ) {
                        $t.addClass('auto-filled');
                        $t.removeClass('required');
                } else {
                    $t.removeClass('auto-filled');
                }

            });

        }

        function disableEnableControls() {

            var $operationDashboardForm = $('.operation-dashboard-form'),
                isPreviewMode = $operationDashboardForm.hasClass('preview-mode'),
                isServiceEditorMode = $operationDashboardForm.hasClass('service-editor-mode'),
                isEditorPanelMode = $operationDashboardForm.hasClass('editor-panel'),
                isReadonlyMode = $operationDashboardForm.hasClass('form-readonly-mode');

            var $dashboardForm = $('.operation-dashboard-form .dashboard-version, #rendered-form, #rendered-form2');

            if(isReadonlyMode || isPreviewMode || isServiceEditorMode) {
                _dashboardService.setPreviewStep(true);
                $('.generating-form-submit').prop('disabled', true).addClass('force-disabled');
                $('.cancel-step').addClass('hidden');
            }

            if(isReadonlyMode) {

                $dashboardForm.find('select[data-step-id], input[data-step-id], textarea[data-step-id]').each(disableFormControl);

                /** business decision */
                // var $fileUploads = $('.atlas-upload-file-wrapper ');
                //
                // if($fileUploads.length) {
                //     $fileUploads.find('.action-btn').remove();
                //     $fileUploads.addClass('readonly-file');
                // }

            }
            else if (isPreviewMode && !isEditorPanelMode) {

                $dashboardForm.find('[not-allow-edit="true"]').each(disableFormControl);

            }
            else if(isEditorPanelMode && !isServiceEditorMode) {

                $dashboardForm.find('select[data-step-id], input[data-step-id], textarea[data-step-id]').each(forceEnableFormControl);

            }

        }

        function forceEnableFormControl() {
            $(this).prop("disabled", false);
        }

        function disableFormControl() {

            var $t = $(this);

            if($t.attr('type') === "radio" || $t.attr('type') === "checkbox" ) {
                $t.addClass('click-disabled').attr("disabled", true);
            }
            else if($t[0].tagName === "SELECT") {
                $t.addClass('click-disabled').attr("disabled", true);
            }
            else {
                $t.addClass('click-disabled').attr("readonly", true);

                if($t.hasClass('date-picker') || $t.hasClass('date-time-picker')) {
                    var _datePicker = $t.data('datepicker');
                    if (_datePicker) {
                        _datePicker.destroy();
                    }
                }
            }

            $t.closest('.toggler-wrapper').addClass('disabled');

        }

        function dashboardModeSwitch(previewId) {

            previewId = previewId || _dashboardService.getCurrentTicket('id');
            // disableEnableControls();

            $('.timeline-block [data-process-instance-id="'+previewId+'"]').addClass('current');

            $('.dashboard-mode-switch input').unbind('change').on('change', function () {

                if ($(this).is(':checked') === false) {

                    $.ajax({
                        url: Routing.generate('case_ajax_clear_and_back_process', {processInstanceId: previewId}),
                        type: "POST",
                        success: function (result) {

                            if(result.processInstanceId) {

                                var _id = parseInt(result.processInstanceId),
                                    data = {id: _id, force: false};

                                reserveTask(data, function () {

                                    _dashboardService.setCurrentTicketId(_id);
                                    activeTicketId = _id;

                                    _socket.getTask(_id, function (result) {
                                        createForm(result);
                                        reportUserAtInstance();
                                        refreshCurrentTaskId();
                                    }, function () {
                                        _socket.cancelReserveCurrentTask();
                                    });

                                });


                            }
                            else {
                                toastr.error("","Problem z cofnięciem sprawy", {timeOut: 7000});
                            }
                        }
                    });
                }

            });
        }

        function hideHamburgerMenu() {

            var message = {
                type: 'dashboard-viewed'
            };

            window.parent.postMessage(message, Routing.generate('index', [], true));

        }

        function dashboardNotifications() {
            $(".notification-container").accordion({
                header: "h4",
                heightStyle: "content",
                collapsible: true,
                beforeActivate: function (event, ui) {
                    if (ui.newHeader[0]) {
                        var currHeader = ui.newHeader;
                        var currContent = currHeader.next('.ui-accordion-content');
                    } else {
                        var currHeader = ui.oldHeader;
                        var currContent = currHeader.next('.ui-accordion-content');
                    }

                    var isPanelSelected = currHeader.attr('aria-selected') == 'true';

                    currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

                    currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

                    currContent.toggleClass('accordion-content-active', !isPanelSelected)

                    return false;
                }
            });

            // TODO - Gdzie to jest? Do wywalenia?
            $("div.panel a").click(function () {
                $('div.panel-collapse').each(function (index) {
                    if ($(this).hasClass('in')) {
                        $("#expandbutton").show();
                        $("#collapsebutton").hide();
                    }
                });
            });

            $('#case-notes').on('click', '.note-title', function (e) {
                if ($(e.target).hasClass('medical-data')) {
                    e.preventDefault();
                    return;
                }
                var $t = $(this);
                $t.next('.note-content').toggle();

                if ($t.hasClass('absolute-header')) {
                    $t.removeClass('absolute-header');
                    $t.find('.header-text').addClass('visible');
                    $t.find('.date-and-time').removeClass('visible');
                } else {
                    $t.addClass('absolute-header');
                    $t.find('.header-text').removeClass('visible');
                    $t.find('.date-and-time').addClass('visible');
                }
            });

            $('#case-notes').on('click', '.medical-data', function () {
                var $t = $(this);
                var $parent = $t.parents('.case-note');
                var $noteId = $parent.attr('data-id');


                $.ajax({
                    url: Routing.generate('operationalDashboardInterfaceSetMedicalDataNote'),
                    type: 'POST',
                    data: {'parentNoteId': $noteId},
                    success: function (response) {
                        var $value = response.value;

                        if ($value === 1) {
                            $t.removeClass('fa-plus-square-o');
                            $t.addClass('fa-plus-square');
                        } else {
                            $t.removeClass('fa-plus-square');
                            $t.addClass('fa-plus-square-o');
                        }
                    },
                    error: function (response) {
                        handleAjaxResponseError(response);
                    },
                });
            });

        }

        function displayAdditionalCaseInfo(selector) {
            selector = selector || '#case-info .more-info';

            $(selector).on('click', function() {
                $(this).toggleClass('active');
                $caseInfo.find('.more-info-container').toggle();
                caseNoteModule.recalculateHeight();
            });

        }

        function setTicketColor() {
            $('.js-single-ticket').each(function () {

                var num = Math.floor(Math.random() * 100),
                    ticket = $(this);
                // ticket.attr('percentage', num);

                var percent = ticket.attr('percentage');
                var firstStatusTitle = "<div class='customTooltipWrapper status1'><div class='pointer'></div><div class='custom-tooltip'><div class='tooltip-quick-info'><p class='client'>Klient: T-mobile</p> <p class='NIP'>NIP: 123-456-789</p> <p class='phone'>Tel.: 123-456-789</p> <p class='time'>Czas: pozostało : 10h</p><p class='custom-note'>Notatka: Klient prosi o ponowny kontakt w celu ustalenia warunków naprawy...</p></div></div></div>";
                var secondStatusTitle = "<div class='customTooltipWrapper status2'><div class='pointer'></div><div class='custom-tooltip'><div class='tooltip-quick-info'><p class='client'>Klient: T-mobile</p> <p class='NIP'>NIP: 123-456-789</p> <p class='phone'>Tel.: 123-456-789</p> <p class='time'>Czas: pozostało : 10h</p><p class='custom-note'>Notatka: Klient prosi o ponowny kontakt w celu ustalenia warunków naprawy...</p></div></div></div>";
                var thirdStatusTitle = "<div class='customTooltipWrapper status3'><div class='pointer'></div><div class='custom-tooltip'><div class='tooltip-quick-info'><p class='client'>Klient: T-mobile</p> <p class='NIP'>NIP: 123-456-789</p> <p class='phone'>Tel.: 123-456-789</p> <p class='time'>Czas: pozostało : 10h</p><p class='custom-note'>Notatka: Klient prosi o ponowny kontakt w celu ustalenia warunków naprawy...</p></div></div></div>";

                if (percent >= 0 && percent <= 33) {
                    $(this).addClashandleWindowHeightchanges('status1').attr('title', firstStatusTitle);
                }
                if (percent >= 34 && percent <= 89) {
                    $(this).addClass('status2').attr('title', secondStatusTitle);
                }
                if (percent >= 90 && percent <= 100) {
                    $(this).addClass('status3').attr('title', thirdStatusTitle);
                }
                $(this).find('.time-bar').css('width', percent + '%');

                if ($(this)[0] === $('.js-single-ticket').last()[0]) {
                    $(this).attr('data-placement', 'top');
                }
            });

        }

        function dashboardCounter() {

            $('.count').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 80000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });

            $('.count-more').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 2000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        }

        function createTicket(payload) {

            if (payload.action === 'create') {

                // ticketListManager.add(payload.task, true);
                // if(isSimulationStarted){
                //     $('.tickets-sidebar li[data-attribute-value-id='+payload.task.id+']').trigger('click');
                //     isSimulationStarted = false;
                // }

            }
            else if(payload.action === "force-create") {
                /** Stworzenie i kliknięcie */

                // createAndClickTicket(payload.task);

                if(isScenarioRun){

                    var detailEvent = {
                        isForce: true,
                        processInstances: payload.task.id
                    };

                    var cEvent = new CustomEvent("telephony-pick-up", {'detail': detailEvent});
                    document.dispatchEvent(cEvent);

                    isScenarioRun = false;
                }

            }
            else if (payload.action === 'update') {

                /** ładowanie formularza */
                if (activeTicketId == payload.task.id) {
                    var focused = $('[data-attribute-value-id]:focus');
                    var locationExtraInfo = $('#comment').val();
                    var locationCombined = $('#city').val() + ', ' + $('#street').val() + ' ' + $('#streetNumber').val() + ', ' +
                        $('#areaName1').val() + ', ' + $('#areaName2').val() + ', ' + $('#areaName3').val() + ' | ' +
                        $('#country').val() + ' | ' + $('#lat').val() + ', ' + $('#lng').val();

                    var focusedId = focused.attr('id');
                    if (focused.length > 0) {
                        focused.attr('readonly', 'readonly');
                        var value = focused.val();
                        _socket.setAttribute({
                            "_this": focused,
                            "path": focused.attr('data-attribute-path'),
                            "groupProcessId": focused.attr('data-group-process-id'),
                            "stepId": focused.attr('data-step-id'),
                            "valueId": focused.attr('data-attribute-value-id'),
                            "value": focused.val(),
                            "valueType": focused.attr('data-type')
                        }, function () {
                            loadStepFormAndReportUser(function () {
                                $('#comment').val(locationExtraInfo).trigger('change');
                                _addNote('text', locationCombined);

                                var focusedControl = $('#' + focusedId);
                                focusedControl.removeAttr('readonly');
                                focusedControl.addClass('disable-change');
                                setTimeout(function () {
                                    //set cursor in the end of input
                                    focusedControl.focus();
                                    focusedControl.val('');
                                    focusedControl.val(value);
                                    focusedControl.removeClass('disable-change');
                                }, 1000);
                                toastr.info("", "Zaktualizowano dane zdarzenia z webserwisu ARC", {timeOut: 5000});
                            });
                        });
                    }
                    else {
                        loadStepFormAndReportUser(function () {
                            toastr.info("", "Zaktualizowano dane zdarzenia z webserwisu ARC", {timeOut: 5000});
                            $('#comment').val(locationExtraInfo).trigger('change');
                            _addNote('text', locationCombined);
                        });
                    }
                }

            }
        }

        function _addNote(type, content, numberPhone, callback, extraData) {

            caseNoteModule.addNote(type, content, numberPhone, null, callback, null, extraData);

        }

        function ajaxSendSmsNote(noteId, groupProcessId, callback) {
            $.ajax({
                url: Routing.generate('case_ajax_send_sms'),
                type: "POST",
                data: {
                    'id': noteId,
                    'groupProcessId' : groupProcessId
                },
                success: function(){
                    if (typeof callback === "function") {
                        callback();
                    }
                }
            });
        }

        function loadStepFormAndReportUser(callback) {
            _socket.getTask(activeTicketId, function (result) {
                createForm(result);
                reportUserAtInstance();
                if (typeof callback === "function") {
                    callback();
                }
            });
        }

        /**
         * @param {Object} result - Dane zwracane przez Get Task
         * @param {Object} result.processInfo - Informacje o procesie
         * @param {String} result.html - Html formularza
         * @param {String} result.type - Typ formularza
         * @param {int} result.permission - Czy uprawnienia
         * @param {Object[]} result.controls - Kontrolki na formularzu
         * @param {Object[]} result.notes - Tablica notatek
         * @param {Boolean=} simulation - Czy symulacja tworzenia kroku
         */
        function createForm(result, simulation) {

            if (!result) {
                console.error('Nie ma rezultatu.');
                return false;
            }

            if(result.permission === 0){
                resetFormView();
                ticketListManager.setActive(null);
                $reservedPopup.find('.title').text('Brak uprawnień');
                $reservedPopup.find('.text').text('Nie posiadasz uprawnień do zadania '+result.processInfo.stepId+'. Skontaktuj się z kierownikiem zmiany w celu przydzielenia uprawnień');
                $reservedPopup.addClass('fadeInDown').show();
                return;
            }

            isSimulation = simulation || false;

            if(!_dashboardService.isPublicDashboard() && !isPartnerInterface){

                /** Aktualizacja adresu URL iframe */
                var stateObj = {processInstanceId:  result.id};
                history.pushState(stateObj, "Form: " + result.id, Routing.generate('operational_dashboard_index', {
                    processInstanceId: parseInt(result.id)
                }));

            }

            $reservedPopup.fadeOut();
            $kickUserPopup.fadeOut();

            var formId = $(result.html).attr('id');

            /** To jeszcze będzie trzeba wywalić */

            _status.stopCounting();

            /** --- */

            changeModeView(result.type);
            renderRightWidget(result);

            $formPlacementDrop.html('').html(result.html);
            var $resultForm = $('#' + formId);

            if(result.type === "PARTNER_INTERFACE") {

                partnerInterfaceModule = PartnerInterfaceModule(_socket, {
                    dashboardService: _dashboardService,
                    onControlsLoad: function($form) {
                        prepareFormAfterInit($form);
                        initEventsHandleOnStep($form);
                    },
                    initWidgetMap: initWidgetMap,
                    mapHandleChooseTypeSearch: handleChooseTypeSearch
                });

                timeLineManager.clearTimeLine();

                return;
            }
            else {
                partnerInterfaceModule = null;
            }

            /** To będzie po inicjalizacji kafelków w gridstack.js, a jest lekki timeout, żeby zdążyło się wyrenderować */

            setTimeout(function () {

                if (result.controls) {
                    initGridStack(result, $resultForm, function () {
                        afterCreateForm($resultForm);
                    })
                }
                else {
                    afterCreateForm($resultForm);
                }

            }, 10);

            if (!isSimulation) {

                checkReloadForm($resultForm);

                emailPreviewModule.closeInbox();

                setTimeout(function () {
                    caseNoteModule.recalculateHeight();
                }, 10);

                refreshRightSide(parseInt(result.id));

            }
        }

        function refreshRightSide(processInstanceId) {

            if(isPublicDashboard || isPartnerInterface || isCaseEditorPanel) return false;

            $.ajax({
                url: Routing.generate('operationalDashboardInterfaceGetNotesAndInfo', {processInstanceId: processInstanceId}),
                type: "GET",
                beforeSend: function() {
                    $('.notification-module').addClass('loading');
                },
                complete: function() {
                    $('.notification-module').removeClass('loading');
                },
                success: function (response) {

                    caseNoteModule.setChatType(response.chatNoteContact, response.disableChatSelect);

                    /** Update notatki */
                    refreshNotes(response.notes);
                    refreshAdditionalInfo(response);

                    /** Jeśli Dzwoni telefon i nie ma żadnej notatki */
                    var noteSet = false;
                    $.each(response.notes.list, function (key, value) {
                        if (value.content) {
                            noteSet = true;
                        }
                    });

                    if (!noteSet && _dashboardService.isCurrentTicket() && _dashboardService.getCurrentTicket('stepId').indexOf("1012.") >= 0) {

                        _socket.getAttribute({
                            "path": '197',
                            "groupProcessId": _dashboardService.getCurrentTicket('groupProcessId'),
                            "stepId": _dashboardService.getCurrentTicket('stepId'),
                            "valueType": 'string'
                        }, function (result) {

                            var extra = {};

                            if(result.value) {
                                extra = {direction: 2};
                            }
                            else {
                                extra = {direction: 2, undefinedNumber: true};
                            }

                            caseNoteModule.addNote('phone', caseNoteModule.getCallText(2), result.value, null, function(){
                                caseNoteModule.recalculateHeight();
                            }, null, extra);

                        });

                    }
                },
                error: function (error) {
                    if(error.statusText !== "abort") {
                        toastr.error('Wystąpił błąd podczas ładowania notatek', 'Błąd ładowania notatek', {timeOut: 10000});
                    }
                }
            });

        }

        function refreshAdditionalInfo(response) {

            if (response.caseInfo) {

                var moreInfoShow = $('#case-info .more-info-container').is(':visible');
                $caseInfo.html(response.caseInfo);

                if (moreInfoShow) {
                    $('#case-info .more-info-container').show();
                    $('#case-info .more-info').addClass('active');
                }
                else {
                    $('#case-info .more-info-container').hide();
                    $('#case-info .more-info').removeClass('active');

                }

                var $specialSituation = $caseInfo.find('[data-id-of-special-situation]');

                if ($specialSituation.length) {
                    var id = $specialSituation.attr('data-id-of-special-situation');

                    $specialSituation.tooltip({
                        container: $caseInfo,
                        html: true,
                        placement: "bottom",
                        title: ticketListManager.getTitleOfSpecialSituation(id),
                        template: '<div class="tooltip note-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                    });

                }

                $caseInfo.show();
                displayAdditionalCaseInfo();
                vinHeadersInfoModal();
                programCardsInfoModal();
                handleModalTrigger($caseInfo);

            }

            if (response.additionalCaseInfo) {

                var moreInfoShow = $('#additional-case-info .more-info-container').is(':visible');
                $addCaseInfo.html(response.additionalCaseInfo);
                if (moreInfoShow) {
                    $('#additional-case-info .more-info-container').show();
                    $('#additional-case-info .more-info').addClass('active');
                }
                else {
                    $('#additional-case-info .more-info-container').hide();
                    $('#additional-case-info .more-info').removeClass('active');
                }

                $addCaseInfo.show();
                displayAdditionalCaseInfo('#additional-case-info .more-info');

            }
            else {
                $addCaseInfo.hide();
            }
        }

        function handleModalTrigger($caseInfo) {

            var $elements = $caseInfo.find('.modal-trigger[data-modal-content]');

            $.each($elements, function(i, ele) {

                var $ele = $(ele),
                    $parent = $ele.parent();

                $parent.css({
                    "cursor": "pointer"
                });

                $parent.on('click', {
                    content: $ele.attr('data-modal-content'),
                    title: $ele.attr('data-modal-title')
                }, openModalFromTrigger);
                
            });

        }

        function openModalFromTrigger(e) {

            var content = e.data.content,
                title = e.data.title;

            var message = {
                    header: title,
                    body: ''
                };

            var dfd = jQuery.Deferred();

            _f.openModal(message, null, null, true, dfd);

            $.ajax({
                url: Routing.generate('ajax_parse_text'),
                data: {
                    processInstanceId: _dashboardService.getCurrentTicket('id'),
                    text: '{-' + content + '-}'
                },
                type: "POST",
                success: function (result) {

                    dfd.resolve(result.text);

                }
            });

        }

        /**
         * @param {Object} result - Dane zwracane przez Get Task
         * @param {Object} result.processInfo - Informacje o procesie
         */
        function renderRightWidget(result) {

            if(result.processInfo) {
                var stepsArray = ['1009.006', '1007.061', '1018.042','1212.002'];
                if( stepsArray.indexOf(result.processInfo.stepId) !== -1 ) {

                    var url = Routing.generate('operationalDashboardInterfaceGetRightWidget', {
                            processInstanceId: parseInt(result.processInfo.id)
                        });

                    $.ajax({
                        url: url,
                        type: "GET",
                        success: function (data) {
                            if(data.html) {
                                $rightWidget.html(data.html);
                                $rightWidget.removeClass('hidden');
                            }
                            else {
                                $rightWidget.empty();
                                $rightWidget.empty().addClass('hidden');
                            }
                            if ($rightWidget.hasClass('hidden')) {
                               $efficiencyRanking.show();
                            } else {
                               $efficiencyRanking.hide();
                            }
                        }
                    });

                }
                else {
                    $rightWidget.empty().addClass('hidden');
                }
                if ($rightWidget.hasClass('hidden')) {
                   $efficiencyRanking.show();
                } else {
                   $efficiencyRanking.hide();
                }
            }

        }

        function changeModeView(mode) {

            if(typeof mode === "undefined") return;

            if(mode === "PARTNER_INTERFACE") {
                $mainContent.find('.create-form-wrapper').removeClass('col-xs-6').addClass('col-xs-10');
                $mainContent.find('.right-side').hide();
            }
            else {
                $mainContent.find('.create-form-wrapper').addClass('col-xs-6').removeClass('col-xs-10');
                $mainContent.find('.right-side').show();
            }

            if(_dashboardService.isReadOnlyDashboard()) {
                $mainContent.find('.create-form-wrapper').removeClass('col-md-10 col-lg-6 col-md-12 col-lg-12 col-xs-12 col-xs-6').addClass('col-md-12 col-lg-12 col-xs-12');
            }

        }

        function createOverview(result) {

            /**
             *  ----------------------------------------------------------------
             *  FUNKCJA TA JEST JUŻ PRAWDOPODOBNIE NIE UŻYWANA !!
             *  ------------------------------------------------------------
             */

            if (!result) {
                console.error('Nie ma rezultatu.');
                return false;
            }

            $reservedPopup.fadeOut();

            $formPlacementDrop.html(result.overview);

            dynamicEditAttributeModule.init({
                onModalLoad: function($bodyModal, result) {
                    prepareFormAfterInit($bodyModal);
                    initEventsHandleOnStep($bodyModal);
                }
            });

            /** Update notatki */
            refreshNotes(result.notes);

            caseNoteModule.hideNoteForm(true);

            $('.types-box').show().addClass('visible');

            $('.notification-module').show();
           $efficiencyRanking.addClass('hidden');
            $('.accordion-expand-all').click();

            _socket.cancelReserveCurrentTask();
            ticketListManager.finishTask(true);

            /* TODO - Z backendu bedzie trzeba troche więcej danych podeslac (groupProcessId, RootId) */

            _dashboardService.setCurrentTicket({
                'id': result.id,
                'stepId': 'XXX',
                'groupProcessId': result.id
            });

            hideCaseInfo();
            caseNoteModule.recalculateHeight();
        }

        function afterCreateForm($resultForm) {

            if(intervalsToClean.length) {
                $.each(intervalsToClean, function (i, ele) {
                    clearInterval(ele);
                });

                intervalsToClean = [];
            }

            var isEcall = $resultForm.hasClass('ecall-form');

            prepareFormAfterInit($resultForm);

            if(!_dashboardService.isReadOnlyDashboard() && !$resultForm.hasClass('form-readonly-mode')) {

                initEventsHandleOnStep($resultForm);

            }

            // Jeżeli company id == "Starter", to pokazuj historie atrybutów

            if(_user) {
                if(_user.companyId === "1") {
                    historyValueViewer = new HistoryValueViewer({
                        dashboardService: _dashboardService
                    });
                    historyValueViewer.initIcons($resultForm);
                }
            }

            gridstackItemsModule.updateGridItemsPosition();

            if(!isEcall) disableEnableControls();

            if($resultForm.find('.panel-edit-attribute').length > 0) {
                WidgetPanelEditAttributeModule($resultForm, _socket, {
                    onControlsLoad: function($bodyModal) {
                        prepareFormAfterInit($bodyModal);
                        initEventsHandleOnStep($bodyModal);
                    }
                });
            }

            if(!_dashboardService.isReadOnlyDashboard()) {
                checkRequiredFields();
                setBordertoYesNoButton();
                serviceSummaryTooltipModule.initServicePanel(_dashboardService.getCurrentTicket());
                serviceSummaryTooltipModule.moveProgressBar();
            }

            /** Jeżeli występuje mapa na formularzu, to uruchomienie skryptu */
            if ($resultForm.find('.atlas-map-container').length) {

                if (typeof _MapMethods === "object") {
                    _MapMethods.setResultForm($resultForm);
                }

                setTimeout(function () {
                    initWidgetMap($resultForm);
                }, 10);
            }
            else {
                _MAP = null;
            }

            if(!$resultForm.hasClass('form-readonly-mode')) {
                tabkeyModule.init();
            }

            $('.notification-module').show();
           $efficiencyRanking.addClass('hidden');
            $('.types-box').show();

            setTimeout(function() {
                $('.types-box').addClass('visible')
            },300);

            $resultForm.addClass('form-loaded');

            // console.log('%cForm loaded', 'background:#91cef3;color:#000;padding:5px');

            if(_dashboardService.isReadOnlyDashboard()) {

                if(getParameterByName('slim-mode') !== null) {
                    $('#form-generator-box').addClass('slim-mode');
                }

                var $iframe = null,
                    interval = null;

                if($('.grid-iframe-wrapper').length) {

                    $iframe = $('.grid-iframe-wrapper iframe');

                    if($iframe.length) {

                        $iframe.on('load', function (e) {

                            var $table = $iframe.contents().find("#tool-table");

                            if($table.length) {
                                $table.on('finish-refresh', function () {
                                    if(interval) {
                                        clearInterval(interval);
                                    }
                                    interval = setInterval(function() {
                                        window.status = 'form-loaded';
                                    }, 1000);
                                });
                            }
                            else {
                                if(interval) {
                                    clearInterval(interval);
                                }
                                interval = setInterval(function() {
                                    window.status = 'form-loaded';
                                }, 1000);
                            }

                        });

                    }

                }

                setTimeout(function() {

                    if($('#quiz-step').length) {
                        $('#quiz').height($('#quiz-step').height());
                        $('#form-generator-box').height($('#quiz').height());
                        $('.page-container').height($('#form-generator-box').outerHeight(true));
                    }
                    else {
                        $('.page-container').height($('.step-form-grid-stack').height() + 100);
                    }

                    if($iframe && $iframe.length) {

                        interval = setInterval(function() {
                            window.status = 'form-loaded';
                        }, 5000);

                    }
                    else {
                        window.status = 'form-loaded';
                    }

                }, 1000)

            }
        }

        function initGridStack(result, $resultForm, callback) {

            /* result = result.controls.filter(function(elem){
             var hiddenItem = elem.isVisible === false;
             return elem != hiddenItem;
             });*/

            var $atlasGridStack = $resultForm.find('.step-form-grid-stack');

            if(_dashboardService.isReadOnlyDashboard()) {
                _gridStackAtlas.staticOptions.disableOneColumnMode = true
            }

            $atlasGridStack.gridstack(_gridStackAtlas.staticOptions);
            var _gridStack = $atlasGridStack.data('gridstack');
            loadGrid(_gridStack, $resultForm, result);
            if (typeof callback === "function") {
                callback();
            }
        }

        function getHtmlOfWidget(node, multiValues, htmlContent) {

            var attrId = node.attrId || null;

            var attribute_value_id = (multiValues) ? multiValues.attribute_value_id : (node.attribute_value_id ? node.attribute_value_id : 'NULL');

            var readonly = (node.controlType === 4 && node.multi);

            return $('<div ' + ((node.multi) ? 'data-multi' : '') + ' ' + ((readonly) ? 'data-readonly-multi="true"' : '') + ' data-widget-attribute-value-id="' + attribute_value_id + '" ' + ' data-extra-data="" data-id="' + node.id + '" style="' + node.styles + '" data-widget="' + node.widget + '" data-path="' + node.path + '" data-attr-id="' + attrId + '">' +
                // ((node.multi && !copyingOfAttribute) ? '<div class="btn-multi-add" data-node-id="' + node.id + '"></div>' : '') +
                '<div class="grid-stack-item-content">' + htmlContent + '<div/></div>');

        }

        function loadGrid(grid, $resultForm, result) {

            multiTemplates = {};
            _gridStackAtlas.setMultiTemplates(multiTemplates);

            var multiValues;
            var controls = result.controls;
            var prepend = (result.prependHtml) ? result.prependHtml : null;

            if (prepend) {
                var $quiz = grid.addWidget('<div id="quiz">' + prepend + '</div>', 0, 0, 12, 5);
                handleEventsOnQuiz($quiz);
            }

            /** Dodawanie podstawowych kontrolek do grid-stack'a */

            _.each(controls, function (node) {

                multiValues = null;
                _gridStackAtlas.setLoopIndex(null);

                if (node.multi) {
                    _gridStackAtlas.setLoopIndex(0);
                    /** Zbieranie pierwszego multi z Ratrybutu do renderu */
                    multiValues = node.multi.multiValues[node.attribute_value_id];
                }

                processGridStackItem(grid, $resultForm, node, multiValues);

                if (node.multi) {
                    multiTemplates[node.id] = {
                        'node': node,
                        'amount': 1,
                        'data': node.multi,
                        'parentId': null
                    };
                }
            });

            copyingOfAttribute = true;
            _gridStackAtlas.setCoping(true);

            /** Dodawanie kontrolek multi */

            // console.groupCollapsed('multiTemplates');
            // console.log(multiTemplates);
            // console.groupEnd();

            // var sortedMultiTemplates = [];
            //
            // $.each(multiTemplates, function (j, multiElement) {
            //     sortedMultiTemplates.push(multiElement);
            // });
            //
            // sortedMultiTemplates = sortedMultiTemplates.sort(function (a, b) {
            //     // if(a.node.attribute_value_id === null && b.position === null) return false;
            //     return (parseInt(a.node.attribute_value_id) > parseInt(b.node.attribute_value_id));
            // });

            // console.log(sortedMultiTemplates);

            // multiTemplates = [];

            $.each(multiTemplates, function (i, ele) {

                updatePosition(ele.node);

                var k = 1;

                var sortedMultiValues = [];

                $.each(ele.data.multiValues, function (j, multiElement) {
                    if (j === ele.node.attribute_value_id) return true;
                    sortedMultiValues.push(multiElement);
                });

                sortedMultiValues = sortedMultiValues.sort(function (a, b) {
                    if(a.position === null && b.position === null) return false;
                    return (parseInt(a.position) > parseInt(b.position));
                });

                // console.groupCollapsed('sortedMultiValues');
                // console.log(sortedMultiValues);
                // console.groupEnd();

                $.each(sortedMultiValues, function (j, multiElement) {

                    // if (j === ele.node.attribute_value_id) return true;

                    _gridStackAtlas.setLoopIndex(k);
                    k++;

                    var copyNode = _.clone(ele.node);
                    copyNode.y = ( copyNode.y + (copyNode.height * ele.amount) );
                    copyNode.attribute_value_id = multiElement.attribute_value_id;

                    // if(multiElement.children.length && copyNode.children.length){
                    //     copyNode.children = multiElement.children;
                    // }
                    //

                    // console.groupCollapsed('copyNode');
                    // console.log(copyNode);
                    // console.log(multiElement);
                    // console.groupEnd();

                    var parentGrid = getParentGridStack(ele, grid);

                    processGridStackItem(parentGrid, $resultForm, copyNode, multiElement, function (result) {

                        if (ele.parentId !== null) {
                            var closestGrid = parentGrid.container.parent().closest('.grid-stack-static').data('gridstack');
                            closestGrid.resize(parentGrid.container.closest('.grid-stack-item'), null, parseInt(parentGrid.container.attr('data-gs-current-height')));
                        }

                        var $el = result.$item.find('[data-attribute-path="' + multiElement.attribute_path + '"]');
                        $el.attr('data-attribute-value-id', multiElement.attribute_value_id);
                        $el.val(multiElement.value);

                    });

                    multiTemplates[i].amount++;

                })


                // return false;

            });

            reDrawMultiButtons();

            copyingOfAttribute = false;
            _gridStackAtlas.setCoping(false);

            afterLoadGrid(grid, $resultForm);
        }

        function handleEventsOnQuiz($quiz) {

            $quiz.on('click', 'input[name="answer"]', function (e) {

                if (e.originalEvent.clientX === 0 && e.originalEvent.clientY === 0) {
                    /** Keyboard click */

                } else {
                    /** Mouse Click */
                    onNextStep();
                }

            }).on('keyup', 'input[name="answer"]', function (e) {
                if(e.which === 13) {
                    onNextStep();
                }
            });

        }

        function getParentGridStack(ele, defaultGrid) {

            if (ele.parentId !== null) {

                var gridStackItem = $('.grid-stack-item[data-id="' + ele.parentId + '"] .grid-stack-nested:first');
                if (gridStackItem.length && gridStackItem.data('gridstack')) {
                    return gridStackItem.data('gridstack');
                }
            }

            return defaultGrid;
        }

        function updatePosition(node) {
            var ele = $('[data-multi][data-id="' + node.id + '"]');
            if (ele.length) {
                node.y = parseInt(ele.attr('data-gs-y'));
            }
        }

        function initConfirmOnMultiRemove() {
            $('.btn-multi-remove').not('.init-confirm').confirmation({
                rootSelector: '.btn-multi-remove',
                btnOkClass: 'btn btn-danger',
                btnOkIcon: '',
                btnOkLabel: 'Tak',
                btnCancelClass: 'btn btn-default',
                btnCancelIcon: '',
                btnCancelLabel: 'Nie',
            }).addClass('init-confirm');
        }

        function afterLoadGrid(grid, $resultForm) {

            /** Dodawanie UUID dla kazdego grid stack item'u */

            $.each($resultForm.find('.grid-stack-item'), function (i, ele) {
                $(ele).attr('data-unique-id', guid());
            });

            setTimeout(function (e) {

                initConfirmOnMultiRemove();

                $resultForm.on('confirmed.bs.confirmation', '.btn-multi-remove', function () {
                });

                $resultForm.on('click', '.btn-multi-remove', function (e) {
                   e.preventDefault();

                   var $t = $(this);

                    if ($t.hasClass('loading') || $t.hasClass('disabled')) return;
                    $t.addClass('loading');

                    var $parent = $t.closest('.grid-stack-item'),
                        nodeId = $parent.attr('data-id'),
                        attributeValueId = $parent.attr('data-widget-attribute-value-id');

                    _socket.deleteAttribute({
                        attributeValueId: attributeValueId
                    }, function (result) {

                        multiTemplates[nodeId].amount--;
                        removeMultiElement(grid, $parent);
                        $parent.remove();
                        $t.removeClass('loading');

                        gridstackItemsModule.updateGridItemsPosition();
                        gridstackItemsModule.refreshVisibleInVirtualForm();
                        gridstackItemsModule.updateLocationOfItems();

                    }, function () {
                        $t.removeClass('loading');
                    });

                });

                $resultForm.on('click', '.btn-multi-up, .btn-multi-down', function (e) {
                    e.preventDefault();

                    var $t = $(this);

                    if ($t.hasClass('loading') || $t.hasClass('disabled')) return;
                    $t.addClass('loading');

                    var $parent = $t.closest('.grid-stack-item'),
                        attributeValueId = $parent.attr('data-widget-attribute-value-id'),
                        move = ($t.hasClass('btn-multi-up')) ? -1 : 1;

                    _socket.sortAttribute({
                        attributeValueId: attributeValueId,
                        move: move
                    }, function () {

                        sortMultiElement(grid, $parent, move);
                        $t.removeClass('loading');

                    }, function() {
                        $t.removeClass('loading');
                    });

                });

                $resultForm.on('click', '.btn-multi-add', function (e) {
                    e.preventDefault();

                    var $t = $(this);

                    if ($t.hasClass('loading') || $t.hasClass('disabled')) return;

                    $t.addClass('loading');
                    copyingOfAttribute = true;

                    var $parent = $t.closest('.grid-stack-item'),
                        nodeId = $parent.attr('data-id'),
                        copyNode = _.clone(multiTemplates[nodeId].node);

                    copyNode.y = ( copyNode.y + (copyNode.height * multiTemplates[nodeId].amount) );

                    // zaktualizowanie czy element jest Visible / Hidden
                    copyNode.isVisible = ($parent.attr('data-is-visible') === "true");

                    /** Odpalenie procedury */

                    if(!multiTemplates[nodeId].data.parentValueId) {

                        toastr.error("Aby zbudować strukture, potrzebny jest parentValueId.",
                            "Budowanie struktury", {timeOut: 7000});

                        copyingOfAttribute = false;
                        $t.removeClass('loading');

                        return false;
                    }

                    _gridStackAtlas.setLoopIndex(multiTemplates[nodeId].amount);

                    _socket.addStructure({
                        'parentValueId': multiTemplates[nodeId].data.parentValueId,
                        'attributeId': multiTemplates[nodeId].data.attributeId
                    }, function (resultStructure) {

                        if (resultStructure.structures.length === 0) {

                            toastr.error("Została zbudowana pusta struktura. Parametry: \"parentValueId\" = \"" + multiTemplates[nodeId].data.parentValueId + "\", \"attributeId\" = \"" + multiTemplates[nodeId].data.attributeId + "\"",
                                "Budowanie struktury", {timeOut: 7000});

                        }
                        else {

                            var multiElement = prepareMultiStructure(resultStructure.structures, copyNode);

                            copyNode.attribute_value_id = multiElement.attribute_value_id;

                            /** Append do rodzica */

                            var parentGrid = getParentGridStack(multiTemplates[nodeId], grid);

                            processGridStackItem(parentGrid, $resultForm, copyNode, multiElement, function (result) {

                                if (multiTemplates[nodeId].parentId !== null) {
                                    var closestGrid = parentGrid.container.parent().closest('.grid-stack-static').data('gridstack');
                                    closestGrid.resize(parentGrid.container.closest('.grid-stack-item'), null, parseInt(parentGrid.container.attr('data-gs-current-height')));
                                }

                                result.$item.attr('data-unique-id', guid());

                                /** Reset all values */
                                result.$item.find('input, textarea').not('[type="checkbox"], [type="radio"]').val('');

                                $.each(resultStructure.structures, function (i, ele) {

                                    var $el = result.$item.find('[data-attribute-path="' + ele.attribute_path + '"]');
                                    $el.attr('data-attribute-value-id', ele.id)

                                });

                                /** Dodanie słuchawki do inputów z numerem */

                                appendPhoneIcon(result.$item);

                                /** Nałożenie event tabów na nowe elementy */
                                tabkeyModule.setTabIndex();
                                tabkeyModule.handleTabKey(result.$item, true);

                                /** Dodanie Confirmation na minusik przy polach multi */
                                initConfirmOnMultiRemove();

                                gridstackItemsModule.updateGridItemsPosition();
                                gridstackItemsModule.refreshVisibleInVirtualForm();
                                gridstackItemsModule.updateLocationOfItems();

                            });

                            multiTemplates[nodeId].amount++;
                        }

                        reDrawMultiButtons();
                        tabkeyModule.setTabIndex();
                        copyingOfAttribute = false;
                        $t.removeClass('loading');
                    }, function(){
                        copyingOfAttribute = false;
                        $t.removeClass('loading');
                    });

                })

            }, 10)

        }

        function getSortedMultiValues(dataPath) {

            var elements = [];

            if(typeof dataPath === "undefined") {
                elements = $('.step-form-grid-stack > .grid-stack-item');
            }
            else {
                elements = $('.grid-stack-item[data-path="'+dataPath+'"]');
            }

            var sortedElements = [];

            elements.each(function (i, ele) {

                var $item = $(ele),
                    offset = $item[0].getBoundingClientRect();

                if(offset.top === 0 && offset.left === 0) return true;

                sortedElements.push({
                    el: $item,
                    t: offset.top,
                    l: offset.left,
                    w: ele.offsetWidth
                });

            });

            sortedElements.sort(function (a, b) {

                if (a.t === b.t) {
                    if (a.l > b.l) return 1;
                    else if (a.l < b.l) return -1;
                    if(a.w < b.w) return 1;

                    return 0;
                }
                else if (a.t > b.t) {
                    return 1;
                }
                else {
                    return -1;
                }

            });

            return sortedElements;
        }

        function sortMultiElement(grid, $t, move) {

            var sortedElements = getSortedMultiValues($t.attr('data-path')),
                thisWidget = grid.grid.getNodeDataByDOMEl($t),
                next = null;

            var originalX = thisWidget.x,
                originalY = thisWidget.y;

            if(move === 1) {
                sortedElements.forEach(function (obj, i) {
                    if($t[0] === obj.el[0]) {
                        next = grid.grid.getNodeDataByDOMEl(sortedElements[i+1].el);
                    }
                });
            }
            else {
                sortedElements.forEach(function (obj, i) {
                    if($t[0] === obj.el[0]) {
                        next = grid.grid.getNodeDataByDOMEl(sortedElements[i-1].el);
                    }
                });
            }

            if(next) {
                if(move === -1) {
                    grid.move(thisWidget.el, next.x, next.y);
                }
                else {
                    grid.move(next.el, originalX, originalY);
                }
            }

            setTimeout(function () {
                reDrawMultiButtons();
            }, 200);

        }

        function removeMultiElement(grid, $t) {

            // var sortedElements = getSortedMultiValues();
            // var others = [],
            //     was = false;
            //
            // var originalH = $t.attr('data-gs-height');
            //
            // sortedElements.forEach(function (obj) {
            //     if(was) {
            //         others.push(grid.grid.getNodeDataByDOMEl(obj.el));
            //     }
            //     if($t[0] === obj.el[0] && !was) {
            //         was = true;
            //     }
            // });
            //
            // others.forEach(function (widget) {
            //     grid.update(widget.el, widget.x, (widget.y - originalH));
            // });

            setTimeout(function () {
                reDrawMultiButtons();
            }, 200);

        }

        function reDrawMultiButtons() {

            var $allMulti = $('.grid-stack-item[data-multi]');

            $.each($allMulti, function (i, ele) {

                var dataPath = ele.getAttribute('data-path');
                if(!dataPath) return true;

                var elements = $('.grid-stack-item[data-path="'+dataPath+'"]');

                var sortedElements = [];

                elements.each(function (i, ele) {

                    var $item = $(ele),
                        offset = $item[0].getBoundingClientRect();

                    if(offset.top === 0 && offset.left === 0) return true;

                    sortedElements.push({
                        el: $item,
                        t: offset.top,
                        l: offset.left,
                        w: ele.offsetWidth
                    });

                });

                sortedElements.sort(function (a, b) {

                    if (a.t === b.t) {
                        if (a.l > b.l) return 1;
                        else if (a.l < b.l) return -1;
                        if(a.w < b.w) return 1;

                        return 0;
                    }
                    else if (a.t > b.t) {
                        return 1;
                    }
                    else {
                        return -1;
                    }

                });

                elements.removeClass('alone-multi-item first-multi-item last-multi-item');

                if(elements.length === 1) {
                    elements.addClass('alone-multi-item');
                }
                else if(elements.length > 1 ) {

                    if(sortedElements.length > 1) {
                        sortedElements[0].el.addClass('first-multi-item');
                        sortedElements[sortedElements.length - 1].el.addClass('last-multi-item');
                    }
                    else {
                        elements[0].classList.add('first-multi-item');
                        elements[elements.length - 1].classList.add('last-multi-item');
                    }

                }
            })
        }

        function handleEventsOnNewControls($content) {

            customCheckBoxHandle($content);
            customSelect2Handle($content);

        }

        function prepareMultiStructure(structures, node) {

            var multiElement = {};

            var parentValueId = parseInt(node.multi.parentValueId);

            var arr = _.filter(structures, function (ele) {
                return (parseInt(ele.parent_attribute_value_id) === parentValueId)
            });

            if (arr.length > 0) {

                var ele = arr[0];

                multiElement = {
                    attribute_path: ele.attribute_path,
                    attribute_value_id: parseInt(ele.id),
                    children: {},
                    group_process_id: _dashboardService.getCurrentTicket('groupProcessId'),
                    parent_attribute_value_id: ele.parent_attribute_value_id,
                    step_id: _dashboardService.getCurrentTicket('stepId'),
                    value: null
                };

                recursivePrepareMultiStructure(multiElement, structures);

            }

            return multiElement;

        }

        function recursivePrepareMultiStructure(parent, structures) {

            var arr = _.filter(structures, function (ele) {
                return (parseInt(ele.parent_attribute_value_id) === parent.attribute_value_id)
            });

            if (arr.length > 0) {

                _.each(arr, function (ele) {

                    var id = getIdFromPath(ele.attribute_path);

                    parent.children[id] = {
                        attribute_path: ele.attribute_path,
                        attribute_value_id: ele.id,
                        children: {},
                        group_process_id: _dashboardService.getCurrentTicket('groupProcessId'),
                        parent_attribute_value_id: ele.parent_attribute_value_id,
                        step_id: _dashboardService.getCurrentTicket('stepId'),
                        value: null
                    };

                    recursivePrepareMultiStructure(parent.children[id].children, structures);

                });

            }

        }

        function getIdFromPath(path) {
            if (path.indexOf(',') > -1) {
                var ids = path.split(",");
                return ids[ids.length - 1];
            }

            return path;
        }

        function processGridStackItem(grid, $resultForm, node, multiValues, callback) {

            // console.groupCollapsed('processGridStackItem');
            // console.log(node);
            // console.groupEnd();

            multiValues = multiValues || null;

            var htmlString = node.html,
                newWidget = null;

            if (htmlString) {

                newWidget = grid.addWidget(getHtmlOfWidget(node, multiValues, decodeEntities(node.html)), node.x, node.y, node.width, node.height);

                if(node.customClass) {
                    newWidget.addClass(node.customClass);
                }

                _gridStackAtlas.onResizeTextArea(newWidget);
                handleEventsOnNewControls(newWidget);

            }
            else {

                var html = _gridStackAtlas.createNestedGridStack(node, multiValues);

                newWidget = grid.addWidget(getHtmlOfWidget(node, multiValues, ''), node.x, node.y, node.width, node.height);

                if(node.customClass) {
                    newWidget.addClass(node.customClass);
                }

                newWidget.find('.grid-stack-item-content').append('<div>' + html + '</div>');
                newWidget.find('.grid-stack').gridstack(_gridStackAtlas.staticOptions);

                $.each(newWidget.find('.grid-stack-item'), function (i, ele) {
                    _gridStackAtlas.onResizeTextArea($(ele));
                    handleEventsOnNewControls($(ele));
                });

                // dodawanie JavaScript'u do elementu

                if(node.extraData && node.extraData.javascript) {

                    try {
                        var script = window.atobUTF8(node.extraData.javascript);

                        var s = document.createElement('script');
                        s.type = "text/javascript";
                        s.text = script;

                        newWidget.children('.grid-stack-item-content').append(s);
                    }
                    catch (e) {
                        console.error(e);
                    }

                }

            }

            if (typeof node !== "undefined" && typeof node.isVisible !== "undefined") {
                newWidget.attr('data-is-visible', (node.isVisible == true) ? 'true' : 'false');
            }

            var extraData = (node.extraData) ? JSON.stringify(node.extraData) : "";

            if (node.id && extraData) {
                $resultForm.find('.grid-stack-item[data-id="' + node.id + '"]').attr('data-extra-data', extraData);
            }

            if (typeof callback === "function") {
                callback({
                    '$item': newWidget
                })
            }

        }

        function checkReloadForm($resultForm) {

            /** Sprawdza czy formularz ma opcje auto-reload */

            if ($resultForm.hasClass('reload-blank-form')) {

                /**  Jeżeli nie ma danych to refresh po 5 sek */

                if (!$('#EVENT_ATP_ID').val() && !wasReloaded) {

                    _status.startCounting();

                    reloadingTimeout = setTimeout(function () {

                        wasReloaded = true;

                        _socket.getTask(_dashboardService.getCurrentTicket('id'), function (result) {
                            _status.stopCounting();
                            createForm(result);
                        }, function () {
                            _status.stopCounting();
                        });

                    }, 5000);

                }

            }

        }

        function prepareFormAfterInit($resultForm) {

            if(!_dashboardService.isPublicDashboard()) {
                appendPhoneIcon($resultForm);
            }

            var windowHeight = $window.height(),
                minusHeight = ($resultForm.hasClass('.operation-dashboard-form')) ? 32 : 92;

            $('.form-placement-message').addClass('disabled');

            // datepickery

            var dateTimeOptions = customDatepickerOptions;
            var options = datePickerOptions;

            options.dateFormat = 'dd-mm-yyyy';
            options.timeFormat = false;
            options.timepicker = false;

            if( !$('body').hasClass('print-mode')) {

               $resultForm.find('.date-picker').each(function(i, ele) {
                   var $t = $(ele);
                   if(!$t.closest('.grid-stack-item').hasClass('default-calendar')) {
                       $t.datepicker(options);
                   }
               });


               dateTimePickerOptions.dateFormat = 'dd-mm-yyyy';

               $resultForm.find('.date-time-picker').each(function(i, ele) {
                   var $t = $(ele);
                   if(!$t.closest('.grid-stack-item').hasClass('default-calendar')) {
                       $t.datepicker(dateTimePickerOptions);
                   }
               });

               $resultForm.find('.date-picker, .date-time-picker').not(':disabled').each(function (i, ele) {
                   if(ele.value)
                   {
                       var date = new Date(ele.value),
                           $ele = $(ele);

                       if($ele.closest('.grid-stack-item').hasClass('default-calendar')) {
                           // $ele.val(moment(ele.val()).format("YYYY-MM-DDTkk:mm"));
                       }
                       else {
                           if(!isNaN( date.getTime()))
                           {
                               ele['disableTrigger'] = 1;
                               $(ele).data('datepicker').selectDate(date);
                               ele['disableTrigger'] = 0;
                           }
                       }

                   }
               });

               $.each($resultForm.find('.date-picker, .date-time-picker'), function (i, ele) {
                   var $t = $(ele),
                       _datePicker = $t.data('datepicker'),
                       date;

                   if (!_datePicker) return false;

                   if ($t.hasClass('from-now')) {
                       _datePicker.update('minDate', new Date());
                   }

                   if ($t.hasClass('to-now')) {
                       _datePicker.update('maxDate', new Date());
                   }

                   if ($t[0].hasAttribute('data-custom-min-date')) {
                       date = moment($t.attr('data-custom-min-date'), 'YYYY-MM-DD HH:mm');
                       _datePicker.update('minDate', date.toDate());
                   }

                   if ($t[0].hasAttribute('data-custom-max-date')) {
                       date = moment($t.attr('data-custom-max-date'), 'YYYY-MM-DD HH:mm');
                       _datePicker.update('maxDate', date.toDate());
                   }

               });


            }

            $resultForm.find('.upload-file-widget').each(function(i, ele) {

                AtlasUploaderDocumentModule({
                    $wrapper: $(ele).find('.atlas-upload-file-wrapper'),
                    $result: $(ele).find('.uploaded-files'),
                    params: {
                        autoUpload: true,
                        maxFileSize: 25000000 // 25MB
                    }
                });

            });

            dynamicalWidgetModule.loadWidgets(function($widget) {

                if($widget.attr('data-name') === "matrix") {
                    dynamicalWidgetModule.checkMatrixInputs($widget);
                }


                if($widget.attr('data-name') === "faq") {

                    dynamicalWidgetModule.initFaq($widget);

                    // $.when(promise.promise()).then(function ($result) {
                    //
                    // });

                }

                customSelect2Handle($widget);
                customCheckBoxHandle($widget);

                if(_user) {
                    if(_user.companyId === "1") {
                        historyValueViewer = new HistoryValueViewer({
                            dashboardService: _dashboardService,
                            appendHistory: false,
                            containerSelector: '.wrapper-form-control',
                            findSelector: '.custom-input-wrapper'
                        });
                        historyValueViewer.initIcons($resultForm);
                    }
                }

                // initEventsHandleOnStep($widget);

            });

        }

        var phoneIconHtmlPrototype = '<button type="button" class="widget-instant-call"><i class="fa fa-phone static"></i></button>';

        function appendPhoneIcon($resultForm) {

            $resultForm.find('input[data-attribute-path$="197"]').each(function (i, ele) {
                $(ele).parent().append(phoneIconHtmlPrototype);
            });

            if($resultForm.hasClass('phone-call-icon')) {

                $resultForm.find('.custom-input-wrapper').each(function (i, ele) {
                    $(ele).append(phoneIconHtmlPrototype);
                });

                $resultForm.find('.wrapped-select select').each(function (i, ele) {
                    $(ele).after(phoneIconHtmlPrototype);
                });

            }
            else {

                $resultForm.find('.grid-stack-item.phone-call-icon .custom-input-wrapper').each(function (i, ele) {
                    $(ele).append(phoneIconHtmlPrototype);
                });

                $resultForm.find('.grid-stack-item.phone-call-icon .wrapped-select select').each(function (i, ele) {
                    $(ele).after(phoneIconHtmlPrototype);
                });

            }

            $resultForm.find('.widget-instant-call').on('click', function (e) {
                e.preventDefault();
                var $t = $(this);
                $t.prop('disabled', true);

                var _number = $t.parent().find('> [data-attribute-value-id]').val();
                callNumber(_number);

                setTimeout(function () {
                    $t.prop('disabled', false);
                }, 2000);

            });

        }

        function displayHumanTime(seconds) {
            if(Math.floor(seconds/60) == 0) {
                return ((seconds % 60) + "s");
            }
            else {
                return (Math.floor(seconds/60) + "min " + (seconds % 60) + "s");
            }
        }

        function initMiniCounter($ele) {

            var time = parseInt($ele.text());
            $ele.text(displayHumanTime(time));

            return setInterval(function () {
                time++;
                $ele.text(displayHumanTime(time));
            }, 1000);

        }

        function __showModalWithExtraActions() {

            var $t = $(this);
            var $_menu = $t.find('.sub-menu-content');

            if(!$t.hasClass('active')) {
                $('body').on('click', _hideSubMenu);
            }
            else {
                $('body').off('click', _hideSubMenu);
            }

            $t.toggleClass('active');

            if(!$t.hasClass('loaded')) {

                $t.addClass('loaded');

                $.ajax({
                    url: Routing.generate('operation_dashboard_get_extra_actions', {
                        processInstanceId: _dashboardService.getCurrentTicket('id')
                    }),
                    type: "GET",
                    error: function(error) {
                        if(error.statusText !== "abort") {
                            handleAjaxResponseError(error);
                        }
                    },
                    success: function (result) {

                        $_menu.html(result.html);
                        _initExtraActionsModal($_menu, _dashboardService, tryGetTask, _socket, {
                            onControlsLoad: function($form) {
                                prepareFormAfterInit($form);
                                initEventsHandleOnStep($form);
                            }
                        });

                    }
                });

            }

        }
        
        function _hideSubMenu(e) {
            if($(e.target).closest('.buttons-wrapper').length === 0) {
                $('.buttons-wrapper .js-btn.active').removeClass('active');
                $('body').off('click', _hideSubMenu);
            }
        }

        function __muteAction(e) {

            var $t = $(this),
                message = {
                    header: $t.hasClass('mute-active') ? 'Odblokowanie komunikacji' : 'Blokowanie komunikacji',
                    body:  '',
                    // bigSize: true
                };

            var dfd = jQuery.Deferred();
            var options = {
                'buttonConfirm' : {'html' : '<i class="fa fa-check"></i> Potwierdź'},
                'buttonCancel' : {'html' : '<i class="fa fa-times"></i> Anuluj'}
            };
            _f.openModal(message, function(){
                var value = $('#mute-type').val();
                $.ajax({
                    url: Routing.generate('ajax_update_case_mute', {
                        rootId: _dashboardService.getCurrentTicket('rootId'),
                        value: value,
                    }),
                    type: "POST",
                    success: function (result) {
                        refreshRightSide(_dashboardService.getCurrentTicket('id'));
                        toastr.info("Zmieniono tryb komunikacji. Dodano notatkę systemową.", {timeOut: 5000});
                        $t.removeClass('mute-active');
                        if($('#mute-type').val() > 0 ){
                            $t.addClass('mute-active');
                        }

                    }
                });

            }, null, false, dfd, options);


            $.ajax({
                url: Routing.generate('ajax_mute_modal_content', {
                    groupProcessInstanceId: _dashboardService.getCurrentTicket('groupProcessId')
                }),
                type: "GET",
                success: function (result) {

                    dfd.resolve(result.html);

                }
            });

        }

        function initEventsHandleOnStep($resultForm) {

            var $nextButton = $resultForm.find('.next-button');

            /* Wyłączenie submit'a na formularzu */

            $resultForm.on('submit', function (e) {
                e.preventDefault();
            });

            if($nextButton.length) {
                _dashboardService.setNextStepLadda($nextButton);
            }
            else {
                _dashboardService.setNextStepLadda(null);
            }

            $resultForm.find('.next-button:not(#forward-button,#reopen-case-button)').on('click', function (e) {
                e.preventDefault();
                onNextStep();
            });

            $resultForm.find('#forward-button').on('click', function (e) {
                e.preventDefault();
                forwardStep($(this).attr('data-variant'), $(this).attr('data-content'));
                return false;
            });

            $resultForm.find('#reopen-case-button').on('click', function (e) {
                e.preventDefault();
                reopenCase();
                return false;
            });

            $resultForm.find('#form-prev-button').on('click', function (e) {
                e.preventDefault();
                onPrevStep($(this).attr('data-previous-process-id'));
            });

            $resultForm.find('#active-previous-step').on('click', function (e) {
                e.preventDefault();
                activeAndLoadPreviousStep($(this).attr('data-previous-process-id'));
            });

            $resultForm.find('.cancel-step').on('click', function (e) {
                e.preventDefault();
                cancelStep($(this).attr('data-description'));
            });

            if(isMobile())
            {
                // Obracanie "Aktywnym" sposobem pobierania informacji do kroku "Mobilne przyjęcie sprawy 2/3 "
                $('.mobile-case-mode #form-generator-box .square .btn').on('click',function(){
                    $('.square .btn.active').removeClass('active');
                    $(this).addClass('active');
                });
            }
            var items = $resultForm.find('.custom-input, .custom-select');
            if(items.length) {
                items.each(function (index) {
                    var required = $(this).attr('required');

                    if (!($(this).val() == "")) {
                        $(this).addClass('validation-success')
                    }
                });
            }

            $resultForm.find('.js-show-extra-actions').on('click', __showModalWithExtraActions);

            // $resultForm.find('.js-show-status-services').on('click', __showStatusServices);
            // $resultForm.find('.js-show-cost-services').on('click', __showCostsServices);
            $resultForm.find('.js-mute-action').on('click', __muteAction);

            $resultForm.find('.js-show-active-users').on('click', function (e) {

                var $t = $(this),
                    message = {
                    header: 'Aktywni użytkownicy w sprawie',
                    body: ''
                },
                    counters = [];

                var dfd = jQuery.Deferred();

                var $modal = _f.openModal(message, function() {

                    $.each(counters, function(i, ele){
                        clearInterval(ele);
                    })

                }, null, true, dfd);

                $.ajax({
                    url: Routing.generate('operation_dashboard_get_active_users_in_case', {
                        rootId: _dashboardService.getCurrentTicket('rootId')
                    }),
                    type: "GET",
                    success: function (result) {
                        dfd.resolve(result.html);

                        $modal.find('.mini-counter').each(function(i, ele){
                            counters.push(initMiniCounter($(ele)));
                        });

                        $t.find('.badge').text(result.amount_user);
                    }
                });

            });

            $resultForm.find('.abandon-step').on('click', function (e) {
                e.preventDefault();
                abandonStep();
            });

            $resultForm.find('.postpone-task').on('click', function (e) {
                e.preventDefault();
                postponeProcessModule.openModel();
                // postponeTask();
            });

            /** init Select2 and events on it */
            customSelect2Handle($resultForm);

            /** Select dla sytuacji szczególnej */

            $resultForm.find('#special-situation-btn').on('click', function (e) {
                e.preventDefault();

                if (e.originalEvent.clientX === 0 && e.originalEvent.clientY === 0) {
                    /** Keyboard click */
                } else {
                    /** Mouse Click */

                    var $t = $(this);
                    $t.prev().fadeIn().css("display","inline-block");
                    $t.prev().find('select').select2('open');

                }

            }).prev().find('select').on('select2:close', function () {
                var $t = $(this);
                setTimeout(function () {
                    if(!$t.next().hasClass('select2-container--open')) {
                        $t.closest('.wrapped-select').fadeOut();
                    }
                }, 1000)
            });

            /** Jakieś magiczne rzeczy dla e-call */

            if ($('#EventQualificationSelector').length) {
                checkEcallStatusesVisibility();
                $('#EventQualificationSelector').on('change', function () {
                    checkEcallStatusesVisibility();
                });
            }

            /** Dzwonienie */

            $resultForm.find('.callNow').on('click', function (e) {
                callNumber($(this).attr('data-number-value'));
            });

            /** Nasłuchiwanie na zmianach inputów - Update danych do bazy */

            $resultForm.on('change', 'input[data-step-id], textarea[data-step-id]', function (e) {

                if (isSimulation) return;

                var $t = $(this),
                    value = null;

                if($t.hasClass('date-picker') || $t.hasClass('date-time-picker')) {

                    if(typeof e.isTrigger === "undefined") {
                        if (e.target.value) {

                            var format = ($t.hasClass('.date-picker')) ? 'DD-MM-YYYY' : 'DD-MM-YYYY H:mm';

                            if($(e.target).data('datepicker')) {

                                var _date = moment(e.target.value, format).toDate();

                                if (!isNaN(_date.getTime())) {
                                    $(e.target).data('datepicker').selectDate(_date);
                                }

                                return false;
                            }
                            else {
                                // Prawdopodobnie użyty custom-calendar
                                value = moment(e.target.value).format(format);
                            }

                        }
                    }

                }

                if(value === null) {

                    switch ($t.attr('type')) {
                        case 'number':
                        {
                            value = $t.val();
                            break;
                        }
                        case 'checkbox':
                        {
                            value = ($t.is(':checked') ? 1 : 0);
                            break;
                        }
                        default:
                        {
                            value = $t.val()
                        }
                    }

                }

                // $nextButton.prop('disabled', true);

                /** Dorzucic walidacje na Backend, gdy krok jest isPreview - zeby nie edytowac pól "not-allow-edit" */

                _socket.setAttribute({
                    "_this": $t,
                    "path": $t.attr('data-attribute-path'),
                    "groupProcessId": $t.attr('data-group-process-id'),
                    "stepId": $t.attr('data-step-id'),
                    "valueId": $t.attr('data-attribute-value-id'),
                    "value": value,
                    "valueType": $t.attr('data-type'),
                    "formControl": $t.closest('.grid-stack-item[data-id]').attr('data-id')
                }, function (result) {

                    if (result.attributeValueId) {
                        $t.attr("data-attribute-value-id", result.attributeValueId);
                    }

                    displayValidation(result, $t);

                });

            });
            $resultForm.find('input[data-step-id], textarea[data-step-id]').on('update-value',function(){

                var $input = $(this),
                $t = $input.closest('.grid-stack-item'),
                controlId = $t.attr('data-id'),
                groupProcessId = _dashboardService.getCurrentTicket('groupProcessId');

                url = Routing.generate('operational_dashboard_updater_value_control', {
                    controlId: controlId,
                    groupProcessId: isNaN(groupProcessId) ? null : groupProcessId ,
                    processInstanceId: _dashboardService.getCurrentTicket('id'),
                    type: 'input'
                });

                if($input.hasClass('date-picker') || $input.hasClass('date-time-picker') || $input.attr('type') === 'checkbox')  {
                   return;
                }

                $.ajax({
                    url: url,
                    type: "GET",
                    success: function (data) {
                        $input.val(data.values);
                        $input.trigger('update-value-success');
                    }
                })

            });

            $resultForm.on('refresh-content', '.js-visual-element-text[data-uid]', onUpdateContent);

            customCheckBoxHandle($resultForm);
            showDependentInput($resultForm);

            parserInputsModule.parseNumber($resultForm.find('input[type="number"]'));

            setTimeout(function () {

                $.each($('[data-auto-set="true"]'), function(i, ele) {

                    if($(this).closest('.disable-auto-set').length === 0) {
                        if(ele.value) {
                            $(ele).trigger('change');
                        }
                    }

                });

            }, 100)
        }

        function onUpdateContent() {

            var $t = $(this),
                groupProcessId = _dashboardService.getCurrentTicket('groupProcessId');

            var controlId = $t.closest('.grid-stack-item').attr('data-id'),
                url = Routing.generate('operational_dashboard_updater_value_control', {
                    controlId: controlId,
                    groupProcessId: isNaN(groupProcessId) ? null : groupProcessId,
                    processInstanceId: _dashboardService.getCurrentTicket('id'),
                    type: 'text_block'
                });

            $.ajax({
                url: url,
                type: "GET",
                success: function (data) {
                    $t.html(data.values);
                    $t.trigger('refresh-content-success');
                }
            })

        }

        function getProtoOfValidationTooltip(uuid, desc) {

            return $('<div class="tooltip-control" role="tooltip" id="tooltip_'+uuid+'">' +
                        '<div class="tooltip-arrow"></div>' +
                        '<div class="tooltip-inner">'+desc+'</div>' +
                    '</div>');

        }

        /** WALIDACJA */

        var controlsBind = [];

        function showTooltip() {
            var $el = $(this);
            if($el.hasClass('validation-error')) {
                var $itemGrid = $el.closest('.grid-stack-item');
                var uuid = $itemGrid.attr('tooltip-uuid');
                if(uuid) {
                    $('#tooltip_' + uuid).fadeIn();
                }
            }
        }

        function hideTooltip() {
            var $el = $(this);
            var $itemGrid = $el.closest('.grid-stack-item');
            var uuid = $itemGrid.attr('tooltip-uuid');
            if(uuid) {
                setTimeout(function () {
                    $('#tooltip_' + uuid).fadeOut();
                }, 500);
            }
        }

        function getOffsetOfParent($el) {
            var offset = 0;
            $el.parents('.grid-stack-item').each(function (i, ele) {
                if(i === 0) return true;
                offset += ele.offsetTop;
            });
            return offset;
        }

        function addTooltip($formGenerator, $elements, desc) {

            if(isMobile()){
                return;
            }
            var $el = ($elements.length > 1) ? $elements.first() : $elements;

            var uuid = (Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000).toString() + ((new Date()).getTime()).toString();
            var $html = getProtoOfValidationTooltip(uuid, desc);

            var $itemGrid = $el.closest('.grid-stack-item'),
                $lastParent = $el.parents('.grid-stack-item').last(),
                extraLeft = 0, _top, _left, _right, extraTop;

            /** Nie zmieści się ani po prawej, ani po lewej stronie */

            $html.addClass('top');

            extraLeft = ($lastParent[0] !== $itemGrid[0]) ? ( $lastParent.position().left + 10) : 0;
            // extraTop = ($lastParent[0] !== $itemGrid[0]) ? ( $lastParent.position().top ) : 0;
            extraTop = getOffsetOfParent($el);
            _left = $itemGrid.position().left + extraLeft + 30;

            if(($itemGrid.width()<100)) {
                _left -= ((200 - ($itemGrid.width() / 2)) / 2);
            }

            $html.css({
                top: extraTop + $itemGrid.position().top + $itemGrid.height() + 52 + 'px',
                left: _left + 'px'
            });

            $itemGrid.attr('tooltip-uuid', uuid);
            $formGenerator.append($html);

            if($elements.length === 1 && $elements[0].tagName !== "SELECT") {
                controlsBind.push($el);
                $el.on('focus', showTooltip);
                $el.on('blur', hideTooltip);
            }

            setTimeout(function () {
                if(!$el.is(':focus')) $html.fadeOut(500);
            }, 3000);
        }

        function isMobile(){
            return $('.mobile-case-mode').length;
        }

        function addMobileValidationError($formGenerator, $elements, desc) {

            var $html = getProtoTypeOfMobileValidation(desc);

            $elements.each(function(index)
            {
                $(this).parent().prepend($($html));
            });

        }


        function getProtoTypeOfMobileValidation(desc) {

            return $('<div class="validation-mobile-control">' +
                '<div class="validation-control-inner"><i class="fa fa-exclamation-triangle"></i> '+desc+'</div>' +
                '</div>');

        }

        function displayValidation(result, $el) {

            /** Błąd walidacji */
            if (result.error > 0) {

                /** Walidacja grupowa przy next step */

                var $formGenerator = $('#form-generator-box');

                if($el === null && result.controls && Array.isArray(result.controls)) {

                    var $control;

                    controlsBind.forEach(function (t) {
                        t.off('focus', showTooltip);
                        t.off('blur', hideTooltip);
                    });

                    controlsBind = [];

                    $('.validation-error').removeClass('validation-error');
                    $('.tooltip-control').remove();
                    $('.validation-mobile-control').remove();

                    result.controls.forEach(function (noValidControl) {

                        $control = $('[data-attribute-value-id="'+noValidControl.attributeValueId+'"]');
                        $control.addClass('validation-error');
                        $control.removeClass('validation-success');

                        addTooltip($formGenerator, $control, noValidControl.desc);
                        if(isMobile())
                        {
                            addMobileValidationError($formGenerator, $control, noValidControl.desc);
                        }


                    });

                    if($control) {
                        var $first = $control.first();

                        if($first[0].type === "radio") {
                            if($first.parent().hasClass("custom-radio-button")) {
                                $control.first().focus();
                            }
                        }
                        else {
                            $control.first().focus();
                        }

                    }

                }
                /** Validacja przy setAttribute */

                else if ($el !== null) {

                    /** Wysłanie Event'a ze błąd zapisu */
                    if(typeof CustomEvent !== 'undefined') {
                        $el[0].dispatchEvent(new CustomEvent("save-attribute", {
                            'detail': {
                                success: false,
                                desc: result.desc
                            }
                        }));
                    }

                    if(dynamicEditAttributeModule.isEdit()) {
                        return false;
                    }

                    if(!$el.hasClass('validation-error')) {

                        $el.addClass('validation-error');
                        addTooltip($formGenerator, $el, result.desc);
                        if(isMobile())
                        {
                            addMobileValidationError($formGenerator, $el, result.desc);
                        }
                        $el.removeClass('validation-success');
                    }
                    else {
                        var $itemGrid = $el.closest('.grid-stack-item');
                        $itemGrid.find('.validation-mobile-control').remove();
                        if(isMobile())
                        {
                            addMobileValidationError($formGenerator,$el,result.desc);
                        }
                        var uuid = $itemGrid.attr('tooltip-uuid');
                        if(uuid) {
                            $('#tooltip_' + uuid + ' .tooltip-inner').html(result.desc);
                        }
                    }
                    // toastr.error("Wartość nie została zapisana.", "Niepoprawne dane.", {timeOut: 5000});

                }

                _dashboardService.blockNextStep();

            } else {

                if($el !== null) {

                    /** Wysłanie Event'a ze zapisano */

                    if(typeof CustomEvent !== 'undefined') {
                        $.each($el, function (i, ele) {
                            ele.dispatchEvent(new CustomEvent("save-attribute", {
                                'detail': {
                                    success: true
                                }
                            }));
                        });
                    }

                    if(dynamicEditAttributeModule.isEdit()) {
                        return false;
                    }

                    var $itemGrid = $el.closest('.grid-stack-item'),
                        uuid = $itemGrid.attr('tooltip-uuid'),
                        _index = null;


                    var isRadio = $itemGrid.find($el[0].tagName).length > 1;

                    var items = $itemGrid.find('.custom-input, .custom-select');
                    if(items.length) {
                        items.each(function (index) {
                            var required = $(this).attr('required');

                            if (!($(this).val() == "")) {
                                $(this).addClass('validation-success')
                            }
                        });
                    }
                    if(isRadio) {

                        $itemGrid.find('.validation-error').removeClass('validation-error');
                    }
                    else {

                        $el.removeClass('validation-error').removeClass('required');
                    }


                    controlsBind.forEach(function (t, index) {

                        if(t[0] === $el[0]) {
                            $el.off('focus', showTooltip);
                            $el.off('blur', hideTooltip);
                            _index = index;
                        }

                    });

                    if (_index !== null) {
                        controlsBind.splice(_index, 1);
                    }

                    if(uuid) {
                        $('#tooltip_' + uuid).remove();
                    }
                    $itemGrid.find('.validation-mobile-control').remove();

                }

                if ($('.validation-error').length === 0 && $('.next-button.force-disabled').length == 0) {
                    _dashboardService.unBlockNextStep(true);
                }
                else {
                    _dashboardService.unBlockNextStep();
                }
            }

        }

        function customSelect2Handle($resultForm) {

                if (typeof $resultForm === "undefined") {
                    $resultForm = $('.operation-dashboard-form');
                }

                var $newSelects = $resultForm.find('select[data-attribute-path]').not('.select2-hidden-accessible').not('.bind-select');

                $newSelects.on('change', function () {

                    if (isSimulation) return;
                    var $t = $(this);
                    var value = $t.val();
                    if($t.hasClass('not-set-attr')) return;
                    if (value == '') {
                        $t.siblings('.select2').addClass('validation-error');
                    }
                    else if (value === '---'){
                        value = null;
                    }
                    else {
                        $t.siblings('.select2').removeClass('validation-error');
                    }

                    _socket.setAttribute({
                        "_this": $t,
                        "path": $t.attr('data-attribute-path'),
                        "groupProcessId": $t.attr('data-group-process-id'),
                        "stepId": (_dashboardService.isPreviewStep()) ? 'xxx' : $t.attr('data-step-id'),
                        "valueId": $t.attr('data-attribute-value-id'),
                        "value": value,
                        "valueType": $t.attr('data-type') ? $t.attr('data-type') : 'string',
                        "formControl": $t.closest('.grid-stack-item[data-id]').attr('data-id')
                    }, function (result) {

                        if (result.attributeValueId) {
                            $t.attr('data-attribute-value-id', result.attributeValueId);
                        }

                        $t.attr('data-value', $t.val());

                        displayValidation(result, $t);

                    });

                }).on('update-options', function (e) {

                    var $t = $(this);

                    var controlId = $t.closest('.grid-stack-item').attr('data-id'),
                        url = Routing.generate('operational_dashboard_updater_value_control', {
                            controlId: controlId,
                            groupProcessId: this.getAttribute('data-group-process-id'),
                            type: 'select'
                        });

                    $.ajax({
                        url: url,
                        type: "GET",
                        success: function (data) {
                            if (data.values && $t && $t.data('select2')) {
                                $t.select2('destroy').empty().select2({data: data.values}).select2('');
                                $t.trigger('change');
                            }
                            else if(data.values) {
                                // TODO - uzupełnienie zwykłego selecta
                            }
                        }
                    })

                });

                /** Ustawienie pierwszej wartości jako domyślna */

                $.each($newSelects, function (i, ele) {

                    var $t = $(ele);

                    $t.addClass('bind-select');

                    if ($t.attr('value')) {
                        if(!isDefaultSelect($t)) {
                            $t.select2();
                        }
                        return true;
                    }

                    if ($t.attr('data-value')) {
                        if ($t.attr('data-value') !== "") {
                            $t.val($t.attr('data-value'));
                        }
                    }

                    if(!isDefaultSelect($t)) {
                        $t.select2();
                    }

                });

            $('#case-previous-locations').unbind('change').on('change', function () {
                var val = $(this).val();
                var target = $('#atlas-map-step').attr('data-location-path');

                if(target) {

                    var instanceId = _dashboardService.getCurrentTicket('id');
                    if(val != '' && typeof target !== "undefined" && target != '') {
                        $.ajax({
                            url: Routing.generate('case_ajax_copy_structure'),
                            data: {
                                sourceId: val,
                                targetPath: target,
                                groupProcessId: _dashboardService.getCurrentTicket('groupProcessId'),
                                instanceId: instanceId
                            },
                            type: "POST",
                            success: function () {
                                _socket.getTask(instanceId, function (result) {
                                    createForm(result);
                                }, false, false);
                            }
                        });
                    }

                }
                else {
                    toastr.warning('Ścieżka lokalizacji nie istnieje...');
                }

            });
        }

        function isDefaultSelect($t) {
            return $t.closest('.grid-stack-item').hasClass('default-select');
        }

        function callNumber(number) {

            if (!number) toastr.info("", "Niepoprawny numer", {timeOut: 3000});

            var cEvent = new CustomEvent("telephony-call-number", {
                'detail': {
                    number: number
                }
            });

            parent.document.dispatchEvent(cEvent);

        }

        function handleChooseTypeSearch($resultForm, searchType ) {

            /** Zmiana typów wyszukiwania na mapie */

            var $boxTypeSearch = $resultForm.find('#choose-type-search-buttons'),
                $buttonsType = $boxTypeSearch.find('li'),
                $searchBar = $('#search-bar-control');

            searchType.type = $boxTypeSearch.attr('data-type-search');
            $boxTypeSearch.find('li[data-type-search="'+searchType.type+'"]').addClass('active');

            if(_dashboardService.isCurrentTicket()) {
                _socket.getAttribute({
                    "path": '513',
                    "groupProcessId": _dashboardService.getCurrentTicket('groupProcessId'),
                    "stepId": _dashboardService.getCurrentTicket('stepId'),
                    "valueType": 'int'
                }, function (result) {

                    if(result.value === "1") {
                        $boxTypeSearch.find('li[data-type-search="object"]')[0].click();
                    }

                });
            }

            $buttonsType.on('click', function (e) {
                e.preventDefault();

                var $t = $(this);

                if($t.hasClass('active')) {
                    return;
                }

                $searchBar.removeClass('no-result');
                $buttonsType.removeClass('active');
                $t.addClass('active');

                var type = this.getAttribute("data-type-search");

                $searchBar.attr('data-type-search', type);

                if(type === "address" || type === "object") {
                    var $mapSearch = $searchBar.find('.map-search');
                    $mapSearch.attr('placeholder', $mapSearch.parent().attr('data-placeholder-' + type));
                    $mapSearch.val(searchType.lastValue[type]);
                } else if (searchType.lastValue[type]) {

                    // if(type === "coordinate") {
                    //
                    // }
                    // else if(type === "coordinate"){
                    //     if(searchType.lastValue[type])
                    //         $searchBar.find()
                    // }
                }

                $searchBar.find('.type-bar:visible input').first().focus();
                searchType.type = type;
            });

            $resultForm.find('.search-by-coordinate').on('paste', function () {

                var $t = $(this);

                setTimeout(function () {

                    var coordinates = $t.val(),
                        charsRegex = /["#&_,.']$/,
                        coordinatesRegex = /([-0-9]{1,4}[,.]{1}[0-9]{2,9})/g, // współrzędne
                        coordinatesRegex2 = /([0-9]{1,3}[ ][0-9]{1,2}[ ][0-9]{1,2}([,.][0-9]{1,9})*)/g; // stopnie i minuty - ale to chyba nie śmiga do końca

                    var coordinateArray = tryParseGoogleMapUrl(coordinates);

                    /** Jeżeli adres nie jest z google map */
                    if(coordinateArray.length !== 2) {
                        if(coordinatesRegex2.test(coordinates)) {
                            coordinateArray = coordinates.match(coordinatesRegex2);
                        }

                        if(coordinatesRegex.test(coordinates) && coordinateArray.length < 2) {
                            coordinateArray = coordinates.match(coordinatesRegex);
                        }
                    }

                    if(coordinateArray.length >= 2) {
                        $resultForm.find('.search-by-coordinate[data-coordinate-type="lat"]').val(coordinateArray[0].replace(charsRegex, ''));
                        $resultForm.find('.search-by-coordinate[data-coordinate-type="lng"]').val(coordinateArray[1].replace(charsRegex, ''));
                    }

                }, 50);
            });

        }

        var coordinatesMapGoogleRegex = /([3-4]+d[-0-9]{1,4}[,.]{1}[0-9]{2,9})/g;

        function tryParseGoogleMapUrl(url) {

            var result = [];

            if(coordinatesMapGoogleRegex.test(url)) {
                var matched = url.match(coordinatesMapGoogleRegex);

                if(matched.length >= 2) {
                    result = [ matched[matched.length-2].replace(/[3-4]d/g, ''), matched[matched.length-1].replace(/[3-4]d/g, '') ];
                }

            }

            return result;

        }

        function isValidCoordinate(coordinate) {

            /** stopnie, minuty, sekundy    ||  dziesiętne */

            return (/^-?[\d°]{1,4}[ ][\d']{1,3}[ ][\d,.']+$/.test(coordinate.trim()) || /^-?[\d]{1,3}[.,][\d]+$/.test(coordinate.trim()) );

        }

        function inputsTrackValues($inputs) {

            var values = {},
                amount = 0;

            $.each($inputs, function (i, ele) {

                var coordinate = ele.getAttribute('data-coordinate-value');
                if(coordinate !== "") {
                    var splitCoordinate = coordinate.split('|');
                    values[ele.getAttribute('data-track-type')] = new AtlasLoc(splitCoordinate[0], splitCoordinate[1]);
                    amount++;
                }

            });

            if(amount === 2) return values;

            return false;
        }

        function drawRouteOnMap(myMap, $searchByTrackInputs, defaultCoordinates) {

            var valuesCoordinate = inputsTrackValues($searchByTrackInputs);

            if(valuesCoordinate !== false) {

                myMap.drawRouteWay(valuesCoordinate.from, valuesCoordinate.to, function (resultRoute) {
                    if(resultRoute === null) {
                        toastr.warning('Nie można wyznaczyć drogi.');
                    }
                    else {

                        var text = "<p><strong>Trasa: </strong> " + resultRoute.getNameRoute() + ", <strong>Dystans: </strong> " + resultRoute.toString() + "</p>";
                        $('#result-route-way').html(text).fadeIn();

                    }
                });

            }
            else if(defaultCoordinates){
                myMap.centerMap(defaultCoordinates, myMap.getCurrentZoom());
            }

        }

        function refreshCoordinates(result) {
            var $googleButton = $('.google-redirect');

            var coordinatesString = '52.408506+16.901980';

            if(result) {
                if(Array.isArray(result)) {
                    coordinatesString = result[0] + "+" + result[1];
                }
                else {
                    coordinatesString = result.coordinates.lat + "+" + result.coordinates.lng;
                }
            }

            $googleButton.attr('data-coordinates', coordinatesString);
        }

        function googleRedirect() {
            var $googleButton = $('.google-redirect');

            $googleButton.on('click', function() {
                var buttonIsDisabled = $googleButton.attr('disabled') == "disabled";
                if ( !buttonIsDisabled ) {
                    var coordinates = $googleButton.attr('data-coordinates');
                    window.open("https://www.google.com/maps?z=12&t=m&q=loc:+"+coordinates+"+");
                }
            });

            refreshCoordinates();
        }

        function initWidgetMap($resultForm) {

            var pathLocation = _MapMethods.getPath('location'),
                isEcall = $resultForm.hasClass('ecall-form'),
                $mapContainer = $('#atlas-map-step'),
                $location = $('[data-attr-id="' + pathLocation + '"]'),
                attrLoc = null,
                $searchBar = $('#search-bar-control'),
                searchType = { type: null, lastValue: { address: '', coordinate: '', object: '', track: '' } },
                $searchCoordinateLat = $resultForm.find('.search-by-coordinate[data-coordinate-type="lat"]'),
                $searchCoordinateLng = $resultForm.find('.search-by-coordinate[data-coordinate-type="lng"]'),
                $searchByTrackInputs = $resultForm.find('.search-by-track');

            /** Pole mapy musi być minimalnie wyżej niż inne pola na formularzu */
            $mapContainer.closest('.grid-stack-item').css('z-index', '3');

            handleChooseTypeSearch($resultForm, searchType);

            googleRedirect();

            /** Wysyłanie SMS'a */

            $resultForm.find('.map-wrapper .send-sms-icon').confirmation({
                rootSelector: '.send-sms-icon',
                btnOkClass: 'btn btn-primary',
                btnOkIcon: '',
                btnOkLabel: 'Tak',
                btnCancelClass: 'btn btn-default',
                btnCancelIcon: '',
                btnCancelLabel: 'Nie'
            });

            $resultForm.find('.map-wrapper .send-sms-icon').on('click', function () {
                var $t = $(this),
                    msg = $t.attr('data-msg');

                var l = Ladda.create($t[0]);
                l.start();

                _socket.getAttribute({
                    'path': '80,342,408,197',
                    "groupProcessId": _dashboardService.getCurrentTicket('groupProcessId'),
                    "stepId": _dashboardService.getCurrentTicket('stepId'),
                    "valueType": 'string'
                }, function (result) {

                    var phoneNumber = result.value;

                    if(!phoneNumber) {
                        toastr.error("Numer klienta nie jest znany.", "Błąd przy wysyłaniu SMS'a.", {timeOut: 5000});
                        l.stop();
                    }
                    else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: Routing.generate('create_position_request', {groupProcessId: _dashboardService.getCurrentTicket('groupProcessId')}),
                            success: function (response) {

                                msg = msg.replace('__LINK__', Routing.generate('client_location_map', {instanceId: response.uniqueId}, true));

                                caseNoteModule.addNote('sms', msg, phoneNumber, function(){
                                    l.stop();
                                }, function(){
                                    toastr.success("", "Sms został pomyślnie wysłany.", {timeOut: 5000});
                                }, function () {
                                    toastr.error("", "Błąd przy wysyłaniu SMS'a.", {timeOut: 5000});
                                });

                            },
                            error: function (err) {
                                toastr.error("", "Błąd przy wysyłaniu SMS'a.", {timeOut: 5000});
                                l.stop();
                            }
                        });
                    }

                });

            });

            if ($location.length !== 0) {
                attrLoc = {
                    path: $location.attr('data-path'),
                    $el: $location
                };
            }
            else {
                attrLoc = {
                    path: ($mapContainer.attr('data-location-path')) ? $mapContainer.attr('data-location-path') : '',
                    $el: null
                };
            }

            attrLoc['paragraphInfo'] = ($('[info-map-address]') ? $('[info-map-address]') : null);
            attrLoc['$inputs'] = _MapMethods.searchInputs($resultForm, attrLoc.path);

            var myMap = null,
                laddaSearch = Ladda.create($resultForm.find('.input-search-icon')[0]),
                $resultInput = $resultForm.find('#map-widget-input');

            var coordinates = [];

            if (attrLoc) {
                coordinates = (attrLoc.$inputs.lat.val() && attrLoc.$inputs.lng.val()) ? [attrLoc.$inputs.lat.val(), attrLoc.$inputs.lng.val()] : [];
            }

            setCountriesOnMap($resultForm, attrLoc);

            var AtlasMapProperties = {
                opts: {
                    mapContainer: $mapContainer.selector,
                    centerCoordinates: coordinates, // Lat , Lng
                    zoomMap: 16
                },
                provider: new ProviderEmapa('6vihz1Nbw5QC5rQ5')
            };

            $mapContainer.on('refresh-map', function () {
                if(myMap) myMap.reDraw();
            });

            if ($(AtlasMapProperties.opts.mapContainer).length === 0) return false;

            AtlasMapProperties.opts.onPutMarker = function (e) {

                myMap.searchByCoordinates([e.lat, e.lng], function (result) {

                    if (result) {

                        if(isEcall) {
                            updateMapInputs(result, $resultForm);
                        } else {
                            if (attrLoc !== null) {
                                updateWidgetInputs(result, attrLoc);
                            }
                        }

                        myMap.addMarker('mainMarker', 'Zdarzenie drogowe', result.coordinates.toArray(), 'Content', '', 'car', {moveable: true});
                        myMap.centerMap(result.coordinates.toArray());
                    }

                });

            };

            AtlasMapProperties.opts.onDragEndMarker = function (marker) {

                myMap.searchByCoordinates([marker.lonLat.lat, marker.lonLat.lng], function (result) {

                    if (result) {

                        if(marker.name.indexOf('RouteMarker') !== -1) {

                            /** Jest to marker od lokalizacji trasy */

                            var dataTrackType = marker.name.replace('RouteMarker', '');
                            $searchByTrackInputs.filter('[data-track-type="'+dataTrackType+'"]').attr('data-coordinate-value', result.coordinates.toString());

                            drawRouteOnMap(myMap, $searchByTrackInputs);

                        }
                        else {

                            if(isEcall) {
                                updateMapInputs(result, $resultForm);
                            } else {
                                if (attrLoc !== null) {
                                    updateWidgetInputs(result, attrLoc);

                                    if(poiRegex.test(attrLoc.$inputs.desc.val())) {
                                        var newDesc = attrLoc.$inputs.desc.val().replace(poiRegex, "");
                                        attrLoc.$inputs.desc.val(newDesc).trigger('change');
                                    }

                                }
                            }

                            myMap.centerMap(result.coordinates.toArray(), myMap.getCurrentZoom());
                        }

                    }

                });

            };

            $resultForm.find('.search-by-coordinate').on('keypress', function (e) {

                if (e.keyCode === 13) {
                    e.preventDefault();

                    var validCoordinate = true;

                    if(!isValidCoordinate($searchCoordinateLat.val())){
                        validCoordinate = $searchCoordinateLat;
                    }
                    else if(!isValidCoordinate($searchCoordinateLng.val())){
                        validCoordinate = $searchCoordinateLng
                    }

                    if(validCoordinate === true) {

                        myMap.searchByCoordinates([$searchCoordinateLat.val(), $searchCoordinateLng.val()], function (result) {

                            if (result) {

                                if(isEcall) {
                                    updateMapInputs(result, $resultForm);
                                } else {
                                    if (attrLoc !== null) {
                                        updateWidgetInputs(result, attrLoc);
                                    }
                                }

                                $resultInput.val("");
                                searchType.lastValue["address"] = "";

                                myMap.addMarker('mainMarker', 'Zdarzenie drogowe', result.coordinates.toArray(), 'Content', '', 'car', {moveable: true});
                                myMap.centerMap(result.coordinates.toArray());

                            }
                            else {
                                console.log('Brak rezultatów');
                            }

                        });

                    }
                    else {

                        validCoordinate.focus();

                        if(validCoordinate.val().length) {
                            validCoordinate[0].selectionStart = 0;
                            validCoordinate[0].selectionend = validCoordinate.val().length;
                        }

                        toastr.warning('Podane współrzędne są niepoprawne: ' + validCoordinate.val(), 'Niepoprawne współrzędne');
                    }
                }
            });

            $searchByTrackInputs.on('keypress', function (e) {

                var $t = $(this);

                if(e.keyCode === 13) {
                    // delayFun(function () {

                    e.preventDefault();

                    if ($t.val().length >= 3 && myMap) {

                        laddaSearch.start();

                        $t.autocomplete("option", {source: [] });

                        myMap.searchByAddress($t.val(), function (result) {

                            laddaSearch.stop();

                            if (result) {

                                if (!Array.isArray(result)) result = [result];

                                if ($t.val() && result.length !== 0) {
                                    $t.autocomplete("option", {source: result});
                                    $t.autocomplete("search");
                                    $t.autocomplete("widget").show();
                                }

                            }
                            else {
                                console.log("Brak wyników");
                            }
                        });

                    }
                }
                // }, 300)

            }).on('keyup', function (e) {
                if (e.keyCode === 8)
                    $(this).trigger('keypress');
            }).autocomplete({
                minLength: 3,
                appendTo: "#autocomplete-search-wrapper",
                source: [],
                autoFocus: true,
                focus: function () {
                    return false;
                },
                select: function (event, ui) {

                    var result = ui.item;
                    this.value = ui.item.string;
                    var self = this;

                    laddaSearch.start();

                    myMap.geocodeFullAddress(result, function(result2) {

                        self.setAttribute('data-coordinate-value', result2.coordinates.toString());
                        myMap.addMarker( self.getAttribute('data-track-type') + 'RouteMarker', self.getAttribute('placeholder'), result2.coordinates.toArray(), 'Content', '', 'point', {moveable: true});
                        drawRouteOnMap(myMap, $searchByTrackInputs, result2.coordinates.toArray());

                        laddaSearch.stop();

                    });

                    return false;
                }
            });

            $resultInput.on('keypress', function (e) {

                var $t = $(this);

                $resultInput.autocomplete("close");
                $resultInput.autocomplete("option", {source: [] });

                if(e.keyCode === 13) {
                    // delayFun(function () {
                    e.preventDefault();
                    e.stopPropagation();

                    if ($t.val().length >= 2) {

                        laddaSearch.start();

                        $searchBar.removeClass('no-result');
                        $resultInput.autocomplete("option", {source: []});

                        searchType.lastValue[searchType.type] = $t.val();

                        if (searchType.type === "object") {

                            $.ajax({
                                url: Routing.generate('map_get_object', {name: $t.val()}),
                                type: "GET",
                                success: function (data) {

                                    if (data.objects.length !== 0) {

                                        if ($resultInput.val()) {
                                            $resultInput.autocomplete("option", {source: data.objects});
                                            // $resultInput.autocomplete("search", $resultInput.val());
                                            $resultInput.autocomplete("search");
                                            $resultInput.autocomplete("widget").show();
                                        }

                                    }
                                    else {
                                        $searchBar.addClass('no-result');
                                    }

                                },
                                complete: function () {
                                    laddaSearch.stop();
                                }
                            });

                        }
                        else {
                            myMap.searchByAddress($t.val(), function (result) {

                                laddaSearch.stop();

                                if (result) {

                                    if (!Array.isArray(result)) result = [result];

                                    if ($resultInput.val() && result.length !== 0) {
                                        $resultInput.autocomplete("option", {source: result});
                                        $resultInput.autocomplete("search");
                                        $resultInput.autocomplete("widget").show();
                                    }
                                }
                                else {
                                    $searchBar.addClass('no-result');
                                }
                            });
                        }
                    }
                }
                // }, 400)

            }).on('keyup', function (e) {
                e.preventDefault();
                if (e.keyCode === 8) {
                    $(this).trigger('keypress');
                    e.stopPropagation();
                }
            });

            $resultInput.autocomplete({
                minLength: 2,
                appendTo: "#autocomplete-search-wrapper",
                source: [],
                autoFocus: true,
                focus: function () {
                    return false;
                },
                select: function (event, ui) {

                    var result = ui.item;

                    if(searchType.type === "address") {

                        laddaSearch.start();

                        myMap.geocodeFullAddress(result, function(result2) {

                            $resultInput.val(ui.item.string);

                            if(result2 && result2.coordinates) {
                                myMap.addMarker('mainMarker', 'Zdarzenie drogowe', result2.coordinates.toArray(), 'Content', '', 'car', {moveable: true});
                                myMap.centerMap(result2.coordinates.toArray(), myMap.getCurrentZoom());
                            }

                            if(isEcall) {
                                updateMapInputs(result2, $resultForm);
                            } else {
                                if (attrLoc !== null) {
                                    updateWidgetInputs(result2, attrLoc);
                                }
                            }

                            laddaSearch.stop();

                        });

                    }
                    else if(searchType.type === "object") {

                        $resultInput.val(result.label);

                        myMap.searchByCoordinates(result.coordinates.split('|'), function (result2) {

                            if (result2) {

                                if(isEcall) {
                                    updateMapInputs(result2, $resultForm, result.description);
                                } else {
                                    if (attrLoc !== null) {
                                        updateWidgetInputs(result2, attrLoc, result.description);
                                    }
                                }

                            }
                            else {
                                console.log('Brak rezultatów');
                            }

                        });

                        myMap.addMarker('mainMarker', 'Zdarzenie drogowe', result.coordinates.split('|'), 'Content', '', 'car', {moveable: true});
                        myMap.centerMap(result.coordinates.split('|'), myMap.getCurrentZoom());

                    }

                    return false;
                }
            });

            var updateMapPosition = function (lat, lng) {

                myMap.searchByCoordinates([lat, lng], function (result) {

                    if (result) {

                        if(isEcall) {
                            updateMapInputs(result, $resultForm);
                        } else {
                            if (attrLoc !== null) {
                                updateWidgetInputs(result, attrLoc);
                            }
                        }

                        myMap.addMarker('mainMarker', 'Zdarzenie drogowe', result.coordinates.toArray(), 'Content', '', 'car', {moveable: true});
                        myMap.centerMap(result.coordinates.toArray());
                    }

                });

            };

            AtlasMapModule.init(AtlasMapProperties, function (map) {

                if (!map) {
                    console.error('Błąd przy załadowaniu mapy.');
                    return false;
                }

                myMap = map;

                _MAP = {
                    updatePositionOnMap: updateMapPosition
                };

                $('#center-main-marker').on('click', function (e) {

                    var foundMarker = myMap.data.markers.find(function (marker, index) {
                        return (marker.name === "mainMarker");
                    });

                    if(typeof foundMarker !== "undefined") {
                        myMap.centerMap([foundMarker.lonLat.lat, foundMarker.lonLat.lng], myMap.getCurrentZoom());
                    }
                });

                /** Dodawanie do mapy */
                if (typeof positionTraces === "object") {
                    $.each(positionTraces, function (i, ele) {
                        var pos = positionTraces.length - i;
                        myMap.addMarker('position_car_' + (pos), 'Wcześniejsza lokalizacja (' + pos + ')', [ele.LATITUDE, ele.LONGITUDE], 'Content', '', 'point');
                        if (i >= 10) {
                            return false;
                        }
                    });
                }

                if(isEcall) {
                    var streetNumber = $('#streetNumber').val(),
                        latitudeValue = $resultForm.find('input#lat').val(),
                        longitudeValue = $resultForm.find('input#lng').val();

                    if (latitudeValue && longitudeValue) {
                        if (streetNumber) {
                            myMap.addMarker('mainMarker', 'Zdarzenie drogowe', [latitudeValue, longitudeValue], 'Content', '', 'car', {moveable: true});
                            myMap.centerMap([latitudeValue, longitudeValue]);
                        }
                        else {
                            myMap.searchByCoordinates([latitudeValue, longitudeValue], function (result) {
                                updateMapInputs(result, $resultForm);
                            });
                        }

                    }
                    else {
                        myMap.centerMap([52.4070216, 16.9174668]);
                    }

                }
                else {
                    if (attrLoc !== null) {
                        var latitudeValue = attrLoc.$inputs.lat.val(),
                            longitudeValue = attrLoc.$inputs.lng.val();

                        // locateEmergencyUnit();

                        var dfd = jQuery.Deferred();

                        if (latitudeValue && longitudeValue) {

                            if (attrLoc.$inputs.city.val()) {
                                myMap.addMarker('mainMarker', 'Zdarzenie drogowe', [latitudeValue, longitudeValue], 'Content', '', 'car', {moveable: true});
                                myMap.centerMap([latitudeValue, longitudeValue]);
                                refreshCoordinates([latitudeValue, longitudeValue]);
                                updateInfoMapAddress(null, attrLoc);
                            }
                            else {
                                myMap.searchByCoordinates([latitudeValue, longitudeValue], function (result) {
                                    updateWidgetInputs(result, attrLoc);
                                    dfd.resolve(result);
                                });
                            }

                        }
                        else {
                            myMap.centerMap(myMap._opts.centerCoordinates);
                        }

                        $.when(dfd.promise()).then(function () {

                            myMap.addMarker('mainMarker', 'Zdarzenie drogowe', [latitudeValue, longitudeValue], 'Content', '', 'car', {moveable: true});
                            myMap.centerMap([latitudeValue, longitudeValue]);

                        });

                    }

                }

            });

        }

        /** Tworzenie polwa wyboru z państwami & zaznaczenie takiego jaki jest wyszukany  */
        function setCountriesOnMap($content, attrLoc) {

            var countryName = null,
                countryValue = null;

            if(attrLoc.$inputs && attrLoc.$inputs.country) {
                countryName = attrLoc.$inputs.country.val();
            }

            if(countryName && $('#map-widget-countries').length) {
                $.each($('#map-widget-countries')[0].options, function(i, ele) {
                    if(ele.text === countryName) {
                        countryValue = ele.value;
                    }
                });
            }

            var _$form = $content.find('#map-widget-countries');
            _$form.select2();

            if(countryValue) {
                _$form.val(countryValue).trigger('change');
            }

        }

        function updateWidgetInputs(result, attrLoc, description) {

            if(!result) return false;

            if(typeof description !== "undefined") {
                result.street = '';
                result.streetNumber = '';
            }

            $.each(result.getIterator(), function (i, ele) {

                if (attrLoc.$inputs.hasOwnProperty(ele)) {
                    attrLoc.$inputs[ele].val(result[ele]).trigger('change');
                }

            });

            if(result.streetNumber.indexOf('/') !== -1) {
                var streetNumberAndObject = result.streetNumber.split('/');
                if(streetNumberAndObject.length === 2) {
                    if(attrLoc.$inputs.objectNumber) {
                        attrLoc.$inputs.objectNumber.val(streetNumberAndObject[1]);
                    }
                }
            }

            if(typeof description !== "undefined") {
                if(poiRegex.test(attrLoc.$inputs.desc.val())) {
                    var newDesc = attrLoc.$inputs.desc.val().replace(poiRegex, description);
                    attrLoc.$inputs.desc.val(newDesc).trigger('change');
                }
                else {
                    attrLoc.$inputs.desc.val(attrLoc.$inputs.desc.val() + "\r\n" + description).trigger('change');
                }
            }
            else {
                if(poiRegex.test(attrLoc.$inputs.desc.val()) && !(result.road)) {
                    var newDesc = attrLoc.$inputs.desc.val().replace(poiRegex, "");
                    attrLoc.$inputs.desc.val(newDesc).trigger('change');
                }
            }

            attrLoc.$inputs.lat.val(result.coordinates.lat).trigger('change');
            attrLoc.$inputs.lng.val(result.coordinates.lng).trigger('change');

            refreshCoordinates(result);
            updateInfoMapAddress(result, attrLoc);
        }

        function updateInfoMapAddress(result, attrLoc) {

            if(attrLoc.paragraphInfo) {
                if(result) {
                    attrLoc.paragraphInfo.html(result.paragraphInfo);
                }
                else {
                    attrLoc.paragraphInfo.html(InfoLocationMethods.paragraphInfo(attrLoc.$inputs));
                }
            }

        }
        
        function updateMapInputs(result, $resultForm, description) {

            var oldValue;

            $.each(result, function (key, value) {
                var matchedInput = $resultForm.find('#' + key + '');

                oldValue = matchedInput.val();
                matchedInput.val(value);

                /** Jak różne to triggeruje zmiane (żeby zapisać do bazy) */
                if (oldValue != value) {
                    matchedInput.trigger('change');
                }
            });

            if(typeof description !== "undefined") {
                $('#comment').val(description).trigger('change');
            }

            $resultForm.find('input#lat').val(result.coordinates.lat).trigger('change');
            $resultForm.find('input#lng').val(result.coordinates.lng).trigger('change');

            refreshCoordinates(result);
        }

        function customCheckBoxHandle($resultForm) {

            var checkboxInput = $resultForm.find('input[type=checkbox]').not('.is-handle');

            checkboxInput.each(function () {
                var $t = $(this);

                if (!($t.attr('readonly') === 'readonly')) {
                    $t.addClass('is-handle').parent().find('span, label').on('click', function () {
                        $(this).parent().find('input').click();
                    });
                }

                if ($t.val() == "1") {
                    $t.attr('checked', 'checked');
                }
                else if ($t.val() == "") {
                    if ($(this).is('[data-attribute-path]') && $(this).closest('.toggler-wrapper').length > 0) {
                        _setAttributeValue($(this).attr('data-attribute-path'), 1);
                    }
                }

            });

        }

        function showDependentInput($resultForm) {

            var checkbox = $resultForm.find('.custom-checkbox-wrapper input[type=checkbox]'),
                checkIfDisabled = $resultForm.find('.additional-input-field input').attr('readonly') === "readonly";

            $resultForm.find('.form-group.checkboxes').each(function () {
                if (checkbox.is(':checked') && checkbox.val() != "") {
                    $('.custom-checkbox-wrapper').addClass('active');
                }
            });

            if (!checkIfDisabled) {
                $('.custom-checkbox-wrapper span, .custom-checkbox-wrapper label ').on('click', function () {
                    $(this).parent().toggleClass('active');
                });
                $('.additional-input-field .btn:first-of-type').on('click', function () {
                    $(this).parent().find('input').val(parseInt($('.additional-input-field input').val(), 10) + 1);
                });
                $('.additional-input-field .btn:last-of-type').on('click', function () {
                    $(this).parent().find('input').val(parseInt($('.additional-input-field input').val(), 10) - 1);
                });
            }
        }

        function refreshNotes(notes) {

            var $notificationModule = $('.notification-module');

            if($('body').hasClass('public-mode')){
                return;
            }

            if(!$notificationModule.length) {
                return;
            }
           $efficiencyRanking.addClass('hidden');
            if(!notes.root) return;
            $notificationModule.removeClass('hidden');
            $notificationModule.attr('data-notes-root-id', notes.root);
            inboxEditorModule.hideInboxEditor();

            $caseNote.find('.case-note').remove();

            $.each(notes.list, function (key, note) {
                if (note.content) {
                    caseNoteModule.appendNote(note, 'prepend', key);
                }
            });

            if($('.accordion-expand-all').hasClass('active')) {

                $('.notification-container').find('.title').addClass('absolute-header').attr({
                    "aria-selected": "true",
                    "aria-expanded": "true"
                }).next().attr("aria-hidden", "false").show().next().next().show();
            }

            $('#customAccordion .case-note').each(function () {
                var text = $(this).attr('data-id').toString().replace('_', '');
                $(this).attr('data-id', text);
                $(this).find('.edit-email-note').attr('data-note-id', text);
            });
        }

        function anyFilterExist() {
            var result = false;
            $('.searcher-filters').find('input[type=text], select').each(function(){

                if($(this).val() && $(this).val().length >0){
                    result =  true;
                }
            });

            $('.searcher-filters').find('input[type=checkbox]').each(function(){
               if($(this).is(":checked")){
                   result = true;
               }
            });

            return result;

        }

        function searchCaseInput() {

            var $ticketGroup = $('#ticket-groups');

            $caseSearchButton.on('click', function(e) {
                e.preventDefault();
                $caseSearchInput.trigger('keyup');
            });

            $caseSearchButtonFilter.on('click', function (e) {
                e.preventDefault();
                $('.searcher-filters').toggle();
            });

            setCaseSearcherFilters();

            $('.searcher-filters').find('input, select').on('change', function () {

                var $t = $(this),
                    name = $t.attr('name'),
                    value = null,
                    filters = {},
                    searcherFiltersString = localStorage.getItem('case-searcher-filters');

                if(searcherFiltersString) {
                    filters = JSON.parse(searcherFiltersString);
                }


                var customType = false;
                if($t[0].type === "checkbox") {
                    value = $t.is(':checked');
                }else if ($($t[0]).hasClass('date-time-picker')){
                    customType =  'date-time-picker'
                    value = $t.val();
                }
                else {
                    value = $t.val();
                }

                filters[name] = {
                    v: value,
                    t: (customType !== false)? customType: $t[0].type
                };

                localStorage.setItem('case-searcher-filters', JSON.stringify(filters));



                $caseSearchInput.trigger('keyup');

            });
            $('.searcher-filters').find('[name="search-filters[dateFrom]"], [name="search-filters[dateTo]"]').on('dp.change',function (ev) {
                $(ev.target).trigger('change');
                $caseSearchInput.trigger('keyup');
            });

            $caseSearchInput.on('keyup', function(){
                if(anyFilterExist()){
                    $('#toggle-searcher-filters').addClass('has-filters');
                }else{
                    $('#toggle-searcher-filters').removeClass('has-filters');
                }

                delayedCallback(function() {

                    var searchValue = $caseSearchInput.val();

                    if(searchValue) {
                        $ticketGroup.addClass('search-mode');
                        ticketListManager.setSearchMode(true);
                    }


                    refreshTasks({
                        filter: searchValue,
                        currentTask: _dashboardService.getCurrentTicket('id')
                    }, function () {

                        if(!searchValue) {
                            $ticketGroup.removeClass('search-mode');
                            ticketListManager.setSearchMode(false);
                        }

                    });

                }, 800);
            });
        }

        function getSearcherAdvancedFilters() {

            var $searcherFiltersForm = $('#searcher-filters-form');

            return {
                postponeEnabled: $searcherFiltersForm.find('[name="search-filters[postpone-enabled]"]').is(":checked"),
                vipEnabled: $searcherFiltersForm.find('[name="search-filters[vip-enabled]"]').is(":checked"),
                dateFrom: $searcherFiltersForm.find('[name="search-filters[dateFrom]"]').val(),
                dateTo: $searcherFiltersForm.find('[name="search-filters[dateTo]"]').val(),
                programs: $searcherFiltersForm.find('[name="search-filters[platform-id]"]').val(),
                attributeFilter: $searcherFiltersForm.find('[name="search-filters[attribute]"]').val(),
                mailEnabled: $searcherFiltersForm.find('[name="search-filters[mail-enabled]"]').is(":checked"),
                inactiveEnabled: $searcherFiltersForm.find('[name="search-filters[inactive-enabled]"]').is(":checked"),
                category: $searcherFiltersForm.find('[name="search-filters[category]"]').val()
            }
        }

        function setCaseSearcherFilters() {

            var filters = {},
                $searcherFilters = $('.searcher-filters'),
                searcherFiltersString = localStorage.getItem('case-searcher-filters');

            if(searcherFiltersString) {
                filters = JSON.parse(searcherFiltersString);
            }

            $.each(filters, function (key, ele) {

                if(ele.t === "checkbox") {
                    $searcherFilters.find('[name="' + key + '"]').prop('checked', ele.v);
                }
                else if (ele.t === 'date-time-picker'){
                    var dateFilterDate = new Date(ele.v);
                    if(ele.v != null && !isNaN( dateFilterDate.getTime())){
                        $searcherFilters.find('[name="' + key + '"]').data('datepicker').selectDate(new Date(ele.v))
                        }else{
                        $searcherFilters.find('[name="' + key + '"]').data('datepicker').clear();
                    }
                }
                else{

                    $searcherFilters.find('[name="' + key + '"]').val(ele.v).trigger('change.select2');;
                }

            });
            if(anyFilterExist()){
                $('#toggle-searcher-filters').addClass('has-filters');
            }else{
                $('#toggle-searcher-filters').removeClass('has-filters');
            }

        }

        function resizeFormPlacmenet() {

            setHeightOfTicketGroups();

        }

        function setHeightOfTicketGroups() {

            setTimeout(function () {

                var portletHeight = $('#page-main-content').height(),
                    header = ($('#case-panel-management-list > .header').length) ? $('#case-panel-management-list > .header').outerHeight(true) : 0,
                    modeSwitchContainer = ($('.mode-switch-container').length) ? $('.mode-switch-container').outerHeight(true) : 0,
                    searchCaseWrapper = $('.case-search-wrapper').outerHeight(),
                    newHeight = portletHeight - searchCaseWrapper - header - modeSwitchContainer;

                $('#ticket-groups, #history-ticket-groups').css('max-height', newHeight + 'px');

            }, 10)

        }


        function checkEcallStatusesVisibility() {
            if ($('#EventQualificationSelector').val() == 'Error') {
                $('#EventCauseSelector').parent('.wrapped-select').addClass('hidden');
                $('#EventCauseSelector').val('-');
                $('#redundant-id').parent('.wrapped-select').addClass('hidden');
            }
            else if ($('#EventQualificationSelector').val() == 'Redundant') {
                $('#redundant-id').parent('.wrapped-select').removeClass('hidden');
                $('#EventCauseSelector').parent('.wrapped-select').removeClass('hidden');
            }
            else {
                $('#redundant-id').parent('.wrapped-select').addClass('hidden');
                $('#EventCauseSelector').parent('.wrapped-select').removeClass('hidden');
            }
        }

        function resetFormView() {
            $formPlacement.find('form').remove();
            serviceSummaryTooltipModule.hideAllPopup();
            $notificationModuleWrapper.hide();
            $notificationModuleWrapper.parent().removeClass('active');
            hideCaseInfo();
        }

        function hideCaseInfo() {
            $caseInfo.html('').hide();
            $addCaseInfo.html('').hide();
        }

        var servicePanelBtn = '<p class="text-center"><button data-service-panel-id="__ID__" class="btn btn-default back-to-service-panel"><i class="fa fa-undo font-blue-dark"></i> Powrót do panelu świadczeń</button></p>';

        var notSaveNoteTemplate = '<p class="text-center font-md font-red" data-note-id="__NOTE_ID__">' +
            '<b>Uwaga!</b> Zarejestrowano, że w tej sprawie nie zapisano notatki z ostatniej rozmowy - <b>__PHONE_NUMBER__</b>. ' +
            '<br>Należy wrócić do sprawy <b>__CASE_NUMBER__</b> i uzupełnić notatkę.' +
            '</p>';

        function finishForm(header, message, icon, error, fadeOut) {

            icon = (typeof icon === "undefined" || !icon) ? 'fa-exclamation' : icon;
            fadeOut = (typeof fadeOut === "undefined") ? true : fadeOut;
            error = (typeof error === "undefined") ? 0 : error;

            var $popup = $('.finish-popup'),
                headerText = $('.finish-popup .header .title'),
                popupText = $('.finish-popup .text'),
                headerIcon = '<i class="fa ' + icon + ' fa-lg" aria-hidden="true"></i>';

            if (error == 1) {
                $popup.addClass('error');
            }
            else {
                $popup.removeClass('error');
            }

            /** Jeżeli istnieje przycisk do panelu świadczeń i nie jest to tryb publiczny, to dodajemy przycisk powrotu do panelu świadczeń */
            if ( $('.top-service-panel.active').length){
                var servicePanelProcessInstanceId = $('.btn-service-panel').attr('data-process-instance-id');
            }

            if(!_dashboardService.isPublicDashboard() && servicePanelProcessInstanceId) {
                message = message + (servicePanelBtn.replace('__ID__', (servicePanelProcessInstanceId )));
            }
            else {

                /** modal się chowa po 4s */
                if(fadeOut) {
                    setTimeout(function () {
                        $popup.fadeOut();
                    }, 4000);
                }
            }

            var lastPhoneEditableNote = caseNoteModule.checkEditableNotes();

            if(lastPhoneEditableNote !== null && window.currentProcess) {
                message = message + (
                    notSaveNoteTemplate.replace('__NOTE_ID__', lastPhoneEditableNote.noteId)
                        .replace('__CASE_NUMBER__', getCaseNumberFromString(window.currentProcess.rootId))
                        .replace('__PHONE_NUMBER__', lastPhoneEditableNote.value)
                )
            }

            $popup.find('.icon').html(headerIcon);
            headerText.text(header);
            popupText.html(message);
            $popup.addClass('fadeInDown').show();

            $popup.height($('.finish-popup .header').height() + popupText.outerHeight());

            if(_dashboardService.isPublicDashboard()) {
                $popup.find('.close-finish-popup').hide(0);
            }
            else {
                $popup.find('.close-finish-popup').show(0);
                $('.close-finish-popup, .finish-popup.error .text a').on('click', function () {
                    $popup.fadeOut();
                });
            }

            $('.form-placement form').remove();

            if(!_dashboardService.isPublicDashboard()) {
                resizeFormPlacmenet();
                $('#customAccordion').hide();
                $('.notification-module').hide();
                $('.basic-info-widget').hide().removeClass('visible');
                $('.types-box').hide().removeClass('visible');
                emailPreviewModule.closeInbox();
            }

            window.currentProcessId = null;
            window.currentProcess = null;

            sendToParentWindow({
                type: 'onGetTask'
            });

        }

        function cancelStep(description) {

            $.ajax({
                url: Routing.generate('ajax_case_trash_check',{'processInstanceId' : _dashboardService.getCurrentTicket('id')}),
                type: "GET",
                success: function (result) {
                    var $cancelModal = $('#cancel-process-modal');

                    $('#action-kill-current').removeAttr('disabled').attr('title','');
                    $('#action-kill-all').removeAttr('disabled').attr('title','');

                    if(result.step_status == 0){
                        $('#action-kill-current').prop('disabled',true).attr('title',result.step_message);
                    }

                    if(result.case_status == 0){
                        $('#action-kill-all').prop('disabled',true).attr('title',result.case_message);
                    }

                    $('#cancel-process-modal #action-kill-current').unbind('click').on('click', function () {
                        onNextStep(92, true, true);
                        $cancelModal.modal('hide');
                        $('.types-box').hide().removeClass('visible');
                    });

                    $('#cancel-process-modal #action-kill-all').unbind('click').on('click', function () {
                        onNextStep(98, true, true);
                        $cancelModal.modal('hide');
                        $('.types-box').hide().removeClass('visible');

                    });

                    $cancelModal.modal('show');
                }
            });



        }

        function forwardStep(variant,description) {
            var $modal = $('#step-forward-modal');
            $modal.find('.modal-body > span').text(description);
            $modal.modal('show');

            $('#step-forward-modal .btn.confirm').unbind('click').on('click', function () {
                onNextStep(variant, true);
                $modal.modal('hide');
            });

        }

        function reopenCase() {
            var $modal = $('#reopen-case-modal');
            $modal.modal('show');

            $modal.find('.btn.confirm').unbind('click').on('click', function () {
                $.ajax({
                    url: Routing.generate('ajax_reopen_case',{'rootId' : _dashboardService.getCurrentTicket('rootId')}),
                    type: "POST",
                    success: function (result) {
                        $modal.modal('hide');
                        toastr.info("", result.message, {timeOut: 3000});
                        tryGetTask({
                            id: _dashboardService.getCurrentTicket('id'),
                            notificationMode: true
                        });
                    }
                });

                return false;
            });

        }

        function abandonStep() {

            $('#abandon-process-modal').modal('show');

        }

        // function postponeTask() {
        //     $('#postpone-process-modal').modal('show');
        //     $('#postpone-time').val('');
        //     $('#postpone-process-modal .add-time').unbind('click').on('click', function(){
        //         var d = new moment();
        //         var newTime = d.add(parseInt($(this).attr('data-add-minutes')), 'minutes');
        //         $('#postpone-time').val(newTime.format('YYYY-MM-DD HH:mm'));
        //         // $('#postpone-confirm').trigger('click');
        //     });
        //
        //     $('#postpone-process-modal #postpone-confirm').unbind('click').on('click', function () {
        //
        //         var time = $('#postpone-time').val();
        //         $('#postpone-process-modal').modal('hide');
        //
        //         $.ajax({
        //             url: Routing.generate('ajax_postpone_task'),
        //             data: {'id': _dashboardService.getCurrentTicket('id'), 'time': time},
        //             type: "POST",
        //             success: function (result) {
        //                 $('.notification-module').hide();
        //                 $('#case-info').html('').hide();
        //                 $('.form-placement form').remove();
        //                 resizeFormPlacmenet();
        //                 ticketListManager.finishTask();
        //                 ticketListManager.refresh();
        //
        //                 _dashboardService.setCurrentTicket(null);
        //
        //                 if(result.error == 0){
        //                     toastr.info("", result.message, {timeOut: 3000});
        //                 }
        //                 else{
        //                     toastr.error("", result.message, {timeOut: 3000});
        //                 }
        //             }
        //         });
        //         return false;
        //     });
        // }

        function handleWindowHeightChange() {

            funResizeHandle.addFunction('resizeTimeLineHeight', timeLineManager.resizeTimeLineHeight);
            funResizeHandle.addFunction('refreshTicketListManager', ticketListManager.refresh);

        }

        function displayRefreshInfo(time) {
            setTimeout(function () {

                var navBarHeight = $('.page-header.navbar').height();
                var chatBarHeight = $('#cometchat').height();
                var notificationTitle = $('.notification-container .title');

                $('.right-side .statBox').addClass('shrinked');
                $notificationsContainer.height($window.height() - 110 - (navBarHeight + chatBarHeight));
                notificationTitle.slice(0, 4).attr('aria-selected', 'true').next().show();
            }, 100);
        }

        function reportUserAtInstance() {
            var currentInstanceId = _dashboardService.getCurrentTicket('id');
            var userId = _user ? _user.id : null;
            if (currentInstanceId && userId) {
                $.ajax({
                    url: Routing.generate('case_report_user_at_instance'),
                    type: "POST",
                    data: {
                        'userId': userId,
                        'instanceId': currentInstanceId
                    },
                    success: function () {
                        var dt = new Date();
                        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
                    }
                });
            }
        }



        function loadProgramFileInfo(){
            var $t = $(this),
                message = {
                    header: 'Karty programów',
                    body: ''
                };

            var dfd = jQuery.Deferred();

            var $modal = _f.openModal(message, null, null, true, dfd);

            $.ajax({
                url: Routing.generate('operational_dashboard_get_program_cars_modal', {
                    groupProcessInstanceId:  _dashboardService.getCurrentTicket('groupProcessId'),
                    rootId:  _dashboardService.getCurrentTicket('rootId')
                }),
                type: "GET",
                success: function (result) {

                    dfd.resolve(result.html);

                    $('body').on('click','#download-case-program',function(ev){
                    ev.preventDefault();
                      var link = $('#case-program-file-download').val();
                        window.open(link,'_blank')
                    });
                }
            });
        }

        function programCardsInfoModal() {
           var cases = $('.open-program-card-modal');
            cases.css('cursor','pointer');
            cases.css('color','#4a9166');
            cases.off('click', loadProgramFileInfo).on('click', loadProgramFileInfo);
        }


        function loadVinHeaderModal(){
            var $t = $(this),
                message = {
                    header: 'Informacje z bazy',
                    body: ''
                };

            var dfd = jQuery.Deferred();

            var $modal = _f.openModal(message, null, null, true, dfd);

            $.ajax({
                url: Routing.generate('operation_dashboard_get_case_info_from_vin_db', {
                    rootId: _dashboardService.getCurrentTicket('rootId')
                }),
                type: "GET",
                success: function (result) {

                    dfd.resolve(result.html);

                }
            });
        }


        function vinHeadersInfoModal(){

            var regex = new RegExp("Platforma\\/Program.*$"); // expression here

            var cases = $("#case-info p").filter(function () {

                return regex.test($(this).text())
            });


                for (var singleIter = 0; singleIter < cases.length;singleIter+=1)
                {
                    var obj = $(cases[singleIter]);
                    var arrSplitted = obj.html().split(':');
                    var arrJoined  =  arrSplitted[0] + ':<i class="fa fa-info-circle"></i>' + arrSplitted[1];
                    obj.html(arrJoined);
                }


            cases.css('cursor','pointer');
            cases.css('color','#3c7091');
            cases.off('click', loadVinHeaderModal).on('click', loadVinHeaderModal);
        }





        function handleATPTestCases() {

            var $testConsole = $('#atp-trigger-buttons');

            if(!$testConsole.length) return false;

            $testConsole.find('.toggle-panel').on('click', function (e) {
                e.preventDefault();
                $(this).parent().toggleClass('open');
            });

            runScenarioPanel($testConsole.find('.scenarios-panel'));

            $('#atp-create, #atp-update').on('click', function () {
                var type = $(this).attr('id').replace('atp-', '');
                laddaNotification = Ladda.create($(this)[0]);
                laddaNotification.start();
                $.ajax({
                    url: Routing.generate('ajax_atp_test_case', {eventType: type}),
                    type: "POST",
                    success: function () {
                        laddaNotification.stop();
                    }
                });
                return false;
            });

            $('#call-ring-start').on('click', function () {

                var $t = $(this),
                    phoneNumber = $('#atp-trigger-buttons #test-phone-number').val(),
                    platformId = $('#atp-trigger-buttons #platform-id').val(),
                    laddaBtn = Ladda.create($t[0]);

                laddaBtn.start();

                document.simulationIncomingCall(phoneNumber, 721, _userId, platformId).always(function(jqXHR) {
                    laddaBtn.stop();
                    $testConsole.find('.toggle-panel').trigger('click');
                });

                return false;

            });

            $('#create-mobile-case').on('click', function(e) {

                e.preventDefault();

                var $t = $(this),
                    phoneNumber = $('#atp-trigger-buttons #test-phone-number').val(),
                    platformId = $('#atp-trigger-buttons #platform-id').val(),
                    laddaBtn = Ladda.create($t[0]);

                if(!phoneNumber || phoneNumber.length < 9) {
                    toastr.error('Numer telefonu jest pusty lub niepoprawny!');
                    return false;
                }

                laddaBtn.start();

                return $.ajax({
                    url: Routing.generate('operationalDashboardInterfaceCreateMobileCase'),
                    type: "POST",
                    data: {
                        phoneNumber: phoneNumber,
                        platformId: platformId
                    },
                    success: function (result) {

                        if(result.success) {

                            toastr.success('Sprawa mobilna została utworzona. Dostaniesz SMS na numer: ' + phoneNumber );

                            if(result.url) {
                                var win = window.open(result.url, '_blank');
                                if(win) {
                                    win.focus();
                                }
                            }

                        }
                        else {
                            toastr.error('Coś poszło nie tak.');
                        }

                    },
                    error: function (error) {
                        handleAjaxResponseError(error);
                    },
                    complete: function () {
                        laddaBtn.stop();
                    }
                });

            });

            $('#follow-up').on('click', function () {
                document.simulationFollowUp(_overridePhoneNumber, _userId);
                return false;
            });
        }

        function runScenarioPanel($content) {

            if($content.length) {
                var $btn = $content.find('#run-scenarios'),
                    laddaBtn = Ladda.create($btn[0]),
                    $form = $content.find('form');

                $form.on('submit', function (e) {
                    e.preventDefault();

                    var $t = $(this);
                    laddaBtn.start();

                    isScenarioRun = true;

                    $.ajax({
                        url: Routing.generate('scenariosRun'),
                        type: "POST",
                        data: $t.serialize(),
                        success: function (data) {

                            if(data.response.indexOf('[ERROR]') !== -1 || data.response.indexOf('Fail!') !== -1) {
                                toastr.error(data.response, 'Scenario', {timeOut: 5000});
                            }
                            else {
                                toastr.success(data.response, 'Scenario', {timeOut: 5000});
                            }

                        },
                        error: function (e) {
                            handleAjaxResponseError(e);
                        },
                        complete: function () {
                            laddaBtn.stop();
                        }
                    });
                });

            }

        }

        function _copyAttributeStructure(sourceId, targetId, refresh, callback) {
            if(_dashboardService.isCurrentTicket()) {
                refresh = refresh || true;
                $.ajax({
                    url: Routing.generate('case_ajax_copy_structure'),
                    data: {sourceId: sourceId, targetId: targetId, instanceId: _dashboardService.getCurrentTicket('id')},
                    type: "POST",
                    success: function () {
                        if (refresh) {
                            onNextStep(99, true);
                        }
                        if (typeof callback === "function") {
                            callback();
                        }
                    }
                });
            }
        }

        function _setAttributeValue(path, value, callback, omitValidation, withDisplayValidation) {

            withDisplayValidation = (typeof withDisplayValidation === "undefined") ? false : withDisplayValidation;

            var $t = $('[data-attribute-path="' + path + '"]');
            if ($t.length > 0) {
                if (typeof callback !== "function") {
                    callback = function () {
                        return false;
                    }
                }

                if ($t[0].type === "checkbox") {
                    $t.prop('checked', (value == 1) ? true : false);
                }
                else if ($t[0].type === "radio") {
                    if(value == null || value == ""){
                        $t = $('[data-attribute-path="' + path + '"]').prop('checked', false);
                    }
                    else {
                        $t = $('[data-attribute-path="' + path + '"][value="' + value + '"]');
                        $t.prop('checked', true);
                    }
                }
                else if((!$($t).hasClass('date-picker')) && ($t[0].type === "text" || $t[0].type === "email" || $t[0].type === "number" ||$($t).hasClass('custom-select'))) {
                    $t.val(value);
                }
                else if($t[0].type === "date" || $($t).hasClass('date-picker')) {

                    /**
                     * Tu prawdopodobnie data wchodzi w formacie 'YYYY-MM-DD', i taka musi być zapisana do input="date",
                     * ale do bazy musi iść w formacie 'DD-MM-YYYY'
                     */

                    $t.val(value);
                    if(value) {
                        var setDate;
                        var dtPicker = $('[data-attribute-path="74,233"]').data('datepicker');
                        var regEx = /^\d{4}-\d{2}-\d{2}$/;
                        if(value.match(regEx)) {
                            value = moment(value, 'YYYY-MM-DD').format('DD-MM-YYYY');
                        }
                        if(dtPicker){
                            setDate =  moment(value, 'DD-MM-YYYY').toDate();
                            dtPicker.selectDate(setDate);
                        }
                    }

                }

                _socket.setAttribute({
                    "_this": $t,
                    "path": $t.attr('data-attribute-path'),
                    "groupProcessId": $t.attr('data-group-process-id'),
                    "stepId": (_dashboardService.isPreviewStep()) ? 'xxx' : $t.attr('data-step-id'),
                    "valueId": $t.attr('data-attribute-value-id'),
                    "value": value,
                    "valueType": $t.attr('data-type') ? $t.attr('data-type') : 'string',
                    "omitValidation" : omitValidation || false,
                    "formControl": $t.closest('.grid-stack-item[data-id]').attr('data-id')
                }, function(result) {

                    if(withDisplayValidation) {
                        displayValidation(result, $t);
                    }

                    if(typeof callback === "function") {
                        callback(result);
                    }

                });
            }
            else {
                console.info('Nie istnieje kontrolka ze ścieżką: ' + path + '. form_control?');
            }
        }

        function _getAttributeValue(path, type, callback) {

            type = type || 'string';

            _socket.getAttribute({
                "path": path,
                "groupProcessId": _dashboardService.getCurrentTicket('groupProcessId'),
                "stepId": _dashboardService.getCurrentTicket('stepId'),
                "valueType": type
            }, function (result) {
                if(typeof callback === "function") {
                    callback(result.value);
                }
            });

        }

        function _nextStep() {
            $('.operation-dashboard-form .next-button').trigger('click');
        }

        function _previousStep(currentInstanceId, instanceId) {
            $.ajax({
                url: Routing.generate('case_ajax_reactivate_process'),
                type: "POST",
                data: {
                    'id': instanceId,
                    'previousId': currentInstanceId
                },
                success: function (newId) {
                    if(newId) {

                        _dashboardService.setCurrentTicketId(newId);

                        _socket.getTask(newId, function (result) {
                            createForm(result);
                            reportUserAtInstance();
                            refreshCurrentTaskId();
                        });
                    }
                }
            });
        }

        function _openModal(url, width, height) {
            var width = width || 1000;
            var height = height || 1000;
            popupWindow = window.open(url, 'window', 'width=' + width + ', height=' + height + ',location=yes,menubar=‌​yes,scrollbars=yes,resizable=no');
            popupWindow.focus();
            document.onmousedown = _focusPopup;
            document.onkeyup = _focusPopup;
            document.onmousemove = _focusPopup;
        }

        function _focusPopup() {
            if (popupWindow && !popupWindow.closed) {
                popupWindow.focus();
            }
        }

        function loadTaskOverview(id){
            _socket.getOverview(id, function(result){
                createOverview(result);
            });
        }

        function _changeServiceStatus(serviceId, statusId) {
            var processInstanceId = _dashboardService.getCurrentTicket('id');
            $.ajax({
                url: Routing.generate('ajax_change_service_status'),
                type: "POST",
                data: {'processInstanceId' : processInstanceId, 'serviceId' : serviceId, 'statusId' : statusId},
                success: function (data) {
                    toastr.info(data.message);
                }
            });
        }

    }(window.jQuery, window, document)
);

/**
 * @param options
 * @returns {*|HistoryValueViewer}
 * @constructor
 */
function HistoryValueViewer (options) {

    /**
     * @typedef {Object} AttributeHistory
     * @property {Number} id
     * @property {Array} histories
     * @property {String} label
     */

    var _defaults,
        /**
         * @type {Object}
         * @property {null|DashboardService} dashboardService
         * @property {String} findSelector
         */
        _options,
        _ajax = null,
        /**
         * @type {Array.<AttributeHistory>} _histories
         */
        _histories = [];

    if (!(this instanceof HistoryValueViewer)) {
        return new HistoryValueViewer(options);
    }

    function prepareItem($item) {
        var $itemContent = $item.closest(_options.containerSelector);
        if(!$itemContent.hasClass('init-viewer')) {
            $itemContent.append(_options.htmlIcon);
            $itemContent.addClass('init-viewer');
        }
    }

    function prepareContent() {

        var htmlContent = '<div class="attribute-history-tables">';

        $.each(_histories, function(i, attribute) {

            htmlContent += '<p class="m-y-1"><b>'+((attribute.label) ? attribute.label : '---')+'</b></p>';
            htmlContent += prepareTable(attribute.histories);

        });

        htmlContent += '</div>';

        return htmlContent;
    }

    function prepareTable(attributeHistory) {

        var tableHtml = '<table class="table table-bordered table-condensed"><thead><tr><th>Data</th><th>Osoba</th><th>Wartość</th></tr></thead><tbody>';

        $.each(attributeHistory, function(i, item) {
            tableHtml += prepareRow(item);
        });

        tableHtml += '</tbody></table>';

        return tableHtml;
    }

    /**
     * @typedef {Object} AttributeHistoryRow
     * @property {String} date
     * @property {String} userName
     * @property {String} value
     */

    /**
     * @param {AttributeHistoryRow} row
     * @returns {string}
     */
    function prepareRow(row) {

        return '<tr>' +
            '<td>'+row.date+'</td>' +
            '<td>'+((row.userName) ? row.userName : '-')+'</td>' +
            '<td>'+((row.value) ? row.value : '')+'</td>' +
            '</tr>';

    }
    
    /**
     * @param $icon
     * @param attributeValueId
     * @returns {Promise}
     */
    function getAttributeValueHistories($icon, attributeValueId) {

        var dfd = jQuery.Deferred();

        if(_ajax !== null) {
            _ajax.abort();
        }

        _ajax = $.ajax({
            url: Routing.generate('ODIattributeValueHistory'),
            type: "GET",
            data: {
                attributeValueId: attributeValueId,
                processInstanceId: _options.dashboardService.getCurrentTicket('id')
            },
            beforeSend: function() {
                $icon.find('.fa').addClass('fa-spinner fa-spin').removeClass('fa-clock-o');
            },
            success: function (result) {
                dfd.resolve(result);
            },
            error: function (error) {
                dfd.reject(error);
            },
            complete: function() {
                $icon.find('.fa').removeClass('fa-spinner fa-spin').addClass('fa-clock-o');
            }
        });

        return dfd.promise();

    }

    function onClickIcon(e) {

        e.preventDefault();

        var $t = $(this),
            attributeValueId = parseInt($t.parent().find('[data-attribute-value-id]').attr('data-attribute-value-id'));

        getAttributeValueHistories($(this), attributeValueId).then(function (result) {

            var index = null;

            result.label = $t.parent().find('.js-label-text').text();

            if(_options.appendHistory) {
                $.each(_histories, function (i, ele) {
                    if(ele.id === result.id) {
                        index = i;
                        return false;
                    }
                });

                if(index !== null) {
                    _histories.splice(index, 1);
                }

                _histories.unshift(result);
            }
            else {
                _histories = [result];
            }

            var content = prepareContent();

            _f.openModal({
                header: 'Historia atrybutów',
                body: content
            }, null, null, true);

        })

    }

    function handleEvents($content) {
        $content.on('click', '.icon-viewer-control', onClickIcon);
    }

    this.initIcons = function($content) {

        $content.find(_options.findSelector).each(function (i, ele) {
            prepareItem($(ele));
        });

        handleEvents($content);

    };

    _init(options);

    function _initDefaults () {

        _defaults = {
            dashboardService: null,
            appendHistory: true,
            containerSelector: '.grid-stack-item-content',
            findSelector: '.form-control[data-attribute-value-id],.custom-select[data-attribute-value-id],[type="radio"][data-attribute-value-id],[type="checkbox"][data-attribute-value-id]',
            htmlIcon: '<div class="icon-viewer-control"><i class="fa fa-clock-o"></i></div>'
        };

        _options = $.extend(_options, _defaults);
    }

    function _setOptions (options) {
        if (typeof options !== "undefined") {
            _options = $.extend(_options, options);
        }
    }

    function _init (options) {
        _initDefaults();
        _setOptions(options);
    }
}

function _showPhoneCall(number) {
    $('.call-helpline').attr('data-number', number).show();
}

function _phoneCall(t) {

    var $t = $(t),
        number = $t.attr('data-number');

    if(number) {
        window.open('tel:' + number);
    }

}

function CreateAztecScript(onClose) {

    var url = "https://aztec.neptis.pl/js/aztec-decoder.js",
        promise = jQuery.Deferred();

    $.getScript( url, function() {
        initWrapper();
        promise.resolve();
    });

    function initWrapper() {

        var _html =
            '<div id="aztec_camera_wrapper" class="hidden">' +
            '<div id="aztec_camera_container"></div>' +
            '<p class="text-center m-y-0 p-y-1"><button type="button" class="btn btn-circle btn-default close-scan"><i class="fa fa-close"></i> Zamknij</button></p>' +
            '</div>';

        $('.operation-dashboard-form').append(_html);
        $('#aztec_camera_container').css('height', window.innerWidth);

        if(typeof onClose === "function") {
            $('#aztec_camera_wrapper .close-scan').on('click', onClose);
        }

    }

    return promise;
}

/**
 * @typedef {Object} options
 * @property {Function} options.onVideoInitializedSuccessHandler
 * @property {Function} options.onVideoInitializedErrorHandler
 * @property {Function} options.onDecodedSuccessHandler
 * @property {Function} options.onDecodedErrorHandler
 */
function initAztecScan(options) {

    var promise = jQuery.Deferred(),
        decoder,
        scanSuccess = false;

    function initDecoder() {

        var apiKey = '1d7ef35f-11d8-4fe0-9d46-c75c20d9fd95';
        var videoContainerId = 'aztec_camera_container';
        var downloadVehicleHistory = false;

        var onVideoInitializedSuccessHandler = function () {
            if(typeof  options.onVideoInitializedSuccessHandler === "function") {
                options.onVideoInitializedSuccessHandler();
            }
        };

        var onScanSuccessHandler = function () {

            toastr.info("Zeskanowano kod z dowodu. Trwa pobieranie danych...");
            stopAztecScan(decoder);

        };

        var onDecodedSuccessHandler = function (registrationCertificateDataJson) {

            if(scanSuccess) return;
            scanSuccess = true;

            if(typeof  options.onDecodedSuccessHandler === "function") {
                options.onDecodedSuccessHandler(registrationCertificateDataJson);
            }

            $.ajax({
                url: window.location.origin + "/js-logger",
                type: "POST",
                data: {
                    msg: registrationCertificateDataJson,
                    method: 'onDecodedSuccessHandler',
                    type: 'AZTEC',
                    processId: window.currentProcessId,
                    browser_url: window.location.href,
                    browser_version: navigator.sayswho
                }
            });

        };

        var onDecodedErrorHandler = function(message) {

            if(typeof options.onDecodedErrorHandler === "function") {
                options.onDecodedErrorHandler(message);
            }

            $.ajax({
                url: window.location.origin + "/js-logger",
                type: "POST",
                data: {
                    msg: message,
                    method: 'onDecodedErrorHandler',
                    type: 'AZTEC',
                    processId: window.currentProcessId,
                    browser_url: window.location.href,
                    browser_version: navigator.sayswho
                }
            });

        };

        var onVideoInitializedErrorHandler = function (message) {

            if(typeof options.onVideoInitializedErrorHandler === "function") {
                options.onVideoInitializedErrorHandler(message);
            }

            $.ajax({
                url: window.location.origin + "/js-logger",
                type: "POST",
                data: {
                    msg: message,
                    method: 'onVideoInitializedErrorHandler',
                    type: 'AZTEC',
                    processId: window.currentProcessId,
                    browser_url: window.location.href,
                    browser_version: navigator.sayswho
                }
            });

        };

        decoder = new Neptis.RegistrationCertificateDecoder(apiKey, videoContainerId, downloadVehicleHistory, onVideoInitializedSuccessHandler, onVideoInitializedErrorHandler, onScanSuccessHandler, onDecodedSuccessHandler, onDecodedErrorHandler);

        promise.resolve(decoder);

    }

    initDecoder();

    return promise;

}

function stopAztecScan(decoder) {

    try {

        var $video = $('#aztec_camera_container video');

        if($video.length) {
            $video[0].srcObject;
            if($video[0].srcObject) {
                $video[0].srcObject.getTracks().forEach(function (value) {
                    value.stop();
                })
            }

        }

    }
    catch (e) {}

    $('#aztec_camera_wrapper').addClass('hidden');
    $('#aztec_camera_container').children().remove();

    if(decoder && decoder.videoCodeReader) {
        decoder.videoCodeReader.stop();
    }

}

function startAztecScan(decoder) {
    decoder.start();
}

/**
 * @typedef {Object} registrationCertificateDataJson
 * @property {Object} registrationCertificateDataJson.aztec
 * @property {Object} registrationCertificateDataJson.vh
 */
function prepareAztecCode(registrationCertificateDataJson) {

    var aztecArray = registrationCertificateDataJson.aztec.split('|');

    /*
    Oznaczenie P.3 w dowodzie rejestracyjnym, oznaczające rodzaj paliwa:
        P – benzyna
        D – olej napędowy
        M – mieszanka (paliwo-olej)
        LPG – gaz płynny (propan-butan)
        CNG – gaz ziemny sprężony (metan)
        H – wodór
        LNG – gaz ziemny skroplony (metan)
        BD – biodiesel
        E85 – etanol
        EE – energia elektryczna
        999 – inne
    */

    return {
        registrationNumber: aztecArray[7].replace(' ', ''),
        mark: aztecArray[8],
        model: aztecArray[12],
        vin: aztecArray[13],
        dmc: aztecArray[39],
        fuel: aztecArray[50].trim(),
        dpr: aztecArray[51]
    };

}