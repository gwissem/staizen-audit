window.defGlobal = {
    'windowResizeHandleInitialized': false,
    'DEBUG_MODE': false
};

if(moment) {
    moment.locale('pl');
}

var globalVar = {
    focusWindow: false,
    lastFocus: new Date(),
    focusTtl: 60000,
    timePassed: function () {
        /** if passed 1 min, refresh summary of tool */
        var now = (new Date()).getTime();

        if ((now - globalVar.lastFocus) > globalVar.focusTtl) {
            globalVar.lastFocus = now;
            return true;
        }

        return false;
    },
    dateTimeDisableRefresh: false
};

function hasIframeAccess($iframe) {

    try {
        $iframe.contents();
    }
    catch (e) {
        return false;
    }

    return true;

}

function frameIsSameOrigin(ifr) {

    var sameOrigin;

    try
    {
        sameOrigin = ifr.contentWindow.location.host === window.location.host;
    }
    catch (e)
    {
        sameOrigin = false;
    }

    return sameOrigin;

    // 18-Feb-2019 10:11:26

}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function clearHomeUrl() {

    var disablePhoneModuleParam = getParameterByName('disable_phone_module'),
        actionParam = getParameterByName('action');

    if(disablePhoneModuleParam !== null && actionParam === null) {
        window.history.pushState("", "Atlas", Routing.generate('index', {}, true));
    }
}

var specialCharsFrom = "ążźćśęńłó";
var specialCharsTo = "azzcsenlo";
var poiRegex = /(\[SŁUPEK:){1}[ążźćęśńłóĄŻŹĆĘŚŃŁÓA-Za-z0-9=,>:();\- ]+(\])/;

String.prototype.replaceSpecialChars = function () {

    var str = this.toString();

    for (var i = 0, l = specialCharsFrom.length; i < l; i++) {
        str = str.replace(new RegExp(specialCharsFrom.charAt(i), 'g'), specialCharsTo.charAt(i));
    }

    return str;
};

String.prototype.getStringBetween = function (s, e) {
    s = s || "{@";
    e = e || "@}";

    var params = this.match(new RegExp(s + '([a-zA-Z0-9 _]*)' + e, "g"));

    if (params === null) return [];

    return params.map(function (item) {
        return item.replace(s, '').replace(e, '');
    });
};

String.prototype.parseParamsInString = function (data, prefix, suffix) {
    prefix = prefix || "{@";
    suffix = suffix || "@}";

    var str = this,
        params = str.getStringBetween(prefix, suffix);

    params.forEach(function (ele) {
        if (typeof data[ele.trim()] !== "undefined") {
            str = str.replace(prefix + ele + suffix, data[ele.trim()]);
        }
    });

    return str;
};

if (!String.prototype.repeat) {
    String.prototype.repeat = function(count) {
        'use strict';
        if (this == null) {
            throw new TypeError('can\'t convert ' + this + ' to object');
        }
        var str = '' + this;
        count = +count;
        if (count != count) {
            count = 0;
        }
        if (count < 0) {
            throw new RangeError('repeat count must be non-negative');
        }
        if (count == Infinity) {
            throw new RangeError('repeat count must be less than infinity');
        }
        count = Math.floor(count);
        if (str.length == 0 || count == 0) {
            return '';
        }
        // Ensuring count is a 31-bit integer allows us to heavily optimize the
        // main part. But anyway, most current (August 2014) browsers can't handle
        // strings 1 << 28 chars or longer, so:
        if (str.length * count >= 1 << 28) {
            throw new RangeError('repeat count must not overflow maximum string size');
        }
        var rpt = '';
        for (;;) {
            if ((count & 1) == 1) {
                rpt += str;
            }
            count >>>= 1;
            if (count == 0) {
                break;
            }
            str += str;
        }
        // Could we try:
        // return Array(count + 1).join(this);
        return rpt;
    }
}

if (typeof Array.prototype.forEach != 'function') {
    Array.prototype.forEach = function (callback) {
        for (var i = 0; i < this.length; i++) {
            callback.apply(this, [this[i], i, this]);
        }
    };
}

$.fn.hasScrollBar = function () {
    return this.get(0).scrollHeight > this.height();
};

function forceReload($btn, link) {
    var href = ($btn === false) ? link : $btn.attr('href');

    window.location.href = href;
    if (window.location.pathname == href.substr(0, href.indexOf('#'))) {
        window.location.reload();
    }
}

/**
 * @function delayedCallback
 */
var delayedCallback = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

/** Druga funkcja po to, żeby nie konfliktowała się z delayedCallback */

var delayedCallbackAttribute = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function functionsForResizeEvent() {

    var self = {};

    // var functions = [];
    var functions = {};

    self.addFunction = function (name, functionRef, run) {
        // functions.push({
        //     n: name,
        //     fun: functionRef
        // });

        functions[name] = {
            n: name,
            fun: functionRef
        };

        if (run === true) functionRef();
    };

    self.run = function () {
        for(var eventName in functions) {
            functions[eventName].fun();
        }
        // functions.forEach(function (ele) {
        //     ele.fun()
        // })
    };

    return self;

}

/**
 * Klasa do odpalania funkcji przy window.resize , wszystko w jednym miejscu
 */

var funResizeHandle = functionsForResizeEvent();

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function humanFileSize(bytes, si) {
    var thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + ' ' + units[u];
}

function windowResizeHandle() {

    /** Blokada przed podwójnym zadeklarowaniem */
    if (window.defGlobal.windowResizeHandleInitialized) return false;

    window.defGlobal.windowResizeHandleInitialized = true;

    var eventTimeout = null;
    $(window).on('resize', function () {
        if (!eventTimeout) {
            eventTimeout = setTimeout(function () {
                funResizeHandle.run();
                eventTimeout = null;
            }, 500);
        }
    });
}

function displayDebugTokenLink(xhr) {

    if(typeof xhr.getAllResponseHeaders === "function" && typeof toastr !== "undefined") {

        var headerMap = getHeadersMap(xhr.getAllResponseHeaders());

        if(headerMap['x-debug-token-link']) {
            toastr.info('<a href="'+ headerMap['x-debug-token-link'] +'" target="_blank">' + headerMap['x-debug-token-link'] + '</a>', 'Link do profilera')
        }

    }

}

function getHeadersMap(headers) {

    var arr = headers.trim().split(/[\r\n]+/);

    var headerMap = {};

    arr.forEach(function (line) {
        var parts = line.split(': ');
        var header = parts.shift();
        var value = parts.join(': ');
        headerMap[header] = value;
    });

    return headerMap;
}

function handleAjaxResponseError(e, useConsole) {

    try {

        var response = JSON.parse(e.responseText);

        if (response.message && response.exception) {
            toastr.error(response.exception.message, response.message, {timeOut: 5000});
        }
        else if (response.msg) {
            toastr.error(response.msg, "Błąd ładowania", {timeOut: 5000});
        }
        else {
            toastr.error(e.responseText, "Błąd ładowania", {timeOut: 5000});
        }

    }
    catch (exception) {

        var $h1 = $(e.responseText).find('.text-exception h1'),
            msg = ($h1.length) ? $h1.html() : '';

        toastr.error(msg, e.statusText + ' (' + e.status + ')', {timeOut: 5000});
    }

    if (useConsole) console.error(e);

}

function initTimer() {

    var time,
        lastLoop,
        i = 1,
        results = [],
        obj;

    return {
        start: function () {
            results = [];
            i = 1;
            time = new Date();
            lastLoop = new Date();
        },
        lap: function (name, display) {

            name = name || ( 'lap ' + i);

            display = display || true;

            var now = new Date();
            obj = {
                'name': name,
                'time': now,
                'diff': now.getTime() - time.getTime(),
                'diffLast': now.getTime() - lastLoop.getTime()
            };

            results.push(obj);

            lastLoop = now;
            i++;

            if (display) console.log('%c' + name + ':%c\t  Diff: %c' + obj.diff + "ms%c. Diff last loop: %c" + obj.diffLast + 'ms', 'border-bottom: 1px #26C281 solid; color: #26C281', 'color: #000; font-weight: 400;', 'color: blue; font-weight: 700;', 'color: #000; font-weight: 400;', 'color: blue; font-weight: 700;');

        }
    }
}

var globalTimer;

/** Funkcja dla datepicker'a, żeby aktualizował jego pozycje - żeby był cały widoczny */

function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();
    var fitsLeft = (rect.left >= 0 && rect.left <= $(window).width());
    var fitsTop = (rect.top >= 0 && rect.top <= $(window).height());
    var fitsRight = (rect.right >= 0 && rect.right <= $(window).width());
    var fitsBottom = (rect.bottom >= 0 && rect.bottom <= $(window).height());
    return {
        top: fitsTop,
        left: fitsLeft,
        right: fitsRight,
        bottom: fitsBottom,
        all: (fitsLeft && fitsTop && fitsRight && fitsBottom)
    };
}


var datePickerOptions = {
        language: 'pl',
        timeFormat: 'hh:ii',
        dateFormat: 'yyyy-mm-dd',
        position: "bottom right",
        autoClose: true,
        clearButton: true,
        keyboardNav: false,
        onSelect: function () /** formattedDate, date, inst */ {
            if (typeof refreshView == "function" && !globalVar.dateTimeDisableRefresh) {
                delayedCallback(function () {
                    refreshView();
                }, 500);
            }
        },
        onShow: function (inst, animationComplete) {
            if (!animationComplete) {
                var iFits = false;
                $.each(['bottom center', 'right center', 'right bottom', 'right top', 'top center', 'left center'], function (i, pos) {
                    if (!iFits) {
                        inst.update('position', pos);
                        var fits = isElementInViewport(inst.$datepicker[0]);
                        if (fits.all) {
                            iFits = true;
                        }
                    }
                });
            }
        }
    };

var dateTimePickerOptions  = Object.create(datePickerOptions);
// dateTimePickerOptions.dateFormat = 'yyyy-mm-dd';
dateTimePickerOptions.timeFormat = 'hh:ii';
dateTimePickerOptions.timepicker = true;

var miniLoaderTemplate = '<div class="loader mini relative"><div class="ball-spin-fade-loader"><div></div><div></div><div></div><div></div><div></div><div></div> <div></div> <div></div> </div></div>';

function getLoaderTemplate(title) {
    return '<div class="loader"><div class="ball-spin-fade-loader"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div><p class="loader-title"><small>'+title+'</small></p></div>';
}

var customDatepickerOptions = datePickerOptions;
customDatepickerOptions.position = "bottom right";

navigator.sayswho= (function(){
    var ua= navigator.userAgent, tem,
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();

window.onerror = function (msg, url, line, col, error) {

    if(window.location.href.indexOf('/app_dev.php') !== -1) {
        return false;
    }

    if(line === 0 && col === 0){
        return false
    }

    conLog(msg, url, line, col, error);

    // if(url.indexOf('cometchat_lib') !== -1) {
    //
    // }

    $.ajax({
        url: window.location.origin + "/js-logger",
        type: "POST",
        data: {
            msg: msg,
            url: url,
            line: line,
            col: col,
            error: error,
            processId: window.currentProcessId,
            browser_url: window.location.href,
            browser_version: navigator.sayswho
        }
    });
};

function conLog(a,b,c,d,e) {
    console.error(a,b,c,d,e);
}

function MaxNumber(event) {

    if(parseInt(event.target.value) > 2147483647) {
        event.target.value = event.target.value.slice(0,9);
        return false;
    }

}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

/**
 * @param event
 * @returns {boolean}
 * @constructor
 */

function FilterNumber(event) {

    var keyCode = ('which' in event) ? event.which : event.keyCode;

    /** Wywalenie e,+, +, -, -  */
    return !(keyCode === 69 || keyCode === 187 || keyCode === 107 || keyCode === 189 || keyCode === 109);

}

function handlePasteNumber (e) {

    if(parseInt(e.target.value) > 2147483647) {
        e.target.value = e.target.value.slice(0,10);
        e.stopPropagation();
        e.preventDefault();
    }

    var clipboardData, pastedData;

    clipboardData = e.clipboardData || window.clipboardData;
    pastedData = clipboardData.getData('Text').toUpperCase();

    if(pastedData.indexOf('E') > -1) {
        e.stopPropagation();
        e.preventDefault();
    }
}

var CASE_NUMBER_LENGTH = 8;

function getCaseNumberFromString(rootId, prefix) {
    prefix = (typeof(prefix) === "undefined") ? "A" : prefix;
    return prefix + "0".repeat(CASE_NUMBER_LENGTH - String(rootId).length) + rootId;
}

/**
 * This function allow you to modify a JS Promise by adding some status properties.
 * Based on: http://stackoverflow.com/questions/21485545/is-there-a-way-to-tell-if-an-es6-promise-is-fulfilled-rejected-resolved
 * But modified according to the specs of promises : https://promisesaplus.com/
 */
function MakeQuerablePromise(promise) {
    // Don't modify any promise that has been already modified.
    if (promise.isResolved) return promise;

    // Set initial state
    var isPending = true;
    var isRejected = false;
    var isFulfilled = false;

    // Observe the promise, saving the fulfillment in a closure scope.
    var result = promise.then(
        function(v) {
            isFulfilled = true;
            isPending = false;
            return v;
        },
        function(e) {
            isRejected = true;
            isPending = false;
            throw e;
        }
    );

    result.isFulfilled = function() { return isFulfilled; };
    result.isPending = function() { return isPending; };
    result.isRejected = function() { return isRejected; };
    return result;
}

function checkIfIE(){

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        return true;
    }
    else  // If another browser, return 0
    {
        return false;
    }

}

if(checkIfIE()) {

    var note = '<div class="ie-warning"">' +
        '<div class="portlet box red metronic-portlet col-xs-12 col-sm-8 col-sm-offset-2">' +
        '<div class="portlet-title"><div class="caption">Powiadomienie</div></div>' +
        '<div class="portlet-body lead">' +
        '<p>Dbając o Twoje bezpieczeństwo, nie pozwalamy korzystać z aplikacji w przeglądarkach niespełniajacych najnowszych standardów bezpieczeństwa IT. </p>' +
        '<p>Prosimy użyj jednej z tych przeglądarek:</p>' +
        '<ul>' +
        '<li><a target="_blank" href="https://www.google.pl/chrome" style="color: #000">Google Chrome</a></li> ' +
        '<li><a target="_blank" href="https://www.mozilla.org/pl/firefox/" style="color: #000">Mozilla Firefox</a></li> ' +
        '<li><a target="_blank" href="https://www.opera.com/pl" style="color: #000">Opera</a></li> ' +
        '<li><a target="_blank" href="https://www.microsoft.com/pl-pl/windows/microsoft-edge" style="color: #000">Microsoft Edge</a></li> ' +
        '<li><a target="_blank" href="https://www.apple.com/safari/" style="color: #000">Safari</a></li> ' +
        '</ul>' +
        '</div></div>' +
        '</div>';

    $('.form-placement-wrapper').html(note);

}

function _Log(msg) {

    if(window.defGlobal.DEBUG_MODE) {
        console.log('%c' + msg, 'background:#91cef3;color:#000;padding:5px');
    }

}

(function(window){
    "use strict";
    var log = Math.log;
    var LN2 = Math.LN2;
    var clz32 = Math.clz32 || function(x) {return 31 - log(x >>> 0) / LN2 | 0};
    var fromCharCode = String.fromCharCode;
    var originalAtob = atob;
    var originalBtoa = btoa;
    function btoaReplacer(nonAsciiChars){
        // make the UTF string into a binary UTF-8 encoded string
        var point = nonAsciiChars.charCodeAt(0);
        if (point >= 0xD800 && point <= 0xDBFF) {
            var nextcode = nonAsciiChars.charCodeAt(1);
            if (nextcode !== nextcode) // NaN because string is 1 code point long
                return fromCharCode(0xef/*11101111*/, 0xbf/*10111111*/, 0xbd/*10111101*/);
            // https://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
            if (nextcode >= 0xDC00 && nextcode <= 0xDFFF) {
                point = (point - 0xD800) * 0x400 + nextcode - 0xDC00 + 0x10000;
                if (point > 0xffff)
                    return fromCharCode(
                        (0x1e/*0b11110*/<<3) | (point>>>18),
                        (0x2/*0b10*/<<6) | ((point>>>12)&0x3f/*0b00111111*/),
                        (0x2/*0b10*/<<6) | ((point>>>6)&0x3f/*0b00111111*/),
                        (0x2/*0b10*/<<6) | (point&0x3f/*0b00111111*/)
                    );
            } else return fromCharCode(0xef, 0xbf, 0xbd);
        }
        if (point <= 0x007f) return inputString;
        else if (point <= 0x07ff) {
            return fromCharCode((0x6<<5)|(point>>>6), (0x2<<6)|(point&0x3f));
        } else return fromCharCode(
            (0xe/*0b1110*/<<4) | (point>>>12),
            (0x2/*0b10*/<<6) | ((point>>>6)&0x3f/*0b00111111*/),
            (0x2/*0b10*/<<6) | (point&0x3f/*0b00111111*/)
        );
    }
    window["btoaUTF8"] = function(inputString, BOMit){
        return originalBtoa((BOMit ? "\xEF\xBB\xBF" : "") + inputString.replace(
            /[\x80-\uD7ff\uDC00-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]?/g, btoaReplacer
        ));
    }
    //////////////////////////////////////////////////////////////////////////////////////
    function atobReplacer(encoded){
        var codePoint = encoded.charCodeAt(0) << 24;
        var leadingOnes = clz32(~codePoint);
        var endPos = 0, stringLen = encoded.length;
        var result = "";
        if (leadingOnes < 5 && stringLen >= leadingOnes) {
            codePoint = (codePoint<<leadingOnes)>>>(24+leadingOnes);
            for (endPos = 1; endPos < leadingOnes; ++endPos)
                codePoint = (codePoint<<6) | (encoded.charCodeAt(endPos)&0x3f/*0b00111111*/);
            if (codePoint <= 0xFFFF) { // BMP code point
                result += fromCharCode(codePoint);
            } else if (codePoint <= 0x10FFFF) {
                // https://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
                codePoint -= 0x10000;
                result += fromCharCode(
                    (codePoint >> 10) + 0xD800,  // highSurrogate
                    (codePoint & 0x3ff) + 0xDC00 // lowSurrogate
                );
            } else endPos = 0; // to fill it in with INVALIDs
        }
        for (; endPos < stringLen; ++endPos) result += "\ufffd"; // replacement character
        return result;
    }
    window["atobUTF8"] = function(inputString, keepBOM){
        if (!keepBOM && inputString.substring(0,3) === "\xEF\xBB\xBF")
            inputString = inputString.substring(3); // eradicate UTF-8 BOM
        // 0xc0 => 0b11000000; 0xff => 0b11111111; 0xc0-0xff => 0b11xxxxxx
        // 0x80 => 0b10000000; 0xbf => 0b10111111; 0x80-0xbf => 0b10xxxxxx
        return originalAtob(inputString).replace(/[\xc0-\xff][\x80-\xbf]*/g, atobReplacer);
    };
})(typeof global == "" + void 0 ? typeof self == "" + void 0 ? this : self : global);

(function ( $ ) {

    $.fn.fixDatePickerValue = function() {

        this.each(function() {

            if(this.value)
            {
                var date = new Date(this.value);

                if( !isNaN( date.getTime() ) )
                {
                    $(this).data('datepicker').selectDate(date);
                }
            }

        });

        return this;

    };

}( jQuery ));

String.prototype.toMM = function () {
    var sec_num = parseInt(this, 10);
    var hours = Math.floor(sec_num / 3600);
    return Math.floor((sec_num - (hours * 3600)) / 60);

    // if (minutes < 10) {
    //     minutes = minutes;
    // }
    //
    // return minutes;
};


Number.prototype.toMM = String.prototype.toMM;


String.prototype.toSS = function () {
    var sec_num = parseInt(this, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    return sec_num - (hours * 3600) - (minutes * 60);

    // if (seconds < 10) {
    //     seconds = seconds;
    // }

    // return seconds;
};

Number.prototype.toSS = String.prototype.toSS;

function initCircleProgress(id, timestampEnd, limitTime) {

    var $ppc = $('#' + id),
        wasTick = false;

    if(window['timer_' + id]) {
        clearInterval(window['timer_' + id]);
    }

    if($ppc.length === 0) return;

    function _tick() {

        if($ppc.length) {

            var now = ((new Date().getTime()) / 1000),
                diff = timestampEnd - now,
                percent = ((diff/limitTime) * 100 ).toFixed(2),
                deg = 360*percent/100;

            if(diff < 0) {
                clearInterval(window['timer_' + id]);
                $ppc.removeClass('hidden');

                if(wasTick) {
                    window.location.reload();
                }

                return false;
            }

            if (percent > 50) {
                $ppc.addClass('gt-50');
            }
            else {
                $ppc.removeClass('gt-50');
            }

            $ppc.find('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
            $ppc.find('.pcc-percents-wrapper .min').html(diff.toMM() + " <small>min</small>");
            $ppc.find('.pcc-percents-wrapper .sec').html(diff.toSS() + " <small>sec</small>");

            if($ppc.hasClass('hidden')) {
                $ppc.removeClass('hidden');
            }

            wasTick = true;
        }
        else {
            clearInterval(window['timer_' + id]);
        }

    }

    _tick();

    window['timer_' + id] = setInterval(_tick, 1000);

}

function enableScaleWindow() {
    $('head meta[name=viewport]').remove();
    $('head').prepend('<meta name="viewport" content="width=device-width"/>');
}

function disableScaleWindow() {
    $('head meta[name=viewport]').remove();
    $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0"/>');
}

function getPaddingAndMarginHeight($ele) {
    return $ele.outerHeight(true) - $ele.height();
}