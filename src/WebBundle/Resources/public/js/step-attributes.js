var $spinner = null;
var attributeSearchValue = '';
var isAttributeEditor = true;

function initAttributeChart(divId, chartData) {
    handleDragDrop();
    $spinner.trigger('spinner-on');
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    // Now we can initialize a Diagram that looks at the visual tree that constitutes the Diagram above.
    attributeDiagram =
        $(go.Diagram, divId,
            {
                initialContentAlignment: go.Spot.Center,
                // have mouse wheel events zoom in and out instead of scroll up and down
                "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
                "undoManager.isEnabled": true,
                "animationManager.isEnabled": false,
                scale: 1,
                padding: 10,
                layout: $(go.TreeLayout, {nodeSpacing: 10})  // automatically laid out as a tree
            });

    attributeDiagram.nodeTemplate =
        $(go.Node, "Auto",
            {
                doubleClick: function (e, node) {
                    if (isAttributeEditor) {
                        openAttributeFormModal(attributeDiagram, node.data);
                    }
                    else {
                        addStepAttribute(node.data);
                    }
                }
            },
            $(go.Shape, {
                fill: "#fafafa", stroke: "#111", strokeWidth: 0.75, portId: "",  // this Shape is the Node's port, not the whole Node
                fromLinkable: true, fromLinkableSelfNode: false, fromLinkableDuplicates: false,
                toLinkable: true, toLinkableSelfNode: false, toLinkableDuplicates: false,
                cursor: "pointer"
            }),
            $(go.TextBlock,
                {
                    font: "10pt Open Sans, bold helvetica",
                    stroke: "#111",
                    margin: new go.Margin(8, 12)
                },
                // bind the text to the Diagram/Layer/Part/GraphObject converted to a string
                new go.Binding("text", "name")
            )
        );
    attributeDiagram.linkTemplate =
        $(go.Link,
            {
                routing: go.Link.AvoidsNodes,
                corner: 2
            },
            $(go.Shape, {stroke: "#111", strokeWidth: 0.75}),
            $(go.Shape,  // the arrowhead
                {toArrow: "standard", stroke: null})
        );

    attributeDiagram.addDiagramListener("SelectionDeleting",
        function (e) {
            for (var it = e.diagram.selection.iterator; it.next();) {
                var part = it.value;  // part is now a Node or a Group or a Link or maybe a simple Part
                if (part instanceof go.Node) {
                    removeFromChart(part);
                }
            }
        });

    attributeDiagram.model = go.Model.fromJson(chartData);
    attributeDiagram.validCycle = go.Diagram.CycleNotDirected;

    $spinner.trigger('spinner-off');
}


function spinnerHandle() {

    $spinner = $('#bottom-spinner');

    $spinner.on('spinner-off', function (e) {
        $spinner.fadeOut(300);
    });

    $spinner.on('spinner-on', function (e) {
        $spinner.fadeIn(300);
    });
}

function calculateAttributeDiagramHeight(diagram, reload, modal) {
    var modal = modal || false;
    var reload = reload || false;
    var offset = modal ? $('#step-modal .modal-header').outerHeight() + 50 : $('.page-header').outerHeight() + 80;
    var btnOffset = modal ? 9 : 60;
    var calculatedHeight = window.innerHeight - offset;
    diagram.div.style.height = calculatedHeight + 'px';
    if (reload || modal) {
        diagram.requestUpdate();
    }
    $('#attribute-list').height(calculatedHeight - btnOffset + 'px');
    if (modal) {
        var stepListHeight = calculatedHeight - 5;
        $('#step-attributes-list').height(stepListHeight + 'px');
        $('#attribute-list').height(calculatedHeight - 50 + 'px');
    }
}

function handleDragDrop() {

    var dragged = null;
    var attributeList = document.getElementById("attribute-list");
    attributeList.addEventListener("dragstart", function (event) {
        if (event.target.className !== "draggable") return;
        // Some data must be set to allow drag
        event.dataTransfer.setData("text", "");

        // store a reference to the dragged element
        dragged = event.target;
        // Objects during drag will have a red border
        event.target.style.background = "#f2784b";
        event.target.style.color = "#fff";
    }, false);

// This event resets styles after a drag has completed (successfully or not)
    attributeList.addEventListener("dragend", function (event) {
        event.target.style.background = "#fff";
        event.target.style.color = "#337ab7";
    }, false);

// Next, events intended for the drop target - the Diagram div

    var div = document.getElementById("attribute-chart");
    div.addEventListener("dragenter", function (event) {
        // Here you could also set effects on the Diagram,
        // such as changing the background color to indicate an acceptable drop zone

        // Requirement in some browsers, such as Internet Explorer
        event.preventDefault();
    }, false);

    div.addEventListener("dragover", function (event) {
        // We call preventDefault to allow a drop
        // But on divs that already contain an element,
        // we want to disallow dropping

        if (event.target.className === "dropzone") {
            // Disallow a drop by returning before a call to preventDefault:
            return;
        }

        // Allow a drop on everything else
        event.preventDefault();
    }, false);

    div.addEventListener("dragleave", function (event) {
        // reset background of potential drop target
        if (event.target.className == "dropzone") {
            event.target.style.background = "";
        }
    }, false);

    div.addEventListener("drop", function (event) {
        // prevent default action
        // (open as link for some elements in some browsers)
        event.preventDefault();

        // Dragging onto a Diagram
        if (this === attributeDiagram.div) {
            attributeDiagram.startTransaction('new node');
            //
            // attributeDiagram.model.addNodeData({
            //     id: dragged.getAttribute('data-id'),
            //     name: dragged.textContent
            // });


            $.ajax({
                    method: 'POST',
                    url: Routing.generate('admin_attribute_diagram_model', {id: dragged.getAttribute('data-id')}),
                    data: {'diagramData': attributeDiagram.model.toJson()},
                    beforeSend: function () {
                        $spinner.trigger('spinner-on');
                    },
                    success: function (response) {
                        attributeDiagram.model = go.Model.fromJson(response);
                        //dragged.remove();
                        $spinner.trigger('spinner-off');
                    }
                }
            );

        }

        attributeDiagram.commitTransaction('new node');
        // If we were using drag data, we could get it here, ie:
        // var data = event.dataTransfer.getData('text');
    }, false);
}

function getNodeLeafs(node, parent, leafsArray) {

    var parent = parent || null;
    var leafsArray = leafsArray || [];

    var rootId = parent ? parent.data.id : null;
    var rootName = parent ? parent.data.name : '';
    var data = {
        'id': node.data.id,
        'name': node.data.name,
        'rootId': rootId,
        'rootName': rootName
    };

    leafsArray.push(data);

    /*
    var parent = parent || null;
    var leafsArray = leafsArray || [];
    var childrenNodes = node.findTreeChildrenNodes();
    if (childrenNodes.count > 0) {
        for (var child = childrenNodes.iterator; child.next();) {
            getNodeLeafs(child.value, node, leafsArray);
        }
    }
    else {
        var rootId = parent ? parent.data.id : null;
        var rootName = parent ? parent.data.name : '';

        var data = {
            'id': node.data.id,
            'name': node.data.name,
            'rootId': rootId,
            'rootName': rootName
        };

        leafsArray.push(data);
     }*/

    return leafsArray;
}

function addStepAttribute(node) {
    var graphNode = attributeDiagram.findNodeForData(node);
    var nodeLeafs = getNodeLeafs(graphNode);
    //nodeLeafs.push(graphNode);

    $.ajax({
        method: 'POST',
        url: Routing.generate('admin_step_attribute_add', {
            'stepId': $('#step-modal').attr('data-id')
        }),
        data: {'attributes': nodeLeafs},
        beforeSend: function () {
            $spinner.trigger('spinner-on');
        },
        success: function (response) {
            $('.step-attribute').removeClass('added');
            handleStepAttributeEditor();
            $.each(response, function (key, stepAttribute) {
                var listItem = '<li class="step-attribute added" data-id="' + stepAttribute.id + '" data-attribute-id="' + stepAttribute.attributeId + '">'
                    + '<div class="attribute-label"><div class="col-xs-11 p-x-0 name-container">'
                    + '<span class="name">' + stepAttribute.name + '<span class="text-small"> ' + stepAttribute.parentName + '</span></span></div>'
                    + '<div class="col-xs-1 p-x-0"><a href="#" class="pull-right font-red-mint tooltips remove-step-attribute">'
                    + '<i class="fa fa-remove"></i>'
                    + '</a></div></div><div class="edit-form-container"></div></li>';
                $('#step-attributes-list').prepend(listItem);
                $('#step-attributes-list .step-attribute:first .name-container').trigger('click');
            });
            handleStepAttributeRemoving();


            $spinner.trigger('spinner-off');
        }
    });
}