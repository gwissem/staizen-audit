(function($, window, document) {

    var last_tool = '',
        tool_history;

    $(function() {

        if(!localStorage) return false;

        initToolHistory();

        last_tool = localStorage.getItem('last_tool');
        tool_history = JSON.parse(localStorage.getItem('tool_history'));

        if(last_tool !== "" && localStorage.getItem('tool_is_back') == 0)
        {
            if(tool_history[tool_history.length - 1] != last_tool){

                if(last_tool != removeVariableFromURL(window.location.href, 'hash'))
                {
                    tool_history.push(last_tool);
                }
            }

            if(tool_history.length > 20) tool_history.shift();
            localStorage.setItem('tool_history', JSON.stringify(tool_history));
        }

        if(isToolView())
        {
            setLastTool(window.location.href);
        }
        else {
            setLastTool("");
        }

        localStorage.setItem('tool_is_back', 0);

        bindEventHandle();
    });

    function bindEventHandle() {

        var $backToolHistory = $('#backToolHistory');

        $(window).on('refreshLastTool', function (e) {
            setLastTool(window.location.href);
        });

        $backToolHistory.on('click', function (e) {
            e.preventDefault();

            if(tool_history.length !== 0){
                localStorage.setItem('tool_is_back', 1);
                var href = tool_history.pop(),
                    oldPathName = window.location.origin + window.location.pathname;
                localStorage.setItem('tool_history', JSON.stringify(tool_history));
                window.location.href = href;

                setTimeout(function (e) {
                    if(oldPathName == href.substr(0, href.indexOf('#'))){
                        window.location.reload();
                    }
                },20);
            }

        });

        if(tool_history.length === 0) {
            $backToolHistory.remove();
            // $backToolHistory.prop('disabled', true);
        }

        $('#side-menu-container a, #user-top-menu a, .top-menu a.summary-link').on('click', function (e) {
            clearHistory();
        });

        $('.back-to-tool').on('click', function () {
            if(tool_history.length > 0) tool_history.pop();
            localStorage.setItem('tool_history', JSON.stringify(tool_history));
        })
    }

    function clearHistory() {
        localStorage.setItem('tool_history', JSON.stringify([]));
        localStorage.setItem('last_tool', '');
    }

    function initToolHistory() {

        if(typeof localStorage.getItem('tool_history') === 'undefined' || localStorage.getItem('tool_history') === null)
        {
            localStorage.setItem('tool_history', JSON.stringify([]));
            localStorage.setItem('last_tool', '');
            localStorage.setItem('tool_is_back', 0);
        }
    }

    function isToolView() {
        return (window.location.pathname.indexOf('tools') !== -1 && window.location.hash !== "");
    }

    function setLastTool(href) {
        if(href === "")
        {
            localStorage.setItem('last_tool', "");
        }
        else {
            localStorage.setItem('last_tool', removeVariableFromURL(href, 'hash'));
        }
    }

    function removeVariableFromURL(url_string, variable_name) {
        var URL = String(url_string);
        var regex = new RegExp( "\\?" + variable_name + "=[^&]*&?", "gi");
        URL = URL.replace(regex,'?');
        regex = new RegExp( "\\&" + variable_name + "=[^&]*&?", "gi");
        URL = URL.replace(regex,'&');
        URL = URL.replace(/(\?|&)$/,'');
        regex = null;
        return URL;
    }

}(window.jQuery, window, document));