var jsTree;
var PartnerInterfaceModule = (function (_socket, opts) {

    var xhrRequest = null,
        $partnerTree,
        $partnerForm,
        $partnerInterfaceWrapper,
        $partnerInterfaceTree,
        spinnerHtml,
        $leafCheckbox,
        $devModeCheckbox,
        groupProcessId,
        $searcher,
        activeForm = {},
        $searcherForm,
        config = {};
        // jsTree = null;

    /** PRIVATE */

    var _init = function () {

        _initVariables();
        _initLayout();
        _loadConfig();
        _handleEvents();
        _loadRoot();

    };

    function _loadConfig() {

        var showLeaf = Cookies.get('partner_interface_show_leaf');
        var devMode = Cookies.get('partner_interface_dev_mode');

        $leafCheckbox.prop('checked', (parseInt(showLeaf)));
        $devModeCheckbox.prop('checked', (parseInt(devMode)));

        config.showLeaf = $leafCheckbox.is(":checked");
        config.devMode = $devModeCheckbox.is(":checked");

    }

    function _initLayout() {
        recalculateHeight();
    }

    function recalculateHeight() {

        var _height = $('#form-generator-box').height();
        $partnerInterfaceTree.css('max-height', _height);
        $partnerForm.css('max-height', _height);

    }

    function _initVariables() {
        $partnerInterfaceWrapper = $('.partner-interface-wrapper');
        $partnerInterfaceTree = $('.partner-interface-tree');
        spinnerHtml = $partnerInterfaceWrapper.attr('data-spinner');
        $partnerTree = $('#partner-tree');
        $partnerForm = $('.partner-interface-form');
        groupProcessId = parseInt($partnerTree.attr('data-group-process-id'));
        $leafCheckbox = $('#partner-interface-active-leaf');
        $devModeCheckbox = $('#partner-interface-dev-mode');
        $searcher = $('#search-partner-tree');
    }

    function _loadRoot() {

        if (xhrRequest !== null){
            xhrRequest.abort();
        }

        $.jstree.destroy();
        $partnerForm.html('');
        activeForm = {};

        $partnerTree.jstree({
            "core" : {
                check_callback: true,
                "themes" : {
                    "variant" : "large"
                },
                "data" : {
                    url: function(e) {
                        return (e.id === "#") ? Routing.generate('partner_interface_get_root', {groupProcessId: groupProcessId})
                            : Routing.generate('partner_interface_get_child', {groupProcessId: groupProcessId, parent: e.id, isMulti: (e.original.isMulti) ? '1' : '0'})
                    },
                    data: function() {
                        return {showLeaf: config.showLeaf, devMode: config.devMode};
                    }
                },
                "error" : function (e) {
                    toastr.error(e.reason, 'Wystąpił błąd podczas ładowania drzewka.', 3000);
                }
            },
            "contextmenu" : {
                "items": function($node) {

                    var contextMenu = {
                        "Refresh": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Odśwież",
                            "icon" : "fa fa-refresh",
                            "action": function (obj) {
                                jsTree.refresh_node($node);
                            }
                        },
                        "OpenAll": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Otwórz wszystko",
                            "icon" : "fa fa-plus-square-o",
                            "action": function (obj) {
                                jsTree.open_all($node);
                            }
                        }
                    };

                    if($node.original.path && $node.original.path === "595,597" && $node.original.isMulti === false) {
                        contextMenu['Copy'] = {
                            "separator_before": false,
                            "extra_data": $node.original,
                            "separator_after": false,
                            "label": "Skopiuj link",
                            "icon" : "fa fa-clipboard",
                            "action": function (obj) {
                                var rootId = parseInt($partnerTree.find('a[data-path="595"]').parent().attr('id'));
                                var locationId = parseInt(obj.item.extra_data.id);

                                if(rootId && locationId) {

                                    var btnCopy = $('.btn-clipboard-url');

                                    var url = Routing.generate('operational_dashboard_partners_index', {
                                        "partner-id": rootId,
                                        "attribute-id": locationId
                                    }, true);

                                    btnCopy.attr('data-clipboard-text', url);
                                    btnCopy.show();
                                    btnCopy[0].click();
                                    btnCopy.hide();
                                }

                            }
                        },
                        contextMenu['CacheClear'] = {
                            "separator_before": false,
                            "extra_data": $node.original,
                            "separator_after": false,
                            "label": "Odśwież cache",
                            "icon" : "fa fa-recycle",
                            "action": function (obj) {
                                var rootId = parseInt($partnerTree.find('a[data-path="595"]').parent().attr('id'));
                                var locationId = parseInt(obj.item.extra_data.id);

                                if(rootId && locationId) {
                                    $.ajax({
                                        url: Routing.generate('partner_interface_location_cache_clear', {location: locationId}),
                                        type: "GET",
                                        success: function (result) {
                                            toastr.success( '', 'Cache odświeżony.', 3000);
                                        }
                                    });
                                }

                            }
                        }
                    }

                    contextMenu["Cancel"] =  {
                        "separator_before": true,
                            "separator_after": false,
                            "label": "Anuluj",
                            "action": function (obj) {

                        }
                    };

                    return contextMenu;
                }
            },
            "search" : {
              "ajax": true
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder"
                }
            },
            "sort" : function (a, b) {
                if(this.get_node(a).original.priority === this.get_node(b).original.priority) {
                    return a > b ? 1 : -1
                    // return this.get_text(a) > this.get_text(b) ? 1 : -1
                }
                return this.get_node(a).original.priority > this.get_node(b).original.priority ? 1 : -1;
            },
            "plugins" : [ "types", "search", "sort", "contextmenu"]

        }).on("select_node.jstree", function (e, data, a) {

            var id;

            if(data.node.original.group === "leaf") {
                // Liść, więc budowanie formularza z parenta
                jsTree.deselect_node(data.node.id, true);
                jsTree.select_node(data.node.parent, true, true);
                return;
            }
            else {
                if(data.node.original.otherParentId) {
                    id = data.node.original.otherParentId.replace('_multi', '');
                }
                else {
                    id = data.node.id.replace('_multi', '');
                }
            }

            $partnerForm.html(spinnerHtml);

            $.ajax({
                url: Routing.generate('partner_interface_get_form', {groupProcessId: groupProcessId, attributeId: id}),
                type: "GET",
                success: function (result) {

                    $partnerForm.html('');

                    if(result.controls.length === 0) {
                        $partnerForm.append('<p style="text-align: center">Brak elementów do edycji</p>');
                    }
                    else {
                        $partnerForm.append('<p style="text-align: center">Edycja: <b>' + data.node.text.replace(/<(?:.|\n)*?>/gm, '') + '</b></p>');

                        $partnerForm.append('<div class="partner-interface-form-content"></div>');

                        var $content = $partnerForm.find('.partner-interface-form-content');

                        if(result.map) {

                            $content.append(result.map);

                            setTimeout(function () {
                                opts.initWidgetMap($partnerForm);
                            }, 200);

                        }

                        result.controls.forEach(function (ele, i) {
                            $content.append(ele.html);
                        });

                        activeForm = {
                            parentId: id,
                            path: (data.node.original.otherParentPath) ? data.node.original.otherParentPath : data.node.original.path
                        };

                        $content.find('[not-allow-edit="true"]').each(function () {

                            var $t = $(this);

                            if($t.attr('type') === "radio" || $t.attr('type') === "checkbox" ) {
                                $t.addClass('click-disabled').attr("disabled", true);
                            }
                            else if($t[0].tagName === "SELECT") {
                                $t.addClass('click-disabled').attr("disabled", true);
                            }
                            else {
                                $t.addClass('click-disabled').attr("readonly", true);

                                if($t.hasClass('date-picker') || $t.hasClass('date-time-picker')) {
                                    var _datePicker = $t.data('datepicker');
                                    if (_datePicker) {
                                        _datePicker.destroy();
                                    }
                                }
                            }

                            $t.closest('.toggler-wrapper').addClass('disabled');

                        });

                        opts.onControlsLoad($content);

                    }

                },
                error: function (error) {
                    handleAjaxResponseError(error);
                }
            });

        }).on('load_node.jstree', function (e, data) {


        }).on('search.jstree', function (e, data) {

        }).on('loaded.jstree', function (e, data) {

            var attribute = $('#partner-list').attr('data-find-attriute');

            if(attribute) {
                jsTree.select_node(parseInt(attribute));
                jsTree.open_node(parseInt(attribute));
            }
            else {
                var rootId = parseInt($partnerTree.find('a[data-path="595"]').parent().attr('id'));
                jsTree.select_node(rootId);
            }

        });

        jsTree = $.jstree.reference('#partner-tree');

    }

    var _handleEvents = function () {

        funResizeHandle.addFunction('recalculateHeight', recalculateHeight);

        $('#refresh-partner-tree').on('click', function (e) {
            e.preventDefault();
            _loadRoot();
        });

        $partnerTree.on('click', '.append-multi', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var $t = $(this),
                l = Ladda.create(this),
                parentAttrId = parseInt($t.attr('data-parent-attr-id')),
                attrId = parseInt($t.attr('data-attr-id'));

            l.start();

            _socket.addStructure({
                'parentValueId': parentAttrId,
                'attributeId': attrId
            }, function (result) {

                var idLi = $t.closest('li').attr('id');
                jsTree.refresh_node(idLi);

                l.stop();
                l.remove();

            }, function (e) {

                l.stop();
                l.remove();

            });

        });

        $leafCheckbox.on('change', function () {
            Cookies.set('partner_interface_show_leaf', ($(this).is(':checked') ? 1 : 0), { expires: 365 });
            config.showLeaf = $leafCheckbox.is(":checked");
            $('#refresh-partner-tree').trigger('click');
        });

        $devModeCheckbox.on('change', function () {
            Cookies.set('partner_interface_dev_mode', ($(this).is(':checked') ? 1 : 0), { expires: 365 });
            config.devMode = $devModeCheckbox.is(":checked");
            $('#refresh-partner-tree').trigger('click');
        });

        $searcher.on('keypress', function (e) {

            if (e.keyCode === 13) {
                e.preventDefault();

                jsTree.clear_search();
                jsTree.search($searcher.val(), true, true);

            }

        }).on('keyup', function () {

            if($searcher.val() === "") {
                jsTree.clear_search();
            }

        });

        document.addEventListener("setAttributeValue", _onChangeValue);
        $partnerTree.on('click', '.remove-multi', _initConfirmation);

    };

    function _initConfirmation(e) {

        var $t = $(this);

        if($t.hasClass('confirmation-init')) return;

        $t.addClass('confirmation-init');

        $t.confirmation({
            rootSelector: '.btn-multi-remove',
            btnOkClass: 'btn btn-danger',
            btnOkIcon: '',
            btnOkLabel: 'Tak',
            btnCancelClass: 'btn btn-default',
            btnCancelIcon: '',
            btnCancelLabel: 'Nie',
            onConfirm: _onRemoveMulti
        }).confirmation('show');

        e.stopPropagation();
        e.preventDefault();
    }
    
    function _onChangeValue(e) {

        if(e.detail) {

            /**
             * e.detail = {groupProcessId, path, processInstanceId, stepId, value, valueId, valueType}
             */

            if(activeForm.parentId) {

                if(activeForm.path === "595,597,642" && e.detail.path === "595,597,642,84") {
                    // Dane kontaktowe lokalizacji
                    // _setTreeText(activeForm.parentId, e.detail.value, e.detail.valueType);
                    _refreshNodeText(activeForm.parentId);
                }
                else if(activeForm.path === "595,597,611,642" && e.detail.path === "595,597,611,642,84") {
                    // Dane kontaktowe usługi
                    // _setTreeText(activeForm.parentId, e.detail.value, e.detail.valueType);
                    _refreshNodeText(activeForm.parentId);
                }
                else if(activeForm.path === "595,597,611,605" && e.detail.path === "595,597,611,605,606") {
                    // Dni tygodnia - godziny swiadczenia uslugi
                    _refreshNodeText(activeForm.parentId);
                }
                else if(activeForm.path === "595,597,611" && e.detail.path === "595,597,611,612") {
                    // typ usługi
                    _refreshNodeText(activeForm.parentId, true);
                }
                else if(activeForm.path === "595,597" && e.detail.path === "595,597,84") {
                    // nazwa konkretnej lokalizacji
                    _refreshNodeText(activeForm.parentId);
                }
                else if(activeForm.path === "595,597,85") {
                    // nazwa konkretnej lokalizacji
                    if(e.detail.path === "595,597,85,87") {
                        _refreshNodeText(jsTree.get_node(activeForm.parentId).parent);
                    }
                    else if(e.detail.path === "595,597,85,94") {
                        _refreshNodeText(jsTree.get_node(activeForm.parentId).parent);
                    }
                }
            }
        }

    }

    function _refreshNodeText(id, withRefresh) {

        if(!id) return false;

        delayedCallback(function () {
            $.ajax({
                url: Routing.generate('partner_interface_get_node_attribute_value', {id: id}),
                type: "GET",
                success: function (result) {

                    if(result && result.text) {

                        jsTree.set_text(id, result.text);

                        if(withRefresh) {
                            jsTree.refresh_node(id);
                        }

                    }

                },
                error: function (error) {
                    handleAjaxResponseError(error);
                }
            });
        }, 500);

    }

    function _setTreeText(id, value, valueType, path) {

        if(valueType === "string") {
            jsTree.set_text(id, value);
        }
        else if(valueType === "int") {

            var $ele = $partnerForm.find('[data-attribute-path="'+path+'"]');

            if($ele.length) {
                if($ele[0].tagName === "SELECT") {
                    if ($ele[0].selectedIndex) {
                        jsTree.set_text(id, $ele[0].options[$ele[0].selectedIndex].text);
                    }
                }
            }
        }
    }


    function _onRemoveMulti() {

        var $t = this,
            l = Ladda.create($t[0]);
        l.start();

        var id = $t.attr('data-attr-value-id');

        _socket.deleteAttribute({
            'attributeValueId': parseInt(id)
        }, function () {

            var liId = $t.closest('[is-multi="1"]').attr('id');

            if(parseInt(liId.replace('_multi', '')) === parseInt(id)) {

                var parent = $t.closest('[is-multi="1"]').parents('li').get(0);
                liId = $(parent).attr('id');

            }

            jsTree.refresh_node(liId);

            l.stop();
            l.remove();
        });
    }

    _init();

    return {

    }

});

var PartnerInterfaceTabModule = (function (_dashboardService, getTask) {

    var $partnerList = $('#partner-list'),
        partnerElementPrototype = '',
        $partnerInterfaceListWrapper = $('.partner-interface-list-wrapper'),
        $buttonAddPartner = $("#add-new-partner"),
        btnAddPartner = ($buttonAddPartner.length) ? Ladda.create($buttonAddPartner[0]) : null;

    function _init() {

        partnerElementPrototype = $partnerList.attr('data-prototype-partner');

        _eventsHandle();
        _loadPartners();
        _initFirstPartner();
    }

    function _initFirstPartner() {

        var _partner = $partnerList.attr('data-init-partner');

        if(_partner) {
            _partner = JSON.parse(_partner);

            if(_partner) {

                _dashboardService.setCurrentTicket({
                    'id': _partner.id,
                    'stepId': _partner.stepId,
                    'groupProcessId': _partner.groupProcessId,
                    'rootId': _partner.rootId
                });

                getTask(_partner.id);
            }
        }

    }

    function _eventsHandle() {

        $partnerList.on('click', '.single-partner-element', _clickOnElement);
        funResizeHandle.addFunction('calculateHeightPartnersList', _calculateHeight, true);

        $buttonAddPartner.on('click', function (e) {
            e.preventDefault();
            btnAddPartner.start();

            $.ajax({
                url: Routing.generate('operation_dashboard_add_partner'),
                type: "POST",
                success: function (data) {

                    _dashboardService.setCurrentTicket({
                        'id': data.description.id,
                        'stepId': data.description.stepId,
                        'groupProcessId': data.description.groupProcessId,
                        'rootId': data.description.rootId
                    });

                    getTask(data.description.id);
                    _loadPartners();

                },
                error: function (e) {
                    handleAjaxResponseError(e);
                },
                complete: function () {
                    btnAddPartner.stop();
                }
            });

        });

        $('#partner-search').on('keypress', function(e){

            if (e.keyCode === 13) {
                e.preventDefault();

                var searchValue = $(this).val();

                _loadPartners(searchValue);

            }

        });

        var clipboard = new Clipboard('.btn-clipboard-url');

        clipboard.on('success', function () {
            toastr.info('Skopiowano link.');
        });

    }

    function _calculateHeight() {

        var _height = $('#page-main-content').height() - ($('.case-search-wrapper').outerHeight(true) + $('.admin-operation-row').outerHeight(true));
        $partnerInterfaceListWrapper.css('max-height', _height);

    }

    function _loadPartners(searchTag) {

        if(typeof searchTag === "undefined") searchTag = '';

        _startLoading();

        $.ajax({
            url: Routing.generate('partner_interface_get_partners'),
            type: "GET",
            data: {'searchTag': searchTag},
            success: function (data) {

                _renderList(data.partners);

            },
            error: function (e) {
                handleAjaxResponseError(e);
            },
            complete: function () {
                _stopLoading();
            }
        });

    }

    function _renderList(partners) {

        var $ul = $partnerList.find('> ul');

        $ul.find('> li').remove();

        $.each(partners, function (i, ele) {
            $ul.append(_renderHtml(ele));
        })
    }

    function _renderHtml(partner) {

        return partnerElementPrototype
            .replace('__NAME__', ( (partner.name) ? partner.name : 'Brak nazwy') + " (" + (partner.partnerId) + ")")
            .replace('__LONG_NAME__', (partner.longName) ? partner.longName : '-')
            .replace(/__GROUP_PROCESS_ID__/g, partner.groupProcessId)
            .replace('__STEP_ID__', partner.stepId)
            .replace('__ROOT_ID__', partner.rootId)
            .replace('__ID__', partner.id)
            .replace('__ ADDITIONAL_CLASSES__', (partner.status == 6) ? 'partner-element-red ' : '');
    }

    function _clickOnElement(e) {

        e.preventDefault();

        var $t = $(this),
            _id = $t.attr('data-attribute-value-id'),
            _rootId = $t.attr('data-root-id'),
            _stepId = $t.attr('data-attribute-value-step-id'),
            _group = $t.attr('data-attribute-value-group-process-id');

        $t.addClass('active').siblings().removeClass('active');

        _dashboardService.setCurrentTicket({
            'id': _id,
            'stepId': _stepId,
            'groupProcessId': _group,
            'rootId': _rootId
        });

        getTask(_id);

    }

    function _startLoading() {
        $partnerList.addClass('loading');
    }

    function _stopLoading() {
        $partnerList.removeClass('loading');
    }

    _init();

    return {

    }

});
