var jexl = require('Jexl');

function strtotime(dateString) {

    if((dateString.toUpperCase() === "NOW")) {
        return (new Date()).getTime() / 1000;
    }

    return (new Date(dateString)).getTime() / 1000;

}

jexl.addTransform('valueOfPath', function(path) {

    path = validatorModule.normalizePath(path);

    var _value = _f.getValueByPath(path);

    /**
     * Nie znaleziono kontrolki na formularzu, odpytanie backendu
     */

    // TODO - Można by dorzucić Promise już wykonywanego Ajaxa
    if(_value === false) {
        return validatorModule.getAttrValue(path);
    }

    return _value;

});

jexl.addTransform('config', function(key) {

    if(window.availableProcessConfig.length) {

        var value = null;

        window.availableProcessConfig.forEach(function (ele, i) {

            if(ele.key === key) {
                value = ele.value;
                return 1;
            }

        });

        if(value !== null) {
            return value;
        }

        // TODO - nie znaleziono, więc też ajaxowo coś...
        return "";

    }
    else {
        // TODO - wyciągniecie config'a ajaxowo
    }

    return "";

});

var validatorModule = (function() {

    var _dashboardService = {},
        promiseAttributeValue = function () {

            var queue = {};

            return {
                isPromise: function (path) {
                    return (typeof queue[path] !== "undefined");
                },
                getPromise: function (path) {
                    return queue[path];
                },
                setPromise: function (path, promise) {
                    queue[path] = promise;
                    return promise;
                },
                removePromise: function (path) {
                    delete queue[path];
                }
            }

        }();

    function _valid(condition, context, cb) {

        if(typeof context === "undefined") context = {};

        // try {

            jexl.eval(condition, context).then(function(res) {
                cb(res);
            }).catch(function (reason) {
                console.error('Error in ValidatorModule! Condition: ' + condition + '. Reason: ' + reason);
            });

        // }
        // catch (e) {
        //
        //     console.error('Error in ValidatorModule! Condition: ' + condition);
        //
        // }

    }

    function _asyncValid(controlId, condition, cb) {

        // TODO - Można by dorzucić Promise już wykonywanego Ajaxa
        try {
            $.ajax({
                url: Routing.generate('operationalDashboardInterfaceCheckControlCondition'),
                type: 'POST',
                data: {
                    "formControlId": controlId,
                    "processInstanceId": _dashboardService.getCurrentTicket('id'),
                    "typeCondition": 'visible'
                },
                beforeSend: function(xhr){
                    if(typeof _processToken !== 'undefined' && _processToken) {
                        xhr.setRequestHeader('Process-Token', _processToken);
                    }
                },
                success: function (response) {
                    cb(response.conditionResult);
                },
                error: function () {
                    cb(false);
                }

            });
        }
        catch (e) {
            console.log('Error in AsyncValid!');
            console.error(e);
        }

    }

    return {

        addVisibilityCondition: function (selector, condition, controlId) {

            setTimeout(function () {

                $('#form-generator-box').find(selector).on('change', function () {
                    _valid(condition, {}, function (isValid) {
                        if(isValid) {
                            _f.show(controlId);
                        }
                        else {
                            _f.hide(controlId);
                        }
                    })
                });

            }, 0);

        },
        addAsyncVisibilityCondition: function (selector, condition, controlId) {

            setTimeout(function () {

                /**
                 * Listener nasłuchuje na "success-set-attribute" czyli moment, w którym już jest success SET ATTRIBUTE
                 */
                $('#form-generator-box').find(selector).on('success-set-attribute', function () {

                    _asyncValid(controlId, condition, function (isValid) {
                        if (isValid) {
                            _f.show(controlId);
                        } else {
                            _f.hide(controlId);
                        }
                    });

                });

            }, 0);

        },
        normalizePath: function (path) {
              return path.replace('{@', '').replace('@}', '').replace('*', '').replace('%', '');
        },
        setDashboardService: function (dashboardService) {
            _dashboardService = dashboardService;
        },
        getAttrValue: function (path) {

            /**
             * Lekki Cache - gdy idzie request z zapytaniem o tą samą ścieżkę, to czeka na zwrotkę, a nie odpytuje drugi raz
             */

            if(promiseAttributeValue.isPromise(path)) {
                return promiseAttributeValue.getPromise(path);
            }
            else {
                return promiseAttributeValue.setPromise(path, (new Promise(function(resolve, reject) {

                    $.ajax({
                        url: Routing.generate('operationalDashboardInterfaceGetAttributeValue'),
                        type: 'POST',
                        data: {
                            "path": path,
                            "groupProcessId": _dashboardService.getCurrentTicket('groupProcessId'),
                            "stepId": _dashboardService.getCurrentTicket('stepId'),
                            "valueType": 'guess'
                        },
                        beforeSend: function(xhr){
                            if(typeof _processToken !== 'undefined' && _processToken) {
                                xhr.setRequestHeader('Process-Token', _processToken);
                            }
                        },
                        complete: function () {
                           promiseAttributeValue.removePromise(path);
                        },
                        success: function (response) {
                            resolve(response.value);
                        },
                        error: function () {
                            reject(null);
                        }
                    });

                })));
            }

        }
    }

})();
