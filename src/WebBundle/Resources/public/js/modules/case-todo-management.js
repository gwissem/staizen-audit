/**
 * @class CaseTodoManagement
 */
var CaseTodoManagement = (function(_tryGetTask, _dashboardService, _socket) {

    var $body,
        $casePanelManagementTodo,
        $casePanelManagementTodoContent,
        $casePanelManagementTodoTask;

    var moduleIsLoaded = false,
        setTimeoutRefreshTask = null,
        alertIntervalTime = 10000,
        alertTimer;

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */


    function eventsHandle() {
        // funResizeHandle.addFunction('recalculateHeightCustomAccordion', recalculateHeightCustomAccordion);

        // $notificationForm.on('click', '.widget-instant-call', function (e) {
        //
        //     e.preventDefault();
        //
        //     var $t = $(this);
        //     $t.prop('disabled', true);
        //
        //     var number = $t.parent().find('> input').val();
        //
        //     if (!number) toastr.info("", "Numer jest pusty", {timeOut: 3000});
        //
        //     var cEvent = new CustomEvent("telephony-call-number", {
        //         'detail': {
        //             number: number
        //         }
        //     });
        //
        //     parent.document.dispatchEvent(cEvent);
        //
        //     setTimeout(function () {
        //         $t.prop('disabled', false);
        //     }, 2000);
        //
        // });

        // window.document.body.dispatchEvent(new CustomEvent("refresh-preview-email"));

    }

    function _afterLoadTask() {
        clearInterval(alertTimer);
        alertTimer = setInterval(_setTaskAlert, alertIntervalTime);
    }

    /**
     * @param {Object} payload
     * @param {int} payload.processInstanceId - Czy uprawnienia
     */
    function incomingReserveTask(payload) {

        if(!moduleIsLoaded) return false;

        /** Ktoś podebrał zadanie, ładowanie nowego */

        if(parseInt($("#user-next-task").attr('data-attribute-value-id')) === parseInt(payload.processInstanceId)) {
            loadTask();
        }
    }

    function _autoRefreshTask() {

        if(setTimeoutRefreshTask) {
            clearTimeout(setTimeoutRefreshTask);
        }

        setTimeoutRefreshTask = setTimeout(loadTask, 15000);

    }

    function _setTaskAlert() {
        if (!_dashboardService.getCurrentTicket('id')) {
            $casePanelManagementTodoTask.addClass('alerts-border');
        } else {
            $casePanelManagementTodoTask.removeClass('alerts-border');
        }
    }

    function refreshCurrentTask(processInstanceId) {

        if(!moduleIsLoaded) return false;

        var _id = (typeof processInstanceId !== "undefined") ? processInstanceId : _dashboardService.getCurrentTicket('id');

        if(_id) {
            $.ajax({
                url: Routing.generate('operationalDashboardInterfaceRefreshViewUserTask', {processInstanceId: _id}),
                type: "GET",
                beforeSend: function () {
                    $casePanelManagementTodoContent.addClass('loading');
                },
                success: function (result) {
                    if(result.html) {
                        $casePanelManagementTodoTask.html(result.html);
                        _afterLoadTask();
                    }
                },
                error: function (error) {
                    if(error.statusText !== "abort") {
                        handleAjaxResponseError(error);
                    }
                },
                complete: function () {
                    $casePanelManagementTodoContent.removeClass('loading');
                }

            });
        }

    }

    function loadTask() {

         if(!moduleIsLoaded) return false;

         $.ajax({
            url: Routing.generate('operationalDashboardInterfaceRenderUserTask'),
            type: "GET",
            beforeSend: function () {
                $casePanelManagementTodoContent.addClass('loading');
            },
            success: function (result) {
                if(result.html) {
                    $casePanelManagementTodoTask.html(result.html);
                    _afterLoadTask();
                }

                // Brak zadania - samoo-dświeżenie
                if(!result.data) {
                    _autoRefreshTask();
                }
            },
            error: function (error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {
                $casePanelManagementTodoContent.removeClass('loading');
            }

        });

    }

    function init() {
        $body = $('body');
        if($body.hasClass('public-mode')) return false;

        $casePanelManagementTodo = $('#case-panel-management-todo');

        if($casePanelManagementTodo.length) {

            $casePanelManagementTodoContent = $casePanelManagementTodo.find('.case-panel-management-todo-content');
            $casePanelManagementTodoTask = $casePanelManagementTodo.find('.case-panel-management-todo-task');

            moduleIsLoaded = true;

            eventsHandle();
            loadTask();

        }

    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */


    init();

    return {
        loadTask: loadTask,
        refreshTask: refreshCurrentTask,
        incomingReserveTask: incomingReserveTask
    }

});
