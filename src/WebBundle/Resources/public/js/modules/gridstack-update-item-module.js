/**
 * @class GridstackUpdateItemModule
 */
var GridstackUpdateItemModule = (function() {

    var _$grid = null;
    var _items = null;
    var _virtualForm = null;
    var _heightOfEmptyRow = 0;

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    /**
     * @param $grid
     */
    function getGridItems($grid) {
        /** Only first child of gridStack */
        return $grid.find('> .grid-stack-item');
    }

    /**
     * @param gridItems
     * @return {Object}
     */
    function getItems(gridItems) {
        var items = {},
            newItem = null;

        $.each(gridItems, function (i, ele) {

            newItem = {
                el: ele,
                id: ele.getAttribute('data-unique-id'),
                h: parseInt(ele.getAttribute('data-gs-height')),
                y: parseInt(ele.getAttribute('data-gs-y')),
                visible: (ele.getAttribute('data-is-visible') === "true") ? 1 : 0
            };

            items[newItem.id] = newItem;

        });

        return items;
    }

    /**
     * @param $content
     * @param index
     */
    function findElementsInRow($content, index) {

        return $content.find('> .grid-stack-item[data-gs-y="' + index + '"]').map(function () {
            return this.getAttribute('data-unique-id');
        }).get();

    }

    /**
     * @param $grid
     * @param items
     * @return {Array}
     */
    function generateVirtualForm($grid, items) {

        var amountOfRow = $grid.attr('data-gs-current-height'),
            virtualForm = [],
            newRow = null,
            emptyRowCount = 0;

        for(var i = 0; i < amountOfRow; i++) {

            var idOfItemsInRow = findElementsInRow($grid, i);

            newRow = {
                visible: 0,
                elements: []
            };

            if(idOfItemsInRow.length) {
                $.each(idOfItemsInRow, function (i, ele) {
                    newRow.elements.push(items[ele]);
                })
            }
            else {
                emptyRowCount++;
            }

            virtualForm.push(newRow);

        }

        _heightOfEmptyRow = emptyRowCount * 30 + ((emptyRowCount-1) * 4);

        return virtualForm;
    }

    function delayRecalculationHeightOfGrid() {

        var emptyRowCount = 0;
        $.each(_virtualForm, function(i, ele) {
            if(ele.visible === 1) emptyRowCount++;
        });
        var height = emptyRowCount * 30 + ((emptyRowCount-1) * 4) + 100;
        _$grid.css('height', height + 'px');

    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */


    function _updateGridItemsPosition() {

        _$grid = $('.step-form-grid-stack');
        _items = getItems(getGridItems(_$grid));
        _virtualForm = generateVirtualForm(_$grid, _items);

        _refreshVisibleInVirtualForm(_virtualForm);
        _updateLocationOfItems();

    }

    function _refreshVisibleInVirtualForm(virtualForm) {

        virtualForm = virtualForm || _virtualForm;

        if(virtualForm === null) return false;

        for(var j = virtualForm.length - 1; j >= 0; j--) {
            virtualForm[j].visible = 0;
        }

        $.each(virtualForm, function (i, row) {

            if(row.elements.length) {

                $.each(row.elements, function (j, item) {
                    if(item.h > 1) {

                        for(var k = i; k < i + item.h; k++) {
                            virtualForm[k].visible = virtualForm[k].visible || item.visible;
                        }

                    }
                    else {
                        row.visible = row.visible || item.visible;
                    }
                })
            }
        });

    }

    function _updateLocationOfItems() {

        if(_$grid === null) return false;

        var $gridItems = getGridItems(_$grid),
            id = null,
            y = 0,
            amountNotVisibleRow = 0,
            newY = 0;

        $.each($gridItems, function (i, ele) {

            /** Pomijanie ukrytych */

            if(ele.getAttribute('data-is-visible') === "true") {

                amountNotVisibleRow = 0;

                id = ele.getAttribute('data-unique-id');

                if(_items[id]) {
                    y = _items[id].y;
                    // y = parseInt(ele.getAttribute('data-gs-y'));

                    for(var j = 0; j < y; j++ ) {
                        if(_virtualForm[j].visible === 0) amountNotVisibleRow++;
                    }

                    newY = y - amountNotVisibleRow;

                    /** Aktualizacja formularza na stronie */
                    ele.setAttribute('data-gs-y', newY);
                }
            }

        });

        delayedCallback(delayRecalculationHeightOfGrid, 500);

    }

    /**
     * @param {String} _id
     */
    function _hideItem(_id) {
        if(_items !== null && _items[_id]) {
            _items[_id].visible = 0;
        }
    }

    /**
     * @param {String} _id
     */
    function _showItem(_id) {
        if(_items !== null && _items[_id]) {
            _items[_id].visible = 1;
        }
    }

    return {
        updateGridItemsPosition: _updateGridItemsPosition,
        updateLocationOfItems: _updateLocationOfItems,
        refreshVisibleInVirtualForm: _refreshVisibleInVirtualForm,
        hideItem: _hideItem,
        showItem: _showItem
    }

});

/**
 * @type {{updateGridItemsPosition, updateLocationOfItems, refreshVisibleInVirtualForm, hideItem, showItem}}
 */
var gridstackItemsModule = GridstackUpdateItemModule();
