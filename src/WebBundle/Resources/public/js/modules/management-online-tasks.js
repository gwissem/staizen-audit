var ManagementOnlineTasks = (function () {

    var INTERVAL_TABLE = 15000;

    var $wrapper,
        $content,
        tableInstance,
        tableInstanceApi,
        $reportTab,
        $reportTabLi,
        $formFilter;

    var loaded = false,
        blockShowEvent = false,
        intervalTable = null,
        ajaxRefresh = null;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $wrapper = $('#online-tasks-wrapper');
        $content = $wrapper.find('.tab-wrapper-content');
        $reportTab = $('a[href="#online-tasks"]');
        $reportTabLi = $reportTab.parent();

        _handleEvents();

    }

    function _tabIsActive() {
        return ($reportTabLi.hasClass('active'));
    }

    function _handleEvents() {

        $reportTab.on('shown.bs.tab', function() {
            if(!blockShowEvent) {
                iniLoad();
            }

        });

        if($reportTabLi.hasClass('active')) {
            iniLoad();
        }

        $reportTab.on("search-tasks", function (e) {

            if(e.detail) {

                blockShowEvent = true;
                $reportTab.tab('show');

                if(!loaded) {

                    _loadPanel(function () {

                        $formFilter.find('#groupId').val(e.detail.groupId);
                        $formFilter.find('#platformGroupId').val(e.detail.platformGroupId);
                        $formFilter.find('#column').val(e.detail.column);

                        loaded = true;
                        _afterLoadPanel();
                        blockShowEvent = false;

                    });

                }
                else {

                    $formFilter.find('#groupId').val(e.detail.groupId).trigger('change.select2');
                    $formFilter.find('#platformGroupId').val(e.detail.platformGroupId).trigger('change.select2');
                    $formFilter.find('#column').val(e.detail.column);
                    _refreshTable();
                    blockShowEvent = false;
                }

            }
        })

    }

    function iniLoad() {
        if(!loaded) {
            _loadPanel();
        }
        else {
            _refreshTable();
        }
    }

    function _loadPanel(cb) {

        $.ajax({
            url: Routing.generate('management_supervisor_report_real_time_online_tasks_content'),
            type: 'GET',
            beforeSend: function () {
                $wrapper.addClass('loading');
            },
            success: function (response) {

                $content.html(response.html);
                $formFilter = $wrapper.find('.filter-form');

                if(typeof cb === "function") {
                    cb();
                }
                else {
                    loaded = true;
                    _afterLoadPanel();
                }

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $wrapper.removeClass('loading');
            }
        });

    }

    function _afterLoadPanel() {

        $wrapper.find('.select2').select2(
            {
                width: '300px',
                allowClear: true
            }
            ).on('change', function () {
            _refreshTable();
        });

        _handleEventsAfterLoadPanel();
        _initTable();
        _refreshTable();

    }

    function getCheckboxHtml(id, isChecked) {

        return '<label class="mt-checkbox mt-checkbox-outline">' +
            '<input class="task-urgent-up" type="checkbox" ' + ((isChecked) ? "checked" : "") + ' name="task_urgent_' + id + '" id="task_urgent_' + id + '" value="'+id+'">' +
            '<span></span></label>';

    }

    function _initTable() {

        var url = Routing.generate('operational_dashboard_index', {
            action: 'telephony-pick-up',
            isforce: 1
        });

        tableInstance = $('#online-tasks-table').dataTable({
            data: [],
            columns: [
                {
                    title: "Id",
                    data: 'id',
                    type: 'integer',
                    visible: false
                },
                {
                    title: "Nazwa zadania",
                    data: 'name',
                    width: '15%',
                    render: function (data, type, row) {
                        return '<a href="' + url + '&process_instance=' + row.id + '" target="_blank">' + data + '</a>';
                    }
                },
                {
                    title: "Nazwa kroku",
                    data: 'stepName',
                    width: '15%'
                },
                {
                    title: "Opis",
                    data: 'description',
                    width: '15%'
                },
                // {
                //     title: "U użytkowników w kolejce",
                //     data: 'online_users'
                // },
                {
                    title: "Platforma",
                    data: 'platform_name',
                    type: 'integer'
                },
                {
                    title: "Priorytet",
                    data: 'priority'
                },
                {
                    title: "Numer sprawy",
                    data: 'root_id'
                },
                {
                    title: "Przeterminowanie",
                    data: 'timeout'
                },
                {
                    title: "Użytkownik",
                    data: 'user_id'
                },
                {
                    title: "Pilne",
                    data: 'urgent',
                    render: function (data, type, row) {
                        return getCheckboxHtml(row.id, parseInt(row.urgent));
                    }
                }
            ],
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Brak wyników",
                "info": "Wyświetlono od _START_ do _END_ z _TOTAL_ wyników",
                "infoEmpty": "Nie znaleziono wyników",
                "infoFiltered": "(1 z _MAX_ wszystkich wierszy)",
                "lengthMenu": "_MENU_ wyników na stronę",
                "search": "Szukaj:",
                "zeroRecords": "No matching records found"
            },

            buttons: [],
            responsive: true,
            "order": [
                [
                    5, 'desc'
                ],
                [
                    3, 'desc'
                ]
            ],
            "lengthMenu": [
                [5, 10, 20, 50, -1],
                [5, 10, 20, 50, "All"] // change per page values here
            ],
            "pageLength": 20
        });

        tableInstanceApi = tableInstance.api();

    }

    function _handleEventsAfterLoadPanel() {

        // window.onfocus = function() { _refreshTable(); };

        intervalTable = setInterval(function () {

            _refreshTable();

        }, INTERVAL_TABLE);

        $wrapper.find('.refresh-online-tasks').on('click', function (e) {
            e.preventDefault();
            _refreshTable();
        });

        $wrapper.on('change', '.task-urgent-up', function (e) {
            e.preventDefault();

            var $t = $(this);

            $.ajax({
                url: Routing.generate('management_supervisor_urgent_instance', { id: $t.val() }),
                type: 'POST',
                beforeSend:function () {
                    $t.prop('disabled', true);
                },
                success: function (response) {

                    var row = tableInstanceApi.row( $t.closest('tr')).data();
                    row.priority = response.priority;
                    row.urgent = (response.urgent) ? "1" : "0";

                    tableInstanceApi.row($t.closest('tr')).data(row);

                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    $t.prop('disabled', false);
                }
            });

        });

    }

    function getFiltersValue() {

        var filtersValue = {};

        $.each($formFilter.serializeArray(), function (i, ele) {
            filtersValue[ele.name] = ele.value;
        });

        return filtersValue;

    }

    function _refreshTable() {

        if(!_tabIsActive()) return;

        if(ajaxRefresh) {
            ajaxRefresh.abort();
        }

        ajaxRefresh = $.ajax({
            url: Routing.generate('management_supervisor_report_real_time_tasks', getFiltersValue()),
            type: 'GET',
            beforeSend:function () {
                $formFilter.addClass('loading');
            },
            success: function (response) {

                tableInstanceApi.clear();
                tableInstanceApi.rows.add(response.tasks);
                tableInstanceApi.draw();

            },
            error: function (error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {
                $formFilter.removeClass('loading');
            }
        });

    }

    $(function () {
        _init();
    });

    return {}

});
