(function () {

    var $adminToolsModal,
        lastTool = '';

    function _init() {
        $adminToolsModal = $('#admin-tools-modal');

        $(".show-admin-tool").on('click', openAdminToolModal);
    }

    function openAdminToolModal(e) {

        e.preventDefault();

        var $t = $(this),
            toolName = $t.attr('data-tool-name');

        $adminToolsModal.modal('show');

        var data = {};

        if(toolName === "parser-helper")
        {
            data['processInstanceId'] = $('#atlas-iframe')[0].contentWindow.currentProcessId
        }

        if(lastTool === toolName) {

            if(toolName === "parser-helper") {

                $('#parser-group-id').val(data.processInstanceId);

            }

        }
        else {

            lastTool = toolName;

            $.ajax({
                url: Routing.generate('aa_parser_helper', {toolName: toolName}),
                type: "GET",
                data: data,
                beforeSend: function() {
                    $adminToolsModal.find('.modal-body').children().remove();
                },
                success: function (result) {
                    $adminToolsModal.find('.modal-body').html(result);
                },
                error: function (error) {
                    if(error.statusText !== "abort") {
                        handleAjaxResponseError(error);
                    }
                },
                complete: function () {

                }

            });

        }

    }

    $(function () {
        _init();
    });

    return {

    }

})();
