var ManagementScheduleTelephonyStatus = (function () {

    var _NAME = 'schedule-telephony-status', // telephony-statistics
        $managementWrapper,
        $managementContent,
        $managementFilter,
        $managementTab,
        $managementTable,
        $managementTableBlocker,
        $managementFilterLoader,
        $managementTabLi;

    var loaded = false,
        loadTableAjax,
        saveAjax;

    var tempDatePickerOptions = Object.assign({}, datePickerOptions, {
        onSelect: function () {
            loadTable();
        }
    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $managementWrapper = $('#' + _NAME + '-wrapper');
        $managementContent = $managementWrapper.find('.tab-wrapper-content');
        $managementTab = $('a[href="#' + _NAME + '"]');
        $managementTabLi = $managementTab.parent();

        _handleEvents();

    }

    // var _delayCallback = (function () {
    //     var timer = 0;
    //     return function (callback, ms) {
    //         clearTimeout(timer);
    //         timer = setTimeout(callback, ms);
    //     };
    // })();

    function _handleEvents() {

        $managementTab.on('shown.bs.tab', function() {

            if(loaded) {
                loadTable();
            }

            iniLoad();
        });

        if($managementTabLi.hasClass('active')) {
            iniLoad();
        }

    }

    function iniLoad() {
        if(!loaded) {
            _loadPanel();
        }
    }

    function _loadPanel() {

        $.ajax({
            url: Routing.generate('management_sts_index'),
            type: 'GET',
            beforeSend: function () {
                $managementWrapper.addClass('loading');
            },
            success: function (response) {

                loaded = true;
                $managementContent.html(response.html);

                _afterLoadPanel();

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $managementWrapper.removeClass('loading');
            }
        });

    }

    function _afterLoadPanel() {

        $managementFilter = $managementContent.find('.' + _NAME + '-filter-form');
        $managementFilterLoader = $managementContent.find('.' + _NAME + '-filter-form .box-spinner');
        $managementTable = $managementContent.find('.' + _NAME + '-content-table table');
        $managementTableBlocker = $managementContent.find('.' + _NAME + '-content-table .blocker');

        setCachedFilter();

        var datepicker = $managementContent.find('.date-picker').datepicker(tempDatePickerOptions).fixDatePickerValue().data('datepicker');

        if (datepicker) {
            datepicker.update('minDate', moment().subtract(7,'days').toDate());
        }

        _handleEventsAfterLoadPanel();

    }

    function setCachedFilter() {

        // TODO

        $('#schedule-telephony-status-date').val(moment().format('YYYY-MM-D'));

    }

    function loadTable(e, page) {

        if(typeof e !== "undefined") {
            e.preventDefault();
        }

        if(typeof page === "undefined") {

           page = parseInt($managementFilter.find('.navigation .page-item.active a').text());

           if(isNaN(page)) {
               page = 1;
           }

        }

        if(!_tabIsActive()) return;

        var isOnline = $('#' + _NAME + '-online').is(":checked"),
            username = $('#' + _NAME + '-username').val(),
            date = _getDate();

        if(loadTableAjax) {
            loadTableAjax.abort();
        }

        loadTableAjax = $.ajax({
            url: Routing.generate('management_sts_table', {page: page}),
            type: 'GET',
            data: {
                online: isOnline,
                username: username,
                date: date
            },
            beforeSend: function () {
                $managementFilterLoader.show();
            },
            success: function (response) {

                $managementFilter.find('.pagination-wrapper').html(response.pagination);
                $managementContent.find('table tbody').html(response.html);

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $managementFilterLoader.hide();
            }
        });

    }

    function _getDate() {
        return  $('#' + _NAME + '-date').val();
    }

    /**
     * Tutaj jakieś eventy, po załadowaniu panelu
     */
    function _handleEventsAfterLoadPanel() {

        $managementFilter.find('.refresh').on('click', loadTable);

        $('#schedule-telephony-status-username').on('change', function (e) {
            loadTable(e,1);
        });

        $('#schedule-telephony-status-online').on('change', function (e) {
            loadTable(e,1);
        });

        $managementFilter.find('.pagination-wrapper').on('click', 'a', function (e) {
            loadTable(e, parseInt(this.innerHTML));
        });

        $managementContent.on('change', '.schedule-hour', onChangeCheckbox);
        $managementContent.on('click', '.select-all', onChangeCheckboxes);

        // /** Przykładowy interval odświeżania */
        // intervalContentAjax = setInterval(function () {
        //
        //     // refreshFunction();
        //
        // }, intervalContentTime);

    }

    function onChangeCheckboxes() {

        var $row = $(this).closest('tr'),
            $uncheckedCheckboxes = $row.find('.schedule-hour').not(':checked');

        var action = ($uncheckedCheckboxes.length > 0) ? "add" : "remove";

        if(action === "add") {

            var hours = $uncheckedCheckboxes.map(function (i, ele) {
                return ele.value;
            }).get();

            _changeHour({
                action: action,
                date: _getDate(),
                hours: hours,
                type: 'multi',
                user_id: $row.attr('data-user-id')
            });

        }
        else {

            var ids = $row.find('.schedule-hour').map(function (i, ele) {
                return parseInt(ele.getAttribute('data-id'));
            }).get();

            _changeHour({
                action: action,
                date: _getDate(),
                ids: ids,
                type: 'multi',
                user_id: $row.attr('data-user-id')
            });

        }

    }

    function onChangeCheckbox() {

        _changeHour({
            action: (this.checked) ? "add" : "remove",
            id: this.getAttribute('data-id'),
            date: _getDate(),
            hour: this.value,
            type: 'single',
            user_id: $(this).closest('tr').attr('data-user-id')
        }, this);

    }

    function _changeHour(data, checkbox) {

        saveAjax = $.ajax({
            url: Routing.generate('management_sts_save_hour'),
            type: 'POST',
            data: data,
            beforeSend: function () {
                if(data.type === "single") {
                    checkbox.disabled = true;
                }
                else if(data.type === "multi") {
                    $managementTableBlocker.show();
                    $managementFilterLoader.show();
                }
            },
            success: function (response) {

                $.each(response.users, function (userId, terms) {

                    var $tr = $managementTable.find('tr[data-user-id="'+userId+'"]');

                    $.each(terms, function (i, term) {

                        if(data.type === "single") {

                            if(term.id) {
                                $tr.find('.schedule-hour[value="' + term.hour + '"]').attr('data-id', term.id);
                            }
                            else {
                                $tr.find('.schedule-hour[value="'+term.hour + '"]').attr('data-id', "");
                            }

                        }
                        else if(data.type === "multi") {

                            if(term.id) {
                                $tr.find('.schedule-hour[value="' + term.hour + '"]').attr('data-id', term.id).prop('checked', true);
                            }
                            else {
                                $tr.find('.schedule-hour[value="'+term.hour + '"]').attr('data-id', "").prop('checked', false);
                            }

                        }

                    })

                });

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                if(data.type === "single") {
                    checkbox.disabled = false;
                }
                else if(data.type === "multi") {
                    $managementTableBlocker.hide();
                    $managementFilterLoader.hide();
                }
            }
        });

    }

    function _tabIsActive() {
        return ($managementTabLi.hasClass('active'));
    }


    $(function () {
        _init();
    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {}

});
