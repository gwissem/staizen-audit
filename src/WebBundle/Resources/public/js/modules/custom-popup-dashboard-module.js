var CustomDashboardPopupModule = (

    function () {

        var $modal,
            $title,
            $content,
            $closeIcon,
            _delay = 2000;

        /*
         * ----------------------------------------------------------------------------------------------------------
         * PRIVATE FUNCTIONS
         * ----------------------------------------------------------------------------------------------------------
         */

        function init() {
            $modal = $('#custom-dashboard-popup');
            $title = $modal.find('.header .title');
            $content = $modal.find('.text');
            $closeIcon = $modal.find('.close-this-popup');
        }

        function hideCloseOption() {
            $closeIcon[0].style.display = 'none';
        }

        function showCloseOption() {
            $closeIcon[0].style.display = 'block';
        }

        /*
         * ----------------------------------------------------------------------------------------------------------
         * PUBLIC FUNCTIONS
         * ----------------------------------------------------------------------------------------------------------
         */

        var _openModal = function (title, msg, withClose, withError, delayHide) {

            delayHide = (typeof delayHide === "undefined") ? false : delayHide;
            withClose = (typeof withClose === "undefined") ? true : withClose;
            withError = (typeof withError === "undefined") ? false : withError;

            if(withClose) {
                showCloseOption();
            } else {
                hideCloseOption();
            }

            if(withError) {
                $modal.addClass('error');
            } else {
                $modal.removeClass('error');
            }

            $title.html(title);
            $content.html(msg);

            $modal.fadeIn(500);

            if(delayHide) {
                setTimeout(function () {
                    $modal.hide();
                }, _delay);
            }

        };

        var _hideModal = function () {
            $modal.hide();
        };

        init();

        return {
            open: _openModal,
            hide: _hideModal
        }

    }

);

var customDashboardPopup = CustomDashboardPopupModule();
