var ManagementVersionAtlas = (function () {

    var $newLogBtn,
        $newAtlasVersionBtn,
        $tableAtlasVersionLogs,
        currentPage = 1;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */


    function _init() {

        $newLogBtn = $('#add-new-log');
        $newAtlasVersionBtn = $('#add-new-atlas-version');
        $tableAtlasVersionLogs = $('.atlas-version-logs-table');
        _loadTable();
        _handleEvents();
    }

    function _handleEvents() {

        $tableAtlasVersionLogs.on("click", ".btn-delete", _removeVersionLog);

        $(window.document.body).on('refresh-atlas-version', function (e) {
            _loadTable(currentPage);
        });

        $('#atlas-version-form').on('submit', function (e) {
            e.preventDefault();
        });

        $newAtlasVersionBtn.on('click', function (e) {
            e.preventDefault();

            var ladda = Ladda.create(this);

            ladda.start();

            var ids = [];

            $.each($('.version-log-id:checked'), function (i, ele) {
               ids.push(ele.value);
            });

            $.ajax({
                url: Routing.generate('management_admin_create_new_version'),
                type: 'POST',
                data: {
                    'version': $('#atlas-version-value').val(),
                    'checkedLogs': ids
                },
                success: function () {
                    _loadTable();
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    ladda.stop();
                }
            });

        });

        $('.atlas-version-pagination').on('click', '.page-link', function (e) {
            e.preventDefault();

            var page = getParameterByName('page', this.href);

            if(page) {
                _loadTable(page);
            }

        })
    }

    function _loadTable(page) {

        page = (typeof page === "undefined" || !page) ? 1 : page;

        var source = $tableAtlasVersionLogs.attr('data-source');

        $.ajax({
            url: source + "?page=" + page,
            type: 'GET',
            success: function (response) {

                if(response.list) {
                    $tableAtlasVersionLogs.find('tbody').html(response.list);
                }

                if(response.pagination) {
                    $('.atlas-version-pagination').html(response.pagination);
                }

                if(response.page) {
                    currentPage = response.page;
                }
            },
            error: function (response) {
                handleAjaxResponseError(response);
            }
        });

    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function _removeVersionLog(e) {

        e.preventDefault();

        var $t = $(this);
        $.ajax({
            url: $t.attr('data-url'),
            type: 'POST',
            success: function () {
                toastr.success("Pomyślnie usunięto.");
                $t.closest('tr').remove();
            },
            error: function (response) {
                handleAjaxResponseError(response);
            }
        });

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {

    }

});
