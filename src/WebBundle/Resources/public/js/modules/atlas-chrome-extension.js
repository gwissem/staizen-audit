/**
 * @class AtlasChromeExtension
 */
var AtlasChromeExtension = (function () {

    var wasReconnect = false;

    /**
     * @constructor
     */
    function AtlasChromeExtension () {

        console.log(window.atlasWebExtensionId);
        
        if(window.atlasWebExtensionId) {
            // Can change in parameters.yml

            AtlasChromeExtension.ID = window.atlasWebExtensionId;

        }
        else {

            AtlasChromeExtension.ID = 'npefhajgabfnhjiieeifbbnpcekidnam'; // FROM CHROME STRORE
            // AtlasChromeExtension.ID = 'hbflhhdgdohgklhiiflbppoaekednjkm'; // FROM LOCALE

        }

        AtlasChromeExtension.port = null;

    }

    function handleEvents() {

    }

    function connectWithExtension() {

        if(typeof chrome !== "undefined" && typeof chrome.runtime !== "undefined") {
            /**
             * @type {chrome.runtime.Port}
             */
            AtlasChromeExtension.port = chrome.runtime.connect(AtlasChromeExtension.ID);

            AtlasChromeExtension.port.onMessage.addListener(_onMessage);

            console.info('Connected with Atlas Extension: ' + AtlasChromeExtension.ID);
            wasReconnect = false;

            AtlasChromeExtension.port.onDisconnect.addListener(function(e){

                if(chrome.runtime.lastError && chrome.runtime.lastError.message === "Could not establish connection. Receiving end does not exist.") {
                    console.error(chrome.runtime.lastError.message);
                    // AtlasChromeExtension.port.onMessage.removeListener(_onMessage);
                    AtlasChromeExtension.port = null;
                    wasReconnect = true;
                }
                else {
                    console.error("Atlas Extension is disconnect.");

                    if(!wasReconnect) {
                        setTimeout(function () {
                            wasReconnect = true;
                            connectWithExtension();
                        }, 3000)
                    }

                }

            });

            if(canMonitorPage) {
                AtlasChromeExtension.port.postMessage({
                    action: 'initTimeWorkManager'
                });
            }

        }
    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    /**
     * @param {String} msg
     * @param {chrome.runtime.MessageSender} sender
     * @private
     */
    function _onMessage(msg, sender) {

        if(msg.type === "getState") {

            sendResponse(homeScript.getWindowState());

        }
        else if(msg.type === "getCurrentCase") {

            AtlasChromeExtension.port.postMessage({
                module: 'timeWorkManager',
                action: 'set-current-case',
                host: window.location.host,
                data: window.getCurrentCase()
            });

        }
        else if(msg.type === "pageStatistics") {

            $.ajax({
                url: Routing.generate('ajax_save_monitor_page'),
                type: "POST",
                data: {
                    'process-instance-id': (msg.currentCase) ? msg.currentCase.id : null,
                    'pages': msg.pages
                }
            });

        }

    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    AtlasChromeExtension.prototype.getPort = function () {
        return AtlasChromeExtension.port;
    };

    AtlasChromeExtension.prototype.connect = function (onMessage) {

        // onMessage(msg, sender)
        this.port.onMessage.addListener(onMessage);

        return {
            send: function (data) {
                AtlasChromeExtension.port.postMessage(data);
            }
        };

    };


    /**
     * @method run
     */
    AtlasChromeExtension.prototype.run = function () {
        connectWithExtension.call(this);
        handleEvents.call(this);
    };

    return AtlasChromeExtension;

})();

var atlasChromeExtension = new AtlasChromeExtension();
atlasChromeExtension.run();