/**
 * @class CaseNoteModule
 */
var CaseNoteModule = (function(inboxEditorModule, _dashboardService, options) {

    var $body,
        $notificationForm = $('#addNotification'),
        $pageContainer = $('.page-container'),
        $customAccordion = $('#customAccordion'),
        $noteTypes = $('#note-types'),
        $caseNote = $('#case-notes'),
        $noteFormWrapper = $('#note-form-wrapper'),
        $notificationFormWrapper = $noteTypes.find('.notification-type-wrapper'),
        $notificationTypeWrapperButton = $notificationFormWrapper.find('.type'),
        $notificationsOuterWrapper= $('.notifications-outer-wrapper'),
        $notificationWrapper = $('.notifications-wrapper'),
        $closeFormButton = $noteTypes.find('.closeForm'),
        $additionalCaseInfo = $('#additional-case-info'),
        $caseInfo = $('#case-info'),
        editMode = false,
        canResendEmail = false,
        $notesFiltersForm = $('.notes-filters-wrapper'),
        $notesFiltersType,
        $notesFiltersDirection,
        $notesFiltersSystem,
        $chatMessageModal,
        noteRootId = null,
        canReplyChat = false,
        $addChatMessage;

    var _getRootId = null;

    var noteTemplates = [],
        smsTemplates = [];

    var bookNumbers = [];

    var iconOfPreviewEmail = '' +
        '<span class="extra-content-note edit-email-note btn btn-icon-only" data-note-id="__NOTE_ID__"><i class="fa fa-pencil"></i></span>' +
        '<span class="extra-content-note preview-email btn btn-icon-only" data-attachment-id="__ATTACHMENT_ID__"><i class="fa fa-search"></i></span>';

    var iconOfChatReply = '' +
        '<span class="extra-content-note chat-reply btn btn-icon-only" data-chat-sender="__CHAT_SENDER__"><i class="fa fa-comments-o"></i></span>';

    var labelOfEmailStatus = '' +
        '<span class="extra-content-note label label-sm label-__STATUS_LABEL__ right" data-status="__STATUS__"><small>__STATUS_NAME__</small></span>';

    var reSendEmailBtn = '<span class="extra-content-note resend-email btn btn-icon-only" data-note-id="__ATTR_NOTE_ID__"><i class="fa fa-repeat"></i></span>';

    if(options) {

        if(options.$wrapper) {
            $caseNote = options.$wrapper;
        }

        if(options.$noteFilters) {
            $notesFiltersForm = options.$noteFilters;
        }

        if(options.$addChatMessage) {
            $addChatMessage = options.$addChatMessage;
            $chatMessageModal = $addChatMessage.closest('.tab-pane').find('.chat-message-modal');
        }

        if(options.noteRootId) {
            noteRootId = options.noteRootId;
        }

        if(typeof options.getRootId === "function") {
            _getRootId = options.getRootId;
        }

        if(options.canResendEmail) {
            canResendEmail = options.canResendEmail
        }

        if(options.canReplyChat) {
            canReplyChat = options.canReplyChat;
        }

    }

    editMode = !($caseNote.hasClass('no-edit-mode'));

    var phoneEditPrototype = $caseNote.attr('data-prototype-phone-content-edit');

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    /**
     * Obliczanie wysokości prawego panelu z notatkami
     */
    function recalculateHeightNotifications() {

        var height;

        if($notificationsOuterWrapper.find('.notes-resizer.full-width').length > 0) {
            height = $('#page-main-content').height();
        }
        else {

            var additionalCaseInfoHeight = ($additionalCaseInfo.is(":visible")) ? $additionalCaseInfo.outerHeight(true) : 0;
            height = $pageContainer.height() - ( $('#case-info').outerHeight(true) ) - ( additionalCaseInfoHeight );
            $notificationWrapper.css('max-height', height);
        }

        // if($noteInboxEditor.find('.full-mode').length === 0) {
        //     top = $('.notifications-outer-wrapper').position().top + $noteTypes.outerHeight(true);
        // }
        //
        // $noteInboxEditor.css({
        //     height: ($('#page-main-content').height() - top)
        // });
        //
        $notificationsOuterWrapper.css('height', height);

    }

    function isOperationalDashboard() {
        return $body.hasClass('operational-page');
    }

    function recalculateHeightCustomAccordion() {

        var height = $notificationsOuterWrapper.height() - ($noteTypes.height() + 20);
        $customAccordion.css('max-height', height);

    }

    function eventsHandle() {

        $('body').on('click','.spoiler_trigger',(function () {
            $(this).siblings('.hidden-text').toggleClass('hidden');
            var oldText = $(this).text();
            var newText = $(this).data('text');
            $(this).text(newText).data('text',oldText);
        }));

        funResizeHandle.addFunction('recalculateHeightCustomAccordion', recalculateHeightCustomAccordion);
        funResizeHandle.addFunction('recalculateHeightNotifications', recalculateHeightNotifications);

        $body.on('recalculate-case-note-height', function () {
            recalculateHeightCustomAccordion();
            recalculateHeightNotifications();
        });

        $notificationForm.on('click', '.widget-instant-call', function (e) {

            e.preventDefault();

            var $t = $(this);
            $t.prop('disabled', true);

            var number = $t.parent().find('> input').val();

            if (!number) toastr.info("", "Numer jest pusty", {timeOut: 3000});

            var cEvent = new CustomEvent("telephony-call-number", {
                'detail': {
                    number: number
                }
            });

            parent.document.dispatchEvent(cEvent);

            setTimeout(function () {
                $t.prop('disabled', false);
            }, 2000);

        });

        if(canResendEmail) {
            $caseNote.on('click', '.resend-email', onResendEmail);
        }


        $('.notes-resizer').on('click','.fa',function (ev) {
            var $t = $('.notes-resizer');

            var top = 0,
                height = 200;

            var $acc =$('#customAccordion');
            var $resizerUp = $('.fa.fa-chevron-up');

            if($t.hasClass('full-width')) {
                $acc.removeClass('full-width');
                $t.removeClass('full-width');
                $resizerUp.css('display', '');

                top = $('.notifications-outer-wrapper').position().top + $noteTypes.outerHeight(true);
                height = ($('#page-main-content').height() - top);

                $('.notifications-outer-wrapper').css({
                    position:'relative',
                    left: 'unset',
                    top: 'unset',
                    height: height,
                    'overflow-y':'unset',
                    'z-index':'unset'
                })

            }
            else {
                $t.addClass('full-width');
                $acc.addClass('full-width');
                $resizerUp.hide();
                height = ($('#page-main-content').height() - top);

                $('.notifications-outer-wrapper').css({
                    left: -($('.form-placement').width() + 15),
                    top: 0,
                    right:0,
                    height: height,
                    position:'absolute',
                    'overflow-y':'scroll',
                    'z-index':65
                })
            }

        });

        $('.notes-resizer-up').on('click', '.fa', function () {
            var $t = $('.notes-resizer-up');
            var $resizer = $('.fa.fa-expand');

            if ($t.hasClass('full-width')) {
                $t.removeClass('full-width');
                $resizer.css('display', '');
                $caseInfo.show();
                $additionalCaseInfo.show();
                recalculateHeightNotifications();
            } else {
                $t.addClass('full-width');
                $resizer.hide();
                $caseInfo.hide();
                $additionalCaseInfo.hide();
                $notificationsOuterWrapper.css('height', 'auto');
            }
        });

        $customAccordion.on('click', '.notes-view', function () {
            var $url = Routing.generate('operationalDashboardInterfaceGetNotesView', {processInstanceId: _dashboardService.getCurrentTicket('id')});

            window.open($url, '', 'toolbar=no,location=no,statusbar=no');

        });

        $notesFiltersForm.find('select').on('select2:select',function(){
            var type = $notesFiltersType.val();
            var direction = $notesFiltersDirection.val();
            var system = $notesFiltersSystem.val();
            _filterShowedCases(type, direction, system);
        });

        window.addEventListener('message', function(e) {

            if(e.data && e.data.action) {

                if(e.data.action === "add-note") {

                    _appendNoteToList(e.data.note, e.data.position, e.data.id, e.data.opts);

                }

            }

        } , false);

        if($addChatMessage) {
            $addChatMessage.on('click', onOpenModalNewChatMessage);
            $chatMessageModal.find('.submit-add-note').on('click', onSubmitChatMessageForm);
        }

    }

    function onSubmitChatMessageForm(e) {

        e.preventDefault();

        var $t = $(this),
            ladda = Ladda.create($t[0]);

        ladda.start();

        var formData = new FormData($chatMessageModal.find('form')[0]),
            message = formData.get("note[content]");

        _addNote("chat", message, null, function () {

            ladda.stop();

        }, function () {

            $chatMessageModal.find('form')[0].reset();
            $chatMessageModal.modal('hide');

            var isSpecialRequest = formData.get('note[special_request]');

            if(isSpecialRequest) {
                $.ajax({
                    url: Routing.generate('ccv_send_special_request'),
                    data: {
                        content: message,
                        rootId: _getRootId()
                    },
                    type: "POST"
                });
            }

        }, function () {

            toastr.error('Wystąpił błąd przy dodawaniu wiadomości.');
            ladda.stop();

        }, { direction: 2 });

    }

    function onOpenModalNewChatMessage() {

        $chatMessageModal.find('form')[0].reset();
        $chatMessageModal.modal('show');

    }

    function onResendEmail(e) {

        if(e.originalEvent) {

            var $t = $(this),
                noteId = $t.attr('data-note-id');

            if(typeof _f.openModal === "function") {

                var dfd = jQuery.Deferred();

                _f.openModal({
                    header: 'Edycja odbiorcy i ponowne wysłanie e-mail',
                    body: ''
                }, function ($modal, $btn) {

                    var $form = $modal.find('form[name=resend_email]'),
                        formData = $form.serialize(),
                        ladda = Ladda.create($btn[0]),
                        isValid = true;


                    $form.find('input[type="email"]').each(function (i, ele) {

                        if(!validateEmail(ele.value)) {
                            isValid = false;
                            $form.find('.errors').show().html('Adres e-mail jest niepoprawny');
                        }

                    });

                    if(!isValid) return;

                    $form.find('.errors').hide().html('');

                    ladda.start();
                    $form.find('input').prop('disabled', true);

                    $.ajax({
                        url: Routing.generate('mailboxSaveAndSendEmail', {
                            noteId: noteId
                        }),
                        data: formData,
                        type: "POST",
                        success: function (result) {

                            if(result.success) {
                                _replaceNoteInList(result.note);
                            }

                        },
                        error: function (error) {
                            if(error.statusText !== "abort") {
                                handleAjaxResponseError(error);
                            }
                        },
                        complete: function () {
                            ladda.stop();
                            $modal.modal('hide');
                        }
                    });

                }, function () {

                }, false, dfd,  {
                    'buttonConfirm' : {'html' : '<i class="fa fa-check"></i> Zapisz i wyślij ponownie'},
                    'buttonCancel' : {'html' : '<i class="fa fa-times"></i> Anuluj'},
                    hideAfterConfirm: false
                });

                $.ajax({
                    url: Routing.generate('mailboxRenderPreviewForResendEmail', {
                        noteId: noteId
                    }),
                    type: "GET",
                    success: function (result) {

                        dfd.resolve(result.html);

                    }
                });

            }

        }
        else {

        }

    }

    function _filterShowedCases(type, direction, system) {

        var $notes = $caseNote.find('.case-note');

        if($notes.length) {
            $.each($notes, function (index, note) {
                note = $(note);
                $(note).removeClass('hidden');
                if (direction != -1) {
                    if (note.data().direction != direction) {
                        note.addClass('hidden');
                    }
                }
                if (system != -1) {

                    if (note.data().system != system) {
                        note.addClass('hidden');
                    }
                }
                if (type != -1) {

                    if (note.data().type !== type) {
                        note.addClass('hidden');
                    }
                }
            });
        }

    }

    function _noteHtml(id, note, directionClass, headerText, extraContent, direction) {

        var $specialData = '';

        if (note.special_data !== null) {
            $specialData = '<i class="absolute-icon medical-data fa fa-plus-square' + ((note.special_data === "0") ? '-o' : '') + ' fa-lg ' + ((note.status === "error") ? 'medical-data-left' : 'medical-data-right') + '"></i>';
        }

        return '' +
            '<div class="case-note '+ (note.special > 0 ? 'special ' : '') + note.icon + ' m-b-1 ' + (note.status === "error" ? 'error-note ' : '') + '" data-original-title="' + note.title + '" ' +
            'data-id="' + id + '"' +
            ' data-special="'+note.special+'"' +
            ' data-type="'+note.type+'"' +
            ' data-system="'+note.system+'"' +
            ' data-direction="'+note.direction+'"' +
            ' data-updated-by-id="'+note.updated_by_id+'"' +
            ' data-value="'+note.value+'"' +
            ' data-chat-sender="' + ((note.chat_sender) ? note.chat_sender : '' ) + '"' +

            '>' +
                '<h4 class="note-title title ' + direction + '">'+
                    $specialData +
                    ((note.status === "error")  ? '<i class="font-red absolute-icon fa fa-exclamation fa-lg"></i>' : '')  +
                    '<i class="absolute-icon fa ' + directionClass + ' fa-lg"></i>' +
                    '<i class="absolute-icon fa fa-' + note.icon + ' fa-lg"></i> ' +
                    '<div class="header-text visible">' + note.title + '</div>' +
                    '<div class="date-and-time">' + note.title + '</div>' +
                '</h4>' +
                '<div class="note-content description m-t-0 clearfix" style="display: none;">' +
                    '<div class="actions-content">'+ extraContent +'</div>' +
                    '<div class="box-content">' + parseContent(note.content) + '</div>' +
                '</div>' +
            '</div>';

    }
    function parseContent(noteContent) {
        var regex  = /\[\w*][^]+\[\/\w*\]/gm;
        
        var matches = noteContent.match(regex);
        if(matches) {

            for (var iter = 0; iter < matches.length; iter++) {
                var tagText = matches[iter];
                noteContent = noteContent.replace(tagText, parseTag(tagText));
            }
        }

        /** Dodanie <br> do notatek */
        noteContent = noteContent.replace(/(?:\r\n|\r|\n)/g, '<br>');

        return noteContent;
    }

    function parseTag(tagFull) {
        var regexValue  = /\[\w*][^]+\[\/\w*\]/gm;
        var regexTag = /\[(\w*)]/gm;
        var tagValue = regexValue.exec(tagFull);
        regexTag = regexTag.exec(tagFull);
        if(regexTag.length){
            regexTag = regexTag[1];
        }else{
         regexTag = false;
        }
        if(tagValue.length){
            tagValue = tagValue[0];
        }
        tagValue = tagValue.replace('['+regexTag+']','').replace('[/'+regexTag+']','')
        switch (regexTag) {
            case 'spoiler':
                return spoiler(tagValue);
            case 'tag':
                return spoiler(tagValue);
            case 'more':
                return spoiler(tagValue);
            default:
                return tagValue;

        }
        return tagValue;
    }

    function spoiler(value) {

        var result = '';
        result+='<div class="spoiler">' +
            '<span class="m-t-1 btn btn-default btn-xs spoiler_trigger" data-text="Ukryj szczegóły">' +
            'Pokaż szczegóły'+
            '</span>' +
            '<div class="hidden-text hidden">' + value+
            '</div>' +
            '</div>';
        return result;


    }

    function _loadNoteTemplates() {

        $.ajax({
            url: Routing.generate('operation_dashboard_note_templates'),
            type: "GET",
            success: function (result) {
                smsTemplates = result.sms_templates;
                noteTemplates = result.note_templates;
            },
            error: function (error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            }
        });

    }

    function split(val) {
        return val.split(/\/\/\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }

    function _keyDownCustomTemplate(event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    }

    function _chooseTemplates(type) {

        if(type === 'sms') {
            return smsTemplates;
        }

        return noteTemplates;
    }

    function init() {
        $body = $('body');

        $notesFiltersType = $notesFiltersForm.find('.notesFiltersType');
        $notesFiltersDirection = $notesFiltersForm.find('.notesFiltersDirection');
        $notesFiltersSystem = $notesFiltersForm.find('.notesFiltersSystem');

        if($body.hasClass('public-mode')) return false;

        eventsHandle();
        _recalculateHeight();
        _loadNoteTemplates();
    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _getNoteHtml(note, id) {

        var direction = (parseInt(note.direction) === 2) ? 'in' : 'out',
            directionClass = '';

        if(isOperationalDashboard() || note.type !== "chat") {
            directionClass = (direction === 'out') ? 'fa-external-link-square direction direction-out' : 'fa-external-link-square fa-rotate-180 direction direction-in';
        }
        else if(note.type === "chat") {
            directionClass = (direction === 'in') ? 'fa-external-link-square direction direction-out' : 'fa-external-link-square fa-rotate-180 direction direction-in';
        }

        var headerText = '';
        var extraContent = '';

        if(note.type === "email") {
            headerText = 'Temat: ' + note.subject;
            extraContent = _getNoteExtraContent(note, id);
        }
        else if(note.type === "chat" && canReplyChat && direction === 'in') {
            extraContent = _getChatReplyBtn(note);
            headerText = note.content;
        }
        else {
            headerText = note.content;
        }

        return _noteHtml(id, note, directionClass, headerText, extraContent, direction)

    }

    function _getChatReplyBtn(note) {
        return iconOfChatReply.replace('__CHAT_SENDER__', note.chat_sender);
    }

    function _getNoteExtraContent(note, noteId) {

        var noteId = noteId.toString().replace('_','');
        var attachment = note.attachment,
            status = note.status,
            extraContent = '';

        if(attachment === "0") attachment = null;

        if(attachment) {
            extraContent = iconOfPreviewEmail.replace('__NOTE_ID__', noteId).replace('__ATTACHMENT_ID__', attachment);
        }
        else {

            if(status === "error") {

                /**
                 * Prawdopodobnie nie udało się znaleźć adresata - przez co e-mail się nie wysłał
                 */

                if((note.value === null || note.value === '')) {

                    extraContent = labelOfEmailStatus.replace('__STATUS_LABEL__', 'warning')
                        .replace('__STATUS_NAME__', 'Nie udało się wysłać - brak odbiorcy')
                        .replace('__STATUS__', 'warning');

                    if(canResendEmail) {
                        extraContent = (
                            reSendEmailBtn.replace('__ATTR_NOTE_ID__', noteId)
                        ) + extraContent;
                    }

                }
                else if(note.type === "email" && !validateEmail(note.value)) {

                    extraContent = labelOfEmailStatus.replace('__STATUS_LABEL__', 'warning')
                        .replace('__STATUS_NAME__', 'Nie udało się wysłać - niepoprawny odbiorca')
                        .replace('__STATUS__', 'warning');

                    if(canResendEmail) {
                        extraContent = (
                            reSendEmailBtn.replace('__ATTR_NOTE_ID__', noteId)
                        ) + extraContent;
                    }

                }
                else {

                    extraContent = labelOfEmailStatus.replace('__STATUS_LABEL__', (status === "pending") ? 'info' : 'danger')
                        .replace('__STATUS_NAME__', 'Problem z wysłaniem')
                        .replace('__STATUS__', status);

                }

            }
            else if(status === "pending") {
                extraContent = labelOfEmailStatus.replace('__STATUS_LABEL__', (status === "pending") ? 'info' : 'danger')
                    .replace('__STATUS_NAME__', 'W trakcie wysyłania...' )
                    .replace('__STATUS__', status);
            }

        }

        return extraContent;
    }


    function _recalculateHeight() {


        if(typeof CustomEvent !== 'undefined') {

            var event;
            if(typeof(Event) === 'function') {
                event = new CustomEvent('recalculate-case-note-height');
            }else{
                event = document.createEvent('Event');
                event.initEvent('recalculate-case-note-height', true, true);
            }

            window.document.body.dispatchEvent(event);

        }
    }

    function _switchNoteTemplate(noteType) {

        var $textarea = $notificationForm.find("textarea.custom-template");

        $textarea.off("keydown", _keyDownCustomTemplate);

        if($textarea.autocomplete( "instance" )) {
            $textarea.autocomplete("destroy");
        }

        _setNoteTemplate(noteType);

    }

    function _setNoteTemplate(noteType) {

        $notificationForm.find("textarea.custom-template")
            .on("keydown", _keyDownCustomTemplate)
            .autocomplete({
                minLength: 2,
                source: function (request, response) {
                    response($.ui.autocomplete.filter((_chooseTemplates(noteType)), extractLast(request.term), true));
                },
                focus: function () {
                    return false;
                },
                select: function (event, ui) {
                    var terms = split(this.value);
                    terms.pop();
                    terms.push(ui.item.value);
                    terms.push("");
                    this.value = terms.join("");
                    return false;
                }
            });

    }

    function _refreshBookNumbers(numbers) {

        bookNumbers = numbers;
        _setNumberAutoComplete();

    }

    function _focusSearch() {

        $(this).autocomplete("search", '');
        $(this).autocomplete("widget").show();

    }

    function _setNumberAutoComplete() {

        var $numberInput = $notificationForm.find('input[name="number"]');

        if($numberInput.autocomplete( "instance" )) {
            $numberInput.autocomplete("destroy");
            $numberInput.off('focus', _focusSearch);
        }

        $numberInput.autocomplete({
            minLength: 0,
            source: function (request, response) {

                if(request.term === "") {
                    response(bookNumbers);
                }
                else {
                    response(bookNumbers.filter(function (number) {
                        return ((number.value.indexOf(request.term) !== -1) || (number.label.toLowerCase().indexOf(request.term) !== -1));
                    }));
                }

            }
        });

        $numberInput.on('focus', _focusSearch);

    }

    function _hideNoteForm() {

        $noteFormWrapper.removeClass('expanded');

        if ($notificationTypeWrapperButton.hasClass('chosen')) {
            $notificationTypeWrapperButton.removeClass('chosen').parent().removeClass('active');
        }

        $noteFormWrapper.hide();
        _recalculateHeight();

    }

    function _initNoteForm() {


        window.document.addEventListener("out-coming-connection", function (e) {

            if (window.currentProcess && e.detail && e.detail.number) {

                _addNote("phone", _getCallText(1), e.detail.number, null, function () {
                    _hideNoteForm();
                }, null, { direction: 1 });

            }

        });

        window.document.addEventListener("conversationEnded", _onConversationEnded);

        $notificationFormWrapper.addClass('active');

        $notificationTypeWrapperButton.on('click', function () {

            var $t = $(this);
            var noteType = $t.attr('data-notification-type');
            var type = noteType === 'notes' ? 'text' : noteType;

            if(noteType === "email") {
                _hideNoteForm();
                inboxEditorModule.showInboxEditor();
            }
            else {
                inboxEditorModule.hideInboxEditor();

                _showNoteForm(type, $t, function () {
                    _recalculateHeight();
                });

                // loadNotificationForm(type, $t, function () {
                //     caseNoteModule.recalculateHeight();
                // });

            }

            return false;
        });

        $closeFormButton.on('click', function () {
            _hideNoteForm();
            return false;
        });

        $caseNote.on('submit', '.update-phone-note-form', function (e) {
           e.preventDefault();

            var $form = $(this);

            var ladda = Ladda.create($form.find('button')[0]);
            ladda.start();

            $.ajax({
                url: $form.attr('action'),
                type: "POST",
                data: $form.serialize(),
                success: function (response) {

                    var $boxContent = $form.closest('.box-content');
                    $boxContent.closest('.case-note.phone').find('.note-title').removeClass('alerting-bg');
                    $boxContent.html(parseContent(response.note.content));

                },
                error: function (error) {
                    handleAjaxResponseError(error);
                },
                complete: function () {
                    ladda.stop();
                }
            });

        });

        $('#addNotification').on('submit', function (e) {
            e.preventDefault();

            var $form = $(this);

            var laddaNotification = Ladda.create($form.find('.js-submit-button')[0]);
            laddaNotification.start();

            var formData = {};

            $.each($(this).serializeArray(), function(i, ele){
                formData[ele.name] = ele.value;
            });

            _addNote(formData.type, formData.content, formData.number, function () {

                laddaNotification.stop();

            }, function () {

                $form[0].reset();
                _hideNoteForm();
                toastr.info("", "Zapisano notatkę", {timeOut: 3000});

            }, function () {

                toastr.error("", "Błąd zapisywania notatki.", {timeOut: 3000});

            },{
                email: formData['to']
            },formData.special);

        });

    }

    function _onConversationEnded(e) {

        // Powiadomienie, że zakończono rozmowę

        if($body.hasClass('operational-page') && window.currentProcess) {

            // Sprawdzenie czy istnieje jakaś nie zapisana notatka

            var $ownPhoneNotes = $('.case-note.phone[data-value="' + e.detail.data.ringingNumber + '"][data-updated-by-id="' + _userId + '"]');
            var $ele;

            $.each($ownPhoneNotes, function (i, ele) {

                $ele = $(ele);

                if($ele.find("form.update-phone-note-form").length > 0) {

                    _notifyAboutNote($ele, e.detail.data);

                    return false;
                }

            });

        }

    }

    function _notifyAboutNote($note, data) {

        var notificationOptions = {
            timeOut: 30000,
            toastClass: 'toast main-notification',
            closeOnHover: false,
            tapToDismiss: false,
            hideEasing: 'linear',
            hideDuration: 400,
            closeClass: 'toast-close-button',
            closeHtml: '<button><i class="fa fa-close"></i></button>',
            closeButton: true,
            progressBar: true
        };

        toastr.info('Właśnie zakończono połączenie z numerem <b>' + data.ringingNumber + '</b>. Nie zapomnij uzupełnić notatki z rozmowy!', 'Uzupełnij notatkę!', notificationOptions);

        $note.find('.note-title').addClass('alerting-bg');

    }

    function _checkEditableNotes() {

        if(typeof _userId !== "undefined" && _userId) {

            var $ownPhoneNotes = $('.case-note.phone[data-updated-by-id="' + _userId + '"]');
            var $ele, $lastNote = null;

            $.each($ownPhoneNotes, function (i, ele) {

                $ele = $(ele);

                if($ele.find("form.update-phone-note-form").length > 0) {

                    $lastNote = $ele;
                    return false;

                }

            });

            if($lastNote !== null && $lastNote.length) {
                return {
                    noteId: $lastNote.attr('data-id'),
                    value: $lastNote.attr('data-value')
                }
            }

        }

        return null;
    }

    function _changeNoteTypesActivity($ele) {

        var noteType = $ele.attr('data-notification-type');

        $notificationTypeWrapperButton.removeClass('chosen');
        $ele.addClass('chosen').parent().addClass('active');

        $notificationForm.find('.form-group').hide();
        $notificationForm.find('.form-group[data-notification-type*="' + noteType + '"]').show();
        $notificationForm.removeClass().addClass(noteType);

        var newFormContainerClass = $ele.attr('class').split(' ')[1];
        $ele.parent().next().removeClass().addClass("add-notification-form-wrapper expanded " + newFormContainerClass);

        $notificationForm.show();
        $noteFormWrapper.show();
        $noteFormWrapper.addClass('expanded');

        _recalculateHeight();
        _switchNoteTemplate(noteType);

    }

    function _showNoteForm(type, $ele, callback) {

        /** Zmiana typu notatki */
        $('#addNotification #406-226-229').val(type);

        if (!$noteFormWrapper.hasClass('expanded') || type === 'phone' || type === 'sms') {

            $.ajax({
                url: Routing.generate('case_ajax_get_book_numbers', {
                    'rootProcessId': _dashboardService.getCurrentTicket('rootId')
                }),
                type: "GET",
                success: function (response) {
                    _refreshBookNumbers(response.bookNumbers);
                }
            });

        }

        _changeNoteTypesActivity($ele);

        if (typeof callback === "function") {
            callback();
        }

    }

    function _replaceNoteInList(newNote, noteIdToReplace) {

        noteIdToReplace = (typeof noteIdToReplace === "undefined") ? newNote.id : noteIdToReplace;

        var html = _getNoteHtml(newNote, newNote.id),
            $noteForReplace = $caseNote.find('.case-note[data-id="' + noteIdToReplace + '"]');

        if($noteForReplace.length) {
            $noteForReplace.replaceWith(html);
        }

        _allowEditPhoneNote(newNote, newNote.id);

    }

    function _appendNoteToList(note, position, id, opts) {

        if($caseNote.length === 0) {
            $caseNote = $('#case-notes', window.parent.frames[0].document);
        }

        id = id || note.id;
        var withPriority = true;

        if(opts) {
            withPriority = (typeof opts.withPriority !== "undefined") ? opts.withPriority : true;
        }

        if (position === 'prepend') {
            $caseNote.prepend(_getNoteHtml(note,id));
        }
        else {
            $caseNote.append(_getNoteHtml(note, id));
        }

        _allowEditPhoneNote(note, id);

        if(opts && opts.openAfterAdd || parseInt(note.special)) {
            $caseNote.find('.case-note[data-id="'+id+'"] .note-title').trigger('click');
        }

    }

    function _refreshNoteList(noteList) {
        if($caseNote.length === 0) {
            $caseNote = $('#case-notes', window.parent.frames[0].document);
        }

        $caseNote.empty();

        $.each(noteList, function (key, note) {
           if (note.content) {
               $caseNote.prepend(_getNoteHtml(note, key));
           }
        });
    }

    function _refreshNoteListWindow(noteList) {
        if($caseNote.length === 0) {
            $caseNote = $('#case-notes');
        }

        $.each(noteList, function (key, note) {
            if (note.content) {
                $caseNote.prepend(_getNoteHtml(note, key));
            }
        });
    }

    function _allowEditPhoneNote(note, id) {

        if(typeof phoneEditPrototype === "undefined") return false;

        if(note.type === "phone" && note.content.indexOf('data-for-edit="true"') !== -1 && editMode) {

            var $note = $caseNote.find('.case-note[data-id="'+id+'"]'),
                $content = $note.find('.box-content span[data-for-edit="true"]'),
                value = $content.text();

            var phoneEdit = phoneEditPrototype.replace(/__ID__/g, id).replace('__DIRECTION__', note.direction);
            $content.replaceWith(phoneEdit);
            $note.find('form textarea').val(value);

        }

    }

    function _addNote(type, content, numberPhone, cbAlways, cbSuccess, cbError, extraData, special) {

        var _noteRootId,
            _groupProcessId;

        if(noteRootId) {
            _noteRootId = noteRootId;
        }
        else {
            _noteRootId = $('.notification-module').attr('data-notes-root-id');
        }

        if(_dashboardService) {
            _groupProcessId =  _dashboardService.getCurrentTicket('groupProcessId');
        }
        else if(typeof _getRootId === "function") {
            _groupProcessId = _getRootId();
        }

        $.ajax({
            url: Routing.generate('case_new_note', {rootNoteId: _noteRootId, groupProcessId: _groupProcessId}),
            type: "POST",
            data: {
                'content': content,
                'type': type,
                'number': numberPhone || null,
                'email': (extraData && extraData.email) ? extraData.email : null,
                'extraData': extraData,
                'special': special ||null
            },
            success: function (data) {

                if(data.note) {
                    _appendNoteToList(data.note, 'prepend', data.note.id, {openAfterAdd: true});
                }

                if (typeof cbSuccess === "function") {
                    cbSuccess(data);
                }

            },
            error: function (error) {

                if (typeof cbError === "function") {
                    cbError(error);
                }
                else {
                    handleAjaxResponseError(error);
                }

            },
            complete: cbAlways
        })

    }

    function _getCallText(type) {

        type = (typeof type === "undefined") ? 1 : type;

        if(type === 1) {
            return '<span data-for-edit="true">Połączenie wychodzące.</span>';
        }
        else if(type === 2) {
            return '<span data-for-edit="true">Połączenie przychodzące.</span>';
        }

    }

    function _setChatType(contacts, disableChatSelect) {

        contacts = (typeof contacts === "undefined") ? [] : contacts;

        var $select = $noteTypes.find('[data-notification-type="chat"] select');

        if(disableChatSelect) {
            $select.addClass('readonly-select');
        }
        else {
            $select.removeClass('readonly-select');
        }

        if(contacts.length) {
            $select.select2('destroy').empty().select2({data: contacts}).select2('');
            $notificationFormWrapper.addClass('enable-chat');
        }
        else {
            $noteTypes.find('[data-notification-type="chat"] select').select2('destroy').empty().select2('');
            $notificationFormWrapper.removeClass('enable-chat');
        }

    }

    init();

    return {
        recalculateHeight: _recalculateHeight,
        getNoteHtml: _getNoteHtml,
        getNoteExtraContent: _getNoteExtraContent,
        setNoteTemplate: _setNoteTemplate,
        switchNoteTemplate: _switchNoteTemplate,
        refreshBookNumbers: _refreshBookNumbers,
        setNumberAutoComplete: _setNumberAutoComplete,
        setChatType: _setChatType,
        showNoteForm: _showNoteForm,
        initNoteForm: _initNoteForm,
        hideNoteForm: _hideNoteForm,
        appendNote: _appendNoteToList,
        addNote: _addNote,
        checkEditableNotes: _checkEditableNotes,
        getCallText: _getCallText,
        refreshNoteList: _refreshNoteList,
        refreshNoteListWindow: _refreshNoteListWindow
    }

});
