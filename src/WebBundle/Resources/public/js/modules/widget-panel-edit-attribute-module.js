var WidgetPanelEditAttributeModule = (function ($formContainer, _socket, options) {

    var $widgetPanel,
        $attributeTree,
        $attributeView,
        prototypeTreeRow,
        groupProcessId,
        $nameOfAttribute,
        spinnerHtml,
        xhrRequest = null,
        afterLoadAttr = null;

    var _onControlsLoad = options.onControlsLoad;

    /** PUBLIC */


    /** PRIVATE */

    var _init = function ($formContainer) {

        $widgetPanel = $formContainer.find('.panel-edit-attribute');
        $attributeTree = $widgetPanel.find('.panel-edit-attribute-tree');
        groupProcessId = parseInt($widgetPanel.attr('data-group-process-id'));
        prototypeTreeRow = $attributeTree.attr('data-prototype-tree-row');
        $attributeView = $widgetPanel.find('.panel-edit-attribute-view');
        // $nameOfAttribute = $widgetPanel.find('.name-of-attribute');
        spinnerHtml = $widgetPanel.attr('data-spinner');

        if ($widgetPanel.length > 1) {
            console.error('Na razie obsługiwany tylko jeden taki panel na formularz!');
            return false;
        }

        _getRoot(function (result) {

            result.forEach(function (ele, i) {

                var treeRow = prototypeTreeRow;
                treeRow = treeRow.replace('__NAME__', ele.name)
                    .replace('__IS_MULTI__', ele.isMulti)
                    .replace(/__ATTR_VAL_ID__/g, ele.id);

                $attributeTree.append(treeRow);

            });

        });

        _handleEvents();

    };

    var _getRoot = function (callback) {

        if (xhrRequest !== null){
            xhrRequest.abort();
        }

        xhrRequest = $.ajax({
            url: Routing.generate('ajax_widget_panel_tree_get_root', {groupProcessId: groupProcessId}),
            type: "GET",
            success: function (result) {
                callback(result);
            },
            error: function (error) {
                handleAjaxResponseError(error);
            }
        });

    };

    var _getAttributesByParentId = function (parentId, callback) {

        if (xhrRequest !== null){
            xhrRequest.abort();
        }

        xhrRequest = $.ajax({
            url: Routing.generate('ajax_widget_panel_tree_get_attributes', {
                groupProcessId: groupProcessId,
                parentId: parentId
            }),
            type: "GET",
            success: function (result) {
                callback(result);
            },
            error: function (error) {
                handleAjaxResponseError(error);
            }
        });

    };

    var _handleEvents = function () {

        $attributeTree.on('click', '.append-multi', function (e) {
            e.stopPropagation();
            e.preventDefault();

            var $t = $(this),
                l = Ladda.create(this),
                parentAttrId = parseInt($t.attr('data-parent-attr-id')),
                attrId = parseInt($t.attr('data-attr-id'));

            l.start();

            _socket.addStructure({
                'parentValueId': parentAttrId,
                'attributeId': attrId
            }, function (result) {

                afterLoadAttr = {
                    action: 'refresh-multi',
                    attrId: attrId,
                    id: parentAttrId
                };

                l.stop();
                l.remove();

                var $parentRow = $t.closest('.panel-edit-attribute-tree-row').parent().closest('.panel-edit-attribute-tree-row').find('> .attribute-row');
                $parentRow.trigger('click');
            });

        });

        $attributeTree.on('click', '.remove-multi', function (e) {
            e.stopPropagation();
            e.preventDefault();

            var $t = $(this),
                l = Ladda.create(this);
            l.start();

            var id = $t.closest('.panel-edit-attribute-tree-row').find('._wrapper-select select').val();

            _socket.deleteAttribute({
                'attributeValueId': parseInt(id)
            }, function (result) {

                toastr.success("Odśwież parenta", "Zostało pomyślnie usunięte.", {timeOut: 5000});

                l.stop();
                l.remove();
            });

        });

        $attributeTree.on('change', '.switch-multi-id', function (e) {

            e.preventDefault();

            var $t = $(this);
            $t.closest('.panel-edit-attribute-tree-row').find('.attribute-row').trigger('click');

        });

        $attributeTree.on('click', '.attribute-row', function (e) {
            e.stopPropagation();

            var $t = $(this),
                $parent = $t.parent(),
                id,
                isMulti = ($parent.attr('data-attr-is-multi') === "true"),
                name = $t.html(),
                $wrapperTree = $parent.find('> ._wrapper-tree');

            $attributeTree.find('.attribute-row.active').removeClass('active');
            $t.addClass('active visited');

            if(isMulti) {
                id = parseInt($parent.find('> ._wrapper-select select').val());
            }
            else {
                id = parseInt($parent.attr('data-attr-value-id'));
            }

            $wrapperTree.html(spinnerHtml);
            $attributeView.html(spinnerHtml);

            _getAttributesByParentId(id, function (result) {

                $wrapperTree.html('');

                result.structure.forEach(function (ele, i) {

                    var treeRow = prototypeTreeRow,
                        $treeRow;
                    treeRow = treeRow.replace('__NAME__', ele.name)
                        .replace('__IS_MULTI__', ele.isMulti)
                        .replace(/__ATTR_VAL_ID__/g, ele.id)
                        .replace(/__PARENT_ID__/g, id)
                        .replace(/__ATTRIBUTE_ID__/g, (ele.attrId) ? ele.attrId : '');

                    $treeRow = $(treeRow);

                    $wrapperTree.append($treeRow);

                    if (ele.isMulti) {

                        var $select = $treeRow.find('._wrapper-select select');

                        ele.multi.forEach(function (ele, i) {
                            var option = '<option value="' + ele.id + '">' + ele.name + '</option>';
                            $select.append(option);
                        });

                        $select.select2({
                            minimumResultsForSearch: -1
                        });

                        $wrapperTree.find('.remove-multi').confirmation({
                            rootSelector: '.btn-multi-remove',
                            btnOkClass: 'btn btn-danger',
                            btnOkIcon: '',
                            btnOkLabel: 'Tak',
                            btnCancelClass: 'btn btn-default',
                            btnCancelIcon: '',
                            btnCancelLabel: 'Nie'
                        });

                    }
                });

                $attributeView.html('');
                // $nameOfAttribute.html(name);

                if(afterLoadAttr !== null) {

                    if(afterLoadAttr.action === 'refresh-multi') {
                        var path = 'button[data-attr-id="'+afterLoadAttr.attrId+'"][data-parent-attr-id="'+afterLoadAttr.id+'"].append-multi';
                        afterLoadAttr = null;
                        $attributeTree.find(path).closest('.panel-edit-attribute-tree-row').find('.attribute-row').trigger('click');
                    }

                }

                result.controls.forEach(function (ele, i) {
                    $attributeView.append(ele.html);
                });

                _onControlsLoad($attributeView);

            })

        });

        $widgetPanel.on('load', function (e) {

        });

    };

    _init($formContainer);

    return {}

});