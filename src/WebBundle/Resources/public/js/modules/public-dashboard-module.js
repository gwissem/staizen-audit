var PublicDashboardModule = (

    /**
     * @param {DashboardSocket} _socket
     * @param {DashboardService} _dashboardService
     * */
        function (_socket, _dashboardService) {

        /*
         * ----------------------------------------------------------------------------------------------------------
         * PRIVATE FUNCTIONS
         * ----------------------------------------------------------------------------------------------------------
         */


        /*
         * ----------------------------------------------------------------------------------------------------------
         * PUBLIC FUNCTIONS
         * ----------------------------------------------------------------------------------------------------------
         */

        /**
         *
         * @param {string} hash
         * @param {function=} onSuccess
         * @param {function=} onError
         * @private
         */
        var _getCase = function (hash, onSuccess, onError) {

            _socket.getTaskByToken(hash,
                function (result) {
                    if (typeof onSuccess === "function") {
                        onSuccess(result);
                    }
                },
                function (error) {
                    if (typeof onError === "function") {
                        onError(error);
                    }
                }
            )

        };

        return {
            getCase: _getCase
        }

    });
