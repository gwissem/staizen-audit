var casePingTimer = (function () {

    /**
     * Skrypt jest stworzony po to, żeby Pingować do serwera ile sekund ktoś siedzi na zadaniu.
     *
     * Po wejściu w zadanie jest odpalane startTimer()
     * W czasie pingowania odpalane jest getPingCounter()
     * Po wyjściu z strony czas jest zatrzymywany
     * Po wejściu na strone czas jest wznawiany
     */

    var counter = 0,
        tempCounter = 0,
        processInstanceId = null,
        isFocus = false,
        interval = null,
        pauseInterval = null;

    function _continueTimer() {

        _clearPauseDelay();

        if(interval === null) {
            interval = setInterval(function () {
                counter++;
            }, 1000);
        }

    }

    function _clearPauseDelay() {
        if(pauseInterval) {
            clearTimeout(pauseInterval);
            pauseInterval = null;
        }
    }

    function _pauseTimer() {

        if(interval) {
            clearInterval(interval);
            interval = null;
        }

    }

    function _stopTimer() {

        counter = 0;
        processInstanceId = null;
        _pauseTimer();

    }

    function _setFocus() {

        isFocus = document.hasFocus() || window.parent.document.hasFocus();

        if(isFocus) {
            _clearPauseDelay();
        }

        if(isFocus && !interval) {
            _continueTimer();
        }
        else if(!isFocus) {
            _pauseTimer();
        }

    }

    function _canPingCounter() {
        return(counter > 0 && processInstanceId !== null);
    }

    function _onReceiveMessage(event) {

        if(!event.data) return;

        if(event.data.action === "focus-email") {
            _continueTimer();
        }
        else if(event.data.action === "blur-email")  {
            pauseInterval = setTimeout(function () {
                _pauseTimer();
            }, 200);
        }

    }

    function init() {

        if(window.parent === window) {

        }
        else {

            isFocus = document.hasFocus() || window.parent.document.hasFocus();
            window.parent.addEventListener("focus", _setFocus);
            window.parent.addEventListener("blur", _setFocus);
            window.addEventListener("focus", _setFocus);
            window.addEventListener("blur", _setFocus);
            window.addEventListener("message", _onReceiveMessage, false);

        }

    }

    init();

    return {
        stopTimer: _stopTimer,
        /** Odpalane przy pobraniu zadania */
        startTimer: function (processId) {
            counter = 0;
            processInstanceId = processId;
            if(isFocus) {
                _continueTimer();
            }
        },
        canPingCounter: _canPingCounter,
        /** Odpalane przy wysyłaniu ping'u */
        getPingCounter: function () {

            if(_canPingCounter()) {
                tempCounter = counter;
                counter = 0;

                return {
                    action: 'ping',
                    processInstanceId: processInstanceId,
                    time: tempCounter
                };
            }

            return {
                action: 'ping'
            };

        }
    }

})();

var DashboardSocketModule = (
    /**
     * @param _sessionDashboard
     * @param _status
     * @param timeLineManager
     * @param {DashboardService} _dashboardService
     * */
    function(_sessionDashboard, _status, timeLineManager, _dashboardService) {

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    var sessionDashboard = _sessionDashboard;

    var enabledCaseTimer = false;

    function checkConnected() {
        if (!sessionDashboard) {
            toastr.error("Nie można nawiązać połączenia z serwerem.", "Błąd połączenia (WebSocket)", {timeOut: 7000});
            throw "No connection with socket";
        }
    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _cancelReserveCurrentTask(data) {

        checkConnected();

        if(sessionDashboard && enabledCaseTimer && casePingTimer.canPingCounter()) {
            sessionDashboard.publish("atlas/task/manager", casePingTimer.getPingCounter());
        }

        casePingTimer.stopTimer();

        sessionDashboard.publish("atlas/task/manager", {
            action: 'cancel_reserved_task',
            only_leave: (typeof data !== "undefined" && typeof data.only_leave !== "undefined") ? data.only_leave : null
        })

    }

    function _tryReserveTask(data, onSuccess, onError) {

        checkConnected();

        if(sessionDashboard && enabledCaseTimer && casePingTimer.canPingCounter()) {
            sessionDashboard.publish("atlas/task/manager", casePingTimer.getPingCounter());
        }

        _status.startLoading();

        sessionDashboard.call("atlas/task/manager/rpc/try_reserve/" + _locale, {
            processInstanceId: data.id,
            forceReserve: data.force || false
        }).then(
            function (result) {
                _status.stopLoading();
                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }
            },
            function (error) {
                console.log("RPC Error", error);
                toastr.error("Opis: " + error.desc, "Błąd rezerwacji zadania", {timeOut: 7000});
                if (typeof onError === "function") {
                    onError(error);
                }
                _status.stopLoading();
            }
        );
    }

    function _getTimeLine(id, onSuccess, onError) {

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceGetTimeLine', {processInstanceId: id}),
            type: "GET",
            success: function (result) {
                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }
            },
            error: function (error) {

                var desc = (error.status + " " + error.statusText);

                toastr.error("Opis: " + desc, "Błąd pobierania zadania", {timeOut: 7000});

                if (typeof onError === "function") {
                    onError(error);
                }

            }
        });

    }

    function _getTask(id, onSuccess, onError, refreshTimeLine) {

        refreshTimeLine = (typeof refreshTimeLine === "undefined") ? true : refreshTimeLine;

        _status.startLoading();

        _dashboardService.setPreviewStep(false);

        var event = {};
        var blog = $('.blog-page');

        if (blog) {
            blog.remove();
        }

        if(typeof id === "object") {
            event = id;
            id = id.id;
        }

        casePingTimer.startTimer(id);

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceGetTask', {processInstanceId: id}),
            type: "GET",
            data: event,
            /**
             * @param {Object} taskResult - Dane zwracane przez Get Task
             *      @param {Object} taskResult.processInfo - Informacje o procesie
             *          @param {int} taskResult.processInfo.id - Id procesu
             *          @param {int} taskResult.processInfo.groupProcessId
             *          @param {int} taskResult.processInfo.rootId
             *          @param {String} taskResult.processInfo.stepId
             *      @param {String} taskResult.html - Html formularza
             *      @param {String} taskResult.type - Typ formularza
             *      @param {int} taskResult.permission - Czy uprawnienia
             *      @param {Object[]} taskResult.controls - Kontrolki na formularzu
             *      @param {Object[]} taskResult.notes - Tablica notatek
             *      @param {String} taskResult.prependHtml - Jakiś HTML :D
             *      @param {Object[]} taskResult.timeline
             *      @param {Object[]} taskResult.caseInfo
             *      @param {String} taskResult.additionalCaseInfo
             */
            success: function (taskResult) {

                window.currentProcessId = id;
                window.currentProcess = taskResult.processInfo;

                sendToParentWindow({
                    type: 'onGetTask'
                });

                if(refreshTimeLine) {
                    timeLineManager.refreshTimeLine(id);
                }
                if (typeof onSuccess === "function") {
                    onSuccess(taskResult);
                }
                _status.stopLoading();
            },
            error: function (error) {

                var desc = (error.status + " " + error.statusText);

                toastr.error("Opis: " + desc, "Błąd pobierania zadania", {timeOut: 7000});

                if (typeof onError === "function") {
                    onError(error);
                }
                _status.stopLoading();

            }
        });

    }

    function _getTaskByToken(token, onSuccess, onError) {

        _status.startLoading();

        var url = '';
        if(typeof _processInstanceId !== "undefined") {
            url = Routing.generate('operationalDashboardInterfaceGetTaskByToken', {token: token, id: _processInstanceId});
        }
        else {
            url = Routing.generate('operationalDashboardInterfaceGetTaskByToken', {token: token});
        }

        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function(xhr){
                xhr.setRequestHeader('Process-Token', _processToken);
            },
            success: function (result) {

                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }

            },
            error: function (error) {

                if (typeof onError === "function") {
                    onError(error);
                }

                if($('body').hasClass('print-mode')) {
                    throw "Error Load Form: " + token;
                }

            },
            complete: function () {
                _status.stopLoading();
            }
        });

    }

    function _getOverview(id, onSuccess, onError) {

        _status.startLoading();

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceGetOverview', {processInstanceId: id}),
            type: "GET",
            success: function (result) {

                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }
                _status.stopLoading();

            },
            error: function (error) {

                var desc = (error.status + " " + error.statusText);

                toastr.error("Opis: " + desc, "Błąd pobierania zadania", {timeOut: 7000});

                if (typeof onError === "function") {
                    onError(error);
                }

                _status.stopLoading();
            }
        });

    }

    function _getNextStep(data, onSuccess, onError) {

        _status.startLoading();

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceNextStep'),
            type: "GET",
            beforeSend: function(xhr){
                if(_dashboardService.isPublicDashboard()) {
                    xhr.setRequestHeader('Process-Token', _processToken);
                }
            },
            data: data,
            success: function (result) {

                _status.stopLoading();
                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }

            },
            error: function (error) {

                var desc = (error.status + " " + error.statusText);

                toastr.error("Opis: " + desc, "Błąd zakończenia zadania", {timeOut: 7000});

                if (typeof onError === "function") {
                    onError(error);
                }
                _status.stopLoading();
            }
        });

    }

    function _addStructure(data, onSuccess, onError) {


        /**
         * data: parentValueId, attributeId
         */

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceAddStructure'),
            type: "POST",
            beforeSend: function(xhr){
                if(typeof _processToken !== "undefined" && _processToken) {
                    xhr.setRequestHeader('Process-Token', _processToken);
                }
            },
            data: data,
            success: function (result) {

                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }

            },
            error: function (error) {

                toastr.error("", "Błąd aplikacji", {timeOut: 7000});
                if (typeof onError === "function") {
                    onError(error);
                }

            }
        });

    }

        /**
         * @param {Object} data
         * @param {Object} data._this
         * @param {String} data.path
         * @param {String} data.groupProcessId
         * @param {String} data.stepId
         * @param {String} data.valueId
         * @param {String} data.value
         * @param {String} data.valueType
         * @param {Object} data.formControl
         * @param {number} data.processInstanceId
         * @param {boolean} data.isPreview
         * @param onSuccess
         * @param onError
         * @private
         */
    function _setAttribute(data, onSuccess, onError) {


        var _changedControl = data._this;
        delete data._this;

        data.processInstanceId = _dashboardService.getCurrentTicket('id');
        data.isPreview = _dashboardService.isPreviewStep();

        _dashboardService.startRequest();

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceSetAttributeValue'),
            type: "POST",
            beforeSend: function(xhr){
                if(typeof _processToken !== "undefined" && _processToken) {
                    xhr.setRequestHeader('Process-Token', _processToken);
                }
            },
            data: data,
            success: function (result) {

                if (typeof onSuccess === "function") {

                    try {
                        onSuccess(result);
                    }
                    catch (e) {

                        console.error(e);

                        if(e.message && e.stack) {
                            $.ajax({
                                url: window.location.origin + "/js-logger",
                                type: "POST",
                                data: {
                                    msg: e.message + '. ' + e.stack,
                                    url: window.location.href,
                                    processId: window.currentProcessId,
                                    browser_url: window.location.href,
                                    browser_version: navigator.sayswho
                                }
                            });
                        }

                    }

                }

                _dashboardService.dispatchDashboardEvent('setAttributeValue', data);

                if(_changedControl && _changedControl.length) {
                    _changedControl.trigger("success-set-attribute", [result]);
                }

            },
            error: function (error) {

                toastr.error("Nie udało się zapisać wartości \"" + data.value + "\"", "Błąd zapisu", {timeOut: 7000});
                if (typeof onError === "function") {

                    try {
                        onError(error);
                    }
                    catch (e) {
                        console.error(e);
                    }

                }

            },
            complete: function () {
                _dashboardService.stopRequest()
            }
        });

    }

    function _deleteAttribute(data, onSuccess, onError) {


        /**
         * data: - attributeValueId
         */

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceRemoveStructure'),
            type: "POST",
            beforeSend: function(xhr){
                if (typeof(_processToken) !== 'undefined') {
                    xhr.setRequestHeader('Process-Token', _processToken);
                }
            },
            data: data,
            success: function (result) {
                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }
            },
            error: function (error) {
                toastr.error("Usuwanie nie powiodło się.", "Błąd aplikacji", {timeOut: 7000});
                if (typeof onError === "function") {
                    onError(error);
                }
            }
        });

    }

    function _sortAttribute(data, onSuccess, onError) {

        /**
         * data: - attributeValueId, move
         */

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceSortAttribute'),
            type: "POST",
            beforeSend: function(xhr){
                if (typeof(_processToken) !== 'undefined') {
                    xhr.setRequestHeader('Process-Token', _processToken);
                }
            },
            data: data,
            success: function (result) {
                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }
            },
            error: function (error) {

                toastr.error("Sortowanie nie powiodło się.", "Błąd aplikacji", {timeOut: 7000});
                if (typeof onError === "function") {
                    onError(error);
                }

            }
        });

    }

    /**
     *
     * @param {Object} query
     * @param {String} query.name
     * @param {Array} query.parameters
     * @param {Boolean} query.fetch
     * @param {Boolean} query.parser
     * @param onSuccess
     * @param onError
     * @param onComplete
     * @private
     */
    function _execProcedure(query, onSuccess, onError, onComplete) {

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceExecProcedure'),
            type: "POST",
            beforeSend: function(xhr){
                if (typeof(_processToken) !== 'undefined') {
                    xhr.setRequestHeader('Process-Token', _processToken);
                }
            },
            data: {
                query: query,
                processInstanceId: _dashboardService.getCurrentTicket('id')
            },
            success: function (result) {
                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }
            },
            error: function (error) {
                toastr.error("Błąd wykonania procedury.", "Błąd aplikacji", {timeOut: 7000});
                if (typeof onError === "function") {
                    onError(error);
                }
            },
            onComplete: function () {
                if (typeof onComplete === "function") {
                    onComplete();
                }
            }

        });

    }

    function _getAttribute(data, onSuccess, onError) {

        /**
         * data: - stepId - groupProcessId - path - valueType
         */

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceGetAttributeValue'),
            type: 'POST',
            data: {
                "path": data.path,
                "groupProcessId": data.groupProcessId,
                "stepId": data.stepId,
                "valueType": data.valueType
            },
            beforeSend: function(xhr){
                if(typeof _processToken !== 'undefined' && _processToken) {
                    xhr.setRequestHeader('Process-Token', _processToken);
                }
            },
            success: function (result) {
                if (typeof onSuccess === "function") {
                    onSuccess(result);
                }
            },
            error: function (error) {
                toastr.error("Usuwanie nie powiodło się.", "Błąd aplikacji", {timeOut: 7000});
                if (typeof onError === "function") {
                    onError(error);
                }
            }
        });

    }

    function _setSessionDashboard(_sessionDashboard) {
        sessionDashboard = _sessionDashboard;
    }

    return {

        cancelReserveCurrentTask : _cancelReserveCurrentTask, // Socket

        tryReserveTask: _tryReserveTask, // Socket

        getTimeLine: _getTimeLine, // Ajax

        getTask: _getTask, // Ajax

        getTaskByToken: _getTaskByToken, // Ajax

        getOverview: _getOverview, // Ajax

        getNextStep: _getNextStep, // Ajax

        addStructure: _addStructure, // Socket | Ajax

        setAttribute: _setAttribute, // Socket | Ajax

        deleteAttribute: _deleteAttribute, // Ajax

        sortAttribute: _sortAttribute, // Socket | Ajax

        getAttribute: _getAttribute, // Socket

        setSessionDashboard: _setSessionDashboard,

        execProcedure: _execProcedure,

        enableCaseTimer: function () {
            enabledCaseTimer = true;
        },

        tryStopTimer: function () {

            if(sessionDashboard && enabledCaseTimer) {
                casePingTimer.stopTimer();
            }

        }
    }

});

/**
 *
 * @typedef {{
 * cancelReserveCurrentTask,
 * tryReserveTask, getTimeLine,
 * getTask,
 * getTaskByToken,
 * getTaskByHash,
 * getOverview,
 * getNextStep,
 * addStructure,
 * setAttribute,
 * deleteAttribute,
 * sortAttribute,
 * getAttribute,
 * setSessionDashboard,
 * execProcedure,
 * enableCaseTimer,
 * tryStopTimer }} DashboardSocket
 */

