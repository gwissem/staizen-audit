var ServiceSummaryTooltipModule = (function(options) {

    var $body = $('body'),
        $servicePopup = null,
        allPopovers = null,
        searchString = '.single-service [data-toggle="popover"]',
        allStatusPopovers = null,
        searchStringStatus = '.single-service .status.show-status',
        allActionsPopovers = null,
        searchStringActions = '.single-service .status.actions-step',
        currentTicket = null,
        __nextStep = null,
        __getTask = null,
        $servicesWithStatus = null,
        __dashboardService = null,
        multiContentHtml = null,
        sectionPrototype = '<div class="multi-content-section tab-pane __ACTIVE__" id="tab___ID__">__CONTENT__</div>',
        // sectionPrototype = '<div class="multi-content-section" data-section-service-id="__ID__">__CONTENT__</div>',
        tabPrototype = '<li class="multi-content-tab __ACTIVE__"><a href="#tab___ID__" data-toggle="tab" aria-expanded="true"><span class="service-group __SERVICE_GROUP_CLASS__">__SERVICE_GROUP__</span>__TITLE__</a></li>';
        // tabPrototype = '<div class="multi-content-tab" data-tab-service-id="__ID__">__TITLE__</div>';

    var _initServicePanel = function (_currentTicket) {

        unbindEvents();

        // Nie ma panelu usług
        if(!$(".consultant-services").length) {
            allPopovers = null;
            allStatusPopovers = null;
            return false;
        }

        beforeLoad();

        currentTicket = _currentTicket;
        allPopovers = $(searchString);
        allStatusPopovers = $(searchStringStatus);
        allActionsPopovers = $(searchStringActions);
        $servicePopup = $body.find('#service-popup');

        allPopovers.popover({
            container: "body",
            html: true,
            trigger: "click",
            template: '<div class="popover services-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        }).on('click', function() {
            allPopovers.not(this).popover('hide');
        });

        $('.custom-progress-bar-table').popover({
            container: "body",
            html: true,
            trigger: "hover",
            template: '<div class="popover services-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        }).on('hover', function() {
            $('.custom-progress-bar-table').not(this).popover('hide');
        });

        allActionsPopovers.popover({
            container: "body",
            html: true,
            trigger: "manual",
            template: templateOfStatusPopover('actions')
        }).on('click', function() {
            $(this).popover('show');
        });

        bindEvents();

        // _calculatePopupHeight();
    };

    function getTemplateOfactiveTasks() {
        return '<div class="status show-status list-active-tasks modal-trigger" ' +
            'data-placement="top" title="Zadania do wykonania" ' +
            'data-content="">' +
                '<span class="status-letter loading">' +
                    '<img src="/images/web/images/spinner.gif">' +
                '</span>' +
            '</div>';
    }

    function beforeLoad() {

        $servicesWithStatus = $('.consultant-services .single-service .custom-progress-bar-table').parent();

        if($servicesWithStatus.length) {
            $.each($servicesWithStatus, function () {
                var $t = $(this);
                $t.find('.status-wrapper').append(getTemplateOfactiveTasks());

            });
        }

        var servicesId = [];

        $.each($servicesWithStatus, function (i, ele) {
            var serviceId = parseInt(ele.getAttribute('data-single-service-id'));
            if(!isNaN(serviceId)) {
                servicesId.push(parseInt(ele.getAttribute('data-single-service-id')));
            }
        });

        getServiceStatus(servicesId).then(function (result) {

            $.each(result, function (i, ele) {

                var $service = $('.single-service[data-single-service-id="'+i+'"]'),
                    $listActiveTasks = $service.find('.list-active-tasks');

                if($listActiveTasks.length) {

                    var dataPlacement = ($service.index() <= 2) ? 'bottom' : 'top';

                    $listActiveTasks.addClass('loaded');

                    if(ele.timeout) {
                        $listActiveTasks.addClass('timeout');
                    }

                    $listActiveTasks.attr('data-placement', dataPlacement);
                    $listActiveTasks.attr('data-content', ele.html_content);
                    $listActiveTasks.find('.status-letter').removeClass('loading').html('<i class="fa fa-tasks" aria-hidden="true"></i>');
                }

            });

            $('.consultant-services .status.list-active-tasks').not('.loaded').remove();

            allStatusPopovers.popover({
                container: "body",
                html: true,
                trigger: "manual",
                template: templateOfStatusPopover('status')
            }).on('click', function() {
                $(this).popover('show');
            });

        })

    }

    function getServiceStatus(servicesId) {

        var dfd = jQuery.Deferred();

        $.ajax({
            url: Routing.generate('ODIActiveStepsInServices'),
            type: "POST",
            data: {
                processId: __dashboardService.getCurrentTicket('id'),
                services: servicesId
            },
            success: function (result) {
                dfd.resolve(result);
            },
            complete: function () {
                dfd.reject([]);
            }
        });

        return dfd;

    }

    /** Private */

    function templateOfStatusPopover(type) {

        return '<div class="popover popover-type-'+ type+' services-popover mt-element-list" role="tooltip">' +
            '<div class="arrow"></div>' +
            '<div class="mt-list-head list-default list-simple ext-1 font-white"><div class="list-head-title-container"><h3 class="list-title popover-title"></h3></div></div>' +
            '<div class="mt-list-container list-default list-simple ext-1"><ul class="popover-content"></ul></div>' +
            '</div>';
    }

    function unbindEvents() {

        $body.off('click', '.services-popover .close-popover', _hidePopovers);
        $body.off('click', '.services-popover .mt-list-item[data-process-id]', _getListItem);
        $body.off('click', '.services-popover.popover-type-actions .mt-list-item[data-actions-value]', _getListActionsItem);
        $body.off('hidden.bs.popover', _fixPopover);
        $body.off('click', '.services-popover .activate-service', _onActiveService);
        $body.off('click', '.single-service .extra-program-btn', _onServiceExtraProgram);
        $body.off('click', _hidePopoverOnOuterClick);
        $body.off('click', _hideStatusPopoverOnOuterClick);
        $body.find('.single-service').off('click', '.awaiting', _openServiceOverlay);
        $body.find('.service-custom-popup').off('click', '.close-popup', _closeServiceOverlay);
    }

    function bindEvents() {

        $body.on('click', '.services-popover .close-popover', _hidePopovers);
        $body.on('click', '.services-popover .mt-list-item[data-process-id]', _getListItem);
        $body.on('click', '.services-popover.popover-type-actions .mt-list-item[data-actions-value]', _getListActionsItem);
        $body.on('hidden.bs.popover', _fixPopover);
        $body.on('click', '.services-popover .activate-service', _onActiveService);
        $body.on('click', '.single-service .extra-program-btn', _onServiceExtraProgram);
        $body.on('click', _hidePopoverOnOuterClick);
        $body.on('click', _hideStatusPopoverOnOuterClick);
        $body.find('.single-service').on('click', '.awaiting', _openServiceOverlay);
        $body.find('.service-custom-popup').on('click', '.close-popup', _closeServiceOverlay);

    }

    function _getListActionsItem() {
        var $t = $(this);

        var message = {
            body: $t.find("h5").text(),
            header: 'Potwierdzenie wykonania akcji'
        };

        allActionsPopovers.popover('hide');

        _f.openModal(message, function() {

            var data = JSON.parse($t.attr('data-actions-value'));

            $.ajax({
                url: Routing.generate('operationalDashboardInterfaceActiveAction'),
                type: "POST",
                data: data,
                success: function (response) {

                    if(response.error) {
                        toastr.error(response.message, "", 7000);
                    }
                    else {
                        if(response.message) {
                            toastr.success(response.message, "", 7000);
                        }
                    }

                    if(response.data.callback === 'getTask') {

                        __getTask({
                            id: parseInt(response.id)
                        });

                    }
                    else if (response.data.callback === 'serviceEditor') {
                        window.location.href = Routing.generate('service_editor', {id: response.id});
                    }
                    else {

                        __getTask({
                            id: parseInt(__dashboardService.getCurrentTicket('id'))
                        });

                    }

                }
            });

        }, null);

    }

    function _getListItem() {
        var $t = $(this);
        allStatusPopovers.popover('hide');
        __getTask({
            id: parseInt($t.attr('data-process-id')),
            notificationMode: true
        })
    }

    function _onLoadServiceOverlay(serviceId, processId) {

        var $serviceCustomPopup = $body.find('.service-custom-popup'),
            $content = $serviceCustomPopup.find('.content');
        $serviceCustomPopup.addClass('loading');

        if(multiContentHtml === null) {
            multiContentHtml = $servicePopup.attr('data-multi-content-html');
        }

        $content.html('');

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceOverviewService', {processId: processId, serviceId: serviceId}),
            type: "GET",
            success: function (result) {

                if(result.services) {

                    // if(result.services.length === 1) {
                    //     $content.html(result.services[0].overview);
                    // }
                    // else {

                        $content.append(multiContentHtml);

                        var $multiContentTabs = $content.find('.multi-content-tabs'),
                            $multiContentSections = $content.find('.multi-content-sections'),
                            groupsArr = [],
                            index = 0
                        ;

                        $.each(result.services, function (i, ele) {
                            index++;
                            _appendTab($multiContentTabs, ele, i);
                            _appendSection($multiContentSections, ele, i);

                            if(ele.serviceGroup !== null){
                                var labelText = '';
                                if(Object.values(groupsArr).indexOf(ele.serviceGroup) < 0) {
                                    groupsArr.push(ele.serviceGroup);
                                }
                            }
                            //$content.find('.multi-content-tabs')
                        });
                    // }

                    groupsArr = groupsArr.sort( function ( a, b ) { return a - b; } );

                    $.each(groupsArr,function(key,value){
                        $multiContentTabs.find(".service-group:contains('"+value+"')").text((key+1));
                    });

                    $content.html(result.overview);
                }
                else {
                    $content.html('');
                }

                setTimeout(function () {
                    _calculatePopupHeight();
                }, 10);

            },
            complete: function () {
                $serviceCustomPopup.removeClass('loading');
            }
        });

    }

    // function _getWidthForTab(amount) {
    //     switch (amount) {
    //         case 1: return 100;
    //         case 2: return 50;
    //         case 3: return 33.3;
    //         case 4: return 25;
    //         case 5: return 20;
    //         case 6: return 33.3;
    //         default: return 50;
    //     }
    // }

    function _appendTab($content, ele, i) {

        var newTab = tabPrototype.replace('__ID__', ele.id).replace('__TITLE__', ele.tabName).replace('__ACTIVE__', ((i === 0) ? 'active' : '')).replace('__SERVICE_GROUP__',ele.serviceGroup);
        newTab = ele.serviceGroup === null ? newTab.replace('__SERVICE_GROUP_CLASS__','hidden') : newTab = newTab.replace('__SERVICE_GROUP_CLASS__','');
        $content.append(newTab);
        // $content.find('[data-tab-service-id="'+ele.id+'"]').css('width', widthTab + '%');
    }

    function _appendSection($content, ele, i) {

        var newSection = sectionPrototype.replace('__ID__', ele.id).replace('__CONTENT__', ele.overview).replace('__ACTIVE__', ((i === 0) ? 'active' : ''));
        $content.append(newSection);

    }

    function _openServiceOverlay() {

        var $t = $(this);

        $body.find('#form-generator-box').addClass('inactive');
        var topScroll = $body.find('#form-generator-box')[0].scrollTop;
        $body.find('.custom-service-overlay').addClass('active');
        $body.addClass('popupActive');

        $servicePopup.find('.service-custom-popup').css('top', topScroll);

        _onLoadServiceOverlay(parseInt($t.attr('data-service-id')), parseInt(__dashboardService.getCurrentTicket('id')));

    }

    function _closeServiceOverlay() {
        $body.find('#form-generator-box').removeClass('inactive');
        $body.find('.custom-service-overlay').removeClass('active');
        $body.removeClass('popupActive');
    }

    function _calculatePopupHeight() {

        var $serviceCustomPopup = $servicePopup.find('.service-custom-popup'),
            $content = $serviceCustomPopup.find('.content');

        if($content.find('.multi-content').length) {
            $content.find('.multi-content-section').css('max-height', $serviceCustomPopup.height() - $servicePopup.find('.service-popup-title').height() - $content.find('.nav-tabs').height());
            $content.css('max-height', 'auto');
        }
        else {
            $content.css('max-height', $serviceCustomPopup.height() - $servicePopup.find('.service-popup-title').height());
        }

    }

    function _hidePopovers() {
        if(allPopovers) {
            allPopovers.popover('hide');
        }
    }

    function _fixPopover(e) {
        $(e.target).data("bs.popover").inState.click = false;
    }

    function _hideStatusPopoverOnOuterClick(e) {
        if ( $(e.target).closest('.show-status').length > 0 ) {
            return false;
        }
        if ( $(e.target).closest('.actions-step').length > 0 ) {
            return false;
        }
        if( $(e.target).closest('.services-popover').length === 0 ) {
            allStatusPopovers.popover('hide');
            allActionsPopovers.popover('hide');
        }
    }

    function _hidePopoverOnOuterClick(e) {
        if ( $(e.target).closest('.popover-button').length > 0 ) {
            return false;
        }
        if( $(e.target).closest('.services-popover').length === 0 ) {
            allPopovers.popover('hide');
        }
    }

    function _onActiveService(e) {

        if(!currentTicket) {
            console.error('Brakuje currentTicket!');
            return false;
        }

        var $t = $(this);

        var btn = Ladda.create($t[0]);
        btn.start();

        var serviceElementId = $(this).closest('.services-popover').attr('id');
        var activateBtn = $('.activate-service-btn[aria-describedby="'+serviceElementId+'"]');
        var startStepId = activateBtn.attr('data-start-step');
        var programId = activateBtn.attr('data-programId');

        $.ajax({
            url: Routing.generate('ajax_service_program'),
            type: "POST",
            data: {
                'programId' : programId,
                'startStepId' : startStepId,
                'processInstanceId' : currentTicket.id,
                'groupProcessId' : currentTicket.groupProcessId
            },
            success: function () {
                _hidePopovers();
                if(typeof __nextStep === "function") {
                    __nextStep();   // Optional arguments (variant, omitValidation, skipStepProcedure)
                }
            }
        });

    }

    function _hideAllPopup() {
        $('.popover.services-popover').remove();
    }

    function _onServiceExtraProgram(){
        var startStepId = $(this).attr('data-start-step');
        $.ajax({
            url: Routing.generate('ajax_special_organisation',{'groupProcessId' : currentTicket.groupProcessId}),
            type: "GET",
            success: function (data) {
                if(parseInt(data) ===  0 )
                {
                    $('#service-program-modal #special-organisation').parent().hide();
                }else{
                    $('#service-program-modal #special-organisation').parent().show();
                }
            }
        });

        $.ajax({
            url: Routing.generate('ajax_service_programs',{'groupProcessId' : currentTicket.groupProcessId, 'startStepId' : startStepId}),
            type: "GET",
            success: function (data) {
                var programsSelect = $('#service-program-modal').find('#service-program');
                var relatedGroup = $('#service-program-modal').find('#related-services-group');
                programsSelect.html('');
                relatedGroup.html('');
                $.each(data.programs,function(key, program){
                    programsSelect.append('<option value="'+program.id+'">'+program.name+'</option>');
                });
                if(data.related.length > 0){
                    relatedGroup.html('<label for="related-services">Powiązane z usługą</label><select name="related-services" id="related-services" class="form-control"><option value="">---</option></select>');
                    $.each(data.related,function(key, related){
                        relatedGroup.find('select').append('<option value="'+related.id+'">'+related.name+'</option>');
                    });
                }
            }
        });

        $('#service-program-modal #post-factum-mode').prop('checked',false);
        $('#service-program-modal #special-organisation').prop('checked',false);

        $('#service-program-modal .form-check').show();

        if(["1009.034", "1009.054", "1007.037", "1142.006", "1014.019"].indexOf(startStepId) === -1) {
            $('#service-program-modal .form-check').hide();
        }

        $('#service-program-modal').modal('show');

        $('#service-program-modal #postpone-confirm').unbind('click').on('click', function () {
            var programId = $('#service-program-modal #service-program').val();
            var relatedService = $('#service-program-modal #related-services').val();
            var postFactum = $('#service-program-modal #post-factum-mode').is(':checked');
            var specialOrganisation = $('#service-program-modal #special-organisation').is(':checked');

            $('#service-program-modal').modal('hide');
            $.ajax({
                url: Routing.generate('ajax_service_program'),
                data: {'processInstanceId': currentTicket.id, 'postFactum': postFactum, 'specialOrganisation': specialOrganisation, 'programId': programId, 'startStepId': startStepId, 'relatedService' : relatedService},
                type: "POST",
                success: function () {
                    _hidePopovers();
                    if(typeof __nextStep === "function") {
                        __nextStep();   // Optional arguments (variant, omitValidation, skipStepProcedure)
                    }
                }
            });
            return false;
        });
    }

    var _moveProgressBar = function () {

        // $('.single-service:not(.inactive) .custom-progress-wrap').each(function() {
        //     $(this).attr('data-progress-percent', randomServiceStatus());
        // });

        $('.single-service .custom-progress-bar').each(function() {

            var getPercent = $(this).parent().data('progress-percent') / 100,
                getProgressWrapWidth = $(this).parent().width();

            $(this).animate({
                left: (getPercent * getProgressWrapWidth)
            }, 800);
        })

    };

    function _setDashboardService(dashboardService) {
        __dashboardService = dashboardService
    }

    var _init = function (nextStep, getTask) {
        __nextStep = nextStep;
        __getTask = getTask;
    };

    /** END PRIVATE */

    _init(options.nextStep, options.getTask);

    return {
        initServicePanel: _initServicePanel,
        moveProgressBar: _moveProgressBar,
        setDashboardService: _setDashboardService,
        hideAllPopup: _hideAllPopup
    }

});