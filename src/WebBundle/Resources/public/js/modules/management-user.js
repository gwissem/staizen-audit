var ManagementUser = (function () {

    var INTERVAL_TABLE = 60000;

    var $managementUserWrapper,
        $managementUserContent,
        $managementUserTBody,
        $managementUserFilterForm,
        $reportTab,
        $reportTabLi;

    var loaded = false,
        intervalTable,
        loadPanelAjax = null;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $managementUserWrapper = $('#management-user-wrapper');
        $managementUserContent = $managementUserWrapper.find('.management-user-content');
        $reportTab = $('a[href="#atlas_management_users"]');
        $reportTabLi = $reportTab.parent();

        _handleEvents();

    }


    var _delayCallback = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function _handleEvents() {

        $reportTab.on('shown.bs.tab', function() {
            iniLoad();
        });

        if($reportTabLi.hasClass('active')) {
            iniLoad();
        }

    }

    function iniLoad() {
        if(!loaded) {
            _loadPanel();
        }
        else {
            _loadTable();
        }
    }

    function _loadPanel() {

        $.ajax({
            url: Routing.generate('management_supervisor_user_index') +  window.location.search,
            type: 'GET',
            beforeSend: function () {
                $managementUserWrapper.addClass('loading');
            },
            success: function (response) {

                loaded = true;
                $managementUserContent.html(response.html);
                $managementUserTBody = $(".management-user-content-table tbody");
                $managementUserFilterForm = $('.management-user-filter-form');

                _afterLoadPanel();

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $managementUserWrapper.removeClass('loading');
            }
        });

    }

    function _afterLoadPanel() {

        _handleEventsAfterLoadPanel();
        _loadConfig();
        _loadTable();
        _collapseTable();
        _changeActiveTemplate();
    }

    function _loadConfig() {

        var isOnline = localStorage.getItem('management-user-control-online'),
            isCollapseUserRoles = localStorage.getItem('collapse-user-roles');

        if(isOnline === "true") {
            $('#management-user-control-online').prop('checked', true);
        }
    }

    function _collapseTable() {

        $('.management-user-content-table .with-collapse-group').each(function (i, ele) {
            var $t = $(ele),
                platformGroupId = $t.attr('data-platform-group-id'),
                platformCookie = localStorage.getItem('user_group_platform_collapse_' + platformGroupId);

            if(platformCookie === '0' || platformCookie === null) {
                $t.addClass('is-compress').attr('colspan', 1);
                $('.management-user-content-table [data-platform-group-id="'+platformGroupId+'"]').addClass('is-compress');
            }
        });

    }

    function _handleEventsAfterLoadPanel() {

        $managementUserFilterForm.find('.refresh-online-tasks').on('click', function (e) {
            e.preventDefault();
            _loadTable();
        });

        $managementUserFilterForm.find('input[type="checkbox"]').on('change', function (e) {
            e.preventDefault();
            localStorage.setItem('management-user-control-online', $(this).is(":checked"));
            _loadTable();
        });

        $managementUserFilterForm.find('input[type="text"]').on('keyup', function (e) {

            _delayCallback(function () {
                _loadTable();
            }, 500);

        });

        $managementUserWrapper.find('.select2').select2({}).on("change", function () {
            _loadTable();
        });

        $managementUserWrapper.find('thead .collapse-group button').on('click', function () {

            var $t = $(this),
                $closestTh = $t.closest('th[data-platform-group-id]'),
                platformGroupId = $closestTh.attr('data-platform-group-id');

            if($t.hasClass('btn-compress')) {

                $('.management-user-content-table table [data-platform-group-id="'+platformGroupId+'"]').addClass('is-compress');
                $closestTh.addClass('is-compress').attr('colspan', 1);
                localStorage.setItem('user_group_platform_collapse_' + platformGroupId, "0");

            }
            else {

                $('.management-user-content-table table [data-platform-group-id="'+platformGroupId+'"]').removeClass('is-compress');
                $closestTh.removeClass('is-compress').attr('colspan', $closestTh.attr('data-colspan'));
                localStorage.setItem('user_group_platform_collapse_' + platformGroupId, "1");

            }

        });

        intervalTable = setInterval(function () {

            if($('#management-user-control-online').is(":checked")) {
                _loadTable();
            }

        }, INTERVAL_TABLE);

        $managementUserContent.find('[data-toggle="tooltip"]').tooltip({
            html: true
        });

    }

    function _tabIsActive() {
        return ($reportTabLi.hasClass('active'));
    }

    function _loadTable() {

        if(!_tabIsActive()) return;

        var isOnline = $('#management-user-control-online').is(":checked"),
            phrase = $('#management-user-control-phrase').val(),
            roles = $('#management-user-control-user-role').val();

        if(loadPanelAjax) {
            loadPanelAjax.abort();
        }

        loadPanelAjax = $.ajax({
            url: Routing.generate('management_supervisor_user_table'),
            type: 'GET',
            data: {
                online: isOnline,
                phrase: phrase,
                roles: roles
            },
            beforeSend: function () {
                $managementUserFilterForm.find('.refresh-online-tasks').prop('disabled', true);
                $managementUserFilterForm.addClass('loading');
            },
            success: function (response) {
                $managementUserTBody.html(response.html);
                _afterLoadTable();
            },
            error: function (response) {
                if(response.statusText !== "abort") {
                    handleAjaxResponseError(response);
                }
            },
            complete: function () {
                $managementUserFilterForm.removeClass('loading');
                $managementUserFilterForm.find('.refresh-online-tasks').prop('disabled', false);
            }
        });

    }

    function _afterLoadTable() {

        $managementUserTBody.find('.select2').select2({

        }).on('change', _saveUserRoleGroup);
        _collapseTable();

        _bindTableEvent();
        _setValueForAllPlatformCheckbox();
    }

    function _saveUserRoleGroup() {

        var $t = $(this),
            userId = $t.closest('tr').attr('data-user-id');

        loadPanelAjax = $.ajax({
            url: Routing.generate('management_supervisor_set_user_role_group', { userId: userId, roleId: $t.val() }),
            type: 'POST',
            success: function (response) {
                if(response.success){
                    toastr.success('Zapisano')
                }
            },
            error: function (response) {
                if(response.statusText !== "abort") {
                    handleAjaxResponseError(response);
                }
            }
        });
    }

    function _saveInput($t, routing)
    {
        $.ajax({
            url: Routing.generate(routing),
            type: 'POST',
            data: {
                'userId': $t.closest('tr[data-user-id]').attr('data-user-id'),
                'checked': $t.is(':checked'),
                'value': $t.val()
            },
            success: function (response) {
                if(response.success) {
                    toastr.success("Zapisano");
                }
            },
            error: function (response) {
                handleAjaxResponseError(response);
            }
        });
    }

    function _saveUserRole() {
        $managementUserTBody.find('.td-change-user-role input[type="checkbox"]').on('change', function (e) {
            e.preventDefault();

            _saveInput($(this), 'management_supervisor_set_user_app_role');

        });
    }

    function _saveUserPlatform() {
        $managementUserTBody.find('.td-change-user-platform input[type="checkbox"]').on('change', function (e) {
            e.preventDefault();

            _saveInput($(this), 'management_supervisor_set_user_platform');
        });
    }

    function _saveUserGroup() {
        $managementUserTBody.find('.td-change-user-group input[type="checkbox"]').on('change', function (e) {
            e.preventDefault();

            _saveInput($(this), 'management_supervisor_set_user_group');

        });
    }

    function _saveAllUserPlatform()
    {
        $managementUserTBody.find('.td-change-all-user-platforms input[type="checkbox"]').on('change', function (e) {
            e.preventDefault();

            var $t = $(this);

            var $userId = $t.closest('tr[data-user-id]').attr('data-user-id');
            var $notCheckedId = _getNotCheckedPlatformArray($userId);

            $.ajax({
                url: Routing.generate('management_supervisor_set_user_platform'),
                type: 'POST',
                data: {
                    'userId': $userId,
                    'checked': $t.is(':checked'),
                    'value': $notCheckedId,
                    'all': true
                },
                success: function(response) {
                    if (response.success) {
                        if ($t.is(':checked')) {
                            $t.closest('tr').find('.td-change-user-platform input[type="checkbox"]').prop('checked', true);
                        } else {
                            $t.closest('tr').find('.td-change-user-platform input[type="checkbox"]').prop('checked', false);
                        }

                        toastr.success("Zapisano");
                    }
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                }
            });
        })
    }

    function _changeActiveTemplate()
    {
        $managementUserFilterForm.find('#template').on('change', function (e) {
            e.preventDefault();

            var $t = $(this);

            $.ajax({
                url: Routing.generate('management_supervisor_change_active_template'),
                type: 'POST',
                data: { 'templateId': $t.val() },
                success: function(response) {
                    if(response.success) {
                        toastr.success("Szablon został zmieniony");
                    }
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                }
            });
        });
    }

    function _getNotCheckedPlatformArray(userId)
    {
        var $input = $('tr[data-user-id='+ userId +']').find('.td-change-user-platform input[type="checkbox"]');
        var $platformId = [];

        $input.each(function () {
            if(!$(this).is(':checked')) {
                $platformId.push($(this).val());
            }
        });

        return $platformId;
    }

    function _setValueForAllPlatformCheckbox()
    {
        $managementUserTBody.find('.check-all-platforms').each(function(){
            var $t = $(this),
                $tr = $t.closest('tr[data-user-id]'),
                $checkbox = $tr.find('.td-change-user-platform input[type="checkbox"]'),
                $checkboxChecked = $tr.find('.td-change-user-platform input[type="checkbox"]:checked');

            if($checkbox.length === $checkboxChecked.length) {
                $t.prop('checked', true);
            }
        });
    }

    function _bindTableEvent() {
        _saveUserRole();
        _saveUserPlatform();
        _saveUserGroup();
        _saveAllUserPlatform();
    }

    $(function () {
        _init();
    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {}

});
