var ParserInputsModule = (function() {

    /** PUBLIC */

    function _parseNumber($inputs) {
        $.each($inputs, function (i, ele) {
            var $t = $(ele);
            if($t.closest('.grid-stack-item').hasClass('not-negative')) {
                $t.attr('min', "0");
            }
        });

        if($inputs.hasClass('not-negative')) {

        }
    }

    /** PRIVATE */

    return {
        parseNumber: _parseNumber
    }

});