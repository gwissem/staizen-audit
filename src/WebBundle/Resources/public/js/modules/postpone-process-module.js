/**
 * @class PostponeProcessModule
 */
var PostponeProcessModule = (function(_dashboardService, onFinishPostpone) {

    var $postponeProcessModal,
        $postponeProcessModalBody,
        dataSpinner,
        $postponeTime,
        $postponeId,
        $postponeConfirm,
        $postponeReason,
        $postponeDescription,
        laddaConfirm;

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function eventsHandle() {

        $postponeProcessModal.on('click', '.add-time', function(){

            $postponeProcessModal.find('.add-time').removeClass('active');

            var $t = $(this),
                d = new moment(),
                newTime = d.add(parseInt($t.attr('data-add-minutes')), 'minutes');
            $postponeTime.val(newTime.format('YYYY-MM-DD HH:mm'));

            $t.addClass('active');

        });

        $postponeConfirm.on('click', function () {

            if(!_valid()) return false;

            laddaConfirm.start();

            $.ajax({
                url: Routing.generate('ajax_postpone_task'),
                data: _getPostponeData(),
                type: "POST",
                success: function (result) {

                    if(result.error) {
                        toastr.error("", result.message, {timeOut: 3000});
                    }
                    else {
                        onFinishPostpone(result.message + '<br>' + $postponeTime.val());
                        $postponeProcessModal.modal('hide');
                    }

                },
                error: function (error) {
                    handleAjaxResponseError(error);
                },
                complete: function () {
                    laddaConfirm.stop();
                }
            });

            return false;
        });

    }

    function _valid() {

        if(!$postponeTime.val()) {
            toastr.warning("Proszę uzupełnić czas odroczenia", "Brak wymaganych danych", 5000);
            return false;
        }

        if(!$postponeDescription.val()) {
            toastr.warning("Proszę uzupełnić wytyczne do zadania", "Brak wymaganych danych", 5000);
            return false;
        }

        return true;
    }

    function _getPostponeData() {

        return {
            processInstanceId: _dashboardService.getCurrentTicket('id'),
            postponeProcessId: $postponeId.val(),
            time: $postponeTime.val(),
            reason: $postponeReason.val(),
            description: $postponeDescription.val()
        };

    }

    function _loadModal() {

        $postponeProcessModalBody.html(dataSpinner);

        $.ajax({
            url: Routing.generate('ajax_postpone_modal_form', {processInstanceId: _dashboardService.getCurrentTicket('id')}),
            type: "GET",
            success: function (result) {

                $postponeProcessModalBody.html(result.html);
                _initFormVariable();
                _afterLoad();

            }
        });

    }

    function _afterLoad() {

        $postponeProcessModalBody.find('.date-time-picker').datepicker(dateTimePickerOptions);

    }

    function _initFormVariable() {

        $postponeTime = $postponeProcessModal.find('#postpone-time');
        $postponeReason = $postponeProcessModal.find('#postpone-reason');
        $postponeDescription = $postponeProcessModal.find('#postpone-description');
        $postponeId = $postponeProcessModal.find('#postpone-id');
        laddaConfirm =  Ladda.create($postponeConfirm[0]);

    }

    function init() {

        $postponeProcessModal = $('#postpone-process-modal');
        $postponeProcessModalBody = $postponeProcessModal.find('.modal-body');
        dataSpinner = $postponeProcessModalBody.attr('data-spinner');
        $postponeConfirm = $postponeProcessModal.find('#postpone-confirm');

        eventsHandle();
    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _openModel() {

        $postponeProcessModal.modal('show');

        _loadModal();

    }

    init();

    return {
        openModel: _openModel
    }

});