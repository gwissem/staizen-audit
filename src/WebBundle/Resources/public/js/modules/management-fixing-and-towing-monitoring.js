var ManagementFixingAndTowingMonitoring = (function () {

    var $monitoringContent,
        $monitoringTab,
        loaded = false;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $monitoringContent = $('.monitoring-fixing-and-towing-content');
        $monitoringTab = $('a[href="#fixing_and_towing_monitor_tab"]').closest('li');

        _handleEvents();

    }

    function _loadMonitoringContent(callback) {

       $.ajax({
            url: Routing.generate('ms_fixing_and_towing_monitoring_view'),
            type: 'GET',
            success: function (response) {
                $monitoringContent.html(response);
                _refreshHandle();
                callback();
            },
            error: function (response) {
                handleAjaxResponseError(response);
            }
        });

    }

    function _refreshHandle() {

        $('.tools-content iframe[data-refresh]').each(function (i, ele) {
            _intervalTools(ele, ele.getAttribute('data-refresh'));
        });

        _intervalStats();

    }

    function _tabIsActive() {
        return ($monitoringTab.hasClass('active'));
    }

    function _intervalTools(iframe, time) {

        var event = new CustomEvent("refresh-tool");

        setInterval(function () {
            if(_tabIsActive()) {
                iframe.contentWindow.dispatchEvent(event);
            }
        }, time * 1000);

    }

    function _intervalStats() {

        setInterval(function () {
            if(_tabIsActive()) {
                refreshStats()
            }
        }, 10000);

    }

    function refreshStats() {

        $.ajax({
            url: Routing.generate('ms_fixing_and_towing_monitoring_stats'),
            type: 'GET',
            success: function (response) {
                if(response) {
                    if(response.stats) {
                        $monitoringContent.find('.stats-content').html(response.stats);
                    }
                }
            },
            error: function (response) {
                handleAjaxResponseError(response);
            }
        });

    }

    function _handleEvents() {

        $monitoringTab.on('shown.bs.tab', function() {
            if(!loaded) {
                loaded = true;
                _loadMonitoringContent(function () {

                });
            }

        });

        $('.change-view-automation [name="view-mode-detailed"]').on('change', function () {

           var $t = $(this);

           if($t.is(":checked")) {
               Cookies.set('automation_detailed_view', 1);
               $('.stats-content .detailed, .tools-content .detailed').show();
           }
           else {
               Cookies.set('automation_detailed_view', 0);
               $('.stats-content .detailed, .tools-content .detailed').hide();
           }

        });

        window.onfocus = function() {
            if(_tabIsActive()) {
                refreshStats();
            }
        };

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {

    }

});
