function ticketManager(options) {

    /**
     * TODO - Performance
     * Zadania renderuja się dopiero jak się rozwinie bloczek z kategorią
     *
     * - nie renderuj zadania gdy parent jest ukryty
     * - po kliknieciu w parenta pokaz/ukryj zadania
     * - updateTicketsAmount wrzucić w jakieś sensowne miejsce
     */

    /**
     * @typedef {Object} TicketType
     * @property {string} taskGroup
     * @property {string} stepId
     * @property {string} priority
     * @property {string} stepName
     * @property {string} special_situation
     * @property {string} name
     * @property {string} processName
     * @property {string} caseId
     * @property {string} dateEnter
     * @property {string} processDescription
     * @property {string} instanceDescription
     * @property {number} id
     */

    var ticketGroups = {},
        tickets = [],
        $ticketGroups = $('#ticket-groups'),
        $currentTicketList = $('.category.currentTask'),
        ticketProto = $ticketGroups.attr('data-prototype-task'),
        groupProto = $ticketGroups.attr('data-prototype-group'),
        specialSituationOptions = ($ticketGroups.length) ? JSON.parse($ticketGroups.attr('data-special-situations')) : [],
        isSearchMode = false,
        $amountTasksLabel = $('#amount-tasks');

    function __existsOnList(ticket, e) {
        return parseInt(e.id) === parseInt(ticket.id);
    }

    function __existsOnListById(ticketId, e) {
        return parseInt(e.id) === parseInt(ticketId);
    }

    /**
     * @param {TicketType} ticket
     * @param fromSocket
     * @returns {boolean}
     * @private
     */
    function _addTicket(ticket, fromSocket) {

        fromSocket = fromSocket || false;

        /** Sprawdzenie czy zadanie istnieje na liście */
        var result = $.grep(tickets, __existsOnList.bind(null, ticket));

        if (result.length !== 0) {
            return false;
        }

        var groupId = parseId(ticket.stepId);

        if(ticket.taskGroup){
            groupId = groupId + ticket.taskGroup.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        }

        if(ticket.priority) {
            groupId = groupId + ticket.priority;
        }

        var group = ticketGroups[groupId];

        if (!group) {

            group = {
                id: groupId,
                name: (ticket.stepName) ? ticket.stepName : ticket.name,
                $container: null
            };

            ticketGroups[groupId] = group;
            renderGroup(group, groupId, ticket);

        }

        var ticketHtml = renderTicketHtml(ticket, fromSocket);

        tickets.push(ticket);

        addToGroup(ticketHtml, group);

        if(ticket.special_situation === "7") {
            _findTicket(ticket.id).css('background-color', 'beige');
        }

    }

    function _getHistoricalTasks() {

        var historicalTasksGroup = $('#history-ticket-groups'),
            historicalTicketHtml = historicalTasksGroup.attr('data-prototype-task'),
            $loader = $('#tickets-history .loader');

        _fastShow($loader);

        $.ajax({
            url: Routing.generate('operation_dashboard_historical_task_get'),
            type: "GET",
            success: function (data) {

                $('#history-ticket-groups li').remove();

                $.each(data, function(i, ele) {

                    historicalTasksGroup.prepend(_replaceHistoryTicketData(ele, historicalTicketHtml));

                });

                var historyTicketsWrapper = $('#history-ticket-groups');
                historyTicketsWrapper.children().not('.loader').each(function(i,ele){historyTicketsWrapper.prepend(ele)});

                _fastHide($loader);

            }
        });

    }

    /**
     * @param {TicketType} ticket
     * @param {string} protoHtml
     * @private
     */
    function _replaceHistoryTicketData(ticket, protoHtml) {

        return protoHtml
            .replace('__ID__', ticket.id)
            .replace('__STEP_ID__', ticket.stepId)
            .replace('__CASE_ID__', ticket.caseId)
            .replace('___PROCESS_NAME___', ticket.processName)
            .replace('___STEP_NAME___', ticket.stepName)
            .replace('__ENTERED_AT__', (ticket.dateEnter) ? moment(ticket.dateEnter).format("D MMM YYYY, HH:mm") : '-')
    }


    function renderTicketHtml(ticket, fromSocket) {

        var description = (ticket.description) ? ticket.description : ticket.instanceDescription;

        if(!description) description = '-';

        var ticketHtml = ticketProto,
            dateCreated = '-'; // TODO - z bazy trzeba zwracac created_at

        if(ticket.dateEnter) {
            if(ticket.dateEnter.date) {
                dateCreated = moment(ticket.dateEnter.date).format("D MMM YYYY, HH:mm")
            }
            else {
                dateCreated = moment(ticket.dateEnter).format("D MMM YYYY, HH:mm")
            }
        }

        var _rootId = (ticket.root_id) ? ticket.root_id : ((ticket.rootId) ? ticket.rootId : ticket.groupProcessId);

        ticketHtml = ticketHtml.replace('__STATUS__', 'status1')
            .replace(/__GROUP_PROCESS_ID__/g, ticket.groupProcessId)
            .replace('__STEP_ID__', ticket.stepId)
            .replace('__ROOT_ID__', _rootId)
            .replace('__TITLE__', getSpecialDescription(ticket.special_situation))
            .replace(/__ID__/g, ticket.id)
            .replace('__SPECIAL_ICON__', getSpecialIcon(ticket.special_situation))
            .replace('__INSTANCE_DESCRIPTION__', description)
            .replace('__CASE_ID__', (ticket.caseNumber) ? ticket.caseNumber : _displayCaseNumber(_rootId, ticket.stepId))
            .replace('__CREATED_DATE__', dateCreated)
            .replace('__CLASS__', getTicketClass(ticket));
            // .replace('__CLASS__', ((fromSocket) ? '' : ''));

        return ticketHtml;
    }

    function getTicketClass(ticket) {

        if(ticket.color) {
            return 'ticket-' + ticket.color;
        }

        return '';

    }

    function _displayCaseNumber(processId, stepId) {
        var prefix = stepId.match(/1012\.*/g) !== null ? 'T' : 'A';
        return getCaseNumberFromString(processId, prefix);
        // return caseNumber + "0".repeat(8 - String(processId).length) + processId;
    }

    function getSpecialDescription(id) {
        return (specialSituationOptions[id]) ? specialSituationOptions[id] : '';
    }

    function getSpecialIcon(specialSituation) {

        switch (specialSituation) {
            case '1' :
            {
                return 'fa-car';
            }
            case '2' :
            case '3' :
            {
                return 'fa-child'
            }
            case '4' :
            {
                return 'fa-clock-o'
            }
            case '5' :
            {
                return 'fa-exclamation'
            }
            case '7' :
            {
                return 'fa-star font-yellow-crusta'
            }
            default :
            {
                return 'none-icon'
            }
        }
    }

    var _index = 0;

    function __addTicketCb(i, ele) {
        _addTicket(ele);
    }

    function _setList(tickets) {

        removeAll();

        $ticketGroups.find('.loader').remove();

        $.each(tickets, __addTicketCb);

        $amountTasksLabel.text(getCountTickets());

        if(typeof taskOverviewAction !== 'undefined' ) {
            options.loadTaskOverview(taskOverviewAction.processInstances);
        }

    }

    function _removeTasks(tasks) {

        var promise = jQuery.Deferred();

        function _removeSubTasks() {

            if(promise.state() === 'rejected') {
                return;
            }

            var subTasks = tasks.splice(0, 200);

            /** Reszta elementów w currentTasksIds nie istnieje - wywalamy z listy */

            var groups = {};

            $.each(subTasks, function(i, idTask) {

                var group = _removeTicket(idTask, true, false);

                if(!groups[group.id]) {
                    groups[group.id] = group;
                }

            });

            $.each(groups, function (i, group) {
                if(group) {
                    updateTicketsAmount(group);
                    if (group.tickets <= 0) {
                        group.visible = false;
                        group.$container.parent()[0].style.display = 'none';
                    }
                }
            });

            if(tasks.length === 0) {
                promise.resolve();
            }
            else {
                setTimeout(_removeSubTasks, 0);
            }

        }

        setTimeout(_removeSubTasks, 0);

        return promise;

    }

    var indexRefresh = 0;

    function _updateTasks(tasks) {

        indexRefresh++;

        var currentTasksIds = _getAllIds(),
            index = -1,
            promise = jQuery.Deferred(),
            removePromise = null;

        promise.fail(function () {
            if(removePromise) {
                removePromise.reject('stop');
            }
        });

        function _updateSubTasks() {

            if(promise.state() === 'rejected') {
                return;
            }

            var subTasks = tasks.splice(0, 200);

            $.each(subTasks, function (i, obj) {

                /** Dodawanie ticketów które nie istnieją na liście */
                index = currentTasksIds.indexOf(obj.id);

                if (index === -1) {
                    /** Nowe */
                    _addTicket(obj);
                }
                else {
                    /** Istnieją już */
                    currentTasksIds.splice(index, 1);
                }

            });

            if(tasks.length === 0) {

                removePromise = _removeTasks(currentTasksIds);

                $.when(removePromise).then(function () {

                    $amountTasksLabel.text(getCountTickets());
                    promise.resolve();

                },function (status) {

                    if(status !== 'stop') {
                        promise.resolve();
                    }

                });

            }
            else {
                setTimeout(_updateSubTasks, 0);
            }

        }

        setTimeout(_updateSubTasks(),0);

        return promise;

    }

    function _replaceTicket(newTicket, oldTicketId) {

        _addTicket(newTicket.description);
        _removeTicket(oldTicketId, true);

    }

    function renderGroup(group, groupId, ticket) {

        var priority = (typeof ticket !== "undefined") ? ticket.priority : '';
        var groupName = (typeof ticket !== "undefined") ? ticket.processDescription + '<i class="fa fa-circle"></i>': '';
        groupId = (typeof groupId === "undefined") ? parseId(group.id) : groupId;
        var groupHtml = '';

        if(groupId.indexOf('1092i') !== -1 && ticket.taskGroup && ticket.taskGroup.indexOf('@') !== -1) {
            groupHtml = renderEmailGroup(groupId, priority, group.name, ticket.taskGroup);
        }
        else {
            groupHtml = renderDefaultGroup(groupId, priority, group.name, groupName);
        }

        var isHidden = localStorage.getItem('ticket_list_' + groupId);

        if(isHidden === "true") {
            groupHtml = groupHtml.replace('class="ticket-category', 'class="ticket-category hidden-ticket-list');
        }

        $ticketGroups.append(groupHtml);

        group.$container = $ticketGroups.find('#' + groupId + ' ul');
        group.visible = true;
        group.tickets = 0;

    }

    function renderDefaultGroup(groupId, priority, originalName, groupName) {

        return groupProto.replace('__ID__', groupId)
            .replace('__NAME__', (originalName))
            .replace('__NAME_NO_HTML__', (originalName + ' (' + groupId.replace('i','.')+')'))
            .replace('__PRIORITY__', priority)
            .replace('__GROUP_NAME__', groupName);

    }

    function renderEmailGroup(groupId, priority, originalName, taskGroup) {

        return groupProto.replace('__ID__', groupId)
            .replace('__NAME__', '<i class="fa fa-envelope-o"></i><span>' + taskGroup + '</span>')
            .replace('__NAME_NO_HTML__', (originalName + ' (' + groupId.replace('i','.')+')'))
            .replace('__PRIORITY__', priority)
            .replace('__GROUP_NAME__', '');

    }

    function updateTicketsAmount(group) {

        var groupBoxHeader =  group.$container.prev();
        var ticketsNumber = group.tickets === 200 ? group.tickets + '+' : group.tickets;
        groupBoxHeader.find('.case-amount').html(ticketsNumber);

    }


    function addToGroup(ticketHtml, group) {

        group.$container.append(ticketHtml);

        group.tickets++;
        updateTicketsAmount(group);

        if (!group.visible) {
            _fastShow(group.$container.parent());
        }

    }

    function __removeTicketCb(i, ele) {
        _removeTicket(ele, true);
    }

    function _clear() {
        $.each(_getAllIds(), __removeTicketCb);
    }

    function removeAll() {
        tickets = [];
    }

    function _removeTicket(ticketId, removeFromView, updateGroup) {

        updateGroup = (typeof updateGroup === "undefined") ? true : updateGroup;
        removeFromView = removeFromView || false;

        var ticket = $.grep(tickets, __existsOnListById.bind(null, ticketId));

        if (!ticket.length) {
            return false;
        }

        ticket = ticket[0];

        var groupId = parseId(ticket.stepId);

        if(ticket.taskGroup){
            groupId = groupId + ticket.taskGroup.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        }

        if(ticket.priority) {
            groupId = groupId + ticket.priority;
        }

        var group = ticketGroups[groupId];

        if (removeFromView) {
            _removeTicketById(ticketId);
        }

        tickets = tickets.filter(function (obj) {
            return parseInt(obj.id) !== parseInt(ticketId);
        });

        if (group) {

            group.tickets--;

            if(updateGroup) {

                updateTicketsAmount(group);
                if (group.tickets <= 0) {
                    group.visible = false;
                    _fastHide(group.$container.parent());
                }

            }

            return group;

        }

    }

    function _removeTicketById(ticketId) {
        var t = document.getElementById('single-ticket-' + ticketId);
        if(t) {
            t.remove();
        }
    }

    function _findTicket(ticketId) {
        return $('#single-ticket-' + ticketId);
    }

    function _fastHide($el) {
        $el[0].style.display = 'none';
    }

    function _fastShow($el) {
        $el[0].style.display = '';
    }

    function getCurrentDomTicket() {
        var c = $currentTicketList.find('li');
        if (c.length) return c;
        return null;
    }

    function _setActiveTicket($ticket) {

        fromActiveToGroup();

        if ($ticket) {
            $currentTicketList.addClass('active');
            $currentTicketList.find('.currentTicketContainer').append($ticket);
            $ticket.css('animation-name', 'borderPulse3');
        }
        else {
            $currentTicketList.removeClass('active');
        }

        resizeList();

    }

    function resizeList() {

        if(typeof options.setHeightOfTicketGroups === "function") {
            setTimeout(function () {
                options.setHeightOfTicketGroups();
            }, 500);
        }

    }

    /** Wyrzucenie ticketa z listy aktywnych z powrotem do grupy */

    function fromActiveToGroup() {

        var $ticket = getCurrentDomTicket();

        if (!$ticket) return false;

        var group = ticketGroups[parseId($ticket.attr('data-attribute-value-step-id'))];

        if (group.$container === null) {
            renderGroup(group);
        }
        else if (!group.visible) {
            _fastShow(group.$container.parent());
        }

        $ticket.appendTo(group.$container);

    }

    function _finishTask(withKickActive) {

        if(withKickActive === true) {
            fromActiveToGroup();
        }
        else {
            // getCurrentDomTicket().remove();
        }

        $(".dashboard-mode-switcher").tabs("disable", 1);
        $('.button-timeline').removeClass('active-task').find('.active-task-id').html('');
        $('.button-activities').trigger('click');

        resizeList();

    }

    function parseId(id) {
        return id.toString().replace('.', 'i');
    }

    function _getAllIds() {

        return tickets.map(function (obj) {
            return String(obj.id);
        });

    }

    function _setSearchMode(mode) {
        isSearchMode = mode;
    }

    function _onClick_TicketCategory(e) {

        e.preventDefault();

        var $t = $(this),
            isHidden = $t.toggleClass('hidden-ticket-list').hasClass('hidden-ticket-list'),
            id = $t.closest('.group-box').attr('id');

        localStorage.setItem('ticket_list_' + id, isHidden);

    }

    function _handleToggleList() {

        $ticketGroups.on('click', '.ticket-category', _onClick_TicketCategory);

    }

    function getCountTickets() {
        return tickets.length;
    }

    _handleToggleList();

    return {
        clear: _clear,
        remove: _removeTicket,
        add: _addTicket,
        setList: _setList,
        setActive: _setActiveTicket, // deprecated
        replace: _replaceTicket,
        finishTask: _finishTask,
        refresh: resizeList,
        getAllIds: _getAllIds,
        updateTicketsAmount: updateTicketsAmount,
        setSearchMode: _setSearchMode,
        getTitleOfSpecialSituation: getSpecialDescription,
        getHistoricalTasks: _getHistoricalTasks,
        displayCaseNumber: _displayCaseNumber,
        updateTasks: _updateTasks,
        getCountTickets: getCountTickets
    }
}