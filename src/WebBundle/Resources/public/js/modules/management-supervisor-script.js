var ManagementSupervisorScript = (function () {

    var $timelineAtlasLog,
        $atlasVersionLogsTab;


    var timelineDatePickerOptions = Object.assign({}, datePickerOptions, {
        onSelect: function () {

            var from = $atlasVersionLogsTab.find('[name="timeline-version-from"]').val();
            var to = $atlasVersionLogsTab.find('[name="timeline-version-to"]').val();

            _loadTimeline(from, to);

        }
    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $timelineAtlasLog = $('.timeline-atlas-log .timeline');
        $atlasVersionLogsTab = $('#atlas_version_logs');

        _handleEvents();

    }

    var _delayCallback = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function _handleEvents() {

        $('a[href="#atlas_version_logs"]').on('click', function (e) {
            e.preventDefault();
            _refreshDatepicker();
        });

        $atlasVersionLogsTab.find('.datepicker-control').on('change', function () {

            var from = $atlasVersionLogsTab.find('[name="timeline-version-from"]').val();
            var to = $atlasVersionLogsTab.find('[name="timeline-version-to"]').val();
            _loadTimeline(from, to);

        });

    }

    function _refreshDatepicker() {

        var $datePickers = $('.datepicker-control');

        $.each($datePickers, function (i, ele) {
            var _datePicker = $(ele).data('datepicker');
            if (_datePicker) {
                _datePicker.destroy();
            }
        });

        $datePickers.datepicker(timelineDatePickerOptions);
        $datePickers.each(function (i, ele) {
            if(ele.value)
            {
                var date = new Date(ele.value);

                if(!isNaN( date.getTime()))
                {
                    $(ele).data('datepicker').selectDate(date);
                }
            }
        });
    }

    function _loadTimeline(from, to) {

        _delayCallback(function () {

            from = (typeof from === "undefined" || !from) ? moment().subtract(7, 'days').format('YYYY-MM-DD') : from;
            to = (typeof to === "undefined" || !to) ? moment().format('YYYY-MM-DD') : to;

            $.ajax({
                url: Routing.generate('management_supervisor_version_log_timeline', {from: from, to: to}),
                type: 'GET',
                success: function (response) {
                    $timelineAtlasLog.html(response.html);
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                }
            });

        }, 300);

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {

    }

});
