"use strict";
    var PublicTelephonyStatistics = (function () {
    

    var loaded = false,
        intervalContentAjax=false,
        intervalContentTime = 6000,
        $managementContent = null, // 6 sec
        $bottomContent = null; // 6 sec

    /*

        _init() -> _handleEvents()

        ON_FIRST_ACTIVE_TAB  -> iniLoad() -> _loadPanel() -> ... -> _afterLoadPanel() -> _handleEventsAfterLoadPanel()

     */

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

       $managementContent = $('#telephony-statistics-wrapper');
       $bottomContent = $('#bottom-stats-wrapper');


        _handleEvents();

    }

    var _delayCallback = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function _handleEvents() {


        iniLoad();

    }

    function iniLoad() {
        if(!loaded) {
            _loadPanel();
        }
    }

    function _loadPanel() {

        $.ajax({
            url: Routing.generate('telephony-public-refresh'),
            type: 'GET',
            beforeSend: function () {
                // $managementWrapper.addClass('loading');
            },
            success: function (response) {

                loaded = true;
                $managementContent.html(response.html);

                _afterLoadPanel();

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                // $managementWrapper.removeClass('loading');
            }
        });

    }

    function _afterLoadPanel() {

        _handleEventsAfterLoadPanel();
    }


        function replaceRestStats() {

            try {
                $.ajax({
                    url: Routing.generate('telephony-breaks-public-refresh'),
                    success: function (response) {
                        $('#bottom-stats-wrapper').html(response.html);
                    },
                });
            } catch (e) {
                console.log(e);
            }

        }

    /**
     * Tutaj jakieś eventy, po załadowaniu panelu
     */
    function _handleEventsAfterLoadPanel() {

        handle();

    }
        function handle(){

            $.ajax({
                url: Routing.generate('telephony-public-refresh'),
                success: function (response) {
                    $('#telephony-statistics-wrapper').html(response.html);
                },
            }).done(function () {

                setInterval(function () {
                    try {

                        $.ajax({
                            url: Routing.generate('telephony-public-refresh'),
                            success: function (response) {

                                $('#telephony-statistics-wrapper').html(response.html);

                            }
                        });
                        replaceRestStats();

                    }catch (e) {

                    }
                }, intervalContentTime)
            })
        }




    $(function () {
        _init();
    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {}

});
