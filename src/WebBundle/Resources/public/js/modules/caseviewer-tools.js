var CaseViewerTools = (function () {

    var $caseServicesSearchForm,
        $caseServicesTable,
        $caseInfoBox,
        $previewOfService,
        $caseServicesPortlet,
        $caseNotesPortlet,
        $caseNotes,
        $matrixOfCase,
        $actionsOfCase,
        $caseInfoPortlet,
        $casesListBody,
        $casesList,
        caseNoteModule,
        emailPreviewModule;
    // $('.caseListOpener').off('click').on('click', function (ev) {
    //     ev.stopPropagation();
    //     ev.preventDefault();
    //
    //     if( $casesList.hasClass('opened')){
    //         $casesList.removeClass('opened');
    //         $casesList.slideUp(500);
    //         $(this).text('Otwórz');
    //     }else{
    //         $casesList.addClass('opened');
    //         $casesList.slideDown(500);
    //         $(this).text('Zwiń');
    //     }
    // });
    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $caseServicesSearchForm = $('#case-services-search-form');
        $caseServicesTable = $('#case-services-table');
        $caseInfoBox = $('#case-info-box');
        $previewOfService = $('#preview-of-service');
        $caseInfoPortlet = $('#case-info-portlet');
        $caseServicesPortlet = $('#case-services-portlet');
        $caseNotesPortlet = $('#case-notes-portlet');
        $caseNotes = $('#case-notes');
        $casesList = $('#case-list');
        $casesListBody = $('#case-list-body');
        $matrixOfCase = $('#matrix-of-case');
        $actionsOfCase = $('#actions-of-case');

        caseNoteModule = CaseNoteModule();
        emailPreviewModule = EmailPreviewModule();

        _handleEvents();
    }

    function refreshNotes(rootId) {

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceGetNotesAndInfo', {processInstanceId: rootId, displayInfo: false}),
            type: "GET",
            beforeSend: function () {

            },
            success: function (response) {

                $.each(response.notes.list, function (key, note) {
                    if (note.content) {
                        caseNoteModule.appendNote(note, 'prepend', key);
                    }
                });

                // if($('.accordion-expand-all').hasClass('active')) {
                //
                //     $('.notification-container').find('.title').addClass('absolute-header').attr({
                //         "aria-selected": "true",
                //         "aria-expanded": "true"
                //     }).next().attr("aria-hidden", "false").show().next().next().show();
                // }
            },
            complete: function () {
                $caseNotesPortlet.fadeIn(300).removeClass('loading');
            }
        });

    }

    function initNoteContainer() {

        $(".notification-container").accordion({
            header: "h4",
            heightStyle: "content",
            collapsible: true,
            beforeActivate: function (event, ui) {
                if (ui.newHeader[0]) {
                    var currHeader = ui.newHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                } else {
                    var currHeader = ui.oldHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                }

                var isPanelSelected = currHeader.attr('aria-selected') == 'true';

                currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

                currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

                currContent.toggleClass('accordion-content-active', !isPanelSelected);

                return false;
            }
        });

        if($caseNotesPortlet.length) {
            $caseNotesPortlet.on('click', '.preview-email', function () {
                $('.note-inbox').attr('data-document-id', this.getAttribute('data-attachment-id'));
                if (typeof CustomEvent !== 'undefined') {
                    window.document.body.dispatchEvent(new CustomEvent("refresh-note-inbox"));
                }
            });
        }

        $('#case-notes').on('click', '.note-title', function () {
            var $t = $(this);
            $t.next('.note-content').toggle();

            if ($t.hasClass('absolute-header')) {
                $t.removeClass('absolute-header');
                $t.find('.header-text').addClass('visible');
                $t.find('.date-and-time').removeClass('visible');
            } else {
                $t.addClass('absolute-header');
                $t.find('.header-text').removeClass('visible');
                $t.find('.date-and-time').addClass('visible');
            }
        })

    }
    function showCase(caseNr) {

      $.ajax({
            url: Routing.generate('caseViewerView', {rootId: caseNr}),
            type: 'GET',
            beforeSend: function () {
                _resetAllPanel();
            },
            success: function (response) {

                if(response.services) {
                    $caseServicesTable.html(response.services);
                }
                else {
                    $caseServicesTable.html('');
                    $caseNotesPortlet.removeClass('loading');
                }

                if(response.caseInfo && response.rootId) {
                    $caseInfoBox.html(response.caseInfo);
                    $caseInfoBox.append(response.rsList);
                    $caseInfoBox.append(response.rzwList);
                    $caseInfoBox.append(response.emailAttachments);
                    $caseInfoBox.append(response.documents);
                    refreshNotes(response.rootId);
                }
                else {
                    $caseInfoBox.html('');
                }

                if(response.matrixProcessInstance) {
                    _loadMatrix(response.matrixProcessInstance);
                }

                if($caseServicesTable.find('.check-in-cdn').length) {
                    var cdnPromise = loadInfoFromCDN(response.rootId);

                    $.when(cdnPromise).done(function (cdnResult) {
                        setCheckCDNinServiceTable(cdnResult);
                    });
                }

            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {
               $caseServicesPortlet.removeClass('loading');
                $caseInfoPortlet.removeClass('loading');
            }
        });
    }
    function setCaseInfoEvent() {
        $('.caseinfo').off('click').on('click',function(){
           showCase($(this).data().case);
           $casesListBody.find('tr.active').removeClass('active');
           $(this).parents('tr').addClass('active');
        });
    }
    function _handleEvents() {

        initNoteContainer();

        $caseServicesSearchForm.on('submit', function (e) {

            e.preventDefault();
            var $t = $(this),
                searchQuery = $t.find('#search-query').val(),
                searchType  = $t.find('#search-type').val();

            if(!searchQuery) return;

            var ladda = Ladda.create($t.find('button')[0]);

            ladda.start();

            $.ajax({
                url: Routing.generate('caseViewerSearch', {searchQuery: searchQuery, searchType:searchType}),
                type: 'GET',
                beforeSend: function () {
                    _resetAllPanel();
                    $casesListBody.html('');
                },
                success: function (response) {

                    for(var $i = 0; $i < response.casesFound.length;$i++ )
                    {
                        var tableToInsert = response.casesFound[$i];
                        var row = $('<tr class="">' +
                            '<td><button class="btn btn-primary caseinfo" data-case="'+tableToInsert.caseNumber+'"> '+tableToInsert.caseNumber+'</button></td>' +
                            '<td>'+tableToInsert.createdAt+'</td>' +
                            '<td>'+((tableToInsert.firstname|| " ")  +' '+  (tableToInsert.lastname|| " "  ) )+'</td>' +
                            '<td>'+ (tableToInsert.registrationNumber || "-" )+'</td>' +
                            '<td>'+ (tableToInsert.makeModel || "-" )+'</td>' +
                            '<td>'+ (tableToInsert.platform || "-" )+'</td>' +
                            '<td>'+ (tableToInsert.program || "-" )+'</td>' +
                            '</tr>');
                        $casesListBody.append(row)

                    }

                    setCaseInfoEvent();

                },
                complete: function () {

                    ladda.stop();
                    $caseServicesPortlet.removeClass('loading');
                    $caseInfoPortlet.removeClass('loading');
                    $caseNotesPortlet.removeClass('loading');

                    var firstCaseButton = $('button.caseinfo').first();

                    if(firstCaseButton.length){
                        showCase(firstCaseButton.data().case);
                        $(firstCaseButton).parents('tr').addClass('active');
                    }

                }
            });

        });

        $caseServicesTable.on('submit', '.service-custom-form form', function (e) {

            e.preventDefault();

            var $btn = $(this).find('button'),
                ladda = Ladda.create($btn[0]);

            $.ajax({
                url: Routing.generate('caseViewerSaveAttributes'),
                type: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    ladda.start();
                },
                success: function (result) {

                    toastr.success("Dane zostały pomyślnie zapisane");

                },
                error: function() {

                    toastr.error("Wystąpił błąd podczas zapisywania danych");

                },
                complete: function () {
                    ladda.stop();
                }

            });


        });

        $caseServicesTable.on('click', '.service-row', function (e) {

            e.preventDefault();
            $previewOfService.fadeIn(300).addClass('loading');

            var $t = $(this),
                serviceId = $t.attr('data-service-id'),
                groupProcessId = $t.attr('data-group-process-id');

            $caseServicesTable.find('.service-row.active').removeClass('active');
            $t.addClass('active');

            $.ajax({
                url: Routing.generate('operationalDashboardInterfaceOverviewForCaseViewerService', {processId: groupProcessId, serviceId: serviceId}),
                type: "GET",
                beforeSend: function () {
                    $previewOfService.find('.portlet-body').html('');
                },
                success: function (result) {

                    if(result.services) {

                        var services = result.services.filter(function (s) {
                            return (parseInt(s.id) ===parseInt(groupProcessId));
                        });

                        if(services.length > 0) {
                            var service = services[0];
                            $previewOfService.find('.portlet-body').html(service.overview);
                        }
                    }

                    if(result.service_form) {
                        $caseServicesTable.find('.service-custom-form').html(result.service_form);
                    }
                    else {
                        $caseServicesTable.find('.service-custom-form').html('');
                    }

                },
                complete: function () {

                    $caseInfoPortlet.find('a.collapse').first().click();
                    $previewOfService.removeClass('loading');
                }
            });

        });

    }

    function setCheckCDNinServiceTable(result) {

        $caseServicesTable.find('.check-in-cdn').html('<i class="fa fa-close fa-2x font-red"></i>');

        $.each(result, function (i, ele) {
            $caseServicesTable.find('.check-in-cdn[data-process-id="'+ele.groupProcessId+'"]').html('<i class="fa fa-check fa-2x font-green"></i>');
        });

    }

    function loadInfoFromCDN(rootId) {

        var dfd = jQuery.Deferred();

        $.ajax({
            url: Routing.generate('ms_case_tools_cdn_info', {rootProcessId: rootId}),
            type: "GET",
            success: function (result) {
                dfd.resolve(result);
            },
            complete: function () {
                dfd.reject([]);
            }
        });

        return dfd;

    }

    function _resetAllPanel() {

        $caseServicesTable.html('');
        $caseInfoBox.html('');
        $previewOfService.fadeOut(300).find('.portlet-body').html('');
        $caseServicesPortlet.fadeIn(300).addClass('loading');
        $caseNotesPortlet.fadeIn(300).addClass('loading');
        $caseNotes.html('');
        $caseInfoPortlet.fadeIn(300).addClass('loading');
        $matrixOfCase.find('.dynamic-widget-content').html('');
        $matrixOfCase.fadeOut(300);
        $actionsOfCase.fadeOut(300);
        $actionsOfCase.find('.portlet-body').html('');
    }



    function _loadMatrix(matrixProcessInstanceId) {

        var dfd = jQuery.Deferred();

        var url = Routing.generate('operationalDashboardInterfaceGetDynamicWidget', {processInstanceId: matrixProcessInstanceId, name: "matrix"});

        $.ajax({
            url: url,
            type: "GET",
            success: function (result) {
                $matrixOfCase.find('.dynamic-widget-content').html(result.widget.html);
                $matrixOfCase.find('input, select').prop("disabled", true);
                $matrixOfCase.find('select[data-attribute-path]').select2();
                dfd.resolve($matrixOfCase.find('.dynamic-widget-content'));
            },
            complete: function () {
                $matrixOfCase.fadeIn(300);
            },
            error: function (error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            }
        });

        return dfd;

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {

    }

});
