var ManagementPushingReport = (function () {

    var INTERVAL_TABLE = 60000;

    var $wrapper,
        $content,
        $contentActions,
        $reportTab,
        $reportContent,
        $agentContent;

    var loaded = false,
        intervalTable = null,
        tableIsLoading = false;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $wrapper = $('#pushing-report-wrapper');
        $content = $wrapper.find('.tab-wrapper-content');
        $reportTab = $('a[href="#pushing-report"]').parent();
        $reportContent = $content.children('.pushing-report-content');
        $agentContent = $content.children('.pushing-agent-content');
        _handleEvents();

    }

    function _handleEvents() {

        $('a[href="#pushing-report"]').on('shown.bs.tab', function () {
            iniLoad();
        });

        if ($('a[href="#pushing-report"]').parent().hasClass('active')) {
            iniLoad();
        }

    }

    function iniLoad() {
        if (!loaded) {
            _loadPanel();
        }
    }


    function _loadPanel() {

        $.ajax({
            url: Routing.generate('management_supervisor_pushing_report_index'),
            type: 'GET',
            beforeSend: function () {
                $wrapper.addClass('loading');
                $content.html('');
            },
            success: function (response) {

                loaded = true;
                $content.html(response.html);
                $contentActions = $wrapper.find('.tab-wrapper-content-actions');

                _afterLoadPanel();

                tableIsLoading = true;
            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $wrapper.removeClass('loading');
            }
        });

    }

    function _reloadPanel() {

        $.ajax({
            url: Routing.generate('management_supervisor_pushing_report_index'),
            type: 'GET',
            success: function (response) {

                loaded = true;
                $content.html('');
                $content.html(response.html);
                $contentActions = $wrapper.find('.tab-wrapper-content-actions');

                _afterReloadPanel();

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
        });

    }

    function _afterLoadPanel() {

        _handleEventsAfterLoadPanel();
    }

    function _afterReloadPanel() {
        _closeButton();
        _getUserStats();
        setView();
    }

    function _handleEventsAfterLoadPanel() {

        intervalTable = setInterval(function () {
            if (!_tabIsActive()) {
                return;
            }
            _reloadPanel();
        }, INTERVAL_TABLE);

        _getUserStats();

        setView();

        $('.change-view-automation [name="view-mode-detailed-pushing"]').on('change', function () {
            changeView($(this));
        });
    }

    function changeView($t)
    {
        if($t.is(":checked")) {
            Cookies.set('pushing_report_detailed_view', 1);
            $('.pushing-report-content .pr-agents').show();
        }
        else {
            Cookies.set('pushing_report_detailed_view', 0);
            $('.pushing-report-content .pr-agents').hide();
        }
    }

    function _closeButton()
    {
        $('.pr-pop-close-button').on('click', function (e) {
            e.preventDefault();
            var agentContent = $('.pushing-agent-pop');

            agentContent.addClass('hidden');
        })
    }

    function setView()
    {
        var $t = $('.change-view-automation [name="view-mode-detailed-pushing"]');
        changeView($t);
    }

    function _getUserStats() {
        $('.pr-agent-av').on('click', function (e) {
            e.preventDefault();

            var $t = $(this);
            var agentContent = $('.pushing-agent-pop');
            var agentSpinner = $('.pushing-agent-pop-spinner');

            $.ajax({
                url: Routing.generate('management_supervisor_pushing_report_user', {'id': $t.data('agent-id')}),
                type: 'GET',
                beforeSend: function () {
                    agentSpinner.removeClass('hidden');
                },
                success: function (response) {
                    agentContent.html(response.html);
                    agentContent.removeClass('hidden');
                    _closeButton();
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    agentSpinner.addClass('hidden');
                }
            });

        });
    }

    function _tabIsActive() {
        return $('a[href="#pushing-report"]').parent().hasClass('active');
    }

    function _afterLoadTable() {

        if (tableIsLoading) return false;

    }

    $(function () {
        _init();
    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {}

});
