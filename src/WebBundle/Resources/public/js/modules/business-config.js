var BusinessConfig = (function () {

    var $content,
        $configContent,
        $formFilter,
        $programSelect,
        $updateConfigBtn,
        $platformSelect,
        timeouts = {},
        lastActiveTab,
        $addKeyModal,
        $addKeyButton,
        $addKeyForm,
        $saveKeyButton,
        $copyConfigsButton,
        $copyConfigsModal,
        $copyConfigsForm;

    var ajaxLoadPrograms,
        ajaxLoadContent;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $formFilter = $('#business-config-filter-form');
        $platformSelect = $('#business_config_platform');
        $programSelect = $('#business_config_program');
        $configContent = $('#business-config-content');
        $content = $configContent.find('.business-config-content');
        $updateConfigBtn = $('#update-business-config');
        $addKeyButton = $('#add-key-business-config');
        $addKeyForm = $('#key-add-form');
        $saveKeyButton = $('#key-add-save-form');
        $addKeyModal = $('#add-key-modal');
        $copyConfigsButton = $('#copy-business-configs');
        $copyConfigsModal = $('#copy-configs-modal');
        $copyConfigsForm = $('#copy-configs-form');

        _handleEvents();
        _afterInit();
    }

    function _handleEvents() {

        $platformSelect.on("change", function (e) {
            e.preventDefault();
            _loadPrograms($(this).val(), _loadContent);
        });

        $programSelect.on('change', function () {
            _loadContent();
        });

        $addKeyButton.on('click', function (e) {
            e.preventDefault();
            $addKeyModal.modal('show');
        });

        $copyConfigsButton.on('click', function (e) {
            e.preventDefault();
            $copyConfigsModal.modal('show');
        });

        $copyConfigsForm.on('submit', function (e) {

            e.preventDefault();

            var $t = $(this),
                $btn = $t.find('button'),
                ladda = Ladda.create($btn[0]);

            $.ajax({
                url: Routing.generate('business_config_copy'),
                type: 'POST',
                data: $copyConfigsForm.serialize(),
                beforeSend: function () {
                    ladda.start();
                },
                /**
                 * @param {Object} response
                 * @param {boolean} response.status
                 * @param {string} response.message
                 */
                success: function (response) {

                    if(response.status) {
                        toastr.success(response.message);
                    }
                    else {
                        toastr.warning(response.message);
                    }

                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    ladda.stop();
                }
            });

        });

        $saveKeyButton.on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url:Routing.generate('business_config_key_add'),
                data: $addKeyForm.serialize()
            }).done(function () {
                $updateConfigBtn.click();

                $addKeyModal.modal('hide');
            });
        });

        $updateConfigBtn.on('click', function (e) {
            e.preventDefault();

            var ladda = Ladda.create(this);

            $.ajax({
                url: Routing.generate('business_config_refresh'),
                type: 'GET',
                beforeSend: function () {
                    ladda.start();
                },
                /**
                 * @param {Object} response
                 * @param {int} response.platforms - liczba zaktualizowanych platform
                 * @param {int} response.programs - liczba zaktualizowanych programów
                 */
                success: function (response) {

                    if(response.platforms === 0 && response.programs === 0) {
                        toastr.info('Wszystko jest aktualne.');
                    }
                    else {
                        toastr.success('Zaktualizowano ' + response.platforms + ' platform i ' + response.programs + ' programów.');
                    }

                    _loadContent();

                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    ladda.stop();
                }
            });

        })

    }

    function _afterInit() {

        $platformSelect.trigger('change');

    }

    function _loadPrograms(platformId, callback) {

        if (ajaxLoadPrograms){
            ajaxLoadPrograms.abort();
        }

        ajaxLoadPrograms = $.ajax({
            url: Routing.generate('business_config_get_programs', {platform: platformId}),
            type: 'GET',
            success: function (response) {

                $programSelect.select2('destroy').empty().select2({
                    placeholder: 'Program',
                    allowClear: true,
                    data: response.options
                });

            },
            error: function (response) {
                if(response.statusText !== "abort") {
                    handleAjaxResponseError(response);
                }
            },
            complete: function () {
                if(typeof callback === "function") {
                    callback();
                }
            }
        });

    }

    function _loadContent() {

        if (ajaxLoadContent){
            ajaxLoadContent.abort();
        }

        ajaxLoadContent = $.ajax({
            url: Routing.generate('business_config_render_content', {
                platform: $platformSelect.val(), programId: $programSelect.val()
            }),
            type: 'GET',
            beforeSend: function () {
                $configContent.addClass('loading');
                $updateConfigBtn.prop('disabled', true);
                // $formFilter.addClass('loading');
            },
            success: function (response) {
                $content.html(response);
                _afterLoadContent();
            },
            error: function (response) {
                if(response.statusText !== "abort") {
                    handleAjaxResponseError(response);
                }
            },
            complete: function () {
                $configContent.removeClass('loading');
                $updateConfigBtn.prop('disabled', false);
                // $formFilter.removeClass('loading');
            }
        });

    }

    function _afterLoadContent() {

        if(lastActiveTab) {
            var $tab = $content.find('.nav-tabs li a[data-tab-name="'+lastActiveTab+'"]');
            if($tab.length) {
                $tab.tab('show');
            }
        }

        $content.find('.nav-tabs li a').on('shown.bs.tab', function() {
            lastActiveTab = $(this).attr('data-tab-name');
        });

        $content.find('form').on('submit', function (e) {
            e.preventDefault();
        });

        dateTimePickerOptions.onSelect = function (d1, d2, event) {
            event.$el.trigger('change');
        };

        datePickerOptions.onSelect = function (d1, d2, event) {
            event.$el.trigger('change');
        };

        $content.find('.datepicker-control').datepicker(datePickerOptions);
        $content.find('.datetimepicker-control').datepicker(dateTimePickerOptions);

        $content.find('.datetimepicker-control, .datepicker-control').each(function (i, ele) {
            if(ele.value)
            {
                var date = new Date(ele.value);
                if(!isNaN( date.getTime()))
                {
                    $(ele).data('datepicker').selectDate(date);
                }
            }
        });

        $content.find('.select2').select2({
            placeholder: '---',
            allowClear: true
        });

        $content.find('.form-control').on('change', function (e) {
            e.preventDefault();

            var $t = $(this),
                $label,
                value,
                _type = $t.attr('data-type');

            if($t[0].type === "checkbox") {
                // $label = $t.parent().prev().find('.label');
                value = $t.is(":checked");
            }
            else {
                value = $t.val();
                // $label = $t.closest('td').prev().find('.label');
            }

            $.ajax({
                url: Routing.generate('business_config_save', {
                    businessConfig: $t.attr('data-config-id')
                }),
                data: {
                    type: _type,
                    value: value
                },
                type: 'POST',
                beforeSend: function () {
                    if(timeouts[$t[0].id]) {
                        clearTimeout(timeouts[$t[0].id]);
                    }
                    // $label.fadeOut(0);
                },
                success: function () {
                    // $label.stop().fadeIn(500);
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {

                    toastr.success('', 'Zapisano');

                }
            });

        });
    }

    $(function () {
        _init();
    });

    return {}

});
