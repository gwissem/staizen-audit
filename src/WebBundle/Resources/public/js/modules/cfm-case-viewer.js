$.fn.dataTable.ext.errMode = 'none';

var CFMCaseViewer = (function () {

    $.fn.dataTable.ext.errMode = "none";

    var $caseSearchForm,
        tableInstance,
        tableInstanceApi,
        $managementNavTabs,
        $managementTabContent,
        $casesTable,
        cfmCasesSearchLadda,
        emailPreviewModule,
        $claimModal,
        $noteModal,
        $finishRentalModal,
        $customAjaxModal,
        $newCaseFormModal,
        lastScrollTop = 0,
        loadingServiceForm;

    var caseNoteAction = '<div class="action-row col-sm-12">' +
            '<div class="editable-note" data-type="textarea"' +
        '                data-name="note"' +
        '                data-value="__VALUE__"' +
        '                data-title="Notatka do sprawy"' +
        '                data-pk="__ID__"' +
        '                data-url="__URL__"></div>' +
        '</div>';

    var claimNumberAction = '<div class="action-row col-sm-12">' +
        '<div class="editable-claim-number" data-type="text"' +
        '                data-name="case_number"' +
        '                data-value="__VALUE__"' +
        '                data-title="Numer szkody"' +
        '                data-pk="__ID__"' +
        '                data-url="__URL__"></div>' +
        '</div>';

    var mvInfo = '<div class="text-center">' +
        '<button type="button" data-root-id="__ID__" class="btn btn-info btn-xs" title="Informacje o wynajmie"><i class="fa fa-info"></i></button>' +
        '</div>';


    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $caseSearchForm = $('#case_search');
        $managementNavTabs = $('.management-nav-tabs');
        $managementTabContent = $('.management-tab-content');
        $casesTable = $('#cases-table');
        cfmCasesSearchLadda = Ladda.create($caseSearchForm.find('.cfm-cases-search')[0]);
        $noteModal = $('#new-note-modal');
        $finishRentalModal = $('#finish-rental-modal');
        $claimModal = $('#new-complaint-modal');
        $customAjaxModal = $('#custom-ajax-modal');
        $newCaseFormModal = $('#new-case-from-viewer');

        emailPreviewModule = EmailPreviewModule();

        if(typeof localStorage !== "undefined" && localStorage) {
            _loadFilters();
        }

        dateTimePickerOptions.onSelect = function (d1, d2, event) {
            event.$el.trigger('change');
        };

        datePickerOptions.onSelect = function (d1, d2, event) {
            event.$el.trigger('change');
        };

        $('.date-time-picker').datepicker(dateTimePickerOptions);
        $('.date-picker').datepicker(datePickerOptions);

        $('.date-time-picker, .date-picker').each(function (i, ele) {
            if(ele.value)
            {
                var date = new Date(ele.value);
                if(!isNaN( date.getTime()))
                {
                    $(ele).data('datepicker').selectDate(date);
                }
            }
        });

        updateRefreshDimensionDate();
        setInterval(function () {
            updateRefreshDimensionDate();
        }, 60000);

        _handleEvents();

        _initCasesTable();

        _initQueryCase();

    }

    function _initQueryCase() {

        var caseNumber = parseInt(getParameterByName('case-number'));

        if(caseNumber) {
            _appendNewCase(caseNumber);
        }

    }

    function updateRefreshDimensionDate(){
        $.ajax({
            url: Routing.generate('get_refresh_dimension_date'),
            type: "GET",
            success: function (response) {
                $('#dimension-refresh-date').text(response);
            }
        });
    }

    function loadNotes(rootId) {

        var $tab = $('#tab-' + rootId),
            $notesBoxPortlet = $tab.find('.case-notes-portlet'),
            $notesBox = $tab.find('.case-notes'),
            $noteFilters = $notesBoxPortlet.find('.notes-filters-wrapper'),
            $addChatMessageBtn = $notesBoxPortlet.find('.add-chat-message');

        if(!$tab.length) return;

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceGetNotesAndInfo', {processInstanceId: rootId, displayInfo: false}),
            type: "GET",
            beforeSend: function() {
                $noteFilters.hide();
                $addChatMessageBtn.hide();
            },
            success: function (response) {

                var amountOfNotes = 0;
                var caseNoteModule = CaseNoteModule(null, null, {
                        $wrapper: $notesBox,
                        $noteFilters: $noteFilters,
                        $addChatMessage : $addChatMessageBtn,
                        noteRootId: response.notes.root,
                        getRootId: function () {
                            return rootId;
                        }
                    });

                $.each(response.notes.list, function (key, note) {

                    if (note.content) {
                        amountOfNotes++;
                        caseNoteModule.appendNote(note, 'prepend', key, { withPriority: false });
                    }

                });

                if($notesBoxPortlet.length) {
                    $notesBoxPortlet.on('click', '.preview-email', function () {

                        $('.note-inbox').attr('data-document-id', this.getAttribute('data-attachment-id'));
                        if (typeof CustomEvent !== 'undefined') {
                            window.document.body.dispatchEvent(new CustomEvent("refresh-note-inbox"));
                        }
                    });
                }

                initNoteContainer($notesBoxPortlet);

                if(amountOfNotes > 0) {
                    $notesBoxPortlet.find('.expand-all').show();
                    $noteFilters.show();
                }

                $addChatMessageBtn.show();

            },
            complete: function () {
                $notesBoxPortlet.find('.box-spinner').remove();
            }
        });

    }

    function initNoteContainer($container) {

        $container.find(".notification-container").accordion({
            header: "h4",
            heightStyle: "content",
            collapsible: true,
            beforeActivate: function (event, ui) {

                var currHeader, currContent;

                if (ui.newHeader[0]) {
                    currHeader = ui.newHeader;
                    currContent = currHeader.next('.ui-accordion-content');
                } else {
                    currHeader = ui.oldHeader;
                    currContent = currHeader.next('.ui-accordion-content');
                }

                var isPanelSelected = currHeader.attr('aria-selected') === 'true';

                currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

                currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

                currContent.toggleClass('accordion-content-active', !isPanelSelected);

                return false;
            }
        });

        $container.on('click', '.note-title', function () {
            var $t = $(this);
            $t.next('.note-content').toggle();

            if ($t.hasClass('absolute-header')) {
                $t.removeClass('absolute-header');
                $t.find('.header-text').addClass('visible');
                $t.find('.date-and-time').removeClass('visible');
            } else {
                $t.addClass('absolute-header');
                $t.find('.header-text').removeClass('visible');
                $t.find('.date-and-time').addClass('visible');
            }
        })

    }

    function _handleEvents() {

        $('#cases-table').on('click', 'span[data-case-id]', appendNewCase);

        $managementNavTabs.on('click', '.close-tab', closeCaseTab);

        $caseSearchForm.find('.clear-cases-search').on('click', function (e) {
            e.preventDefault();
            $caseSearchForm[0].reset();
            $caseSearchForm.find('select').trigger('change');
        });

        $noteModal.on('show.bs.modal', _setCaseIdInModal);
        $finishRentalModal.on('show.bs.modal', _setCaseIdInModal);
        $claimModal.on('show.bs.modal', _setCaseIdInModal);

        $caseSearchForm.on('submit', function (e) {

            e.preventDefault();

            if(cfmCasesSearchLadda.isLoading()) return;

            tableInstanceApi.ajax.reload();

        });

        if(typeof localStorage !== "undefined" && localStorage) {
            $caseSearchForm.find('input,select').on('change', function () {
                delayedCallback(_saveFilters, 500)
            });
        }

        $('#finish-rental-save').on('click', _finishRental);
        $('#new-note-modal-save').on('click', _appendNewNote);
        $('#new-complaint-modal-save').on('click', _appendNewNote);
        $('.export-data').on('click', exportData);

        $('a[href="#case-searcher"]').on('click', function () {
            if(lastScrollTop > 0) {
                setTimeout(function () {
                    window.scrollTo(0,lastScrollTop);
                    lastScrollTop = 0;
                }, 50);
            }
        });

        $('#btn-viewer-create-case').on('click', loadModalWithCaseForm);

        $newCaseFormModal.on('submit', 'form', onCreateNewCase);

    }

    function onCreateNewCase(e) {

        e.preventDefault();

        var $form = $(this),
            ladda = Ladda.create($form.find('button[type="submit"]')[0]);

        $.ajax({
            url: Routing.generate('ccv_create_case'),
            type: "POST",
            data: $form.serialize(),
            beforeSend: function () {
                ladda.start();
            },
            success: function (response) {

                if(response.rootId) {

                    $newCaseFormModal.modal('hide');

                    swal({
                        title: "Sprawa została założona ",
                        text: "Numer sprawy to: " + getCaseNumberFromString(response.rootId) + ". Wyświetli się ona w Atlas Viewer zaraz po tym jak jeden z konsultantów rozpocznie nad nią pracę.",
                        icon: "success"
                    }).then(function () {

                    });

                }
                else {
                    prepareForm(response.form);
                }

            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {
                ladda.stop();
            }
        });

    }

    function prepareForm(html) {

        $newCaseFormModal.find('.modal-body').html(html);
        $newCaseFormModal.modal('show');

        $newCaseFormModal.find('.date-picker').datepicker(datePickerOptions);
        $newCaseFormModal.find('.datetime-picker').datepicker(dateTimePickerOptions);

        $newCaseFormModal.find('.date-picker, .datetime-picker').fixDatePickerValue();

        $newCaseFormModal.find('.select2').select2();

    }

    function loadModalWithCaseForm(e) {

        var $t = $(this),
            ladda = Ladda.create($t[0]);

        $.ajax({
            url: Routing.generate('ccv_create_case'),
            type: "GET",
            beforeSend: function () {
                ladda.start();
                $newCaseFormModal.find('.modal-body').html('');
            },
            success: function (response) {

                prepareForm(response);

            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {
                ladda.stop();
            }
        });

    }

    function exportData(e) {

        e.preventDefault();

        var url = Routing.generate('ccv_export_view') + '?' + $caseSearchForm.serialize();
        var win = window.open(url, '_blank');

        if(win) {
            win.focus();
        }

    }

    function _setCaseIdInModal(e) {
        var $button = $(e.relatedTarget);
        var rootId = $button.attr('data-root-id');
        $(e.target).find('[name="note[case_id]"]').val(rootId);
    }

    function _appendNewNote(e) {

        e.preventDefault();

        var $t = $(this),
            $modal = $t.closest('.modal'),
            $form = $modal.find('form'),
            rootId = $form.find('[name="note[case_id]"]').val(),
            laddaBtn = Ladda.create($t[0]);

        $.ajax({
            url: Routing.generate('ccv_add_note'),
            type: "POST",
            data: $form.serialize(),
            beforeSend: function () {
                laddaBtn.start();
            },
            success: function () {

                $form[0].reset();
                $modal.modal('hide');

                $('#tab-' + rootId + ' .refresh-case-details').trigger('click');

            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {
                laddaBtn.stop();
            }
        });

    }

    function _finishRental(e) {

        e.preventDefault();

        var $t = $(this),
            $modal = $t.closest('.modal'),
            $form = $modal.find('form'),
            rootId = $form.find('[name="note[case_id]"]').val(),
            laddaBtn = Ladda.create($t[0]);

        $.ajax({
            url: Routing.generate('ccv_finish_rental'),
            type: "POST",
            data: {'taskId': $('button.finish-rental').attr('data-id')},
            beforeSend: function () {
                laddaBtn.start();
            },
            success: function (result) {

                $modal.modal('hide');
                $('.refresh-case-details').trigger('click');
                toastr.success(result.message);
            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {
                laddaBtn.stop();
            }
        });

    }

    function _saveFilters() {

        localStorage.setItem('cfm_case_searcher', JSON.stringify($caseSearchForm.serializeArray()));

    }

    function _loadFilters() {

        var formData = localStorage.getItem('cfm_case_searcher');

        if(!formData) return;

        $.each(JSON.parse(formData), function (i, ele) {
            $caseSearchForm.find('[name="'+ele.name+'"]').val(ele.value);
        });

    }

    var liCase = '<li><a href="#tab-__ID__" data-toggle="tab" data-case-number="__CASE_NUMBER__">__NAME__<span class="close-tab"><i class="fa fa-close"></i></span></a></li>';
    var tabCase = '<div class="tab-pane" id="tab-__ID__"><div class="row"><div class="col-md-12"><div class="portlet light">' + miniLoaderTemplate + ' </div></div></div></div>';

    function closeCaseTab(e) {

        e.preventDefault();

        var $a = $(this).parent(),
            caseId = $a.attr('data-case-number'),
            isActive = $a.parent().hasClass('active');


        $managementTabContent.find($a.attr('href')).remove();
        $a.parent().remove();

        if(isActive) {
            $('a[href="#case-searcher"]')[0].click();
        }

        tableInstance.find('#' + caseId).removeClass("selected");

    }

    function appendNewCase(e) {

        e.preventDefault();

        var id = parseInt($(this).attr('data-case-id')),
            $row = $(this).closest('tr');

        _appendNewCase(id, $row);

    }

    function _appendNewCase(id, $row) {

        lastScrollTop = window.scrollY;

        var caseNumber = getCaseNumberFromString(id),
            $tab = $('[href="#tab-' + id + '"]');

        if($tab.length) {
            $tab[0].click();
            _loadCase(id);
            return;
        }

        if($row && $row.length) {
            $row.addClass('selected').removeClass('active-case');
        }

        var newLiCase = liCase.replace('__ID__', id).replace('__CASE_NUMBER__', caseNumber).replace('__NAME__', caseNumber);
        var newTabCase = tabCase.replace('__ID__', id);

        var $newLi = $(newLiCase).appendTo($managementNavTabs);
        $managementTabContent.append(newTabCase);
        $newLi.find('a')[0].click();

        _loadCase(id);

    }

    function _loadCase(id) {

        return _loadCaseInNewTab(id).done(function (response) {

            var $tabContent = $('#tab-' + id);
            $tabContent.find('.portlet').html(response.html);

            $tabContent.find('.select2').select2();

            $tabContent.find('.case-services-portlet').on('click', '.service-row', function (e) {

                e.preventDefault();
                _showService($(this), $tabContent);

            });

            $tabContent.find('.refresh-case-details').on('click', {caseId: id}, _refreshCase);

            $tabContent.find('[name="coordinator-in-case"]').on('change', _changeCoordinator);

            $tabContent.find('.toggle-status-case-form').on('submit', _toggleStatusOfCase);

            $tabContent.find('#claim_number').on('change', _changeClaimNumber);

            $tabContent.find('.case-notes-portlet .expand-all').on('click', _toggleNotes);

            $tabContent.find('.documents-case-form').on('submit', _sendDocumentRequest);

            $tabContent.find('[name="is-total-damage"]').on('change', _changeTotalDamage);

            loadNotes(id);

        })

    }

    function _toggleNotes(e) {

        e.preventDefault();

        var $t = $(this);

        if($t.hasClass('expand')) {

            $t.addClass('collapse').removeClass('expand');

            $t.parent().find('.case-notes').find('.title').addClass('absolute-header').attr({
                "aria-selected": "true",
                "aria-expanded": "true"
            }).next().attr("aria-hidden", "false").show().next().next().show();


        }
        else if ($t.hasClass('collapse')) {

            $t.removeClass('collapse').addClass('expand');

            $t.parent().find('.case-notes').find('.title').removeClass('absolute-header').attr({
                "aria-selected": "false",
                "aria-expanded": "false"
            }).next().attr("aria-hidden", "true").hide().next().next().hide();

        }

    }

    function _changeCoordinator(e) {

        e.preventDefault();

        $.ajax({
            url: Routing.generate('ccv_change_coordinator'),
            type: "POST",
            data: {
                coordinator_id: $(this).val(),
                root_id: $(this).attr('data-root-id')
            },
            success: function (result) {
                toastr.success(result.message);
            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            }
        });

    }

    function loadAjaxModal(title, callback) {

        $customAjaxModal.find('.modal-title').text(title);
        $customAjaxModal.find('.modal-inner-content').html("");
        $customAjaxModal.addClass('loading');

        $('#custom-ajax-modal').modal('show');

        if(typeof callback === "function") {
            callback($customAjaxModal.find('.modal-inner-content')).done(function () {
                $customAjaxModal.removeClass('loading');
            });
        }

    }

    function _sendDocumentRequest(e) {

        e.preventDefault();

        var $t = $(this),
            ladda = Ladda.create($t.find('button')[0]),
            action = $t.find('input[name="action"]').val();

        if(action === "download_documents") {

            var rootId = $t.find('input[name="root_id"]').val();

            ladda.start();

            loadAjaxModal('Dokumenty w sprawie', function ($modalContent) {

                return $.ajax({
                    url: Routing.generate('ccv_documents_preview', { rootId: rootId }),
                    type: "GET",
                    success: function (result) {

                        $modalContent.html(result);

                    },
                    error: function(error) {
                        if(error.statusText !== "abort") {
                            handleAjaxResponseError(error);
                        }
                    },
                    complete: function () {
                        ladda.stop();
                    }
                });

            });

        }
        else {

            ladda.start();

            $.ajax({
                url: Routing.generate('ccv_documents_request'),
                type: "POST",
                data: $t.serialize(),
                success: function (result) {

                    toastr.success('Pomyślnie wysłano zapytanie o dokumenty');

                },
                error: function(error) {
                    if(error.statusText !== "abort") {
                        handleAjaxResponseError(error);
                    }
                },
                complete: function () {
                    ladda.stop();

                    if(action === "send_request") {
                        $t.find('button').prop('disabled', true);
                    }
                }
            });
        }

    }

    function _toggleStatusOfCase(e) {

        e.preventDefault();

        var $t = $(this),
            ladda = Ladda.create($t.find('button')[0]);

        ladda.start();

        $.ajax({
            url: Routing.generate('ccv_change_status_case'),
            type: "POST",
            data: $t.serialize(),
            success: function (result) {
                toastr.success(result.message);
                $t.closest('.tab-pane').find('.refresh-case-details').trigger('click');
            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {
                ladda.stop();
            }
        });

    }

    function _changeTotalDamage(e) {

        e.preventDefault();

        var $t = $(this);

        $.ajax({
            url: Routing.generate('ccv_change_total_damage'),
            type: "POST",
            data: {
                value: ($t.val()),
                root_id: $t.attr('data-root-id')
            },
            success: function (result) {
                toastr.success("Pomyślnie zapisano");
            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            }
        });


    }

    function _changeClaimNumber(e) {

        e.preventDefault();

        var $t = $(this);

        $.ajax({
            url: Routing.generate('ccv_change_claim_number'),
            type: "POST",
            data: {
                claim_number: $(this).val(),
                root_id: $(this).attr('data-root-id')
            },
            success: function (result) {
                toastr.success(result.message);

                if(result.refresh) {
                    $t.closest('.tab-pane').find('.refresh-case-details').trigger('click');
                }

            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            }
        });

    }

    function _showService($t, $tabContent) {

        var $previewOfService = $tabContent.find('.preview-of-service');

        $previewOfService.fadeIn(300).addClass('loading');

        var serviceId = $t.attr('data-service-id'),
            groupProcessId = $t.attr('data-group-process-id');

        $tabContent.find('.case-services-portlet').find('.service-row.active').removeClass('active');
        $t.addClass('active');

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceOverviewForCaseViewerService', {processId: groupProcessId, serviceId: serviceId}),
            type: "GET",
            beforeSend: function () {
                $previewOfService.find('.portlet-body').html('');
            },
            success: function (result) {

                try {
                    if(result.services) {

                        var services = result.services.filter(function (s) {
                            return (parseInt(s.id) === parseInt(groupProcessId));
                        });

                        if(services.length > 0) {
                            var service = services[0];
                            $previewOfService.find('.portlet-body').html(service.overview);
                        }
                    }
                }
                catch (e) {

                    if(e.message && e.stack) {
                        $.ajax({
                            url: window.location.origin + "/js-logger",
                            type: "POST",
                            data: {
                                msg: e.message + '. ' + e.stack,
                                url: window.location.href,
                                processId: groupProcessId,
                                browser_url: window.location.href,
                                browser_version: navigator.sayswho
                            }
                        });
                    }

                }

            },
            error: function(xhr) {

                toastr.error('Wystąpił problem z załadowaniem informacji o usłudze.');

                $.ajax({
                    url: window.location.origin + "/js-logger",
                    type: "POST",
                    data: {
                        msg: xhr.statusText  + " " + xhr.status + ", Service: " + serviceId,
                        url: window.location.href,
                        processId: groupProcessId,
                        browser_url: window.location.href,
                        browser_version: navigator.sayswho
                    }
                });

            },
            complete: function () {
                //
                // $caseInfoPortlet.find('a.collapse').first().click();
                $previewOfService.removeClass('loading');
            }
        });

        /**
         * Jednak funkcjonalność tymczasowo wyłączono
         */
        loadFormForService(groupProcessId, serviceId);

    }

    function loadFormForService(groupProcessId, serviceId) {

        var $caseServiceFormPortlet = $('.case-service-form-portlet');

        if(loadingServiceForm) {
            loadingServiceForm.abort();
            loadingServiceForm = null;
        }

        loadingServiceForm = $.ajax({
            url: Routing.generate('get_form_of_service', {groupProcessId: groupProcessId, serviceId: serviceId}),
            type: "GET",
            beforeSend: function () {
                $caseServiceFormPortlet.hide();
                $caseServiceFormPortlet.find('.portlet-body form').remove();
            },
            success: function (result) {

                if(result.form) {
                    $caseServiceFormPortlet.find('.portlet-body').html(result.form);
                    $caseServiceFormPortlet.show();
                }

            },
            error: function(response) {

                if(response.statusText !== "abort") {
                    toastr.error('Wystąpił problem z załadowaniem formularza do usługi.');
                }

            },
            complete: function () {

            }
        });

    }

    function _refreshCase(e) {

       var ladda = Ladda.create(this);

       ladda.start();

        _loadCase(e.data.caseId).error(function () {
            ladda.stop()
        });

    }

    function _loadCaseInNewTab(caseId) {

        return $.ajax({
            url: Routing.generate('ccv_find_case', {rootId: caseId}),
            type: 'GET',
            beforeSend: function () {

            },
            success: function (response) {

            },
            error: function(error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            },
            complete: function () {

            }
        });

    }

    function _renderCaseNote( data, type, row ) {
        data = (typeof data === "undefined") ? null : data;
        return caseNoteAction.replace('__ID__', row.id).replace('__VALUE__', data).replace('__URL__', Routing.generate('ccv_change_coordinators_note'));
    }

    function _renderClaimNumber( data, type, row ) {
        data = (typeof data === "undefined") ? null : data;
        return claimNumberAction.replace('__ID__', row.id).replace('__VALUE__', data).replace('__URL__', Routing.generate('ccv_change_claim_number'));
    }

    function _renderCaseNumber( data, type, row ) {
        return '<span data-case-id="'+row.id+'">' + data + '</span>';
    }

    function _prepareColumns() {

        var columns = $casesTable.data('table-columns');

        // custom  columns
        $.each(columns, function (i, ele) {

            if(ele.width === "") {
                delete(ele.width);
            }

            if(ele.data === "id") {
                ele.type = "integer";
                ele.visible = false;
            }
            else if (ele.data === "case_number") {
                ele.render = _renderCaseNumber;
            }
            else if (ele.data === "case_note") {
                ele.type = "text";
                ele.render = _renderCaseNote;
            }
            else if (ele.data === "claim_number") {
                ele.type = "text";
                ele.render = _renderClaimNumber;
            }
            else if (ele.data === "last_note_created") {
                ele.visible = false;
                ele.type = "date";
            }
            else if (ele.data === "last_update_at") {
                ele.type = "date";
            }


        });

        // columns.push({
        //     title: "Wynajem",
        //     width: '40px',
        //     render: function ( data, type, row ) {
        //         return mvInfo.replace('__ID__', row.id);
        //     }
        // });

        return columns;

    }

    function _initCasesTable() {

        var columns = _prepareColumns(),
            order = [];

        $.each(columns, function (i, ele) {

            if(ele.data === "id") {
                order = [[ i, 'desc']];
            }
            else if(ele.data === "last_note_created") {
                order = [[ i, 'desc']];
            }
            else if(ele.data === "last_update_at") {
                order = [[ i, 'desc']];
            }

        });

        $('#cases-table').on( 'error.dt', function ( e, settings, techNote, message ) {
            e.stopPropagation();
            console.error( message);
        } );

        tableInstance = $('#cases-table').dataTable({
            ajax: function(data, callback) {

                $.ajax({
                    url: Routing.generate('ccv_find_cases'),
                    type: 'GET',
                    data: $caseSearchForm.serialize(),
                    beforeSend: function () {
                        cfmCasesSearchLadda.start();
                    },
                    success: function (response) {

                        callback({
                            data: response.cases
                        });

                    },
                    error: function(error) {
                        if(error.statusText !== "abort") {
                            handleAjaxResponseError(error);
                        }
                        callback({
                            data: []
                        });
                    },
                    complete: function () {
                        cfmCasesSearchLadda.stop();
                    }
                });

            },
            autoWidth: false,
            order: order,
            columns: columns,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Brak wyników",
                "info": "Wyświetlono od _START_ do _END_ z _TOTAL_ wyników",
                "infoEmpty": "Nie znaleziono wyników",
                "infoFiltered": "(1 z _MAX_ wszystkich wierszy)",
                "lengthMenu": "_MENU_ wyników na stronę",
                "search": "Szukaj po znalezionych sprawach:",
                "zeroRecords": "No matching records found"
            },
            createdRow: function( row, data, dataIndex ) {

                if(typeof data.last_note_created === "undefined") return;

                if(data.last_preview === null) {
                    $(row).addClass( 'active-case' );
                }
                else if (new Date(data.last_note_created) > new Date(data.last_preview) ) {
                    $(row).addClass( 'active-case' );
                }

            },
            drawCallback: function( settings ) {

                $('#cases-table').find('.editable-note').editable({
                    placement: 'right',
                    mode: 'inline',
                    emptytext: '<span class="font-blue-dark">Dodaj notatkę <i class="fa fa-plus small"></i></span>',
                    inputclass: ''
                });

                $('#cases-table').find('.editable-claim-number').editable({
                    placement: 'right',
                    mode: 'inline',
                    emptytext: '<span class="font-blue-dark">Dodaj <i class="fa fa-plus small"></i></span>',
                    inputclass: ''
                });

            },
            rowId: 'case_number',
            buttons: [],
            "lengthMenu": [
                [5, 10, 20, 50, -1],
                [5, 10, 20, 50, "All"] // change per page values here
            ],
            "pageLength": 20
        });

        tableInstanceApi = tableInstance.api();

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {
        tableInstance: tableInstance,
        tableInstanceApi: tableInstanceApi,
        printPreview: function () {

            if($('.form-claim .tools .expand').length) {

                $('.form-claim .tools .expand')[0].click();

                setTimeout(function () {
                    window.print();
                }, 500);

            }
            else {
                window.print();
            }

        }
    }

});
