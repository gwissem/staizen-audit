/**
 * @class EmailPreviewModule
 */
var EmailPreviewModule = (function(inboxEditorModule, opts) {

    var $body,
        useIframe = false;

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function recalculateHeight($content) {

        if(!$content) {
            $content = $('.email-preview-wrapper');
        }

        var $widget = $content.closest('.widget-email-preview');

        var allHeight,
            _headerHeight,
            _viewInfoHeight,
            _attachedHeight,
            _hrHeight;

        if(opts && opts.fullscreen) {

            allHeight = $content.find('.inbox-body').height();
            _headerHeight = $content.find('.inbox-view-header').outerHeight(true);
            _viewInfoHeight = $content.find('.inbox-view-info').outerHeight(true);
            _attachedHeight = $content.find('.inbox-attached').outerHeight(true);
            _hrHeight = $content.find('.inbox-content .content-view > hr').outerHeight(true);

            if(allHeight < 400) {

                $content.find('.inbox-view').css('height', 200);

            }
            else {

                $content.find('.inbox-view').css('height', (allHeight - 30 - (_hrHeight + _headerHeight + _viewInfoHeight + _attachedHeight)));

            }

        }
        else if($widget.length) {

            allHeight = $widget.height();
            _headerHeight = $content.find('.inbox-view-header').outerHeight(true);
            _viewInfoHeight = $content.find('.inbox-view-info').outerHeight(true);
            _attachedHeight = $content.find('.inbox-attached').outerHeight(true);
            _hrHeight = $content.find('.inbox-content .content-view > hr').outerHeight(true);

            $content.find('.inbox-view').css('height', (allHeight - 30 - (_hrHeight + _headerHeight + _viewInfoHeight + _attachedHeight)));

        }

    }

    function refreshPreviewEmail($inboxes) {

        $inboxes.each(function (i, ele) {

            var $emailPreviewWrapper = $(ele),
                documentId = $emailPreviewWrapper.attr('data-document-id'),
                url;

            $emailPreviewWrapper.find('.inbox-content').html('');
            $emailPreviewWrapper.show();

            if(!documentId) {

                $emailPreviewWrapper.addClass('loaded');

                var errorHtml = '<div class="note note-warning">' +
                    '<h4 class="block">Błąd ładowania</h4> ' +
                    '<p>Żaden e-mail nie został podpięty pod sprawę</p></div>';

                $emailPreviewWrapper.find('.inbox-content').html(errorHtml);

                return true;
            }

            $emailPreviewWrapper.removeClass('loaded');

            if(useIframe) {
                url = Routing.generate('mailboxGetEmailPreview', {id: documentId, iframe: true});
            }
            else {
                url = Routing.generate('mailboxGetEmailPreview', {id: documentId});
            }

            $.ajax({
                url: url,
                type: "GET",
                success: function (result) {

                    if(result.success) {
                        $emailPreviewWrapper.find('.inbox-content').html(result.emailContent);
                        afterSuccessLoadEmail($emailPreviewWrapper);
                    }
                    else {

                        var errorHtml = '<div class="note note-danger">' +
                            '<h4 class="block">Błąd otwierania e-maila</h4> ' +
                            '<p>'+result.error+'</p></div>';

                        $emailPreviewWrapper.find('.inbox-content').html(errorHtml);
                    }

                },
                error: function (error) {
                    handleAjaxResponseError(error);
                },
                complete: function () {
                    $emailPreviewWrapper.addClass('loaded');
                    recalculateHeight($emailPreviewWrapper);
                }
            });

        })

    }

    function afterSuccessLoadEmail($emailPreviewWrapper) {

        $emailPreviewWrapper.find('#reply-inbox,#forward-inbox').on('click', function (e) {
            e.preventDefault();

            var dataString = $emailPreviewWrapper.find('.content-view').attr('data-json-email'),
                dataEmail,
                $t = $(this);

            if(dataString) {

                dataEmail = JSON.parse(dataString);

                if($t.attr('id') === "forward-inbox") {
                    dataEmail.subject = 'FW: ' + dataEmail.subject;
                    dataEmail.isForward = true;
                }
                else {
                    dataEmail.subject = 'Re: ' + dataEmail.subject;
                }

            }

            inboxEditorModule.showInboxEditor(dataEmail);

        });


        $emailPreviewWrapper.find('#forward-inbox').on('click', function (e) {
            e.preventDefault();

            // $('#inbox-forward-modal').modal('show');

        });

    }

    function eventsHandle() {

        // window.document.body.dispatchEvent(new CustomEvent("refresh-preview-email"));

        if($('.email-preview-wrapper').length > 0) {
            $('.email-preview-wrapper .close-inbox').on('click', function () {
                $(this).closest('.email-preview-wrapper').fadeOut(300);
            });
        }
        else {
            $body.on('click', '.email-preview-wrapper .close-inbox', function (e) {
                $(this).closest('.email-preview-wrapper').fadeOut(300);
            });
        }

        $body.on('refresh-preview-email', function () {
            refreshPreviewEmail($('.email-preview-wrapper.inbox').not('.note-inbox'));
        });

        $body.on('refresh-note-inbox', function () {
            refreshPreviewEmail($('.email-preview-wrapper.note-inbox'));
        });

        $('#confirm-forward-email').on('click', function (e) {
            e.preventDefault();

            var l = Ladda.create(this),
                $modal = $('#inbox-forward-modal');

            l.start();

            setTimeout(function () {

                l.stop();
                $modal.modal('hide');

            }, 2000);

        });

        funResizeHandle.addFunction('recalculateHeightInboxPreview', recalculateHeight);

    }

    function _closeInbox() {

        var $wrapper = $body.find(".email-preview-wrapper");

        if($wrapper.length && $wrapper.is(':visible')) {
            $wrapper.find(".close-inbox").trigger('click');
        }

    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function init() {

        if(typeof opts !== "undefined") {
            if(typeof opts.iframe !== "undefined") {
                useIframe = opts.iframe;
            }
        }

        $body = $('body');
        eventsHandle();
    }

    init();

    return {
        closeInbox: _closeInbox
    }

});

/**
 * @type {{updateGridItemsPosition, updateLocationOfItems, refreshVisibleInVirtualForm, hideItem, showItem}}
 */