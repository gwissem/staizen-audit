function StepRow (options, _scenario) {

    var _defaults,
        _options,
        _this,
        _template = '',
        $item;

    var _actionOptions = [
        {v: 'setVariant', l: 'Ustaw wariant'},
        {v: 'reloadForm', l: 'Przeładuj formularz po zapisaniu atrybutów'},
        {v: 'stopScenario', l: 'Zatrzymaj scenariusz'},
        {v: 'specialSituation', l: '"Trudna sprawa"'}
    ];

    var _stopScenarioOptions = [
        {v: 'normal_before', l: 'Normalnie (przed zapisaniem danych)'},
        {v: 'normal_after', l: 'Normalnie (po zapisaniu danych)'},
        {v: 'error', l: 'Jako błąd'}
    ];

    if (!(this instanceof StepRow)) {
        return new StepRow(options, _scenario);
    }

    _this = this;

    _init(options);

    this.getId = function () {
        return _options.id;
    };

    this.setItem = function ($i) {
        $item = $i;
        return this;
    };

    this.getItem = function () {
        return $item;
    };

    this.getData = function () {
        return _options;
    };

    this.afterInserted = function () {

        $item.hide().fadeIn(500);
        _listeners();

    };

    this.getConfig = function () {

        return {
            value: _options.value,
            action: _options.action,
            stepId: _options.stepId
        };

    };

    this.getTemplate = function () {
        return _template;
    };

    function _initTemplate () {

        _template = '' +
            '<li class="task-list-item" data-uid="' + _options.id + '">' +
                '<div class="row">' +
                    '<div class="col-md-2">' +
                        _stepIdTemplate() +
                    '</div>' +
                    '<div class="col-md-3">' +
                        _stepSelectActionTemplate() +
                    '</div>' +
                    '<div class="col-md-6">' +
                        _stepValueTemplate() +
                    '</div>' +
                    '<div class="col-md-1 task-status">' +
                        // '<a class="pending event-order" title="Order" href="javascript:;">' +
                        //     '<i class="fa fa-sort"></i>' +
                        // '</a>' +
                        '<a class="pending remove" title="Remove" href="javascript:;">' +
                          '<i class="fa fa-close"></i>' +
                        '</a>' +
                    '</div>' +
                '</div>' +
            '</li>';

    }

    function _stepIdTemplate() {

        return '' +
            '<div class="form-group form-md-line-input has-success">' +
            '<input type="text" value="' + _options.stepId + '" class="form-control" data-step="id" id="step_' + _options.id + '" placeholder="Step id">' +
            '<div class="form-control-focus"></div>' +
            '</div>';
    }

    function _stepSelectActionTemplate() {

        return '' +
            '<div class="form-group m-b-0">' +
                '<select class="form-control select2" data-step="action" id="action_' + _options.id + '">' +
                    _getActionOptions() +
                '</select>' +
            '</div>';

    }

    function _stepValueTemplate() {

        var html = '<div class="value-box">';

        switch (_options.action) {
            case 'setVariant' : {

                html += '' +
                    '<div class="form-group form-md-line-input has-success">' +
                        '<input type="number" class="form-control" data-step-value="variant" value="' + _options.value.variant + '" id="value_variant_' + _options.id + '" placeholder="Numer wariantu">' +
                        '<div class="form-control-focus"> </div>' +
                    '</div>';
                break;

            }
            case 'reloadForm' : {

                html += '';
                    // '<div class="form-group form-md-line-input has-success">' +
                    // '<input type="number" class="form-control" data-step-value="reloadForm" value="' + _options.value.when + '" id="value_variant_' + _options.id + '" placeholder="Numer wariantu">' +
                    // '<div class="form-control-focus"> </div>' +
                    // '</div>';
                break;

            }
            case 'stopScenario' : {

                html += '' +
                    '<div class="form-group m-b-0">' +
                    '<select class="form-control select2" data-step-value="stop_type" id="stop_type_' + _options.id + '">' +
                    _getStopScenarioOptions() +
                    '</select>' +
                    '</div>';
                break;

            }
        }

        html += '</div>';

        return html;

    }

    function _getActionOptions() {

        return _getOptions(_actionOptions, 'action');

    }

    function _getStopScenarioOptions() {

        return _getOptions(_stopScenarioOptions, 'value');

    }

    function _getOptions(arr, type) {

        var options = '';

        $.each(arr, function (i, ele) {

            var disabled = (ele.d) ? 'disabled' : '';

            if(ele.v === _options[type]) {
                options += '<option value="' + ele.v + '" selected="" ' + disabled + '>' + ele.l + '</option>';
            } else {
                options += '<option value="' + ele.v + '" ' + disabled + '>' + ele.l + '</option>';
            }

        });

        return options;
    }

    function _changeStepId(e) {

        e.preventDefault();
        _options.stepId = this.value;

    }

    function _listeners () {

        $item.find('[data-step="id"]').on('change', _changeStepId);
        $item.find('[data-step="action"]').select2().on('change', _changeAction);

        _valuesListeners();

    }

    function _valuesListeners() {

        switch (_options.action) {
            case "setVariant" : {

                $item.find('[data-step-value="variant"]').on('change', function () {
                    _options.value.variant = this.value;
                });

                break;
            }
            case "stopScenario" : {

                $item.find('[data-step-value="stop_type"]').on('change', function () {
                    _options.value = this.value;
                });

                break;
            }

        }

    }

    function _changeAction() {

        var $t = $(this);

        _options.action = $t.val();

        switch (_options.action) {
            case "setVariant" : {

                _options.value = {
                    variant: ''
                };

                break;
            }
            case "reloadForm" : {

                _options.value = {};
                break;
            }
            case "stopScenario" : {
                _options.value = _stopScenarioOptions[0].v;
                break;
            }
            case "specialSituation" : {
                _options.value = {};
                break;
            }
        }

        var htmlValueBox = _stepValueTemplate();
        $item.find('.value-box').replaceWith($(htmlValueBox));

        _valuesListeners();

    }

    function _initDefaults () {

        _defaults = {
            stepId: '',
            action: "setVariant",
            value: {
                variant: ''
            }
        };

        _options = $.extend(_options, _defaults);

    }

    function _setOptions (options) {
        if (typeof options !== "undefined") {
            _options = $.extend(_options, options);
            _options['id'] = guid();
        }
    }

    function _init (options) {

        _initDefaults();
        _setOptions(options);
        _initTemplate();

    }
}