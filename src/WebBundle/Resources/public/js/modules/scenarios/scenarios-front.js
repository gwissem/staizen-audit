var ScenarioFront = (function () {

    var $scenarioIconWrapper,
        $scenarioModal,
        $availableScenariosTab,
        modalLoaded = false,
        $currentRunScenario,
        $scenarioLogger,
        $scenarioSearch,
        $scenarioLoggerTbody,
        scenarioLoggerPrototype,
        $scenarioProgressBar,
        $stopCurrentScenario,
        requestAjaxScenario,
        currentScenarioId,
        logNumber = 0,
        spinnerHtml = '',
        searchAjax;

    function _init() {

        $scenarioModal = $('#scenario-runner-modal');
        $scenarioIconWrapper = $('#scenario-icon-wrapper');

        _handleEvents();

    }

    function _handleEvents() {

        $scenarioIconWrapper.on('click', function () {
            $scenarioModal.modal('show');
            iniModal();
        })

    }

    function iniModal() {
        if(!modalLoaded) {
            modalLoaded = true;
            _loadPanel();
        }
        else {
            _loadAvailableScenarios();
        }
    }

    function _loadPanel() {

        _afterLoadedPanel();

    }

    function _afterLoadedPanel() {

        $availableScenariosTab = $("#available-scenarios-tab");
        spinnerHtml = $availableScenariosTab.attr('data-spinner-html');
        $currentRunScenario = $('#current-run-scenario');
        $scenarioLogger = $('#scenario-logger');
        $scenarioSearch = $('#scenario-search');

        _loadAvailableScenarios();
        _modalListeners();

    }


    function _searchScenarios(e) {

        e.preventDefault();
        delayedCallback(_loadAvailableScenarios, 300);

    }

    function _loadAvailableScenarios() {

        var url,
            data = {'front-mode': true},
            searchValue = $scenarioSearch.val();

        if(searchValue) {
            url = Routing.generate('management_admin_panel_scenarios_builder_find_scenarios_by_name');
            data.name = searchValue
        }
        else {
            url = Routing.generate('management_admin_panel_scenarios_builder_available_scenarios');
        }

        if(searchAjax) {
            searchAjax.abort();
        }

        searchAjax = $.ajax({
            url: url,
            data: data,
            type: 'GET',
            beforeSend: function () {
                $availableScenariosTab.find('.mt-list-container').html($availableScenariosTab.attr('data-spinner'));
            },
            success: function (response) {

                if(response.list) {
                    $availableScenariosTab.find('.mt-list-container').html(response.list);

                    if(currentScenarioId) {
                        $availableScenariosTab.find('.mt-list-item[data-id="'+currentScenarioId+'"]').addClass('active');
                    }

                }

            },
            error: function (response) {
                if(response.statusText !== "abort") {
                    $availableScenariosTab.find('.mt-list-container').html('');
                    handleAjaxResponseError(response);
                }
            },
            complete: function () {
                // ladda.stop();
            }
        });

    }

    function _onClickOpenResultScenario(e) {

        e.preventDefault();

        var $t = $(this);
        $t.prop('disabled', true);

        $scenarioModal.modal('hide');

        reserveAndGetTask({
            id: parseInt($t.attr('data-process-id'))
        },null, function () {
            $t.prop('disabled', false);
        })

    }

    // function _onOpenPdf(e) {
    //
    //     var $t = (this),
    //         scenarioInstanceId = $t.attr('data-scenario-instance-id');
    //
    //     var url = Routing.generate('management_admin_panel_scenarios_builder_download_pdf', {scenarioInstanceId: scenarioInstanceId});
    //
    //     Object.assign(document.createElement('a'), { target: '_blank', href: url}).click();
    //
    // }

    function _modalListeners() {

        $availableScenariosTab.on('click', '.mt-list-item', _initCurrentScenario);

        $currentRunScenario.find('.open-result-scenario').on('click', _onClickOpenResultScenario);

        $scenarioSearch.on('keyup', _searchScenarios);

        // $currentRunScenario.find('.open-pdf-scenario').on('click', _onOpenPdf);

    }

    function _initCurrentScenario() {

        var $t = $(this),
            _scenarioId = $t.attr('data-id');

        if(currentScenarioId === _scenarioId) {
            $('[href="#current-run-scenario"]').trigger('click');
            return;
        }

        $('#available-scenarios-tab .mt-list-item.active').removeClass('active');
        $t.addClass('active');

        currentScenarioId = _scenarioId;

        $('[href="#current-run-scenario"]').removeClass('disabled').trigger('click');

        var html = $currentRunScenario.attr('data-prototype-current-scenario');

        html = html.replace('__NAME__', $t.attr('data-name'))
            // .replace('__USER__', $t.attr('data-user'))
            .replace('__DESCRIPTION__', $t.attr('data-description'))
            .replace('__SCENARIO_ID__', _scenarioId)
            // .replace('__UPDATED_AT__', $t.attr('data-updated-at'));

        $currentRunScenario.find('.current-run-scenario-content').html(html);

        scenarioLoggerPrototype = $scenarioLogger.attr('data-prototype-logger');
        $scenarioLoggerTbody = $scenarioLogger.find('tbody');
        $scenarioProgressBar = $('.scenario-progress-bar');
        $stopCurrentScenario = $('#stop-current-scenario');
        $stopCurrentScenario.prop('disabled', true);
        $currentRunScenario.find('.scenario-result').hide();
        $scenarioLoggerTbody.html('');
        $scenarioProgressBar.css("width", 0);

        _initListenerOnCurrentScenario();

    }

    function _initListenerOnCurrentScenario() {

        $currentRunScenario.find('#run-current-scenario').on('click', function (e) {

            logNumber = 0;
            $scenarioProgressBar.css("width", 0);
            $scenarioLoggerTbody.html('');
            $currentRunScenario.find('.scenario-result').hide();

            var ladda = Ladda.create(this),
                $form = $currentRunScenario.find('.current-run-scenario-content form');

            var last_response_len = false;

            // Must be app.php or app_dev.php for no-gzip Response
            var url = Routing.generate('management_admin_panel_scenarios_front_run_scenario');
            if(url.indexOf('/app.php') === -1 && url.indexOf('/app_dev.php') === -1) {
                url = '/app.php' + url;
            }

            requestAjaxScenario = $.ajax({
                url: url,
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function() {
                    ladda.start();
                    $stopCurrentScenario.prop('disabled', false);
                },
                xhrFields: {
                    onprogress: function(e)
                    {

                        var this_response, response = e.currentTarget.response;

                        if(last_response_len === false)
                        {
                            this_response = response;
                            last_response_len = response.length;
                        }
                        else
                        {
                            this_response = response.substring(last_response_len);
                            last_response_len = response.length;
                        }

                        _appendLog(this_response);

                    }
                },
                success: function(result) {
                    // console.log(result);
                },
                error: function(error) {
                    if(error.statusText !== "abort") {
                        handleAjaxResponseError(error);
                    }
                },
                complete: function () {
                    ladda.stop();
                    $stopCurrentScenario.prop('disabled', true);
                }
            });

        });

        $stopCurrentScenario.on('click', _onClickStopScenario);
    }

    function _onClickStopScenario(e) {

        if(requestAjaxScenario) {
            requestAjaxScenario.abort();
            requestAjaxScenario = null;
        }

        $stopCurrentScenario.prop('disabled', true);

    }

    function _appendLog(response, lastAppend) {

        if(!response) return;

        var log = null;

        try {
            log = (JSON.parse(response)).log;
        }
        catch (e) {

            if(lastAppend !== true) {

                var logs = response.split(/(?!})(?={"log")/);

                $.each(logs, function (i, ele) {
                    _appendLog(ele, true);
                })

            }
            else {
                console.error('Problem z JSON parse log!');
                toastr.error('Problem z logiem...');
            }

        }

        if(!log) return;

        $scenarioProgressBar.css("width", log.progress + "%");

        if(!log.update) logNumber++;

        var tr = scenarioLoggerPrototype
            .replace('__ID__', log.id)
            .replace('__NR__', logNumber)
            .replace('__LOG__', log.message)
            .replace('__TIME__', log.time)
            .replace('__TYPE__', '<span title="'+log.type+'" class="label label-'+ log.color +' label-inline-block"><i class="fa '+ log.icon+'"></i></span>');

        if(log.update) {
            $scenarioLoggerTbody.find('[data-log-id="'+log.id+'"]').replaceWith(tr);
        }
        else {
            $scenarioLoggerTbody.prepend(tr);
        }

        if(log.type === "complete") {

            $currentRunScenario.find('.scenario-result .open-result-scenario').attr('data-process-id', log.extraData.processInstanceId);
            var url = Routing.generate('management_admin_panel_scenarios_builder_download_pdf', {scenarioInstanceId: log.extraData.scenarioInstanceId});
            if(log.extraData.hasPDF)
            {
                $currentRunScenario.find('.scenario-result .open-pdf-scenario').show();
            }else{
                $currentRunScenario.find('.scenario-result .open-pdf-scenario').hide();

            }
            $currentRunScenario.find('.scenario-result .open-pdf-scenario').attr('href', url);
            $currentRunScenario.find('.scenario-result').show();

        }

    }

    _init();

    return {

    }

});

/**
 * @returns {*|ScenarioRecorder}
 * @constructor
 */
function ScenarioRecorder () {

    var  _this,
        _isRecording = false,
        $iconWrapper,
        _attributes = [],
        _events = [],
        _steps = [];

    if (!(this instanceof ScenarioRecorder)) {
        return new ScenarioRecorder();
    }

    _this = this;

    _init();

    function _listeners () {

        $iconWrapper.on('click', _clickIconWrapper);

    }

    function _init () {

        $iconWrapper = $('#scenario-recorder-icon-wrapper');
        _listeners();

    }

    function _clickIconWrapper(e) {

        e.preventDefault();

        $(this).toggleClass('recording');

        if(_isRecording) {
            _this.stop();
        }
        else {
            _this.start();
        }

    }

    function _onSetAttribute(e) {

        if(!_isRecording) return;

        if (e.detail) {

            var indexAttribute = _attributes.findIndex(function (value) {
                return (value.path === e.detail.path);
            });

            if(indexAttribute !== -1) {
                _attributes.splice(indexAttribute, 1);
            }

            _attributes.push({
                stepId: e.detail.stepId,
                value: e.detail.value,
                path: e.detail.path,
                valueType: e.detail.valueType,
                type: "value"
            });

        }

    }

    function _saveRecording() {

        if(_attributes.length > 0 || _steps.length > 0) {

            var newItem = {
                uuid: guid(),
                date: (new Date()),
                attributes: _attributes,
                steps: _steps
            };

            var scenarioRecording = localStorage.getItem('scenario_recording');

            if(!scenarioRecording) {
                scenarioRecording = [];
            }
            else {
                scenarioRecording = JSON.parse(scenarioRecording);
            }

            scenarioRecording.push(newItem);

            localStorage.setItem('scenario_recording', JSON.stringify(scenarioRecording));

            toastr.success('Nagrywanie zakończone.');

        }
        else {
            toastr.info('Nie ma nic do zapisania.');
        }

    }


    /**
     * stepId, variant, isQuiz
     * @param {Object} data
     * @param {int} data.variant
     * @param {string} data.stepId
     * @param {boolean} data.isQuiz
     */
    this.successNextStep = function (data) {

        if(!_isRecording) return;

        if(data.variant && (data.variant > 1 || data.isQuiz) && data.stepId) {

            if(data.variant === parseInt('99')) {
                // Form Reload
                _steps.push({
                    stepId: data.stepId,
                    action: 'reloadForm',
                    value: {}
                })
            }
            else {
                _steps.push({
                    stepId: data.stepId,
                    action: 'setVariant',
                    value: {
                        variant: data.variant
                    }
                })
            }


        }

    };

    this.onStartService = function (data) {


        if(!_isRecording) return;

    }

    this.start = function () {

        _isRecording = true;

        _attributes = [];
        _steps = [];

        document.addEventListener("setAttributeValue", _onSetAttribute);

    };

    this.stop = function () {

        _isRecording = false;

        document.removeEventListener("setAttributeValue", _onSetAttribute);

        _saveRecording();

        _attributes = [];
        _steps = [];

    };

    this.getAttributes = function () {
        return _attributes;
    };

    this.getSteps = function () {
        return _steps;
    };

}

/**
 * @type {ScenarioRecorder}
 */
var scenarioRecorder;

$(function () {

    ScenarioFront();
    scenarioRecorder = ScenarioRecorder();

});