function AttributeValueRow (options, _scenario) {

    var _defaults,
        _options,
        _this,
        _template = '',
        $item;

    var _proceduresOptions = [
        {v: 'p_scenario_get_random_firstname', l: 'Losowe imię'}
    ];

    var _typeOptions = [
        {v: 'value', l: 'Wartość', d: false},
        {v: 'random', l: 'Random', d: false},
        {v: 'procedure', l: 'Procedure', d: false},
        {v: 'query', l: 'Query', d: true}
    ];

    var _valueOptions = [
        {v: 'string', l: 'String'},
        {v: 'int', l: 'Int'},
        {v: 'decimal', l: 'Decimal'},
        {v: 'date', l: 'Date'},
        {v: 'text', l: 'Text'}
    ];

    var _randomOptions = [
        {v: 'string', l: 'String'},
        {v: 'int', l: 'Int'}
    ];

    if (!(this instanceof AttributeValueRow)) {
        return new AttributeValueRow(options, _scenario);
    }

    _this = this;

    _init(options);

    this.getId = function () {
        return _options.id;
    };

    this.getItem = function () {
        return $item;
    };

    this.getData = function () {
        return _options;
    };

    this.getConfig = function () {

        var object = {
            path: _options.path,
            value: _options.value,
            type: _options.type,
            valueType: _options.valueType
        };

        if(_options.type === "random") {
            object.randomType = _options.randomType;
        }

        return object;

    };

    this.getTemplate = function () {
        return _template;
    };

    function _setOptions (options) {
        if (typeof options !== "undefined") {
            _options = $.extend(_options, options);
            _options['id'] = guid();
        }
    }

    function _initTemplate () {

        _template = '' +
            '<li class="task-list-item" data-uid="' + _options.id + '">' +
            '<div class="row">' +
            _templateControlPath() +
            _templateControlType() +
            _templateControlValue() +
            '<div class="col-md-1 task-status">' +
            '<a class="done copy" title="Copy" href="javascript:;">' +
            '<i class="fa fa-copy"></i>' +
            '</a>' +
            '<a class="pending remove" title="Remove" href="javascript:;">' +
            '<i class="fa fa-close"></i>' +
            '</a>' +
            '</div>' +
            '</div>' +
            '</li>';

    }

    this._insert = function($ele) {

        $item = $(_template).insertAfter($ele);
        _afterInserted();

    };

    this.appendToList = function($list) {

        $item = $(_template).appendTo($list);
        _afterInserted();

    };

    function _afterInserted() {

        $item.hide().fadeIn(500);
        _listeners();

    }

    function _templateControlValue() {

        var html = '<div class="value-box">';

        switch (_options.type) {
            case 'value' : {

                html += '' +
                    '<div class="col-md-2">' +
                    '<div class="form-group m-b-0">' +
                    '<select class="form-control select2" data-attribute-type="valueType" id="value_type_' + _options.id + '">' +
                    _getValueOptions() +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-4">' +
                    '<div class="form-group form-md-line-input has-success">' +
                    '<input type="text" class="form-control" data-attribute-type="value" value="' + _options.value + '" id="' + _options.id + '" placeholder="Value">' +
                    '<div class="form-control-focus"></div>' +
                    '</div>' +
                    '</div>';
                break;

            }
            case 'random' : {

                html += '' +
                    '<div class="col-md-2">' +
                    '<div class="form-group m-b-0">' +
                    '<select class="form-control select2" data-attribute-type="valueType" id="value_type_' + _options.id + '">' +
                    _getValueOptions() +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group m-b-0">' +
                    '<select class="form-control select2" data-attribute-type="randomType" id="random_type_' + _options.id + '">' +
                    _getRandomOptions() +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="form-group form-md-line-input has-success">' +
                    '<input type="text" class="form-control" data-attribute-type="value" value="' + _options.value + '" id="' + _options.id + '" placeholder="eg. 1-2 | 5">' +
                    '<div class="form-control-focus"> </div>' +
                    '</div>' +
                    '</div>';
                break;

            }
            case 'procedure' : {

                html += '' +
                    '<div class="col-md-2">' +
                    '<div class="form-group m-b-0">' +
                    '<select class="form-control select2" data-attribute-type="valueType" id="value_type_' + _options.id + '">' +
                        _getValueOptions() +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-4">' +
                    '<div class="form-group m-b-0">' +
                    '<select class="form-control select2" data-attribute-type="value" id="value_procedure_' + _options.id + '">' +
                        _getProceduresOptions() +
                    '</select>' +
                    '</div>' +
                    '</div>';
                break;

            }
        }

        html += '</div>';

        return html;

    }

    function _templateControlPath() {

        return '' +
            '<div class="col-md-3">' +
            '<div class="form-group form-md-line-input has-success">' +
            '<input type="text" value="' + _options.path + '" class="form-control" data-attribute-type="path" id="path_' + _options.id + '" placeholder="Path">' +
            '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>';
    }

    function _templateControlType() {

        return '' +
            '<div class="col-md-2">' +
            '<div class="form-group m-b-0">' +
            '<select class="form-control select2" data-attribute-type="type" id="type_' + _options.id + '">' +
            _getTypeOptions() +
            '</select>' +
            '</div>' +
            '</div>';

    }

    function _getProceduresOptions() {

        return _getOptions(_availableProcedures, 'value');

    }

    function _getValueOptions() {

        return _getOptions(_valueOptions, 'valueType');

    }

    function _getTypeOptions() {

        return _getOptions(_typeOptions, 'type');

    }

    function _getRandomOptions() {

        return _getOptions(_randomOptions, 'randomType');

    }

    function _getOptions(arr, type) {

        var options = '';

        $.each(arr, function (i, ele) {

            var disabled = (ele.d) ? 'disabled' : '';

            if(ele.v === _options[type]) {
                options += '<option value="' + ele.v + '" selected="" ' + disabled + '>' + ele.l + '</option>';
            } else {
                options += '<option value="' + ele.v + '" ' + disabled + '>' + ele.l + '</option>';
            }

        });

        return options;
    }

    function _initDefaults () {

        _defaults = {
            path: '',
            type: 'value',
            valueType: 'string',
            randomType: 'string',
            value: ''
        };

        _options = $.extend(_options, _defaults);

    }

    function _listeners () {

        $item.find('[data-attribute-type]').on('change', _changeValue);

        $item.find('[data-attribute-type="path"]').autocomplete({
            minLength: 2,
            source: availablePaths
        }).on('mouseenter', function () {

            var $t = $(this);

            if($t.hasClass('init-tooltip')) return;
            $t.addClass('init-tooltip');

            $t.parent().prepend(_scenario.getMiniSpinner());

            var path = $t.val();

            $.ajax({
                url: Routing.generate('management_admin_panel_scenarios_builder_tooltip_attribute'),
                type: 'POST',
                data: {
                    paths: [path]
                },
                success: function (response) {

                    if(response.results) {

                        if(response.results[path] !== false) {

                            var title = '';

                            $.each(response.results[path], function (i, ele) {

                                var space = "&ensp;".repeat(i);

                                title += "<span style='white-space: nowrap;'>" + space + "- " +  ele + "</span></br>";

                            });

                            $t.attr('data-original-title', title );

                            $t.tooltip({
                                html: true,
                                placement: "right",
                                template: '<div class="tooltip attribute-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                            }).tooltip('show');

                            $t.parent().removeClass('has-error').addClass('has-success');
                        }
                        else {

                            $t.tooltip('destroy');
                            $t.parent().addClass('has-error').removeClass('has-success');

                        }

                    }

                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    $t.parent().find('.mini-spinner').remove();
                }
            });

        });


    }

    function _changeValue() {

        var $t = $(this),
            type = $t.attr('data-attribute-type');

        _options[type] = $t.val();

        if(type === "type") {

            var chooseType = $t.val();

            if(chooseType === "value" || chooseType === "random") {
                _options.value = "";
            }
            else if(chooseType === "procedure") {
                _options.value = _availableProcedures[0].v;
            }

            var htmlValueBox = _templateControlValue();
            $item.find('.value-box').replaceWith($(htmlValueBox));
            $item.find('.value-box [data-attribute-type]').on('change', _changeValue);

        }
        else if(type === "path") {
            $t.removeClass('init-tooltip');
        }

    }

    function _init (options) {

        _initDefaults();
        _setOptions(options);
        _initTemplate();

    }
}