_debug = function() {};

/**
 * @param options
 * @returns {*|ScenarioCreator}
 * @constructor
 */
function ScenarioCreator(options) {

    var _defaults,
        _options,
        _this,
        _config = {},
        _configJson = '',
        _attributes = [],
        /**
         * @type {EventRow[]}
         */
        _events = [],
        /**
         * @type {StepRow[]}
         */
        _steps = [],
        _codeMirrorInstance = null,
        _codeMirrorScenarioConfigInstance = null;

    _debug = function () {

        console.log(_options);
        // console.log(_options.config);

        $.each(_attributes, function (i, ele) {
           console.log(i);
           console.log(ele.getId());
        });

        return _attributes;
    };

    var $scenarioContentEditor,
        $attributesSection,
        $attributeValueList,
        $attributeModal,
        $scenarioConfigModal,
        $eventsSection,
        $eventsList,
        $stepsSection,
        $stepsList,
        $basicConfig;

    if (!(this instanceof ScenarioCreator)) {
        return new ScenarioCreator(options);
    }

    _this = this;

    _init(options);

    function _initVariables() {

        $attributesSection = $('#attributes-section');
        $attributeValueList = $('#attribute-value-list');
        $attributeModal = $('#attributes-config-modal');
        $scenarioConfigModal = $('#scenario-config-modal');
        $scenarioContentEditor = $('#scenarios-content-editor');
        $eventsList = $('#scenarios-events-list');
        $eventsSection = $('#events-sections');
        $stepsList = $('#scenarios-steps-list');
        $stepsSection = $('#steps-section');
        $basicConfig= $('#basic-config');

        $attributeModal.find('textarea').val('');

    }

    function updateScenariosList(oldId, newId) {

        var $el = $('#available-scenarios-list .mt-list-item[data-id="'+oldId+'"]');

        if($el.length) {
            $el.attr('data-id', newId);
        }

    }

    function _initDefaults () {

        _defaults = {
            id: null,
            name: 'New Scenario',
            description: '',
            version: 1,
            createdAt: null,
            updatedAt: null,
            config: {
                basic: {
                    pdf: false,
                    validation_step: true,
                    diagnosis_code: '',
                    start_step: '',
                    end_step: ''
                },
                attributes: [],
                events: [],
                steps: []
            }
        };

        _options = $.extend(_options, _defaults);

    }

    function __onUpdateBasicConfig(e) {

        e.preventDefault();

        var $t = $(this),
            name = $t.attr('name'),
            value;

        if($t[0].type === "checkbox") {
            value = $t.is(':checked');
        }
        else {
            value = $t.val();
        }

        if(name.indexOf('config_') !== -1) {

            name = name.replace('config_', '');

            if(name.indexOf('basic_') !== -1) {

                name = name.replace('basic_', '');

                _options.config.basic[name] = value;

            }
            else {
                _options.config[name] = value;
            }

        }
        else {
            _options[name] = value;
        }

    }

    function __removeScenario(e) {

        e.preventDefault();

        var ladda = Ladda.create(this);

        $.ajax({
            url: Routing.generate('management_admin_panel_scenarios_builder_remove', {id: _options.id}),
            type: 'POST',
            beforeSend: function () {
                ladda.start();
            },
            success: function (response) {

                if(response.success) {

                    $scenarioContentEditor.remove();
                    toastr.success('', 'Scenariusz został usunięty.');

                    // var cEvent = new CustomEvent("refresh-list");
                    // $('#available-scenarios-list')[0].dispatchEvent(cEvent);
                    $('#available-scenarios-list').trigger('refresh-list');
                }

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                ladda.stop();
            }
        });

    }

    function _addNewEventAction(e) {

        e.preventDefault();

        var newEvent = new EventRow({}, _this);
        _appendEventToList(newEvent);

    }

    function _removeEventAction(e) {

        e.preventDefault();

        var $t = $(this),
            $item = $t.closest('.task-list-item'),
            _id = $item.attr('data-uid');

        var pos = _events.map(function(e) { return e.getId(); }).indexOf(_id);

        $item.remove();

        _events.splice(pos, 1);

        _afterInsertOrRemoveEvents();

    }

    function _addNewStepAction(e) {

        e.preventDefault();

        var newStep = new StepRow({}, _this);
        _appendStepToList(newStep);

    }

    /**
     * @param {StepRow} stepObject
     * @private
     */
    function _appendStepToList(stepObject) {

        var $el = $(stepObject.getTemplate()).appendTo($stepsList);
        stepObject.setItem($el).afterInserted();
        _steps.push(stepObject);
        _afterInsertOrRemoveSteps();

    }

    function _removeStepAction(e) {

        e.preventDefault();

        var $t = $(this),
            $item = $t.closest('.task-list-item'),
            _id = $item.attr('data-uid');

        var pos = _steps.map(function(e) { return e.getId(); }).indexOf(_id);

        $item.remove();

        _steps.splice(pos, 1);

        _afterInsertOrRemoveSteps();

    }

    function _addAttributeValue(e) {

        e.preventDefault();

        var newAttributeValue = new AttributeValueRow({}, _this);
        newAttributeValue.appendToList($attributeValueList);

        _attributes.push(newAttributeValue);

        _afterInsertOrRemoveAttributes();

    }

    function _copyAttributeValue(e) {

        e.preventDefault();
        var $t = $(this),
            _id = $t.closest('.task-list-item').attr('data-uid');

        var pos = _attributes.map(function(e) { return e.getId(); }).indexOf(_id);

        var attributeValue = _attributes[pos];

        var data = attributeValue.getData();
        var newRow = new AttributeValueRow(data, _this);

        newRow._insert(attributeValue.getItem());

        _attributes.splice((pos + 1), 0, newRow);

        _afterInsertOrRemoveAttributes();

    }

    function _removeAttributeValue(e) {

        e.preventDefault();

        var $t = $(this),
            $item = $t.closest('.task-list-item'),
            _id = $item.attr('data-uid');

        var pos = _attributes.map(function(e) { return e.getId(); }).indexOf(_id);

        $item.remove();

        _attributes.splice(pos, 1);

        _afterInsertOrRemoveAttributes();

    }

    function _showScenarioConfig() {

        var config = JSON.stringify(_getConfig(true), undefined, 4);

        _codeMirrorScenarioConfigInstance.setValue(config);

        $scenarioConfigModal.modal('show');

    }

    function _showAttributeConfig() {

        var config = JSON.stringify(_getConfigOf(_attributes), undefined, 4);

        _codeMirrorInstance.setValue(config);

        // $attributeModal.find('textarea').val(config);

        $attributeModal.modal('show');

    }

    function _saveScenario() {

        var $t = $(this),
            ladda = Ladda.create(this);

        $.ajax({
            url: Routing.generate('management_admin_panel_scenarios_builder_save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(_this.getData()),
            beforeSend: function () {
                ladda.start();
            },
            success: function (response) {

                if(response.success) {

                    _updateBaseData(response.scenario);
                    toastr.success('', 'Pomyślnie zapisano.');

                }

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                ladda.stop();
            }
        });
    }

    function _copyScenario() {

        var ladda = Ladda.create(this);

        $.ajax({
            url: Routing.generate('management_admin_panel_scenarios_builder_copy'),
            type: 'POST',
            data: {
                id: _options.id
            },
            beforeSend: function () {
                ladda.start();
            },
            success: function (response) {

                if(response.success) {

                    scenarioBuilder.loadScenario(response.scenario.id);
                    scenarioBuilder.refreshScenarios();
                    toastr.success('', 'Pomyślnie skopiowano.');

                }

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                ladda.stop();
            }
        });
    }

    function _listeners() {

        $scenarioContentEditor.find('.basic-config-control').on('change', __onUpdateBasicConfig);
        $scenarioContentEditor.find('#remove-scenario').on('click', __removeScenario);
        $scenarioContentEditor.find('#copy-scenario').on('click', _copyScenario);
        $scenarioContentEditor.find('#show-config-scenario').on('click', _showScenarioConfig);


        $('#save-scenario').on('click', _saveScenario);

        $eventsSection.find('#add-event-action').on('click', _addNewEventAction);
        $eventsList.on('click', 'a.remove', _removeEventAction);

        $stepsSection.find('#add-step').on('click', _addNewStepAction);
        $stepsList.on('click', 'a.remove', _removeStepAction);

        $attributesSection.find('#add-attriute-value').on('click', _addAttributeValue);
        $attributeValueList.on('click', '.task-status .copy', _copyAttributeValue);
        $attributeValueList.on('click', '.task-status .remove', _removeAttributeValue);
        $attributesSection.find('.show-attributes-config').on('click', _showAttributeConfig);

        new Sortable($eventsList[0], {
            group: 'scenarios',
            draggable: '.task-list-item',
            handle: ".event-order",
            animation: 300,
            filter: '.ignore',
            sort: true,
            onSort: function (/**Event*/evt) {
                array_move(_events, evt.oldIndex, evt.newIndex);
            }
        });

    }

    function _updateBaseData(scenarioData) {

        var oldId = _options.id;

        _options.id = scenarioData.id;
        _options.version = scenarioData.version;
        _options.updatedAt = scenarioData.updatedAt;

        _updateHeaderInfo();
        updateScenariosList(oldId, _options.id);

    }

    function _updateHeaderInfo() {

        $scenarioContentEditor.find('.scenario-name').text(_options.name);
        $scenarioContentEditor.find('.scenario-version').text(_options.version);
        $scenarioContentEditor.find('.scenario-updated-at').text(moment(_options.updatedAt.date).format('YYYY/MM/DD HH:mm'));

    }

    function _reConfig() {

        var attributes = [];

        $.each(_attributes, function (i, ele) {
            attributes.push(ele.getData());
        });

        _config = {
            attributes: attributes
        };

        _configJson = JSON.stringify(_config);
    }

    function _getConfigOf(configArr) {

        var arr = [];

        $.each(configArr, function (i, ele) {
            arr.push(ele.getConfig());
        });

        return arr;
    }

    function _afterInsertOrRemoveEvents() {
        $eventsSection.find('.amount-list').text(_events.length);
    }

    function _afterInsertOrRemoveSteps() {
        $stepsSection.find('.amount-list').text(_steps.length);
    }

    function _afterInsertOrRemoveAttributes() {
        $attributesSection.find('.amount-list').text(_attributes.length);
    }

    function _clearAttributesList() {

        _attributes = [];
        $attributeValueList.children().remove();

    }

    function _setAttributesList(attributesList) {

        _clearAttributesList();

        $.each(attributesList, function (i, ele) {

            var newAttributeValue = new AttributeValueRow(ele, _this);
            newAttributeValue.appendToList($attributeValueList);
            _attributes.push(newAttributeValue);

        });

        _afterInsertOrRemoveAttributes();

    }

    function _clearEventsList() {

        _events = [];
        $eventsList.children().remove();

    }

    function _setEventsList(eventsList) {

        _clearEventsList();

        $.each(eventsList, function (i, ele) {

            var newEvent = new EventRow(ele, _this);
            _appendEventToList(newEvent);

        });

    }

    function _clearStepsList() {

        _steps = [];
        $stepsList.children().remove();

    }

    function _setStepsList(stepsList) {

        _clearStepsList();

        $.each(stepsList, function (i, ele) {

            var stepRow = new StepRow(ele, _this);
            _appendStepToList(stepRow);

        });

    }

    /**
     * @param {EventRow} eventObject
     * @private
     */
    function _appendEventToList(eventObject) {

        var $el = $(eventObject.getTemplate()).appendTo($eventsList);
        eventObject.setItem($el).afterInserted();

        _events.push(eventObject);

        _afterInsertOrRemoveEvents();

    }

    function _initTemplate() {

        $('#scenarios-content-editor .tooltips').tooltip();

        $attributeModal.find('.CodeMirror').remove();
        $scenarioConfigModal.find('.CodeMirror').remove();

        _codeMirrorInstance = CodeMirror.fromTextArea($attributeModal.find('textarea')[0], {
            matchBrackets: true,
            autoCloseBrackets: true,
            lineWrapping: true,
            jsonld: true,
            mode: {
                name: "javascript"
            },
            lineNumbers: true
        });

        _codeMirrorScenarioConfigInstance = CodeMirror.fromTextArea($scenarioConfigModal.find('textarea')[0], {
            matchBrackets: true,
            autoCloseBrackets: true,
            lineWrapping: true,
            jsonld: true,
            mode: {
                name: "javascript"
            },
            lineNumbers: true
        });

        _updateHeaderInfo();

    }

    function _setOptions (options) {
        if (typeof options !== "undefined") {
            _options = $.extend(_options, options);
        }
    }

    function _getConfig(includeName) {

        includeName = (typeof includeName === "undefined") ? false : includeName;

        var config = {};

        if(includeName) {
            config.name = _options.name;
            config.description = _options.description;
        }

        config.basic = _options.config.basic;

        config.attributes = _getConfigOf(_attributes);

        config.steps = _getConfigOf(_steps);

        config.events = _getConfigOf(_events);

        return config;

    }

    function _setConfig(config, withBasic) {

        withBasic = (typeof withBasic === "undefined") ? false : withBasic;

        if(!config) return;

        if(config.attributes) {
            _setAttributesList(config.attributes);
        }

        if(config.events) {
            _setEventsList(config.events);
        }

        if(config.steps) {
            _setStepsList(config.steps);
        }

        if(withBasic && config.basic) {

            _saveBasicConfig(config.basic);

        }

    }

    function _saveBasicConfig(basicConfig) {

        $.each(['pdf', 'validation_step'], function (i, ele) {


            if(typeof basicConfig[ele] !== "undefined") {
                _options.config.basic[ele] = basicConfig[ele];
                $basicConfig.find('[name="config_basic_' + ele + '"]').prop('checked', basicConfig[ele]);
            }

        });

        $.each(['diagnosis_code', 'start_step', 'end_step'], function (i, ele) {

            if(typeof basicConfig[ele] !== "undefined") {
                _options.config.basic[ele] = basicConfig[ele];
                $basicConfig.find('[name="config_basic_' + ele + '"]').val(basicConfig[ele]);
            }

        });

    }

    this.getMiniSpinner = function() {
        return $attributeValueList.attr('data-mini-spinner');
    };

    this.getData = function () {

        return {
            'id': _options.id,
            'name': _options.name,
            'description': _options.description,
            'version': _options.version,
            'createdAt': _options.createdAt,
            'config': _getConfig()
        };

    };

    this.setId = function (id) {

        _options.id = id;

    };

    this.saveConfigAttributesFromModal = function () {

        var $attributesConfig = _codeMirrorInstance.getValue();

        var attributesList = null;

        try {

            attributesList = JSON.parse($attributesConfig);

        }
        catch (e) {

            toastr.error("Błąd przy parsowaniu JSON'a! \n\n " + e.message);

        }
        finally {
            // ladda.stop();
        }

        if(attributesList !== null) {
            _setAttributesList(attributesList);
            $attributeModal.modal('hide');
        }

    };

    this.saveScenarioConfigFromModal = function () {


        var scenarioConfig = _codeMirrorScenarioConfigInstance.getValue();

        try {

            scenarioConfig = JSON.parse(scenarioConfig);

        }
        catch (e) {

            toastr.error("Błąd przy parsowaniu JSON'a! \n\n " + e.message);
            scenarioConfig = null;

        }
        finally {
        }

        if(scenarioConfig !== null) {

            _setConfig(scenarioConfig, true);

            $('#scenario_name').val(scenarioConfig.name);
            $('#scenario_description').val(scenarioConfig.description);

            $scenarioConfigModal.modal('hide');

        }

    };

    this.initCreator = function () {

        $.ajax({
            url: Routing.generate('management_admin_panel_scenarios_builder_creator_template'),
            type: 'GET',
            data: {
                id: _options.id
            },
            beforeSend: function () {
                // ladda.start();
            },
            success: function (response) {

                $('.scenario-main-content').html(response.content);

                _initVariables();
                _initTemplate();

                _setConfig(_options.config);

                _listeners();

                setTimeout(function () {
                    $('#basic-config .list-todo-item a').trigger('click');
                }, 100)

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                // ladda.stop();
            }
        });

    };

    function _init (options) {

        _initDefaults();
        _setOptions(options);

    }

}

function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
}