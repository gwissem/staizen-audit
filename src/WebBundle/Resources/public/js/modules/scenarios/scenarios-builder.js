var availablePaths = [],
    _availableServices = [],
    _availableProcedures = [];

var ScenarioBuilder = (function () {


    var _ID = 'scenarios-builder',
        /**
         * @type {ScenarioCreator}
         */
        _currentScenario = {},
        $wrapper,
        $content,
        $contentActions,
        $reportTab,
        $scenarioMainContent,
        $scenarioSearch,
        $scenariosContentEditor;

    var loaded = false,
        searchAjax = null;

    var $availableScenariosList,
        _codeMirrorSourceInstance,
        $recordingTable;

    var _modules = (function () {

        // var availablePaths = [];

        function _init() {

            $.ajax({
                url: Routing.generate('management_admin_panel_scenarios_builder_controls'),
                type: 'GET',
                beforeSend: function () {
                    // $wrapper.addClass('loading');
                },
                success: function (response) {

                    availablePaths = response.paths;

                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    // $wrapper.removeClass('loading');
                }
            });


        }

        _init();

        return {
            getAvailablePaths: function () {
                return availablePaths
            }
        }
    })();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $wrapper = $('#' + _ID + '-wrapper');
        $content = $wrapper.find('.tab-wrapper-content');
        $reportTab = $('a[href="#' + _ID + '"]').parent();

        _handleEvents();

    }

    function _handleEvents() {

        $('a[href="#' + _ID + '"]').on('shown.bs.tab', function() {
            iniLoad();
        });

        if($('a[href="#' + _ID + '"]').parent().hasClass('active')) {
            iniLoad();
        }

    }

    function iniLoad() {
        if(!loaded) {

            _loadPanel();
        }
    }

    function _loadPanel() {

        $.ajax({
            url: Routing.generate('management_admin_panel_scenarios_builder_get_services_and_procedures'),
            type: 'GET',
            success: function (response) {
                _availableServices = response.services;
                _availableProcedures = response.procedures;
            },
            error: function (response) {
                toastr.warning('Nie udało się załadować serwisów');
                handleAjaxResponseError(response);
            }
        });

        $.ajax({
            url: Routing.generate('management_admin_panel_scenarios_builder_index') +  window.location.search,
            type: 'GET',
            beforeSend: function () {
                $wrapper.addClass('loading');
            },
            success: function (response) {

                loaded = true;
                $content.html(response.content);
                // $contentActions = $wrapper.find('.tab-wrapper-content-actions');
                //
                _afterLoadPanel();

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $wrapper.removeClass('loading');
            }
        });

    }

    function _afterLoadPanel() {

        $availableScenariosList = $('#available-scenarios-list');
        $scenarioMainContent = $('.scenario-main-content');
        $scenarioSearch = $('#scenario-search');

        _loadAvailableScenarios();
        _handleListenersAfterLoadPanel();
        _handleListenersOfActions();
        _handleListenersOfRecording();


    }

    function _loadAvailableScenarios() {

        var $listContainer = $('#available-scenarios-list .mt-list-container');

        var url,
            data = {'front-mode': false},
            searchValue = $scenarioSearch.val();

        if(searchValue) {
            url = Routing.generate('management_admin_panel_scenarios_builder_find_scenarios_by_name');
            data.name = searchValue
        }
        else {
            url = Routing.generate('management_admin_panel_scenarios_builder_available_scenarios');
        }

        if(searchAjax) {
            searchAjax.abort();
        }

        searchAjax = $.ajax({
            url: url,
            type: 'GET',
            data: data,
            beforeSend: function () {

                $listContainer.html($availableScenariosList.attr('data-spinner'));

            },
            success: function (response) {

                if(response.list) {
                    $('#available-scenarios-list .mt-list-container').html(response.list);
                }

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                // ladda.stop();
            }
        });

    }
    
    function __refreshList() {
        _loadAvailableScenarios();
    }

    function _handleListenersOfRecording() {

        $recordingTable = $('#available-recording-list table tbody');

        $recordingTable.on('click', '.remove-recording', function (e) {

            e.preventDefault();

            var scenarioRecording = localStorage.getItem('scenario_recording');

            if(scenarioRecording) {

                scenarioRecording = JSON.parse(scenarioRecording);

                var uuid = $(this).closest('tr').attr('data-id');

                $(this).closest('tr').remove();

                var index = scenarioRecording.findIndex(function (value) {
                    return (value.uuid === uuid);
                });

                if(index !== -1) {
                    scenarioRecording.splice(index, 1);
                }

                localStorage.setItem('scenario_recording', JSON.stringify(scenarioRecording));

            }

        });

        $recordingTable.on('click', '.show-steps', _displayConfigSource);

        $recordingTable.on('click', '.show-attributes', _displayConfigSource);

    }

    function _displayConfigSource(e) {

        e.preventDefault();

        var scenarioRecording = localStorage.getItem('scenario_recording');

        if(scenarioRecording) {

            scenarioRecording = JSON.parse(scenarioRecording);

            var uuid = $(this).closest('tr').attr('data-id');

            var record;

            $.each(scenarioRecording, function (i, ele) {
                if(ele.uuid === uuid) {
                    record = ele;
                }
            });

            if(record) {
                if($(this).hasClass('show-attributes')) {
                    _showSource(record.attributes);
                }
                else {
                    _showSource(record.steps);
                }
            }

        }

    }

    function formatDate(date) {
        date = new Date(date);
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
    }

    function _refreshRecordingScenarios() {

        $recordingTable.find('tr').remove();

        var scenarioRecording = localStorage.getItem('scenario_recording');

        if(scenarioRecording) {

            scenarioRecording = JSON.parse(scenarioRecording);

            $.each(scenarioRecording, function (i, ele) {
                $recordingTable.append(
                    '<tr data-id="'+ele.uuid+'">' +
                    '<td>'+moment(ele.date).format('YYYY-MM-DD HH:mm')+'</td>' +
                    '<td>' +
                        '<button class="btn btn-sm btn-green-meadow show-attributes m-r-1">Atrybuty ('+ele.attributes.length+')</button>' +
                        '<button class="btn btn-sm btn-yellow-crusta show-steps">Kroki ('+ele.steps.length+')</button>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn btn-sm btn-danger remove-recording"><i class="fa fa-remove"></i></button>' +
                    '</td>' +
                    '</tr>'
                )
            })

        }

    }


    function _loadScenarioInstances() {
        var $listContainer = $('#available-actions-list .mt-list-container');

        $.ajax({
            url: Routing.generate('management_admin_panel_scenarios_builder_historical_instances'),
            type: 'GET',
            beforeSend: function () {

                $listContainer.html($availableScenariosList.attr('data-spinner'));

            },
            success: function (response) {

                if(response.list) {
                    $listContainer.html(response.list);
                    _bindScenarioInstanceListeners();
                }

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {

            }
        });
    }

    function _bindScenarioInstanceListeners() {
        $('.error_info').on('click', '.link', function () {
            $(this).next().toggle();
        });
    }

    function __refreshInstancesList() {
      _loadScenarioInstances();
    }

    function _handleListenersOfActions() {

        $availableScenariosList.on('refresh-list', __refreshList);

        $('#scenarios-panel-actions [href="#available-recording-list"]').on('shown.bs.tab', _refreshRecordingScenarios);

        $('#scenarios-panel-actions [href="#available-scenarios-list"]').on('shown.bs.tab', __refreshList);
        $('#scenarios-panel-actions [href="#available-actions-list"]').on('shown.bs.tab', __refreshInstancesList);

        $('#create-new-scenario').on('click', function () {

            var ladda = Ladda.create(this);

            $.ajax({
                url: Routing.generate('management_admin_panel_scenarios_builder_create'),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify((new ScenarioCreator()).getData()),
                beforeSend: function () {
                    ladda.start();
                },
                success: function (response) {

                    if(response.success) {

                        _currentScenario = new ScenarioCreator(response.scenario);
                        $('#available-scenarios-list').trigger('refresh-list');
                        _currentScenario.initCreator();

                    }

                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    ladda.stop();
                }
            });

        })

    }

    function _showSource(source) {

        _codeMirrorSourceInstance.setValue("");

        $('#source-viewer-modal').modal('show');

        var config = JSON.stringify(source, undefined, 4);

        _codeMirrorSourceInstance.setValue(config);

    }

    function _searchScenarios(e) {

        e.preventDefault();
        delayedCallback(_loadAvailableScenarios, 300);

    }

    function _handleListenersAfterLoadPanel() {

        $scenarioSearch.on('keyup', _searchScenarios);

        $('#scenario-btn-confirm-attributes').on('click', function () {
            _currentScenario.saveConfigAttributesFromModal();
        });

        $('#scenario-btn-confirm-scenario').on('click', function () {
            _currentScenario.saveScenarioConfigFromModal();
        });

        _codeMirrorSourceInstance = CodeMirror.fromTextArea($('#source-viewer-modal').find('textarea')[0], {
            matchBrackets: true,
            autoCloseBrackets: true,
            lineWrapping: true,
            jsonld: true,
            mode: {
                name: "javascript"
            },
            lineNumbers: true
        });

        $availableScenariosList.on('click', '.mt-list-item', function (e) {

            e.preventDefault();

            var $t = $(this),
                id = $t.attr('data-id');

            $availableScenariosList.find('.mt-list-item.active').removeClass('active');
            $t.addClass('active');

            _getScenario(id);

        })

    }

    function _getScenario(id) {


        $.ajax({
            url: Routing.generate('management_admin_panel_scenarios_builder_get_scenario', {id: id}),
            type: 'GET',
            beforeSend: function () {

                $('#scenarios-content-editor').remove();
                $scenarioMainContent.html($scenarioMainContent.attr('data-spinner'));

            },
            success: function (response) {

                if(response.scenario) {

                    _currentScenario = new ScenarioCreator(response.scenario);
                    _currentScenario.initCreator();
                }

            },
            error: function (response) {
                $scenarioMainContent.html('');
                handleAjaxResponseError(response);
            },
            complete: function () {
                // ladda.stop();
            }
        });

    }

    function _loadScenario() {

        $scenariosContentEditor = $('#scenarios-content-editor');

        _currentScenario = new ScenarioCreator();

        _scenarioListeners();
    }

    function _scenarioListeners() {

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {
        loadScenario: _getScenario,
        refreshScenarios: _loadAvailableScenarios
    }

});