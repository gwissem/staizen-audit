function EventRow (options, _scenario) {

    var _defaults,
        _options,
        _template = '',
        $item;

    var _typeOptions = [
        {v: 'scenario', l: 'Scenariusz'},
        {v: 'ring', l: 'Dzwoni telefon'},
        {v: 'service', l: 'Usługa'},
        {v: 'rs', l: 'Raport serwisowy'},
        {v: 'email', l: 'Sprawa z emaila'}
    ];

    if (!(this instanceof EventRow)) {
        return new EventRow(options, _scenario);
    }

    _init(options);

    this.getId = function () {
        return _options.id;
    };

    this.setItem = function ($i) {
        $item = $i;
        return this;
    };

    this.getItem = function () {
        return $item;
    };

    this.getData = function () {
        return _options;
    };

    this.afterInserted = function () {

        $item.hide().fadeIn(500);
        _listeners();

    };

    this.getConfig = function () {

        return {
            value: _options.value,
            type: _options.type
        };

    };

    this.getTemplate = function () {
        return _template;
    };

    function _initTemplate () {

        _template = '' +
            '<li class="task-list-item" data-uid="' + _options.id + '">' +
                '<div class="row">' +
                    '<div class="col-md-3">' +
                        _selectEventTypeTemplate() +
                    '</div>' +
                    '<div class="col-md-8">' +
                        _eventValueTemplate() +
                    '</div>' +
                    '<div class="col-md-1 task-status">' +
                        '<a class="pending event-order" title="Order" href="javascript:;">' +
                            '<i class="fa fa-sort"></i>' +
                        '</a>' +
                        '<a class="pending remove" title="Remove" href="javascript:;">' +
                          '<i class="fa fa-close"></i>' +
                        '</a>' +
                    '</div>' +
                '</div>' +
            '</li>';

    }

    function _eventValueTemplate() {

        var html = '<div class="value-box">';

        switch (_options.type) {
            case 'scenario' : {

                html += '' +
                    '<div class="form-group m-b-0">' +
                        '<select class="form-control select2" data-attribute-value="scenario_id" id="scenario_type_' + _options.id + '"></select>' +
                    '</div>';
                break;

            }
            case 'ring' : {

                html += '' +
                    '<div class="row">' +
                        '<div class="col-md-6">' +
                            '<div class="form-group form-md-line-input has-success">' +
                                '<input type="number" class="form-control" data-attribute-value="ringer_number" value="' + _options.value.ringerNumber + '" id="ringer_number_' + _options.id + '" placeholder="Numer dzwoniącego">' +
                                '<div class="form-control-focus"> </div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-md-6">' +
                            '<div class="form-group m-b-0">' +
                                '<select class="form-control select2" data-attribute-value="platform_id" id="platform_id_' + _options.id + '">' +
                                '</select>' +
                            '</div>' +
                            // '<div class="form-group form-md-line-input has-success">' +
                            //     '<input type="text" class="form-control" data-attribute-value="platform_id" value="' + _options.value.platformId + '" id="' + _options.id + '" placeholder="Infolinia / Platforma">' +
                            //     '<div class="form-control-focus"> </div>' +
                            // '</div>' +
                        '</div>' +
                    '</div>';
                break;

            }
            case 'service' : {

                html += '' +
                    '<div class="form-group m-b-0">' +
                    '<select class="form-control select2" data-attribute-value="service_id" id="service_type_' + _options.id + '">' +
                        _getServiceOptions() +
                    '</select>' +
                    '</div>';
                break;

            }
            case 'rs' : {
                html += '';
                break;
            }
            case 'email' : {
                html += '';
                break;

            }
        }

        html += '</div>';

        return html;

    }

    function _selectEventTypeTemplate() {

        return '' +
            '<div class="form-group m-b-0">' +
                '<select class="form-control select2" data-attribute-type="type" id="type_' + _options.id + '">' +
                    _getTypeOptions() +
                '</select>' +
            '</div>';

    }

    function _getTypeOptions() {

        return _getOptions(_typeOptions, 'type');

    }

    function _getServiceOptions() {

        return _getOptions(_availableServices, 'service');

    }

    function _getOptions(arr, type) {

        var options = '';

        $.each(arr, function (i, ele) {

            var disabled = (ele.d) ? 'disabled' : '';

            if(ele.v === _options[type]) {
                options += '<option value="' + ele.v + '" selected="" ' + disabled + '>' + ele.l + '</option>';
            } else {
                options += '<option value="' + ele.v + '" ' + disabled + '>' + ele.l + '</option>';
            }

        });

        return options;
    }

    function _listeners () {


        $item.find('[data-attribute-type]').select2().on('change', _changeType);

        _valuesListeners();

    }

    function _valuesListeners() {

        switch (_options.type) {
            case "scenario" : {

                var $selectScenario = $item.find('[data-attribute-value="scenario_id"]');

                $selectScenario.select2({
                    ajax: {
                        delay: 250,
                        url: Routing.generate('management_admin_panel_scenarios_builder_find_scenarios'),
                        dataType: 'json'
                    }
                });

                $selectScenario.on('change', function () {

                    _options.value.scenarioId = $selectScenario.val();

                });

                if(_options.value.scenarioId) {
                    $.ajax({
                        type: 'GET',
                        url: Routing.generate('management_admin_panel_scenarios_builder_find_scenarios', {id: _options.value.scenarioId })
                    }).then(function (data) {

                        var option = new Option(data.text, data.id, true, true);
                        $selectScenario.append(option).trigger('change');

                    });
                }

                break;
            }
            case "ring" : {

                var $select = $item.find('[data-attribute-value="platform_id"]');

                $select.select2({
                    ajax: {
                        delay: 250,
                        url: Routing.generate('management_admin_panel_scenarios_builder_find_platform'),
                        dataType: 'json'
                    }
                });

                $select.on('change', function () {

                    _options.value.platformId = $select.val();

                });

                if(_options.value.platformId) {
                    $.ajax({
                        type: 'GET',
                        url: Routing.generate('management_admin_panel_scenarios_builder_find_platform', {id: _options.value.platformId })
                    }).then(function (data) {

                        var option = new Option(data.text, data.id, true, true);
                        $select.append(option).trigger('change');

                    });
                }

                $item.find('[data-attribute-value="ringer_number"]').on('change', function () {

                    _options.value.ringerNumber = this.value;

                });

                break;
            }
            case "service" : {

                var $selectServices = $item.find('[data-attribute-value="service_id"]');

                if(_options.value.serviceId) {
                    $selectServices.val(_options.value.serviceId);
                }

                $selectServices.on('change', function () {
                    _options.value.serviceId = $selectServices.val();

                });

                break;
            }
            case "rs" : {
                break;
            }

        }

    }

    function _changeType() {

        var $t = $(this),
            type = $t.attr('data-attribute-type');

        _options[type] = $t.val();

        switch (_options.type) {
            case "scenario" : {

                _options.value = {
                    scenarioId: ''
                };

                break;
            }
            case "ring" : {

                _options.value = {
                    ringerNumber: '',
                    platformId: ''
                };

                break;
            }
            case "service" : {

                _options.value = {
                    serviceId: ''
                };

                break;
            }
            case "rs" : {
                _options.value = '';

                break;
            }
            case "email" : {
                _options.value = '';

                break;
            }

        }

        var htmlValueBox = _eventValueTemplate();
        $item.find('.value-box').replaceWith($(htmlValueBox));

        _valuesListeners();


    }

    function _initDefaults () {

        _defaults = {
            type: 'scenario',
            value: {}
        };

        _options = $.extend(_options, _defaults);

    }

    function _setOptions (options) {
        if (typeof options !== "undefined") {
            _options = $.extend(_options, options);
            _options['id'] = guid();
        }
    }

    function _init (options) {

        _initDefaults();
        _setOptions(options);
        _initTemplate();

    }
}