var AdminPanelModule = (function () {

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    if (typeof WS === "undefined" || !document.WSConnectingString) {
        console.error('WebSocket is not loaded.');
    }

    var webSocket = webSocket || WS.connect(document.WSConnectingString),
        connectionSocket;

    function _init() {

        handleWebSocket();
        telephonyStatus();
        // projectSize();
        notificationsSender();

    }

    function handleWebSocket() {

        webSocket.on("socket/disconnect", function (error) {
            switch (error.code) {
                case 6: {
                    toastr.error("Połączenie z serwerem zostało zerwane.", "Błąd połączenia (WebSocket)", {timeOut: 7000});
                    break;
                }
                case 3: {
                    toastr.error("Nie można nawiązać połączenia z serwerem.", "Błąd połączenia (WebSocket)", {timeOut: 7000});
                    break;
                }
            }

            console.log("Disconnected for " + error.reason + " with code " + error.code);
        });

        webSocket.on("socket/connect", function (session) {

            connectionSocket = session;
            reservedTask();

        });

    }

    var reservedTask = (function () {

        var $table = $('#body-of-reserved-table'),
            $loader = $('#reservation-loader'),
            prototypeRow = '<tr>' +
                '<td>__INDEX__</td>' +
                '<td>__USERNAME__</td>' +
                '<td>__TASKS__</td>' +
                '<td>__CONN__</td>' +
                '</tr>',
            prototypeLabel = '<label data-content="__CONTENT__" class="label label-primary">' +
                '__NAME__</label>',
            titlePrototype = '<div>' +
                '<span>Id: <b>__ID__</b></span><br>' +
                '<span>Step: <b>__STEP__</b></span><br>' +
                '<span>Duration: <b>__TIME__</b></span>' +
                '</div>';

        function _getReservation(callback) {

            connectionSocket.call("atlas/task/manager/rpc/get_reservation/" + _locale, {}).then(
                function (result) {

                    if (result.reservation.length === 0) {
                        $table.html('<tr><td colspan="4">Brak połączonych użytkowników.</td></tr>');
                        $loader.hide();
                    }
                    else {

                        $.ajax({
                            url: Routing.generate('socket_admin_panel_reservation'),
                            type: "POST",
                            data: {
                                reservation: result.reservation
                            },
                            success: function (result) {

                                callback(result);

                            },
                            error: function (error) {
                                handleAjaxResponseError(error);
                            }
                        });

                    }

                },
                function (error) {
                    console.log(error);
                }
            );

        }

        function drawReservation(data) {

            var index = 1,
                labelProcess;

            $table.html('');

            $.each(data.reservation, function (i, ele) {

                var row = prototypeRow,
                    tasks = '';

//                        for (var i = 0; i < ele.tasks.length; i++) {
//                            if (ele.tasks[i] === null) {
//                                ele.tasks.splice(i, 1);
//                            }
//                        }

                $.each(ele.tasks, function (i, ele) {

                    var title = titlePrototype.replace('__ID__', ele.id)
                        .replace('__STEP__', ele.stepId)
                        .replace('__TIME__', '//todo');

                    labelProcess = prototypeLabel.replace('__NAME__', ele.stepName)
                        .replace('__CONTENT__', title);
                    tasks += labelProcess;
                });

                row = row.replace('__INDEX__', index.toString());
                row = row.replace('__USERNAME__', ele.username);
//                        row = row.replace('__TASKS__', ele.tasks.join(', '));
                row = row.replace('__TASKS__', tasks);
                row = row.replace('__CONN__', ele.connections);

                $table.append(row);

                index++;
            });


            var template = '<div class="popover services-popover" role="tooltip">' +
                '<div class="arrow"></div>' +
                '<h3 class="popover-title"></h3>' +
                '<div class="popover-content" style="text-align: left"></div>' +
                '</div>';

            $table.find('.label').popover({
                container: $table,
                html: true,
                template: template,
                trigger: "click"
            });

            $loader.hide();

        }

        function reloadReservation(withLoader) {

            if (typeof withLoader === "undefined") withLoader = true;

            if (withLoader) {
                $loader.show();
            }

            _getReservation(function (data) {
                drawReservation(data);
            })
        }

        function _handleEvent() {
            $('#reload-reservation').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                reloadReservation();
            })
        }

        function _startInterval() {
            setInterval(function () {
                reloadReservation(false);
            }, 60000); // 60 sekund
        }

        _handleEvent();
        reloadReservation();
        _startInterval();

    });

    var projectSize = (function () {

        var $table = $('#body-of-project-size-table'),
            prototypeRow = '<tr>' +
                '<td></td>' +
                '<td>__NAME__</td>' +
                '<td style="text-align: center">__SIZE__</td>' +
                '</tr>';

        function getProjectSize() {

            $.ajax({
                url: Routing.generate('socket_admin_panel_project_size'),
                type: "GET",
                success: function (result) {
                    _refreshView(result);
                    $('#project-size-loader').remove();
                },
                error: function (error) {
                    handleAjaxResponseError(error);
                }
            });

        }

        function _refreshView(data) {

            $table.html('');

            var row = prototypeRow;
            row = row.replace('__NAME__', 'Dostępne miejsce');
            row = row.replace('__SIZE__', '<span class="label label-primary">' + data.free + '</span>');
            $table.append(row);

            $.each(data.fileSize, function (i, ele) {

                var row = prototypeRow;
                row = row.replace('__NAME__', ele.name);
                row = row.replace('__SIZE__', '<span class="label label-warning">' + ele.size + '</span>');
                $table.append(row);

            });

        }

        getProjectSize();

    });

    var telephonyStatus = (function () {


        function init() {

            getStatuses();

            setInterval(function () {
                getStatuses();
            }, 50000);

        }

        function getStatuses() {
            $.ajax({
                url: Routing.generate('adminPanelTelephonyStatus'),
                type: "GET",
                success: function (result) {
                    if (result.statuses) {
                        refreshResult(result.statuses);
                    }
                },
                error: function (error) {
                    handleAjaxResponseError(error);
                }
            });
        }

        function refreshResult(statuses) {

            $.each(statuses, function (i, ele) {
                $('.status-' + i).text(ele);
            });
        }

        init();

    });

    var notificationsSender = (function () {

        var $selectUser = $('#select-users'),
            $selectGroup = $('#select-groups'),
            $atlasNotificationForm = $('#atlas-notification-form'),
            $typeReceivers = $('input[name="type-receivers"]');

        function init() {

            handleGroupSelect();
            handleUserSelect();
            handleEvents();

        }

        function handleUserSelect() {

            $selectUser.select2({
                minimumInputLength: 1,
                ajax: {
                    type: 'GET',
                    url: Routing.generate('adminPanelFindUsers'),
                    dataType: 'json'
                }
            });

        }

        function handleGroupSelect() {

            $selectGroup.select2({
                minimumInputLength: 1,
                ajax: {
                    type: 'GET',
                    url: Routing.generate('adminPanelFindGroups'),
                    dataType: 'json'
                }
            });

        }

        function handleEvents() {

            $atlasNotificationForm.on('submit', function (e) {

                e.preventDefault();

                var $t = $(this);

                $.ajax({
                    url: Routing.generate('adminPanelSendNotification'),
                    type: "POST",
                    data: $t.serialize(),
                    success: function (result) {

                        toastr.success("Wysłano do " + result.amount + " osób.", "Pomyślnie wysłano", {
                            timeOut: 5000,
                            positionClass: "toast-bottom-right"
                        });

                    },
                    error: function (error) {
                        handleAjaxResponseError(error);
                    }
                });

            });

            $typeReceivers.on('change', function () {
                _changeTypeReceivers(this.value);
            });

            _changeTypeReceivers($typeReceivers.filter(":checked").val())

        }

        function _changeTypeReceivers(value) {
            $('.type-receivers-option').hide();
            $('.type-receivers-option[data-type="'+value+'"]').show();
        }

        init();

    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */


    _init();

    return {

    }

});
