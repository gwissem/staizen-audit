var ManagementCaseTools = (function () {

    var $caseServicesSearchForm,
        $caseServicesTable,
        $caseInfoBox,
        $previewOfService,
        $caseServicesPortlet,
        $caseNotesPortlet,
        $caseNotes,
        $matrixOfCase,
        $actionsOfCase,
        $caseInfoPortlet,
        caseNoteModule,
        emailPreviewModule,
        $makeComplaintButton,
        $makecomplaintModal,
        $changeCarTypeModal,
        $changeCarTypeForm,
        $dfCloseCaseProposalForm,
        $dfMakeComplaintForm,
        $closeCaseProposalButton,
        $dfBox,
        caseRootId,
        $closeCaseProposalModal,
        $caseListBody;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $caseServicesSearchForm = $('#case-services-search-form');
        $caseServicesTable = $('#case-services-table');
        $caseInfoBox = $('#case-info-box');
        $previewOfService = $('#preview-of-service');
        $caseInfoPortlet = $('#case-info-portlet');
        $caseServicesPortlet = $('#case-services-portlet');
        $caseNotesPortlet = $('#case-notes-portlet');
        $caseNotes = $('#case-notes');
        $matrixOfCase = $('#matrix-of-case');
        $actionsOfCase = $('#actions-of-case');
        $caseListBody = $('#case-list-body');
        $dfBox =$('#case-df-complaints');
        $closeCaseProposalButton = $('#case-info-make-close-case-proposal');
        $closeCaseProposalModal = $('#modalCloseCaseProposal');
        $makeComplaintButton = $('#case-info-make-complaint');
        $makecomplaintModal = $('#modalMakeComplaint');
        $dfMakeComplaintForm = $('#modalMakeComplaint form');
        $changeCarTypeButton = $('#case-info-make-car-change-proposal');
        $changeCarTypeModal = $('#modalChangeCarType');
        $changeCarTypeForm = $('#modalChangeCarType form');
        $dfCloseCaseProposalForm = $('#modalCloseCaseProposal form');
        caseNoteModule = CaseNoteModule();
        emailPreviewModule = EmailPreviewModule();

        _handleEvents();
    }

    function setDFinButtons(rootId) {
        loadDFButtonsNumbers(rootId);
        $('#complaintRootId').val(rootId);
        updateDFServicesList(rootId);
        setupCloseCaseProposalModal(rootId)
        setupCarChangeModal(rootId)

    //    SET LIST OF GROUPS

    }
    function setupCloseCaseProposalModal(rootId) {
        $('#closeProposalRootId').val(rootId);
    }
    function setupCarChangeModal(rootId) {
        $('#carChangeRootId').val(rootId);
    }

    function updateDFServicesList(rootId) {
        var listOfServices = [];

        $.ajax({
            url: Routing.generate('management_supervisor_case_tools_get_services_list', {rootId: rootId}),
            type: 'GET',
            success: function (response) {
                listOfServices = response.options;
                $('#complaintGroupProcessInstance option').each(function (){
                    if (
                        $(this).attr('value') != ""){
                        $(this).remove();
                    }
                });
                $(listOfServices).each(function (index) {
                    optionText = this.name;
                    optionValue = this.value;
                    $('#complaintGroupProcessInstance').append(`<option value="${optionValue}">
                                       (${optionValue}) - ${optionText}
                                  </option>`);
                })
            },
        });

        $.ajax({
            url:Routing.generate('management_supervisor_case_tools_get_towing_services_list', {rootId: rootId}),
            type:'GET',
            success:function (response) {
                var listOfServicesTowing = response.options;
                $(listOfServicesTowing).each(function (index) {
                    optionText = this.name;
                    optionValue = this.value;
                    $('#modalChangeCarGroupProcessInstanceId option').each(function (){
                        if (
                            $(this).attr('value') != ""){
                            $(this).remove();
                        }
                    });
                    $('#modalChangeCarGroupProcessInstanceId').append(`<option value="${optionValue}">
                                       (${optionValue}) - ${optionText}
                                  </option>`);
                })

            }

        })

    }

    function refreshNotes(rootId) {

        $.ajax({
            url: Routing.generate('operationalDashboardInterfaceGetNotesAndInfo', {processInstanceId: rootId, displayInfo: false}),
            type: "GET",
            beforeSend: function () {

            },
            success: function (response) {

                $.each(response.notes.list, function (key, note) {
                    if (note.content) {
                        caseNoteModule.appendNote(note, 'prepend', key);
                    }

                    if($caseNotesPortlet.length) {
                        $caseNotesPortlet.on('click', '.preview-email', function () {
                            $('.note-inbox').attr('data-document-id', this.getAttribute('data-attachment-id'));
                            if (typeof CustomEvent !== 'undefined') {
                                window.document.body.dispatchEvent(new CustomEvent("refresh-note-inbox"));
                            }
                        });
                    }

                });

                // if($('.accordion-expand-all').hasClass('active')) {
                //
                //     $('.notification-container').find('.title').addClass('absolute-header').attr({
                //         "aria-selected": "true",
                //         "aria-expanded": "true"
                //     }).next().attr("aria-hidden", "false").show().next().next().show();
                // }
            },
            complete: function () {
                $caseNotesPortlet.fadeIn(300).removeClass('loading');
            }
        });

    }

    function initNoteContainer() {

        $(".notification-container").accordion({
            header: "h4",
            heightStyle: "content",
            collapsible: true,
            beforeActivate: function (event, ui) {
                if (ui.newHeader[0]) {
                    var currHeader = ui.newHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                } else {
                    var currHeader = ui.oldHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                }

                var isPanelSelected = currHeader.attr('aria-selected') == 'true';

                currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

                currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

                currContent.toggleClass('accordion-content-active', !isPanelSelected);

                return false;
            }
        });

        $('#case-notes').on('click', '.note-title', function () {
            var $t = $(this);
            $t.next('.note-content').toggle();

            if ($t.hasClass('absolute-header')) {
                $t.removeClass('absolute-header');
                $t.find('.header-text').addClass('visible');
                $t.find('.date-and-time').removeClass('visible');
            } else {
                $t.addClass('absolute-header');
                $t.find('.header-text').removeClass('visible');
                $t.find('.date-and-time').addClass('visible');
            }
        })

    }
    function setCaseInfoEvent() {
        $('.caseinfo').off('click').on('click',function(){
           var caseID = $(this).data().case;
            $('#search-query').val(caseID);
            $('#search-type').val(0).trigger('change.select2');

            $caseServicesSearchForm.submit();
        });
    }

    function loadDFButtonsNumbers(rootId) {
        $.ajax({
            url: Routing.generate('management_supervisor_df_case_stats', {rootId: rootId}),
            type: "GET",
            success: function (response) {
                    $('#rootClaim').html(response.claim.rootCount);
                    $('#groupClaim').html(response.claim.groupCount);
                    $('#closeCaseProposalCount').html(response.closeCase);
                    $('#carChangeActionCount').html(response.carChange);

            }
        })

    }

    function _handleEvents() {

        initNoteContainer();



        $closeCaseProposalButton.on('click',function (e) {
            e.preventDefault();
            $closeCaseProposalModal.modal('show');
        });

        $changeCarTypeButton.on('click',function (e) {
            e.preventDefault();
            $("#modalCarChange option:selected").removeAttr('selected');
            $("#modalCarChange").select2('destroy').select2();
            $('#modalChangeCarGroupProcessInstanceId option:selected').removeAttr('selected');
            $('#modalChangeCarGroupProcessInstanceId').select2('destroy').select2();
            $changeCarTypeModal.modal('show');
        });

        $dfCloseCaseProposalForm.on('submit',function (e)
        {
            e.preventDefault();
            if($('#closeProposalText').val().length >0) {

                $.ajax({
                    url: Routing.generate('management_supervisor_case_tools_make_close_case_proposal'),
                    type: "POST",
                    data: {
                        rootId: $('#closeProposalRootId').val(),
                        proposalCaseClosedText: $('#closeProposalText').val(),
                    },
                    success: function (resp) {
                        toastr.success('Wysłano prośbę o ZO');
                        $('#closeProposalText').val('');
                        $closeCaseProposalModal.modal('hide');
                        loadDFButtonsNumbers($('#closeProposalRootId').val())
                    }
                });
            }else{
                toastr.error('Uzupełnij wszystkie pola')
            }

        });

        $changeCarTypeForm.on('submit',function (e)
        {
            e.preventDefault();
            if($('#modalCarChange').val() >0 ){
                    //MAKE COMPLAINT FORM SEND

                    $.ajax({
                        url: Routing.generate('management_supervisor_case_tools_car_change'),
                        type: "POST",
                        data: {
                            rootId: $('#carChangeRootId').val(),
                            contractorCarTypeId: $('#modalCarChange').val(),
                            groupProcessInstanceId:  $('#modalChangeCarGroupProcessInstanceId').val(),
                            contractorCarType: $("#modalCarChange option:selected").html()
                        },
                        success: function (resp) {
                            toastr.success('Zmieniono samochód kontraktora w sprawie');
                            $changeCarTypeModal.modal('hide');
                            $("#modalCarChange option:selected").removeAttr('selected');
                            $("#modalCarChange").select2('destroy').select2();

                        }
                    });

                }else{
                toastr.error('Uzupełnij wszystkie pola')
            }
        });

        $makeComplaintButton.on('click',function (e) {
            e.preventDefault();
            updateDFServicesList($('#complaintRootId').val());
            $('#complaintGroupProcessInstance option:selected').removeAttr('selected');
            $('#complaintGroupProcessInstance').select2('destroy').select2();
            $('#complaintType option:selected').removeAttr('selected');
            $('#complaintType ').select2('destroy').select2();
            $('#complaintText').val('');
            $makecomplaintModal.modal('show');
        });


        $dfMakeComplaintForm.on('submit', function (e) {
            //MAKE COMPLAINT FORM SEND
            e.preventDefault();

            if (
                $('#complaintGroupProcessInstance').val().length > 0 &&
                $('#complaintType').val().length > 0 &&
                $('#complaintText').val().length > 0)
            {
                $.ajax({
                    url: Routing.generate('management_supervisor_case_tools_make_complaint'),
                    type: "POST",
                    data: {
                        rootId: $('#complaintRootId').val(),
                        groupOfComplaint: $('#complaintGroupProcessInstance').val(),
                        complaintType: $('#complaintType').val(),
                        complaintText: $('#complaintText').val()
                    },
                    success: function (resp) {
                        toastr.success('Zgłoszono reklamację');
                        $makecomplaintModal.modal('hide');
                        $('#complaintGroupProcessInstance option:selected').removeAttr('selected');
                        $('#complaintGroupProcessInstance').select2('destroy').select2();
                        $('#complaintType option:selected').removeAttr('selected');
                        $('#complaintType ').select2('destroy').select2();
                        $('#complaintText').val('');
                    }
                });
            }else{
                toastr.error('Uzupełnij wszystkie pola')
            }


        });



            $caseServicesSearchForm.on('submit', function (e) {

            e.preventDefault();

            var $t = $(this),
                rootId = $t.find('#search-query').val(),
                searchQuery = $t.find('#search-query').val(),
                searchType = $t.find('#search-type').val();

            if(!rootId) return;

            var ladda = Ladda.create($t.find('button')[0]);

            ladda.start();

            $.ajax({
                url: Routing.generate('management_supervisor_case_tools_services_table', {rootId: rootId,searchQuery: searchQuery, searchType :searchType}),
                type: 'GET',
                beforeSend: function () {
                    _resetAllPanel();
                    $caseListBody.html('');
                },
                success: function (response) {
                    if (searchType == 0) {

                        // Tu lepiej używać response.rootId niż rootId, bo jest przeparsowane

                        if (response.services) {
                            $caseServicesTable.html(response.services);
                        }
                        else {
                            $caseServicesTable.html('');
                            $caseNotesPortlet.removeClass('loading');
                        }

                        if (response.caseInfo && response.rootId) {
                            $caseInfoBox.html(response.caseInfo);
                            $caseInfoBox.append(response.rsList);
                            $caseInfoBox.append(response.rzwList);
                            $caseInfoBox.append(response.emailAttachments);
                            $caseInfoBox.append(response.documents);
                            caseRootId = response.rootId;
                            setDFinButtons(response.rootId);
                            refreshNotes(response.rootId);
                        }
                        else {
                            $caseInfoBox.html('');
                        }

                        if(response.matrixProcessInstance) {
                            _loadMatrix(response.matrixProcessInstance);
                        }

                        var cdnPromise = loadInfoFromCDN(response.rootId);

                        $.when(cdnPromise).done(function (cdnResult) {
                            setCheckCDNinServiceTable(cdnResult);
                        });

                    } else
                        {
                            caseRootId = response.rootId;
                            setDFinButtons(response.rootId);

                        for(var $i = 0; $i < response.casesFound.length;$i++ )
                        {
                            var tableToInsert = response.casesFound[$i];
                            var row = $('<tr class="">' +
                                '<td><button class="btn btn-primary caseinfo" data-case="'+tableToInsert.caseNumber+'"> '+tableToInsert.caseNumber+'</button></td>' +
                                '<td>'+tableToInsert.createdAt+'</td>' +
                                '<td>'+((tableToInsert.firstname|| " ")  +' '+  (tableToInsert.lastname|| " "  ) )+'</td>' +
                                '<td>'+ (tableToInsert.registrationNumber || "-" )+'</td>' +
                                '<td>'+ (tableToInsert.makeModel || "-" )+'</td>' +
                                // '<td>'+ (tableToInsert.platform || "-" )+'</td>' +
                                // '<td>'+ (tableToInsert.program || "-" )+'</td>' +
                                '</tr>');
                            $caseListBody.append(row)

                        }
                            $('#case-search-portlet').fadeIn(300)
                            $('#case-df-complaints').fadeIn(300)
                        setCaseInfoEvent();
                        // $('.caseinfo').first().click();
                    }
                },
                error: function(error) {
                    if(error.statusText !== "abort") {
                        handleAjaxResponseError(error);
                    }
                },
                complete: function () {
                    ladda.stop();
                    $caseServicesPortlet.removeClass('loading');
                    $caseInfoPortlet.removeClass('loading');
                }
            });

        });

        $caseServicesTable.on('click', '.service-row', function (e) {

            e.preventDefault();
            $previewOfService.fadeIn(300).addClass('loading');

            var $t = $(this),
                serviceId = $t.attr('data-service-id'),
                groupProcessId = $t.attr('data-group-process-id');

            $caseServicesTable.find('.service-row.active').removeClass('active');
            $t.addClass('active');

            $.ajax({
                url: Routing.generate('operationalDashboardInterfaceOverviewService', {processId: groupProcessId, serviceId: serviceId, extraDataService: true}),
                type: "GET",
                beforeSend: function () {
                    $previewOfService.find('.portlet-body').html('');
                },
                success: function (result) {

                    if(result.services) {

                        var services = result.services.filter(function (s) {
                            return (parseInt(s.id) ===parseInt(groupProcessId));
                        });

                        if(services.length > 0) {
                            var service = services[0];
                            $previewOfService.find('.portlet-body').html(service.overview);
                        }
                    }

                },
                complete: function () {
                    $previewOfService.removeClass('loading');
                }
            });

        });

    }

    function setCheckCDNinServiceTable(result) {

        $caseServicesTable.find('.check-in-cdn').html('<i class="fa fa-close fa-2x font-red"></i>');

        $.each(result, function (i, ele) {
            $caseServicesTable.find('.check-in-cdn[data-process-id="'+ele.groupProcessId+'"]').html('<i class="fa fa-check fa-2x font-green"></i>');
        });

    }

    function loadInfoFromCDN(rootId) {

        var dfd = jQuery.Deferred();

        $.ajax({
            url: Routing.generate('ms_case_tools_cdn_info', {rootProcessId: rootId}),
            type: "GET",
            success: function (result) {
                dfd.resolve(result);
            },
            complete: function () {
                dfd.reject([]);
            }
        });

        return dfd;

    }

    function _resetAllPanel() {
        $caseServicesTable.html('');
        $caseInfoBox.html('');
        $previewOfService.fadeOut(300).find('.portlet-body').html('');
        $caseServicesPortlet.fadeIn(300).addClass('loading');
        $caseNotesPortlet.fadeIn(300).addClass('loading');
        $caseNotes.html('');
        $caseInfoPortlet.fadeIn(300).addClass('loading');
        $matrixOfCase.find('.dynamic-widget-content').html('');
        $matrixOfCase.fadeOut(300);
        $dfBox.fadeIn(300)
        $actionsOfCase.fadeOut(300);
        $actionsOfCase.find('.portlet-body').html('');
    }

    /** Chwilowo nie używane */
    function _loadActionsOfService(groupProcessId, serviceId) {

        var url = Routing.generate('management_supervisor_case_tools_actions_service', {groupProcessId: groupProcessId, serviceId: serviceId});

        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                $actionsOfCase.fadeIn(300).addClass('loading');
                $actionsOfCase.find('.portlet-body').html('');
            },
            success: function (result) {

                if(result.actions) {

                    $.each(result.actions, function (i, ele) {

                        var newBtn = '<button class="btn blue btn-block" data-json="'+ele.data+'">'+ele.name+'</button>';
                        $actionsOfCase.find('.portlet-body').append(newBtn);

                    });

                }

            },
            complete: function () {
                $actionsOfCase.removeClass('loading');
            },
            error: function (error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            }
        });


    }

    function _loadMatrix(matrixProcessInstanceId) {

        var dfd = jQuery.Deferred();

        var url = Routing.generate('operationalDashboardInterfaceGetDynamicWidget', {processInstanceId: matrixProcessInstanceId, name: "matrix"});

        $.ajax({
            url: url,
            type: "GET",
            success: function (result) {
                $matrixOfCase.find('.dynamic-widget-content').html(result.widget.html);
                $matrixOfCase.find('input, select').prop("disabled", true);
                $matrixOfCase.find('select[data-attribute-path]').select2();
                dfd.resolve($matrixOfCase.find('.dynamic-widget-content'));
            },
            complete: function () {
                $matrixOfCase.fadeIn(300);
            },
            error: function (error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            }
        });

        return dfd;

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {

    }

});
