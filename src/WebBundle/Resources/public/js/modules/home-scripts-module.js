/**
 * @class HomeScriptModule
 */
var HomeScriptModule = (function (window) {

    /**
     * @constructor
     */
    function HomeScriptModule () {

        HomeScriptModule.$atlasIframe = document.getElementById('atlas-iframe');
        window.STATE = HomeScriptModule.const.STATE_OTHER;
    }

    HomeScriptModule.const = {};
    HomeScriptModule.const.STATE_DASHBOARD = "dashboard";
    HomeScriptModule.const.STATE_OTHER = "other";

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    /**
     * @description "Przechwytywanie Eventów "message" dla Window"
     */
    function handleEvents() {
        window.addEventListener("message", receiveMessage, false);
    }

    function receiveMessage(event) {
        execMessage(event.data);
    }

    function execMessage(message) {

        if (message.action === 'set-state' && message.url) {

            if(message.url.indexOf('/operational-dashboard') !== -1) {
                window.STATE = HomeScriptModule.const.STATE_DASHBOARD;
            }
            else {
                window.STATE = HomeScriptModule.const.STATE_OTHER;
            }

        }

    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */


    /**
     * @method sendToIframe
     * @param {Object | String} message
     * @description "Wysyła wiadomość do iframe"
     */

    HomeScriptModule.prototype.sendToIframe = function (message) {

        HomeScriptModule.postMessage(message, Routing.generate('index', [], true));

    };

    /**
     * @method getWindowState
     * @return {String}
     */

    HomeScriptModule.prototype.getWindowState = function () {

        return window.STATE;

    };


    /**
     * @method run
     */
    HomeScriptModule.prototype.run = function () {
        handleEvents.call(this);
    };

    return HomeScriptModule;

})(window);

var homeScript = new HomeScriptModule();
homeScript.run();