var ManagementPrioritySteps = (function () {

    var $wrapper,
        $content,
        $reportTab,
        $reportTabLi;

    var loaded = false;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $wrapper = $('#priority-steps-wrapper');
        $content = $wrapper.find('.tab-wrapper-content');
        $reportTab = $('a[href="#priority-steps"]');
        $reportTabLi = $reportTab.parent();

        _handleEvents();

    }

    function _handleEvents() {

        $reportTab.on('shown.bs.tab', function() {
            iniLoad();
        });

        if($reportTabLi.hasClass('active')) {
            iniLoad();
        }

    }

    function iniLoad() {
        if(!loaded) {
            _loadPanel();
        }
        else {
            // _refreshTable();
        }
    }

    function _loadPanel() {

        var templateId = null;

        if (true === loaded) {
            templateId = $('#pushing-template select').val();
            $content.html('');
        }

        $.ajax({
            url: Routing.generate('management_supervisor_priority_index', {'id' : templateId}),
            type: 'GET',
            beforeSend: function () {
                $wrapper.addClass('loading');
            },
            success: function (response) {

                loaded = true;
                $content.html(response.html);
                _afterLoadPanel();

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $wrapper.removeClass('loading');
                $('.select2').select2();
            }
        });

    }

    function _afterLoadPanel() {

        $('.toggle-steps').on('click',function(e){
            e.preventDefault();

            var $t = $(this);
            var taskId = $t.closest('tr[data-type="task"]').attr('data-id');
            if($t.hasClass('expanded')){
                $t.find('i.fa').removeClass('fa-minus').addClass('fa-plus');
            }
            else{
                $t.find('i.fa').removeClass('fa-plus').addClass('fa-minus');
            }

            $('#priority-table tr[data-type="step"][data-parent-id="'+taskId+'"]').toggleClass('hidden');
            $t.toggleClass('expanded');

        });

        $('#priority-table .btn-save').on('click',function(e){

            e.preventDefault();

            var $t = $(this);
            var $row = $t.closest('tr');
            var id = $row.attr('data-id');
            var type = $row.attr('data-type');
            var priority = $row.find('input.priority').val();
            var maxPriority = $row.find('input.maxPriority').val();
            var stepPriority = $row.find('input.stepPriority').val();
            var currentTemplate = $('#pushing-template select').val();
            var groupId = $row.attr('data-parent-id');

            var data = {
                'priority' : priority,
                'maxPriority' : maxPriority,
                'stepPriority': stepPriority,
                'currentTemplate' : currentTemplate,
                'userGroupId': groupId
            };

            $.ajax({
                url: Routing.generate('management_supervisor_priority_change', {'id' : id, 'type' : type}),
                data: data,
                type: "POST",
            }).success(function (response) {
                toastr.success("Pomyślnie zapisano priorytet", "Zapisano", {timeOut: 5000});
                if(type == 'task'){
                    var $step = $('#priority-table tr[data-type="step"][data-parent-id="'+id+'"]');
                    $step.find('input.priority').val(priority);
                    $step.find('input.maxPriority').val(maxPriority);
                }
            });
        });

        $('.add-new-template-form').on('submit', function (e) {

            e.preventDefault();

            var $t = $(this);

            var data = $t.serializeArray();
            data.push({name: 'chosenTemplate', value: $('#template').val()});

            $.ajax({
                url: Routing.generate('management_supervisor_priority_add_template'),
                type: 'POST',
                data: data,
                success: function (response) {
                    $('#template').html(response.html);
                    toastr.success("Dodano nowy szablon.", {timeOut: 5000});
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    $('.select2').select2();
                }
            });
        });

        $('.remove-template').on('submit', function (e) {

            e.preventDefault();

            var $t = $(this);

            $.ajax({
                url: Routing.generate('management_supervisor_priority_remove_template'),
                type: 'POST',
                data: $t.serialize(),
                success: function (response) {
                    _loadPanel();
                    toastr.success("Szablon został usunięty.", {timeOut: 5000});
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    $('.select2').select2();
                }
            });
        });

        $('#pushing-template select').on('change', function () {
            _loadPanel();
        });

    }

    $(function () {
        _init();
    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {}

});
