var DynamicalWidgetModule = (function (_dashboardService) {

    var _init = function () {

    };

    var _handleEvents = function () {


    };

    function _loadWidgets(callback) {

        $.each($('.dynamic-widget'), function (i, ele) {
            _loadWidget($(ele), callback);
        })

    }

    function _loadWidget($ele, callback) {

        var name = $ele.attr('data-name'),
            url = Routing.generate('operationalDashboardInterfaceGetDynamicWidget', {processInstanceId: _dashboardService.getCurrentTicket('id'), name: name});

        $.ajax({
            url: url,
            type: "GET",
            success: function (result) {

                $ele.find('.dynamic-widget-content').html(result.widget.html);

                if(typeof callback !== "undefined") {
                    callback($ele);
                }

            },
            error: function (error) {
                if(error.statusText !== "abort") {
                    handleAjaxResponseError(error);
                }
            }
        });

    }

    function _checkMatrixInputs($content) {

        $.each($content.find('.row.entity-row'), function (i, ele) {

            _checkRowSum($(ele));

        })

    }

    function _checkRowSum($row) {

        $row.find('input[type="text"]').on('change', function (e) {
            _checkSum.call(this, $row, e);
        });

        _checkSum($row);

    }

    function _checkSum($row, e) {

        var validSum = parseInt($row.find('[data-attribute-path="820,821,822,221"]').val()),
            sum = 0;

        $.each($row.find('input[type="text"]').not('[data-attribute-path="820,821,822,221"]'), function (i, ele) {
            sum += parseInt(ele.value) || 0;
        });

        if(validSum !== sum) {

            if(typeof e !== "undefined" && this.value) {
                // e.stopPropagation();
            }
            _f.disableNext();
            $row.addClass('bg-red');
        }
        else {
            $row.removeClass('bg-red');
            _f.enableNext();
        }

    }

    function _initFaq($widget) {

        var $accordion = $widget.find('.faq-accordion'),
            $answer = $('.grid-stack-item[data-path="997"] input');

        $widget.find('.select2').select2().on('change', function (e) {

            e.preventDefault();

            var _type = $(this).val();

            if(!_type) return;

            $.ajax({
                url: Routing.generate('faq_content', { groupProcessInstanceId: _dashboardService.getCurrentTicket('groupProcessId'), type: _type}),
                type: "GET",
                beforeSend: function() {
                    $accordion.html(miniLoaderTemplate);
                },
                success: function (result) {

                    $widget.find('.faq-accordion').html(result.html);
                    _initCheckboxes($answer.val());

                },
                error: function (error) {
                    if(error.statusText !== "abort") {
                        handleAjaxResponseError(error);
                    }
                }
            });

        }).trigger('change');

        $accordion.on('click', '.activate-service-button', _activeService);

        $accordion.on('click', '.question-checkbox input', function () {

            var $t = $(this);

            _updateAnswer($answer, this.value);

            if($t.is(":checked")) {
                $t.closest('.panel.panel-default').find('button').prop("disabled", false);
            }
            else {
                $t.closest('.panel.panel-default').find('button').prop("disabled", true);
            }

        });

    }

    function _initCheckboxes(values) {
        $.each(values.split(','), function (i, ele) {
            var $check = $('#question-' + ele);
            $check.prop('checked', "checked");
            $check.closest('.panel.panel-default').find('button').prop("disabled", false);
        })
    }

    function _updateAnswer($answer, value) {

        var _val = $answer.val(),
            values = [];

        if(_val) {
            values = $answer.val().split(',');
        }

        var index = values.indexOf(value);

        if(index !== -1) {
            // Jeżeli istnieje to usuwanie
            values.splice(index, 1);
        }
        else {
            // jeżeli nie istnieje to dodanie do tablicy
            values.push(value);
        }

        $answer.val(values.join()).trigger('change');

    }

    function _activeService() {

        var $t = $(this),
            btn = Ladda.create($t[0]),
            startStepId = $t.attr('data-start-step');

        if($t.is(':disabled')) return false;

        $.ajax({
            url: Routing.generate('case_ajax_activate_service'),
            type: "POST",
            beforeSend: function() {
                btn.start();
            },
            data: {
                'startStepId' : startStepId,
                'processInstanceId' : _dashboardService.getCurrentTicket('id'),
                'groupProcessId' :  _dashboardService.getCurrentTicket('groupProcessId'),
                'attributePath' : $t.attr('data-attribute-path'),
                'value' : $t.attr('data-attribute-value')
            },
            success: function () {
                _f.nextStep();
            },
            complete: function () {
                btn.stop();
            }
        });

    }

    _init();

    return {
        loadWidgets: _loadWidgets,
        checkMatrixInputs: _checkMatrixInputs,
        initFaq: _initFaq
    }

});