/**
 * @class EmailInboxEditorModule
 */
var EmailInboxEditorModule = (function(opts) {

    var $body,
        $noteInboxEditor,
        $noteTypes,
        $inboxEditorWrapper,
        $inboxEditorContent,
        $emailNoteBtn,
        $currentInboxForm = null,
        addressBook = [],
        isEmbeded = false;

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function recalculateHeight() {

        if(isEmbeded) return false;

        var top = 0;

        if($noteInboxEditor.find('.full-mode').length === 0) {
            top = $('.notifications-outer-wrapper').position().top + $noteTypes.outerHeight(true);
        }

        $noteInboxEditor.css({
            height: ($('#page-main-content').height() - top)
        });

    }

    function _setPositionNoteInbox() {

        if(!isEmbeded) {

            var top = $('.notifications-outer-wrapper').position().top + $noteTypes.outerHeight(true),
                height = ($('#page-main-content').height() - top);

            $noteInboxEditor.css({
                height: height,
                top: top,
                left: 0
            });

            $noteInboxEditor.find('.full-mode').removeClass('full-mode');

        }

        $noteInboxEditor.addClass('active');

    }

    function _loadInboxEditorContent(dataEmail) {

        $inboxEditorWrapper.addClass('loading');
        $inboxEditorContent.empty();

        var rootNoteId;

        if(isEmbeded) {
            rootNoteId = parseInt($noteInboxEditor.attr('data-notes-root-id'));
        }
        else {
            rootNoteId = parseInt($('.notification-module').attr('data-notes-root-id'));
        }

        console.log(dataEmail);

        $.ajax({
            url: Routing.generate('mailboxGetInboxEditor', {rootNoteId: rootNoteId}),
            type: "GET",
            data: (typeof dataEmail !== "undefined") ? dataEmail : null,
            success: function (result) {

                _beforeLoadInboxEditor(result);
                $inboxEditorContent.html(result.inboxEditor);
                $currentInboxForm = $inboxEditorContent.find('form');
                _afterLoadInboxEditor();

            },
            error: function (error) {
                handleAjaxResponseError(error);
            },
            complete: function () {
                $inboxEditorWrapper.removeClass('loading');
                // recalculateHeight($emailPreviewWrapper);
            }
        });

    }

    function _beforeLoadInboxEditor(result) {
        if(!result.addressBook) {
            addressBook = [];
        }
        else {
            addressBook = result.addressBook;
        }

    }

    function _parseAddressBook(queryTag, $ele) {

        var results = [],
            values = $ele.closest('.bootstrap-tagsinput').siblings('input.tagsinput-control').val();

        $.each(addressBook, function (i, ele) {
            if((ele.id.indexOf(queryTag) !== -1 || ele.label.indexOf(queryTag) !== -1) && values.indexOf(ele.id) === -1) {
                results.push(ele);
            }
        });

        return results;
    }

    function _simpleValidEmail(email)
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    function _afterLoadInboxEditor() {

        $currentInboxForm.find('.select2').select2({
            minimumResultsForSearch: -1
        });

        $currentInboxForm.find('.tagsinput-control').tagsinput({
            freeInput: true,
            cancelConfirmKeysOnEmpty: false,
            hint: false,
            typeaheadjs: {
                source: function (query, processSync) {
                    processSync(_parseAddressBook(query, this.$el));
                },
                async: false,
                display: function(a) {
                    return a.id;
                },
                templates: {
                    suggestion: function (data) {
                        return '<div>' + data.label + '</div>';
                    }
                },
                valueKey: 'id',
                displayKey: 'id',
                limit: 50
            }
        });

        /** Blur na wpisywanie adresu email */
        $currentInboxForm.find('.tt-input').on('blur', function () {
            var $t = $(this),
                value = $t.val();

            if(value) {
                if(_simpleValidEmail(value)) {
                    $t.closest('.bootstrap-tagsinput').next().tagsinput('add', value);
                }
                $t.val('');
            }

        });

        /** Blur na wpisywanie adresu email */
        $currentInboxForm.find('.tt-input').on('focus', function () {
            var $t = $(this);

            if(!$t.val()) {
                $t.typeahead('val', '@').typeahead('open').val('');
            }

        });

        AtlasUploaderDocumentModule({
            $wrapper: $currentInboxForm.find('.atlas-upload-file-wrapper'),
            $result: $currentInboxForm.find('.uploaded-files'),
            params: {
                maxFileSize: 25000000 // 25MB
            }
        });

        $currentInboxForm.on('submit', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        $currentInboxForm.find('button[type="submit"]').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();

            // var rootNoteId = parseInt($('.notification-module').attr('data-notes-root-id'));
            var rootNoteId = parseInt($currentInboxForm.attr('data-note-root-id'));
            var btn = Ladda.create($currentInboxForm.find('button[type="submit"]')[0]);

            btn.start();

            var postData = {
                    files: [],
                    controls: {}
                },
                control = null;

            postData.files = $currentInboxForm.find('.uploaded-files').val().split(',');

            var formData = { };
            $.each($currentInboxForm.serializeArray(), function() {
                formData[this.name] = this.value;
            });

            $currentInboxForm.find('[data-id]').each(function (i, ele) {

                control = {
                    'id': ele.getAttribute('data-id'),
                    'parent_attribute_value_id': ele.getAttribute('data-parent_attribute_value_id'),
                    'attribute_path': ele.getAttribute('data-attribute_path')
                };

                control['value'] = formData[control.attribute_path];
                postData.controls[control.attribute_path] = control;

            });

            postData.isForward = formData.is_forward;
            postData.fileUuid = formData.email_file_uuid;

            $.ajax({
                url: Routing.generate('mailboxSaveEmailInboxEditor', {rootNoteId: rootNoteId}),
                type: "POST",
                data: postData,
                success: function (result) {

                    if(result.success) {

                        $.each(result.notes, function (key, note) {
                            if (note.content) {

                                if(typeof opts._addEmailNote === "function") {
                                    opts._addEmailNote(note, 'prepend', key, {openAfterAdd: true});
                                }

                                if(typeof opts.appendEmail === "function") {
                                    opts.appendEmail(key);
                                }
                            }
                        });

                        if(isEmbeded) {
                            if(result.notes.length ) {
                                var firstEmail = result.notes[0];
                                toastr.success('Odbiorca: ' + firstEmail.value + '<br>Temat: ' + firstEmail.subject, 'E-mail został pomyślnie wysłany.');
                            }
                        }

                        $currentInboxForm[0].reset();
                        _hideInboxEditor();

                    }
                    else {
                        toastr.error(result.message, 'Błąd wysłania e-maila.', 5000);
                    }

                },
                error: function (error) {
                    handleAjaxResponseError(error);
                },
                complete: function () {
                    btn.stop();
                }
            });
        });

    }

    function eventsHandle() {

        // window.document.body.dispatchEvent(new CustomEvent("refresh-preview-email"));

        $inboxEditorWrapper.on('click', '.close-inbox', function (e) {
            e.preventDefault();
            _hideInboxEditor();
        });

        $inboxEditorWrapper.on('click', '.resize-inbox', function (e) {
            e.preventDefault();

            var $t = $(this);

            var top = 0,
                height = 200;

            if($t.hasClass('full-mode')) {

                $t.removeClass('full-mode');

                if(isEmbeded) {

                    $noteInboxEditor.removeClass('full-width-mode');

                }
                else {

                    top = $('.notifications-outer-wrapper').position().top + $noteTypes.outerHeight(true);
                    height = ($('#page-main-content').height() - top);

                    $noteInboxEditor.css({
                        left: 0,
                        top: top,
                        height: height
                    })

                }

            }
            else {

                $t.addClass('full-mode');

                if(isEmbeded) {

                    $noteInboxEditor.addClass('full-width-mode');

                }
                else {

                    height = ($('#page-main-content').height() - top);

                    $noteInboxEditor.css({
                        left: -($('.form-placement').width() + 15),
                        top: top,
                        height: height
                    })

                }


            }

        });

        $inboxEditorWrapper.on('click', '.inbox-cc', function (e) {
            e.preventDefault();
            $inboxEditorContent.find('.input-cc, .input-bcc').toggle();
        });

        funResizeHandle.addFunction('recalculateHeightInboxEditor', recalculateHeight);

    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _showInboxEditor(data) {

        if($noteInboxEditor.length) {
            $emailNoteBtn.addClass('chosen');
            _setPositionNoteInbox();
            _loadInboxEditorContent(data);
        }

    }

    function _hideInboxEditor() {
        if($noteInboxEditor.length) {
            $noteInboxEditor.removeClass('active');
            $emailNoteBtn.removeClass('chosen');
        }
    }

    function init() {

        $noteInboxEditor = $('#note-inbox-editor');

        if($noteInboxEditor.length) {
            $body = $('body');

            if($noteInboxEditor.hasClass('embeded-inbox')) {
                isEmbeded = true;
            }

            $noteTypes = $('#note-types');
            $inboxEditorWrapper = $noteInboxEditor.find('.inbox-editor-wrapper');
            $inboxEditorContent = $inboxEditorWrapper.find('.inbox-editor-content');
            $emailNoteBtn = $('#email-note-btn');

            eventsHandle();
        }

    }

    init();

    return {
        showInboxEditor: _showInboxEditor,
        hideInboxEditor: _hideInboxEditor
    }

});
