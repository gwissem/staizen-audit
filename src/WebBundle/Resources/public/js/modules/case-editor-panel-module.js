var changeForeignPartnerData;
var CaseEditorPanelModule = (

    /**
     * @param {DashboardSocket} _socket
     * @param {DashboardService} _dashboardService
     * @param {function=} _getTask
     * */
        function (_socket, _dashboardService, _getTask) {

        /*
         * ----------------------------------------------------------------------------------------------------------
         * PRIVATE FUNCTIONS
         * ----------------------------------------------------------------------------------------------------------
         */

        var $caseEditorForm,
            $formSubmit,
            formSubmitLadda,
            $formResult,
            xhrFormRequest,
            isLoading = false;

        function _init() {

            changeForeignPartnerData = _changeForeignPartnerData;

            $caseEditorForm = $("#case-editor-form");
            $formSubmit = $caseEditorForm.find('button[type="submit"]');
            formSubmitLadda = Ladda.create($formSubmit[0]);
            $formResult = $('#form-result');
            $gop = $('#gop');
            _handleEvents();

            _openFirstStep();
        }

        function _openFirstStep(){
            if($('body').hasClass('service-editor')){
                $formResult.find('.mt-list-item:first').trigger('click');
            }
        }

        function _handleEvents() {

            $caseEditorForm.on('submit', function (e) {

                e.preventDefault();

                formSubmitLadda.start();

                var $t = $(this);

                if(xhrFormRequest) {
                    xhrFormRequest.abort();
                }

                xhrFormRequest = $.ajax({
                    type: "POST",
                    url: Routing.generate('case_editor_search_processes'),
                    data: $t.serialize(),
                    success: function (result) {
                        $formResult.find('.result-content').html(result.view);
                    },
                    error: function () {

                    },
                    complete: function () {
                        formSubmitLadda.stop();
                    }
                });

            });

            $formResult.on('click', '.mt-list-item', function () {

                if(isLoading) return false;

                var $t = $(this),
                    id = parseInt($t.attr('data-process-id'));

                if(id) {

                    $formResult.find('.mt-list-item').removeClass('done');
                    $t.addClass('done');

                    _dashboardService.setCurrentTicket({
                        'id': id,
                        'stepId': $t.attr('data-step-id'),
                        'groupProcessId': parseInt($t.attr('data-group-id')),
                        'rootId': parseInt($t.attr('data-root-id'))
                    });

                    isLoading = true;
                    _getTask(id, function () {
                        isLoading = false;
                    });

                }

            });

            $gop.find('#automat-change-state').on('click',function(){
                var $t = $(this),
                    groupProcessId = $gop.attr('data-group-process-id'),
                    action = $t.attr('data-action'),
                    url = Routing.generate('rental_automat_change_state',{
                        groupProcessId : groupProcessId,
                        action : action
                    }),
                    l = Ladda.create($t[0]);
                l.start();

                if(xhrFormRequest) {
                    xhrFormRequest.abort();
                }

                xhrFormRequest = $.ajax({
                    type: "POST",
                    url: url,
                    success: function (result) {
                        if(result.err == 1){
                            toastr.error("Nie można zatrzymać/uruchomić automatu", "Błąd", {timeOut: 5000});
                        }
                        else {
                            if (action == 'start') {
                                $t.attr('data-action', 'stop');
                                $t.html('<i class="fa fa-pause-circle"></i> Zatrzymaj automat przedłużenia wynajmu')
                                toastr.success("Pomyślnie uruchomiono automat przedłużania wynajmu", "Gotowe", {timeOut: 5000});
                            }
                            else {
                                $t.attr('data-action', 'start');
                                $t.html('<i class="fa fa-play-circle"></i> Uruchom automat przedłużenia wynajmu')
                                toastr.success("Pomyślnie zatrzymano automat przedłużania wynajmu", "Gotowe", {timeOut: 5000});
                            }
                        }
                    },
                    error: function () {
                        toastr.error("Nie można zatrzymać/uruchomić automatu", "Błąd", {timeOut: 5000});
                    },
                    complete: function () {
                        l.stop();
                    }
                });

            });

            $gop.find('#gop-preview').on('click',function(){
                var groupProcessId = $gop.attr('data-group-process-id');
                var $final = $('#final');

                var url = Routing.generate('gop_preview', {
                    'groupProcessId' : groupProcessId,
                    'final' : ($final.is(':checked')) ? '1' : '0'
                });
                window.open(url, '_blank');

            });

            $gop.find('#gop-send').on('click',function(){

                var $t = $(this);
                var groupProcessId = $gop.attr('data-group-process-id');
                var $final = $('#final');
                var $email = $('#gop-email');
                var url = Routing.generate('gop_send', {
                    'groupProcessId' : groupProcessId,
                    'final' : ($final.is(':checked')) ? '1' : '0',
                    'email' : $email.val()
                });

                var l = Ladda.create($t[0]);
                l.start();


                if(xhrFormRequest) {
                    xhrFormRequest.abort();
                }

                xhrFormRequest = $.ajax({
                    type: "POST",
                    url: url,
                    success: function (result) {
                        toastr.success("Pomyślnie wysłano GOP", "Gotowe", {timeOut: 5000});
                    },
                    error: function () {

                    },
                    complete: function () {
                        l.stop();
                    }
                });

            });

            document.addEventListener("setAttributeValue", function (e) {
                if (e.detail) {
                    var path = e.detail.path;
                    var partnerPaths = ['610','741','764,742'];
                    var crossBorderRemarkPaths = [];
                    var value = e.detail.value;

                    if($.inArray(path,partnerPaths) >= 0){
                        updateGopEmail(value);
                    }

                    // dostęp do takich danych jak:
                    // path - groupProcessId - stepId - valueId - value - valueType
                }
            });

        }

        /*
         * ----------------------------------------------------------------------------------------------------------
         * PUBLIC FUNCTIONS
         * ----------------------------------------------------------------------------------------------------------
         */

        _init();

        function updateGopEmail(partnerId){
            $.ajax({
                type: "GET",
                url: Routing.generate('ajax_partner_contact',
                {
                    partnerId : partnerId,
                    instanceId: _dashboardService.getCurrentTicket('id'),
                    contactType : 4
                }),
                success: function (result) {
                 $('#gop-email').val(result);
                }
            });
        }

        function _changeForeignPartnerData(id){

            $.ajax({
                type: "POST",
                url: Routing.generate('ajax_set_foreign_partner_contact_data',{id : id, instanceId : _dashboardService.getCurrentTicket('id')}),
                success: function (result) {
                    $('#gop-email').val(result);
                }
            });
        }

        return {

        }



    });
