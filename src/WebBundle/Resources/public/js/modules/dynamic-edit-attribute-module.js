var DynamicEditAttributeModule = (function() {

    var searchAttr = 'data-dynamic-edit-attr',
        attributesSpan = null,
        loaderSrc = '/images/web/images/spinner.gif',
        $modal,
        $modalContent,
        _onModalLoad = null,
        _modalIsOpen = false,
        currentEditSpan = null,
        enterClicked = false;

    /** PUBLIC */

    var _init = function (options) {

        $modal.modal('hide');

        attributesSpan = $("[" + searchAttr + "]");
        attributesSpan.on('dblclick', _onDoubleClick);

        attributesSpan.each(function (e, ele) {
            if(ele.innerText === "") {
                ele.classList.add('empty');
            }
        });

        _onModalLoad = null;

        if(typeof options.onModalLoad === "function") {
            _onModalLoad = options.onModalLoad;
        }

    };

    function _isEdit() {
        return _modalIsOpen;
    }

    function _displayErrorInModal(error) {

        $modal.find('.alert').html(error).show();

    }

    function _hideError() {

        $modal.find('.alert').hide();

    }

    /** PRIVATE */

    var _onSave = function (e) {

        if(currentEditSpan !== null) {

            var $el = $(this);

            if(e.detail.success === true) {

                if(["text", "textarea", "number", "datetime", "date"].indexOf(currentEditSpan.type) !== -1) {

                    currentEditSpan.span.html($el.val());

                }
                else if(["select", "multiselet"].indexOf(currentEditSpan.type) !== -1) {

                    currentEditSpan.span.html($el.find('option:selected').text());

                }
                else if(currentEditSpan.type === "radio") {

                    var _checked = $el.filter(':checked');
                    currentEditSpan.span.html(_checked.next().text());

                }
                else if(currentEditSpan.type === "checkbox") {

                    currentEditSpan.span.html( ($el.filter(':checked').length === 1) ? "Tak" : "Nie" );

                }
                else if(currentEditSpan.type === "yes-no") {

                    currentEditSpan.span.html( (($el.filter(':checked').val() === "1") ? "Tak" : "Nie") );

                }

                var $parent = currentEditSpan.span.parent();
                if($parent.hasClass('empty') && $parent[0].innerText !== "") {
                    $parent.removeClass('empty');
                }
                else if($parent[0].innerText === "") {
                    $parent.addClass('empty');
                }

                toastr.success("", "Pomyślnie zapisano.", {timeOut: 3000});
                $el.removeClass('validation-error');
                _hideError();
                if(enterClicked === true) {
                    if($modal) {
                        $modal.modal('hide');
                    }
                }
            }
            else {

                toastr.error("", "Niepoprawna wartość.", {timeOut: 3000});
                _displayErrorInModal(e.detail.desc);
                $el.addClass('validation-error');

            }

        }

        enterClicked = false;

    };

    var _handleSaveAttribute = function ($el) {

        $el.on('save-attribute', _onSave);

    };

    var _onSubmitForm = function (e) {

        e.preventDefault();
        enterClicked = true;

    };

    var _onDoubleClick = function () {

        var $span = $(this),
            path = $span.attr(searchAttr),
            groupProcessId = $span.closest('.process-info').attr('data-group-process-id');

        currentEditSpan = {
            span: $span.find('.attr-value')
        };

        if($span.hasClass('loading')) return false;

        $span.addClass('loading').append('<img src="'+loaderSrc+'"/>');

        $.ajax({
            url: Routing.generate('case_ajax_dynamic_edit_attribute', {groupProcessId: groupProcessId, path: path}),
            type: "GET",
            success: function (response) {

                if(response.status === true) {
                    $modalContent.html(response.html);

                    currentEditSpan.type = response.type;
                    $modal.modal('show');
                    _handleSaveAttribute($modal.find('[data-attribute-value-id]'));

                    if(_onModalLoad) {
                        _onModalLoad($modalContent.find('.body-edit-attribute'), response);
                    }
                }
                else {

                    currentEditSpan = null;
                    $span.off('dblclick', _onDoubleClick).removeAttr('data-dynamic-edit-attr');
                    toastr.info("", response.msg, {timeOut: 3000});

                }

                $span.removeClass('loading').find('img').remove();

            },
            error: function (error) {
                handleAjaxResponseError(error);
            }
        });

    };



    var _run = function () {

        $modal = $('#dynamic-edit-attribute-modal');
        $modalContent = $modal.find('.modal-content-form');
        $modalContent.on('submit', '.body-edit-attribute', _onSubmitForm);

        $modal.on('shown.bs.modal', function () {
            _modalIsOpen = true;
        });

        $modal.on('show.bs.modal', function () {
            $modal.find('.alert').hide();
        });

        $modal.on('hidden.bs.modal', function () {
            _modalIsOpen = false;
        });

    };

    _run();

    /** END PRIVATE */

    return {
        init: _init,
        isEdit: _isEdit
    }

});