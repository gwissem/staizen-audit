var DynamicTranslationEditor = (function () {


    var $searchForm,
        $mainEditingArea,
        $modalOccurrencesText,
        $modalOccurrences,
        $listContainer = null,
        dynamicTranslationEditAjax = false;


    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */


    function _init() {

        $searchForm = $('#search-form-dynamic-translation');
        $listContainer = $('#dynamic-translations-list .list-news');
        $mainEditingArea = $('#dynamic-translation-edit-add-holder');
        $modalOccurrences = $('#modalOccurrences');
        $modalOccurrencesText = $modalOccurrences.find('.modal-body');
        _handleEvents();
        _loadList();
    }

    function _handleEvents() {


        $searchForm.keypress(function (e) {
            if (e.which == 13) {//Enter key pressed
                _loadList();
            }
        });


    }

    function _loadList() {
        var $searchQuery = $searchForm.val();

        $.ajax({
            url: Routing.generate('management_admin_panel_dynamic_translations_key_list', {searchQuery: $searchQuery}),
            type: 'GET',
            success: function (response) {
                _replaceList(response.html);
            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {

            }
        });

    }

    // function _handleFormSubmit(e) {
    //     e.preventDefault();
    //
    //     var $t = $(this),
    //         $btnSubmit = $t.find('.modal-save-form'),
    //         laddaSubmit = Ladda.create($btnSubmit[0]);
    //
    //     laddaSubmit.start();
    //
    //     $.ajax({
    //         url: $t.attr('action'),
    //         type: 'POST',
    //         data: $t.serialize(),
    //         success: function (response) {
    //             if(response.message) {
    //                 toastr.success("Zapisywanie formularza", response.message);
    //             }
    //
    //             if($t.attr('data-event-success')) {
    //                 var event = new CustomEvent($t.attr('data-event-success'), $t.serializeArray());
    //                 window.document.body.dispatchEvent(event);
    //             }
    //
    //             $t[0].reset();
    //             $t.closest('.modal').modal('hide');
    //
    //         },
    //         error: function (response) {
    //             handleAjaxResponseError(response);
    //         },
    //         complete: function () {
    //             laddaSubmit.stop();
    //         }
    //     });
    //
    // }

    function _bindListEvents() {
        $listContainer.on('click', 'li .dynamic-translation-code', function (event) {
            $('li.mt-list-item').removeClass('active');
           var $clickedItem = $(event.target);
            $clickedItem = $clickedItem.parents('.mt-list-item');
            $clickedItem.addClass('active');
            var key = $clickedItem.data().name;
            _replaceForm(key);

        });
        $('#dynamic-translations-list li').on('click', function (ev) {
            $(ev.target).find('.dynamic-translation-code').click();
        });
        $listContainer.on('click', '#add-new-dynamic-translation', _addNewTranslationKeyFromList);
    }


    function _replaceList(replaceHTML) {

        $listContainer.html(replaceHTML);

        _bindListEvents();


    }


    //***********************************************
    //*
    // *Form Edit functions
    //*
    //***********************************************

    function _replaceForm(key) {
        if (dynamicTranslationEditAjax === false) {
            var $formHtml = _getFormForKey(key);

            $formHtml.done(function (html) {
                $mainEditingArea.html(html);
                _bindEditorEvents()
            })


        }


    }


    function _getFormForKey(key) {
        var $deferred = $.Deferred();
        //    Ajax, form editor main view getting
        dynamicTranslationEditAjax = true;
        $.ajax({
            url: Routing.generate('management_admin_panel_dynamic_translations_single_form', {key: key}),
            method: 'GET',
            success: function (response) {
                return $deferred.resolve(response.html)
            },
            error: function (response) {
                return $deferred.reject('<span style="color:red">' + response + '</span>');

            },
            complete: function () {
                dynamicTranslationEditAjax = false

            }
        });
        return $deferred.promise();

    }

    function _bindEditorEvents() {
        _bindFullKeyActions();
    }

    function _bindFullKeyActions() {


        $('#removeKey').on('click', function (ev) {
            var $key = $(ev.target).parent().data().key;
            swal({
                title: "Czy jesteś pewien?",
                text: "Idąc dalej usuniesz ten tekst wszędzie!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {

                    _deleteFullKey($key).done(function () {
                        $mainEditingArea.html('');
                        toastr.success('Usunięto klucze');
                        _removeKeyFromList($key)
                    })

                }
            });

        });

        $('#changeKeyName').on('click', function () {
            $('#key-name-input').removeProp('disabled');
            $('#changeKeyNameSave').removeClass('hidden');

        });

        $('#findAllKeyOccurrences').on('click', function (ev) {
            var $key = $(ev.target).parent().data().key;
            _findAllKeyOccurrences($key).done(function ($html) {
                $modalOccurrencesText.html($html);
                $modalOccurrences.modal('show');
            });

        });

        $('#addNewKeyTranslation').on('click', function (ev) {
            var $key = $(ev.target).parent().data().key;
            _addNewTranslationKey($key).done(function () {
                _replaceForm($key);
                _loadList();
                toastr.success('Dodano nowy klucz');
            });
        });

        $('#changeKeyNameSave').on('click', function (ev) {
            var $oldName = $(ev.target).parent().data().key;
            var $newKeyName = $('#key-name-input').val();
            _renameKeys($oldName, $newKeyName).done(function () {
                toastr.success('Nazwa została zmieniona');
                $('#key-name-input').attr('disabled', 1);
                $('#changeKeyNameSave').addClass('hidden');
                _loadList();
                _replaceForm($newKeyName);
            })
        });

        $('.translation-key-item-save').on('click', function (ev) {
            var form = $(ev.target).parents('.translation-key-item');
            var $id = form.data().id;
            var $validation = form.find('.translation-query').val();
            var $text = form.find('.translation-text').val();

            _updateTranslationKey($id, $validation, $text).done(function () {
                toastr.success('Tekst zaktualizowany');
            }).fail(function () {
                toastr.error('Wystąpił błąd, spróbuj ponownie później');
            })
        });

        $('.translation-key-item-delete').on('click', function (ev) {
            var form = $(ev.target).parents('.translation-key-item');
            var $key = $(ev.target).parent().data().key;
            var $id = form.data().id;
            swal({
                title: "Czy jesteś pewien?",
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {

                    _deleteSingleKey($id).done(function () {
                        _removeKeyFromForm($id, $key);
                        toastr.success('Tekst został usunięty');
                    })

                }
            });

        })
    }

    function _removeKeyFromForm($id, $key) {

        $('li.translation-key-item[data-id="' + $id + '"').remove();
        _loadList();
    }

    function _deleteSingleKey($id) {
        var $deferred = $.Deferred();
        $.ajax({
            url: Routing.generate('management_admin_panel_dynamic_translations_key_delete', {id: $id}),
            type: 'DELETE',
            success: function (response) {

                $deferred.resolve(response.id)
            },
            error: function (response) {
                $deferred.reject();
                handleAjaxResponseError(response);
            },
            complete: function () {

            }
        });
        return $deferred.promise();
    }


    function _updateTranslationKey($id, $validation, $text) {
        var $deferred = $.Deferred();
        $.ajax({
            url: Routing.generate('management_admin_panel_dynamic_translations_key_save', {
                id: $id,
                validation: $validation,
                text: $text
            }),
            type: 'PUT',
            success: function (response) {
                $deferred.resolve(response.id)
            },
            error: function (response) {
                $deferred.reject();
                handleAjaxResponseError(response);
            },
            complete: function () {

            }
        });
        return $deferred.promise();
    }

    function _addNewTranslationKeyFromList() {
        swal({
            text: 'Podaj nazwę klucza',
            content: "input",
            button: {
                text: "Dodaj",
                closeModal: true
            }
        })
        .then(function (name) {

            if (!name) throw null;

            _addNewTranslationKey(name).done(function () {
                _replaceForm(name);
                _loadList();
                toastr.success('Dodano nowy klucz');
            });

        });
    }


    function _addNewTranslationKey($key) {
        var $deferred = $.Deferred();
        $.ajax({
            url: Routing.generate('management_admin_panel_dynamic_translations_key_add', {key: $key}),
            type: 'POST',
            success: function (response) {
                $deferred.resolve(response.id)
            },
            error: function (response) {
                $deferred.reject();
                handleAjaxResponseError(response);
            },
            complete: function () {

            }
        });
        return $deferred.promise();
    }


    function _removeKeyFromList($key) {
        $('li.mt-list-item[data-name="' + $key + '"').remove();
    }

    function _renameKeys(oldName, newName) {

        var $deferred = $.Deferred();

        $.ajax({
            url: Routing.generate('management_admin_panel_dynamic_translations_keys_rename', {
                oldName: oldName,
                newName: newName
            }),
            type: 'PUT',
            success: function (response) {
                $deferred.resolve(response.key)
            },
            error: function (response) {
                $deferred.reject();
                handleAjaxResponseError(response);
            },
            complete: function () {

            }
        });

        return $deferred.promise();
    }


    function _findAllKeyOccurrences($key) {
        var $deferred = $.Deferred();

        $.ajax({
            url: Routing.generate('management_admin_panel_dynamic_translations_keys_places', {key: $key}),
            type: 'GET',
            success: function (response) {
                $deferred.resolve(response.html)
            },
            error: function (response) {
                $deferred.reject();
                handleAjaxResponseError(response);
            },
            complete: function () {

            }
        });

        return $deferred.promise();
    }

    function _deleteFullKey($key) {
        var $deferred = $.Deferred();
        $.ajax({
            url: Routing.generate('management_admin_panel_dynamic_translations_keys_delete', {key: $key}),
            type: 'DELETE',
            success: function (response) {
                $deferred.resolve(response.key)
            },
            error: function (response) {
                $deferred.reject();
                handleAjaxResponseError(response);
            },
            complete: function () {

            }
        });
        return $deferred.promise();
    }


    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {}

});
