var ManagementTemplate = (function () {

    var _NAME = 'id-content', // telephony-statistics
        $managementWrapper,
        $managementContent,
        $managementTab,
        $managementTabLi;

    var loaded = false,
        intervalContentAjax,
        intervalContentTime = 10000; // 10 sec

    /*

        _init() -> _handleEvents()

        ON_FIRST_ACTIVE_TAB  -> iniLoad() -> _loadPanel() -> ... -> _afterLoadPanel() -> _handleEventsAfterLoadPanel()

     */

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $managementWrapper = $('#' + _NAME + '-wrapper');
        $managementContent = $managementWrapper.find('.tab-wrapper-content');
        $managementTab = $('a[href="#' + _NAME + '"]');
        $managementTabLi = $managementTab.parent();

        _handleEvents();

    }

    var _delayCallback = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function _handleEvents() {

        $managementTab.on('shown.bs.tab', function() {
            iniLoad();
        });

        if($managementTabLi.hasClass('active')) {
            iniLoad();
        }

    }

    function iniLoad() {
        if(!loaded) {
            _loadPanel();
        }
    }

    function _loadPanel() {

        // $.ajax({
        //     url: Routing.generate('management_supervisor_user_index'),
        //     type: 'GET',
        //     beforeSend: function () {
        //         $managementWrapper.addClass('loading');
        //     },
        //     success: function (response) {
        //
        //         loaded = true;
        //         $managementContent.html(response.html);
        //
        //         _afterLoadPanel();
        //
        //     },
        //     error: function (response) {
        //         handleAjaxResponseError(response);
        //     },
        //     complete: function () {
        //         $managementWrapper.removeClass('loading');
        //     }
        // });

    }

    function _afterLoadPanel() {

        _handleEventsAfterLoadPanel();

    }

    /**
     * Tutaj jakieś eventy, po załadowaniu panelu
     */
    function _handleEventsAfterLoadPanel() {

        /** Przykładowy interval odświeżania */
        intervalContentAjax = setInterval(function () {

            // refreshFunction();

        }, intervalContentTime);

    }

    function _tabIsActive() {
        return ($managementTabLi.hasClass('active'));
    }


    $(function () {
        _init();
    });

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {}

});
