var ModalFormModule = (function () {


    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */


    function _init() {

        _handleEvents();
    }

    function _handleEvents() {

        $('.ajax-form-modal')
            .on('submit', 'form', _handleFormSubmit)
            .on('click', '.modal-save-form', function () {
                $(this).closest('form').trigger('submit');
            });

    }

    function _handleFormSubmit(e) {
        e.preventDefault();

        var $t = $(this),
            $btnSubmit = $t.find('.modal-save-form'),
            laddaSubmit = Ladda.create($btnSubmit[0]);

        laddaSubmit.start();

        $.ajax({
            url: $t.attr('action'),
            type: 'POST',
            data: $t.serialize(),
            success: function (response) {
                if(response.message) {
                    toastr.success("Zapisywanie formularza", response.message);
                }

                if($t.attr('data-event-success')) {
                    var event = new CustomEvent($t.attr('data-event-success'), $t.serializeArray());
                    window.document.body.dispatchEvent(event);
                }

                $t[0].reset();
                $t.closest('.modal').modal('hide');

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                laddaSubmit.stop();
            }
        });

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {

    }

});
