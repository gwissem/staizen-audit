var ManagementBusinessRole = (function () {

    var $businessRoleWrapper,
        $businessRoleContent,
        $businessRoleTable,
        $managementUserRoleTaskActionsForm;

    var loaded = false;

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _init() {

        $businessRoleWrapper = $('#business-role-wrapper');
        $businessRoleContent = $businessRoleWrapper.find('.business-role-content');

        _handleEvents();

    }

    function _handleEvents() {

        $('a[href="#atlas_business"]').on('click', function (e) {
            e.preventDefault();
            if(!loaded) {
                funResizeHandle.addFunction('_calculateTableHeight', _calculateTableHeight);
                _loadPanel();
            }
            else {
                _loadTable();
            }

        });

    }

    function _loadPanel() {

        $.ajax({
            url: Routing.generate('management_supervisor_business_user_role_index'),
            type: 'GET',
            beforeSend: function () {
                $businessRoleWrapper.addClass('loading');
            },
            success: function (response) {

                loaded = true;
                $businessRoleContent.html(response.html);
                $businessRoleTable = $("#management-user-role-task-table");
                $managementUserRoleTaskActionsForm = $('#management-user-role-task-actions form');

                _afterLoadPanel();

            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $businessRoleWrapper.removeClass('loading');

            }
        });

    }

    function _afterLoadPanel() {

        $(".select2").select2({}).on("change", function () {
            $('.user-role-task-table .box-spinner').show();
            _loadTable();
        });

        $('.add-new-role-form').on('submit', function (e) {

            e.preventDefault();

            var $t = $(this),
                $btn = $t.find('button');

            $.ajax({
                url: Routing.generate('management_supervisor_add_user_role_group'),
                type: 'POST',
                data: $t.serialize(),
                beforeSend: function () {
                    $btn.prop('disabled', true);
                },
                success: function (response) {
                    if(response.success) {
                        toastr.success('Dodano rolę ' + $('#user-role-group-name').val());
                        $t[0].reset();
                        _loadTable();
                    }
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    $btn.prop('disabled', false);
                }
            });

        });

        setTimeout(_calculateTableHeight,0);

        _loadTable();

    }

    function _calculateTableHeight() {

        if(!$businessRoleTable) return false;

        var height = window.innerHeight - (
            $('#management-user-role-task-actions').outerHeight(true)
            + getPaddingAndMarginHeight($('.management-tab-content'))
            + $('.management-nav-tabs').outerHeight(true)
            + getPaddingAndMarginHeight($('.page-container'))
            + 60
        );

        $businessRoleTable.css('max-height', height);

    }

    function _loadTable() {

        var roleIds = $('#user-role-group').val();
        var platformGroups = $('#platform-groups').val();

        $.ajax({
            url: Routing.generate('management_supervisor_business_user_role_table'),
            type: 'GET',
            data: {
                filters: {
                    'roleIds': roleIds,
                    'platformGroupsIds': platformGroups
                }
            },
            beforeSend: function () {
                $managementUserRoleTaskActionsForm.addClass('loading');
            },
            success: function (response) {
                $businessRoleTable.html(response.html);

                _afterLoadTable();
            },
            error: function (response) {
                handleAjaxResponseError(response);
            },
            complete: function () {
                $managementUserRoleTaskActionsForm.removeClass('loading');
                // $businessRoleWrapper.removeClass('loading');
                $('.select2').select2();
            }
        });

    }

    function _afterLoadTable() {

        _collapseTable();
        _bindTableEvent();

    }

    function _collapseTable() {

        $('.user-role-task-table th[data-platform-group-id]').each(function (i, ele) {
             var $t = $(ele),
                 platformGroupId = $t.attr('data-platform-group-id');
             if(localStorage.getItem('group_platform_collapse_' + platformGroupId) === "0") {
                 $t.addClass('is-compress').attr('colspan', 1);
                 $('.user-role-task-table tbody [data-platform-group-id="'+platformGroupId+'"]').addClass('is-compress');
             }
        });

    }

    function _bindTableEvent() {

        var $userRoleTaskTable = $('.user-role-task-table'),
            $boxSpinner = $userRoleTaskTable.find('.box-spinner');

        $userRoleTaskTable.find('thead .collapse-group button').on('click', function () {

            var $t = $(this),
                $closestRoleTh = $t.closest('th[data-platform-group-id]'),
                platformGroupId = $closestRoleTh.attr('data-platform-group-id');

            if($t.hasClass('btn-compress')) {
                localStorage.setItem('group_platform_collapse_' + platformGroupId, "0");
                $closestRoleTh.addClass('is-compress').attr('colspan', 1);
                $('.user-role-task-table tbody [data-platform-group-id="'+platformGroupId+'"]').addClass('is-compress');
            }
            else {
                localStorage.setItem('group_platform_collapse_' + platformGroupId, "1");
                $closestRoleTh.removeClass('is-compress').attr('colspan', $closestRoleTh.attr('data-colspan'));
                $('.user-role-task-table tbody [data-platform-group-id="'+platformGroupId+'"]').removeClass('is-compress');
            }

        });

        $userRoleTaskTable.find('tbody td.role-table-td input[type="checkbox"]').on('change', function (e) {

            var $t = $(this);

            $.ajax({
                url: Routing.generate('management_supervisor_toggle_task_role'),
                type: 'POST',
                data: {
                    'group-platform-id': $t.closest('.role-table-td').attr('data-platform-group-id'),
                    'role-id': $t.closest('tr[data-role-id]').attr('data-role-id'),
                    'checked': $t.is(':checked'),
                    'group-id': $t.val()
                },
                success: function (response) {
                    if(response.success) {
                        _calculateCheckedGroups(response.roleId, response.groupPlatformId);
                    }
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                }
            });

        });

        $userRoleTaskTable.find('.check-all-groups').on('change', function () {

            var $t = $(this);

            $.ajax({
                url: Routing.generate('management_supervisor_toggle_group_platform_role'),
                type: 'POST',
                data: {
                    'group-platform-id': $t.closest('td[data-platform-group-id]').attr('data-platform-group-id'),
                    'role-id': $t.closest('tr[data-role-id]').attr('data-role-id'),
                    'checked': $t.is(':checked')
                },
                beforeSend: function () {
                    $t.prop('readonly', true);
                    $boxSpinner.show();
                },
                success: function (response) {

                    if(response.success) {
                        if($t.is(':checked')) {
                            $t.closest('tr').find('.role-table-td[data-platform-group-id="'+response.groupPlatformId+'"] [type="checkbox"]').prop('checked', true);
                        }
                        else {
                            $t.closest('tr').find('.role-table-td[data-platform-group-id="'+response.groupPlatformId+'"] [type="checkbox"]').prop('checked', false);
                        }
                        _calculateCheckedGroups(response.roleId, response.groupPlatformId);
                    }
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                },
                complete: function () {
                    $t.prop('readonly', false);
                    if($boxSpinner) {
                        $boxSpinner.hide();
                    }
                }
            });

        });

        $('.user-role-task-table .remove-role').confirmation({
            btnOkClass: 'btn-success',
            btnOkIcon: 'icon-check',
            btnOkLabel: 'Tak',
            btnCancelClass: 'btn-danger',
            btnCancelIcon: 'icon-close',
            btnCancelLabel: 'Nie',
            title: 'Czy na pewno usunąć?',
            onConfirm: function () {

                var $btn = $(this),
                    userRoleGroupId = $btn.attr('data-user-role-group-id');

                $.ajax({
                    url: Routing.generate('management_supervisor_remove_user_role_group'),
                    type: 'POST',
                    data: {
                        userRoleGroupId: userRoleGroupId
                    },
                    success: function (response) {
                        if(response.success) {
                            toastr.success('Pomyślnie usunięto.');
                            $btn.closest('tr').remove();
                        }
                    },
                    error: function (response) {
                        handleAjaxResponseError(response);
                    }
                });

            }
        });

        $('.user-role-task-table .task-group').change(function(){

            var $select = $(this),
                userRoleGroupId = $select.attr('data-user-role-group-id');

            $.ajax({
                url: Routing.generate('management_supervisor_change_filtered_emails'),
                type: 'POST',
                data: {
                    userRoleGroupId: userRoleGroupId,
                    emails: $select.val()
                },
                success: function (response) {
                    if(response.success) {
                        toastr.success('Pomyślnie zapisano.');
                    }
                },
                error: function (response) {
                    handleAjaxResponseError(response);
                }
            });
        });

        $userRoleTaskTable.find('.copy-role').on('click',function (e) {

            e.preventDefault();

            var $t = $(this),
                $tr = $t.closest('tr'),
                userRoleGroupId = $tr.attr('data-role-id');

            $.ajax({
                url: Routing.generate('management_supervisor_copy_user_role_group'),
                type: 'POST',
                data: {
                    userRoleGroupId: userRoleGroupId
                },
                beforeSend: function () {
                    $boxSpinner.show();
                },
                success: function () {
                    _loadTable();
                },
                error: function (response) {
                    if($boxSpinner) {
                        $boxSpinner.hide();
                    }
                    handleAjaxResponseError(response);
                }
            });
        });

        $('.dynamic-editable').editable({
            placement: 'right'
        });

    }

    function _calculateCheckedGroups(roleId, groupPlatformId) {

        var $tr = $('[data-role-id="'+roleId+'"]'),
            $tds = $tr.find('.role-table-td[data-platform-group-id="'+groupPlatformId+'"]'),
            amount = 0;

        $.each($tds, function (i, ele) {
            if($(ele).find('input[type="checkbox"]').is(':checked')) {
                amount++;
            }
        });

        $tr.find('[data-platform-group-id="'+groupPlatformId+'"] .checked-groups').text(amount + " / " + $tds.length);

    }

    _init();

    /*
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    return {}

});
