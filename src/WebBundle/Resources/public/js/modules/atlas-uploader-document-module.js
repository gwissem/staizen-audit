/**
 * @class AtlasUploaderDocumentModule
 */
var AtlasUploaderDocumentModule = (function(opts) {

    var $body,
        $wrapper,
        $tablePresentationBody,
        instanceFileupload,
        url,
        fileList = [],
        isFormUpload = false,
        $inputFormUpload;

    var fileTemplate = '' +
        '<tr class="template-download fade in">' +
            '<td class="name">' +
                '<span>__NAME__</span>' +
            '</td>__EXTRA_CONTENT__' +
            '<td class="delete" align="right">' +
                '<button class="btn btn-danger btn-sm remove-file not-absolute" data-type="" data-url="">' +
                    '<i class="fa fa-times"></i>' +
                '</button>' +
            '</td>' +
        '</tr>';

    var progressTemplate = '' +
        '<td class="text-center size"> ' +
            '__SIZE__' +
        '</td>' +
        '<td class="progress-td">' +
            '<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">' +
                '<div class="progress-bar progress-bar-info" data-uid="__UID__" style="width:0%;"></div>' +
            '</div>' +
        '</td>';

    var errorTemplate = '' +
        '<td class="error">' +
            '<span class="label label-danger">Błąd</span>__ERROR__' +
        '</td>';

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _initVariables() {
        $body = $('body');
        $wrapper = opts.$wrapper;
        $tablePresentationBody = $wrapper.find('table[role="presentation"] tbody');
        url = $wrapper.attr('data-url');

        if($wrapper.find('input[data-attribute-path]').length) {
            isFormUpload = true;
            $inputFormUpload = $wrapper.find('input[data-attribute-path]');
        }
    }

    function appendFileToView(file) {

        var uid = guid();
        var html = fileTemplate.replace('__EXTRA_CONTENT__', progressTemplate),
            name = '',
            fileUrl = '';

        if(isFormUpload && file.uuid) {
            fileUrl = Routing.generate('download_file', {uuid: file.uuid});
            name = '<a href="' + fileUrl + '" target="_blank">' + file.name + '</a>';
        }
        else {
            name = file.name;
        }

        html = html.replace('__NAME__', name)
            .replace('__SIZE__', humanFileSize(file.size, true))
            .replace('__UID__', uid);

        $tablePresentationBody.append(html);

        return $tablePresentationBody.find('[data-uid="'+uid+'"]');
    }

    function updateRemoveResult(id) {

        var value = opts.$result.val(),
            arrayValue = value.split(',');

        var index = arrayValue.indexOf(id.toString());
        if (index > -1) {
            arrayValue.splice(index, 1);
        }
        opts.$result.val(arrayValue.join(','));

    }

    function updateAddResult(id) {

        var value = opts.$result.val(),
            arrayValue = [];
        if(value) {
            arrayValue = value.split(',');
        }
        arrayValue.push(id);

        opts.$result.val(arrayValue.join(','));

    }

    function _initFileUploader() {

        var maxFileSize = (opts.params.maxFileSize) ? opts.params.maxFileSize : 2000000, // 2MB
            autoUploadFile = (typeof opts.params.autoUpload !== "undefined") ? opts.params.autoUpload : true;

        instanceFileupload = $wrapper.find('input[type="file"]').fileupload({
            dataType: 'json',
            replaceFileInput: false,
            autoUpload: autoUploadFile,
            // progressInterval: 15,
            url: url,
            dropZone: $wrapper,
            maxFileSize: (opts.params.maxFileSize) ? opts.params.maxFileSize : 2000000,
            // maxFileSize: 250,
            add: function (e, data) {

                var uploadErrors = [];
                data.customContext = {};

                if(data.originalFiles.length && data.originalFiles[0]['size'] > maxFileSize) {
                    uploadErrors.push(' Plik za duży');
                }

                if(uploadErrors.length > 0) {
                    data.files.forEach(function (ele, i) {
                        data.customContext.$progress = appendFileToView(ele);
                        data.customContext.$progress.closest('.template-download').addClass('fail');
                        var error = errorTemplate.replace('__ERROR__', uploadErrors[0]);
                        data.customContext.$progress.closest('.progress-td').empty().html(error);
                        // humanFileSize
                    });
                } else {

                    data.files.forEach(function (ele, i) {
                        data.customContext.$progress = appendFileToView(ele);
                    });

                    if(autoUploadFile) {
                        data.submit();
                    }
                }

            },
            start: function (e) {
                "use strict";

            },
            fail: function (e, data) {

                var error = errorTemplate.replace('__ERROR__', 'Błąd serwera.');

                data.customContext.$progress.closest('.template-download').addClass('fail');
                data.customContext.$progress.closest('.progress-td').empty().html(error);

            },
            done: function (e, data) {

                data.customContext.$progress.closest('.template-download').addClass('done')
                    .attr('data-file-id', data.result.fileId).attr('data-file-uid', data.result.fileUuid);

                if(isFormUpload) {
                    if(!$inputFormUpload.val() && data.result.documentId) {

                        $inputFormUpload.val(data.result.documentId).trigger('change');
                        var newUrl = $wrapper.attr('data-url').replace('?document-id=&', '?document-id='+data.result.documentId+'&');
                        $wrapper.attr('data-url', newUrl);

                        $wrapper.find('input[type="file"]').fileupload(
                            'option',
                            'url',
                            newUrl
                        );

                    }

                    if(data.result.fileUuid) {

                        var fileUrl = Routing.generate('download_file', {uuid: data.result.fileUuid}),
                            nameSpan = data.customContext.$progress.closest('.template-download').find('.name span'),
                            _name = nameSpan.text(),
                            aHref = '<a href="' + fileUrl + '" target="_blank">' + _name + '</a>';

                        nameSpan.html(aHref);
                    }

                    $wrapper.find('.presentation-col').animate({scrollTop: $wrapper.find('.presentation-col')[0].scrollHeight}, 1000);

                }
                else {
                    updateAddResult(data.result.fileUuid);

                    if(typeof opts.afterUpload === "function") {
                        opts.afterUpload();
                    }
                }

            }

        }).bind('fileuploadprogress', function (e, data) {

            var progress =  parseInt(data.loaded / data.total * 100, 10) + '%';
            data.customContext.$progress.css(
                'width',
                progress
            ).text(progress);

        });

        var files = $wrapper.attr('data-files');

        if(files) {
            files = JSON.parse(files);

            var progress;

            files.forEach(function (ele, i) {
                progress = appendFileToView(ele);
                progress.closest('.template-download').addClass('done').attr('data-file-id', ele.id).attr('data-file-uid', ele.uuid).find('.progress').remove();
            });

        }

        /** Update documentId w atrybucie */

        if(isFormUpload) {

            if($body.hasClass('print-mode')) return false;

            if(!$inputFormUpload.val()) {

                var groupProcessId = $inputFormUpload.attr('data-group-process-id');

                if(!groupProcessId) return false;

                $.ajax({
                    url: Routing.generate('ajax_widget_add_document', {groupProcessId: groupProcessId}),
                    type: "POST",
                    success: function (response) {
                        if(response.documentId) {

                            $inputFormUpload.val(response.documentId).trigger('change');
                            var newUrl = $wrapper.attr('data-url').replace('?document-id=&', '?document-id='+response.documentId+'&');
                            $wrapper.attr('data-url', newUrl);

                            $wrapper.find('input[type="file"]').fileupload(
                                'option',
                                'url',
                                newUrl
                            );
                        }
                    }
                });
            }
        }

    }

    function eventsHandle() {

        // funResizeHandle.addFunction('recalculateHeightInboxEditor', recalculateHeight);

        $tablePresentationBody.on('click', '.remove-file', function (e) {
            e.preventDefault();

            var $row = $(this).closest('.template-download'),
                uid = $row.attr('data-file-uid'),
                id = $row.attr('data-file-id');

            if(isFormUpload) {

                var documentId = $inputFormUpload.val();

                $.ajax({
                    url: Routing.generate('ajax_widget_remove_file', {fileUId: uid, documentId: documentId}),
                    type: "POST",
                    success: function () {
                        $row.remove();
                    },
                    error: function () {
                        toastr.error('Wystąpił błąd przy usuwaniu pliku.', 'Błąd usuwania.');
                    }
                });

            }
            else {
                updateRemoveResult(id);
                $row.remove();
            }

        })

    }

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function init() {

        _initVariables();
        eventsHandle();
        _initFileUploader();
    }

    init();

    return {}

});
