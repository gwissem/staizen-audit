(function ($, window, document) {

    var instances = [],
        $linkedBox = null,
        $currentLinked = null,
        $classedBox = null,
        $currentClassed = null,
        $renameRowBox = null,
        $currentRename = null;

    var methods = {
        init: function (options) {

            if(instances.indexOf(this.selector) !== -1) return this;
            instances.push(this.selector);

            var builderModal = this;
            var $window = $(window);

            var settings = $.extend({
                availableFields: '#available-fields',
                editableFields: '#editable-fields',
                gridX: 12,
                minGrid: 2,
                linkedArr: [],
                linkedHtml: '<div id="linked-input-box" class="linked-box"><div class="linked-wrapper">' +
                '<i class="fa fa-eye" aria-hidden="true"></i> : ' +
                '<select class="linked-select"></select> == <input class="form-control" type="text" /> ' +
                '<button class="apply-linked btn green-meadow"> <i class="fa fa-check" aria-hidden="true"></i> </button> ' +
                '<button class="remove-linked btn red"> <i class="fa fa-trash-o" aria-hidden="true"></i> </button>' +
                '</div></div>',
                $linkedBox : null,
                classedHtml: '<div id="classed-input-box" class="linked-box"><div class="linked-wrapper">' +
                '<i class="fa fa-paint-brush" aria-hidden="true"></i> : ' +
                '<input class="form-control" type="text" />' +
                '<button class="apply-classed btn green-meadow"><i class="fa fa-check" aria-hidden="true"></i></button> ' +
                '</div></div>',
                $classedBox : null,
                renameRowHtml: '<div id="rename-input-box" class="linked-box"><div class="linked-wrapper">' +
                '<i class="fa fa-tag" aria-hidden="true"></i> : ' +
                '<input class="form-control" name="slug" placeholder="tag" type="text" /> ' +
                '<i class="fa fa-font" aria-hidden="true"></i> : ' +
                '<input class="form-control" name="name" placeholder="name" type="text" /> ' +
                '<button class="apply-rename btn green-meadow"> <i class="fa fa-check" aria-hidden="true"></i> </button> ' +
                '<button class="remove-rename btn red"> <i class="fa fa-trash-o" aria-hidden="true"></i> </button>' +
                '</div></div>',
                $renameRowBox : null
            }, options);

            return this.each(function () {

                var that = this,
                    $that = $(that);

                var vars = {
                    $availableF: null,
                    $editableF: null,
                    fullWidth: 0,
                    perGrid: 0,
                    availableSort: null,
                    // selectedInput: null,
                    $toolEditBuilderModal: $('#tool-edit-builder-modal')
                };

                initLinkedBox(settings, that);
                initClassedBox(settings, that);
                setVars.call(this, settings, vars);

                vars.availableSort = new Sortable(vars.$availableF.find('.row')[0], getSortableOptions({
                    sort: false,
                    onAdd: function (/**Event*/evt) {
                        $(evt.item).resizable("destroy");
                    }
                }));

                $.each(vars.$editableF.find('.row'), function () {
                    new Sortable(this, getSortableOptions({
                        onSort: function (/**Event*/evt) {
                            setResizableField($(evt.item), vars, settings);
                        }
                    }));
                });

                $that.find('.change-view').on('click', function (e) {
                   e.preventDefault();

                    var $t = $(this),
                        height;
                    $that.toggleClass('advanced-view');

                    modalFixView($that);

                    // height = $(window).height() - 60;
                    //
                    // $that.find('.modal-content, .modal-header').css({
                    //     'max-height' : height
                    // });

                });

                $that.find('.add-row').on('click', function () {

                    var newRow = $($(settings.editableFields).data('prototype-row'));
                    vars.$editableF.append(newRow);
                    new Sortable(newRow[0], getSortableOptions({
                        onSort: function (/**Event*/evt) {
                            setResizableField($(evt.item), vars, settings);
                        }
                    }));
                });

                vars.$editableF.on('click', '.row.dropzone .remove-row', function (e) {
                    e.preventDefault();

                    var row = $(this).parent();
                    $.each(row.find('.draggable-field'), function (i, ele) {
                        $(this).resizable("destroy");
                        vars.$availableF.find('.row').append(this);
                    });
                    row.remove();
                });


                vars.$editableF.on('click', '.control-order a', function (e) {
                    e.preventDefault();

                    var $t = $(this),
                        row, secondRow;

                    if($t.hasClass('up'))
                    {
                        row = $t.closest('.row');
                        secondRow = row.prev();

                        $({deg: 0}).animate({
                                deg: - (secondRow.outerHeight(true) - 10)
                            },
                            {
                            step: function(now) {
                                row.css({
                                    transform: 'translate(0,' + now + 'px)'
                                });
                            },
                            duration: 300
                        });

                        $({deg: 0}).animate({
                                deg: row.outerHeight(true) - 10
                            },
                            {
                                step: function(now) {
                                    secondRow.css({
                                        transform: 'translate(0,' + now + 'px)'
                                    });
                                },
                                duration: 300,
                                complete: function () {
                                    row.insertBefore(secondRow);
                                    row.add(secondRow).css({
                                        transform: 'translate(0, 0)'
                                    });
                                }
                            });

                    }
                    else if($t.hasClass('down'))
                    {
                        row = $t.closest('.row');
                        secondRow = row.next();

                        $({deg: 0}).animate({
                                deg: secondRow.outerHeight(true) - 10
                            },
                            {
                                step: function(now) {
                                    row.css({
                                        transform: 'translate(0,' + now + 'px)'
                                    });
                                },
                                duration: 300
                            });

                        $({deg: 0}).animate({
                                deg: - (row.outerHeight(true) - 10)
                            },
                            {
                                step: function(now) {
                                    secondRow.css({
                                        transform: 'translate(0,' + now + 'px)'
                                    });
                                },
                                duration: 300,
                                complete: function () {
                                    row.insertAfter(secondRow);
                                    row.add(secondRow).css({
                                        transform: 'translate(0, 0)'
                                    });
                                }
                            });

                    }
                    else if($t.hasClass('add-row-under'))
                    {
                        var newRow = $($(settings.editableFields).data('prototype-row'));
                        $(this).closest('.row').after(newRow);

                        new Sortable(newRow[0], getSortableOptions({
                            onSort: function (/**Event*/evt) {
                                setResizableField($(evt.item), vars, settings);
                            }
                        }));
                    }

                });


                $that.find('.linked-icon').on('click', function (e) {
                    e.preventDefault();

                    var $t = $(this);

                    $currentLinked = $t;

                    updateSelect($t.data('name'), settings.linkedArr, $t.attr('data-target-name'));
                    updateInput($t.attr('data-target-value'));

                    settings.$linkedBox.css({
                        'top' : $t.offset().top - 30 - $window.scrollTop() + vars.$toolEditBuilderModal.scrollTop(),
                        'left' : $t.offset().left - 15
                    }).fadeIn(300);

                    setTimeout(function (e) {
                        bindClose('closeLinked');
                    }, 10)
                });

                $that.find('.class-icon').on('click', function (e) {
                    e.preventDefault();

                    var $t = $(this);

                    $currentClassed = $t;

                    updateClassedInput($t.attr('data-class-value'));

                    settings.$classedBox.css({
                        'top' : $t.offset().top - 20 - $window.scrollTop() + vars.$toolEditBuilderModal.scrollTop(),
                        'left' : $t.offset().left - 30
                    }).fadeIn(300);

                    setTimeout(function (e) {
                        bindClose('closeClassed');
                    }, 10)

                });

                $that.on('click', '.rename-row', function (e) {
                    e.preventDefault();

                    var $t = $(this),
                        $row = $t.closest('.row');
                    $currentRename = $row.find('.name-row');

                    updateInputs($currentRename);

                    settings.$renameRowBox.css({
                        'top' : $row.offset().top + 10 - $window.scrollTop() + vars.$toolEditBuilderModal.scrollTop(),
                        'left' : $row.offset().left + 10
                    }).fadeIn(500);

                    setTimeout(function (e) {
                        bindClose('closeRename');
                    }, 10)
                });

                $that.find('.save-form-builder').on('click', function (e) {
                    e.preventDefault();
                    builderModal.atlasFormBuilder('save');
                });

                $.each(vars.$editableF.find('.resizable-field'), function () {
                    setResizableField($(this), vars, settings);
                });

                vars.$editableF.find('.resizable-field').on('mouseenter', function (e) {
                    var $t = $(this),
                        target = $t.find('.linked-icon').attr('data-target-name');

                    if(target)
                    {
                        $('.resizable-field[data-col-name="' + target + '"]').addClass('focus-link');
                    }

                }).on('mouseleave', function (e) {
                    var $t = $(this),
                        target = $t.find('.linked-icon').attr('data-target-name');

                    if(target)
                    {
                        $('.resizable-field[data-col-name="' + target + '"]').removeClass('focus-link');
                    }
                });

                eventResize($that);
            });

        },
        save: function () {

            var $this = $(this),
                rows, elements, fields = [], nameRow,
                l = Ladda.create($this.find(".save-form-builder")[0]);

            l.start();

            rows = $this.find('#editable-fields .row');

            $.each(rows, function (j, ele) {

                elements = $(this).find('.draggable-field:not(.fake-item)');
                nameRow = $(this).find('.name-row');

                $.each(elements, function (i, ele) {
                    var linked = $(ele).find('.linked-icon');
                    var classed = $(ele).find('.class-icon');

                    fields.push({
                        id: ele.getAttribute('data-id'),
                        order: i + 1,
                        col: ele.getAttribute('data-col'),
                        row: j + 1,
                        customClass: (classed.attr('data-class-value')) ? classed.attr('data-class-value') : '',
                        targetName: (linked.attr('data-target-name')) ? linked.attr('data-target-name') : null,
                        targetValue: (linked.attr('data-target-value')) ? linked.attr('data-target-value') : null,
                        sectionRow: (nameRow.attr('data-name') == 'noname') ? '' : nameRow.attr('data-name-slug') + '||' + nameRow.attr('data-name')
                    })
                });
            });

            updateFormBuilder(fields, $this.data('tool-id')).done(function(data) {

            }).fail(function (data) {
                console.error('Błąd przy aktualizacji formularza. Msg: ' +data.statusText);
            }).always(function () {
                l.stop();
            });
        }
    };

    function modalFixView($that) {

        if($that.hasClass("advanced-view"))
        {
            var height = $(window).height() - 60;
            $that.find('.modal-content, .modal-header').css({
                'max-height' : height
            });
        }

    }

    function eventResize($that) {
        $(window).on('resize', function (e) {
            modalFixView($that);
        });
    }

    function updateInputs($nameRow) {
        $renameRowBox.find('input[name="slug"]').focus();
        if($nameRow.attr('data-name') != 'noname')
        {
            $renameRowBox.find('input[name="slug"]').val($nameRow.attr('data-name-slug'));
            $renameRowBox.find('input[name="name"]').val($nameRow.attr('data-name'));
        }
        else {
            $renameRowBox.find('input[name="slug"]').val('');
            $renameRowBox.find('input[name="name"]').val('');
        }
    }

    function updateSelect(activeLinkedName, linkedArr, selected) {

        var newOptions = {};

        $.each(linkedArr, function (i, ele) {
            if(ele['name'] != activeLinkedName){
                newOptions[ele['label']] = ele['name'];
            }
        });

        var $el = $linkedBox.find('select');

        $el.empty(); // remove old options
        $.each(newOptions, function(key,value) {
            $el.append($("<option></option>")
                .attr("value", value).text(key));
        });

        $el.val('');

        if(selected)
        {
            $el.val(selected);
        }

        $el.change();
    }

    function updateInput(val){
        if(val)
        {
            $linkedBox.find('input').val(val);
        }
        else $linkedBox.find('input').val('');
    }

    function updateClassedInput(val) {
        if(val)
        {
            $classedBox.find('input').val(val);
        }
        else $classedBox.find('input').val('');
    }


    this.closeClassed = function (e) {
        if($(e.target).closest($classedBox).size() <= 0
            && $(e.target).closest('.class-icon').size() <= 0 )
        {
            $classedBox.fadeOut(300);
            unbindClose('closeClassed');
        }
    };

    this.closeLinked = function (e) {
        if($(e.target).closest($linkedBox).size() <= 0
            && $(e.target).closest('.linked-icon').size() <= 0 )
        {
            $linkedBox.fadeOut(300);
            unbindClose('closeLinked');
        }
    };

    this.closeRename = function (e) {
        if($(e.target).closest($renameRowBox).size() <= 0
            && $(e.target).closest('.rename-row').size() <= 0 )
        {
            $renameRowBox.fadeOut(300);
            unbindClose('closeRename');
        }
    };

    function bindClose(method) {
        $(window).on('click', this[method]);
    }

    function unbindClose(method) {
        $(window).off('click', this[method]);
    }

    function initClassedBox(settings, box) {

        $(box).parent().append(settings.classedHtml);
        settings.$classedBox = $('#classed-input-box');
        $classedBox = settings.$classedBox;

        var input = $classedBox.find('input');

        $classedBox.on('click', '.apply-classed', function (e) {
            e.preventDefault();

            $currentClassed.attr('data-class-value', input.val());
            $classedBox.fadeOut(300);
            $currentClassed = null;

        });

    }

    function initLinkedBox(settings, box) {
        if(settings.linkedArr){
            $(box).parent().append(settings.linkedHtml);
            settings.$linkedBox = $('#linked-input-box');
            $linkedBox = settings.$linkedBox;

            var select = $linkedBox.find('select').select2({
                minimumResultsForSearch: -1
            });
            var input = $linkedBox.find('input');

            $linkedBox.on('click', '.apply-linked', function (e) {
                e.preventDefault();

                $currentLinked.attr('data-target-name', select.val());
                $currentLinked.attr('data-target-value', input.val());
                $currentLinked.addClass('has-link');

                $linkedBox.fadeOut(300);
                $currentLinked = null;
            }).on('click', '.remove-linked', function (e) {
                e.preventDefault();

                $currentLinked.attr('data-target-name', '');
                $currentLinked.attr('data-target-value', '');
                $currentLinked.removeClass('has-link');

                $linkedBox.fadeOut(300);
                $currentLinked = null;
            })
        }

        if(settings.renameRowHtml)
        {
            $(box).parent().append(settings.renameRowHtml);
            settings.$renameRowBox = $('#rename-input-box');

            $renameRowBox = settings.$renameRowBox;

            var slug = $renameRowBox.find('input[name="slug"]'),
                name = $renameRowBox.find('input[name="name"]');

            $renameRowBox.on('click', '.apply-rename', function (e) {
                e.preventDefault();

                $currentRename.attr('data-name', name.val());
                $currentRename.attr('data-name-slug', slug.val());
                $currentRename.find('span').text(name.val());

                $renameRowBox.fadeOut(300);
                slug.val('');
                name.val('');
                $currentRename = null;

            }).on('click', '.remove-rename', function (e) {
                e.preventDefault();

                $currentRename.attr('data-name', 'noname');
                $currentRename.attr('data-name-slug', '');
                $currentRename.find('span').text('');

                $renameRowBox.fadeOut(300);
                slug.val('');
                name.val('');
                $currentRename = null;
            })
        }
    }

    function updateFormBuilder(formBuilderData, toolId) {
        return $.ajax({
            url: Routing.generate('admin_tool_form_builder_update', {toolId : toolId}),
            type: "POST",
            data: {form_builder: formBuilderData}
        });
    }

    function getSortableOptions(options) {
        return $.extend({
            group: 'fields',
            draggable: '.draggable-field',
            handle: "label",
            animation: 300,
            filter: '.ignore',
            sort: true
        }, options);
    }

    function setResizableField($t, vars, settings) {
        var direction = null,
            startWidth = 0;

        $t.resizable({
            maxHeight: 50,
            minHeight: 50,
            minWidth: vars.perGrid * settings.minGrid,
            maxWidth: vars.fullWidth,
            handles: "e",
            // grid: [ vars.perGrid, 0 ],
            start: function (event, ui) {
                ui.element.addClass("resizable-active");
                startWidth = ui.size.width + 30;
            },
            stop: function (event, ui) {
                ui.element.removeClass("resizable-active");
            },
            resize: function (event, ui) {

                direction = $(this).data('ui-resizable').axis;
                var indexGrid;

                if (direction == 'w') {
                    // indexGrid = Math.ceil(ui.size.left / vars.perGrid);
                    // if (indexGrid > 12) indexGrid = 12;
                    // ui.position.left = vars.fullWidth * (indexGrid / settings.gridX);
                    // console.log(ui.position.left);
                    // ui.position.left = 0;
                    // ui.size.width = startWidth;
                }
                else if (direction == 'e') {
                    indexGrid = Math.ceil(ui.size.width / vars.perGrid);
                    if (indexGrid > 12) indexGrid = 12;
                    ui.element.attr('data-col', indexGrid);
                    ui.size.width = vars.fullWidth * (indexGrid / settings.gridX);
                }
            }
        });
    }

    function setVars(settings, vars) {
        vars.$availableF = $(this).find(settings.availableFields);
        vars.$editableF = $(this).find(settings.editableFields);
        vars.fullWidth = vars.$editableF.width() + 30 - parseInt(vars.$editableF.find('.row:first-child').css('border-width')) * 2;
        vars.perGrid = parseInt(vars.fullWidth / settings.gridX);
    }

    $.fn.atlasFormBuilder = function (methodOrOptions) {

        if (methods[methodOrOptions]) {
            return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.atlasFormBuilder');
        }
    };


}(window.jQuery, window, document));