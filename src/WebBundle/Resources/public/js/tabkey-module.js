var tabkeyModule = (function() {

    var $firstVisibleElement = null,
        $nextSubmit,
        $formGeneratorBox;

    var inited = false;

    var _setTabIndex = function () {

        if(!inited) return false;

        var scroll = $formGeneratorBox.scrollTop(),
            controlsTypes = 'input, textarea, select, button',
            elements = [];

        $formGeneratorBox.find(controlsTypes).not(':disabled').each(function (i, ele) {

            var $item = $(ele).closest('.grid-stack-item'),
                offset = $item[0].getBoundingClientRect();

            if(offset.top === 0 && offset.left === 0) return true;

            elements.push({
                el: $(ele),
                t: offset.top + scroll,
                l: offset.left,
                w: ele.offsetWidth
            });

            // $item.find(controlsTypes).each(function () {
            //     elements.push({
            //         el: $(this),
            //         t: offset.top + scroll,
            //         l: offset.left,
            //         w: ele.offsetWidth
            //     });
            // })

        });

        elements.push({
            el: $nextSubmit,
            t: 999999,
            l: 999999
        });

        elements.sort(function (a, b) {

            if (a.t === b.t) {
                if (a.l > b.l) return 1;
                else if (a.l < b.l) return -1;
                if(a.w < b.w) return 1;

                return 0;
            }
            else if (a.t > b.t) {
                return 1;
            }
            else {
                return -1;
            }

        });

        /** Ustawienie pierwszego widocznego elementu */
        if(elements.length > 0) {
            _setFirstVisibleElement(elements[0].el);
        }
        else {
            _setFirstVisibleElement(null);
        }

        var beginFrom = 100;
        $.each(elements, function (i, ele) {
            ele.el.attr('tabindex', beginFrom + i + '');
        });

    };

    function isEnterKey(e) {
        return (e.which === 13);
    }

    function isTabKey(e) {
        return (e.which === 9);
    }

    function isSpaceKey(e) {
        return (e.which === 32);
    }

    function isShiftKey(e) {
        return (e.shiftKey);
    }

    function isArrowUp(e) {
        return (e.which === 38);
    }

    function isArrowDown(e) {
        return (e.which === 40);
    }

    function getGridStackItemWithSubmit($gridStackItem, withSubmit) {

        if(withSubmit) {
            return $gridStackItem.add('.generating-form-submit');
        }

        return $gridStackItem;

    }

    var _handleTabKey = function ($wrapper, withOutSubmit) {

        if(!inited) return false;

        $wrapper = $wrapper || $('.step-form-grid-stack');
        withOutSubmit = withOutSubmit || false;

        var $gridStackItem = $wrapper.find('.grid-stack-item:not(:has(.grid-stack-item))');

        $wrapper.find('.grid-stack-item select').on('keydown', function (e) {

            if ( isEnterKey(e) || isSpaceKey(e) ) {

                e.preventDefault();

                var $t = $(this);

                if ($t.is('select')) {
                    $t.select2('open');
                }

            }

        }).on('select2:close', function (e) {

            $(this).focus().closest('.grid-stack-item').addClass('highlighted');

        });

        if(!withOutSubmit) {

            /** allow to switch to first el with tab and prev element with shift+tab */

            $nextSubmit.on('keydown', function (e) {

                if ( isTabKey(e) && !isShiftKey(e) ) {

                    $firstVisibleElement.focus();
                    return false;

                }
                else if ( isShiftKey(e) && isTabKey(e) ) {

                    var beforeLastIndex = parseInt($nextSubmit.attr('tabindex')) - 1;
                    $formGeneratorBox.find('[tabindex="' + beforeLastIndex + '"]').focus();

                    return false;
                }
            });
        }

        getGridStackItemWithSubmit($gridStackItem, !withOutSubmit).on('keyup', function (e) {

            var $t = $(this);

            if ( isTabKey(e) ) {

                $('.step-form-grid-stack .grid-stack-item, .generating-form-submit').removeClass('highlighted');

                $t.addClass('highlighted');

                var isChecked = ($t.find('input:checked').length > 0);

                if(isChecked) {
                    $t.find('input:checked').focus();
                }
                else if ($t.find('input').length > 1) {
                    $t.find('input').first().focus();
                }

            }
            else if ( isEnterKey(e) ) {
                if ($t.find('select[data-attribute-value-id]').length) {
                    $t.find('select[data-attribute-value-id]').focus();
                }
            }

        });

        $gridStackItem.find('input.urgent[type="checkbox"]').on('keydown', function (e) {
            var $t = $(this);

            if ( isSpaceKey(e) ) {
                e.preventDefault();
                $t.prop("checked") === true ? $t.prop("checked", false).attr('value', '0') : $t.prop("checked", true).attr('value', '1');
            }

        });

        $gridStackItem.find('.custom-radio-button input').on('keydown', function (e) {

            var $t = $(this);


            // if ($t.val() == "1") {
            //     $t.parent().find('.radio-button-yes').click();
            // }
            //
            // if ($t.val() == "2") {
            //     $t.parent().find('.radio-button-no').click();
            // }
            //
            // if ( isSpaceKey(e) ) {
            //     e.preventDefault();
            //
            //     if ($t.val()) {
            //         $t.parent().find('.radio-button-yes').click();
            //     }
            //
            //     $t.is(':checked') && $t.val() == "1" ? $t.parent().find('.radio-button-no').click() : $t.parent().find('.radio-button-yes').click();
            // }

        });

        $gridStackItem.on('blur', 'select, input:not([type="radio"]), textarea, button', function (e) {
            clearAllHighlighted();
        });

        $gridStackItem.on('click', function (e) {
            var $t = $(this);
            if($t.find('.panel-edit-attribute').length > 0 || $t.find('.atlas-upload-file-wrapper')) return;

            if($t.find(':focus').length) {

                var el = $t.find(':focus');

                if(el.hasClass('select2-selection')) {
                    $t.find('select').focus();
                    $t.addClass('highlighted');
                }

            }
            else {
                clearAllHighlighted();

                if(($t.find('input:checked').length > 0)) {
                    $t.find('input:checked').focus();
                }
                else if ( $t.attr('data-widget') === '' || $t.attr('data-widget') === 'button'  ) {
                    $t.addClass('highlighted').find('input, textarea, select, button').first().focus();
                }
            }

        });
    };

    function clearAllHighlighted() {
        $('.operation-dashboard-form').find('.highlighted').removeClass('highlighted');
    }

    var _focusOnFirstFormElement = function () {

        if($firstVisibleElement === null || $firstVisibleElement.length === 0) return;
        if ($firstVisibleElement[0].tagName === "BUTTON"){
            return;
        }

        if($firstVisibleElement.hasClass('date-time-picker') || $firstVisibleElement.hasClass('date-picker')) {
            return;
        }

        if ($firstVisibleElement.hasClass('generating-form-submit') || $firstVisibleElement === null) {
            $nextSubmit.addClass('highlighted').focus();
        } else {

            if($firstVisibleElement.closest('.grid-stack-item').find('input:checked').length > 0) {
                $firstVisibleElement.closest('.grid-stack-item').find('input:checked').focus();
            }
            else {
                $firstVisibleElement.focus();
            }

            $firstVisibleElement.closest('.grid-stack-item').addClass('highlighted');
        }
    };

    /** allow to switch to last element with shift+tab only on first list el */

    var allowSwitchToLastElement = function (e) {

        if ( isShiftKey(e) && isTabKey(e) ) {
            e.preventDefault();
            $nextSubmit.focus();
            return false;
        }

    };

    var _setFirstVisibleElement = function ($el) {

        var isRadio = false;

        if($el !== null && $el.length) {
            isRadio = ($el[0].tagName === "INPUT" && $el[0].type === "radio");
        }

        if($firstVisibleElement !== null ) {
            if(isRadio) {
                $firstVisibleElement.closest('.grid-stack-item').find('input').off('keydown', allowSwitchToLastElement);
            }
            else {
                $firstVisibleElement.off('keydown', allowSwitchToLastElement);
            }
        }

        if($el === null || $el.length === 0) {
            $firstVisibleElement = null;
            return false;
        }

        $firstVisibleElement = $el;

        if($firstVisibleElement !== null ) {

            if(isRadio) {
                $firstVisibleElement.closest('.grid-stack-item').find('input').on('keydown', allowSwitchToLastElement);
            }
            else {
                $firstVisibleElement.on('keydown', allowSwitchToLastElement);
            }
        }

    };

    var _init = function () {

        if($('.step-form-grid-stack').length <= 0) return false;

        inited = true;

        $nextSubmit = $('.generating-form-submit');
        $formGeneratorBox = $('#form-generator-box');

        _setTabIndex();
        _handleTabKey();
        _focusOnFirstFormElement();

    };


    return {
        init: _init,
        setTabIndex: _setTabIndex,
        handleTabKey: _handleTabKey,
        focusOnFirstFormElement : _focusOnFirstFormElement
    }

})();