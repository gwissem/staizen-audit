if (typeof _gridStackAtlas === "undefined") {

    throw 'You must add this file: grid-stack-atlas-component.js !';

}

(function ($, window, document) {

    $.fn.autoCompleteWithTemplate = function (opts) {

        if (this.length === 0) return this;

        $.extend(defaultOpts(), opts);

        function defaultOpts() {
            return {
                templates: [
                    {
                        label: "/template",
                        value: "First example template"
                    }
                ]
            }
        }

        function split(val) {
            return val.split(/\/\/\s*/);
        }

        function extractLast(term) {
            return split(term).pop();
        }

        var _this = this;

        this.on("keydown", function (event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }

            if (event.keyCode === $.ui.keyCode.TAB) {
                var position = this.value.indexOf("»");

                if (position !== -1) {
                    _this[0].selectionStart = position;
                    _this[0].selectionEnd = position + 1;
                    // _this[0].scrollTop = _this[0].scrollHeight;
                    event.preventDefault();
                }
                else {
                    event.preventDefault();

                    var start = _this[0].selectionStart,
                        end = _this[0].selectionEnd;

                    _this.val(_this.val().substring(0, start) + "\t" + _this.val().substring(end));
                    _this[0].selectionStart = _this[0].selectionEnd = start + 1;

                }
            }


        })
            .autocomplete({
                minLength: 2,
                classes: {
                    "ui-autocomplete": "ui-autocomplete-javascript"
                },
                source: function (request, response) {
                    response($.ui.autocomplete.filter(
                        opts.templates, extractLast(request.term.substring(0, _this[0].selectionEnd))));
                },
                focus: function () {
                    return false;
                },
                select: function (event, ui) {

                    var position = _this[0].selectionEnd,
                        suffixTerm = null;

                    if (position !== -1) {
                        suffixTerm = this.value.substring(position, this.value.length);
                    }

                    var terms = split(this.value);
                    terms.pop();
                    terms.push(ui.item.value);
                    if (suffixTerm) terms.push(suffixTerm);
                    terms.push("");

                    this.value = terms.join("");

                    position = this.value.indexOf("»");

                    if (position !== -1) {
                        setTimeout(function () {
                            _this[0].selectionStart = position;
                            _this[0].selectionEnd = position + 1;
                            // _this[0].scrollTop = _this[0].scrollHeight;
                        }, 100);
                    }

                    return false;
                }
            });

        return this;
    };


    var currentElements = [];
    var config = [];
    var savedGridItems = [];
    var serializedData = [];
    var gridItemContent = '';
    var $stepModal = $('#step-modal'),
        generatorType,
        $formContent = $('#form-content'),
        currentStep = '',
        $spinner = $('#bottom-spinner'),
        $formGeneratorView = '',
        generatorConfig,
        globalClipboard= null;


    var $attributesList = null;
    var allFields = [];

    var fileAdvancedUpload;

    // var _shortAction;

    $(function () {

        if ($('#step-modal').length) {
            $stepModal = $('#step-modal');
            generatorType = 'step';
        }
        else if ($('#attribute-modal').length) {
            $stepModal = $('#attribute-modal');
            generatorType = 'attribute';
        }

        buttonHandle();

        generatorConfig = (function() {

            var prefixKey = 'generatorControl_',
                attributesConfig = $('.grid-stack-item');

            function _getConfig(id) {

                var controlConfig = localStorage.getItem(prefixKey + id);

                if(!controlConfig) {
                    controlConfig = _defaultConf(id);
                    _setConfig(controlConfig);
                } else {
                    controlConfig = JSON.parse(controlConfig);
                }

                return controlConfig;

            }

            function _setConfig(config) {
                localStorage.setItem(prefixKey + config.id, JSON.stringify(config));
            }

            function _defaultConf(id) {
                return {
                    'id': id,
                    'label': false,
                    'placeholder': false,
                    'styles': false,
                    'class': false,
                    'js': false,
                    'conditions': true
                }
            }

            function _saveValue(config, key, value) {
                config[key] = value;
                _setConfig(config);
                return config[key];
            }

            return {
                get: _getConfig,
                set: _setConfig,
                save: _saveValue
            }
        })();



        fileAdvancedUpload = new FileAdvancedUpload();
        fileAdvancedUpload.Run();

    });

    function nestedAccordion() {
        var nestedParent = $('.nested-accordion');
        nestedParent.find('.comment').slideUp();
        nestedParent.on('click', '.fa', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).parent().next().slideToggle(100);
            $(this).toggleClass('selected');
        });
    }

    /**
     * @param {object} $gridStackItem
     */
    function getPickedElements($gridStackItem) {

        var typeControl = '';

        if ($gridStackItem.find('.custom-radio-wrapper').length !== 0) {
            typeControl = 'radio';
        }
        else if ($gridStackItem.find('.wrapped-select').length !== 0) {
            typeControl = 'select';
        }
        else if ($gridStackItem.find('.custom-checkbox-wrapper').length !== 0) {
            typeControl = 'checkbox';
        }

        switch (typeControl) {
            case 'radio':
            {
                return 'div.custom-radio-wrapper, div.custom-radio:first label, .custom-radio-wrapper > .custom-radio-text';
            }
            case 'select':
            {
                return '.wrapped-select, .wrapped-select > label';
            }
            case 'checkbox':
            {
                return '.custom-checkbox-wrapper, .custom-checkbox-wrapper > span';
            }
            default:
            {
                return 'input, .custom-radio label, textarea, select, p, h1, h2, h3, h4, label, div.vertical-divider, div.horizontal-divider, button, div.grid-image-wrapper, div.grid-image-wrapper img';
            }
        }
    }

    function stylingAvailableElements() {
        var grid = $('#atlasGrid'),
            $activeItem = $('.grid-stack-item.highlighted'),
            pickedElements = getPickedElements($activeItem),
            visualSelect = $('.visualElementSelect');

        visualSelect.find('option').remove();
        gridItemContent = $('.grid-stack-item-content');

        /** RADIO INPUT jeszcze nie działa */

        $activeItem.find(pickedElements).each(function () {
            var thisNodeName = this.nodeName;
            // suffixStyleName = $t.attr('data-style-node-name');

            $('.singleField').find(visualSelect).append('<option value="' + thisNodeName + '">' + thisNodeName + '</option>')
        })

    }

    function leftSideTabs() {
        $("#attr-tabs").tabs();
    }

    function leftSideTabsHeightSet() {
        $("#attr-tab-1").height($(window).height() - 185);
    }

    function deleteFormPlacementStyles() {
        $('#form-generator-box').removeAttr('style');
    }

    function resizeRightPanel() {
        
        var resizeTimer;

        $(window).on('resize', function(e) {

            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                var notificationsWrapper = $('.notifications-wrapper'),
                    windowHeight = $(window).height(),
                    titleHeight = $('.generator-part-title').height(),
                    infobar = $('.generate-title').height(),
                    modalHeader = $('#step-modal .modal-header').height();
                notificationsWrapper.height(windowHeight - (titleHeight + infobar + modalHeader + 70 ));
            }, 250);

        });
    }


    function JsSyntaxHighlight() {

        if(isReadOnly){
            return false;
        }

        var jsSnippetsSelect = $('.js-snippets'),
            optionValue,
            textArea = $("textarea[name='javascript']").get(0);

        if(!textArea) return;

        var editor = CodeMirror.fromTextArea(textArea, {
                lineNumbers: true,
                autoRefresh:true,
                lineWrapping : false,
                styleActiveLine: true,
                fixedGutter:true,
                coverGutterNextToScrollbar:false,
                gutters: ['CodeMirror-lint-markers'],
                mode: {
                    name: "javascript"
                },
                extraKeys: {
                    "F11": function(cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                        if ( $('.modal-header .js-snippets').length ) {
                            $('.modal-header .js-snippets').remove();
                        } else {
                            $('.js-snippets').clone().appendTo($('.modal-header')).show();
                            $('.modal-header .js-snippets').addClass('m-l-2');
                            JsSnippetsChangeInit(editor);
                        }

                    },
                    "Esc": function(cm) {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                        $('.modal-header .js-snippets').remove();
                    }
                }
            });

        setTimeout(function() {
            editor.setValue($("textarea[name='javascript']").val());
        },100);

        editor.on('change',function(cm){
            $("textarea[name='javascript']").val(cm.getValue()).trigger('change');
        });

        $.each(templatesOfScripts, function() {
            var lineUp = $(this)[0].lineup || 0;
            jsSnippetsSelect.append($('<option data-lineup="'+lineUp+'" data-snippet="'+$(this)[0].value+'">').text($(this)[0].label));
        });

        function JsSnippetsChangeInit (editor) {

            var jsSnippetsSelect = $('.js-snippets'),
                optionValue,
                lineUpValue;

            jsSnippetsSelect.on('change', function() {
                optionValue = $(this).find('option:selected').attr('data-snippet');
                lineUpValue = $(this).find('option:selected').attr('data-lineup') || 0;


                $(this).val('-');
                editor.replaceSelection(optionValue, editor.getCursor("from"), editor.getCursor("to") );
               /* CodeMirror.commands["selectAll"](editor);
                var range = { from: editor.getCursor(true), to: editor.getCursor(false) };
                editor.autoFormatRange(range.from, range.to);*/
                editor.focus();
                editor.setCursor({line: 0, ch: 0});
                var vall = editor.getValue();
                var position = vall.indexOf("»");
                var obj = editor.posFromIndex(position);
                var cloneObj = JSON.parse(JSON.stringify(obj));
                cloneObj.ch++;
                editor.setSelection(obj, cloneObj);
            })
        }

        JsSnippetsChangeInit(editor);


    }


    function handleAccordionHeight() {
        var $notificationsWrapper = $('.notifications-wrapper'),
            $formPlacementWrapper = $('.form-placement-wrapper'),
            $modalHeaderHeight = $('#step-modal .modal-header, #attribute-modal .modal-header ').height(),
            $generatorPartTitleHeight = $('#step-modal .generator-part-title, #attribute-modal .generator-part-title').height();
        $notificationsWrapper.height($(window).height() - ($modalHeaderHeight + $generatorPartTitleHeight + 92 ));
        $formPlacementWrapper.height($(window).height() - ($modalHeaderHeight + $generatorPartTitleHeight + 90 ));
    }

    function checkAccordionStatus(id) {

        var accordionPanel = $('.notifications-wrapper .attr-accordion');

        var accordionConfig = generatorConfig.get(id);

        accordionPanel.each(function() {
            var $t = $(this),
                accordionType = $t.attr('data-panel-name');

            if ( accordionConfig[accordionType] ) {
                $t.find('.panel-collapse').addClass('in');
            }

        });

        updateAccordionConfig(id);
        attributeConfigAccordion(id);

    }

    function updateAccordionConfig(id) {
        var accordionPanel = $('.attr-accordion[data-panel-name]'),
            accordionConfig = generatorConfig.get(id);

        accordionPanel.on('click','a', function() {
            var $t = $(this),
                accordionType = $t.closest('.attr-accordion').attr('data-panel-name');

            setTimeout(function() {
                accordionConfig[accordionType] = (!$t.hasClass('collapsed'));
                generatorConfig.set(accordionConfig);
            },50)

        })
    }

    function attributeConfigAccordion(id) {

        var accordionPanel = $('.notifications-wrapper .attr-accordion'),
            accordionConfig = generatorConfig.get(id);

        $(".toggle-accordion").on("click", function () {
            var accordionId = $(this).attr("accordion-id"),
                numPanelOpen = $(accordionId + ' .collapse.in').length;

            $(this).toggleClass("active");

            if (numPanelOpen == 0) {
                openAllPanels(accordionId);
                accordionPanel.each(function() {
                    var accordionType =  $(this).attr('data-panel-name');
                    accordionConfig[accordionType] = true;
                });

                generatorConfig.set(accordionConfig);
            } else {
                closeAllPanels(accordionId);
                accordionPanel.each(function() {
                    var accordionType =  $(this).attr('data-panel-name');
                    accordionConfig[accordionType] = false;
                });
                generatorConfig.set(accordionConfig);
            }
        })

        openAllPanels = function (aId) {
            $(aId + ' .panel-collapse:not(".in")').collapse('show');
        }
        closeAllPanels = function (aId) {
            $(aId + ' .panel-collapse.in').collapse('hide');
        }


    }

    function handleModalHeight() {
        $stepModal.addClass('formGenerate');
    }

    var conditionFormPrototype = '<div class="panel attr-accordion 1" data-panel-name="conditions"><div class="row condition">' +
        '<label for="currentField" class="current-field-styles-label">' +
        '<i class="fa fa-check" aria-hidden="true">' +
        '</i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">Warunki</a></label>' +
        '<div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><div class="dynamic-condition-form">' +
        '<div class="loader-wrapper">' +
        '<div class="bottom-divider"></div>' +
        '<div class="loader">' +
        '<div class="ball-spin-fade-loader">' +
        '<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>' +
        '</div></div></div>' +
        '<div class="form-wrapper"></div>' +
        '</div></div></div></div></div>';

    var classFieldPrototype = function (fieldCustomClass) {

        if (typeof fieldCustomClass === "undefined") fieldCustomClass = '';

        return '<div class="panel margin-panel attr-accordion 10" data-panel-name="class"><div class="row styles">' +
            '<label for="classes" class="current-field-styles-label classes">' +
            '<i class="fa fa-paint-brush" ></i>' +
            '<a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse10" aria-expanded="true" aria-controls="collapse10">Klasy</a>' +
            '</label>' +
            '<div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><input name="classes" value="' + fieldCustomClass + '" class="current-field-input js-current-class-input" type="text" placeholder="Klasy oddzielone spacją"/></div></div>' +
            '</div></div>';
    };

    var availableTypeButtons = [
        {value: 'btn-default', label: 'Basic'},
        {value: 'green-meadow', label: 'Success'},
        {value: 'btn-primary', label: 'Primary'},
        {value: 'btn-primary-blue', label: 'Primary Dark'},
        {value: 'green', label: 'Green'},
        {value: 'btn-info', label: 'Info'},
        {value: 'btn-warning', label: 'Warning'},
        {value: 'btn-danger', label: 'Danger'},
        {value: 'red-mint', label: 'Red Mint'},
        {value: 'yellow-lemon', label: 'Yellow Lemon'}
    ];

    function getAccordionOfTypeButton() {

        var _options = '';

        availableTypeButtons.forEach(function (option) {
            _options += '<option value="'+option.value+'">'+option.label+'</option>'
        });

        return '<div class="panel attr-accordion 8" data-panel-name="label">' +
                '<div class="row styles">' +
                    '<label for="iframe-attribute-path" class="current-field-styles-label">' +
                        '<i class="fa fa-external-link" aria-hidden="true"></i>' +
                        '<a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse8" aria-expanded="true" aria-controls="collapse8">Typ buttona</a>' +
                    '</label>' +
                    '<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">' +
                        '<div class="panel-body">' +
                            '<select class="form-control js-button-type extra-data-field" name="type">' +
                                _options +
                            '</select>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>';

    }

    var javascriptTextareaPrototype = function (placeholder) {

        placeholder = placeholder || '';

        return '<div class="panel attr-accordion 3" data-panel-name="js"><div class="row placeholder">' +
            '<label for="iframe-attribute-path" class="current-field-styles-label placeholder"><i class="fa fa-code" aria-hidden="true"></i><select class="js-snippets"><option disabled selected value="-">Wybierz szablon</option></select><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">Javascript</a></label>' +
            '<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><textarea rows="8" name="javascript" class="extra-data-field" type="text" placeholder="' + placeholder + '"></textarea></div></div>' +
            '</div></div>';
    };

    var XHR_loadingForm = null;

    function currentItemsCreate() {

        var configBox = $('.right-side .config-box');
        var gridFirstElement = $('#atlasGrid .grid-stack-item').first();

        var configelementsContainer = $('.current-field-config-wrapper');
        var firstHighlightedElement = $('#atlasGrid').find('.grid-stack-item.highlighted');
        var fieldLabel = firstHighlightedElement.find('.js-label-text').text();
        var elementId = firstHighlightedElement.attr('id');

        configBox.addClass('active');
        $('.js-current-label').text($('.grid-stack-item.highlighted').find('.js-label-text').text());
        updateFieldsOnChange();

        $('#atlasGrid').on('click', '.grid-stack-item', function (e) {

            // if(_shortAction.isActive()) return false;

            e.stopPropagation();

            var $t = $(this);
            var elementId = $t.attr('id'),
                formControlId = $t.attr('data-id');

            var isNestedItem = ($t.hasClass('nested-stack-item') && $t.parents('.nested-stack-item').length) ? true : false;

            if (isNestedItem) {

                if(generatorType === "step") {
                    var singleField = $('<div data-current-id="' + elementId + '" class="singleField image-config visible col-xs-12">' +
                        conditionFormPrototype +
                        '</div>');
                }
                else {
                    var singleField = "<div class='singleField col-xs-12 visible'>Konfiguracja niedostępna (dla elementów zagnieżdżonych w generatorze atrybutu)</div>";
                }

            } else {
                var fieldLabel = $t.find('.grid-stack-item-content > div > .js-label-text').html(),
                    fieldCustomClass = $t.attr('data-custom-class');

                var visualElementText = '';

                if ($t.attr('data-widget') === "button" || $t.attr('data-widget') === "paragraph" || $t.attr('data-widget') === "header") {
                    visualElementText = $t.find('.grid-stack-item-content .js-visual-element-text').html();
                }
                else {
                    visualElementText = $t.find('.grid-stack-item-content .js-visual-element-text').text();
                }
                var tempLabel = $('<div />').append(visualElementText);

                if(tempLabel.find('.openTranslationModal').length)
                {
                    tempLabel.find('.openTranslationModal').each(function (){
                        $(this).replaceWith($(this).text())
                    });

                    visualElementText = tempLabel.html();
                }

                if ($t.attr('data-widget') == "image") {

                    var singleField = $('<div data-current-id="' + elementId + '" class="singleField image-config visible col-xs-12">' +
                        '<div class="panel attr-accordion 7" data-panel-name="styles"><div class="row styles">' +
                        '<select class="visualElementSelect"></select><label for="currentFieldStyles" class="current-field-styles-label"><i class="fa fa-pencil" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">Style css</a></label>' +
                        '<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><textarea rows="5" name="currentField" class="current-field-styles-input js-current-styles" value="" type="text"></textarea></div></div>' +
                        '</div></div>' +
                        classFieldPrototype(fieldCustomClass) +
                        '<div class="row file">' +
                        ((generatorType === "step") ? conditionFormPrototype : '' ) +
                        '<form class="form-block-upload-file" action="">' +
                        '<input type="file" id="widget-image" name="file" required="required" class="advanced-upload" data-url="' + oneupUploaderEndpoint + '">' +
                        '<button id="upload-widget-image" data-style="zoom-in" data-size="s" type="button" disabled class="btn disabled ladda-button">' +
                        '<span class="ladda-label">Załaduj obrazek</span></button></form>' +
                        '<div class="preview-image-widget"><img src="" alt=""></div>' +
                        '</div>');
                }

                else if ($t.attr('data-widget') == "button") {
                    var singleField = $('<div data-current-id="' + elementId + '" class="singleField visible col-xs-12">' +
                        '<div class="panel attr-accordion 7" data-panel-name="label"><div class="row label">' +
                        '<label for="currentField" class="current-field-styles-label"><i class="fa fa-font" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">Nazwa pola</a></label>' +
                        '<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><textarea class="current-field-input js-current-label" value="' + visualElementText + '" type="text"></textarea></div></div>' +
                        '</div></div>' +
                        getAccordionOfTypeButton() +
                        '<div class="panel attr-accordion 9" data-panel-name="label"><div class="row styles">' +
                        '<select class="visualElementSelect"></select><label for="currentFieldStyles" class="current-field-styles-label"><i class="fa fa-pencil" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse9" aria-expanded="true" aria-controls="collapse9">Style css</a></label>' +
                        '<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><textarea name="currentField" class="current-field-styles-input js-current-styles" value="" type="text"></textarea></div></div>' +
                        '</div>' + classFieldPrototype(fieldCustomClass) + javascriptTextareaPrototype() + ((generatorType === "step") ? conditionFormPrototype : '' ) +
                        '</div></div>');
                }

                else if ($t.attr('data-widget') == "iframe") {

                    var singleField = $('<div data-current-id="' + elementId + '" class="singleField visible col-xs-12">' +
                        '<div class="row label iframe-src">' +
                        '<label for="iframe-attribute-path" class="current-field-styles-label"><i class="fa fa-external-link" aria-hidden="true"></i>src</label>' +
                        '<textarea name="src" rows="6" class="extra-data-field" type="text"></textarea>' +
                        '</div>' + javascriptTextareaPrototype('Wartości klikniętego wiersza znajdują się w zamiennej: values') + ((generatorType === "step") ? conditionFormPrototype : '' ) +
                        '</div>');
                }
                else if ($t.attr('data-widget') == "div") {

                    var fieldLabel = $t.find('.grid-stack-item-content > .js-label-text').html(),
                        fieldCustomClass = $t.attr('data-custom-class');

                    var tempLabel = $('<div />').append(fieldLabel);

                    if(tempLabel.find('.openTranslationModal').length)
                    {
                        tempLabel.find('.openTranslationModal').each(function (){
                            $(this).replaceWith($(this).text())
                        });

                        fieldLabel = tempLabel.html();
                    }

                    var singleField = $('<div data-current-id="' + elementId + '" class="singleField visible col-xs-12">' +
                        '<div class="panel attr-accordion 4" data-panel-name="label"><div class="row label">' +
                        '<label for="currentField" class="current-field-styles-label"><i class="fa fa-font" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">Nazwa pola</a></label>' +
                        '<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><textarea rows="10" class="current-field-input js-current-label" value="' + fieldLabel + '" type="text"></textarea></div></div>' +
                        '</div></div>' +
                        '<div class="panel attr-accordion 5" data-panel-name="styles"><div class="row styles">' +
                        '<select class="visualElementSelect"></select><label for="currentFieldStyles" class="current-field-styles-label"><i class="fa fa-pencil" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">Style css</a></label>' +
                        '<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><textarea rows="5" name="currentField" class="current-field-styles-input js-current-styles" value="" type="text"></textarea></div></div>' +
                        '</div></div>' +
                        classFieldPrototype(fieldCustomClass) +
                        '<div class="panel attr-accordion 6" data-panel-name="placeholder"><div class="row placeholder">' +
                        '<label for="currentField" class="current-field-styles-label"><i class="fa fa-file-text-o" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">Placeholder</a></label>' +
                        '<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><input name="currentField" class="current-field-input js-current-placeholder"  value="" type="text"></div></div>' +
                        '</div></div>' + javascriptTextareaPrototype() +
                        ((generatorType === "step") ? conditionFormPrototype : '' ) +
                        '</div>');
                }
                else {

                    var singleField = $('<div data-current-id="' + elementId + '" class="singleField visible col-xs-12">' +
                        '<div class="panel attr-accordion 4" data-panel-name="label"><div class="row label">' +
                        '<label for="currentField" class="current-field-styles-label"><i class="fa fa-font" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">Nazwa pola</a></label>' +
                        '<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><textarea rows="10" class="current-field-input js-current-label" value="' + fieldLabel + '" type="text"></textarea></div></div>' +
                        '</div></div>' +
                        '<div class="panel attr-accordion 5" data-panel-name="styles"><div class="row styles">' +
                        '<select class="visualElementSelect"></select><label for="currentFieldStyles" class="current-field-styles-label"><i class="fa fa-pencil" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">Style css</a></label>' +
                        '<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><textarea rows="5" name="currentField" class="current-field-styles-input js-current-styles" value="" type="text"></textarea></div></div>' +
                        '</div></div>' +
                        classFieldPrototype(fieldCustomClass) +
                        '<div class="panel attr-accordion 6" data-panel-name="placeholder"><div class="row placeholder">' +
                        '<label for="currentField" class="current-field-styles-label"><i class="fa fa-file-text-o" aria-hidden="true"></i><a role="button" data-toggle="collapse" data-parent="#attrAccordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">Placeholder</a></label>' +
                        '<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><input name="currentField" class="current-field-input js-current-placeholder"  value="" type="text"></div></div>' +
                        '</div></div>' + javascriptTextareaPrototype() +
                        ((generatorType === "step") ? conditionFormPrototype : '' ) +
                        '</div>');
                }
            }

            configelementsContainer.find('.singleField').remove();
            configelementsContainer.append(singleField);

            var extraDataAttr = $t.attr('data-extra-data');

            if ($t.attr('data-widget') === "image") {
                initImageUploadControl($(this), configelementsContainer);
            }
            else if (typeof extraDataAttr !== typeof undefined && extraDataAttr !== false && extraDataAttr != '') {
                initExtraData($t, configelementsContainer);
            }

            if (generatorType === "step") {
                loadingConditionForm($t.attr('data-id'), configelementsContainer.find('.dynamic-condition-form'));
            }

            redrawControlId(formControlId, $t.attr('data-attribute-path'));

            $("#atlasGrid .grid-stack-item").not(this).removeClass('highlighted');
            $(this).addClass('highlighted');
            $('.js-current-styles').val($t.find('.grid-stack-item-content ' + selectedVisualElement).attr('style'));
            $('.js-current-placeholder').val($t.find('.custom-input').attr('placeholder'));
            visualElementText = visualElementText == '.' ? '' : visualElementText;
            $('.js-current-label').text(fieldLabel || visualElementText);

            stylingAvailableElements();
            updateFieldsOnChange();
            setAutoCompleteOnJsTextarea(configelementsContainer);
            checkAccordionStatus(formControlId);
            JsSyntaxHighlight();
            resizeRightPanel();
            $('.js-current-label').trigger('change');
            var selectedVisualElement = $('.visualElementSelect').val();
            $('.js-current-styles').val($t.find('.grid-stack-item-content ' + selectedVisualElement).attr('style'));

        });

        $('#atlasGrid').on('click', '.openTranslationModal',function (e) {
            e.stopPropagation();
            $key = $(this).text();
        $.ajax({
                type: "GET",
                url: Routing.generate('ajaxTextFormTranslation',
                    {'key':$key}),
            success:function(data){
                    var modalObject = $('#ajaxTextFormTranslationModal');
                    var modalObjectBody = $('#ajaxTextFormTranslationModal .modal-body');
                    var modalObjectHeader = $('#ajaxTextFormTranslationModal .key-name');
                    modalObjectBody.html(data.html);
                    modalObjectHeader.html(data.key);
                     modalObject.modal('show');
                    bindKeyButtons();
                }
        });
            });
        if (gridFirstElement.length) {
            gridFirstElement.addClass('highlighted');
            gridFirstElement.click();
        }

        configBox.on('click', '.next-item', function () {
            var fieldLabel = $('#atlasGrid .grid-stack-item.highlighted').next().find('.js-label-text').text();
            var elementId = $('#atlasGrid .grid-stack-item.highlighted').next().attr('id');
            var singleField = $('<div data-current-id="' + elementId + '" class="singleField visible col-xs-12">' +
                '<div class="row label">' +
                '<label for="currentField" class="current-field-styles-label"><i class="fa fa-font" aria-hidden="true"></i>Nazwa pola</label>' +
                '<textarea class="current-field-input js-current-label" value="' + fieldLabel + '" type="text"></textarea>' +
                '</div>' +
                '<div class="row styles">' +
                '<label for="currentFieldStyles" class="currrent-field-styles-label"><i class="fa fa-pencil" aria-hidden="true"></i>Style css</label>' +
                '<select class="visualElementSelect"></select><textarea name="currentField" class="current-field-styles-input js-current-styles" value="" type="text"></textarea>' +
                '</div>' +
                '<div class="row placeholder xxx">' +
                '<label for="currentFieldStyles" class="current-field-styles-label"><i class="fa fa-file-text-o" aria-hidden="true"></i>Placeholder</label>' +
                '<input name="currentField" class="current-field-input js-current-placeholder"  value="" type="text">' +
                '</div>' +
                '</div>');
            configelementsContainer.find('.singleField').remove();
            configelementsContainer.append(singleField);
            $('#atlasGrid').find('.grid-stack-item.highlighted').next().click();
            updateFieldsOnChange();
            $('.js-current-styles').val($('.grid-stack-item.highlighted').attr('style'));
            $('.js-current-placeholder').val($('.grid-stack-item.highlighted').find('.custom-input').attr('placeholder'));
            $('.js-current-label').text($('.grid-stack-item.highlighted').find('.js-label-text').text());
        });

        configBox.on('click', '.prev-item', function () {
            var fieldLabel = $('#atlasGrid .grid-stack-item.highlighted').prev().find('.js-label-text').text();
            var elementId = $('#atlasGrid .grid-stack-item.highlighted').prev().attr('id');
            var singleField = $('<div data-current-id="' + elementId + '" class="singleField visible col-xs-12">' +
                '<div class="row label">' +
                '<label for="currentField" class="current-field-styles-label"><i class="fa fa-font" aria-hidden="true"></i>Nazwa pola</label>' +
                '<textarea name="currentField" class="current-field-input js-current-label" value="' + fieldLabel + '" type="text"></textarea>' +
                '</div>' +
                '<div class="row styles">' +
                '<label for="currentFieldStyles" class="current-field-styles-label"><i class="fa fa-pencil" aria-hidden="true"></i>Style css</label>' +
                '<select class="visualElementSelect"></select><textarea name="currentField" class="current-field-styles-input js-current-styles" value="" type="text"></textarea>' +
                '</div>' +
                '<div class="row placeholder yyy">' +
                '<label for="currentFieldStyles" class="current-field-styles-label"><i class="fa fa-file-text-o" aria-hidden="true"></i>Placeholder</label>' +
                '<input name="currentField" class="current-field-input js-current-placeholder"  value="" type="text">' +
                '</div>' +
                '</div>');
            configelementsContainer.find('.singleField').remove();
            configelementsContainer.append(singleField);
            $('#atlasGrid').find('.grid-stack-item.highlighted').prev().click();
            updateFieldsOnChange();
            $('.js-current-styles').val($('.grid-stack-item.highlighted').attr('style'));
            $('.js-current-placeholder').val($('.grid-stack-item.highlighted').find('.custom-input').attr('placeholder'));
            $('.js-current-label').text($('.grid-stack-item.highlighted').find('.js-label-text').text());
        });

    }

    function redrawControlId(id, path) {

        if(globalClipboard) {
            globalClipboard.destroy();
        }

        if(path) {

            globalClipboard = new Clipboard('#copy-current-attr-path');

            globalClipboard.on('success', function (e) {
                toastr.info('Skopiowano: ' + e.text);
                if (window.getSelection) {
                    if (window.getSelection().empty) {  // Chrome
                        window.getSelection().empty();
                    } else if (window.getSelection().removeAllRanges) {  // Firefox
                        window.getSelection().removeAllRanges();
                    }
                } else if (document.selection) {  // IE?
                    document.selection.empty();
                }
            });

        }

        $('#id-current-attr-path').val(path);
        $('#id-current-control').val(((id) ? id : '-'));
        $('#id-current-attr-path-with-prefix').val('{@' + path + '@}');
    }

    function loadingConditionForm(id, $formBox) {

        if (!id) {
            $formBox.find('.form-wrapper').html('<p style="color: #fff; padding: 10px; font-weight: 700;">Najpierw należy zapisać formularz z nowym elemenetem.</p>');
            $formBox.addClass('loaded');
            return;
        }

        var urlConditionForm = Routing.generate('admin_attribute_condition', {step: currentStep, formControlId: id});

        if (XHR_loadingForm) {
            XHR_loadingForm.abort();
            XHR_loadingForm = null;
        }

        $formBox.removeClass('loaded');

        XHR_loadingForm = $.ajax({
            type: "GET",
            url: urlConditionForm,
            success: function (msg) {
                $formBox.find('.form-wrapper').html(msg.form);
            },
            complete: function () {
                $formBox.addClass('loaded');
            }
        });

    }

    function initImageUploadControl($t, configelementsContainer) {

        fileAdvancedUpload.refreshInit();

        var l;

        if ($t.attr('data-extra-data')) {
            var attrExtraData = JSON.parse($t.attr('data-extra-data'));
            configelementsContainer.find('.preview-image-widget img')[0].src = attrExtraData.path;
        }

        $('#widget-image').fileupload({
            dataType: 'json',
            replaceFileInput: false,
            progressInterval: 15,
            add: function (e, data) {
                data.context = $('#upload-widget-image').removeClass('disabled').prop('disabled', false)
                    .on('click', function (e) {
                        e.preventDefault();
                        l = Ladda.create(this);
                        l.start();
                        data.submit();
                    });
            },
            start: function (e) {

            },
            done: function (e, data) {
                if (l) {
                    l.stop();
                }

                if (data.result.path) {
                    $t.find('.grid-image')[0].src = data.result.path;
                    configelementsContainer.find('.preview-image-widget img')[0].src = data.result.path;

                    var extraData = {
                        'path': data.result.path
                    };

                    $t.attr('data-extra-data', JSON.stringify(extraData));
                }

                $('#upload-widget-image').addClass('disabled').prop('disabled', true);
                $('#widget-image').trigger('clearFiles');

            }

        }).bind('fileuploadprogress', function (e, data) {
        });

    }

    function initExtraData($t, configelementsContainer) {

        var extraDataAttr = $t.attr('data-extra-data');
        if (extraDataAttr) {
            setTimeout(function () {
                var extraData = JSON.parse($t.attr('data-extra-data'));
                $.each(extraData, function (key, value) {
                    if (key == 'javascript') {
                        value = decodeURIComponent(escape(window.atob(value.replace(/\s/g, ''))));
                    }
                    configelementsContainer.find('.extra-data-field[name=' + key + ']').val(value);
                });

            }, 10);
        }
    }

    function saveGeneratedForm(savedGridItems, defaultHeight, $t) {

        if (savedGridItems.length === 0) {
            savedGridItems = [];
        }
        for (var iter = 0; iter<savedGridItems.length;iter++)
        {
            try {
                var tempLabel = $('<div />').append(savedGridItems[iter].label);

                if(tempLabel.find('.openTranslationModal').length)
                {
                    tempLabel.find('.openTranslationModal').each(function (){
                        $(this).replaceWith($(this).text())
                    });

                    savedGridItems[iter].label = tempLabel.html();
                }


            }catch(err){
            }

        }

        var readyForm = {
            config: {
                height: (defaultHeight) ? defaultHeight : 2
            },
            controls: savedGridItems
        };

        $.ajax({
            url: Routing.generate('admin_process_form_generator_update', {type: generatorType, id: currentStep}),
            type: "POST",
            dataType: "json",
            data: {formGenerator: readyForm}
        }).done(function (data) {

            $('body').find('.info-wrapper').addClass('active');
            setTimeout(function () {
                $('body').find('.info-wrapper').removeClass('active')
            }, 3000);
            $('body').find('.info-wrapper .success').addClass('active');
            setTimeout(function () {
                $('body').find('.info-wrapper .success').removeClass('active')
            }, 3000);

            updateUniqueIdOfItems(data.news); 

            var $activeElement = $('.grid-stack-item.highlighted');

            if ($activeElement.length) {
                if ($activeElement.is('[data-unique-id]')) {
                    $activeElement[0].click();
                }
            }

            toastr.success("Pomyślnie zapisano formularz.", "Zapisano", {timeOut: 5000});

        }).fail(function (data) {
            $('body').find('.info-wrapper').addClass('active');

            setTimeout(function () {
                $('body').find('.info-wrapper').removeClass('active')
            }, 3000);

            $('body').find('.info-wrapper .error').addClass('active');

            setTimeout(function () {
                $('body').find('.info-wrapper .error').removeClass('active')
            }, 3000);

        }).complete(function () {
            $t.removeClass('saving');
        });
    }

    function updateUniqueIdOfItems(items) {

        $.each(items, function (i, ele) {
            if (ele.uniqueId) {
                $('[data-unique-id="' + ele.uniqueId + '"]').attr('data-id', ele.id);
            }
        });
    }

    function detectVisualElementChange() {
        $('.visualElementSelect').on('change', function () {
            var $t = $(this);
            var currentValue = $t.find(":selected").text();
            var gridVisualElement = $('#atlasGrid').find('.grid-stack-item.highlighted .grid-stack-item-content ' + currentValue + '');
            var gridVisualElementStyles = gridVisualElement.attr('style');
            $t.siblings("textarea").val(gridVisualElementStyles);
        })
    }

    var templatesOfScripts = [
        {
            label: 'click',
            value: '_this._click(function() {\n\t»\n});',
            lineup: 1
        },
        {
            label: 'change',
            value: '_this._change(function() {\n\t»\n});'
        },
        {
            label: 'keyup',
            value: '_this._keyup(function() {\n\t»\n});'
        },
        {
            label: 'toggleVisible',
            value: '_f.toggleVisible([ » ]);'
        },
        {
            label: 'hide',
            value: '_f.hide([ » ]);'
        },
        {
            label: 'show',
            value: '_f.show([ » ]);'
        },
        {
            label: 'wait',
            value: '_f.wait(100, function() {\n\t»\n});'
        },
        {
            label: 'checkedValue',
            value: 'if(_f.checkedValue(_this) == \'»\') {\n\t»\n};'
        },
        {
            label: 'if',
            value: 'if(»){\n\t»\n};'
        },
        {
            label: 'if-value',
            value: "if(_this.val(») == »){\n\t»\n};"
        },
        {
            label: 'if-else',
            value: "if(»){\n\t»\n} else(»){\n\t»\n};"
        },
        {
            label: "disable",
            value: "_f.disable([ » ], 'button');"
        },
        {
            label: "refreshMap",
            value: "_f.refreshMap();»"
        },
        {
            label: "setMapPath",
            value: "$('#atlas-map-step').attr('data-location-path', '»');"
        },
        {
            label: 'openModal',
            value: "var $customModal = _f.openModal('»',\n\t function($modal){ \n\t //confirm \n\t\t»\n\t }, function($modal){ \n\t\t//cancel \n\t\t»\n\t });"
        },
        {
            label: 'getAttributeValue',
            value: "getAttributeValue('»','string', function(value){\n\tif(!value){\n\t\t»\n\t}\n});"
        },
        {
            label: 'formTooltip',
            value: "_f.formTooltip(_this, '»', {placement: 'top'});"
        },
        {
            label: 'setAttributeValue',
            value: "setAttributeValue('»', », function(response){\n\t»\n});"
        },
        {
            label: 'getValueByPath',
            value: "_f.getValueByPath('»');"
        },
        {
            label: 'disableNext',
            value: "_f.disableNext();"
        },
        {
            label: 'enableNext',
            value: "_f.enableNext();"
        },
        {
            label: 'nextStep',
            value: "_f.nextStep(»);"
        },
        {
            label: 'onSuccessSetAttribute',
            value: "_this.on(\'success-set-attribute\', function(data){\n\t»\n});"
        },
        {
            label: 'refreshContent',
            value: "».trigger('refresh-content');"
        }
    ];

    function setAutoCompleteOnJsTextarea($configWrapper) {

        $configWrapper.find('textarea[name="javascript"]').autoCompleteWithTemplate({
            templates: templatesOfScripts
        });

    }

    function updateFieldsOnChange() {
        detectVisualElementChange();
        var currentFieldInput = $('.js-current-label, .js-current-styles, .js-current-placeholder, .js-button-type, .extra-data-field, .js-current-class-input');

        currentFieldInput.each(function () {
            $(this).on('change', function () {

                var elementInGrid = $('#atlasGrid .grid-stack-item.highlighted'),
                    $this = $(this);
                //removing the
                if ($this.hasClass('js-current-label')) {
                    // var currentLabel = $(this).val().replace(/\n/g, "") == '.' ? '' : $(this).val();
                    var currentLabel = $(this).val();


                    var regexForTranslation = /{-(\S*)-}/gm;
                    if(regexForTranslation.test(currentLabel)) {

                        currentLabel = currentLabel.replace(regexForTranslation, function (x) {
                            return '<span class="openTranslationModal">' + x + '</span>'
                        });
                    }
                    //Check for existing translation key
                    //replaceexistingkey with the span with following
                    //
                    elementInGrid.find(".js-label-text, .js-visual-element-text").html(currentLabel);
                }

                if ($this.hasClass('js-current-styles')) {
                    var thisValue = $(this).val();
                    var selectedVisualElement = $('.visualElementSelect').find(":selected").text();
                    elementInGrid.find('.grid-stack-item-content ' + selectedVisualElement).attr('style', thisValue);
                }
                else if ($this.hasClass('js-current-class-input')) {

                    if (elementInGrid.attr('data-custom-class')) {
                        elementInGrid.removeClass(elementInGrid.attr('data-custom-class'));
                    }

                    elementInGrid.attr('data-custom-class', $this.val());
                    elementInGrid.addClass($this.val());

                }

                if ($this.hasClass('js-current-placeholder')) {
                    elementInGrid.find('input').attr('placeholder', $(this).val())
                }

                if ($this.hasClass('js-button-type')) {
                    var thisValue = $(this).val();
                    elementInGrid.attr('data-button-type', thisValue).find('button').removeClass().addClass('btn js-visual-element-text js-label-text ' + thisValue)
                }

                if ($this.hasClass('extra-data-field')) {

                    var extraDataAttr = elementInGrid.attr('data-extra-data');
                    var data = {};
                    var value = '';
                    if (typeof extraDataAttr !== typeof undefined && extraDataAttr !== false && extraDataAttr != '') {
                        var data = JSON.parse(elementInGrid.attr('data-extra-data'));
                    }
                    if ($(this).attr('name') == 'javascript') {
                        value = window.btoa(unescape(encodeURIComponent($(this).val())));
                    }
                    else {
                        value = $(this).val();
                    }
                    data[$(this).attr('name')] = value;
                    elementInGrid.attr('data-extra-data', JSON.stringify(data));
                }
            });

            currentFieldInput.keyup(function () {
                $(this).change();
            })

        });
    }

    function checkifDisabled() {

        var nestedAttribute = $('.nested-attributes');
        nestedAttribute.each(function () {
            var childItemsLength = $(this).children("li").length;
            var childItemsDisabledLength = $(this).children("li.disabled").length;
            if (childItemsLength == childItemsDisabledLength) {
                $(this).parent().addClass('disabled');
            } else {
                $(this).parent().removeClass('disabled');
            }
        });

    }

    function addAttribute() {


        $('#attr-tabs').on('click', '.grid-stack-parent.enabled', function () {
            event.stopPropagation();
            var childElements = $(this).find('ul:first').children("li");

            var $t = $(this);

            if ($t.is('[data-json]')) {

                console.error('TO CHYBA DO WYWALNIA?');

            } else {
                $.each(childElements, function (i) {
                    var uniqueId;
                    if ($(this).attr('data-id')) {
                        uniqueId = ''
                    } else {
                        uniqueId = guid();
                    }
                    $(this).attr('data-unique-id', uniqueId);
                    $(this).addClass('disabled').removeClass('enabled');
                    var content = $(this).attr('data-html');
                    var clickedElementId = $(this).attr('data-attr-id');
                    var parentChildEl = $.parseHTML("<div data-unique-id=\"" + uniqueId + "\" data-id=\"" + clickedElementId + "\"><div class=\"grid-stack-item-content\"><i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>" + content + "</div><div/>");
                    grid.addWidget(parentChildEl, 0, 0, 4, 2, true);
                });
            }

        });

        $('#attr-tabs').on('click', '.grid-stack-item.child.enabled', function () {
            event.stopPropagation();
            var uniqueId;
            if ($(this).attr('data-id')) {
                uniqueId = ''
            } else {
                uniqueId = guid();
            }



            $(this).attr('data-unique-id', uniqueId);
            $(this).addClass('disabled').removeClass('enabled');
            var grid = $('.grid-stack').data('gridstack');
            var content = $(this).attr('data-html');
            var clickedElementId = $(this).attr('data-attr-id');
            var el = $.parseHTML("<div data-unique-id=\"" + uniqueId + "\" data-id=\"" + clickedElementId + "\"><div class=\"grid-stack-item-content\"><i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>" + content + "</div><div/>");
            grid.addWidget(el, 0, 0, 4, 2, false);

        });

        $('#attr-tabs #visual-elements-list').on('click', '.visual-element', function () {
            event.stopPropagation();
            var uniqueId;
            if ($(this).attr('data-id')) {
                uniqueId = ''
            } else {
                uniqueId = guid();
            }
            // $(this).attr('data-unique-id', uniqueId);
            var checkType = $(this).attr('data-visual-type');

            var grid = $('.grid-stack').data('gridstack');
            var content = $(this).attr('data-html');
            var widgetType = $(this).attr('data-visual-type');
            var lastRowPosition = parseInt($('#atlasGrid').attr('data-gs-current-height'));
            var el = $.parseHTML("<div data-unique-id=\"" + uniqueId + "\" data-widget=\"" + widgetType + "\" data-attr-id=\"\"><div class=\"grid-stack-item-content\"><i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>" + content + "</div><div/>");
            switch (checkType) {
                case 'divider_horizontal':
                    grid.addWidget(el, 0, lastRowPosition, 12, 1, false);
                    break;
                case 'divider_vertical':
                    grid.addWidget(el, 0, lastRowPosition, 1, 4, false);
                    break;
                case 'paragraph':
                    grid.addWidget(el, 0, lastRowPosition, 12, 1, false);
                    break;
                case 'header':
                    grid.addWidget(el, 0, lastRowPosition, 4, 1, false);
                    break;
                case 'image':
                    grid.addWidget(el, 0, lastRowPosition, 4, 5, false);
                    break;
                case 'button':
                    grid.addWidget(el, 0, lastRowPosition, 3, 2, false);
                    break;
                case 'map':
                    grid.addWidget(el, 0, lastRowPosition, 12, 7, false);
                    break;
                case 'email_preview':
                    grid.addWidget(el, 0, lastRowPosition, 12, 8, false);
                    break;
                case 'matrix':
                    grid.addWidget(el, 0, lastRowPosition, 12, 8, false);
                    break;
                default:
                    grid.addWidget(el, 0, lastRowPosition, 12, 2, false);
            }

        });

        var grid = $('.grid-stack').data('gridstack');


    }

    function fullHeightSidebar() {
        var attributeModal = $('#step-modal .portlet.light, #attribute-modal .portlet.light'),
            attributeModalHeaderHeight = $('#step-modal .modal-header, #attribute-modal .modal-header').innerHeight(),
            pageHeaderHeight = $('.page-header.navbar').innerHeight(),
            chatFooterHeight = $('#cometchat').innerHeight();
        attributeModal.height($(window).height() - ( pageHeaderHeight + attributeModalHeaderHeight + chatFooterHeight ) - 61)
        $stepModal.addClass('overflow-hidden');
    }

    function attributesSlimscroll() {
        var attributeContainer = $('#attr-tab-1');
        var windowHeight = $(window).height();
        var slimscrollHeight = windowHeight - 160;
        /*   attributeContainer.slimScroll({
         height: slimscrollHeight
         });*/
        /* $('#form-generator-box').css('max-height', slimscrollHeight)*/
    }

    function buttonHandle() {

        $stepModal.on('click', '#btn-step-form', function () {
            var $t = $(this);

            currentStep = $stepModal.attr('data-id');

            $formContent.html('');
            resetVariables();
            loadDesignForm();
        });

        $stepModal.on('click', '#btn-api-call', function () {
            // var $t = $(this);
            currentStep = $stepModal.attr('data-id');

            $formContent.html('');
            resetVariables();
            loadApiCallForm();
        });

    }

    function resetVariables() {
        $formGeneratorView = null;
        $attributesList = null;
    }

    function loadDesignForm(stepId) {

        stepId = stepId || currentStep;

        loadingFormStart();

        // var type = 'step';

        $.ajax({
            url: Routing.generate('admin_process_json_form', {type: generatorType, id: stepId}),
            type: "GET",
            dataType: 'json'
        }).done(function (response) {

            serializedData = response.formGeneratorJson;

            $.ajax({
                url: Routing.generate('admin_process_form_generator', {type: generatorType, id: stepId}),
                type: "GET"
            }).done(function (data) {

                onLoadForm(data);
                addAttribute();
                nestedAccordion();
                serializeThis();
                handleModalHeight();
                attributesSlimscroll();
                leftSideTabs();
                handleAccordionHeight();
                leftSideTabsHeightSet();
                deleteFormPlacementStyles();
                fullHeightSidebar();
                handleTranslationModule();

                if(isReadOnly){
                    $('#attr-tabs,#property-panel').remove();
                }

            }).fail(function (data) {

                console.error('Błąd przy wczytaniu kreatora. Msg: ' + data.statusText);

            }).always(function (data) {
                loadingFormStop();
            });

        }).fail(function (data) {
            console.error('Błąd przy wczytaniu kreatora. Msg: ' + data.statusText);
            loadingFormStop();
        });

    }


    function bindApiCallButtons() {

        var $collectionHolder = $('.calls .row');
        $collectionHolder.data('index', $collectionHolder.find(':input').length);
        var $addNewFormButton =  $('#html_form_api .add_new_form');

        $addNewFormButton.off('click').on('click', function(e) {
            e.preventDefault();
            addApiCallForm($collectionHolder);
        });

        $('.apicall-form .btn-remove').on('click', function () {
            var $apicall = $(this).parents('.apicall-form');
          var   $id = $apicall.data().id;
          var index = $('.calls .row').data('index');
            if($id) {
                swal({
                    title: "Czy jesteś pewien?",
                    text: "Usunięcie tego elementu wyrzuci go również z bazy",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function (willDelete) {
                    if (willDelete) {

                        $.ajax({
                            url: Routing.generate('admin_api_call_remove_api_call', {id: $id}),
                            type: "DELETE",
                            success: function () {

                                $('.calls .row').data('index', index  -1);
                                loadApiCallForm();

                            }
                        })

                    }
                });

            }else{
                $('.calls .row').data('index', index  -1);
                loadApiCallForm();
            }
        });
    }

    /**
     * Add new form
     * @param $collectionHolder
     */
    function addApiCallForm($collectionHolder) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');
        var newForm = prototype;
        newForm = newForm.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        var $newFormLi = $('<div class="col-md-6 apicall-form"></div>').append(newForm);
        $collectionHolder.append($newFormLi);
        bindApiCallButtons();
        // store it


    }

    /**
     * Reload form
     * @param stepId
     */
    function loadApiCallForm(stepId) {

        stepId = stepId || currentStep;

        loadingFormStart();

        // var type = 'step';

        $.ajax({
            url: Routing.generate('admin_api_call_editor', {stepId : stepId}),
            type: "GET"
        }).done(function (data) {
            $('#api-call-content').html(data).removeClass('hidden');
            bindApiCallButtons();



            $('#api-call-form').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                        method: 'POST',
                        url: $(this).attr('action'),
                        data: $(this).serialize(),
                        beforeSend: function () {
                            $spinner.trigger('spinner-on');
                        },
                        success: function (response) {

                            $('#api-call-form').html($(response).find('#api-call-form').html());
                            // codeMirrorHandleJson();
                            bindApiCallButtons();

                            if($.trim($('#api-call-content #form-errors').text()) === ""){
                                toastr.success("Pomyślnie zapisano konfigurację API dla kroku", "Zapisano", {timeOut: 5000});
                            }

                        },
                        complete: function(response){
                            $spinner.trigger('spinner-off');
                            bindApiCallButtons();
                        }
                    }
                );
                return false;
            });

        }).always(function (data) {
            loadingFormStop();
        });

    }


    /** Funkcja odpalana po pobraniu z serwera html'a do formularza */

    function onLoadForm(data) {

        $formContent.html(data);

        /** /TODO - do dokończenia potem */
        // _shortAction = initShortActions();

        disableUsedAttributes();

        /** Odpalenie funkcjonalności Generatora */
        handleEventsOnFormGenerator();
    }

    function disableUsedAttributes() {
        /*$('#attr-tabs').find('.grid-stack-item[data-used="1"]').addClass('disabled');*/
        $('#attr-tabs').find('.grid-stack-item[data-used="1"]').addClass('disabled');
        /*currentItemEdit();*/
    }

    function serializeThis() {

        var atlasGrid = $('#atlasGrid');
        var options = _gridStackAtlas.generatorOptions;
        if(isReadOnly){
            options = _gridStackAtlas.staticOptions;
        }
        atlasGrid.gridstack(options);

        var jsonItems = [];

        var grid = atlasGrid.data('gridstack');

        removeGridElement();

        function removeGridElement() {
            atlasGrid.on('click', '.fa-minus-circle', function () {
                var gridElement = $(this).parent().parent();
                var gridElementId = $(this).parent().parent().attr('data-attr-id');
                var parentElementLength = $('#attr-tabs .grid-stack-parent').length;
                var parentElementDisabledLength = $('#attr-tabs .grid-stack-parent.disabled').length;
                grid.removeWidget(gridElement);
                $('#attributes-list li').each(function () {
                    if (gridElementId == $(this).attr('data-attr-id')) {
                        $(this).removeClass('disabled');
                    }
                });
                checkifDisabled();
            });
        }

        atlasGrid.on('added', function (event, items) {
            for (var i = 0; i < items.length; i++) {
                _gridStackAtlas.onResizeTextArea(items[i].el);
            }
        });

        atlasGrid.on('gsresizestop', function (event, elem) {
            var $element = $(elem);

            if ($element.find('> .grid-stack-item-content > .custom-input-wrapper textarea').length) {
                _gridStackAtlas.onResizeTextArea($element);
            }

        });

        var tilePlacementGrid = $('.gridDrawTableWrapper');
        $('#form-generator-box').height($(window).height() - 100);

        atlasGrid.on('dragstart', function () {
            tilePlacementGrid.show();
            if ($('#atlasGrid').height() > $('#form-generator-box').height()) {
                tilePlacementGrid.height($('#atlasGrid').height() + 60);
            } else {
                tilePlacementGrid.height($('#form-generator-box').height());
            }
        });

        atlasGrid.on('dragstop', function () {
            tilePlacementGrid.hide();
        });

        atlasGrid.on('removed', function () {
            checkifDisabled();
            $('#attr-tabs .grid-stack-parent').first().removeClass('disabled');
        });

        function updateDataFromParent(event) {

            var $t = $(this);

            if ($t.closest('.grid-stack-parent[data-is-multi="1"]').length && $t[0] !== $t.closest('.grid-stack-parent[data-is-multi="1"]')[0]) return false;

            if (!$t.hasClass('disabled')) {

                event.stopPropagation();

                var uniqueId = guid();

                if ($t.is('[data-json]')) {

                    $t.addClass('disabled');

                    var html = _gridStackAtlas.createNestedGridStack({
                        json: $t.attr('data-json')
                    });

                    var newWidget = grid.addWidget($('<div data-attribute-path="' + $t.attr('data-path') + '" data-unique-id="' + uniqueId + '"  data-attr-id="' + $t.attr('data-attr-id') + '">' +
                            '<div class="grid-stack-item-content"></div><div/>'),
                        0, 0, 12, $t.attr('data-height'), true);

                    newWidget.find('.grid-stack-item-content').append('<i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>' + html + '</div>');

                    newWidget.find('.grid-stack').gridstack(_gridStackAtlas.staticOptions);

                    $.each(newWidget.find('.grid-stack-item'), function (i, ele) {
                        _gridStackAtlas.onResizeTextArea($(ele));
                    });


                }
                else if (parseInt($t.attr('data-is-multi')) === 1 && $t.hasClass('grid-stack-parent')) {

                    /** DOROBIĆ MULTI NA głębokich zagnieżdżeniach */

                    toastr.options.escapeHtml = false;

                    var url = Routing.generate('index', [], true) + '#redirectTo=' + Routing.generate('admin_attribute_chart', {'id': $t.attr('data-attr-id')}, true);

                    toastr.error("Attrybuty typu \"Multi\" muszą mieć zbudowany własny formularz. <p class='toast-link'><a target='_blank' href='" + url + "'>< Zbuduj teraz ></a></p>", "Brak zdefiniowanego szablonu", {timeOut: 7000});

                }
                else {

                    if ($t.hasClass('grid-stack-parent')) {

                        var childElements = $t.find('> ul:first').children("li[data-html]:not(.disabled)");
                        $t.addClass('disabled');
                        childElements.addClass('disabled');

                        _.each(childElements, function (item) {
                            var content = $(item).attr('data-html');
                            var elementId = $(item).attr('data-attr-id');
                            var parentChildEl = $("<div data-attribute-path=\"" + $(item).attr('data-path') + "\" data-unique-id=\"" + uniqueId + "\" data-attr-id=\"" + elementId + "\"><div class=\"grid-stack-item-content\"><i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>" + content + "</div><div/>");
                            grid.addWidget(parentChildEl, 0, 0, 4, 2, true);
                            checkifDisabled();
                        });

                    }
                    else {
                        var lastRowPosition = parseInt($('#atlasGrid').attr('data-gs-current-height'));
                        var lastElementX = parseInt($('#atlasGrid .grid-stack-item:last').attr('data-gs-x'));
                        var content = $(this).attr('data-html');
                        var clickedElementId = $(this).attr('data-attr-id');
                        var element = $.parseHTML("<div data-attribute-path=\"" + $t.attr('data-path') + "\" data-unique-id=\"" + uniqueId + "\" data-attr-id=\"" + clickedElementId + "\"><div class=\"grid-stack-item-content\"><i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>" + content + "</div><div/>");
                        if (content.indexOf("textarea") >= 0) {
                            grid.addWidget(element, 0, lastRowPosition, 4, 4, false);
                        } else if (content.indexOf("radio") >= 0) {
                            grid.addWidget(element, 0, lastRowPosition, 4, 2, false);
                        } else {
                            grid.addWidget(element, 0, lastRowPosition, 4, 2, false);
                        }
                        /*  grid.addWidget(element,0,0,4,2, true);*/
                        $(this).addClass('disabled');
                        checkifDisabled();
                    }
                }
            }

            return false;


        }

        function loadGrid() {
            grid.removeAll();
            var items = GridStackUI.Utils.sort(serializedData);

            var formGeneratorBox = $('#form-generator-box');

            _.each(items, function (node) {
                var attrId = node.attrId || null;
                var htmlString = node.html;
                var newWidget = null;

                if (htmlString) {
                    newWidget = grid.addWidget($('<div data-attribute-path="' + node.path + '" data-extra-data="" data-id="' + node.id + '" style="' + node.styles + '" data-widget="' + node.widget + '" data-attr-id="' + attrId + '"><div class="grid-stack-item-content"><i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>' + $('<div/>').html(node.html).text() + '</div><div/>'),
                        node.x, node.y, node.width, node.height);
                }
                else {

                    var html = _gridStackAtlas.createNestedGridStack(node);

                    newWidget = grid.addWidget($('<div data-attribute-path="' + node.path + '" class="nested-stack-item" data-extra-data="" data-id="' + node.id + '" style="' + node.styles + '" data-widget="' + node.widget + '" data-attr-id="' + attrId + '">' +
                            '<div class="grid-stack-item-content"><div/>'),
                        node.x, node.y, node.width, node.height);

                    newWidget.find('.grid-stack-item-content').append('<i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>' + html + '</div>');

                    newWidget.find('.grid-stack').gridstack(_gridStackAtlas.staticOptions);

                    $.each(newWidget.find('.grid-stack-item'), function (i, ele) {
                        _gridStackAtlas.onResizeTextArea($(ele));
                    });

                }

                if (newWidget !== null && node.customClass) {
                    newWidget.addClass(node.customClass);
                    newWidget.attr('data-custom-class', node.customClass);
                }

                var extraData = (node.extraData) ? JSON.stringify(node.extraData) : "";

                if (node.id && extraData) {
                    formGeneratorBox.find('.grid-stack-item[data-id="' + node.id + '"]').attr('data-extra-data', extraData);
                }

            });


            currentItemsCreate();
            stylingAvailableElements();
        }

        function saveGrid() {

            var $t = $(this);

            savedGridItems = [];

            serializedData = _.map($('#atlasGrid > .grid-stack-item:visible'), function (el) {
                el = $(el);
                var widgetType = el.attr('data-widget') || null;
                var attrId = parseInt(el.attr('data-attr-id')) || null;

                var attrPath = (el.attr('data-attribute-path')) ? el.attr('data-attribute-path') : '';
                // var attrPath = el.find('.form-control').length > 0 ? el.find('.form-control').attr('data-attribute-path') : '';
                var node = el.data('_gridstack_node');
                var uuid = guid();
                var uniqueId = el.attr('data-unique-id');
                // var pickedElements = 'input, textarea, select, p, h1, h2, h3, h4, label, div.vertical-divider, div.horizontal-divider, button';

                var pickedElements = getPickedElements(el);

                var gridItemElementStyle = {},
                    style = null;

                el.find(pickedElements).each(function (i, element) {

                    // gridItemElementStyle[($(element)[0].nodeName)] = $(element).attr('style') || "";

                    style = element.getAttribute('style');
                    gridItemElementStyle[element.nodeName.toLowerCase()] = (style) ? style.trim().replace(/(\r\n|\n|\r)/gm, "") : null;

                });

                //removing span with modal on save


                var savedGridElement = {
                    x: node.x,
                    y: node.y,
                    width: node.width,
                    height: node.height,
                    id: parseInt(el.attr('data-id')) || null,
                    "attrId": attrId,
                    "path": attrPath,
                    "label": el.find('.js-label-text').html() || "",
                    "value": el.find('input,textarea,select').val() || "",
                    "placeholder": el.find('input').attr('placeholder') || "",
                    // "styles": el.attr('style') || "", /* StyledElements || "" DYLESIU HERE!!! */
                    "styles": gridItemElementStyle || null, /* StyledElements || "" DYLESIU HERE!!! */
                    "widget": widgetType,
                    "customClass": el.attr('data-custom-class') || "",
                    "extraData": el.attr('data-extra-data') || "",
                    "uniqueId": uniqueId
                };

                savedGridItems.push(savedGridElement);

            });
            saveGeneratedForm(savedGridItems, $('#atlasGrid').attr('data-gs-current-height'), $t);
            return false;
        }

        function clearGrid() {
            grid.removeAll();
            return false;
        }

        $('#save-grid').on('click', function () {

            var $t = $(this),
                _this = this;

            $t.addClass('saving');

            //  zapisanie warunków kontrolki, jeżeli formularz warunków aktywny
            var $form = $('#attrAccordion .dynamic-condition-form form');

            if($form.length) {
                saveDynamicConditionControl($form).done(function (response) {

                    if(response.success) {
                        saveGrid.call(_this);
                    }
                    else {
                        $t.removeClass('saving');
                    }

                }).fail(function () {
                    $t.removeClass('saving');
                });
            }
            else {
                saveGrid.call(_this);
            }

        });

        /* $('#load-grid').click(loadGrid);*/
        $('#clear-grid').click(clearGrid);
        $('#attr-tabs').find('.grid-stack-item').click(updateDataFromParent);

        loadGrid();

    }

    function handleEventsOnFormGenerator() {

        $formGeneratorView = $formContent.find('#form-generator-view');

        if ($formGeneratorView === null || $formGeneratorView.length === 0) {
            console.error('Brak formularza, lub id: #form-generator-view');
            return false;
        }

        handleMode($formGeneratorView);

        /** Rozbiłem na 2, ale można dać w 1 */

        handleAttributesInList();
        handleAttributesInForm();
        handleAttributesConfigurator();
        handleTranslationModule();

    }

    //Every bind every key into the span with opening
    function handleTranslationModule($formgenerator){

        var regexForTranslation = /{-(\S*)-}/gm;
        var arr = $('.js-label-text').each(function ( index) {
            $(this).html($(this).html().replace(regexForTranslation, function(x){
                return '<span class="openTranslationModal">' + x + '</span>'
            }))
        })

    }

    function handleMode($view) {

        $view.find('.change-view-mode-generator a').on('click', function (e) {

            e.preventDefault();

            var $t = $(this);

            if($t.hasClass('hide-panel')) {
                $view.addClass('light-mode');
            }
            else if($t.hasClass('show-panel')) {
                $view.removeClass('light-mode');
            }

        });

    }

    function handleAttributesConfigurator() {
        var $currentConfigField = $('.current-field-config-wrapper');

        $currentConfigField.on('submit', '.dynamic-condition-form form', function (e) {

            e.preventDefault();
            saveDynamicConditionControl($(this));

        })
    }

    function saveDynamicConditionControl($form) {

        var btn = $form.find('button'),
            l = Ladda.create(btn[0]);

        l.start();

        return $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),

        success: function (response) {

                if(response.success) {
                    toastr.success("Pomyślnie zapisano warunek", "Zapisano", {timeOut: 5000});
                }
                else {
                    toastr.error("", "Błąd podczas zapisywania warunku", {timeOut: 5000});
                }

                $form.closest('.form-wrapper').html(response.form);

            },
            error: function () {

                toastr.error("", "Błąd podczas zapisywania warunku", {timeOut: 5000});

            },
            complete: function () {
                l.stop();
            }
        });

    }
    
    function initShortActions() {

        var $grid = $('#atlasGrid'),
            $wrapper = $('#short-actions'),
            $buttons = $wrapper.find('button'),
            _isActive = false,
            _modeName = null;

        if (generatorType === "attribute") {

            $wrapper.hide();

        }
        else {

            $buttons.on('click', function () {

                var $t = $(this);

                if($t.hasClass('active')) {

                    $('body').off(_bind_esc);
                    _disableMode();
                    return false;
                }

                $buttons.removeClass('active');
                $t.addClass('active');
                _modeName = $t.data('action');

                _enableMode();

            });

            function _getSingleGridStackItem() {
                return $grid.find('.grid-stack-item').filter(function() {
                    return $(this).find('.grid-stack-item').length === 0;
                })
            }

            function _enableMode() {
                _ON_OFF_MODE(true);
                _isActive = true;
                $('body').off(_bind_esc);
                _bind_esc();
            }

            function _disableMode() {
                $buttons.removeClass('active');
                _ON_OFF_MODE(false);
                _modeName = null;
                _isActive = false;
            }

            function _ON_OFF_MODE(ON) {

                switch (_modeName) {
                    case SA_MODE_HIDDEN : {

                        if(ON) _mode_SA_HIDDEN_ON();
                        else _mode_SA_HIDDEN_OFF();

                        break;
                    }
                    case SA_MODE_VISIBLE : {

                        break;
                    }
                }
            }

            function _bind_esc() {
                $('body').on('key', function (e) {
                    if (e.keyCode === 27) {
                        _disableMode();
                        setTimeout(function () {
                            $('body').off(_bind_esc);
                        }, 10);
                    }
                })
            }

            function _() {

            }
            
            /** DEFINITION MODES */

            /** MODE HIDDEN */

            function _mode_SA_HIDDEN_ON() {

                _getSingleGridStackItem()
                    .on('mouseenter', _mode_hidden_mouseenter)
                    .on('mouseleave', _mode_hidden_mouseout)
                    .on('click', _mode_hidden_click)
            }

            function _mode_SA_HIDDEN_OFF() {

                _getSingleGridStackItem()
                    .off('mouseenter', _mode_hidden_mouseenter)
                    .off('mouseleave', _mode_hidden_mouseout)
                    .off('click', _mode_hidden_click)
            }

            function _mode_hidden_mouseenter(e) {
                // e.preventDefault();
                this.classList.add('sa-hover-action');
            }

            function _mode_hidden_mouseout(e) {
                // e.preventDefault();
                this.classList.remove('sa-hover-action');
            }

            function _mode_hidden_click(e) {

                e.preventDefault();

                var controlId = parseInt(this.getAttribute('data-id'));

                if(controlId) {
                    var urlConditionForm = Routing.generate('admin_attribute_condition_visible', {step: currentStep, formControlId: controlId, isVisible: "false"});

                    $.ajax({
                        type: "POST",
                        url: urlConditionForm,
                        success: function () {
                            toastr.success("1", "Zapisano", {timeOut: 5000});
                        },
                        error: function () {
                            toastr.error("", "Błąd podczas zapisywania warunku", {timeOut: 5000});
                        }
                    });
                    
                    $('body').append('<div class="fixed-icon"><span class="icon"><i class="fa fa-eye-slash"></i></span></div>');

                    $('.fixed-icon').css({
                        'top': e.clientY,
                        'left': e.clientX
                    });

                    setTimeout(function () {
                        $('.fixed-icon').animate({
                            'font-size': '50px',
                            'opacity': 0
                        }, 500, function () {
                            $(this).remove();
                        });
                    }, 10);

                }
                else {
                    toastr.error("", "Najpierw zapisz formularz!", {timeOut: 3000});
                }

            }

            /** END MODE HIDDEN */

            /** END */

        }

        return {
            isActive: function () {
                return _isActive;
            }
        }

    }


    function handleAttributesInList() {

        $attributesList = $formGeneratorView.find('#attributes-list');

    }

    function handleAttributesInForm() {

        var formGeneratorBox = $formGeneratorView.find('#form-generator-box');

    }

    function sendTranslationForm(e) {
        e.preventDefault();


    }


    function bindKeyButtons() {

        var $collectionHolder = $('.calls .row');
        $collectionHolder.data('index', $collectionHolder.find(':input').length);
        var $addNewFormButton = $('#dynamic-translations-html .add_new_form');

        $('#dynamic-translations-form').off('submit').on('submit', function(e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {
                    toastr.success( '', 'Zapisano.', 3000);
                    $('#ajaxTextFormTranslationModal').modal('hide');
                    // show response from the php script.
                }
            });
        });


        $addNewFormButton.off('click').on('click', function (e) {
            e.preventDefault();
            addKeyForm($collectionHolder);
        });



        /**
         * Delete key binding
         * */
        $('body').off('click').on('click', '#dynamic-translations-form .btn-remove', function (e) {
            e.preventDefault();
            var $key = $(this).parents('.key-form');
            var $id = $key.data().id;
            var index = $('.calls .row').data('index');

            if ($id) {
                $.ajax({
                    url: Routing.generate('deleteDynamicTranslationKey', {id: $id}),
                    type: "DELETE",
                    success: function (data) {

                        $('.calls .row').data('index', index - 1);
                        // loadKeyForm();
                        toastr.success( '', 'Usunięto.', 3000);
                        $('#atlasGrid .openTranslationModal').trigger('click');
                    }
                })
            }
        });
    }

    /**
     * Add new form
     * @param $collectionHolder
     */
    function addKeyForm($collectionHolder) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');
        var newForm = prototype;
        newForm = newForm.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        var $newFormLi = $('<div class="col-md-6 key-form"></div>').append(newForm);
        $collectionHolder.append($newFormLi);
        bindKeyButtons();
        // store it

    }

    /** Uruchamianie / zatrzymywanie loadera */

    function loadingFormStart() {
        $spinner.trigger('spinner-on');
    }

    function loadingFormStop() {
        $spinner.trigger('spinner-off');
    }

}(window.jQuery, window, document));

var SA_MODE_HIDDEN = 'SA_HIDDEN';
var SA_MODE_VISIBLE = 'SA_VISIBLE';