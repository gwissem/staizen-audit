var _gridStackAtlas = function () {

    var verticalMargin = 4,
        cellHeight = 30,
        copying = false,
        multiTemplates = null,
        globalCountMulti = 0;

    var _generatorOptions = {
        cellHeight: cellHeight,
        verticalMargin: verticalMargin,
        float: false,
        animate: false,
        resizable: {
            handles: 'se'
        },
        draggable: {
            scroll: true
        }
    };

    var _staticOptions = {
        cellHeight: cellHeight,
        verticalMargin: verticalMargin,
        disableDrag: true,
        disableResize: true,
        staticGrid: true,
        float: true
    };

    var prototypeNestedStackItem = '<div __DATA_MULTI__ __IS_VISIBLE__ data-attr-val-id="__ATTR_VAL_ID__" data-readonly-multi="false" class="grid-stack-item nested-stack-item __CUSTOM_CLASS__" data-path="__PATH__" data-id="__ID__" data-gs-x="__X__" data-gs-y="__Y__" data-gs-width="__W__" data-gs-height="__H__">' +
            '<div class="grid-stack-item-content">__HTML__</div></div>',
        emptyHTML = '<div>EMPTY HTML</div>';

    function _setMultiTemplates(multiTemp) {
        multiTemplates = multiTemp;
    }

    function _onResizeTextArea($element) {

        var textarea = $element.find('textarea');

        if(textarea.length) {

            var elementHeight = $element.attr('data-gs-height');

            if(elementHeight === "2") {
                $element.find('textarea').height(18);
            }
            else {
                var heightUnit = (cellHeight + verticalMargin); // withMargin
                var finalTextAreaHeight = ((elementHeight * heightUnit) - 50);
                $element.find('textarea').height(finalTextAreaHeight);
            }

        }

    }

    var _buttonsForMultiControl = '<div class="wrapper-multi-btn">' +
        '<div><div class="btn-multi-up"><i class="fa fa-arrow-up" aria-hidden="true"></i></div>' +
        '<div class="btn-multi-down"><i class="fa fa-arrow-down" aria-hidden="true"></i></div></div></div>' +
        '<div class="wrapper-multi-btn-right">' +
        '<div class="btn-multi-add"><i class="fa fa fa-plus-circle" aria-hidden="true"></i></div>' +
        '<div class="btn-multi-remove" data-title="Jesteś pewny?" data-placement="left" data-singleton="true" data-popout="true"><i class="fa fa-minus-circle2" aria-hidden="true"></i></div>' +
        '</div>';

    function _createNestedGridStack(node, multiValues) {

        var multiValuesChildren = null;

        if (multiValues === null && node.multi) {
            if (node.multi.multiValues[node.attribute_value_id]) {
                multiValuesChildren = node.multi.multiValues[node.attribute_value_id].children;
            }
        }
        else if (multiValues) {
            multiValuesChildren = multiValues.children;
        }

        // console.groupCollapsed('_createNestedGridStack');
        // console.log(node);
        // console.log(multiValues);
        // console.log(multiValuesChildren);
        // console.groupEnd();

        var content = (multiValuesChildren !== null) ? _buttonsForMultiControl : '';

        var $childrenGridStack = $('<div data-gs-locked="true" class="grid-stack">'+ content +'</div>');

        if (node.children) {

            $.each(node.children, function (i, ele) {
                $childrenGridStack.append(generateNestedStackItem(ele, multiValuesChildren));
                appendToMultiTemplates(ele, node.id);
            })

        }
        else if (node.json) {

            var children = JSON.parse(node.json);

            $.each(children, function (i, ele) {
                $childrenGridStack.append(generateNestedStackItem(ele, multiValuesChildren));
                appendToMultiTemplates(ele, node.id);
            })

        }
        else {
            $childrenGridStack.append('<div>NO ELEMENTS!</div>')
        }

        return ($childrenGridStack[0]) ? $childrenGridStack[0].outerHTML : '';
    }

    function appendToMultiTemplates(node, parentId) {

        if (node.multi && multiTemplates !== null) {

            // console.groupCollapsed('appendToMulti');
            // console.log(copying);
            // console.log(node, parentId);
            // console.groupEnd();

            multiTemplates[node.id] = {
                'node': node,
                'amount': 1,
                'data': node.multi,
                'parentId': parentId
            };
        }

    }

    function generateNestedStackItem(node, multiValuesChildren) {

        multiValuesChildren = multiValuesChildren || null;

        var newStackItem = prototypeNestedStackItem;
        newStackItem = newStackItem.replace('__X__', ((node.x) ? node.x : 0).toString())
            .replace('__Y__', ((node.y) ? node.y : 0).toString())
            .replace('__W__', ((node.width) ? node.width : 0).toString())
            .replace('__H__', ((node.height) ? node.height : 0).toString())
            .replace('__PATH__', ((node.path) ? node.path : ''))
            .replace('__ID__', ((node.id) ? node.id : ''))
            .replace('__ATTR_VAL_ID__', ((node.attribute_value_id) ? node.attribute_value_id : ''))
            .replace('__CUSTOM_CLASS__', ((node.customClass) ? node.customClass : ''));

        if (node.multi && !copying) {
            newStackItem = newStackItem.replace('__DATA_MULTI__', 'data-multi');

                // .replace('__ADD_MULTI__', ('<div class="btn-multi-add" data-node-id="' + node.id + '"></div>'));
        }
        else {
            newStackItem = newStackItem.replace('__DATA_MULTI__', '').replace('__ADD_MULTI__', '');
        }

        if(node.multi) {
            if(node.controlType === 4) {
                newStackItem = newStackItem.replace('data-readonly-multi="false"', 'data-readonly-multi="true"')
            }
        }

        if (typeof node !== "undefined" && typeof node.isVisible !== "undefined") {
            var visible = ('data-is-visible="' + ((node.isVisible == true) ? 'true' : 'false') + '"' );
            newStackItem = newStackItem.replace('__IS_VISIBLE__', visible);
        }
        else {
            newStackItem = newStackItem.replace('__IS_VISIBLE__', '');
        }

        var html = '';

        if (multiValuesChildren && node.html) {

            // console.log(node.html);

            var $html = $(decodeEntities(node.html));

            // console.log($html);

            var inputWithValue = $html.find('[data-attribute-value-id]');

            if (inputWithValue.length) {

                if (!multiValuesChildren[node.attrId]) {
                    console.error('Błąd! Brakuje child\'a w tablicy: ' + node.attrId +". Zmieniono strukture formularza?");
                }
                else {
                    var child = multiValuesChildren[node.attrId],
                        prefix = (globalCountMulti !== null) ? (globalCountMulti + '-') : '';

                    inputWithValue.attr('data-attribute-value-id', child.attribute_value_id);
                    inputWithValue.attr('data-attribute-path', child.attribute_path);
                    inputWithValue.attr('parent-attribute-value-id', child.parent_attribute_value_id);
                    inputWithValue.attr('data-group-process-id', child.group_process_id);
                    inputWithValue.attr('data-step-id', child.step_id);
                    inputWithValue.attr('auto-field', child.isAuto);

                    /** Check if is control type Yes-No */

                    var id = prefix + child.attribute_path.replace(/,/g, "-");

                    if(inputWithValue.length > 1 && inputWithValue[0].type === "radio") {

                        var $ele = null;

                        $.each(inputWithValue, function (i, ele) {

                            $ele = $(ele);

                            if(child.value === ele.value) {
                                $ele.attr('checked', 'checked');
                            }

                            $ele.attr('id', i + '-' + id).attr('name', 'radio-' + id);
                            $ele.next().attr('for', i + '-' + id);
                        })

                    }
                    else {

                        inputWithValue.attr('id', id);

                    }

                    // if(child.value !== null) {
                    if(typeof child.value !== "undefined") {
                        if(inputWithValue[0].tagName === "TEXTAREA") {

                            if(child.value === null) {
                                inputWithValue.text("");
                            }
                            else {
                                inputWithValue.text(child.value);
                            }

                        }
                        else if(inputWithValue[0].tagName === "SELECT") {
                            var option = inputWithValue.find('option[value="' + child.value + '"]');
                            if(option.length) option.attr('selected', 'selected');
                        }
                        else if(inputWithValue[0].type !== "radio") {

                            if(child.value === null) {
                                inputWithValue.attr('value', "");
                            }
                            else {
                                inputWithValue.attr('value', child.value);
                            }

                        }
                    }

                }
            }

            $html.each(function (i, ele) {
                html += ele.outerHTML;
            });

        }
        else if (node.html) {
            html = decodeEntities(node.html);
        }
        else if (node.children) {
            html = _createNestedGridStack(node, (multiValuesChildren) ? multiValuesChildren[node.attrId] : null);
        }

        newStackItem = newStackItem.replace('__HTML__', ((html) ? html : emptyHTML));

        return newStackItem;

    }

    function _setLoopIndex(index) {
        globalCountMulti = index;
    }

    return {
        generatorOptions: _generatorOptions,
        staticOptions: _staticOptions,
        onResizeTextArea: _onResizeTextArea,
        createNestedGridStack: _createNestedGridStack,
        setMultiTemplates: _setMultiTemplates,
        setLoopIndex: _setLoopIndex,
        setCoping: function (status) {
            copying = status;
        }
    }

}();