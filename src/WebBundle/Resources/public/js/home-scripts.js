// THIS SCRIPT IS LOAD ONLY IN HEADER ATLAS

var atlasTabChannel;

(function ($, window, document) {

    var $atlasIframe,
        $body;

    var toolSummaryObj = {
        url: '',
        source: null,
        callback: function () {

        }
    };

    atlasTabChannel = duel.channel('atlas_home');

    // atlasTabChannel.on('focusWindow', function () {
    //
    //     var notification = new Notification('Zakładka z telefonią', {
    //         icon: 'https://atlas.starter24.pl/favicon.png',
    //         body: "Kliknij żeby przejść do zakładki z telefonią!"
    //     });
    //
    //     notification.onclick = function () {
    //         window.focus();
    //         notification.close();
    //     };
    //
    // });

    atlasTabChannel.on('conversationEnded', function (obj) {

        sendToIframe({
            data: obj,
            type: 'conversationEnded'
        });

    });

    $(function () {

        $atlasIframe = $('#atlas-iframe');
        $body = $('body');

        disableHistoryButton();
        checkHash();
        tabsHandle();
        postMessageHandle();
        communicationWithIframe();
        toolSummaryBarHandle();
        focusWindowHandle();
        /** nasłuchuje na focus window i odświeża summaries */
        reportProblemHandle();
        setNavTooltipDisabled();
        expandMobileSidebar();
        reloadSiteAtRelogin();
        detectIos();
        toggleMenuOverlay();
        new WOW().init();

        $(window).on('beforeunload', onBeforeUnload);

        processSearchHandle();

        publicFunctions();

    });

    function publicFunctions() {

        window.getCurrentCase = function () {
            return $atlasIframe[0].contentWindow.currentProcess;
        };

    }

    function toggleMenuOverlay() {
        $('.menu-toggler').click(function () {
            $('body').toggleClass('scrollDisabled');
            $('html').toggleClass('iosFix');
        });

    }

    function detectIos() {
        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            $('#atlas-iframe').attr("scrolling", "no");
        }
    }

    function reloadSiteAtRelogin() {
        $('.js-relogin').click(function () {
            reloadSiteWithDelay(1000);
        })
    }

    function expandMobileSidebar() {
        $('.sidebar-mobile-toggler').click(function () {
            $('.page-top').toggleClass('mobile-header-expanded');
        });

    }

    function setNavTooltipDisabled() {
        var customTooltipWrappers = $(".custom-tooltip-wrapper, .custom-chart-tooltip-wrapper");
        var checkClosedTooltipCookie = Cookies.get('tooltipClosed');
        if (!checkClosedTooltipCookie) {
            customTooltipWrappers.click(function () {
                $(this).fadeOut();
                Cookies.set('tooltipClosed', true, {expires: 30});
            });
        } else {
            customTooltipWrappers.remove();
        }
    }

    function tryGetScreen() {

        if(!atlasChromeExtension.getPort()) {
            return false;
        }

        var dfd = jQuery.Deferred(),
            timeout = null;

        function _onMessage(msg) {
            if(msg.type === "captureScreen") {
                if(timeout !== null) {
                    clearTimeout(timeout);
                    timeout = null;
                    atlasChromeExtension.getPort().onMessage.removeListener(_onMessage);
                    dfd.resolve(msg.imgSrc);
                }
            }
        }

        atlasChromeExtension.getPort().onMessage.addListener(_onMessage);

        timeout = setTimeout(function () {
            dfd.resolve(null);
        }, 3000);

        atlasChromeExtension.getPort().postMessage({action: "capture-screen"});

        return dfd;

    }

    function sendReportProblem($form, submit) {
        $.ajax({
            url: $form.attr('action'),
            type: "POST",
            data: $form.serialize()
        }).success(function (data) {
            if (data === 'OK') {

                $('#report-problem-response').removeClass('hide');
                $form[0].reset();

                setTimeout(function () {
                    $("#report-problem-modal").modal('hide');
                }, 5000);

            }
        }).always(function () {
            submit.stop();
        });
    }

    function reportProblemHandle() {

        var $problemModal = $("#report-problem-modal"),
            $atlasIframe = $('#atlas-iframe'),
            $form = $('#report-problem-form');

        $body.on('click', '#report-problem', function () {
            $('#report-problem-response').addClass('hide');
            $form.find('input[name=url]').val(window.location.href);
            $form.find('input[name=screenResolution]').val($(window).width() + 'x' + $(window).height());

            var currentProcess = $atlasIframe[0].contentWindow.currentProcess;

            if(currentProcess) {
                $form.find('input[name=case_info]').val('Nr kroku: ' + currentProcess.stepId + ', Nr sprawy: ' + currentProcess.rootId + ", id: " + currentProcess.id);
            }

            $problemModal.modal();
            return false;
        });

        $body.on('click', '#report-problem-modal .cancel', function () {
            $problemModal.modal('hide');
            return false;
        });

        $form.on('submit', function (e) {
            e.preventDefault();
            var _self = this;

            var l = Ladda.create($form.find('button[type=submit]')[0]);
            l.start();

            setTimeout(function () {

                if(!atlasChromeExtension.getPort()) {
                    sendReportProblem($(_self), l);
                }
                else {

                    $problemModal.modal('hide');

                    setTimeout(function () {

                        var captureScreen = tryGetScreen();
                        captureScreen.then(function (image) {

                            $problemModal.modal();
                            $form.find('input[name=screenShootFileData]').val(image);
                            sendReportProblem($(_self), l);

                        });

                    }, 500);
                }
            }, 100);

            return false;
        });

    }

    function toolSummaryBarHandle() {

        runIntervalSummaryTools();

        var modalRemovalSummary = $('#removal-modal-tool-summary');

        modalRemovalSummary.find('#confirm-modal-ajax').click(function () {

            var l = Ladda.create($(this)[0]);
            l.start();

            $.ajax({
                url: toolSummaryObj.url,
                type: "POST"
            }).done(function (data) {
                toolSummaryObj.callback(data);
            }).fail(function (data) {
                console.error('Błąd. Msg: ' + data.statusText);
            }).always(function () {
                toolSummaryObj.url = '';
                toolSummaryObj.callback = function () {
                };
                l.stop();
                modalRemovalSummary.modal('hide');
                renderSummaryTools();
            });
        });

        modalRemovalSummary.find('#cancel-modal-ajax').click(function () {
            modalRemovalSummary.modal('hide');
            toolSummaryObj.url = '';
            toolSummaryObj.callback = function () {
            };
        });

        $('#summary-tools').on('click', '.remove-tool-summary', function (e) {
            e.preventDefault();
            var $t = $(this);

            toolSummaryObj.url = $t.attr('data-url');
            toolSummaryObj.callback = function (data) {
                if (data.success) {
                    $t.closest('.dropdown').remove();
                }
            };

            modalRemovalSummary.modal();

        }).on('click', '.summary-link', function (e) {
            // e.preventDefault();
            // forceReload($(this));
        });
    }

    function focusWindowHandle() {

        globalVar.focusWindow = document.hasFocus();

        $(window).on('focus', function () {
            globalVar.focusWindow = true;
            renderSummaryTools();
        }).on('blur', function () {
            globalVar.focusWindow = false;
        });
    }

    function renderSummaryTools(force) {

        force = force || false;
        if(globalVar.timePassed() || force) {
            $.ajax({
                url: Routing.generate('tool_summary_get_view'),
                type: "GET"
            }).done(function (data) {


            if(data.status === true ) {
                $('#summary-tools').html(data.html).find('.tooltips').tooltip();
            }
            else {
                $('#summary-tools').html('');
            }

        }).fail(function (data) {
            console.error('Błąd. Msg: ' + data.statusText);
        }).always(function () {
            headerSummaryToolsWidthHandle();
        });
}
    }

    function runIntervalSummaryTools() {
        if (typeof inIframe === 'function') {
            if (!inIframe()) {

                setInterval(function () {
                    if (globalVar.focusWindow || (document.activeElement == $atlasIframe[0])) {
                        renderSummaryTools();
                    }
                }, 60000);

                renderSummaryTools(true);
            }
        }
    }

    function onBeforeUnload(e) {
        if (Cookies.getJSON('dev_mode') != true) {
            e.preventDefault();
            return "Unloading this page may lose data.";
        }
    }

    function disableHistoryButton() {
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        }
    }

    function receiveWindowMessage(event) {
        // var origin = event.origin || event.originalEvent.origin;
        //
        // if ((origin + '/') !== Routing.generate('index', [], true).replace('app_dev.php/', ''))
        //     return;

        execMessage(event.data);
    }

    function postMessageHandle() {

        window.addEventListener("message", receiveWindowMessage, false);

    }

    function reloadSiteWithDelay(delay) {
        var delayTime = delay || 1000;
        setTimeout(function () {
            $(window).off('beforeunload', onBeforeUnload);
            window.location.reload();
        }, delayTime);
    }

    function foldMobileNavbar() {
        if ($('.page-top').hasClass('mobile-header-expanded')) {
            $('.page-top').removeClass('mobile-header-expanded');
        }
    }

    function execMessage(message) {

        // TODO - zmienić na switch...

        if(message.type === "onGetTask") {

            if(atlasChromeExtension && atlasChromeExtension.getPort()) {

                atlasChromeExtension.getPort().postMessage({
                    module: 'timeWorkManager',
                    action: 'set-current-case',
                    host: window.location.host,
                    data: window.getCurrentCase()
                });

            }

        }
        else if (message.type === 'force-redirect') {
            reloadSiteWithDelay();
        }
        else if (message.type === "refresh-summary") {
            renderSummaryTools();
        }
        else if(message.type === "dashboard-viewed") {
            $body.find('.sidebar-toggler').addClass('invisible');
        }
        else if(message.type === "dashboard-not-viewed") {
            $body.find('.sidebar-toggler').removeClass('invisible');
        }
        else if(message.type === "error-displayed") {
            $body.find('.page-header.navbar.navbar-fixed-top').hide();
            $body.find('.atlas-iframe-wrapper.enabled-chat').css('padding-bottom', '0');
        }
        else if (message.type === "error-return") {
            reloadSiteWithDelay();
        }
        else if (message.type === 'title-change') {

            foldMobileNavbar();

            if (message.data) {
                document.title = message.data;
            }

        }
        else if (message.type === "on-focus") {
            renderSummaryTools();
        }
        else if(message.type === "reserve-task") {
            $('.notification-id-' + message.id).remove();
        }

    }

    function communicationWithIframe() {
        $('.menu-toggler').on('click', function () {

            sendToIframe({
                type: 'sidebar-toggle',
                data: !($('body').hasClass('page-sidebar-closed'))
            });

        });

        $('.sidebar-mobile-toggler').on('click', function () {

            sendToIframe({
                type: 'mobile-sidebar-toggle'
            });

        })
    }

    function sendToIframe(message) {
        $atlasIframe[0].contentWindow.postMessage(message, Routing.generate('index', [], true));
    }

    function changeSrc(src) {
        $atlasIframe.attr('src', src);
    }

    function headerSummaryToolsWidthHandle() {
        var headerElements = $('.page-logo').outerWidth() + $('.page-actions').outerWidth() + $('.top-menu .navbar-nav').outerWidth() + $('#summary-tools').outerWidth();
        var windowWidth = $(window).width();
        var availableWidth = windowWidth - ($('.page-logo').outerWidth() + $('.page-actions').outerWidth() + $('.top-menu .navbar-nav').outerWidth() + 70);
        if (headerElements > windowWidth && windowWidth > 768) {
            $('#summary-tools').addClass('shrinked').css({
                "width": availableWidth,
                "overflow-y": "hidden",
                "overflow-x": "auto",
                /* "height": "40px",*/
                "white-space": "nowrap"
            }).find('li').addClass('initialFloat');
        }
    }

    function tabsHandle() {

        $('#user-top-menu a, .brand, .top-menu > .nav ul a').not('#report-problem').on('click', function (e) {

            e.preventDefault();
            if($(this).hasClass('ignore-tabs-handle')) return;
            changeSrc($(this).attr('href'))

        }).on('auxclick', function (e) {

            if(e.which === 2) {
                e.preventDefault();

                var url = Routing.generate('index', [], true) + '#redirectTo=' + window.location.origin + $(this).attr('href');

                window.open(url);
                window.focus()

            }

        });

        $('#summary-tools').on('click', '.summary-link', function (e) {
            e.preventDefault();
            sendToIframe({
                type: 'force-reload',
                link: $(this).attr('href')
            });
        });
    }

    function checkHash() {
        if (window.location.hash) {

            if ((window.location.hash.indexOf('redirectTo=') !== -1) && (window.location.hash.indexOf(window.location.host) !== -1)) {

                changeSrc(window.location.hash.replace('#redirectTo=', ''));

                window.location.hash = '';

                clearHomeUrl();

            }
        }
    }

    function processSearchHandle() {
        $('#process-search-form .fa.fa-search').on('click', function(){
            $('#process-search-form').trigger('submit');
        });

        $('#process-search-form').on('submit', function (e) {
            e.preventDefault();
            var phrase = $(this).find('input[name="value"]').val();
            $.ajax({
                url: Routing.generate('case_ajax_process_search'),
                type: "POST",
                data: {
                    'value': phrase
                },
                success: function (result) {
                    if(result.ids == 1){
                        sendToIframe({
                            type: 'task-overview',
                            id: result.ids[0].id
                        });
                    }
                    else if(result.toolId){
                        var height = $(window).height() - $('#process-search-modal .modal-dialog').outerHeight() - 250;
                        var src = Routing.generate('tool_show_embed', {
                            'toolId' : result.toolId,
                            'search' : phrase,
                            'vin' : result.filters.vin ? result.filters.vin : '',
                            'registrationNumber' : result.filters.registrationNumber ? result.filters.registrationNumber : '',
                            'city' : result.filters.city ? result.filters.city : '',
                            'caseNumber' : result.filters.caseNumber ? result.filters.caseNumber : ''
                        });
                        $('#process-search-modal .modal-body').html('<iframe id="embed-iframe" src="'+src+'" frameborder="0" height="'+height+'" width="100%"></iframe>');
                        $('#process-search-modal').addClass('overlay-visible').modal('show');
                        $('#embed-iframe').on('load', function(){
                            var toolBody = $('#embed-iframe')[0].contentDocument.body;
                            $(toolBody).find('#tool-table').on('click', 'td', function(){
                                var groupProcessId = $(this).siblings('[data-column="id"]').attr('data-value');
                                if(typeof groupProcessId != "undefined" && groupProcessId != "") {
                                    sendToIframe({
                                        type: 'task-overview',
                                        id: groupProcessId
                                    });
                                    $('#process-search-modal').removeClass('overlay-visible').modal('hide');
                                }
                                else{
                                    return false;
                                }
                            });
                        })

                    }
                }
            });

        });
    }

}(window.jQuery, window, document));