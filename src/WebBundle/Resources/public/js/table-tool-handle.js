var draggableTable = null;

function handleInitTableInfinityScroll($box, opts, callback) {

    var $table = $box.find('> table'),
        isLoading = false,
        isEnd = false,
        currentPage = 2;
    /** =2 , ponieważ pierwsza strona jest załadowana od razu po załadowaniu strony */

    opts = $.extend({
        offset: 350,
        spinner: $('#tool-spinner')[0],
        url: '',
        perPage: 20,
        data: {}
    }, opts);

    function getResults() {

        if (isLoading) return false;

        if (( $table.height() - $box.scrollTop() - $box.height() ) < opts.offset) {

            isLoading = true;
            opts.spinner.style.display = 'block';

            $.ajax({
                url: opts.url,
                data: opts.data,
                type: 'GET',
                headers: {'Pagination-Page': currentPage},
                success: function (response) {

                    if (response.loadedRowCount === 0 || response.loadedRowCount < opts.perPage) {
                        isEnd = true;
                    }

                    currentPage++;

                    if (typeof callback === "function") {
                        callback(response);
                    }

                    setTimeout(function () {
                        isLoading = false;
                        _checkScroll(response.loadedRowCount);
                    }, 10);

                },
                error: function (error) {
                    handleAjaxResponseError(error);
                },
                complete: function () {
                    opts.spinner.style.display = 'none';
                }
            });

        }

    }

    function onScroll(e) {

        if (e.originalEvent && !isEnd) {

            delayedCallback(getResults, 300);

        }

    }

    function _checkScroll(amountResult) {

        /** Jeżeli brak scrolla, a zwróciło wyniki równe paginationLimit */

        if (!$box.hasScrollBar() && amountResult >= opts.perPage) {
            getResults();
        }
    }

    $box.on('scroll', onScroll);

    return {
        reset: function (options) {
            $box.off('scroll', onScroll);

            currentPage = 2;
            isEnd = false;
            $box.scrollTop(0);
            opts = $.extend(opts, options);

            $box.on('scroll', onScroll);

            return this;
        },
        checkScroll: function (amountResult) {
            _checkScroll(amountResult);
            return this;
        },
        destroy: function () {
            $box.scrollTop(0);
            $box.off('scroll', onScroll);
        }
    }
}

function handleDraggableTable() {

    if (!draggableTable) {
        draggableTable = new DraggableTable();
        draggableTable.Run();
    }
    else {
        draggableTable.recalculateTh();
    }

}

function fixToolTableHandle() {
    $.each($('.scroll-box-table'), function () {
        var $t = $(this);

        /** 2ms */
        fixScrollBarOfDiv($t, $($t.data('target')));

        /** 6ms */
        setTableHeight();

        /** 592ms */
        setWidthScrollBox($t, function () {
        });

    });
}

function fixScrollBarOfDiv($t, $target) {

    $t.hasScrollBar();

    if ($t.hasScrollBar()) {
        $target.parent().css('overflow-y', 'scroll');
    }
    else {
        $target.parent().css('overflow-y', 'auto');
    }

    $('.scroll-box-fixed-head').css({
        'overflow-x': 'hidden'
    });

    // forceRedraw($target);

}

var forceRedraw = function (element) {
    element.hide(0, function () {
        $(this).show();
    });
};

/** Ustawia wysokość tabelki z wynikami narzędzia */

function setTableHeight() {

    var isMonitorMode = getParameterByName('monitor-mode');

    if ($('.table-responsive').size() && $('.table-responsive .scroll-box-table').size()) {

        var offset = 0;

        if ($('body').hasClass('guest-view') || isMonitorMode === "1") {

            offset = 4 + $('.scroll-box-fixed-head').outerHeight();

            if(isMonitorMode) {
                offset += $('.title-tool').outerHeight() - 4;
            }

        }
        else if ($('.embed-tool').length){
            filter = ($('#filters-div').is(':visible')) ? $('#filters-div').outerHeight(true) : 0;
            offset = filter + 75;
        }
        else {
            var $pageContent = $('.page-content'),
                filter = ($('#filters-div').is(':visible')) ? $('#filters-div').outerHeight(true) : 0;
            offset = 80 + filter + $pageContent.find('.scroll-box-fixed-head').height() + $pageContent.find('.page-toolbar').outerHeight() + ( parseInt($pageContent.find('.portlet').css('padding-top')) * 2);

        }

        if ($('#tool-helper').size()) {
            offset = offset + $("#tool-helper").outerHeight(true);
        }

        $('.table-responsive .scroll-box-table').css('max-height', window.innerHeight - offset);

    }

}

function setWidthScrollBox($t, callback) {

    var $target = $($t.data('target')),
        widthList = [];

    // detectedInputsSize($t, $target);

    var index = ($('#tool-table-query').length ? 2 : 1);

    var tds = $t.find('tbody tr:nth-of-type('+index+') td:visible');

    /** 207ms - odpala się RecalculateStyle */
    $.each(tds, function () {
        widthList.push(this.offsetWidth);
    });

    if ($('tr#no-result-info').length == 0) {

        $.each($target.find('thead tr:first-child th:visible'), function () {
            var width = widthList.shift();

            // if($(this).hasClass('with-limit-width')) return false;
            if(!this.classList.contains('with-limit-width')) {
                $(this).css({
                    'min-width': width,
                    'max-width': width,
                    'width': width
                });
            }

        });
    }

    var tableToolTh = $t.find('thead tr:first-child th');

    tableToolTh.addClass('hide-it');

    $target.show();

    /** 198ms */
    handleDraggableTable();

    /** 42ms */
    $.each($target.find('thead tr:first-child th'), function (i, ele) {

        if(!ele.classList.contains('with-limit-width')) {
            tableToolTh.eq(i).css('min-width', $(this).outerWidth());
        }

    });

    if (typeof callback == 'function') {
        callback();
    }
}

function detectedInputsSize($t, $target) {

    if ($target.find('tbody').size()) {

        var inputs = [];

        $.each($target.find('tbody tr:first-child td'), function () {
            var $ele = $(this);
            if ($ele.find('.tool-filter').size()) {

                var width = 0;
                $.each($ele.find('.tool-filter > div'), function () {
                    width += $(this).outerWidth();
                });

                inputs.push([$ele.index(), (width + 20)]);
            }
        });

        if ($target.css('display') == 'none') {
            $.each(inputs, function (i, ele) {
                $target.find('thead tr:first-child th:nth-of-type(' + ele[0] + ')').css({
                    'width': ele[1],
                    'min-width': ele[1]
                });
            });
        }

        $.each(inputs, function (i, ele) {
            $t.find('thead tr:first-child th:nth-of-type(' + ele[0] + ')').css({
                'width': ele[1],
                'min-width': ele[1]
            });
        });
    }
}


/**
 * @class DraggableTable
 */
var DraggableTable = (function () {

    function DraggableTable() {

        DraggableTable.selected = null;
        DraggableTable.lastSelected = null;
        DraggableTable.startOffsetLeft = 0;
        DraggableTable.scrollBox = $('#scroll-box-head');
        DraggableTable.draggableWrapper = $('.draggable-wrapper');
        DraggableTable.toolTableContainer = $('#tool-table-container > div.scroll-box-table');

        DraggableTable.ths = DraggableTable.scrollBox.find('thead > tr:nth-of-type(1) > th');
        DraggableTable.draggableThs = [];
        DraggableTable.toolTable = $('#tool-table');

        var currentEle = DraggableTable.draggableWrapper[0];

        while (currentEle.nodeName !== "BODY") {
            DraggableTable.startOffsetLeft += currentEle.offsetLeft;

            if(currentEle.offsetParent === null) {
                break;
            }

            currentEle = currentEle.offsetParent;
        }

    }

    function handleEvents() {

        DraggableTable.draggableWrapper.on('mousedown', '.draggable-th-element', function () {

            $(document)
                .on('mousemove', _moveElem)
                .on('mouseup', _destroy);

            _dragInit(this);
            return false;
        });

    }


    /**
     * ----------------------------------------------------------------------------------------------------------
     * PUBLIC FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    DraggableTable.prototype.getData = function () {
        return DraggableTable.data;
    };


    DraggableTable.prototype.recalculateTh = function () {
        _recalculateTh();
    };

    /**
     * ----------------------------------------------------------------------------------------------------------
     * PRIVATE FUNCTIONS
     * ----------------------------------------------------------------------------------------------------------
     */

    function _moveElem(e) {

        // delayedCallback(function () {

        var i = Array.prototype.indexOf.call(DraggableTable.lastSelected.parentNode.children, DraggableTable.lastSelected),
            ele = DraggableTable.ths.eq(i);

        var diffWidth = (e.clientX - (DraggableTable.startOffsetLeft + DraggableTable.lastSelected.offsetLeft - DraggableTable.toolTableContainer[0].scrollLeft));

        var width = (diffWidth + ele[0].offsetWidth);

        // var smaller = (width < ele[0].offsetWidth);

        if (width < 50) return false;

        width += 'px';

        ele.css({
            'width': width,
            'min-width': width,
            'max-width': width
        });

        DraggableTable.toolTable.find('tbody > tr > td:nth-child(' + (i + 1) + '), thead > tr > th:nth-child(' + (i + 1) + ')').css({
            'width': width,
            'min-width': width,
            'max-width': width
        });

        // if(!DraggableTable.toolTableContainer.hasScrollBar()) {
        //     var nextEle = DraggableTable.ths.eq(i+1);
        //
        //     if(nextEle.length && smaller) {
        //         width = ((-1 * diffWidth) + nextEle[0].offsetWidth);
        //         width += "px";
        //
        //         nextEle.css({
        //             'width': width,
        //             'min-width': width,
        //             'max-width': width
        //         });
        //
        //         DraggableTable.toolTable.find('tbody > tr > td:nth-child('+(i+2)+'), thead > tr > th:nth-child('+(i+2)+')').css({
        //             'width': width,
        //             'min-width': width,
        //             'max-width': width
        //         });
        //
        //     }
        // }

        _recalculateTh();


        // }, 50);

    }

    function _dragInit(elem) {

        DraggableTable.selected = elem;
        DraggableTable.lastSelected = DraggableTable.selected;

    }

    function _destroy() {

        DraggableTable.lastSelected = DraggableTable.selected;
        DraggableTable.selected = null;

        $(document)
            .off('mousemove', _moveElem)
            .off('mouseup', _destroy);

        _fixTableTd();

        /** Personalizacja ustawień */

        var i = Array.prototype.indexOf.call(DraggableTable.lastSelected.parentNode.children, DraggableTable.lastSelected),
            ele = DraggableTable.ths.eq(i);
        $(document).trigger('toolColResize', ele);
    }

    function _fixTableTd() {

        var diff = false;

        $.each(DraggableTable.toolTable.find('thead > tr > th'), function (i, ele) {
            if (ele.offsetWidth !== DraggableTable.ths.eq(i).outerWidth(true)) {
                diff = true;
            }
        });

        if (diff) {
            fixToolTableHandle();
        }

    }

    function _appendDraggableHtml() {

        if (DraggableTable.scrollBox.size()) {
            $.each(DraggableTable.ths, function (i, ele) {

                var newDragTh = $('<div class="draggable-th-element"></div>');

                DraggableTable.draggableWrapper.append(newDragTh);

                if (ele.offsetParent === null) {
                    newDragTh.css('display', 'none');
                }

                DraggableTable.draggableThs.push(newDragTh);

            });
        }

        _recalculateTh();
    }

    function _recalculateTh() {

        var startLeft = 0;

        $.each(DraggableTable.ths, function (i, ele) {

            startLeft += ele.offsetWidth;
            DraggableTable.draggableThs[i].css('left', startLeft + 'px');

        });
    }

    DraggableTable.prototype.Run = function () {

        handleEvents.call(this);

        _appendDraggableHtml();
    };

    return DraggableTable;
})();



// dependentInputsHandle

function dependentInputsHandle() {

    dependentWatchInputs();

}

function dependentWatchInputs() {

    var $form = $('.edit-row-of-tool');

    if($form.size() > 0) {
        $form.on('change', 'input, select, checkbox, textarea', function () {
            dependentEventOnChange($(this));
        });

        $.each($form.find('.date-time-picker, .date-picker'), function (i, ele) {
            var $ele = $(ele);
            $ele.data('datepicker').update('onSelect', function () {
                dependentEventOnChange($ele);
                $ele.trigger('change');
            })
        });

        $form.find('input, select, checkbox, textarea').trigger('change');
    }
}

function dependentEventOnChange($t) {

    var targetName = $t.attr('data-col-name'),
        isCheckBox = ($t.attr('type') == 'checkbox'),
        value = (isCheckBox) ? $t.prop('checked') : $t.val(),
        targetValue;

    $.each($('[data-target-name="'+ targetName+ '"]'), function () {
        var $t = $(this);

        targetValue = $t.attr('data-target-value');

        if((typeof value == "string" && targetValue && (targetValue.split('||')).indexOf(value) === -1)
            || typeof value == "boolean" && targetValue != value
        ){
            if($t.attr('type') != 'hidden') $t.val('');

            $t.closest('div[class^="col-"] input').attr("required", false);
            $t.closest('div[class^="col-"]').hide();
            permanentlyHide($t.attr('data-col-name'));
        }
        else {
            if ($t.closest('div[class^="col-"] input').hasClass("atlas-custom-required")) {
                $t.closest('div[class^="col-"] input').attr("required", true);
            }
            $t.closest('div[class^="col-"]').show();
            dependentEventOnChange($t);
        }
    });
}

function permanentlyHide(targetName) {

    if(!targetName) return false;

    $.each($('[data-target-name="'+ targetName+ '"]'), function () {
        var $t = $(this);

        if($t.attr('type') != 'hidden') $t.val('');
        $t.closest('div[class^="col-"]').hide();

        permanentlyHide($t.attr('data-col-name'));
    });
}

function checkEllipsis() {
    $.each($('#tool-edit-modal').find('.form-group label'), function (i, ele) {
        if(isEllipsisActive(ele))
        {
            $(ele).attr('title', $(ele).text());
        }
    });
}

function isEllipsisActive(e) {
    return (e.offsetWidth < e.scrollWidth);
}