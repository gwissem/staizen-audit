<?php

namespace WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Test\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TranslationFormType extends AbstractType
{

    protected $availableLocales;
    protected $defaultLocale;

    protected function setDefaultLocale($defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
    }

    protected function getAvailableLocales()
    {
        return $this->availableLocales;
    }

    public function setAvailableLocales($availableLocales)
    {
        $this->availableLocales = $availableLocales;
    }


}