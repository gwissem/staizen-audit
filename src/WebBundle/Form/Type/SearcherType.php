<?php

namespace WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearcherType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
                'phrase',
                TextType::class,
                [
                    'required' => false,
                    'attr' => [
                        'class' => 'lower',
                        'placeholder' => 'Wpisz frazę aby wyszukać...',
                    ],
                ]
            )->setMethod('GET');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array()
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_searcher';
    }

}