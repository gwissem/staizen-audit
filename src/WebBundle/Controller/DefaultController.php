<?php

namespace WebBundle\Controller;

use AppBundle\Entity\CustomContent;
use AppBundle\Utils\QueryManager;
use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Tests\Fixtures\Controller\NullableController;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index", options={"expose"=true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');
        $ipAddressChecker = $this->get('app.service.ip_address_checking');
        $parameterBag = $this->get('assetic.parameter_bag');

        if ($authChecker->isGranted('IS_AUTHENTICATED_FULLY')) {

            /** @var SessionInterface $session */
            $session = $request->getSession();

            /**
             * Wylogowanie użytkownika gdy spełnione są wszystkie poniższe warunki:
             *
             * - użytkownik jest użytkownikiem domenowym (ActiveDirectory)
             * - Request nie jest z IP domenowego
             * - jest to środowisko produkcyjne
             */
            if (
                true === $session->get('is_domain_user')
                && false === $ipAddressChecker->isDomainIP($request)
                && 'prod' === $parameterBag->get('atlas_environment')) {

                $this->get('security.token_storage')->setToken();
                $session->invalidate();

                return $this->redirect($this->generateUrl('fos_user_security_login'));
            }

            if ($authChecker->isGranted('ROLE_ADMIN')) {
                $url = $this->generateUrl('admin_tools');
            } elseif (true === $this->userIsContractor()) {
                $url = $this->generateUrl('operational_dashboard_index');
            } else {
                $url = $this->generateUrl('dashboard');
            }

            $jabberAccount = null;

            if($authChecker->isGranted(User::ROLE_TELEPHONY)) {

                $redis = $this->get('snc_redis.default');
                $originalUser = $this->get('user.info')->getOriginalUser();
                $userInfo = $this->get('user.info')->getInfo();

                $data = $redis->hgetall(\JabberBundle\Controller\DefaultController::INDEX_JABBER_REDIS . $originalUser->getId());

                $pass = base64_encode(json_encode($data, JSON_FORCE_OBJECT));

                $jabberAccount = $this->renderView(
                    '@Jabber/Components/jabber-account-variable.html.twig',
                    [
                        'data' => $pass,
                        'user' => $userInfo
                    ]
                );

            }

            $canMonitorPage = $this->checkMonitorPage();

            return $this->render('@Web/Home/index.html.twig', [
                'canMonitorPage' => $canMonitorPage,
                'url' => $url,
                'jabberAccount' => $jabberAccount
            ]);
        }

        return $this->redirect($this->generateUrl('fos_user_security_login'));
    }

    /**
     * @return bool|mixed
     */
    private function checkMonitorPage() {

        return self::_checkMonitorPage($this->getUser(), $this->get('app.query_manager'));

    }

    /**
     * Sprawdza, czy użytkownik ma być monitorowany na jakie strony wchodzi w  czasie swojej pracy
     *
     * @param User $user
     * @param QueryManager $queryManager
     * @return bool|mixed
     */
    static function _checkMonitorPage(User $user, QueryManager $queryManager) {

        if($user->isAdmin()) return false;

        $output = $queryManager->executeProcedure('SELECT dbo.f_checkMonitorPage(:userId) as canMonitor',[
            [
                'key' => 'userId',
                'value' => (int)$user->getId(),
                'type' => \MssqlBundle\PDO\PDO::PARAM_INT
            ]
        ]);

        if(!empty($output) && isset($output['canMonitor'])) {
            return  filter_var($output['canMonitor'], FILTER_VALIDATE_BOOLEAN);
        }

        return true;

    }

    /**
     * @example:
     *
     * /monitor-tool?
     *      tool[]=2616&tool[]=2616
     *      &view-mode=horizontal
     *      &time=10000
     *
     * @Route("/monitor-tool", name="index_monitor_tool", options={"expose"=true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function monitorToolAction(Request $request)
    {

        $tools = $request->query->get('tool', []);
        $time = $request->query->get('time', 5000);

        if(intval($time)<3000) {
            $time = 5000;
        }

        $user = $this->getUser();

        if(!$user instanceof User) {
            $this->loginCustomUser($request);
        }

        return $this->render('@Web/Home/monitor-tool.html.twig', [
            'tools' => $tools,
            'time' => $time
        ]);

    }

    /**
     * @param Request $request
     * @param User $user
     */
    private function loginCustomUser(Request $request)
    {

        $user = $this->getDoctrine()->getManager()->getRepository("UserBundle:User")->findOneBy(array('username' => 'cc_monitoring'));

        if($user instanceof User) {
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);

            $this->get('session')->set('_security_main', serialize($token));

            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        }

    }

    /**
     * @Route("/clear-flash/{key}", name="clear_flash_msg")
     * @param Request $request
     * @param $key
     * @return JsonResponse
     */
    public function clearFlashAction(Request $request, $key)
    {

        $flashBag = $this->get('session')->getFlashBag();
        foreach ($flashBag->keys() as $flash) {
            if ($flash == $key) {
                $flashBag->set($key, []);
            }
        }

        return new JsonResponse("", 200);
    }

    /**
     * @Route("/documents/{filename}", name="get_document")
     * @param $filename
     * @return BinaryFileResponse
     */
    public function getDocumentAction($filename){

        $publicResourcesFolderPath = $this->get('kernel')->getRootDir() . '/../data/documents/'.$filename;

        return new BinaryFileResponse($publicResourcesFolderPath);

    }

    /**
     * @Route("/info/{webUrl}", name="info_page")
     * @param $webUrl
     * @return Response|NotFoundHttpException
     */
    public function infoAction($webUrl){

        /** @var CustomContent $customContent */
        $customContent = $this->getDoctrine()->getRepository(CustomContent::class)->findOneByWebUrl($webUrl);
        if(!$customContent){
            return new NotFoundHttpException();
        }

        return $this->render('@Web/Default/info.html.twig', [
            'content' => $customContent->getContent()
        ]);

    }

    private function userIsContractor()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $starter24 = $em->getRepository('UserBundle:Company')->findOneBy(['name' => 'Starter24']);
        $contractorGroup = $em->getRepository('UserBundle:Group')->findOneBy(['name' => 'Kontraktorzy']);

        return $user->hasGroup($contractorGroup->getId()) && $starter24->getId() !== $user->getCompanyId();
    }

}
