<?php

namespace WebBundle\Controller;

use PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Tests\Fixtures\Controller\NullableController;
use UserBundle\Entity\User;

/**
 * Class AdminMenuActionsController
 * @package WebBundle\Controller
 * @Route("/admin-actions")
 */
class AdminMenuActionsController extends Controller
{
    /**
     * @Route("/parser-helper", name="aa_parser_helper", options={"expose"=true})
     * @param Request $request
     * @Security("has_role('ROLE_ADMIN')")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {

        $toolName = $request->query->get('toolName');
        $data = $request->query->all();

        return new Response($this->getAdminActionTool($toolName, $data));

    }

    private function getAdminActionTool($name, $data) {
        return $this->renderView('@Web/Admin/Tools/' . $name. '.html.twig', $data);
    }

}
