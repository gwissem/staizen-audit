<?php

namespace WebBundle\Controller;

use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Tests\Fixtures\Controller\NullableController;
use UserBundle\Entity\User;

/**
 * Class ApiTemplateController
 * @Route("/api-template")
 */

class ApiTemplateController extends Controller
{

    /**
     * @Route("/get-html/{fileName}/{groupProcessId}", name="api_template_get_html", defaults={"groupProcessId"=0})
     * @param $fileName
     * @param $groupProcessId
     * @return Response
     */
    public function getHtmlAction($fileName, $groupProcessId)
    {

        try {
            return $this->render('@Web/ApiTemplate/'.$fileName.'.html.twig');
         } catch (\Exception $ex) {
            return new Response('Template is not exists.');
        }

    }

}
