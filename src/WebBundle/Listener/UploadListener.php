<?php

namespace WebBundle\Listener;

use Oneup\UploaderBundle\Event\PostPersistEvent;
use KonsultExpert\EntityBundle\Entity\File;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class UploadListener
{
    /**
     * @var ObjectManager
     */
    private $em;
    private $tokenStorage;
    private $sc;

    public function __construct($em, TokenStorage $tokenStorage, $sc)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->sc = $sc;
    }

    public function onUpload(PostPersistEvent $event)
    {

        $returnArr = array();
        $file = $event->getFile();


        $request = $event->getRequest();

        $uploads = $request->files->all();
        $eventFile = $event->getFile();
        $config = $event->getConfig();

        $file = $this->em->getRepository('KonsultExpertEntityBundle:File')->findOneByPath($eventFile->getPath());
        $file->setFilesystem($config['storage']['filesystem']);

        if ($appointmentId = $request->get('appointmentId')) {

        }

        if ($appointmentId = $request->get('appointmentId')) {
            if ($appointment = $this->em->getRepository('KonsultExpertClientBundle:Appointment')->find(
                $appointmentId
            )
            ) {

                if (in_array($file->getUser(), array($appointment->getUser(), $appointment->getExpert()))) {
                    $appointment->addFile($file);
                    $this->em->persist($appointment);
                }


            }
        }

        $this->em->persist($file);
        $this->em->flush();

        $response = $event->getResponse();
        $response['file'] = array(
            'id' => $file->getId(),
            'path' => $file->getPath(),
            'name' => $file->getName(),
        );

        return $response;
    }

    protected function getFiles(FileBag $bag)
    {
        $files = array();
        $fileBag = $bag->all();
        $fileIterator = new \RecursiveIteratorIterator(
            new \RecursiveArrayIterator($fileBag),
            \RecursiveIteratorIterator::SELF_FIRST
        );
        foreach ($fileIterator as $file) {
            if (is_array($file) || null === $file) {
                continue;
            }
            $files[] = $file;
        }

        return $files;
    }
}