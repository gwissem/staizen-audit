<?php

namespace WebBundle\Service;

use Knp\Menu\Twig\MenuExtension;
use Predis\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use UserBundle\Service\UserInfoService;

class CachedMenuService
{

    protected $userInfo;

    /** @var  Client */
    protected $redis;

    /** @var  MenuExtension */
    protected $menuExtension;

    protected $request;

    const TTL_CACHE = 3600;

    /** @var ContainerInterface */
    protected $container;

    /**
     * CachedMenuService constructor.
     * @param ContainerInterface $container
     * @param UserInfoService $userInfo
     * @param $redis
     * @param MenuExtension $menuExtension
     * @param RequestStack $requestStack
     */
    public function __construct(ContainerInterface $container, UserInfoService $userInfo, $redis, MenuExtension $menuExtension, RequestStack $requestStack)
    {
        $this->redis = $redis;
        $this->menuExtension = $menuExtension;
        $this->userInfo = $userInfo;
        $this->request = $requestStack->getCurrentRequest();
        $this->container = $container;
    }

    private function getAvailableMenu() {
        return [
            'userToolsMenu'
        ];
    }

    public function renderMenu($name, $options) {

        if(!$this->userInfo->getUser()) {
            return $this->menuExtension->render($name, $options);
        }

        $redisKey = $this->getRedisKey($name);

        if($this->redis->exists($redisKey))
        {
            return $this->redis->get($redisKey);
        }

        $html = $this->menuExtension->render($name, $options);

        if($html) {
            $this->redis->setex($redisKey, self::TTL_CACHE, preg_replace('/^\s+|\n|\r|\s+$/m', '', $html));
        }

        return $html;

    }

    public function clearRedisKey($name) {
        $redisKey = $this->getRedisKey($name);
        if($redisKey) $this->redis->del([$redisKey]);
    }

    public function clearCacheOfUser($ids) {

        $keys = [];

        if(is_array($ids)) {
            foreach ($ids as $id) {
                foreach ($this->getAvailableMenu() as $menu) {
                    foreach ($this->availableLocales() as $locale) {
                        $keys[] = $this->getRedisKey($menu, $id, $locale);
                    }
                }
            }
        }
        else {
            foreach ($this->getAvailableMenu() as $menu) {
                foreach ($this->availableLocales() as $locale) {
                    $keys[] = $this->getRedisKey($menu, $ids, $locale);
                }
            }
        }

        if($keys)
        {
            $this->redis->del($keys);
        }

    }

    private function getRedisKey($name, $userId = null, $locale = null){
        if(!$userId) $userId = ($this->userInfo->getUser()) ? $this->userInfo->getUser()->getId() : 'no_name';
        if(!$locale) $locale = ($this->request) ? $this->request->getLocale() : 'pl';

        return $userId . "_" . $name . "_" . $locale;
    }

    private function availableLocales() {
        return $this->container->getParameter('available_locales');
    }
}