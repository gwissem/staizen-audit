<?php

namespace WebBundle\Events;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use UserBundle\Entity\User;

class SoftDeleteListener
{
    public function preRemove(LifecycleEventArgs $event)
    {
        $entity = $event->getObject();

        if ($entity instanceof User) {

            /** Ustawienie Username po usunięciu */
            $entity->setUsername($entity->getUsername() . '_' . time());

            $om = $event->getObjectManager();
            $om->persist($entity);
            $om->flush();
        }
    }
}