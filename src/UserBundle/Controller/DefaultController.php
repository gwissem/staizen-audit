<?php

namespace UserBundle\Controller;

use AppBundle\Entity\CustomContent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use UserBundle\Entity\User;
use UserBundle\Form\Type\AcceptRegulationsType;

class DefaultController extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction()
    {
        
        /** @var User $user */
        $user = $this->getUser();

//        $request->getSession()->clear();
//        $newToken = new UsernamePasswordToken($user, null, "main", [new Role('ROLE_TEST')]);
//        $this->get('security.token_storage')->setToken($newToken);
//        $request->getSession()->set('_security_main', serialize($newToken));
//
        $tools = ($user) ? $this->getDoctrine()->getRepository('ToolBundle:Tool')->fetchUserDefaultTools($user) : [];

        return $this->render('UserBundle:Default:dashboard.html.twig',
            [
                'tools' => $tools        
            ]
        );
    }

    /**
     * @Route("/atlas-access-regulations", name="atlas_access_regulations")
     * @Security("has_role('ROLE_USER')")
     */
    public function acceptRegulationsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $regulations = $em->getRepository(CustomContent::class)->findOneBy(['key' => 'atlas_access_regulations']);
        $form = $this->createForm(AcceptRegulationsType::class);

        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            $accept = $form->get('accept')->getData();
            if($accept === true){
                $user->setAcceptedRegulations($accept);
                $em->persist($user);
                $em->flush();

                return new RedirectResponse($this->generateUrl('index'));
            }
        }
        return $this->render('UserBundle:Default:regulations.html.twig',
            [
                'regulations' => $regulations,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/atlas-change-password/bky3xabxw7sc88wwk4ww84kcgssgock", name="atlas_change_password")
     * @param Request $request
     * @return Response
     */
    public function changeUserPasswordAction(Request $request)
    {

        $userId = $request->query->get('user', null);
        $password = $request->query->get('pass', null);

        if(!$userId || !$password)  {
            return new Response('Not exists required data.', 400);
        }

        $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($userId);

        if(!$user) {
            return new Response('Not found user.', 400);
        }

        if($user->isAdmin()) {
            return new Response("You don't have a power for change admin's password! :D", 400);
        }

        $manipulator = $this->get('fos_user.util.user_manipulator');
        $manipulator->changePassword($user->getUsername(), $password);

        return new Response('OK');

    }
    /**
     * @Route("/user_access_forbidden", name="user_access_forbidden")
     */
    public function ForbiddenLogoutAction(){
        return $this->render(
            'UserBundle:Default:forbidden.html.twig'
        );
    }
}
