<?php

namespace UserBundle\Controller;

use ApiBundle\Entity\AccessToken;
use ApiBundle\Entity\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\PermissionTransfer;
use UserBundle\Entity\User;
use UserBundle\Form\Type\ChangePasswordType;
use UserBundle\Form\Type\PermissionTransferType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProfileController extends Controller
{
    /**
     * @Route("/profile/settings", name="user_settings")
     */
    public function settingsAction(Request $request)
    {
        $user = $this->getUser();

        if ($user->getId() !== $this->get('user.info')->getOriginalUser()->getId()) {
            throw new AccessDeniedException();
        }

        return $this->render(
            'UserBundle:Profile:settings.html.twig'
        );
    }

    /**
     * @Route("/profile/change-password", name="user_change_password")
     */
    public function changePasswordAction(Request $request)
    {
        $um = $this->get('fos_user.user_manager');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($user->getId() !== $this->get('user.info')->getOriginalUser()->getId()) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->setData($user);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            $um->updateUser($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('Hasło zostało zmienione')
            );

            return $this->redirect($this->generateUrl('user_settings'));
        }

        return $this->render(
            'UserBundle:Profile:change-password.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/profile/change-locale", name="user_change_locale")
     */
    public function changeLocaleAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user->getId() !== $this->get('user.info')->getOriginalUser()->getId()) {
            throw new AccessDeniedException();
        }

        $response = new Response();
        $locales = $this->getParameter('available_locales');

        $form = $this->createFormBuilder()
            ->add(
                'locale',
                ChoiceType::class,
                [
                    'choices' => array_combine($locales, $locales),
                    'label' => 'Język',
                    'data' => $request->getLocale(),
                    'attr' => [
                        'class' => 'select2',
                    ],
                ]
            )
            ->setMethod('POST')
            ->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $selected = $form->get('locale')->getData();
            $localeCookie = new Cookie('_locale', $selected, time() + 3600 * 24 * 365, '/', null, false, false);
            $response->headers->setCookie($localeCookie);

            $response->sendHeaders();

            $user->setLocale($selected);
            $this->get('fos_user.user_manager')->updateUser($user);

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('Wersja językowa została zmieniona')
            );

            return $this->redirect($this->generateUrl('user_settings'));
        }

        return $this->render(
            'UserBundle:Profile:change-locale.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/profile/permission-transfer/{id}/{page}", requirements={"page" = "\d+"}, name="user_permission_transfer_editor")
     */
    public function permissionTransferAction(Request $request, $page = 1, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($user->getId() !== $this->get('user.info')->getOriginalUser()->getId())
            throw new AccessDeniedException();

        $history = false;
        $isNew = false;
        if ($id) {
            $permissionTransfer = $em->getRepository('UserBundle:PermissionTransfer')->find($id);
            if (!$permissionTransfer) {
                throw $this->createNotFoundException('Nie znaleziono cesji');
            }
            $this->denyAccessUnlessGranted('edit', $permissionTransfer);
        } else {
            $isNew = true;
            $permissionTransfer = new PermissionTransfer($user);
        }


        $form = $this->createForm(PermissionTransferType::class, $permissionTransfer, ['user' => $user]);

        $form->setData($user);
        $form->handleRequest($request);

        if ($isNew) {
            $historyQuery = $em->getRepository('UserBundle:PermissionTransfer')->fetchUserHistory($user);
            $perPage = 20;
            $history = $this->get('knp_paginator')->paginate($historyQuery, $page, $perPage);
        }
        if ($request->isMethod('POST') && $form->isValid()) {
            $em->persist($permissionTransfer);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('Cesja uprawnień została ustawiona')
            );

            return $this->redirect($this->generateUrl('user_permission_transfer_editor'));
        }

        return $this->render(
            'UserBundle:Profile:permission-transfer.html.twig',
            array(
                'form' => $form->createView(),
                'history' => $history,
            )
        );
    }

    /**
     * @Route("/profile/permission-transfer-delete/{id}", name="user_permission_transfer_delete")
     */
    public function permissionTransferDeleteAction(Request $request, $id)
    {
        $user = $this->getUser();
        if ($user->getId() !== $this->get('user.info')->getOriginalUser()->getId())
            throw new AccessDeniedException();

        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('UserBundle:PermissionTransfer')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono cesji'));
        }


        $this->denyAccessUnlessGranted('delete', $entity);

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('Cesja została pomyślnie usunięta')
        );

        return $this->redirect($this->generateUrl('user_permission_transfer_editor'));

    }

    /**
     * @Route("/profile/generate-api-token", name="user_api_token_generate")
     */
    public function generateApiTokenAction(Request $request)
    {
        $token = '';
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('ApiBundle:Client')->findOneBy(['name' => Client::NAME_API_TOOLS]);

        if ($request->getMethod() == 'POST' && $client) {
            $now = new \DateTime();
            $expires = $now->modify('+10 years');
            $accessToken = new AccessToken();

//            $user = $em->getRepository('UserBundle:User')->find(1625);
            $user = $this->getUser();

            $accessToken->setClient($client);
            $accessToken->setUser($user);
            $accessToken->setExpiresAt($expires->getTimestamp());
            $accessToken->setScope(AccessToken::TOOLS_SCOPE);
            $accessToken->setToken($this->get('user.info')->genApiAccessToken());

            $this->get('fos_oauth_server.access_token_manager')->updateToken($accessToken);

            $token = $accessToken->getToken();
        }

        return $this->render(
            'UserBundle:Profile:generate-api-token.html.twig',
            [
                'token' => $token,
            ]
        );
    }
}
