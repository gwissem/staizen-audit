<?php

namespace UserBundle\Controller;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use UserBundle\Entity\User;
use UserBundle\Form\Type\UserType;

class UserController extends Controller
{
    /**
     * @Route("/list/{group}/{page}", requirements={"page" = "\d+"}, name="admin_users")
     * @param int $page
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = 1, $group = 0)
    {
        $em = $this->getDoctrine()->getManager();

        $searchForm = $this->createFormBuilder()
            ->add(
                'phrase',
                TextType::class,
                [
                    'required' => false,
                    'attr' => [
                        'class' => 'lower',
                        'placeholder' => $this->get('translator')->trans('Wpisz frazę aby wyszukać...'),
                    ],
                ]
            )
            ->add('only_online',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => "Online"
                ]
            )
            ->setMethod('GET')
            ->getForm();

        $searchForm->handleRequest($request);

        $perPage = 30;
        $phrase = $searchForm->get('phrase')->getData();

        $onlyOnline = filter_var($searchForm->get('only_online')->getData(), FILTER_VALIDATE_BOOLEAN);

        $usersQuery = $em->getRepository('UserBundle:User')->getQueryOfAllUser($phrase, $group, false, $onlyOnline);

        $pagination = $this->get('knp_paginator')->paginate($usersQuery, $page, $perPage);

        $template = $group != 0 ? 'UserBundle:Admin:user/partials/user-list.html.twig' : 'UserBundle:Admin:user/index.html.twig';

        return $this->render(
            $template,
            array(
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView(),
                'group' => $group,
                'page' => $page,
                'perPage' => $perPage
            )
        );
    }

    /**
     * @Route("/definition/{userId}", name="admin_user_editor", requirements={"userId": "\d+"})
     * @param Request $request
     * @param null $userId
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function editorAction(Request $request, $userId = null)
    {

        $em = $this->getDoctrine()->getManager();
        $userManager = $this->get('fos_user.user_manager');

        if ($userId) {
            $entity = $em->getRepository('UserBundle:User')->find($userId);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono użytkownika'));
            }
        } else {
            $entity = $userManager->createUser();
            $entity->setEnabled(true);
        }

        /** Gdy zaznaczone "auto_generate", to generuje losowe hasło dla użytkownika */

        $generateRandomPassword = $this->generateRandomPasswordForUser($request);

        if($generateRandomPassword) $entity->setEnabled(false);

        $dispatcher = $this->get('event_dispatcher');
        $event = new GetResponseUserEvent($entity, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        $form = $this->createForm(UserType::class, $entity);
        $form->setData($entity);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {

            $emailAlreadyExists = $em->getRepository('UserBundle:User')->checkIfEmailExists($entity);
            $usernameAlreadyExists = $em->getRepository('UserBundle:User')->checkIfUserNameExists($entity);
            if ($emailAlreadyExists) {
                $constraint = new UniqueEntity(['email']);
                /** @Ignore */
                $form->get('email')->addError(new FormError($this->get('translator')->trans(/** @ignore */
                    $constraint->message, [], 'validators')));
            }
            if ($usernameAlreadyExists) {
                $constraint = new UniqueEntity(['username']);
                /** @Ignore */
                $form->get('username')->addError(new FormError($this->get('translator')->trans(/** @ignore */
                    $constraint->message, [], 'validators')));
            }
            if ($form->isValid()) {


                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $entity->setUpdatedAt(new \DateTime());

                if ($userId) {
                    $userManager->updateUser($entity);
                } else {

                    /** Gdy zaznaczone "auto_generate", to dodaje confirmation token, żeby wysłać e-mail */

                    if ($generateRandomPassword) {
                        if (null === $entity->getConfirmationToken()) {
                            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
                            $tokenGenerator = $this->get('fos_user.util.token_generator');
                            $entity->setConfirmationToken($tokenGenerator->generateToken());
                        }
                    }

                    $em->persist($entity);
                }

                /** Wysyła email dla nowo dodanych użytkowników  */

                if ($generateRandomPassword) $this->sendActivationEmail($entity);

                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans(
                        'Dane Użytkownika "%name%" zostały pomyślnie zaktualizowane',
                        ['%name%' => $entity->getName()]
                    )
                );

                $response = new RedirectResponse($this->generateUrl('admin_user_editor', ['userId' => $userId]));

                return $response;
            }

        }

        return $this->render(
            'UserBundle:Admin:user/editor.html.twig',
            array(
                'form' => $form->createView(),
                'user' => $entity,
            )
        );
    }

    private function generateRandomPasswordForUser(Request $request) {

        if ($request->isMethod('POST')) {
            if($atlasUser = $request->request->get('atlas_user', null)) {
                if(isset($atlasUser['auto_generate']) && $atlasUser['auto_generate']) {

                    $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                    $password = substr($tokenGenerator->generateToken(), 0, 12);

                    $atlasUser['plainPassword'] = [
                        'first' => $password,
                        'second' => $password
                    ];

                    $request->request->set('atlas_user', $atlasUser);

                    return true;
                }
            }
        }

        return false;
    }

    private function sendActivationEmail(User $entity) {

        $this->get('fos_user.mailer')->sendActivationEmailMessage($entity);
        $entity->setPasswordRequestedAt(new \DateTime());

    }

    /**
     * @Route("/delete/{id}", name="admin_user_delete")
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('UserBundle:User')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono użytkownika'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Użytkownik "%name%" został pomyślnie usunięty',
                ['%name%' => $entity->getName()]
            )
        );

        return $this->redirect($this->generateUrl('admin_users'));
    }

}
