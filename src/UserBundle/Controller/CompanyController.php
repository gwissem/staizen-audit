<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Tests\Matcher\DumpedUrlMatcherTest;
use UserBundle\Entity\Company;
use UserBundle\Form\Type\CompanyType;
use WebBundle\Form\Type\SearcherType;

class CompanyController extends Controller
{

    private $perPage = 20;

    /**
     * @Route("/{page}", requirements={"page" = "\d+"}, name="admin_companies")
     * @param Request $request
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = 1)
    {

        $em = $this->getDoctrine()->getManager();

        $searchForm = $this->createForm(SearcherType::class);
        $searchForm->handleRequest($request);

        $phrase = $searchForm->get('phrase')->getData();
        $query = $em->getRepository('UserBundle:Company')->findAllByPhrase($phrase);
        $pagination = $this->get('knp_paginator')->paginate($query, $page, $this->perPage);

        $template = ($request->isXmlHttpRequest()) ? 'UserBundle:Admin/company:company-list.html.twig' : 'UserBundle:Admin/company:index.html.twig';

        return $this->render(
            $template,
            array(
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            )
        );
    }

    /**
     * @Route("/editor/{id}", name="admin_company_editor", requirements={"id": "\d+"})
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function editorAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $attributeValueRepository = $em->getRepository('CaseBundle:AttributeValue');

        if ($id) {
            $entity = $em->getRepository('UserBundle:Company')->find($id);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono firmy'));
            }
        } else {
            $entity = new Company();
        }

        $partnerNameOptions = $attributeValueRepository->getPartnerNameOptions();

        $form = $this->createForm(CompanyType::class, $entity, [
            'partnerNameOptions' => $partnerNameOptions,
        ]);

        $form->setData($entity);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isValid()) {
            /** @var Company $company */
            $company = $form->getData();

            if(null !== $company->getPartnerLocationId()) {
                $partnerData = $attributeValueRepository->getPartnerData($company->getPartnerLocationId(), '595,631');
                $company->setCode($partnerData->getValueString());
            }

            $em->persist($company);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Firma "%name%" została pomyślnie zapisana',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_companies'));
        }

        return $this->render(
            'UserBundle:Admin/company:editor.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }


    /**
     * @Route("/delete/{id}", name="admin_company_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('UserBundle:Company')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono firmy'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Firma "%name%" została pomyślnie usunięta',
                ['%name%' => $entity->getName()]
            )
        );

        return $this->redirect($this->generateUrl('admin_companies'));

    }

}
