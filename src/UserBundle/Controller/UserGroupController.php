<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\UserGroupTool;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;
use UserBundle\Form\Type\GroupAddUserType;
use UserBundle\Form\Type\UserGroupType;

class UserGroupController extends Controller
{

    /**
     * @Route("/{page}", requirements={"page" = "\d+"}, name="admin_user_groups")
     * @param Request $request
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = 1)
    {

        $em = $this->getDoctrine()->getManager();

        $searchForm = $this->createFormBuilder()
            ->add(
                'phrase',
                TextType::class,
                [
                    'required' => false,
                    'attr' => [
                        'class' => 'lower',
                        'placeholder' => $this->get('translator')->trans('Wpisz frazę aby wyszukać...'),
                    ],
                ]
            )
            ->setMethod('GET')
            ->getForm();

        $searchForm->handleRequest($request);

        $perPage = 30;
        $phrase = $searchForm->get('phrase')->getData();

        $query = $em->getRepository('UserBundle:Group')->findAllForToolByPhrase($phrase);
        $pagination = $this->get('knp_paginator')->paginate($query, $page, $perPage);

        $template = ($request->isXmlHttpRequest()) ? 'UserBundle:Admin:user-group/partials/group-list.html.twig' : 'UserBundle:Admin/user-group:index.html.twig';

        return $this->render(
            $template,
            array(
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView(),
            )
        );

    }

    /**
     * @Route("/editor/{id}/{page}", name="admin_user_group_editor", requirements={"id": "\d+", "page" : "\d+"})
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function editorAction(Request $request, $id = null, $page = 1)
    {
        $em = $this->getDoctrine()->getManager();

        if ($id) {
            $entity = $em->getRepository('UserBundle:Group')->find($id);
            if (!$entity) {
                throw $this->createNotFoundException(
                    $this->get('translator')->trans('Nie znaleziono grupy użytkowników')
                );
            }
        } else {
            $entity = new Group('Nowa grupa');
        }

        $allTools = $em->getRepository('ToolBundle:Tool')->findAll();

        $selected = ($entity->getId()) ? $entity->getTools(true) : [];

        $orderedTools = [];
        $orderedTools = array_merge($orderedTools, $selected);

        foreach ($allTools as $tool) {
            if(!in_array($tool, $selected, true)) $orderedTools[] = $tool;
        }

        $addUsersForm = $this->createForm(GroupAddUserType::class, $entity, ['group' => $entity]);

        $form = $this->createForm(UserGroupType::class, $entity);

        $form->add('multiselectTools', ChoiceType::class, [
            'mapped' => false,
            'multiple' => true,
            'choices' => $orderedTools,
            'choice_label' => 'name',
            'choice_value' => 'id',
            'data' => $selected,
            'required' => false
        ])->add('defaultToolsString', HiddenType::class, [
            'attr' => ['class' => 'hide'],
            'mapped' => false,
            'data' => implode(',', array_map(
                function ($tool) {
                    /** @var Tool $tool */
                    return $tool->getId();
                },
                $selected
            )),
            'required' => false
        ]) ;

        $form->setData($entity);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isValid()) {

            /** Usunięcie obecnych narzędzi*/

            $groupTools = $em->getRepository('ToolBundle:UserGroupTool')->findBy(['group' => $entity]);
            foreach ($groupTools as $item) {
                $em->remove($item);
            }
            $em->flush();

            $defaultToolsString = $request->request->get('atlas_user_group')['defaultToolsString'];

            if($defaultToolsString)
            {
                /** Dodanie nowych */

                $defaultToolsArray = explode(',', $defaultToolsString);
                $position = 1;

                foreach ($defaultToolsArray as $id) {
                    foreach ($allTools as $tool) {
                        if($tool->getId() == $id)
                        {
                            $entity->addTool($tool, $position);
                            $position++;
                        }
                    }
                }
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Grupa użytkowników "%name%" została pomyślnie utworzona',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_user_groups'));
        }

        return $this->render(
            'UserBundle:Admin/user-group:editor.html.twig',
            array(
                'form' => $form->createView(),
                'addUsersForm' => $addUsersForm->createView(),
                'tools' => $allTools,
                'entity' => $entity,
                'page' => $page,
            )
        );

    }


    /**
     * @Route("/delete/{id}", name="admin_user_group_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('UserBundle:Group')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono grupy użytkowników'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Grupa użytkowników "%name%" została pomyślnie usunięta',
                ['%name%' => $entity->getName()]
            )

        );

        return $this->redirect($this->generateUrl('admin_user_groups'));

    }

    /**
     * @Route("/add-to-group/{groupId}", requirements={"groupId" = "\d+"}, name="admin_add_user_to_group")
     * @Method("POST")
     */
    public function addUserToGroupAction(Request $request, $groupId)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Group $group */
        $group = $em->getRepository('UserBundle:Group')->find($groupId);

        $addUsersForm = $this->createForm(GroupAddUserType::class, $group, ['group' => $group, 'query_builder' => false]);

        /** W Request idą tylko nowi użytkownicy, więc dodaje do requesta obecnych żeby w formularzu się zgadzało */

        $atlas_add_user_to_group = $request->request->get('atlas_add_user_to_group');

        if($atlas_add_user_to_group['users']) {
            /** @var User $item */
            foreach ($group->getUsers()->toArray() as $item) {
                $atlas_add_user_to_group['users'][] = (string)$item->getId();
            }
            $request->request->set('atlas_add_user_to_group', $atlas_add_user_to_group);
        }

        $addUsersForm->handleRequest($request);

        if ($request->isMethod('POST')) {
            $users = $addUsersForm->get('users')->getData();

            foreach ($users as $user) {
                if (!$user->hasGroup($group)) {
                    $group->addUser($user);
                }
            }

            $now = new \DateTime();
            $group->setUpdatedAt($now);
            $em->persist($group);
            $em->flush();
        }

        return new RedirectResponse($this->generateUrl('admin_user_group_editor', ['id' => $group->getId()]));
    }

    /**
     * @Route("/remove-user-from-group", requirements={"userId" = "\d+", "groupId" = "\d+"}, name="admin_remove_user_from_group")
     * @Method("POST")
     */
    public function removeUserFromGroupAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $request->request->get('userId', null);
        $groupId = $request->request->get('groupId', null);

        if ($userId && $groupId) {
            $user = $em->getRepository('UserBundle:User')->find($userId);
            $group = $em->getRepository('UserBundle:Group')->find($groupId);

            if ($group->getUsers()->contains($user)) {
                $group->removeUser($user);
                $em->persist($group);
                $em->flush();
            }
        }

        return new JsonResponse("OK");
    }

}
