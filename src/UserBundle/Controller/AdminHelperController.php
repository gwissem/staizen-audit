<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\Translator;
use UserBundle\Entity\User;

/**
 * Class PersonalizationController
 * @package UserBundle\Controller
 */

class AdminHelperController extends Controller
{

    /**
     * @Route("/", name="admin_helper_index")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $trans = $this->get('translator');

        $tests = [
            [$trans->trans('Następny miesiąc'), '{$ M+1 $}'],
            [$trans->trans('Następny dzień / jutro'), '{$ D+1 $}'],
            [$trans->trans('2 miesiące wstecz'), '{$ M-2 $}'],
            [$trans->trans('9 dni wstecz'), '{$ D-9 $}'],
            [$trans->trans('18 dni do przodu'), '{$ D+18 $}'],
            [$trans->trans('Ostatni dzień bieżącego roku'), '{$ M12.!last-d $}'],
            [$trans->trans('Pierwszy dzień miesiąca'), '{$ !first-d $}'],
            [$trans->trans('Ostatni dzień przyszłego miesiąca'), '{$ M+1.!last-d $}'],
            [$trans->trans('1 dzień i 12 godzin temu'), '{$ D-1.H-12 $}'],
            [$trans->trans('Wczoraj o 15:00'), '{$ D-1.H15.I0 $}'],
        ];
        
        $examples = [];

        $parser = $this->get('tool.date_parser.service');

        $dateStr = 'NOW';
        $date = new \DateTime($dateStr);

        foreach ($tests as $key => $test) {

            $examples[] = [
                $test[0],
                $test[1],
                $parser->parseDate($test[1], new \DateTime($dateStr)),
            ];
        }

        $userInfo = $this->get('user.info')->getInfo();

        $globalParameters = [
            ['name' => $trans->trans('Id bieżącego użytkownika'), 'param' => '{#userId#}'],
            [
                'name' => $trans->trans(
                    'Id oryginalnego użytkownika (jeśli zalogowany przez zastępstwo)'
                ),
                'param' => '{#originalUserId#}',
            ],
            ['name' => $trans->trans('Id firmy bieżącego użytkownika'), 'param' => '{#companyId#}'],
            ['name' => $trans->trans('Grupy bieżącego użytkownika'), 'param' => '{#userGroups#}'],
            ['name' => $trans->trans('Język'), 'param' => '{#locale#}'],
            ['name' => $trans->trans('Nazwa zalogowanego użytkownika'), 'param' => '{#userName#}'],
            ];

        $globalParameters[0]['result'] = $userInfo['userId'];
        $globalParameters[1]['result'] = $userInfo['originalUserId'];
        $globalParameters[2]['result'] = $userInfo['companyId'];
        $globalParameters[3]['result'] = $userInfo['userGroups'];
        $globalParameters[4]['result'] = $userInfo['locale'];
        $globalParameters[5]['result'] = $userInfo['userName'];

        $procedureParameters = $this->getParametersForProcedure($trans);

        $parserParameters = $this->getParserParameters();

        $rolesDescription = $this->getRolesDescription();

        return $this->render('@User/Admin/helper/index.html.twig', [
            'examples' => $examples,
            'for_date' => $date->format('Y-m-d H:i:s'),
            'globalParameters' => $globalParameters,
            'procedureParameters' => $procedureParameters,
            'parserParameters' => $parserParameters,
            'rolesDescription' => $rolesDescription
        ]);
    }

    private function getRolesDescription() {

        return [
            USER::ROLE_ADMIN => [
                /** @Desc("Admin") */
                'name' => $this->get('translator')->trans('app.role.admin'),
                'desc' => 'Wszechmocny Administrator.'
            ],
            USER::ROLE_SUPER_ADMIN => [
                /** @Desc("Super Admin") */
                'name' => $this->get('translator')->trans('app.role.super_admin'),
                'desc' => 'Super Uber Administrator.'
            ],
            User::ROLE_PROCESS_EDITOR => [
                /** @Desc("Edycja procesów") */
                'name' => $this->get('translator')->trans('app.role.process_editor'),
                'desc' => 'Edycja procesów w Atlasie.'
            ],
            User::ROLE_PROCESS_VIEWER => [
                /** @Desc("Podgląd procesów") */
                'name' => $this->get('translator')->trans('app.role.process_viewer'),
                'desc' => 'Podgląd procesów w Atlasie.'
            ],
            User::ROLE_CHAT => [
                /** @Desc("Czat") */
                'name' => $this->get('translator')->trans('app.role.chat'),
                'desc' => 'Dostęp do czata.'
            ],
            User::ROLE_TRANSLATOR => [
                /** @Desc("Tłumacz") */
                'name' => $this->get('translator')->trans('app.role.translator'),
                'desc' => 'Tłumacz'
            ],
            USER::ROLE_DASHBOARD_INTERFACE => [
                /** @Desc("Dostęp do formularzy") */
                'name' => $this->get('translator')->trans('app.role.operational.interface'),
                'desc' => 'Dostęp do ładowania formularzy.'
            ],
            USER::ROLE_OPERATIONAL_DASHBOARD => [
                /** @Desc("Dostęp do pulpitu operacyjnego") */
                'name' => $this->get('translator')->trans('app.role.operational.dashboard'),
                'desc' => 'Dostęp do Pulpitu Operacyjnego (+ ROLE_DASHBOARD_INTERFACE)'
            ],
            USER::ROLE_TASK_MANAGER => [
                /** @Desc("TASK_MANAGER") */
                'name' => $this->get('translator')->trans('app.role.task.manager'),
                'desc' => 'Do wywalenia...'
            ],
            USER::ROLE_SCENARIO_MODERATOR => [
                /** @Desc("Dostęp do scenariuszy") */
                'name' => $this->get('translator')->trans('app.role.scenario_moderator'),
                'desc' => ''
            ],
            USER::ROLE_CASE_MASTER_EDITOR => [
                /** @Desc("Edycja danych w sprawie") */
                'name' => $this->get('translator')->trans('app.role.case_master_editor'),
                'desc' => ''
            ],
            USER::ROLE_SUPERVISOR => [
                /** @Desc("Supervisor") */
                'name' => $this->get('translator')->trans('app.role.role_supervisor'),
                'desc' => ''
            ],
            USER::ROLE_PARTNER_VIEWER => [
                /** @Desc("Podgląd panelu partnerów") */
                'name' => $this->get('translator')->trans('app.role.partner_viewer'),
                'desc' => ''
            ],
            USER::ROLE_PARTNER_EDITOR => [
                /** @Desc("Edytor panelu partnerów oraz VIP") */
                'name' => $this->get('translator')->trans('app.role.partner_editor'),
                'desc' => ''
            ],
            USER::ROLE_HIDE_NOTES => [
                /** @Desc("Ukrywanie panelu notatek") */
                'name' => $this->get('translator')->trans('app.role.hide_notes'),
                'desc' => ''
            ],
            USER::ROLE_TESTER => [
                /** @Desc("Tester") */
                'name' => $this->get('translator')->trans('app.role.tester'),
                'desc' => ''
            ],
            USER::ROLE_CASE_SERVICE_ACTIONS => [
                /** @Desc("Dzwoneczki") */
                'name' => $this->get('translator')->trans('app.role.case_service_actions'),
                'desc' => ''
            ],
            USER::ROLE_ANALYST => [
                /** @Desc("Analiza") */
                'name' => $this->get('translator')->trans('app.role.analyst'),
                'desc' => ''
            ],
            USER::ROLE_CASE_FORWARD => [
                /** @Desc("Wywoływanie rozliczenia") */
                'name' => $this->get('translator')->trans('app.role.case_forward'),
                'desc' => ''
            ],
            USER::ROLE_CASE_SERVICE_ADVANCE => [
                /** @Desc("Rozliczenia 'Plusiki'") */
                'name' => $this->get('translator')->trans('app.role.case_service_advance'),
                'desc' => ''
            ],
            USER::ROLE_RECORDING_CALL => [
                /** @Desc("Odsłuch rozmowy") */
                'name' => $this->get('translator')->trans('app.role.recording_call'),
                'desc' => ''
            ],
            USER::ROLE_ATLAS_MONITORING => [
                /** @Desc("Raport 'Monitoring automatów'") */
                'name' => $this->get('translator')->trans('app.role.atlas_monitoring'),
                'desc' => ''
            ],
            USER::ROLE_PHONE_STATISTIC => [
                /** @Desc("Statystyki telefonii") */
                'name' => $this->get('translator')->trans('app.role.phone_statistic'),
                'desc' => ''
            ],
            USER::ROLE_PUSH_CASE => [
                /** @Desc("Push zadań") */
                'name' => $this->get('translator')->trans('app.role.push_case'),
                'desc' => 'Losowanie i wrzucanie zadań konsultantowi'
            ],
            USER::ROLE_FINANCE_VIEW => [
                /** @Desc("Finansowy podgląd sprawy") */
                'name' => $this->get('translator')->trans('app.role.finance_view'),
                'desc' => ''
            ],
            USER::ROLE_KZ => [
                /** @Desc("KZ") */
                'name' => $this->get('translator')->trans('app.role.kz'),
                'desc' => ''
            ],
            USER::ROLE_CASE_VIEWER => [
                /** @Desc("Podgląd spraw (narzędzie) ") */
                'name' => $this->get('translator')->trans('app.role.case_viewer'),
                'desc' => ''
            ],
            USER::ROLE_KCZ => [
                /** @Desc("KCZ") */
                'name' => $this->get('translator')->trans('app.role.kcz'),
                'desc' => ''
            ],
            USER::ROLE_TELEPHONY => [
                /** @Desc("Telefonia") */
                'name' => $this->get('translator')->trans('app.role.telephony'),
                'desc' => 'Dostęp do telefonii'
            ],
            USER::ROLE_CONSULTANT => [
                /** @Desc("Konsultant") */
                'name' => $this->get('translator')->trans('app.role.consultant'),
                'desc' => 'Po prostu konsultant'
            ],
            USER::ROLE_CONSULTANT_ADVANCED => [
                /** @Desc("Konsultant Zaawansowany") */
                'name' => $this->get('translator')->trans('app.role.consultant_advanced'),
                'desc' => 'Konsultant zaawansowany'
            ],
            USER::ROLE_CFM_CASE_VIEWER => [
                /** @Desc("Atlas Viewer") */
                'name' => $this->get('translator')->trans('app.role.cfm_case_viewer'),
                'desc' => 'Atlas Viewer'
            ],
            USER::ROLE_HIDE_GUEST_TOOLS => [
                /** @Desc("Ukryj narzędzia dla gości") */
                'name' => $this->get('translator')->trans('app.role.hide_guest_tools'),
                'desc' => 'Ukryj narzędzia dla gości'
            ],
            USER::ROLE_USER_AS_SYSTEM => [
                /** @Desc("Użytkownik jako system") */
                'name' => $this->get('translator')->trans('app.role.user_as_system'),
                'desc' => 'Użytkownik jest traktowany jako użytkownik systemowy'
            ],
            USER::ROLE_BLOGGER => [
                /** @Desc("Dodawanie treści do bloga") */
                'name' => $this->get('translator')->trans('app.role.blogger'),
                'desc' => 'Użytkownik może dodać treść do bloga'
            ]

        ];
    }

    private function getParserParameters() {
        $parameters = [];
        $parameters[] = ['name' => '{@123,456,789@}', 'desc' => 'Wyświetlanie wartości atrybutu - {@PATH_TO_ATTRIBUTE@}'];
        $parameters[] = ['name' => '"{@253*@}" in ["11", "58"]', 'desc' => 'Sprawdzanie elementu w tablicy'];
        $parameters[] = ['name' => '{#stepid#}', 'desc' => 'StepId'];
        $parameters[] = ['name' => '{#groupid#}', 'desc' => 'groupProcessInstanceId'];
        $parameters[] = ['name' => '{#instanceid#}', 'desc' => 'processInstanceId'];
        $parameters[] = ['name' => '{#caseid#}', 'desc' => 'A + rootId'];
        $parameters[] = ['name' => '{#rootid#}', 'desc' => 'rootId'];
        $parameters[] = ['name' => '{#functionName(param1|param2|param3)#}', 'desc' => 'Uruchomienie funkcji zdefiniowanej w parserMethods.php'];
        $parameters[] = ['name' => '{#displayMulti(MULTI_PATH|EXTRA_DATA |CHILD_ID=LABEL=PARSER|CHILD_2_ID=LABEL_2=PARSER)#}', 'desc' => 'Funkcja do wyświetlania elementów multi. Parserem może być nazwa metody w klasie "ParserMethods" lub "typeD" ze słownika z przedroskiem "d_". Przykład: {#displayMulti(701,702|class:row,empty-row:false|704:Nazwa lokalizacji:partnerName|703:Powód odmowy:d_infoRefuseReasonPZ)#}.  Drugi argument to ExtraData, można podać dodatkową klasę do wrappera tabelki, i ustawić czy ma się pokazywać napis "Brak wyników", przy braku wyników.'];
        $parameters[] = ['name' => '...', 'desc' => '...i wiele wiele więcej'];

        return $parameters;
    }

    private function getParametersForProcedure($trans)
    {
        $parameters = [];
        $parameters[] = ['name' => '{@date@}', 'desc' => $trans->trans('Data otrzymania e-maila')];
        $parameters[] = ['name' => '{@subject@}', 'desc' => $trans->trans('Temat e-maila')];
        $parameters[] = ['name' => '{@fromName@}', 'desc' => $trans->trans('Adresat e-maila (nazwa)')];
        $parameters[] = ['name' => '{@fromAddress@}', 'desc' => $trans->trans('Adresat e-maila (adres email)')];
        $parameters[] = ['name' => '{@toString@}', 'desc' => $trans->trans('Odbiorcy e-maila (lista odbiorców oddzielona przecinkiem)')];
        $parameters[] = ['name' => '{@textPlain@}', 'desc' => $trans->trans('Zawartość e-maila bez znaczników HTML')];
        $parameters[] = ['name' => '{@documentId@}', 'desc' => $trans->trans('Id dokumentu do którego jest dołączony e-mail (.eml)')];
        $parameters[] = ['name' => '{@caseNumber@}', 'desc' => $trans->trans('Próba wyciągnięcia numeru sprawy z tematu e-mailu')];

        return $parameters;
    }

    /**
     * @Route("/parse-date", options={"expose"=true}, name="admin_helper_parse_date")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */

    public function parseDateAction(Request $request)
    {
        /** Check Ajax Method */
        if($request->isXmlHttpRequest())
        {
            $response = new JsonResponse();

            $data = $request->request->all();
            
            $parser = $this->get('tool.date_parser.service');
            $date = $parser->parseDate($data['parse_date'], new \DateTime($data['date']));

            $response->setData(['date' => $date] );

            return $response;
        }

        throw new AccessDeniedException('Only Ajax Method');
    }

    /**
     * @Route("/parse-string", options={"expose"=true}, name="admin_helper_parse_string")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */

    public function parseStringAction(Request $request)
    {
        /** Check Ajax Method */
        if($request->isXmlHttpRequest())
        {

            $string = $request->request->get('string', "");
            $processInstanceId = $request->request->get('processInstanceId', null);

            $result = $this->get('case.service.helper_parser_service')->parseString($string, $processInstanceId, true);

            return new JsonResponse($result);
        }

        throw new AccessDeniedException('Only Ajax Method');
    }

}
