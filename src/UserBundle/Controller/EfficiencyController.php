<?php

namespace UserBundle\Controller;

use MssqlBundle\PDO\PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;

class EfficiencyController extends Controller
{

    /**
     * @Method("GET")
     * @Rest\Route("/{id}/current", name="user-efficiency-current", options={"expose":true})
     */
    public function getEfficiencyLast($id)
    {
        $points = $this->getEfficiencyPoints($id);
        return new JsonResponse(['points' => $points]);
    }

    /**
     * @Method("GET")
     * @Rest\Route("/{id}/historical", name="user-efficiency-historical", options={"expose":true})
     */
    public function getEfficiencySummed($id)
    {
        $points = $this->getEfficiencyPoints($id, 1);
        return new JsonResponse(['points' => $points]);
    }


    /**
     * @param $currentUserId
     * @Method("GET")
     * @Rest\Route("/ranking/month",name="user-efficiency-rank-month", options={"expose":true})
     */
    public function getEfficiencyCurrentMonthRankingPrepared(Request $request)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $currentUserId = $user->getId();
        $queryManager = $this->get('app.query_manager');
        $parameters =[
            [
                'key' => 'type',
                'value' => 2,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' =>'user',
                'value' =>$currentUserId,
                'type' => PDO::PARAM_INT
            ]
        ];
        $resultTable = $queryManager->executeProcedure(
            "EXEC efficiency_ranking @type = :type, @user = :user",
            $parameters
        );

        $view = $this->render('UserBundle:Efficiency:ranking-table.hml.twig',
            [
                'user_id' => $currentUserId,
                'result_table' => $resultTable,
                'ranking_type' => 'all-time'
            ]
        )->getContent();


        return new JsonResponse(['html' => $view]);
    }

    /**
     * @param $currentUserId
     * @Method("GET")
     * @Rest\Route("/ranking/recent",name="user-efficiency-rank-recent", options={"expose":true})
     */
    public function getCurrentRankingPrepared()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $currentUserId = $user->getId();
        $queryManager = $this->get('app.query_manager');
        $parameters =[
            [
                'key' => 'type',
                'value' => 1,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' =>'user',
                'value' =>$currentUserId,
                'type' => PDO::PARAM_INT
            ]
        ];
        $resultTable = $queryManager->executeProcedure(
            "EXEC efficiency_ranking @type = :type, @user = :user",
            $parameters
        );


        $view = $this->render('UserBundle:Efficiency:ranking-table.hml.twig',
            [
                'user_id' => $currentUserId,
                'result_table' => $resultTable,
                'ranking_type' => 'all-time'
            ]
        )->getContent();


        return new JsonResponse(['html' => $view]);
    }


    private function getEfficiencyPoints($userId, $accumulated = 0)
    {
        $queryManager = $this->get('app.query_manager');

        $parameters = [
            [
                'key' => 'userId',
                'value' => intval($userId),
                'type' => PDO::PARAM_INT
            ],
            [

                'key' => 'accumulated',
                'value' => $accumulated,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'points',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100

            ]
        ];

        $points = $queryManager->executeProcedure("EXEC Atlasdb_def.dbo.p_agentstatistics_speed @userid = :userId, @accumulated = :accumulated, @points = :points", $parameters);

        $points = is_array($points) && isset($points[0]) ? $points[0]['points'] : 0;
        return $points;
    }


    /**
     * @Method("GET")
     * @Rest\Route("/ranking/stats", name="user-efficiency-stats-global", options={"expose" :true})
     */
    public function getStatistics()
    {
        $current = $this->getStatisticMonth();
        $month = $this->getStatisticMonth();

        $view = $this->render('UserBundle:Efficiency:stats.html.twig',
            [
                'stats' =>
                    [
                        'current' =>
                            [
                                'title'=> 'KPI dzisiaj',
                                'stats'=>$current
                            ],
                        'month' =>
                            [
                                'title'=> 'KPI od początku miesiąca',
                                'stats'=>$month
                            ],

                    ]
            ]
        )->getContent();
        return new JsonResponse(['html' => $view]);

    }




    /**
     * @Method("GET")
     * @Rest\Route("/ranking/full/recent", name="user-efficiency-ranking-full-recent", options={"expose":true})
     */
    public function getFullRankingRecent(){

        $queryManager = $this->get('app.query_manager');
        // Zrobione w sposób wklejenia id w query, @todo:ogarnąć
        $parameters =[
            [
                'key' => 'type',
                'value' => 1,
                'type' => PDO::PARAM_INT
            ]
        ];
        $resultTable = $queryManager->executeProcedure(
            "EXEC efficiency_ranking @type = :type",
            $parameters
        );


        $view = $this->render('UserBundle:Efficiency:ranking-table-custom.html.twig',
            [
                'user_id' => null,
                'result_table' => $resultTable,
                'ranking_type' => 'all-time'
            ]
        )->getContent();

        return new Response($view);

    }

    /**
     * @Method("GET")
     * @Rest\Route("/ranking/full/month", name="user-efficiency-ranking-full-month", options={"expose":true})
     */
    public function getFullRankingMonth(){

        $queryManager = $this->get('app.query_manager');
        $parameters =[
            [
                'key' => 'type',
                'value' => 2,
                'type' => PDO::PARAM_INT
            ]
        ];
        $resultTable = $queryManager->executeProcedure(
            "EXEC efficiency_ranking @type = :type",
            $parameters
        );


        $view = $this->render('UserBundle:Efficiency:ranking-table-custom.html.twig',
            [
                'user_id' => null,
                'result_table' => $resultTable,
                'ranking_type' => 'all-time'
            ]
        )->getContent();

        return new Response($view);

    }

    /**
     * @Method("GET")
     * @Rest\Route("/ranking/{user}/month", name="user-efficiency-ranking-user-month", options={"expose":true})
     */
    public function getRankingUserMonth($user){

        $queryManager = $this->get('app.query_manager');
        $userId = $user;
        $parameters =[
            [
                'key' => 'type',
                'value' => 2,
                'type' => PDO::PARAM_INT
            ]
            ,[
                'key' => 'user',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ]
        ];
        $resultTable = $queryManager->executeProcedure(
            "EXEC efficiency_ranking @type = :type,@user = :user",
            $parameters
        );


        $view = $this->render('UserBundle:Efficiency:ranking-table-custom.html.twig',
            [
                'user_id' => $userId,
                'result_table' => $resultTable,
                'ranking_type' => 'all-time'
            ]
        )->getContent();

        return new Response($view);

    }
    /**
     * @Method("GET")
     * @Rest\Route("/ranking/{user}/recent", name="user-efficiency-ranking-user-recent", options={"expose":true})
     */
    public function getRankingUserRecent($user){

        $queryManager = $this->get('app.query_manager');
        $userId = $user;
        $parameters =[
            [
                'key' => 'type',
                'value' => 1,
                'type' => PDO::PARAM_INT
            ]
            ,[
                'key' => 'user',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ]
        ];
        $resultTable = $queryManager->executeProcedure(
            "EXEC efficiency_ranking @type = :type,@user = :user",
            $parameters
        );


        $view = $this->render('UserBundle:Efficiency:ranking-table-custom.html.twig',
            [
                'user_id' => $userId,
                'result_table' => $resultTable,
                'ranking_type' => 'all-time'
            ]
        )->getContent();

        return new Response($view);

    }


    private function getStatisticMonth()
    {
        $currentMonth = new \DateTime();
        $queryManager = $this->get('app.query_manager');
        $parameters = [
            [
                'key' => 'yearMonth',
                'value' => $currentMonth->format('Y-m'),
                'type' => PDO::PARAM_STR
            ]
        ];

        return $queryManager->executeProcedure('select SOA, SR from AtlasDB_calls.dbo.KPI where yearMonth = :yearMonth', $parameters);
    }
}
