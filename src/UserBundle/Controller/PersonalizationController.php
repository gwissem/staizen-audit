<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Service\UserPersonalizationService;

/**
 * Class PersonalizationController
 * @package UserBundle\Controller
 * @Route("/personalization", service="user.personalization.controller")
 */

class PersonalizationController extends Controller
{
    
    /** @var  UserPersonalizationService */
    protected $personService;
    
    public function __construct($container)
    {
        $this->container = $container;
        $this->personService = $this->get('user.personalization.service');
        $this->personService->setUser($this->getUser());
    }

    /**
     * @Route("/{option}", options={"expose"=true}, name="user_personalization_update")
     * @Method("POST")
     * @param Request $request
     * @param $option
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateOption(Request $request, $option)
    {

        /** Check Ajax Method */
        if($request->isXmlHttpRequest())
        {
            if(!$option)
            {
                return new JsonResponse([], 500);
            }

            $response = new JsonResponse();

            $data = $request->request->get('personalization');

            $response->setData($this->personService->updateOption($option, $data));

            return $response;
        }

        throw new AccessDeniedException('Only Ajax Method');
    }

}
