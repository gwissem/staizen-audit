<?php

namespace UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'Nazwa firmy',
                )
            )
            ->add(
                'partnerLocationId',
                ChoiceType::class,
                [

                    'label' => 'Partner',
                    'choices' => $options['partnerNameOptions'],
                    'attr' => ['class' => 'select2-deselect'],
                    'required' => false,
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => 'Typ Firmy',
                    'choices' => [
                        'Klient' => null,
                        'ICS' => 1
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'UserBundle\Entity\Company',
                'partnerNameOptions' => null,
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_company';
    }

}