<?php

namespace UserBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupAddUserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $group = $options['group'];

        $builder
            ->add(
                'users',
                null,
                [
                    'label' => false,
                    'attr' => [
                        'class' => 'select2-deselect',
                        'placeholder' => 'Dodaj użytkowników',
                    ],
                    'multiple' => true,
                    'query_builder' => ($options['query_builder']) ? function (EntityRepository $er) use ($group) {
                        return $er->createQueryBuilder('u')
                            ->leftJoin('u.groups', 'g', 'WITH', 'g.id = :groupId')
                            ->groupBy('u')
                            ->having('count(g) = 0')
                            ->setParameter('groupId', $group->getId());

                    } : null,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'UserBundle\Entity\Group',
                'group' => null,
                'query_builder' => true
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_add_user_to_group';
    }

}