<?php

namespace UserBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class PermissionTransferType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];

        $builder
            ->add(
                'toUser',
                null,
                [
                    'label' => 'Użytkownik',
                    'attr' => ['class' => 'select2-deselect'],
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) use ($user) {
                        return $er->createQueryBuilder('u')
                            ->innerJoin('u.company', 'c')
                            ->where('u.company = :company')
                            ->andWhere('u.id != :userId')
                            ->andWhere('u.roles not like :admin')
                            ->orderBy('u.lastname', 'ASC')
                            ->setParameter('admin', "'%ROLE_ADMIN%'")
                            ->setParameter('userId', $user->getId())
                            ->setParameter('company', $user->getCompany());
                    },
                ]
            )
            ->add(
                'fromDate',
                DateTimeType::class,
                [
                    'label' => 'Od (data)',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'attr' => array(
                        'class' => 'date-time-picker',
                    ),
                ]
            )
            ->add(
                'toDate',
                DateTimeType::class,
                [
                    'label' => 'Do (data)',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false,
                    'attr' => array(
                        'class' => 'date-picker',
                    ),
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'UserBundle\Entity\PermissionTransfer',
                'user' => null,
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_user_permission_transfer';
    }

}