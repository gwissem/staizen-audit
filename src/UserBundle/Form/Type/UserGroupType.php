<?php

namespace UserBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;

class UserGroupType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $id = $builder->getData()->getId();

        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'Nazwa grupy',
                )
            )
            ->add(
                'parent',
                EntityType::class,
                array(
                    'class' => 'UserBundle:Group',
                    'query_builder' => function (EntityRepository $er) use ($id) {
                        $query = $er->createQueryBuilder('g')
                            ->orderBy('g.root', 'ASC')
                            ->addOrderBy('g.lvl', 'ASC')
                            ->addOrderBy('g.name', 'ASC');
                        if ($id) {
                            $query
                                ->where('g.id != :groupId')
                                ->andWhere('g.parent IS NULL OR g.parent != :groupId')
                                ->setParameter('groupId', $id);
                        }

                        return $query;
                    },
                    'choice_label' => 'indentedName',
                    'required' => false,
                )
            )
            ->add(
                'roles',
                ChoiceType::class,
                array(
                    'label' => 'Rola',
                    'multiple' => true,
                    'choices' => User::getAppRoles(),
                    'attr' => ['class' => 'select2-deselect'],
                    'required' => false,
                )
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'UserBundle\Entity\Group',
                'parentChoices' => null,
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_user_group';
    }

}