<?php

namespace UserBundle\Form\Type;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use UserBundle\Entity\Company;
use UserBundle\Entity\User;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var User $entity */
        $entity = $options['data'];

        $role = User::ROLE_DEFAULT;

        if($entity->getId())
        {
            if(in_array(User::ROLE_ADMIN, $entity->getRoles())) $role = User::ROLE_ADMIN;
            $builder->remove('plainPassword');

        } else {
            $builder
                ->add('auto_generate', CheckboxType::class, array(
                    'label' => "Wygeneruj hasło",
                    'required' => false,
                    'mapped' => false
                ));
        }

        $builder->remove('email')
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array(
                'label' => 'form.email',
                'required' => false,
                'translation_domain' => 'FOSUserBundle',
                'constraints' => [new Email()]
            ));

        $builder
            ->add('firstname', TextType::class, array(
                'label' => 'Imię',
                'required' => false
            ))
            ->add('lastname', TextType::class, array(
                'label' => 'Nazwisko',
                'required' => false
            ))
            ->add(
                'company',
                null,
                array(
                    'label' => 'Firma',
                    'choice_label' => function (Company $company) {
                        return sprintf('%s (id: %s, partner: %s)', $company->getName(), $company->getId(), $company->getPartnerLocationId());
                    },
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect'],
                )
            )
            ->add(
                'roles',
                ChoiceType::class,
                array(
                    'label' => 'Rola',
                    'multiple' => true,
                    'choices' => User::getAppRoles(),
                    'attr' => ['class' => 'select2-deselect'],
                    'required' => false,
                )
            )
            ->add('groups', null, array(
                'attr' => ['class' => 'select2-deselect'],
                'multiple' => true,
            ))
            ->add('cucumExtension', null, [
                'label' => 'atlas_user.form.jabber_number',
                'attr' => ['readonly' => true]
            ])
        ;

    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'UserBundle\Entity\User',
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'atlas_user';
    }

}