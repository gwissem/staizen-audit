<?php
namespace UserBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use UserBundle\Entity\User;
use WebBundle\Service\CachedMenuService;

class UserPersonalizationService
{
    
    const SIDEBAR_NAV_ITEMS = 'sidebar-nav-items';
    const FILTER_TOGGLE = 'filter_toggle';

    /** @var  ContainerInterface */
    protected $container;

    /** @var  EntityManager */
    protected $em;

    /** @var  Request */
    protected $request;

    /** @var  User */
    protected $user;

    /** @var CachedMenuService */
    protected $cachedMenuService;

    /**
     * UserPersonalizationService constructor.
     * @param ContainerInterface $container
     * @param EntityManager $entityManager
     * @param RequestStack $requestStack
     * @param CachedMenuService $cachedMenuService
     */
    public function __construct(ContainerInterface $container, EntityManager $entityManager, RequestStack $requestStack, CachedMenuService $cachedMenuService)
    {
        $this->container = $container;
        $this->em = $entityManager;
        $this->request = $requestStack->getCurrentRequest();
        $this->cachedMenuService = $cachedMenuService;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    static function getOptions()
    {
        return array(
            self::SIDEBAR_NAV_ITEMS
        );
    }

    /**
     * Sprawdza czy istnieje podana opcja personalizacji
     *
     * @param $option
     * @return bool
     */
    private function validOption($option)
    {
        return ( (substr($option, 0, 1) == "$") || (in_array($option, self::getOptions())));
    }

    private function createResponse($success, $msg)
    {
        return array(
            'success' => $success,
            'msg' => $msg
        );
    }

    private function getMsgResponse($optionName)
    {
        $messages = [
            self::SIDEBAR_NAV_ITEMS => 'Lista zaktualizowana',
            self::FILTER_TOGGLE => 'Widoczność filtra zaktualizowana'
        ];

        return (isset($messages[$optionName])) ? $messages[$optionName] : '';
    }

    private function generateFirstPersonalizationJson()
    {
        $options = [];
        foreach ($this->getOptions() as $option) {
            $options[$option] = '';
        }
        return $options;
    }

    public function updateOption($option, $data)
    {
        if(!$this->validOption($option))
        {
            return $this->createResponse(false, 'Option do not exists');
        }

        $resp = $this->runUpdateOption($option, $data);

        return $this->createResponse(true, $resp);
    }

    public function runUpdateOption($option, $data)
    {
        $options = $this->user->getPersonalizationJson();

        if(!$options) $options = $this->generateFirstPersonalizationJson();

        if(is_array($options) && array_key_exists($option, $options))
        {
            if (strpos($option, '$tool_col_width') !== false) {

                $options[$option] = array_merge($options[$option], $data);

            }
            else {
                $options[$option] = $data;
            }

        }
        else {
            /** TODO */
            $options[$option] = $data;
        }

        $this->user->setPersonalizationJson($options);
        $this->em->persist($this->user);
        $this->em->flush();

        $this->afterUpdate($option);

        return $this->getMsgResponse($option);
    }

    private function afterUpdate($option){
        if($option == self::SIDEBAR_NAV_ITEMS) {
            $this->cachedMenuService->clearRedisKey('userToolsMenu');
        }
    }

}