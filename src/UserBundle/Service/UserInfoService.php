<?php
namespace UserBundle\Service;

use AppBundle\Utils\QueryManager;
use MssqlBundle\PDO\PDO;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Role\SwitchUserRole;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class UserInfoService
{

    protected $token;
    protected $user;
    protected $originalUser;
    protected $company;
    protected $group;
    protected $tokenStorage;
    protected $requestStack;

    /** @var User|null */
    protected $virtualUser = null;

    public function __construct(TokenStorage $tokenStorage, RequestStack $requestStack)
    {
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
    }

    public function setVirtualUser(User $user){
        $this->virtualUser = $user;
    }

    public function getInfo()
    {
        $locale = 'pl';
        $userId = NULL;
        $originalUserId = NULL;
        $companyId = NULL;
        $userGroups = '';
        $userName = '';

        /* Wirtualny User - np. dla publicznego dashboard'u */
        if($this->virtualUser instanceof User) {
            $userId = $this->virtualUser->getId();
            $originalUserId = $userId;
            $companyId =  $this->virtualUser->getCompany() ? $this->virtualUser->getCompany()->getId() : null;
            $userGroups = $this->getUserGroups(true);
            $userName = $this->virtualUser->getName();
        }
        else if ($this->tokenStorage->getToken()) {
            $user = $this->getUser();
            $originalUser = $this->getOriginalUser();
            if ($user instanceof User && $originalUser instanceof User) {
                $userId = $user->getId();
                $originalUserId = $originalUser->getId();
                $companyId = $this->getCompany() ? $this->getCompany()->getId() : null;
                $userGroups = $this->getUserGroups(true);
                $userName = $user->getName();
            }
        }

        if(!empty($this->requestStack)){
            $currentRequest = $this->requestStack->getCurrentRequest();
            if($currentRequest){
                $locale = $currentRequest->getLocale();
            }
        }

        return [
            'userId' => $userId,
            'originalUserId' => $originalUserId,
            'companyId' => $companyId,
            'userGroups' => $userGroups,
            'locale' => $locale,
            'userName' => $userName
        ];

    }

    /**
     * @return int|null
     */
    public function getUserId() {

        $user = $this->getUser();
        return ($user) ? $user->getId() : null;

    }

    /**
     * @return int|null
     */
    public function getOriginalUserId() {

        $user = $this->getOriginalUser();
        return ($user) ? $user->getId() : null;

    }

    /**
     * @return null | User | string
     */
    public function getUser()
    {
        if($this->virtualUser !== null) {
            return $this->virtualUser;
        }

        $token = $this->tokenStorage->getToken();

        return ($token) ? $token->getUser() : null;

    }

    /**
     * @return mixed|null|User
     */
    public function getOriginalUser()
    {
        if($this->virtualUser !== null) return $this->virtualUser;
        if ($this->tokenStorage->getToken()) {
            $roles = $this->tokenStorage->getToken()->getRoles();
            foreach ($roles as $role) {
                if ($role instanceof SwitchUserRole) {
                    return $role->getSource()->getUser();
                }
            }

            return $this->getUser();
        }

        return null;

    }

    public function getCompany()
    {
        return $this->getUser()->getCompany();
    }

    public function getLocale($short = true){
        $user = $this->getUser();

        if(empty($user)){
            return;
        }

        $locale = $user->getLocale();

        if ($short = false){
            $localeArr = [
                'pl' => 'Polish',
                'en' => 'English',
                'ru' => 'Russian'
            ];
            $locale = $localeArr[$locale];
        }

        return $locale;
    }

    /**
     * @param $id
     * @param QueryManager $queryManager
     * @return array|null
     */
    public function getWorker($id, QueryManager $queryManager) {

        $parameters = [
            [
                'key' => 'userId',
                'value' => (int)$id,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $queryManager->executeProcedure('SELECT firstname, lastname, position FROM mbo.workers WHERE userId = :userId', $parameters);

        if(!empty($output)) {
            return $output[0];
        }

        return null;

    }

    public function getUserGroups($ids = false)
    {
        /** @var Group[] $groups */
        $groups = $this->getUser()->getGroups();
        if ($ids) {
            $groupsArray = [];
            if ($groups) {
                foreach ($groups as $group) {
                    $groupsArray[] = $group->getId();
                }
            }

            return implode(',', $groupsArray);
        }

        return $groups;
    }

    public function genApiAccessToken()
    {

        if (@file_exists('/dev/urandom')) { // Get 100 bytes of random data
            $randomData = file_get_contents('/dev/urandom', false, null, 0, 100);
        } elseif (function_exists('openssl_random_pseudo_bytes')) { // Get 100 bytes of pseudo-random data
            $bytes = openssl_random_pseudo_bytes(100, $strong);
            if (true === $strong && false !== $bytes) {
                $randomData = $bytes;
            }
        }
        // Last resort: mt_rand
        if (empty($randomData)) { // Get 108 bytes of (pseudo-random, insecure) data
            $randomData = mt_rand().mt_rand().mt_rand().uniqid(mt_rand(), true).microtime(true).uniqid(
                    mt_rand(),
                    true
                );
        }

        return rtrim(strtr(base64_encode(hash('sha256', $randomData)), '+/', '-_'), '=');
    }
}