<?php

namespace UserBundle\Service;

use CaseBundle\Service\ParserInterface;

class UserInfoParserService implements ParserInterface
{
    const PREFIX = '{#';
    const SUFFIX = '#}';
    const REGEX_VALIDATION = '[A-Za-z0-9 ]+';

    private $userInfoService;
    static $paramsMapping = [
        'userId',
        'originalUserId',
        'companyId',
        'userGroups',
        'locale',
        'userName'
    ];

    public function __construct(UserInfoService $userInfoService)
    {
        $this->userInfoService = $userInfoService;
    }

    public function getSchemaRegex(): string
    {
        return '/' . self::PREFIX
            . self::REGEX_VALIDATION . self::SUFFIX . '/';
    }

    public function parse(string $input): string
    {
        $key = trim(trim(trim($input, self::SUFFIX), self::PREFIX));

        if (!in_array($key, self::$paramsMapping)) {
            return '';
        }
        $userInfo = $this->userInfoService->getInfo();

        return $userInfo[$key];
    }
}