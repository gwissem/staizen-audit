<?php

namespace UserBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\DataCollectorTranslator;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class MenuBuilder
{

    private $factory;
    private $em;
    private $requestStack;
    private $tokenStorage;

    /** @var  DataCollectorTranslator $translator */
    private $translator;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(
        FactoryInterface $factory,
        EntityManager $em,
        $requestStack,
        $tokenStorage,
        $translator,
        ContainerInterface $container
    )
    {
        $this->factory = $factory;
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
        $this->container = $container;
    }

    public function createUserTopMenu(array $options)
    {

        $authChecker = $this->container->get('security.authorization_checker');
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $companyId = null;

        if($user->getCompany()) {
            $companyId = $user->getCompany()->getId();
        }

        $menu = $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'id' => 'user-top-menu',
                    'class' => 'list-unstyled list-inline',
                ],
            ]
        );
        $menu->addChild(
            $this->translator->trans('Pulpit'),
            [
                'route' => 'dashboard',
                'attributes' => [
                    'icon' => 'fa fa-home',
                ],
                'linkAttributes' => [
                    'class' => 'btn grey-cascade tooltips',
                    'data-placement' => 'bottom',
                    'data-original-title' => $this->translator->trans('Pulpit'),
                ],
            ]
        );

        if ($authChecker->isGranted('ROLE_PROCESS_VIEWER')) {

            $menu->addChild(
                $this->translator->trans('Procesy'),
                [
                    'route' => 'admin_process_definition_index',
                    'attributes' => [
                        'icon' => 'fa fa-share-alt',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn green-meadow tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Procesy'),
                    ],
                ]
            );
        }

        if ($authChecker->isGranted('ROLE_PROCESS_EDITOR')) {

            $menu->addChild(
                $this->translator->trans('Atrybuty'),
                [
                    'route' => 'admin_attribute_chart',
                    'attributes' => [
                        'icon' => 'fa fa-cube',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn green-meadow tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Atrybuty'),
                    ],
                ]
            );

            $menu->addChild(
            /** @Desc("Konfigurator Platform") */
                $this->translator->trans('user_menu.business_config'),
                [
                    'route' => 'business_config_index',
                    'attributes' => [
                        'icon' => 'fa fa-key',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn green-meadow tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('user_menu.business_config'),
                    ],
                ]
            );

            $menu->addChild(
                $this->translator->trans('Sparx'),
                [
                    'route' => 'sparx_mapping',
                    'attributes' => [
                        'icon' => 'fa fa-cubes',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn green-meadow tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Sparx'),
                    ],
                ]
            );
        }

        if($authChecker->isGranted(USER::ROLE_ADMIN)){

            $menu->addChild(
                $this->translator->trans('Narzędzia'),
                [
                    'route' => 'admin_tools',
                    'attributes' => [
                        'icon' => 'fa fa-wrench',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn green tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Narzędzia'),
                    ],
                ]
            );

            $menu->addChild(
                $this->translator->trans('Grupy narzędzi'),
                [
                    'route' => 'admin_tool_groups',
                    'attributes' => [
                        'icon' => 'fa fa-paperclip',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn green tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Grupy narzędzi'),
                    ],
                ]
            );

            $menu->addChild(
                $this->translator->trans('Podsumowania'),
                [
                    'route' => 'tool_summary_admin_view',
                    'attributes' => [
                        'icon' => 'fa fa-flag',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn green tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Podsumowania'),
                    ],
                ]
            );

        }

        if($authChecker->isGranted(USER::ROLE_KZ)){

            $menu->addChild(
                $this->translator->trans('Vip'),
                [
                    'route'=>'vip_users',
                    'attributes' => [
                        'icon' => 'fa fa-star',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn yellow-lemon tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('VIP'),
                    ],
                ]
            );

        }

        if ($authChecker->isGranted(USER::ROLE_ADMIN)) {

            $menu->addChild(
                $this->translator->trans('Firmy'),
                [
                    'route' => 'admin_companies',
                    'attributes' => [
                        'icon' => 'fa fa-briefcase',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn yellow-lemon tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Firmy'),
                    ],
                ]
            );

            $menu->addChild(
                $this->translator->trans('Użytkownicy'),
                [
                    'route' => 'admin_users',
                    'attributes' => [
                        'icon' => 'fa fa-user',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn yellow-lemon tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Użytkownicy'),
                    ],
                ]
            );
            $menu->addChild(
                $this->translator->trans('Grupy użytkownikow'),
                [
                    'route' => 'admin_user_groups',
                    'attributes' => [
                        'icon' => 'fa fa-users',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn yellow-lemon tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Grupy użytkownikow'),
                    ],
                ]
            );


            $menu->addChild(
                $this->translator->trans('Skrzynki pocztowe'),
                [
                    'route' => 'admin_mailbox_list',
                    'attributes' => [
                        'icon' => 'fa fa-envelope-o',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn yellow-casablanca tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Skrzynki pocztowe'),
                    ],
                ]
            );

            $menu->addChild(
                $this->translator->trans('Import VIN'),
                [
                    'route' => 'admin_insurance_programs_index',
                    'attributes' => [
                        'icon' => 'fa fa-car',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn yellow-casablanca tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Import VIN'),
                    ],
                ]
            );

            $menu->addChild(
                $this->translator->trans('Kolejka zadań'),
                [
                    'route' => 'admin_job_index',
                    'attributes' => [
                        'icon' => 'fa fa-clock-o',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn yellow-casablanca tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Kolejka zadań'),
                    ],
                ]
            );
        }

        if ($authChecker->isGranted(USER::ROLE_TRANSLATOR)) {

            $menu->addChild(
                $this->translator->trans('Tłumaczenia'),
                [
                    'route' => 'jms_translation_index',
                    'attributes' => [
                        'icon' => 'fa fa-language',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn purple-plum tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Tłumaczenia'),
                    ],
                ]
            );
        }

        if ($authChecker->isGranted(USER::ROLE_ADMIN)) {
            $menu->addChild(
                $this->translator->trans('Zdefiniowane treści'),
                [
                    'route' => 'admin_custom_content_index',
                    'attributes' => [
                        'icon' => 'fa fa-book',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn purple-plum tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Zdefiniowane treści'),
                    ],
                ]
            );

            $menu->addChild(
                $this->translator->trans('Helper'),
                [
                    'route' => 'admin_helper_index',
                    'attributes' => [
                        'icon' => 'fa fa-question',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn purple-plum tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Helper'),
                    ],
                ]
            );

        }

        if($authChecker->isGranted('ROLE_CFM_CASE_VIEWER')) {
            $menu->addChild(
                $this->translator->trans('user_menu.cfm_case_viewer'),
                [
                    'route'=>'ccv_index',
                    'attributes' => [
                        'icon' => 'fa fa-search',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn purple-plum tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('user_menu.cfm_case_viewer'),
                    ]
                ]

            );
        }

        if($authChecker->isGranted('ROLE_CASE_VIEWER')) {
            $menu->addChild(
                $this->translator->trans('Podgląd sprawy'),
                [
                    'route'=>'defaultCaseViewer',
                    'attributes' => [
                        'icon' => 'fa fa-briefcase ',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn purple-plum tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Podgląd sprawy'),
                    ]
                ]

            );
        }

        if ($authChecker->isGranted('ROLE_BLOGGER') || $authChecker->isGranted('ROLE_SUPPORT')) {

            $menu->addChild(
                $this->translator->trans('Blog'),
                [
                    'route' => 'admin_post_index',
                    'attributes' => [
                        'icon' => 'fa fa-file',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn purple-plum tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Blog'),
                    ],
                ]
            );
        }

        if ($authChecker->isGranted('ROLE_SUPPORT')) {

            $menu->addChild(
                $this->translator->trans('Zgłoszenia o błędach'),
                [
                    'route' => 'admin_ticket_index',
                    'attributes' => [
                        'icon' => 'fa fa-exclamation-triangle',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn red-mint tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Zgłoszenia o błędach'),
                    ],
                ]
            );


        }
        else if(in_array($companyId, [1,2])) {
            $menu->addChild(
                $this->translator->trans('Zgłoś problem'),
                [
                    'uri' => 'javascript:;',
                    'attributes' => [
                        'icon' => 'fa fa-exclamation-triangle',
                    ],
                    'linkAttributes' => [
                        'id' => 'report-problem',
                        'class' => 'btn red-soft tooltips',
                        'data-toggle' => 'modal',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Zgłoś problem'),
                    ],
                ]
            );
        }

//        if(in_array($companyId, [1,2])) {
//            $menu->addChild(
//                $this->translator->trans('Helper'),
//                [
//                    'route' => 'admin_helper_index',
//                    'attributes' => [
//                        'icon' => 'fa fa-question',
//                    ],
//                    'linkAttributes' => [
//                        'class' => 'btn purple-plum tooltips',
//                        'data-placement' => 'bottom',
//                        'data-original-title' => $this->translator->trans('Helper'),
//                    ],
//                ]
//            );
//        }

        if($authChecker->isGranted('ROLE_ADMIN')) {

//            $menu->addChild('divider', [
//                    'divider' => true,
//                    'attributes' => [
//                        'class' => 'divider-menu',
//                    ],
//                ]
//            );

            $menu->addChild(
                $this->translator->trans('Admin Panel'),
                [
                    'route' => 'socket_admin_panel_index',
                    'attributes' => [
                        'icon' => 'fa fa-area-chart fa-3x',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn blue-sharp tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Admin Panel'),
                    ],
                ]
            );

        }

        if($authChecker->isGranted('ROLE_SCENARIO_MODERATOR') || $authChecker->isGranted('ROLE_ADMIN')) {

            $menu->addChild(
            /** @Desc("Panel Administratora") */
                $this->translator->trans('management.panel.admin'),
                [
                    'route' => 'management_admin_panel',
                    'attributes' => [
                        'icon' => 'fa fa-user-secret fa-3x',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn blue-sharp tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('management.panel.admin'),
                    ],
                ]
            );

        }

        if($authChecker->isGranted('ROLE_SUPERVISOR')) {
            $menu->addChild(
                /** @Desc("Panel zarządzania") */
                $this->translator->trans('management.panel.supervisor'),
                [
                    'route' => 'management_supervisor_panel',
                    'attributes' => [
                        'icon' => 'fa fa-shield fa-3x',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn blue-sharp tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('management.panel.supervisor'),
                    ],
                ]
            );
        }

//        if(in_array($companyId, [1,2])) {
//            $menu->addChild(
//                $this->translator->trans('Zarządzanie Rolami'),
//                [
//                    'route' => 'role_task_manager',
//                    'attributes' => [
//                        'icon' => 'fa fa-bullhorn fa-3x',
//                    ],
//                    'linkAttributes' => [
//                        'class' => 'btn operational-dashboard tooltips',
//                        'data-placement' => 'bottom',
//                        'data-original-title' => $this->translator->trans('Zarządzanie Rolami'),
//                    ],
//                ]
//            );
//        }

        if ($authChecker->isGranted('ROLE_ADMIN') || $authChecker->isGranted(User::ROLE_PARTNER_VIEWER)) {
            $menu->addChild(
                /** @Desc("Panel Partnerów") */
                $this->translator->trans('user_menu.partner_interface'),
                [
                    'route' => 'operational_dashboard_partners_index',
                    'attributes' => [
                        'icon' => 'fa fa-building fa-3x',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn operational-dashboard tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('user_menu.partner_interface'),
                    ],
                ]
            );
        }

        if ($authChecker->isGranted(User::ROLE_CASE_MASTER_EDITOR)) {
            $menu->addChild(
                $this->translator->trans('user_menu.case_editor'),
                [
                    'route' => 'case_editor',
                    'attributes' => [
                        'icon' => 'fa fa-font',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn operational-dashboard tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('user_menu.case_editor'),
                    ],
                ]
            );
        }

        if ($authChecker->isGranted('ROLE_ADMIN') || $authChecker->isGranted(User::ROLE_OPERATIONAL_DASHBOARD)) {

            $menu->addChild(
                $this->translator->trans('Dashboard operacyjny'),
                [
                    'route' => 'operational_dashboard_index',
                    'attributes' => [
                        'icon' => 'fa fa-cog fa-3x',
                    ],
                    'linkAttributes' => [
                        'class' => 'btn operational-dashboard tooltips',
                        'data-placement' => 'bottom',
                        'data-original-title' => $this->translator->trans('Dashboard operacyjny'),
                    ],
                ]
            );

        }


        return $menu;
    }

}