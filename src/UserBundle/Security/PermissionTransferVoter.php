<?php

namespace UserBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use UserBundle\Entity\PermissionTransfer;
use UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class PermissionTransferVoter extends Voter
{
// these strings are just invented: you can use anything
    const EDIT = 'edit';
    const DELETE = 'delete';
    const IMPERSONATE = 'impersonate';
    const IMPERSONATED = 'impersonated';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {

// if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::EDIT, self::DELETE, self::IMPERSONATE, self::IMPERSONATED))) {
            return false;
        }

// only vote on Tool objects inside this voter
        if (!$subject instanceof PermissionTransfer) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Tool object, thanks to supports
        /** @var PermissionTransfer $permissionTransfer */
        $permissionTransfer = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($permissionTransfer, $user);
            case self::DELETE:
                return $this->canDelete($permissionTransfer, $user);
            case self::IMPERSONATE:
                return $this->canImpersonate($permissionTransfer, $user);
            case self::IMPERSONATED:
                return $this->canBeImpersonated($permissionTransfer, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canEdit(PermissionTransfer $permissionTransfer, User $user)
    {
        $now = new \DateTime();

        if ($permissionTransfer->getFromUser()->getId() !== $user->getId()) {
            return false;
        }
        if (($permissionTransfer->getToDate() && ($permissionTransfer->getToDate(
                    ) < $now)) || $permissionTransfer->getFromDate() > $now
        ) {
            return false;
        }

        return true;
    }

    private function canDelete(PermissionTransfer $permissionTransfer, User $user)
    {
        $now = new \DateTime();

        if ($permissionTransfer->getFromUser()->getId() !== $user->getId()) {
            return false;
        }
        if (($permissionTransfer->getToDate() && ($permissionTransfer->getToDate(
                    ) < $now)) || $permissionTransfer->getFromDate(
            ) > $now || $permissionTransfer->getDeletedAt() !== null
        ) {
            return false;
        }

        return true;
    }

    private function canImpersonate(PermissionTransfer $permissionTransfer, User $user)
    {

        if ($user->hasRole('ROLE_ADMIN')) {
            return true;
        }

        $now = new \DateTime();
        if ($permissionTransfer->getToUser()->getId() !== $user->getId()) {
            return false;
        }

        if (($permissionTransfer->getToDate() && ($permissionTransfer->getToDate(
                    ) < $now)) || $permissionTransfer->getFromDate(
            ) > $now || $permissionTransfer->getDeletedAt() !== null
        ) {
            return false;
        }

        return true;
    }

    private function canBeImpersonated(PermissionTransfer $permissionTransfer, User $user)
    {

        $now = new \DateTime();
        if ($permissionTransfer->getFromUser()->getId() !== $user->getId()) {
            return false;
        }
        if (($permissionTransfer->getToDate() && ($permissionTransfer->getToDate(
                    ) < $now)) || $permissionTransfer->getFromDate(
            ) > $now || $permissionTransfer->getDeletedAt() !== null
        ) {
            return false;
        }

        return true;
    }

}