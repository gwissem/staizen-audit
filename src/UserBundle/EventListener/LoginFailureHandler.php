<?php

namespace UserBundle\EventListener;

use FOS\UserBundle\Model\UserManager;
use JabberBundle\Controller\DefaultController;
use Predis\Client;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use UserBundle\Entity\User;

class LoginFailureHandler implements AuthenticationFailureHandlerInterface
{
    /** @var Router  */
    protected $router;

    /** @var AuthorizationChecker  */
    protected $authorizationChecker;

    /** @var UserManager  */
    protected $userManager;

    /** @var TokenStorage  */
    protected $tokenStorage;

    /** @var Client  */
    protected $redis;

    /** @var Container  */
    protected $container;

    public function __construct(
        Router $router,
        AuthorizationChecker $authorizationChecker,
        UserManager $userManager,
        TokenStorage $tokenStorage,
        Client $redis,
        Container $container
    )
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
        $this->userManager = $userManager;
        $this->tokenStorage = $tokenStorage;
        $this->redis = $redis;
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $response = new RedirectResponse($this->router->generate('fos_user_security_login'));

        $ipAddressChecker = $this->container->get('app.service.ip_address_checking');

        /** Jeżeli jest IP domenowe, to próba logowania przez ActiveDirectory */

        $isDomainIp = $ipAddressChecker->isDomainIP($request);

        if($isDomainIp) {

            $username = $request->request->get('_username');
            $password = $request->request->get('_password');
            $domain = 'starter';

            $userManager = $this->userManager;
            /** @var User $user */
            $user = $userManager->findUserByUsername($username);

            if($user) {

                $ldap = ldap_connect('10.10.77.35');
                ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);

                if (ldap_bind($ldap, $domain."\\".$username, $password)) {

                    //ldap user authenticated
//                if (!$user) {
//                    $user = $userManager->createUser();
//                    $user->setUsername($username);
//                    $user->setPlainPassword($password);
//                    $user->setEnabled(true);
//                    $userManager->updateUser($user);
//                }

                    if($ipAddressChecker->isUserRestricted($user, $request))
                    {

                        $this->tokenStorage->setToken();
//                    $this->container->get('session')->invalidate();
                        $request->getSession()->invalidate();
                        return new RedirectResponse($this->router->generate('user_access_forbidden'));

                    }

                    $this->authenticateSuccessfully($user);

                    $request->getSession()->set('is_domain_user', true);

                    /** Zapisanie haseł w redisie */

                    $data = [
                        'username' => $user->getUsername() . DefaultController::SUFFIX_USERNAME_JABBER,
                        'password' => $request->request->get('_password'),
                        'extension' => $user->getCucumExtension()
                    ];

                    $this->redis->hmset(DefaultController::INDEX_JABBER_REDIS . $user->getId(), $data);

                    return new RedirectResponse($this->router->generate('index'));

                } else {
                    $request->getSession()->invalidate();
                }
            }

        }

        if($isDomainIp) {
            $request->getSession()->getFlashBag()->add('danger', 'Nieprawidłowy login lub hasło.');
        }
        else {
            $request->getSession()->getFlashBag()->add('danger', 'Nieprawidłowy login/hasło lub brak połączenia VPN.');
        }

        return $response;

    }

    private function authenticateSuccessfully($user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->tokenStorage->setToken($token); //now the user is logged in
        $user->setLastLogin(new \DateTime());
        $this->userManager->updateUser($user);

    }
}
