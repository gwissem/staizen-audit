<?php
namespace UserBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\SecurityContextInterface;
use UserBundle\Entity\User;

class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;
    private $tokenStorage;

    public function __construct($defaultLocale = 'pl', TokenStorage $tokenStorage)
    {
        $this->defaultLocale = $defaultLocale;
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered after the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 15)),
            KernelEvents::CONTROLLER => array(array('onKernelController', 20)),
        );
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $locale = $request->cookies->get('_locale', $this->defaultLocale);
        $request->setLocale($locale);

    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $token = $this->tokenStorage->getToken();
        if(!$token){
            return;
        }

        /** @var User $user */
        $user = $token->getUser();

        if($user && $user instanceof User) {
            $locale = $user->getLocale();
            if($locale) {
                $request->setLocale($locale);
                $request->cookies->set('_locale', $locale);
            }
        }

    }

}