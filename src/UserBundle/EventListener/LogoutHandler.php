<?php
namespace UserBundle\EventListener;

use JabberBundle\Communication\ClientRequest;
use JabberBundle\Communication\Entity\CUCUMUser;
use JabberBundle\Controller\DefaultController;
use Predis\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use UserBundle\Entity\User;

class LogoutHandler implements LogoutHandlerInterface
{

    protected $redis;

    /**
     * LogoutHandler constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->redis = $client;
    }

    public function logout(Request $Request, Response $Response, TokenInterface $Token)
    {

        try {

            // WYLOGOWANIE UŻYTKOWNIKA Z TELEFONII

            /** @var User $user */
            $user = $Token->getUser();

            if(!is_string($user) && $user->getCucumExtension()) {

                $data = $this->redis->hgetall(DefaultController::INDEX_JABBER_REDIS . $user->getId());

                $data['id'] = $user->getId();
                $data['username'] = $user->getUsername();

                $cucmUser = new CUCUMUser(null,null, $data);
                $clientRequest = new ClientRequest($cucmUser);

                $templateBody = '<User><state>LOGOUT</state></User>';

                $clientRequest
                    ->setActionUrl(sprintf('User/%s', $user->getUsername()))
                    ->setOptions([
                        'body' => $templateBody,
                        'headers' => [
                            'Content-Type' => 'application/xml'
                        ]
                    ])
                    ->execute('put');
            }
        }
        catch (\Exception $exception) {

        }

    }

}