<?php

namespace UserBundle\EventListener;

use UserBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use AppBundle\Service\LogsUserService;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;


class ActivityListener
{
    private $em;
    private $security;
    private $logUserService;

    public function __construct(EntityManagerInterface $em, TokenStorage $security, LogsUserService $logUserService)
    {
        $this->em = $em;
        $this->security = $security;
        $this->logUserService = $logUserService;
    }

    public function onCoreController(FilterControllerEvent $event)
    {
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }

        if ($this->security->getToken()) {
            $user = $this->security->getToken()->getUser();

            if ( ($user instanceof User) ) {
                $this->logUserService->createLoginLog($user);
            }
        }
    }
}