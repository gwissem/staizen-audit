<?php
namespace UserBundle\EventListener;

use JabberBundle\Controller\DefaultController;
use Predis\Client;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use UserBundle\Entity\User;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{

    protected $router;
    protected $authorizationChecker;
    protected $container;

    /**
     * @var Client $redis
     */
    protected $redis;

    /**
     * LoginSuccessHandler constructor.
     * @param Router $router
     * @param AuthorizationChecker $authorizationChecker
     * @param $redis
     */
    public function __construct(Router $router, AuthorizationChecker $authorizationChecker, $redis, Container $container)
    {
        $this->router = $router;
        $this->redis = $redis;
        $this->authorizationChecker = $authorizationChecker;
        $this->container = $container;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $response = null;

        /** @var User $user */
        $user = $token->getUser();

        $data = [
            'username' => $user->getUsername() . DefaultController::SUFFIX_USERNAME_JABBER,
            'password' => $request->request->get('_password'),
            'extension' => $user->getCucumExtension()
        ];

        $this->redis->hmset(DefaultController::INDEX_JABBER_REDIS . $user->getId(), $data);

        $ipAddressChecker = $this->container->get('app.service.ip_address_checking');

        if($ipAddressChecker->isUserRestricted($user, $request))
        {
            $this->container->get('security.token_storage')->setToken();
//            $this->container->get('session')->invalidate();
            $request->getSession()->invalidate();
            return new RedirectResponse($this->router->generate('user_access_forbidden'));
        }

        if ($this->authorizationChecker->isGranted('ROLE_USER')) {
            $response = new RedirectResponse($this->router->generate('index'));
        }

        return $response;
    }

}