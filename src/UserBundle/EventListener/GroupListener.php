<?php
namespace UserBundle\EventListener;

use ChatBundle\Service\ChatUserManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\Group;
use WebBundle\Service\CachedMenuService;

class GroupListener
{
    private $cachedMenuService;

    /** @var  ContainerInterface $container */
    private $chatUserManager;

    private $chatMustReload = false;

    public function __construct(CachedMenuService $cachedMenuService, ChatUserManager $chatUserManager)
    {
        $this->cachedMenuService = $cachedMenuService;
        $this->chatUserManager = $chatUserManager;
    }

    public function preUpdate(Group $group, PreUpdateEventArgs $args)
    {
        /** Sprawdza czy w edytowanej grupie występowała rola - ROLE_CHAT */

        if($args->hasChangedField('roles')) {
            if(in_array('ROLE_CHAT',
                array_unique(
                    array_merge(
                        $args->getOldValue('roles'),
                        $args->getNewValue('roles')
                    ),
                    SORT_REGULAR)
            )
            ) {
                /** Reload Users of Chat in Redis in postUpdate */

                $this->chatMustReload = true;
            }
        }

    }

    public function postUpdate(Group $group, LifecycleEventArgs $args)
    {
        if($this->chatMustReload) {
            $this->reloadChatUsers();
        }

        $this->clearToolMenuCache($group);

    }

    private function reloadChatUsers(){

        $this->chatUserManager->reloadChatUsersInRedis();

    }

    public function clearToolMenuCache($group)
    {

        $userIds = array_unique(
            $group->getUsers()->map(
                function ($entity) {
                    return $entity->getId();
                }
            )->toArray()
        );

        $this->cachedMenuService->clearCacheOfUser($userIds);
    }

    public function postPersist(Group $group, LifecycleEventArgs $args)
    {
        $this->clearToolMenuCache($group);
    }
}