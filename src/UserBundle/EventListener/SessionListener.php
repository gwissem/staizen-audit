<?php
namespace UserBundle\EventListener;

use FOS\UserBundle\Model\UserManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContextInterface;
use UserBundle\Service\UserInfoService;

class SessionListener
{

    protected $userInfo;
    protected $router;
    protected $authChecker;
    protected $tokenStorage;
    protected $userManager;

    public function __construct(
        UserInfoService $userInfo,
        AuthorizationChecker $authChecker,
        TokenStorage $tokenStorage,
        RouterInterface $router,
        UserManager $userManager
    ) {
        $this->userInfo = $userInfo;
        $this->router = $router;
        $this->authChecker = $authChecker;
        $this->tokenStorage = $tokenStorage;
        $this->um = $userManager;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) {

            return;
        }

        $info = $this->userInfo->getInfo();

        if (!empty($info) && $info['userId'] !== $info['originalUserId']) {
            $user = $this->userInfo->getUser();
            $originalUser = $this->um->findUserByUsername($this->userInfo->getOriginalUser()->getUsername());

            if (!$originalUser->hasRole('ROLE_ADMIN')) {
                $activeTransfers = $user->getActivePermissionTransfersToUser($originalUser);
                if (!$activeTransfers) {
                    $this->logout($event);
                }
                if ($user) {
                    foreach ($activeTransfers as $transfer) {
                        if (!$this->authChecker->isGranted('impersonated', $transfer)) {
                            $this->logout($event);
                        }
                    }
                }
            }
        }
    }

    private function logout($event)
    {
        $this->tokenStorage->setToken(null);
        $event->setResponse(new RedirectResponse($this->router->generate('fos_user_security_login')));
    }

}