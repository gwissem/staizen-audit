<?php
namespace UserBundle\EventListener;

use AppBundle\Utils\QueryManager;
use ChatBundle\Service\ChatUserManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use UserBundle\Entity\User;
use WebBundle\Service\CachedMenuService;

class UserListener
{
    /** @var  ContainerInterface $container */
    protected $container;
    /** @var  ChatUserManager $chatUserManager */
    protected $chatUserManager;
    private $cachedMenuService;

    /** @var  QueryManager */
    private $queryManager;

    public function __construct(CachedMenuService $cachedMenuService, QueryManager $queryManager)
    {
        $this->cachedMenuService = $cachedMenuService;
        $this->queryManager = $queryManager;
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function preUpdate(User $user, PreUpdateEventArgs $args)
    {
        /** Sprawdza czy w edytowanym użytkowniku występuje rola - ROLE_CHAT */

        if($args->hasChangedField('roles')) {
            if(in_array('ROLE_CHAT',
                array_unique(
                    array_merge(
                        $args->getOldValue('roles'),
                        $args->getNewValue('roles')
                    ),
                    SORT_REGULAR)
            )
            ) {
                /** Reload Users of Chat in Redis */

                $this->checkIfAddToChatGroup($user);
            }
        }

    }

    private function checkIfAddToChatGroup(User $user)
    {
        $this->container->get('chat_bundle.user_manager')->updateChatUserInRedis($user);
    }

    public function postUpdate(User $user, LifecycleEventArgs $args)
    {
        if($user->getId()) {
            $this->cachedMenuService->clearCacheOfUser($user->getId());
        }
    }

    public function postPersist(User $user, LifecycleEventArgs $args)
    {

        if(!$this->chatUserManager) $this->chatUserManager = $this->container->get('chat_bundle.user_manager');

        if($user->getId()) {
            $this->chatUserManager->migrateUser($user);
            $this->checkIfAddToChatGroup($user);
        }
    }
}