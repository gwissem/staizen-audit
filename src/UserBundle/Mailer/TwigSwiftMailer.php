<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UserBundle\Mailer;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;
use MailboxBundle\Entity\ConnectedMailbox;
use MailboxBundle\Service\SimpleOutlookSenderService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\UserBundle\Mailer\TwigSwiftMailer as TwigSwiftMailerBase;

class TwigSwiftMailer extends TwigSwiftMailerBase
{

    /** @var SimpleOutlookSenderService $simpleEmailSender */
    private $simpleEmailSender;

    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, \Twig_Environment $twig, array $parameters)
    {

        $https['ssl']['verify_peer'] = false;
        $https['ssl']['verify_peer_name'] = false; // seems to work fine without this line so far

        $mailer->getTransport()->setStreamOptions($https);

        parent::__construct($mailer, $router, $twig, $parameters);
    }

    public function setAtlasMailbox($mailboxName, EntityManager $em) {

        $mailbox = $em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => $mailboxName
        ]);

        if($mailbox instanceof ConnectedMailbox) {
            $this->simpleEmailSender = new SimpleOutlookSenderService($mailbox);
        }

    }

    public function sendActivationEmailMessage(UserInterface $user)
    {

        $template = $this->parameters['template']['activation'];
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url . '?active'
        );

        if($this->simpleEmailSender) {

            $context = $this->twig->mergeGlobals($context);
            $template = $this->twig->loadTemplate($template);
            $subject = $template->renderBlock('subject', $context);
            $htmlBody = $template->renderBlock('body_html', $context);

            $this->simpleEmailSender->send($user->getEmail(), $subject, $htmlBody);

        }
        else {
            $this->sendMessage($template, $context, $this->parameters['from_email']['resetting'], $user->getEmail());
        }

    }

    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['template']['resetting'];
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url
        );

        if($this->simpleEmailSender) {

            $context = $this->twig->mergeGlobals($context);
            $template = $this->twig->loadTemplate($template);
            $subject = $template->renderBlock('subject', $context);
            $htmlBody = $template->renderBlock('body_html', $context);

            $this->simpleEmailSender->send($user->getEmail(), $subject, $htmlBody);

        }
        else {
            $this->sendMessage($template, $context, $this->parameters['from_email']['resetting'], $user->getEmail());
        }

    }

}
