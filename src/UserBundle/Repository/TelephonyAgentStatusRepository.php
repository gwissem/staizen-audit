<?php

namespace UserBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;

class TelephonyAgentStatusRepository extends \Doctrine\ORM\EntityRepository
{

    public function findAllAsArray()
    {
        $qb = $this->createQueryBuilder('t');
        $qb->select('t.username','t.stateChangeTime');
        return $qb->getQuery()->getResult();
    }

    public function fetchAsArray()
    {
        $qb = $this->createQueryBuilder('t');
        $qb->select('t.username as username', 't.state as state', 't.codeId as code', 't.stateChangeTime as time');
        $result = $qb->getQuery()->getResult( Query::HYDRATE_ARRAY);

        $output = [];

        foreach ($result as $item) {
            $output[$item['username']] = $item;
        }

        return $output;
    }

}
