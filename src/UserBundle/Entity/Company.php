<?php
namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\CompanyRepository")
 * @ORM\Table(name="company")
 * @ORM\HasLifecycleCallbacks
 */
class Company
{
    use ORMBehaviors\Sluggable\Sluggable,
        ORMBehaviors\Timestampable\Timestampable;

    const TYPE_SELL = 0;
    const TYPE_BUY = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="company")
     */
    protected $users;
    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @var type
     */
    protected $name;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $fullName;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $address;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $externalAccountId;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $identifierNumber;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $code;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $partnerLocationId;
    /**
     * @ORM\ManyToMany(targetEntity="DocumentBundle\Entity\Document", mappedBy="companies")
     */
    private $documents;
    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\Service", mappedBy="company")
     */
    private $services;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Company
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     * @return Company
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * @return type
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param type $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param mixed $documents
     * @return Company
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     * @return Company
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Company
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExternalAccountId()
    {
        return $this->externalAccountId;
    }

    /**
     * @param mixed $externalAccountId
     * @return Company
     */
    public function setExternalAccountId($externalAccountId)
    {
        $this->externalAccountId = $externalAccountId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Company
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifierNumber()
    {
        return $this->identifierNumber;
    }

    /**
     * @param mixed $identifierNumber
     * @return Company
     */
    public function setIdentifierNumber($identifierNumber)
    {
        $this->identifierNumber = $identifierNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param mixed $services
     * @return Company
     */
    public function setServices($services)
    {
        $this->services = $services;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPartnerLocationId()
    {
        return $this->partnerLocationId;
    }

    /**
     * @param mixed $partnerLocationId
     */
    public function setPartnerLocationId($partnerLocationId)
    {
        $this->partnerLocationId = $partnerLocationId;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    public function getSluggableFields()
    {
        return ['name', 'id'];
    }

    public function generateSlugValue($values)
    {
        return str_replace(' ', '', implode('-', $values));
    }


}
