<?php
namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use ToolBundle\Entity\Tool;
use Gedmo\Mapping\Annotation as Gedmo;
use ToolBundle\Entity\ToolSummary;
use JabberBundle\Entity\Traits\CucumUser as CucumUser;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user", indexes={@Orm\Index(name="_dta_index_fos_user_8_203147769__K1_20_21", columns={"firstname", "lastname", "id"})})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\AttributeOverrides({
 *              @ORM\AttributeOverride(name="email", column=@ORM\Column(nullable=true)),
 *              @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(nullable=true, unique=false))
 * })
 */
class User extends BaseUser implements EquatableInterface
{

    use ORMBehaviors\Sluggable\Sluggable,
        ORMBehaviors\Timestampable\Timestampable,
        CucumUser;

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';
    const ROLE_USER_AS_SYSTEM = 'ROLE_USER_AS_SYSTEM';
    const ROLE_OPERATIONAL_DASHBOARD = 'ROLE_OPERATIONAL_DASHBOARD';
    const ROLE_TASK_MANAGER = 'ROLE_TASK_MANAGER';
    const ROLE_DASHBOARD_INTERFACE = 'ROLE_DASHBOARD_INTERFACE';
    const ROLE_HIDE_NOTES = 'ROLE_HIDE_NOTES';
    const ROLE_PROCESS_EDITOR = 'ROLE_PROCESS_EDITOR';
    const ROLE_PROCESS_VIEWER = 'ROLE_PROCESS_VIEWER';
    const ROLE_CHAT = 'ROLE_CHAT';
    const ROLE_TRANSLATOR = 'ROLE_TRANSLATOR';
    const ROLE_TESTER = 'ROLE_TESTER';
    const ROLE_SCENARIO_MODERATOR = 'ROLE_SCENARIO_MODERATOR';
    const ROLE_CASE_MASTER_EDITOR = 'ROLE_CASE_MASTER_EDITOR';
    const ROLE_SUPERVISOR = 'ROLE_SUPERVISOR';
    const ROLE_PARTNER_VIEWER = 'ROLE_PARTNER_VIEWER';
    const ROLE_PARTNER_EDITOR = 'ROLE_PARTNER_EDITOR';
    const ROLE_CASE_SERVICE_ACTIONS = 'ROLE_CASE_SERVICE_ACTIONS';
    const ROLE_HIDE_GUEST_TOOLS = 'ROLE_HIDE_GUEST_TOOLS';
    const ROLE_CASE_FORWARD = 'ROLE_CASE_FORWARD';
    const ROLE_CASE_SERVICE_ADVANCE = 'ROLE_CASE_SERVICE_ADVANCE';
    const ROLE_RECORDING_CALL = 'ROLE_RECORDING_CALL';
    const ROLE_ATLAS_MONITORING = 'ROLE_ATLAS_MONITORING';
    const ROLE_PHONE_STATISTIC = 'ROLE_PHONE_STATISTIC';
    const ROLE_PUSH_CASE = 'ROLE_PUSH_CASE';
    const ROLE_FINANCE_VIEW = 'ROLE_FINANCE_VIEW';
    const ROLE_KZ = 'ROLE_KZ';
    const ROLE_KCZ = 'ROLE_KCZ';
    const ROLE_CONSULTANT = 'ROLE_CONSULTANT';
    const ROLE_CONSULTANT_ADVANCED = 'ROLE_CONSULTANT_ADVANCED';
    const ROLE_TELEPHONY = 'ROLE_TELEPHONY';
    const ROLE_SUPPORT = 'ROLE_SUPPORT';
    const ROLE_CASE_VIEWER = 'ROLE_CASE_VIEWER';
    const ROLE_ANALYST = 'ROLE_ANALYST';
    CONST ROLE_CFM_CASE_VIEWER = 'ROLE_CFM_CASE_VIEWER';
    CONST ROLE_OPERATIONAL_DASHBOARD_READ_ONLY = 'ROLE_OPERATIONAL_DASHBOARD_READ_ONLY';
    const ROLE_BLOGGER = 'ROLE_BLOGGER';
    CONST ROLE_RODO_MEDICAL = 'ROLE_RODO_MEDICAL';
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastname;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\Group", inversedBy="users")
     * @ORM\JoinTable(name="user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Platform", inversedBy="userPlatforms")
     * @ORM\JoinTable(name="user_platform",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="platform_id", referencedColumnName="id")}
     * )
     */
    protected $platforms;

    /**
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    protected $groupsToken;

    /**
     * @ORM\Column(type="string", name="filters_token", length=4000, nullable=true)
     */
    protected $filtersToken;

    /**
     * @ORM\Column(type="array", name="personalization_json", nullable=true)
     */
    protected $personalizationJson;

    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;
    /**
     * @ORM\OneToMany(targetEntity="PermissionTransfer", mappedBy="fromUser")
     */
    private $permissionTransfers;
    /**
     * @ORM\OneToMany(targetEntity="PermissionTransfer", mappedBy="toUser")
     */
    private $permissionGranted;
    /**
     * @ORM\OneToMany(targetEntity="ToolBundle\Entity\UserFilterSet", mappedBy="user")
     */
    private $filterSets;
    /**
     * @ORM\OneToOne(targetEntity="ToolBundle\Entity\Tool")
     * @ORM\JoinColumn(name="summary_tool_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $summaryTool;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
     */
    private $manager;

    /**
     * @ORM\OneToMany(targetEntity="ToolBundle\Entity\ToolSummary", mappedBy="user")
     */
    private $summaryTools;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\ProcessInstance", mappedBy="user")
     */
    private $processInstances;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $acceptedRegulations = false;

    /**
     * @var boolean $online
     * @ORM\Column(type="boolean", name="online", nullable=true)
     */
    private $online;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\UserRoleGroup", inversedBy="users")
     * @ORM\JoinColumn(name="role_group_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $roleGroup;

    /**
     * @var string $locale
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locale;

    /*
    * @var boolean $online
    * @ORM\Column(type="datetime", name="last_shown_efficiency_popup", nullable=true)
    */
    private $lastShownEfficiencyPopupDateTime;

    public $agentState;
    public $agentLabel = null;
    public $stateChangeTime;
    public $activeTasks;
    public $timeWork = null;
    public $currentTasks = [];
    public $lastTaskTime;

    public function __construct()
    {
        parent::__construct();
        $this->summaryTools = new ArrayCollection();
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->platforms = new ArrayCollection();;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        if ($this->firstname && $this->lastname) {
            return $this->firstname . ' ' . $this->lastname;
        }

        return $this->username;
    }

    public function getDefaultTools()
    {
        $tools = [];
        /** @var Group $group */
        foreach ($this->getGroups() as $group) {
            /** @var Tool $tool */
            foreach ($group->getTools(true) as $tool) {
                if (!in_array($tool, $tools)) {
                    $tools[] = $tool;
                }
            }
        }

        return $tools;
    }

    /**
     * @return ArrayCollection | PersistentCollection
     */
    public function getGroups()
    {
        return ($this->groups === null) ? (new ArrayCollection()) : $this->groups;
    }

    /**
     * @param mixed $groups
     * @return User
     */
    public function setGroups($groups)
    {
        $this->groups[] = $groups;

        return $this;
    }

    public function hasGroup($groupId)
    {
        if ($groupId instanceof Group) {
            $groupId = $groupId->getId();
        }

        return $this->groups->exists(
            function ($key, $element) use ($groupId) {
                return ($element->getId() == $groupId);
            });
    }

    public function hasPlatform($platformId)
    {
        return $this->platforms->exists(
          function ($key, $element) use ($platformId) {
              return ($element->getId() == $platformId);
          });
    }

    public function getSluggableFields()
    {
        return ['username'];
    }

    public function generateSlugValue($values)
    {
        return str_replace(' ', '', implode('-', $values));
    }

    /**
     * Add company
     *
     * @param \UserBundle\Entity\Company $company
     *
     * @return User
     */
    public function addCompany(\UserBundle\Entity\Company $company)
    {
        $this->company[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \UserBundle\Entity\Company $company
     */
    public function removeCompany(\UserBundle\Entity\Company $company)
    {
        $this->company->removeElement($company);
    }

    /**
     * Get company
     *
     * @return Company | null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Get companyId
     *
     * @return int | null
     */
    public function getCompanyId()
    {
        return ($this->company) ? $this->company->getId() : null;
    }

    /**
     * Set company
     *
     * @param \UserBundle\Entity\Company $company
     *
     * @return User
     */
    public function setCompany(\UserBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Add permissionTransfer
     *
     * @param \UserBundle\Entity\PermissionTransfer $permissionTransfer
     *
     * @return User
     */
    public function addPermissionTransfer(\UserBundle\Entity\PermissionTransfer $permissionTransfer)
    {
        $this->permissionTransfers[] = $permissionTransfer;

        return $this;
    }

    /**
     * Remove permissionTransfer
     *
     * @param \UserBundle\Entity\PermissionTransfer $permissionTransfer
     */
    public function removePermissionTransfer(\UserBundle\Entity\PermissionTransfer $permissionTransfer)
    {
        $this->permissionTransfers->removeElement($permissionTransfer);
    }

    /**
     * Get permissionTransfers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPermissionTransfers()
    {
        return $this->permissionTransfers;
    }

    public function getActivePermissionTransfersToUser($user)
    {
        $now = new \DateTime();
        $resultArray = [];
        foreach ($this->permissionTransfers as $permissionTransfer) {
            if ((!$permissionTransfer->getToDate() || $permissionTransfer->getToDate() >= $now) && $permissionTransfer->getFromDate() <= $now && $permissionTransfer->getDeletedAt() === null && $permissionTransfer->getToUser()->getId() === $user->getId()
            ) {
                $resultArray[] = $permissionTransfer;
            }
        };

        return $resultArray;
    }

    /**
     * Add loginOwner
     *
     * @param \UserBundle\Entity\User $loginOwner
     *
     * @return User
     */
    public function addLoginOwner(\UserBundle\Entity\User $loginOwner)
    {
        $this->loginOwners[] = $loginOwner;

        return $this;
    }

    /**
     * Remove loginOwner
     *
     * @param \UserBundle\Entity\User $loginOwner
     */
    public function removeLoginOwner(\UserBundle\Entity\User $loginOwner)
    {
        $this->loginOwners->removeElement($loginOwner);
    }

    /**
     * Get loginOwners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLoginOwners()
    {
        return $this->loginOwners;
    }

    /**
     * Add loginOwnedUser
     *
     * @param \UserBundle\Entity\User $loginOwnedUser
     *
     * @return User
     */
    public function addLoginOwnedUser(\UserBundle\Entity\User $loginOwnedUser)
    {
        $this->loginOwnedUsers[] = $loginOwnedUser;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get summaryTool
     *
     * @return \ToolBundle\Entity\Tool
     */
    public function getSummaryTool()
    {
        return $this->summaryTool;
    }

    /**
     * Set summaryTool
     *
     * @param \ToolBundle\Entity\Tool $summaryTool
     *
     * @return User
     */
    public function setSummaryTool(\ToolBundle\Entity\Tool $summaryTool = null)
    {
        $this->summaryTool = $summaryTool;

        return $this;
    }

    /**
     * Add filterSet
     *
     * @param \ToolBundle\Entity\UserFilterSet $filterSet
     *
     * @return User
     */
    public function addFilterSet(\ToolBundle\Entity\UserFilterSet $filterSet)
    {
        $this->filterSets[] = $filterSet;

        return $this;
    }

    /**
     * Remove filterSet
     *
     * @param \ToolBundle\Entity\UserFilterSet $filterSet
     */
    public function removeFilterSet(\ToolBundle\Entity\UserFilterSet $filterSet)
    {
        $this->filterSets->removeElement($filterSet);
    }

    /**
     * Get filterSets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilterSets()
    {
        return $this->filterSets;
    }

    /**
     * @return mixed
     */
    public function getPersonalizationJson()
    {
        return $this->personalizationJson;
    }

    /**
     * @param mixed $personalizationJson
     */
    public function setPersonalizationJson($personalizationJson)
    {
        $this->personalizationJson = $personalizationJson;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * Add filterSet
     *
     * @param ToolSummary $summaryTool
     * @return User
     * @internal param \ToolBundle\Entity\UserFilterSet $filterSet
     *
     */
    public function addSummaryTools(ToolSummary $summaryTool)
    {
        $this->summaryTools[] = $summaryTool;

        return $this;
    }

    /**
     * Remove filterSet
     *
     * @param ToolSummary $summaryTool
     * @internal param \ToolBundle\Entity\UserFilterSet $filterSet
     */
    public function removeSummaryTools(ToolSummary$summaryTool)
    {
        $this->summaryTools->removeElement($summaryTool);
    }

    /**
     * @return mixed
     */
    public function getSummaryTools()
    {
        return $this->summaryTools;
    }

    /**
     * @param mixed $summaryTools
     */
    public function setSummaryTools($summaryTools)
    {
        $this->summaryTools = $summaryTools;
    }

    /**
     * @return mixed
     */
    public function getPermissionGranted()
    {
        return $this->permissionGranted;
    }

    /**
     * @param mixed $permissionGranted
     */
    public function setPermissionGranted($permissionGranted)
    {
        $this->permissionGranted = $permissionGranted;
    }

    public function isEqualTo(UserInterface $user)
    {
        if ($user instanceof User) {
            // Check that the roles are the same, in any order
            $isEqual = count($this->getRoles()) == count($user->getRoles());
            if ($isEqual) {
                foreach ($this->getRoles() as $role) {
                    $isEqual = $isEqual && in_array($role, $user->getRoles());
                }
            }

            if ($isEqual) {
                $isEqual = count($this->getGroups()) == count($user->getGroups());
            }

            return $isEqual;
        }

        return false;
    }

    /**
     * Serializes the user.
     *
     * The serialized data have to contain the fields used during check for
     * changes and the id.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize(
            array(
                $this->password,
                $this->salt,
                $this->usernameCanonical,
                $this->username,
                $this->expired,
                $this->locked,
                $this->credentialsExpired,
                $this->enabled,
                $this->id,
                $this->expiresAt,
                $this->roleGroup,
                $this->credentialsExpireAt,
                $this->email,
                $this->emailCanonical,
                $this->firstname,
                $this->lastname,
                $this->permissionGranted,
                $this->permissionTransfers,
                $this->roles
            )
        );
    }

    /**
     * Unserializes the user.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        // add a few extra elements in the array to ensure that we have enough keys when unserializing
        // older data which does not include all properties.
        $data = array_merge($data, array_fill(0, 2, null));

        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->expiresAt,
            $this->roleGroup,
            $this->credentialsExpireAt,
            $this->email,
            $this->emailCanonical,
            $this->firstname,
            $this->lastname,
            $this->permissionGranted,
            $this->permissionTransfers,
            $this->roles
            ) = $data;
    }

    public function isAdmin()
    {
        return $this->hasRole('ROLE_ADMIN');
    }

    /**
     * Add permissionGranted
     *
     * @param \UserBundle\Entity\PermissionTransfer $permissionGranted
     *
     * @return User
     */
    public function addPermissionGranted(\UserBundle\Entity\PermissionTransfer $permissionGranted)
    {
        $this->permissionGranted[] = $permissionGranted;

        return $this;
    }

    /**
     * Remove permissionGranted
     *
     * @param \UserBundle\Entity\PermissionTransfer $permissionGranted
     */
    public function removePermissionGranted(\UserBundle\Entity\PermissionTransfer $permissionGranted)
    {
        $this->permissionGranted->removeElement($permissionGranted);
    }

    /**
     * Add summaryTool
     *
     * @param \ToolBundle\Entity\ToolSummary $summaryTool
     *
     * @return User
     */
    public function addSummaryTool(\ToolBundle\Entity\ToolSummary $summaryTool)
    {
        $this->summaryTools[] = $summaryTool;

        return $this;
    }

    /**
     * Remove summaryTool
     *
     * @param \ToolBundle\Entity\ToolSummary $summaryTool
     */
    public function removeSummaryTool(\ToolBundle\Entity\ToolSummary $summaryTool)
    {
        $this->summaryTools->removeElement($summaryTool);
    }

    /**
     * Get manager
     *
     * @return \UserBundle\Entity\User
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set manager
     *
     * @param \UserBundle\Entity\User $manager
     *
     * @return User
     */
    public function setManager(\UserBundle\Entity\User $manager = null)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProcessInstances()
    {
        return $this->processInstances;
    }

    /**
     * @param mixed $processInstances
     * @return User
     */
    public function setProcessInstances($processInstances)
    {
        $this->processInstances = $processInstances;

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;

        if(($groups = $this->getGroups()) !== null) {

            foreach ($groups as $group) {
                $roles = array_merge($roles, $group->getRoles());
            }

        }

        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    private $rolesInGroups = null;

    public function getRolesFromGroups()
    {

        $roles = [];

        if (($groups = $this->getGroups()) !== null) {

            foreach ($groups as $group) {
                $roles = array_merge($roles, $group->getRoles());
            }

        }

        return $roles;

    }

    public function isRoleInGroup($roleName)
    {
        if ($this->rolesInGroups === null) $this->rolesInGroups = $this->getRolesFromGroups();
        return (in_array($roleName, $this->rolesInGroups));
    }

    /**
     * @return mixed
     */
    public function getAcceptedRegulations()
    {
        return $this->acceptedRegulations;
    }

    /**
     * @param mixed $acceptedRegulations
     * @return User
     */
    public function setAcceptedRegulations($acceptedRegulations)
    {
        $this->acceptedRegulations = $acceptedRegulations;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupsToken()
    {
        return $this->groupsToken;
    }

    /**
     * @param mixed $groupsToken
     * @return User
     */
    public function setGroupsToken($groupsToken)
    {
        $this->groupsToken = $groupsToken;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isOnline()
    {
        return $this->online;
    }

    /**
     * @param mixed $online
     */
    public function setOnline($online)
    {
        $this->online = $online;
    }

    /**
     * @return mixed
     */
    public function getFiltersToken()
    {
        return $this->filtersToken;
    }

    /**
     * @param mixed $filtersToken
     */
    public function setFiltersToken($filtersToken)
    {
        $this->filtersToken = $filtersToken;
    }

    /**
     * @return UserRoleGroup
     */
    public function getRoleGroup()
    {
        return $this->roleGroup;
    }

    /**
     * @param mixed $roleGroup
     */
    public function setRoleGroup($roleGroup)
    {
        $this->roleGroup = $roleGroup;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return User
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastShownEfficiencyPopupDateTime()
    {
        return $this->lastShownEfficiencyPopupDateTime;
    }

    /**
     * @param \DateTime $lastShownEfficiencyPopupDateTime
     */
    public function setLastShownEfficiencyPopupDateTime(\DateTime $lastShownEfficiencyPopupDateTime)
    {
        $this->lastShownEfficiencyPopupDateTime = $lastShownEfficiencyPopupDateTime;
    }


    /**
     * @return array
     */
    static function getAppRoles()
    {
        return [
            'app.role.process_editor' => self::ROLE_PROCESS_EDITOR,
            'app.role.process_viewer' => self::ROLE_PROCESS_VIEWER,
            'app.role.chat' => self::ROLE_CHAT,
            'app.role.translator' => self::ROLE_TRANSLATOR,
            'app.role.user_as_system' => self::ROLE_USER_AS_SYSTEM,
            'app.role.telephony' => self::ROLE_TELEPHONY,
            'app.role.consultant' => self::ROLE_CONSULTANT,
            'app.role.consultant_advanced' => self::ROLE_CONSULTANT_ADVANCED,
            'app.role.admin' => self::ROLE_ADMIN,
            'app.role.operational.interface' => self::ROLE_DASHBOARD_INTERFACE,
            'app.role.operational.dashboard' => self::ROLE_OPERATIONAL_DASHBOARD,
            'app.role.task.manager' => self::ROLE_TASK_MANAGER,
            'app.role.scenario_moderator' => self::ROLE_SCENARIO_MODERATOR,
            'app.role.case_master_editor' => self::ROLE_CASE_MASTER_EDITOR,
//            'app.role.role_supervisor' => self::ROLE_SUPERVISOR,
            'app.role.partner_viewer' => self::ROLE_PARTNER_VIEWER,
            'app.role.partner_editor' => self::ROLE_PARTNER_EDITOR,
            'app.role.hide_notes' => self::ROLE_HIDE_NOTES,
            'app.role.tester' => self::ROLE_TESTER,
            'app.role.case_service_actions' => self::ROLE_CASE_SERVICE_ACTIONS,
            'app.role.hide_guest_tools' => self::ROLE_HIDE_GUEST_TOOLS,
            'app.role.case_forward' => self::ROLE_CASE_FORWARD,
            'app.role.case_service_advance' => self::ROLE_CASE_SERVICE_ADVANCE,
            'app.role.recording_call' => self::ROLE_RECORDING_CALL,
            'app.role.atlas_monitoring' => self::ROLE_ATLAS_MONITORING,
            'app.role.phone_statistic' => self::ROLE_PHONE_STATISTIC,
            'app.role.push_case' => self::ROLE_PUSH_CASE,
            'app.role.finance_view' => self::ROLE_FINANCE_VIEW,
            'app.role.kz' => self::ROLE_KZ,
            'app.role.kcz' => self::ROLE_KCZ,
            'app.role.support' => self::ROLE_SUPPORT,
            'app.role.case_viewer' => self::ROLE_CASE_VIEWER,
            'app.role.cfm_case_viewer' => self::ROLE_CFM_CASE_VIEWER,
            'app.role.analyst' => self::ROLE_ANALYST,
            'app.role.operational.dashboard.read_only' => self::ROLE_OPERATIONAL_DASHBOARD_READ_ONLY,
            'app.role.blogger' => self::ROLE_BLOGGER,
        ];
    }

    /**
     * @return mixed
     */
    public function getPlatforms()
    {
        return $this->platforms;
    }

    /**
     * @param mixed $platforms
     */
    public function setPlatforms($platforms)
    {
        $this->platforms[] = $platforms;
    }

    public function removePlatforms($platform)
    {
        if ($this->getPlatforms()->contains($platform)) {
            $this->getPlatforms()->removeElement($platform);
        }

        return $this;
    }


    const MANAGEMENT_ROLES = [
        self::ROLE_TELEPHONY => [
            'label' => 'app.role.telephony'
        ],
        self::ROLE_CONSULTANT_ADVANCED => [
            'label' => 'app.role.consultant_advanced',
            'info' => [
                'Możliwość edycji wszystkich danych w panelu edycji usługi',
                'Zmiana Kontraktora N/H bez odwołania'
            ]
        ],
        self::ROLE_PARTNER_EDITOR => [
            'label' => 'app.role.partner_editor'
        ],
        self::ROLE_PARTNER_VIEWER => [
            'label' => 'app.role.partner_viewer'
        ],
        self::ROLE_CASE_MASTER_EDITOR => [
            'label' => 'app.role.case_master_editor',
            'info' => [
                'Panel "A" do zmiany danych'
                ]
        ],
        self::ROLE_CASE_SERVICE_ACTIONS => [
            'label' => 'app.role.case_service_actions',
            'info' => [
                'Edycja danych usługi',
                'Zakończ wynajem',
                'Przedłuż wynajem',
                'Wznów wynajem',
                'Zmiana Partnera Serwisowego',
                'Odwołanie kontraktora',
                'Cofnij do weryfikacji zamknięcia usługi',
                'Cofnij anulowanie usługi'
            ]
        ],
        self::ROLE_CASE_FORWARD => [
            'label' => 'app.role.case_forward',
            'info' => [
                'Przyspieszenie wygenerowania macierzy'
            ]
        ],
        self::ROLE_CASE_SERVICE_ADVANCE => [
            'label' => 'app.role.case_service_advance',
            'info' => [
                'Uruchomienie usługi poza logiką aplikacji'
            ]
        ],
        self::ROLE_RECORDING_CALL => [
            'label' => 'app.role.recording_call'
        ],
        self::ROLE_FINANCE_VIEW => [
            'label' => 'app.role.finance_view'
        ],
        self::ROLE_ATLAS_MONITORING => [
            'label' => 'app.role.atlas_monitoring'
        ],
        self::ROLE_PUSH_CASE => [
            'label' => 'app.role.push_case',
            'info' => [
                'Dashboard bez listy zadań (losowanie zadania do wykonania)'
            ]
        ],
        self::ROLE_RODO_MEDICAL => [
            'label' => 'app.role.rodo_medical'
        ]
    ];

    static function getManagementRoles()
    {
        return self::MANAGEMENT_ROLES;
    }

}
