<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\TelephonyAgentStatusRepository")
 * @ORM\Table(name="telephony_agent_status")
 */
class TelephonyAgentStatus
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $number;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $state;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $codeId;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $stateChangeTime;

    /**
     * @ORM\Column(type="text", nullable = true)
     */
    protected $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param mixed $codeId
     */
    public function setCodeId($codeId)
    {
        $this->codeId = $codeId;
    }

    /**
     * @return mixed
     */
    public function getStateChangeTime()
    {
        return $this->stateChangeTime;
    }

    /**
     * @param mixed $stateChangeTime
     */
    public function setStateChangeTime($stateChangeTime)
    {
        $this->stateChangeTime = $stateChangeTime;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return TelephonyAgentStatus
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }



}