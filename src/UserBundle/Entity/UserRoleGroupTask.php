<?php

namespace UserBundle\Entity;


use CaseBundle\Entity\Platform;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRoleGroupRepository")
 * @ORM\Table(name="user_role_group_task")
 */
class UserRoleGroupTask
{

    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Platform $platform
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Platform")
     * @ORM\JoinColumn(name="platform_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $platform;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\UserRoleGroup", inversedBy="tasks")
     * @ORM\JoinColumn(name="user_role_group_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $userRoleGroup;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Group", inversedBy="tasks")
     * @ORM\JoinColumn(name="user_group_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $userGroup;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Platform
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     * @return UserRoleGroupTask
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserRoleGroup()
    {
        return $this->userRoleGroup;
    }

    /**
     * @param mixed $userRoleGroup
     * @return UserRoleGroupTask
     */
    public function setUserRoleGroup($userRoleGroup)
    {
        $this->userRoleGroup = $userRoleGroup;
        return $this;
    }

    /**
     * @return Group|null
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }

    /**
     * @param mixed $userGroup
     * @return UserRoleGroupTask
     */
    public function setUserGroup($userGroup)
    {
        $this->userGroup = $userGroup;
        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return UserRoleGroupTask
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     * @return UserRoleGroupTask
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        return $this;
    }

    function __clone()
    {
        $this->id = null;
    }


}
