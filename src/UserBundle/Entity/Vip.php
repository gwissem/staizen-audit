<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\VipRepository")
 * @ORM\Table(name="vip")
 */
class Vip
{
    use ORMBehaviors\Timestampable\Timestampable;

    const TYPE_TOP_PRIORITY = 1;

    const VIP_PERSON = 1;
    const VIP_COMPANY = 2;
    const VIP_TYPE_PERSON_TEXT = "Ważna osoba";
    const VIP_TYPE_COMPANY_TEXT = "Firma";
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="boolean", options={"default" = true})
     */
    protected $active = true;

    /**
     * @ORM\Column(type="text", nullable = true)
     */
    protected $welcomeMessage;

    /**
     * @ORM\Column(type="text", nullable = true)
     */
    protected $supervisorMessage;

    /**
     * @ORM\Column(type="text", nullable = true)
     */
    protected $companyName;

    /**
     * @var Company $company
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Company")
     * @ORM\JoinColumn(referencedColumnName="id", nullable = true)
     */
    protected $company =null;


    /**
     * @ORM\Column(type="text", nullable = true,length=20)
     */
    protected $vin = null;


    /**
     * @ORM\Column(type="text", nullable = true, length=10)
     */
    protected $regNumber = null;


    /**
     * @ORM\Column(type="text", nullable = true)
     */
    protected $position;

    /**
     *@ORM\Column(type="integer", nullable = true, options={"default": 1})
     */
    protected $type;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;
    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Vip
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Vip
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Vip
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Vip
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @param bool $text
     * @return mixed
     */
    public function getType($text = false)
    {

        if($text) {
            switch ($this->type) {
                case self::VIP_PERSON:
                    return self::VIP_TYPE_PERSON_TEXT;
                case self::VIP_COMPANY:
                    return self::VIP_TYPE_COMPANY_TEXT;
                default:
                    return self::VIP_TYPE_PERSON_TEXT;
            }
        }

        return $this->type;

    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get welcomeMessage
     *
     * @return string
     */
    public function getWelcomeMessage()
    {
        return $this->welcomeMessage;
    }

    /**
     * Set welcomeMessage
     *
     * @param string $welcomeMessage
     *
     * @return Vip
     */
    public function setWelcomeMessage($welcomeMessage)
    {
        $this->welcomeMessage = $welcomeMessage;

        return $this;
    }

    /**
     * Get supervisorMessage
     *
     * @return string
     */
    public function getSupervisorMessage()
    {
        return $this->supervisorMessage;
    }

    /**
     * Set supervisorMessage
     *
     * @param string $supervisorMessage
     *
     * @return Vip
     */
    public function setSupervisorMessage($supervisorMessage)
    {
        $this->supervisorMessage = $supervisorMessage;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param \UserBundle\Entity\User $createdBy
     *
     * @return Vip
     */
    public function setCreatedBy(\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy
     *
     * @param \UserBundle\Entity\User $updatedBy
     *
     * @return Vip
     */
    public function setUpdatedBy(\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * @param mixed $vin
     */
    public function setVin($vin)
    {
        $this->vin = $vin;
    }

    /**
     * @return mixed
     */
    public function getRegNumber()
    {
        return $this->regNumber;
    }

    /**
     * @param mixed $regNumber
     */
    public function setRegNumber($regNumber)
    {
        $this->regNumber = $regNumber;
    }



}
