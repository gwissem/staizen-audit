<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\Group as BaseGroup;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ToolBundle\Entity\Tool;
use ToolBundle\Entity\UserGroupTool;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\GroupRepository")
 * @ORM\Table(name="user_group")
 * @UniqueEntity("uniqueId")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\Tree(type="nested")
 */
class Group extends BaseGroup
{

    use ORMBehaviors\Sluggable\Sluggable,
        ORMBehaviors\Timestampable\Timestampable;

    const DRIVERS = 43;
    const DISPOSITORS = 3510;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User", mappedBy="groups")
     *
     */
    protected $users;
    /**
     * @ORM\ManyToMany(targetEntity="ToolBundle\Entity\Tool")
     * @ORM\JoinTable(name="users_group_tool",
     *      joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tool_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $defaultTools;
    /**
     * @ORM\OneToMany(targetEntity="ToolBundle\Entity\UserGroupTool", mappedBy="group", cascade={"persist","remove"} )
     */
    protected $hasTools;
    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lft;
    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lvl;
    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rgt;
    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Group")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $root;
    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $parent;
    /**
     * @ORM\OneToMany(targetEntity="Group", mappedBy="parent", orphanRemoval=true)
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;
    /**
     * @ORM\OneToMany(targetEntity="ToolBundle\Entity\ToolPermission", mappedBy="group")
     */
    private $toolPermissions;

    /**
     * @ORM\ManyToMany(targetEntity="ToolBundle\Entity\ToolAction", mappedBy="userGroups")
     */
    private $toolActions;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Step", mappedBy="permittedGroups")
     */
    private $steps;

    /**
     * @var string
     * @ORM\Column(type="string", length=32, unique=true)
     */
    private $uniqueId;

    /**
     * @ORM\Column(type="smallint", length=1, options={"default" = 0})
     */
    private $type = 0;

    /**
     * @ORM\Column(name="short_name", type="string", length=32, nullable=true)
     */
    private $shortName = '';

    /**
     * @ORM\Column(name="order_by", type="integer", nullable=true)
     */
    private $orderBy = 300;

    /**
     * Constructor
     * @param $name
     * @param array $roles
     */
    public function __construct($name, $roles = array())
    {
        parent::__construct($name, $roles);

        $this->toolPermissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->defaultTools = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hasTools= new \Doctrine\Common\Collections\ArrayCollection();
        $this->users= new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new ArrayCollection();
        $this->uniqueId = md5(rand().time());
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    public function getSluggableFields()
    {
        return ['name'];
    }

    public function generateSlugValue($values)
    {
        return str_replace(' ', '', implode('-', $values));
    }

    /**
     * Add toolPermission
     *
     * @param \ToolBundle\Entity\ToolPermission $toolPermission
     *
     * @return Group
     */
    public function addToolPermission(\ToolBundle\Entity\ToolPermission $toolPermission)
    {
        $this->toolPermissions[] = $toolPermission;

        return $this;
    }

    /**
     * Remove toolPermission
     *
     * @param \ToolBundle\Entity\ToolPermission $toolPermission
     */
    public function removeToolPermission(\ToolBundle\Entity\ToolPermission $toolPermission)
    {
        $this->toolPermissions->removeElement($toolPermission);
    }

    /**
     * Get toolPermissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getToolPermissions()
    {
        return $this->toolPermissions;
    }

    /**
     * @param Tool $tool
     * @param $action
     * @return bool
     */
    public function canModifyTool($tool, $action)
    {
        $actionMethod = 'getCan'.ucwords($action);
        foreach ($tool->getPermissions() as $permission) {
            if ($permission->getGroup()->getId() === $this->getId() && $permission->$actionMethod() === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDefaultTools()
    {
        return $this->defaultTools;
    }

    /**
     * @param mixed $defaultTools
     */
    public function setDefaultTools($defaultTools)
    {
        $this->defaultTools = $defaultTools;
    }

    /**
     * @return mixed
     */
    public function getHasTools()
    {
        return $this->hasTools;
    }

    /**
     * @param mixed $hasTools
     */
    public function setHasTools($hasTools)
    {
        $this->hasTools = $hasTools;
    }

    public function addTool(Tool $tool, $order = 0)
    {
        $newTool = new UserGroupTool();
        $newTool->setTool($tool);
        $newTool->setPosition($order);

        return $this->addToolToGroup($newTool);
    }

    public function addToolToGroup(UserGroupTool $userGroupTool)
    {
        if (!$this->hasTools->contains($userGroupTool)) {
            $this->hasTools->add($userGroupTool);
            $userGroupTool->setGroup($this);
        }

        return $this;
    }

    public function clear()
    {
        $this->hasTools = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getTools($orderPosition = false)
    {
        $GroupTools = $this->hasTools->toArray();

        if($orderPosition && $GroupTools)
        {
            usort($GroupTools, function($a, $b){
                /** @var UserGroupTool $a */
                /** @var UserGroupTool $b */
                return $a->getPosition() > $b->getPosition();
            });
        }

        $tools = array_map(
            function ($userGroupTool) {
                /** @var UserGroupTool $userGroupTool */
                return $userGroupTool->getTool();
            },
            $GroupTools
        );

        return $tools;
    }

    /**
     * Add user
     *
     * @param User $user
     *
     * @return Group
     */
    public function addUser(User $user)
    {
        $this->users->add($user);
        $user->addGroup($this);

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
        $user->removeGroup($this);
    }

    /**
     * Add defaultTool
     *
     * @param \ToolBundle\Entity\Tool $defaultTool
     *
     * @return Group
     */
    public function addDefaultTool(\ToolBundle\Entity\Tool $defaultTool)
    {
        $this->defaultTools[] = $defaultTool;

        return $this;
    }

    /**
     * Remove defaultTool
     *
     * @param \ToolBundle\Entity\Tool $defaultTool
     */
    public function removeDefaultTool(\ToolBundle\Entity\Tool $defaultTool)
    {
        $this->defaultTools->removeElement($defaultTool);
    }

    /**
     * Add hasTool
     *
     * @param \ToolBundle\Entity\UserGroupTool $hasTool
     *
     * @return Group
     */
    public function addHasTool(\ToolBundle\Entity\UserGroupTool $hasTool)
    {
        $this->hasTools[] = $hasTool;

        return $this;
    }

    /**
     * Remove hasTool
     *
     * @param \ToolBundle\Entity\UserGroupTool $hasTool
     */
    public function removeHasTool(\ToolBundle\Entity\UserGroupTool $hasTool)
    {
        $this->hasTools->removeElement($hasTool);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Group
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Group
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Group
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get root
     *
     * @return \UserBundle\Entity\Group
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set root
     *
     * @param \UserBundle\Entity\Group $root
     *
     * @return Group
     */
    public function setRoot(\UserBundle\Entity\Group $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \UserBundle\Entity\Group
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param \UserBundle\Entity\Group $parent
     *
     * @return Group
     */
    public function setParent(\UserBundle\Entity\Group $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add child
     *
     * @param \UserBundle\Entity\Group $child
     *
     * @return Group
     */
    public function addChild(\UserBundle\Entity\Group $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \UserBundle\Entity\Group $child
     */
    public function removeChild(\UserBundle\Entity\Group $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function getIndentedName()
    {
        return str_repeat("|—", $this->lvl)." ".$this->name;
    }

    /**
     * Add toolAction
     *
     * @param \ToolBundle\Entity\ToolAction $toolAction
     *
     * @return Group
     */
    public function addToolAction(\ToolBundle\Entity\ToolAction $toolAction)
    {
        $this->toolActions[] = $toolAction;

        return $this;
    }

    /**
     * Remove toolAction
     *
     * @param \ToolBundle\Entity\ToolAction $toolAction
     */
    public function removeToolAction(\ToolBundle\Entity\ToolAction $toolAction)
    {
        $this->toolActions->removeElement($toolAction);
    }

    /**
     * Get toolActions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getToolActions()
    {
        return $this->toolActions;
    }

    /**
     * @return string
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * @param string $uniqueId
     * @return Group
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Group
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShortName()
    {
        if(empty($this->shortName)) return $this->getName();
        return $this->shortName;
    }

    /**
     * @param mixed $shortName
     * @return Group
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param mixed $steps
     * @return Group
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
        return $this;
    }

    public function getPriority(){
        return $this->getSameValueForAllSteps('priority');
    }

    public function getMaxPriority(){
        return $this->getSameValueForAllSteps('maxPriority');
    }

    public function getPriorityBoostInterval(){
        return $this->getSameValueForAllSteps('priorityBoostInterval');
    }

    public function getUrgentPriorityBoost(){
        return $this->getSameValueForAllSteps('urgentPriorityBoost');
    }


    private function getSameValueForAllSteps($property){
        $result = [];
        $method = 'get'.ucfirst($property);
        foreach($this->getSteps() as $step){
            $result[] = $step->$method();
        }

        if(count(array_unique($result)) === 1){
            return $result[0];
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param mixed $orderBy
     * @return Group
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
        return $this;
    }


}
