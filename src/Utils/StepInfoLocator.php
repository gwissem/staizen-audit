<?php

namespace Utils;


use Doctrine\ORM\QueryBuilder;

class StepInfoLocator
{
    public $step, $platform, $program, $alias;

    public function __construct($step, $platform, $program = null)
    {
        $this->step = $step;
        $this->platform = $platform;
        $this->program = $program;
        $this->alias = null;
    }

    /**
     * Setting up Alias (if existing)
     * @param $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return QueryBuilder
     */
    public function getWherePart(QueryBuilder $queryBuilder)
    {
        $alias = $this->alias;

        $queryBuilder->where(($alias ? $alias . '.' : '') . 'platform = :platform')
            ->andWhere(($alias ? $alias . '.' : '') . 'step = :step')
            ->setParameter('step', $this->step)
            ->setParameter('platform', $this->platform);

        if ($this->program) {
            $queryBuilder = $queryBuilder
            ->andWhere( ($alias ? $alias . '.' : '') . 'program = :program 
            OR '.($alias ? $alias . '.' : '') .'program is null')
                ->setParameter('program',$this->program);

        }else{
            $queryBuilder = $queryBuilder
                ->andWhere(($alias ? $alias . '.' : '') . 'program IS NULL');
        }

        return $queryBuilder;
    }
}