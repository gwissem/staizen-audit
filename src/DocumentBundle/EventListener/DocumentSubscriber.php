<?php
namespace DocumentBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use DocumentBundle\Entity\Document;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DocumentSubscriber implements EventSubscriber
{

    /** @var  ContainerInterface $container */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist'
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $document = $args->getEntity();

        if ($document instanceof Document) {

            $entityManager = $args->getEntityManager();

            $starter24 = $entityManager->getRepository('UserBundle:Company')->findOneBy(['name' => 'Starter24']);

            if($starter24 && !$document->getCompanies()->contains($starter24)) {
                $document->addCompany($starter24);
            }

        }
    }

}