<?php

namespace DocumentBundle\Command;

use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\Company;

class TransferCaseFilesCommand extends ContainerAwareCommand
{

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var EntityManager */
    private $em;

    protected function configure()
    {
        $this->setName('case-file:transfer')
             ->addArgument('groupProcessId', InputArgument::REQUIRED, 'GroupProcessId ')
             ->addArgument('filePath', InputArgument::REQUIRED, 'File path')
             ->addArgument('attributePath', InputArgument::REQUIRED, 'attribute path where to put the document id')
             ->addArgument('companyId', InputArgument::OPTIONAL, 'company that has access to a file')
             ->addArgument('replaceDuplicate', InputArgument::OPTIONAL, 'determines if file should replace previous with same name and size')
             ->addArgument('action', InputArgument::OPTIONAL, 'if file should be copied or cut');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $this->input = $input;
        $this->output = $output;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $filesDir = $container->getParameter('files_directory');
        $filePath = $input->getArgument('filePath');
        $attributePath = $input->getArgument('attributePath');
        $groupProcessInstanceId = $input->getArgument('groupProcessId');
        $companyId = $input->getArgument('companyId');
        $replaceDuplicate = $input->getArgument('replaceDuplicate');
        $action = $input->getArgument('action');

        if(!$action){
            $action = 'cut';
        }
        /** move file to new location */

        $name = basename($filePath);

        if(empty($name)){
            $output->writeln('File :'.$name.' not found.');
            return;
        }

        $extension = '';
        $mimeType = mime_content_type($filePath);

        $pathInfoExtension = pathinfo($filePath, PATHINFO_EXTENSION);
        if(!empty($pathInfoExtension)){
            $extension = '.'.$pathInfoExtension;
        }
        else{
            $extension = '.'.explode('/',$mimeType)[1];
        }

        if(strpos($name,'.') === false){
            $name = $name.$extension;
        }

        $hashName = uniqid();

        $subDir = sprintf('/%s/%s/', substr($hashName, 0, 2), substr($hashName, 2, 2));

        if (!is_dir($filesDir.$subDir)) {
            mkdir($filesDir.$subDir, 0777, true);
        }

        $movedFilePath = $filesDir.$subDir.$hashName.$extension;
        if($action == 'cut'){
            rename($filePath,$movedFilePath);
        }
        else{
            copy($filePath,$movedFilePath);
        }


        /** create entities */

        $processHandler = $container->get('case.process_handler');
        $documentId = $processHandler->getAttributeValue($attributePath,$groupProcessInstanceId,'int');


        $file = new File();
        $file->setName($name);
        $file->setPath($subDir.$hashName.$extension);
        $file->setMimeType($mimeType);
        $file->setFileSize(filesize($movedFilePath));
        $this->em->persist($file);

        if($documentId){
            /** @var Document $document */
            $document = $this->em->getRepository(Document::class)->find($documentId);
            if($replaceDuplicate){
                 /** @var File $duplicate */
                 $duplicate = $this->em->getRepository(File::class)->findOneBy(
                     [
                         'document' => $document,
                         'name' => $name,
                         'fileSize' => filesize($movedFilePath)
                     ]
                 );

                 if(!empty($duplicate)){
                     $document->removeFile($duplicate);
                     $duplicate->setDocument(NULL);
                     $this->em->persist($duplicate);
                 }
            }
        }
        else{
            $document = new Document();
        }

        $document->addFile($file);
        $file->setDocument($document);

        if($companyId){
            $company = $this->em->getRepository(Company::class)->find($companyId);
            if(!$document->getCompanies()->contains($company)){
                $document->addCompany($company);
            }
        }

        $this->em->persist($document);
        $this->em->flush();

        /** link attribute value */
        $documentId = $documentId ?: $document->getId();
        $processHandler->editAttributeValue($attributePath,$groupProcessInstanceId,$documentId,'int','xxx',1,1);

    }

}
