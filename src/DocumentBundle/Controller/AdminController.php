<?php

namespace DocumentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    /**
     * @Route("/admin/file/remove/{id}", requirements={"id": "\d+"}, name="admin_file_delete")
     * @Security("is_granted('ROLE_USER')")
     */
    public function deleteFileAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('DocumentBundle:File')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono pliku'));
        }

        $filePath = $this->getParameter('files_directory').$entity->getPath();
        $em->remove($entity);
        $em->flush();

        unlink($filePath);

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Plik "%name%" został pomyślnie usunięty',
                ['%name%' => $entity->getName()]
            )

        );

        return $this->redirect($request->headers->get('referer'));
    }

}
