<?php

namespace DocumentBundle\Controller;

use DocumentBundle\Entity\File;
use DocumentBundle\Security\FileVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/{id}", requirements={"id": "\d+"}, name="document_files", options={"expose"=true})
     */
    public function documentFilesAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $document = $em->getRepository('DocumentBundle:Document')->find($id);


        return $this->render(
            $request->isXmlHttpRequest(
            ) ? 'DocumentBundle:Default:files-list.html.twig' : 'DocumentBundle:Default:document-files.html.twig',
            [
                'document' => $document,
            ]
        );
    }


    /**
     * /documents/download-file/AB204D83-B49F-4703-AAA2-EEF9A0A17296
     *
     *
     * @Route("/download-file/{uuid}", name="download_file", options={"expose"=true})
     * @param Request $request
     * @param $uuid
     * @return Response
     */
    public function downloadFileAction(Request $request, $uuid)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var File $file */
        $file = $em->getRepository('DocumentBundle:File')->findBy([
            'uuid' => $uuid
        ]);

        if (!$file) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono pliku'));
        }

        if(!empty($file)) {
            $file = $file[0];
        }

        //$this->denyAccessUnlessGranted(FileVoter::READ, $file);

        $isGranted = $this->isGranted(FileVoter::READ, $file);

        /** @var User $user */
        $user = $this->getUser();

        if (!$isGranted && $user) {

            // Sprawdzenie poprzez procedure, czy może mieć dostęp do pliku w inny sposób
            $hasPermissionForFile = $this->get('case.process_handler')->checkPermissionForFile($uuid, $user->getId());

            if(!$hasPermissionForFile) {

                $exception = $this->createAccessDeniedException('Unable to access this page!');
                $exception->setAttributes(FileVoter::READ);
                $exception->setSubject($file);

                throw $exception;
            }

        }
//        else {
//
//            $exception = $this->createAccessDeniedException('Unable to access this page!');
//            $exception->setAttributes(FileVoter::READ);
//            $exception->setSubject($file);
//
//            throw $exception;
//
//        }

        $fileContent = file_get_contents($this->getParameter('files_directory').$file->getPath());

        if(!$fileContent) {
            throw new NotFoundHttpException();
        }

        $response = new Response();
        $response->headers->set('Content-Type', $file->getMimeType());
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$file->getName());
        $response->setStatusCode(200);
        $response->setContent($fileContent);

        return $response;
    }

    /**
     * /documents/download-file/AB204D83-B49F-4703-AAA2-EEF9A0A17296
     *
     *
     * @Route("/download-report/{uuid}", name="download_report", options={"expose"=true})
     * @param Request $request
     * @param $uuid
     * @return Response
     */
    public function downloadReport(Request $request, $uuid)
    {

        $em = $this->getDoctrine()->getManager();

        /** @var File $file */
        $file = $em->getRepository('DocumentBundle:File')->findBy([
            'uuid' => $uuid
        ]);

        if (!$file) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono pliku'));
        }

        if(!empty($file)) {
            $file = $file[0];
        }


        $fileContent = file_get_contents( $this->getParameter('kernel.root_dir').'/../'.$file->getPath());


        if(!$fileContent) {
            throw new NotFoundHttpException();
        }

        $response = new Response();
        $response->headers->set('Content-Type', $file->getMimeType());
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$file->getName());
        $response->setStatusCode(200);
        $response->setContent($fileContent);

        return $response;
    }
}
