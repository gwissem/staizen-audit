<?php

namespace DocumentBundle\Uploader;

use DocumentBundle\Entity\File;
use MailboxBundle\Service\MailContainerFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDir;
    private $userInfo;

    public function __construct($targetDir, $userInfo)
    {
        $this->targetDir = $targetDir;
        $this->userInfo = $userInfo;
    }

    public function upload($uploadedFile)
    {
        $file = new File();
        $name = uniqid();
        $folderPath = sprintf('/%s/%s/', substr($name, 0, 2), substr($name, 2, 2));
        $this->createDirectoriesFromPath($folderPath);

        if($uploadedFile instanceof UploadedFile )
        {
            $ext = $uploadedFile->guessExtension();

            if(!$ext) {
                // Nie udało się odczytać rozszerzenia
                if($uploadedFile->getMimeType() == "application/CDFV2-unknown") { // Z outlook'a
                    $ext = "msg";
                }
            }

            $fileName = $name.'.'.$ext;
            $filePath = $folderPath.$fileName;

            $file->setName($uploadedFile->getClientOriginalName());
            $file->setFileSize($uploadedFile->getClientSize());
            $file->setMimeType($uploadedFile->getMimeType());
            $file->setPath($filePath);
            $file->setCreatedBy($this->userInfo->getUser());
            $uploadedFile->move($this->targetDir.$folderPath, $fileName);

            return $file;
        }
        else if ($uploadedFile instanceof MailContainerFile)
        {
            $fileName = $name.'.'.$uploadedFile->getExtension();
            $filePath = $folderPath.$fileName;

            $file->setName($uploadedFile->getOriginalName());
            $file->setFileSize($uploadedFile->getFileSize());
            $file->setMimeType($uploadedFile->getMimeType());
            $file->setCreatedBy($this->userInfo->getUser());
            $file->setPath($filePath);

            $uploadedFile->move($this->targetDir.$filePath);

            return $file;
        }

        return false;

    }

    public function uploadFromSource($fileData, $type = 'jpeg') {

        $name = uniqid();
        $folderPath = sprintf('/%s/%s/', substr($name, 0, 2), substr($name, 2, 2));
        $this->createDirectoriesFromPath($folderPath);

        $fileName = $name.'.'.$type;
        $filePath = $folderPath.$fileName;

        file_put_contents($this->targetDir.$filePath, $fileData);

        return $filePath;
    }

    private function createDirectoriesFromPath($path)
    {
        if (!is_dir($this->targetDir.$path)) {
            mkdir($this->targetDir.$path, 0777, true);
        }
    }

    public function getTargetDir() {
        return $this->targetDir;
    }
}