<?php

namespace DocumentBundle\Security;

use DocumentBundle\Entity\File;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use ToolBundle\Entity\ToolAction;
use UserBundle\Entity\User;

class FileVoter extends Voter
{
    const READ = 'read';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::READ))) {
            return false;
        }

        // only vote on File objects inside this voter
        if (!$subject instanceof File) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var File $subject */

        $user = $token->getUser();
        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::READ:
                if (!$user instanceof User) {
                    return false;
                }

                return $this->canRead($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }


    /**
     * Jeżeli dokument i użytkownik mają takie samo Company, to true
     * 
     * @param File $file
     * @param User $user
     * @return bool
     */
    private function canRead(File $file, User $user)
    {
        if ($user->getCompany() && $file->getDocument()) {

            /** Jeżeli dokument nie ma ustalonego Companies, to każdy może pobrać */

            if($file->getDocument()->getCompanies()->count() === 0) {
                return true;
            }

            if ($file->getDocument()->getCompanies()->contains($user->getCompany())) {
                return true;
            }

        }

        return false;
    }
}