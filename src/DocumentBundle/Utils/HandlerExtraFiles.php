<?php

namespace DocumentBundle\Utils;

use MailboxBundle\Service\MailboxOutgoingEmailService;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

class HandlerExtraFiles
{

    const PATH_TO_EXTRA_DIR = __DIR__ . "/../../../data/extra-files";

    const DYNAMIC_FILE_SOURCE_ATTRIBUTE = 'ATTRIBUTE';
    const DYNAMIC_FILE_SOURCE_FILE = 'FILE';
    const DYNAMIC_FILE_SOURCE_FORM = 'FORM';

    static $mappingFile = null;
    static $fileSystem = null;

    static function getPathExtraFile($fileName) {

        if(self::$mappingFile === null) {
            self::$mappingFile = Yaml::parse(file_get_contents(self::PATH_TO_EXTRA_DIR . '/mapping_extra_files'));
        }

        if(self::$fileSystem === null) {
            self::$fileSystem = new Filesystem();
        }

        if(isset(self::$mappingFile[$fileName])) {
            if(self::$fileSystem->exists(self::PATH_TO_EXTRA_DIR . self::$mappingFile[$fileName])) {
                return self::PATH_TO_EXTRA_DIR . self::$mappingFile[$fileName];
            }
        }

        return null;
    }

    /**
     * @param MailboxOutgoingEmailService $mailboxOutgoingEmailService
     * @param $options
     * @param $dataEmail
     * @return string
     */
    static function getPathDynamicFile(MailboxOutgoingEmailService $mailboxOutgoingEmailService, $options, $dataEmail) {

        // SOURCE::ID|FILENAME::NAME::WITH_PARSE

        // {FORM::41696::raport serwisowy}
        // {FILE::assistance_organization_request::OrganizationRequest::true}
        // {ATTRIBUTE::2992034::tresc atrybutu 2992034::true}

        if(!empty($options)) {

            if(count($options) < 3) {
                $mailboxOutgoingEmailService->log('Too less arguments of dynamic file {'.implode('::', $options).'}!', ['AttributeId' => $dataEmail['id']], 'ERROR');
                return null;
            }

            $source = strtoupper($options[0]);

            if($source !== self::DYNAMIC_FILE_SOURCE_ATTRIBUTE && $source !== self::DYNAMIC_FILE_SOURCE_FILE && $source !== self::DYNAMIC_FILE_SOURCE_FORM) {
                $mailboxOutgoingEmailService->log('Bad source of dynamic file {'.implode('::', $options).'}!', ['AttributeId' => $dataEmail['id']], 'ERROR');
                return null;
            }

            $value = $options[1];
//            $name = $options[2];

            /** Konwertowanie Formularza (ProcessInstance) do PDF'a */
            if($source === self::DYNAMIC_FILE_SOURCE_FORM) {

                return $mailboxOutgoingEmailService->convertProcessInstanceFormToPdf($value);

            }
            else {

                $withParse = (isset($options[3])) ? filter_var($options[3], FILTER_VALIDATE_BOOLEAN) : false;
                $html = '';

                if($source === self::DYNAMIC_FILE_SOURCE_ATTRIBUTE) {
                    $html = $mailboxOutgoingEmailService->getEmailBodyFromAttribute($value);
                }
                elseif ($source === self::DYNAMIC_FILE_SOURCE_FILE) {
                    $html = $mailboxOutgoingEmailService->getEmailBodyFromFile($value, [
                        'groupProcessInstanceId' => $dataEmail['groupProcessId']
                    ]);
                }

                if($withParse) {
                    $html = $mailboxOutgoingEmailService->parseEmailBody($html, $dataEmail['groupProcessId']);
                }

                return $mailboxOutgoingEmailService->convertHtmlToPdf($html);

            }

        }

        return null;

    }

}
