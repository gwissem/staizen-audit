<?php

namespace DocumentBundle\Repository;

/**
 * FilterRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FileRepository extends \Doctrine\ORM\EntityRepository
{

    public function getFilesFromDate($date)
    {
        $qb = $this->createQueryBuilder('f');

        $qb->where('f.createdAt >= :date')
            ->select('f.id,f.path')
            ->setParameter('date', $date);

        return $qb->getQuery()->getResult();
    }

}
