<?php
namespace DocumentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping\OneToOne;
use MapBundle\Controller\FindBySmsController;

/**
 * @ORM\Entity(repositoryClass="DocumentBundle\Repository\FileRepository")
 * @ORM\Table(name="files")
 * @ORM\HasLifecycleCallbacks
 */
class File
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var type
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @var type
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var type
     */
    protected $mimeType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var type
     */
    protected $fileSize;

    /**
     * @ORM\ManyToOne(targetEntity="Document", inversedBy="files")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id", nullable=true)
     */
    private $document;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\Column(name="uuid", type="uniqueidentifier")
     * @var string
     */
    private $uuid;

    /**
     * File constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->uuid = FindBySmsController::gen_uuid();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @param mixed $fileSize
     * @return File
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param mixed $document
     * @return File
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param mixed $mimeType
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    }

    public function getIconClass()
    {
        $mimeType = $this->mimeType;
        if ($mimeType == 'application/pdf') {
            $icon = 'fa fa-file-pdf-o';
        } elseif ($mimeType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || $mimeType == 'application/msword') {
            $icon = 'fa fa-file-word-o';
        } elseif ($mimeType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || $mimeType == 'application/vnd.ms-excel') {
            $icon = 'fa fa-file-excel-o';
        } elseif (strpos($mimeType, 'image') !== false) {
            $icon = 'fa fa-file-image-o';
        } elseif (strpos($mimeType, 'audio') !== false) {
            $icon = 'fa fa-audio-o';
        } elseif (strpos($mimeType, 'video') !== false) {
            $icon = 'fa fa-video-o';
        } else {
            $icon = 'fa fa-zip-o';
        }

        return $icon;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return File
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }


}
