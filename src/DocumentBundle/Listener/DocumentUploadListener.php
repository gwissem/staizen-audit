<?php

namespace DocumentBundle\Listener;

use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use DocumentBundle\Uploader\FileUploader;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Oneup\UploaderBundle\Event\PreUploadEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use UserBundle\Entity\User;

class DocumentUploadListener
{

    const PATH_FILES = '/../web/files';
    const PATH_TEMP_FILES = '/../data/temp-files';

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var ContainerInterface
     */
    private $container;

    /** @var  FileUploader $fileUploader */
    private $fileUploader;

    public function __construct(EntityManager $em, ContainerInterface $container, FileUploader $fileUploader)
    {
        $this->em = $em;
        $this->container = $container;
        $this->fileUploader = $fileUploader;
    }

    public function onPreUpload(PreUploadEvent $event)
    {
        $directory = $this->container->getParameter('kernel.root_dir') . self::PATH_TEMP_FILES;
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    public function onUpload(PostPersistEvent $event)
    {

        $document = null;

        /** @var User $user */
        $user = $this->container->get('user.info')->getUser();

        // TODO -  to jest wejście z zewnątrz, trzeba by to chyba jakoś inaczej ogarnąć, żeby rozpoznać formularz Raportu zamknięcia

        if($user === "anon.") {
            $user = $this->em->getRepository('UserBundle:User')->findOneBy(['username' => 'virtual_partner']);
            if(!$user instanceof User) {
                throw new NotFoundResourceException('Not found Virtual Partner!');
            }
            $this->container->get('user.info')->setVirtualUser($user);
        }

        if($event->getRequest()->query->has('document-id')) {
            $documentId = $event->getRequest()->query->get('document-id', null);
            $groupProcessId = $event->getRequest()->query->get('group-process-id', null);

            if(empty($documentId)) {
                $document = new Document();
                $document->setName('process_form_'.$groupProcessId);
                $document->setCreatedBy($user);

                $userCompany = $user->getCompany();

                if($userCompany) {
                    $document->addCompany($userCompany);
                }

                if($userCompany->getId() !== 1) {
                    $starterCompany = $this->em->getRepository('UserBundle:Company')->find(1);
                    $document->addCompany($starterCompany);
                }

                $this->em->persist($document);
            }
            else {
                $document = $this->em->getRepository('DocumentBundle:Document')->find(intval($documentId));
            }
        }

        /** @var \Symfony\Component\HttpFoundation\File\File $file */
        $file = $event->getFile();

        /** @var UploadedFile $originalFile */
        $originalFiles = $event->getRequest()->files->get('files');
        $originalFile = $originalFiles[0];
        $originalName = $originalFile->getClientOriginalName();

        $newFile = new File();
        $name = uniqid();
        $folderPath = sprintf('/%s/%s/', substr($name, 0, 2), substr($name, 2, 2));

        if (!is_dir($this->fileUploader->getTargetDir().$folderPath)) {
            mkdir($this->fileUploader->getTargetDir().$folderPath, 0777, true);
        }

        $ext = $file->getExtension();
        $fileName = $name.'.'.$ext;
        $file = $file->move($this->fileUploader->getTargetDir() . $folderPath, $fileName);

        $filePath = $folderPath.$fileName;

        $newFile->setCreatedBy($user);
        $newFile->setName($originalName);
        $newFile->setFileSize($file->getSize());
        $newFile->setMimeType($file->getMimeType());
        $newFile->setPath($filePath);

        if($document !== null) {
            $document->addFile($newFile);
            $this->em->persist($document);
            $newFile->setDocument($document);
        }

        $this->em->persist($newFile);
        $this->em->flush();

        $response = $event->getResponse();
        $response['fileId'] = $newFile->getId();
        $response['fileUuid'] = $newFile->getUuid();

        if($document !== null) {
            $response['documentId'] = $document->getId();
        }

    }

}
