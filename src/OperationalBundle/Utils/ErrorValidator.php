<?php

namespace OperationalBundle\Utils;

use CaseBundle\Entity\Attribute;
use Symfony\Component\Translation\TranslatorInterface;

class ErrorValidator extends ErrorValidatorBase {

    public function __construct(Attribute $attribute, $value, TranslatorInterface $trans)
    {

        $description = $attribute->translate()->getValidationDescription();

        if($description) {
            if(strpos($description, '{#value#}') !== false) {
                $this->msg = str_replace('{#value#}', (string)$value, $description);
            }
            else {
                $this->msg = $description;
            }

        }
        else {
            $this->msg = $trans->trans('Invalid value: ') . (string)$value;
        }

    }

}