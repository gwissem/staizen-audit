<?php

namespace OperationalBundle\Utils\Timeline;

use UserBundle\Entity\User;

class TimelineAttributeEvent extends TimelineEvent {


    /**
     * TimelineAttributeEvent constructor.
     * @param $path
     * @param $value
     * @param $eventDate
     * @param User|null $user
     * @throws \Exception
     */

    public function __construct($path, $value, $eventDate, User $user = null)
    {

        parent::__construct(TimelineEvent::EVENT_TYPE_ATTRIBUTE,
            new \Datetime($eventDate),
            ($user) ? $user->getId() : NULL,
            ($user) ? $user->getUsername() : NULL,
            $path,
            "Utworzenie lub edycja atrybutu",
            $value
        );

    }

}