<?php

namespace OperationalBundle\Utils\Timeline;

use CaseBundle\Entity\ProcessInstance;

class TimelineProcessFactory {


    static public function createEvents(ProcessInstance $processInstance, $created = true, $enter = true, $leave = true) {

        $processes = [];

        if($created) {
            $processes[] = new TimelineProcessEvent(
                $processInstance,
                TimelineEvent::EVENT_TYPE_STEP_CREATED,
                $processInstance->getCreatedAt(),
                'Utworzono zadanie',
                $processInstance->getCreatedByOriginal()
            );
        }

        if($enter && $processInstance->getDateEnter()) {
            $processes[] = new TimelineProcessEvent(
                $processInstance,
                TimelineEvent::EVENT_TYPE_STEP_ENTER,
                $processInstance->getDateEnter(),
                'Pierwsze wejście na krok',
                NULL
            );
        }

        if($leave && $processInstance->getDateLeave()) {
            $processes[] = new TimelineProcessEvent(
                $processInstance,
                TimelineEvent::EVENT_TYPE_STEP_LEAVE,
                $processInstance->getDateLeave(),
                'Wyjście z kroku',
                NULL
            );
        }

        return $processes;

    }

}