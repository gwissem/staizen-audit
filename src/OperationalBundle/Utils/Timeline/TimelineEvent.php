<?php

namespace OperationalBundle\Utils\Timeline;

abstract class TimelineEvent {

    const EVENT_TYPE_ATTRIBUTE = 'attribute';
    const EVENT_TYPE_STEP_CREATED = 'step_created';
    const EVENT_TYPE_STEP_ENTER = 'step_enter';
    const EVENT_TYPE_STEP_LEAVE = 'step_leave';
    const EVENT_TYPE_ADD_NOTE = 'add_note';
    const EVENT_TYPE_ENTER_OF_USER = 'user_enter_step';
    const EVENT_TYPE_LEAVE_OF_USER = 'user_leave_step';

    protected $eventType;

    protected $eventDate;

    protected $userId;

    protected $userName;

    protected $label;

    protected $description;

    protected $value;

    /**
     * TimelineEvent constructor.
     * @param $eventType
     * @param $eventDate
     * @param $userId
     * @param $userName
     * @param $label
     * @param $description
     * @param null $value
     */
    public function __construct($eventType, $eventDate, $userId, $userName, $label, $description, $value = null)
    {

        $this->eventType = $eventType;
        $this->eventDate = $eventDate;
        $this->userId = $userId;
        $this->userName = $userName;
        $this->label = $label;
        $this->description = $description;
        $this->value = $value;

    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

}