<?php

namespace OperationalBundle\Utils\Timeline;

class TimelineNoteEvent extends TimelineEvent {


    /**
     * TimelineNoteEvent constructor.
     * @param $data
     * @param $noteId
     * @throws \Exception
     */

    public function __construct($data, $noteId)
    {

        $status = ($data['type'] === "email") ? ('(' . $data['status'] . ')') : '';

        parent::__construct(
            TimelineEvent::EVENT_TYPE_ADD_NOTE,
            new \Datetime($data['date']),
            NULL,
            $data['title'],
            '<i class="fa fa-2x block m-y-1 fa-' . $data["icon"] . '"></i> '. $status,
            'Dodano notatkę (' . $noteId . ')',
            $data['content']
        );

    }

    // Dodać kolor czerwony dla nie wysłanej notatki
    // Dodać Attachment notatki ("attachment" => null)
    // Czy notatka specjalna "special" => 0
    // "direction" => "1"

}