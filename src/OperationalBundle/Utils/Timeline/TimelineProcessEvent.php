<?php

namespace OperationalBundle\Utils\Timeline;

use CaseBundle\Entity\ProcessInstance;
use UserBundle\Entity\User;

class TimelineProcessEvent extends TimelineEvent {


    /**
     * TimelineAttributeEvent constructor.
     * @param ProcessInstance $processInstance
     * @param $type
     * @param $eventDate
     * @param $description
     * @param User $user
     */

    public function __construct(ProcessInstance $processInstance, $type, $eventDate, $description, User $user = NULL)
    {

        parent::__construct(
            $type,
            $eventDate,
            ($user) ? $user->getId() : NULL,
            ($user) ? $user->getUsername() : NULL,
            $processInstance->getStep()->getId(),
            $description
        );

    }

}