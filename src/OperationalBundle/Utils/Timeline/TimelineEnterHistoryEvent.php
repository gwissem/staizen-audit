<?php

namespace OperationalBundle\Utils\Timeline;

use UserBundle\Entity\User;

class TimelineEnterHistoryEvent extends TimelineEvent {


    /**
     * TimelineEnterHistoryEvent constructor.
     * @param $type
     * @param $eventDate
     * @param User|null $user
     * @param $stepId
     * @param $description
     * @param null $allTime
     * @param null $realTime
     * @throws \Exception
     */

    public function __construct($type, $eventDate, $stepId, $description, User $user, $allTime = null, $realTime = null)
    {

        parent::__construct(
            $type,
            new \Datetime($eventDate),
            ($user) ? $user->getId() : NULL,
            ($user) ? $user->getUsername() : NULL,
            $stepId,
            $description,
            ($allTime !== NULL) ? ( $realTime . 's | ' . $allTime . 's' ) : NULL
        );

    }

}