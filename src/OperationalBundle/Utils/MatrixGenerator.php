<?php

namespace OperationalBundle\Utils;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeCondition;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\FormTemplate;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\AttributeConditionService;
use CaseBundle\Service\FormTemplateGenerator;
use CaseBundle\Service\ServiceHandler;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Twig_Environment;

class MatrixGenerator
{

    const ID_ATTR_OF_MULTI_MATRIX = 821;

    /** @var FormTemplateGenerator  */
    private $formTemplateGenerator;

    /** @var QueryManager  */
    private $queryManager;

    /** @var QueryManager  */
    private $em;

    /** @var $twig */
    private $twig;

    /** @var ServiceHandler $serviceHandler */
    private $serviceHandler;

    /** @var AttributeConditionService $attributeConditionService */
    private $attributeConditionService;

    private $formControls = [];
    private $entityAttributes = [];
    private $matrix = [
                'services' => []
            ];

    private $groupProcessInstanceId;
    private $rootProcessInstanceId;
    private $processInstanceId;
    private $processInstance;
    private $formControlsAttribute;

    private $pathAllowEdit = [
        '820,821,822,824',
        '820,821,822,825',
        '820,821,822,826',
        '820,821,822,827',
        '820,821,822,828',
        '820,821,822,861'
    ];

    private $orderEntities = [
        "820,821,822,829",
        "820,821,822,221",
        "820,821,822,824",
        "820,821,822,825",
        "820,821,822,826",
        "820,821,822,827",
        "820,821,822,828",
        "820,821,822,861"
    ];

    public function __construct(FormTemplateGenerator $formTemplateGenerator,
                                QueryManager $queryManager,
                                EntityManager $entityManager,
                                Twig_Environment $twig,
                                AttributeConditionService $attributeConditionService,
                                ServiceHandler $serviceHandler
    )
    {

        $this->formTemplateGenerator = $formTemplateGenerator;
        $this->queryManager = $queryManager;
        $this->em = $entityManager;
        $this->twig = $twig;
        $this->attributeConditionService = $attributeConditionService;
        $this->serviceHandler = $serviceHandler;

    }

    /**
     * @param AttributeValue $attributeMatrix
     * @param $attributes
     * @param ProcessInstance $processInstance
     * @return string
     */
    public function generateMatrix(AttributeValue $attributeMatrix, $attributes, ProcessInstance $processInstance) {

        $this->formControlsAttribute = $attributes;

        $this->groupProcessInstanceId = $processInstance->getGroupProcess()->getId();
        $this->rootProcessInstanceId = $processInstance->getRoot()->getId();
        $this->processInstanceId = $processInstance->getId();
        $this->processInstance = $processInstance;

        $multiOfMatrix = $this->em->getRepository('CaseBundle:Attribute')->find(self::ID_ATTR_OF_MULTI_MATRIX);

        /** @var FormTemplate $formTemplateOfMatrix */
        $formTemplateOfMatrix = $multiOfMatrix->getFormTemplate();

        $this->recursiveGetControls($formTemplateOfMatrix->getControls(), '820,821');

        return $this->renderMatrix();
    }

    private function renderMatrix() {

        $matrixId = null;
        $entitiesId = [];

        foreach ($this->formControlsAttribute as $attribute) {
            if($attribute['attribute_path'] === "820") {
                $matrixId = $attribute['id'];
                break;
            }
        }

        $services = $this->findAttributesByParentId($matrixId);

        $infoFromCDN = $this->serviceHandler->getCdnInfo($this->rootProcessInstanceId);

        if(!empty($infoFromCDN)) {
            foreach ($infoFromCDN as $item) {
                if(!empty($item['entityValueId'])) {
                    $entitiesId[] = $item['entityValueId'];
                }
            }
        }

        foreach ($services as $service) {

            $newService = [
                'entities' => []
            ];

            $dataOfServices = $this->findAttributesByParentId($service['id']);

            foreach ($dataOfServices as $dataOfService) {

                if($dataOfService['attribute_path'] == "820,821,822") {
                    $newService['service_entity_is_imported'] = in_array($dataOfService['id'], $entitiesId);
                    $newService['entities'][] = $this->findAttributesByParentId($dataOfService['id']);
                }
                elseif($dataOfService['attribute_path'] == "820,821,805") {
                    $newService['service_name'] = $dataOfService;
                }
                elseif($dataOfService['attribute_path'] == "820,821,830") {
                    $newService['service_program'] = $dataOfService;
                }
                elseif($dataOfService['attribute_path'] == "820,821,823") {
                    $newService['service_start_date'] = $dataOfService;
                }
                elseif($dataOfService['attribute_path'] == "820,821,63") {
                    $newService['service_note'] = $dataOfService;
                }

            }

            $this->matrix['services'][] = $newService;

        }

        $this->renderHtmlOfMatrix();

        $this->orderMatrix();

        return $this->twig->render('@Operational/Matrix/matrix-content.html.twig', [
            'matrix' => $this->matrix
        ]);
    }

    private function orderMatrix() {

        $order = $this->orderEntities;

        if(empty($this->matrix['services'])) return;

        foreach ($this->matrix['services'] as $k1 => $service) {

            if(empty($this->matrix['services'][$k1]['entities'])) continue;

            foreach ($this->matrix['services'][$k1]['entities'] as $k2 => $entities) {

                try {
                    uasort($this->matrix['services'][$k1]['entities'][$k2], function($entity1, $entity2) use($order) {

                        $key1 = array_search($entity1['data']['attribute_path'], $order);
                        if($key1 === FALSE) $key1 = 100;

                        $key2 = array_search($entity2['data']['attribute_path'], $order);
                        if($key2 === FALSE) $key2 = 100;

                        return $key1 > $key2;

                    });
                }
                catch (\Exception $exception) {

                }

            }

        }

    }

    private function renderHtmlOfMatrix() {

        foreach ($this->matrix['services'] as $key => $matrixRow) {

            foreach ($matrixRow as $name => $item) {

                if($name == 'entities') {

                    foreach ($item as $entityKey => $entity) {

                        foreach ($entity as $key2 => $entityArray) {

                            /** @var FormControl $copyFormControl */
                            $copyFormControl = clone $this->formControls[$entityArray['attribute_path']];
                            $this->setTempDataOfControl($copyFormControl, $entityArray['attribute_path']);
                            $this->setAttrValue($entityArray, $copyFormControl->getAttribute());

                            $this->matrix['services'][$key][$name][$entityKey][$key2] = [
                                'html' => $this->formTemplateGenerator->renderHtmlOfFormControl($copyFormControl, false),
                                'data' => $entityArray,
                                'label' => $copyFormControl->getLabel()
                            ];

                        }

                        usort($this->matrix['services'][$key][$name][$entityKey], function ($a, $b){
                            return ($b['data']['attribute_path'] == "820,821,822,829");
                        });
                    }

                }
                elseif($name == "service_entity_is_imported") {

                }
                else {

                    if($item['attribute_path'] == "820,821,63") {
                        $this->matrix['services'][$key][$name] = $item['value_text'];
                    }
                    else {

                        /** @var FormControl $copyFormControl */
                        $copyFormControl = clone $this->formControls[$item['attribute_path']];
                        $this->setTempDataOfControl($copyFormControl, $item['attribute_path']);
                        $this->setAttrValue($item, $copyFormControl->getAttribute());
                        $this->matrix['services'][$key][$name] = $this->formTemplateGenerator->renderHtmlOfFormControl($copyFormControl, false);

                    }

                }

            }

        }
        
    }

    private function setAttrValue($arr, Attribute $attribute) {

        $attribute->setAttrValue('path', $arr['attribute_path']);
        $attribute->setAttrValue('stepId', 'xxx');
        $attribute->setAttrValue('groupProcessId', $this->groupProcessInstanceId);
        $attribute->setAttrValue('id', $arr['id']);
        $attribute->setAttrValue('value', $arr[$attribute->getValueType()]);
        $attribute->setAttrValue('parentAttributeValueId', $arr['parent_attribute_value_id']);

    }

    private function isAutoValue(Attribute $attribute, $arr) {
        return (int)($arr['updated_by_id'] == 1 || $arr['updated_by_id'] == NULL) && !empty($arr[$attribute->getValueType()]) ;
    }
    
    private function findAttributesByParentId($attributeValueId)
    {
        return array_filter($this->formControlsAttribute, function ($item) use ($attributeValueId) {
            return $item['parent_attribute_value_id'] == $attributeValueId;
        });
    }

    private function findFormControlAttribute($id)
    {

        return array_filter($this->formControlsAttribute, function ($item) use ($id) {
            return $item['id'] == $id;
        });

    }

    /**
     * @param ArrayCollection $controls
     * @param string $path
     */
    private function recursiveGetControls($controls, $path = '')
    {

        /** @var FormControl $control */
        foreach ($controls as $control) {

            if(!$control->getAttribute()) continue;

            $this->formControls[$path . ',' . $control->getAttributePath()] = $control;
            $this->entityAttributes[$control->getAttribute()->getId()] = $control->getAttribute();

            if ($control->getAttribute() && $control->getAttribute()->hasFormTemplate()) {
                $this->recursiveGetControls($control->getAttribute()->getFormTemplate()->getControls(), $path . ',' . $control->getAttributePath());
            }

        }

    }

    private function setTempDataOfControl(FormControl $formControl, $fullPath) {

        $formControl->groupProcessId = $this->groupProcessInstanceId;
        $formControl->processInstanceId = $this->processInstanceId;

        if(!in_array($fullPath, $this->pathAllowEdit)) {
            $formControl->controlType = AttributeCondition::TYPE_READ_ONLY;
        }

//        $formControl->isVisible = $this->attributeConditionService->checkCondition('visible', $formControl->getId(), $this->processInstance);
//        $formControl->notAllowEdit = $this->attributeConditionService->checkCondition('notAllowEdit', $formControl->getId(), $this->processInstance);
//        $formControl->controlType = $this->getFormControlHtmlProperty($formControl->getId());


    }

    private function getFormControlHtmlProperty($formControlId){
        if($this->attributeConditionService->checkCondition('required', $formControlId, $this->processInstance)){
            return AttributeCondition::TYPE_REQUIRED;
        }
        else if($this->attributeConditionService->checkCondition('readOnly', $formControlId, $this->processInstance)){
            return AttributeCondition::TYPE_READ_ONLY;
        }
        return AttributeCondition::TYPE_NOT_REQUIRED;
    }


}