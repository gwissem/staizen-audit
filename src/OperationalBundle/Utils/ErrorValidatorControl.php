<?php

namespace OperationalBundle\Utils;

use Symfony\Component\Translation\TranslatorInterface;

class ErrorValidatorControl extends ErrorValidatorBase {

    public function __construct($value, TranslatorInterface $trans)
    {

        /** @Desc("Niepoprawna wartość: ") */
        $this->msg = $trans->trans('error_validator.valid_value') . ": " . (string)$value;

    }

}
