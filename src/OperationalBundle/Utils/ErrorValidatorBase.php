<?php

namespace OperationalBundle\Utils;

use CaseBundle\Entity\Attribute;
use Symfony\Component\Translation\TranslatorInterface;

class ErrorValidatorBase {

    /**
     * @var string
     */
    protected $msg = '';

    /**
     * ErrorValidatorBase constructor.
     */
    public function __construct(){}

    public function getMessage() {
        return $this->msg;
    }

}
