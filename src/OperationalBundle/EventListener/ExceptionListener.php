<?php

namespace OperationalBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExceptionListener
{
    /**
     * Define exception classes that will return
     * JSON response with error message
     *
     * @var array
     */
    private $allowedExceptionClasses = [
        \OperationalBundle\Exception\NotFoundProcessException::class,
    ];

    /**
     * @inheritdoc
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (in_array(get_class($exception), $this->allowedExceptionClasses))
        {
            $response = new JsonResponse([
                'status' => false,
                'message' => $exception->getMessage()
            ]);

            $event->setResponse($response);
        }
    }
}