<?php

namespace OperationalBundle\EventListener;

use AppBundle\Utils\QueryMonitor;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

class MonitorQueryListener implements EventSubscriberInterface
{

    /** @var TokenStorage  */
    private $securityTokenStorage;

    /** @var EntityManager */
    private $em;

    /** @var User|null  */
    private $user = null;

    private $routeName = '';

    /**
     * MonitorQueryListener constructor.
     * @param EntityManager $entityManager
     * @param TokenStorage $storage
     */
    public function __construct(
        EntityManager $entityManager,
        TokenStorage $storage
    )
    {

        $this->securityTokenStorage = $storage;
        $this->em = $entityManager;

        if($storage->getToken()) {
            $user = $storage->getToken()->getUser();
            if(is_object($user)) {
                $this->user = $user;
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function onKernelTerminate(PostResponseEvent $event)
    {

        if($currentRequest = $event->getRequest()) {
            $this->routeName = $currentRequest->attributes->get('_route');
        }

        $queryHashLogRepo =  $this->em->getRepository('AppBundle:QueryHashLog');

//         Wszystkie przechwycone zapytania w QueryManager
        foreach (QueryMonitor::getInstance()->getQueries() as $query) {

            $queryHashLog = $queryHashLogRepo->findOneBy([
                'hash' => $query->getHash()
            ]);

            if(!$queryHashLog) {
                $queryHashLog = $query->createQueryHashLog();
                $this->em->persist($queryHashLog);
                $this->em->flush();
            }

            $queryLog = $query->createQueryLog($queryHashLog);
            $queryLog->setCreatedBy($this->user);
            $queryLog->setRouting($this->routeName);

            $this->em->persist($queryLog);

        };

        $this->em->flush();

    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::TERMINATE => array(array('onKernelTerminate', 0))
        );
    }

}