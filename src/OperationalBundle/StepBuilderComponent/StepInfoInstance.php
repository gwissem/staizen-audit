<?php

namespace OperationalBundle\StepBuilderComponent;

use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;

class StepInfoInstance
{
    /** @var  ProcessInstance */
    public $processInstance;

    /** @var Step */
    public $step;

    public $groupProcessInstanceId;
    public $rootProcessInstanceId;
    public $preview = false;

    public function __construct()
    {

    }

    static function createStepInfoByProcessInstance(ProcessInstance $processInstance) {

        $stepInfo = new StepInfoInstance();
        $stepInfo->processInstance = $processInstance;
        $stepInfo->groupProcessInstanceId = $processInstance->getGroupProcess()->getId();
        $stepInfo->rootProcessInstanceId = $processInstance->getRoot()->getId();
        $stepInfo->step = $processInstance->getStep();

        return $stepInfo;

    }

    static function createStepInfoByStep(Step $step) {

        $stepInfo = new StepInfoInstance();
        $stepInfo->step = $step;
        $stepInfo->preview = false;

        return $stepInfo;

    }
}