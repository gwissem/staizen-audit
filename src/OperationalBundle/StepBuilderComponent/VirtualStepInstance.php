<?php

namespace OperationalBundle\StepBuilderComponent;

use CaseBundle\Entity\FormTemplate;
use CaseBundle\Entity\ProcessInstance;
use Predis\Client;
use Symfony\Component\Stopwatch\Stopwatch;

class VirtualStepInstance
{

    /** @var  ProcessInstance */
    private $processInstance;

    /** @var  FormTemplate */
    private $formTemplate;

    private $controls;

    /** @var Client  */
    private $redis;

    public function __construct(Client $redis)
    {
        $this->redis = $redis;
        $this->controls= [];
    }

    static function createVirtualStep(ProcessInstance $processInstance, Client $redis)
    {

        $virtualStep = new VirtualStepInstance($redis);
        $virtualStep->processInstance = $processInstance;
        $virtualStep->formTemplate = $processInstance->getStep()->getFormTemplate();

        return $virtualStep;

    }

    public function initFormControls() {

        // Próba pobrania z cache

        // Jak nie ma to generate

//        $watcher = new Stopwatch();
//
//        $watcher->start('test');

//        dump(unserialize($this->redis->get('test_controls')));
//        $watcher->lap('test');
//
//        $this->recursiveGenerateFormControls();
//        $watcher->lap('test');
//
//        $result = $watcher->stop('test');
//        dump($result->getPeriods());
//
//        dump($this->controls);

//        $this->redis->set('test_controls', serialize($this->controls));


    }

    private function recursiveGenerateFormControls() {

        foreach ($this->formTemplate->getControls() as $control) {

            $this->controls[] = new FormControlInstance($control);

        }

//        /** @var FormControl $control */
//        foreach ($formTemplate->getControls() as $control) {
//
//            $children = [];
//            $childFormTemplate = null;
//            $this->setTempData($control);
//
//            if ($control->getAttribute() && $control->getAttribute()->hasFormTemplate()) {
//
//                $childFormTemplate = $control->getAttribute()->getFormTemplate();
//                $this->recursivePrepareControls($childFormTemplate->getControls(), $children);
//
//            }
//
//            $this->controlsArray[] = $this->prepareControl($control, $children, $childFormTemplate);
//        }

    }

}