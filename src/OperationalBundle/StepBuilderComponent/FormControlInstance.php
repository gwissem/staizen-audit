<?php

namespace OperationalBundle\StepBuilderComponent;

use CaseBundle\Entity\FormControl;

class FormControlInstance
{

    public $attributeValueId;

    public $attributeId;

    public $formControlId;

    public $path;

    public $x;

    public $y;

    public $width;

    public $height;

    public $label;

    public $styles;

    public $widget;

    public $extraData;

    public $customClass;

    public $isVisible;

    public $notAllowEdit;

    public $controlType;

    public $html;

    public $multi;

    /** @var  FormControlInstance[] */
    public $children = [];

    /** @var  StepInfoInstance */
    private $stepInfo;

    public function __construct(FormControl $formControl)
    {
        $this->formControlId = $formControl->getId();
    }

    /**
     * @return StepInfoInstance
     */
    public function getStepInfo(): StepInfoInstance
    {
        return $this->stepInfo;
    }

    /**
     * @param StepInfoInstance $stepInfo
     */
    public function setStepInfo(StepInfoInstance $stepInfo)
    {
        $this->stepInfo = $stepInfo;
    }

}