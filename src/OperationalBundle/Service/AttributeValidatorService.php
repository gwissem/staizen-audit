<?php

namespace OperationalBundle\Service;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeCondition;
use CaseBundle\Service\AttributeConditionService;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Service\ProcessHandler;
use CaseBundle\Utils\Language;
use Doctrine\ORM\EntityManagerInterface;
use OperationalBundle\Utils\ErrorValidator;
use OperationalBundle\Utils\ErrorValidatorControl;
use Symfony\Component\Translation\TranslatorInterface;

class AttributeValidatorService
{
    const ATTRIBUTE_SEPARATOR = ',';

    protected $em;
    protected $processHandler;
    protected $conditionService;
    protected $attributeParser;
    protected $translator;

    public function __construct(EntityManagerInterface $em, ProcessHandler $processHandler, AttributeConditionService $conditionService, AttributeParserService $attributeParser, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->processHandler = $processHandler;
        $this->conditionService = $conditionService;
        $this->attributeParser = $attributeParser;
        $this->translator = $translator;
    }

    public function validateFormControlRule(AttributeCondition $attributeCondition) {

        $rule = $attributeCondition->getRules();

        if (empty($rule)) {
            return null;
        }



    }

    public function validate(string $attributePath, $value, $groupProcessId, AttributeCondition $attributeCondition = null)
    {
        if (empty($attributePath) || $value === '' || $value === NULL) {
            return true;
        }

        $attribute = null;
        $rule = null;

        if($attributeCondition) {
            $rule = $attributeCondition->getRules();
        }

        if (empty($rule)) {

            $attributeId = $this->getAttributeChild($attributePath);
            $attribute = $this->em->getRepository(Attribute::class)->find($attributeId);

            if(!$attribute) return true;

            $rule = $attribute->getValidationRule();

            if (empty($rule)) {
                return true;
            }

        }

        $this->attributeParser->setGroupProcessInstanceId($groupProcessId);
        $rule = str_replace('{#value#}', $value, $rule);
        $rule = str_replace('{#path#}', $attributePath , $rule);

        $lang = new Language();

        try {
            $isValid = $lang->evaluate($this->attributeParser->parseString($rule,false,false,null,false, true));
        }
        catch(\Throwable $e) {
            $isValid = false;
        }

        if(!$isValid) {
            if($attribute) {
                return new ErrorValidator($attribute, $value, $this->translator);
            }
            else {
                return new ErrorValidatorControl($value, $this->translator);
            }
        }

        return true;
    }

    private function getAttributeChild(string $attributePath): string
    {
        if (strpos($attributePath, self::ATTRIBUTE_SEPARATOR) !== false) {
            return substr($attributePath, strrpos($attributePath, self::ATTRIBUTE_SEPARATOR) + 1);
        }
        return $attributePath;
    }

    private function getRule($attributeId)
    {
        if ($attributeId) {
            /** @var Attribute $attribute */
            $attribute = $this->em->getRepository(Attribute::class)->findOneById($attributeId);
            if($attribute) {
                return $attribute->getValidationRule();
            }
        }
        return '';
    }

    public function validateRequired($value, $formControlId, $processInstanceId)
    {
        if ($this->conditionService->checkCondition('required', $formControlId, $processInstanceId)) {
            return ($value !== NULL && $value !== '');
        }

        return true;
    }

    public function validateSemiRequired(string $value, int $formControlId, $processInstanceId): bool
    {
        if ($this->conditionService->checkCondition('semiRequired', $formControlId, $processInstanceId)) {
            return ($value !== NULL && $value !== '');
        }

        return true;
    }

}