<?php

namespace OperationalBundle\Service;

use CaseBundle\Service\AttributeParserService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Predis\Client;
use SocketBundle\Topic\TaskManagerTopic;
use ToolBundle\Utils\ToolQueryManager;
use CaseBundle\Entity\ProcessEnterHistory;

class ApiTaskService
{

    /** @var  Client */
    private $redis;

    /** @var  EntityManager */
    private $em;

    /**
     * ApiTaskService constructor.
     * @param Client $predis
     * @param EntityManager $em
     */
    public function __construct(Client $predis, $em)
    {
        $this->redis = $predis;
        $this->em = $em;
    }

    /**
     * @param string $format
     * @return array
     */
    public function getAllActiveTasks($format = 'array') {

        $activeTasks = $this->getActiveTasks();

        if($format == "string") {
            return $this->parseResultAsString($activeTasks);
        }
        if($format == "assoc-array") {
            return $this->parseResultAsAssociativeArray($activeTasks);
        }
        else {
            return $this->parseResultAsArray($activeTasks);
        }
    }

    /**
     * @param $rootId
     * @return mixed
     */
    public function getAllProcess($rootId)
    {
        return $this->em->getRepository(ProcessEnterHistory::class)->getAllProcessFromRootId($rootId);
    }

    /**
     * @param $id
     * @param string $format
     * @param array $activeTasks
     * @return array|string
     */
    public function getActiveTasksOfUser($id, $format = 'array', $activeTasks = []) {

        if(empty($activeTasks)) {
            $activeTasks = $this->getActiveTasks();
        }

        $searchKey = '|' . $id . '|';

        $activeTaskOfUser = array_filter($activeTasks, function ($v, $k) use ($searchKey) {
            return (strpos($v, $searchKey) !== false);
        }, ARRAY_FILTER_USE_BOTH);

        if($format == "string") {
            return $this->parseResultAsString($activeTaskOfUser);
        }
        else {
            return $this->parseResultAsArray($activeTaskOfUser);
        }

    }

    public function amountUsersInCase($rootId, $activeUsersInCase = null) {

        if($activeUsersInCase === null) {
            $activeUsersInCase = $this->getActiveUsersInCase($rootId);
        }

        $users = [];

        foreach ($activeUsersInCase as $item) {
            if(!in_array($item['user_id'],$users)) {
                $users[] = $item['user_id'];
            }
        }

        return count($users);
    }

    public function getUsersInCase($rootId) {

        $result = $this->getActiveUsersInCase($rootId);

        return [
            'tasks' => $result,
            'amount_users' => $this->amountUsersInCase($rootId, $result)
        ];

    }

    public function getActiveUsersInCase($rootId) {

        $activeTasks = $this->getActiveTasks();
        $searchKey = '|' . $rootId;

        $activeUsersInCase = array_filter($activeTasks, function ($v, $k) use ($searchKey) {
            return (strpos($v, $searchKey) !== false);
        }, ARRAY_FILTER_USE_BOTH);

        return $this->parseResultAsArray($activeUsersInCase);

    }

    public function getActiveTasks() {
        return $this->redis->hGetAll(TaskManagerTopic::ENTER_ON_TASK);
    }

    protected function parseResultAsAssociativeArray($activeTasks) {

        $result = [];

        foreach ($this->parseResultAsArray($activeTasks) as $item) {
            $result[$item['process_instance_id']] = $item;
        }

        return $result;
    }

    protected function parseResultAsArray($activeTasks) {

        $result = [];

        foreach ($activeTasks as $activeTask) {

            $data = explode('|', $activeTask);

            $result[] = [
                'process_instance_id' => $data[0],
                'user_id' => $data[1],
                'timestamp' => $data[2],
                'root_id' => (isset($data[3])) ? $data[3]: null
            ];

        }

        return $result;

    }

    protected function parseResultAsString($activeTasks) {

        return implode('&', $activeTasks);

    }


}