<?php

namespace OperationalBundle\Service;

use AppBundle\Entity\ValueDictionary;
use AppBundle\Service\ElasticSearchLogService;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeCondition;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\CustomValidationMessageTranslation;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Service\CaseFormGenerator;
use CaseBundle\Service\ProcessHandler;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use MssqlBundle\PDO\PDO;
use OperationalBundle\Controller\OperationDashboardApi;
use OperationalBundle\Utils\ErrorValidatorBase;
use OperationalBundle\Utils\MatrixGenerator;
use SocketBundle\Exception\AtlasWebsocketException;
use SocketBundle\Service\RedisListListenerService;
use SocketBundle\Topic\TaskManagerTopic;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Translation\Translator;
use UserBundle\Entity\User;

class OperationDashboardInterface
{

    const KEY_REDIS_SPECIAL_SITUATION = 'special_situation_';

    const PATH_NUMBER_OF_CLIENT = '197';
    const PATH_FAQ_ANSWER = '997';
    const PATH_PLATFORM_ID = '253';
    const FOLLOW_UP_START = '1006.001';
    const PATH_CASE_ID = '235';
    const PATH_LINE_ID = '321';
//    const PATH_WELCOME = '403';

    const PROCESS_STOP = '-999';
    const ARC_TWIN_URL = 'https://atp.eu.com/arctwin/PendingEvents';
    const SPECIAL_SITUATION = '415';

    /** @var ContainerInterface  */
    private $container;
    /** @var EntityManager */
    private $em;
    /** @var  Translator $translator */
    private $translator;
    /** @var  AttributeValidatorService $attributeValidator */
    private $attributeValidator;
    /** @var  ProcessHandler */
    private $processHandler;

    /** @var ElasticSearchLogService */
    private $esLogger;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    private function setServices() {
        if(!$this->translator) $this->translator = $this->container->get('translator');
        if(!$this->attributeValidator) $this->attributeValidator = $this->container->get('operational.attribute_validator');
        if(!$this->processHandler) $this->processHandler = $this->container->get('case.process_handler');
    }

    /**
     * @param ProcessInstance $processInstance
     * @param array $opts
     * @return array
     * @throws AtlasWebsocketException
     */
    public function getTask(ProcessInstance $processInstance, $opts = []) {

        $stopwatch = new Stopwatch();
        $stopwatch->start('getTask');

        $options = $this->setDefaults_getTask($opts);

        /** @var User $user */
        $user = $this->getUser();
        $isAdmin = ($user instanceof User) ? $user->isAdmin() : false;

        if($options['process_token'] || $options['previewMode']){
            $hasPermission = 1;
        }
        else {
            $hasPermission = $this->container->get('case.process_handler')->checkPermission($user->getId(), $processInstance->getId());
        }

        try {

            $form = $this->container->get('case.form.generator')->generateFormOfStepInstance($processInstance, [
                'publicForm' => $options['publicForm'],
                'process_token' => $options['process_token'],
                'previewMode' => $options['previewMode']
            ]);

            $stopwatch->lap('getTask');

            $this->container->get('case.user_process_history_manager')->logProcess($processInstance);

        } catch (\Throwable $exception) {

            $this->logException($exception);

            throw new AtlasWebsocketException($exception, $isAdmin, $this->container->get('translator.default')->trans('fail_get_task'));

        }

        if($processInstance->getActive() == 0 && $options['process_token']) {
            $form['html'] = str_replace('class="operation-dashboard-form', 'class="operation-dashboard-form form-readonly-mode', $form['html']);
        }

        /** Ustawienie pierwszego date_enter */
        if($processInstance->getActive() && $processInstance->getDateEnter() === NULL) {
            $this->container->get('case.process_handler')->updateDateEnterIfEmpty($processInstance->getId());
        }

        $response = [
            'id' => $processInstance->getId(),
            'permission' => $hasPermission,
            'html' => $form['html'],
            'controls' => $form['controls'],
            'prependHtml' => $form['prependHtml'],
            'type' => $form['type'],
            'processInfo' => [
                'id' => $processInstance->getId(),
                'groupProcessId' => $processInstance->getGroupProcess()->getId(),
                'rootId' => $processInstance->getRoot()->getId(),
                'stepId' => $processInstance->getStep()->getId()
            ]
        ];

        if($options['processInfo'] ) {
            $response['processInfo'] = $form['params'];
        }

        $this->logGetTaskWatcher($stopwatch, $response['processInfo']);

        return $response;

    }

    private function setDefaults_getTask($opts) {
        $resolver = new OptionsResolver();
        $resolver->setDefaults(array(
            'displayNotes' => true,
            'processInfo' => false,
            'publicForm' => false,
            'process_token' => null,
            'previewMode' => false
        ));
        return $resolver->resolve($opts);
    }

    /**
     * @param ProcessInstance $processInstance
     * @return array
     * @throws AtlasWebsocketException
     */
    public function getOverview(ProcessInstance $processInstance)
    {

        try {

            $form = $this->container->get('case.form.generator')->generateFormOfStepInstance($processInstance, ['overview' => true]);
            $overview = $this->container->get('case.attribute_parser.service')->getOverview($processInstance->getId());

        } catch (\Throwable $exception) {

            $this->logException($exception);

            /** @var User $user */
            $user = $this->getUser();

            throw new AtlasWebsocketException($exception, $user->isAdmin(), $this->container->get('translator.default')->trans('fail_get_overview'));

        }

        return [
            'id' => $processInstance->getId(),
            'controls' => [],
            'notes' => $form['notes'],
            'timeline' => [],
            'prependHtml' => $form['prependHtml'],
            'caseInfo' => NULL,
            'overview' => $overview
        ];

    }

    /**
     * @param ProcessInstance $processInstance
     * @param string $locale
     * @return array
     * @throws AtlasWebsocketException
     */
    public function getTimeLine(ProcessInstance $processInstance, $locale = 'pl')
    {

        $userInfo = $this->container->get('user.info');

        try {

            $timeLine = $this->container->get('case.process_handler')->tasks(
                $userInfo->getUser()->getId(),
                $userInfo->getOriginalUser()->getId(),
                NULL,
                1,
                $locale,
                $processInstance->getId(),
                1);

        } catch (\Throwable $exception) {

            $this->logException($exception);

            throw new AtlasWebsocketException($exception, $userInfo->getUser()->isAdmin(), $this->container->get('translator.default')->trans('fail_get_timeline'));

        }

        return array(
            'timeLine' => $timeLine
        );

    }

    /**
     * @param ProcessInstance $processInstance
     * @param string $locale
     * @return array
     * @throws AtlasWebsocketException
     */
    public function getDynamicWidget(ProcessInstance $processInstance, $options, $locale = 'pl')
    {

        $userInfo = $this->container->get('user.info');

        try {

            if($options['name'] === 'matrix') {
                $html = $this->renderMatrix($processInstance);
            }
            elseif($options['name'] === 'faq') {
                $html = $this->renderFaq($processInstance);
            }
            else {
                $html = 'Widget do not exists.';
            }

        } catch (\Throwable $exception) {

            $this->logException($exception);
            throw new AtlasWebsocketException($exception, $userInfo->getUser()->isAdmin(), $this->container->get('translator.default')->trans('fail_get_timeline'));

        }

        return array(
            'html' => $html
        );

    }

    /**
     * @param ProcessInstance $processInstance
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function renderFaq(ProcessInstance $processInstance) {

        $processHandler = $this->container->get('case.process_handler');
        $groupProcessInstance = $processInstance->getGroupProcess();
        $dictionaryRepo = $this->em->getRepository(ValueDictionary::class);
        $currentCategory = null;

        $answerAttributeValue = $processHandler->getAttributeValue(self::PATH_FAQ_ANSWER, $groupProcessInstance->getId(), AttributeValue::VALUE_STRING);

        if(!empty($answerAttributeValue)) {

            $values = explode(',', $answerAttributeValue);

            $firstId = $values[0];

            /** @var ValueDictionary $question */
            $question = $dictionaryRepo->findOneBy([
                'id' => intval($firstId)
            ]);

            /** @var ValueDictionary $categoryOfQuestion */
            $categoryOfQuestion = $dictionaryRepo->findOneBy([
                'value' => str_replace('ALP_Cat', '', $question->getType()),
                'type' => 'ALP_Cat'
            ]);

            $currentCategory = 'ALP_Cat' . $categoryOfQuestion->getValue();

        }

        /** @var ArrayCollection $categories */
        $categories = $dictionaryRepo->findBy(['type' => 'ALP_Cat']);

        return $this->container->get('twig')->render('@Case/FormTemplate/templates/faq-wrapper.html.twig', [
            'currentCategory' => $currentCategory,
            'categories' => $categories
        ]);
    }

    /**
     * @param AttributeValue $attributeValue
     * @param ProcessInstance $processInstance
     * @return array
     */
    public function attributeValueHistory(AttributeValue $attributeValue, ProcessInstance $processInstance)
    {

        $this->setServices();

        $response = [
            'id' => intval($attributeValue->getId()),
            'histories' => []
        ];

        $valueType = $attributeValue->getAttribute()->getValueType();
        $inputType = $attributeValue->getAttribute()->getInputType();
        $query = $attributeValue->getAttribute()->getQuery();

        if($query) {
            $options = $this->container->get('updater.form.control')->getOptions(
                $processInstance->getGroupProcess()->getId(),
                $query,
                $processInstance->getId(),
                $attributeValue->getAttributePath());

        }
        else {
            $options = null;
        }

        $histories = $this->processHandler->getAttributeHistory($attributeValue->getId());

        foreach ($histories as $history) {
            $response['histories'][] = [
                'userName' => $history['user'],
                'date' => (new \DateTime($history['date']))->format('Y-m-d H:i:s'),
                'value' => $this->getValueOfAttribute($history, $valueType, $inputType, $options)
            ];
        }

        return $response;

    }

    private function getValueOfAttribute($row, $valueType, $inputType, $options) {


        $value = $row[$valueType];

        if($value === null) return null;

        if($inputType === Attribute::INPUT_TYPE_YESNO) {
            $booleanValue = filter_var($value, FILTER_VALIDATE_BOOLEAN);
            return (($booleanValue) ? $this->translator->trans('tak') : $this->translator->trans('nie'));
        }
        elseif($inputType === Attribute::INPUT_TYPE_CHECKBOX) {
            $booleanValue = filter_var($value, FILTER_VALIDATE_BOOLEAN);
            return (($booleanValue) ? '✔' : '✘');
        }
        elseif(in_array($inputType, [Attribute::INPUT_TYPE_SELECT, Attribute::INPUT_TYPE_MULTISELECT, Attribute::INPUT_TYPE_RADIO]) && !empty($options)) {

            foreach ($options as $key => $option) {
                if($option == $value) {
                    return $key;
                }
            }

            return $value;

        }

        return $row[$valueType];
    }

    private function renderMatrix(ProcessInstance $processInstance) {

        $attributeMatrix = $this->em->getRepository('CaseBundle:AttributeValue')->findOneBy([
            'attributePath' => 820,
            'groupProcessInstance' => $processInstance->getRoot()
        ]);

        if(!$attributeMatrix) {
            return 'Nie istnieje.';
        }

        $this->setServices();
        $queryManager = $this->container->get('app.query_manager');

        $attributes = $this->processHandler->getAttributeStructureValue($attributeMatrix->getId());

        $formTemplateGenerator = $this->container->get('form_template.generator');
        $formTemplateGenerator->setFullMode();
        $formTemplateGenerator->setAdditionalData([
            'groupProcessId' => $processInstance->getGroupProcess()->getId(),
            'processInstanceId' => $processInstance->getId(),
            'formControlsAttribute' => $attributes,
            'stepId' => $processInstance->getStep()->getId()
        ]);

        $matrixGenerator = new MatrixGenerator(
            $formTemplateGenerator,
                $queryManager,
                $this->em,
                $this->container->get('twig'),
                $this->container->get('case.attribute_condition'),
                $this->container->get('case.service_handler'));

        $matrixHtml = $matrixGenerator->generateMatrix($attributeMatrix, $attributes, $processInstance);

        return $matrixHtml;
    }


    /**
     * @param ProcessInstance $processInstance
     * @param $queryOptions
     * @param string $locale
     * @return array
     * @throws AtlasWebsocketException
     */
    public function nextStep(ProcessInstance $processInstance, $queryOptions, $locale = 'pl')
    {

        $stopwatch = new Stopwatch();
        $stopwatch->start('nextStep');

        if(!array_key_exists('variant', $queryOptions) || $queryOptions['variant'] === '') $queryOptions['variant'] = 1;

        if(isset($queryOptions['description'])) $queryOptions['description'] = filter_var($queryOptions['description'], FILTER_VALIDATE_BOOLEAN);
        if(isset($queryOptions['omitValidation'])) $queryOptions['omitValidation'] = filter_var($queryOptions['omitValidation'], FILTER_VALIDATE_BOOLEAN);
        if(isset($queryOptions['skipStepProcedure'])) $queryOptions['skipStepProcedure'] = filter_var($queryOptions['skipStepProcedure'], FILTER_VALIDATE_BOOLEAN);
        if(isset($queryOptions['skipErrors'])) $queryOptions['skipErrors'] = filter_var($queryOptions['skipErrors'], FILTER_VALIDATE_BOOLEAN);
        if(isset($queryOptions['skipPostpone'])) $queryOptions['skipPostpone'] = filter_var($queryOptions['skipPostpone'], FILTER_VALIDATE_BOOLEAN);

        $resolver = new OptionsResolver();
        $resolver->setDefaults(array(
            'id' => 0,
            'variant' => 1,
            'description' => true,
            'omitValidation' => false,
            'skipStepProcedure' => false,
            'skipErrors' => true,
            'omitedInputs' => [],
            'omitReason' => '',
            'process_token' => null,
            'skipPostpone' => 0
        ));

        $options = $resolver->resolve($queryOptions);

//        $options['skipPostpone'] = ($options['skipPostpone']) ? 1 : 0;

        $userInfo = $this->container->get('user.info');
        $translator = $this->container->get('translator.default');

        try {

            $processHandler = $this->container->get('case.process_handler');
            $variant = $options['variant'];
            $skipStepFunction = $options['skipStepProcedure'];
            $skipErrors = $options['skipErrors'];
            if ($variant != 99) {

                if (!$options['omitValidation']) {

                    $validationResult = $this->validateNextProcess($processInstance);

                    if ($validationResult['error'] > 0) {
                        return $validationResult;
                    }
                }

                if (!(empty($options['omitedInputs']) || empty($options['omitReason']))) {

                    /** @var AttributeValue[] $attributes */
                    $attributes = $this->em->getRepository('CaseBundle:AttributeValue')->findBy(['id' => $options['omitedInputs']]);

                    foreach ($attributes as $attribute) {
                        $attribute->setOmitSemirequiredReason($options['omitReason']);
                        $this->em->flush();
                    }
                }
            }

            /** request API CALL if defined for instance step */
//            $this->container->get('api_call.manager')->queueApiCall($processInstance);

            $previousProcessInfo = $processHandler->getProcessInstanceDescription($processInstance->getId());

            $stopwatch->lap('nextStep');

            $nextResult = $processHandler->next(
                $processInstance->getId(),
                $variant,
                $userInfo->getUser()->getId(),
                $userInfo->getOriginalUser()->getId(),
                0,
                $skipStepFunction,
                $skipErrors);

            $stopwatch->lap('nextStep');

            if ($previousProcessInfo['stepId'] == '1011.002' && $variant < 98) {

                $processHandler->cepikAttributes($previousProcessInfo['groupProcessId']);

                /** Chwilowo zakomentowane kolejkowanie Cepika */
//                $job = new Job('atlas:case:call-cepik', [$previousProcessInfo['groupProcessId']]);
//                $this->em->persist($job);
//                $this->em->flush($job);

            }

            $this->logNextStepWatcher($stopwatch, $previousProcessInfo);

            $data = ['message' => '', 'id' => NULL];

            if(!$skipErrors){
                $data = [
                    'message' => $nextResult['message'],
                    'error' => $nextResult['err'],
                    'id' => NULL
                ];
                $nextProcessInstances = $nextResult['ids'];
            }
            else{
                $nextProcessInstances = $nextResult;
            }

            usort($nextProcessInstances, function ($a, $b) {
                if(intval($b) == "-999") return false;
                return (intval($a) < intval($b));
            });


            if(!empty($nextResult['token'])) {

                $data['token'] = $nextResult['token'];

                /** @var ProcessInstance $tokenProcess */
                $tokenProcessId = $this->em->getRepository('CaseBundle:ProcessInstance')->findActiveProcessByToken($nextResult['token']);
                if($tokenProcessId !== null) {
                    $data['id'] = $tokenProcessId['id'];
                }
            }
            else {
                foreach ($nextProcessInstances as $instance) {

                    if(empty($instance)) continue;

                    if ((string)$instance !== self::PROCESS_STOP) {

                        if($processInstance->getStep()->getId() == '1012.006' ){
                            $hasAccess = true;
                        }
                        else{
                            $hasAccess = $this->container->get('case.process_handler')->checkPermission($userInfo->getUser()->getId(), intval($instance), 1);
                        }

                        if ($hasAccess) {
                            $data['id'] = $instance;
                            break;
                        }

                    } else {

                        if(!empty($nextResult['notification'])) {
                            $message = $nextResult['notification'];
                        }
                        else {
                            $message = 'Proces został przekazany do dalszej realizacji.';
                        }

                        $err = 0;
                        $groupProcessId = $previousProcessInfo['groupProcessId'];

                        if (strpos($previousProcessInfo['stepId'], '1023.') !== false) {
                            try {
                                $message = $translator->trans('Zdarzenie zostało poprawnie zakończone i zamknięte w platformie ARC');
                                $this->container->get('apt_event_handler')->closeCase($groupProcessId);
                            } catch (\Exception $e) {
                                $err = 1;
                                $arcTwinUrl = $processHandler->getAttributeValue('372,377', $groupProcessId, AttributeValue::VALUE_STRING) ?: self::ARC_TWIN_URL;
                                $message = $translator->trans('Wystąpił błąd komunikacji webserwisu i zdarzenie nie zostało poprawnie zamknięte w systemie ARC') . '.<br/>' .
                                    '<a href="' . $arcTwinUrl . '" target="_blank">' . $translator->trans('Kliknij, aby przejść do ARCTwin') . '</a>';
                            }
                        }
                        $data['message'] = $message;
                        $data['err'] = $err;

                        if(isset($previousProcessInfo['rootId']) && isset($previousProcessInfo['stepId']) && isset($previousProcessInfo['name'])) {
                            $data['caseNumber'] = $previousProcessInfo['rootId'];
                            $data['stepId'] = $previousProcessInfo['stepId'];
                            $data['name'] = $previousProcessInfo['name'];
                        }

                        return $data;
                    }
                }
            }

            /** Jeżeli istnieje kolejny krok i wymagam szczegółowych danych */

            if ($data['id'] && $options['description']) {
                /**
                 *  id, groupProcessId, stepId, name, description, dateEnter, lifeTime, percentage, referenceTime
                 */
                $description = $processHandler->getProcessInstanceDescription($data['id']);
                $data['stepId'] = $description['stepId'];
                $data['groupProcessId'] = $description['groupProcessId'];
                $data['description'] = $description;

            }

            $error = array_key_exists('error',$data) ? $data['error'] :  0;

            $isError = ($error < 0 || (isset($data['err']) && $data['err'] != 0));

            if ($data['id'] === NULL && !$isError) {
                if($options['process_token']) {
                    $data['message'] = "Dziękujemy za uzupełnienie formularza.";
                }
                else {
                    if(!empty($nextResult['notification'])) {
                        $data['message'] = $nextResult['notification'];
                    }
                    else {
                        $data['message'] = "Proces został przekazany do dalszej realizacji.";
                    }
                }
            }

            if($error < -100) {
                $data['info_error'] = true;
            }

//            else if($options['process_token']) {
//
//                /** @var ProcessInstance $newProcessInstance */
//                $newProcessInstance = $this->em->getRepository('CaseBundle:ProcessInstance')->find($data['id']);
//
//                if($newProcessInstance) {
//                    $newProcessInstance->setToken($options['process_token']);
//                    $this->em->persist($newProcessInstance);
//                    $this->em->flush();
//                }
//            }

        }
        catch (\Throwable $exception) {

            $this->logException($exception);
            throw new AtlasWebsocketException($exception, $userInfo->getUser()->isAdmin(), $translator->trans('fail_next_step'));

        }

        return $data;

    }

    /**
     * @param $idPlatform
     * @param null $numberPhone
     * @param string $stepId
     * @param string $callLineId
     * @return null
     */
    public function createNewCase($idPlatform, $numberPhone = null, $stepId = null, $callLineId = null) {

        $this->setServices();
        $eCallPlatformId = $this->container->getParameter('e_call_platform_id');
        $carCallHandler = $this->container->get('case.car_call_handler');
        $userInfo = $this->container->get('user.info');
        $userId = $userInfo->getUser()->getId();
        $originalUserId = $userInfo->getOriginalUser()->getId();

        $processId = null;

        if ($idPlatform == $eCallPlatformId) {

            /** Stworzenie sprawy dla E-CALL */
            $currentProcessInstances = $carCallHandler->handleEcallPhoneRequest($numberPhone, [
                'userId' => $userId,
                'originalUserId' => $originalUserId
            ], $idPlatform);

            $processId = (!empty($currentProcessInstances[0])) ? $currentProcessInstances[0]['id'] : null;

        } else {
            /** generowanie sprawy dla innych platform */

            if($stepId === null) {
                $stepId = OperationDashboardApi::CALL_PHONE_PROCESS_ID;
            }

            /** 1 */
            $processInstanceId = $this->processHandler->start($stepId, NULL,
                $userId, $originalUserId);

            /** 2. UStawianie parametrów:
             * 197 [nr telefonu którym gościu dzwoni]
             * 253 [id platformy]
             */

            $this->processHandler->setAttributeValue(
                self::PATH_NUMBER_OF_CLIENT,
                $numberPhone,
                AttributeValue::VALUE_STRING,
                $stepId,
                $processInstanceId,
                $userId, $originalUserId
            );

            $this->processHandler->setAttributeValue(
                self::PATH_PLATFORM_ID,
                $idPlatform,
                AttributeValue::VALUE_INT,
                $stepId,
                $processInstanceId,
                $userId, $originalUserId
            );

            if($stepId == self::FOLLOW_UP_START){
                $this->processHandler->setAttributeValue(
                    self::PATH_CASE_ID,
                    9999,
                    AttributeValue::VALUE_STRING,
                    $stepId,
                    $processInstanceId,
                    $userId, $originalUserId
                );
            }


            $this->processHandler->setAttributeValue(
                self::PATH_LINE_ID,
                $callLineId,
                AttributeValue::VALUE_STRING,
                $stepId,
                $processInstanceId,
                $userId, $originalUserId
            );

            /** 3. Odpalanie next dla instancji którą stworzono w punkcie 1 */
            $processIds = $this->processHandler->next($processInstanceId, 1, $userId, $originalUserId);

            foreach ($processIds as $instance) {
                if ((string)$instance !== self::PROCESS_STOP) {
                    $processId = $instance;
                } else {

                    /** //TODO  co robić jeżeli stepId == 999 */
//                    $previousProcessInfo = $this->processHandler->getProcessInstanceDescription($previousId);
//                    $this->aptEventHandler->closeCase($previousProcessInfo['groupProcessId']);
//                    $data['message'] = 'Proces został przekazany do dalszej realizacji.';
                }
            }

        }

        return $processId;

    }

    /**
     * @param ProcessInstance $processInstance
     * @return string
     */
    private function sendSparx(ProcessInstance $processInstance)
    {

        $sparxManager = $this->container->get('sparx.sparx_manager');

        /** @var Step $step */
        $step = $this->em->getRepository('CaseBundle:Step')->findOneById($processInstance->getStep()->getId());

        if ($step->getCallSparx()) {
            if ($sparxManager->toUpdate($processInstance->getGroupProcess()->getId())) {
                return $sparxManager->update($processInstance->getGroupProcess()->getId(), $step);
            }
            return $sparxManager->create($processInstance->getGroupProcess()->getId(), $step);
        }

        return '';
    }

    private function isECallFirstStep(ProcessInstance $processInstance): bool
    {
        return ($processInstance->getStep()->getId() == "1023.002");
    }

    public function validateProcess($processId) {

        /** @var ProcessInstance $processInstance */
        $processInstance = $this->em->getRepository('CaseBundle:ProcessInstance')->find($processId);
        return $this->validateNextProcess($processInstance);

    }

    private function validateNextProcessECall($groupProcessId) {

        $firstSelect = $this->container->get('case.process_handler')->getAttributeValue('372,369,370', $groupProcessId);

        $firstSelectValue = (!empty($firstSelect)) ? $firstSelect[0]['value_string'] : "";

        $eCallControls = [];

        if ($firstSelectValue == "") {
            $eCallControls[] = "#EventQualificationSelector";
        }

        return [
            'error' => (empty($eCallControls)) ? 0 : 3,
            'desc' => (empty($eCallControls)) ? "" : $this->translator->trans('This value is required'),
            'eCallControls' => $eCallControls
        ];

    }

    private function validateNextProcess(ProcessInstance $processInstance, $controls = null)
    {

        $this->setServices();

        $attributeValidator = $this->container->get('operational.attribute_validator');

        $groupProcessId = $processInstance->getGroupProcess()->getId();

        if ($this->isECallFirstStep($processInstance)) {
            return $this->validateNextProcessECall($groupProcessId);
        }

        if($controls === null) {
            $controls = $this->processHandler->formControls($processInstance->getId(), null, 0);
        }

        $semiRequireds = [];
        $noValidControls = [];

        $excludedFormControls = $processInstance->getStep()->getExcludedFormControls();
        $isExcluded = (!empty($excludedFormControls));
        $availableTypes = ['int', 'string', 'date', 'text', 'decimal'];

        foreach ($controls as $control) {

            /**  Pomijanie kontrolek z notatką */
            if (substr($control['attribute_path'], 0, 3) === CaseFormGenerator::NOTES) {
                continue;
            }

            /** Pomijanie kontrolek których Atrybut jest w 'excludedAttributes' */

            if($isExcluded) {
                if( in_array( $control['id'], $excludedFormControls) ){
                    continue;
                }
            }

            $controlValue = '';

            foreach ($availableTypes as $type) {
                if ($control['value_' . $type] !== null) {
                    $controlValue = $control['value_' . $type];
                    break;
                }
            }

            /** @var AttributeCondition $attributeCondition */
            $attributeCondition = $this->em->getRepository('CaseBundle:AttributeCondition')
                ->findOneBy(['formControlId' => $control['id'], 'step' => $processInstance->getStep()]);

            $isValid = $attributeValidator->validate($control['attribute_path'], $controlValue, $groupProcessId, $attributeCondition);

            if($isValid instanceof ErrorValidatorBase) {

                $noValidControls[] = [
                    'desc' => $isValid->getMessage(),
                    'control' => $control['id'],
                    'attributeValueId' => $control['attribute_value_id']
                ];

                continue;

            }

            if (!empty($attributeCondition)) {
                $validationResult = $attributeValidator->validateRequired($controlValue, $control['id'], $processInstance->getId());
                if (!$validationResult) {

                    $noValidControls[] = [
                        'desc' => $this->translator->trans('Ta wartość jest wymagana'),
                        'control' => $control['id'],
                        'attributeValueId' => $control['attribute_value_id']
                    ];

                    continue;

                }

                if (!$attributeValidator->validateSemiRequired($controlValue, $control['id'], $processInstance->getId())) {
                    $semiRequireds[$control['id']] = $control['id'];
                }
            }
        }


        if (!empty($noValidControls)) {
            return [
                'error' => 1,
                'controls' => $noValidControls
            ];
        }

        if (!empty($semiRequireds)) {
            return [
                'error' => 2,
                'semirequireds' => array_values($semiRequireds),
                'desc' => $this->translator->trans('This value is semirequired')
            ];
        }

        return ['error' => 0];
    }

    /**
     * @param $userId
     * @param $originalUserId
     * @param $params ['omitValidation', 'formControl', 'stepId', 'value', 'path', 'valueId', 'valueType', 'groupProcessId', 'processInstanceId]
     * @return array
     */
    public function setAttributeValue($userId, $originalUserId, $params) {

        $this->setServices();

        $omitValidation = (isset($params['omitValidation']) && $params['omitValidation'] == "1") ? true : false;

        $isExcluded = false;
        $excludedFormControls = [];

        if($params['stepId']) {
            /** @var Step $step */
            $step = $this->em->getRepository('CaseBundle:Step')->find($params['stepId']);
            if($step) {
                $excludedFormControls = $step->getExcludedFormControls();
                $isExcluded = (!empty($excludedFormControls));
            }
        }
        else {
            $step = null;
        }

        if($omitValidation === true || ($isExcluded && in_array( $params['formControl'], $excludedFormControls))) {
            $validationResult = [];
        }
        else {
            $validationResult = $this->validateSetAttributeValue($params, $step);
        }

        $attributeValueId = null;
        $isPreview = false;

        if(isset($params['isPreview'])) {
            $isPreview = filter_var($params['isPreview'], FILTER_VALIDATE_BOOLEAN);
        }

        if (empty($validationResult) || (!empty($validationResult) && ($params['value'] === "" || $params['value'] === NULL))) {

            $stepId = ($isPreview) ? "xxx" : $params['stepId'];

            $attributeValueId = $this->processHandler->setAttributeValue(
                [
                    $params['path'] => (empty($params['valueId'])) ? NULL : $params['valueId']
                ],
                $params['value'],
                $params['valueType'],
                $stepId,
                $params['groupProcessId'],
                $userId,
                $originalUserId
            );

        }

        if (!empty($validationResult)) {
            return $validationResult;
        }

        return $attributeValueId[$params['path']];

    }

    /**
     * @param ProcessInstance $processInstance
     * @param $query
     * @return string|array
     */
    public function execProcedureFromFront(ProcessInstance $processInstance, $query) {

        if(empty($query['name'])) {
            return 'Procedure name is empty.';
        }

        if(strpos($query['name'], '.P_FRONT') === FALSE) {
            return 'Niepoprawna procedura.';
        }

        $this->setServices();

        if($this->processHandler->queryManager->existsProcedure($query['name'])) {

            $fetch = ( isset($query['fetch']) ? (filter_var($query['fetch'], FILTER_VALIDATE_BOOLEAN)) : true );
            $parameters = [];
            $queryParameters = '';

            if(isset($query['parameters']) && !empty($query['parameters']) && is_array($query['parameters'])) {

                $useParse = ( isset($query['parser']) ? (filter_var($query['parser'], FILTER_VALIDATE_BOOLEAN)) : true );

                if($useParse) {
                    $parser = $this->container->get('case.attribute_parser.service');
                    $parser->setProcessInstanceId($processInstance->getId());
                    $parser->setGroupProcessInstanceId($processInstance->getGroupProcess()->getId());
                }

                foreach ($query['parameters'] as $key => $parameter) {

                    $value = $parameter['value'];

                    if($useParse && !empty($value)) {
                        $value = $parser->parse($value);
                    }

                    if(in_array($parameter['type'], ['INT_OUT', 'STR_OUT']) && empty($value)) {
                        $value = null;
                    }

                    $newParameter = [
                        'key' => $parameter['key'],
                        'value' => $value,
                        'type' => QueryManager::getPDOParam($parameter['type'])
                    ];

                    if(array_key_exists('length', $parameter)) {
                        $newParameter['length'] = $parameter['length'];
                    }

                    $parameters[] = $newParameter;

                    $queryParameters .= '@'.$parameter['key'] . ' = :' . $parameter['key'];

                    if((count($query['parameters'])-1) != $key) {
                        $queryParameters .= ", ";
                    }

                }

            }

            $request = 'EXEC ' . $query['name'] . " " . $queryParameters;

            return $this->processHandler->queryManager->executeProcedure($request, $parameters, $fetch);


        }
        else {
            return 'Procedure do not exists.';
        }

    }

    /**
     * @param int $attributeId
     * @param int $parentAttributeId
     * @return array
     * @throws AtlasWebsocketException
     */
    public function addStructure(int $attributeId, int $parentAttributeId) {

        $newStructureOfAttribute = null;
        $this->setServices();

        try {

            $newStructureOfAttribute = $this->processHandler->attributeAddStructure(
                $parentAttributeId,
                $attributeId
            );

        }
        catch (\Throwable $exception) {

            $user = $this->container->get('user.info')->getUser();
            $this->logException($exception);
            throw new AtlasWebsocketException($exception, $user->isAdmin(), $this->translator->trans('fail_add_structure'));

        }

        return $newStructureOfAttribute;

    }

    /**
     * @param int $attributeValueId
     * @return bool
     * @throws AtlasWebsocketException
     */
    public function removeStructure(int $attributeValueId) {

        $this->setServices();

        $userInfo = $this->container->get('user.info');

        try {

            $this->processHandler->deleteAttributeValue($attributeValueId, $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());

        } catch (\Throwable $exception) {

            throw new AtlasWebsocketException($exception, $userInfo->getUser()->isAdmin(), $this->translator->trans('Fail remove attribute.'));

        }

        return true;

    }

    /**
     * @param int $attributeValueId
     * @param $move
     * @return bool
     * @throws AtlasWebsocketException
     */
    public function sortAttribute(int $attributeValueId, $move) {

        $this->setServices();

        $userInfo = $this->container->get('user.info');

        try {

            $this->processHandler->sortAttributeValue($attributeValueId, $move);

        } catch (\Throwable $exception) {

            $this->logException($exception);
            throw new AtlasWebsocketException($exception, $userInfo->getUser()->isAdmin(), $this->translator->trans('fail_sort_attribute'));

        }

        return true;

    }

    /**
     * @param string $locale
     * @param null $filter
     * @param null $currentTask
     * @param array $advancedFilters
     * @return array
     * @throws AtlasWebsocketException
     */
    public function refreshTasks($locale = 'pl', $filter = null, $currentTask = null, $advancedFilters = [])
    {

        $userInfo = $this->container->get('user.info');

        try {

            $startRequest = new \DateTime();

            if(isset($_SERVER['REQUEST_TIME'])) {
                $startRequest->setTimestamp($_SERVER['REQUEST_TIME']);
            }

            $processHandler = $this->container->get('case.process_handler');

            $start = microtime(true);

            $showInactive = (false === empty($advancedFilters["inactiveEnabled"])) ? $advancedFilters["inactiveEnabled"] : 0;

            $tasks = $processHandler->tasks(
                $userInfo->getUser()->getId(),
                $userInfo->getOriginalUser()->getId(),
                $filter,
                1,
                $locale,
                0,
                0,
                $showInactive,
                $advancedFilters??[]);

            $refreshTime = microtime(true) - $start;

            $reservedTasks = $this->container->get('snc_redis.default')->hKeys(TaskManagerTopic::RESERVED_TASKS);

            $emailTasks = [];

            foreach ($tasks as $key => $task) {

                if ($task['id'] !== $currentTask && in_array($task['id'], $reservedTasks)) {

                    unset($tasks[$key]);

                }
                elseif(!empty($task['taskGroup'] && strpos($task['stepId'], '1092') !== FALSE) ) {

                    /** Specjalnie, żeby zadania z e-mailami  trafiały do jednej grupy */
                    $tasks[$key]['stepId'] = '1092.001';

                    $emailTasks[] = $tasks[$key];
                    unset($tasks[$key]);

                }

            }

            /** Sortowanie, żeby zadania e-mailowe, były sortowane w innej kolejności */
            usort($emailTasks, function ($a, $b) {
                return intval($a['id']) > intval($b['id']) ? -1 : 1;
            });

            $tasks = array_merge($emailTasks, $tasks);

            try {

                $user = $userInfo->getUser();
                $userId = null;
                $roleId = null;

                if($user instanceof User) {
                    $userId = $user->getId();
                    if($user->getRoleGroup()) {
                        $roleId = $user->getRoleGroup()->getId();
                    }
                }

                $this->logRefreshTask([
                    'filterWord' => $filter,
                    'createdBy' => $userId,
                    'roleId' => $roleId,
                    'requestDateTime' => $startRequest->format('Y-m-d H:i:s'),
                    'requestTime' => (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']),
                    'refreshTime' => $refreshTime,
                    'advancedFilters' => json_encode($advancedFilters, true)
                ]);

            }
            catch (\Exception $exception) {

            }

        } catch (\Throwable $exception) {

            $this->logException($exception);

            throw new AtlasWebsocketException($exception, $userInfo->getUser()->isAdmin(), $this->container->get('translator.default')->trans('fail_refresh_tasks'));

        }

        return [
            'tasks' => $tasks
        ];

    }

    /**
     * @param string $locale
     * @param null $processInstanceId
     * @return array
     */
    public function renderUserTask($locale = 'pl', $processInstanceId = null)
    {

        $userInfo = $this->container->get('user.info');
        $userId = $userInfo->getUser()->getId();
        $processHandler = $this->container->get('case.process_handler');

        if($processInstanceId) {
            $processes = $processHandler->task($userId, $locale, $processInstanceId);
            if(!empty($processes)){
                return $processes[0];
            }
            return null;
        }

        $reservedTasks = $this->container->get('snc_redis.default')->hKeys(TaskManagerTopic::RESERVED_TASKS);
        $processes = $processHandler->task($userId, $locale);

        if(empty($processes)) {
           return null;
        }

        foreach ($processes as $process) {
            // Sprawdzenie czy znalezione zadanie jest zarezerwowane
            if(!in_array($process['id'], $reservedTasks)) {
                return $process;
            }
        }

        return null;

    }

    public function getOverviewService($groupProcessId) {

        try {

            $overview = $this->container->get('case.attribute_parser.service')->getOverview($groupProcessId, false);
            $serviceGroup = $this->container->get('case.process_handler')->getAttributeValue('168,839',$groupProcessId,'int');

        } catch (\Throwable $exception) {

            $this->logException($exception);

            /** @var User $user */
            $user = $this->getUser();

            throw new AtlasWebsocketException($exception, $user->isAdmin(), $this->container->get('translator.default')->trans('fail_get_overview'));

        }

        return [
            'id' => $groupProcessId,
            'overview' => $overview,
            'serviceGroup' => is_numeric($serviceGroup) ? $serviceGroup : null
        ];

    }

    public function getViewerInfoService($groupProcessId) {

        try {

            $overview = $this->container->get('case.attribute_parser.service')->getInfoForCaseViewer($groupProcessId, false);

        } catch (\Throwable $exception) {

            $this->logException($exception);

            /** @var User $user */
            $user = $this->getUser();

            throw new AtlasWebsocketException($exception, $user->isAdmin(), $this->container->get('translator.default')->trans('fail_get_overview'));

        }

        return [
            'id' => $groupProcessId,
            'overview' => $overview
        ];

    }


    public function getNotesAndAdditionalInfo(ProcessInstance $processInstance, $opts) {

        $this->setServices();
        $user = $this->getUser();

        $resolver = new OptionsResolver();
        $resolver->setDefaults(array(
            'displayNotes' => true,
            'displayInfo' => true
        ));

        $options = $resolver->resolve($opts);

        if($options['displayInfo']) {
            $additionalCaseInfo = $this->container->get('case.attribute_parser.service')->getAdditionalCaseInfo($processInstance);
            $caseInfo = $this->container->get('case.attribute_parser.service')->getCaseInfo($processInstance);
        }
        else {
            $additionalCaseInfo = null;
            $caseInfo = null;
        }

        $platformId = $this->container->get('case.process_handler')->getAttributeValue('253', $processInstance->getRoot()->getId(), 'int');

        $chatNoteContact = [];

        if($user instanceof User && $platformId) {

            $atlasViewerService = $this->container->get('app.atlas_viewer');
            $chatMessageIsEnabled = $atlasViewerService->isChatEnabled($platformId, $user->getId());

            if($chatMessageIsEnabled) {
                $chatNoteContact = $this->container->get('case.process_handler')->getContactsForChatNote($platformId, $processInstance->getRootId());
            }

        }

        $formControlValues = $this->processHandler->formControls($processInstance->getId());

        /** Pobranie notatek  */
        $notes = $options['displayNotes'] ? $this->generateNoteList($formControlValues, $processInstance) : [];

        return [
            'additionalCaseInfo' => $additionalCaseInfo,
            'caseInfo' => $caseInfo,
            'notes' => $notes,
            'chatNoteContact' => $chatNoteContact,
            'disableChatSelect' => (in_array($platformId, ['87', '88'], true))
        ];
    }

    public function getActiveStepsInServices(ProcessInstance $process, $services, $locale = 'pl') {

        $servicesWithActiveTasks = $this->container->get('case.service_handler')->activeTaskInServices($process->getGroupProcess()->getId(), $services, $locale);

        foreach ($servicesWithActiveTasks as $serviceId => $data) {
            $servicesWithActiveTasks[$serviceId]['html_content'] = $this->container->get('twig')->render('@Case/FormTemplate/templates/active-tasks-list-template.html.twig', [
                'active_tasks' => $data['tasks']
            ]);
        }

        return $servicesWithActiveTasks;

    }

    public function generateNoteList($formControlValues, ProcessInstance $processInstance) {


        $noteParents = [];

        $root = $this->processHandler->getAttributeValue(CaseFormGenerator::NOTES, $processInstance->getGroupProcess()->getId());

        foreach ($formControlValues as $key => $formControl) {
            $path = $formControl['attribute_path'];
            if (strpos($path, CaseFormGenerator::NOTE, 0) !== false) {
                $parentId = $formControl['parent_attribute_value_id'];
                $noteParents[$parentId][$formControl['attribute_path']] = $formControl;
            }
        }

        $notes = $this->container->get('note.handler')->prepareNoteList($noteParents, $processInstance->getRoot()->getId());

        uasort($notes,function($a,$b){
                $format ="Y-m-d H:i:s.u";
                $aTime = (date_create_from_format($format,$a['date']));
                $bTime = (date_create_from_format($format,$b['date']));
                return $aTime >$bTime;
        });

        return [
            'list' => $notes,
            'root' => $root ? $root[0]['id'] : NULL
        ];

    }

    /**
     * @param $params
     * @param $step
     * @return array
     */
    private function validateSetAttributeValue($params, $step)
    {

        // TODO - trzeba tu zrobić porządek z tymi  valid / require / semirequire

        if (empty($params['groupProcessId'] || empty($params['path']) || empty($params['stepId']))) {
            return ["error" => 1, "desc" => $this->translator->trans('No data required')];
        }

        if(!$step instanceof Step) {
            $step = $this->em->getRepository('CaseBundle:Step')->find($params['stepId']);
        }

        $isValid = [];
        $formControlCondition = null;

        if(!empty($params['formControl'])) {

            /**
             * Walidacja wartości - warunek brany z AttributeCondition
             *
             * @var AttributeCondition $attributeCondition
             */

            /** @var  $attributeCondition[] */
            $formControlCondition = $this->em->getRepository('CaseBundle:AttributeCondition')
                ->getCondition(intval($params['formControl']), $step);

            if(!empty($formControlCondition)) {

                /** @var AttributeCondition $formControlCondition */
                $formControlCondition = $formControlCondition[0];

                $isValid = $this->attributeValidator->validate($params['path'], $params['value'], $params['groupProcessId'], $formControlCondition);

            }
            else {
                $formControlCondition = null;
            }

//            $attributeCondition = $this->em->getRepository('CaseBundle:AttributeCondition')
//                ->findOneBy(['formControlId' => $params['formControl'], 'step' => $step]);

        }

        if($formControlCondition === null) {
            $isValid = $this->attributeValidator->validate($params['path'], $params['value'], $params['groupProcessId']);
        }

        if($isValid instanceof ErrorValidatorBase) {
//            Check if we override the error message by form-control
            if($formControlCondition){
                $locale = $this->container->get('request_stack')->getCurrentRequest()->getLocale();
                /** @var CustomValidationMessageTranslation $validationMessage */
                $validationMessage = $this->em->getRepository('CaseBundle:CustomValidationMessageTranslation')->findOneBy(
                    [
                        'translatable' => $formControlCondition->getId(),
                        'locale'=> $locale
                    ]
                );

                if($validationMessage && $validationMessage->getMessage() !== '')
                {
                    $cond = $validationMessage->getMessage();
                    $attributeParser = $this->container->get('case.attribute_parser.service');
                    $attributeParser->setGroupProcessInstanceId($params['groupProcessId']);
                    $cond = $attributeParser->parseString($cond);
                }
            }
            $errorDesc=  $cond ?? $isValid->getMessage();

            return ["error" => 1, "desc" => $errorDesc];
        }

        if (empty($params['formControl'])) {
            return [];
        }

        if(empty($formControlCondition)) {
            return [];
        }

        return $this->validateAttributeRequired($params);

    }

    /**
     * @param $params
     * @param null $formControlCondition
     * @return array
     */
    private function validateAttributeRequired($params)
    {

        $validationResult = $this->attributeValidator->validateRequired($params['value'], $params['formControl'], $params['processInstanceId']);

        if (!$validationResult) {
            return ["error" => 1, "desc" => $this->translator->trans('This value is required') . $params['value']];
        }

        return [];
    }

    /**
     * @param $exception
     */
    private function logException($exception) {

        $this->container->get('monolog.logger.dashboard_websocket')->log('ERROR', AtlasWebsocketException::ExceptionToJson($exception));

    }

    /**
     * @return mixed
     */
    private function getUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

    private function logGetTaskWatcher(Stopwatch $stopwatch, $process) {

        try {

            $event = $stopwatch->stop('getTask');

            if($this->container->getParameter('elastica_logging')) {

                $log = [
                    'name' => 'case_monitor_get',
                    'data' => [
                        'case_monitor_get.time.full' => $event->getDuration(),
                        'case_monitor_get.process.id' => $process['id'],
                        'case_monitor_get.process.root_id' => $process['rootId'],
                        'case_monitor_get.process.step_id' => $process['stepId'],
                        'case_monitor_get.user.id' => ($this->getUser() instanceof User) ? $this->getUser()->getId() : 1
                    ]
                ];

                $this->container->get('snc_redis.default')->rpush(
                    RedisListListenerService::ELASTICA_LOG,
                    [
                        json_encode($log)
                    ]
                );

            }

        }
        catch (\Exception $exception) {

            $this->logException($exception);

        }


//        $periods = $event->getPeriods();
//
//        $this->logWatcher('WATCHER_GET_TASK', [
//            'generateTask' => $periods[0]->getDuration(),
//            'additionalInfo' => $periods[1]->getDuration(),
//            'all' => $event->getDuration(),
//            'id' => $process['id'],
//            'stepId' => $process['stepId'],
//            'rootId' => $process['rootId']
//        ]);

    }

    private function logNextStepWatcher(Stopwatch $stopwatch, $process) {

        try {

            $event = $stopwatch->stop('nextStep');

            if($this->container->getParameter('elastica_logging')) {

                $log = [
                    'name' => 'case_monitor_next',
                    'data' => [
                        'case_monitor_next.time.full' => $event->getDuration(),
                        'case_monitor_next.process.id' => $process['id'],
                        'case_monitor_next.process.root_id' => $process['rootId'],
                        'case_monitor_next.process.step_id' => $process['stepId'],
                        'case_monitor_next.user.id' => ($this->getUser() instanceof User) ? $this->getUser()->getId() : 1
                    ]
                ];

                $this->container->get('snc_redis.default')->rpush(
                    RedisListListenerService::ELASTICA_LOG,
                    [
                        json_encode($log)
                    ]
                );

            }

        }
        catch (\Exception $exception) {

            $this->logException($exception);

        }

//        $periods = $event->getPeriods();
//
//        $this->logWatcher('WATCHER_NEXT_STEP', [
//            'prepareDate' => $periods[0]->getDuration(),
//            'runNextStep' => $periods[1]->getDuration(),
//            'cepik' => $periods[2]->getDuration(),
//            'all' => $event->getDuration(),
//            'id' => $process['id'],
//            'stepId' => $process['stepId'],
//            'rootId' => $process['rootId']
//        ]);

    }

    private function logWatcher($name, $logData) {

        $this->container->get('monolog.logger.stopwatch')->info($name, $logData);

    }

    public function logRefreshTask($data) {

        $query = 'INSERT INTO dbo.refresh_tasks_log (filterWord,createdBy,roleId,requestDateTime,requestTime,refreshTime,advancedFilters)
	        VALUES (:filterWord,:createdBy,:roleId,:requestDateTime,:requestTime,:refreshTime,:advancedFilters)';

        $parameters = [
            [
                'key' => 'filterWord',
                'value' => $data['filterWord'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'createdBy',
                'value' => $data['createdBy'],
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'roleId',
                'value' => $data['roleId'],
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'requestDateTime',
                'value' => $data['requestDateTime'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'requestTime',
                'value' => $data['requestTime'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'refreshTime',
                'value' => $data['refreshTime'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'advancedFilters',
                'value' => $data['advancedFilters'],
                'type' => PDO::PARAM_STR
            ]
        ];

        $this->container->get('app.query_manager')->executeProcedure($query, $parameters, false);


    }


    /**
     * @param ProcessInstance $processInstance
     * @return array
     */
    public function getControlsForExtraForm(ProcessInstance $processInstance) {

        $controls = [];

        $user = $this->getUser();
        $userId = ($user) ? $user->getId() : null;

        if(!$this->processHandler) {
            $this->processHandler = $this->container->get('case.process_handler');
        }

        $controlsIds = $this->processHandler->getControlsFormExtraForm($processInstance->getId(), $userId);

        foreach ($controlsIds as $control) {

            if(empty($control['attributeValueId'])) {
                continue;
            }

            /** @var AttributeValue $attributeValue */
            $attributeValue = $this->em->getRepository(AttributeValue::class)->find($control['attributeValueId']);

            /** @var Attribute $attribute */
            $attribute = $attributeValue->getAttribute();

            $formTemplateGenerator = $this->container->get('form_template.generator');
            $formTemplateGenerator->setFullMode();

            $attribute->setAttrValue('path', $attributeValue->getAttributePath());
            $attribute->setAttrValue('stepId', 'xxx');
            $attribute->setAttrValue('groupProcessId', $processInstance->getGroupProcess()->getId());
            $attribute->setAttrValue('id', $attributeValue->getId());
            $attribute->setAttrValue('value', $attributeValue->getValueByType($attribute->getValueType()));
            $attribute->setAttrValue('parentAttributeValueId', null);
            $attribute->setAttrValue('isAuto', false);

            $virtualFormControl = new FormControl();
            $virtualFormControl->setAttribute($attribute);
            $virtualFormControl->setAttributePath($attributeValue->getAttributePath());
            $virtualFormControl->groupProcessId = $processInstance->getGroupProcess()->getId();

            if(!empty($control['label'])) {
                $virtualFormControl->setVirtualLabel($control['label']);
            }

            $controls[] = $formTemplateGenerator->renderHtmlOfFormControl($virtualFormControl, false);
        }

        return $controls;

    }

}