<?php

namespace OperationalBundle\Service;

use Symfony\Component\DomCrawler\Crawler;
/**
 * Class PSAVinSearchService
 * @package OperationalBundle\Service
 */
class PSAVinSearchService
{

    const TYPE_CITROEN = 1;
    const TYPE_PEUGEOT = 2;

    private static $loginData = array(
        'citroen' =>
            [
                'login' => 'DD06377',
                'password' => 'dkpd5018'
            ],
        'peugeot' =>
            [
                'login' => 'DD06387',
                'password' => 'locr6833'
            ],
    );

    private $currentLogin;
    private $currentPassword;
    private $currentCookies;


    public function searchVinInMPSA($vin, $type) {

        $this->currentLogin = $type == self::TYPE_CITROEN ? self::$loginData['citroen']['login'] : self::$loginData['peugeot']['login'];
        $this->currentPassword = $type == self::TYPE_CITROEN ? self::$loginData['citroen']['password'] : self::$loginData['peugeot']['password'];

        $this->trySetSessionInMPSA($vin);

        return $this->getVinDetails($vin);

    }

    private function trySetSessionInMPSA($vin) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://servicebox.mpsa.com/panier/vehicule.do?vin=" . $vin,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => 1,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_VERBOSE => 1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: servicebox.mpsa.com",
                "Connection: keep-alive".
                "Pragma: no-cache",
                "Cache-Control: no-cache",
                "Upgrade-Insecure-Requests: 1",
                "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
                "Sec-Metadata: cause=forced, destination=document, site=cross-site",
                "Sec-Origin-Policy: 0",
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Accept-Encoding: gzip, deflate, br",
                "Accept-Language: pl-PL,pl;q=0.9,en-US;q=0.8,en;q=0.7\""
            ),
        ));

        curl_setopt($curl, CURLOPT_USERPWD, "{$this->currentLogin}:{$this->currentPassword}");

        $response = curl_exec($curl);

        curl_close($curl);

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response, $matches);

        $cookies = array();

        foreach($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }

        $this->currentCookies = $this->glueCookiesArray($cookies);

        preg_match_all("/(window.name=')([a-z0-9]*)(';\\n)/mi", $response, $matches);

        $windowName = $matches[2][0];

        $curl = curl_init();

        $oldLocation = 'https://servicebox.mpsa.com/pages/referer.jsp';
        $url = 'https://servicebox.mpsa.com/do/newApvprStart?referer=' . $oldLocation .'&window=' . $windowName;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_VERBOSE => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Accept-Encoding: gzip, deflate, br",
                "Accept-Language: pl-PL,pl;q=0.9,en-US;q=0.8,en;q=0.7",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Cookie: " . $this->currentCookies,
                "Pragma: no-cache",
                "Referer: https://servicebox.mpsa.com/pages/referer.jsp",
                "Sec-Metadata: cause=forced, destination=document, site=same-origin",
                "Sec-Origin-Policy: 0",
                "Upgrade-Insecure-Requests: 1",
                "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36"
            ),
        ));

        curl_exec($curl);
        curl_close($curl);

    }

    private function getVinDetails($vin) {

        $curl = curl_init();

        $url = 'https://servicebox.mpsa.com/panier/vehicule.do?vin=' . $vin;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Accept-Encoding: gzip, deflate, br",
                "Accept-Language: pl-PL,pl;q=0.9,en-US;q=0.8,en;q=0.7",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Cookie: " . $this->currentCookies,
                "Pragma: no-cache",
                "Sec-Metadata: cause=forced, destination=document, site=cross-site",
                "Sec-Origin-Policy: 0",
                "Upgrade-Insecure-Requests: 1",
                "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $crawler = new Crawler($response);


        $carTable = $crawler->filterXPath('//*[@id="tab1"]/table[1]');




        if($carTable->count())
        {
            $html = '<p>Pojazd</p><table class="bg-white table table-bordered"><tbody>' . $carTable->html() . '<tbody></table>';
        }else{
            $html = '<p>Pojazd</p><table class="bg-white table table-bordered"><tbody><tr><th style="color:red">Nie znaleziono pojazdu w bazie</th></tr></tbody></table>';

        }
        $guaranteeTable = $crawler->filter('#tab1 table + div[align="left"]');


        if($carTable->count() && $guaranteeTable->count())
        {

            $html.='<br /><span class="bold" style="border: 1px solid black; padding: 10px;display: block;"> '. str_replace('<img src="resources/5.1.19/images/picto/vert.gif">','',$guaranteeTable->text()).'</span>';


            if(strpos($guaranteeTable->text(),'Poza gwarancją')!== false)
            {
                $outOfGuarantee = $crawler->filter('#tab1 table.modifTh');
                if($outOfGuarantee->count()) {
                    $outOfGuaranteeHtml = str_replace('<img src="resources/5.1.19/images/picto/vert.gif">','',$outOfGuarantee->html());
                    $html .= '<table class="bg-white table table-bordered"><tbody>' . $outOfGuaranteeHtml . '<tbody></table>';
                }

            }

        }


        $guaranteeDateStart = $crawler->filter('div#tab1 div.borderBlock table.innerForm.large tr:first-of-type td:nth-of-type(2)');
       if($guaranteeDateStart->count()) {
            $html .= '<p>Data początku gwarancji</p><table class="bg-white table table-bordered"><tbody>' . $guaranteeDateStart->html() . '<tbody></table>';
        }


        $guaranteeDateEnd = $crawler->filter('div#tab1 div.borderBlock table.innerForm.large tr:nth-of-type(2) td:nth-of-type(2)');
        if($guaranteeDateEnd->count()) {
            $html .= '<p>Data końca gwarancji</p><table class="bg-white table table-bordered"><tbody>' . $guaranteeDateEnd->html() . '<tbody></table>';
        }
        $eventTable = $crawler->filterXPath('//*[@id="tab6"]/div[3]/table');

        if($eventTable->count()) {
            $html .= '<p>Incydenty na gwarancji</p><table class="bg-white table table-bordered"><tbody>' . $eventTable->html() . '<tbody></table>';
        }
        else {
            $html .= '<p>Incydenty na gwarancji</p><table class="bg-white table table-bordered"><tbody><tr><th>Brak incydentów do wyświetlenia</th></tr></tbody></table>';
        }
        $message = $crawler->filter('#tab1 p.message');
//
        if ($carTable->count() && $message->count())
        {
            $html .= '<p>Dodatkowa wiadomość</p><table class="bg-white table table-bordered"><tbody><tr><th style="font-weight: bold;" ">'.$message->html().'</th></tr></tbody></table>';


        }


        return $html;

    }







    private function glueCookiesArray($cookies) {

        $cookiesArray = [];

        foreach ($cookies as $name => $value) {

//            if(strpos($name, 'BIGipServerNEWAPVPRHUB') !== FALSE) {
//                continue;
//            }

            $value = str_replace(" ", "+", $value);
            $name = str_replace("_app", ".app", $name);

            $cookiesArray[] = $name . '=' . $value;
        }

        return implode(";", $cookiesArray);
    }

}