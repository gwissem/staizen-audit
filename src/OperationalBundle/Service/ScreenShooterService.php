<?php
namespace OperationalBundle\Service;

use ClassesWithParents\F;
use Doctrine\ORM\EntityManager;
use Jurosh\PDFMerge\PDFMerger;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ScreenShooterService {

    const SCREENS_DIR = '/data/screens/';
    private $em;
    private $container;

    private $screensDir;

    public function __construct(EntityManager $em, Container $container)
    {

        $this->em =$em;
        $this->container = $container;
        $this->screensDir = $this->container->getParameter('kernel.project_dir') . self::SCREENS_DIR;

    }

    /**
     * @param $processInstanceId
     * @param $name
     * @return string
     * @throws \Exception
     */
    public function generateStepPDF($processInstanceId, $name)
    {
        set_time_limit(60);

        if (!file_exists($this->screensDir . $name )) {
            mkdir($this->screensDir . $name , 0777, true);
        }

//        $processInstance = $this->em->getRepository('CaseBundle:ProcessInstance')->find($processInstanceId);
//
//        if (empty($processInstance)) {
//            throw new NotFoundHttpException();
//        }

        $fileName = $this->screensDir  . $name . '/' . $processInstanceId . '.pdf';

        if(file_exists($fileName)) {
            return $fileName;
        }

        $snappy = $this->container->get('knp_snappy.pdf');

//        $snappy->setOption('zoom', 0.7);
        $snappy->setTimeout(60);
//        $snappy->setOption('viewport-size', '1920x1080');
        $snappy->setOption('enable-javascript', true);
        $snappy->setOption('window-status', 'form-loaded');
        $snappy->setOption('debug-javascript', true);
        $snappy->setOption('no-stop-slow-scripts', true);
        $snappy->setOption('load-error-handling', 'ignore');

        // Wyciągniecie sesji użytkownika i ustawienie
        $sessionName = session_name();
        if($_COOKIE[$sessionName]) {
            $snappy->setOption('cookie', array($sessionName => $_COOKIE[$sessionName]));
        }

        $host = $this->container->getParameter('server_host');

        $url = $host . $this->container->get('router')->generate('public_operational_dashboard_print', [
                'processInstanceId' => $processInstanceId
            ]) . '?slim-mode';

        $snappy->generate($url, $fileName);

        return $fileName;
    }

    /**
     * @param $name
     * @return bool
     * @throws \Exception
     */
    public function generateCombinedPDF($name){

        $merger = new PDFMerger();
        $fullDir = $this->screensDir . $name . '/';

        if(!file_exists($fullDir)) {
            return false;
        }

        $files =  scandir($fullDir);

        foreach ($files as $file) {
            if ($file == '.' || $file == '..') {
                continue;
            }
            $merger->addPDF($fullDir . $file);
        }

        $merger->merge('file',$this->screensDir . $name . '/merged.pdf');

        $finder = new Finder();
        $finder->in($this->screensDir . $name)->exclude('merged');

        $fileSystem = new Filesystem();

        foreach ($finder as $file) {
            if($file->getBasename() !== "merged.pdf") {
                $fileSystem->remove($file->getRealPath());
            }
        }

        return true;

    }

    public function getPathOfPDF($name) {

        $fileName = $this->screensDir  . $name . '/merged.pdf';

        if(!file_exists($fileName)) {
            return new NotFoundHttpException('PDF do not exists!');
        }

        return $fileName;
    }

}