<?php

namespace OperationalBundle\Service;

use CaseBundle\Service\AttributeParserService;
use Doctrine\ORM\EntityManagerInterface;
use ToolBundle\Utils\ToolQueryManager;

class UpdaterOfFormControlService
{

    private $em;
    private $toolQueryManager;
    private $attributeParserService;

    public function __construct(EntityManagerInterface $em, ToolQueryManager $toolQueryManager, AttributeParserService $attributeParserService)
    {
        $this->em = $em;
        $this->toolQueryManager = $toolQueryManager;
        $this->attributeParserService = $attributeParserService;
    }

//    public function getValue(Attribute $attribute, $groupProcessId) {
//
//        if($attribute && $groupProcessId) {
//
//        }
//
//        return null;
//
//    }

    /** Wyciąganie options dla select'a
     *
     * @param $groupProcessId
     * @param $query
     * @param null $processInstanceId
     * @return array
     */

    public function getOptions($groupProcessId, $query, $processInstanceId = null, $attributePath = null) {

        $this->attributeParserService->setGroupProcessInstanceId($groupProcessId);
        $this->attributeParserService->setProcessInstanceId($processInstanceId);

        if($attributePath){
            $query = str_replace('{#path#}',$attributePath,$query);
        }

        $values = $this->prepareOptionsOfSelect($query);

        return $values;
    }

    private function prepareOptionsOfSelect($query) {

        if (empty($query)) return [];

        $choices = [];

        if (strpos($query, '|') === false && $this->toolQueryManager) {

            if(!empty($this->attributeParserService->getGroupProcessInstanceId())) {
                $query = $this->attributeParserService->parseString($query,false,false,null,false,true);
            }

            try {
                $results = $this->toolQueryManager->executeCustomQuery($query, true, NULL, false);

                if ($results && is_array($results)) {
                    foreach ($results as $result) {
                        if (is_array($result)) {
                            $keys = array_keys($result);
                            $value = $keys[0];
                            if (count($result) > 1) {
                                $label = $keys[1];
                            } else {
                                $label = $value;
                            }

                            $choices[$result[$label]] = $result[$value];
                        }
                    }
                }
            } catch (\PDOException $e) {
                $choices = [];
            }


        } else {

            $options = explode('|', $query);
            foreach ($options as $option) {
                $choices[$option] = $option;
            }
        }

        return $choices;

    }

}