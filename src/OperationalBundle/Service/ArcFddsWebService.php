<?php

namespace OperationalBundle\Service;

use Symfony\Component\DomCrawler\Crawler;

class ArcFddsWebService
{

    const NAME = 'directaccessfdds';
    const PASS = 'pwddirectaccessfdds1';

    public function __construct()
    {

    }

    /**
     * @param $vin
     * @return string
     */
    public function verifyVinInArcFDDS($vin)
    {

        $locationUrl = $this->tryLoginAndGetLocation($vin);
        return $this->getResultOfVerification($locationUrl);

    }

    /**
     * Funkcja robi POST Request z danymi logowania.
     * W zwrotce jest adres URL na którym znajduje się wynik weryfikacji VIN (metoda getResultOfVerification)
     *
     * @param $vin
     * @return mixed
     */
    private function tryLoginAndGetLocation($vin) {

        $postFieldsArray = [
            '__EVENTTARGET' => 'LinkButton1',
            '__EVENTARGUMENT' => '',
            '__VIEWSTATE' => '/wEPDwUKLTY4MTgzOTk3Ng9kFgICAQ9kFgwCAQ8PFgIeB1Zpc2libGVoZGQCBQ8PZBYCHglvbmtleWRvd24FnwFpZiAoKGV2ZW50LndoaWNoICYmIGV2ZW50LndoaWNoID09IDEzKSB8fCAoZXZlbnQua2V5Q29kZSAmJiBldmVudC5rZXlDb2RlID09IDEzKSkge2RvY3VtZW50LkZvcm0xLmVsZW1lbnRzWydCdXR0b24yJ10uY2xpY2soKTtyZXR1cm4gZmFsc2U7fSBlbHNlIHJldHVybiB0cnVlOyBkAgcPD2QWAh8BBZ8BaWYgKChldmVudC53aGljaCAmJiBldmVudC53aGljaCA9PSAxMykgfHwgKGV2ZW50LmtleUNvZGUgJiYgZXZlbnQua2V5Q29kZSA9PSAxMykpIHtkb2N1bWVudC5Gb3JtMS5lbGVtZW50c1snQnV0dG9uMiddLmNsaWNrKCk7cmV0dXJuIGZhbHNlO30gZWxzZSByZXR1cm4gdHJ1ZTsgZAIJDw9kFgIeBG5hbWUFC0xpbmtCdXR0b24xZAILDw8WBB4LTmF2aWdhdGVVcmwFK21haWx0bzp2ZXJvbmlrYS5uZXVtYW5uQGFyY3RyYW5zaXN0YW5jZS5jb20fAGhkZAIRDxYCHgRUZXh0BQQyMDE4ZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAQUNY2hrUmVtZW1iZXJNZQqr0PGhoX0CfqlG3ft3zPXkR+w/',
            '__VIEWSTATEGENERATOR' => '1806D926',
            '__EVENTVALIDATION' => '/wEWBgLi2rufCgLEhISFCwK1qbSRCwLM9PumDwLC8qqaCgK7q7GGCNFUjZPvL9MMI40Vd5rICqIkXLpY',
            'txtName' => self::NAME,
            'txtPassword' => self::PASS
        ];

        $postFields = $this->glueAssocArray($postFieldsArray);

        $postFields = str_replace('/', "%2F", $postFields);
        $postFields = str_replace('+', "%2B", $postFields);

        $url = 'http://cuam.arctransistance.com/login/login.aspx?returnurl=http%3a%2f%2ffdds.arctransistance.com%2flogin.aspx%3fReturnUrl%3d%252fFrontEnd%252fSearchVin.aspx%253fVin%253d' . $vin . '%26Vin%3d' . $vin;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_HTTPHEADER => array(
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "Accept-Language: en-US,en;q=0.5",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/x-www-form-urlencoded",
                "Referer : " . $url,
//                "Referer: http://cuam.arctransistance.com/login/login.aspx?returnurl=http%3a%2f%2ffdds.arctransistance.com%2flogin.aspx%3fReturnUrl%3d%252ffrontend%252fQuickSearch.aspx",
                "Upgrade-Insecure-Requests: 1",
                "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0"
            ),
        ));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 1);

        $response = curl_exec($curl);

        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);

        $headers = $this->get_headers_from_curl_response($header);

        curl_close($curl);

        $locationOfTable = $headers['Location'];

        return $locationOfTable;

    }

    /**
     * Funkcja robi Request GET, żeby odebrać wynik weryfikacji
     *
     * @param $locationUrl
     * @return string
     */
    private function getResultOfVerification($locationUrl) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $locationUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTPHEADER => array(
//                'Cache-Control: private, no-cache="Set-Cookie"'
//            ),
        ));

        curl_setopt($curl, CURLOPT_COOKIEFILE, "");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 1);

        $response = curl_exec($curl);

        $crawler = new Crawler($response);
//        $table = $crawler->filter('.detailtable');
        $table = $crawler->filter('.pageadmin');

        curl_close($curl);

        return $table->html();

    }

    private function glueAssocArray($array, $char = '&') {

        $string = '';

        $i = 0;
        $n = count($array);

        foreach ($array as $key => $item) {
            $i++;
            $string .= $key . '=' . $item;
            if($i < $n) {
                $string .= $char;
            }
        }

        return $string;

    }

    private function get_headers_from_curl_response($header_text)
    {
        $headers = array();

        foreach (explode("\r\n", $header_text) as $i => $line) {

            if(empty($line) || strpos($line,':') === FALSE) continue;

            if ($i === 0)
                $headers['http_code'] = $line;
            else {

                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;

            }
        }

        return $headers;
    }


}