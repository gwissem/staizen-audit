<?php

namespace OperationalBundle\Service;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\AttributeConditionService;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Service\ProcessHandler;
use Doctrine\ORM\EntityManager;
use OperationalBundle\StepBuilderComponent\StepInfoInstance;
use OperationalBundle\StepBuilderComponent\VirtualStepInstance;
use Predis\Client;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig_Environment;

class DashboardStepBuilderService
{

    const WIDGET_DIVIDER_HORIZONTAL = "divider_horizontal";
    const WIDGET_DIVIDER_VERTICAL = "divider_vertical";
    const WIDGET_DIV = "div";
    const WIDGET_PARAGRAPH = "paragraph";
    const WIDGET_HEADER = "header";
    const WIDGET_IMAGE = "image";
    const WIDGET_EMBEDDED_TOOL = "embedded_tool";
    const WIDGET_IFRAME = "iframe";
    const WIDGET_BUTTON = "button";
    const WIDGET_MAP = "map";
    const WIDGET_SERVICES = "services";
    const WIDGET_PANEL_EDIT_ATTRIBUTE = "panel_edit_attribute";
    const WIDGET_RSA_COSTS_VERIFICATION = 'rsa_costs_verification';
    const WIDGET_EMAIL_PREVIEW = 'email_preview';
    const WIDGET_MATRIX = 'matrix';

    const MODE_NONE = '';
    const MODE_PREVIEW = 'mode_preview';
    const MODE_FULL = 'full';

    /** @var Client  */
    protected $redis;

    /** @var EntityManager  */
    protected $em;

    /** @var Twig_Environment  */
    protected $twig;

    /** @var  AttributeConditionService */
    protected $attributeConditionService;

    /** @var AttributeParserService  */
    protected $attributeParserService;

    public function __construct(
        Client $clientRedis,
        EntityManager $entityManager,
        Twig_Environment $twig,
        AttributeConditionService $attributeConditionService,
        AttributeParserService $attributeParserService
    )
    {
        $this->redis = $clientRedis;
        $this->em = $entityManager;
        $this->twig = $twig;
        $this->attributeParserService = $attributeConditionService;
        $this->attributeParserService = $attributeParserService;
    }

    public function createFormBuilder($options) {

        $resolver = new OptionsResolver();
        $resolver->setDefaults(
            [
                'processInstance' => null
            ]
        );
        $resolver->setRequired('processInstance');

        $opts = $resolver->resolve($options);

//        if($opts['processInstanceId']) {
//
//            if($opts['processInstanceId'] instanceof ProcessInstance) {
//                $processInstance = $opts['processInstanceId'];
//            }
//            else {
//                $processInstance = $this->em->getRepository('CaseBundle:ProcessInstance')->find(intval($opts['processInstanceId']));
//            }
//
//            $stepInfo = StepInfoInstance::createStepInfo($processInstance);
//
//        }
//        else {
//            $stepInfo = StepInfoInstance::createStepInfoByStep($opts['step']);
//        }

        return VirtualStepInstance::createVirtualStep($opts['processInstance'], $this->redis);

    }

    function buildVirtualStep(VirtualStepInstance $virtualStep){

//        if ($this->mode === null) {
//            $this->mode = $mode;
//            $this->twig->addGlobal('modeFormGenerator', $mode);
//        }
//
//        return $this->generateControlsOfFormTemplate($formTemplate);



//        $this->controlsArray = [];
//
//        /** @var FormControl $control */
//        foreach ($formTemplate->getControls() as $control) {
//
//            $children = [];
//            $childFormTemplate = null;
//            $this->setTempData($control);
//
//            if ($control->getAttribute() && $control->getAttribute()->hasFormTemplate()) {
//
//                $childFormTemplate = $control->getAttribute()->getFormTemplate();
//                $this->recursivePrepareControls($childFormTemplate->getControls(), $children);
//
//            }
//
//            $this->controlsArray[] = $this->prepareControl($control, $children, $childFormTemplate);
//        }
//
//        usort($this->controlsArray, function ($a, $b){ return ($a['y'] * 100 + $a['x']) > ($b['y'] * 100 + $b['x']); });
//
//        return $this->controlsArray;

    }

    function test() {
//        $this->formTemplateGenerator->setAdditionalData([
//            'groupProcessId' => $this->currentGroupProcessId,
//            'processInstanceId' => $caseId,
//            'formControlsAttribute' => $formControlValues,
//            'stepId' => $step->getId()
//        ]);
//
//        $formTemplateJSON = $this->formTemplateGenerator->generate($formTemplate, FormTemplateGenerator::MODE_FULL);

    }
}