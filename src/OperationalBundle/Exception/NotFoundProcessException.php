<?php

namespace OperationalBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Thrown when request timeout
 */
class NotFoundProcessException extends HttpException {

    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = 'Process don\'t exists.', \Exception $previous = null, $code = 0)
    {
        parent::__construct(404, $message, $previous, array(), $code);
    }

}