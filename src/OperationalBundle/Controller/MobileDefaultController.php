<?php

namespace OperationalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MobileDefaultController extends Controller
{

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->getDoctrine()->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);
    }

    /**
     * @Route("/m/{processToken}", name="public_operational_dashboard_mobile_index", options = { "expose" = true })
     * @param Request $request
     * @param $processToken
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mobilePublicIndexAction(Request $request, $processToken)
    {

        $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy(['token' => $processToken]);

        if(empty($processInstances)) {
            throw new NotFoundHttpException();
        }

        $hotJarScript = $this->getDoctrine()->getRepository('AppBundle:CustomContent')->findOneBy([
            'key' => 'hotjar_script'
        ]);

        return $this->render('OperationalBundle:PublicDashboard:index.html.twig',
            [
                'processToken' => $processToken,
                'is_mobile_case' => true,
                'hotjar_script' => ($hotJarScript) ? $hotJarScript->getContent() : null
            ]
        );
    }

}