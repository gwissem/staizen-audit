<?php

namespace OperationalBundle\Controller;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\ProcessInstance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class ApiUpdaterController extends Controller
{

    const TYPE_SELECT = 'select';
    const TYPE_TEXT_BLOCK = 'text_block';
    const TYPE_INPUT = 'input';
    /**
     * @Route("/updater/value-control/{controlId}/{type}/{groupProcessId}", name="operational_dashboard_updater_value_control", options = { "expose" = true } )
     * @param Request $request
     * @param $controlId
     * @param $groupProcessId
     * @param $type
     * @return JsonResponse
     */

    public function updateValueControlAction(Request $request, $controlId, $type, $groupProcessId = null)
    {
        $this->validType($type);

        $formControl = $this->getDoctrine()->getRepository('CaseBundle:FormControl')->find($controlId);
        $processInstanceId = $request->query->get('processInstanceId', null);

        if($processInstanceId and !$groupProcessId){
            /** @var ProcessInstance $processInstance */
            $processInstance = $this->getDoctrine()->getRepository(ProcessInstance::class)->find($processInstanceId);
            if($processInstance){
                $groupProcessId = $processInstance->getGroupProcess()->getId();
            }
        }
        if(!$formControl) {
            throw new NotFoundResourceException('Nie znaleziono pola o id: ' . $controlId);
        }

        $updaterOfFormControl = $this->get('updater.form.control');

        $values = "";

        switch ($type) {
            case self::TYPE_SELECT: {

                /** @var Attribute $attribute */
                $attribute = $formControl->getAttribute();

                if(!$attribute) throw new NotFoundResourceException('Kontrolka nie posiada atrybutu.');

                $values = [];

                if($attribute->getQuery()) {
                    $attributePath = $formControl->getAttributePath();
                    $values = $updaterOfFormControl->getOptions($groupProcessId, $attribute->getQuery(), null, $attributePath);
                    $temp = [];
                    foreach ($values as $key => $item) {
                        $key = $key !== NULL ? $key : '';
                        $item = $item !== NULL ? $item : '';

                        $temp[] = [
                            "id" => $item,
                            "text" => $key
                        ];
                    }
                    $values = $temp;
                }

                break;
            }
            case self::TYPE_TEXT_BLOCK: {

                $parser = $this->get('case.attribute_parser.service');
                $parser->setProcessInstanceId($processInstanceId);
                $parser->setGroupProcessInstanceId($groupProcessId);
                $label = ($formControl->getLabel()) ? $formControl->getLabel() : "";
                $values = $parser->parse($label);

                break;
            }
            case self::TYPE_INPUT: {

                /** @var Attribute $attribute */

                $values = $this->get('case.process_handler')->getAttributeValue(
                    $formControl->getAttributePath(),
                    $groupProcessId,
                    $formControl->getAttribute()->getNameValueType()
                );

                break;
            }
        }

        return new JsonResponse([
            'values' => $values
        ]);

    }

    private function validType($type) {

        if(!in_array($type, [
            self::TYPE_SELECT, self::TYPE_TEXT_BLOCK, self::TYPE_INPUT
        ])) {

            throw new ResourceNotFoundException('Nie obsługiwany typ pola: ' . $type . ".");

        }

    }
}
