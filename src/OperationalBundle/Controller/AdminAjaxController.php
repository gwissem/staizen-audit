<?php

namespace OperationalBundle\Controller;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\FormTemplate;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Service\FormTemplateGenerator;
use Doctrine\ORM\EntityManager;
use DOMDocument;
use Exception;
use MssqlBundle\PDO\PDO;
use OperationalBundle\Utils\Timeline\TimelineAttributeEvent;
use OperationalBundle\Utils\Timeline\TimelineEnterHistoryEvent;
use OperationalBundle\Utils\Timeline\TimelineEvent;
use OperationalBundle\Utils\Timeline\TimelineNoteEvent;
use OperationalBundle\Utils\Timeline\TimelineProcessFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

class AdminAjaxController extends Controller
{
    /**
     * @Route("/ajax/simulation-task", name="operation_dashboard_simulation_task", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function simulationTaskAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) throw new BadRequestHttpException('Only AJAX');

        $taskId = $request->request->get('task-id', null);

        /** @var ProcessDefinition $process */
        $process = $this->getDoctrine()->getRepository('CaseBundle:ProcessDefinition')->find($taskId);

        if (!$process) {
            return new JsonResponse([
                'msg' => 'Proces nie istnieje'
            ], 404);
        }

        /** @var Step $firstStep */
        $firstStep = $process->getSteps()->first();

        if (!$firstStep) {
            return new JsonResponse([
                'msg' => 'Proces nie posiada żadnego kroku.'
            ], 404);
        }

        $userInfo = $this->get('user.info');
        $processHandler = $this->get('case.process_handler');

        $processInstanceId = $processHandler->start($firstStep->getId(), NULL, $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());
        $processHandler->setAttributeValue('253', 11, 'int', 'xxx', $processInstanceId, $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());
        $nextProcessInstanceId = $processHandler->next($processInstanceId, 1, $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());

        $processHandler->pushNewTaskToUser($userInfo->getUser(), $nextProcessInstanceId);

        return new JsonResponse([
            'processInstanceId' => $nextProcessInstanceId,
            'msg' => 'Sprawa utworzona i wysłana do ' . $userInfo->getUser()->getName() . '.',
            'title' => 'Sprawa "' . $firstStep->getId() . '" utworzona'
        ]);
    }

    /**
     * @Route("/ajax/find-steps/{id}", name="operation_dashboard_get_steps", requirements={"id": "\d+"}, options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     * @ParamConverter("processDefinition", class="CaseBundle:ProcessDefinition")
     * @param $processDefinition
     * @return Response
     */
    public function getStepsAction($processDefinition)
    {
        /** @var ProcessDefinition $processDefinition */
        if (!$processDefinition) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono definicji procesu'));
        }

        $steps = $processDefinition->getStepsForSelect();

        return new JsonResponse(
            [
                'results' => $steps
            ]
        );
    }


    /**
     * @Route("/ajax/render-step/{id}", name="operation_dashboard_render_step", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     * @ParamConverter("step", class="CaseBundle:Step")
     * @param Step $step
     * @return Response
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderPreviewStepAction($step)
    {
        $prependHtml = false;

        if (!$step) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono kroku'));
        }

        $isQuiz = $step->getProcessDefinition()->getType() == ProcessDefinition::TYPE_QUIZ;

        if (!$step->hasFormTemplate() && !$isQuiz) {
            return new JsonResponse([
                'msg' => 'Krok (' . $step->getId() . ') nie posiada formularza'
            ], 404);
        }

        $formTemplate = $step->getFormTemplate();
        if ($isQuiz && !$formTemplate) {
            $formTemplate = new FormTemplate();
            $prependHtml = $this->get('form_template.generator')->generateQuizHtml($step);
        }

        $caseFormGenerator = $this->get('case.form.generator');
        $formTemplateJSON = $this->get('form_template.generator')->generate($formTemplate, FormTemplateGenerator::MODE_PREVIEW);

        $data = [
            'id' => $step->getId(),
            'html' => $caseFormGenerator->generateWrapperFormOfStep($step, ['active' => 1]),
            'controls' => $formTemplateJSON,
            'prependHtml' => $prependHtml
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/ajax/get-partners-list", name="operation_dashboard_get_partners_list", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_TESTER')")
     * @return Response
     */
    public function getPartnersListAction()
    {

        $processRepository = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance');

        $processHandler = $this->get('case.process_handler');

        $partners = [];

        /** @var ProcessInstance $partner */
        foreach ($processRepository->findAllProcessOfPartners() as $partner) {

            $partnerShortName = $processHandler->getAttributeValue('595,614', $partner->getGroupProcess()->getId(), AttributeValue::VALUE_STRING);

            if (empty($partnerShortName)) $partnerShortName = '';

            $partners[] = [
                'id' => $partner->getId(),
                'text' => ('Partner ' . $partnerShortName . ' (' . $partner->getId() . ')')
            ];

        }

        return new JsonResponse(
            [
                'partners' => $partners
            ]
        );
    }

    /**
     * @Route("/ajax/add-partner", name="operation_dashboard_add_partner", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_PARTNER_EDITOR')")
     * @return Response
     * @throws Exception
     */
    public function addNewPartnerAction()
    {

        $userInfo = $this->get('user.info');
        $processHandler = $this->get('case.process_handler');

        $processInstanceId = $processHandler->start('1091.001', NULL, $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());
        $nextProcessInstanceIds = $processHandler->next($processInstanceId, 1, $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());
        $processHandler->formControls($nextProcessInstanceIds[0]);
        $nextProcessInstanceIds = $processHandler->next($nextProcessInstanceIds[0], 1, $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());

//        $processHandler->pushNewTaskToUser($userInfo->getUser(), $nextProcessInstanceIds[0]);

        return new JsonResponse([
            'description' => $processHandler->getProcessInstanceDescription($nextProcessInstanceIds[0]),
            'processInstanceId' => $nextProcessInstanceIds[0],
            'msg' => '',
            'title' => 'Partner utworzony'
        ]);
    }

    /**
     * @Route("/ajax/active-process-instance/{processInstanceId}", name="operation_dashboard_active_process_instance", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     * @return Response
     * @throws Exception
     */
    public function activeProcessInstanceIdAction($processInstanceId)
    {

        /** @var ProcessInstance $process */
        $process = $this->getDoctrine()->getRepository(ProcessInstance::class)->find($processInstanceId);

        if($process) {
            $process->setActive(1);
            $this->getDoctrine()->getManager()->persist($process);
            $this->getDoctrine()->getManager()->flush();
        }

        return new JsonResponse([
            'status' => 1
        ]);
    }

    /**
     * @Route("/ajax/parse-xml", name="operation_dashboard_parse_xml", options={"expose":true})
     * @Method("GET")
     * @return Response
     */
    public function parseXMLAction()
    {

        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML('');
        $dom->formatOutput = TRUE;

        return new Response($dom->saveXml(), 200, [
            'Content-Type' => 'text/xml'
        ]);

    }

    /**
     * @Route("/timeline-case/table/{groupProcessId}", name="operation_dashboard_timeline_case_table", options={"expose":true})
     * @Method({"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param $groupProcessId
     * @return Response
     * @throws Exception
     */
    public function timelineOfCaseTableAction(Request $request, $groupProcessId)
    {

        $options = [
            "date_from" => $request->query->get('data_from', ''),
            "date_to" => $request->query->get('date_to', ''),
            "attributes" => $request->query->get('attributes', ''),
            "steps" => $request->query->get('steps', ''),
            "show_notes" => filter_var($request->query->get('show_notes', FALSE), FILTER_VALIDATE_BOOLEAN),
            "show_history_enter" => filter_var($request->query->get('show_history_enter', FALSE), FILTER_VALIDATE_BOOLEAN),
            "show_attributes" => filter_var($request->query->get('show_attributes', FALSE), FILTER_VALIDATE_BOOLEAN),
            "show_steps" => filter_var($request->query->get('show_steps', FALSE), FILTER_VALIDATE_BOOLEAN),
        ];

        return $this->render('@Web/Timeline/table.html.twig', [
            'events' => $this->getTimelineEvents($groupProcessId, $options)
        ]);

    }

    /**
     * @Route("/timeline-case/{groupProcessId}", name="operation_dashboard_timeline_case", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param $groupProcessId
     * @return Response
     * @throws Exception
     */
    public function timelineOfCaseAction($groupProcessId)
    {

        return $this->render('@Web/Timeline/index.html.twig', [
            'events' => $this->getTimelineEvents($groupProcessId),
            'groupProcessId' => $groupProcessId
        ]);

    }

    private function getDefaultsTimelineOptions()
    {

        return [
            'attributes' => '',
            'steps' => '',
            'date_from' => '',
            'date_to' => '',
            'show_attributes' => true,
            'show_steps' => true,
            'show_history_enter' => true,
            'show_notes' => true
        ];

    }

    /**
     * @param $groupProcessId
     * @param array $opts
     * @return array
     * @throws Exception
     */
    private function getTimelineEvents($groupProcessId, $opts = [])
    {

        $resolve = new OptionsResolver();
        $resolve->setDefaults($this->getDefaultsTimelineOptions());
        $options = $resolve->resolve($opts);

        /** @var EntityManager $em */
        $em = $this->get('doctrine.orm.entity_manager');

        $userRepo = $em->getRepository('UserBundle:User');
        $processRepo = $em->getRepository('CaseBundle:ProcessInstance');

        /** @var ProcessInstance $process */
        $process = $processRepo->find($groupProcessId);

        $events = [];

        if ($options['show_steps']) {

            $filters = [];

            if(!empty($options['steps'])) {
                $steps = explode(';', $options['steps']);
                $filters['step'] = $steps;
            }

            if ($groupProcessId == $process->getRoot()->getId()) {

                $filters['root'] = intval($groupProcessId);

            } else {

                $filters['groupProcess'] = intval($groupProcessId);

            }

            $processInstances = $processRepo->findBy($filters);

            foreach ($processInstances as $processInstance) {

                $events = array_merge($events, TimelineProcessFactory::createEvents($processInstance, true, false, false));

            }

        }

        if ($options['show_attributes']) {

            foreach ($this->getAttributes($groupProcessId, $options['attributes']) as $attribute) {

                $user = ($attribute['who']) ? $userRepo->find($attribute['who']) : NULL;

                $events[] = new TimelineAttributeEvent(
                    $attribute['name'],
                    $attribute['value'],
                    $attribute['updated'],
                    $user
                );

            }

        }

        if ($options['show_history_enter']) {

            foreach ($this->getEnterHistory($groupProcessId) as $item) {

                $user = ($item['user_id']) ? $userRepo->find($item['user_id']) : NULL;

                if ($item['date_leave']) {
                    $events[] = new TimelineEnterHistoryEvent(
                        TimelineEvent::EVENT_TYPE_LEAVE_OF_USER, $item['date_leave'], $item['step_id'], 'Wyjście z kroku', $user, $item['all_time'], $item['real_time']
                    );
                }

                $events[] = new TimelineEnterHistoryEvent(
                    TimelineEvent::EVENT_TYPE_ENTER_OF_USER, $item['date_enter'], $item['step_id'], 'Wejście na krok', $user
                );

            }

        }

        if ($options['show_notes']) {

            $info = $this->get('operational.interface')->getNotesAndAdditionalInfo($process, [
                'displayNotes' => true,
                'displayInfo' => false
            ]);

            $notes = $info['notes']['list'];

            foreach ($notes as $noteId => $note) {

                $events[] = new TimelineNoteEvent(
                    $note, $noteId
                );

            }

        }

        usort($events, array($this, "orderEvents"));

        return $events;

    }

    private function orderEvents(TimelineEvent $event1, TimelineEvent $event2)
    {

        if ($event1->getEventDate()->getTimestamp() == $event2->getEventDate()->getTimestamp()) {

            if (
                in_array($event1->getEventType(), [TimelineEvent::EVENT_TYPE_ENTER_OF_USER, TimelineEvent::EVENT_TYPE_LEAVE_OF_USER])
                && in_array($event2->getEventType(), [TimelineEvent::EVENT_TYPE_ENTER_OF_USER, TimelineEvent::EVENT_TYPE_LEAVE_OF_USER])
            ) {

                if ($event1->getEventType() == TimelineEvent::EVENT_TYPE_LEAVE_OF_USER) {
                    return -1;
                } else {
                    return 1;
                }

            } else {
                return $event1->getEventDate()->getTimestamp() - $event2->getEventDate()->getTimestamp();
            }

        } else {
            return $event1->getEventDate()->getTimestamp() - $event2->getEventDate()->getTimestamp();
        }

    }

    /**
     * @param $groupProcessId
     * @param string $attributesPath
     * @param int $onlyUpdated
     * @return array
     */
    private function getAttributes($groupProcessId, $attributesPath = '', $onlyUpdated = 0)
    {

        $qm = $this->get('app.query_manager');

        $parameters = [
            [
                'key' => 'groupProcessId',
                'value' => (int)$groupProcessId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'onlyUpdated',
                'value' => (int)$onlyUpdated,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'attributesPath',
                'value' => $attributesPath,
                'type' => PDO::PARAM_STR
            ],

        ];

        return $qm->executeProcedure(
            "EXEC [dbo].[P_timelineCase_getAttributes] 
            @attributesPath = :attributesPath,
            @groupProcessInstanceId = :groupProcessId,
            @onlyUpdated = :onlyUpdated",
            $parameters);

    }

    private function getEnterHistory($groupProcessId)
    {

        $qm = $this->get('app.query_manager');

        $parameters = [
            [
                'key' => 'groupProcessId',
                'value' => (int)$groupProcessId,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $qm->executeProcedure(
            "EXEC [dbo].[P_timelineCase_getEnteredOnStep] @groupProcessInstanceId = :groupProcessId",
            $parameters);

    }

}
