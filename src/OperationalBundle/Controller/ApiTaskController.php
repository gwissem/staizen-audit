<?php

namespace OperationalBundle\Controller;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\ProcessHandler;
use CaseBundle\Utils\ParserMethods;
use FOS\RestBundle\Exception\InvalidParameterException;
use OperationalBundle\Exception\NotFoundProcessException;
use PDO;
use SocketBundle\Exception\AtlasWebsocketException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Finder\Exception\OperationNotPermitedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use UserBundle\Entity\User;

/**
 * Class ApiTaskController
 * @package OperationalBundle\Controller
 */
class ApiTaskController extends Controller
{

    /**
     * @Route("/active/all.{_format}", defaults={"_format": "json"}, name="apiTaskActiveAll")
     * @param string $_format
     * @return JsonResponse|Response
     */

    public function getTaskAction($_format)
    {

        $apiTask = $this->get('api_task.service');

        if($_format == "string") {
            return new Response($apiTask->getAllActiveTasks('string'));
        }
        else {
            return new JsonResponse(
                [
                    'active_tasks' => $apiTask->getAllActiveTasks()
                ]
            );
        }

    }

    /**
     * @Route("/active/user/{id}.{_format}", defaults={"_format": "json"}, name="apiTaskActiveOfUser")
     * @param $id
     * @param string $_format
     * @return JsonResponse|Response
     */

    public function getActiveTasksOfUserAction($id, $_format)
    {

        $apiTask = $this->get('api_task.service');

        if($_format == "string") {
            return new Response($apiTask->getActiveTasksOfUser($id,'string'));
        }
        else {
            return new JsonResponse(
                [
                    'active_tasks' => $apiTask->getActiveTasksOfUser($id)
                ]
            );
        }

    }
}
