<?php

namespace OperationalBundle\Controller;

use AppBundle\Entity\Post;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use DocumentBundle\Listener\DocumentUploadListener;
use FOS\RestBundle\Controller\Annotations as Rest;
use OperationalBundle\Service\OperationDashboardInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use UserBundle\Entity\User;

class DefaultController extends Controller
{

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->getDoctrine()->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);
    }

    /**
     * @Route("/takescreenshot/{name}/{processInstanceId}", name="takeScreenshot",requirements={"processInstanceId" = "\d+"}, options = { "expose" = true } )
     * @param $processInstanceId
     * @param $name
     * @return Response
     * @throws \Exception
     */
    public function takeScreenshotAction($processInstanceId, $name)
    {
        $filename  = $this->get('screenshooter.service')->generateStepPDF($processInstanceId, $name);
            return new BinaryFileResponse($filename);
    }


    /**
     * @Route("/merge-screenshots/{name}", name="takeAllScreenshots", options = { "expose" = true } )
     * @param $name
     * @return Response
     * @throws \Exception
     */
    public function takeAllScreenshots( $name)
    {
        return $this->get('screenshooter.service')->generateCombinedPDF($name);
    }

    /**
     * @Route("/{page}", defaults={"page" = 1}, requirements={"page" = "\d+"}, name="operational_dashboard_index", options = { "expose" = true } )
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request, $page = 1)
    {

        $url = $this->generateUrl('index', [],UrlGeneratorInterface::ABSOLUTE_URL);

        $fromAtlas = (strpos($request->headers->get('referer'), $url) !== false);

        $disablePhoneModule = ($request->query->has('disable_phone_module')) ? '?disable_phone_module=1' : '';

        // Chyba da się to uprościć
        if(
            !$fromAtlas
            &&
            $request->headers->get('referer') != $url
            &&
            ($request->headers->get('referer') . '/') != $url
        ) {

            $url .= $disablePhoneModule . '#redirectTo=' . $request->getUri();
            return $this->redirect($url);

        }

        $specialSituations = $this->getSpecialSituationOptions();

        $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->getPostsByType($this->getUser());

        $perPage = 5;
        $pagination = $this->get('knp_paginator')->paginate($posts, $page, $perPage);

        $platforms = $this->getDoctrine()->getRepository('CaseBundle:Platform')->findBy(['active'=>true]);
        $categories = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy(['type' => 1]);

        return $this->render('OperationalBundle:Default:index.html.twig',
            [
                'specialSituations' => $specialSituations,
                'pagination'        => $pagination,
                'platforms'         =>  $platforms,
                'categories' => $categories
            ]);
    }

    /**
     * @Route("/public/{processToken}", name="public_operational_dashboard_index", options = { "expose" = true } )
     * @param Request $request
     * @param $processToken
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function publicIndexAction(Request $request, $processToken)
    {

        $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy(['token' => $processToken]);

        if(empty($processInstances)) {
            throw new NotFoundHttpException();
        }

        return $this->render('OperationalBundle:PublicDashboard:index.html.twig',
            [
                'processToken' => $processToken
            ]
        );
    }

    /**
     * @Route("/public/generate_pdf/{processInstanceId}", name="public_operational_dashboard_generate_pdf", options = { "expose" = true } )
     * @param Request $request
     * @param $processInstanceId
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param $processToken
     */
    public function publicPrint2Action(Request $request, $processInstanceId)
    {

        $pdfConverter = $this->container->get('knp_snappy.pdf');

        $directory = $this->container->getParameter('kernel.root_dir') . DocumentUploadListener::PATH_TEMP_FILES;

        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        $stepId = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')
            ->getStepOfProcessInstanceId(intval($processInstanceId));

        $name = uniqid();
        $fullPath = $directory . '/' . $name . '.pdf';

        if($stepId == "1021.003") {
//            $pdfConverter->setOption('page-width', 210);
//            $pdfConverter->setOption('page-height', 600);
        }

        $pdfConverter->setTimeout(60);
        $pdfConverter->setOption('zoom', 0.7);
        $pdfConverter->setOption('viewport-size', '1920x1080');
        $pdfConverter->setOption('enable-javascript', true);
        $pdfConverter->setOption('window-status', 'form-loaded');
        $pdfConverter->setOption('debug-javascript', true);

        $host = $this->container->getParameter('server_host');

        $url = $host . $this->container->get('router')->generate('public_operational_dashboard_print', [
                'processInstanceId' => $processInstanceId
            ]);

        $pdfConverter->generate($url, $fullPath, [], true);

        return new Response($fullPath);

    }

    /**
     * @Route("/public/print/{processInstanceId}", name="public_operational_dashboard_print", options = { "expose" = true } )
     * @param Request $request
     * @param $processInstanceId
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param $processToken
     */
    public function publicPrintAction(Request $request, $processInstanceId)
    {

        set_time_limit(60);
        $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find($processInstanceId);

        if(empty($processInstance)) {
            throw new NotFoundHttpException();
        }

        return $this->render('OperationalBundle:PublicDashboard:index.html.twig',
            [
                'processInstanceId' => $processInstanceId,
                'subToken' => ($processInstance->getToken()) ?: 'no-token',
                'printMode' => true
            ]
        );
    }

    // http://atlas.starter24.test/app_dev.php/operational-dashboard/partner?partner-id=931541&attribute-id=931542

    /**
     * @Route("/partner", name="operational_dashboard_partners_index", options = { "expose" = true } )
     * @Security("is_granted('ROLE_USER')")
     *
     */
    public function partnerAction(Request $request)
    {

        $partnerId = $request->query->get('partner-id', null);
        $attributeId = $request->query->get('attribute-id', null);

        /** @var AttributeValue $attributeValue */
        $attributeValue = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find(intval($partnerId));

        $partner = null;

        if($attributeValue) {

            $partner = [
                'partnerId' => $attributeValue->getId(),
                'stepId' => '1091.011',
                'groupProcessId' => $attributeValue->getGroupProcessInstance()->getId(),
                'rootId' => $attributeValue->getRootProcess()->getId()
            ];

            $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy([
                'groupProcess' => intval($partner['groupProcessId']),
                'step' => '1091.011'
            ]);

            if(!empty($processInstance)) {

                /** @var ProcessInstance $step */
                $processInstance = $processInstance[0];
                $partner['id'] = $processInstance->getId();

                $partner = \GuzzleHttp\json_encode($partner);
            }

        }

        return $this->render('@Operational/Default/partner-interface-index.html.twig',
            [
                'partner' => $partner,
                'attribute' => $attributeId
            ]
        );
    }


    /**
     * @Route("/historical-tasks", name="operation_dashboard_historical_task_get", options={"expose":true})
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return JsonResponse
     * @throws BadRequestHttpException
     */


    public function getHistoricalTasksAction(Request $request)
    {

        $user = $this->getUser();
        if(!$user){
            return [];
        }

        $userId = $user->getId();
        $history = $this->get('case.process_handler')->userHistory($userId);

        return new JsonResponse($history);

    }


    private function getSpecialSituationOptions() {

        $options = [];

        $attrSpecialSituation = $this->getDoctrine()->getRepository('CaseBundle:Attribute')->find(OperationDashboardInterface::SPECIAL_SITUATION);

        if($attrSpecialSituation) {
            $updater = $this->get('updater.form.control');
            $options = $updater->getOptions(null, $attrSpecialSituation->getQuery());
        }

        return json_encode(array_flip($options));
    }

    /**
     * @Route("/get-note-templates", name="operation_dashboard_note_templates", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return JsonResponse
     * @throws BadRequestHttpException
     */


    public function getNoteTemplatesAction(Request $request)
    {

        $repository = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary');

        $smsTemplates = [];
        $noteTemplates = [];

        $templates = $repository->findBy([
            'type' => 'messageTemplate'
        ]);

        foreach ($templates as $template) {

            if($template->getArgument1() == "sms") {
                $smsTemplates[] = [
                    'label' => '/' . $template->getText(),
                    'value' => $template->getDescription()
                ];
            }
            elseif($template->getArgument1() == "note") {
                $noteTemplates[] = [
                    'label' => '/' . $template->getText(),
                    'value' => $template->getDescription()
                ];
            }

        }

        return new JsonResponse([
            'sms_templates' => $smsTemplates,
            'note_templates' => $noteTemplates
        ]);
    }

    /**
     * @Route("/get-active-users-in-case", name="operation_dashboard_get_active_users_in_case", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return JsonResponse
     * @throws BadRequestHttpException
     */


    public function getActiveUsersInCaseAction(Request $request)
    {

        $rootId = $request->query->get('rootId', null);
        $apiTask = $this->container->get('api_task.service');

        $activeUsersInCase = $apiTask->getUsersInCase($rootId);
        $results = [];

        $now = (new \DateTime())->getTimestamp();

        foreach ($activeUsersInCase['tasks'] as $item) {

            $process = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($item['process_instance_id']));
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->find(intval($item['user_id']));

            $duration = $now - intval($item['timestamp']);

            $results[] = [
                'username' => $user->getName(),
                'stepId' => $process->getStep()->getId(),
                'stepName' => $process->getStep()->getName(),
                'duration' => $duration
            ];
        }

        $html = $this->renderView('@Operational/Default/partials/user-activity.html.twig',
            [
                'results' => $results,
                'processes' => $apiTask->getAllProcess($rootId)
            ]
        );

        return new JsonResponse([
            'html' => $html,
            'amount_user' => $activeUsersInCase['amount_users'],
        ]);
    }


    /**
     * @Route("/get-summary-status-service/{processInstanceId}", name="operation_dashboard_get_summary_status_service", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @param $processInstanceId
     * @return JsonResponse
     */


    public function getSummaryStatusServiceAction(Request $request, $processInstanceId)
    {

        $process = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($processInstanceId));

        $processHandler = $this->get('case.process_handler');
        $attributeParser = $this->get('case.attribute_parser.service');

        $services = $processHandler->getStatusServices($process->getId(), $request->getLocale());

        if(count($services) > 0) {

            foreach ($services as $key => $service) {
                $attributeParser->setGroupProcessInstanceId($service['group_process_id']);
                $services[$key]['status_label'] = $attributeParser->parseString($service['status_label']);
            }

        }

        $html = $this->renderView('@Case/FormTemplate/templates/services-status-summary.html.twig', [
            'services' => $services
        ]);

        return new JsonResponse([
            'html' => $html,
            'statuses' => $services
        ]);
    }

    /**
     * @Route("/get-extra-actions/{processInstanceId}", name="operation_dashboard_get_extra_actions", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $processInstanceId
     * @return JsonResponse
     */

    public function getExtraActionsAction(Request $request, $processInstanceId)
    {

        /** @var ProcessInstance $processInstance */
        $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($processInstanceId));

        $html = $this->renderView('@Case/FormTemplate/templates/forms-container-extra-actions.html.twig', [
            'actions' => $this->getEnabledExtraActions($processInstance)
        ]);

        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @param ProcessInstance $processInstance
     * @return array
     */
    private function getEnabledExtraActions(ProcessInstance $processInstance) {

        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'cost_services' => true,
            'status_services' => true,
            'create_damage_handling' => false,  // Stworzenie przyjęcia szkody
            'extra_form' => false , // Extra formularz,
            'api_call' => false,
            'medical_case' => false
        ]);

        $availableActions = $this->get('case.process_handler')->getAvailableExtraActions($processInstance->getId(), $this->getUser()->getId());

        return $resolver->resolve($availableActions);

    }

    /**
     * @Rest\Route("/get-case-vin-info/{rootId}", name="operation_dashboard_get_case_info_from_vin_db", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param $rootId
     * @return JsonResponse
     */

    public function getInfoFromVinDatabase($rootId)
    {
        $caseHandler = $this->get('case.handler');

        $vinInfo = $caseHandler->getInfoFromVinDatabase($rootId);


        $html = $this->renderView('@Case/FormTemplate/templates/case-vin-info.html.twig', [
            'infos' => $vinInfo
        ]);
        return new JsonResponse([
            'html' => $html,
        ]);
    }

    /**
     * @Rest\Route("/get-program-card-modal/{groupProcessInstanceId}/{rootId}", name="operational_dashboard_get_program_cars_modal", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param $groupProcessInstanceId
     * @param $rootId
     * @return JsonResponse
     */
    public function getProgramCardModal($groupProcessInstanceId, $rootId)
    {

        $processHandler = $this->get('case.process_handler');
        $platform = $processHandler->getAttributeValue('253',$rootId,'int');
        $programDefault = $processHandler->getAttributeValue('202', $groupProcessInstanceId,'string');

        $caseHandler = $this->get('case.handler');

        $allForPlatform = $caseHandler->getProgramFileByPlatformProgram($platform);


        $html = $this->renderView('@Case/FormTemplate/templates/case-program-files-modal.html.twig', [
            'files' => $allForPlatform,
            'selected' =>$programDefault
        ]);

        return new JsonResponse([
            'html' => $html,
        ]);

    }


    /**
     * @Route("/get-case-costs/{rootId}", name="operation_dashboard_get_case_costs", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param $rootId
     * @return JsonResponse
     */
    public function getCaseCostsAction($rootId)
    {

        $caseHandler = $this->get('case.handler');

        $costs = $caseHandler->getCaseCosts($rootId);


        $html = $this->renderView('@Case/FormTemplate/templates/case-costs.html.twig', [
            'costs' => $costs
        ]);

        return new JsonResponse([
            'html' => $html,
            'costs' => $costs
        ]);
    }

    /**
     * @Route("/get-medical-case-form/{rootId}", name="operation_dashboard_get_medical_case_form", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param $rootId
     * @return JsonResponse
     */
    public function getMedicalCaseForm($rootId)
    {

        $isMedicalCase = 0;
        $attributeId = null;
        $attribute = $this->getDoctrine()->getRepository("CaseBundle:AttributeValue")->findOneBy([
            'attributePath' => '1168',
            'rootProcess' => $rootId,
        ]);

        if (null !== $attribute) {
            $isMedicalCase = empty($attribute->getValueInt()) ? 0 : 1;
            $attributeId = $attribute->getId();
        }

        $html = $this->renderView('@Case/FormTemplate/templates/medical-case.html.twig', [
            'isMedicalCase' => $isMedicalCase,
            'rootId' => $rootId,
            'attributeId' => $attributeId
        ]);

        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @Route("/skip-auto/{rootId}", name="operation_dashboard_skip_auto", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param $rootId
     * @return JsonResponse
     */
    public function skipAutoAction($rootId)
    {

        $caseHandler = $this->get('case.handler');

        $instances = $caseHandler->autoSkip((int)str_replace('A','',$rootId));

        return $this->render('@Operational/Default/skip-auto.html.twig',
            [
                'instances' => $instances
            ]
        );
    }

    /**
     * @Route("/simulate-rental-start/{groupProcessInstanceId}/{startDate}", name="operation_dashboard_simulate_rental_start", options={"expose":true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_USER')")
     * @param $groupProcessInstanceId
     * @param $startDate
     * @return JsonResponse
     */
    public function simulateRentalAction($groupProcessInstanceId, $startDate)
    {
        $caseHandler = $this->get('case.handler');

        $caseHandler->autoSimulateRental($groupProcessInstanceId, $startDate);

        return new Response('Done.');
    }


}