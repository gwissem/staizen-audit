<?php

namespace OperationalBundle\Controller;

use AppBundle\Entity\Log;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\PlatformPhone;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Form\Type\ApiCallType;
use CaseBundle\Service\ProcessHandler;
use CaseBundle\Utils\ParserMethods;
use FOS\RestBundle\Exception\InvalidParameterException;
use OperationalBundle\Exception\NotFoundProcessException;
use PDO;
use SocketBundle\Exception\AtlasWebsocketException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use UserBundle\Entity\User;

/**
 * Class OperationDashboardApi
 * @package OperationalBundle\Controller
 * @Route("/interface")
 */
class OperationDashboardApi extends Controller
{

    const REDIS_PREFIX_ID_DIALOG = 'jabber_id_dialog_';
    const CALL_PHONE_PROCESS_ID = '1012.002';

    /** @var ProcessHandler */
    private $processHandler = null;

    /** @var ParserMethods */
    private $parserMethods = null;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->getDoctrine()->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);
    }

    /**
     * @Route("/get-task/{processInstanceId}", name="operationalDashboardInterfaceGetTask", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $processInstanceId
     * @return JsonResponse
     */

    public function getTaskAction(Request $request, $processInstanceId)
    {

        $processInstance = $this->findOr404($processInstanceId);

        $response = $this->get('operational.interface')->getTask($processInstance, [
            'previewMode' => filter_var($request->query->get('previewMode', false), FILTER_VALIDATE_BOOLEAN)
        ]);

        return new JsonResponse($response);

    }

    /**
     * @Route("/get-notes-and-info/{processInstanceId}", name="operationalDashboardInterfaceGetNotesAndInfo", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE') or is_granted('ROLE_CASE_VIEWER') or is_granted('ROLE_CFM_CASE_VIEWER')")
     * @param Request $request
     * @param $processInstanceId
     * @return JsonResponse
     */

    public function getNotesAndAdditionalInfoAction(Request $request, $processInstanceId)
    {

        $processInstance = $this->findOr404($processInstanceId);

        $response = $this->get('operational.interface')->getNotesAndAdditionalInfo($processInstance, [
            'displayNotes' => filter_var($request->query->get('displayNotes', true), FILTER_VALIDATE_BOOLEAN),
            'displayInfo' => filter_var($request->query->get('displayInfo', true), FILTER_VALIDATE_BOOLEAN)
        ]);

        return new JsonResponse($response);
    }

    /**
     * @Route("/get-notes-view/{processInstanceId}", name="operationalDashboardInterfaceGetNotesView", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE') or is_granted('ROLE_CASE_VIEWER') or is_granted('ROLE_CFM_CASE_VIEWER')")
     * @param Request $request
     * @param $processInstanceId
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function getNotesView(Request $request, $processInstanceId)
    {
        $processInstance = $this->findOr404($processInstanceId);

        return $this->render('@Operational/Default/notes-view.html.twig', [
            'processInstance' => $processInstance
        ]);
    }

    /**
     * @Route("/get-task-by-hash/{token}/{id}", name="operationalDashboardInterfaceGetTaskByToken", options = { "expose" = true }, defaults={"id": null}, )
     * @param Request $request
     * @param string $token
     * @param $id
     * @return JsonResponse
     */

    public function getTaskByHashAction(Request $request, $token, $id)
    {

        $processInstance = $this->findOr404($token, true, $id);

        $this->setVirtualUser();

        $response = $this->get('operational.interface')->getTask($processInstance, [
            'displayNotes' => false,
            'processInfo' => true,
            'publicForm' => true,
            'process_token' => $token
        ]);

        return new JsonResponse($response);

    }

    /**
     * @Route("/get-overview/{processInstanceId}", name="operationalDashboardInterfaceGetOverview", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param $processInstanceId
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function getOverviewAction($processInstanceId)
    {

        $processInstance = $this->findOr404($processInstanceId);

        $response = $this->get('operational.interface')->getOverview($processInstance);

        return new JsonResponse($response);

    }

    /**
     * @Route("/get-right-widget/{processInstanceId}", name="operationalDashboardInterfaceGetRightWidget", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param $processInstanceId
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function getRightWidgetAction($processInstanceId)
    {

        $processInstance = $this->findOr404($processInstanceId);

        $parameters = [
            [
                'key' => 'groupProcessId',
                'value' => (int)$processInstance->getGroupProcess()->getId(),
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = "DECLARE @groupProcessId INT = :groupProcessId 
            DECLARE @widget NVARCHAR(255) = 'rsa_costs_verification'
            DECLARE @label NVARCHAR(255)
            
            select TOP 1 f.id FROM dbo.form_control f 
            INNER JOIN dbo.form_template ft ON ft.id = f.form_template
            INNER JOIN dbo.step_form_template sft ON sft.form_template_id = ft.id
            INNER JOIN dbo.process_instance p ON p.step_id = sft.step_id
            LEFT JOIN dbo.form_control_translation fct ON fct.translatable_id = f.id
            WHERE p.group_process_id = @groupProcessId AND f.widget = @widget AND fct.label = ISNULL(@label,fct.label)
            ";

        $queryManager = $this->get('app.query_manager');
        $result = $queryManager->executeProcedure($query, $parameters);

        $html = '';

        if(!empty($result)) {

            $control = $this->getDoctrine()->getRepository('CaseBundle:FormControl')->find(intval($result[0]['id']));

            $control->groupProcessId = $processInstance->getGroupProcess()->getId();

            $formTemplateGenerator = $this->get('form_template.generator');
            $formTemplateGenerator->setFullMode();

            $formTemplateGenerator->setAdditionalData([
                'groupProcessId' => $processInstance->getGroupProcess()->getId(),
                'processInstanceId' => $processInstance->getId(),
                'formControlsAttribute' => [],
                'stepId' => $processInstance->getStep()->getId()
            ]);

            $html = $formTemplateGenerator->renderHtmlOfWidget($control);

        }

        return new JsonResponse([
            'html' => $html
        ]);

    }


    /**
     * @Route("/get-dynamic-widget/{processInstanceId}/{name}", name="operationalDashboardInterfaceGetDynamicWidget", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $processInstanceId
     * @param $name
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function getDynamicWidgetAction(Request $request, $processInstanceId, $name)
    {

        $processInstance = $this->findOr404($processInstanceId);

        $widget = $this->get('operational.interface')->getDynamicWidget($processInstance, [
            'name' => $name,
            ],
            $request->getLocale());


        return new JsonResponse([
            'widget' => $widget
        ]);

    }


    /**
     * @Route("/get-time-line/{processInstanceId}", name="operationalDashboardInterfaceGetTimeLine", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $processInstanceId
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function getTimeLineAction(Request $request, $processInstanceId)
    {

        $processInstance = $this->findOr404($processInstanceId);

        $response = $this->get('operational.interface')->getTimeLine($processInstance, $request->getLocale());

        return new JsonResponse($response);

    }

    /**
     * @Route("/next-step", name="operationalDashboardInterfaceNextStep", options = { "expose" = true } )
     * @param Request $request
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function nextStepAction(Request $request)
    {

        $processInstance = $this->findOr404($request->query->get('id', null));

        $options = $request->query->all();

        if(($token = $request->headers->get('Process-Token'))) {

            if($processInstance->getToken() == $token) {
                $this->setVirtualUser();
                $options['process_token'] = $token;
            }
            else {
                throw new NotFoundResourceException('Not found active Token!');
            }

        }
        else {
            if(!$this->isGranted('ROLE_DASHBOARD_INTERFACE')) {
                throw new AccessDeniedHttpException();
            }
        }

        $response = $this->get('operational.interface')->nextStep($processInstance, $options, $request->getLocale());

        return new JsonResponse($response);

    }

    /**
     * @Route("/render-user-task", name="operationalDashboardInterfaceRenderUserTask", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @return JsonResponse
     */

    public function renderUserTaskAction(Request $request)
    {

        $data = $this->get('operational.interface')->renderUserTask($request->getLocale());
        $html = $this->get('twig')->render('OperationalBundle:Default:user-task.html.twig', [
            'instance' => $data
        ]);

        return new JsonResponse(
            [
                'data' => $data,
                'html' => $html
            ]
        );
    }

    /**
     * @Route("/refresh-view-user-task/{processInstanceId}", name="operationalDashboardInterfaceRefreshViewUserTask", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $processInstanceId
     * @return JsonResponse
     */

    public function refreshViewUserTaskAction(Request $request, $processInstanceId)
    {

        /** @var ProcessInstance|null $process */
        $process = $this->findOr404($processInstanceId);

        $data = $this->get('operational.interface')->renderUserTask($request->getLocale(), $process->getId());

        $html = $this->get('twig')->render('OperationalBundle:Default:user-task.html.twig', [
            'instance' => $data
        ]);

        return new JsonResponse(
            [
                'data' => $data,
                'html' => $html
            ]
        );
    }

    /**
     * @Route("/set-medical-data-note", name="operationalDashboardInterfaceSetMedicalDataNote", options = { "expose" = true } )
     * @Security("is_granted('ROLE_RODO_MEDICAL')")
     * @param Request $request
     * @return JsonResponse
     */

    public function setMedicalDataNote(Request $request)
    {
        $parentNoteId = $request->get('parentNoteId');
        $processHandler = $this->get('case.process_handler');
        $attributeValueRepo = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue');
        $userId = $this->getUser()->getId();

        $medicalDateAttribute = $attributeValueRepo->findOneBy([
               'parentAttributeValue' => $parentNoteId,
                'attributePath' => '406,226,1168'
            ]);


        $dataValue = 1;
        $attributeId = null;

        if (null ==! $medicalDateAttribute) {
            $value = $medicalDateAttribute->getValueInt();

            $dataValue = $value === 1 ? 0 : 1;

            $groupProcessId = $medicalDateAttribute->getGroupProcessInstance()->getId();
            $attributeId = $medicalDateAttribute->getId();
        } else {
            $parentNote = $attributeValueRepo->find($parentNoteId);

            $groupProcessId = $parentNote->getGroupProcessInstance()->getId();
        }

        $processHandler->editAttributeValueNew(['406,226,1168' => $attributeId], $dataValue, AttributeValue::VALUE_INT, Step::ALWAYS_ACTIVE, $groupProcessId, $parentNoteId, $userId);

        return new JsonResponse([
            'status' => 'success',
            'value' => $dataValue
        ]);
    }

    /**
     * @Route("/set-medical-data-case", name="operationalDashboardInterfaceSetMedicalDataCase", options = { "expose" = true } )
     * @Security("is_granted('ROLE_RODO_MEDICAL')")
     * @param Request $request
     * @return JsonResponse
     */
    public function setMedicalDataCase(Request $request)
    {
        $groupProcessId = $request->get('groupProcessId');
        $attributeValueId = $request->get('valueId');
        $value = $request->get('value');
        $userId = $this->getUser()->getId();

        $processHandler = $this->get('case.process_handler');

        $processHandler->editAttributeValueNew(['1168' => $attributeValueId], $value, AttributeValue::VALUE_INT, Step::ALWAYS_ACTIVE, $groupProcessId, null, $userId);


        $queryManager = $this->get('app.query_manager');
        $query = 'update dbo.attribute_value 
        set value_int = :valueInt
        where attribute_path = :attributePath 
        and root_process_instance_id = :rootProcessId';

        $params = [
            [
                'key' => 'valueInt',
                'value' => $value,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'attributePath',
                'value' => '406,226,1168',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'rootProcessId',
                'value' => $groupProcessId,
                'type' => PDO::PARAM_INT
            ],

        ];

        $queryManager->executeProcedure($query, $params, false);

        return new JsonResponse();
    }


    /**
     * @Route("/create-mobile-case", name="operationalDashboardInterfaceCreateMobileCase", options = { "expose" = true } )
     * @param Request $request
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function createMobileCaseAction(Request $request)
    {

        $phoneNumber = $request->get('phoneNumber');
        $platformId = $request->get('platformId');

        $processHandler = $this->get('case.process_handler');
        $em  = $this->getDoctrine()->getManager();

        /** @var PlatformPhone[] $phones */
        $phones = $em->getRepository('CaseBundle:PlatformPhone')->findBy([
            'platform' => $platformId,
        ], [
            'id' => 'ASC'
        ], 1);

        if(!empty($phones)) {

            $phone = $phones[0];
            $token = $processHandler->createCaseFromMobile($phoneNumber, $phone->getNumber());

            $url = $this->get('router')->getGenerator()->generate('public_operational_dashboard_mobile_index', [
                'processToken' => $token
            ], UrlGeneratorInterface::ABSOLUTE_URL);

            return new JsonResponse([
                "success" => true,
                "phoneNumber" => $phoneNumber,
                "platformId" => $platformId,
                "token" => $token,
                "url" => $url
            ]);

        }
        else {

            return new JsonResponse([
                "success" => false,
                "message" => "Numer infolini nie istnieje"
            ]);

        }

    }

    /**
     * @Route("/create-new-task", name="operationalDashboardInterfaceCreateNewTask", options = { "expose" = true } )
     * @param Request $request
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function createNewTaskAction(Request $request)
    {

        $params = $request->request->all();
        $idPlatform = $request->request->get('idPlatform', null);
        $platformNumber = $request->request->get('platformNumber', null);
        $numberPhone = $request->request->get('numberAddress', null);
        $stepId = $request->request->get('stepId', self::CALL_PHONE_PROCESS_ID);

        $redis = $this->get('snc_redis.default');
        $pusher = $this->get('gos_web_socket.zmq.pusher');
        $this->processHandler = $this->get('case.process_handler');
        $user = $this->getUser();

        $em  = $this->getDoctrine()->getManager();
        $idDialog = (isset($params['idDialog'])) ? $params['idDialog'] : null; // To by trzeba po prostu z Requesta Brać

        try {

            $log = new Log();
            $log->setCreatedBy($user);
            $log->setName('CREATE_NEW_TASK_BY_PHONE');
            $log->setContent(json_encode($params));
            $log->setParam1($idDialog);
            $log->setParam2($idPlatform);

            $processDescription = null;
            $processId = null;
            $processInstanceId = null;

            /** Sprawdzenie, czy przychodząca rozmowa, stworzyła już sprawę */
            $processInstanceFromRedis = ($idDialog) ? $redis->get(self::REDIS_PREFIX_ID_DIALOG . $idDialog) : null;

            if ($processInstanceFromRedis) {
                $processInstanceFromRedis = json_decode($processInstanceFromRedis, true);

                $processInstanceId = $processInstanceFromRedis['processInstanceId'];
                $processId = $processInstanceFromRedis['processId'];

            } else {

                //TODO - jeżeli idPlatformy === null, to błąd?

                $processId = $this->get('operational.interface')->createNewCase($idPlatform, $numberPhone, $stepId, $platformNumber);

            }

            /** Zapisanie w Redisie, że stworzono sprawę dla dzwoniącego numeru */

            if ($processInstanceFromRedis === null && $idDialog !== null) {

                $data = [
                    'processInstanceId' => $processInstanceId,
                    'processId' => $processId
                ];

                $redis->setex(self::REDIS_PREFIX_ID_DIALOG . $idDialog, 120, json_encode($data));

            }

            if ($processId) {
                $processDescription = $this->processHandler->getProcessInstanceDescription($processId);
            }

            if ($processId && $user instanceof User) {

                /**  SPRAWDZENIE UPRAWNIEN */
                $isAccess = $this->container->get('case.process_handler')->checkPermission($user->getId(), $processId);

                if($isAccess) {

                    $data = [
                        'users' => [$user->getId()],
                        'task' => $processDescription
                    ];

                    $pusher->push($data, 'atlas_dashboard_case');
                }

            }

            try {
                $log->setParam3($processId);
                $em->persist($log);
                $em->flush();
            }
            catch (\Exception $exception) {

            }

        }
        catch (\Throwable $exception) {

//            $this->get('monolog')->log('ERROR', AtlasWebsocketException::ExceptionToJson($exception));

            throw new AtlasWebsocketException($exception, $user->isAdmin(), $this->get('translator')->trans('fail_generate_task'));

        }

        return new JsonResponse([
            "numberAddress" => $numberPhone,
            "platformNumber" => $platformNumber,
            "idPlatform" => $idPlatform,
            "groupProcessId" => $processId,
            "processInstances" => $processId,
            'processDescription' => $processDescription
        ]);

    }

    /**
     * @Route("/refresh-tasks", name="operationalDashboardInterfaceRefreshTasks", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function refreshTasksAction(Request $request)
    {

        $filter = $request->query->get('filter', null);
        $currentTask = $request->query->get('currentTask', null);
        $advancedFilters = $request->query->get('options', null);

        $response = $this->get('operational.interface')->refreshTasks($request->getLocale(), $filter, $currentTask, $advancedFilters);

        return new JsonResponse($response);

    }

    /**
     * @Route("/attribute-value-histories", name="ODIattributeValueHistory", options = { "expose" = true } )
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function attributeValueHistoryAction(Request $request)
    {

        $attributeValueId = $request->query->get('attributeValueId', null);
        $processInstanceId = $request->query->get('processInstanceId', null);

        if($attributeValueId && $processInstanceId) {

            $attributeValue = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($attributeValueId);
            $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find($processInstanceId);

            if($attributeValue && $processInstance) {

                $response = $this->get('operational.interface')->attributeValueHistory($attributeValue, $processInstance);

                return new JsonResponse($response);

            }

        }

        return new JsonResponse([
            'id' => $attributeValueId,
            'histories' => false
        ]);
    }

    /**
     * @Route("/set-attribute-value", name="operationalDashboardInterfaceSetAttributeValue", options = { "expose" = true } )
     * @param Request $request
     * @Method("POST")
     * @return JsonResponse
     */

    public function setAttributeValueAction(Request $request)
    {

        /* $params = ['path', 'groupProcessId', 'stepId', 'valueId', 'value', 'valueType', 'formControl', 'omitValidation', 'isPreview'] */

        $params = $request->request->all();

        if(($token = $request->headers->get('Process-Token', null)) && !empty($params['groupProcessId'])) {

            if($this->checkIfTokenExistsInGroupProcess($token, $params['groupProcessId'])) {
                $this->setVirtualUser();
            }
            else {
                throw new NotFoundResourceException('Not found attribute!');
            }

        }
        else {
            if(!$this->isGranted('ROLE_DASHBOARD_INTERFACE')) {
                throw new AccessDeniedHttpException();
            }
        }

        $userInfo = $this->get('user.info')->getInfo();

        $response = $this->get('operational.interface')->setAttributeValue($userInfo['userId'], $userInfo['originalUserId'], $params);

        return new JsonResponse($response);

    }

    /**
     * @Route("/get-attribute-value", name="operationalDashboardInterfaceGetAttributeValue", options = { "expose" = true } )
     * @param Request $request
     * @Method("POST")
     * @return JsonResponse
     */

    public function getAttributeValueAction(Request $request)
    {

        /* $params = ['path', 'groupProcessId', 'stepId', 'valueType'] */

        $params = $request->request->all();

        if (empty($params['groupProcessId'] || empty($params['path']) || empty($params['stepId']))) {
            throw new \InvalidArgumentException('No data required');
        }

        if(($token = $request->headers->get('Process-Token', null)) && !empty($params['groupProcessId'])) {

            if($this->checkIfTokenExistsInGroupProcess($token, $params['groupProcessId'])) {
                $this->setVirtualUser();
            }
            else {
                throw new NotFoundResourceException('Not found attribute!');
            }

        }
        else {
            if(!$this->isGranted('ROLE_DASHBOARD_INTERFACE')) {
                throw new AccessDeniedHttpException();
            }
        }

        if (true === empty($params['parentAttributeId'])) {
            $params['parentAttributeId'] = null;
        }

        $response = $this->get('case.process_handler')->prepareDataForAttribute($params['path'], $params['valueType'], $params['groupProcessId'], $params['stepId'], $params['parentAttributeId']);

        if($response['value'] === null) {
            $response['value'] = "";
        }

        return new JsonResponse($response);

    }

    /**
     * @Route("/check-control-condition", name="operationalDashboardInterfaceCheckControlCondition", options = { "expose" = true } )
     * @param Request $request
     * @Method("POST")
     * @return JsonResponse
     */

    public function checkControlConditionAction(Request $request)
    {

        /* $params = ['typeCondition (visible/..)', 'formControlId', 'processInstanceId'] */

        $params = $request->request->all();

        if (empty($params['typeCondition'] || empty($params['formControlId']) || empty($params['processInstanceId']))) {
            throw new \InvalidArgumentException('No data required');
        }

        $condition = $this->container->get('case.attribute_condition')->checkCondition($params['typeCondition'], intval($params['formControlId']), intval($params['processInstanceId']));

        return new JsonResponse([
            'conditionResult' => $condition
        ]);

    }

    /**
     * @Route("/add-structure", name="operationalDashboardInterfaceAddStructure", options = { "expose" = true } )
     * @param Request $request
     * @Method("POST")
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function addStructureAction(Request $request)
    {

        $attributeId = $request->request->get('attributeId', null);
        $parentAttributeId = $request->request->get('parentValueId', null);

        if(empty($attributeId) || empty($parentAttributeId)) {
            throw new InvalidParameterException('Not found requires fields.');
        }

        if(($token = $request->headers->get('Process-Token', null))) {

            /** @var AttributeValue $attributeValue */
            $attributeValue = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($parentAttributeId);

            if(empty($attributeValue)) {
                throw new NotFoundResourceException('Not found attribute!');
            }

            if($this->checkIfTokenExistsInGroupProcess($token, $attributeValue->getGroupProcessInstance()->getId())) {
                $this->setVirtualUser();
            }
            else {
                throw new NotFoundResourceException('Not found active token!');
            }

        }
        else {
            if(!$this->isGranted('ROLE_DASHBOARD_INTERFACE')) {
                throw new AccessDeniedHttpException();
            }
        }

        $response = $this->get('operational.interface')->addStructure(intval($attributeId), intval($parentAttributeId));

        return new JsonResponse(
            [
                'structures' => $response
            ]
        );

    }

    /**
     * @Route("/remove-structure", name="operationalDashboardInterfaceRemoveStructure", options = { "expose" = true } )
     * @param Request $request
     * @Method("POST")
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function removeStructureAction(Request $request)
    {

        $attributeValueId = $request->request->get('attributeValueId', null);

        if(empty($attributeValueId)) {
            throw new InvalidParameterException('Not found requires fields.');
        }

        if(($token = $request->headers->get('Process-Token', null))) {

            /** @var AttributeValue $attributeValue */
            $attributeValue = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($attributeValueId);

            if(empty($attributeValue)) {
                throw new NotFoundResourceException('Not found attribute!');
            }

            if($this->checkIfTokenExistsInGroupProcess($token, $attributeValue->getGroupProcessInstance()->getId())) {
                $this->setVirtualUser();
            }
            else {
                throw new NotFoundResourceException('Not found active token!');
            }

        }
        else {
            if(!$this->isGranted('ROLE_DASHBOARD_INTERFACE')) {
                throw new AccessDeniedHttpException();
            }
        }

        $response = $this->get('operational.interface')->removeStructure(intval($attributeValueId));

        return new JsonResponse(
            [
                "status" => $response
            ]
        );

    }

    /**
     * @Route("/sort-attribute", name="operationalDashboardInterfaceSortAttribute", options = { "expose" = true } )
     * @param Request $request
     * @Method("POST")
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function sortAttributeAction(Request $request)
    {

        $attributeValueId = $request->request->get('attributeValueId', null);
        $move = $request->request->get('move', null);

        if(empty($attributeValueId) || empty($move)) {
            throw new InvalidParameterException('Not found requires fields.');
        }

        if(($token = $request->headers->get('Process-Token', null))) {

            /** @var AttributeValue $attributeValue */
            $attributeValue = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($attributeValueId);

            if(empty($attributeValue)) {
                throw new NotFoundResourceException('Not found attribute!');
            }

            if($this->checkIfTokenExistsInGroupProcess($token, $attributeValue->getGroupProcessInstance()->getId())) {
                $this->setVirtualUser();
            }
            else {
                throw new NotFoundResourceException('Not found active token!');
            }

        }
        else {
            if(!$this->isGranted('ROLE_DASHBOARD_INTERFACE')) {
                throw new AccessDeniedHttpException();
            }
        }

        $response = $this->get('operational.interface')->sortAttribute(intval($attributeValueId), $move);

        return new JsonResponse(
            [
                "status" => $response
            ]
        );

    }

    /**
     * @Route("/exec-procedure", name="operationalDashboardInterfaceExecProcedure", options = { "expose" = true } )
     * @param Request $request
     * @Method("POST")
     * @return JsonResponse
     * @throws AtlasWebsocketException
     */

    public function ExecProcedureAction(Request $request)
    {

        $processInstanceId = $request->get('processInstanceId', null);
        $query = $request->get('query', null);

        if(empty($processInstanceId ) || empty($query)) {

            return new JsonResponse(
                [
                    "message" => 'Brak wymaganych danych.'
                ], 404
            );

        }

        /** @var ProcessInstance $processInstance */
        $processInstance = $this->get('doctrine.orm.entity_manager')->getRepository(ProcessInstance::class)->find($processInstanceId);

        if(empty($processInstance)) {
            return new JsonResponse([
                'message' => 'Nie znaleziono procesu.'
            ], 404);
        }

        if(($token = $request->headers->get('Process-Token', null))) {


            if($this->checkIfTokenExistsInGroupProcess($token, $processInstance->getGroupProcessInstance()->getId())) {
                $this->setVirtualUser();
            }
            else {
                return new JsonResponse([
                    'message' => 'Brak uprawnień.'
                ], 403);
            }

        }
        else {
            if(!$this->isGranted('ROLE_DASHBOARD_INTERFACE')) {
                return new JsonResponse([
                    'message' => 'Brak uprawnień.'
                ], 403);
            }
        }

        $result = $this->get('operational.interface')->execProcedureFromFront($processInstance, $query);

        if(!is_array($result)) {
            $result = [
                'error' => $result
            ];
        }

        return new JsonResponse(
            $result
        );

    }


    /**
     * @Route("/actions-of-step", name="operationalDashboardInterfaceActiveAction", options = { "expose" = true } )
     * @param Request $request
     * @return JsonResponse
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @Method("POST")
     */

    public function actionsOfStepAction(Request $request)
    {

        $processHandler = $this->processHandler = $this->get('case.process_handler');
        $data = $request->request->all();
        $user = $this->getUser();

        try {

            $output = $processHandler->serviceActionRun($data['step_id'], $data['id'], $data['actionId'], $user->getId());

            if(!empty($output)) {
                $response = [
                    'error' => false,
                    'id' => $output['processInstanceId'],
                    'data' => $output,
                    'message' => $output['message']
                ];

            }
            else {
                $response = [
                    'error' => false,
                    'id' => null,
                    'data' => $output,
                    'message' => "Wykonano akcje."
                ];
            }


        }
        catch (\Exception $exception) {

            $response = [
                'error' => true,
                'message' => "Błąd wykonywania akcji."
            ];

        }

        return new JsonResponse($response);
    }


    /**
     * @Route("/active-steps-in-services", name="ODIActiveStepsInServices", options = { "expose" = true } )
     * @param Request $request
     * @return JsonResponse
     * @Method("POST")
     */

    public function getActiveStepsInServicesAction(Request $request)
    {

        $services = $request->get('services', []);
        $processId = $request->get('processId', null);

        if(empty($processId)) {
            return new JsonResponse(['active_tasks' => false]);
        }

        $process = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($processId));

        if(!$process) {
            return new JsonResponse(['active_tasks' => false]);
        }

        $activeTasks = $this->get('operational.interface')->getActiveStepsInServices($process, $services, $request->getLocale());

        return new JsonResponse($activeTasks);
    }

    /**
     * @Route("/extra-form-controls", name="ODIExtraFormControls", options = { "expose" = true } )
     * @param Request $request
     * @return JsonResponse
     * @Method("GET")
     */

    public function getControlsForExtraFormAction(Request $request)
    {

        $processInstanceId = $request->get('processInstanceId');
        $process = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($processInstanceId));

        $controls = $this->get('operational.interface')->getControlsForExtraForm($process);

        $formView = $this->renderView('@Operational/Forms/extra-form-view.html.twig', [
            'controls' => $controls,
            'processInstanceId' => $processInstanceId
        ]);

        return new JsonResponse([
            'form' => $formView
        ]);
    }

    /**
     * @Route("/api-call", name="ODIApiCall", options = { "expose" = true } )
     * @param Request $request
     * @return JsonResponse
     * @Method({"GET","POST"})
     * @Security("is_granted('ROLE_CONSULTANT_ADVANCED')")
     */

    public function getApiCallFormAction(Request $request)
    {

        $processInstanceId = $request->get('processInstanceId');
        $groups = [];

        $rootId = $process = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($processInstanceId))->getRootId();
        $output = $this->get('app.query_manager')->executeProcedure('select * from dbo.f_case_services(:rootId, 1)',[
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ],
        ]);
//
        foreach($output as $group){
            $groups[$group['name']]  = $group['group_process_id'];
        }

        $form = $this->createForm(ApiCallType::class, null, ['groups' => $groups]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $apiData = $form->getData();
            $this->container->get('api_call.manager')->sendCallForCustomJsonDefinition($apiData['groupProcessId'], $apiData['actions']->getId());
            return new JsonResponse($this->container->get('translator')->trans('api.call.sent'));
        }

        $formView = $this->renderView('@Case/FormTemplate/api-call-form.html.twig', [
            'processInstanceId' => $processInstanceId,
            'form' => $form->createView()
        ]);


        return new JsonResponse([
            'form' => $formView,
        ]);
    }

    /**
     * @Route("/overview-service/{processId}/{serviceId}", name="operationalDashboardInterfaceOverviewService", options = { "expose" = true } )
     * @param Request $request
     * @param $processId
     * @param $serviceId
     * @return JsonResponse
     * @Method("GET")
     * @throws AtlasWebsocketException
     */

    public function getOverviewServiceAction(Request $request, $processId, $serviceId)
    {

        $process = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find($processId);

        $extraDataService = $request->get('extraDataService', false);
        $extraDataService = filter_var($extraDataService, FILTER_VALIDATE_BOOLEAN);


        if(!$process) {
            return new JsonResponse(['status' => false]);
        }
        $activeServiceInfo = $this->get('case.process_handler')->getActiveServiceById($process->getRoot()->getId(), $serviceId);


        $groups = [];


        foreach ($activeServiceInfo as $activeService) {
            if(!in_array($activeService['group_process_id'], $groups)) {
                $groups[] = $activeService['group_process_id'];
            }
        }
        $processHandler = $this->container->get('case.process_handler');
        $root= $processHandler->getRootId($processId);
        $wasDC = filter_var($processHandler->getAttributeValue('802',$root,AttributeValue::VALUE_INT),FILTER_VALIDATE_BOOLEAN);


        if($serviceId == 12  && $wasDC){
            $groups[] = $processId;
        }


        if(!empty($groups)) {

            $response = [
                'services' => []
            ];

            /** @var ProcessHandler $processHandler */
            $this->processHandler = $this->get('case.process_handler');
            $this->parserMethods = $this->get('case.parser_methods');

            foreach ($groups as $group) {

                $newService = $this->get('operational.interface')->getOverviewService($group);


                if($extraDataService) {

                    if($serviceId == "1" || $serviceId == "2") {
                        $newService['overview'] .= $this->renderView('@Case/FormTemplate/templates/rsa-costs-verification.html.twig', [
                            'report' => $this->get('case.service_handler')->generateCostsVerificationReport($group, 'p_rsa_costs_report')
                        ]);
                    }

//                    else {
//                        $extraDataList = $this->get('case.service_handler')->getExtraDataService($group, $serviceId);
//
//                        if(!empty($extraDataList)) {
//
//                            if($extraDataList[0]['label'] !== "empty") {
//                                $html = $this->renderView('@Operational/Default/case-overview-extra-data.html.twig', [
//                                    'rows' => $extraDataList
//                                ]);
//
//                                $newService['overview'] .= $html;
//                            }
//
//                        }
//
//                    }

                }

                $newService['tabName'] = $this->getTabNameForServicePopup($group);
                $response['services'][] = $newService;
            }
        }
        else {
            $response = ['status' => false];
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/overview-service-case-viewer/{processId}/{serviceId}", name="operationalDashboardInterfaceOverviewForCaseViewerService", options = { "expose" = true } )
     * @param Request $request
     * @param $processId
     * @param $serviceId
     * @return JsonResponse
     * @Method("GET")
     * @throws AtlasWebsocketException
     */

    public function getOverviewForCaseViewerServiceAction(Request $request, $processId, $serviceId)
    {

        $process = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find($processId);

        if(!$process) {
            return new JsonResponse(['status' => false]);
        }

        $activeServiceInfo = $this->get('case.process_handler')->getActiveServiceById($process->getRoot()->getId(), $serviceId);

        $groups = [];

        foreach ($activeServiceInfo as $activeService) {
            if(!in_array($activeService['group_process_id'], $groups)) {
                $groups[] = $activeService['group_process_id'];
            }
        }

        if(!empty($groups)) {

            $response = [
                'services' => []
            ];

            /** @var ProcessHandler $processHandler */
            $this->processHandler = $this->get('case.process_handler');
            $this->parserMethods = $this->get('case.parser_methods');

            foreach ($groups as $group) {
                $newService = $this->get('operational.interface')->getViewerInfoService($group);
                $newService['tabName'] = $this->getTabNameForServicePopup($group);
                $response['services'][] = $newService;
            }

            $stepId = $process->getStep()->getId();

            if(strpos($stepId, '1007.') !== FALSE) {

                // TODO - trzeba by sprawdzenie zamienić na  $this->processHandler->canExtendRental

                $programId = $this->processHandler->getAttributeValue('202', $process->getRoot()->getId(), 'string');

                if(!empty($programId)) {

                    $program = $this->getDoctrine()->getRepository('CaseBundle:Program')->find(intval($programId));

                    $platformId = $program->getPlatform()->getId();

                    if(in_array(intval($platformId), [79, 80, 81, 82]) && strpos($program->getName(), 'CAM') === FALSE ) {

                        // Jeżeli jest to Citroen, Peugeot, DS, Motor homes to dodatkowo formularz na usłudze

                        $attr_985_986 = $this->processHandler->getAttributeValue('985,986', $process->getId(), 'int');
                        $attr_985_317 = $this->processHandler->getAttributeValue('985,317', $process->getId(), 'string');

                        $response['service_form'] = $this->renderView('@App/CaseViewer/CaseTools/attributes_form_of_service.html.twig', [
                            'groupProcessId' => $process->getId(),
                            'attr_985_986' => (!empty($attr_985_986)) ? $attr_985_986 : null,
                            'attr_985_317' => (!empty($attr_985_317)) ? $attr_985_317 : null
                        ]);

                    }

                }

            }

        }
        else {
            $response = ['status' => false];
        }

        return new JsonResponse($response);
    }

    private function getTabNameForServicePopup($groupProcessId) {

        $doctrine = $this->getDoctrine();

        $process = $doctrine->getRepository('CaseBundle:ProcessInstance')->find($groupProcessId);
        $stepId = $process->getStep()->getId();

        $definitionName = $doctrine->getRepository('CaseBundle:ProcessDefinition')->find($process->getDefinitionId())->getName();

        $attributeParser = $this->container->get('case.attribute_parser.service');
        $attributeParser->setGroupProcessInstanceId($groupProcessId);

        if (strpos($stepId, '1009.') !== false) {
            $fixingOrTowing = $this->processHandler->getAttributeValue('560',$groupProcessId, 'int');

            if($fixingOrTowing == 1) {
                $status = $doctrine->getRepository('CaseBundle:ServiceStatus')->findCurrent($groupProcessId,1);
                $parking = $this->processHandler->getAttributeValue('697',$groupProcessId, 'int');

                if($parking == 1) {
                    $definitionName = $this->container->get('translator')->trans('parking.towing');
                }
                else {
                    $definitionName = $this->container->get('translator')->trans('towing');
                }

            }
            else {
                $status = $doctrine->getRepository('CaseBundle:ServiceStatus')->findCurrent($groupProcessId,2);
                $definitionName = $this->container->get('translator')->trans('roadside.repair');
            }
        }
        else if (strpos($stepId, '1007.') !== false){
            $summary = $this->processHandler->getAttributeValue('168,159',$groupProcessId, 'int');
            $status = $doctrine->getRepository('CaseBundle:ServiceStatus')->findCurrent($groupProcessId,3);
            if($summary == 1){
                $definitionName = $this->container->get('translator')->trans('replacement_car.summary');
            }
        }
        else{
            $status = $doctrine->getRepository('CaseBundle:ServiceStatus')->findCurrent($groupProcessId);
        }

        $class = $status['progress'] == 5 ? 'font-red' : 'font-blue-chambray';
        $tabName = '<span class="semibold">'.$definitionName.' - '. $groupProcessId . '</span><br/><span class="small '.$class.'">('.$attributeParser->parseString($status['message']).')</span>';

        return $tabName;

    }

    /*
     * ------------------------------------------------
     *                      PRIVATE
     * ------------------------------------------------
     *
     */

    /**
     * @param $idOrToken
     * @param bool $byToken
     * @param null $id
     * @return ProcessInstance|null|object
     */
    private function findOr404($idOrToken, $byToken = false, $id = null) {

        if(!$byToken) {
            $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findFullProcessInstance($idOrToken);
        }
        else {
            if($id !== null) {
                $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findFullProcessInstance($id);
            }
            else {
                $processInstance = $this->getDoctrine()->getRepository(ProcessInstance::class)->findProcessByToken($idOrToken,1,0);

                if(!$processInstance) {
                    $processInstance = $this->getDoctrine()->getRepository(ProcessInstance::class)->findProcessByToken($idOrToken,0,0);
                }

            }
        }

        if (!$processInstance) {
            if($byToken) {
                throw new NotFoundProcessException($this->get('translator')->trans('public_dashboard.process_not_exists'));
            }
            else {
                throw new NotFoundProcessException();
            }

        }

        return $processInstance;
    }

    /**
     * @param $token
     * @param $groupProcessInstanceId
     * @return bool
     */
    private function checkIfTokenExistsInGroupProcess($token, $groupProcessInstanceId) {

        $processInstances = $this->getDoctrine()
            ->getRepository(ProcessInstance::class)
            ->findProcessByTokenAndGroupProcessId($token, $groupProcessInstanceId);

        return (count($processInstances) > 0);

    }

    private function setVirtualUser() {
        $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(['username' => 'virtual_partner']);
        if(!$user instanceof User) {
            throw new NotFoundResourceException('Not found Virtual Partner!');
        }
        $this->get('user.info')->setVirtualUser($user);
    }

}
