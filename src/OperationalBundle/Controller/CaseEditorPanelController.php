<?php

namespace OperationalBundle\Controller;

use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\ServiceEditor;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

class CaseEditorPanelController extends Controller
{
    /**
     * @Route("/case-editor", name="case_editor", options = { "expose" = true } )
     * @Security("is_granted('ROLE_CASE_MASTER_EDITOR')")
     *
     */
    public function indexAction(Request $request)
    {

        return $this->render('OperationalBundle:CaseEditor:index.html.twig',
            [
                'type' => 'caseEditor',
                'processes' => []
            ]);
    }


    /**
     * @Route("/gop-send/{groupProcessId}/{email}/{final}", name="gop_send", options = { "expose" = true } )
     * @Security("is_granted('ROLE_CASE_SERVICE_ACTIONS')")
     * @Method("POST")
     * @param $groupProcessId
     * @param $email
     * @param $final
     * @return Response
     */
    public function gopSendAction($groupProcessId, $email, $final)
    {
        $this->get('case.process_handler')->sendGop($groupProcessId, $final, $email);
        return new JsonResponse();
    }

    /**
     * @Route("/gop-preview/{groupProcessId}/{final}", name="gop_preview", options = { "expose" = true } )
     * @Security("is_granted('ROLE_CASE_SERVICE_ACTIONS')")
     * @param $groupProcessId
     * @param $final
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function gopPreviewAction($groupProcessId, $final)
    {
        $crossBorder = false;
        $processHandler = $this->get('case.process_handler');

        $eventLocation = $processHandler->getAttributeValue('101,85,86',$groupProcessId,'string');
        $homeOrg = $processHandler->getAttributeValue('206',$groupProcessId,'int');
        if(!empty($eventLocation) && $eventLocation !== 'Polska' && $homeOrg != 1){
            $crossBorder = true;
        }

        if($crossBorder){
            $userName = $this->getUser()->getName();
            $serviceId = $this->get('case.process_handler')->getServiceIdFromGroupId($groupProcessId);
            $html = $this->get('twig')->render('@Mailbox/EmailTemplate/assistance_organization_request.html.twig', [
                'serviceId' => $serviceId,
                'groupProcessInstanceId' => $groupProcessId,
                'userName' => $userName
            ]);

            $attributeParser = $this->get('case.attribute_parser.service');
            $attributeParser->setGroupProcessInstanceId($groupProcessId);
            $html = $attributeParser->parseString($html);

        }
        else{
            $html = $this->get('case.process_handler')->sendGop($groupProcessId, $final, null, true);
        }

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'gop-preview.pdf',
            'application/pdf',
            'inline'
        );
    }

    /**
     * @Route("/rental-automat-change-state/{groupProcessId}/{action}", name="rental_automat_change_state", options = { "expose" = true } )
     * @Security("is_granted('ROLE_CASE_SERVICE_ACTIONS')")
     * @param $groupProcessId
     * @param $action
     * @return Response
     */
    public function rentalAutomatChangeState($groupProcessId, $action)
    {

        $instanceRepo = $this->getDoctrine()->getRepository(ProcessInstance::class);
        $em = $this->getDoctrine()->getManager();
        $err = 0;

        $lastGroupInstance = $instanceRepo->findLastGroupInstance($groupProcessId)[0];
        if($lastGroupInstance->getStep()->getId() != '1007.053'){
            $err = 1;
        }
        else{
            if($action == 'stop'){
                $lastGroupInstance->setActive(0);
            }
            else if($action == 'start'){
                $lastGroupInstance->setActive(1);
            }
        }

        $em->persist($lastGroupInstance);
        $em->flush();

        return new JsonResponse([
           'err' => $err
        ]);
    }

    /**
     * @Route("/service-editor/{id}", name="service_editor", options = { "expose" = true } )
     * @Security("is_granted('ROLE_CASE_SERVICE_ACTIONS')")
     * @param $id
     * @return Response
     */
    public function serviceEditorAction($id)
    {
        $doctrine = $this->getDoctrine();
        $instanceRepo = $this->getDoctrine()->getRepository(ProcessInstance::class);
        $processHandler = $this->get('case.process_handler');
        $caseHandler = $this->get('case.handler');
        $editorSteps = [];
        $isGop = false;
        $rentalAutomat = -1;

        /** @var ProcessInstance $instance */
        $instance = $instanceRepo->find($id);
        $processDefinition = $instance->getStep()->getProcessDefinition();
        $groupProcess = $instance->getGroupProcess();

        $lastGroupInstance = $instanceRepo->findLastGroupInstance($groupProcess);

        if($lastGroupInstance[0]->getStep()->getId() == '1007.053'){
            $rentalAutomat = $lastGroupInstance[0]->getActive();
        }

        $caseNumber = $processHandler->getCaseId($id);
        $serviceName = $processDefinition->getName().' ('.$groupProcess->getId().')';
        $servicePanel = $doctrine->getRepository(ProcessInstance::class)->findBy(['root' => $instance->getRoot(), 'step' => '1011.010', 'active' => true],['id' => 'desc']);
        $step = $instance->getStep();
        $serviceEditorForStep = $doctrine->getRepository(ServiceEditor::class)->findBy(['step' => $step]);

        $servicePanelUrl = $this->generateUrl('operational_dashboard_index', [
            'action' => 'telephony-pick-up',
            'isforce' => 1,
            'process_instance' => $servicePanel[0]->getId()
        ]);

        $crossBorder = $caseHandler->isCrossBorder($groupProcess->getId());
        $email = $processHandler->getGopEmail($groupProcess->getId(), $processDefinition, $crossBorder);

        if(count($serviceEditorForStep) > 0){
            $serviceDefinition = $serviceEditorForStep[0]->getServiceDefinition();
            $editorSteps = $this->get('case.process_handler')->getEditorSteps($serviceDefinition, $groupProcess);


            if($crossBorder == 1){
                if(in_array($serviceDefinition->getId(),[1,2,3,4,5,17,21])){
                    $isGop = true;
                }
            }
            else{
                if(in_array($serviceDefinition->getId(),[3,4,5,7,17,21])){
                    $isGop = true;
                }
            }
        }

        return $this->render('OperationalBundle:CaseEditor:index.html.twig',
            [
                'id' => $id,
                'type' => 'serviceEditor',
                'isGop' => $isGop,
                'crossBorder' => $crossBorder,
                'processes' => $editorSteps,
                'gopEmail' => $email,
                'serviceName' => $serviceName,
                'caseNumber' => $caseNumber,
                'servicePanelUrl' => $servicePanelUrl,
                'groupProcessId' => $groupProcess->getId(),
                'rentalAutomat' => $rentalAutomat
            ]);
    }

    /**
     * @Route("/case-editor/search-processes", name="case_editor_search_processes", options = { "expose" = true } )
     * @Security("is_granted('ROLE_CASE_MASTER_EDITOR')")
     * @param Request $request
     * @return JsonResponse
     */
    public function searchProcessesAction(Request $request)
    {

        $queryManager = $this->get('app.query_manager');

        $data = $request->request->all();
        $rootId = intval(str_replace('A', '', $data['case-number']));

        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$rootId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => (string)$request->getLocale(),
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'label',
                'value' => (string)$data['search-word'],
                'type' => PDO::PARAM_STR
            ]
        ];


        $processes = $queryManager->executeProcedure(
            "EXEC dbo.p_searchForm @label = :label, @rootId = :rootId, @locale = :locale",
            $parameters
        );

        $listView = $this->renderView('@Operational/CaseEditor/process-list.html.twig', [
            'processes' => $processes
        ]);

        return new JsonResponse([
            'view' => $listView
        ]);

    }

}