<?php

namespace OperationalBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class PSAVinSearchController extends Controller
{


    /**
     * @Route("/searching-psa-vin/{type}/{vin}", name="searchingPSAVin", options = { "expose" = true } )
     * @param $vin
     * @param $type
     * @return Response
     */
    public function searchPSAVin($type, $vin)
    {

        try {

            $psaService = $this->get('operational.service.psa_vin_search_service');
            $result = $psaService->searchVinInMPSA($vin, $type);

        }
        catch (\Exception $exception) {


            $result = '<h4 class="font-red">Wystąpił problem podczas weryfikacji numeru VIN</h4>';

        }

        return new Response($result);

    }


}