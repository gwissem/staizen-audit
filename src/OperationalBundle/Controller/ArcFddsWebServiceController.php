<?php

namespace OperationalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArcFddsWebServiceController extends Controller
{

    /**
     * @Route("/search-arc-fdds/{vin}", name="searchArcFdds", options = { "expose" = true } )
     * @param $vin
     * @return Response
     */
    public function searchFddsAction($vin)
    {

        try {
            $arcFdds = $this->get('operational.service.arc_fdds_web_service');
            $result = $arcFdds->verifyVinInArcFDDS($vin);
        }
        catch (\Exception $exception) {
            $result = '<h4 class="font-red">Wystąpił problem podczas weryfikacji numeru VIN.</h4>';
        }

        // Fix po to, żeby nie waliło błędów, że grafiki nie znaleziono
        $result = str_replace('src="/design/images/ok.png"', 'src="" data-src-ok ', $result);
        $result = str_replace('src="/design/images/cancel.png"', 'src="" data-src-cancel ', $result);

        return $this->render('@Operational/ArcFdds/iframe.html.twig', [
            'result' => $result,
            'vin' => $vin,
            'serviceName' => 'http://fdds.arctransistance.com'
        ]);
    }

    /**
     * @Route("/open-arc-fdds/{vin}", name="openArcFdds", options = { "expose" = true } )
     * @param Request $request
     * @param $vin
     * @return Response
     */
    public function openFddsAction(Request $request, $vin)
    {

        $protocol = ($request->isSecure()) ? "https" : "http";

        $url = $protocol . '://cuam.arctransistance.com/login/login.aspx?returnurl='.$protocol.'%3a%2f%2ffdds.arctransistance.com%2flogin.aspx%3fReturnUrl%3d%252fFrontEnd%252fSearchVin.aspx%253fVin%253d' . $vin . '%26Vin%3d' . $vin;

        return $this->render('@Operational/ArcFdds/arc_fdds_form.html.twig', [
            'url' => $url
        ]);
    }

    /**
     * @Route("/open-psa-car-details/{type}/{vin}", name="openPsaCarDetails", options = { "expose" = true } )
     * @param Request $request
     * @param $vin
     * @return Response
     */
    public function testOpenFddsAction(Request $request, $vin, $type)
    {

        $url = $this->get('router')->generate('searchingPSAVin', [
            'vin' => $vin,
            'type' => $type
        ]);

        return $this->render('@Operational/ArcFdds/psa_vin_car.html.twig', [
            'url_' => $url
        ]);
    }

}