<?php

namespace OperationalBundle\Controller;

use PDO;
use SocketBundle\Topic\TaskManagerTopic;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class RoleManagerController extends Controller
{

    const FILTER_ROLE_GROUP = 3396;
    const PERMISSIONS_ROLE_GROUP = 3397;

    /**
     * @Route("/role-task-manager", name="role_task_manager", options = { "expose" = true } )
     * @Security("is_granted('ROLE_USER')")
     *
     */
    public function indexAction(Request $request)
    {

        $filterGroups = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy([
            'parent' => self::FILTER_ROLE_GROUP
        ]);

        if($this->isGranted('ROLE_TASK_MANAGER')) {
            $permissionGroup = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy([
                'parent' => self::PERMISSIONS_ROLE_GROUP
            ]);
        }
        else {
            $permissionGroup = [];
        }

        $allCategories = array_merge($filterGroups, $permissionGroup);
        $topData = [];

        if($this->isGranted('ROLE_TASK_MANAGER')) {
            $topData = $this->getTaskStatistic($filterGroups);
        }

        return $this->render('@Operational/RoleManager/index.html.twig',
            [
                'topData' => $topData,
                'filterGroups' => $filterGroups,
                'categories' => $allCategories
            ]
        );
    }

    /**
     * @Route("/role-task-manager/statistics", name="role_task_manager_statistics", options = { "expose" = true } )
     * @Security("is_granted('ROLE_TASK_MANAGER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTaskStatisticAction(Request $request)
    {

        $filterGroups = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy([
            'parent' => self::FILTER_ROLE_GROUP
        ]);

        $topData = $this->getTaskStatistic($filterGroups);

        $html = $this->renderView('@Operational/RoleManager/statistics-table.html.twig',
            [
                'topData' => $topData,
                'categories' => $filterGroups,
            ]
        );

        return new JsonResponse([
            'html' => $html
        ]);

    }

    /**
     * @Route("/role-task-manager/users", name="role_task_manager_users", options = { "expose" = true } )
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUsersAction(Request $request)
    {

        $onlyOnline = $request->query->get('online', 1);

        $filterGroups = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy([
            'parent' => self::FILTER_ROLE_GROUP
        ]);

        if($this->isGranted('ROLE_TASK_MANAGER')) {
            $permissionGroup = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy([
                'parent' => self::PERMISSIONS_ROLE_GROUP
            ]);
        }
        else {
            $permissionGroup = [];
        }

        $allCategories = array_merge($filterGroups, $permissionGroup);

        if($this->isGranted('ROLE_TASK_MANAGER')) {
            $users = $this->getDoctrine()->getRepository('UserBundle:User')->getAllUserInGroup(33, $onlyOnline);
        }
        else {
            $users = [
                $this->getDoctrine()->getRepository('UserBundle:User')->find($this->getUser()->getId())
            ];
        }

        $data = [];

        /** @var User $user */
        foreach ($users as $user) {

            $newRow = [
                'user_name' => $user->getName(),
                'user_id' => $user->getId(),
                'online' => $user->isOnline()
            ];

            /** @var Group $category */
            foreach ($allCategories as $category) {
                if($user->hasGroup($category->getId())) {
                    $newRow['group_' . $category->getId()] = 1;
                }
                else {
                    $newRow['group_' . $category->getId()] = 0;
                }
            }

            $data[] = $newRow;
        }

        return new JsonResponse($data);

    }

    public function getTaskStatistic($categories) {

        $topData = [];
        $categoriesResults = [];

        $queryManager = $this->get('app.query_manager');
        $redis = $this->get('snc_redis.default');

        $query = "EXEC dbo.p_tasks_online @user_group_id = :groupId";

        $stats = [
            [
                'name' => 'Ile zadań od początku dnia',
                'key' => 'countStepsToday'
            ],
            [
                'name' => 'Ile wszystkich zadań',
                'key' => 'allSteps'
            ],
            [
                'name' => 'Ile zadań na teraz',
                'key' => 'countStepsNow'
            ],
            [
                'name' => 'Ile zadań na teraz podjętych',
                'key' => 'activeTasks'
            ],
            [
                'name' => 'Ile zadań na teraz niepodjętych',
                'key' => '',
                'label' => 'activeTasksNotWork'
            ],
            [
                'name' => 'Ile osób obsluguje kolejkę',
                'key' => 'countActiveUsersPerGroup'
            ],
            [
                'name' => 'Średni czas realizacji zadania w kolejce [za ostatnią 1 h]',
                'key' => 'averageStepTime'
            ]
        ];

        /** @var Group $category */
        foreach ($categories as $category) {

            $parameters = [
                [
                    'key' => 'groupId',
                    'value' => (int)$category->getId(),
                    'type' => PDO::PARAM_INT
                ]
            ];

            $result = $queryManager->executeProcedure($query, $parameters);
            $categoriesResults[] = [
                'id' => (int)$category->getId(),
                'result' => $result[0]
            ];

        }

        $activeTasks = $redis->hvals(TaskManagerTopic::ENTER_ON_TASK);
        $activeTasksId = [];
        $processRepository = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance');
        $groupsIds = [];

        foreach ($categories as $category) {
            $groupsIds[] = $category->getId();
        }

        foreach ($activeTasks as $activeTask) {
            $data = explode('|', $activeTask);
            $activeTasksId[] = intval($data[0]);
        }

        $activeTaskInGroups = $processRepository->filterProcessWithPermissionOfGroup($activeTasksId, $groupsIds);

        foreach ($stats as $stat) {

            $newRow = [
                'name' => $stat['name'],
                'key' => $stat['key'],
                'label' => (isset($stat['label'])) ? $stat['label'] : '',
                'categories' => []
            ];

            foreach ($categoriesResults as $categoriesResult) {

                if($newRow['key'] == "activeTasks"){

                    $countFilteredGroups = count(array_filter($activeTaskInGroups, function($value, $key2) use($categoriesResult) {
                        return ($value['groupId'] == $categoriesResult['id']);
                    }, ARRAY_FILTER_USE_BOTH));

                    $newRow['categories'][] = $countFilteredGroups;

                }
                elseif($newRow['key'] && array_key_exists($newRow['key'], $categoriesResult['result']) ) {
                    $newRow['categories'][] = $categoriesResult['result'][$newRow['key']];
                }
                else {
                    $newRow['categories'][] = null;
                }
            }

            $topData[] = $newRow;
        }

        $countStepsNow = [];
        $activeTasksValue = [];

        foreach ($topData as $key => $datum) {

            if($datum['key'] == "countStepsNow") {
                foreach ($datum['categories'] as $key2 => $category) {
                    $countStepsNow[] = $category;
                }
            }
            elseif($datum['key'] == "activeTasks") {
                foreach ($datum['categories'] as $key2 => $category) {
                    $activeTasksValue[] = $category;
                }
            }
            elseif(isset($datum['label']) && $datum['label'] == "activeTasksNotWork") {
                foreach ($datum['categories'] as $key2 => $category) {
                    $topData[$key]['categories'][$key2] = $countStepsNow[$key2] - $activeTasksValue[$key2];
                }
            }

        }

        return $topData;

    }

    /**
     * @Route("/role-task-manager/change-role", name="role_task_manager_change_role", options = { "expose" = true } )
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return JsonResponse
     */
    public function changeRoleAction(Request $request)
    {

        $userId = $request->request->get('userId', null);
        $groupId = $request->request->get('catId', null);
        $isChecked = $request->request->get('isChecked', 0);

        $isChecked = filter_var($isChecked, FILTER_VALIDATE_BOOLEAN);

        if($userId && $groupId) {

            $user = $this->getDoctrine()->getRepository('UserBundle:User')->find(intval($userId));
            $group = $this->getDoctrine()->getRepository('UserBundle:Group')->find(intval($groupId));

            if($user && $group) {

                $currentUserId = $this->getUser()->getId();

                if(!$this->isGranted('ROLE_TASK_MANAGER') && $currentUserId != $userId ) {
                    return new JsonResponse([
                        'status' => false
                    ]);
                }

                if($isChecked) {
                    $user->addGroup($group);
                }
                else if($user->hasGroup($group)) {
                    $user->removeGroup($group);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->get('case.process_handler')->syncUserGroups($user->getId());

                return new JsonResponse([
                    'status' => true
                ]);

            }

        }

        return new JsonResponse([
            'status' => false
        ]);

    }

    /**
     * @Route("/role-task-manager/filter-steps/{groupId}", name="role_task_manager_filter_steps", options = { "expose" = true } )
     * @Security("is_granted('ROLE_TASK_MANAGER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getFilterStepsAction(Request $request, $groupId)
    {
        $steps = $this->get('case.process_handler')->getStepsForFilterGroup($groupId);
        return new JsonResponse($steps);
    }

}