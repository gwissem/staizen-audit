<?php
namespace ManagementBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScenarioCleanerCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:scenariobuilder:clean')
            ->setDescription('Check list of mailboxes');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln([
            '',
            '<comment>Cron is running...</comment>',
            '===================',
            '',
        ]);

        $cleaner = $this->getContainer()->get('atlas.scenarioinstance.cleaner.service');
        $result = $cleaner->clean();

//        $output->writeln([
//            '<info>Result</info>',
//            '==================='
//        ]);

//        $output->writeln([
//            '',
//            $result,
//            ''
//        ]);

        $output->writeln(sprintf("%s\n", "<info>End process.</info>"));
    }
}
