<?php

namespace ManagementBundle\Utils;


use CaseBundle\Entity\Platform;
use CaseBundle\Entity\PlatformGroup;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\Group;
use UserBundle\Entity\UserRoleGroupTask;

class GroupPlatformInstance
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var array
     */
    public $businessGroup;

    /**
     * @var integer
     */
    public $amountChecked = 0;

    /** @var  integer[] */
    private $platformsId;

    /**
     * @var bool
     */
    public $allChecked = true;

    public function __construct(PlatformGroup $platformGroup)
    {
        $this->id = $platformGroup->getId();
        $this->name = $platformGroup->getName();
        $this->platformsId = [];

        foreach ($platformGroup->getPlatforms() as $platform) {
            $this->platformsId[] = $platform->getId();
        }

        $this->businessGroup = [];
    }

    /**
     * @param Group[] $groups
     */
    public function initGroups($groups) {

        foreach ($groups as $group) {
            $this->businessGroup[$group->getId()] = [
                'group' => $group,
                'checked' => false,
                'groupPlatformId' => $this->id
            ];
        }

    }

    /**
     * @param ArrayCollection $userRoleGroupTasks
     */
    public function initData($userRoleGroupTasks) {

        foreach ($userRoleGroupTasks as $key => $userRoleGroupTask) {
            if(in_array($userRoleGroupTask->getPlatform()->getId(), $this->platformsId)) {
                if(isset($this->businessGroup[$userRoleGroupTask->getUserGroup()->getId()])) {
                    $this->businessGroup[$userRoleGroupTask->getUserGroup()->getId()]['checked'] = true;
                    $userRoleGroupTasks->remove($key);
//                    unset($userRoleGroupTasks[$key]);
                }
            }
        }

    }

    public function recalculateChecked() {

        foreach ($this->businessGroup as $item) {
            if($item['checked'] === false) {
                $this->allChecked = false;
            }
            else {
                $this->amountChecked++;
            }
        }

    }

}