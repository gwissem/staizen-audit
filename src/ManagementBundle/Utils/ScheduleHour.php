<?php

namespace ManagementBundle\Utils;

use DateTime;
use Exception;
use ManagementBundle\Entity\ScheduleTelephonyStatus;

class ScheduleHour
{

    public $label;

    /** @var DateTime */
    public $to;

    /** @var DateTime */
    public $from;

    /** @var ScheduleTelephonyStatus */
    public $scheduleTelephonyStatus;

    /** @var DateTime */
    private $day;

    /**
     * ScheduleHour constructor.
     * @param $label
     * @param null $scheduleTelephonyStatus
     * @param DateTime $day
     */
    public function __construct($label, $scheduleTelephonyStatus = null, $day = null)
    {
        $this->label = $label;
        $this->scheduleTelephonyStatus = $scheduleTelephonyStatus;
        $this->day = $day;

        if($this->day) {
            $this->day->setTime(0, 0);
            $this->setTime();

        }

    }

    public function isSchedule() {
        return ($this->scheduleTelephonyStatus !== null);
    }

    private function setTime()
    {
        list($f, $t) = explode("-", $this->label);

        $this->from = (clone $this->day)->setTime($f, 0);

        if($t === "0") {
            $this->to = (clone $this->day)->modify('+1 day');
        }
        else {
            $this->to = (clone $this->day)->setTime($t, 0);
        }

    }

    /**
     * @param DateTime $datetime
     * @return ScheduleHour
     */
    public static function createFromDate(Datetime $datetime) {

        $startHour = intval($datetime->format('H'));
        $endHour = $startHour + 1;

        if($startHour >= 0 && $startHour < 6) {
            $startHour = 0;
            $endHour = 6;
        }
        elseif($startHour >= 20) {
            $startHour = 20;
            $endHour = 0;
        }

        return new ScheduleHour($startHour . '-' . $endHour, null, $datetime);

    }

}