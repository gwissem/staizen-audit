<?php

namespace ManagementBundle\Utils;

use ManagementBundle\Controller\Supervisor\ScheduleTelephonyStatusController;
use ManagementBundle\Entity\ScheduleTelephonyStatus;
use UserBundle\Entity\User;

class ManagementScheduleHourRow
{

    /** @var User */
    public $user;

    /** @var ScheduleHour[] */
    public $items;

    public function __construct($user, $statuses = [])
    {
        $this->user = $user;
        $this->createItems($statuses);
    }

    /**
     * @param ScheduleTelephonyStatus[] $statuses
     */
    private function createItems($statuses) {

        $userStatuses = [];

        foreach ($statuses as $status) {
            $userStatuses[$status->getLabel()] = $status;
        }

        foreach (ScheduleTelephonyStatusController::_availableHours() as $labelHour) {

            if(isset($userStatuses[$labelHour])) {
                $this->items[$labelHour] = new ScheduleHour($labelHour, $userStatuses[$labelHour]);
            }
            else {
                $this->items[$labelHour] = new ScheduleHour($labelHour);
            }

        }

    }

}