<?php

namespace ManagementBundle\Utils;


use CaseBundle\Entity\PlatformGroup;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\UserRoleGroup;

class UserRoleGroupWrapper
{

    /**
     * @var UserRoleGroup
     */
    public $userRoleGroup;

    /** @var GroupPlatformInstance[] */
    public $platformGroups;

    public function __construct($userRoleGroup)
    {
        $this->userRoleGroup = $userRoleGroup;
        $this->platformGroups = [];
    }

    public function recalculateGroups() {

        foreach ($this->platformGroups as $platformGroup) {
            $platformGroup->recalculateChecked();
        }

    }

    /**
     * @param GroupPlatformInstance[] $platformGroups
     * @param ArrayCollection $userRoleGroupTasks
     */
    public function initData($platformGroups, $userRoleGroupTasks)
    {
        foreach ($platformGroups as $item) {
            $this->platformGroups[$item->id] = clone $item;
            $this->platformGroups[$item->id]->initData($userRoleGroupTasks);
        }
    }


}