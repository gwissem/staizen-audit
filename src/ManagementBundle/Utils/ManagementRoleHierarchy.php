<?php

namespace ManagementBundle\Utils;

use Symfony\Component\Security\Core\Role\RoleHierarchy;
use UserBundle\Entity\User;

/**
 * RoleHierarchy defines a role hierarchy.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ManagementRoleHierarchy extends RoleHierarchy
{
    /**
     * {@inheritdoc}
     */
    public function getReachableRoles(array $roles)
    {

        $reachableRoles = $roles;
        foreach ($roles as $role) {
            if (!isset($this->map[$role])) {
                continue;
            }

            foreach ($this->map[$role] as $r) {
                $reachableRoles[] = $r;
            }
        }

        return $reachableRoles;
    }

    public function allUserRoles(User $user) {
        return $this->getReachableRoles($user->getRoles());
    }

}
