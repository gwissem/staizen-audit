<?php

namespace ManagementBundle\Service;

use AppBundle\Utils\QueryManager;
use CaseBundle\Service\ServiceHandler;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use MailboxBundle\Service\MailAttachmentsService;
use ManagementBundle\Utils\ScheduleHour;
use MssqlBundle\PDO\PDO;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Router;
use Twig\Environment;
use UserBundle\Entity\User;

class ScheduleTelephonyStatusService
{

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * CaseToolsService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }


    public function hasTerm(User $user, $datetime = null)
    {

        if(!$datetime) {
            $datetime = new \DateTime();
        }
        else {
            $datetime = new \DateTime($datetime);
        }

        $scheduleHour = ScheduleHour::createFromDate($datetime);

        $status = $this->em->getRepository('ManagementBundle:ScheduleTelephonyStatus')->findFromScheduleHour($scheduleHour, $user);

        return (!empty($status));

    }

}