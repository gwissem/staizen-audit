<?php

namespace ManagementBundle\Service;

use CaseBundle\Entity\ProcessInstance;
use ManagementBundle\Entity\ScenarioInstance;
use ManagementBundle\Exception\ScenarioStopException;
use ManagementBundle\ScenarioUtils\ScenarioEventEmail;
use ManagementBundle\ScenarioUtils\ScenarioEventRing;
use ManagementBundle\ScenarioUtils\ScenarioEventRs;
use ManagementBundle\ScenarioUtils\ScenarioEventScenario;
use ManagementBundle\ScenarioUtils\ScenarioEventService;
use ManagementBundle\ScenarioUtils\ScenarioLoggerInterface;
use ManagementBundle\ScenarioUtils\ScenarioPoint;
use ManagementBundle\ScenarioUtils\ScenarioProgressLog;
use ManagementBundle\ScenarioUtils\ScenarioWrapper;
use OperationalBundle\Service\OperationDashboardInterface;
use OperationalBundle\Service\ScreenShooterService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ScenarioBuilderService
{

    const PROCESS_STOP = '-999';

    /** @var ScenarioWrapper */
    protected $currentScenario;

    /** @var ScenarioLoggerInterface */
    protected $logger;

    /** @var ScenarioPoint */
    protected $scenarioPoint;

    /** @var ScenarioHandlerService */
    protected $scenarioHandler;

    /** @var OperationDashboardInterface */
    protected $operationDashboardInterface;

    /** @var ScreenShooterService */
    protected $screenShooter;

    /** @var string */
    private $screenName;

    /** @var ScenarioProgressLog[] */
    public $catchErrors = [];

    /**
     * ApiTaskService constructor.
     * @param ScenarioHandlerService $scenarioHandler
     * @param OperationDashboardInterface $operationDashboard
     * @param ScreenShooterService $screenShooter
     */
    public function __construct(ScenarioHandlerService $scenarioHandler, OperationDashboardInterface $operationDashboard, ScreenShooterService $screenShooter)
    {

        $this->scenarioHandler = $scenarioHandler;
        $this->operationDashboardInterface = $operationDashboard;
        $this->screenShooter = $screenShooter;

        $this->scenarioHandler->em->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);

    }

    /**
     * @param ScenarioLoggerInterface $logger
     */
    public function setLogger(ScenarioLoggerInterface $logger) {
        $this->logger = $logger;
    }

    /**
     * @param ScenarioWrapper $scenario
     */
    public function setScenario(ScenarioWrapper $scenario) {
        $this->currentScenario = $scenario;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function startBuilder() {

        if(!$this->currentScenario) throw new NotFoundHttpException('Before runScenario, please set scenario.');

        set_time_limit(0);
        ini_set('memory_limit', '2G');
        ini_set('mssql.connect_timeout',-1);

        $time = microtime(true);

        $newScenarioInstance = new ScenarioInstance();

        $newScenarioInstance->setScenario($this->currentScenario->scenario);
        $newScenarioInstance->setUser($this->scenarioHandler->userInfo->getUser());
        $newScenarioInstance->setCreatedAt((new \DateTime()));
        $newScenarioInstance->setHasPDF($this->currentScenario->scenario->isPdf());
        $this->scenarioHandler->em->persist($newScenarioInstance);
        $this->scenarioHandler->em->flush();

        $this->screenName = 'scenario_' . $this->currentScenario->scenario->getId() . '_' . $newScenarioInstance->getId();

        $isSuccess = true;

        try {

            $this->runScenario($this->currentScenario);

            if($this->scenarioPoint) {

                $processInstance = $this->scenarioHandler->em->getRepository('CaseBundle:ProcessInstance')->find($this->scenarioPoint->getId());
                $rootInstance = $this->scenarioHandler->em->getRepository('CaseBundle:ProcessInstance')->find($this->scenarioPoint->getRootId());

                $newScenarioInstance->setLastProcessInstance($processInstance);
                $newScenarioInstance->setRoot($rootInstance);
            }

        }
        catch (ScenarioStopException $exception) {

            $this->logError('Scenario stop! ' . $exception->getMessage());

            $isSuccess = false;

        }
        catch (\Exception $e) {

            $this->logError('Critical Error! ' . $e->getMessage(). ', file:' . $e->getFile() . ', line: ' . $e->getLine());

            $isSuccess = false;
        }

        if($this->currentScenario->scenario->isPdf()) {
            $this->screenShooter->generateCombinedPDF($this->screenName);
        }

        $newScenarioInstance->setTime(round((microtime(true) - $time), 2));

        $errors = [];

        foreach ($this->catchErrors as $catchError) {
            $errors[] = $catchError->toArray();
        }

        if(!empty($errors)) {
            $newScenarioInstance->setErrorMessage(json_encode($errors));
        }

        $this->scenarioHandler->em->persist($newScenarioInstance);
        $this->scenarioHandler->em->flush();

        $this->logger->log(new ScenarioProgressLog('complete', [

            'message' => 'Build scenario is done. Case number: ' . $this->scenarioPoint->getRootId(),
            'extraData' => [
                'rootId' => $this->scenarioPoint->getRootId(),
                'processInstanceId' => $this->scenarioPoint->getId(),
                'stepId' => $this->scenarioPoint->getStepId(),
                'scenarioInstanceId' => $newScenarioInstance->getId(),
                'isSuccess' => $isSuccess,
                'isPDF'=> $newScenarioInstance->hasPDF()
            ],
            'time' => round((microtime(true) - $time), 2) . 's'

        ]));

        return $isSuccess;

    }

    /**
     * @param ScenarioWrapper $scenario
     * @return bool
     * @throws \Exception
     */
    public function runScenario(ScenarioWrapper $scenario) {

        $successStart = $this->startEvents($scenario);

        if(!$successStart) {
            return false;
        }

        if(!$this->scenarioPoint) {
            throw new \Exception('scenarioPoint not exists!');
        }

        if(!$this->checkStartStep($scenario)) {
            return false;
        }

        $availableAttributes = $scenario->getAttributes();

        $stepsWithVariant = $scenario->getStepsWithSetVariant();

        $diagnosisSteps = $scenario->getDiagnosisSteps();

        $nextProcessInstanceId = null;

        do {

            // -- Sprawdzenie czy zatrzymać scenariusz przed zapisaniem atrybutów
            if(!$this->checkStopScenario($scenario)) {
                break;
            }

            // -- Zapisanie atrybutów
            if(!$this->setAttributes($availableAttributes)) {
                return false;
            }

            // -- Ustawienie "trudna sprawa"
            $this->setSpecialSytuation($scenario);

            // -- Sprawdzenie czy przeładować formularz (Variant 99)
            if($scenario->isFormReload($this->scenarioPoint->getStepId())) {
                $this->reloadCurrentStep();
            }

            // -- Generowanie PDF'a
            $this->generatePdf();


            // -- Sprawdzenie czy zatrzymać scenariusz po zapisaniu atrybutów
            if(!$this->checkStopScenario($scenario, 'after')) {
                break;
            }

            $variant = 1;

            // -- Ustalenie wariantu jakim pójdzie Next
            if(array_key_exists($this->scenarioPoint->getStepId(), $stepsWithVariant)) {
                $variant = intval($stepsWithVariant[$this->scenarioPoint->getStepId()]->value->variant);
            }
            else {

                $key = array_search($this->scenarioPoint->getStepId(), array_column($diagnosisSteps, 'step_id'));

                if($key) {
                    $variant = $diagnosisSteps[$key]['variant'];
                }

            }

            // -- Next formularza
            $nextProcessInstanceId = $this->nextStep($scenario, $variant);

            if(!$nextProcessInstanceId) {
                $this->customLog('Do not exists process for next step.');
                break;
            }

            $this->scenarioPoint = new ScenarioPoint($nextProcessInstanceId, $this->scenarioHandler);

            // -- Sprawdzenie, czy dotarliśmy do kroku na którym trzeba się zatrzymać
            $endStep = $scenario->scenario->getEndStep();

            if(!empty($endStep)) {
                if($this->scenarioPoint->checkStepId($endStep)) {
                    $this->customLog('Reached end step: ' . $this->scenarioPoint->getStepId());

                    if($this->currentScenario === $scenario) {
                        $this->generatePdf();
                    }

                    break;
                }
            }

        } while ($nextProcessInstanceId);

        return true;

    }

    /**
     * @param ScenarioWrapper $scenario
     * @param string $when
     * @return bool
     */
    private function checkStopScenario(ScenarioWrapper $scenario, $when = 'before') {

        $stopScenario = $scenario->getStopScenarioStep($this->scenarioPoint->getStepId());

        if($stopScenario !== null) {

            // normal_before / normal_after
            if($stopScenario->value === "normal_" . $when ) {
                $this->customLog('Scenario is stopped on step: ' . $this->scenarioPoint->getStepId());
                return false;
            }
            elseif($stopScenario->value === "error") {
                $this->logError('Scenario is stopped on step: ' . $this->scenarioPoint->getStepId() . ' with ERROR.');
                return false;
            }

        }

        return true;

    }

    /**
     * @param ScenarioWrapper $scenario
     */
    private function setSpecialSytuation(ScenarioWrapper $scenario) {

        foreach ($scenario->getEventSteps("specialSituation") as $step) {
            if($step->stepId == $this->scenarioPoint->getStepId()) {

                $this->operationDashboardInterface->setAttributeValue(
                    $this->scenarioHandler->getUserId(),
                    $this->scenarioHandler->getOriginalUserId(),
                    [
                        'omitValidation' => false,
                        'formControl' => null,
                        'stepId' => 'xxx',
                        'value' => 5,
                        'path' => '415',
                        'valueId' => null,
                        'valueType' => 'int',
                        'groupProcessId' => $this->scenarioPoint->getGroupProcessId(),
                        'processInstanceId' => $this->scenarioPoint->getId()
                    ]
                );

                $this->customLog('Set special situation.');

                break;
            }
        }

    }

    /**
     * @param ScenarioWrapper $scenario
     * @throws \Exception
     */
    private function generatePdf(){

        if($this->currentScenario->scenario->isPdf()) {

            $time = microtime(true);

            $log = $this->logger->log(new ScenarioProgressLog('custom', [
                'message' => 'Generate PDF...'
            ]));

            $this->screenShooter->generateStepPDF($this->scenarioPoint->getId(), $this->screenName);

            $this->logger->log(new ScenarioProgressLog('custom', [
                'message' => 'Generated PDF.',
                'time' => round((microtime(true) - $time), 2) . 's'
            ], $log->id));

        }

    }

    private function reloadCurrentStep() {

        $time = microtime(true);

        $log = $this->logger->log(new ScenarioProgressLog('run-step', [
            'message' => 'Reload Step... ' . $this->scenarioPoint->getStepId()
        ]));

        $this->scenarioHandler->processHandler->next(
            $this->scenarioPoint->getId(),
            99,
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId());

        $this->logger->log(new ScenarioProgressLog('run-step', [
            'message' => 'Reload Step ' .  $this->scenarioPoint->getStepId() . ' ✔',
            'time' => round((microtime(true) - $time), 2) . 's'
        ], $log->id));

    }

    private function nextStep(ScenarioWrapper $scenario, $variant) {

        // TODO - ogarnać jeszcze sprawe ($variant != 99)

        $time = microtime(true);

        $log = $this->logger->log(new ScenarioProgressLog('run-step', [
            'message' => 'Next step (' . $variant . ')... ' . $this->scenarioPoint->getStepId()
        ]));

        if($scenario->scenario->isValidationSteps()) {
            $validationResult = $this->operationDashboardInterface->validateProcess($this->scenarioPoint->getId());

            if($validationResult['error'] > 0) {

                $this->logError('Error validation on step: ' . $this->scenarioPoint->getStepId());

                if(!empty($validationResult['controls'] )) {
                    foreach ($validationResult['controls'] as $control) {
                        $this->logError($control['desc'] . '(' . 'ControlId: ' . $control['control'] . ', attributeValueId: ' . $control['attributeValueId']);
                    }

                    return false;

                }
//                elseif(!empty($error['semirequireds'] )) {
//                    foreach ($error['semirequireds'] as $control) {
//                        ScenarioBuilder::$ioSymfony->error('ControlId: ' . $control . " - is semirequireds");
//                    }
//                }

            }
        }

        $nextResult = $this->scenarioHandler->processHandler->next(
            $this->scenarioPoint->getId(),
            $variant,
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId(),
            0,
            false,
            false);

//        if ($this->scenarioHandler->em !== null && $this->scenarioHandler->em->getConnection()->ping() === false) {
//            $this->scenarioHandler->em->getConnection()->close();
//            $this->scenarioHandler->em->getConnection()->connect();
//        }
//

        if ($this->scenarioPoint->checkStepId('1011.002') && $variant < 98) {
            $this->scenarioHandler->processHandler->cepikAttributes($this->scenarioPoint->getId(), false, false);
        }

        $this->logger->log(new ScenarioProgressLog('run-step', [
            'message' => 'Next step ' .  $this->scenarioPoint->getStepId() . '( ' . $variant . ') ✔',
            'time' => round((microtime(true) - $time), 2) . 's'
        ], $log->id));

        $nextProcessInstanceId = null;

        if(!empty($nextResult['token'])) {

            /** @var ProcessInstance $tokenProcess */
            $tokenProcessId = $this->scenarioHandler->em->getRepository('CaseBundle:ProcessInstance')->findActiveProcessByToken($nextResult['token']);

            if($tokenProcessId !== null) {
                $nextProcessInstanceId = $tokenProcessId['id'];
            }
        }
        else {

            $ids = $nextResult['ids'];

            usort($ids, function ($a, $b) {
                if(intval($b) == "-999") return false;
                return (intval($a) < intval($b));
            });

            foreach ($ids as $instance) {
                if (empty($instance)) continue;
                if ((string)$instance !== self::PROCESS_STOP) {

                    $hasAccess = $this->scenarioHandler->processHandler->checkPermission($this->scenarioHandler->getUserId(), intval($instance), 1);

                    if ($hasAccess) {
                        $nextProcessInstanceId = $instance;
                        break;
                    }
                }
            }
        }


        return $nextProcessInstanceId;
    }

    /**
     * @param $availableAttributes
     * @return bool
     */
    private function setAttributes($availableAttributes) {

        $formControls = $this->scenarioHandler->processHandler->formControls($this->scenarioPoint->getId(),null, 0);

        $log = new ScenarioProgressLog('save-attributes', [
            'message' => 'Save attributes on ' . $this->scenarioPoint->getStepId() . '...',
        ]);
        $this->logger->log($log);

        $time = microtime(true);
        $a = 0;

        foreach ($availableAttributes as $path => $availableAttribute) {

            foreach ($formControls as $formControl) {
                if($path == $formControl['attribute_path']) {

                    $value = '';

                    switch ($availableAttribute->type) {
                        case 'value': {

                            $value = $availableAttribute->value;
                            break;

                        }
                        case 'random': {

                            break;
                        }
                        case 'procedure': {

                            $value = $this->scenarioHandler->runCustomProcedure($availableAttribute->value, $this->scenarioPoint->getGroupProcessId());
                            break;

                        }
                    }

                    $result = $this->operationDashboardInterface->setAttributeValue(
                        $this->scenarioHandler->getUserId(),
                        $this->scenarioHandler->getOriginalUserId(),
                        [
                            'omitValidation' => false,
                            'formControl' => $formControl['id'],
                            'stepId' => $this->scenarioPoint->getStepId(),
                            'value' => $value,
                            'path' => $availableAttribute->path,
                            'valueId' => $formControl['attribute_value_id'],
                            'valueType' => $availableAttribute->valueType,
                            'groupProcessId' => $this->scenarioPoint->getGroupProcessId(),
                            'processInstanceId' => $this->scenarioPoint->getId()
                        ]
                    );

                    if(!empty($result['error'])) {
                        $this->logError('Invalid save path: ' . $availableAttribute->path . '. Desc: ' . $result['desc']);
                        return false;
                    }

                    $a++;

                    break;
                }
            }

        }

        $this->logger->log(new ScenarioProgressLog('save-attributes', [
            'message' => 'Save attributes on ' . $this->scenarioPoint->getStepId() . '. (' . $a . ')',
            'time' => round((microtime(true) - $time), 2) . 's'
        ], $log->id));

        return true;

    }

    /**
     * @param ScenarioWrapper $scenario
     * @return bool
     * @throws \Exception
     */
    private function startEvents(ScenarioWrapper $scenario) {

        foreach ($scenario->getEvents() as $event) {

            if($event instanceof ScenarioEventScenario) {

                $this->customLog('Start scenario: ' . $event->getScenario()->getName());

                $time = microtime(true);

                $scenarioWrapper = new ScenarioWrapper($event->getScenario(), [], $this->scenarioHandler);

                $result = $this->runScenario($scenarioWrapper);

                if(!$result) {
                    return false;
                }

                $this->logger->log(new ScenarioProgressLog('info', [
                    'message' => 'End scenario: ' . $event->getScenario()->getName(),
                    'time' =>  round((microtime(true) - $time), 2) . 's'
                ]));

            }
            elseif($event instanceof ScenarioEventRing || $event instanceof ScenarioEventRs || $event instanceof ScenarioEventEmail) {

                $this->logEvent($event->getType());

                $event->dispatchEvent();

                $this->scenarioPoint = new ScenarioPoint($event->getProcessInstanceId(), $this->scenarioHandler);

            }
            elseif ($event instanceof ScenarioEventService) {

                if(!$this->scenarioPoint) {
                    throw new ScenarioStopException('ScenarioPoint is required in Service Event!');
                }

                $this->logEvent($event->getType());

                $event->setScenarioPoint($this->scenarioPoint)->dispatchEvent();

                $this->scenarioPoint = new ScenarioPoint($event->getProcessInstanceId(), $this->scenarioHandler);

            }

        }

        return true;

    }

    private function checkStartStep(ScenarioWrapper $scenario) {

        $startStep = $scenario->scenario->getStartStep();

        if(!empty($startStep)) {

            if(!$this->scenarioPoint->checkStepId($startStep)) {

                $this->logError('Scenario: "<i>' .  $scenario->scenario->getName() . '</i>" on start <b>requires step ' . $startStep . '!</b>');

                return false;

            }

        }

        return true;

    }

    /**
     * @param $message
     * @return ScenarioProgressLog
     */
    private function logError($message) {

        $log = new ScenarioProgressLog('error', [
            'message' => $message,
        ]);

        $this->logger->log($log);

        $this->catchErrors[] = $log;

        return $log;

    }

    /**
     * @param $type
     * @param string $message
     * @return ScenarioProgressLog
     */
    private function logEvent($type, $message = '') {
        return $this->logger->log(new ScenarioProgressLog('event', [
            'message' => 'Event (' . $type . ') ' . $message,
        ]));
    }


    /**
     * @param $message
     * @return ScenarioProgressLog
     */
    private function customLog($message) {
        return $this->logger->log(new ScenarioProgressLog('info', [
            'message' => $message
        ]));
    }
}