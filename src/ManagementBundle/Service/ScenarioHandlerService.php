<?php

namespace ManagementBundle\Service;

use CaseBundle\Service\ProcessHandler;
use CaseBundle\Service\ServiceHandler;
use Doctrine\ORM\EntityManager;
use PDO;
use UserBundle\Service\UserInfoService;

class ScenarioHandlerService
{

    /** @var ProcessHandler */
    public $processHandler;

    /** @var UserInfoService */
    public $userInfo;

    /** @var ServiceHandler */
    public $serviceHandler;

    /** @var EntityManager */
    public $em;

    private $userId = null;
    private $originalUserId = null;

    /**
     * ScenarioHandlerService constructor.
     * @param ProcessHandler $processHandler
     * @param UserInfoService $userInfo
     * @param EntityManager $entityManager
     */
    public function __construct(ProcessHandler $processHandler, UserInfoService $userInfo, EntityManager $entityManager, ServiceHandler $serviceHandler)
    {
        $this->processHandler = $processHandler;
        $this->userInfo = $userInfo;
        $this->em = $entityManager;
        $this->serviceHandler = $serviceHandler;

        $this->userId = $this->userInfo->getUser()->getId();
        $this->originalUserId = $this->userInfo->getOriginalUser()->getId();

    }

    public function runCustomProcedure($procedureName, $groupProcessInstanceId = null) {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'EXEC dbo.' . $procedureName . ' @groupProcessInstanceId = :groupProcessInstanceId';

        $output = $this->processHandler->queryManager->executeProcedure($query, $parameters);

        if(!empty($output) && !empty($output[0]['value'])) {
            return $output[0]['value'];
        }

        return null;

    }

    public function getUserId() {

        return $this->userId;

    }


    public function getOriginalUserId() {

        return $this->originalUserId;

    }



}