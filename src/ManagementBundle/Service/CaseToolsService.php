<?php

namespace ManagementBundle\Service;


use AppBundle\Utils\QueryManager;
use CaseBundle\Service\ServiceHandler;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use MailboxBundle\Service\MailAttachmentsService;
use MssqlBundle\PDO\PDO;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Router;
use Twig\Environment;

class CaseToolsService
{

    /**
     * @var QueryManager
     */
    protected $queryManager;

    /** @var ServiceHandler */
    protected $serviceHandler;

    /** @var Router */
    protected $router;

    /** @var Environment */
    protected $twig;

    /** @var MailAttachmentsService */
    protected $mailboxMailAttachmentService;

    /** @var EntityManager */
    protected $em;

    /**
     * CaseToolsService constructor.
     * @param QueryManager $queryManager
     * @param ServiceHandler $serviceHandler
     * @param Router $router
     * @param Environment $twig
     * @param MailAttachmentsService $mailboxMailAttachmentService
     * @param EntityManager $em
     */
    public function __construct(QueryManager $queryManager, ServiceHandler $serviceHandler, Router $router, Environment $twig, MailAttachmentsService $mailboxMailAttachmentService, EntityManager $em)
    {
        $this->queryManager = $queryManager;
        $this->serviceHandler = $serviceHandler;
        $this->router = $router;
        $this->twig = $twig;
        $this->mailboxMailAttachmentService = $mailboxMailAttachmentService;
        $this->em = $em;
    }

    /**
     * @param $rootId
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getRZWList($rootId) {

        $rsArray = $this->serviceHandler->getAllRZWinCase($rootId);

        $links = [];

        foreach ($rsArray as $item) {

            $links[] = [
                'url' => $this->router->generate('public_operational_dashboard_index', [
                    'processToken' => $item['token']
                ]),
                'groupProcessId' => $item['group_process_id']
            ];

        }

        $rsHtmlList = $this->twig->render('AppBundle:CaseViewer/CaseTools:rzw-list.html.twig', [
            'rs_list' => $links
        ]);

        return $rsHtmlList;

    }

    /**
     * @param $rootId
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getRsList($rootId) {

        $rsArray = $this->serviceHandler->getAll_RS_inCase($rootId);

        $links = [];

        foreach ($rsArray as $item) {

            $links[] = [
                'url' => $this->router->generate('public_operational_dashboard_index', [
                    'processToken' => $item['token']
                ]),
                'groupProcessId' => $item['group_process_id']
            ];

        }

        $rsHtmlList = $this->twig->render('AppBundle:CaseViewer/CaseTools:rs-list.html.twig', [
            'rs_list' => $links
        ]);

        return $rsHtmlList;

    }

    /**
     * @param $rootId
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getEmailAttachmentsForCase($rootId) {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $notesWithGOP = $this->queryManager->executeProcedure("SELECT 
            b.noteId,
            b.name,
            documentId.value_int as documentId,
            documentId.created_at
            FROM (
                SELECT 
                a.parent_attribute_value_id as noteId,
                av.value_string as name
                FROM (
                    SELECT *
                    FROM dbo.attribute_value WITH(NOLOCK)
                    WHERE root_process_instance_id = :rootId
                    AND attribute_path = '406,226,229'
                    AND value_string = 'email'
                ) as a 
                INNER JOIN dbo.attribute_value as av WITH(NOLOCK) ON av.parent_attribute_value_id = a.parent_attribute_value_id
                WHERE av.attribute_path = '406,226,409'
                AND (av.value_string LIKE 'Gwarancja płatności%' OR av.value_string LIKE 'Ostateczna gwarancja płatności%')
            ) as b
            INNER JOIN dbo.attribute_value as documentId WITH(NOLOCK) ON documentId.parent_attribute_value_id = b.noteId AND documentId.attribute_path = '406,226,228'",
            $parameters);

        if(!empty($notesWithGOP)) {

            $attachmentsList = [];

            foreach ($notesWithGOP as $item) {

                $attachments = $this->mailboxMailAttachmentService->getAttachmentsOfDocument($item['documentId']);

                if(!$attachments instanceof JsonResponse) {

                    foreach ($attachments as $key => $attachment) {
                        $attachments[$key]['label'] = $item['name'];
                        $attachments[$key]['created_at'] = $item['created_at'];
                    }

                    $attachmentsList = array_merge($attachmentsList, $attachments);

                }

            }

            return $this->twig->render('AppBundle:CaseViewer/CaseTools:attachments-list.html.twig', [
                'attachments' => $attachmentsList
            ]);

        }

        return '';

    }

    /**
     * @param $rootId
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getOtherDocumentsForCase($rootId) {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $extraFiles = $this->queryManager->executeProcedure("SELECT value_string, ISNULL(updated_at, created_at) as updated_at
            FROM dbo.attribute_value WITH(NOLOCK)
            WHERE root_process_instance_id = :rootId
            AND attribute_path = '638,646,84'",
            $parameters);

        $extraRow = '';

        if(!empty($extraFiles)) {

            foreach ($extraFiles as $file) {
                if(!empty($file['value_string'])) {

                    $extraRow .= '<tr>
                    <td>Dodatkowy raport</td>
                        <td>' . $file['updated_at'] . '</td>
                        <td><a href="' . $file['value_string'] . '" target="_blank">Link</a>
                        </td>
                    </tr>';

                }
            }

        }

        $html = $this->renderFileList($rootId, '638,845', 'Raporty (pliki) z drogi', $extraRow);
        $html .= $this->renderFileList($rootId, '638,846', 'Dokumentacja zdjęciowa zlecenia');

        return $html;

    }

    /**
     * @param $rootId
     * @param $path
     * @param $title
     * @param string $extraRow
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function renderFileList($rootId, $path, $title, $extraRow = '') {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'path',
                'value' => $path,
                'type' => PDO::PARAM_STR
            ],

        ];

        $documentsId = $this->queryManager->executeProcedure("SELECT value_int
            FROM dbo.attribute_value WITH(NOLOCK)
            WHERE root_process_instance_id = :rootId
            AND attribute_path = :path",
            $parameters);

        $documents = [];

        if(!empty($documentsId)) {

            foreach ($documentsId as $documentId) {

                if(!empty($documentId['value_int'])) {

                    /** @var Document $document */
                    $documents[] = $this->em->getRepository('DocumentBundle:Document')->find(intval($documentId['value_int']));

                }

            }

        }

        if(empty($extraRow) && empty($documents)) {
            return '';
        }

        return $this->twig->render('AppBundle:CaseViewer/CaseTools:other-documents.html.twig', [
            'documents' => $documents,
            'title' => $title,
            'extraRow' => $extraRow
        ]);

    }

}