<?php

namespace ManagementBundle\Service;

use Doctrine\ORM\EntityManager;
use Gedmo\SoftDeleteable\SoftDeleteableListener;
use ManagementBundle\Entity\ScenarioInstance;
use OperationalBundle\Service\ScreenShooterService;
use Symfony\Component\DependencyInjection\Container;

class ScenarioCleanerService
{

    private $em;
    private $container;


    public function __construct(EntityManager $entityManager, Container $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function clean()
    {
        $originalEventListeners = [];  // initiate an array for the removed listeners
        $em = $this->em;

        foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
            foreach ($listeners as $listener) {
                if ($listener instanceof SoftDeleteableListener) {

                    // store the event listener, that gets removed
                    $originalEventListeners[$eventName] = $listener;

                    // remove the SoftDeletableSubscriber event listener
                    $em->getEventManager()->removeEventListener($eventName, $listener);
                }
            }
        }
        $list = $this->getList();



        /**
         * @var  ScenarioInstance $scenarioInstance
         */
        foreach ($list as $scenarioInstance) {
            if ($scenarioInstance->hasPDF()) {
                $this->removePDFDirectory($scenarioInstance);
            }
            $this->em->remove($scenarioInstance);
            $this->em->flush(); // or $em->flush();
        }



        foreach ($originalEventListeners as $eventName => $listener) {
            $em->getEventManager()->addEventListener($eventName, $listener);
        }


    }

    private function removePDFDirectory(ScenarioInstance $scenarioInstance)
    {
       $dirAddress =  $this->container->getParameter('kernel.project_dir') . ScreenShooterService::SCREENS_DIR .'scenario_' . $scenarioInstance->getScenario()->getId() . '_' . $scenarioInstance->getId();
//        $dirAddres =    ($scenarioInstance->getScenario()->getName());
        if(is_dir($dirAddress))
        {
            array_map('unlink', glob("$dirAddress/*.*"));
            rmdir($dirAddress);
        }else{
            unlink($dirAddress);
        }
    }


    private function getList(): array
    {
        return $this->em->getRepository('ManagementBundle:ScenarioInstance')->getInstancesToDelete();
    }
}