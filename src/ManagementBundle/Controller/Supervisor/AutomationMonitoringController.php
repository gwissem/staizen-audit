<?php

namespace ManagementBundle\Controller\Supervisor;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/automation-monitoring")
 */
class AutomationMonitoringController extends Controller
{

    /**
     * @Route("/automation_monitoring_view", name="management_supervisor_automation_monitoring_view", options={"expose":true})
     * @Security("is_granted('ROLE_ATLAS_MONITORING')")
     */
    public function automationMonitoringViewAction()
    {

        list($stats, $tools) = $this->getAutomationMonitoringStats('dbo.p_aStatistics');

        return $this->render('ManagementBundle:Supervisor:automation_monitoring_view.html.twig',
            [
                'stats' => $stats,
                'tools' => $tools
            ]
        );
    }

    /**
     * @Route("/automation_monitoring_view/stats", name="management_supervisor_automation_monitoring_stats", options={"expose":true})
     * @Security("is_granted('ROLE_ATLAS_MONITORING')")
     */
    public function automationMonitoringGetStatsAction()
    {

        list($stats) = $this->getAutomationMonitoringStats('dbo.p_aStatistics');

        $basicStats = $this->renderView('ManagementBundle:Supervisor:automation_monitoring_stats.html.twig',
            [
                'stats' => $stats
            ]);

        return new JsonResponse([
            'stats' => $basicStats
        ]);

    }

    /**
     * @Route("/fixing-and-towing_monitoring_view", name="ms_fixing_and_towing_monitoring_view", options={"expose":true})
     * @Security("is_granted('ROLE_ATLAS_MONITORING')")
     */
    public function fixingAndTowingMonitoringViewAction()
    {

        list($stats, $tools) = $this->getAutomationMonitoringStats('dbo.p_fixing_towing_Statistics');

        return $this->render('ManagementBundle:Supervisor:automation_monitoring_view.html.twig',
            [
                'stats' => $stats,
                'tools' => $tools
            ]
        );
    }

    /**
     * @Route("/fixingAndTowing_monitoring_view/stats", name="ms_fixing_and_towing_monitoring_stats", options={"expose":true})
     * @Security("is_granted('ROLE_ATLAS_MONITORING')")
     */
    public function fixingAndTowingMonitoringGetStatsAction()
    {

        list($stats) = $this->getAutomationMonitoringStats('dbo.p_fixing_towing_Statistics');

        $basicStats = $this->renderView('ManagementBundle:Supervisor:automation_monitoring_stats.html.twig',
            [
                'stats' => $stats
            ]);

        return new JsonResponse([
            'stats' => $basicStats
        ]);

    }

    private function getAutomationMonitoringStats($procName) {

        $queryManager = $this->get('app.query_manager');

        $query = 'EXEC ' . $procName;

        $output = $queryManager->executeProcedure($query, []);

        $stats = [];
        $tools = [];

        foreach ($output as $item) {
            if (isset($item['type'])) {

                if (!isset($item['view'])) {
                    $item['view'] = 'basic';
                }

                if ($item['type'] === "stat") {

                    if (empty($item['icon'])) {
                        $item['icon'] = 'fa-bar-chart-o';
                    }

                    if (empty($item['color'])) {
                        $item['color'] = 'blue';
                    }

                    $stats[] = $item;

                } elseif ($item['type'] === "tool") {
                    if (isset($item['value']) && $item['value']) {

                        if (empty($item['color'])) {
                            $item['color'] = 'red';
                        }

                        $tools[] = $item;

                    }
                }

            }
        }

        return [$stats,$tools];

    }


}
