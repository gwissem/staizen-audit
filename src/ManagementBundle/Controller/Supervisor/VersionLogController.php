<?php

namespace ManagementBundle\Controller\Supervisor;

use CaseBundle\Entity\AtlasVersionLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class VersionLogController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/version-log")
 */
class VersionLogController extends Controller
{
    /**
     * @Route("/timeline", name="management_supervisor_version_log_timeline", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        $from = $request->query->get('from', (new \DateTime())->modify('-7 days'));
        $to = $request->query->get('to', (new \DateTime()));

        if(!$from instanceof \DateTime) {
            $from = new \DateTime($from);
        }

        if(!$to instanceof \DateTime) {
            $to = new \DateTime($to);
        }

        /** @var AtlasVersionLog[] $logs */
        $logs =  $this->getDoctrine()->getRepository('CaseBundle:AtlasVersionLog')->getLogsForTimeline($from, $to);

        $versions = [];

        foreach ($logs as $log) {

            if(!isset($versions[$log->getVersion()])) {
                $versions[$log->getVersion()] = [
                    'logs' => [],
                    'date' => $log->getVersionDate(),
                    'version' => $log->getVersion()
                ];
            }

            $versions[$log->getVersion()]['logs'][] = [
                'type' => $log->getType(),
                'icon' => $log->getIconOfType(),
                'description' => $log->getDescription()
            ];

        }

        $html = $this->renderView('@Management/Supervisor/AtlasVersionLog/atlas_version_log_list.html.twig', [
            'versions' => $versions
        ]);

        return new JsonResponse([
            'html' => $html,
            'versions' => $versions,
            'from' => $from,
            'to' => $to
        ]);

    }

}
