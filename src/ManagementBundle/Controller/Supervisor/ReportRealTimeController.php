<?php

namespace ManagementBundle\Controller\Supervisor;

use CaseBundle\Entity\Platform;
use CaseBundle\Entity\PlatformGroup;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\ProcessInstanceProperty;
use ManagementBundle\Form\Type\TaskFilterType;
use ManagementBundle\Utils\ManagementRoleHierarchy;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;
use UserBundle\Entity\UserRoleGroup;
use UserBundle\Entity\UserRoleGroupTask;

/**
 * Class ReportRealTimeController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/report-real-time")
 */
class ReportRealTimeController extends Controller
{

    const RK_REPORT_CACHE = 'report_real_time';

    private $availableColumns;

    /**
     * ReportRealTimeController constructor.
     */
    public function __construct()
    {
        $this->availableColumns = [
            'active_count' => 'Zadań na teraz',
            'timeout' => 'Jak długo oczekuje',
            'hour_postpone_count' => 'Terminów w najbliższej godzinie',
            'day_postpone_count' => 'Terminy do wykonania dzisiaj'
        ];
    }

    /**
     * @Route("/", name="management_supervisor_report_real_time_index", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $groups = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy(
            [
                'type' => 1
            ],
            [
                'orderBy' => 'ASC'
            ]
        );

        $groupsRow = $this->mappingGroups($groups);

        $platformGroups = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->findBy([
            'active' => 1
        ]);

        // TODO - TYMCZASOWY ZABIEG, prawdopodobnie trzeba będzie dodać filtrowanie na widoku, które grupy platform wyświetlić.
        usort($platformGroups, function($group1)
        {
            return !in_array($group1->getId(), [1,4]);
        });

        $groupPlatformWrapper = $this->getReportTables($platformGroups, 0);

        $onlineRoles = $this->getOnlineUsersWithRoles();

        $html = $this->renderView('@Management/Supervisor/ReportRealTime/content.html.twig', [
            'groupsRow' => $groupsRow,
            'platformGroups' => $platformGroups,
            'availableColumns' => $this->availableColumns,
            'groupPlatformWrapper' => $groupPlatformWrapper,
            'onlineRoles' => $onlineRoles
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }


    /**
     * @Route("/refresh-online-roles", name="management_supervisor_report_real_time_refresh_online_role", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function refreshOnlineRolesAction()
    {

        $onlineRoles = $this->getOnlineUsersWithRoles();

        // Rola = [ platformId, groupId]

        $html = $this->renderView('@Management/Supervisor/ReportRealTime/current_rules.html.twig', [
            'onlineRoles' => $onlineRoles
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }

    private function getOnlineUsersWithRoles() {

        $onlineUsers = $this->getDoctrine()->getRepository('UserBundle:User')->getAllUserInGroup(33, true);

        return $this->aggregatePerRoles($onlineUsers);

    }

    /**
     * @param User[] $users
     * @return array
     */
    private function aggregatePerRoles(array $users) {

        $result = [];

        $groupsWithPlatform = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->findPlatformGroupsPerPlatforms();

        /** @var User $user */
        foreach ($users as $user) {

            $roleGroup = $user->getRoleGroup();
            if(!$roleGroup) continue;

            $id = $roleGroup->getId();

            if(!isset($result[$id])) {
                
                $tasks = [];

                /** @var UserRoleGroupTask $task */
                foreach ($roleGroup->getTasks() as $task) {

                    $tasks[] = [
                        'groupId' => $task->getUserGroup()->getId(),
                        'platformGroupId' => $groupsWithPlatform[$task->getPlatform()->getId()]
                    ];

                }

//                $tempArr = array_unique(array_column($tasks, 'groupId'));

                $result[$id] = [
                    'count' => 0,
                    'name' => $roleGroup->getName(),
//                    'tasks' => array_intersect_key($tasks, $tempArr)
                    'tasks' => $tasks
                ];
            }

            $result[$id]['count']++;

        }

        return $result;
    }

    /**
     * @Route("/online-tasks-content", name="management_supervisor_report_real_time_online_tasks_content", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @Method("GET")
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contentOfOnlineTasksAction(Request $request)
    {
        $platformGroups = $this->getDoctrine()->getRepository(PlatformGroup::class)->fetchPlatformGroupsArray();
        $groups = $this->getDoctrine()->getRepository(Group::class)->fetchGroupsArray();

        $platformGroupsArray = [];
        $groupsArray = [];

        foreach ($platformGroups as $platformGroup) {
            $platformGroupsArray[$platformGroup['name']] = $platformGroup['id'];
        }

        foreach ($groups as $group) {
            $groupsArray[$group['name']] = $group['id'];
        }

        unset($platformGroups);
        unset($groups);

        $form = $this->createForm(TaskFilterType::class, null, [
            'platformGroups' => $platformGroupsArray,
            'groups' => $groupsArray
        ]);

        $html = $this->renderView('@Management/Supervisor/OnlineTasks/content.html.twig', [
            'form' => $form->createView()
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }

    /**
     * @Route("/tasks", name="management_supervisor_report_real_time_tasks", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @Method("GET")
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tasksAction(Request $request)
    {

        $platformGroups = $this->getDoctrine()->getRepository(PlatformGroup::class)->fetchPlatformGroupsArray();
        $groups = $this->getDoctrine()->getRepository(Group::class)->fetchGroupsArray();

        $form = $this->createForm(TaskFilterType::class, null, [
            'platformGroups' => $platformGroups,
            'groups' => $groups
        ]);

        $form->handleRequest($request);
        $platformGroupId = $form->get('platformGroupId')->getData();
        $groupId = $form->get('groupId')->getData();
        $column = $form->get('column')->getData();

        $tasks = $this->get('case.process_handler')->taskReport($platformGroupId, $groupId, $column, 0);

        $apiSocketTask = $this->get('api_task.service');
        $allActiveTasks = $apiSocketTask->getAllActiveTasks('assoc-array');

        $onlineUsers = $this->getUsersName();

        if(!empty($allActiveTasks)) {
            foreach ($tasks as $key => $task) {
                if(key_exists($task['id'], $allActiveTasks)) {

                    $userId = $allActiveTasks[$task['id']]['user_id'];

                    if(key_exists($userId, $onlineUsers)) {
                        $tasks[$key]['user_id'] = $onlineUsers[$userId];
                    }
                    else {
                        $tasks[$key]['user_id'] = $userId;
                    }
                }
            }
        }

        return new JsonResponse([
            'tasks' => $tasks
        ]);

    }

    private function getUsersName() {
        $users = [];

        $allOnlineUsers = $this->getDoctrine()->getRepository('UserBundle:User')->getAllOnlineUserInGroup(33);

        foreach ($allOnlineUsers as $user) {
            $users[$user['id']] = (!empty($user['lastname']) && !empty($user['firstname'])) ? ($user['firstname'] . " " . $user['lastname']) : $user['username'];
        }

        return $users;
    }

    /**
     * @Route("/urgent/{id}", name="management_supervisor_urgent_instance", options={"expose":true}, requirements={"id": "\d+"})
     * @Security("is_granted('ROLE_KZ')")
     * @Method("POST")
     * @ParamConverter("processInstance", class="CaseBundle:ProcessInstance")
     * @param $processInstance
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function processInstanceUrgentAction($processInstance)
    {

        /** @var ProcessInstance $processInstance */
        if (!$processInstance) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono zadania'));
        }
        $em = $this->getDoctrine()->getManager();

        $property = $processInstance->getProperty();
        if(!$property){
            $property = new ProcessInstanceProperty();
            $platformId = $this->get('case.process_handler')->getAttributeValue('253', $processInstance->getGroupProcess()->getId(),'int');
            $platform = $em->getRepository(Platform::class)->find($platformId);
            if($platform){
                $property->setPlatform($platform);
            }
            $property->setProcessInstance($processInstance);
        }

        $currentPriority = $property->getPriority() ?: 0;
        $urgent = $property->getUrgent();
        $priority = $processInstance->getStep()->getUrgentPriorityBoost() ?: 100;

        if($urgent){
            $priority = $currentPriority - $priority;
            $urgent = false;
        }
        else{
            $priority = $currentPriority + $priority;
            $urgent = true;
        }

        if($priority < 0) $priority = 0;

        $property->setPriority($priority);
        $property->setUrgent($urgent);

        $em->persist($property);
        $em->flush();

        return new JsonResponse(
            [
                'urgent' => $urgent,
                'priority' => $priority
            ]
        );

    }

    /**
     * @param PlatformGroup[] $platformGroups
     * @param int $withTaskReport
     * @return array
     */
    private function getReportTables($platformGroups, $withTaskReport = 1) {

        $processHandler = $this->get('case.process_handler');
        $groupPlatformWrapper = [];

        foreach ($platformGroups as $platformGroup) {
            $groupPlatformWrapper[$platformGroup->getId()] = [
                'platformGroup' => $platformGroup,
                'results' => ($withTaskReport) ? $this->mappingResults($processHandler->taskReport($platformGroup->getId())) : $this->mappingResults([])
            ];
        }

        return $groupPlatformWrapper;

    }

    /**
     * @Route("/refresh-report", name="management_supervisor_refresh_report", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function refreshReportAction(Request $request)
    {

        $platformGroupId = $request->query->get('groupPlatformId', null);
        $redis = $this->get('snc_redis.default');

        $cacheReport = $redis->get(self::RK_REPORT_CACHE . "." . $platformGroupId);

        if($cacheReport) {
            return new JsonResponse([
                'html' => $cacheReport
            ]);
        }

        $platformGroup = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->find(intval($platformGroupId));

        $groupPlatformWrapper = $this->getReportTables([$platformGroup]);

        $html = $this->renderView('@Management/Supervisor/ReportRealTime/td_body.html.twig', [
            'platformGroup' => $platformGroup,
            'availableColumns' => $this->availableColumns,
            'groupPlatformWrapper' => $groupPlatformWrapper
        ]);

        $redis->setex(self::RK_REPORT_CACHE . "." . $platformGroupId, 30, $html);

        return new JsonResponse([
            'html' => $html
        ]);

    }

    private $groupsRow = [
        'FRONT' => [
            'label' => 'Front',
            'groups' => []
        ],
        'ORGA_' => [
            'label' => 'Organizacja',
            'groups' => []
        ],
        'MONIT_' => [
            'label' => 'Monitoring',
            'groups' => []
        ],
        'KONIEC_' => [
            'label' => 'Zamykanie',
            'groups' => []
        ],
        '_KZ' => [
            'label' => 'KZ',
            'groups' => []
        ],
        'OTHER' => [
            'label' => 'Inne zadania',
            'groups' => []
        ]
    ];

    private function mappingGroups($groups) {

        $groupsRow = $this->groupsRow;

        /** @var Group $group */
        foreach ($groups as $group) {

            $shortName = $group->getShortName();
            $added = false;

            foreach ($groupsRow as $key => $item) {
                if(strpos($shortName, $key) !== FALSE) {
                    $groupsRow[$key]['groups'][] = $group;
                    $added = true;
                    break;
                }
            }

            if(!$added) {
                $groupsRow['OTHER']['groups'][] = $group;
            }

        }

        return $groupsRow;
    }

    private function mappingResults($results) {

        $groupsRow = $this->groupsRow;

        foreach ($results as $result) {

            $added = false;

            foreach ($groupsRow as $key => $item) {
                if(strpos($result['short_name'], $key) !== FALSE) {
                    $groupsRow[$key]['groups'][] = $result;
                    $added = true;
                    break;
                }
            }

            if(!$added) {
                $groupsRow['OTHER']['groups'][] = $result;
            }

            // Usunięcie niepotrzebnych kolumn
            unset($groupsRow['OTHER']['groups']['short_name']);
            unset($groupsRow['OTHER']['groups']['name']);
        }

        return $groupsRow;

    }
}
