<?php

namespace ManagementBundle\Controller\Supervisor;

use AppBundle\Entity\Config;
use CaseBundle\Entity\PushingDefinition;
use CaseBundle\Entity\PushingTemplate;
use CaseBundle\Entity\Step;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Group;

/**
 * Class ReportRealTimeController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/priority")
 */
class PriorityController extends Controller
{

    private $availableColumns;

    /**
     * PriorityController constructor.
     */
    public function __construct()
    {
        $this->availableColumns = [];
    }

    /**
     * @Route("/{id}", name="management_supervisor_priority_index", options={"expose":true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param int|null $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $id = null)
    {
        $em = $this->getDoctrine()->getManager();

        if (false === isset($id)) {
            $id = $em->getRepository(Config::class)
                ->findOneBy(['key' => Config::PUSHING_TEMPLATE_ID_KEY])
                ->getValue();
        }

        $template = $em->getRepository(PushingTemplate::class)->getAllArray();
        $groupRepo = $em->getRepository(Group::class);

        $tasks = null;
        if($id){
            $tasks = $groupRepo->findAllForTemplate($id);
        }

        $html = $this->renderView('ManagementBundle:Supervisor/PrioritySteps:content.html.twig', [
            'tasks' => $tasks,
            'templates' => $template,
            'chosenTemplate' => $id
        ]);

        return new JsonResponse([
            'html' => $html,
            'tasks' => $tasks,
        ]);

    }

    /**
     * @Route("/change/{id}/{type}", name="management_supervisor_priority_change", options={"expose":true}, requirements={"id": "[a-zA-Z.0-9]+", "type" : "task|step"})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method("POST")
     * @param Group $userGroup
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeAction(Request $request, $id, $type)
    {

        $em = $this->getDoctrine()->getManager();
        $currentTemplateId = (int)$request->request->get('currentTemplate');
        $steps = [];
        if($type == 'step'){
            $currentStep = $em->getRepository(Step::class)->find($id);
            if (!$currentStep) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono grupy zadań'));
            }
            $steps[] = $currentStep;

        }
        elseif($type == 'task'){
            $userGroup = $em->getRepository(Group::class)->find($id);
            if (!$userGroup) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono grupy zadań'));
            }
            $steps = $userGroup->getSteps();
        }

        $priority = $request->request->get('priority');
        $maxPriority = $request->request->get('maxPriority');
        $userGroupId = $request->request->get('userGroupId');
        $stepPriority = $request->request->get('stepPriority', 0);

        /** @var PushingTemplate $currentTemplate */
        $currentTemplate = $em->getRepository(PushingTemplate::class)
            ->find($currentTemplateId);

        /** @var Step $step */
        foreach($steps as $step){
            /** @var PushingDefinition $pushingDefinition */
            $pushingDefinition = $em->getRepository(PushingDefinition::class)
                ->findOneBy([
                    'step' => $step,
                    'template' => $currentTemplate,
                    'userGroup' => $userGroupId
                ]);

            if(null === $pushingDefinition) {
                $pushingDefinition = new PushingDefinition();
                $pushingDefinition->setStep($step);
                $pushingDefinition->setTemplate($currentTemplate);
            }

            $pushingDefinition->setUserGroup($userGroupId);
            $pushingDefinition->setPriority($priority);
            $pushingDefinition->setStepPriority($stepPriority);
            $step->setPushingDefinition($pushingDefinition);
            $step->setMaxPriority($maxPriority);

            $em->persist($pushingDefinition);
            $em->persist($step);
        }

        $em->flush();

        return new JsonResponse();

    }

    /**
     * @Route("/template/add", name="management_supervisor_priority_add_template", options={"expose":true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addTemplateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $templateName = $request->request->get('template-name');
        $chosenTemplate = $request->request->get('chosenTemplate');

        $pushingTemplateRepo = $em->getRepository(PushingTemplate::class);
        $pushingTemplate = $pushingTemplateRepo->findOneBy(['name' => $templateName]);

        if(true === isset($pushingTemplate)) {
            throw $this->createNotFoundException($this->get('translator')->trans('Istnieje szablon o podanej nazwie.'));
        }

        $pushingTemplate = new PushingTemplate();
        $pushingTemplate->setName($templateName);

        $em->persist($pushingTemplate);

        $em->flush();

        $html = $this->renderView('@Management/Supervisor/PrioritySteps/template_select.html.twig', [
            'templates' => $pushingTemplateRepo->findAll(),
            'chosenTemplate' => $chosenTemplate
        ]);

        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @Route("/template/remove", name="management_supervisor_priority_remove_template", options={"expose":true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeTemplateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $templateId = $request->request->get('template');

        $templateRepo = $em->getRepository(PushingTemplate::class);

        $templateToRemove = $templateRepo->find($templateId);

        $em->remove($templateToRemove);
        $em->flush();

        return new JsonResponse();
    }

}
