<?php

namespace ManagementBundle\Controller\Supervisor;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\ProcessHandler;
use FOS\RestBundle\Controller\Annotations as Rest;
use PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Class CaseToolsController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/case-tools")
 */
class CaseToolsController extends Controller
{
    const PATH_COMPLAINT_TEXT = '1150,63';
    const PATH_COMPLAINT_TYPE = '1150,1151';
    const PATH_COMPLAINT_GROUP_ID = '1150,839';
    const PATH_CAR_USED_BY_CONTRACTOR = '638,273';
    const PATH_CLOSE_CASE_PROPOSAL_TEXT = '1161';
    const PATH_COMPLAINT_REQUEST_BY_DF = '1150,1160';
    const PATH_COMPLAINT_USER = '1150,404';
    const STEP_CLOSE_CASE_PROPOSAL_STEP = '1214.006';
    const STEP_COMPLAINT_DF_START = '1214.005';
    const STATUS_START_ZO = '1049';
    const STATUS_START_COMPLAINT = '1047';


    /**
     * @Route("/get-services-table", name="management_supervisor_case_tools_services_table", options={"expose":true})
     * @Security("is_granted('ROLE_SUPERVISOR')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function indexAction(Request $request)
    {

        $servicesTable = '';
        $caseInfo = '';
        $processInstanceMatrix = null;
        $rsHtmlList = '';
        $emailAttachments = '';
        $documents = '';
        $rzwHtmlList = '';

        $rootId = $request->query->get('searchQuery', null);
        $searchType = $request->query->get('searchType', null);
        $searchQuery = $request->query->get('searchQuery', null);

        if ($searchType == 0) {
            if ($rootId) {

                $rootId = $this->parseCaseNumber($rootId);

                $isRoot = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy([
                    'root' => intval($rootId)
                ], [], 1);

                if (!empty($isRoot)) {

                    $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($rootId));

                    if ($processInstance) {

                        $processHandler = $this->get('case.process_handler');
                        $attributeParser = $this->get('case.attribute_parser.service');
                        $caseToolsService = $this->get('management.case_tools.service');

                        $services = $processHandler->getStatusServices($processInstance->getId(), $request->getLocale(), 1);

                        $isSpecialDataCase = (bool)$processHandler->getAttributeValue('1168', $rootId, AttributeValue::VALUE_INT);

                        if (count($services) > 0) {
                            foreach ($services as $key => $service) {


                                if ($service['serviceId'] == 12)
                                {
                                    $wasDC = filter_var($processHandler->getAttributeValue('802',$service['group_process_id'],AttributeValue::VALUE_INT),FILTER_VALIDATE_BOOLEAN);

                                    if($wasDC) {
                                        $services[$key]['service_name'] .= ' / DC';
                                    }
                                }

                                $attributeParser->setGroupProcessInstanceId($service['group_process_id']);
                                $services[$key]['status_label'] = $attributeParser->parseString($service['status_label']);

                                if ((int)$service['serviceId'] === 18 && true === $isSpecialDataCase && false === $this->isGranted('ROLE_RODO_MEDICAL')) {
                                    unset($services[$key]);
                                }
                            }
                        }

                        $servicesTable = $this->renderView('@Management/Supervisor/CaseTools/list_services_of_case_table.html.twig', [
                            'services' => $services
                        ]);

                        $caseInfo = $this->container->get('case.attribute_parser.service')->getCaseInfo($processInstance);

                        if($this->isGranted('ROLE_FINANCE_VIEW')) {

                            /** Adresy URL do RS'ów w sprawie */
                            $rsHtmlList = $caseToolsService->getRsList($rootId);
                            $emailAttachments = $caseToolsService->getEmailAttachmentsForCase($rootId);
                            $documents = $caseToolsService->getOtherDocumentsForCase($rootId);
                            $rzwHtmlList = $caseToolsService->getRZWList($rootId);

                        }

                        if($processHandler->matrixExists($rootId)) {

                            $matrixStep = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findStepWithMatrix($processInstance);

                            if($matrixStep instanceof ProcessInstance) {
                                $processInstanceMatrix = $matrixStep->getId();
                            }
                            else {
                                $processInstanceMatrix = $rootId;

                            }

                        }

                    }

                }

            }
        } else if ($searchType == 2) //nr Rejestracyjny
        {
            if ($searchQuery) {


                $user = $this->getUser();
                $userId = $user->getId();

                $parameters = [
                    [
                        'key' => 'searchQuery',
                        'value' => $searchQuery,
                        'type' => PDO::PARAM_STR,
                        'length' => 200
                    ],
                    [
                        'key' => 'searchType',
                        'value' => (int)$searchType,
                        'type' => PDO::PARAM_INT
                    ],
                    [
                        'key' => 'userId',
                        'value' => (int)$userId,
                        'type' => PDO::PARAM_INT
                    ]
                ];

                $casesFound = $this->container->get('app.query_manager')->executeProcedure(
                    "EXECUTE dbo.p_searchCaseForViewer @searchQuery = :searchQuery, @searchType = :searchType, @userId = :userId",
                    $parameters
                );


             return new JsonResponse(
                 ['casesFound'=>$casesFound]
             );
            }
        }

        return new JsonResponse([
            'services' => $servicesTable,
            'caseInfo' => $caseInfo,
            'matrixProcessInstance' => $processInstanceMatrix,
            'rootId' => $rootId,
            'rsList' => $rsHtmlList,
            'emailAttachments' => $emailAttachments,
            'documents' => $documents,
            'rzwList' => $rzwHtmlList
        ]);

    }

    /**
     * @Route("/case-close-proposal", name="management_supervisor_case_tools_make_close_case_proposal", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     */
    public function caseCloseProposalAction(Request $request)
    {
        $processHandler = $this->get('case.process_handler');
        $noteHandler = $this->get('note.handler');
        $queryManager = $this->get('app.query_manager');
        $root = intval($request->request->get('rootId'));
        $proposalText = $request->request->get('proposalCaseClosedText');
        $userId = 1;
        $originalUserId = 1;
        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if ($user) {
            $userId = $user->getId();
            $originalUserId = $userId;
        }
        $noteHandler->addNote($root, 'system',$proposalText,null,null,$userId,1);

//        MAKE ZO

        $parameters = [
            [
                'key' => 'stepId',
                'value' => self::STEP_CLOSE_CASE_PROPOSAL_STEP,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'rootId',
                'value' => $root,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],    [
                'key' => 'parentProcessId',
                'value' => $root,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'originalUserId',
                'value' => $originalUserId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'processInstanceId',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
        ];


        $result = $queryManager->executeProcedure(
            'EXEC dbo.p_process_new @stepId = :stepId, @err = :err, @message = :message, @processInstanceId = :processInstanceId, @parentProcessId = :parentProcessId,  @userId = :userId, @originalUserId = :originalUserId, @rootId = :rootId',
            $parameters
        );

        if(isset($result['processInstanceId']) && !empty($result['processInstanceId']))
        {
            $processInstance  = $result['processInstanceId'];

        };
        if($processInstance) {
            $parametersFormControl = [
                [
                    'key' => 'instanceId',
                    'value' => $processInstance,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
            ];

            $queryManager->executeProcedure('SET NOCOUNT ON ; EXEC p_form_controls @instance_id = :instanceId', $parametersFormControl);


            $parametersAddService = [
                [
                    'key' => 'groupProcessInstanceId',
                    'value' => $processInstance,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
                [
                    'key' => 'statusId',
                    'value' => self::STATUS_START_ZO,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
            ];
           $queryManager->executeProcedure('EXEC dbo.p_add_service_status
			@groupProcessInstanceId = :groupProcessInstanceId,
			@status = :statusId,
			@serviceId = 33', $parametersAddService,false);


            $processHandler->editAttributeValue(
                self::PATH_CLOSE_CASE_PROPOSAL_TEXT,
                $processInstance,
                $proposalText,
                AttributeValue::VALUE_TEXT,
                'xxx',
                1,
                1
            );
        }

        return new JsonResponse('Ok');


        }

    /**
     * @Route("/case-car-change", name="management_supervisor_case_tools_car_change", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     */
    public function caseCarChange(Request $request)
    {
        $noteHandler = $this->get('note.handler');
        $queryManager = $this->get('app.query_manager');
        $root = intval($request->request->get('rootId'));
        $groupId= intval($request->request->get('groupProcessInstanceId'));
        $carType = $request->request->get('contractorCarType');
        $carTypeId = $request->request->get('contractorCarTypeId');
        $userId = 1;
        $originalUserId = 1;
        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if ($user) {
            $userId = $user->getId();
            $originalUserId = $userId;
        }


        $noteText = $this->get('translator')->trans(
            'Użytkownik "%username%" zmienił samochód kontraktora na %changedTo%.',
            [
                '%username%' => $user->getName(),
                '%changedTo%' =>$carType,
            ]
        );

        $noteHandler->addNote($root, 'system',$noteText,null,null,$userId,1);


        $parameters = [
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],     [
                'key' => 'attribute_path',
                'value' => self::PATH_CAR_USED_BY_CONTRACTOR,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ],
            [
                'key' => 'groupProcessId',
                'value' => $groupId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'originalUserId',
                'value' => $originalUserId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
            [
                'key' => 'value',
                'value' => $carTypeId,
                'type' => PDO::PARAM_INT,
                'length' => 100,
            ],
        ];
        
        $queryManager->executeProcedure("
        EXEC [dbo].[p_attribute_edit]
	   @attributePath = :attribute_path, 
	   @groupProcessInstanceId = :groupProcessId,
	   @stepId = 'xxx',
	   @userId = :userId,
	   @originalUserId = :originalUserId,
	   @valueInt = :value,
	   @err = :err ,
	   @message = :message  
	   ",$parameters);

        return new JsonResponse('Ok');


        }


    /**
     * @Rest\Route("/df-stat-case/{rootId}", name="management_supervisor_df_case_stats", options={"expose":true})
     * @return JsonResponse
     */
        public function dfStats(Request $request, $rootId)
        {

//            $root = $request->request->get('rootId');



            $queryManager = $this->container->get('app.query_manager');

            $parameters = [
                [
                    'key' => 'rootId',
                    'value' => intval($rootId),
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
            ];
//            GET rootClaimnCount
//
            $rootCount = $queryManager->executeProcedure("SELECT count (id) counter from attribute_value a with (nolock )
    where a.attribute_path = '1150,839' and a.root_process_instance_id =:rootId and isnull(value_int,root_process_instance_id ) = root_process_instance_id",
                $parameters);
            $rootCount =  $rootCount[0]['counter']?? 0;

     $groupCount = $queryManager->executeProcedure("
     SELECT count (id) counter from attribute_value a with (nolock )
    where a.attribute_path = '1150,839' and a.root_process_instance_id =:rootId and isnull(value_int,root_process_instance_id ) <> root_process_instance_id", $parameters);

            $groupCount =  $groupCount[0]['counter']?? 0;


            $carChangeCount = $queryManager->executeProcedure("
     SELECT count (id) counter from attribute_value a with (nolock )
    where a.attribute_path = '406,226,227' and a.root_process_instance_id =:rootId and value_text like '%mienił samochód kontraktora na%'", $parameters);
            $carChangeCount =  $carChangeCount[0]['counter']?? 0;
            $parameters = [
                [
                    'key' => 'rootId',
                    'value' => intval($rootId),
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
                [
                    'key' => 'stepId',
                    'value' => self::STEP_CLOSE_CASE_PROPOSAL_STEP,
                    'type' => PDO::PARAM_STR
                ],
            ];
            $closeCaseMesssage = $queryManager->executeProcedure("
SELECT count (id) counter from process_instance pi with (nolock )
    where pi.root_id =:rootId and step_id = :stepId", $parameters);
            $closeCaseMesssage =  $closeCaseMesssage[0]['counter']?? 0;
            return new JsonResponse(
                [
                    'claim' => [
                        'rootCount'=>(int)$rootCount,
                        'groupCount'=>(int)$groupCount
                    ],
                    'closeCase' => (int)$closeCaseMesssage,
                    'carChange' => (int)$carChangeCount
                ]
            );
        }

    /**
     * @Route("/make-complaint", name="management_supervisor_case_tools_make_complaint", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     */
    public function makeComplaintAction(Request $request)
    {
        $processHandler = $this->get('case.process_handler');
        $root = intval($request->request->get('rootId'));
        $complaintType = $request->request->get('complaintType');
        $complaintText = $request->request->get('complaintText');
        $complaintGroupId = $request->request->get('groupOfComplaint');
        $queryManager = $this->container->get('app.query_manager');

//        MAKE NEW Complaint

        $userId = null;
        $originalUserId = null;
        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if ($user) {
            $userId = $user->getId();
            $originalUserId = $userId;
        }
        $parameters = [
            [
                'key' => 'stepId',
                'value' => self::STEP_COMPLAINT_DF_START,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'rootId',
                'value' => $root,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'parentProcessId',
                'value' => $root,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'originalUserId',
                'value' => $originalUserId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'processInstanceId',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
        ];


        $result = $queryManager->executeProcedure(
            'EXEC dbo.p_process_new @stepId = :stepId, @err = :err, @message = :message, @processInstanceId = :processInstanceId, @parentProcessId = :parentProcessId,  @userId = :userId, @originalUserId = :originalUserId, @rootId = :rootId',
            $parameters
        );

        if(isset($result['processInstanceId']) && !empty($result['processInstanceId']))
        {
            $processInstance  = $result['processInstanceId'];

        };
        if($processInstance){
            $parametersFormControl=[
                [
                    'key' => 'instanceId',
                    'value' => $processInstance,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
            ];

            $queryManager->executeProcedure('EXEC p_form_controls @instance_id = :instanceId', $parametersFormControl);
            $processHandler->editAttributeValue(
                self::PATH_COMPLAINT_TEXT,
                $processInstance,
                $complaintText,
                AttributeValue::VALUE_TEXT,
                'xxx',
                1,
                1
            );

            $processHandler->editAttributeValue(
                self::PATH_COMPLAINT_TYPE,
                $processInstance,
                $complaintType,
                AttributeValue::VALUE_INT,
                'xxx',
                1,
                1
            );

            $processHandler->editAttributeValue(
                self::PATH_COMPLAINT_GROUP_ID,
                $processInstance,
                $complaintGroupId,
                AttributeValue::VALUE_INT,
                'xxx',
                1,
                1
            );
            $processHandler->editAttributeValue(
                self::PATH_COMPLAINT_REQUEST_BY_DF,
                $processInstance,
                1,
                AttributeValue::VALUE_INT,
                'xxx',
                1,
                1
            );
            $processHandler->editAttributeValue(
                self::PATH_COMPLAINT_USER,
                $processInstance,
                $userId,
                AttributeValue::VALUE_INT,
                'xxx',
                1,
                1
            );
            $parametersAddService = [
                [
                    'key' => 'groupProcessInstanceId',
                    'value' => $processInstance,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
                [
                    'key' => 'statusId',
                    'value' => self::STATUS_START_COMPLAINT,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
            ];
          $queryManager->executeProcedure('EXEC dbo.p_add_service_status
			@groupProcessInstanceId = :groupProcessInstanceId,
			@status = :statusId,
			@serviceId = 33', $parametersAddService, false);
            $parametersNext=[
                [
                    'key' => 'instanceId',
                    'value' => $processInstance,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
                [
                    'key' => 'error',
                    'value' => 0,
                    'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                    'length' => 100
                ],
                [
                    'key' => 'message',
                    'value' => '',
                    'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                    'length' => 255,
                ],
            ];

            $queryManager->executeProcedure('EXEC p_process_next  @previousProcessId = :instanceId, @message = :message , @err= :error', $parametersNext);

        }else{
            throw new \Exception('Not created');
        }

        return new JsonResponse(['status'=>'Ok']);
    }


    /**
     * @Rest\Route("/list-of-services/{rootId}",name="management_supervisor_case_tools_get_services_list", options={"expose":true})
     * @Method("GET")
     * @param $rootId
     */
    public function getListOfServices(Request $request, Int $rootId)
    {
        $processHandler = $this->get('case.process_handler');

        $services = $processHandler->getStatusServices($rootId, $request->getLocale(), 1);

        $result = [];
        if(is_array($services)) {
            $result = array_map(function ($single) {

                return [
                    'name' => $single['service_name'],
                    'value' => (int)$single['group_process_id']
                ];
            }, $services);
        }

        $result[] = [
            'name' => 'Cała sprawa',
            'value'=>  $rootId
        ];

        return new JsonResponse(['options' => $result]);
    }

    /**
     * @Rest\Route("/list-of-towing-services/{rootId}",name="management_supervisor_case_tools_get_towing_services_list", options={"expose":true})
     * @Method("GET")
     * @param $rootId
     */
    public function getListOfTowingServices(Request $request, Int $rootId)
    {
        $processHandler = $this->get('case.process_handler');

        $services = $processHandler->getStatusServices($rootId, $request->getLocale(), 1);

        $result = [];
        if(is_array($services)) {
            $services  = array_filter($services,
                function ($single){
                return $single['serviceId'] <=2;
            });

            $result = array_map(function ($single) {

                return [
                    'name' => $single['service_name'],
                    'value' => (int)$single['group_process_id']
                ];
            }, $services);
        }

        return new JsonResponse(['options' => $result]);
    }


    private function parseCaseNumber($caseNumber) {

        return intval(preg_replace('/[AT][0]*/', "", $caseNumber));

    }

    /**
     * @Route("/get-actions-of-service/{groupProcessId}/{serviceId}", name="management_supervisor_case_tools_actions_service", options={"expose":true})
     * @Security("is_granted('ROLE_SUPERVISOR')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getActionsOfServiceAction(Request $request, $groupProcessId, $serviceId)
    {

        $response = [];

        if($groupProcessId and $serviceId) {

            $serviceHandler = $this->get('case.service_handler');
            $response['actions'] = $serviceHandler->getServiceActions($groupProcessId, $serviceId);

        }

        return new JsonResponse($response);

    }

    /**
     * @Route("/get-info-from-cdn/{rootProcessId}", name="ms_case_tools_cdn_info", options={"expose":true})
     * @Security("is_granted('ROLE_SUPERVISOR')")
     * @param $rootProcessId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getInfoFromCDNAction($rootProcessId)
    {

        $response = [];

        if($rootProcessId) {

            $serviceHandler = $this->get('case.service_handler');
            $response = $serviceHandler->getCdnInfo($rootProcessId);

        }

        return new JsonResponse($response);

    }

    /**
     * @param $rootId
     * @return string
     */
    private function servicesForChangeAso_getQuery($rootId) {

        return "SELECT a.*, ssd.progress, pdt.name FROM (
                        SELECT 
                        av.group_process_instance_id as group_process_instance_id,
                        av.root_process_instance_id as RootId,
                        ss.status_dictionary_id,
                        CONCAT(dbo.f_partnerNameFull(av.value_int), ' (', av.value_int, ')') as PartnerName,
                        row_number() over (partition by ss.serviceId order by ss.id desc) rowN
                        FROM dbo.attribute_value av
                        LEFT JOIN dbo.service_status as ss ON ss.group_process_id = av.group_process_instance_id
                        WHERE av.attribute_path = '522'
                        AND av.value_int IS NOT NULL
                        AND av.root_process_instance_id = ". intval($rootId) ."
                        ) a
                        LEFT JOIN dbo.service_status_dictionary as ssd ON ssd.id = a.status_dictionary_id
                        LEFT JOIN dbo.process_instance as pi ON pi.id = a.group_process_instance_id
                        LEFT JOIN dbo.step as s ON s.id = pi.step_id
                        LEFT JOIN dbo.process_definition_translation as pdt ON pdt.translatable_id = s.process_definition_id
                        WHERE a.rowN = 1
                        AND pdt.locale = 'pl'";

    }

}
