<?php

namespace ManagementBundle\Controller\Supervisor;

use CaseBundle\Entity\Platform;
use CaseBundle\Entity\PlatformGroup;
use MailboxBundle\Entity\ConnectedMailbox;
use ManagementBundle\Utils\GroupPlatformInstance;
use ManagementBundle\Utils\UserRoleGroupWrapper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method as Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;
use UserBundle\Entity\UserRoleGroup;
use UserBundle\Entity\UserRoleGroupTask;

/**
 * Class VersionLogController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/business-role")
 */
class BusinessRoleController extends Controller
{
    /**
     * @Route("/", name="management_supervisor_business_user_role_index", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $roles = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->findAll();
        /** @var PlatformGroup[] $platformsGroup */
        $platformsGroup = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->findBy([
            'active' => 1
        ]);

        $html = $this->renderView('@Management/Supervisor/BusinessRole/business_role_content.html.twig', [
            'roles' => $roles,
            'platformsGroup' => $platformsGroup
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }

    /**
     * @Route("/get-table", name="management_supervisor_business_user_role_table", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loadTableAction(Request $request)
    {

        $filters = $request->query->get('filters', []);
        $filterRoleId = (!empty($filters['roleIds'])) ? $filters['roleIds'] : [];
        $filterPlatformGroups = (!empty($filters['platformGroupsIds'])) ? $filters['platformGroupsIds'] : [];

        $businessGroups = $this->getBusinessGroups();
        $activePlatforms = $this->getActivePlatformGroup($businessGroups, $filterPlatformGroups);
        $emails = $this->getDoctrine()->getManager()->getRepository(ConnectedMailbox::class)->findAll();
        $roles = $this->initRoles($activePlatforms, $filterRoleId);

        $html = $this->renderView('@Management/Supervisor/BusinessRole/business_role_table.html.twig', [
            'roles' => $roles,
            'activePlatforms' => $activePlatforms,
            'emails' => $emails,
            'amountBusinessGroups' => $businessGroups
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }

    /**
     * @Route("/toggle-group-platform-role", name="management_supervisor_toggle_group_platform_role", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function togglePlatformGroupTaskRoleAction(Request $request)
    {
        $groupPlatformId = $request->request->get('group-platform-id', null);
        $roleId = $request->request->get('role-id', null);
        $checked = $request->request->get('checked', null);

        $groupPlatform = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->find(intval($groupPlatformId));
        $role = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->find(intval($roleId));

        $data = [
            'success' => false
        ];

        if($role && $groupPlatform && $checked !== null) {

            $checked = filter_var($checked, FILTER_VALIDATE_BOOLEAN);

            /** @var Platform[] $platforms */
            $platforms = $groupPlatform->getPlatforms();
            $ids = [];
            foreach ($platforms as $platform) {
                $ids[] = $platform->getId();
            }

            $em = $this->getDoctrine()->getManager();

            if($checked === false) {

                $userGroupTask = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroupTask')->findBy([
                    'userRoleGroup' => $role,
                    'platform' => $ids
                ]);

                foreach ($userGroupTask as $item) {
                    $em->remove($item);
                }

            }
            else {

                /** Najpierw wyczyszczenie całej tabelki - żeby na nowo dodać wszystkie grupy */

                $userGroupTask = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroupTask')->findBy([
                    'userRoleGroup' => $role,
                    'platform' => $ids
                ]);

                foreach ($userGroupTask as $item) {
                    $em->remove($item);
                }

                $em->flush();

                $businessGroups = $this->getBusinessGroups();

                /** Dodawane nowych roli */

                foreach ($platforms as $platform) {

                    foreach ($businessGroups as $businessGroup) {
                        $newUserRoleGroupTask = new UserRoleGroupTask();
                        $newUserRoleGroupTask->setPlatform($platform);
                        $newUserRoleGroupTask->setUserGroup($businessGroup);
                        $newUserRoleGroupTask->setUserRoleGroup($role);
                        $em->persist($newUserRoleGroupTask);
                    }

                    $em->flush();

                }

            }

            $em->flush();

            $data = [
                'success' => true,
                'roleId' => $roleId,
                'groupPlatformId' => $groupPlatformId
            ];

        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/toggle-task-role", name="management_supervisor_toggle_task_role", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toggleTaskRoleAction(Request $request)
    {

        $groupPlatformId = $request->request->get('group-platform-id', null);
        $roleId = $request->request->get('role-id', null);
        $groupId = $request->request->get('group-id', null);
        $checked = $request->request->get('checked', null);

        $groupPlatform = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->find(intval($groupPlatformId));
        $role = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->find(intval($roleId));
        $group = $this->getDoctrine()->getRepository('UserBundle:Group')->find(intval($groupId));

        $data = [
            'success' => false
        ];

        if($role && $groupPlatform && $group && $checked !== null) {
            $checked = filter_var($checked, FILTER_VALIDATE_BOOLEAN);
            /** @var Platform[] $platforms */
            $platforms = $groupPlatform->getPlatforms();

            $em = $this->getDoctrine()->getManager();

            if($checked === false) {

                $ids = [];

                foreach ($platforms as $platform) {
                    $ids[] = $platform->getId();
                }

                $userGroupTask = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroupTask')->findBy([
                    'userRoleGroup' => $role,
                    'platform' => $ids,
                    'userGroup' => $group
                ]);

                foreach ($userGroupTask as $item) {
                    $em->remove($item);
                }

            }
            else {

                /** Dodawane nowych roli */

                foreach ($platforms as $platform) {
                    $newUserRoleGroupTask = new UserRoleGroupTask();
                    $newUserRoleGroupTask->setPlatform($platform);
                    $newUserRoleGroupTask->setUserGroup($group);
                    $newUserRoleGroupTask->setUserRoleGroup($role);
                    $em->persist($newUserRoleGroupTask);
                }

            }

            $em->flush();

            $data = [
                'success' => true,
                'roleId' => $roleId,
                'groupId' => $groupId,
                'groupPlatformId' => $groupPlatformId
            ];

        }

        return new JsonResponse($data);

    }

    /**
     * @Route("/add-user-role-group", name="management_supervisor_add_user_role_group", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addTaskRoleAction(Request $request)
    {

        $data = $request->request->all();

        $response = [
            'success' => false,
        ];

        if(!empty($data['user-role-group-name'])) {

            $userRoleGroup = new UserRoleGroup();
            $userRoleGroup->setName($data['user-role-group-name']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($userRoleGroup);
            $em->flush();

            $response = [
                'success' => true,
            ];
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/copy-user-role-group", name="management_supervisor_copy_user_role_group", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function copyTaskRoleAction(Request $request)
    {
        $userRoleGroupId = $request->request->get('userRoleGroupId', null);

        $response = [
            'success' => false,
        ];

        if($userRoleGroupId) {

            $userRoleGroup = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->find(intval($userRoleGroupId));

            if($userRoleGroup) {

                $em = $this->getDoctrine()->getManager();
                $cloneUserRoleGroup = clone $userRoleGroup;
                $em->persist($cloneUserRoleGroup);
                $em->flush();

                /** @var UserRoleGroupTask $task */
                foreach ($userRoleGroup->getTasks() as $task) {
                    $copyTask = clone $task;
                    $copyTask->setUserRoleGroup($cloneUserRoleGroup);
                    $em->persist($copyTask);
                }

                $em->flush();

                $response = [
                    'success' => true,
                ];

            }

        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/change-name-user-role-group", name="management_supervisor_change_name_user_group_role", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeNameUserGroupRoleAction(Request $request)
    {

        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();
        /** @var UserRoleGroup $entity */
        $userRoleGroup = $em->getRepository('UserBundle:UserRoleGroup')->find(intval($data['pk']));

        if($userRoleGroup && $data['name'] === "name") {
            $userRoleGroup->setName($data['value']);
            $em->persist($userRoleGroup);
            $em->flush();
        }

        return new JsonResponse($data);

    }

    /**
     * @Route("/remove-user-role-group", name="management_supervisor_remove_user_role_group", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeTaskRoleAction(Request $request)
    {

        $data = $request->request->all();

        $response = [
            'success' => false,
        ];

        if(!empty($data['userRoleGroupId'])) {

            $userRoleGroup = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->find(intval($data['userRoleGroupId']));

            if($userRoleGroup) {
                $em = $this->getDoctrine()->getManager();

                /** @var User $user */
                foreach ($userRoleGroup->getUsers() as $user) {
                    $user->setRoleGroup(null);
                    $em->persist($user);
                }

                foreach ($userRoleGroup->getTasks() as $task) {
                    $em->remove($task);
                }

                $userRoleGroup->setName($userRoleGroup->getName() . '_' . (new \DateTime('now'))->format('Y_m_d_H_i_s'));
                $em->persist($userRoleGroup);
                $em->flush();

                $em->remove($userRoleGroup);
                $em->flush();

                $response = [
                    'success' => true,
                ];
            }
        }

        return new JsonResponse($response);

    }

    /**
     * @Route("/change-filtered-emails", name="management_supervisor_change_filtered_emails", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeFilteredEmails(Request $request)
    {

        $data = $request->request->all();

        $response = [
            'success' => false,
        ];

        if(!empty($data['userRoleGroupId'])) {

            $userRoleGroup = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->find(intval($data['userRoleGroupId']));

            if($userRoleGroup) {
                $em = $this->getDoctrine()->getManager();
                $emails = implode (",", $data['emails']);

                if(empty($emails)){
                    $emails = null;
                }

                $userRoleGroup->setTaskGroup($emails);
                $em->persist($userRoleGroup);
                $em->flush();

                $response = [
                    'success' => true,
                ];
            }
        }

        return new JsonResponse($response);

    }

        /**
     * @param $businessGroups
     * @param array $filterPlatformGroups
     * @return GroupPlatformInstance[]
     */
    private function getActivePlatformGroup($businessGroups, $filterPlatformGroups = []) {


        if(!empty($filterPlatformGroups)) {
            /** @var PlatformGroup[] $platformsGroup */
            $platformsGroup = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->findBy([
                'active' => 1,
                'id' => $filterPlatformGroups
            ]);
        }
        else {
            /** @var PlatformGroup[] $platformsGroup */
            $platformsGroup = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->findBy([
                'active' => 1
            ]);
        }

        $activePlatformGroupInstance = [];

        foreach ($platformsGroup as $platformGroup) {

            $platformGroupInstance = new GroupPlatformInstance($platformGroup);
            $platformGroupInstance->initGroups($businessGroups);
            $activePlatformGroupInstance[] = $platformGroupInstance;

        }

        return $activePlatformGroupInstance;

    }

    private function getBusinessGroups() {

        /** @var Group[] $platformsGroup */
        return $this->getDoctrine()->getRepository('UserBundle:Group')->findBy([
            'type' => 1
        ]);

    }

    /**
     * @param GroupPlatformInstance[] $activePlatformGroupInstance
     * @param array $filterRoleId
     * @return array
     */
    private function initRoles($activePlatformGroupInstance, $filterRoleId = [])
    {

        $roles = [];

        if(!empty($filterRoleId)) {
            /** @var UserRoleGroup[] $platformsGroup */
            $userRoleGroups = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->findBy([
                'id' => $filterRoleId
            ]);
        }
        else {
            /** @var UserRoleGroup[] $platformsGroup */
            $userRoleGroups = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->findBy([
//            'active' => 1
            ]);
        }

        foreach ($userRoleGroups as $userRoleGroup) {

            $userRoleGroupWrapper = new UserRoleGroupWrapper($userRoleGroup);
            $userRoleGroupWrapper->initData($activePlatformGroupInstance, clone($userRoleGroup->getTasks()));
            $userRoleGroupWrapper->recalculateGroups();
            $roles[] = $userRoleGroupWrapper;

        }

        return $roles;
    }
}
