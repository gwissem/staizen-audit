<?php

namespace ManagementBundle\Controller\Supervisor;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController
 * @package ManagementBundle\Controller
 * @Route("/supervisor")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="management_supervisor_panel")
     * @Security("is_granted('ROLE_SUPERVISOR')")
     */
    public function indexAction()
    {
        return $this->render('ManagementBundle:Supervisor:index.html.twig');
    }

}
