<?php

namespace ManagementBundle\Controller\Supervisor;

use AppBundle\Entity\Config;
use JabberBundle\Controller\ApiProfileController;
use ManagementBundle\Utils\ManagementRoleHierarchy;
use MssqlBundle\PDO\PDO;
use SocketBundle\Topic\TaskManagerTopic;
use SocketBundle\Utils\CurrentTaskRedisValue;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method as Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\TelephonyAgentStatus;
use UserBundle\Entity\User;
use UserBundle\Entity\UserRoleGroup;

/**
 * Class ManagementUserController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/management-user")
 */
class ManagementUserController extends Controller
{
    /**
     * @Route("/", name="management_supervisor_user_index", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $roles = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->findBy([
            'active' => 1
        ]);

        $platformGroups = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->findBy(['active' => true]);

        $pushingTemplates = $this->getDoctrine()->getRepository('CaseBundle:PushingTemplate')->findAll();

        $activeTemplate = $this->getDoctrine()->getRepository('AppBundle:Config')
            ->findOneBy([
                'key' => Config::PUSHING_TEMPLATE_ID_KEY
            ]);

        $html = $this->renderView('@Management/Supervisor/ManagementUser/management_user_content.html.twig', [
            'roles' => $roles,
            'managementRoles' => User::getManagementRoles(),
            'platformGroups' => $platformGroups,
            'pushingTemplates' => $pushingTemplates,
            'activeTemplateId' => $activeTemplate->getValue()
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }

    /**
     * @Route("/get-users-table", name="management_supervisor_user_table", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userTableAction(Request $request)
    {

        $onlyOnline = filter_var($request->query->get('online', false), FILTER_VALIDATE_BOOLEAN);
        $phrase = $request->query->get('phrase', false);
        $phrase = (empty($phrase)) ? false : $phrase;
        $rolesId = $request->query->get('roles', []);

        $usersQuery = $this->getDoctrine()->getRepository('UserBundle:User')->getQueryOfAllUser(
            $phrase, 33, false, $onlyOnline, $rolesId, false);

        /** @var User[] $users */
        $users = $usersQuery->getResult();

        $this->setAgentStatuses($users);
//        $this->setActiveTasks($users);

        if($onlyOnline) {
            $this->setTimeWorkUsers($users);
        }

        $this->setCurrentTasks($users);

        $roleHierarchy = new ManagementRoleHierarchy($this->getParameter('security.role_hierarchy.roles'));

        $roles = $this->getDoctrine()->getRepository('UserBundle:UserRoleGroup')->findAll();
        $managementsRoles = User::getManagementRoles();

        $userGroups = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy(['type' => 1]);

        $platformGroup = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->findBy(['active' => true]);

        $html = $this->renderView('@Management/Supervisor/ManagementUser/management_user_tbody.html.twig', [
            'users' => $users,
            'roles' => $roles,
            'managementRoles' => $managementsRoles,
            'roleHierarchy' => $roleHierarchy,
            'userGroups' => $userGroups,
            'platformGroups' => $platformGroup,
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }

    /**
     * @param User[] $users
     */
    private function setCurrentTasks(&$users) {

        $redisTasks = $this->get('snc_redis.default')->hgetall(TaskManagerTopic::REDIS_KEY_CURRENT_TASKS);

        /** @var CurrentTaskRedisValue[] $tasks */
        $tasks = [];

        foreach ($redisTasks as $key => $redisTask) {
            $tasks[] = unserialize($redisTask);
        }

        foreach ($users as $user) {
            if($user->isOnline()) {
                foreach ($tasks as $key => $task) {
                    if($task->userId === $user->getId()) {
                        $user->currentTasks[] = $task;
                    }
                }

                if(empty($user->currentTasks)) {

                    $timestamp = $this->get('snc_redis.default')->get(TaskManagerTopic::REDIS_KEY_DATE_LAST_LEAVE_TASK . $user->getId());

                    if($timestamp) {
                        $user->lastTaskTime = (new \DateTime())->getTimestamp() - intval($timestamp);
                    }

                }
            }
        }

    }

    /**
     * @param User[] $users
     */
    private function setTimeWorkUsers(&$users) {

        foreach ($users as $user) {
            if($user->isOnline()) {

                $timeStart = $this->getTimeWork($user->getId());

                if($timeStart !== null ) {
                    $timeStart = new \DateTime($timeStart);
                    $user->timeWork = (new \DateTime())->getTimestamp() - $timeStart->getTimestamp();
                }

            }
        }
    }

    private function getTimeWork($userId) {

        $parameters = [
            [
                'key' => 'userId',
                'value' => intval($userId),
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->get('app.query_manager')->executeProcedure(
            "SELECT dbo.f_workTime(:userId) as time",
            $parameters
        );

        if(!empty($result)) {
            if(isset($result[0]['time'])) {
                return $result[0]['time'];
            }
        }

        return null;
    }

    private function setActiveTasks(&$users) {

        $apiTask = $this->get('api_task.service');
        $processHandler = $this->get('case.process_handler');

        $activeTasks = $apiTask->getActiveTasks();

        /** @var User $user */
        foreach ($users as $user) {

            if(!$user->isOnline()) continue;

            $tasks = $apiTask->getActiveTasksOfUser($user->getId(), 'array', $activeTasks);
            $user->activeTasks = [];

            foreach ($tasks as $task) {
                $user->activeTasks[] = $processHandler->taskNameOfProcessInstance($task['process_instance_id']);
            }

        }

    }

    private function setAgentStatuses(&$users) {

        /** @var TelephonyAgentStatus[] $statuses */
        $statuses = $this->getDoctrine()->getRepository('UserBundle:TelephonyAgentStatus')->fetchAsArray();

        $supportedStatuses = $this->getSupportedStatus();

        /** @var User $user */
        foreach ($users as $user) {

            if(!$user->isOnline()) continue;

            $username = $user->getUsername();

            if(array_key_exists($username, $statuses)) {

                if($statuses[$username]['code'] !== null && array_key_exists($statuses[$username]['code'], $supportedStatuses)) {
                    $user->agentLabel = $supportedStatuses[$statuses[$username]['code']]->label;
                    $user->agentState = $statuses[$username]['state'];
                }
                else {
                    $user->agentState = $statuses[$username]['state'];
                }
                $user->stateChangeTime = $statuses[$username]['time'];
            }

        }

    }

    private function getSupportedStatus()
    {

        $jsonStatuses = '{"statuses":[{"type":"READY","label":"Dost\u0119pny","icon":"fa-check-circle","idCode":0,"isActive":false},{"type":"NOT_READY","label":"Nie gotowy - Short Break 5 min","icon":"fa-times-circle","idCode":"1","isActive":false},{"type":"NOT_READY","label":"Nie gotowy - Long Break 15 min","icon":"fa-times-circle","idCode":"2","isActive":false},{"type":"NOT_READY","label":"Nie gotowy - Zadanie Specjalne","icon":"fa-times-circle","idCode":"3","isActive":false},{"type":"NOT_READY","label":"Nie gotowy - Spotkanie poza CC","icon":"fa-times-circle","idCode":"4","isActive":false},{"type":"NOT_READY","label":"Nie gotowy - Praca w Aplikacji","icon":"fa-times-circle","idCode":"5","isActive":false},{"type":"NOT_READY","label":"Nie gotowy - Second account in use","icon":"fa-times-circle","idCode":"6","isActive":false}]}';

        $supportedStatusesArray =  json_decode($jsonStatuses);
        $supportedStatuses = [];

        foreach ($supportedStatusesArray->statuses as $item) {
            $supportedStatuses[$item->idCode] = $item;
        }

        return $supportedStatuses;

    }

    /**
     * @Route("/set-user-role-group/{userId}/{roleId}", defaults={"roleId" = 0}, name="management_supervisor_set_user_role_group", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param $userId
     * @param $roleId
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function setUserRoleGroupAction($userId, $roleId)
    {

        $response = [
            'success' => false
        ];

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository('UserBundle:User')->find(intval($userId));

        if($roleId === 0) {

            $user->setRoleGroup(null);
            $em->persist($user);
            $em->flush();

            $response = [
                'success' => true
            ];

        }
        else {
            /** @var UserRoleGroup $entity */
            $userRoleGroup = $em->getRepository('UserBundle:UserRoleGroup')->find(intval($roleId));

            if($userRoleGroup && $user) {

                $user->setRoleGroup($userRoleGroup);
                $em->persist($user);
                $em->flush();

                $response = [
                    'success' => true
                ];
            }
        }

        if($response['success'] === true ) {
            $this->get('case.process_handler')->syncUserGroups($user->getId());
        }

        return new JsonResponse($response);

    }

    /**
     * @Route("/set-user-app-role", name="management_supervisor_set_user_app_role", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function setUserAppRoleAction(Request $request)
    {

        $userId = $request->request->get('userId', null);
        $roleName = $request->request->get('value', null);
        $checked = $request->request->get('checked', null);

        $response = [
            'success' => false
        ];

        try {

            $em = $this->getDoctrine()->getManager();

            /** @var User $user */
            $user = $em->getRepository('UserBundle:User')->find(intval($userId));
            $availableRoles = User::getManagementRoles();

            if($user && isset($availableRoles[$roleName]) && $checked !== null ) {

                if(!$user->isRoleInGroup($roleName)) {
                    $checked = filter_var($checked, FILTER_VALIDATE_BOOLEAN);

                    if($checked) {
                        $user->addRole($roleName);
                    }
                    else {
                        $user->removeRole($roleName);
                    }

                    $em->persist($user);
                    $em->flush();

                    $response = [
                        'success' => true
                    ];
                }

            }

        }
        catch (\Exception $exception) {

        }

        return new JsonResponse($response);

    }

    /**
     * @Route("/set-user-platform", name="management_supervisor_set_user_platform", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function setUserPlatformAction(Request $request)
    {
        $userId = $request->request->get('userId');
        $inputChecked = ($request->request->get('checked') == 'true');
        $platformId = $request->request->get('value');
        $all = ($request->request->get('all') == 'true');

        $response = [
            'success' => false
        ];

        try {
            $em = $this->getDoctrine()->getManager();
            $platforms = [];

            $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($userId);

            if(false === empty($platformId)) {
                $platforms = $this->getDoctrine()->getRepository('CaseBundle:Platform')->findBy(['id' => $platformId]);
            } elseif (true === $all && false === $inputChecked) {
                $platforms = $this->getDoctrine()->getRepository('CaseBundle:PlatformGroup')->findPlatformForActiveGroups();
            }

            foreach ($platforms as $platform) {
                if (true === $inputChecked) {
                    $user->setPlatforms($platform);
                } else {
                    $user->removePlatforms($platform);
                }
            }

            $em->persist($user);

            $em->flush();

            $response = [
                'success' => true
            ];

        } catch (\Exception $exception) {

        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/set-user-group", name="management_supervisor_set_user_group", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function setUserGroupAction(Request $request)
    {
        $userId = $request->request->get('userId');
        $inputChecked = ($request->request->get('checked') == 'true');
        $groupId = $request->request->get('value');

        $response = [
            'success' => false
        ];

        try {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($userId);
            $group = $this->getDoctrine()->getRepository('UserBundle:Group')->find($groupId);

            if (true === $inputChecked) {
                $user->setGroups($group);
            } else {
                $user->removeGroup($group);
            }

            $em->persist($user);

            $em->flush();

            $response = [
                'success' => true
            ];

        } catch (\Exception $exception) {

        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/change-active-template", name="management_supervisor_change_active_template", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function changeActiveTemplate(Request $request)
    {
        $templateId = $request->request->get('templateId');

        $templateConfig = $this->getDoctrine()->getRepository('AppBundle:Config')
            ->findOneBy([
                'key' => Config::PUSHING_TEMPLATE_ID_KEY
            ]);

        $response = [
            'success' => false
        ];

        try {
            $em = $this->getDoctrine()->getManager();

            if($templateConfig) {
                $templateConfig->setValue($templateId);
                $em->persist($templateConfig);
                $em->flush();

                $response = [
                    'success' => true
                ];
            }

        } catch (\Exception $exception) {

        }

        return new JsonResponse($response);
    }

}
