<?php


namespace ManagementBundle\Controller\Supervisor;


use AppBundle\Utils\QueryManager;
use MssqlBundle\PDO\PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;

class PushingReportController extends Controller
{
    /** @var QueryManager */
    protected $queryManager;

    /**
     * @Route("/", name="management_supervisor_pushing_report_index", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $this->queryManager = $this->container->get('app.query_manager');

        $groups = $this->getDoctrine()->getRepository('UserBundle:Group')->fetchIdGroupNameArray();

        $parameters = [
            [
                'key' => 'startDate',
                'value' => new \DateTime('-1 week'),
                'type' => PDO::PARAM_INT
             ]
        ];

        $data = $this->queryManager->executeProcedure(
            'EXEC dbo.p_agent_tasks_statistics'
        );

        $html = $this->renderView('ManagementBundle:Supervisor/PushingReport:content.html.twig', [
            'groupsName' => $groups,
            'data' => $data
        ]);

        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @Route("/user/{id}", name="management_supervisor_pushing_report_user", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userStatsAction($id)
    {
        $this->queryManager = $this->container->get('app.query_manager');

        $groupsName = $this->getDoctrine()->getRepository('UserBundle:Group')->fetchIdGroupNameArray(true);

        $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($id);

        $params = [
            [
                'key' => 'userId',
                'value' => $id,
                'type' => PDO::PARAM_INT
            ]
        ];

        $data = $this->queryManager->executeProcedure(
            'EXEC dbo.p_agent_tasks_statistics @userId = :userId',
            $params
        );


        $html = $this->renderView('ManagementBundle:Supervisor/PushingReport:user-content.html.twig', [
            'groupsName' => $groupsName,
            'usersData' => $data,
            'user' => $user
        ]);

        return new JsonResponse([
           'html' => $html
        ]);
    }
}