<?php

namespace ManagementBundle\Controller\Supervisor;

use FOS\RestBundle\Controller\Annotations as Rest;
use JabberBundle\Controller\ApiProfileController;
use ManagementBundle\Utils\ManagementRoleHierarchy;
use MssqlBundle\PDO\PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SocketBundle\Topic\TaskManagerTopic;
use SocketBundle\Utils\CurrentTaskRedisValue;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method as Method;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\TelephonyAgentStatus;
use UserBundle\Entity\User;
use UserBundle\Entity\UserRoleGroup;

/**
 * Class TelephonyStatisticsController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/telephony-statistics")
 */
class TelephonyStatisticsController extends Controller
{
    /**
     * @Route("/", name="management_supervisor_telephony_statistics_index", options={"expose":true})
     * @Security("is_granted('ROLE_PHONE_STATISTIC')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $requestTabs = $this->get('app.query_manager')->executeProcedure('select distinct tabName from dbo.statOperators');

        /** @var User $user */
        $user = $this->getUser();

        $isViaVox = ($user->hasGroup(3502));

        $tabs = [];

        foreach ($requestTabs as $tab) {

            if($isViaVox && $tab['tabName'] != "ViaVox") continue;

            $tabs[] = $tab['tabName'];

        }

        if(empty($tabs)) {
            return new JsonResponse([
                'html' => '<p>Nie masz uprawnień do żadnych statystyk telefonii</p>'
            ]);
        }

        $activeTab = $tabs[0];

        $html = $this->renderView('@Management/Supervisor/TelephonyStatistics/content.html.twig', [
            'tabs'=>$tabs,
            'activeTab' =>$activeTab
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }


    /**
     * @Route("/telephony-stats-tab-content/{tab}/{update}", name="telephony-stats-tab-content", options={"expose":true})
     * @Security("is_granted('ROLE_PHONE_STATISTIC')")
     * @Method({"GET"})
     * @param $tab
     * @param bool $update
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function telephonyStatsTabContent( $tab, $update = false){

        $statOperators = $this->getStatOperators($tab);
        $statQueues = $this->getStatQueues($tab);
        $statPersons = $this->getStatPersons($tab);
        foreach ($statPersons as $index =>$statPerson) {
            $statPersons[$index]['slug'] = str_replace(' ','_',$statPerson['name']);
        }

        $params = [
            'operators'=>$statOperators,
            'queues'=>$statQueues,
            'persons'=>$statPersons,
            'tabname' =>$tab,
            'update' => $update
        ];

        $tabContent = $this->get('twig')->render('@Management/Supervisor/TelephonyStatistics/tab_ajax.html.twig', $params);
        $response = new JsonResponse(['html'=>$tabContent]);

        return $response;
    }



    private function getStatOperators($tabName){
       return $this->get('app.query_manager')
            ->executeProcedure('SELECT * FROM dbo.statOperators with(nolock) where tabName=:tabname order by name',
                [
                    [
                        'key' => 'tabname',
                        'value' => $tabName,
                        'type' => PDO::PARAM_STR
                    ]
                ]
            );
    }
    private function getStatQueues($tabName){
        return  $this->get('app.query_manager')
            ->executeProcedure('SELECT * FROM dbo.statQueues with(nolock) where tabName=:tabname',
                [
                    [
                        'key' => 'tabname',
                        'value' => $tabName,
                        'type' => PDO::PARAM_STR
                    ]
                ]
            );
    }
    private function getStatPersons($tabName){
        return $this->get('app.query_manager')
            ->executeProcedure('SELECT * FROM dbo.statPersons with(nolock) where tabName=:tabname',
                [
                    [
                        'key' => 'tabname',
                        'value' => $tabName,
                        'type' => PDO::PARAM_STR
                    ]
                ]
            );
    }
}
