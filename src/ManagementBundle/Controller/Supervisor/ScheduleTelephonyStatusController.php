<?php

namespace ManagementBundle\Controller\Supervisor;

use ManagementBundle\Entity\ScheduleTelephonyStatus;
use ManagementBundle\Utils\ManagementScheduleHourRow;
use ManagementBundle\Utils\ScheduleHour;
use SocketBundle\Topic\AtlasTelephony;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Class ManagementUserController
 * @package ManagementBundle\Controller
 * @Route("/supervisor/schedule-telephony-status")
 */
class ScheduleTelephonyStatusController extends Controller
{

    const PER_PAGE = 20;

    /** @var ScheduleHour[] */
    private $availableHours = [];

    /**
     * @Route("/", name="management_sts_index", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $html = $this->renderView('@Management/Supervisor/ScheduleTelephonyStatus/_content.html.twig', [
            'availableHours' => $this->availableHours()
        ]);

        return new JsonResponse([
            'html' => $html
        ]);

    }

    static function _availableHours()
    {
        return ['0-6',
            '6-7',
            '7-8',
            '8-9',
            '9-10',
            '10-11',
            '11-12',
            '12-13',
            '13-14',
            '14-15',
            '15-16',
            '16-17',
            '17-18',
            '18-19',
            '19-20',
            '20-0'];
    }

    private function availableHours()
    {

        if (empty($this->availableHours)) {

            foreach (self::_availableHours() as $availableHour) {
                $this->availableHours[] = new ScheduleHour($availableHour);
            }


        }

        return $this->availableHours;

    }

    /**
     * @Route("/get-table/{page}", name="management_sts_table", requirements={"page" = "\d+"}, options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function tableAction(Request $request, $page = 1)
    {

        $onlyOnline = filter_var($request->query->get('online', false), FILTER_VALIDATE_BOOLEAN);
        $username = $request->query->get('username', null);
        $date = $request->query->get('date', null);

        $date = new \DateTime($date);

        $usersQuery = $this->getDoctrine()->getRepository('UserBundle:User')->getQueryOfAllUser(
            $username, 33, false, $onlyOnline, [], false);

        $pagination = $this->get('knp_paginator')->paginate($usersQuery, $page, self::PER_PAGE);

        $scheduleTelephonyStatusQuery = $this->getDoctrine()->getRepository('ManagementBundle:ScheduleTelephonyStatus')->getQueryOfAllSchedule($date);

        /** @var ScheduleTelephonyStatus[] $users */
        $scheduleTelephonyStatus = $scheduleTelephonyStatusQuery->getResult();

        $rows = [];

        /** @var User $user */
        foreach ($pagination as $user) {

            $userStatuses = array_filter($scheduleTelephonyStatus, function ($a) use ($user) {
                /** @var ScheduleTelephonyStatus $a */
                return ($a->getUser()->getId() === $user->getId());
            });

            $rows[] = new ManagementScheduleHourRow($user, $userStatuses);

        }

        $html = $this->renderView('@Management/Supervisor/ScheduleTelephonyStatus/_table_body.html.twig', [
            'rows' => $rows,
            'availableHours' => self::_availableHours()
        ]);

        $paginationView = $this->renderView('@Management/Supervisor/ScheduleTelephonyStatus/_pagination.html.twig', [
            'pagination' => $pagination
        ]);

        return new JsonResponse([
            'html' => $html,
            'pagination' => $paginationView
        ]);

    }

    /**
     * @Route("/save-hour", name="management_sts_save_hour", options={"expose":true})
     * @Security("is_granted('ROLE_KZ')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function saveHour(Request $request)
    {

        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('ManagementBundle:ScheduleTelephonyStatus');
        $result = [];

        if ($data['action'] === "add") {

            if ($data['type'] === "single") {

                $term = $this->createScheduleTelephonyStatus($data['date'], $data['hour'], $data['user_id']);
                $em->persist($term);
                $this->addToResult($result, $data['user_id'], $term);

            }
            elseif ($data['type'] === "multi") {

                foreach ($data['hours'] as $hour) {

                    $term = $this->createScheduleTelephonyStatus($data['date'], $hour, $data['user_id']);
                    $em->persist($term);
                    $this->addToResult($result, $data['user_id'], $term);

                }

            }

        }
        elseif ($data['action'] === "remove") {

            if ($data['type'] === "single") {

                $entity = $repo->find($data['id']);

                if($entity) {
                    $this->addToResult($result, $data['user_id'], $entity->getLabel());
                    $em->remove($entity);
                }

            }
            elseif ($data['type'] === "multi") {

                foreach ($data['ids'] as $id) {

                    $entity = $repo->find($id);

                    if($entity) {
                        $this->addToResult($result, $data['user_id'], $entity->getLabel());
                        $em->remove($entity);
                    }

                }

            }

        }

        $em->flush();

        $nowLabel = null;

        if(!empty($data['user_id'])) {
            $datetime = new \DateTime($data['date']);
            if((new \DateTime())->format('Ymd') === $datetime->format('Ymd')) {
                $scheduleHour = ScheduleHour::createFromDate(new \DateTime());
                $nowLabel = $scheduleHour->label;
            }
        }

        $response = [];

        foreach ($result as $userId => $items) {

            foreach ($items as $term) {
                $this->addToResponse($response, $userId, $term);
            }

        }

        $mustRefreshStatus = false;

        if($nowLabel !== null && !empty($response)) {

            foreach ($response[$data['user_id']] as $item) {

                if($item['hour'] === $nowLabel) {
                    $mustRefreshStatus = true;
                    break;
                }

            }

        }

        /** Jeżeli wymaga odświeżenia statusu użytkownikowi, to robi pusha (webSocket) do niego */
        if($mustRefreshStatus) {

            $data = [
                'users' => [$data['user_id']],
                'action' => AtlasTelephony::PUSH_ACTION_REFRESH_STATUSES
            ];

            $this->get('gos_web_socket.zmq.pusher')->push($data, 'atlas_telephony');

        }

        return new JsonResponse([
            'users' => $response
        ]);

    }

    /**
     * @param $response
     * @param $userId
     * @param ScheduleTelephonyStatus $term
     */
    private function addToResponse(&$response, $userId, $term = null) {

        if(!isset($response[$userId])) {
            $response[$userId] = [];
        }

        if($term instanceof ScheduleTelephonyStatus ) {
            $response[$userId][] = [
                'hour' => $term->getLabel(),
                'id' => $term->getId()
            ];
        }
        else {
            $response[$userId][] = $term;
        }

    }

    /**
     * @param $result
     * @param $userId
     * @param ScheduleTelephonyStatus $term
     */
    private function addToResult(&$result, $userId, $term) {

        if(!isset($result[$userId])) {
            $result[$userId] = [];
        }

        if($term instanceof ScheduleTelephonyStatus ) {
            $result[$userId][] = $term;
        }
        else {
            $result[$userId][] = [
                'hour' => $term,
                'id' => null
            ];
        }

    }

    /**
     * @param $date
     * @param $hour
     * @param $userId
     * @return ScheduleTelephonyStatus
     * @throws \Exception
     */
    private function createScheduleTelephonyStatus($date, $hour, $userId) {

        $date = new \DateTime($date);
        $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($userId);
        $scheduleHour = new ScheduleHour($hour,  null, $date);

        $scheduleTelephonyStatus = new ScheduleTelephonyStatus();
        $scheduleTelephonyStatus->setUser($user);
        $scheduleTelephonyStatus->setLabel($scheduleHour->label);
        $scheduleTelephonyStatus->setStart($scheduleHour->from);
        $scheduleTelephonyStatus->setEnd($scheduleHour->to);

        return $scheduleTelephonyStatus;

    }

    /**
     * @Route("/check-status/{datetime}", name="management_sts_check_status", options={"expose":true})
     * @Security("is_granted('ROLE_USER')")
     * @param null $datetime
     * @return JsonResponse
     * @throws \Exception
     */
    public function checkScheduleStatus($datetime = null)
    {

        return new JsonResponse([
            'hasSchedule' => $this->get('management.schedule_telephony_status.service')->hasTerm($this->getUser(), $datetime)
        ]);

    }

}
