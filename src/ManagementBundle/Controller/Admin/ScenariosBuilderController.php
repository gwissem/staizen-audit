<?php

namespace ManagementBundle\Controller\Admin;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\FormControl;
use Doctrine\Common\Collections\Collection;
use FOS\RestBundle\Controller\Annotations as Rest;
use ManagementBundle\Entity\Scenario;
use ManagementBundle\Entity\ScenarioInstance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method as Method;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ScenariosBuilderController
 * @package ManagementBundle\Controller
 * @Route("/admin/scenarios-builder")
 */
class ScenariosBuilderController extends Controller
{

    /**
     * @Route("/", name="management_admin_panel_scenarios_builder_index",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function scenarioBuilderAction(Request $request)
    {

        $content = $this->renderView('@Management/Admin/ScenariosBuilder/content.html.twig');

        return new JsonResponse([
            'content' => $content
        ]);

    }

    /**
     * @Route("/load-scenario-creator", name="management_admin_panel_scenarios_builder_creator_template",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getScenarioCreatorTemplateAction(Request $request)
    {

        $scenario = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->find(intval($request->query->get('id')));

        $content = $this->renderView('@Management/Admin/ScenariosBuilder/scenario_creator_template.html.twig', [
            'scenario' => $scenario
        ]);

        return new JsonResponse([
            'content' => $content
        ]);

    }

    /**
     * @Route("/get-scenario/{id}", name="management_admin_panel_scenarios_builder_get_scenario",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getScenarioAction(Request $request, $id)
    {

        $scenario = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->find(intval($id));

        if($scenario) {
            return new JsonResponse([
                'success' => true,
                'scenario' => $scenario->toArray()
            ]);
        }

        return new JsonResponse([
            'success' => false,
            'message' => "Nie znaleziono scenariusza"
        ], 404);


    }

    /**
     * @Route("/load-available-scenarios-list", name="management_admin_panel_scenarios_builder_available_scenarios",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAvailableScenariosAction(Request $request)
    {

        $frontMode = filter_var($request->query->get('front-mode', null), FILTER_VALIDATE_BOOLEAN);

        /** @var Scenario[] $scenarios */
        $scenarios = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->findBy([],[
            'createdAt' => 'DESC'
        ]);

        $list = $this->renderView('@Management/Admin/ScenariosBuilder/scenarios_list.html.twig', [
            'scenarios' => $scenarios,
            'frontMode' => $frontMode
        ]);

        return new JsonResponse([
            'list' => $list
        ]);

    }

    /**
     * @Rest\Route("loads-historical-scenario-instances", name="management_admin_panel_scenarios_builder_historical_instances", options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     * */
    public function getInstancesHistory(Request $request){

        $scenarios = $this->getDoctrine()->getRepository('ManagementBundle:ScenarioInstance')->findBy([],[
            'createdAt' => 'DESC'
        ], 50);

        $list = $this->renderView('@Management/Admin/ScenariosBuilder/scenarios_instances_list.html.twig', [
            'scenarios' => $scenarios
        ]);

        return new JsonResponse([
            'list' => $list
        ]);
    }

    /**
     * @Rest\Route("/remove-scenario-instance/{id}", name="removeScenarioInstance", options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param int $id
     * @return JsonResponse
     * */
    public function removeScenarioInstance($id){


       $instance =  $this->getDoctrine()->getRepository(ScenarioInstance::class)->find($id);
       $em = $this->getDoctrine()->getManager();
       $em->remove($instance);
       $em->flush();


       return new JsonResponse([
          'status' =>1
       ]);
    }

    /**
     * @Route("/find-scenarios-by-name", name="management_admin_panel_scenarios_builder_find_scenarios_by_name",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findScenariosByNameAction(Request $request)
    {
        $name  = $request->get('name') ?? null;
        $frontMode = 1;
        $list = '';
        if($name) {

            $scenarios = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->createQueryBuilder('scenario')
                ->where('LOWER(scenario.name) like LOWER(:name)')
                ->orWhere('LOWER(scenario.description) like LOWER(:name)')
                ->setParameter('name', '%'.$name.'%')
                ->orderBy('scenario.name')
                ->getQuery()
                ->getResult();

            $list = $this->renderView('@Management/Admin/ScenariosBuilder/scenarios_list.html.twig', [
                'scenarios' => $scenarios,
                'frontMode' => $frontMode
            ]);
        }

        /** @var Scenario[] $scenarios */

        return new JsonResponse([
            'list' => $list
        ]);

    }





    /**
     * @Route("/find-scenarios/{id}", name="management_admin_panel_scenarios_builder_find_scenarios", defaults={"id"=0},  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function findScenariosAction(Request $request, $id)
    {

        if($id) {

            $scenario = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->find(intval($id));

            return new JsonResponse([
                'text' => $scenario->getName(),
                'id' => $scenario->getId()
            ]);

        }

        $result = [];

        $term = $request->query->get('term', '');

        if(!empty($term)) {

            /** @var Scenario[] $scenarios */
            $scenarios = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->findScenarios($term);

            foreach ($scenarios as $scenario) {

                $result[] = [
                    'text' => $scenario->getName(),
                    'id' => $scenario->getId()
                ];

            }

        }

        return new JsonResponse([
            "results" => $result
        ]);

    }

    /**
     * @Route("/find-platform/{id}", name="management_admin_panel_scenarios_builder_find_platform", defaults={"id"=0}, options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function findPlatformAction(Request $request, $id)
    {

        if($id) {

            $scenario = $this->getDoctrine()->getRepository('CaseBundle:Platform')->find(intval($id));

            return new JsonResponse([
                'text' => $scenario->getName(),
                'id' => $scenario->getId()
            ]);

        }

        $result = [];

        $term = $request->query->get('term', '');

        if(!empty($term)) {

            /** @var Scenario[] $scenarios */
            $scenarios = $this->getDoctrine()->getRepository('CaseBundle:Platform')->findPlatform($term);

            foreach ($scenarios as $scenario) {

                $result[] = [
                    'text' => $scenario->getName(),
                    'id' => $scenario->getId()
                ];

            }

        }

        return new JsonResponse([
            "results" => $result
        ]);

    }

    /**
     * @Route("/save-scenario", name="management_admin_panel_scenarios_builder_save",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function saveScenarioAction(Request $request)
    {

        $scenarioArray = $request->request->all();

        if($scenarioArray) {

            $scenario = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->find(intval($scenarioArray['id']));

            if($scenario) {

                $em = $this->getDoctrine()->getManager();

//                $copyScenario = clone $scenario;
//                $em->remove($scenario);
//
//                $copyScenario->setName($scenarioArray['name']);
//                $copyScenario->setDescription($scenarioArray['description']);
//                $copyScenario->setConfig(json_encode($scenarioArray['config']));


                $scenario->setName($scenarioArray['name']);
                $scenario->setDescription($scenarioArray['description']);
                $scenario->setConfig(json_encode($scenarioArray['config']));
                $scenario->setVersion($scenario->getVersion() + 1);

                $em->persist($scenario);

                $em->flush();

                return new JsonResponse([
                    'success' => true,
                    'scenario' => $scenario->toArray()
                ]);
            }

        }

        return new JsonResponse([
            'success' => false
        ]);

    }

    /**
     * @Route("/copy-scenario", name="management_admin_panel_scenarios_builder_copy",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function copyScenarioAction(Request $request)
    {

        $id = $request->request->get('id', null);

        if($id) {

            $scenario = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->find(intval($id));

            if($scenario) {

                $em = $this->getDoctrine()->getManager();

                $newScenario = new Scenario();
                $newScenario->setConfig($scenario->getConfig());
                $newScenario->setVersion(1);
                $newScenario->setDescription($scenario->getDescription());
                $newScenario->setName('Copy ' . $scenario->getName());

                $em->persist($newScenario);

                $em->flush();

                return new JsonResponse([
                    'success' => true,
                    'scenario' => $newScenario->toArray()
                ]);
            }

        }

        return new JsonResponse([
            'success' => false
        ]);

    }

    /**
     * @Route("/remove-scenario/{id}", name="management_admin_panel_scenarios_builder_remove",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function removeScenarioAction(Request $request, $id)
    {

        $scenario = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->find(intval($id));

        if($scenario) {

            $scenarioInstances = $this->getDoctrine()->getRepository('ManagementBundle:ScenarioInstance')->findBy([
                'scenario' => $scenario
            ]);

            $em = $this->getDoctrine()->getManager();

            foreach ($scenarioInstances as $scenarioInstance) {
                $em->remove($scenarioInstance);
            }

            $em->remove($scenario);
            $em->flush();

            return new JsonResponse([
                'success' => true
            ]);
        }

        return new JsonResponse([
            'success' => false
        ]);

    }

    /**
     * @Route("/create-scenario", name="management_admin_panel_scenarios_builder_create",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function createScenarioAction(Request $request)
    {

        $scenarioConfig = $request->request->get('config', []);
        $scenarioName = $request->request->get('name', 'Nowy scenariusz');

        $scenario = new Scenario();

        $em = $this->getDoctrine()->getManager();

        $scenario->setName($scenarioName);
        $scenario->setConfig(json_encode($scenarioConfig));

        $em->persist($scenario);
        $em->flush();

        return new JsonResponse([
            'success' => true,
            'id' => $scenario->getId(),
            'scenario' => $scenario->toArray()
        ]);

    }

    /**
     * @Route("/controls", name="management_admin_panel_scenarios_builder_controls",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllFormControlsAction(Request $request)
    {

        // TODO - ZAPYTANIE, KTÓRE ZWRACA TYLKO PATH TYCH KONTROLEK  - DISTINCT!

        /** @var FormControl[] $formControls */
        $formControls = $this->getDoctrine()->getRepository('CaseBundle:FormControl')->findAllWithAttributePath();

        $distinctPaths = [];

        foreach ($formControls as $formControl) {

            $path = $formControl->getAttributePath();

            if(!in_array($path, $distinctPaths)) {
                $distinctPaths[] = $path;
            }

        }

        $paths = [];

        foreach ($distinctPaths as $path) {

            $paths[] = [
                'label' => $path,
                'value' => $path
            ];

        }

        return new JsonResponse([
            'paths' => $paths
        ]);

    }


    /**
     * @Route("/check-attribute-path/{path}", name="management_admin_panel_scenarios_builder_check_path",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function checkAttributePathAction(Request $request, $path)
    {

        // TODO -

        /** @var FormControl[] $formControls */
        $formControls = $this->getDoctrine()->getRepository('CaseBundle:FormControl')->findBy([
            'attributePath' => $path
        ]);
//
//        $distinctPaths = [];
//
//        foreach ($formControls as $formControl) {
//
//            $path = $formControl->getAttributePath();
//
//            if(!in_array($path, $distinctPaths)) {
//                $distinctPaths[] = $path;
//            }
//
//        }
//
//        $paths = [];
//
//        foreach ($distinctPaths as $path) {
//
//            $paths[] = [
//                'label' => $path,
//                'value' => $path
//            ];
//
//        }

        return new JsonResponse([
            'controls' => $formControls
        ]);

    }

    /**
     * @Route("/get-services-and-procedures", name="management_admin_panel_scenarios_builder_get_services_and_procedures", options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getServicesAction()
    {

        $services = $this->getDoctrine()->getRepository('CaseBundle:ServiceDefinition')->findAll();

        $servicesArr = [];

        $servicesArr[] = [
            'l' => '---',
            'v' => null
        ];

        foreach ($services as $service) {

            $servicesArr[] = [
                'l' => $service->getName(),
                'v' => $service->getId()
            ];

        }

        $procedures = $this->get('app.query_manager')->executeProcedure('EXEC [dbo].[p_get_available_procedures_for_scenarios]');

        return new JsonResponse([
            'services' => $servicesArr,
            'procedures' => $procedures
        ]);

    }

    /**
     * @Route("/download-pdf/{scenarioInstanceId}", name="management_admin_panel_scenarios_builder_download_pdf", options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"GET"})
     * @param $scenarioInstanceId
     * @return BinaryFileResponse
     */
    public function downloadPdfAction($scenarioInstanceId)
    {

        $scenarioInstance = $this->getDoctrine()->getRepository('ManagementBundle:ScenarioInstance')->find(intval($scenarioInstanceId));

        if(!$scenarioInstance) {
            throw new NotFoundHttpException('Scenario Instance do not exists.');
        }

        $screenShooter = $this->get('screenshooter.service');

        if(!$scenarioInstance->getScenario() instanceof Scenario) {
            $scenario = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->find(intval($scenarioInstance->getScenario()));
        }
        else {
            $scenario = $scenarioInstance->getScenario();
        }

        $name = 'scenario_' . $scenario->getId() . '_' . $scenarioInstance->getId();

        $file = $screenShooter->getPathOfPDF($name);

        $response = new BinaryFileResponse($file);

        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $name . '.pdf');

        return $response;

    }


    /**
     * @Route("/tooltip-attribute", name="management_admin_panel_scenarios_builder_tooltip_attribute", options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function tooltipAttributeAction(Request $request)
    {

        $paths = $request->request->get('paths', []);

        $resultPaths = [];

        $repository = $this->getDoctrine()->getRepository('CaseBundle:Attribute');
        $redis = $this->get('snc_redis.default');

        foreach ($paths as $path) {

            $key = str_replace(',', '.', $path);

            $value = $redis->get('tooltip_attribute:' . $key);

            if($value) {
                $resultPaths[$path] = json_decode($value);
            }
            else {

                $ids = explode(',', $path);

                $names = [];
                $success = true;

                /** @var Attribute|null $lastAttribute */
                $lastAttribute = null;

                foreach ($ids as $id) {

                    $attribute = $repository->find(intval($id));

                    if(!$attribute) {
                        $resultPaths[$path] = false;
                        $success = false;
                        break;
                    }

                    if($lastAttribute !== null) {

                        /** @var Collection $attributes */
                        $attributes = $lastAttribute->getChildren();
                        if(!$attributes->contains($attribute)) {
                            $resultPaths[$path] = false;
                            break;
                        }

                    }

                    $names[] = $attribute->getName();

                    $lastAttribute = $attribute;

                }

                if(!empty($names) && $success) {
                    $redis->setex('tooltip_attribute:' . $key, 86400, json_encode($names)); // save 1 day
                }

                if($success) {
                    $resultPaths[$path] = $names;
                }

            }

        }

        return new JsonResponse([
            'results' => $resultPaths
        ]);

    }

}
