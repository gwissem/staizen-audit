<?php

namespace ManagementBundle\Controller\Admin;

use AppBundle\Entity\DynamicTranslation;
use AppBundle\Repository\DynamicTranslationRepository;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\Step;
use CaseBundle\Repository\FormControlTranslationRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use PhpImap\Exception;
use Proxies\__CG__\CaseBundle\Entity\FormControlTranslation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method as Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DynamicTranslationsFrontController
 * @package ManagementBundle\Controller
 * @Route("/admin/dynamic-translations")
 */
class DynamicTranslationsFrontController extends Controller
{

    /**
     * @Route("/", name="management_admin_panel_dynamic_translations_key_list",  options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getKeysList(Request $request)
    {

        try {
            $searchQuery = $request->get('searchQuery');
            $em = $this->getDoctrine();
            $repository = $em->getRepository('AppBundle:DynamicTranslation');

            $keyList = $repository->getTranslationsKeysList($searchQuery);


            $code = $this->renderView('@Management/Admin/DynamicTranslations/list.html.twig', ['keys' => $keyList]);


            return new JsonResponse(
                [
                    'status' => 'ok',
                    'html' => $code
                ], 200
            );
        } catch (\Throwable $exception) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'html' => []
                ], 500);
        }
    }

    /**
     * @Route("/single-form", name="management_admin_panel_dynamic_translations_single_form",  options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getSingleForm(Request $request)
    {
        $key = $request->get('key');
        $repository = $this->getDoctrine()->getRepository('AppBundle:DynamicTranslation');
        $dynamicTranslations = $repository->findBy(['translationKey' => $key], ['id' => 'DESC']) ?? [];

        $view = $this->renderView('@Management/Admin/DynamicTranslations/form.html.twig',
            ['translations' => $dynamicTranslations,
                'translation_key' => $key
            ]
        );

        return new JsonResponse(
            [
                'status' => 'ok',
                'html' => $view
            ], 200
        );

    }


    /**
     * @Route("/all-keys/occurrences", name="management_admin_panel_dynamic_translations_keys_places",  options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getOccurrences(Request $request)
    {
        $key = $request->get('key');
        $manager = $this->getDoctrine()->getManager();
        $formControlsTranslationsRepo = $manager->getRepository('CaseBundle:FormControlTranslation');
        $formTranslations = $formControlsTranslationsRepo->findAllWithString($key);

        $modalTranslations = [];
        $router =$this->container->get('router');

        /** @var FormControlTranslation $formTranslation */
        foreach ($formTranslations as $formTranslation) {
            $id = $formTranslation->getId();
            /** @var FormControl $form */
            $form = $formTranslation->getTranslatable();
            $modalTranslations[$id]['label']=$form->getLabel();
            $steps = $form->getFormTemplate()->getSteps()->toArray();
            /** @var Step $step */
            foreach ($steps as $step)
            {
                $modalTranslations[$id]['step'][$step->getId()]['id'] = $step->getId();
                $route = $router->generate('admin_process_design_chart',['id' =>$step->getProcessDefinition()->getId()]) . '?stepId='.$step->getId();
                $modalTranslations[$id]['step'][$step->getId()]['editUrl'] =$route;
            }
        }

        $view = $this->renderView('@Management/Admin/DynamicTranslations/occurrencesTable.html.twig',
            [
                'occurrences' => $modalTranslations
            ]
        );

        return new JsonResponse(
            [
                'status' => 'ok',
                'html' => $view
            ], 200
        );

    }


    /**
     * @Route("/all-keys", name="management_admin_panel_dynamic_translations_keys_rename",  options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"PUT"})
     * @param Request $request
     * @return JsonResponse
     */
    public function renameKeys(Request $request)
    {
        $oldName = $request->get('oldName');
        $newName = $request->get('newName');
        try {
            $repository = $this->getDoctrine()->getRepository('AppBundle:DynamicTranslation');
            $dynamicTranslations = $repository->findBy(['translationKey' => $oldName]) ?? [];


            $em = $this->getDoctrine();
            $manager = $em->getManager();
            /** @var DynamicTranslation $dynamicTranslation */
            foreach ($dynamicTranslations as $dynamicTranslation) {
                $dynamicTranslation->setTranslationKey($newName);
                $manager->persist($dynamicTranslation);
            }
            $manager->flush();


            return new JsonResponse(
                [
                    'status' => 'ok',
                    'key' => $newName
                ], 200
            );
        } catch (\Throwable $exception) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg' => $exception->getMessage()
                ], 500
            );
        }

    }

    /**
     * @Route("/all-keys", name="management_admin_panel_dynamic_translations_keys_delete",  options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"DELETE"})
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteAllKeys(Request $request)
    {
        $key = $request->get('key');
        try {
            $repository = $this->getDoctrine()->getRepository('AppBundle:DynamicTranslation');
            $dynamicTranslations = $repository->findBy(['translationKey' => $key]) ?? [];


            $em = $this->getDoctrine();
            $manager = $em->getManager();


            /** @var DynamicTranslation $dynamicTranslation */
            foreach ($dynamicTranslations as $dynamicTranslation) {
                $manager->remove($dynamicTranslation);
                $manager->flush();
            }


            return new JsonResponse(
                [
                    'status' => 'ok',
                    'key' => $key
                ], 200
            );
        } catch (\Throwable $exception) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg' => $exception->getMessage()
                ], 500
            );
        }

    }


    /**
     * @Route("/single-key", name="management_admin_panel_dynamic_translations_key_add",  options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addKey(Request $request)
    {
        $key = $request->get('key');
        try {
            if (is_string($key) && strlen($key) > 0) {

                $dynamicTranslation = new DynamicTranslation();
                $dynamicTranslation->setLocale('pl');
                $dynamicTranslation->setTranslationKey($key);
                $em = $this->getDoctrine();
                $em->getManager()->persist($dynamicTranslation);
                $em->getManager()->flush();
                $em->getManager()->refresh($dynamicTranslation);

                $id = $dynamicTranslation->getId();


                return new JsonResponse(
                    [
                        'status' => 'ok',
                        'id' => $id
                    ], 200
                );
            } else {
                throwException(new \Exception('Not valid string'));
            }


        } catch (\Throwable $exception) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg' => $exception->getMessage(),
                    'id' => false
                ], 500);
        }

    }


    /**
     * @Route("/single-key", name="management_admin_panel_dynamic_translations_key_save",  options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"PUT"})
     * @param Request $request
     * @return JsonResponse
     */
    public function saveKey(Request $request)
    {
        $id = $request->get('id');
        $validation = $request->get('validation');
        $text = $request->get('text');
        try {

            $em = $this->getDoctrine();
            $dynamicTranslation = $em->getRepository('AppBundle:DynamicTranslation')->find($id);
            if ($dynamicTranslation) {
                $dynamicTranslation->setText($text);
                $dynamicTranslation->setValidation($validation);
                $em->getManager()->persist($dynamicTranslation);
                $em->getManager()->flush();
                $em->getManager()->refresh($dynamicTranslation);
                $id = $dynamicTranslation->getId();
            } else {
                throwException(new \Exception('Translation not exist'));
            }


            return new JsonResponse(
                [
                    'status' => 'ok',
                    'id' => $id
                ], 200
            );


        } catch (\Throwable $exception) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg' => $exception->getMessage(),
                    'id' => false
                ], 500);
        }

    }


    /**
     * @Route("/single-key", name="management_admin_panel_dynamic_translations_key_delete",  options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"DELETE"})
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteKey(Request $request)
    {
        $id = $request->get('id');
        try {

            $em = $this->getDoctrine();
            $dynamicTranslation = $em->getRepository('AppBundle:DynamicTranslation')->find($id);
            $em->getManager()->remove($dynamicTranslation);
            $em->getManager()->flush();

            return new JsonResponse(
                [
                    'status' => 'ok',
                ], 200
            );


        } catch (\Throwable $exception) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'msg' => $exception->getMessage(),
                    'id' => false
                ], 500);
        }

    }


}