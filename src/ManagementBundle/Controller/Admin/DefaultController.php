<?php

namespace ManagementBundle\Controller\Admin;

use CaseBundle\Entity\AtlasVersionLog;
use ManagementBundle\Form\Type\AtlasVersionLogType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method as Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 * @package ManagementBundle\Controller
 * @Route("/admin")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="management_admin_panel")
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     */
    public function indexAction()
    {
        return $this->render('ManagementBundle:Admin:index.html.twig', [
            'newVersion' => $this->suggestNewVersion()
        ]);
    }

    private function suggestNewVersion() {

        /** @var AtlasVersionLog $versionLog */
        $versionLog = $this->getDoctrine()->getRepository('CaseBundle:AtlasVersionLog')->getLastVersion();

        if(empty($versionLog)) return '1.0.0';

        $versionLog = $versionLog[0];
        $parsedVersion = $versionLog->parseVersionToNumber();
        $parsedVersion++;

        return $versionLog->parseNumberToVersion($parsedVersion);

    }

}
