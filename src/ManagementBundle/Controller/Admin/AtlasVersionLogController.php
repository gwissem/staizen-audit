<?php

namespace ManagementBundle\Controller\Admin;

use CaseBundle\Entity\AtlasVersionLog;
use ManagementBundle\Form\Type\AtlasVersionLogSelectedType;
use ManagementBundle\Form\Type\AtlasVersionLogType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method as Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AtlasVersionLog
 * @package ManagementBundle\Controller
 * @Route("/admin/atlas-version-log")
 */
class AtlasVersionLogController extends Controller
{

    /**
     * @Route("/list", name="management_admin_panel_version_log_list")
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function atlasVersionLogListAction(Request $request)
    {

        $page = $request->query->get('page', 1);

        $versionLogsQuery = $this->getDoctrine()->getRepository('CaseBundle:AtlasVersionLog')->getLogs();

        $perPage = 40;

        $pagination = $this->get('knp_paginator')->paginate($versionLogsQuery, $page, $perPage, [

        ]);

        $list = $this->renderView('ManagementBundle:Admin/AtlasVersionLog:atlas_version_log_list.html.twig', [
            'pagination' => $pagination
        ]);

        $paginationView = $this->renderView('ManagementBundle:Admin/AtlasVersionLog:atlas_version_log_pagination.html.twig', [
            'pagination' => $pagination
        ]);

        return new JsonResponse([
            'list' => $list,
            'pagination' => $paginationView,
            'page' => $page
        ]);

    }

    /**
     * @Route("/form-view/{id}", defaults={"id"=0}, name="management_admin_panel_version_log_form")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @Method({"GET"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function atlasVersionLogFormAction(Request $request, $id)
    {

        if($id) {
            $atlasVersionLog = $this->getDoctrine()->getRepository('CaseBundle:AtlasVersionLog')->find(intval($id));
            if(!$atlasVersionLog) {
                throw new NotFoundHttpException('Nie znaleziono logu.');
            }
        }
        else {
            $atlasVersionLog = new AtlasVersionLog();
        }

        $form = $this->createForm(
            AtlasVersionLogType::class,
            $atlasVersionLog
        );

        return $this->render('@Management/Admin/AtlasVersionLog/atlas_version_log_form.html.twig',[
            'atlasVersionLog' => $atlasVersionLog,
            'atlasVersionForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", defaults={"id":0}, name="management_admin_panel_version_log_edit")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function atlasVersionLogEditAction(Request $request, $id)
    {

        if(!empty($id)) {
            $atlasVersionLog = $this->getDoctrine()->getRepository('CaseBundle:AtlasVersionLog')->find(intval($id));
        }
        else {
            $atlasVersionLog = new AtlasVersionLog();
        }

        $form = $this->createForm(
            AtlasVersionLogType::class,
            $atlasVersionLog
        );

        $form->handleRequest($request);

        if($form->isValid()) {

            $entity = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

        }

        return new JsonResponse([
            'status' => true
        ]);
    }

    /**
     * @Route("/remove/{id}", name="management_admin_panel_version_log_remove")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function atlasVersionLogRemoveAction($id)
    {

        $atlasVersionLog = $this->getDoctrine()->getRepository('CaseBundle:AtlasVersionLog')->find(intval($id));

        if(!$atlasVersionLog) {
            throw new NotFoundHttpException('Nie znaleziono Logu.');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($atlasVersionLog);
        $em->flush();

        return new JsonResponse([
            'status' => true
        ]);
    }

    /**
     * @Route("/create-new-version", name="management_admin_create_new_version", options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \HttpException
     * @internal param $id
     * @Method({"POST"})
     */
    public function createNewVersionAction(Request $request)
    {

        $version = $request->request->get('version', null);
        $checkedLogs = $request->request->get('checkedLogs', null);

        if(empty($checkedLogs)) {
            throw new HttpException(500,"Brak zaznaczonych elementów.");
        }

        $versionIsValid = preg_match('/([0-9]{1}\.{1}[0-9]{1,2}\.{1}[0-9]{1,3})$/', $version);

        if(!$versionIsValid) {
            throw new HttpException(500,"Niepoprawna wersja.");
        }

        $atlasVersionLog = $this->getDoctrine()->getRepository('CaseBundle:AtlasVersionLog')->findBy([
            'version' => $version
        ]);

        if($atlasVersionLog) {
            throw new HttpException(500,"Ta wersja jest już używana.");
        }

        // TODO - dorzucić czy wersja jest niższa niż poprzednie

        /** @var AtlasVersionLog[] $atlasVersionLogs */
        $atlasVersionLogs = $this->getDoctrine()->getRepository('CaseBundle:AtlasVersionLog')->findBy([
            'id' => $checkedLogs
        ]);

        $now = new \DateTime();
        $em = $this->getDoctrine()->getManager();

        foreach ($atlasVersionLogs as $atlasVersionLog) {
            $atlasVersionLog->setVersion($version);
            $atlasVersionLog->setVersionDate($now);
            $em->persist($atlasVersionLog);
        }

        $em->flush();

        return new JsonResponse([
            'status' => true
        ]);
    }


    /**
     * @Route("/log-chart-design-form/{id}", defaults={"id":0}, name="management_version_log_for_chart_design", options={"expose":true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function atlasVersionLogChartDesignAction(Request $request, $id)
    {

        $response = [];

        $atlasVersionLog = new AtlasVersionLog();

        $form = $this->createForm(
            AtlasVersionLogSelectedType::class,
            $atlasVersionLog
        );

        if($request->isMethod('GET')) {

            $view = $this->renderView('@Management/Admin/AtlasVersionLog/atlas_version_log_form_chart_design.html.twig', [
                'atlasVersionForm' => $form->createView()
            ]);

            $response = [
                'form' => $view
            ];

        }
        elseif ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if($form->isValid()) {

                $entity = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $response = [
                    'status' => true
                ];
            }

        }

        return new JsonResponse($response);
    }
}
