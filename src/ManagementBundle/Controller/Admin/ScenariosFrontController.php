<?php

namespace ManagementBundle\Controller\Admin;

use ManagementBundle\ScenarioUtils\ScenarioLoggerHttp;
use ManagementBundle\ScenarioUtils\ScenarioWrapper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method as Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ScenariosBuilderController
 * @package ManagementBundle\Controller
 * @Route("/admin/scenarios-front")
 */
class ScenariosFrontController extends Controller
{

    /**
     * @Route("/run-scenario", name="management_admin_panel_scenarios_front_run_scenario",  options={"expose"=true})
     * @Security("is_granted('ROLE_SCENARIO_MODERATOR')")
     * @Method({"POST"})
     * @param Request $request
     * @return StreamedResponse
     */
    public function scenarioBuilderAction(Request $request)
    {

        $data = $request->request->all();

        $response = new StreamedResponse();

        $scenarioBuilder = $this->get('management.service.scenario_builder');

        $scenario = $this->getDoctrine()->getRepository('ManagementBundle:Scenario')->find(intval($data['scenario-id']));

        if(!$scenario) throw new NotFoundHttpException('Not found Scenario!');

        $scenarioBuilder->setScenario(new ScenarioWrapper($scenario, $data, $this->get('management.service.scenario_handler')));
        $scenarioBuilder->setLogger(new ScenarioLoggerHttp());

        $response->setCallback(function () use ($scenarioBuilder) {

            $scenarioBuilder->startBuilder();

        });

        return $response;
    }

}
