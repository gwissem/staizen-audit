<?php

namespace ManagementBundle\ScenarioUtils;


use CaseBundle\Service\ProcessHandler;
use ManagementBundle\Entity\Scenario;
use ManagementBundle\Service\ScenarioHandlerService;

class ScenarioPoint
{

    /** @var ScenarioHandlerService $scenarioHandler */
    protected $scenarioHandler;

    protected $processInstanceId;

    protected $description;

    public function __construct($processInstanceId, ScenarioHandlerService $scenarioHandler)
    {
        $this->scenarioHandler = $scenarioHandler;
        $this->processInstanceId = $processInstanceId;

        // TODO - to przebudować na coś lżejszego
        // rootId, groupId, stepId,
        $this->description = $this->scenarioHandler->processHandler->getProcessInstanceDescription(intval($processInstanceId));
    }

    public function getId() {
        return $this->description['id'];
    }

    public function getRootId() {
        return $this->description['rootId'];
    }

    public function getName() {
        return $this->description['name'] . ' (' . $this->description['stepId'] . ')';
    }

    public function getGroupProcessId() {
        return $this->description['groupProcessId'];
    }

    public function getDescription() {
        return $this->description;
    }

    public function getStepId() {
        return $this->description['stepId'];
    }

//    public function checkAttributeValue($path, $type, $value) {
//
//        $attributeValue = $this->processHandler->getAttributeValue($path, $this->getGroupProcessId(), $type);
//
//        if($attributeValue === null) {
//            return ($value === null);
//        }
//        else {
//            return ($attributeValue == $value);
//        }
//
//    }

    public function checkStepId($expectedStep) {
        return ($this->description['stepId'] == $expectedStep);
    }

//    public function expectedStep($stepId) {
//
//        if(is_array($stepId)) {
//            if (!in_array($this->description['stepId'], $stepId)) {
//                throw new \Exception(self::class . ' - Expected step ids: ' . print_r($stepId) . ', but get ' . $this->description['stepId'] . ' (' . $this->description['id'] . ')');
//            }
//        }
//        else if($this->description['stepId'] != $stepId) {
//            throw new \Exception(self::class . ' - Expected step id: ' . $stepId . ', but get ' . $this->description['stepId'] . ' (' . $this->description['id'] . ')');
//        }
//    }

}