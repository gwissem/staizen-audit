<?php

namespace ManagementBundle\ScenarioUtils;

use ManagementBundle\Entity\Scenario;

class ScenarioEventScenario extends ScenarioEvent
{

    protected $scenario;

    public function setType()
    {
        $this->type = self::EVENT_SCENARIO;
    }

    public function dispatchEvent() {

    }

    /**
     * @return integer
     */
    public function getProcessInstanceId() {

        return 1;

    }


    public function setData($data) {

        $scenarioEntity = $this->scenarioHandler->em->getRepository('ManagementBundle:Scenario')->find(intval($data->scenarioId));

        if($scenarioEntity) {
            $this->scenario = $scenarioEntity;
        }
        else {

        }

        return $this;
    }

    /**
     * @return Scenario
     */
    public function getScenario()
    {
        return $this->scenario;
    }

    /**
     * @param mixed $scenario
     */
    public function setScenario($scenario)
    {
        $this->scenario = $scenario;
    }


}