<?php

namespace ManagementBundle\ScenarioUtils;

use CaseBundle\Entity\AttributeValue;
use MssqlBundle\PDO\PDO;
use OperationalBundle\Controller\OperationDashboardApi;

class ScenarioEventRing extends ScenarioEvent
{

    protected $ringerNumber;

    protected $platformId;

    public function setType()
    {
        $this->type = self::EVENT_RING;
    }

    /**
     * @return self
     * @throws \Exception
     */
    public function dispatchEvent()
    {

        $processInstanceId = $this->scenarioHandler->processHandler->start(
            OperationDashboardApi::CALL_PHONE_PROCESS_ID,
            NULL,
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId());

        $stagePoint = new ScenarioPoint($processInstanceId, $this->scenarioHandler);

        $this->scenarioHandler->processHandler->editAttributeValue(
            '197',
            $stagePoint->getGroupProcessId(),
            $this->ringerNumber,
            AttributeValue::VALUE_STRING,
            $stagePoint->getStepId(),
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId()
        );

        $this->scenarioHandler->processHandler->editAttributeValue(
            '253',
            $stagePoint->getGroupProcessId(),
            $this->platformId,
            AttributeValue::VALUE_INT,
            $stagePoint->getStepId(),
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId()
        );

        $ids = $this->scenarioHandler->processHandler->next(
            $processInstanceId,
            1,
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId());

        if (empty($ids)) {
            $this->processInstanceId = null;
            // TODO błąd!
        }

        if (is_array($ids)) {
            $this->processInstanceId = $ids[0];
        } else {
            $this->processInstanceId = intval($ids);
        }

        /**
         * Dodanie notatki do sprawy - dzwoni telefon
         */

        $this->scenarioHandler->processHandler->formControls($processInstanceId,null, 1);
        $this->createRingNote($stagePoint);

        return $this;
    }

    /**
     * @return integer
     */
    public function getProcessInstanceId()
    {

        return intval($this->processInstanceId);

    }

    public function setData($data)
    {

        $this->ringerNumber = $data->ringerNumber;
        $this->platformId = $data->platformId;

        return $this;
    }

    private function createRingNote(ScenarioPoint $stagePoint)
    {

        $parameters = [
            [
                'key' => 'groupProcessId',
                'value' => (int)$stagePoint->getGroupProcessId(),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'email',
                'value' => null,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ],
            [
                'key' => 'phoneNumber',
                'value' => $this->ringerNumber,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ],
            [
                'key' => 'type',
                'value' => 'phone',
                'type' => PDO::PARAM_STR,
                'length' => 50,
            ],
            [
                'key' => 'content',
                'value' => 'Połączenie przychodzące - wygenerowane przez scenariusz',
                'type' => PDO::PARAM_STR,
                'length' => 4000,
            ],
            [
                'key' => 'userId',
                'value' => 1,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ]
        ];

        $this->scenarioHandler->processHandler->queryManager->executeProcedure('EXEC dbo.p_note_new @groupProcessId = :groupProcessId, @type = :type, 
        @content = :content, @phoneNumber = :phoneNumber, @email = :email, @userId = :userId, @err = :err, @message = :message',
            $parameters, false);

    }

}