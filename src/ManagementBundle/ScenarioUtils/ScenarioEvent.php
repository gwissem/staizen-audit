<?php

namespace ManagementBundle\ScenarioUtils;

use ManagementBundle\Service\ScenarioHandlerService;

abstract class ScenarioEvent implements ScenarioEventInterface
{

    const EVENT_RING = 'ring';
    const EVENT_SERVICE = 'service';
    const EVENT_SCENARIO = 'scenario';
    const EVENT_RS = 'rs';
    const EVENT_EMAIL = 'email';

    protected $type;

    protected $processInstanceId;

    /** @var ScenarioHandlerService */
    protected $scenarioHandler;

    /**
     * ScenarioEvent constructor.
     * @param ScenarioHandlerService $scenarioHandler
     */
    public function __construct(ScenarioHandlerService $scenarioHandler)
    {
        $this->scenarioHandler = $scenarioHandler;
        $this->setType();

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

}