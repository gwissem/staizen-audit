<?php

namespace ManagementBundle\ScenarioUtils;

interface ScenarioEventInterface
{

    /**
     * @return integer
     */
    public function getProcessInstanceId();

    public function dispatchEvent();

    public function setType();

    /**
     * @return string
     */
    public function getType();

    /**
     * @param $data
     * @return self
     */
    public function setData($data);
}