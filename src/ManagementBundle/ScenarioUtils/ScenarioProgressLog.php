<?php

namespace ManagementBundle\ScenarioUtils;


use MapBundle\Controller\FindBySmsController;

class ScenarioProgressLog
{

    public $type;

    public $progress;

    public $message;

    public $extraData;

    public $icon;

    public $color;

    public $id;

    public $update;

    public $time;

    /**
     * ScenarioProgressLog constructor.
     * @param $type
     * @param array $data
     * @param null $id
     */
    public function __construct($type, $data = [], $id = null)
    {

        $this->update = ($id !== null);
        $this->id = ($id === null) ? FindBySmsController::gen_uuid() : $id;
        $this->type = $type;
        $this->progress = ($data['progress']) ?? null;
        $this->message = ($data['message']) ?? '';
        $this->extraData = ($data['extraData']) ?? null;
        $this->time = ($data['time']) ?? '-';
        $this->chooseIconAndColor();

        return $this;
    }

    public function toArray() {

        return get_object_vars($this);

    }

    private function chooseIconAndColor() {

//        label-danger
//        label-default
//        label-green
//        label-info
//        label-primary
//        label-warning
//        label-success
//
        switch ($this->type) {
            case 'error' : {

                $data = ['fa-exclamation', 'danger'];
                break;

            }
            case 'warning' : {

                $data = ['fa-exclamation-triangle', 'warning'];
                break;

            }
            case 'complete' : {

                $data = ['fa-check', 'success'];
                break;

            }
            case 'run-step' : {

                $data = ['fa-share-alt', 'primary'];
                break;

            }
            case 'save-attributes' : {

                $data = ['fa-cube', 'green'];
                break;

            }
            case 'event' : {

                $data = ['fa-database', 'green'];
                break;

            }
            case 'custom' : {

                $data = ['fa-commenting-o', 'default'];
                break;

            }
            default : {

                $data = ['fa-info', 'info'];

            }
        }


        list($this->icon, $this->color) = $data;
    }

    /**
     * @return false|mixed|string
     */
    public function toJson() {

        return json_encode($this->toArray());

    }
}