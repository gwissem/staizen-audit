<?php

namespace ManagementBundle\ScenarioUtils;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\CaseFormGenerator;
use ManagementBundle\Exception\ScenarioStopException;
use OperationalBundle\Controller\OperationDashboardApi;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ScenarioEventEmail extends ScenarioEvent
{

    const NEW_CASE_FROM_EMAIL_STEP_ID = '1011.51';

    public function setType()
    {
        $this->type = self::EVENT_EMAIL;
    }

    /**
     * @return self
     * @throws \Exception
     */
    public function dispatchEvent() {


        $processInstanceId = $this->scenarioHandler->processHandler->start(
            self::NEW_CASE_FROM_EMAIL_STEP_ID,
            NULL,
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId());

        $this->processInstanceId = $processInstanceId;

        return $this;

    }

    /**
     * @return integer
     */
    public function getProcessInstanceId() {

        return intval($this->processInstanceId);

    }

    /**
     * @param $data
     * @return self
     */
    public function setData($data)
    {
        return $this;
    }
}