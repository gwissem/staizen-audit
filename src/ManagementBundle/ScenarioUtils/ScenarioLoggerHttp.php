<?php

namespace ManagementBundle\ScenarioUtils;


class ScenarioLoggerHttp implements ScenarioLoggerInterface
{

    const OUTPUT_BUFFERING = 4096;

    /**
     * ScenarioLoggerHttp constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param ScenarioProgressLog $log
     * @return ScenarioProgressLog
     */
    public function log(ScenarioProgressLog $log) {

        echo json_encode([
            'log' => $log->toArray(),
            'buffer' => str_pad('', self::OUTPUT_BUFFERING)
        ]);

        ob_flush();
        flush();
        usleep(100000); // 100 ms

        return $log;
    }

}