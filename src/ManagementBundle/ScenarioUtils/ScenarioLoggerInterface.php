<?php

namespace ManagementBundle\ScenarioUtils;


interface ScenarioLoggerInterface
{
    /**
     * @param ScenarioProgressLog $message
     * @return ScenarioProgressLog
     */
    public function log(ScenarioProgressLog $message);
}