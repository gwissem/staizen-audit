<?php

namespace ManagementBundle\ScenarioUtils;


use CaseBundle\Service\ProcessHandler;
use ManagementBundle\Entity\Scenario;
use ManagementBundle\Service\ScenarioHandlerService;

class ScenarioWrapper
{

    /** @var Scenario */
    public $scenario;

    /** @var ScenarioHandlerService */
    protected $scenarioHandler;

    protected $config;

    private $cachedSteps = null;

    /**
     * ScenarioWrapper constructor.
     * @param Scenario $scenario
     * @param array $data
     * @param ScenarioHandlerService $scenarioHandler
     */
    public function __construct(Scenario $scenario, $data = [], ScenarioHandlerService $scenarioHandler)
    {
        $this->scenario = $scenario;
        $this->scenarioHandler = $scenarioHandler;
    }

    public function getEvents() {

        /** @var ScenarioEvent[] $events */
        $events = [];

        foreach ($this->scenario->getConfigValue('events') as $event) {

            switch ($event->type) {
                case ScenarioEvent::EVENT_RING : {
                    $events[] = (new ScenarioEventRing($this->scenarioHandler))->setData($event->value);
                    break;
                }
                case ScenarioEvent::EVENT_SCENARIO : {
                    $events[] = (new ScenarioEventScenario($this->scenarioHandler))->setData($event->value);
                    break;
                }
                case ScenarioEvent::EVENT_SERVICE : {
                    $events[] = (new ScenarioEventService($this->scenarioHandler))->setData($event->value);
                    break;
                }
                case ScenarioEvent::EVENT_RS : {
                    $events[] = (new ScenarioEventRs($this->scenarioHandler));
                    break;
                }
                case ScenarioEvent::EVENT_EMAIL : {
                    $events[] = (new ScenarioEventEmail($this->scenarioHandler));
                    break;
                }
            }

        }

        return $events;

    }

    public function getDiagnosisSteps() {

        $diagnosisCode = $this->scenario->getDiagnosisCode();

        if(!$diagnosisCode) return [];

        return $this->scenarioHandler->processHandler->getDiagnosisFromCode($diagnosisCode);

    }

    public function getSteps() {

        if($this->cachedSteps === null) {
            $this->cachedSteps = $this->scenario->getConfigValue('steps');
        }

        return $this->cachedSteps;

    }

    public function getStopScenarioStep($stepId) {

        foreach ($this->getEventSteps("stopScenario") as $step) {
            if($step->stepId == $stepId) {
                return $step;
            }
        }

        return null;

    }

    /**
     * available $event: setVariant, stopScenario, reloadForm
     *
     * @param $event
     * @return array
     */
    public function getEventSteps($event) {

        $steps = [];

        foreach ($this->getSteps() as $step) {
            if($step->action === $event) {
                $steps[$step->stepId] = $step;
            }
        }

        return $steps;
    }

    public function isFormReload($stepId) {

        foreach ($this->getSteps() as $step) {
            if($step->stepId == $stepId && $step->action === "reloadForm") {
                return true;
            }
        }

        return false;
    }

    // TODO - To wywalić i wywołanie ZMIENIĆ NA FUNKCJE:  getEventSteps(setVariant)
    public function getStepsWithSetVariant() {

        $steps = [];

        foreach ($this->getSteps() as $step) {
            if($step->action === "setVariant") {
                $steps[$step->stepId] = $step;
            }
        }

        return $steps;
    }

    public function getAttributes() {

        $attributes = [];

        foreach ($this->scenario->getConfigValue('attributes') as $attribute) {

            $attributes[$attribute->path] = $attribute;

        }

        return $attributes;

    }

}