<?php

namespace ManagementBundle\ScenarioUtils;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\CaseFormGenerator;
use ManagementBundle\Exception\ScenarioStopException;
use OperationalBundle\Controller\OperationDashboardApi;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ScenarioEventRs extends ScenarioEvent
{

    public function setType()
    {
        $this->type = self::EVENT_SERVICE;
    }

    /**
     * @return self
     * @throws \Exception
     */
    public function dispatchEvent() {

        /** @var ProcessInstance $processInstance */
        $processInstances = $this->scenarioHandler->em->getRepository('CaseBundle:ProcessInstance')->findBy(['token' => 'raport-serwisowy']);

        if(empty($processInstances)) {
            throw new NotFoundHttpException('Nie znaleziono procesu RAPORT SERWISOWY!');
        }

        $processInstance = $processInstances[0];

        $ids = $this->scenarioHandler->processHandler->next(
            $processInstance->getId(),
            1,
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId());

        if(empty($ids)) {
            $this->processInstanceId = null;
            // TODO błąd!
        }

        if(is_array($ids)) {
            $this->processInstanceId = $ids[0];
        }
        else {
            $this->processInstanceId = intval($ids);
        }

        return $this;

    }

    /**
     * @return integer
     */
    public function getProcessInstanceId() {

        return intval($this->processInstanceId);

    }

    /**
     * @param $data
     * @return self
     */
    public function setData($data)
    {
        return $this;
    }
}