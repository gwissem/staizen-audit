<?php

namespace ManagementBundle\ScenarioUtils;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Service\CaseFormGenerator;
use ManagementBundle\Exception\ScenarioStopException;
use OperationalBundle\Controller\OperationDashboardApi;

class ScenarioEventService extends ScenarioEvent
{

    protected $serviceId;

    /** @var ScenarioPoint */
    private $scenarioPoint;

    public function setType()
    {
        $this->type = self::EVENT_SERVICE;
    }

    /**
     * @return self
     * @throws \Exception
     */
    public function dispatchEvent() {

        if(!$this->scenarioPoint->checkStepId(CaseFormGenerator::STEP_OF_SERVICES)) {
            throw new ScenarioStopException('Service Event require step ' . CaseFormGenerator::STEP_OF_SERVICES);
        }

        // id, name, active, message, start_step_id
        $availableServices = $this->scenarioHandler->serviceHandler->availableServices(
            $this->scenarioPoint->getId(),
            $this->scenarioPoint->getGroupProcessId());

        $found_key = array_search($this->serviceId, array_column($availableServices, 'id'));

        $serviceData = $availableServices[$found_key];

        if($serviceData['active'] == 0) {
            throw new ScenarioStopException('Service ' . $serviceData['name'] . ' is disabled. Reaason: ' . $serviceData['message']);
        }

        $this->scenarioHandler->serviceHandler->runService(
            $serviceData['start_step_id'],
            $this->scenarioPoint->getId(),
            $this->scenarioPoint->getGroupProcessId());

        $ids = $this->scenarioHandler->processHandler->next(
            $this->scenarioPoint->getId(),
            1,
            $this->scenarioHandler->getUserId(),
            $this->scenarioHandler->getOriginalUserId());

        if(empty($ids)) {
            $this->processInstanceId = null;
            // TODO błąd!
        }

        if(is_array($ids)) {
            $this->processInstanceId = $ids[0];
        }
        else {
            $this->processInstanceId = intval($ids);
        }

        return $this;

//        $this->container->get('case.service_handler')->runService('1009.034', $stagePoint->getId(), $stagePoint->getGroupProcessId());
//        $stage = new NewCase($this->processHandler, $stagePoint);
//        return $stage->onlyNext($stagePoint->getId(), 1);

    }

    /**
     * @return integer
     */
    public function getProcessInstanceId() {

        return intval($this->processInstanceId);

    }

    public function setScenarioPoint(ScenarioPoint $scenarioPoint) {
        $this->scenarioPoint = $scenarioPoint;
        return $this;
    }

    public function setData($data) {

        $this->serviceId = $data->serviceId;

        return $this;
    }

}