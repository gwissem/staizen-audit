<?php

namespace ManagementBundle\Entity;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeCondition;
use CaseBundle\Entity\FormTemplate;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessFlow;
use CaseBundle\Entity\Step;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="dev_log")
 */
class DevLog
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;

    const NAME_LOG_CREATED_STEP = 'created_step';
    const NAME_LOG_UPDATED_STEP = 'updated_step';
    const NAME_LOG_DELETED_STEP = 'deleted_step';
    const NAME_LOG_CREATED_FLOW = 'created_flow';
    const NAME_LOG_UPDATED_FLOW = 'updated_flow';
    const NAME_LOG_DELETED_FLOW = 'deleted_flow';
    const NAME_LOG_CREATED_FORM_TEMPLATE = 'created_form_template';
    const NAME_LOG_UPDATED_FORM_TEMPLATE = 'updated_form_template';
    const NAME_LOG_UPDATED_CONTROL_CONDITION = 'updated_control_condition';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = "";

    /**
     * Constructor
     * @param null $name
     * @param null $description
     */
    public function __construct($name = null, $description = null)
    {
        if($name) $this->setName($name);
        if($description) $this->setDescription($description);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    static function CREATED_STEP(Step $step, ProcessDefinition $processDefinition) {

        $logDescription = 'Do procesu: ' . $processDefinition->getName() . ' (' . $processDefinition->getId() . '), dodano nowy krok: ';
        $logDescription .= $step->getName() . ' (' . $step->getId() . ').';
        return new DevLog(DevLog::NAME_LOG_CREATED_STEP, $logDescription);

    }

    static function DELETED_STEP(Step $step, ProcessDefinition $processDefinition) {

        $logDescription = 'W procesie: ' . $processDefinition->getName() . ' (' . $processDefinition->getId() . '), został usunięty krok: ';
        $logDescription .= $step->getName() . ' (' . $step->getId() . ').';
        return new DevLog(DevLog::NAME_LOG_DELETED_STEP, $logDescription);

    }

    static function UPDATED_STEP(Step $step) {

        $logDescription = 'Krok: ' . $step->getName() . ' (' . $step->getId() . ') został zaktualizowany.';
        return new DevLog(DevLog::NAME_LOG_UPDATED_STEP, $logDescription);

    }

    static function CREATED_FLOW(ProcessDefinition $processDefinition, Step $stepFrom, Step $stepTo) {

        $logDescription = 'W procesie: ' . $processDefinition->getName() . ' (' . $processDefinition->getId() . '), utworzono FLOW pomiędzy krokami: "' . $stepFrom->getDefinitionName() . ' <=> ' . $stepTo->getDefinitionName() . '"';
        return new DevLog(DevLog::NAME_LOG_CREATED_FLOW, $logDescription);

    }

    static function UPDATED_PATH_FLOW(ProcessDefinition $processDefinition, ProcessFlow $flow, Step $stepFrom, Step $stepTo) {

        $logDescription = 'W procesie: ' . $processDefinition->getName() . ' (' . $processDefinition->getId() . '), zaktualizowano FLOW (' . $flow->getId() . '). ';
        $logDescription .= 'Z "' . $flow->getStep()->getDefinitionName() . ' <=>' . $flow->getNextStep()->getDefinitionName() . '"';
        $logDescription .= ' zmieniono na "' . $stepFrom->getDefinitionName() . ' <=> ' . $stepTo->getDefinitionName() . '"';
        return new DevLog(DevLog::NAME_LOG_UPDATED_FLOW, $logDescription);

    }

    static function UPDATED_FLOW(ProcessFlow $flow) {

        $logDescription = 'W procesie: ' . $flow->getProcessDefinition()->getName() . ' (' . $flow->getProcessDefinition()->getId() . '), zaktualizowano FLOW (' . $flow->getId() . ') ';
        $logDescription .= ' pomiędzy "' . $flow->getStep()->getDefinitionName() . ' <=>' . ($flow->getNextStep()) ? $flow->getNextStep()->getDefinitionName() : '---' . '"';
        return new DevLog(DevLog::NAME_LOG_UPDATED_FLOW, $logDescription);

    }

    static function DELETED_FLOW(ProcessDefinition $processDefinition, Step $stepFrom = null, Step $stepTo = null) {
        $logDescription = 'W procesie: ' . $processDefinition->getName() . ' (' . $processDefinition->getId() . '), usunięto FLOW';
        if($stepFrom && $stepTo) {
            $logDescription = 'W procesie: ' . $processDefinition->getName() . ' (' . $processDefinition->getId() . '), usunięto FLOW pomiędzy krokami: "' . $stepFrom->getDefinitionName() . ' <=> ' . $stepTo->getDefinitionName() . '"';
        }
        return new DevLog(DevLog::NAME_LOG_DELETED_FLOW, $logDescription);

    }

    /**
     * @param Attribute|Step $entity
     * @return DevLog
     */
    static function CREATED_FORM_TEMPLATE($entity) {

        $logDescription = '';

        if ($entity instanceof Attribute) {
            $logDescription .= 'Dla atrybutu ' . $entity->getDefinitionName() . ' został utworzony formularz.';
        }
        elseif ($entity instanceof Step){
            $logDescription .= 'Dla kroku ' . $entity->getDefinitionName() . ' został utworzony formularz.';
        }

        if($logDescription) {
            return new DevLog(DevLog::NAME_LOG_CREATED_FORM_TEMPLATE, $logDescription);
        }

        return null;

    }

    /**
     * @param Attribute|Step $entity
     * @return DevLog
     */
    static function UPDATED_FORM_TEMPLATE($entity) {

        $logDescription = '';

        if ($entity instanceof Attribute) {
            $logDescription .= 'W atrybucie ' . $entity->getDefinitionName() . ' został zaktualizowany formularz.';
        }
        elseif ($entity instanceof Step){
            $logDescription .= 'W kroku ' . $entity->getDefinitionName() . ' został zaktualizowany formularz.';
        }

        if($logDescription) {
            return new DevLog(DevLog::NAME_LOG_UPDATED_FORM_TEMPLATE, $logDescription);
        }

        return null;

    }

    static function UPDATE_CONTROL_CONDITION(AttributeCondition $attributeCondition) {

        $logDescription = 'W kroku ' . $attributeCondition->getStep()->getDefinitionName() . ' zaktualizowany warunki dla kontrolki ' . $attributeCondition->getFormControlId();
        return new DevLog(DevLog::NAME_LOG_UPDATED_CONTROL_CONDITION, $logDescription);

    }

}
