<?php

namespace ManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="ManagementBundle\Repository\ScenarioRepository")
 * @ORM\Table(name="scenario")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Scenario
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = "";

    /**
     * @var string
     * @ORM\Column(name="config", type="text")
     */
    protected $config;

    /**
     * @var int
     * @ORM\Column(name="version", type="integer")
     */
    protected $version;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    private $configObject = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->version = 1;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getConfig(): string
    {
        return $this->config;
    }

    /**
     * @param string $config
     */
    public function setConfig(string $config)
    {
        $this->config = $config;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * @param $key
     * @param null $subKey
     * @return null|mixed
     */
    public function getConfigValue($key, $subKey = null) {

        if($this->configObject === null) {
            $this->configObject = json_decode($this->getConfig());
        }

        if(isset($this->configObject->$key)) {

            if($subKey !== null) {
                $subObject = $this->configObject->$key;

                if(isset($subObject->$subKey)) {
                    return $this->configObject->$key->$subKey;
                }

                return null;
            }

            return $this->configObject->$key;
        }

        return null;
    }

    public function isPdf() {

        $basicConfig = $this->getConfigValue('basic');

        if(isset($basicConfig->pdf)) {
            return filter_var($basicConfig->pdf, FILTER_VALIDATE_BOOLEAN);
        }

        return false;

    }

    public function isValidationSteps() {

        $basicConfig = $this->getConfigValue('basic');

        if(isset($basicConfig->validation_step)) {
            return filter_var($basicConfig->validation_step, FILTER_VALIDATE_BOOLEAN);
        }

        return false;

    }

    public function getStartStep() {

        $basicConfig = $this->getConfigValue('basic');

        if(isset($basicConfig->start_step)) {
            return $basicConfig->start_step;
        }

        return null;

    }

    public function getDiagnosisCode() {

        $basicConfig = $this->getConfigValue('basic');

        if(isset($basicConfig->diagnosis_code)) {
            return $basicConfig->diagnosis_code;
        }

        return null;

    }

    public function getEndStep() {

        $basicConfig = $this->getConfigValue('basic');

        if(isset($basicConfig->end_step)) {
            return $basicConfig->end_step;
        }

        return null;

    }

    public function toArray() {

        return [
            'id' => $this->id,
            'name' =>  $this->name,
            'description' => $this->getDescription(),
            'version' => $this->getVersion(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'createdBy' => $this->getCreatedBy()->getName(),
            'config' => json_decode($this->getConfig())
        ];

    }


    public function __clone()
    {
        $this->id = null;
//        $this->createdAt = null;
//        $this->updatedAt = null;
        $this->version++;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }


}
