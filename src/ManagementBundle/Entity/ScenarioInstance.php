<?php

namespace ManagementBundle\Entity;

use CaseBundle\Entity\ProcessInstance;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use UserBundle\Entity\User;

/**
 * ScenarioInstance
 *
 * @ORM\Table(name="scenario_instance")
 * @ORM\Entity(repositoryClass="ManagementBundle\Repository\ScenarioInstanceRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class ScenarioInstance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="ManagementBundle\Entity\Scenario")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $scenario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="time", type="integer")
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="error_message", type="text", nullable=true)
     */
    private $errorMessage;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $root;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $lastProcessInstance;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    private $result;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_pdf", type="boolean", )
     */
    private $hasPDF = false;

    /**
     * ScenarioInstance constructor.
     */
    public function __construct()
    {
        $this->time = 0;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scenarioId
     *
     * @param Scenario $scenario
     *
     * @return ScenarioInstance
     */
    public function setScenario($scenario)
    {
        $this->scenario = $scenario;

        return $this;
    }

    /**
     * Get scenarioId
     *
     * @return int
     */
    public function getScenario()
    {
        return $this->scenario;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ScenarioInstance
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set userId
     *
     * @param User $user
     *
     * @return ScenarioInstance
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return ScenarioInstance
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set errorMessage
     *
     * @param string $errorMessage
     *
     * @return ScenarioInstance
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * Get errorMessage
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Set root
     *
     * @param ProcessInstance $root
     *
     * @return ScenarioInstance
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return int
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set lastProcessInstanceId
     *
     * @param ProcessInstance $lastProcessInstance
     *
     * @return ScenarioInstance
     */
    public function setLastProcessInstance($lastProcessInstance)
    {
        $this->lastProcessInstance = $lastProcessInstance;

        return $this;
    }

    /**
     * Get lastProcessInstance
     *
     * @return int
     */
    public function getLastProcessInstance()
    {
        return $this->lastProcessInstance;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return ScenarioInstance
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return bool
     */
    public function hasPDF()
    {
        return $this->hasPDF;
    }

    /**
     * @param bool $hasPDF
     */
    public function setHasPDF($hasPDF)
    {
        $this->hasPDF = $hasPDF;
    }




    /**
     * @return array
     */
    public function getErrorList(){
        if($this->getErrorMessage()){
            try{
                $res =  json_decode($this->getErrorMessage());
            } catch (\Exception $e){
                $res =[];
            }
            return $res;
        }else{
            return [];
        }
    }

}

