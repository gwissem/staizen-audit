<?php

namespace ManagementBundle\Exception;

class ScenarioStopException extends \Exception {

    /**
     * ScenarioStopException constructor.
     * @param string $message
     */
    public function __construct($message = 'Scenario is stoped.')
    {
        parent::__construct($message);
    }

    public function toJsonLog() {

    }

}
