<?php

namespace ManagementBundle\Form\Type;

use CaseBundle\Entity\AtlasVersionLog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AtlasVersionLogSelectedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => 'type',
                    'required' => true,
                    'choices' => array_flip(AtlasVersionLog::getTypes()),
                    'attr' => ['class' => 'select2-deselect']
                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label' => 'description',
                    'required' => true
                ]
            )
        ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => AtlasVersionLog::class,
                'csrf_token_id' => false
            ]
        );
    }

}