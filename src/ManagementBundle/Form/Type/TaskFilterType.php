<?php

namespace ManagementBundle\Form\Type;

use CaseBundle\Entity\PlatformGroup;
use CaseBundle\Repository\PlatformGroupRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\Group;

class TaskFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'platformGroupId',
                ChoiceType::class,
                array(
                    'choices' => $options['platformGroups'],
                    'label' => 'platform.group',
                    'required' => false,
                    'attr' => array('class' => 'select2-deselect')
                )
            )
            ->add(
                'column',
                HiddenType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'groupId',
                ChoiceType::class,
                [
                    'choices' => $options['groups'],
                    'label' => 'task.group',
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect']
                ]
            )
        ->setMethod('GET');

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'platformGroups' => null,
            'groups' => null
        ]);
    }

    public function getBlockPrefix()
    {
        return "";
    }


}