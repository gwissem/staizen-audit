<?php

namespace SoapBundle\Message;

class CloseEventRequest
{

    /**
     * @var EventCorrelation $EventCorrelation
     */
    protected $EventCorrelation = null;

    /**
     * @var CallReport $CallReport
     */
    protected $CallReport = null;

    /**
     * @param EventCorrelation $EventCorrelation
     * @param CallReport $CallReport
     */
    public function __construct($EventCorrelation, $CallReport)
    {
      $this->EventCorrelation = $EventCorrelation;
      $this->CallReport = $CallReport;
    }

    /**
     * @return EventCorrelation
     */
    public function getEventCorrelation()
    {
      return $this->EventCorrelation;
    }

    /**
     * @param EventCorrelation $EventCorrelation
     * @return \SoapBundle\Model\CloseEventRequest
     */
    public function setEventCorrelation($EventCorrelation)
    {
      $this->EventCorrelation = $EventCorrelation;
      return $this;
    }

    /**
     * @return CallReport
     */
    public function getCallReport()
    {
      return $this->CallReport;
    }

    /**
     * @param CallReport $CallReport
     * @return \SoapBundle\Model\CloseEventRequest
     */
    public function setCallReport($CallReport)
    {
      $this->CallReport = $CallReport;
      return $this;
    }

}
