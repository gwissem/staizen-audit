<?php

namespace SoapBundle\Message;

class PingResponse
{

    /**
     * @var \DateTime $Timestamp
     */
    protected $Timestamp = null;

    /**
     * @param \DateTime $Timestamp
     */
    public function __construct(\DateTime $Timestamp)
    {
      $this->Timestamp = $Timestamp->format(\DateTime::ATOM);
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
      if ($this->Timestamp == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Timestamp);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Timestamp
     * @return \SoapBundle\Model\PingResponse
     */
    public function setTimestamp(\DateTime $Timestamp)
    {
      $this->Timestamp = $Timestamp->format(\DateTime::ATOM);
      return $this;
    }

}
