<?php

namespace SoapBundle\Message;

class CreateEventRequest
{

    /**
     * @var Service $Service
     */
    protected $Service = null;

    /**
     * @var Event $Event
     */
    protected $Event = null;

    /**
     * @param Service $Service
     * @param Event $Event
     */
    public function __construct($Service, $Event)
    {
      $this->Service = $Service;
      $this->Event = $Event;
    }

    /**
     * @return Service
     */
    public function getService()
    {
      return $this->Service;
    }

    /**
     * @param Service $Service
     * @return \SoapBundle\Model\CreateEventRequest
     */
    public function setService($Service)
    {
      $this->Service = $Service;
      return $this;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
      return $this->Event;
    }

    /**
     * @param Event $Event
     * @return \SoapBundle\Model\CreateEventRequest
     */
    public function setEvent($Event)
    {
      $this->Event = $Event;
      return $this;
    }

}
