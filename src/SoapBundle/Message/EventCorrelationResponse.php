<?php

namespace SoapBundle\Message;

class EventCorrelationResponse
{

    /**
     * @var EventCorrelation $EventCorrelation
     */
    protected $EventCorrelation = null;

    /**
     * @param EventCorrelation $EventCorrelation
     */
    public function __construct($EventCorrelation)
    {
      $this->EventCorrelation = $EventCorrelation;
    }

    /**
     * @return EventCorrelation
     */
    public function getEventCorrelation()
    {
      return $this->EventCorrelation;
    }

    /**
     * @param EventCorrelation $EventCorrelation
     * @return \SoapBundle\Model\EventCorrelationResponse
     */
    public function setEventCorrelation($EventCorrelation)
    {
      $this->EventCorrelation = $EventCorrelation;
      return $this;
    }

}
