<?php

namespace SoapBundle\Message;

use SoapBundle\Model\Event;

class UpdateEventRequest
{

    /**
     * @var Event $Event
     */
    protected $Event = null;

    /**
     * @param Event $Event
     */
    public function __construct($Event)
    {
      $this->Event = $Event;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
      return $this->Event;
    }

    /**
     * @param Event $Event
     * @return \SoapBundle\Model\UpdateEventRequest
     */
    public function setEvent($Event)
    {
      $this->Event = $Event;
      return $this;
    }

}
