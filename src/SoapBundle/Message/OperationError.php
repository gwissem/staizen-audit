<?php

namespace SoapBundle\Message;

class OperationError
{

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $Reason
     */
    protected $Reason = null;

    /**
     * @param string $ErrorCode
     * @param string $Reason
     */
    public function __construct($ErrorCode, $Reason)
    {
      $this->ErrorCode = $ErrorCode;
      $this->Reason = $Reason;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return \SoapBundle\Model\OperationError
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getReason()
    {
      return $this->Reason;
    }

    /**
     * @param string $Reason
     * @return \SoapBundle\Model\OperationError
     */
    public function setReason($Reason)
    {
      $this->Reason = $Reason;
      return $this;
    }

}
