<?php

namespace SoapBundle\Service;

use AppBundle\Entity\CustomContent;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Repository\ProcessInstanceRepository;
use SoapBundle\Message\CloseEventRequest;
use SoapBundle\Message\CreateEventRequest;
use SoapBundle\Message\CreateEventResponse;
use SoapBundle\Message\PingRequest;
use SoapBundle\Message\PingResponse;
use SoapBundle\Message\UpdateEventRequest;
use SoapBundle\Message\UpdateEventResponse;
use SoapBundle\Model\CallReport;
use SoapBundle\Model\EventCorrelation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\Group;

class AptEventHandlerService
{
    const OPERATOR_NAME = 'PL_STARTER';
    const WEBSERVICE_URL_KEY = 'ATP_WEBSERVICE_URL';
    const WEBSERVICE_USER_KEY = 'ATP_WEBSERVICE_USER';
    const WEBSERVICE_PASS_KEY = 'ATP_WEBSERVICE_PASS';
    const FIRST_STEP_ID = '1023.002';
    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array(
        'CreateEventRequest' => 'SoapBundle\\Message\\CreateEventRequest',
        'CreateEventResponse' => 'SoapBundle\\Message\\CreateEventResponse',
        'UpdateEventRequest' => 'SoapBundle\\Message\\UpdateEventRequest',
        'UpdateEventResponse' => 'SoapBundle\\Message\\UpdateEventResponse',
        'PingRequest' => 'SoapBundle\\Message\\PingRequest',
        'PingResponse' => 'SoapBundle\\Message\\PingResponse',
        'OperationError' => 'SoapBundle\\Message\\OperationError',
        'Service' => 'SoapBundle\\Model\\Service',
        'Event' => 'SoapBundle\\Model\\Event',
        'EventCorrelation' => 'SoapBundle\\Model\\EventCorrelation',
        'Caller' => 'SoapBundle\\Model\\Caller',
        'Vehicle' => 'SoapBundle\\Model\\Vehicle',
        'Vin' => 'SoapBundle\\Model\\Vin',
        'VehicleStatus' => 'SoapBundle\\Model\\VehicleStatus',
        'DiagnosticInfo' => 'SoapBundle\\Model\\DiagnosticInfo',
        'Location' => 'SoapBundle\\Model\\Location',
        'Position' => 'SoapBundle\\Model\\Position',
        'ArrayOfReverseGeocoding' => 'SoapBundle\\Model\\ArrayOfReverseGeocoding',
        'ReverseGeocoding' => 'SoapBundle\\Model\\ReverseGeocoding',
        'ArrayOfPosition' => 'SoapBundle\\Model\\ArrayOfPosition',
        'Device' => 'SoapBundle\\Model\\Device',
    );
    protected $container;

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(ContainerInterface $container, array $options = array(), $wsdl = null)
    {
        foreach (self::$classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        $this->container = $container;
    }

    /**
     * This action creates a new event
     *
     * @param CreateEventRequest $parameters
     * @return CreateEventResponse
     */
    public function CreateEvent(CreateEventRequest $parameters)
    {
        $processHandler = $this->container->get('case.process_handler');
        $em = $this->container->get('doctrine.orm.entity_manager');
        /** @var EventCorrelation $correlation */
        $correlation = $this->handleEvent($parameters);

        $groupProcessId = $correlation->getServiceProviderEventId();
        $instanceId = $processHandler->getActiveInstanceForRoot($groupProcessId);

        if ($instanceId) {
            $ecallUserGroup = $em->getRepository(Group::class)->find($this->container->getParameter('e_call_group_id'));
            $this->container->get('case.process_handler')->pushNewTaskToUserGroup($ecallUserGroup, $instanceId);
        }

        return new CreateEventResponse($correlation);
    }

    private function handleEvent($parameters)
    {

        $caseIds = $this->container->get('case.car_call_handler')->handleEcallSoapRequest(
            $parameters->getEvent()->getCallerId(),
            $parameters

        );

        $caseId = !empty($caseIds) ? $caseIds[0]['id'] : NULL;

        $processInstance = $this->container->get('doctrine.orm.entity_manager')->getRepository(ProcessInstance::class)->find($caseId);
        $groupProcessInstanceId = $processInstance->getGroupProcess()->getId();

        /** @var EventCorrelation $eventCorrelation */
        $eventCorrelation = $parameters->getEvent()->getCorrelation();
        $eventCorrelation->setServiceProviderCreationTimestamp(new \DateTime());
        $eventCorrelation->setServiceProviderLastUpdateTimestamp(new \DateTime());
        $eventCorrelation->setServiceProviderEventId($groupProcessInstanceId);

        $this->container->get('case.process_handler')->setProcessInstanceName($caseId, self::FIRST_STEP_ID);

        return $eventCorrelation;
    }

    public function closeCase($groupProcessId)
    {
        ini_set("soap.wsdl_cache_enabled", "0");

        $customContentRepository = $this->container->get('doctrine.orm.entity_manager')->getRepository(CustomContent::class);
        $platformWebserviceUrl = $customContentRepository->findOneBy(['key' => self::WEBSERVICE_URL_KEY])->getContent();
        $platformWebserviceUser = $customContentRepository->findOneBy(['key' => self::WEBSERVICE_USER_KEY])->getContent();
        $platformWebservicePass = $customContentRepository->findOneBy(['key' => self::WEBSERVICE_PASS_KEY])->getContent();

        $client = new \SoapClient(
            "$platformWebserviceUrl",
            [
                'soap_version' => SOAP_1_1,
                'login' => "$platformWebserviceUser",
                'password' => "$platformWebservicePass"
            ]
        );

        $processHandler = $this->container->get('case.process_handler');
        $aptEventId = $processHandler->getAttributeValue(EventCorrelation::PATH_ATP_EVENT_ID, $groupProcessId, AttributeValue::VALUE_INT);

        if ($aptEventId) {
            $cause = $processHandler->getAttributeValue(CallReport::PATH_CAUSE, $groupProcessId, AttributeValue::VALUE_STRING);
            $qualification = $processHandler->getAttributeValue(CallReport::PATH_QUALIFICATION, $groupProcessId, AttributeValue::VALUE_STRING);

            if(empty($cause)){
                $cause = "";
            }

            $eventCorrelation = new EventCorrelation();
            $callReport = new CallReport();
            $eventCorrelation->setAtpEventId($aptEventId);
            $eventCorrelation->setServiceProviderEventId($groupProcessId);
            $callReport->setClosureTimestamp(new \DateTime());
            $callReport->setEventCause($cause);
            $callReport->setEventQualification($qualification);
            $callReport->setClosedBy(self::OPERATOR_NAME);
            $response = $client->CloseEvent(['EventCorrelation' => $eventCorrelation, 'CallReport' => $callReport]);

            return $response;
        }

        return false;
    }

    /**
     * This action updates the details of an existing event
     *
     * @param UpdateEventRequest $parameters
     * @return UpdateEventResponse
     */
    public function UpdateEvent($parameters)
    {
        $processHandler = $this->container->get('case.process_handler');

        /** @var EventCorrelation $correlation */
        $correlation = $this->handleEvent($parameters);
        $groupProcessId = $correlation->getServiceProviderEventId();
        $instanceId = $processHandler->getActiveInstanceForRoot($groupProcessId);
        $processHandler->pushNewTaskToUserAtInstance($instanceId);

        return new UpdateEventResponse($correlation);
    }

    /**
     * @param PingRequest $parameters
     * @return PingResponse
     */
    public function Ping($parameters)
    {
        return new PingResponse(new \DateTime());
    }

    public function getClassMap()
    {
        return self::$classmap;
    }

}