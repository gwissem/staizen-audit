<?php

namespace SoapBundle\Controller;

use AppBundle\Entity\Log;
use SoapBundle\Model\Caller;
use SoapBundle\Model\CallReport;
use SoapBundle\Model\Event;
use SoapBundle\Model\EventCorrelation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/e-call")
 */
class ECallController extends Controller
{

    const OPERATOR_NAME = 'starteroperator';

    /**
     * @Route("/atp-event", name="server_atp_event")
     */
    public function serverAptAction(Request $request)
    {
        $xml = $request->getContent();
        $decodedRequest = preg_replace('/ ([-\w]+\:)?(mustUnderstand=")(1|true)(")/', '', $xml);

        ini_set("soap.wsdl_cache_enabled", "0");
        $server = new \SoapServer(
            $this->getParameter('kernel.root_dir').'/../data/wsdl/AtpEventHandler.20160817.wsdl',
            [
                'classmap' => $this->get('apt_event_handler')->getClassMap(),
                'soap_version' => SOAP_1_1
            ]
        );

        $server->setObject($this->get('apt_event_handler'));

        $response = new Response();
        $response->headers->set('Content-Type', 'application/soap+xml; charset=utf-8');

        try {
            ob_start();
            //This explicitely passes clear request content to the service handler
            $server->handle($decodedRequest);
            $response->setContent(@ob_get_clean());
        } catch (\Exception $e) {
            $this->get('logger')->err($e->getMessage());
            $response->setContent($e->getMessage());
        }

        if(!empty($xml)){
            $em  = $this->getDoctrine()->getManager();
            $log = new Log();
            $log->setName('E-CALL REQUEST');
            $log->setContent($xml);
            $em->persist($log);
            $em->flush();
        }

        $this->get('monolog.logger.atp_websocket')->log('notice', $xml);
        return $response;
    }
//
//    /**
//     * @Route("/sp-event/close/{id}")
//     */
//    public function clientSpCloseAction($id)
//    {
//        ini_set("soap.wsdl_cache_enabled", "0");
//
//        $client = new \SoapClient(
//            'https://test.atp.eu.com/api/sp/v2.0/?singleWsdl',
//            [
//                'soap_version' => SOAP_1_1,
//                'login' => 'test',
//                'password' => 'tester'
//            ]
//        );
//
//        $processInstanceId = 54321;
//
//        $eventCorrelation = new EventCorrelation();
//        $callReport = new CallReport();
//        $eventCorrelation->setAtpEventId($id);
//        $eventCorrelation->setServiceProviderEventId($processInstanceId);
//        $callReport->setClosureTimestamp(new \DateTime());
//        $callReport->setEventCause('Accident');
//        $callReport->setEventQualification('Test');
//        $callReport->setClosedBy(self::OPERATOR_NAME);
//        $response = $client->CloseEvent(['EventCorrelation' => $eventCorrelation, 'CallReport' => $callReport]);
//
//        return new Response('OK');
//    }

}
