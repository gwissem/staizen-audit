<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class EventCorrelation
{

    const PATH_ATP_EVENT_ID = '372,373';
    const PATH_CREATED_AT = '372,374';
    const PATH_UPDATED_AT = '372,375';

    /**
     * @var string $AtpEventId
     */
    protected $AtpEventId = null;

    /**
     * @var string $CustomerEventId
     */
    protected $CustomerEventId = null;

    /**
     * @var string $ServiceProviderEventId
     */
    protected $ServiceProviderEventId = null;

    /**
     * @var \DateTime $AtpCreationTimestamp
     */
    protected $AtpCreationTimestamp = null;

    /**
     * @var \DateTime $ServiceProviderCreationTimestamp
     */
    protected $ServiceProviderCreationTimestamp = null;

    /**
     * @var \DateTime $CustomerCreationTimestamp
     */
    protected $CustomerCreationTimestamp = null;

    /**
     * @var \DateTime $AtpLastUpdateTimestamp
     */
    protected $AtpLastUpdateTimestamp = null;

    /**
     * @var \DateTime $CustomerLastUpdateTimestamp
     */
    protected $CustomerLastUpdateTimestamp = null;

    /**
     * @var \DateTime $ServiceProviderLastUpdateTimestamp
     */
    protected $ServiceProviderLastUpdateTimestamp = null;


    public function __construct()
    {

    }

    public function mapPropertiesToAttributes()
    {
        return [
            self::PATH_ATP_EVENT_ID => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getAtpEventId()],
            self::PATH_CREATED_AT => ['type' => AttributeValue::VALUE_DATE, 'value' => $this->getAtpCreationTimestamp()],
            self::PATH_UPDATED_AT => ['type' => AttributeValue::VALUE_DATE, 'value' => $this->getAtpLastUpdateTimestamp()]
        ];
    }

    /**
     * @return string
     */
    public function getAtpEventId()
    {
        return $this->AtpEventId;
    }

    /**
     * @param string $AtpEventId
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setAtpEventId($AtpEventId)
    {
        $this->AtpEventId = $AtpEventId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAtpCreationTimestamp()
    {
        if ($this->AtpCreationTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->AtpCreationTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $AtpCreationTimestamp
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setAtpCreationTimestamp(\DateTime $AtpCreationTimestamp = null)
    {
        if ($AtpCreationTimestamp == null) {
            $this->AtpCreationTimestamp = null;
        } else {
            $this->AtpCreationTimestamp = $AtpCreationTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAtpLastUpdateTimestamp()
    {
        if ($this->AtpLastUpdateTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->AtpLastUpdateTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $AtpLastUpdateTimestamp
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setAtpLastUpdateTimestamp(\DateTime $AtpLastUpdateTimestamp = null)
    {
        if ($AtpLastUpdateTimestamp == null) {
            $this->AtpLastUpdateTimestamp = null;
        } else {
            $this->AtpLastUpdateTimestamp = $AtpLastUpdateTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerEventId()
    {
        return $this->CustomerEventId;
    }

    /**
     * @param string $CustomerEventId
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setCustomerEventId($CustomerEventId)
    {
        $this->CustomerEventId = $CustomerEventId;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceProviderEventId()
    {
        return $this->ServiceProviderEventId;
    }

    /**
     * @param string $ServiceProviderEventId
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setServiceProviderEventId($ServiceProviderEventId)
    {
        $this->ServiceProviderEventId = $ServiceProviderEventId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getServiceProviderCreationTimestamp()
    {
        if ($this->ServiceProviderCreationTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->ServiceProviderCreationTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $ServiceProviderCreationTimestamp
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setServiceProviderCreationTimestamp(\DateTime $ServiceProviderCreationTimestamp = null)
    {
        if ($ServiceProviderCreationTimestamp == null) {
            $this->ServiceProviderCreationTimestamp = null;
        } else {
            $this->ServiceProviderCreationTimestamp = $ServiceProviderCreationTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCustomerCreationTimestamp()
    {
        if ($this->CustomerCreationTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->CustomerCreationTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $CustomerCreationTimestamp
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setCustomerCreationTimestamp(\DateTime $CustomerCreationTimestamp = null)
    {
        if ($CustomerCreationTimestamp == null) {
            $this->CustomerCreationTimestamp = null;
        } else {
            $this->CustomerCreationTimestamp = $CustomerCreationTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCustomerLastUpdateTimestamp()
    {
        if ($this->CustomerLastUpdateTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->CustomerLastUpdateTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $CustomerLastUpdateTimestamp
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setCustomerLastUpdateTimestamp(\DateTime $CustomerLastUpdateTimestamp = null)
    {
        if ($CustomerLastUpdateTimestamp == null) {
            $this->CustomerLastUpdateTimestamp = null;
        } else {
            $this->CustomerLastUpdateTimestamp = $CustomerLastUpdateTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getServiceProviderLastUpdateTimestamp()
    {
        if ($this->ServiceProviderLastUpdateTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->ServiceProviderLastUpdateTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $ServiceProviderLastUpdateTimestamp
     * @return \SoapBundle\Model\EventCorrelation
     */
    public function setServiceProviderLastUpdateTimestamp(\DateTime $ServiceProviderLastUpdateTimestamp = null)
    {
        if ($ServiceProviderLastUpdateTimestamp == null) {
            $this->ServiceProviderLastUpdateTimestamp = null;
        } else {
            $this->ServiceProviderLastUpdateTimestamp = $ServiceProviderLastUpdateTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

}
