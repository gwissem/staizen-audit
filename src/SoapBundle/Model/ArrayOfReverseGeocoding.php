<?php

namespace SoapBundle\Model;

class ArrayOfReverseGeocoding implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ReverseGeocoding[] $ReverseGeocoding
     */
    protected $ReverseGeocoding = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ReverseGeocoding[]
     */
    public function getReverseGeocoding()
    {
      return $this->ReverseGeocoding;
    }

    /**
     * @param ReverseGeocoding[] $ReverseGeocoding
     * @return \SoapBundle\Model\ArrayOfReverseGeocoding
     */
    public function setReverseGeocoding(array $ReverseGeocoding = null)
    {
      $this->ReverseGeocoding = $ReverseGeocoding;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ReverseGeocoding[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ReverseGeocoding
     */
    public function offsetGet($offset)
    {
      return $this->ReverseGeocoding[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ReverseGeocoding $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ReverseGeocoding[] = $value;
      } else {
        $this->ReverseGeocoding[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ReverseGeocoding[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ReverseGeocoding Return the current element
     */
    public function current()
    {
      return current($this->ReverseGeocoding);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ReverseGeocoding);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ReverseGeocoding);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ReverseGeocoding);
    }

    /**
     * Countable implementation
     *
     * @return ReverseGeocoding Return count of elements
     */
    public function count()
    {
      return count($this->ReverseGeocoding);
    }

}
