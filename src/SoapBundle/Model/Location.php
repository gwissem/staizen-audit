<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class Location
{
    const PATH_LOCATION = '357,378,85';
    const PATH_LONGITUDE = '357,378,85,92';
    const PATH_LATITUDE = '357,378,85,93';
    const PATH_CITY = '357,378,85,87';
    const PATH_COUNTRY = '357,378,85,86';
    const PATH_AREA_NAME_1 = '357,378,85,88';
    const PATH_AREA_NAME_2 = '357,378,85,91';
    const PATH_AREA_NAME_3 = '357,378,85,90';
    const PATH_STREET_NAME = '357,378,85,94';
    const PATH_STREET_NUMBER = '357,378,85,95';
    const PATH_ZIP = '357,378,85,89';
    const PATH_COMMENT = '357,378,85,63';

    /**
     * @var \DateTime $CreationTimestamp
     */
    protected $CreationTimestamp = null;

    /**
     * @var int $Srid
     */
    protected $Srid = null;

    /**
     * @var Position $Position
     */
    protected $Position = null;

    /**
     * @var float $PositionAccuracy
     */
    protected $PositionAccuracy = null;

    /**
     * @var float $Altitude
     */
    protected $Altitude = null;

    /**
     * @var float $AltitudeAccuracy
     */
    protected $AltitudeAccuracy = null;

    /**
     * @var float $Heading
     */
    protected $Heading = null;

    /**
     * @var float $HeadingAccuracy
     */
    protected $HeadingAccuracy = null;

    /**
     * @var int $Speed
     */
    protected $Speed = null;

    /**
     * @var int $NumberOfSatellites
     */
    protected $NumberOfSatellites = null;

    /**
     * @var boolean $IsPositionTrustable
     */
    protected $IsPositionTrustable = null;

    /**
     * @var ArrayOfReverseGeocoding $ReverseGeocodingList
     */
    protected $ReverseGeocodingList = null;

    /**
     * @var ArrayOPosition $PositionTraces
     */
    protected $PositionTraces = null;


    public function __construct()
    {
        $this->mapPropertiesToAttributes();
    }

    public function mapPropertiesToAttributes()
    {
        $position = $this->getPosition();
        /** $traces ArrayOfPosition */
        $traces = $this->getPositionTraces();
        /** $decoded ArrayOfReverseGeocoding */
        $decoded = $this->getReverseGeocodingList();
        /** $location Location */
        $location = $decoded->getReverseGeocoding();

        // position traces
        foreach ($traces as $trace) {
            foreach ($trace as $tracePosition) {
                $locations[] = [
                    self::PATH_LONGITUDE => ['type' => AttributeValue::VALUE_STRING, 'value' => $tracePosition->getLongitude()],
                    self::PATH_LATITUDE => ['type' => AttributeValue::VALUE_STRING, 'value' => $tracePosition->getLatitude()]
                ];
            }
        }

        //current position
        $locations[] = [
            self::PATH_LONGITUDE => ['type' => AttributeValue::VALUE_STRING, 'value' => $position->getLongitude()],
            self::PATH_LATITUDE => ['type' => AttributeValue::VALUE_STRING, 'value' => $position->getLatitude()],
            self::PATH_COUNTRY => ['type' => AttributeValue::VALUE_STRING, 'value' => $location->getCountry()],
            self::PATH_CITY => ['type' => AttributeValue::VALUE_STRING, 'value' => $location->getLocality()],
            self::PATH_AREA_NAME_1 => ['type' => AttributeValue::VALUE_STRING, 'value' => $location->getAreaLevel1()],
            self::PATH_AREA_NAME_2 => ['type' => AttributeValue::VALUE_STRING, 'value' => $location->getAreaLevel2()],
            self::PATH_AREA_NAME_3 => ['type' => AttributeValue::VALUE_STRING, 'value' => $location->getAreaLevel3()],
            self::PATH_STREET_NAME => ['type' => AttributeValue::VALUE_STRING, 'value' => $location->getStreetName()],
            self::PATH_STREET_NUMBER => ['type' => AttributeValue::VALUE_STRING, 'value' => $location->getStreetNumber()],
            self::PATH_ZIP => ['type' => AttributeValue::VALUE_STRING, 'value' => $location->getZipCode()],
            self::PATH_COMMENT => ['type' => AttributeValue::VALUE_TEXT, 'value' => '']
        ];

        return $locations;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->Position;
    }

    /**
     * @param Position $Position
     * @return \SoapBundle\Model\Location
     */
    public function setPosition($Position)
    {
        $this->Position = $Position;
        return $this;
    }

    /**
     * @return ArrayOPosition
     */
    public function getPositionTraces()
    {
        return $this->PositionTraces;
    }

    /**
     * @param ArrayOPosition $PositionTraces
     * @return \SoapBundle\Model\Location
     */
    public function setPositionTraces($PositionTraces)
    {
        $this->PositionTraces = $PositionTraces;
        return $this;
    }

    /**
     * @return ArrayOfReverseGeocoding
     */
    public function getReverseGeocodingList()
    {
        return $this->ReverseGeocodingList;
    }

    /**
     * @param ArrayOfReverseGeocoding $ReverseGeocodingList
     * @return \SoapBundle\Model\Location
     */
    public function setReverseGeocodingList($ReverseGeocodingList)
    {
        $this->ReverseGeocodingList = $ReverseGeocodingList;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationTimestamp()
    {
        if ($this->CreationTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->CreationTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $CreationTimestamp
     * @return \SoapBundle\Model\Location
     */
    public function setCreationTimestamp(\DateTime $CreationTimestamp = null)
    {
        if ($CreationTimestamp == null) {
            $this->CreationTimestamp = null;
        } else {
            $this->CreationTimestamp = $CreationTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getSrid()
    {
        return $this->Srid;
    }

    /**
     * @param int $Srid
     * @return \SoapBundle\Model\Location
     */
    public function setSrid($Srid)
    {
        $this->Srid = $Srid;
        return $this;
    }

    /**
     * @return float
     */
    public function getPositionAccuracy()
    {
        return $this->PositionAccuracy;
    }

    /**
     * @param float $PositionAccuracy
     * @return \SoapBundle\Model\Location
     */
    public function setPositionAccuracy($PositionAccuracy)
    {
        $this->PositionAccuracy = $PositionAccuracy;
        return $this;
    }

    /**
     * @return float
     */
    public function getAltitude()
    {
        return $this->Altitude;
    }

    /**
     * @param float $Altitude
     * @return \SoapBundle\Model\Location
     */
    public function setAltitude($Altitude)
    {
        $this->Altitude = $Altitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getAltitudeAccuracy()
    {
        return $this->AltitudeAccuracy;
    }

    /**
     * @param float $AltitudeAccuracy
     * @return \SoapBundle\Model\Location
     */
    public function setAltitudeAccuracy($AltitudeAccuracy)
    {
        $this->AltitudeAccuracy = $AltitudeAccuracy;
        return $this;
    }

    /**
     * @return float
     */
    public function getHeading()
    {
        return $this->Heading;
    }

    /**
     * @param float $Heading
     * @return \SoapBundle\Model\Location
     */
    public function setHeading($Heading)
    {
        $this->Heading = $Heading;
        return $this;
    }

    /**
     * @return float
     */
    public function getHeadingAccuracy()
    {
        return $this->HeadingAccuracy;
    }

    /**
     * @param float $HeadingAccuracy
     * @return \SoapBundle\Model\Location
     */
    public function setHeadingAccuracy($HeadingAccuracy)
    {
        $this->HeadingAccuracy = $HeadingAccuracy;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpeed()
    {
        return $this->Speed;
    }

    /**
     * @param int $Speed
     * @return \SoapBundle\Model\Location
     */
    public function setSpeed($Speed)
    {
        $this->Speed = $Speed;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfSatellites()
    {
        return $this->NumberOfSatellites;
    }

    /**
     * @param int $NumberOfSatellites
     * @return \SoapBundle\Model\Location
     */
    public function setNumberOfSatellites($NumberOfSatellites)
    {
        $this->NumberOfSatellites = $NumberOfSatellites;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPositionTrustable()
    {
        return $this->IsPositionTrustable;
    }

    /**
     * @param boolean $IsPositionTrustable
     * @return \SoapBundle\Model\Location
     */
    public function setIsPositionTrustable($IsPositionTrustable)
    {
        $this->IsPositionTrustable = $IsPositionTrustable;
        return $this;
    }

}
