<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class Vehicle
{

    const PATH_VIN = '357,393,71';
    const PATH_LICENCE_PLATE = '357,393,72';
    const PATH_MODEL = '357,393,392';
    const PATH_MAKE = '357,393,391';
    const PATH_COLOR = '357,393,343';
    const PATH_ENERGY = '357,393,395';
    const PATH_SERIES = '357,393,394';

    /**
     * @var Vin $Vin
     */
    protected $Vin = null;

    /**
     * @var string $Make
     */
    protected $Make = null;

    /**
     * @var string $Series
     */
    protected $Series = null;

    /**
     * @var string $Model
     */
    protected $Model = null;

    /**
     * @var \DateTime $ModelDate
     */
    protected $ModelDate = null;

    /**
     * @var string $BodyType
     */
    protected $BodyType = null;

    /**
     * @var string $Color
     */
    protected $Color = null;

    /**
     * @var \DateTime $ManufactureDate
     */
    protected $ManufactureDate = null;

    /**
     * @var \DateTime $DeliveryDate
     */
    protected $DeliveryDate = null;

    /**
     * @var string $Energy
     */
    protected $Energy = null;

    /**
     * @var string $LicencePlate
     */
    protected $LicencePlate = null;

    /**
     * @var VehicleStatus $Status
     */
    protected $Status = null;

    /**
     * @var DiagnosticInfo $DiagnosticInfo
     */
    protected $DiagnosticInfo = null;


    public function __construct()
    {

    }

    public function mapPropertiesToAttributes()
    {
        $vin = $this->getVin();

        return [
            self::PATH_VIN => ['type' => AttributeValue::VALUE_STRING, 'value' => $vin->getWmi() . $vin->getVds() . $vin->getVis()],
            self::PATH_LICENCE_PLATE => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getLicencePlate()],
            self::PATH_MODEL => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getModel()],
            self::PATH_MAKE => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getMake()],
            self::PATH_COLOR => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getColor()],
            self::PATH_SERIES => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getSeries()],
            self::PATH_ENERGY => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getEnergy()]
        ];
    }

    /**
     * @return Vin
     */
    public function getVin()
    {
        return $this->Vin;
    }

    /**
     * @param Vin $Vin
     * @return \SoapBundle\Model\Vehicle
     */
    public function setVin($Vin)
    {
        $this->Vin = $Vin;
        return $this;
    }

    /**
     * @return string
     */
    public function getLicencePlate()
    {
        return $this->LicencePlate;
    }

    /**
     * @param string $LicencePlate
     * @return \SoapBundle\Model\Vehicle
     */
    public function setLicencePlate($LicencePlate)
    {
        $this->LicencePlate = $LicencePlate;
        return $this;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->Model;
    }

    /**
     * @param string $Model
     * @return \SoapBundle\Model\Vehicle
     */
    public function setModel($Model)
    {
        $this->Model = $Model;
        return $this;
    }

    /**
     * @return string
     */
    public function getMake()
    {
        return $this->Make;
    }

    /**
     * @param string $Make
     * @return \SoapBundle\Model\Vehicle
     */
    public function setMake($Make)
    {
        $this->Make = $Make;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->Color;
    }

    /**
     * @param string $Color
     * @return \SoapBundle\Model\Vehicle
     */
    public function setColor($Color)
    {
        $this->Color = $Color;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeries()
    {
        return $this->Series;
    }

    /**
     * @param string $Series
     * @return \SoapBundle\Model\Vehicle
     */
    public function setSeries($Series)
    {
        $this->Series = $Series;
        return $this;
    }

    /**
     * @return string
     */
    public function getEnergy()
    {
        return $this->Energy;
    }

    /**
     * @param string $Energy
     * @return \SoapBundle\Model\Vehicle
     */
    public function setEnergy($Energy)
    {
        $this->Energy = $Energy;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModelDate()
    {
        if ($this->ModelDate == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->ModelDate);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $ModelDate
     * @return \SoapBundle\Model\Vehicle
     */
    public function setModelDate(\DateTime $ModelDate = null)
    {
        if ($ModelDate == null) {
            $this->ModelDate = null;
        } else {
            $this->ModelDate = $ModelDate->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getBodyType()
    {
        return $this->BodyType;
    }

    /**
     * @param string $BodyType
     * @return \SoapBundle\Model\Vehicle
     */
    public function setBodyType($BodyType)
    {
        $this->BodyType = $BodyType;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getManufactureDate()
    {
        if ($this->ManufactureDate == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->ManufactureDate);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $ManufactureDate
     * @return \SoapBundle\Model\Vehicle
     */
    public function setManufactureDate(\DateTime $ManufactureDate = null)
    {
        if ($ManufactureDate == null) {
            $this->ManufactureDate = null;
        } else {
            $this->ManufactureDate = $ManufactureDate->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        if ($this->DeliveryDate == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->DeliveryDate);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $DeliveryDate
     * @return \SoapBundle\Model\Vehicle
     */
    public function setDeliveryDate(\DateTime $DeliveryDate = null)
    {
        if ($DeliveryDate == null) {
            $this->DeliveryDate = null;
        } else {
            $this->DeliveryDate = $DeliveryDate->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return VehicleStatus
     */
    public function getStatus()
    {
        return $this->Status;
    }

    /**
     * @param VehicleStatus $Status
     * @return \SoapBundle\Model\Vehicle
     */
    public function setStatus($Status)
    {
        $this->Status = $Status;
        return $this;
    }

    /**
     * @return DiagnosticInfo
     */
    public function getDiagnosticInfo()
    {
        return $this->DiagnosticInfo;
    }

    /**
     * @param DiagnosticInfo $DiagnosticInfo
     * @return \SoapBundle\Model\Vehicle
     */
    public function setDiagnosticInfo($DiagnosticInfo)
    {
        $this->DiagnosticInfo = $DiagnosticInfo;
        return $this;
    }

}
