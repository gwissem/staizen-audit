<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class VehicleStatus
{
    const PATH_ROLLED = '357,358,359';
    const PATH_CRASHED = '357,358,360';
    const PATH_AIRBAG_TRIGGERED = '357,358,361';
    const PATH_NUMBER_OF_TRIGGERED_AIRBAGS = '357,358,362';
    const PATH_TOW_TRUCK_NEED = '357,358,363';
    const PATH_THEFT_TRACK = '357,358,364';
    const PATH_ENGINE_ON = '357,358,365';
    const PATH_IGNITION_ON = '357,358,366';
    const PATH_MOVING = '357,358,367';
    const PATH_FRONT_CRASH = '357,358,381';
    const PATH_REAR_CRASH = '357,358,382';
    const PATH_SIDE_CRASH = '357,358,383';
    const PATH_DRIVER_SIDE_CRASH = '357,358,384';
    const PATH_PASSENGER_SIDE_CRASH = '357,358,385';
    const PATH_NUMBER_OF_OCCUPIED_SEATS = '357,358,386';
    const PATH_NUMBER_OF_FASTEN_SEAT_BELTS = '357,358,387';
    const PATH_DECELERATION = '357,358,388';
    const PATH_IN_ALARM = '357,358,389';
    const PATH_CRANK_INHIBITION = '357,358,390';
    const PATH_DAMAGE_TYPE = '357,358,399';
    const PATH_VICTIMS = '357,358,400';

    /**
     * @var boolean $Rolled
     */
    protected $Rolled = null;

    /**
     * @var boolean $Crashed
     */
    protected $Crashed = null;

    /**
     * @var boolean $AirbagTriggered
     */
    protected $AirbagTriggered = null;

    /**
     * @var int $NumberOfTriggeredAirbags
     */
    protected $NumberOfTriggeredAirbags = null;

    /**
     * @var boolean $TowTruckNeed
     */
    protected $TowTruckNeed = null;

    /**
     * @var boolean $TheftTrackingActivated
     */
    protected $TheftTrackingActivated = null;

    /**
     * @var boolean $EngineOn
     */
    protected $EngineOn = null;

    /**
     * @var boolean $IgnitionOn
     */
    protected $IgnitionOn = null;

    /**
     * @var boolean $Moving
     */
    protected $Moving = null;

    /**
     * @var boolean $FrontCrash
     */
    protected $FrontCrash = null;

    /**
     * @var boolean $RearCrash
     */
    protected $RearCrash = null;

    /**
     * @var boolean $SideCrash
     */
    protected $SideCrash = null;

    /**
     * @var boolean $DriverSideCrash
     */
    protected $DriverSideCrash = null;

    /**
     * @var boolean $PassengerSideCrash
     */
    protected $PassengerSideCrash = null;

    /**
     * @var boolean $InCarAlarmActivated
     */
    protected $InCarAlarmActivated = null;

    /**
     * @var int $NumberOfOccupiedSeats
     */
    protected $NumberOfOccupiedSeats = null;

    /**
     * @var int $Deceleration
     */
    protected $Deceleration = null;

    /**
     * @var int $NumberOfFastenSeatBelts
     */
    protected $NumberOfFastenSeatBelts = null;

    /**
     * @var boolean $CrankInhibitionActivated
     */
    protected $CrankInhibitionActivated = null;


    public function __construct()
    {

    }

    public function mapPropertiesToAttributes()
    {
        return [
            self::PATH_AIRBAG_TRIGGERED => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getAirbagTriggered()],
            self::PATH_CRANK_INHIBITION => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getCrankInhibitionActivated()],
            self::PATH_CRASHED => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getCrashed()],
            self::PATH_DECELERATION => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getDeceleration()],
            self::PATH_DRIVER_SIDE_CRASH => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getDriverSideCrash()],
            self::PATH_ENGINE_ON => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getEngineOn()],
            self::PATH_FRONT_CRASH => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getFrontCrash()],
            self::PATH_IGNITION_ON => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getIgnitionOn()],
            self::PATH_IN_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getInCarAlarmActivated()],
            self::PATH_MOVING => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getMoving()],
            self::PATH_NUMBER_OF_FASTEN_SEAT_BELTS => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getNumberOfFastenSeatBelts()],
            self::PATH_NUMBER_OF_OCCUPIED_SEATS => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getNumberOfOccupiedSeats()],
            self::PATH_NUMBER_OF_TRIGGERED_AIRBAGS => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getNumberOfTriggeredAirbags()],
            self::PATH_PASSENGER_SIDE_CRASH => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getPassengerSideCrash()],
            self::PATH_REAR_CRASH => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getRearCrash()],
            self::PATH_ROLLED => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getRolled()],
            self::PATH_SIDE_CRASH => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getSideCrash()],
            self::PATH_THEFT_TRACK => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getTheftTrackingActivated()],
            self::PATH_TOW_TRUCK_NEED => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getTowTruckNeed()]
        ];
    }

    /**
     * @return boolean
     */
    public function getAirbagTriggered()
    {
        return $this->AirbagTriggered;
    }

    /**
     * @param boolean $AirbagTriggered
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setAirbagTriggered($AirbagTriggered)
    {
        $this->AirbagTriggered = $AirbagTriggered;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getCrankInhibitionActivated()
    {
        return $this->CrankInhibitionActivated;
    }

    /**
     * @param boolean $CrankInhibitionActivated
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setCrankInhibitionActivated($CrankInhibitionActivated)
    {
        $this->CrankInhibitionActivated = $CrankInhibitionActivated;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getCrashed()
    {
        return $this->Crashed;
    }

    /**
     * @param boolean $Crashed
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setCrashed($Crashed)
    {
        $this->Crashed = $Crashed;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeceleration()
    {
        return $this->Deceleration;
    }

    /**
     * @param int $Deceleration
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setDeceleration($Deceleration)
    {
        $this->Deceleration = $Deceleration;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDriverSideCrash()
    {
        return $this->DriverSideCrash;
    }

    /**
     * @param boolean $DriverSideCrash
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setDriverSideCrash($DriverSideCrash)
    {
        $this->DriverSideCrash = $DriverSideCrash;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getEngineOn()
    {
        return $this->EngineOn;
    }

    /**
     * @param boolean $EngineOn
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setEngineOn($EngineOn)
    {
        $this->EngineOn = $EngineOn;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getFrontCrash()
    {
        return $this->FrontCrash;
    }

    /**
     * @param boolean $FrontCrash
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setFrontCrash($FrontCrash)
    {
        $this->FrontCrash = $FrontCrash;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIgnitionOn()
    {
        return $this->IgnitionOn;
    }

    /**
     * @param boolean $IgnitionOn
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setIgnitionOn($IgnitionOn)
    {
        $this->IgnitionOn = $IgnitionOn;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getInCarAlarmActivated()
    {
        return $this->InCarAlarmActivated;
    }

    /**
     * @param boolean $InCarAlarmActivated
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setInCarAlarmActivated($InCarAlarmActivated)
    {
        $this->InCarAlarmActivated = $InCarAlarmActivated;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getMoving()
    {
        return $this->Moving;
    }

    /**
     * @param boolean $Moving
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setMoving($Moving)
    {
        $this->Moving = $Moving;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfFastenSeatBelts()
    {
        return $this->NumberOfFastenSeatBelts;
    }

    /**
     * @param int $NumberOfFastenSeatBelts
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setNumberOfFastenSeatBelts($NumberOfFastenSeatBelts)
    {
        $this->NumberOfFastenSeatBelts = $NumberOfFastenSeatBelts;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfOccupiedSeats()
    {
        return $this->NumberOfOccupiedSeats;
    }

    /**
     * @param int $NumberOfOccupiedSeats
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setNumberOfOccupiedSeats($NumberOfOccupiedSeats)
    {
        $this->NumberOfOccupiedSeats = $NumberOfOccupiedSeats;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfTriggeredAirbags()
    {
        return $this->NumberOfTriggeredAirbags;
    }

    /**
     * @param int $NumberOfTriggeredAirbags
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setNumberOfTriggeredAirbags($NumberOfTriggeredAirbags)
    {
        $this->NumberOfTriggeredAirbags = $NumberOfTriggeredAirbags;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getPassengerSideCrash()
    {
        return $this->PassengerSideCrash;
    }

    /**
     * @param boolean $PassengerSideCrash
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setPassengerSideCrash($PassengerSideCrash)
    {
        $this->PassengerSideCrash = $PassengerSideCrash;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getRearCrash()
    {
        return $this->RearCrash;
    }

    /**
     * @param boolean $RearCrash
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setRearCrash($RearCrash)
    {
        $this->RearCrash = $RearCrash;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getRolled()
    {
        return $this->Rolled;
    }

    /**
     * @param boolean $Rolled
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setRolled($Rolled)
    {
        $this->Rolled = $Rolled;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSideCrash()
    {
        return $this->SideCrash;
    }

    /**
     * @param boolean $SideCrash
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setSideCrash($SideCrash)
    {
        $this->SideCrash = $SideCrash;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTheftTrackingActivated()
    {
        return $this->TheftTrackingActivated;
    }

    /**
     * @param boolean $TheftTrackingActivated
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setTheftTrackingActivated($TheftTrackingActivated)
    {
        $this->TheftTrackingActivated = $TheftTrackingActivated;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTowTruckNeed()
    {
        return $this->TowTruckNeed;
    }

    /**
     * @param boolean $TowTruckNeed
     * @return \SoapBundle\Model\VehicleStatus
     */
    public function setTowTruckNeed($TowTruckNeed)
    {
        $this->TowTruckNeed = $TowTruckNeed;
        return $this;
    }

}
