<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class Caller
{

    const PATH_EMAIL = '357,397,368';
    const PATH_FIRSTNAME = '357,397,64';
    const PATH_LASTNAME = '357,397,66';
    const PATH_GENDER = '357,397,355';
    const PATH_LANGUAGE = '357,397,356';
    const PATH_CALLBACK_PHONE = '357,397,197';
    const PATH_CALLBACK_PHONE_2 = '357,397,398';

    /**
     * @var string $CallbackPhoneNumber
     */
    protected $CallbackPhoneNumber = null;

    /**
     * @var string $Email
     */
    protected $Email = null;

    /**
     * @var string $FirstName
     */
    protected $FirstName = null;

    /**
     * @var string $Gender
     */
    protected $Gender = null;

    /**
     * @var string $Language
     */
    protected $Language = null;

    /**
     * @var string $LastName
     */
    protected $LastName = null;

    /**
     * @var string $MemberId
     */
    protected $MemberId = null;

    public function __construct()
    {

    }

    public function mapPropertiesToAttributes()
    {
        return [
            self::PATH_EMAIL => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getEmail()],
            self::PATH_FIRSTNAME => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getFirstName()],
            self::PATH_LASTNAME => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getLastName()],
            self::PATH_GENDER => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getGender()],
            self::PATH_LANGUAGE => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getLanguage()],
            self::PATH_CALLBACK_PHONE => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getCallbackPhoneNumber()],
        ];
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * @param string $Email
     * @return \SoapBundle\Model\Caller
     */
    public function setEmail($Email)
    {
        $this->Email = $Email;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return \SoapBundle\Model\Caller
     */
    public function setFirstName($FirstName)
    {
        $this->FirstName = $FirstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return \SoapBundle\Model\Caller
     */
    public function setLastName($LastName)
    {
        $this->LastName = $LastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->Gender;
    }

    /**
     * @param string $Gender
     * @return \SoapBundle\Model\Caller
     */
    public function setGender($Gender)
    {
        $this->Gender = $Gender;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->Language;
    }

    /**
     * @param string $Language
     * @return \SoapBundle\Model\Caller
     */
    public function setLanguage($Language)
    {
        $this->Language = $Language;
        return $this;
    }

    /**
     * @return string
     */
    public function getCallbackPhoneNumber()
    {
        return $this->CallbackPhoneNumber;
    }

    /**
     * @param string $CallbackPhoneNumber
     * @return \SoapBundle\Model\Caller
     */
    public function setCallbackPhoneNumber($CallbackPhoneNumber)
    {
        $this->CallbackPhoneNumber = $CallbackPhoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getMemberId()
    {
        return $this->MemberId;
    }

    /**
     * @param string $MemberId
     * @return \SoapBundle\Model\Caller
     */
    public function setMemberId($MemberId)
    {
        $this->MemberId = $MemberId;
        return $this;
    }

}
