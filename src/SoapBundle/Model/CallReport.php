<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class CallReport
{

    const PATH_QUALIFICATION = '372,369,370';
    const PATH_CAUSE = '372,369,371';
    const PATH_REDUNDANT_ID = '372,401';

    /**
     * @var string $ClosedBy
     */
    protected $ClosedBy = null;

    /**
     * @var string $Comment
     */
    protected $Comment = null;

    /**
     * @var \DateTime $ClosureTimestamp
     */
    protected $ClosureTimestamp = null;

    /**
     * @var \DateTime $VisualizationTimestamp
     */
    protected $VisualizationTimestamp = null;

    /**
     * @var \DateTime $PickingUpTimestamp
     */
    protected $PickingUpTimestamp = null;

    /**
     * @var int $NumberOfRings
     */
    protected $NumberOfRings = null;

    /**
     * @var boolean $DataTransmissionTooLate
     */
    protected $DataTransmissionTooLate = null;

    /**
     * @var boolean $InaccurateDiagnosticInformation
     */
    protected $InaccurateDiagnosticInformation = null;

    /**
     * @var boolean $InaccurateLocation
     */
    protected $InaccurateLocation = null;

    /**
     * @var boolean $InaccurateVehicleDescription
     */
    protected $InaccurateVehicleDescription = null;

    /**
     * @var boolean $MaskedCallerCli
     */
    protected $MaskedCallerCli = null;

    /**
     * @var boolean $OtherCitizenCalled
     */
    protected $OtherCitizenCalled = null;

    /**
     * @var boolean $OutsideTerritoryCall
     */
    protected $OutsideTerritoryCall = null;

    /**
     * @var boolean $TransmittedToBreakdownServices
     */
    protected $TransmittedToBreakdownServices = null;

    /**
     * @var boolean $TransmittedToDoctor
     */
    protected $TransmittedToDoctor = null;

    /**
     * @var boolean $TransmittedToEmergencyServices
     */
    protected $TransmittedToEmergencyServices = null;

    /**
     * @var boolean $TransmittedToRoadServices
     */
    protected $TransmittedToRoadServices = null;

    /**
     * @var string $EventQualification
     */
    protected $EventQualification = null;

    /**
     * @var string $EventCause
     */
    protected $EventCause = null;


    public function __construct()
    {

    }

    public function mapPropertiesToAttributes()
    {
        return [
            self::PATH_CAUSE => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getEventCause()],
            self::PATH_QUALIFICATION => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getEventQualification()],
        ];
    }

    /**
     * @return string
     */
    public function getEventCause()
    {
        return $this->EventCause;
    }

    /**
     * @param string $EventCause
     * @return \SoapBundle\Model\CallReport
     */
    public function setEventCause($EventCause)
    {
        $this->EventCause = $EventCause;
        return $this;
    }

    /**
     * @return string
     */
    public function getEventQualification()
    {
        return $this->EventQualification;
    }

    /**
     * @param string $EventQualification
     * @return \SoapBundle\Model\CallReport
     */
    public function setEventQualification($EventQualification)
    {
        $this->EventQualification = $EventQualification;
        return $this;
    }

    /**
     * @return string
     */
    public function getClosedBy()
    {
        return $this->ClosedBy;
    }

    /**
     * @param string $ClosedBy
     * @return \SoapBundle\Model\CallReport
     */
    public function setClosedBy($ClosedBy)
    {
        $this->ClosedBy = $ClosedBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->Comment;
    }

    /**
     * @param string $Comment
     * @return \SoapBundle\Model\CallReport
     */
    public function setComment($Comment)
    {
        $this->Comment = $Comment;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getClosureTimestamp()
    {
        if ($this->ClosureTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->ClosureTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $ClosureTimestamp
     * @return \SoapBundle\Model\CallReport
     */
    public function setClosureTimestamp(\DateTime $ClosureTimestamp = null)
    {
        if ($ClosureTimestamp == null) {
            $this->ClosureTimestamp = null;
        } else {
            $this->ClosureTimestamp = $ClosureTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getVisualizationTimestamp()
    {
        if ($this->VisualizationTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->VisualizationTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $VisualizationTimestamp
     * @return \SoapBundle\Model\CallReport
     */
    public function setVisualizationTimestamp(\DateTime $VisualizationTimestamp = null)
    {
        if ($VisualizationTimestamp == null) {
            $this->VisualizationTimestamp = null;
        } else {
            $this->VisualizationTimestamp = $VisualizationTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPickingUpTimestamp()
    {
        if ($this->PickingUpTimestamp == null) {
            return null;
        } else {
            try {
                return new \DateTime($this->PickingUpTimestamp);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @param \DateTime $PickingUpTimestamp
     * @return \SoapBundle\Model\CallReport
     */
    public function setPickingUpTimestamp(\DateTime $PickingUpTimestamp = null)
    {
        if ($PickingUpTimestamp == null) {
            $this->PickingUpTimestamp = null;
        } else {
            $this->PickingUpTimestamp = $PickingUpTimestamp->format(\DateTime::ATOM);
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfRings()
    {
        return $this->NumberOfRings;
    }

    /**
     * @param int $NumberOfRings
     * @return \SoapBundle\Model\CallReport
     */
    public function setNumberOfRings($NumberOfRings)
    {
        $this->NumberOfRings = $NumberOfRings;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDataTransmissionTooLate()
    {
        return $this->DataTransmissionTooLate;
    }

    /**
     * @param boolean $DataTransmissionTooLate
     * @return \SoapBundle\Model\CallReport
     */
    public function setDataTransmissionTooLate($DataTransmissionTooLate)
    {
        $this->DataTransmissionTooLate = $DataTransmissionTooLate;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getInaccurateDiagnosticInformation()
    {
        return $this->InaccurateDiagnosticInformation;
    }

    /**
     * @param boolean $InaccurateDiagnosticInformation
     * @return \SoapBundle\Model\CallReport
     */
    public function setInaccurateDiagnosticInformation($InaccurateDiagnosticInformation)
    {
        $this->InaccurateDiagnosticInformation = $InaccurateDiagnosticInformation;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getInaccurateLocation()
    {
        return $this->InaccurateLocation;
    }

    /**
     * @param boolean $InaccurateLocation
     * @return \SoapBundle\Model\CallReport
     */
    public function setInaccurateLocation($InaccurateLocation)
    {
        $this->InaccurateLocation = $InaccurateLocation;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getInaccurateVehicleDescription()
    {
        return $this->InaccurateVehicleDescription;
    }

    /**
     * @param boolean $InaccurateVehicleDescription
     * @return \SoapBundle\Model\CallReport
     */
    public function setInaccurateVehicleDescription($InaccurateVehicleDescription)
    {
        $this->InaccurateVehicleDescription = $InaccurateVehicleDescription;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getMaskedCallerCli()
    {
        return $this->MaskedCallerCli;
    }

    /**
     * @param boolean $MaskedCallerCli
     * @return \SoapBundle\Model\CallReport
     */
    public function setMaskedCallerCli($MaskedCallerCli)
    {
        $this->MaskedCallerCli = $MaskedCallerCli;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getOtherCitizenCalled()
    {
        return $this->OtherCitizenCalled;
    }

    /**
     * @param boolean $OtherCitizenCalled
     * @return \SoapBundle\Model\CallReport
     */
    public function setOtherCitizenCalled($OtherCitizenCalled)
    {
        $this->OtherCitizenCalled = $OtherCitizenCalled;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getOutsideTerritoryCall()
    {
        return $this->OutsideTerritoryCall;
    }

    /**
     * @param boolean $OutsideTerritoryCall
     * @return \SoapBundle\Model\CallReport
     */
    public function setOutsideTerritoryCall($OutsideTerritoryCall)
    {
        $this->OutsideTerritoryCall = $OutsideTerritoryCall;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTransmittedToBreakdownServices()
    {
        return $this->TransmittedToBreakdownServices;
    }

    /**
     * @param boolean $TransmittedToBreakdownServices
     * @return \SoapBundle\Model\CallReport
     */
    public function setTransmittedToBreakdownServices($TransmittedToBreakdownServices)
    {
        $this->TransmittedToBreakdownServices = $TransmittedToBreakdownServices;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTransmittedToDoctor()
    {
        return $this->TransmittedToDoctor;
    }

    /**
     * @param boolean $TransmittedToDoctor
     * @return \SoapBundle\Model\CallReport
     */
    public function setTransmittedToDoctor($TransmittedToDoctor)
    {
        $this->TransmittedToDoctor = $TransmittedToDoctor;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTransmittedToEmergencyServices()
    {
        return $this->TransmittedToEmergencyServices;
    }

    /**
     * @param boolean $TransmittedToEmergencyServices
     * @return \SoapBundle\Model\CallReport
     */
    public function setTransmittedToEmergencyServices($TransmittedToEmergencyServices)
    {
        $this->TransmittedToEmergencyServices = $TransmittedToEmergencyServices;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTransmittedToRoadServices()
    {
        return $this->TransmittedToRoadServices;
    }

    /**
     * @param boolean $TransmittedToRoadServices
     * @return \SoapBundle\Model\CallReport
     */
    public function setTransmittedToRoadServices($TransmittedToRoadServices)
    {
        $this->TransmittedToRoadServices = $TransmittedToRoadServices;
        return $this;
    }

}
