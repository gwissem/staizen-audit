<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class Event
{
    const PATH_ARC_TWIN_URL = '372,377';
    const PATH_TYPE = '372,376';
    const PATH_ACTIVATION = '372,396';
    const PATH_CALLER_ID = '357,197';

    /**
     * @var EventCorrelation $Correlation
     */
    protected $Correlation = null;

    /**
     * @var string $Type
     */
    protected $Type = null;

    /**
     * @var string $Status
     */
    protected $Status = null;

    /**
     * @var string $ActivationMethod
     */
    protected $ActivationMethod = null;

    /**
     * @var string $CallerId
     */
    protected $CallerId = null;

    /**
     * @var string $Qualification
     */
    protected $Qualification = null;

    /**
     * @var string $Cause
     */
    protected $Cause = null;

    /**
     * @var anyURI $PresentationUrl
     */
    protected $PresentationUrl = null;

    /**
     * @var Caller $Caller
     */
    protected $Caller = null;

    /**
     * @var Vehicle $Vehicle
     */
    protected $Vehicle = null;

    /**
     * @var Location $Location
     */
    protected $Location = null;

    /**
     * @var Device $Device
     */
    protected $Device = null;

    /**
     * @param string $Type
     * @param string $Status
     * @param string $ActivationMethod
     * @param string $CallerId
     */
    public function __construct($Type, $Status, $ActivationMethod, $CallerId)
    {
        $this->Type = $Type;
        $this->Status = $Status;
        $this->ActivationMethod = $ActivationMethod;
        $this->CallerId = $CallerId;
    }

    public function mapPropertiesToAttributes()
    {
        return [
            self::PATH_TYPE => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getType()],
            self::PATH_ARC_TWIN_URL => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getPresentationUrl()],
            self::PATH_CALLER_ID => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getCallerId()],
            self::PATH_ACTIVATION => ['type' => AttributeValue::VALUE_STRING, 'value' => $this->getActivationMethod()]
        ];
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     * @param string $Type
     * @return \SoapBundle\Model\Event
     */
    public function setType($Type)
    {
        $this->Type = $Type;
        return $this;
    }

    /**
     * @return anyURI
     */
    public function getPresentationUrl()
    {
        return $this->PresentationUrl;
    }

    /**
     * @param anyURI $PresentationUrl
     * @return \SoapBundle\Model\Event
     */
    public function setPresentationUrl($PresentationUrl)
    {
        $this->PresentationUrl = $PresentationUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getCallerId()
    {
        return $this->CallerId;
    }

    /**
     * @param string $CallerId
     * @return \SoapBundle\Model\Event
     */
    public function setCallerId($CallerId)
    {
        $this->CallerId = $CallerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getActivationMethod()
    {
        return $this->ActivationMethod;
    }

    /**
     * @param string $ActivationMethod
     * @return \SoapBundle\Model\Event
     */
    public function setActivationMethod($ActivationMethod)
    {
        $this->ActivationMethod = $ActivationMethod;
        return $this;
    }

    /**
     * @return EventCorrelation
     */
    public function getCorrelation()
    {
        return $this->Correlation;
    }

    /**
     * @param EventCorrelation $Correlation
     * @return \SoapBundle\Model\Event
     */
    public function setCorrelation($Correlation)
    {
        $this->Correlation = $Correlation;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->Status;
    }

    /**
     * @param string $Status
     * @return \SoapBundle\Model\Event
     */
    public function setStatus($Status)
    {
        $this->Status = $Status;
        return $this;
    }

    /**
     * @return string
     */
    public function getQualification()
    {
        return $this->Qualification;
    }

    /**
     * @param string $Qualification
     * @return \SoapBundle\Model\Event
     */
    public function setQualification($Qualification)
    {
        $this->Qualification = $Qualification;
        return $this;
    }

    /**
     * @return string
     */
    public function getCause()
    {
        return $this->Cause;
    }

    /**
     * @param string $Cause
     * @return \SoapBundle\Model\Event
     */
    public function setCause($Cause)
    {
        $this->Cause = $Cause;
        return $this;
    }

    /**
     * @return Caller
     */
    public function getCaller()
    {
        return $this->Caller;
    }

    /**
     * @param Caller $Caller
     * @return \SoapBundle\Model\Event
     */
    public function setCaller($Caller)
    {
        $this->Caller = $Caller;
        return $this;
    }

    /**
     * @return Vehicle
     */
    public function getVehicle()
    {
        return $this->Vehicle;
    }

    /**
     * @param Vehicle $Vehicle
     * @return \SoapBundle\Model\Event
     */
    public function setVehicle($Vehicle)
    {
        $this->Vehicle = $Vehicle;
        return $this;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->Location;
    }

    /**
     * @param Location $Location
     * @return \SoapBundle\Model\Event
     */
    public function setLocation($Location)
    {
        $this->Location = $Location;
        return $this;
    }

    /**
     * @return Device
     */
    public function getDevice()
    {
        return $this->Device;
    }

    /**
     * @param Device $Device
     * @return \SoapBundle\Model\Event
     */
    public function setDevice($Device)
    {
        $this->Device = $Device;
        return $this;
    }

}
