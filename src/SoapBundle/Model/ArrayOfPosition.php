<?php

namespace SoapBundle\Model;

class ArrayOfPosition implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Position[] $Position
     */
    protected $Position = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Position[]
     */
    public function getPosition()
    {
      return $this->Position;
    }

    /**
     * @param Position[] $Position
     * @return \SoapBundle\Model\ArrayOfPosition
     */
    public function setPosition(array $Position = null)
    {
      $this->Position = $Position;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Position[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Position
     */
    public function offsetGet($offset)
    {
      return $this->Position[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Position $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->Position[] = $value;
      } else {
        $this->Position[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Position[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Position Return the current element
     */
    public function current()
    {
      return current($this->Position);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Position);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Position);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Position);
    }

    /**
     * Countable implementation
     *
     * @return Position Return count of elements
     */
    public function count()
    {
      return count($this->Position);
    }

}
