<?php

namespace SoapBundle\Model;

class Service
{

    /**
     * @var string $ProjectCode
     */
    protected $ProjectCode = null;

    /**
     * @var string $BrandCode
     */
    protected $BrandCode = null;

    /**
     * @var string $AssistanceType
     */
    protected $AssistanceType = null;

    /**
     * @var string $Role
     */
    protected $Role = null;

    /**
     * @param string $ProjectCode
     * @param string $AssistanceType
     */
    public function __construct($ProjectCode, $AssistanceType)
    {
      $this->ProjectCode = $ProjectCode;
      $this->AssistanceType = $AssistanceType;
    }

    /**
     * @return string
     */
    public function getProjectCode()
    {
      return $this->ProjectCode;
    }

    /**
     * @param string $ProjectCode
     * @return \SoapBundle\Model\Service
     */
    public function setProjectCode($ProjectCode)
    {
      $this->ProjectCode = $ProjectCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBrandCode()
    {
      return $this->BrandCode;
    }

    /**
     * @param string $BrandCode
     * @return \SoapBundle\Model\Service
     */
    public function setBrandCode($BrandCode)
    {
      $this->BrandCode = $BrandCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAssistanceType()
    {
      return $this->AssistanceType;
    }

    /**
     * @param string $AssistanceType
     * @return \SoapBundle\Model\Service
     */
    public function setAssistanceType($AssistanceType)
    {
      $this->AssistanceType = $AssistanceType;
      return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
      return $this->Role;
    }

    /**
     * @param string $Role
     * @return \SoapBundle\Model\Service
     */
    public function setRole($Role)
    {
      $this->Role = $Role;
      return $this;
    }

}
