<?php

namespace SoapBundle\Model;

class Vin
{

    /**
     * @var string $Wmi
     */
    protected $Wmi = null;

    /**
     * @var string $Vds
     */
    protected $Vds = null;

    /**
     * @var string $Vis
     */
    protected $Vis = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getWmi()
    {
      return $this->Wmi;
    }

    /**
     * @param string $Wmi
     * @return \SoapBundle\Model\Vin
     */
    public function setWmi($Wmi)
    {
      $this->Wmi = $Wmi;
      return $this;
    }

    /**
     * @return string
     */
    public function getVds()
    {
      return $this->Vds;
    }

    /**
     * @param string $Vds
     * @return \SoapBundle\Model\Vin
     */
    public function setVds($Vds)
    {
      $this->Vds = $Vds;
      return $this;
    }

    /**
     * @return string
     */
    public function getVis()
    {
      return $this->Vis;
    }

    /**
     * @param string $Vis
     * @return \SoapBundle\Model\Vin
     */
    public function setVis($Vis)
    {
      $this->Vis = $Vis;
      return $this;
    }

}
