<?php

namespace SoapBundle\Model;

class ReverseGeocoding
{

    /**
     * @var string $Source
     */
    protected $Source = null;

    /**
     * @var string $FormattedAddress
     */
    protected $FormattedAddress = null;

    /**
     * @var string $StreetNumber
     */
    protected $StreetNumber = null;

    /**
     * @var string $StreetName
     */
    protected $StreetName = null;

    /**
     * @var string $Locality
     */
    protected $Locality = null;

    /**
     * @var string $ZipCode
     */
    protected $ZipCode = null;

    /**
     * @var string $AreaLevel3
     */
    protected $AreaLevel3 = null;

    /**
     * @var string $AreaLevel2
     */
    protected $AreaLevel2 = null;

    /**
     * @var string $AreaLevel1
     */
    protected $AreaLevel1 = null;

    /**
     * @var string $Country
     */
    protected $Country = null;

    /**
     * @var \DateTime $CreationTimestamp
     */
    protected $CreationTimestamp = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSource()
    {
      return $this->Source;
    }

    /**
     * @param string $Source
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setSource($Source)
    {
      $this->Source = $Source;
      return $this;
    }

    /**
     * @return string
     */
    public function getFormattedAddress()
    {
      return $this->FormattedAddress;
    }

    /**
     * @param string $FormattedAddress
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setFormattedAddress($FormattedAddress)
    {
      $this->FormattedAddress = $FormattedAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreetNumber()
    {
      return $this->StreetNumber;
    }

    /**
     * @param string $StreetNumber
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setStreetNumber($StreetNumber)
    {
      $this->StreetNumber = $StreetNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreetName()
    {
      return $this->StreetName;
    }

    /**
     * @param string $StreetName
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setStreetName($StreetName)
    {
      $this->StreetName = $StreetName;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocality()
    {
      return $this->Locality;
    }

    /**
     * @param string $Locality
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setLocality($Locality)
    {
      $this->Locality = $Locality;
      return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
      return $this->ZipCode;
    }

    /**
     * @param string $ZipCode
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setZipCode($ZipCode)
    {
      $this->ZipCode = $ZipCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAreaLevel3()
    {
      return $this->AreaLevel3;
    }

    /**
     * @param string $AreaLevel3
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setAreaLevel3($AreaLevel3)
    {
      $this->AreaLevel3 = $AreaLevel3;
      return $this;
    }

    /**
     * @return string
     */
    public function getAreaLevel2()
    {
      return $this->AreaLevel2;
    }

    /**
     * @param string $AreaLevel2
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setAreaLevel2($AreaLevel2)
    {
      $this->AreaLevel2 = $AreaLevel2;
      return $this;
    }

    /**
     * @return string
     */
    public function getAreaLevel1()
    {
      return $this->AreaLevel1;
    }

    /**
     * @param string $AreaLevel1
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setAreaLevel1($AreaLevel1)
    {
      $this->AreaLevel1 = $AreaLevel1;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
      return $this->Country;
    }

    /**
     * @param string $Country
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setCountry($Country)
    {
      $this->Country = $Country;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationTimestamp()
    {
      if ($this->CreationTimestamp == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CreationTimestamp);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CreationTimestamp
     * @return \SoapBundle\Model\ReverseGeocoding
     */
    public function setCreationTimestamp(\DateTime $CreationTimestamp = null)
    {
      if ($CreationTimestamp == null) {
       $this->CreationTimestamp = null;
      } else {
        $this->CreationTimestamp = $CreationTimestamp->format(\DateTime::ATOM);
      }
      return $this;
    }

}
