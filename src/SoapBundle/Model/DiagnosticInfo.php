<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class DiagnosticInfo
{

    const PATH_ABS_ALARM = '357,345,344';
    const PATH_AIRBAG_ALARM = '357,345,346';
    const PATH_ANTI_THEFT_ALARM = '357,345,347';
    const PATH_BATTERY_ALARM = '357,345,348';
    const PATH_BATTERY_LEVEL_ALARM = '357,345,349';
    const PATH_BREAK_ALARM = '357,345,350';
    const PATH_BREAK_LEVEL_ALARM = '357,345,351';
    const PATH_BREAK_PAD_ALARM = '357,345,352';
    const PATH_CRASH_ALARM = '357,345,353';
    const PATH_ENGINE_ALARM = '357,345,354';

    /**
     * @var boolean $AbsAlarm
     */
    protected $AbsAlarm = null;

    /**
     * @var boolean $AirbagAlarm
     */
    protected $AirbagAlarm = null;

    /**
     * @var boolean $AntiTheftAlarm
     */
    protected $AntiTheftAlarm = null;

    /**
     * @var boolean $BatteryAlarm
     */
    protected $BatteryAlarm = null;

    /**
     * @var int $BatteryChargingPercentage
     */
    protected $BatteryChargingPercentage = null;

    /**
     * @var boolean $BrakeAlarm
     */
    protected $BrakeAlarm = null;

    /**
     * @var boolean $BrakeLevelAlarm
     */
    protected $BrakeLevelAlarm = null;

    /**
     * @var boolean $BrakePadAlarm
     */
    protected $BrakePadAlarm = null;

    /**
     * @var boolean $CrashAlarm
     */
    protected $CrashAlarm = null;

    /**
     * @var boolean $EngineAlarm
     */
    protected $EngineAlarm = null;

    /**
     * @var boolean $EobdAlarm
     */
    protected $EobdAlarm = null;

    /**
     * @var boolean $EspAlarm
     */
    protected $EspAlarm = null;

    /**
     * @var boolean $FluidPressureAlarm
     */
    protected $FluidPressureAlarm = null;

    /**
     * @var boolean $FpsFluid2Alarm
     */
    protected $FpsFluid2Alarm = null;

    /**
     * @var boolean $FpsFluidLevelAlarm
     */
    protected $FpsFluidLevelAlarm = null;

    /**
     * @var boolean $FpsFullAlarm
     */
    protected $FpsFullAlarm = null;

    /**
     * @var int $FuelEconomy
     */
    protected $FuelEconomy = null;

    /**
     * @var int $FuelLevelPercentage
     */
    protected $FuelLevelPercentage = null;

    /**
     * @var int $FuelRange
     */
    protected $FuelRange = null;

    /**
     * @var boolean $GearOilTempAlarm
     */
    protected $GearOilTempAlarm = null;

    /**
     * @var boolean $GearboxAlarm
     */
    protected $GearboxAlarm = null;

    /**
     * @var int $Kilometers
     */
    protected $Kilometers = null;

    /**
     * @var int $LpgLevelPercentage
     */
    protected $LpgLevelPercentage = null;

    /**
     * @var int $LpgRange
     */
    protected $LpgRange = null;

    /**
     * @var int $OilLevel
     */
    protected $OilLevel = null;

    /**
     * @var boolean $OilLevelAlarm
     */
    protected $OilLevelAlarm = null;

    /**
     * @var boolean $OilPressureAlarm
     */
    protected $OilPressureAlarm = null;

    /**
     * @var int $OilTemperature
     */
    protected $OilTemperature = null;

    /**
     * @var boolean $OilTemperatureAlarm
     */
    protected $OilTemperatureAlarm = null;

    /**
     * @var boolean $ParkBrakeAlarm
     */
    protected $ParkBrakeAlarm = null;

    /**
     * @var int $ServiceDays
     */
    protected $ServiceDays = null;

    /**
     * @var int $ServiceRange
     */
    protected $ServiceRange = null;

    /**
     * @var string $StarterStatus
     */
    protected $StarterStatus = null;

    /**
     * @var boolean $Steering2Alarm
     */
    protected $Steering2Alarm = null;

    /**
     * @var boolean $SteeringAlarm
     */
    protected $SteeringAlarm = null;

    /**
     * @var boolean $Suspension2Alarm
     */
    protected $Suspension2Alarm = null;

    /**
     * @var boolean $Suspension3Alarm
     */
    protected $Suspension3Alarm = null;

    /**
     * @var boolean $SuspensionMajorAlarm
     */
    protected $SuspensionMajorAlarm = null;

    /**
     * @var boolean $SuspensionSpeedAlarm
     */
    protected $SuspensionSpeedAlarm = null;

    /**
     * @var boolean $TireFrontLeftAlarm
     */
    protected $TireFrontLeftAlarm = null;

    /**
     * @var float $TireFrontLeftDeltaPressure
     */
    protected $TireFrontLeftDeltaPressure = null;

    /**
     * @var float $TireFrontLeftPressure
     */
    protected $TireFrontLeftPressure = null;

    /**
     * @var boolean $TireFrontRightAlarm
     */
    protected $TireFrontRightAlarm = null;

    /**
     * @var float $TireFrontRightDeltaPressure
     */
    protected $TireFrontRightDeltaPressure = null;

    /**
     * @var float $TireFrontRightPressure
     */
    protected $TireFrontRightPressure = null;

    /**
     * @var boolean $TirePressureAlarm
     */
    protected $TirePressureAlarm = null;

    /**
     * @var boolean $TireRearLeftAlarm
     */
    protected $TireRearLeftAlarm = null;

    /**
     * @var float $TireRearLeftDeltaPressure
     */
    protected $TireRearLeftDeltaPressure = null;

    /**
     * @var float $TireRearLeftPressure
     */
    protected $TireRearLeftPressure = null;

    /**
     * @var boolean $TireRearRightAlarm
     */
    protected $TireRearRightAlarm = null;

    /**
     * @var float $TireRearRightDeltaPressure
     */
    protected $TireRearRightDeltaPressure = null;

    /**
     * @var float $TireRearRightPressure
     */
    protected $TireRearRightPressure = null;

    /**
     * @var boolean $WaterInFuelAlarm
     */
    protected $WaterInFuelAlarm = null;

    /**
     * @var boolean $WaterLevelAlarm
     */
    protected $WaterLevelAlarm = null;

    /**
     * @var int $WaterTemperature
     */
    protected $WaterTemperature = null;

    /**
     * @var boolean $WaterTemperatureAlarm
     */
    protected $WaterTemperatureAlarm = null;


    public function __construct()
    {

    }

    public function mapPropertiesToAttributes()
    {
        return [
            self::PATH_ABS_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getAbsAlarm()],
            self::PATH_AIRBAG_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getAirbagAlarm()],
            self::PATH_ANTI_THEFT_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getAntiTheftAlarm()],
            self::PATH_BATTERY_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getBatteryAlarm()],
            self::PATH_BATTERY_LEVEL_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getBatteryChargingPercentage()],
            self::PATH_BREAK_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getBrakeAlarm()],
            self::PATH_BREAK_LEVEL_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getBrakeLevelAlarm()],
            self::PATH_BREAK_PAD_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getBrakePadAlarm()],
            self::PATH_CRASH_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getCrashAlarm()],
            self::PATH_ENGINE_ALARM => ['type' => AttributeValue::VALUE_INT, 'value' => $this->getEngineAlarm()],
        ];
    }

    /**
     * @return boolean
     */
    public function getAbsAlarm()
    {
        return $this->AbsAlarm;
    }

    /**
     * @param boolean $AbsAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setAbsAlarm($AbsAlarm)
    {
        $this->AbsAlarm = $AbsAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getAirbagAlarm()
    {
        return $this->AirbagAlarm;
    }

    /**
     * @param boolean $AirbagAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setAirbagAlarm($AirbagAlarm)
    {
        $this->AirbagAlarm = $AirbagAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getAntiTheftAlarm()
    {
        return $this->AntiTheftAlarm;
    }

    /**
     * @param boolean $AntiTheftAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setAntiTheftAlarm($AntiTheftAlarm)
    {
        $this->AntiTheftAlarm = $AntiTheftAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBatteryAlarm()
    {
        return $this->BatteryAlarm;
    }

    /**
     * @param boolean $BatteryAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setBatteryAlarm($BatteryAlarm)
    {
        $this->BatteryAlarm = $BatteryAlarm;
        return $this;
    }

    /**
     * @return int
     */
    public function getBatteryChargingPercentage()
    {
        return $this->BatteryChargingPercentage;
    }

    /**
     * @param int $BatteryChargingPercentage
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setBatteryChargingPercentage($BatteryChargingPercentage)
    {
        $this->BatteryChargingPercentage = $BatteryChargingPercentage;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBrakeAlarm()
    {
        return $this->BrakeAlarm;
    }

    /**
     * @param boolean $BrakeAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setBrakeAlarm($BrakeAlarm)
    {
        $this->BrakeAlarm = $BrakeAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBrakeLevelAlarm()
    {
        return $this->BrakeLevelAlarm;
    }

    /**
     * @param boolean $BrakeLevelAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setBrakeLevelAlarm($BrakeLevelAlarm)
    {
        $this->BrakeLevelAlarm = $BrakeLevelAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBrakePadAlarm()
    {
        return $this->BrakePadAlarm;
    }

    /**
     * @param boolean $BrakePadAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setBrakePadAlarm($BrakePadAlarm)
    {
        $this->BrakePadAlarm = $BrakePadAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getCrashAlarm()
    {
        return $this->CrashAlarm;
    }

    /**
     * @param boolean $CrashAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setCrashAlarm($CrashAlarm)
    {
        $this->CrashAlarm = $CrashAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getEngineAlarm()
    {
        return $this->EngineAlarm;
    }

    /**
     * @param boolean $EngineAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setEngineAlarm($EngineAlarm)
    {
        $this->EngineAlarm = $EngineAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getEobdAlarm()
    {
        return $this->EobdAlarm;
    }

    /**
     * @param boolean $EobdAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setEobdAlarm($EobdAlarm)
    {
        $this->EobdAlarm = $EobdAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getEspAlarm()
    {
        return $this->EspAlarm;
    }

    /**
     * @param boolean $EspAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setEspAlarm($EspAlarm)
    {
        $this->EspAlarm = $EspAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getFluidPressureAlarm()
    {
        return $this->FluidPressureAlarm;
    }

    /**
     * @param boolean $FluidPressureAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setFluidPressureAlarm($FluidPressureAlarm)
    {
        $this->FluidPressureAlarm = $FluidPressureAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getFpsFluid2Alarm()
    {
        return $this->FpsFluid2Alarm;
    }

    /**
     * @param boolean $FpsFluid2Alarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setFpsFluid2Alarm($FpsFluid2Alarm)
    {
        $this->FpsFluid2Alarm = $FpsFluid2Alarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getFpsFluidLevelAlarm()
    {
        return $this->FpsFluidLevelAlarm;
    }

    /**
     * @param boolean $FpsFluidLevelAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setFpsFluidLevelAlarm($FpsFluidLevelAlarm)
    {
        $this->FpsFluidLevelAlarm = $FpsFluidLevelAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getFpsFullAlarm()
    {
        return $this->FpsFullAlarm;
    }

    /**
     * @param boolean $FpsFullAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setFpsFullAlarm($FpsFullAlarm)
    {
        $this->FpsFullAlarm = $FpsFullAlarm;
        return $this;
    }

    /**
     * @return int
     */
    public function getFuelEconomy()
    {
        return $this->FuelEconomy;
    }

    /**
     * @param int $FuelEconomy
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setFuelEconomy($FuelEconomy)
    {
        $this->FuelEconomy = $FuelEconomy;
        return $this;
    }

    /**
     * @return int
     */
    public function getFuelLevelPercentage()
    {
        return $this->FuelLevelPercentage;
    }

    /**
     * @param int $FuelLevelPercentage
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setFuelLevelPercentage($FuelLevelPercentage)
    {
        $this->FuelLevelPercentage = $FuelLevelPercentage;
        return $this;
    }

    /**
     * @return int
     */
    public function getFuelRange()
    {
        return $this->FuelRange;
    }

    /**
     * @param int $FuelRange
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setFuelRange($FuelRange)
    {
        $this->FuelRange = $FuelRange;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getGearOilTempAlarm()
    {
        return $this->GearOilTempAlarm;
    }

    /**
     * @param boolean $GearOilTempAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setGearOilTempAlarm($GearOilTempAlarm)
    {
        $this->GearOilTempAlarm = $GearOilTempAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getGearboxAlarm()
    {
        return $this->GearboxAlarm;
    }

    /**
     * @param boolean $GearboxAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setGearboxAlarm($GearboxAlarm)
    {
        $this->GearboxAlarm = $GearboxAlarm;
        return $this;
    }

    /**
     * @return int
     */
    public function getKilometers()
    {
        return $this->Kilometers;
    }

    /**
     * @param int $Kilometers
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setKilometers($Kilometers)
    {
        $this->Kilometers = $Kilometers;
        return $this;
    }

    /**
     * @return int
     */
    public function getLpgLevelPercentage()
    {
        return $this->LpgLevelPercentage;
    }

    /**
     * @param int $LpgLevelPercentage
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setLpgLevelPercentage($LpgLevelPercentage)
    {
        $this->LpgLevelPercentage = $LpgLevelPercentage;
        return $this;
    }

    /**
     * @return int
     */
    public function getLpgRange()
    {
        return $this->LpgRange;
    }

    /**
     * @param int $LpgRange
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setLpgRange($LpgRange)
    {
        $this->LpgRange = $LpgRange;
        return $this;
    }

    /**
     * @return int
     */
    public function getOilLevel()
    {
        return $this->OilLevel;
    }

    /**
     * @param int $OilLevel
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setOilLevel($OilLevel)
    {
        $this->OilLevel = $OilLevel;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getOilLevelAlarm()
    {
        return $this->OilLevelAlarm;
    }

    /**
     * @param boolean $OilLevelAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setOilLevelAlarm($OilLevelAlarm)
    {
        $this->OilLevelAlarm = $OilLevelAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getOilPressureAlarm()
    {
        return $this->OilPressureAlarm;
    }

    /**
     * @param boolean $OilPressureAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setOilPressureAlarm($OilPressureAlarm)
    {
        $this->OilPressureAlarm = $OilPressureAlarm;
        return $this;
    }

    /**
     * @return int
     */
    public function getOilTemperature()
    {
        return $this->OilTemperature;
    }

    /**
     * @param int $OilTemperature
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setOilTemperature($OilTemperature)
    {
        $this->OilTemperature = $OilTemperature;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getOilTemperatureAlarm()
    {
        return $this->OilTemperatureAlarm;
    }

    /**
     * @param boolean $OilTemperatureAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setOilTemperatureAlarm($OilTemperatureAlarm)
    {
        $this->OilTemperatureAlarm = $OilTemperatureAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getParkBrakeAlarm()
    {
        return $this->ParkBrakeAlarm;
    }

    /**
     * @param boolean $ParkBrakeAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setParkBrakeAlarm($ParkBrakeAlarm)
    {
        $this->ParkBrakeAlarm = $ParkBrakeAlarm;
        return $this;
    }

    /**
     * @return int
     */
    public function getServiceDays()
    {
        return $this->ServiceDays;
    }

    /**
     * @param int $ServiceDays
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setServiceDays($ServiceDays)
    {
        $this->ServiceDays = $ServiceDays;
        return $this;
    }

    /**
     * @return int
     */
    public function getServiceRange()
    {
        return $this->ServiceRange;
    }

    /**
     * @param int $ServiceRange
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setServiceRange($ServiceRange)
    {
        $this->ServiceRange = $ServiceRange;
        return $this;
    }

    /**
     * @return string
     */
    public function getStarterStatus()
    {
        return $this->StarterStatus;
    }

    /**
     * @param string $StarterStatus
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setStarterStatus($StarterStatus)
    {
        $this->StarterStatus = $StarterStatus;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSteering2Alarm()
    {
        return $this->Steering2Alarm;
    }

    /**
     * @param boolean $Steering2Alarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setSteering2Alarm($Steering2Alarm)
    {
        $this->Steering2Alarm = $Steering2Alarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSteeringAlarm()
    {
        return $this->SteeringAlarm;
    }

    /**
     * @param boolean $SteeringAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setSteeringAlarm($SteeringAlarm)
    {
        $this->SteeringAlarm = $SteeringAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSuspension2Alarm()
    {
        return $this->Suspension2Alarm;
    }

    /**
     * @param boolean $Suspension2Alarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setSuspension2Alarm($Suspension2Alarm)
    {
        $this->Suspension2Alarm = $Suspension2Alarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSuspension3Alarm()
    {
        return $this->Suspension3Alarm;
    }

    /**
     * @param boolean $Suspension3Alarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setSuspension3Alarm($Suspension3Alarm)
    {
        $this->Suspension3Alarm = $Suspension3Alarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSuspensionMajorAlarm()
    {
        return $this->SuspensionMajorAlarm;
    }

    /**
     * @param boolean $SuspensionMajorAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setSuspensionMajorAlarm($SuspensionMajorAlarm)
    {
        $this->SuspensionMajorAlarm = $SuspensionMajorAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSuspensionSpeedAlarm()
    {
        return $this->SuspensionSpeedAlarm;
    }

    /**
     * @param boolean $SuspensionSpeedAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setSuspensionSpeedAlarm($SuspensionSpeedAlarm)
    {
        $this->SuspensionSpeedAlarm = $SuspensionSpeedAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTireFrontLeftAlarm()
    {
        return $this->TireFrontLeftAlarm;
    }

    /**
     * @param boolean $TireFrontLeftAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireFrontLeftAlarm($TireFrontLeftAlarm)
    {
        $this->TireFrontLeftAlarm = $TireFrontLeftAlarm;
        return $this;
    }

    /**
     * @return float
     */
    public function getTireFrontLeftDeltaPressure()
    {
        return $this->TireFrontLeftDeltaPressure;
    }

    /**
     * @param float $TireFrontLeftDeltaPressure
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireFrontLeftDeltaPressure($TireFrontLeftDeltaPressure)
    {
        $this->TireFrontLeftDeltaPressure = $TireFrontLeftDeltaPressure;
        return $this;
    }

    /**
     * @return float
     */
    public function getTireFrontLeftPressure()
    {
        return $this->TireFrontLeftPressure;
    }

    /**
     * @param float $TireFrontLeftPressure
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireFrontLeftPressure($TireFrontLeftPressure)
    {
        $this->TireFrontLeftPressure = $TireFrontLeftPressure;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTireFrontRightAlarm()
    {
        return $this->TireFrontRightAlarm;
    }

    /**
     * @param boolean $TireFrontRightAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireFrontRightAlarm($TireFrontRightAlarm)
    {
        $this->TireFrontRightAlarm = $TireFrontRightAlarm;
        return $this;
    }

    /**
     * @return float
     */
    public function getTireFrontRightDeltaPressure()
    {
        return $this->TireFrontRightDeltaPressure;
    }

    /**
     * @param float $TireFrontRightDeltaPressure
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireFrontRightDeltaPressure($TireFrontRightDeltaPressure)
    {
        $this->TireFrontRightDeltaPressure = $TireFrontRightDeltaPressure;
        return $this;
    }

    /**
     * @return float
     */
    public function getTireFrontRightPressure()
    {
        return $this->TireFrontRightPressure;
    }

    /**
     * @param float $TireFrontRightPressure
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireFrontRightPressure($TireFrontRightPressure)
    {
        $this->TireFrontRightPressure = $TireFrontRightPressure;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTirePressureAlarm()
    {
        return $this->TirePressureAlarm;
    }

    /**
     * @param boolean $TirePressureAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTirePressureAlarm($TirePressureAlarm)
    {
        $this->TirePressureAlarm = $TirePressureAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTireRearLeftAlarm()
    {
        return $this->TireRearLeftAlarm;
    }

    /**
     * @param boolean $TireRearLeftAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireRearLeftAlarm($TireRearLeftAlarm)
    {
        $this->TireRearLeftAlarm = $TireRearLeftAlarm;
        return $this;
    }

    /**
     * @return float
     */
    public function getTireRearLeftDeltaPressure()
    {
        return $this->TireRearLeftDeltaPressure;
    }

    /**
     * @param float $TireRearLeftDeltaPressure
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireRearLeftDeltaPressure($TireRearLeftDeltaPressure)
    {
        $this->TireRearLeftDeltaPressure = $TireRearLeftDeltaPressure;
        return $this;
    }

    /**
     * @return float
     */
    public function getTireRearLeftPressure()
    {
        return $this->TireRearLeftPressure;
    }

    /**
     * @param float $TireRearLeftPressure
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireRearLeftPressure($TireRearLeftPressure)
    {
        $this->TireRearLeftPressure = $TireRearLeftPressure;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getTireRearRightAlarm()
    {
        return $this->TireRearRightAlarm;
    }

    /**
     * @param boolean $TireRearRightAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireRearRightAlarm($TireRearRightAlarm)
    {
        $this->TireRearRightAlarm = $TireRearRightAlarm;
        return $this;
    }

    /**
     * @return float
     */
    public function getTireRearRightDeltaPressure()
    {
        return $this->TireRearRightDeltaPressure;
    }

    /**
     * @param float $TireRearRightDeltaPressure
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireRearRightDeltaPressure($TireRearRightDeltaPressure)
    {
        $this->TireRearRightDeltaPressure = $TireRearRightDeltaPressure;
        return $this;
    }

    /**
     * @return float
     */
    public function getTireRearRightPressure()
    {
        return $this->TireRearRightPressure;
    }

    /**
     * @param float $TireRearRightPressure
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setTireRearRightPressure($TireRearRightPressure)
    {
        $this->TireRearRightPressure = $TireRearRightPressure;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getWaterInFuelAlarm()
    {
        return $this->WaterInFuelAlarm;
    }

    /**
     * @param boolean $WaterInFuelAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setWaterInFuelAlarm($WaterInFuelAlarm)
    {
        $this->WaterInFuelAlarm = $WaterInFuelAlarm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getWaterLevelAlarm()
    {
        return $this->WaterLevelAlarm;
    }

    /**
     * @param boolean $WaterLevelAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setWaterLevelAlarm($WaterLevelAlarm)
    {
        $this->WaterLevelAlarm = $WaterLevelAlarm;
        return $this;
    }

    /**
     * @return int
     */
    public function getWaterTemperature()
    {
        return $this->WaterTemperature;
    }

    /**
     * @param int $WaterTemperature
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setWaterTemperature($WaterTemperature)
    {
        $this->WaterTemperature = $WaterTemperature;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getWaterTemperatureAlarm()
    {
        return $this->WaterTemperatureAlarm;
    }

    /**
     * @param boolean $WaterTemperatureAlarm
     * @return \SoapBundle\Model\DiagnosticInfo
     */
    public function setWaterTemperatureAlarm($WaterTemperatureAlarm)
    {
        $this->WaterTemperatureAlarm = $WaterTemperatureAlarm;
        return $this;
    }

}
