<?php

namespace SoapBundle\Model;

use CaseBundle\Entity\AttributeValue;

class Position
{
    /**
     * @var float $Latitude
     */
    protected $Latitude = null;

    /**
     * @var float $Longitude
     */
    protected $Longitude = null;


    public function __construct()
    {

    }


    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->Longitude;
    }

    /**
     * @param float $Longitude
     * @return \SoapBundle\Model\Position
     */
    public function setLongitude($Longitude)
    {
        $this->Longitude = $Longitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->Latitude;
    }

    /**
     * @param float $Latitude
     * @return \SoapBundle\Model\Position
     */
    public function setLatitude($Latitude)
    {
        $this->Latitude = $Latitude;
        return $this;
    }

}
