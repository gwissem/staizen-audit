<?php

namespace SoapBundle\Model;

class Device
{

    /**
     * @var string $ApplicationBuild
     */
    protected $ApplicationBuild = null;

    /**
     * @var string $ApplicationLanguage
     */
    protected $ApplicationLanguage = null;

    /**
     * @var string $ApplicationName
     */
    protected $ApplicationName = null;

    /**
     * @var string $ApplicationVersion
     */
    protected $ApplicationVersion = null;

    /**
     * @var string $HardwareVersion
     */
    protected $HardwareVersion = null;

    /**
     * @var string $Id
     */
    protected $Id = null;

    /**
     * @var string $Make
     */
    protected $Make = null;

    /**
     * @var string $Model
     */
    protected $Model = null;

    /**
     * @var string $OperatingSystem
     */
    protected $OperatingSystem = null;

    /**
     * @var string $OperatingSystemBuild
     */
    protected $OperatingSystemBuild = null;

    /**
     * @var string $OperatingSystemLanguage
     */
    protected $OperatingSystemLanguage = null;

    /**
     * @var string $OperatingSystemVersion
     */
    protected $OperatingSystemVersion = null;

    /**
     * @var string $Series
     */
    protected $Series = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getApplicationBuild()
    {
      return $this->ApplicationBuild;
    }

    /**
     * @param string $ApplicationBuild
     * @return \SoapBundle\Model\Device
     */
    public function setApplicationBuild($ApplicationBuild)
    {
      $this->ApplicationBuild = $ApplicationBuild;
      return $this;
    }

    /**
     * @return string
     */
    public function getApplicationLanguage()
    {
      return $this->ApplicationLanguage;
    }

    /**
     * @param string $ApplicationLanguage
     * @return \SoapBundle\Model\Device
     */
    public function setApplicationLanguage($ApplicationLanguage)
    {
      $this->ApplicationLanguage = $ApplicationLanguage;
      return $this;
    }

    /**
     * @return string
     */
    public function getApplicationName()
    {
      return $this->ApplicationName;
    }

    /**
     * @param string $ApplicationName
     * @return \SoapBundle\Model\Device
     */
    public function setApplicationName($ApplicationName)
    {
      $this->ApplicationName = $ApplicationName;
      return $this;
    }

    /**
     * @return string
     */
    public function getApplicationVersion()
    {
      return $this->ApplicationVersion;
    }

    /**
     * @param string $ApplicationVersion
     * @return \SoapBundle\Model\Device
     */
    public function setApplicationVersion($ApplicationVersion)
    {
      $this->ApplicationVersion = $ApplicationVersion;
      return $this;
    }

    /**
     * @return string
     */
    public function getHardwareVersion()
    {
      return $this->HardwareVersion;
    }

    /**
     * @param string $HardwareVersion
     * @return \SoapBundle\Model\Device
     */
    public function setHardwareVersion($HardwareVersion)
    {
      $this->HardwareVersion = $HardwareVersion;
      return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return \SoapBundle\Model\Device
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getMake()
    {
      return $this->Make;
    }

    /**
     * @param string $Make
     * @return \SoapBundle\Model\Device
     */
    public function setMake($Make)
    {
      $this->Make = $Make;
      return $this;
    }

    /**
     * @return string
     */
    public function getModel()
    {
      return $this->Model;
    }

    /**
     * @param string $Model
     * @return \SoapBundle\Model\Device
     */
    public function setModel($Model)
    {
      $this->Model = $Model;
      return $this;
    }

    /**
     * @return string
     */
    public function getOperatingSystem()
    {
      return $this->OperatingSystem;
    }

    /**
     * @param string $OperatingSystem
     * @return \SoapBundle\Model\Device
     */
    public function setOperatingSystem($OperatingSystem)
    {
      $this->OperatingSystem = $OperatingSystem;
      return $this;
    }

    /**
     * @return string
     */
    public function getOperatingSystemBuild()
    {
      return $this->OperatingSystemBuild;
    }

    /**
     * @param string $OperatingSystemBuild
     * @return \SoapBundle\Model\Device
     */
    public function setOperatingSystemBuild($OperatingSystemBuild)
    {
      $this->OperatingSystemBuild = $OperatingSystemBuild;
      return $this;
    }

    /**
     * @return string
     */
    public function getOperatingSystemLanguage()
    {
      return $this->OperatingSystemLanguage;
    }

    /**
     * @param string $OperatingSystemLanguage
     * @return \SoapBundle\Model\Device
     */
    public function setOperatingSystemLanguage($OperatingSystemLanguage)
    {
      $this->OperatingSystemLanguage = $OperatingSystemLanguage;
      return $this;
    }

    /**
     * @return string
     */
    public function getOperatingSystemVersion()
    {
      return $this->OperatingSystemVersion;
    }

    /**
     * @param string $OperatingSystemVersion
     * @return \SoapBundle\Model\Device
     */
    public function setOperatingSystemVersion($OperatingSystemVersion)
    {
      $this->OperatingSystemVersion = $OperatingSystemVersion;
      return $this;
    }

    /**
     * @return string
     */
    public function getSeries()
    {
      return $this->Series;
    }

    /**
     * @param string $Series
     * @return \SoapBundle\Model\Device
     */
    public function setSeries($Series)
    {
      $this->Series = $Series;
      return $this;
    }

}
