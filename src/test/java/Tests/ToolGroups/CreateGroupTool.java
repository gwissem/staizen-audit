package Tests.ToolGroups;

import Data.Users;
import Pages.BasePage;
import Pages.LoginPage;
import Pages.ToolGroupPage;
import Tests.AbstractTest;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CreateGroupTool extends AbstractTest {

    public final String groupNamePL = "Grupa testowa-" + ToolGroupPage.getCurrentDate();
    public final String groupNameEN = "Testing Group-" + ToolGroupPage.getCurrentDate();
    public String notificationToolGroupCreatePL = "Grupa narzędzi \"" + groupNamePL + "\" została pomyślnie utworzona";

    @Test
    public void shouldCreateToolGroup(){
        LoginPage loginPage = new LoginPage();
        loginPage.login(Users.LOGIN_ADMIN, Users.PASS_ADMIN);
        ToolGroupPage toolGroupPage = new ToolGroupPage();
        toolGroupPage.goToPage();
        Assert.assertEquals("Page url is not correct", toolGroupPage.path, toolGroupPage.currentUrl());
        toolGroupPage.driver.switchTo().frame(toolGroupPage.atlasIfram());
        Assert.assertTrue(toolGroupPage.addGroupButton().isDisplayed());
        toolGroupPage.createGroup( groupNamePL, groupNameEN);
        Assert.assertEquals("Tool group is not create - notification is not displayed",  notificationToolGroupCreatePL, toolGroupPage.notification().getText());
    }

    @Test
    public void shouldNotCreateGroupWhenUseExistingNamePL(){
        LoginPage loginPage = new LoginPage();
        loginPage.login(Users.LOGIN_ADMIN, Users.PASS_ADMIN);
        ToolGroupPage toolGroupPage = new ToolGroupPage();
        toolGroupPage.goToPage();
        Assert.assertEquals("Page url is not correct", toolGroupPage.path, toolGroupPage.currentUrl());
        toolGroupPage.driver.switchTo().frame(toolGroupPage.atlasIfram());
        Assert.assertTrue(toolGroupPage.addGroupButton().isDisplayed());
        toolGroupPage.createGroup(groupNamePL, groupNameEN);
        toolGroupPage.createGroup(groupNamePL, groupNameEN + "1");
        Assert.assertTrue("Count of alerts is not correct",  toolGroupPage.existingGroupAllert().size() == 1);
    }

    @Test
    public void shouldNotCreateGroupWhenUseExistingNameEN(){
        LoginPage loginPage = new LoginPage();
        loginPage.login(Users.LOGIN_ADMIN, Users.PASS_ADMIN);
        ToolGroupPage toolGroupPage = new ToolGroupPage();
        toolGroupPage.goToPage();
        Assert.assertEquals("Page url is not correct", toolGroupPage.path, toolGroupPage.currentUrl());
        toolGroupPage.driver.switchTo().frame(toolGroupPage.atlasIfram());
        Assert.assertTrue(toolGroupPage.addGroupButton().isDisplayed());
        toolGroupPage.createGroup(groupNamePL, groupNameEN);
        toolGroupPage.createGroup(groupNamePL + "1", groupNameEN);
        Assert.assertTrue("Count of alerts is not correct",  toolGroupPage.existingGroupAllert().size() == 1);
    }

    @Test
    public void shouldNotCreateGroupWhenUseExistingNamePLandEN(){
        LoginPage loginPage = new LoginPage();
        loginPage.login(Users.LOGIN_ADMIN, Users.PASS_ADMIN);
        ToolGroupPage toolGroupPage = new ToolGroupPage();
        toolGroupPage.goToPage();
        Assert.assertEquals("Page url is not correct", toolGroupPage.path, toolGroupPage.currentUrl());
        toolGroupPage.driver.switchTo().frame(toolGroupPage.atlasIfram());
        Assert.assertTrue(toolGroupPage.addGroupButton().isDisplayed());
        toolGroupPage.createGroup(groupNamePL, groupNameEN);
        toolGroupPage.createGroup(groupNamePL, groupNameEN);
        Assert.assertTrue("Count of alerts is not correct",  toolGroupPage.existingGroupAllert().size() == 2);
    }

}