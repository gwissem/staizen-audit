package Tests.BenefitsPanel;

import Data.Users;
import Helpers.DbManagement;
import Pages.BenefitsPanel;
import Pages.LoginPage;
import Pages.OperationDashboardPage;
import SandBox.NaprawaHolowanieData;
import Tests.AbstractTest;
import Pages.ProcesesPages.NowaSprawa.StepId042;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.sql.SQLException;

public class RepairAndTowing extends AbstractTest{

    int CASE_ID = 0;

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    //PO declaration
    OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
    DbManagement db = new DbManagement(DB_NAME);
    NaprawaHolowanieData xyz = new NaprawaHolowanieData(DB_NAME);
    BenefitsPanel benefitsPanelPage = new BenefitsPanel();
    StepId042 choiceBenefits = new StepId042();

    //Data constans
    int AUTOSTRADA = 1;
    int MIASTO = 0;
    int WYPADEK = 1;
    int AWARIA = 0;
    String[] localisationPoznan = {"16.915305", "52.430286"};

    //VW osobowe

    @Test
    public void a1() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwOsobowePanelSwiadczen(AWARIA,MIASTO, "AH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void a2() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwOsobowePanelSwiadczen(AWARIA,MIASTO, "APN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsActive();
        benefitsPanelPage.verifyAreTowingContainerIsInactive();
    }

    @Test
    public void a3() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwOsobowePanelSwiadczen(AWARIA,MIASTO, "AN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsActive();
        benefitsPanelPage.verifyAreTowingContainerIsInactive();
    }

    @Test
    public void a4() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwOsobowePanelSwiadczen(AWARIA, AUTOSTRADA, "APN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void a5() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwOsobowePanelSwiadczen(AWARIA, AUTOSTRADA, "AH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void a6() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwOsobowePanelSwiadczen(AWARIA, AUTOSTRADA, "AN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void a7() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwOsobowePanelSwiadczen(WYPADEK, AUTOSTRADA, "WH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void a8() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwOsobowePanelSwiadczen(WYPADEK, MIASTO, "WH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    //AUDI

    @Test
    public void audi() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.audiPanelSwiadczen(WYPADEK, MIASTO, "WH", 1620, 1, localisationPoznan );
        goToCase();
        //goto benefits panel from localisation
        operationDashboardPage.nextButton().click();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void audi2() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.audiPanelSwiadczen(AWARIA,MIASTO, "AN", 1620, 1, localisationPoznan );
        goToCase();
        //goto choice benefits from localisation
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.elementToBeClickable(choiceBenefits.paragraphWhichBenefits()));
        //click towing
        choiceBenefits.radioButtonTowing().click();
        //goto benefits panel
        operationDashboardPage.nextButton().click();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void audi3() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.audiPanelSwiadczen(AWARIA,MIASTO, "AN", 1620, 1, localisationPoznan );
        goToCase();
        //goto choice benefits from localisation
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.elementToBeClickable(choiceBenefits.paragraphWhichBenefits()));
        //click repair
        choiceBenefits.radioButtonRepair().click();
        //goto benefits panel
        operationDashboardPage.nextButton().click();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsActive();
//        Assert.assertTrue("Repair container is active", benefitsPanelPage.repairContainerActive().isDisplayed());
//        Assert.assertTrue("Towing activation button is not displayed", benefitsPanelPage.repairActivateServiceButton().isDisplayed());
        benefitsPanelPage.verifyAreTowingContainerIsInactive();
    }

    @Test
    public void audi4() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.audiPanelSwiadczen(AWARIA,MIASTO, "APN", 1620, 1, localisationPoznan );
        goToCase();
        //goto choice benefits from localisation
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.elementToBeClickable(choiceBenefits.paragraphWhichBenefits()));
        //click towing
        choiceBenefits.radioButtonTowing().click();
        //goto benefits panel
        operationDashboardPage.nextButton().click();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void audi5() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.audiPanelSwiadczen(AWARIA,MIASTO, "APN", 1620, 1, localisationPoznan );
        goToCase();
        //goto choice benefits from localisation
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.elementToBeClickable(choiceBenefits.paragraphWhichBenefits()));
        //click repair
        choiceBenefits.radioButtonRepair().click();
        //goto benefits panel
        operationDashboardPage.nextButton().click();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsActive();
//        Assert.assertTrue("Repair container is active", benefitsPanelPage.repairContainerActive().isDisplayed());
//        Assert.assertTrue("Towing activation button is not displayed", benefitsPanelPage.repairActivateServiceButton().isDisplayed());
        benefitsPanelPage.verifyAreTowingContainerIsInactive();
    }

    @Test
    public void audi6() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 1620, 1, localisationPoznan );
        goToCase();
        //goto benefits panel from localisation
        operationDashboardPage.nextButton().click();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void audi7() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.audiPanelSwiadczen(AWARIA, AUTOSTRADA, "AN", 1620, 1, localisationPoznan );
        goToCase();
        //goto benefits panel from localisation
        operationDashboardPage.nextButton().click();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    //SKODA

    @Test
    public void skoda() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.skodaPanelSwiadczen(AWARIA,MIASTO, "AH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void skoda1() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.skodaPanelSwiadczen(AWARIA,MIASTO, "APN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsActive();
        benefitsPanelPage.verifyAreTowingContainerIsInactive();
    }

    @Test
    public void skoda2() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.skodaPanelSwiadczen(AWARIA,MIASTO, "AN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsActive();
        benefitsPanelPage.verifyAreTowingContainerIsInactive();
    }

    @Test
    public void skoda3() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.skodaPanelSwiadczen(AWARIA, AUTOSTRADA, "APN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void skoda4() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.skodaPanelSwiadczen(AWARIA, AUTOSTRADA, "AH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void skoda5() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.skodaPanelSwiadczen(AWARIA, AUTOSTRADA, "AN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void skoda6() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.skodaPanelSwiadczen(WYPADEK, AUTOSTRADA, "WH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void skoda7() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.skodaPanelSwiadczen(WYPADEK, MIASTO, "WH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    //VW uzytkowe

    @Test
    public void vwUzytkowe() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwUzytkowePanelSwiadczen(WYPADEK, MIASTO, "WH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    @Test
    public void vwUzytkowe1() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwUzytkowePanelSwiadczen(AWARIA, MIASTO, "APN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsActive();
        benefitsPanelPage.verifyAreTowingContainerIsInactive();
    }

    @Test
    public void vwUzytkowe2() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwUzytkowePanelSwiadczen(AWARIA, MIASTO, "AN", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsActive();
        benefitsPanelPage.verifyAreTowingContainerIsInactive();
    }

    @Test
    public void vwUzytkowe3() throws SQLException, ClassNotFoundException {
        CASE_ID = xyz.vwUzytkowePanelSwiadczen(AWARIA, MIASTO, "AH", 1620, 1, localisationPoznan );
        goToCase();
        benefitsPanelPage.waitForBenefitsPanelIsLoaded();
        benefitsPanelPage.verifyAreRepairContainerIsInactive();
        benefitsPanelPage.verifyAreTowingContainerIsActive();
    }

    //METHOD

    private void goToCase() {
        String caseId = Integer.toString(CASE_ID);
        new LoginPage().login(Users.LOGIN_JANUSZ_TESTER, Users.PASS_JANUSZ);
        operationDashboardPage.goToPage();
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(caseId);
    }
}
