package Tests.Bwb;

import Data.Users;
import Helpers.DbManagement;
import Helpers.Form;
import Helpers.GenerateData;
import Pages.ProcesesPages.Bwb.*;
import Pages.ProcesesPages.NowaSprawa.StepId098;
import Tests.AbstractTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Bwb extends AbstractTest{

    int CASE_ID = 0;
    //Page declaration
   DbManagement db = new DbManagement(DB_NAME);
    Form formHelper = new Form();
    GenerateData generateData = new GenerateData();
    StepId002 jakiPakietAssistanceForm = new StepId002();
    StepId004 czyKupionyNowyWsalonieForm = new StepId004();
    StepId005 uKtoregoDealerForm = new StepId005();
    StepId006 dataZakupuForm = new StepId006();
    StepId015 warunkowePotwierdzenieSwiadczen = new StepId015();
    StepId003 czyPrzegladyUPSa = new StepId003();
    StepId010 dataOstatniegoPrzegladu = new StepId010();
    StepId012 przebiegZprzegladu = new StepId012();
    StepId007 uslugiPlatne = new StepId007();

    String REGISTRATION_DATE = getCurrentDate();
    String REGISTRATION_DATE_SQL = getCurrentDateSQLFormat();

    String VW = "vwOsobowe";
    String AUDI = "audi";
    String SKODA = "skoda";

   //Declaration new Car
    String[] carNew = {"PZ650ux","WVWZZZ3CZDE511113", REGISTRATION_DATE, "120", REGISTRATION_DATE_SQL};
    String[] carOld = {"PL650ux","WVWZZZ3CZDE511112", "09-21-2015", "91222", "2015-09-21"};

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

   @Test
    public void vwNewCarMobilityWarrantyHappyPath() throws SQLException, ClassNotFoundException {
       //Set car < 2 years
       CASE_ID = goToCase(carNew, VW);
       _newCarMobilityWarrantyHappyPath();
   }

    @Test
    public void skodaNewCarMobilityWarrantyHappyPath() throws SQLException, ClassNotFoundException {
        //Set car < 2 years
        CASE_ID = goToCase(carNew, SKODA);
        _newCarMobilityWarrantyHappyPath();
    }

    private void _newCarMobilityWarrantyHappyPath() {
        //Set warranty - new car
        jakiPakietAssistanceForm.radioButtonContainerInput("1").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(czyKupionyNowyWsalonieForm.paragraphWhereDidYouBuyTheCar()));
        //Set where buy car - from dealer in PL
        czyKupionyNowyWsalonieForm.radioButtonContainerInput("1").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(uKtoregoDealerForm.paragraphWhichDealer()));
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(dataZakupuForm.paragraphBuyCarDate()));
        //set date when car was buy
        dataZakupuForm.inputBuyCarDate().sendKeys(carNew[2]);
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(warunkowePotwierdzenieSwiadczen.paragraphWhitInformation()));
    }

    @Test
    public void vwNewCarOtherWarrantyHappyPath() throws SQLException, ClassNotFoundException {
        //Set car < 2 years
        CASE_ID = goToCase(carNew, VW);
        _newCarOtherWarrantyHappyPath();
    }

    @Test
    public void skodaNewCarOtherWarrantyHappyPath() throws SQLException, ClassNotFoundException {
        //Set car < 2 years
        CASE_ID = goToCase(carNew, SKODA);
        _newCarOtherWarrantyHappyPath();
    }

    private void _newCarOtherWarrantyHappyPath() {
        //Set warranty - new car
        jakiPakietAssistanceForm.radioButtonContainerInput("10").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(czyKupionyNowyWsalonieForm.paragraphWhereDidYouBuyTheCar()));
        //Set where buy car - from dealer in PL
        czyKupionyNowyWsalonieForm.radioButtonContainerInput("1").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(uKtoregoDealerForm.paragraphWhichDealer()));
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(dataZakupuForm.paragraphBuyCarDate()));
        //set date when car was buy
        dataZakupuForm.inputBuyCarDate().sendKeys(carNew[2]);
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(warunkowePotwierdzenieSwiadczen.paragraphWhitInformation()));
    }

    @Test
   public void vwOldCarResignationFromHelp() throws SQLException, ClassNotFoundException {
       CASE_ID = goToCase(carOld, VW);
       _oldCarResignationFromHelp();
   }

    @Test
    public void skodaOldCarResignationFromHelp() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase(carOld, SKODA);
        _oldCarResignationFromHelp();
    }

    private void _oldCarResignationFromHelp() {
        //Set warranty - resignation from help
        jakiPakietAssistanceForm.radioButtonContainerInput("0").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //verification path of process - 1011.98 - Anuluj Proces
        wait.until(ExpectedConditions.visibilityOf(new StepId098().paragraphContactReason()));
    }

    @Test
   public void vwOldCarWarrantyMobilityHappyPath() throws SQLException, ClassNotFoundException {
       CASE_ID = goToCase(carOld, VW);
       _oldCarWarrantyMobilityHappyPath();
   }

    @Test
    public void skodaOldCarWarrantyMobilityHappyPath() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase(carOld, SKODA);
        _oldCarWarrantyMobilityHappyPath();
    }

    private void _oldCarWarrantyMobilityHappyPath() {
        //Set warranty - warranty mobility
        jakiPakietAssistanceForm.radioButtonContainerInput("2").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(czyPrzegladyUPSa.paragraphWhereInspection()));
        //Click on Yes option
        czyPrzegladyUPSa.radioButtonContainerInput("1").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //set PS service
        wait.until(ExpectedConditions.elementToBeClickable(uKtoregoDealerForm.paragraphWhichDealer()));
        //goto next step
        wait.until(ExpectedConditions.elementToBeClickable(operationDashboardPage.nextButton()));
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //When was do inspection
        wait.until(ExpectedConditions.visibilityOf(dataOstatniegoPrzegladu.paragraphWhenWasLastInspection()));
        Assert.assertTrue("Date last inspection input is not display", dataOstatniegoPrzegladu.inputDateLastInspectionFrom().isDisplayed());
        dataOstatniegoPrzegladu.inputDateLastInspectionFrom().sendKeys(pastDate());
        Actions actions = new Actions(driver);
        actions.moveToElement(dataOstatniegoPrzegladu.paragraphWhenWasLastInspection()).perform();
        System.out.println("Data ktora jest wpisywana" + pastDate());
        //goto next step
        actions.moveToElement(dataOstatniegoPrzegladu.paragraphWhenWasLastInspection()).click().perform();
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //Mileage from inspection
        wait.until(ExpectedConditions.visibilityOf(przebiegZprzegladu.paragraphWhatMileage()));
        przebiegZprzegladu.inputMileageFromInspection().sendKeys(mileageFromInspectionCorrect(carOld[3]));
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //verification correct path process
        wait.until(ExpectedConditions.visibilityOf(warunkowePotwierdzenieSwiadczen.paragraphWhitInformation()));
    }

    @Test
    public void vwOldCarWarrantyMobilityToHighMileageFromInspection() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase(carOld, VW);
        _oldCarWarrantyMobilityToHighMileageFromInspection();
    }

    @Test
    public void skodaOldCarWarrantyMobilityToHighMileageFromInspection() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase(carOld, SKODA);
        _oldCarWarrantyMobilityToHighMileageFromInspection();
    }

    private void _oldCarWarrantyMobilityToHighMileageFromInspection() {
        //Set warranty - warranty mobility
        jakiPakietAssistanceForm.radioButtonContainerInput("2").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(czyPrzegladyUPSa.paragraphWhereInspection()));
        //Clink on Yes option
        czyPrzegladyUPSa.radioButtonContainerInput("1").click();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //set PS service
        wait.until(ExpectedConditions.visibilityOf(uKtoregoDealerForm.paragraphWhichDealer()));
        //goto next step
        wait.until(ExpectedConditions.elementToBeClickable(operationDashboardPage.nextButton()));
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //When was do inspection
        wait.until(ExpectedConditions.visibilityOf(dataOstatniegoPrzegladu.paragraphWhenWasLastInspection()));
        Assert.assertTrue("Date last inspection input is not display", dataOstatniegoPrzegladu.inputDateLastInspectionFrom().isDisplayed());
        dataOstatniegoPrzegladu.inputDateLastInspectionFrom().sendKeys(pastDate());
        Actions actions = new Actions(driver);
        actions.moveToElement(dataOstatniegoPrzegladu.paragraphWhenWasLastInspection()).click().perform();
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //Mileage from inspection
        wait.until(ExpectedConditions.visibilityOf(przebiegZprzegladu.paragraphWhatMileage()));
        przebiegZprzegladu.inputMileageFromInspection().sendKeys(mileageFromInspectionIncorrect(carOld[3]));
        //goto next step
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        //verification correct path process
        wait.until(ExpectedConditions.visibilityOf(uslugiPlatne.paragraphOfferPaidService()));
    }

    public int goToCase(String[] car, String platformName) throws SQLException, ClassNotFoundException {
        String phoneNumber = generateData.phoneNumber();
        String firstName = generateData.firstName();
        String lastName = generateData.lastName();
        String caseId = db.createNewCaseBwb(firstName, lastName, phoneNumber, car, platformName);
        loginPage.login(Users.LOGIN_JANUSZ, Users.PASS_JANUSZ);
        operationDashboardPage.goToPage();
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(caseId);
        return Integer.parseInt(caseId);
    }

    private String pastDate(){
        Calendar originalDate = Calendar.getInstance();
        Calendar previousMonthDay = (Calendar) originalDate.clone();
        previousMonthDay.add(Calendar.MONTH, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return sdf.format(previousMonthDay.getTime());
    }

    private String mileageFromInspection(String currentMileage, int pastMileage){
        int mileageInt = Integer.parseInt(currentMileage);
        return Integer.toString(mileageInt - pastMileage);
    }

    private String mileageFromInspectionCorrect(String currentMileage){
        return mileageFromInspection(currentMileage, 10000);
    }

    private String mileageFromInspectionIncorrect(String currentMileage){
        return mileageFromInspection(currentMileage, 31000);
    }
}
