package Tests;

import Helpers.DbManagement;
import Pages.BasePage;
import Tests.Login.Login;
import Tests.ToolGroups.CreateGroupTool;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.sql.SQLException;

@RunWith(Suite.class)
@Suite.SuiteClasses({Login.class, CreateGroupTool.class, ProcessesTestSuite.class })
public class TestSuite {


//    @BeforeClass
//    public static void updatUserJanusz() throws SQLException, ClassNotFoundException {
//        new DbManagement().updatJanuszNietypowy();
//    }
}
