package Tests.ScenarioFromOperationTeam;

import Data.Cars.Skoda;
import Data.Users;
import Helpers.DbManagement;
import Helpers.Form;
import Helpers.GenerateData;
import Pages.ProcesesPages.DiagnosisByPhone;
import Pages.ProcesesPages.NowaSprawa.StepId002;
import Pages.ProcesesPages.NowaSprawa.StepId006;
import Pages.ProcesesPages.NowaSprawa.StepId015;
import Pages.ProcesesPages.NowaSprawa.StepId018;
import Tests.AbstractTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.sql.SQLException;

public class SkodaPlatform extends AbstractTest{

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    //PO declaration
    DbManagement db = new DbManagement(DB_NAME);
    GenerateData generateData = new GenerateData();
    StepId006 enterDriverData = new StepId006();
    Form formHelper = new Form();
    StepId015 formulaSafety = new StepId015();
    StepId002 basicVehicleDataForm = new StepId002();
    StepId018 repairByPhoneStepForm = new StepId018();
    DiagnosisByPhone diagnosisByPhone = new DiagnosisByPhone();

    //Variable declaration
    int CASE_ID = 0;
    String PHONE_NUMBER =   "";
    String FIRST_NAME =   "";
    String LAST_NAME =   "";

    @Test
    public void scenario1() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //Are you driver
        enterDriverData.radioButtonAreYouDriverYes().click();
        goToNextStep();
        //Formula safety
        formulaSafety.radioButtonCrashNo().click();
        wait.until(ExpectedConditions.elementToBeClickable(formulaSafety.paragraphRout()));
        formulaSafety.radioButtonRoutNo().click();
        goToNextStep();
        //Basic vehicle data
        //fill registration number input
        basicVehicleDataForm.inputRegistrationNumber().sendKeys("WI275EF");
        //fill vin input
        basicVehicleDataForm.inputVin().sendKeys("TMBAB7NE2G0110876");
        //fill first registration input
        basicVehicleDataForm.inputRegistrationDate().sendKeys("17-11-2015");
        //fill Current Mileage input
        basicVehicleDataForm.inputCurrentMileage().sendKeys("104500");
        goToNextStep();
        //Permission confirm
        //checking number of elements
        Assert.assertEquals("Number of element in form is different of declaration", 8, formHelper.formContainer().size());
        goToNextStep();
        //Repair By phone
        repairByPhoneStepForm.radioButtonSuccesRepairNo().click();
        repairByPhoneStepForm.closeSabioWindows();
//        wait.until(ExpectedConditions.elementToBeClickable(repairByPhoneStepForm.radioButtonSuccesRepairNo()));
        goToNextStep();
        //Diagnosis by phone
        //Określ rodzaj awarii
        diagnosisByPhone.silnik().click();
        formHelper.waitForLoaderExperimental();
        //Sprecyzuj objawy
//        wait.until(ExpectedConditions.visibilityOf(diagnosisByPhone.nieMoznaUruchomicSilnika()));
        wait.until(ExpectedConditions.elementToBeClickable(diagnosisByPhone.nieMoznaUruchomicSilnika()));
        diagnosisByPhone.nieMoznaUruchomicSilnika().click();
        formHelper.waitForLoaderExperimental();
        //Czy słychać rozrusznik
        diagnosisByPhone.nieSlychacRozrusznika().click();
        //Jak długo pojazd nie był odpalany
        diagnosisByPhone.ponadTydzien().click();
        //opis diagnozy
        Assert.assertTrue(diagnosisByPhone.inputDiagnosisDescription().isDisplayed());
        Assert.assertTrue(diagnosisByPhone.summaryDiagnosisContainer().isDisplayed());
        goToNextStep();
        //
    }

    public void goToNextStep() {
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
    }

    private int goToCase() throws SQLException, ClassNotFoundException {
        PHONE_NUMBER = generateData.phoneNumber();
        FIRST_NAME = generateData.firstName();
        LAST_NAME = generateData.lastName();
//        db.processInstanceSetInactive();
        int caseId = db.createNewCaseEnterDriverData(FIRST_NAME, LAST_NAME, PHONE_NUMBER);
        loginPage.login(Users.LOGIN_JANUSZ_TESTER, Users.PASS_JANUSZ);
        operationDashboardPage.goToPage();
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(Integer.toString(caseId));
        return caseId;
    }
}
