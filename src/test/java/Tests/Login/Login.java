package Tests.Login;

import Data.Users;
import Helpers.DbManagement;
import Pages.*;
import Tests.AbstractTest;
import com.sun.jna.platform.win32.Netapi32Util;
import org.junit.*;
import org.openqa.selenium.By;

import java.sql.SQLException;


public class Login extends AbstractTest {

    @BeforeClass
    public static void updatUserJanusz() throws SQLException, ClassNotFoundException {
        new DbManagement(DB_NAME).updatJanuszNietypowy();
    }

    @After
    public void returnToTopIfram(){
        BasePage.returnToDefoultIfram();
    }

    @AfterClass
    public static void closeRunProcess(){
        BasePage.closeBrowser();
    }

    @Test
    public void shouldLoginInAtlas() {
        LoginPage loginPage = new LoginPage();
        loginPage.login(Users.LOGIN_ADMIN, Users.PASS_ADMIN);
        MainPage topBar = new MainPage();
        Assert.assertEquals("User nam is not display", "admin", topBar.userName().getText());
        ToolsListPage toolPage = new ToolsListPage();
        toolPage.driver.switchTo().frame(toolPage.driver.findElement(By.cssSelector("#atlas-iframe")));
        Assert.assertTrue("User nam is not display", toolPage.searchInput().isDisplayed());
    }

    @Test
    public void shouldGoToUserList(){
        LoginPage loginPage = new LoginPage();
         loginPage.login(Users.LOGIN_ADMIN, Users.PASS_ADMIN);
        MainPage topBar = new MainPage();
        topBar.usersListLink().click();
        UserListPage userListPage = new UserListPage();
        Assert.assertEquals("Page url is not correct", userListPage.path, userListPage.currentUrl());
    }

    @Test
    public void shouldLoginAsNotAdmin(){
        LoginPage loginPage = new LoginPage();
        loginPage.login(Users.LOGIN_JANUSZ, Users.PASS_JANUSZ);
        MainPage topBar = new MainPage();
        Assert.assertEquals("User nam is not display", Users.LOGIN_JANUSZ, topBar.userName().getText());
    }

    @Test
    public void validationLoginForm() {
        LoginPage loginPage = new LoginPage();
        loginPage.goToPage();
        loginPage.validationLogin("ad", "wsx");
        loginPage.validationLogin("admin", "wsx");
        loginPage.validationLogin("ad", "admin");
        loginPage.validationLogin("janusz.ecall", "admin");
        loginPage.validationLogin("admin", "Starter7531");
    }
}