package Tests.NewCase;

import Data.Users;
import Helpers.DbManagement;
import Helpers.Form;
import Helpers.GenerateData;
import Pages.LoginPage;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.NowaSprawa.StepId002;
import Pages.ProcesesPages.NowaSprawa.StepId015;
import Pages.ProcesesPages.NowaSprawa.StepId096;
import Tests.AbstractTest;
import org.junit.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;

import static Pages.BasePage.mainDriver;

public class FormulaSafetyValidation extends AbstractTest{

    public static final String DATA_IS_VISIBLE = "data-is-visible";

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException { db.deleteCase(CASE_ID); }

    DbManagement db = new DbManagement(DB_NAME);
    Form formHelper = new Form();
    StepId002 basicVehicleData = new StepId002();

    int CASE_ID = 0;

    @Test
    public void newCase015StepCrashYesValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = gotoForm();
        //Verification visibility od required elements.
        //checking number of elements
        Assert.assertEquals("Number of element in form is different of declaration", 15, formHelper.formContainer().size());
        StepId015 stepId015Form = new StepId015();
        Assert.assertTrue("Paragraph is not display - crash question", stepId015Form.paragraphCrashQuestion().isDisplayed());
        assertContainerMedicalIsEnabled(stepId015Form);
        assertContainerRoutIsEnabled(stepId015Form);
        assertParagraph112Visibility(stepId015Form, "false");
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.radioButtonCrashYes()));
        //Check the answer - Yes
        stepId015Form.radioButtonCrashYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphMedical()));
        //Verification the visibility of elements.
        Assert.assertTrue("RadioButton Medical-Yes is not display", stepId015Form.radioButtonMedicalYes().isEnabled());
//        Assert.assertTrue("RadioButton Medical-Yes is not display", stepId015Form.radioButtonMedicalYes().isDisplayed());
        Assert.assertTrue("RadioButton Medical-No is not display", stepId015Form.radioButtonMedicalNo().isDisplayed());
        Assert.assertTrue("RadioButton Medical - Already Called is not display", stepId015Form.radioButtonMedicalAlreadyCalled().isDisplayed());
        assertContainerRoutIsEnabled(stepId015Form);
        //Check the next answer - Yes
        stepId015Form.radioButtonMedicalYes().click();
        //Verification the visibility od elements.
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraph112()));
        assertContainerRoutIsEnabled(stepId015Form);
        //Check the other answer - No
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId015Form.radioButtonMedicalNo()).click().perform();
        //Verification the visibility the required elements.
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraph112()));
        assertContainerRoutIsDisplayed(stepId015Form);
        //Again check the answer -Yes
        stepId015Form.radioButtonMedicalYes().click();
        //Verification visibility of elements
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraph112()));
        assertContainerRoutIsEnabled(stepId015Form);
        //Check the answer - AlreadyCalled
        actions.moveToElement(stepId015Form.radioButtonMedicalAlreadyCalled()).click().perform();
        //Verification visibility of elements
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraph112()));
        assertContainerRoutIsDisplayed(stepId015Form);
        //Again check the answer -Yes
        stepId015Form.radioButtonMedicalYes().click();
        //Verification visibility of elements
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraph112()));
        assertContainerRoutIsEnabled(stepId015Form);
    }

    @Test
    public void newCase015StepCrashNoValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = gotoForm();
        //Verification visibility od required elements.
        StepId015 stepId015Form = new StepId015();
        Assert.assertTrue("Paragraph is not display - crash question", stepId015Form.paragraphCrashQuestion().isEnabled());
        assertContainerMedicalIsEnabled(stepId015Form);
        assertContainerRoutIsEnabled(stepId015Form);
        assertParagraph112Visibility(stepId015Form, "false");
        //Check the answer - No
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.radioButtonCrashNo()));
        wait.until(ExpectedConditions.elementToBeClickable(stepId015Form.radioButtonCrashNo()));
        stepId015Form.radioButtonCrashNo().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphRout()));
        assertContainerRoutIsDisplayed(stepId015Form);
        assertContainerMedicalIsEnabled(stepId015Form);
        assertParagraph112Visibility(stepId015Form, "false");
    }

    @Test
    public void newCase015StepCrashYesChangeToNoValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = gotoForm();
        //Verification visibility od required elements.
        StepId015 stepId015Form = new StepId015();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        new Form().waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.radioButtonCrashYes()));
        stepId015Form.radioButtonCrashYes().click();
        //Check the answer crash - Yes
        stepId015Form.radioButtonCrashYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphMedical()));
        stepId015Form.radioButtonMedicalYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraph112()));
        //Change decision - crash no
        stepId015Form.radioButtonCrashNo().click();
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraphMedical()));
//        Assert.assertTrue("Paragraph is displayed - 112", stepId015Form.paragraph112().isEnabled());
        assertParagraph112Visibility(stepId015Form, "false");
        assertContainerRoutIsDisplayed(stepId015Form);
    }

    @Test
    public void newCase015WhereIsCareValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = gotoForm();
        //Verification visibility od required elements.
        StepId015 stepId015Form = new StepId015();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.radioButtonCrashYes()));
        stepId015Form.radioButtonCrashYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphMedical()));
        //Check the answer Medical - No
        stepId015Form.radioButtonMedicalNo().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphRout()));
        //Check the answer car in fast rout - Yes
        stepId015Form.radioButtonRoutYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphWhereIsCar()));
        //Verify visibility required elements when selected is "emergency belt"
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.selectWhereIsCarOption(3)));
        stepId015Form.selectWhereIsCarOption(2).click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "true", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        //Select option number 5
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        stepId015Form.selectWhereIsCarOption(5).click();
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "false", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        //Verify visibility required elements when selected is "in the rout"
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.selectWhereIsCarOption(3)));
        stepId015Form.selectWhereIsCarOption(3).click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "true", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        //Select option number 6
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        stepId015Form.selectWhereIsCarOption(6).click();
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "false", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        //Verify visibility required elements when selected is "at the entrance / exit"
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.selectWhereIsCarOption(3)));
        stepId015Form.selectWhereIsCarOption(4).click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "true", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        //Select option number 7
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        stepId015Form.selectWhereIsCarOption(7).click();
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "false", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        //Select option number 8
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.selectWhereIsCarOption(3)));
        stepId015Form.selectWhereIsCarOption(2).click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphPrompt()));
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        stepId015Form.selectWhereIsCarOption(8).click();
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "false", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        //Select option number 9
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.selectWhereIsCarOption(3)));
        stepId015Form.selectWhereIsCarOption(2).click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphPrompt()));
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        stepId015Form.selectWhereIsCarOption(9).click();
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "false", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        //Select option number 10
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.selectWhereIsCarOption(3)));
        stepId015Form.selectWhereIsCarOption(2).click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphPrompt()));
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        stepId015Form.selectWhereIsCarOption(10).click();
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("", "false", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
    }

    @Test
    public void newCase015StepVisibilityOfWhereIsCarSectionValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = gotoForm();
        StepId015 stepId015Form = new StepId015();
        //Check the answer crash - yes
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.radioButtonCrashYes()));
        stepId015Form.radioButtonCrashYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphMedical()));
        //check medical - no
        stepId015Form.radioButtonMedicalNo().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphRout()));
        //check car in highway - yes
        stepId015Form.radioButtonRoutYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphWhereIsCar()));
        //select option 2
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.selectWhereIsCarOption(2)));
        stepId015Form.selectWhereIsCarOption(2).click();
        // check visibility of prompt
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphPrompt()));
        Assert.assertEquals("Paragraph with safety information is not displayed.", "true", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        // check medical - yes
        stepId015Form.radioButtonMedicalYes().click();
        // verify visibility of prompt
        assertContainerRoutIsEnabled(stepId015Form);
        // check medical - no
        stepId015Form.radioButtonMedicalNo().click();
        //verify visibility where is car all elements
        assertContainerRoutIsDisplayed(stepId015Form);
        Assert.assertFalse("asd", stepId015Form.radioButtonRoutYes().isSelected());
        Assert.assertFalse("asd", stepId015Form.radioButtonRoutNo().isSelected());
        Assert.assertFalse("Select where is car is displayed", stepId015Form.selectWhereIsCarContainer().isDisplayed());
        Assert.assertEquals("Prompt for consultant is display", "false", stepId015Form.paragraphPrompt().getAttribute(DATA_IS_VISIBLE));
        Assert.assertEquals("Paragraph with safety information is display", "false", stepId015Form.paragraphSafetyInformation().getAttribute(DATA_IS_VISIBLE));
        Assert.assertEquals("Paragraph where is car is display", "false", stepId015Form.paragraphWhereIsCar().getAttribute(DATA_IS_VISIBLE));
    }

    @Test
    public void newCase015StepVisibilityOfWhereIsCarWhenChangeCrashAnswerValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = gotoForm();
        StepId015 stepId015Form = new StepId015();
        //Check the answer crash - yes
        stepId015Form.radioButtonCrashYes().click();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphMedical()));
        //check medical - no
        stepId015Form.radioButtonMedicalNo().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphRout()));
        //Check the answer crash - no
        stepId015Form.radioButtonCrashNo().click();
        wait.until(ExpectedConditions.invisibilityOf(stepId015Form.paragraphMedical()));
        assertContainerMedicalIsEnabled(stepId015Form);
        //check car in highway - yes
        stepId015Form.radioButtonRoutYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphWhereIsCar()));
        //select option 2
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId015Form.selectWhereIsCarContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.selectWhereIsCarOption(2)));
        stepId015Form.selectWhereIsCarOption(2).click();
        //check visibility of prompt
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphPrompt()));
        //Check the answer crash - yes
        stepId015Form.radioButtonCrashYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.paragraphMedical()));
        //check visibility where is car section
        assertParagraph112Visibility(stepId015Form, "false");
        assertContainerRoutIsDisplayed(stepId015Form);
    }

    private void checkingIfTheSelectedOptionIsDisplayed(StepId015 stepId015Form,int intOption) {
        int newIntOption = intOption - 1;
        Assert.assertEquals("Selected option is not correct", Integer.toString(newIntOption), stepId015Form.selectExcludingCircumstancesContainer().getAttribute("data-value"));
    }

    private StepId015 prepareStepToExcludingCircumstancesValidate() throws SQLException, ClassNotFoundException {
        //Prepare data in step
        CASE_ID = gotoForm();
        StepId015 stepId015Form = new StepId015();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 100);
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.radioButtonCrashNo()));
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId015Form.radioButtonCrashNo()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(stepId015Form.radioButtonRoutNo()));
        actions.moveToElement(stepId015Form.radioButtonRoutNo()).click().perform();
        return stepId015Form;
    }

    @Test
    public void noReasonExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 1;
        StepId015 stepId015Form = prepareStepToExcludingCircumstancesValidate();
        //Verify excluding circumstances select
        stepId015Form.selectValue(numberOfOptionInSelect);
        //checking if the selected option is displayed
        checkingIfTheSelectedOptionIsDisplayed(stepId015Form, numberOfOptionInSelect);
        //goto next step
        new OperationDashboardPage().nextButton().click();
        //verify correct path in process
//        StepId007 stepId007Form = new StepId007();
//        Assert.assertTrue(stepId007Form.paragraphInformation().isDisplayed());
        Assert.assertTrue(basicVehicleData.paragraphPleasePrepareDocuments().isDisplayed());
    }

    @Test
    public void policeCarExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 2;
        StepId015 stepId015Form = prepareStepToExcludingCircumstancesValidate();
        //Verify excluding circumstances select
        stepId015Form.selectValue(numberOfOptionInSelect);
        //checking if the selected option is displayed
        checkingIfTheSelectedOptionIsDisplayed(stepId015Form, numberOfOptionInSelect);
        //goto next step
        new OperationDashboardPage().nextButton().click();
        //verify correct path in process
//        StepId007 stepId007Form = new StepId007();
//        Assert.assertTrue(stepId007Form.paragraphInformation().isDisplayed());
        Assert.assertTrue(basicVehicleData.paragraphPleasePrepareDocuments().isDisplayed());
    }

    @Test
    public void failureToImmediatelyReportExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 3;
        verifyExcludingCircumstancesPaidPath(numberOfOptionInSelect);
    }

    @Test
    public void holdupOfTheVehicleDuringTheReviewExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 4;
        verifyExcludingCircumstancesPaidPath(numberOfOptionInSelect);
    }

    @Test
    public void periodicInspectionExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 5;
        verifyExcludingCircumstancesPaidPath(numberOfOptionInSelect);
    }

    @Test
    public void damageDuringRallyExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 6;
        verifyExcludingCircumstancesPaidPath(numberOfOptionInSelect);
    }

    @Test
    public void damageByIncompatibleUseExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 7;
        verifyExcludingCircumstancesPaidPath(numberOfOptionInSelect);
    }

    @Test
    public void damageResultingFromIntentionalInjuryExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 8;
        verifyExcludingCircumstancesPaidPath(numberOfOptionInSelect);
    }

    @Test
    public void naturalDisasterExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 9;
        verifyExcludingCircumstancesPaidPath(numberOfOptionInSelect);
    }

    @Test
    public void warfareExcludingCircumstances() throws SQLException, ClassNotFoundException {
        int numberOfOptionInSelect = 10;
        verifyExcludingCircumstancesPaidPath(numberOfOptionInSelect);
    }

    private void verifyExcludingCircumstancesPaidPath(int numberOfOptionInSelect) throws SQLException, ClassNotFoundException {
        StepId015 stepId015Form = prepareStepToExcludingCircumstancesValidate();
        //Verify excluding circumstances select
        stepId015Form.selectValue(numberOfOptionInSelect);
        //checking if the selected option is displayed
        checkingIfTheSelectedOptionIsDisplayed(stepId015Form, numberOfOptionInSelect);
        //goto next step
        new OperationDashboardPage().nextButton().click();
        //verify correct path in process
        StepId096 stepId096Form = new StepId096();
        Assert.assertTrue(stepId096Form.paragraphInformation().isDisplayed());
    }

    public void _assertContainerRoutVisibility(StepId015 stepId015, String value){
        Assert.assertEquals("Paragraph 'Where is car' is displayed - " + value, value, stepId015.paragraphRout().getAttribute(DATA_IS_VISIBLE));
    }

    private void assertParagraph112Visibility(StepId015 stepId015Form, String visibility) {
        Assert.assertEquals("Visibility od Paragraph 112 is not correct", visibility, stepId015Form.paragraph112().getAttribute(DATA_IS_VISIBLE));
    }

    public void assertContainerRoutIsEnabled(StepId015 stepId015Form){
        _assertContainerRoutVisibility(stepId015Form, "false");
        Assert.assertTrue("Radio button 'Car in highway is displayed' ",  stepId015Form.radioButtonRoutYes().isEnabled());
        Assert.assertTrue("Radio button 'Car in highway is displayed", stepId015Form.radioButtonRoutNo().isEnabled());
    }

    public void assertContainerRoutIsDisplayed(StepId015 stepId015Form){
        _assertContainerRoutVisibility(stepId015Form, "true");
        Assert.assertTrue("Radio button 'Car in highway is not displayed",  stepId015Form.radioButtonRoutYes().isDisplayed());
        Assert.assertTrue("Radio button 'Car in highway is not displayed", stepId015Form.radioButtonRoutNo().isDisplayed());
    }

    public void assertContainerMedicalIsEnabled(StepId015 stepId015Form){
        Assert.assertTrue("RadioButton Medical-Yes is not display", stepId015Form.radioButtonMedicalYes().isEnabled());
        Assert.assertTrue("RadioButton Medical-No is not display", stepId015Form.radioButtonMedicalNo().isEnabled());
        Assert.assertTrue("RadioButton Medical - Already Called is not display", stepId015Form.radioButtonMedicalAlreadyCalled().isEnabled());
    }

    public int gotoForm() throws SQLException, ClassNotFoundException {
        GenerateData generateData = new GenerateData();
        String firstName = generateData.firstName();
        String lastName = generateData.lastName();
        String phoneNumber = generateData.phoneNumber();
//        DbManagement dbManagement = new DbManagement();
//        db.processInstanceSetInactive();
        int caseID = db.createNewCaseFormulaSafety(firstName, lastName, phoneNumber);
        String caseId = Integer.toString(caseID);
        LoginPage loginPage = new LoginPage();
        loginPage.login(Users.LOGIN_JANUSZ_TESTER, Users.PASS_JANUSZ);
        OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
        operationDashboardPage.goToPage();
        Assert.assertEquals("Page url is not correct", operationDashboardPage.path, operationDashboardPage.currentUrl());
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(caseId);
        return caseID;
    }
}
