package Tests.NewCase;

import Data.Cars.Car;
import Data.Users;
import Helpers.DbManagement;
import Helpers.Form;
import Helpers.GenerateData;
import Pages.LoginPage;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.NowaSprawa.StepId002;
import Tests.AbstractTest;
import org.junit.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;

import static Pages.BasePage.mainDriver;

public class BasicVehicleData extends AbstractTest{

    String PROCESS_ID = "1011";
    String STEP_ID = "002";
    String REQUIRED_FIELD_CLASS = "form-control custom-input validation-error";
    String REQUIRED_DATEPICKER_CLASS = "date-picker form-control custom-input to-now validation-error";

    //Page object
    OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
    GenerateData generateData = new GenerateData();
    StepId002 basicVehicleDataForm = new StepId002();
    DbManagement db = new DbManagement(DB_NAME);
    LoginPage loginPage = new LoginPage();
    Form formHelper = new Form();

    //WebDriverWait declaration
    WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);

    //Car
    Car car = createCar();

    int CASE_ID = 0;

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    @Test
    public void newCase002VisibilityRequiredElementsOnStart() throws SQLException, ClassNotFoundException {
        //Create process and go to step
        loginPage.login(Data.Users.LOGIN_JANUSZ, Data.Users.PASS_JANUSZ);
        CASE_ID = formHelper.goToStepInProcess(PROCESS_ID, STEP_ID);
        //verification visibility of elements
        //checking number of elements
        Assert.assertEquals("Number of element in form is different of declaration", 27, formHelper.formContainer().size());
        //Info section
        Assert.assertTrue("Paragraph 'please prepare documents' is not display", basicVehicleDataForm.paragraphPleasePrepareDocuments().isDisplayed());
        //Registration number section
        Assert.assertTrue("Paragraph 'read registration number' is not display", basicVehicleDataForm.paragraphReadRegistrationNumber().isDisplayed());
        Assert.assertTrue("Prompt 'get registration number and vin' is not display", basicVehicleDataForm.promptGetVin().isDisplayed());
        Assert.assertTrue("Input 'registration number' is not display", basicVehicleDataForm.inputRegistrationNumber().isDisplayed());
        Assert.assertTrue("Button 'Search' is not display", basicVehicleDataForm.buttonSearch().isDisplayed());
        Assert.assertTrue("Button 'No registration document' is not display", basicVehicleDataForm.buttonNoRegistrationDocument().isDisplayed());
        //Vin section
        Assert.assertTrue("Paragraph 'read vin' is not display", basicVehicleDataForm.paragraphReadVin().isDisplayed());
        Assert.assertTrue("Input 'vin' is not display", basicVehicleDataForm.inputVin().isDisplayed());
        Assert.assertTrue("Paragraph 'read date first registration' is not display", basicVehicleDataForm.paragraphReadRegistrationDate().isDisplayed());
        Assert.assertTrue("Input 'registration date' is not display", basicVehicleDataForm.inputRegistrationDate().isDisplayed());
        //Current mileage
        Assert.assertTrue("Paragraph 'read current mileage' is not display", basicVehicleDataForm.paragraphReadCurrentMileage().isDisplayed());
        Assert.assertTrue("Input 'current mileage' is not display", basicVehicleDataForm.inputCurrentMileage().isDisplayed());
        Assert.assertTrue("Checkbox 'Indicative Mileage' is not display", basicVehicleDataForm.checkBoxIndicativeMileage().isDisplayed());
        //Verify hidden elements
        Assert.assertFalse("Paragraph 'At The Car' is display", basicVehicleDataForm.paragraphAtTheCar().isDisplayed());
        Assert.assertFalse("Radio button Yes - 'At The Car' is display", basicVehicleDataForm.radioButtonAtTheCarYes().isDisplayed());
        Assert.assertFalse("Radio button No - 'At The Car' is display", basicVehicleDataForm.radioButtonAtTheCarNo().isDisplayed());
        Assert.assertFalse("Paragraph 'Missing Documents' is display", basicVehicleDataForm.paragraphMissingDocuments().isDisplayed());
        Assert.assertFalse("Paragraph 'Read Vin And Registration Number' is display", basicVehicleDataForm.paragraphReadVinAndRegistrationNumber().isDisplayed());
    }

    @Test
    public void newCase002RequiredFieldValidation() throws SQLException, ClassNotFoundException {
        //Create process and go to step
        loginPage.login(Data.Users.LOGIN_JANUSZ, Data.Users.PASS_JANUSZ);
        CASE_ID = formHelper.goToStepInProcess(PROCESS_ID, STEP_ID);
        //Click next button and verify required validation
        wait.until(ExpectedConditions.elementToBeClickable(operationDashboardPage.nextButton()));
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.nextButtonInactive()));
        Assert.assertEquals("Registration number - Element is not required", REQUIRED_FIELD_CLASS, basicVehicleDataForm.inputRegistrationNumber().getAttribute("class"));
        Assert.assertEquals("VIN input - Element is not required", REQUIRED_FIELD_CLASS, basicVehicleDataForm.inputVin().getAttribute("class"));
        Assert.assertEquals("Registration Date - Element is not required", REQUIRED_DATEPICKER_CLASS, basicVehicleDataForm.inputRegistrationDate().getAttribute("class"));
    }

    @Test
    public void newCase002NoRegistrationDocumentSectionValidation() throws SQLException, ClassNotFoundException {
        loginPage.login(Data.Users.LOGIN_JANUSZ, Data.Users.PASS_JANUSZ);
        CASE_ID = formHelper.goToStepInProcess(PROCESS_ID, STEP_ID);
        wait.until(ExpectedConditions.elementToBeClickable(basicVehicleDataForm.buttonNoRegistrationDocument()));
        basicVehicleDataForm.buttonNoRegistrationDocument().click();
        wait.until(ExpectedConditions.visibilityOf(basicVehicleDataForm.paragraphAtTheCar()));
        Assert.assertTrue("Radio button - Yes 'At the car' is not displayed", basicVehicleDataForm.radioButtonAtTheCarYes().isDisplayed());
        Assert.assertTrue("Radio button - No 'At the car' is not displayed", basicVehicleDataForm.radioButtonAtTheCarNo().isDisplayed());
        basicVehicleDataForm.radioButtonAtTheCarYes().click();
        wait.until(ExpectedConditions.visibilityOf(basicVehicleDataForm.paragraphReadVinAndRegistrationNumber()));
        basicVehicleDataForm.radioButtonAtTheCarNo().click();
        wait.until(ExpectedConditions.invisibilityOf(basicVehicleDataForm.paragraphReadVinAndRegistrationNumber()));
        Assert.assertTrue("Paragraph 'Missing Documents' is not display", basicVehicleDataForm.paragraphMissingDocuments().isDisplayed());
        //Registration number section
        Assert.assertTrue("Paragraph 'read registration number' is not display", basicVehicleDataForm.paragraphReadRegistrationNumber().isDisplayed());
        Assert.assertTrue("Prompt 'get registration number and vin' is not display", basicVehicleDataForm.promptGetVin().isEnabled());
        Assert.assertTrue("Input 'registration number' is not display", basicVehicleDataForm.inputRegistrationNumber().isEnabled());
        Assert.assertFalse("Button 'Search' is not display", basicVehicleDataForm.buttonSearch().isEnabled());
        Assert.assertTrue("Button 'No registration document' is not display", basicVehicleDataForm.buttonNoRegistrationDocument().isEnabled());
        //Vin section
        Assert.assertTrue("Paragraph 'read vin' is not display", basicVehicleDataForm.paragraphReadVin().isEnabled());
        Assert.assertTrue("Input 'vin' is not display", basicVehicleDataForm.inputVin().isEnabled());
        Assert.assertTrue("Paragraph 'read date first registration' is not display", basicVehicleDataForm.paragraphReadRegistrationDate().isEnabled());
        Assert.assertTrue("Input 'registration date' is not display", basicVehicleDataForm.inputRegistrationDate().isEnabled());
        //Current mileage
        Assert.assertTrue("Paragraph 'read current mileage' is not display", basicVehicleDataForm.paragraphReadCurrentMileage().isEnabled());
        Assert.assertTrue("Input 'current mileage' is not display", basicVehicleDataForm.inputCurrentMileage().isEnabled());
        Assert.assertTrue("Checkbox 'Indicative Mileage' is not display", basicVehicleDataForm.checkBoxIndicativeMileage().isEnabled());
        //Verify hidden elements
        Assert.assertTrue("Paragraph 'At The Car' is display", basicVehicleDataForm.paragraphAtTheCar().isDisplayed());
        Assert.assertTrue("Radio button Yes - 'At The Car' is display", basicVehicleDataForm.radioButtonAtTheCarYes().isDisplayed());
        Assert.assertTrue("Radio button No - 'At The Car' is display", basicVehicleDataForm.radioButtonAtTheCarNo().isDisplayed());
        Assert.assertTrue("Paragraph 'Missing Documents' is display", basicVehicleDataForm.paragraphMissingDocuments().isDisplayed());
        Assert.assertFalse("Paragraph 'Read Vin And Registration Number' is display", basicVehicleDataForm.paragraphReadVinAndRegistrationNumber().isDisplayed());
        //Next button is inactive
        Assert.assertTrue("Next button is active or is not display", operationDashboardPage.nextButtonInactive().isDisplayed());
    }

    @Test
    public void newCaseBaseVehicleDataSearchCarIsInBaseSuccessPath() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //Search car
        basicVehicleDataForm.searchCar(car.getRegistrationNumber());
        //Verify correct value vin and data first registration
        Assert.assertEquals("Value in vin input is not correct", car.getVin(), basicVehicleDataForm.inputVin().getAttribute("value"));
        Assert.assertTrue("Paragraph read 6 last char of vin is not display", basicVehicleDataForm.paragraphReadLastCharVin().isDisplayed());
        String vinNumberLastChar = car.getVin().substring(11);
        Assert.assertEquals("Last 6 char of vin is not correct", vinNumberLastChar, basicVehicleDataForm.paragraphLastCharVin().getText());
        Assert.assertEquals("Value in firs registration date input is not correct", "", basicVehicleDataForm.inputRegistrationDate().getAttribute("value"));
    }

    @Test
    public void newCaseBaseVehicleDataSearchCarIsInBaseCarNotFind() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //Search car
        basicVehicleDataForm.searchCar("qq111pp");
        //Verify correct value vin and data first registration
        Assert.assertEquals("Value in vin input is not empty", "", basicVehicleDataForm.inputVin().getAttribute("value"));
        Assert.assertTrue("Paragraph read 6 last char of vin is not display", basicVehicleDataForm.paragraphReadLastCharVin().isEnabled());
        Assert.assertEquals("Value in firs registration date input is not correct", "", basicVehicleDataForm.inputRegistrationDate().getAttribute("value"));
    }

    @Test
    public void newCaseBaseVehicleDataManualFillFieldValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //fill registration number input
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        //click next
        wait.until(ExpectedConditions.elementToBeClickable(operationDashboardPage.nextButton()));
        operationDashboardPage.nextButton().click();
        //verify required field
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.nextButtonInactive()));
        Assert.assertEquals("VIN input - Element is not required", REQUIRED_FIELD_CLASS, basicVehicleDataForm.inputVin().getAttribute("class"));
        Assert.assertEquals("Registration Date - Element is not required", REQUIRED_DATEPICKER_CLASS, basicVehicleDataForm.inputRegistrationDate().getAttribute("class"));
        // fill vin input
        basicVehicleDataForm.inputVin().sendKeys(car.getVin());
        Assert.assertEquals("Registration Date - Element is not required", REQUIRED_DATEPICKER_CLASS, basicVehicleDataForm.inputRegistrationDate().getAttribute("class"));
        Assert.assertTrue("Next Button is active", operationDashboardPage.nextButtonInactive().isDisplayed());
        // click next
        new Form().waitForLoaderExperimental();
        operationDashboardPage.nextButton().click();
        // verify required field
        Assert.assertNotEquals("VIN input - Element is not required", REQUIRED_FIELD_CLASS, basicVehicleDataForm.inputVin().getAttribute("class"));
        Assert.assertEquals("Registration Date Input - Element is not required", REQUIRED_DATEPICKER_CLASS, basicVehicleDataForm.inputRegistrationDate().getAttribute("class"));
        //fill first registration input
        basicVehicleDataForm.inputRegistrationDate().sendKeys("09-01-2017");
//        basicVehicleDataForm.inputRegistrationDate().sendKeys(car.getRegistrationDate());
        //click next
        operationDashboardPage.nextButton().click();
//        wait.until(ExpectedConditions.invisibilityOf(operationDashboardPage.nextButtonInactive()));
//        Assert.assertFalse("Next button is inactive", operationDashboardPage.nextButtonInactive().isDisplayed());
        Assert.assertTrue("Next button is inactive", operationDashboardPage.nextButton().isDisplayed());
        //verify required field
        Assert.assertNotEquals("Registration number - Element is stile required", REQUIRED_FIELD_CLASS, basicVehicleDataForm.inputRegistrationNumber().getAttribute("class"));
        Assert.assertNotEquals("VIN input - Element is stile required", REQUIRED_FIELD_CLASS, basicVehicleDataForm.inputVin().getAttribute("class"));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Assert.assertNotEquals("Registration Date Input - Element is stile required", REQUIRED_DATEPICKER_CLASS, basicVehicleDataForm.inputRegistrationDate().getAttribute("class"));
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.nextButtonInactive()));
        basicVehicleDataForm.inputCurrentMileage().sendKeys("12");
        basicVehicleDataForm.inputCurrentMileage().clear();
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.nextButton()));
        //click next
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.visibilityOf(formHelper.semiRequiredFieldModal()));
    }

    @Test
    public void newCaseBaseVehicleDataManualFillFieldSuccessPath() throws SQLException, ClassNotFoundException {
        //proper data
        CASE_ID = goToCase();
        //fill registration number input
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        //fill vin input
        basicVehicleDataForm.inputVin().sendKeys(car.getVin());
        //check vin correct
//        basicVehicleDataForm.checkboxCorrectVin().click();
        //fill first registration input
        basicVehicleDataForm.inputRegistrationDate().sendKeys(car.getRegistrationDate());
        //fill Current Mileage input
        basicVehicleDataForm.inputCurrentMileage().sendKeys("12000");
        //click next
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoader();
    }

    @Test
    public void newCaseBaseVehicleDataVinInputValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //fill registration number input
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        //fill first registration input
        basicVehicleDataForm.inputRegistrationDate().sendKeys(car.getRegistrationDate());
        //validate vin input
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "1");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "123456789");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "12345678901234");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "WVWZZ?3CZDE529235");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "WVWZZZ3CZDE52923O");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "WVWZZZ3CZDO529235");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "WVWZZZ3CZDQ529235");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "WVWZZZZ3CZDE529235");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "WVWZZ3CZDE529235");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "WVWZ!Z3CZDE529235");
        formHelper.inputDateFormatValidation(basicVehicleDataForm.inputVin(), "W?WZ!Z#CZDE529235");
    }

    @Test
    public void newCaseBaseVehicleDataFirstRegistrationDateInputValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //fill registration number input
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        //fill first registration input
        basicVehicleDataForm.inputVin().sendKeys(car.getVin());
        //validate vin input
//        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "");
        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "xx-xx-xxxx");
//        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "12-12-30");
        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "12-12-121");
        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "12-12-121x");
        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "12-12-x121");
        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "12-1?-2x21");
        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "1-12-2x21");
        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "12-14-2021");
        formHelper.datePickerDateFormatValidation(basicVehicleDataForm.inputRegistrationDate(), "12-14-2015");
    }

    @Test
    public void newCaseBaseVehicleDataCurrentMileageInputValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //fill registration number input
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        //fill vin input
        basicVehicleDataForm.inputVin().sendKeys(car.getVin());
        //fill first registration input
        basicVehicleDataForm.inputRegistrationDate().sendKeys(car.getRegistrationDate());
        //validate current mileage input
        basicVehicleDataForm.dataCurrentMileageInputForMinusValueValidation("-12");
        basicVehicleDataForm.dataCurrentMileageInputForMinusValueValidation("-1");
        basicVehicleDataForm.dataCurrentMileageInputForMinusValueValidation("-0");
        basicVehicleDataForm.dataCurrentMileageInputForMinusValueValidation("-15231");
        formHelper.dateCurrentMileageInputIncorectFormatValidation(basicVehicleDataForm.inputCurrentMileage(), "0");
        formHelper.dateCurrentMileageInputIncorectFormatValidation(basicVehicleDataForm.inputCurrentMileage(), "9910000");

    }

    public int goToCase() throws SQLException, ClassNotFoundException {
        String phoneNumber = generateData.phoneNumber();
        String firstName = generateData.firstName();
        String lastName = generateData.lastName();
        String caseId = db.createNewCaseBasicVehicleData(firstName, lastName, phoneNumber);
        loginPage.login(Users.LOGIN_JANUSZ, Users.PASS_JANUSZ);
        operationDashboardPage.goToPage();
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(caseId);
        return Integer.parseInt(caseId);
    }

    public Car createCar(){
        return new Car();
    }
}