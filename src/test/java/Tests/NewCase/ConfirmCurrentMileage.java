package Tests.NewCase;

import Data.Cars.Car;
import Data.Users;
import Helpers.DbManagement;
import Helpers.Form;
import Helpers.GenerateData;
import Pages.LoginPage;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.NowaSprawa.StepId002;
import Pages.ProcesesPages.NowaSprawa.StepId003;
import Pages.ProcesesPages.NowaSprawa.StepId016;
import Pages.ProcesesPages.NowaSprawa.StepId043;
import Tests.AbstractTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.sql.SQLException;
import java.text.ParseException;

public class ConfirmCurrentMileage extends AbstractTest{

    //Page Object
    OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
    GenerateData generateData = new GenerateData();
    StepId002 basicVehicleDataForm = new StepId002();
    DbManagement db = new DbManagement(DB_NAME);
    LoginPage loginPage = new LoginPage();
    Form formHelper = new Form();
    BasicVehicleData basicVehicleData = new BasicVehicleData();
    StepId043 confirmCurrentMileageForm = new StepId043();
    StepId003 confirmOfPermissionsForm = new StepId003();
    StepId016 notFoundInDatabase = new StepId016();

    //Data
    int CASE_ID = 0;
    Car car = new Car();

    //Clean database after test
    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    @Test
    public void confirmCurrentMileageVerifyElementOnStart() throws SQLException, ClassNotFoundException, ParseException {
       CASE_ID = goToCase();
        Assert.assertEquals("Number of element in form is different of declaration", 4, formHelper.formContainer().size());
        Assert.assertTrue("Element - paragraphLastRegisteredMileage - is not display", confirmCurrentMileageForm.paragraphLastRegisteredMileage().isDisplayed());
        Assert.assertTrue("Element - paragraphPleaseConfirmMileage - is not display", confirmCurrentMileageForm.paragraphPleaseConfirmMileage().isDisplayed());
        Assert.assertTrue("Element - buttonConfirm - is not display", confirmCurrentMileageForm.buttonConfirm().isDisplayed());
        Assert.assertTrue("Element - buttonCorrect - is not display", confirmCurrentMileageForm.buttonCorrect().isDisplayed());
    }

    @Test
    public void confirmCurrentMileageBackAndCorrect() throws SQLException, ClassNotFoundException {
        String[] car= {"PO650ux","WVWZZZ3CZDE529235", "21-09-2012", "91226"};
//        String[] car= {"SD70773", "WV1ZZZ7HZ6H048481", "2017-08-12", "1"};
        CASE_ID = basicVehicleData.goToCase();
        //fill registration number input
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car[0]);
        //fill vin input
        basicVehicleDataForm.inputVin().sendKeys(car[1]);
        //fill first registration input
        basicVehicleDataForm.inputRegistrationDate().sendKeys(car[2]);
        //fill Current Mileage input
        basicVehicleDataForm.inputCurrentMileage().sendKeys(car[3]);
        //click next
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        int maxMileage = db.getMaxDeclaredMileage(car[1]);
        String mileage = confirmCurrentMileageForm.getMaxDeclaredMileage(maxMileage);
        confirmCurrentMileageForm.buttonCorrect().click();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(basicVehicleDataForm.inputCurrentMileage()));
        basicVehicleDataForm.inputCurrentMileage().clear();
        basicVehicleDataForm.inputCurrentMileage().sendKeys(mileage);
        basicVehicleDataForm.checkboxCorrectVin().click();
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        Assert.assertTrue("Element - paragraphLastRegisteredMileage - is not display", confirmCurrentMileageForm.paragraphLastRegisteredMileage().isDisplayed());
        confirmCurrentMileageForm.buttonConfirm().click();
        formHelper.waitForLoaderExperimental();
        Assert.assertTrue("Expected form is not display - confirm permissions", notFoundInDatabase.paragraphPermissionsInformation().isDisplayed());
    }

    @Test
    public void confirmCurrentMileageConfirmAndGoNext() throws SQLException, ClassNotFoundException, ParseException {
        CASE_ID = goToCase();
        confirmCurrentMileageForm.buttonConfirm().click();
        wait.until(ExpectedConditions.visibilityOf(confirmOfPermissionsForm.paragraphPermissionsInformation()));
    }

    private int goToCase() throws SQLException, ClassNotFoundException, ParseException {
        String phoneNumber = generateData.phoneNumber();
        String firstName = generateData.firstName();
        String lastName = generateData.lastName();
        String caseId = db.createNewCaseConfirmMileage(firstName, lastName, phoneNumber);
        loginPage.login(Users.LOGIN_JANUSZ, Users.PASS_JANUSZ);
        operationDashboardPage.goToPage();
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(caseId);
        return Integer.parseInt(caseId);
    }
}
