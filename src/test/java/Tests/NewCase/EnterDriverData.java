package Tests.NewCase;

import Data.Users;
import Helpers.DbManagement;
import Helpers.Form;
import Helpers.GenerateData;
import Pages.LoginPage;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.NowaSprawa.StepId006;
import Pages.ProcesesPages.NowaSprawa.StepId015;
import Pages.ProcesesPages.NowaSprawa.StepId026;
import Tests.AbstractTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;

import static Pages.BasePage.mainDriver;

public class EnterDriverData extends AbstractTest{

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    //PageObject
    OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
    DbManagement db = new DbManagement(DB_NAME);
    GenerateData generateData = new GenerateData();
    LoginPage loginPage = new LoginPage();
    StepId006 enterDriverData = new StepId006();
    StepId015 formulaSafety = new StepId015();
    Form formHelper = new Form();

    //Data
    int CASE_ID = 0;
    String PHONE_NUMBER =   "";
    String FIRST_NAME =   "";
    String LAST_NAME =   "";

    WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);

    public static final String DATA_IS_VISIBLE = "data-is-visible";

    @Test
    public void enterDriverDataVerifyVisibilityElementsOnStart() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //checking number of elements
        Assert.assertEquals("Number of element in form is different of declaration", 16, formHelper.formContainer().size());
        //checking of visible elements
        Assert.assertTrue(enterDriverData.paragraphAreYouDriver().isDisplayed());
        Assert.assertTrue(enterDriverData.radioButtonAreYouDriverYes().isDisplayed());
        Assert.assertTrue(enterDriverData.radioButtonAreYouDriverNo().isDisplayed());
        //checking of invisible elements
        Assert.assertTrue(enterDriverData.paragraphPleaseEnterYourName().isEnabled());
        Assert.assertTrue(enterDriverData.toolIfram().isEnabled());
        Assert.assertTrue(enterDriverData.paragraphExceptionalSituation().isEnabled());
        Assert.assertTrue(enterDriverData.radioButtonExceptionalSituationYes().isEnabled());
        Assert.assertTrue(enterDriverData.radioButtonExceptionalSituationNo().isEnabled());
        Assert.assertTrue(enterDriverData.buttonAddPerson().isEnabled());
    }

    @Test
    public void enterDriverDataAreYouDriverYes() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //check radioButton Are You Driver - Yes
        enterDriverData.radioButtonAreYouDriverYes().click();
        //checking of invisible elements
        Assert.assertTrue(enterDriverData.paragraphPleaseEnterYourName().isEnabled());
        Assert.assertTrue(enterDriverData.toolIfram().isEnabled());
        Assert.assertTrue(enterDriverData.paragraphExceptionalSituation().isEnabled());
        Assert.assertTrue(enterDriverData.radioButtonExceptionalSituationYes().isEnabled());
        Assert.assertTrue(enterDriverData.radioButtonExceptionalSituationNo().isEnabled());
        Assert.assertTrue(enterDriverData.buttonAddPerson().isEnabled());
        //go to next step
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.visibilityOf(formulaSafety.paragraphCrashQuestion()));
    }

    @Test
    public void enterDriverDataAreYouDriverNo() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //check radioButton Are You Driver - No
        enterDriverData.radioButtonAreYouDriverNo().click();
        //checking of visible add driver section
        Assert.assertTrue(enterDriverData.paragraphPleaseEnterYourName().isDisplayed());
        Assert.assertTrue(enterDriverData.toolIfram().isDisplayed());
        Assert.assertTrue(enterDriverData.paragraphExceptionalSituation().isDisplayed());
        Assert.assertTrue(enterDriverData.radioButtonExceptionalSituationYes().isDisplayed());
        Assert.assertTrue(enterDriverData.radioButtonExceptionalSituationNo().isDisplayed());
        Assert.assertTrue(enterDriverData.buttonAddPerson().isDisplayed());
    }

    @Test
    public void enterDriverDataRequiredFieldValidation() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //try go to next step without check radio button Are You Driver
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.nextButtonInactive()));
        Assert.assertEquals("Element is not required", "validation-error", enterDriverData.radioButtonAreYouDriverYesInput().getAttribute("class"));
        Assert.assertEquals("Element is not required", "validation-error", enterDriverData.radioButtonAreYouDriverNoInput().getAttribute("class"));
        //check radioButton Are You Driver - No
        wait.until(ExpectedConditions.elementToBeClickable(enterDriverData.radioButtonAreYouDriverNo()));
        enterDriverData.radioButtonAreYouDriverNo().click();
        wait.until(ExpectedConditions.visibilityOf(enterDriverData.toolIfram()));
//        operationDashboardPage.nextButton().click();
        //TODO
        //dopisac resztę po wyjasnieniu od OP - zapytania w notatkach.
        // plus nowy scenariusz dla kombinacji zaznaczeń odpowiedzi pod narzedziem.
    }

    @Test
    public void enterDriverDataExceptionalSituationWithDriverData() throws SQLException, ClassNotFoundException {
        CASE_ID = goToCase();
        //check radioButton Are You Driver - No
        enterDriverData.radioButtonAreYouDriverNo().click();
        wait.until(ExpectedConditions.visibilityOf(enterDriverData.toolIfram()));
        enterDriverData.radioButtonExceptionalSituationYes().click();
        enterDriverData.radioButtonGiveDriverDataYes().click();
        operationDashboardPage.nextButton().click();
        StepId026 addDriverForm = new StepId026();
        wait.until(ExpectedConditions.visibilityOf(addDriverForm.containerFirstName()));
    }

    private int goToCase() throws SQLException, ClassNotFoundException {
        PHONE_NUMBER = generateData.phoneNumber();
        FIRST_NAME = generateData.firstName();
        LAST_NAME = generateData.lastName();
//        db.processInstanceSetInactive();
        int caseId = db.createNewCaseEnterDriverData(FIRST_NAME, LAST_NAME, PHONE_NUMBER);
        loginPage.login(Users.LOGIN_JANUSZ, Users.PASS_JANUSZ);
        operationDashboardPage.goToPage();
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(Integer.toString(caseId));
        return caseId;
    }
}
