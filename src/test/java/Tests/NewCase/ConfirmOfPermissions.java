package Tests.NewCase;

import Data.Cars.Car;
import Helpers.DbManagement;
import Helpers.Form;
import Helpers.GenerateData;
import Pages.LoginPage;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.NowaSprawa.StepId003;
import Pages.ProcesesPages.NowaSprawa.StepId016;
import Pages.ProcesesPages.NowaSprawa.StepId002;
import Pages.ProcesesPages.NowaSprawa.StepId043;
import Tests.AbstractTest;
import org.junit.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;

import static Pages.BasePage.mainDriver;

public class ConfirmOfPermissions extends AbstractTest{

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    //Page object
    OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
    StepId002 basicVehicleDataForm = new StepId002();
    DbManagement db = new DbManagement(DB_NAME);
    Form formHelper = new Form();
    StepId003 confirmPermission = new StepId003();
    StepId016 noneInDataBase = new StepId016();
    StepId043 confirmMileageForm = new StepId043();

    //WebDriverWait declaration
    WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);

    Actions actions = new Actions(driver);

    //Car
    Car car = new Car();

    int CASE_ID = 0;

    @Test
    public void permissionsNotFound() throws SQLException, ClassNotFoundException {
        int maxMileage = Integer.parseInt(car.getMaxMileage());
        String mileage = Integer.toString(maxMileage + 10);
        CASE_ID = formHelper.goToCaseInBasicVehicleData();
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        basicVehicleDataForm.inputVin().sendKeys(car.getVin());
        basicVehicleDataForm.inputCurrentMileage().sendKeys(mileage);
        basicVehicleDataForm.inputRegistrationDate().sendKeys(car.getRegistrationDate());
        actions.moveToElement(basicVehicleDataForm.paragraphPleasePrepareDocuments()).perform();
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        confirmMileageException();
        Assert.assertTrue("mesage fail", noneInDataBase.paragraphPermissionsInformation().isDisplayed());
    }

    @Test
    public void permissionsFound() throws SQLException, ClassNotFoundException {
        int maxMileage = Integer.parseInt(car.getMaxMileage());
        String mileage = Integer.toString(maxMileage - 10);
        CASE_ID = formHelper.goToCaseInBasicVehicleData();
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        basicVehicleDataForm.inputVin().sendKeys(car.getVin());
        basicVehicleDataForm.inputRegistrationDate().sendKeys(car.getRegistrationDate());
        basicVehicleDataForm.inputCurrentMileage().sendKeys(mileage);
        actions.moveToElement(basicVehicleDataForm.paragraphPleasePrepareDocuments()).perform();
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        confirmMileageException();
        Assert.assertTrue("message fail", confirmPermission.paragraphPermissionsInformation().isDisplayed());
    }

    @Test
    public void permissionsFoundMaxMileage() throws SQLException, ClassNotFoundException {
        int maxMileage = Integer.parseInt(car.getMaxMileage());
        String mileage = Integer.toString(maxMileage - 1);
        CASE_ID = formHelper.goToCaseInBasicVehicleData();
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        basicVehicleDataForm.inputVin().sendKeys(car.getVin());
        basicVehicleDataForm.inputRegistrationDate().sendKeys(car.getRegistrationDate());
        basicVehicleDataForm.inputCurrentMileage().sendKeys(mileage);
        actions.moveToElement(basicVehicleDataForm.paragraphPleasePrepareDocuments()).perform();
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        confirmMileageException();
        Assert.assertTrue("message fail", confirmPermission.paragraphPermissionsInformation().isDisplayed());
    }

    @Test
    public void permissionsNotFoundMaxMileage() throws SQLException, ClassNotFoundException {
        CASE_ID = formHelper.goToCaseInBasicVehicleData();
        System.out.println("Pojazd: " + car.getRegistrationNumber());
        basicVehicleDataForm.inputRegistrationNumber().sendKeys(car.getRegistrationNumber());
        basicVehicleDataForm.inputVin().sendKeys(car.getVin());
        basicVehicleDataForm.inputRegistrationDate().sendKeys(car.getRegistrationDate());
        basicVehicleDataForm.inputCurrentMileage().sendKeys(car.getMaxMileage());
        actions.moveToElement(basicVehicleDataForm.paragraphPleasePrepareDocuments()).perform();
        operationDashboardPage.nextButton().click();
        formHelper.waitForLoaderExperimental();
        confirmMileageException();
        Assert.assertTrue("message fail", noneInDataBase.paragraphPermissionsInformation().isDisplayed());
    }

    private void confirmMileageException() {
        try {
            if (confirmMileageForm.formContainer().isDisplayed()){
                confirmMileageForm.buttonConfirm().click();
                formHelper.waitForLoaderExperimental();
            }
        } catch (Exception e){}
    }
}
