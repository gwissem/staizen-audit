package Tests.NewCase;

import Helpers.DbManagement;
import Helpers.Form;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.NowaSprawa.StepId018;
import Tests.AbstractTest;
import org.junit.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Set;

import static Pages.BasePage.mainDriver;

public class RepairByPhone extends AbstractTest{

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    //Page object
    OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
    DbManagement db = new DbManagement(DB_NAME);
    Form formHelper = new Form();
    StepId018 repairByPhoneStepForm = new StepId018();

    //WebDriverWait declaration
    WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);


    int CASE_ID = 0;

    @Test
    public void repairByPhoneValidationElementOnStart() throws SQLException, ClassNotFoundException, ParseException {
        CASE_ID = formHelper.goToCaseInRepairByPhone();
        //checking number of elements
        Assert.assertEquals("Number of element in form is different of declaration", 8, formHelper.formContainer().size());
        Assert.assertTrue(repairByPhoneStepForm.repairButton().isDisplayed());
        Assert.assertTrue(repairByPhoneStepForm.promptClickOnButton().isDisplayed());
    }



    @Test
    public void manualOpenSabioService() throws SQLException, ClassNotFoundException, ParseException {
        CASE_ID = formHelper.goToCaseInRepairByPhone();
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        Object[] windowsNameArray = getWindowsNameArray();
        mainDriver.switchTo().window(windowsNameArray[1].toString());
        mainDriver.close();
        mainDriver.switchTo().window(windowsNameArray[0].toString());
        Assert.assertEquals("Number open windows in not correct wit expected", 1, mainDriver.getWindowHandles().size());
        //switch to iframe Atlas
        mainDriver.switchTo().frame(operationDashboardPage.atlasIfram());
        //open new window
        repairByPhoneStepForm.repairButton().click();
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        windowsNameArray = getWindowsNameArray();
        mainDriver.switchTo().window(windowsNameArray[1].toString());
        String sabioWindowsTitle = mainDriver.getTitle();
        Assert.assertEquals("Windows title is wrong.", "SABIO", sabioWindowsTitle);
        Assert.assertEquals("Sabio url is not correct", repairByPhoneStepForm.sabioURL, mainDriver.getCurrentUrl());
        mainDriver.close();
        mainDriver.switchTo().window(windowsNameArray[0].toString());
    }

    @Test
    public void automationOpenWindowsWitchSabioVerification() throws SQLException, ClassNotFoundException, ParseException {
        //System.out.println("Timestamp " + MainPage.getTimestamp());
        CASE_ID = formHelper.goToCaseInRepairByPhone();
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        Object[] windowsNameArray = getWindowsNameArray();
        mainDriver.switchTo().window(windowsNameArray[1].toString());
        String sabioWindowsTitle = mainDriver.getTitle();
        Assert.assertEquals("Windows title is wrong.", "SABIO", sabioWindowsTitle);
        Assert.assertEquals("Sabio url is not correct", repairByPhoneStepForm.sabioURL, mainDriver.getCurrentUrl());
        mainDriver.close();
        mainDriver.switchTo().window(windowsNameArray[0].toString());
    }

    private Object[] getWindowsNameArray() {
        Set<String> windowHandles = mainDriver.getWindowHandles();
        return windowHandles.toArray();
    }
}
