package Tests.NewCase;

import Helpers.DbManagement;
import Helpers.Form;
import Pages.BasePage;
import Pages.LoginPage;
import Pages.MainPage;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.NowaSprawa.StepId007;
import Tests.AbstractTest;
import org.junit.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;

import static Pages.BasePage.mainDriver;

public class ProcessingOfPersonalData extends AbstractTest{

    String PROCESS_ID = "1011";
    String STEP_ID = "007";
    String LOGIN_JANUSZ = "janusz.nietypowy";
    String PASS_JANUSZ = "Starter1234@";
    String DATA_IS_VISIBLE = "data-is-visible";
    DbManagement db = new DbManagement(DB_NAME);
    Form formHelper = new Form();
    int CASE_ID = 0;

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    @Test
    public void newCase007VisibilityRequiredElementsOnStart() throws SQLException, ClassNotFoundException {
        LoginPage loginPage = new LoginPage();
        loginPage.login(LOGIN_JANUSZ, PASS_JANUSZ);
        CASE_ID = formHelper.goToStepInProcess(PROCESS_ID, STEP_ID);
        //checking number of elements
        Assert.assertEquals("Number of element in form is different of declaration", 12, formHelper.formContainer().size());
        StepId007 processingOfPersonalDataForm = new StepId007();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(processingOfPersonalDataForm.paragraphInformation()));
        Assert.assertTrue("Paragraph with Permission to contact is not display", processingOfPersonalDataForm.paragraphWithPermissionToContact().isDisplayed());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("Radio button is not display - permission to contact", processingOfPersonalDataForm.radioButtonPermissionToContactYes().isDisplayed());
        Assert.assertTrue("Radio button is not display - permission to contact", processingOfPersonalDataForm.radioButtonPermissionToContactNo().isDisplayed());
        Assert.assertTrue("Prompt for consultant is not display", processingOfPersonalDataForm.promptWithPermissionToContact().isDisplayed());
        //Verification invisible elements
        Assert.assertFalse("Radio button is not display - PersonalDataCorrect", processingOfPersonalDataForm.radioButtonPersonalDataCorrectYes().isDisplayed());
        Assert.assertFalse("Radio button is not display - PersonalDataCorrect", processingOfPersonalDataForm.radioButtonPersonalDataCorrectNo().isDisplayed());
    }

    @Test
    public void newCase007PermissionToContactYesValidation() throws SQLException, ClassNotFoundException {
        LoginPage loginPage = new LoginPage();
        loginPage.login(LOGIN_JANUSZ, PASS_JANUSZ);
        CASE_ID = new Helpers.Form().goToStepInProcess(PROCESS_ID, STEP_ID);
        StepId007 processingOfPersonalDataForm = new StepId007();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(processingOfPersonalDataForm.paragraphInformation()));
        wait.until(ExpectedConditions.visibilityOf(processingOfPersonalDataForm.radioButtonPermissionToContactYes()));
        Actions action = new Actions(mainDriver);
        action.moveToElement(processingOfPersonalDataForm.radioButtonPermissionToContactYes()).click().perform();
        processingOfPersonalDataForm.radioButtonPermissionToContactYes().click();
        wait.until(ExpectedConditions.visibilityOf(processingOfPersonalDataForm.paragraphPersonalDataCorrect()));
        Assert.assertTrue("Radio button is not display - PersonalDataCorrect", processingOfPersonalDataForm.radioButtonPersonalDataCorrectYes().isDisplayed());
        Assert.assertTrue("Radio button is not display - PersonalDataCorrect", processingOfPersonalDataForm.radioButtonPersonalDataCorrectNo().isDisplayed());
    }

    @Test
    public void newCase007PermissionToContactNoValidation() throws SQLException, ClassNotFoundException {
        LoginPage loginPage = new LoginPage();
        loginPage.login(LOGIN_JANUSZ, PASS_JANUSZ);
        CASE_ID = new Helpers.Form().goToStepInProcess(PROCESS_ID, STEP_ID);
        StepId007 processingOfPersonalDataForm = new StepId007();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(processingOfPersonalDataForm.paragraphInformation()));
        Actions action = new Actions(mainDriver);
        wait.until(ExpectedConditions.elementToBeClickable(processingOfPersonalDataForm.radioButtonPermissionToContactNo()));
        action.moveToElement(processingOfPersonalDataForm.radioButtonPermissionToContactNo()).click().perform();
        processingOfPersonalDataForm.radioButtonPermissionToContactNo().click();
        Assert.assertFalse("Paragraph Personal Data Correct is display", processingOfPersonalDataForm.paragraphPersonalDataCorrect().isDisplayed());
        Assert.assertFalse("Radio button is display - PersonalDataCorrect", processingOfPersonalDataForm.radioButtonPersonalDataCorrectYes().isDisplayed());
        Assert.assertFalse("Radio button is display - PersonalDataCorrect", processingOfPersonalDataForm.radioButtonPersonalDataCorrectNo().isDisplayed());
    }

    @Test
    public void newCase007PermissionRequiredFieldValidation() throws SQLException, ClassNotFoundException {
        LoginPage loginPage = new LoginPage();
        loginPage.login(LOGIN_JANUSZ, PASS_JANUSZ);
        CASE_ID = new Helpers.Form().goToStepInProcess(PROCESS_ID, STEP_ID);
        StepId007 processingOfPersonalDataForm = new StepId007();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 100);
        wait.until(ExpectedConditions.visibilityOf(processingOfPersonalDataForm.paragraphInformation()));
        OperationDashboardPage operationDashboard = new OperationDashboardPage();
        operationDashboard.nextButton().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboard.nextButtonInactive()));
        Assert.assertEquals("Element is not required", "validation-error", processingOfPersonalDataForm.radioButtonPermissionToContactNoInput().getAttribute("class"));
        new Form().waitForLoaderExperimental();
        //        Form formHelper = new Form();
//        wait.until(ExpectedConditions.invisibilityOf(formHelper.loader()));
        Actions action = new Actions(mainDriver);
        action.moveToElement(processingOfPersonalDataForm.radioButtonPermissionToContactYes()).click().perform();
        //processingOfPersonalDataForm.radioButtonPermissionToContactYes().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboard.nextButton()));
//        wait.until(ExpectedConditions.elementToBeClickable(operationDashboard.nextButton()));
        action.moveToElement(operationDashboard.nextButton()).click().perform();
//        operationDashboard.nextButton().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboard.nextButtonInactive()));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //wait.until(ExpectedConditions.attributeContains(processingOfPersonalDataForm.radioButtonPersonalDataCorrectYesInput(), "class", "validation-error"));
        Assert.assertEquals("Element is not required", "validation-error", processingOfPersonalDataForm.radioButtonPersonalDataCorrectYesInput().getAttribute("class"));
        action.moveToElement(processingOfPersonalDataForm.radioButtonPersonalDataCorrectYes()).click().perform();
        action.moveToElement(processingOfPersonalDataForm.radioButtonPersonalDataCorrectYes()).click().perform();
//        processingOfPersonalDataForm.radioButtonPersonalDataCorrectYes().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboard.nextButton()));
//        wait.until(ExpectedConditions.invisibilityOf(operationDashboard.nextButtonInactive()));
        action.moveToElement(processingOfPersonalDataForm.radioButtonPersonalDataCorrectNo()).click().perform();
        wait.until(ExpectedConditions.visibilityOf(operationDashboard.nextButtonInactive()));
        Assert.assertEquals("Element is not required", "validation-error", processingOfPersonalDataForm.radioButtonPersonalDataCorrectNoInput().getAttribute("class"));
    }
}