package Tests.DzwoniTelefon;

import Helpers.DbManagement;
import Helpers.Form;
import Helpers.GenerateData;

import Pages.LoginPage;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.DzwoniTelefon.*;
import Pages.ProcesesPages.NowaSprawa.StepId002;
//import Pages.ProcesesPages.NowaSprawa.StepId006;
import Tests.AbstractTest;
import org.junit.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;

import static Pages.BasePage.mainDriver;

public class DzwoniTelefon extends AbstractTest{

    @After
    public void cleanDB() throws SQLException, ClassNotFoundException {db.deleteCase(CASE_ID);}

    public static GenerateData generateData = new GenerateData();
    Form formHelper = new Form();

    public static String BASE_PHONE_NUMBER = generateData.phoneNumber();
    public static String BASE_FIRST_NAME = generateData.firstName();
    DbManagement db = new DbManagement(DB_NAME);
    int CASE_ID = 0;

    JavascriptExecutor jse = (JavascriptExecutor) mainDriver;

    @Test
    public void dzwoniTelefonTest1() throws SQLException, ClassNotFoundException {
        String firstName = BASE_FIRST_NAME;
        String lastName = generateData.lastName();
        //Generate process
        int CaseID = db.createNewCaseFormulaSafety(firstName, lastName, BASE_PHONE_NUMBER);
        try {
            db.processInstanceSetInactiveCase(Integer.toString(CaseID));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String clientPhoneNumber = BASE_PHONE_NUMBER;
        LoginPage loginPage = new LoginPage();
        loginPage.login("janusz.nietypowy", "Starter1234@");
        OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
        operationDashboardPage.goToPage();
        Assert.assertEquals("Page url is not correct", operationDashboardPage.path, operationDashboardPage.currentUrl());
        WebDriverWait wait = new WebDriverWait(operationDashboardPage.driver, 10, 500);
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        jse.executeScript("console.log('czekam na załadowanie się klasy do body')");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until(ExpectedConditions.visibilityOf(new Form().loadedForm()));
        operationDashboardPage.createTask(clientPhoneNumber);
        StepId016 stepId016Form = new StepId016();
        //sprawdzić czy w komunikacie widzimy imie i nazwisko dzwoniącego
        wait.until(ExpectedConditions.visibilityOf(stepId016Form.radioYes()));
        stepId016Form.radioYes().click();
        stepId016Form.radioYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId016Form.phoneNumberInput()));
        Assert.assertEquals("Wrong phone number", clientPhoneNumber, stepId016Form.phoneNumberInput().getAttribute("value"));
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.invisibilityOf(stepId016Form.phoneNumberInput()));
        StepId006 stepId006Form = new StepId006();
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.radioNo()));
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.titleStepForm()));
        Assert.assertEquals("Next button is not active", null, operationDashboardPage.nextButton().getAttribute("disabled"));
        wait.until(ExpectedConditions.elementToBeClickable(stepId006Form.radioNo()));
        stepId006Form.radioNo().click();
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.poradaButton()));
        // Verify next button - is not active
        Assert.assertEquals("Next button is active", "true", operationDashboardPage.nextButton().getAttribute("disabled"));
        // New case - click
        stepId006Form.newCaseButton().click();
        //sprawdzić czy idziemy do nowego zadania
        wait.until(ExpectedConditions.invisibilityOf(stepId006Form.newCaseButton()));
        Pages.ProcesesPages.NowaSprawa.StepId006 enterDriverData = new Pages.ProcesesPages.NowaSprawa.StepId006();
        Assert.assertTrue("Form Formula Safety is not display", enterDriverData.paragraphAreYouDriver().isDisplayed());
        CASE_ID = CaseID;
    }

    @Test
    public void dzwoniTelefonTest2() throws SQLException, ClassNotFoundException {
        String clientPhoneNumber = BASE_PHONE_NUMBER;
        LoginPage loginPage = new LoginPage();
        loginPage.login("janusz.nietypowy", "Starter1234@");
        OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
        operationDashboardPage.goToPage();
        Assert.assertEquals("Page url is not correct", operationDashboardPage.path, operationDashboardPage.currentUrl());
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until(ExpectedConditions.visibilityOf(new Form().loadedForm()));
        operationDashboardPage.createTask(clientPhoneNumber);
        StepId016 stepId016Form = new StepId016();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 100);
        wait.until(ExpectedConditions.visibilityOf(stepId016Form.paragraphTalkingWith()));
        Actions actions = new Actions(mainDriver);
        wait.until(ExpectedConditions.elementToBeClickable(stepId016Form.radioYes()));
        actions.moveToElement(stepId016Form.radioYes()).click().perform();
        stepId016Form.radioYes().click();
        wait.until(ExpectedConditions.visibilityOf(stepId016Form.phoneNumberInput()));
        Assert.assertEquals("Wrong phone number", clientPhoneNumber, stepId016Form.phoneNumberInput().getAttribute("value"));
        operationDashboardPage.nextButton().click();
        StepId017 stepId017Form = new StepId017();
        Form formHelper = new Form();
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.elementToBeClickable(stepId017Form.paragraphExistCase()));
        wait.until(ExpectedConditions.elementToBeClickable(stepId017Form.radioYes()));
        stepId017Form.radioYes().click();
        actions.moveToElement(stepId017Form.radioNo()).click().perform();
        stepId017Form.radioNo().click();
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.invisibilityOf(formHelper.loader()));
        StepId006 stepId006Form = new StepId006();
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.radioNo()));
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.titleStepForm()));
        stepId006Form.radioYes().click();
        actions.moveToElement(stepId006Form.radioNo()).click().perform();
        stepId006Form.radioNo().click();
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.poradaButton()));
        //operationDashboardPage.nextButton().click();
        //sprawdić czy prowadzi  do notatki
        CASE_ID = getCaseId();
    }

    @Test
    public void dzwoniTelefonTest3() throws SQLException, ClassNotFoundException {
        int caseId1 = 0;
        int caseId2 = 0;
        try {
            GenerateData generateData = new GenerateData();
            String firstName = generateData.firstName();
            String firstName1 = generateData.firstName();
            String lastName = generateData.lastName();
            String lastName1 = generateData.lastName();
            //Generate proces
            caseId1 = db.createNewCaseFormulaSafety(firstName, lastName, BASE_PHONE_NUMBER);
            caseId2 = db.createNewCaseFormulaSafety(firstName1, lastName1, BASE_PHONE_NUMBER);
            String clientPhoneNumber = BASE_PHONE_NUMBER;
            db.processInstanceSetInactiveCase(Integer.toString(caseId1));
            db.processInstanceSetInactiveCase(Integer.toString(caseId2));
            OperationDashboardPage operationDashboardPage = simulateIncomingCall(clientPhoneNumber);
            CASE_ID = getCaseId();
            StepId011 stepId011Form = new StepId011();
            stepId011Form.driver.switchTo().frame(stepId011Form.toolIfram());
            WebDriverWait wait = new WebDriverWait(mainDriver, 20, 500);
            wait.until(ExpectedConditions.visibilityOf(stepId011Form.phoneNumberInput()));
            stepId011Form.nameInput().sendKeys(firstName);
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            wait.until(ExpectedConditions.elementToBeClickable(stepId011Form.resultClientSearch(BASE_PHONE_NUMBER, lastName)));
            stepId011Form.resultClientSearch(BASE_PHONE_NUMBER, lastName).click();
            operationDashboardPage.driver.switchTo().defaultContent();
            operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
            operationDashboardPage.nextButton().click();
            //Przejście przez krok 1012.013
            Form formHelper = new Form();
            formHelper.waitForLoaderExperimental();
//            wait.until(ExpectedConditions.invisibilityOf(formHelper.loader()));
            wait.until(ExpectedConditions.visibilityOf(new StepId013().formTitle()));
            wait.until(ExpectedConditions.elementToBeClickable(operationDashboardPage.nextButton()));
            operationDashboardPage.nextButton().click();
            StepId006 stepId006Form = new StepId006();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            wait.until(ExpectedConditions.visibilityOf(stepId006Form.radioNo()));
            wait.until(ExpectedConditions.visibilityOf(stepId006Form.titleStepForm()));
            Actions actions = new Actions(mainDriver);
            actions.moveToElement(stepId006Form.radioNo()).click().perform();
            stepId006Form.radioNo().click();
            wait.until(ExpectedConditions.visibilityOf(stepId006Form.poradaButton()));
            operationDashboardPage.nextButton().click();
        } catch (Exception e){
            System.out.print("Tests Error - dzwoniTelefon3 - ten test nie zakończył się sukcesem.");
            e.printStackTrace();
        }
        finally {
            db.deleteCase(caseId1);
            db.deleteCase(caseId2);
        }
    }

    @Test
    public void dzwoniTelefonNieznanyNumerNowaSprawa() throws SQLException, ClassNotFoundException {
        _dzwoniTelefonNieznanyNumer("Imie", "Nazwisko", "");
        OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
        operationDashboardPage.nextButton().click();
        CASE_ID = getCaseId();
    }

    @Test
    public void dzwoniTelefonPorada() throws SQLException, ClassNotFoundException {
        CASE_ID = _dzwoniTelefonNieznanyNumer("Imie", "Nazwisko", "");
        StepId006 stepId006Form = new StepId006();
        stepId006Form.poradaButton().click();
        formHelper.waitForLoaderExperimental();
        StepId004 stepId004Form = new StepId004();
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId004Form.contactReasonContainer()).click().perform();
        stepId004Form.contactReasonOption(2).click();
        OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
        operationDashboardPage.nextButton().click();
        WebDriverWait wait = new WebDriverWait(operationDashboardPage.driver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.finishProcessPopup()));
    }

    @Test
    public void dzwoniTelefonPoradaValidation() throws SQLException, ClassNotFoundException, InterruptedException {
        CASE_ID = _dzwoniTelefonNieznanyNumer("Imie", "Nazwisko", "");
        StepId006 stepId006Form = new StepId006();
        stepId006Form.poradaButton().click();
        formHelper.waitForLoaderExperimental();
        selectRelationValidation(2, 6);
        Thread.sleep(1000);
        selectRelationValidation(3, 6);
        Thread.sleep(1000);
        selectRelationValidation(4, 4);
        StepId004 stepId004Form = selectRelationValidation(5, 6);
        Assert.assertEquals("Element text area is not display", "false", stepId004Form.otherTextarea().getAttribute("data-is-visible"));
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId004Form.contactReasonContainer()).click().perform();
        stepId004Form.contactReasonOption(6).click();
        Assert.assertTrue("Element is present - contact reason details select", stepId004Form.contactReasonDetailsContainer().isEnabled());
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(stepId004Form.otherTextarea()));
        Assert.assertEquals("Element text area is not display", "true", stepId004Form.otherTextarea().getAttribute("data-is-visible"));
    }

    @Test
    public void dzwoniTelefonLaczenieSpraw() throws SQLException, ClassNotFoundException {
        //Generate first case data
        String procesId = "1011";
        String stepId = "002";
        String caseIdAttribiuthPath = "235";
        GenerateData generateData = new GenerateData();
        String phoneNumber = generateData.phoneNumber();
        String firstName = "Michaaaa";
        String lastName = "Gawix";
        //Generate proces
        int caseID = db.createProcess(procesId + "." + stepId);
        db.setAttributeProcessInt(caseIdAttribiuthPath, caseID, caseID);
        db.setHumanAttribiut(caseID, phoneNumber, firstName, lastName);
        //Create new case and connect to other case
        String clientPhoneNumber = generateData.phoneNumber();
        simulateIncomingCall( clientPhoneNumber);
        CASE_ID = getCaseId();
        StepId013 stepForm013 = new StepId013();
        stepForm013.addClient("Imię", "Nazwisko", "");
        Assert.assertEquals("Wrong phone number in phoneInput", clientPhoneNumber, stepForm013.phoneNumberInput().getAttribute("value"));
        OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
        operationDashboardPage.nextButton().click();
        StepId006 stepId006Form = new StepId006();
        WebDriverWait wait = new WebDriverWait(mainDriver, 20, 100);
        Form formHelper = new Form();
        wait.until(ExpectedConditions.invisibilityOf(formHelper.loader()));
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.radioNo()));
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.titleStepForm()));
        wait.until(ExpectedConditions.elementToBeClickable(stepId006Form.radioYes()));
        stepId006Form.radioYes().click();
        stepId006Form.driver.switchTo().frame(stepId006Form.toolIfram());
        stepId006Form.phoneNumberInput().clear();
        stepId006Form.firstNameInput().sendKeys(firstName);
        stepId006Form.lastNameInput().sendKeys(lastName);
        String caseId = Integer.toString(caseID);
        stepId006Form.caseNumberInput().sendKeys(caseId);
        wait.until(ExpectedConditions.elementToBeClickable(stepId006Form.resultClientSearch(caseId, lastName)));
        stepId006Form.resultClientSearch(caseId, lastName).click();
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.activeResult(caseId)));
        mainDriver.switchTo().defaultContent();
        mainDriver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.nextButton().click();
        StepId002 basicVehicleDataForm = new StepId002();
        wait.until(ExpectedConditions.visibilityOf(basicVehicleDataForm.paragraphPleasePrepareDocuments()));
    }

    @Test
    public void dzwoniTelefonReservedNumber() throws SQLException, ClassNotFoundException {
        String clientPhoneNumber = generateData.phoneNumber();
        simulateIncomingCall(clientPhoneNumber);
        CASE_ID = getCaseId();
        StepId013 stepForm013 = new StepId013();
        stepForm013.addClient("Imię", "Nazwisko", "");
        Assert.assertEquals("Wrong phone number in phoneInput", clientPhoneNumber, stepForm013.phoneNumberInput().getAttribute("value"));
        OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
        operationDashboardPage.nextButton().click();
        StepId006 stepId006Form = new StepId006();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.radioNo()));
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.titleStepForm()));
        Actions actions =new Actions(mainDriver);
        actions.moveToElement(stepId006Form.radioNo()).click().perform();
        stepId006Form.radioNo().click();
        stepId006Form.radioNo().click();
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.poradaButton()));
        operationDashboardPage.nextButton().click();
    }

    //----------------------------- METHOD ----------------------------------------

    private OperationDashboardPage simulateIncomingCall( String clientPhoneNumber) {
        LoginPage loginPage = new LoginPage();
        loginPage.login("janusz.nietypowy", "Starter1234@");
        OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
        operationDashboardPage.goToPage();
        Assert.assertEquals("Page url is not correct", operationDashboardPage.path, operationDashboardPage.currentUrl());
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        jse.executeScript("console.log('czekam na załadowanie się klasy do body')");
        wait.until(ExpectedConditions.visibilityOf(new Form().loadedForm()));
        operationDashboardPage.createTask(clientPhoneNumber);
        return operationDashboardPage;
    }

    public int _dzwoniTelefonNieznanyNumer(String firstName, String lastName, String clientPhoneNumber){
        OperationDashboardPage operationDashboardPage = simulateIncomingCall( clientPhoneNumber);
        StepId014 stepId014 = new StepId014();
        WebDriverWait wait = new WebDriverWait(operationDashboardPage.driver, 10, 100);
        int taskId = getCaseId();
        stepId014.addClient(firstName, lastName, clientPhoneNumber);
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.invisibilityOf(stepId014.firstNameInput()));
        StepId006 stepId006Form = new StepId006();
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.titleStepForm()));
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.radioNo()));
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId006Form.radioNo()).click().perform();
        stepId006Form.radioNo().click();
        wait.until(ExpectedConditions.visibilityOf(stepId006Form.poradaButton()));
        return taskId;
    }

    public StepId004 selectRelationValidation(int contactReasonOption, int contactReasonDetailOption){
        StepId004 stepId004Form = new StepId004();
        Actions actions = new Actions(mainDriver);
        actions.moveToElement(stepId004Form.contactReasonContainer()).click().perform();
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        wait.until(ExpectedConditions.visibilityOfAllElements(stepId004Form.contactReasonList()));
        Assert.assertTrue("Option is not displayed in select list.", stepId004Form.contactReasonOption(contactReasonOption).isDisplayed());
        stepId004Form.contactReasonOption(contactReasonOption).click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(stepId004Form.contactReasonList()));
        actions.moveToElement(stepId004Form.contactReasonDetailsContainer()).click().perform();
        wait.until(ExpectedConditions.visibilityOfAllElements(stepId004Form.contactReasonDetailsList()));
//        wait.until(ExpectedConditions.visibilityOf(stepId004Form.contactReasonDetailsList()));
        Assert.assertTrue("Option in reason details is not displayed", stepId004Form.contactReasonDetailsOption(contactReasonDetailOption).isDisplayed());
        return stepId004Form;
    }

    public int getCaseId(){
        String taskId = new OperationDashboardPage().activeTaskId().getText().substring(1);
        return Integer.parseInt(taskId);
    }
}