package Tests;

import Pages.BasePage;
import Tests.BenefitsPanel.RepairAndTowing;
import Tests.Bwb.Bwb;
import Tests.DzwoniTelefon.DzwoniTelefon;
import Tests.NewCase.*;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({RepairAndTowing.class, Bwb.class, DzwoniTelefon.class, FormulaSafetyValidation.class, ProcessingOfPersonalData.class, BasicVehicleData.class,
        ConfirmOfPermissions.class, RepairByPhone.class, EnterDriverData.class, ConfirmCurrentMileage.class})

public class  ProcessesTestSuite {

    @AfterClass
    public static void closeRunProcess(){ BasePage.closeBrowser(); }
}

