package Tests;

import Config.DbSet;
import Config.selenium.junit.ScreenshotOnFailure;
import Pages.BasePage;
import Pages.LoginPage;
import Pages.OperationDashboardPage;
import org.junit.*;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static Pages.BasePage.mainDriver;

public class AbstractTest extends DbSet {

    //PageObject declaration
    public WebDriver driver = BasePage.driver();
    public LoginPage loginPage = new LoginPage();
    public OperationDashboardPage operationDashboardPage = new OperationDashboardPage();

    @BeforeClass
    public static void initializeWebDriver(){  BasePage.driver(); }

    @Rule
    public TestRule watcher = new TestWatcher() {
    protected void starting(Description description) { System.out.println("Starting test: " + description.getMethodName()); }
    };

    @After
    public void returnToTopIfram(){ BasePage.returnToDefoultIfram(); }

//    @AfterClass
//    public static void closeRunProcess(){ BasePage.closeBrowser(); }

    @Rule
    public ScreenshotOnFailure src = new ScreenshotOnFailure(driver, new File("./target/screenshots/" + getCurrentDateAndTime()));

    //WebDriverWait declaration
    public WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);

    private String getCurrentDateAndTime(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy/HH-mm-ss");
        return sdf.format(date);
    }

    public String getCurrentDate(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        return sdf.format(date);
    }

    public String getCurrentDateSQLFormat(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}
