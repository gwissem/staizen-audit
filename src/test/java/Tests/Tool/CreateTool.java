package Tests.Tool;

import Pages.LoginPage;
import Pages.ToolsListPage;
import org.junit.Assert;
import org.junit.Test;

public class CreateTool {

    public String QUERY_TEXT = "select * from tool";
    public String TOOL_NAME_PL = "Narzędzie testowe" + ToolsListPage.getCurrentDate();
    public String TOOL_NAME_EN = "Test tool" + ToolsListPage.getCurrentDate();
    public String TOOL_DESCRIPTION = "Testowy opis narzędzia.";
    public String notificationToolCreatePL = "Narzędzie \"" + TOOL_NAME_PL + "\" zostało pomyślnie utworzone";

    @Test
    public void shouldCreateToolUnassignedToGroup(){
        LoginPage loginPage = new LoginPage();
        loginPage.login("admin", "admin");
        ToolsListPage toolsListPage = new ToolsListPage();
        toolsListPage.goToPage();
        Assert.assertEquals("Page url is not correct", toolsListPage.path, toolsListPage.currentUrl());
        toolsListPage.driver.switchTo().frame(toolsListPage.atlasIfram());
        Assert.assertTrue(toolsListPage.addToolButton().isDisplayed());
        toolsListPage.createToolUnassignedToGroup(TOOL_NAME_PL, TOOL_NAME_EN, TOOL_DESCRIPTION, QUERY_TEXT);
        Assert.assertEquals("text", notificationToolCreatePL, toolsListPage.notification().getText());
    }

    @Test
    public void shouldCreateToolAssignedToGroup(){
        //TODO write scenario
    }

    @Test
    public void shouldImportTool(){
        //TODO
    }
}
