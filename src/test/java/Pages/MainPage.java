package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainPage extends BasePage {
    public WebDriver driver = driver();

    public WebElement userName() {
        return driver.findElement(By.cssSelector(".top-menu .username"));
    }

    public WebElement usersListLink() {
        return driver.findElement(By.cssSelector("i.fa.fa-user"));
    }

    public WebElement toolGroupListLink() {
        return driver.findElement(By.cssSelector("i.fa.fa-paperclip"));
    }

    public WebElement operationDashboardLink() {
        return driver.findElement(By.cssSelector("i.fa.fa-cog.fa-3x"));
    }

    public WebElement notification(){
        return driver.findElement(By.xpath("//div[@id='flash-messages']/div"));
    }

    public WebElement toolsListLink(){ return driver.findElement(By.cssSelector("i.fa.fa-wrench"));}

    public WebElement jaberStatusNotificationContainer() {return driver.findElement(By.cssSelector("#toast-container"));}

    public static String getCurrentDate(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return sdf.format(date);
    }

    public static String getTimestamp(){
        Date date = new Date();
        return  date.toString();
    }


}