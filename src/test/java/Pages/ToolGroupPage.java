package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ToolGroupPage extends MainPage {

    public WebDriver driver = driver();

    public String srcPath = "/admin/tools/groups";
    public String path = BasePage.baseUrl + srcPath;

    public WebElement addGroupButton(){
        return driver.findElement(By.xpath("//a[i[contains(@class, 'fa fa-plus')]]"));
    }

    public WebElement groupNamePLInput(){
        return driver.findElement(By.cssSelector("#atlas_tool_group_translations_pl_name"));
    }

    public WebElement groupNameEnInput(){
        return driver.findElement(By.cssSelector("#atlas_tool_group_translations_en_name"));
    }

    public WebElement tabPL(){
        return driver.findElement(By.xpath("//a[contains(@data-target, 'translationsFields-pl')]"));
    }

    public WebElement tabEN(){
        return driver.findElement(By.xpath("//a[contains(@data-target, 'translationsFields-en')]"));
    }

    public WebElement submitButton(){
        return driver.findElement(By.xpath("//form[contains(@id, 'tool-group-form')]//button[contains(@type, 'submit')]"));
    }

    public List<WebElement> existingGroupAllert(){
        return driver.findElements(By.xpath("//li[contains(@text, '')][span[contains(@class, 'exclamation-circle')]]"));
    }

    public void goToPage(){
        driver.switchTo().defaultContent();
        MainPage mainPage = new MainPage();
        mainPage.toolGroupListLink().click();
    }

    public void createGroup(String groupNamePL, String groupNameEN) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(addGroupButton()));
        Actions action = new Actions(driver);
        action.moveToElement(addGroupButton()).perform();
        addGroupButton().click();
        wait.until(ExpectedConditions.visibilityOf(groupNamePLInput()));
        groupNamePLInput().sendKeys(groupNamePL);
        tabEN().click();
        wait.until(ExpectedConditions.visibilityOf(groupNameEnInput()));
        groupNameEnInput().sendKeys(groupNameEN);
        submitButton().click();
    }
}