package Pages.ProcesesPages.DzwoniTelefon;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepId013 extends MainPage {

    public WebElement firstNameInput(){return driver.findElement(By.xpath("//input[contains(@id, '-81-342-64')]"));}

    public WebElement lastNameInput(){return driver.findElement(By.xpath("//input[contains(@id, '-81-342-66')]"));}

    public WebElement phoneNumberInput(){return driver.findElement(By.xpath("//input[contains(@id, '0-81-342-408-197')]"));}
    public WebElement formTitle() {return driver.findElement(By.xpath("//form[contains(@id, \"render-form-1012-013\")]"));}

    public void addClient(String firstName, String lastName, String phoneNumber){
        WebDriverWait wait = new WebDriverWait(driver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(firstNameInput()));
        firstNameInput().sendKeys(firstName+ getCurrentDate() );
        lastNameInput().sendKeys(lastName + getCurrentDate());
    }
}