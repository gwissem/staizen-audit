package Pages.ProcesesPages.DzwoniTelefon;


import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StepId016 extends MainPage {

    public WebDriver driver = driver();

    public WebElement radioYes(){
        return driver.findElement(By.xpath("//label[@for='1-451']"));
    }

    public WebElement radioNo(){
        return driver.findElement(By.xpath("//label[@for='0-451']"));
    }
    public WebElement paragraphTalkingWith() {return driver.findElement(By.xpath("//div[@data-id=\"349\"]"));}
    public WebElement phoneNumberInput(){ return driver.findElement(By.xpath("//input[contains(@id, '0-81-342-408-197')]"));}
}
