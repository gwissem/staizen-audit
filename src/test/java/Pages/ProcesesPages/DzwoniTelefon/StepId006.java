package Pages.ProcesesPages.DzwoniTelefon;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StepId006 extends MainPage{

    public WebDriver driver = driver();

    public WebElement radioYes(){
        return driver.findElement(By.xpath("//label[@for='1-452']"));
    }

    public WebElement radioNo(){
       return driver.findElement(By.xpath("//label[@for='0-452']"));
    }

    public WebElement poradaButton(){
        return driver.findElement(By.cssSelector("#button-50"));
    }
    public WebElement newCaseButton(){  return driver.findElement(By.cssSelector("#button-49"));
    }

    public WebElement titleStepForm() {
        return driver.findElement(By.cssSelector("#render-form-1012-006"));
    }

    public WebElement caseNumberInput(){ return driver.findElement(By.cssSelector("#caseNumber")); }
    public WebElement firstNameInput() {return  driver.findElement(By.cssSelector("#firstname"));}
    public WebElement lastNameInput() {return  driver.findElement(By.cssSelector("#lastname"));}
    public WebElement phoneNumberInput() {return  driver.findElement(By.xpath("//input[@id=\"phoneNumber\"]"));}

    public WebElement toolIfram(){
        return mainDriver.findElement(By.cssSelector("#embed-iframe-36"));
    }
    public WebElement resultClientSearch(String caseId, String lastName){ return driver.findElement(By.xpath("//tr[td[contains(text(), \"" + caseId + "\")]][td[contains(text(), \"" + lastName + "\")]]"));}

    public WebElement activeResult(String caseId){return  driver.findElement(By.xpath("//tr[contains(@class, 'active-row')]/td[contains(text(), \"" + caseId + "\")]"));}
}
