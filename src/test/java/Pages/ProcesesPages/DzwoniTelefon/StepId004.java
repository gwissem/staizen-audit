package Pages.ProcesesPages.DzwoniTelefon;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class StepId004 extends MainPage {

    public WebDriver driver = driver();

    public WebElement titleStepForm() {
        return driver.findElement(By.cssSelector("#render-form-1012-004"));
    }

    public WebElement contactReasonContainer() {return driver.findElement(By.xpath("//select[contains(@data-attribute-path, '497')]"));}
    public WebElement contactReasonOption(int valueNumber) {return driver.findElement(By.xpath("//li[contains(@id, 'select2--497-result-')][" + valueNumber + "]"));}
    public List<WebElement> contactReasonList(){return driver.findElements(By.xpath("//ul[@id=\"select2--497-results\"]"));}
    public WebElement contactReasonDetailsContainer() {return driver.findElement(By.xpath("//select[contains(@data-attribute-path, '498')]"));}
    public WebElement contactReasonDetailsOption(int valueNumber) {return driver.findElement(By.xpath("(//ul[contains(@id, 'select2--498-results')]/li)[" + valueNumber + "]"));}
    public List<WebElement> contactReasonDetailsList() {return driver.findElements(By.xpath("//ul[@id=\"select2--498-results\"]"));}
    public WebElement otherTextarea() {return driver.findElement(By.xpath("//div[contains(@data-attr-id, '499')]"));}

}
