package Pages.ProcesesPages.DzwoniTelefon;

import Helpers.GenerateData;
import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepId014 extends MainPage {

    public WebDriver driver = driver();

    String FORM_TITLE = "Wprowadź numer telefonu ";

    public WebElement firstNameInput(){return driver.findElement(By.xpath("//input[contains(@id, '-81-342-64')]"));}

    public WebElement lastNameInput(){return driver.findElement(By.xpath("//input[contains(@id, '-81-342-66')]"));}

    public WebElement phonNumberInput(){return driver.findElement(By.xpath("//input[contains(@id, '0-81-342-408-197')]"));}

    public WebElement permissionCheckbox(){return driver.findElement(By.xpath("//input[contains(@id, '0-81-342-408-405')]"));}

    public void addClient(String firstName, String lastName, String phoneNumber){
        GenerateData generateData = new GenerateData();
        WebDriverWait wait = new WebDriverWait(driver, 10, 500);
        wait.until(ExpectedConditions.visibilityOf(firstNameInput()));
        firstNameInput().sendKeys(firstName+ getCurrentDate() );
        lastNameInput().sendKeys(lastName + getCurrentDate());
        if (phoneNumber.length() == 0) {
            phonNumberInput().sendKeys(phoneNumber + generateData.phoneNumber());
        } else {
            phonNumberInput().sendKeys(phoneNumber);
        }
    }

}
