package Pages.ProcesesPages.DzwoniTelefon;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class StepId011 extends MainPage{

    public WebDriver driver = driver();

    public WebElement phoneNumberInput(){ return driver.findElement(By.cssSelector("#pNumber"));}
    public WebElement nameInput(){ return driver.findElement(By.cssSelector("#firstname"));}

    public WebElement titleStepForm() {
        return driver.findElement(By.xpath("//form[contains(@id, \"render-form-1012-011\")]"));
    }

    public WebElement toolIfram(){
        return mainDriver.findElement(By.cssSelector("#embed-iframe-16"));
    }

    public WebElement resultClientSearch(String phoneNumber, String clientName){ return driver.findElement(By.xpath("//tr[td[contains(text(), \"" + phoneNumber + "\")]][td[contains(text(), \"" + clientName + "\")]]"));}

}
