package Pages.ProcesesPages.DzwoniTelefon;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Sigmeo on 30.08.2017.
 */
public class StepId017 extends MainPage {

    public WebDriver driver = driver();

    public WebElement radioYes(){
        return driver.findElement(By.xpath("//label[@for='1-453']"));
    }

    public WebElement radioNo(){
        return driver.findElement(By.xpath("//label[@for='0-453']"));
    }
    public WebElement paragraphExistCase() {return driver.findElement(By.xpath("//div[@data-id=\"360\"]"));}
}
