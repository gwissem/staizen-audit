package Pages.ProcesesPages.Bwb;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId012 extends MainPage {

    public WebElement paragraphWhatMileage(){return driver.findElement(By.xpath("//div[@data-id=\"733\"]"));}
    public WebElement inputMileageFromInspection(){return driver.findElement(By.xpath("//input[@id=\"-74-456\"]"));}
}
