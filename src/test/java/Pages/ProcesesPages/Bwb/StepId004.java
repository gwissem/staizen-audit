package Pages.ProcesesPages.Bwb;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId004 extends MainPage{

    public WebElement paragraphWhereDidYouBuyTheCar(){return driver.findElement(By.xpath("//div[@data-id=\"719\"]"));}
    public WebElement radioButtonContainerInput(String value){return driver.findElement(By.xpath("//input[@data-attribute-path=\"521\"][@value=\""+ value +"\"]"));}
}
