package Pages.ProcesesPages.Bwb;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId007 extends MainPage{

    public WebElement paragraphOfferPaidService(){return driver.findElement(By.xpath("//div[@data-id=\"756\"]"));}
    public WebElement promptAreClientWantPaidHelp(){return driver.findElement(By.xpath("//div[@data-id=\"737\"]"));}
    public WebElement radioButtonPaidHelpYes(){return driver.findElement(By.xpath("//label[@for=\"1-533\"]"));}
    public WebElement radioButtonPaidHelpNo(){return driver.findElement(By.xpath("//label[@for=\"0-533\"]"));}
}
