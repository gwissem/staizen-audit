package Pages.ProcesesPages.Bwb;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId006 extends MainPage {

    public WebElement paragraphBuyCarDate(){return driver.findElement(By.xpath("//div[@data-id=\"723\"]"));}
    public WebElement prompt(){return driver.findElement(By.xpath("//div[@data-id=\"725\"]"));}
    public WebElement inputBuyCarDate(){return driver.findElement(By.xpath("//input[@id=\"-74-523\"]"));}
}
