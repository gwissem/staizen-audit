package Pages.ProcesesPages.Bwb;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId005 extends MainPage{

    public WebElement paragraphWhichDealer(){return driver.findElement(By.xpath("//div[@data-id=\"721\"]"));}
    public WebElement selectDealersListContainer() {return driver.findElement(By.xpath("//select[contains(@data-attribute-path, '522')]"));}
    public WebElement selectDealersListOption(int valueNumber) {return driver.findElement(By.xpath("(//ul[contains(@id, 'select2--522-results')]/li)[" + valueNumber + "]"));}

}