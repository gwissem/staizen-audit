package Pages.ProcesesPages.Bwb;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId002 extends MainPage{

    public WebElement paragraphTypeOfService(){return driver.findElement(By.xpath("//div[@data-id=\"714\"]"));}
    public WebElement radioButtonContainer(){return driver.findElement(By.xpath("//div[@data-id=\"718\"]"));}
    public WebElement radioButtonContainerInput(String value){return driver.findElement(By.xpath("//input[@data-attribute-path=\"519\"][@value=\""+ value +"\"]"));}
}
