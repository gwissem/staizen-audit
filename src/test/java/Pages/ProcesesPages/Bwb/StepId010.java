package Pages.ProcesesPages.Bwb;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId010 extends MainPage{

    public WebElement paragraphWhenWasLastInspection(){return driver.findElement(By.xpath("//div[@data-id=\"727\"]"));}
    public WebElement promptWhereFindInformation(){return driver.findElement(By.xpath("//div[@data-id=\"728\"]"));}
    public WebElement radioButtonContainer(){return driver.findElement(By.xpath("//div[@data-id=\"753\"]"));}
    public WebElement radioButtonContainerInput(){return driver.findElement(By.xpath("//input[@data-attribute-path=\"531\"]"));}
    public WebElement inputDateLastInspectionFrom(){return driver.findElement(By.xpath("//input[@id=\"-74-105\"]"));}
    public WebElement inputDateLastInspectionTo(){return driver.findElement(By.xpath("//input[@data-attribute=path=\"525\"]"));}
}