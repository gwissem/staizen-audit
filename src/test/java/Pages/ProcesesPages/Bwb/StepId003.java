package Pages.ProcesesPages.Bwb;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId003 extends MainPage{

    public WebElement paragraphWhereInspection(){return driver.findElement(By.xpath("//div[@data-id=\"716\"]"));}
    public WebElement radioButtonContainer(){return driver.findElement(By.xpath("//div[@data-id=\"717\"]"));}
    public WebElement radioButtonContainerInput(String value){return driver.findElement(By.xpath("//input[@data-attribute-path=\"520\"][@value=\""+ value +"\"]"));}
//    public WebElement radioButtonContainerInput(){return driver.findElement(By.xpath("//input[@data-attribute-path=\"520\"]"));}
}