package Pages.ProcesesPages.NowaSprawa;

import Helpers.DbManagement;
import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.sql.SQLException;

public class StepId043 extends MainPage{

    public WebElement paragraphPleaseConfirmMileage() {return driver.findElement(By.xpath("//div[@data-id=\"854\"]"));}
    public WebElement paragraphLastRegisteredMileage() {return driver.findElement(By.xpath("//div[@data-id=\"853\"]"));}
    public WebElement buttonConfirm() {return driver.findElement(By.xpath("//button[@id=\"button-2245\"]"));}
    public WebElement buttonCorrect() {return driver.findElement(By.xpath("//button[@id=\"button-2244\"]"));}
    public WebElement formContainer() {return driver.findElement(By.xpath("//form[@id=\"render-form-1011-043\"]"));}

    public String getMaxDeclaredMileage(int mileage) throws SQLException, ClassNotFoundException {
        mileage = mileage -1;
        return Integer.toString(mileage);
    }

}
