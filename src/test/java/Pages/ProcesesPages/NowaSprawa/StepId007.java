package Pages.ProcesesPages.NowaSprawa;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId007 extends MainPage {

    public WebElement paragraphInformation(){return driver.findElement(By.xpath("//div[@data-id=\"103\"]"));}
    public WebElement paragraphWithPermissionToContact(){return driver.findElement(By.xpath("//div[@data-id=\"140\"]"));}
    public WebElement promptWithPermissionToContact(){return driver.findElement(By.xpath("//div[@data-id=\"591\"]"));}
    public WebElement radioButtonPermissionToContactYesInput(){return driver.findElement(By.xpath("//input[@id=\"1-269\"]"));}
    public WebElement radioButtonPermissionToContactNoInput(){return driver.findElement(By.xpath("//input[@id=\"0-269\"]"));}
    public WebElement radioButtonPermissionToContactYes(){return driver.findElement(By.xpath("//label[@for=\"1-269\"]"));}
    public WebElement radioButtonPermissionToContactNo(){return driver.findElement(By.xpath("//label[@for=\"0-269\"]"));}
    public WebElement paragraphPersonalDataCorrect(){return driver.findElement(By.xpath("//div[@data-id=\"143\"]"));}
    public WebElement radioButtonPersonalDataCorrectYesInput(){return driver.findElement(By.xpath("//input[@id=\"1-420\"]"));}
    public WebElement radioButtonPersonalDataCorrectNoInput(){return driver.findElement(By.xpath("//input[@id=\"0-420\"]"));}
    public WebElement radioButtonPersonalDataCorrectYes(){return driver.findElement(By.xpath("//label[@for=\"1-420\"]"));}
    public WebElement radioButtonPersonalDataCorrectNo(){return driver.findElement(By.xpath("//label[@for=\"0-420\"]"));}
    public WebElement paragraphReadAgain(){return driver.findElement(By.xpath("//div[@data-id=\"147\"]"));}
//    public WebElement xxx(){return driver.findElement(By.xpath("//div[@data-id=\"\"]"));}
}
