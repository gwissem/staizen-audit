package Pages.ProcesesPages.NowaSprawa;

import Pages.MainPage;
import Pages.ProcesesPages.DzwoniTelefon.StepId004;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class StepId015 extends MainPage {

    public WebElement radioButtonCrashYes(){return driver.findElement(By.xpath("//label[@for=\"1-419\"]")) ;}
    public WebElement radioButtonCrashNo(){return driver.findElement(By.xpath("//label[@for=\"0-419\"]")) ;}
    public WebElement radioButtonMedicalYes() {return driver.findElement(By.xpath("//label[@for=\"1-512\"]"));}
    public WebElement radioButtonMedicalNo() {return driver.findElement(By.xpath("//label[@for=\"2-512\"]"));}
    public WebElement radioButtonMedicalAlreadyCalled() {return driver.findElement(By.xpath("//label[@for=\"3-512\"]"));}
    public WebElement radioButtonRoutYes() {return driver.findElement(By.xpath("//label[@for=\"1-513\"]"));}
    public WebElement radioButtonRoutInput() {return driver.findElement(By.xpath("//input[@data-attribute-path=\"513\"]"));}
    public WebElement radioButtonRoutNo() {return driver.findElement(By.xpath("//label[@for=\"0-513\"]"));}
    public WebElement paragraph112() {return driver.findElement(By.xpath("//div[@data-id=\"704\"]"));}
    public WebElement paragraphRout() {return driver.findElement(By.xpath("//div[@data-id=\"702\"]"));}
    public WebElement paragraphCrashQuestion() {return driver.findElement(By.xpath("//div[@data-id=\"95\"]"));}
    public WebElement paragraphMedical() {return driver.findElement(By.xpath("//div[@data-id=\"699\"]"));}
    public WebElement paragraphWhereIsCar() {return driver.findElement(By.xpath("//div[@data-id=\"705\"]"));}
    public WebElement paragraphPrompt() {return driver.findElement(By.xpath("//div[@data-id=\"612\"]"));}
    public WebElement paragraphSafetyInformation() {return driver.findElement(By.xpath("//div[@data-id=\"613\"]"));}
    public WebElement selectWhereIsCarContainer() {return driver.findElement(By.xpath("//select[contains(@id, '-414')]"));}
    public WebElement selectWhereIsCarOption(int valueNumber) {return driver.findElement(By.xpath("//li[contains(@id, 'select2--414-result-')][" + valueNumber + "]"));}
    public WebElement paragraphExcludingCircumstances(){ return driver.findElement(By.xpath("//div[@data-id=\"858\"]"));}
    public WebElement selectExcludingCircumstancesContainer(){return driver.findElement(By.xpath("//select[contains(@data-attribute-path, '538')]"));}
    public WebElement excludingCircumstancesOption(int valueNumber) {return driver.findElement(By.xpath("//li[contains(@id, 'select2--538-result-')][" + valueNumber + "]"));}
    public List<WebElement> excludingCircumstancesList(){return driver.findElements(By.xpath("//ul[@id=\"select2--538-results\"]"));}

    public void selectValue(int intOption){
        Actions actions = new Actions(driver);
        actions.moveToElement(selectExcludingCircumstancesContainer()).click().perform();
        WebDriverWait wait = new WebDriverWait(driver, 10, 100);
        wait.until(ExpectedConditions.visibilityOfAllElements(excludingCircumstancesList()));
        Assert.assertTrue("Option is not displayed in select list.", excludingCircumstancesOption(intOption).isDisplayed());
        excludingCircumstancesOption(intOption).click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(excludingCircumstancesList()));

    }
}
