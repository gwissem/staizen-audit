package Pages.ProcesesPages.NowaSprawa;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId016 extends MainPage{

    public WebElement paragraphPermissionsInformation(){return driver.findElement(By.xpath("//div[@data-id=\"189\"]"));}
    public WebElement paragraphRepeatBasicCarData(){return driver.findElement(By.xpath("//div[@data-id=\"498\"]"));}
    public WebElement radioButtonCorrectBasicCarDataNo() {return driver.findElement(By.xpath("//label[@for=\"0-467\"]"));}
    public WebElement radioButtonCorrectBasicCarDataYes() {return driver.findElement(By.xpath("//label[@for=\"1-467\"]"));}
    public WebElement promptAdditionalVeryfication() {return driver.findElement(By.xpath("//div[@data-id=\"500\"]"));}
    public WebElement radioButtonAdditionalVeryficationNo() {return driver.findElement(By.xpath("//label[@for=\"0-467\"]"));}
    public WebElement radioButtonAdditionalVeryficationYes() {return driver.findElement(By.xpath("//label[@for=\"1-467\"]"));}
}
