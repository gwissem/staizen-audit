package Pages.ProcesesPages.NowaSprawa;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId042 extends MainPage{

    public WebElement paragraphWhichBenefits() { return driver.findElement(By.xpath("//div[@data-id=\"837\"]"));}
    public WebElement radioButtonRepair() { return driver.findElement(By.xpath("//label[@for=\"1-539\"]"));}
    public WebElement radioButtonTowing() { return driver.findElement(By.xpath("//label[@for=\"2-539\"]"));}

}
