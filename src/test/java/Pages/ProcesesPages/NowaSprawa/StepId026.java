package Pages.ProcesesPages.NowaSprawa;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId026 extends MainPage{

    public WebElement inputFirstName() {return driver.findElement(By.xpath("//input[@id=\"-80-342-64\"]"));}
    public WebElement containerFirstName() {return driver.findElement(By.xpath("//div[@data-id=\"266\"]"));}
    public WebElement inputLastName() {return driver.findElement(By.xpath("//input[@id=\"-80-342-66\"]"));}
    public WebElement containerLastName() {return driver.findElement(By.xpath("//div[@data-id=\"198\"]"));}
}
