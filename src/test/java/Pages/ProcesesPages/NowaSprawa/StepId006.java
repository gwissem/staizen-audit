package Pages.ProcesesPages.NowaSprawa;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class StepId006 extends MainPage {

    public WebElement paragraphAreYouDriver() {return driver.findElement(By.xpath("//div[@data-id=\"177\"]"));}
    public WebElement radioButtonAreYouDriverYes() {return driver.findElement(By.xpath("//label[@for=\"1-424\"]"));}
    public WebElement radioButtonAreYouDriverNo() {return driver.findElement(By.xpath("//label[@for=\"0-424\"]"));}
    public WebElement radioButtonAreYouDriverYesInput() {return driver.findElement(By.xpath("//input[@id=\"1-424\"]"));}
    public WebElement radioButtonAreYouDriverNoInput() {return driver.findElement(By.xpath("//input[@id=\"0-424\"]"));}
    public WebElement paragraphPleaseEnterYourName() {return driver.findElement(By.xpath("//div[@data-id=\"546\"]"));}
    public WebElement toolIfram(){
        return mainDriver.findElement(By.cssSelector("#embed-iframe-194"));
    }
    public WebElement paragraphExceptionalSituation() {return driver.findElement(By.xpath("//div[@data-id=\"679\"]"));}
    public WebElement radioButtonExceptionalSituationYes() {return driver.findElement(By.xpath("//label[@for=\"1-507\"]"));}
    public WebElement radioButtonExceptionalSituationNo() {return driver.findElement(By.xpath("//label[@for=\"0-507\"]"));}
    public WebElement radioButtonGiveDriverDataYes() {return driver.findElement(By.xpath("//label[@for=\"1-508\"]"));}
    public WebElement radioButtonGiveDriverDataNo() {return driver.findElement(By.xpath("//label[@for=\"0-508\"]"));}
    public WebElement buttonAddPerson() {return driver.findElement(By.cssSelector("#button-195"));}
}
