package Pages.ProcesesPages.NowaSprawa;

import Pages.MainPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

public class StepId018 extends MainPage {

    public WebElement repairButton(){ return driver.findElement(By.xpath("//button[@id=\"button-121\"]")); }
    public WebElement promptClickOnButton() { return driver.findElement(By.xpath("//div[@data-id=\"122\"]")); }
    public WebElement radioButtonSuccesRepairYes() { return driver.findElement(By.xpath("//label[@for=\"1-717\"]")); }
    public WebElement radioButtonSuccesRepairNo() { return driver.findElement(By.xpath("//label[@for=\"0-717\"]")); }

    //Sabio elements
    public String sabioURL = "https://hpst.info/vw-group/services/client/";

    public void closeSabioWindows(){
        WebDriverWait wait = new WebDriverWait(driver, 10, 500);
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        Object[] windowsNameArray = getWindowsNameArray();
        mainDriver.switchTo().window(windowsNameArray[1].toString());
        mainDriver.close();
        mainDriver.switchTo().window(windowsNameArray[0].toString());
    }

    private Object[] getWindowsNameArray() {
        Set<String> windowHandles = mainDriver.getWindowHandles();
        return windowHandles.toArray();
    }
}
