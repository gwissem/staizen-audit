package Pages.ProcesesPages.NowaSprawa;

import Helpers.Form;
import Pages.MainPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepId002 extends MainPage {

    String REQUIRED_FIELD_CLASS = "form-control custom-input validation-error";

    public WebElement paragraphPleasePrepareDocuments(){return driver.findElement(By.xpath("//div[@data-id=\"180\"]"));}
    public WebElement paragraphReadRegistrationNumber(){return driver.findElement(By.xpath("//div[@data-id=\"105\"]"));}
    public WebElement promptGetVin(){return driver.findElement(By.xpath("//div[@data-id=\"669\"]"));}
    public WebElement inputRegistrationNumber(){return driver.findElement(By.xpath("//input[@id=\"-74-72\"]"));}
    public WebElement buttonSearch(){return driver.findElement(By.xpath("//button[@id=\"button-128\"]"));}
    public WebElement buttonNoRegistrationDocument(){return driver.findElement(By.xpath("//button[@id=\"button-113\"]"));}
    public WebElement paragraphAtTheCar(){return driver.findElement(By.xpath("//div[@data-id=\"115\"]"));}
    public WebElement radioButtonAtTheCarYes(){return driver.findElement(By.xpath("//label[@for=\"1-421\"]"));}
    public WebElement radioButtonAtTheCarNo(){return driver.findElement(By.xpath("//label[@for=\"0-421\"]"));}
    public WebElement paragraphReadVin(){return driver.findElement(By.xpath("//div[@data-id=\"108\"]"));}
    public WebElement inputVin(){return driver.findElement(By.xpath("//input[@id=\"-74-71\"]"));}
    public WebElement paragraphReadRegistrationDate(){return driver.findElement(By.xpath("//div[@data-id=\"110\"]"));}
    public WebElement inputRegistrationDate(){return driver.findElement(By.xpath("//input[@id=\"-74-233\"]"));}
    public WebElement paragraphReadCurrentMileage(){return driver.findElement(By.xpath("//div[@data-id=\"184\"]"));}
    public WebElement inputCurrentMileage(){return driver.findElement(By.xpath("//input[@id=\"-74-75\"]"));}
    public WebElement checkBoxIndicativeMileage(){return driver.findElement(By.xpath("//div[@data-id=\"641\"]"));}
    public WebElement paragraphReadVinAndRegistrationNumber(){return driver.findElement(By.xpath("//div[@data-id=\"116\"]"));}
    public WebElement paragraphMissingDocuments(){return driver.findElement(By.xpath("//div[@data-id=\"114\"]"));}
    public WebElement paragraphReadLastCharVin(){return driver.findElement(By.xpath("//div[@data-id=\"621\"]"));}
    public WebElement paragraphLastCharVin(){return driver.findElement(By.xpath("//div[@data-id=\"620\"]/div/p"));}
    public WebElement checkboxCorrectVin(){return driver.findElement(By.xpath("//div[@data-id=\"181\"]"));}
    public WebElement xxx(){return driver.findElement(By.xpath("//div[@data-id=\"\"]"));}

    public void searchCar(String registrationNumber){
        WebDriverWait wait = new WebDriverWait(mainDriver, 10, 500);
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        inputRegistrationNumber().clear();
        inputRegistrationNumber().sendKeys(registrationNumber);
        buttonSearch().click();
//        wait.until(ExpectedConditions.visibilityOf(new Form().loader()));
        wait.until(ExpectedConditions.invisibilityOf(paragraphReadVin()));
        wait.until(ExpectedConditions.visibilityOf(paragraphReadVin()));

    }

    public void dataCurrentMileageInputForMinusValueValidation(String data){
    inputCurrentMileage().clear();
    Form formHelper = new Form();
    formHelper.waitForLoaderExperimental();
    Assert.assertNotEquals("Validation field is still visible - CurrentMileage Input", REQUIRED_FIELD_CLASS, inputCurrentMileage().getAttribute("class"));
    inputCurrentMileage().sendKeys(data);
    formHelper.formTitle().click();
    String[] dataPositive = data.split("-");
    Assert.assertEquals("Current mileage value is not correct ", dataPositive[1], inputCurrentMileage().getAttribute("value"));
    }
}
