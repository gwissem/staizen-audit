package Pages.ProcesesPages.NowaSprawa;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class StepId098 extends MainPage{

    public WebElement paragraphContactReason() {return driver.findElement(By.xpath("//div[@data-id=\"650\"]"));}
    public WebElement contactReasonContainer() {return driver.findElement(By.xpath("//select[contains(@data-attribute-path, '497')]"));}
    public WebElement contactReasonOption(int valueNumber) {return driver.findElement(By.xpath("//li[contains(@id, 'select2--497-result-')][" + valueNumber + "]"));}
    public List<WebElement> contactReasonList(){return driver.findElements(By.xpath("//ul[@id=\"select2--497-results\"]"));}
    public WebElement contactReasonDetailsContainer() {return driver.findElement(By.xpath("//select[contains(@data-attribute-path, '498')]"));}
    public WebElement contactReasonDetailsOption(int valueNumber) {return driver.findElement(By.xpath("(//ul[contains(@id, 'select2--498-results')]/li)[" + valueNumber + "]"));}
    public List<WebElement> contactReasonDetailsList() {return driver.findElements(By.xpath("//ul[@id=\"select2--498-results\"]"));}
}
