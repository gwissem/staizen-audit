package Pages.ProcesesPages;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DiagnosisByPhone extends MainPage{

    public WebElement radioButtonContainerInput(String value){return driver.findElement(By.xpath("//div[@id=\"quiz-step\"]//input[@value=\""+ value +"\"]"));}

    //1140.0299
    public WebElement silnik(){ return radioButtonContainerInput("1"); }
    public WebElement kolaZawieszenieUkladKierowniczy (){ return radioButtonContainerInput("2"); }

    //1140.0300
    public WebElement nieMoznaUruchomicSilnika() { return radioButtonContainerInput("1"); }

    //1140.0301
    public WebElement nieSlychacRozrusznika(){ return radioButtonContainerInput("3"); }

    //1140.0329
    public WebElement ponadTydzien(){ return radioButtonContainerInput("3"); }

    //opis diagnozy
    public WebElement inputDiagnosisDescription(){ return driver.findElement(By.xpath("//textarea[@id=\"-417\"]")); }
    public WebElement summaryDiagnosisContainer(){ return driver.findElement(By.xpath("//iframe[@id=\"embed-iframe-264\"]")); }
}
