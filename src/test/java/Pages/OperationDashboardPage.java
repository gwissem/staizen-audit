package Pages;

import Helpers.Form;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OperationDashboardPage extends MainPage {

    public WebDriver driver = driver();

    public String srcPath = "/operational-dashboard/";
    public String path = BasePage.baseUrl + srcPath;

    public WebElement formTitle(String procesId, String stepId){return driver.findElement(By.cssSelector("#render-form-"+ procesId +"-"+ stepId +""));}
    public WebElement nextButton() {return driver.findElement(By.xpath("//button[contains(@class, 'generating-form-submit button-input next-button')]"));}
    public WebElement nextButtonInactive() {return driver.findElement(By.xpath("//button[contains(@class, 'generating-form-submit button-input next-button')][@disabled]"));}

    public WebElement finishProcessPopup() {return driver.findElement(By.xpath("//div[contains(@class, 'finish-popup')]"));}
    public WebElement activeTaskId() {return driver.findElement(By.xpath("//span[@class=\"active-task-id\"]"));}

    public OperationDashboardPage goToPage() {
        driver.switchTo().defaultContent();
        MainPage mainPage = new MainPage();
        mainPage.operationDashboardLink().click();
        return new OperationDashboardPage();
    }

    public void gotoForm(String caseId) {
        WebDriverWait wait = new WebDriverWait(driver, 10, 500);
        Form formHelper = new Form();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until(ExpectedConditions.elementToBeClickable(formHelper.loadedForm()));
        driver.findElement(By.xpath("//input[@id=\"case-search\"]")).sendKeys(caseId);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//li[@data-root-id=\"" + caseId + "\"]"))));
//        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//li[@data-attribute-value-id=\"" + caseId + "\"]"))));
        WebElement taskOnList = driver.findElement(By.xpath("//li[@data-root-id=" + caseId + "]"));
//        WebElement taskOnList = driver.findElement(By.xpath("//li[@data-attribute-value-id=" + caseId + "]"));
        formHelper.scrollToElement(taskOnList);
        formHelper.clickToElement(taskOnList);
        formHelper.waitForLoaderExperimental();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//form[contains(@id, \"render-form-\")]"))));
    }

    public void createTask(String clientPhoneNumber){
        JavascriptExecutor jse = (JavascriptExecutor) mainDriver;
        jse.executeScript("document.simulationIncomingCall('" + clientPhoneNumber + "', 721, 1426, 11)");
        WebDriverWait wait = new WebDriverWait(driver, 20, 500);
        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//form[contains(@id, 'render-form-')]")));
    }
}
