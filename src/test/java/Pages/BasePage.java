package Pages;

import Config.DbSet;
import Config.Driver;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class BasePage {

    //Base Url for new DB
    public static String baseUrl = DbSet.BASE_URL;

    //Base Url for old DB
    //public static String baseUrl = DbSet.HTTP + "atlas.starter24.pl" + DbSet.PORT_NUMBER;
   public static String atlasIframe = "atlas-iframe";

    public static WebDriver mainDriver;

    public static WebDriver driver(){
        if ( mainDriver!=null ){
            return mainDriver;
        }
        System.setProperty("webdriver.chrome.driver", Driver.pathChromeDriver);
               ChromeOptions options = new ChromeOptions();
        options.addArguments(Driver.browserOptions);
        mainDriver = new ChromeDriver(options);
//        mainDriver = new ChromeDriver();
        mainDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//        mainDriver.manage().window().setPosition(new Point(-1920,0));
//        mainDriver.manage().window().maximize();
        return mainDriver;
    }

    public WebElement atlasIfram(){
        return mainDriver.findElement(By.cssSelector("#atlas-iframe"));
    }

    public String currentUrl(){
        return atlasIfram().getAttribute("src");
    }

    public static void closeBrowser(){
        mainDriver.switchTo().defaultContent();
        JavascriptExecutor jse = (JavascriptExecutor) mainDriver;
        jse.executeScript("$(window).off('beforeunload');");
        mainDriver.quit();
        mainDriver = null;
    }

    public static void returnToDefoultIfram(){
        mainDriver.switchTo().defaultContent();
        JavascriptExecutor jse = (JavascriptExecutor) mainDriver;
        jse.executeScript("$(window).off('beforeunload');");
    }
}