package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BenefitsPanel extends MainPage{

    public WebElement benefitsPanelLoadedForm() {return driver.findElement(By.xpath("//form[@id=\"render-form-1011-010\"][contains(@class, 'form-loaded')]"));}
    public WebElement repairContainerActive() { return driver.findElement(By.xpath("//div[@class=\"single-service float \"]/div[contains(text(), 'Naprawa')]"));}
    public WebElement repairContainerInactive() { return driver.findElement(By.xpath("//div[@class=\"single-service float inactive\"]/div[contains(text(), 'Naprawa')]"));}
    public WebElement repairActivateServiceButton() {return driver.findElement(By.xpath("//div[@data-start-step=\"1009.054\"][contains(@class, 'activate-service-btn')]"));}
    public WebElement towingContainerActive() { return driver.findElement(By.xpath("//div[@class=\"single-service float \"]/div[contains(text(), 'Holowanie')]"));}
    public WebElement towingContainerInactive() { return driver.findElement(By.xpath("//div[@class=\"single-service float inactive\"]/div[contains(text(), 'Holowanie')]"));}
    public WebElement towingActivateServiceButton() {return driver.findElement(By.xpath("//div[@data-start-step=\"1009.034\"][contains(@class, 'activate-service-btn')]"));}

    public void verifyAreRepairContainerIsActive() {
        Assert.assertTrue("Repair container is inactive", repairContainerActive().isDisplayed());
        Assert.assertTrue("Repair activation button is not displayed", repairActivateServiceButton().isDisplayed());
    }

    public void verifyAreRepairContainerIsInactive() {
        Assert.assertTrue("Repair container is active", repairContainerInactive().isDisplayed());
    }

    public void verifyAreTowingContainerIsActive() {
        Assert.assertTrue("Towing container is not displayed", towingContainerActive().isDisplayed());
        Assert.assertTrue("Towing activation button is not displayed", towingActivateServiceButton().isDisplayed());
    }

    public void verifyAreTowingContainerIsInactive() {
        Assert.assertTrue("Towing container is active", towingContainerInactive().isDisplayed());
    }

    public void waitForBenefitsPanelIsLoaded() {
        WebDriverWait wait = new WebDriverWait(driver, 10, 500);
        wait.until(ExpectedConditions.elementToBeClickable(benefitsPanelLoadedForm()));
    }
}
