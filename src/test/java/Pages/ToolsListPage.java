package Pages;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ToolsListPage extends MainPage {

    public final String ATLAS_TOOL_QUERY_INPUT = "atlas_tool_query";
    public WebDriver driver = driver();

    public String srcPath = "/admin/tools";
    public String path = BasePage.baseUrl + srcPath;

    public String toolDescriptionIframe = "atlas_tool_translations_pl_description_ifr";

    public WebElement searchInput(){
       return driver.findElement(By.cssSelector("#atlas_searcher_phrase"));
    }
    public WebElement addToolButton() {
        return driver.findElement(By.xpath("//a[i[contains(@class, 'fa fa-plus')]]"));
    }
    public WebElement toolNameInputPL(){
        return driver.findElement(By.cssSelector("#atlas_tool_translations_pl_name"));
    }
    public WebElement toolNameInputEN(){
        return driver.findElement(By.cssSelector("#atlas_tool_translations_en_name"));
    }
    public WebElement toolDescriptionInput(){
        return driver.findElement(By.xpath("//body[@id='tinymce']/p"));
    }
    public String toolDescriptionInputLocator = "#atlas_tool_translations_pl_description";
    public WebElement toolGroupNameSelect(){
        return driver.findElement((By.cssSelector("#select2-atlas_tool_toolGroup-container")));
    }
    public WebElement toolGroupNameInput(){
        return driver.findElement(By.cssSelector(".select2-search__field"));
    }
    public WebElement toolGroupNameResults(String toolGroupName){
        return driver.findElement(By.xpath("//li[contains(@id, 'select2-atlas_tool_toolGroup-result')][contains(@text, '" + toolGroupName + "')]"));
    }
    public WebElement submitButton(){
        return driver.findElement(By.xpath("//form[@id='tool-editor-form']//button[@type='submit']"));
    }
    public WebElement tabPL(){
        return driver.findElement(By.xpath("//a[contains(@data-target, 'translationsFields-pl')]"));
    }
    public WebElement tabEN(){
        return driver.findElement(By.xpath("//a[contains(@data-target, 'translationsFields-en')]"));
    }
    public WebElement queryInput(){
        return driver.findElement(By.cssSelector("#atlas_tool_query"));
    }
    public WebElement connectionInput(){
        return driver.findElement(By.cssSelector("#atlas_tool_connectionString"));
    }

    public void goToPage() {
        driver.switchTo().defaultContent();
        MainPage mainPage = new MainPage();
        mainPage.toolsListLink().click();
    }

    private void _createTool( String toolNamePL, String toolNameEN, String toolDescription, String toolGroupName, String queryText ){
        addToolButton().click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(toolNameInputPL()));
        toolNameInputPL().sendKeys(toolNamePL);
        ((JavascriptExecutor) driver).executeScript("$(\" "+ toolDescriptionInputLocator + "\").text('" + toolDescription + "')");
        tabEN().click();
        toolNameInputEN().sendKeys(toolNameEN);
        ((JavascriptExecutor) driver).executeScript("(CodeMirror.fromTextArea(document.getElementById('" + ATLAS_TOOL_QUERY_INPUT + "'))).getDoc().setValue('" + queryText + "')") ;
        selectToolGroup(toolGroupName);
        submitButton().click();
    }

    public void selectToolGroup(String toolGroupName) {
        if (toolGroupName.length() >= 1) {
            //driver.switchTo().frame(atlasIframe);
            toolGroupNameSelect().click();
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(toolGroupNameInput()));
            toolGroupNameInput().sendKeys(toolGroupName);
            wait.until(ExpectedConditions.visibilityOf(toolGroupNameResults(toolGroupName)));
            toolGroupNameResults(toolGroupName).click();
            Assert.assertEquals("Wrong selected group name", toolGroupName, toolGroupNameSelect().getText());
        }
    }

    public void createToolUnassignedToGroup(String toolNamePL, String toolNameEN, String toolDescription, String queryText){
        _createTool(toolNamePL, toolNameEN, toolDescription, "", queryText);
    }
}