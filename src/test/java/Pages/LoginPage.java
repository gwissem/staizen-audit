package Pages;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import  java.lang.String;

import static java.lang.Thread.sleep;

public class LoginPage extends MainPage{


    public WebDriver driver = driver();
    public String srcPath = "/login";
    public String path = BasePage.baseUrl + srcPath;

    public MainPage topBar = new MainPage();

    public WebElement loginInput(){
       return driver.findElement(By.cssSelector("#username"));
    }

    public WebElement passwordInput(){
        return driver.findElement(By.cssSelector("#password"));
    }

    public WebElement submitButton(){
        return driver.findElement(By.cssSelector("button[type='submit']"));
    }

    public WebElement loginForm(){
        return driver.findElement(By.cssSelector("#login-form"));
    }

    public WebElement wrongLoginAlert(){
        return driver.findElement(By.cssSelector(".alert.alert-danger.m-b-1"));
    }

    public WebElement closeAlertButton(){
        return driver.findElement(By.cssSelector("button.close"));
    }

    public void login(String userName, String pass)  {
        driver.switchTo().defaultContent();
        if ( driver.findElements(By.xpath("//span[@class=\"username\"]")).size() == 0 ) {
            _login(userName, pass);
        }
         else{
           if ( !topBar.userName().getText().equals( userName )) {
               logout();
               _login(userName, pass);
           }
        }
    }

    public void goToPage(){
        driver.get(path);
    }

    public void _login(String userName, String pass){
        driver.get(path);
        fillLoginForm(userName, pass);
        submitButton().click();
    }

    public void logout(){
        driver.switchTo().defaultContent();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("$(window).off('beforeunload');");
        driver.get(path);
    }

    public void fillLoginForm(String userName, String pass){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(loginInput()));
        loginInput().clear();
        loginInput().sendKeys(userName);
        passwordInput().clear();
        passwordInput().sendKeys(pass);
    }

    public void validationLogin( String userName, String pass){
        fillLoginForm(userName, pass);
        submitButton().click();
        WebDriverWait wait = new WebDriverWait(driver, 10, 200);
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.cssSelector(".wow.fadeInDown"))));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".wow.fadeInDown"))));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("Login alert is not display", "Nieprawidłowy login lub hasło.", wrongLoginAlert().getText());
        closeAlertButton().click();
        wait.until(ExpectedConditions.invisibilityOf(wrongLoginAlert()));
    }
}
