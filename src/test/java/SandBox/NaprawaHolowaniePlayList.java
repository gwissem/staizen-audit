package SandBox;

import Config.DbSet;
import Helpers.DbManagement;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

public class NaprawaHolowaniePlayList extends DbSet{

    public static final String LOKALIZACJA_1 = "LOK-1, ul. Suchodolska, Warszawa";
    public static final String LOKALIZACJA_2 = "LOK-2, Droga, Karczew";
    public static final String LOKALIZACJA_3 = "LOK-3, Górczewska 124, Warszawa lub jakaś droga";
    public static final String LOKALIZACJA_4 = "LOK-4, ul. Warszawska, Kałuszyn";
    NaprawaHolowanieData naprawaHolowanie = new NaprawaHolowanieData(DB_NAME);
    int AUTOSTRADA = 1;
    int MIASTO = 0;
    int WYPADEK = 1;
    int AWARIA = 0;

    String[] localisation1 = {"21.072833", "52.241639"};
    String[] localisation2 = {"21.263425", "52.048800"};
    String[] localisation3 = {"20.928944", "52.241630"};
    String[] localisation4 = {"21.8379", "52.21002"};
    String[] localisationPoznan = {"16.915305", "52.430286"};
    String[] localisationWro = {"17.034183", "51.077292"};
    String[] localisationKrk = {"19.938471", "50.089414"};
    String[] localisationParis = {"12.238242", "51.839213"};

    @Test
    public void dezaktywacjaWszystkichZadan() throws SQLException, ClassNotFoundException {
        naprawaHolowanie.processInstanceSetInactive();
//        naprawaHolowanie.processInstanceSetInactiveExperimental();
    }

    @Test
    public void awariaAutostradaVwOsobowe() throws SQLException, ClassNotFoundException {
        System.out.println("Platforma VW Osobowe");
        //Scenariusz 1
        System.out.println();
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 1 A-A-A-A:");
        System.out.println(LOKALIZACJA_1 + ", Diagnoza: APN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, AUTOSTRADA, "APN", 2500, 1, localisation1);
        System.out.println();
        System.out.println(LOKALIZACJA_2 + ", Diagnoza: APN, DMC: 2400, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, AUTOSTRADA, "APN", 2400, 1, localisation2);
        System.out.println();
        System.out.println(LOKALIZACJA_3 + ", Diagnoza: APN, DMC: 2600, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, AUTOSTRADA, "APN", 2600, 1, localisation3);
        System.out.println();
        System.out.println(LOKALIZACJA_4 + ", Diagnoza: APN, DMC: 250, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, AUTOSTRADA, "APN", 2500, 1, localisation4);
        //Scenariusz 2
        System.out.println();
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 2: A-A-A-B:");
        System.out.println(LOKALIZACJA_1 + ", Diagnoza: APN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "APN", 2500, 3, localisation1);
        System.out.println();
        System.out.println(LOKALIZACJA_2 + ", Diagnoza: APN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "APN", 2500, 3, localisation2);
        System.out.println();
        System.out.println(LOKALIZACJA_3 + ", Diagnoza: APN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "APN", 2500, 3, localisation3);
        System.out.println();
        System.out.println(LOKALIZACJA_4 + ", Diagnoza: APN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "APN", 2500, 3, localisation4);
        //Scenariusz 3
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 3 A-A-B-A:");
        System.out.println(LOKALIZACJA_1 + ", Diagnoza: AN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AN", 2500, 1, localisation1);
        System.out.println();
        System.out.println(LOKALIZACJA_2 + ", Diagnoza: AN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AN", 2500, 1, localisation2);
        System.out.println();
        System.out.println(LOKALIZACJA_3 + ", Diagnoza: AN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AN", 2500, 1, localisation3);
        System.out.println();
        System.out.println(LOKALIZACJA_4 + ", Diagnoza: AN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AN", 2500, 1, localisation4);
        //Scenariusz 4
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 4: A-A-B-B:");
        System.out.println(LOKALIZACJA_1 + ", Diagnoza: AN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AN", 2500, 3, localisation1);
        System.out.println();
        System.out.println(LOKALIZACJA_2 + ", Diagnoza: AN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AN", 2500, 3, localisation2);
        System.out.println();
        System.out.println(LOKALIZACJA_3 + ", Diagnoza: AN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AN", 2500, 3, localisation3);
        System.out.println();
        System.out.println(LOKALIZACJA_4 + ", Diagnoza: AN, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AN", 2500, 3, localisation4);
        //Scenariusz 5
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 5: A-A-C-A:");
        System.out.println(LOKALIZACJA_1 + ", Diagnoza: AH, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AH", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2 + ", Diagnoza: AH, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AH", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3 + ", Diagnoza: AH, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AH", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4 + ", Diagnoza: AH, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AH", 2500, 1, localisation4);
        //Scenariusz 6
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 6: A-A-C-B:");
        System.out.println(LOKALIZACJA_1 + ", Diagnoza: AH, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AH", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2 + ", Diagnoza: AH, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AH", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3 + ", Diagnoza: AH, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AH", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4 + ", Diagnoza: AH, DMC: 2500, ");
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, 1, "AH", 2500, 3, localisation4);
    }

    @Test
    public void awariaAutostradaVwUzytkowe() throws SQLException, ClassNotFoundException {
        System.out.println("Platforma VW Uzytkowe");
        //Scenariusz 7
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 7 B-A-A-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "APN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "APN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "APN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "APN", 2500, 1, localisation4);
        //Scenariusz 8
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 8: B-A-A-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "APN", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "APN", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "APN", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "APN", 2500, 3, localisation4);
        //Scenariusz 9
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 9 B-A-B-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AN", 2500, 1, localisation4);
        //Scenariusz 10
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 10: B-A-B-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AN", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AN", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AN", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AN", 2500, 3, localisation4);
        //Scenariusz 11
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 11 B-A-C-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AH", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AH", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AH", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AH", 2500, 1, localisation4);
        //Scenariusz 12
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 12: B-A-C-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AH", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AH", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AH", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 1, "AH", 2500, 3, localisation4);
    }

    @Test
    public void awariaAutostradaAudi() throws SQLException, ClassNotFoundException {
        System.out.println("Platforma AUDI");
        //Scenariusz 13
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 13 C-A-A-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "APN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "APN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "APN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "APN", 2500, 1, localisation4);
        //Scenariusz 14
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 14: C-A-A-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "APN", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "APN", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "APN", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "APN", 2500, 3, localisation4);
        //Scenariusz 15
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 15 C-A-B-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AN", 2500, 1, localisation4);
        //Scenariusz 16
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 16: C-A-B-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AN", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AN", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AN", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AN", 2500, 3, localisation4);
        //Scenariusz 17
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 17 C-A-C-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AH", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AH", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AH", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AH", 2500, 1, localisation4);
        //Scenariusz 18
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 18: C-A-C-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AH", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AH", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AH", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(2, 1, "AH", 2500, 3, localisation4);
    }

    @Test
    public void awariaAutostradaSkoda() throws SQLException, ClassNotFoundException {
        System.out.println("Platforma SKODA");
        //Scenariusz 19
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 19 D-A-A-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "APN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "APN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "APN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "APN", 2500, 1, localisation4);
        //Scenariusz 20
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 20: D-A-A-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "APN", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "APN", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "APN", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "APN", 2500, 3, localisation4);
        //Scenariusz 21
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 21 D-A-B-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AN", 2500, 1, localisation4);
        //Scenariusz 22
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 22: D-A-B-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AN", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AN", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AN", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AN", 2500, 3, localisation4);
        //Scenariusz 23
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 23: D-A-C-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AH", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AH", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AH", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AH", 2500, 1, localisation4);
        //Scenariusz 24
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 24: D-A-C-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AH", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AH", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AH", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 1, "AH", 2500, 3, localisation4);
    }

    @Test
    public void awariaMiastoVwOsobowe() throws SQLException, ClassNotFoundException {
        System.out.println("Platforma VW Osobowe");
        //Scenariusz 25
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 25: A-B-A-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "APN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "APN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "APN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "APN", 2500, 1, localisation4);
        //Scenariusz 26
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 26: A-B-B-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AN", 2500, 1, localisation4);
        //Scenariusz 27
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 27: A-B-C-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 1, localisation4);
        //Scenariusz 28
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 28: A-B-C-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwOsobowePanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 3, localisation4);
    }

    @Test
    public void awariaMiastoVwUzytkowe() throws SQLException, ClassNotFoundException {
        System.out.println("Platforma VW Uzytkowe");
        //Scenariusz 29
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 29 B-B-A-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "APN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "APN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "APN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "APN", 2500, 1, localisation4);
        //Scenariusz 30
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 30: B-B-B-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AN", 2500, 1, localisation4);
        //Scenariusz 31
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 31: B-B-C-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AH", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AH", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AH", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AH", 2500, 1, localisation4);
        //Scenariusz 32
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 32: B-B-C-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AH", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AH", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AH", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.vwUzytkowePanelSwiadczen(2, 2, "AH", 2500, 3, localisation4);
    }

    @Test
    public void awariaMiastoAudi() throws SQLException, ClassNotFoundException {
        System.out.println("Platforma AUDI");
        //Scenariusz 33
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 33: C-B-A-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "APN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "APN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "APN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "APN", 2500, 1, localisation4);
        //Scenariusz 34
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 34: C-B-B-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AN", 2500, 1, localisation4);
        //Scenariusz 35
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 35: C-B-C-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 1, localisation4);
        //Scenariusz 36
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 36: C-B-C-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.audiPanelSwiadczen(AWARIA, MIASTO, "AH", 2500, 3, localisation4);
    }

    @Test
    public void awariaMiastoSkoda() throws SQLException, ClassNotFoundException {
        System.out.println("Platforma SKODA");
        //Scenariusz 37
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 37: D-B-A-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "APN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "APN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "APN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "APN", 2500, 1, localisation4);
        //Scenariusz 38
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 38: D-B-B-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AN", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AN", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AN", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AN", 2500, 1, localisation4);
        //Scenariusz 39
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 39: D-B-C-A:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AH", 2500, 1, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AH", 2500, 1, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AH", 2500, 1, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AH", 2500, 1, localisation4);
        //Scenariusz 40
        System.out.println("Sprawy w których diagnoza wskazuje na awarię - scenariusz 40: D-B-C-B:");
        System.out.println(LOKALIZACJA_1);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AH", 2500, 3, localisation1);
        System.out.println(LOKALIZACJA_2);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AH", 2500, 3, localisation2);
        System.out.println(LOKALIZACJA_3);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AH", 2500, 3, localisation3);
        System.out.println(LOKALIZACJA_4);
        naprawaHolowanie.skodaPanelSwiadczen(2, 2, "AH", 2500, 3, localisation4);
    }

    //MACIEJ SPRAWY DO TESTÓW QUIZU

    @Test
    public void awariaMiastoQuiz() throws SQLException, ClassNotFoundException {
        naprawaHolowanie.newCaseDiagnoza(AWARIA, MIASTO);
        naprawaHolowanie.newCaseDiagnoza(AWARIA, MIASTO);
        naprawaHolowanie.newCaseDiagnoza(AWARIA, MIASTO);
        naprawaHolowanie.newCaseDiagnoza(AWARIA, MIASTO);
        naprawaHolowanie.newCaseDiagnoza(AWARIA, MIASTO);
    }

    @Test
    public void awariaAutostradaQuiz() throws SQLException, ClassNotFoundException {
        naprawaHolowanie.newCaseDiagnoza(AWARIA, AUTOSTRADA);
        naprawaHolowanie.newCaseDiagnoza(AWARIA, AUTOSTRADA);
        naprawaHolowanie.newCaseDiagnoza(AWARIA, AUTOSTRADA);
        naprawaHolowanie.newCaseDiagnoza(AWARIA, AUTOSTRADA);
        naprawaHolowanie.newCaseDiagnoza(AWARIA, AUTOSTRADA);
    }

    @Test
    public void wypadekMiastoQuiz() throws SQLException, ClassNotFoundException {
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, MIASTO);
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, MIASTO);
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, MIASTO);
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, MIASTO);
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, MIASTO);
    }

    @Test
    public void wypadekAutostrada() throws SQLException, ClassNotFoundException {
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, AUTOSTRADA);
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, AUTOSTRADA);
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, AUTOSTRADA);
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, AUTOSTRADA);
        naprawaHolowanie.newCaseDiagnoza(WYPADEK, AUTOSTRADA);
    }

    @Test
    public void panelSwiadczenLokalizacjaParis() throws SQLException, ClassNotFoundException {
//        System.out.println("VW uzytkowe");
//        naprawaHolowanie.vwUzytkowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//        naprawaHolowanie.vwUzytkowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//        naprawaHolowanie.vwUzytkowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//
//        System.out.println("VW");
//                naprawaHolowanie.vwOsobowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//                naprawaHolowanie.vwOsobowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//                naprawaHolowanie.vwOsobowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//                naprawaHolowanie.vwOsobowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//                naprawaHolowanie.vwOsobowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//                naprawaHolowanie.vwOsobowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//                naprawaHolowanie.vwOsobowePanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//
//        System.out.println("SKODA");
//        naprawaHolowanie.skodaPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//        naprawaHolowanie.skodaPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//        naprawaHolowanie.skodaPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//        naprawaHolowanie.skodaPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//        naprawaHolowanie.skodaPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//        naprawaHolowanie.skodaPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
//        naprawaHolowanie.skodaPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);

        System.out.println("AUDI");
        naprawaHolowanie.audiPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
        naprawaHolowanie.audiPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
        naprawaHolowanie.audiPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
        naprawaHolowanie.audiPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
        naprawaHolowanie.audiPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
        naprawaHolowanie.audiPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);
        naprawaHolowanie.audiPanelSwiadczen(WYPADEK, MIASTO, "WH", 2500, 1, localisationWro);


    }
}
