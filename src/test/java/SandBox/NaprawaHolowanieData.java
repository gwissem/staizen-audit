package SandBox;

import Data.Cars.Car;
import Helpers.DbManagement;
import Helpers.GenerateData;
import Data.Cars.Audi;
import Data.Cars.Skoda;
import Data.Cars.VwOsobowe;
import Data.Cars.VwUzytkowe;

import java.sql.*;

public class NaprawaHolowanieData extends DbManagement{

    GenerateData generateData = new GenerateData();
    String VW = "vwOsobowe";
    String UZYTKOWE = "vwUżytkowe";
    String AUDI = "audi";
    String SKODA = "skoda";

    public NaprawaHolowanieData(String dbName) {
        super(dbName);
    }

    public int vwOsobowePanelSwiadczen(int typeOfEvent, int routType, String quizName, int dmc, int personToTransport, String[] localisation ) throws SQLException, ClassNotFoundException {
//        newCasePanelSwiadczen(typeOfEvent, dmc, routType, quizName, localisation, personToTransport, VW, "Polska");
        return newCasePanelSwiadczen(typeOfEvent, dmc, routType, quizName, localisation, personToTransport, VW, "Polska");
//        return caseId;
    }

    public int vwUzytkowePanelSwiadczen(int typeOfEvent, int routType, String quizName, int dmc, int personToTransport, String[] localisation ) throws SQLException, ClassNotFoundException {
       return newCasePanelSwiadczen( typeOfEvent, dmc, routType, quizName, localisation, personToTransport, UZYTKOWE, "Polska");
    }

    public int audiPanelSwiadczen(int typeOfEvent, int routType, String quizName, int dmc, int personToTransport, String[] localisation ) throws SQLException, ClassNotFoundException {
       return newCasePanelSwiadczen( typeOfEvent, dmc, routType, quizName, localisation, personToTransport, AUDI, "Polska");
    }

    public int skodaPanelSwiadczen(int typeOfEvent, int routType, String quizName, int dmc, int personToTransport, String[] localisation ) throws SQLException, ClassNotFoundException {
        return newCasePanelSwiadczen( typeOfEvent, dmc, routType, quizName, localisation, personToTransport, SKODA, "Polska");
    }

    public int vwOsobowePanelSwiadczenFrancja(int typeOfEvent, int routType, String quizName, int dmc, int personToTransport, String[] localisation ) throws SQLException, ClassNotFoundException {
        return newCasePanelSwiadczen( typeOfEvent, dmc, routType, quizName, localisation, personToTransport, VW, "Niemcy");
    }

    /**
     *
     * @param typeOfEvent 1- crash; 0- repair
     * @param dmc
     * @param routType 1-autostrada, 2-zwykła droga
     * @param quizName options: APN, AN, AH, WH;
     * @param localisation współrzędne (dł, sz);
     * @param personToTransport 1 or 3
     * @param platformName
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public int newCasePanelSwiadczen(int typeOfEvent, int dmc, int routType, String quizName, String[] localisation, int personToTransport, String platformName, String country) throws SQLException, ClassNotFoundException {
        String phoneNumber = generateData.phoneNumber();
        String firstName = generateData.firstName();
        String lastName = generateData.lastName();
        Connection con = connect();
        int previousProcessId = 0;
        switch (platformName){
            case "vwOsobowe":
                previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 11);
                break;
            case "vwUżytkowe":
                previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 11);
                break;
            case "skoda":
                previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 6);
                break;
            case "audi":
                previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 31);
                break;
        }
        //Next_procedure execute
        int caseId = previousProcessId;
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        int newCaseIdT = _procedureNextStep(con, previousProcessId, 1);
        //on step enter driver data
        //set is driver
        int newCaseId = getGroupProcessIdByInstanceId(con, newCaseIdT);
//        int caseId = newCaseId;
        _procedurePFormControls(newCaseIdT, con);
        CallableStatement st = _procedureAttributeEdit(con, "424", newCaseId);
        st.setInt("valueInt", 1);
        st.execute();
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, newCaseIdT, 1);
        System.out.println("---------------------------------------------------------------------- ");
        System.out.println("Nr id który został przekazany dalej : " + previousProcessId);
        System.out.println("---------------------------------------------------------------------- ");
        //formula safety
        //set crash or repair
        setCrashOrRepair(con, previousProcessId, typeOfEvent);
        setRout(con, previousProcessId, routType);
        if (platformName.equals(AUDI)) {
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        }
        int newCaseId1 = _procedureNextStep(con, previousProcessId, 1);
        //set car
        //set platform
        Car car = setPlatform(platformName);
        System.out.println("Pojazd: " + car.getRegistrationNumber() + ", " + car.getVin() + ", " + car.getRegistrationDate());
        setBasicCarData(con, newCaseId1, previousProcessId, car);
        //go to next step
        previousProcessId = _procedureNextStep(con, newCaseId1, 1);
        //naprawa przez telefon
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        //diagnoza
        switch( quizName ){
            case "APN":
                previousProcessId = setQuizAPN(con, previousProcessId);
                break;
            case "AN":
                previousProcessId = setQuizAN(con, previousProcessId);
                break;
            case "AH":
                previousProcessId = setQuizAH(con, previousProcessId);
                break;
            case "WH":
                previousProcessId = setQuizWH(con, previousProcessId);
        }
        //opis diagnozy
//        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
        //step 1011.040
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        //wprowadz pozostałe dane
        setDmc(con, previousProcessId, dmc);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        // komunikat o brakujących danych
//        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        //wprowadź liczbę osób
        setHumanToTransport(con, previousProcessId, personToTransport);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        //zlokalizuj klienta
        setLocalisation(con, previousProcessId, localisation, country);
        if (!platformName.equals(AUDI)){
            previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        }
        disconnect(con);
        return newCaseId;
//        return previousProcessId;
    }

    private Car setPlatform(String platformName) {
            if (platformName == "vwOsobowe") {
                return new VwOsobowe();
            } else {
                if (platformName == "vwUżytkowe") {
                    return new VwUzytkowe();
                } else {
                    if (platformName == "audi") {
                        return new Audi();
                    } else {
                            return new Skoda();
                        }
                    }
                }
            }

    private void setBasicCarData(Connection con, int newCaseId, int previousProcessId, Car car) throws SQLException, ClassNotFoundException {
        _procedurePFormControls(newCaseId, con);
        Statement cs = con.createStatement();
        ResultSet groupIds = cs.executeQuery("SELECT root_id FROM process_instance WHERE id = '" + previousProcessId + "'");
        int groupProcessId = 23;
        while (groupIds.next()) {
            groupProcessId = groupIds.getInt("root_id");
        }
        groupIds.close();
        String query = "SELECT id FROM attribute_value WITH (NOLOCK) WHERE attribute_path = '74' and group_process_instance_id = " + groupProcessId ;
        ResultSet parentID = cs.executeQuery(query);
        int parentAttributeValueId = 0;
        while (parentID.next()) {
            parentAttributeValueId =  parentID.getInt("id");
        }
        //set registration number
        CallableStatement st = _procedureAttributeEdit(con, "74,72", groupProcessId);
        st.setString("valueString", car.getRegistrationNumber());
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.execute();
        //set vin
        st = _procedureAttributeEdit(con, "74,71", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        setStringValue(st, car.getVin());
        st.execute();
        //set first registration date
        Date registrationDate = Date.valueOf(car.getRegistrationDateSql());
        st = _procedureAttributeEdit(con, "74,233", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.setDate("valueDate", registrationDate);
        st.execute();
        //set current mileage
        int maxMileage = Integer.parseInt(car.getMaxMileage());
        int mileage = maxMileage - 10;
        st = _procedureAttributeEdit(con, "74,75", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.setInt("valueInt", mileage);
        st.execute();
        //check correct vin
        st = _procedureAttributeEdit(con, "422", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.setInt("valueInt", 1);
        st.execute();
    }

    private void setHumanToTransport(Connection con, int processId, int personToTransport) throws SQLException, ClassNotFoundException {
        int newCaseId = getGroupProcessIdByInstanceId(con, processId);
        _procedurePFormControls(processId, con);
        CallableStatement st = _procedureAttributeEdit(con, "428", newCaseId);
        st.setInt("valueInt", personToTransport);
        st.execute();
        st = _procedureAttributeEdit(con, "79", newCaseId);
        st.setInt("valueInt", personToTransport);
        st.execute();
        st = _procedureAttributeEdit(con, "492", newCaseId);
        st.setInt("valueInt", 0);
        st.execute();
    }

    /**
     *
     * @param con
     * @param processId
     * @param routType 1-autostrada; 0-normalna
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void setRout(Connection con, int processId, int routType) throws SQLException, ClassNotFoundException {
        int newCaseId = getGroupProcessIdByInstanceId(con, processId);
        _procedurePFormControls(processId, con);
        CallableStatement st = _procedureAttributeEdit(con, "513", newCaseId);
        st.setInt("valueInt", routType);
        st.execute();
        if (routType == 1){
            st = _procedureAttributeEdit(con, "414", newCaseId);
            st.setInt("valueInt", 2);
            st.execute();
        }
    }

    private void setLocalisation(Connection con, int processId, String[] localisation, String country ) throws SQLException, ClassNotFoundException {
        int newCaseId = getGroupProcessIdByInstanceId(con, processId);
        _procedurePFormControls(processId, con);
        CallableStatement st = _procedureAttributeEdit(con, "101,85,92", newCaseId);
        st.setString("valueString", localisation[0]);
        st.execute();
        st = _procedureAttributeEdit(con, "101,85,93", newCaseId);
        st.setString("valueString", localisation[1]);
        st.execute();
        st = _procedureAttributeEdit(con, "101,85,86", newCaseId);
        st.setString("valueString", country);
        st.execute();
    }

    private void setDmc(Connection con, int processId, int dmc) throws SQLException, ClassNotFoundException {
        int newCaseId = getGroupProcessIdByInstanceId(con, processId);
        _procedurePFormControls(processId, con);
        CallableStatement st = _procedureAttributeEdit(con, "74,76", newCaseId);
        st.setInt("valueInt", dmc);
        st.execute();
    }

    /**
     *
     * @param con
     * @param processId
     * @param variant 1- crash; 0- repair
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void setCrashOrRepair(Connection con, int processId, int variant) throws SQLException, ClassNotFoundException {
        int newCaseId = getGroupProcessIdByInstanceId(con, processId);
        System.out.println("Numer sprawy: " + newCaseId);
        _procedurePFormControls(newCaseId, con);
        CallableStatement st = _procedureAttributeEdit(con, "419", newCaseId);
        st.setInt("valueInt", variant);
        st.execute();
        st = _procedureAttributeEdit(con, "491", newCaseId);
        if(variant == 1){
        st.setInt("valueInt", 1);
        } else {
            st.setInt("valueInt", 2);
        }
        st.execute();
    }

    private int setQuizWH(Connection con, int previousProcessId) throws SQLException, ClassNotFoundException {
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        return _procedureNextStep(con, previousProcessId, 2);
    }

    private int setQuizAPN(Connection con, int previousProcessId) throws SQLException, ClassNotFoundException {
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        return _procedureNextStep(con, previousProcessId, 1);
    }

    private int setQuizAN(Connection con, int previousProcessId) throws SQLException, ClassNotFoundException {
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
//        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
        return _procedureNextStep(con, previousProcessId, 2);
    }

    private int setQuizAH(Connection con, int previousProcessId) throws SQLException, ClassNotFoundException {
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
//        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
        return _procedureNextStep(con, previousProcessId, 4);
    }

    public void newCaseDiagnoza(int typeOfEvent, int routType) throws SQLException, ClassNotFoundException {
        String phoneNumber = generateData.phoneNumber();
        String firstName = generateData.firstName();
        String lastName = generateData.lastName();
        Connection con = connect();
        int previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 11);
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        int newCaseIdT = _procedureNextStep(con, previousProcessId, 1);
        //on step enter driver data
        //set is driver
        int newCaseId = getGroupProcessIdByInstanceId(con, newCaseIdT);
        _procedurePFormControls(newCaseIdT, con);
        CallableStatement st = _procedureAttributeEdit(con, "424", newCaseId);
        st.setInt("valueInt", 1);
        st.execute();
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, newCaseIdT, 1);
        //formula safety
        //set crash or repair
        setCrashOrRepair(con, previousProcessId, typeOfEvent);
        setRout(con, previousProcessId, routType);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        //set car
        newCaseId = _procedureNextStep(con, previousProcessId, 1);
        //set platform
        Car car = setPlatform("vwOsobowe");
        System.out.println("Pojazd: " + car.getRegistrationNumber() + ", " + car.getVin());
        setBasicCarData(con, newCaseId, previousProcessId, car);
        //go to next step
        previousProcessId = _procedureNextStep(con, newCaseId, 1);
        //naprawa przez telefon
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
//        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
    }
}
