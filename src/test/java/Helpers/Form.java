package Helpers;

import Data.Users;
import Pages.BasePage;
import Pages.LoginPage;
import Pages.OperationDashboardPage;
import Pages.ProcesesPages.NowaSprawa.StepId015;
import Tests.AbstractTest;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static Pages.BasePage.mainDriver;


public class Form extends AbstractTest{

    WebDriverWait wait = new WebDriverWait(mainDriver, 10, 200);

    String REQUIRED_FIELD_CLASS = "form-control custom-input validation-error";
    String REQUIRED_DATEPICKER_CLASS = "date-picker form-control custom-input to-now validation-error";

    OperationDashboardPage operationDashboardPage = new OperationDashboardPage();
    GenerateData generateData = new GenerateData();
    DbManagement db = new DbManagement(DB_NAME);

    public List<WebElement> formContainer() {return mainDriver.findElements(By.xpath("//div[contains(@class, 'step-form-grid-stack')]/div"));}

    public Object checkRadioButtonValue(String attributePath, StepId015 stepId015){
        JavascriptExecutor jse = (JavascriptExecutor) mainDriver;
        mainDriver.switchTo().defaultContent();
        mainDriver.switchTo().frame(BasePage.atlasIframe);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[@for=\"1-513\"]")));
        System.out.print("att: " + attributePath);
        Object value = jse.executeScript("$('input[data-attribute-path=\"" + attributePath + "\"]:checked').val()");
        System.out.print("value: " + value);
        return value;
    }

    public int goToStepInProcess(String ProcessId, String StepId) throws SQLException, ClassNotFoundException {
//        db.processInstanceSetInactive();
        int caseID = db.createProcess(ProcessId + "." + StepId);
        String caseId = Integer.toString(caseID);
        operationDashboardPage.goToPage();
        Assert.assertEquals("Page url is not correct", operationDashboardPage.path, operationDashboardPage.currentUrl());
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(caseId);
        return caseID;
    }

    public WebElement loader(){return mainDriver.findElement(By.xpath("//div[@class=\"loader-wrapper\"][contains(@style, 'display: block;')]"));}
    public WebElement loaderInTimeLine(){return mainDriver.findElement(By.xpath("//div[@class=\"loader\"]"));}
    public WebElement semiRequiredFieldModal() {return mainDriver.findElement(By.cssSelector("#semirequired-confirmation-modal"));}
    public WebElement formTitle(){ return mainDriver.findElement(By.cssSelector(".form-title"));}
    public WebElement loadedForm(){ return mainDriver.findElement(By.xpath("//body[@id=\'mobileView\'][contains(@class, 'websocket-connected')]"));}
    public WebElement caseSearchInput() {return driver.findElement(By.xpath("//input[@id=\"case-search\"]"));}

    public void inputDateFormatValidation(WebElement input, String data){
        Assert.assertNotEquals("Validation is stile visible - input: " + input, REQUIRED_FIELD_CLASS, input.getAttribute("class"));
        input.sendKeys(data);
        waitForLoaderExperimental();
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.nextButtonInactive()));
        Assert.assertEquals("This format is not correct, need validate this format", REQUIRED_FIELD_CLASS, input.getAttribute("class"));
        input.clear();
        input.sendKeys("WVWZZZ3CZDE529235");
        waitForLoaderExperimental();
        formTitle().click();
        input.clear();
    }

    public void datePickerDateFormatValidation(WebElement datePicker, String date){
        Assert.assertNotEquals("Validation is stile visible - datePicker: " + datePicker, REQUIRED_DATEPICKER_CLASS, datePicker.getAttribute("class"));
        datePicker.sendKeys(date);
//        wait.until(ExpectedConditions.elementToBeClickable(operationDashboardPage.nextButton()));
        waitForLoaderExperimental();
        operationDashboardPage.nextButton().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.nextButtonInactive()));
        Assert.assertEquals("This format is not correct, need validate this format", REQUIRED_DATEPICKER_CLASS, datePicker.getAttribute("class"));
        datePicker.clear();
        datePicker.sendKeys("01-01-2017");
        waitForLoaderExperimental();
        formTitle().click();
        datePicker.clear();
    }

    public void dateCurrentMileageInputIncorectFormatValidation(WebElement input, String data){
        input.clear();
        waitForLoaderExperimental();
        Assert.assertNotEquals("Validation field is still visible - CurrentMileage Input", REQUIRED_FIELD_CLASS, input.getAttribute("class"));
        input.sendKeys(data);
        formTitle().click();
        wait.until(ExpectedConditions.visibilityOf(operationDashboardPage.nextButtonInactive()));
        operationDashboardPage.nextButtonInactive().click();
//        operationDashboardPage.nextButton().click();
        Assert.assertEquals("This format is not correct, need validate this format", REQUIRED_FIELD_CLASS, input.getAttribute("class"));
    }

    public void waitForLoader(){
        wait.until(ExpectedConditions.visibilityOf(loader()));
        wait.until(ExpectedConditions.invisibilityOf(loader()));
    }

    public void waitForLoaderExperimental(){
        wait = new WebDriverWait(mainDriver, 20, 200);
        try {
            if (loader().isEnabled()){
                wait.until(ExpectedConditions.visibilityOf(loader()));
                wait.until(ExpectedConditions.invisibilityOf(loader()));
            } else {
                wait.until(ExpectedConditions.invisibilityOf(loader()));
            }
        }catch (TimeoutException e){}
        catch (NoSuchElementException e){}
    }

    public int goToCaseInBasicVehicleData() throws SQLException, ClassNotFoundException {
        String phoneNumber = generateData.phoneNumber();
        String firstName = generateData.firstName();
        String lastName = generateData.lastName();
//        db.processInstanceSetInactive();
        String caseId = db.createNewCaseBasicVehicleData(firstName, lastName, phoneNumber);
        new LoginPage().login(Users.LOGIN_JANUSZ, Users.PASS_JANUSZ);
        operationDashboardPage.goToPage();
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        operationDashboardPage.gotoForm(caseId);
        return Integer.parseInt(caseId);
    }

    public int goToCaseInRepairByPhone() throws SQLException, ClassNotFoundException, ParseException {
        String phoneNumber = generateData.phoneNumber();
        String firstName = generateData.firstName();
        String lastName = generateData.lastName();
        String caseId = db.createNewCaseRepairByPhone(firstName, lastName, phoneNumber);
        new LoginPage().login(Users.LOGIN_JANUSZ, Users.PASS_JANUSZ);
        operationDashboardPage.goToPage();
        operationDashboardPage.driver.switchTo().frame(operationDashboardPage.atlasIfram());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        operationDashboardPage.gotoForm(caseId);
        return Integer.parseInt(caseId);
    }

    public void scrollToElement(WebElement element){
        JavascriptExecutor jse = (JavascriptExecutor) mainDriver;
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void clickToElement(WebElement element){
        JavascriptExecutor jse = (JavascriptExecutor) mainDriver;
        jse.executeScript("arguments[0].click();", element);
    }
}
