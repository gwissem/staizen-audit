package Helpers;

import Config.DbSet;
import Data.Cars.Car;
import org.apache.commons.lang3.ObjectUtils;

import java.sql.*;
import java.sql.Date;
import java.text.ParseException;


public class DbManagement {

    public static String dbName;

    public DbManagement(String dbName){
        this.dbName = dbName;
    }

    //platform number
    int VW = 11;
    int SKODA = 6;
    int AUDI = 31;

//    public static Connection connect() throws ClassNotFoundException, SQLException {
//        String dbURL = "jdbc:sqlserver://10.10.77.93:1433;database=" + dbName;
//        String userName= "SigmeoAtlas";
//        String password= "gfd3W4%*$";
//        //Load MS SQL JDBC Driver
//        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//        //Creating connection to the database
//        Connection con = DriverManager.getConnection(dbURL,userName,password);
//        return con;
//    }

    public static Connection connect() throws ClassNotFoundException, SQLException {
        String dbURL = "jdbc:sqlserver://" + DbSet.HOST + ":1433;database=" + dbName;
        String userName= "SigmeoAtlas";
        String password= "gfd3W4%*$";
        //Load MS SQL JDBC Driver
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        //Creating connection to the database
        Connection con = DriverManager.getConnection(dbURL,userName,password);
        return con;
    }

    public void disconnect(Connection con) throws SQLException {
        con.close();
    }

    public void processInstanceSetInactive() throws SQLException, ClassNotFoundException {
        Connection con = connect();
        //Creating statement object
        Statement st = con.createStatement();
        String updateQuery = "UPDATE process_instance set active = 0";
        //Executing the SQL Query and store the results in ResultSet
        st.executeUpdate(updateQuery);
        disconnect(con);
    }

    public void processInstanceSetInactiveCase(String caseId) throws SQLException, ClassNotFoundException {
        Connection con = connect();
        //Creating statement object
        Statement st = con.createStatement();
        String updateQuery = "UPDATE process_instance set active = 0 where root_id IN (" + caseId + ")";
        //Executing the SQL Query and store the results in ResultSet
        st.executeUpdate(updateQuery);
        disconnect(con);
    }

    public int createProcess(String stepID) throws SQLException, ClassNotFoundException {
        Connection con = connect();
        String procedureName = "{call p_process_new(?,?,?,?,?,?,?,?,?,?,?,?) }";
            CallableStatement st = con.prepareCall(procedureName);
            st.setString("stepId", stepID);
            st.setNull( "userId", Types.INTEGER);
            st.setNull( "originalUserId", Types.INTEGER);
            st.setNull("previousProcessId", Types.INTEGER);
            st.setInt("uniqueDefInProcess", 0);
            st.setInt("skipTransaction", 0);
            st.setNull("parentProcessId", Types.INTEGER);
            st.setNull("rootId", Types.INTEGER);
            st.setNull("groupProcessId", Types.INTEGER);
            st.registerOutParameter("err", Types.INTEGER);
            st.registerOutParameter("processInstanceId", Types.INTEGER);
            st.registerOutParameter("message", Types.VARCHAR);
            st.execute();
            int caseID = st.getInt("processInstanceId");
        disconnect(con);
        return caseID;
    }

    public void setAttributeProcessInt(String attributePath, int caseId, int value) throws SQLException, ClassNotFoundException {
        Connection con = connect();
        CallableStatement st = _setAttributeProcess(con, attributePath, caseId);
        st.setInt("valueInt", value);
        st.execute();
        disconnect(con);
    }

    public CallableStatement _setAttributeProcess(Connection con, String attributePath, int caseId) throws SQLException {
        String procedureName = "{call p_attribute_set2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        CallableStatement st = con.prepareCall(procedureName);
        st.setString("attributePath", attributePath);
        st.setInt("groupProcessInstanceId", caseId);
        st.setString("stepId", "xxx");
        st.setNull("attributeSourceId", Types.INTEGER);
        st.setNull("parentAttributeValueId", Types.INTEGER);
        st.setInt("skipTransaction", 0);
        st.setNull("userId", Types.INTEGER);
        st.setNull("originalUserId", Types.INTEGER);
        st.setNull("attributeValueId", Types.INTEGER);
        st.setNull("valueInt", Types.INTEGER);
        st.setNull("valueDate", Types.DATE);
        st.setNull("valueString", Types.LONGVARCHAR);
        st.setNull("valueText", Types.LONGVARCHAR);
        st.setNull("valueDecimal", Types.DECIMAL);
        st.setInt("skipHistoryCheck", 0);
        st.setInt("isParent", 1);
        st.registerOutParameter("attributeValueId", Types.INTEGER);
        st.registerOutParameter("err", Types.INTEGER);
        st.registerOutParameter("message", Types.VARCHAR);
        return st;
    }

    public void setStringValue(CallableStatement st, String value) throws SQLException{
        st.setString("valueString", value);
    }

    public void setHumanAttribiut(int caseId, String phoneNumber, String firstName, String lastName) throws SQLException, ClassNotFoundException {
        String telephonAttribiutPath = "81,342,408,197";
        String firstNameAttribiuthPath = "81,342,64";
        String lastNameAttribiuthPath = "81,342,66";
        String idNumberAttribiuthPath = "81,342,327";
        String genderAttribiuthPath = "81,342,355";
        Connection con = connect();
        CallableStatement st = _setAttributeProcess(con, "81", caseId);
        st.execute();
        int callerId = st.getInt("attributeValueId");
        st = _setAttributeProcess(con, "81,342", caseId);
        st.setInt("parentAttributeValueId", callerId);
        st.execute();
        int humanId = st.getInt("attributeValueId");
        CallableStatement st2 = _setAttributeProcess(con, firstNameAttribiuthPath, caseId);
        st2.setInt("parentAttributeValueId", humanId);
        setStringValue(st2, firstName);
        st2.execute();
        CallableStatement st3 = _setAttributeProcess(con, lastNameAttribiuthPath, caseId);
        st3.setInt("parentAttributeValueId", humanId);
        setStringValue(st3, lastName);
        st3.execute();
        CallableStatement st4 = _setAttributeProcess(con, "81,342,408", caseId);
        st4.setInt("parentAttributeValueId", humanId);
        st4.execute();
        int phonesId = st4.getInt("attributeValueId");
        st4 = _setAttributeProcess(con, "81,342,327", caseId);
        st4.setInt("parentAttributeValueId", humanId);
        st4.execute();
        st4 = _setAttributeProcess(con, "81,342,355", caseId);
        st4.setInt("parentAttributeValueId", humanId);
        st4.execute();
        st4 = _setAttributeProcess(con, "81,342,368", caseId);
        st4.setInt("parentAttributeValueId", humanId);
        st4.execute();
        st4 = _setAttributeProcess(con, "81,342,402", caseId);
        st4.setInt("parentAttributeValueId", humanId);
        st4.execute();

        CallableStatement st5 = _setAttributeProcess(con, "81,342,408,197", caseId);
        st5.setInt("parentAttributeValueId", phonesId);
        setStringValue(st5, phoneNumber);
        st5.execute();
        st5 = _setAttributeProcess(con, "81,342,408,405", caseId);
        st5.setInt("parentAttributeValueId", phonesId);
        st5.execute();
        st5 = _setAttributeProcess(con, "81,342,408,63", caseId);
        st5.setInt("parentAttributeValueId", phonesId);
        st5.execute();
        String caseIdString = Integer.toString(caseId);
        String query = "UPDATE attribute_value set root_process_instance_id = " + caseIdString + " where group_process_instance_id = " + caseIdString;
        Statement statment = con.createStatement();
        statment.executeUpdate(query);
        disconnect(con);
    }

    public void _procedurePFormControls(int caseId, Connection con) throws SQLException, ClassNotFoundException {
        String procedureName = "{call p_form_controls(?, ?, ?) }";
        CallableStatement st = con.prepareCall(procedureName);
        st.setInt("instance_id", caseId);
        st.setInt("returnResults", 0);
        st.setNull("parentAttributeValueId", Types.INTEGER);
        st.execute();
    }

    public CallableStatement _procedureAttributeEdit(Connection con, String attributePath, int caseId) throws SQLException, ClassNotFoundException {
        String procedureName = "{call p_attribute_edit(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        CallableStatement st = con.prepareCall(procedureName);
        st.setString("attributePath", attributePath);
        st.setInt("groupProcessInstanceId", caseId);
        st.setString("stepId", "xxx");
        st.setNull("attributeSourceId", Types.INTEGER);
        st.setNull("parentAttributeValueId", Types.INTEGER);
        st.setInt("skipTransaction", 0);
        st.setNull("userId", Types.INTEGER);
        st.setNull("originalUserId", Types.INTEGER);
        st.setNull("attributeValueId", Types.INTEGER);
        st.setNull("valueInt", Types.INTEGER);
        st.setNull("valueDate", Types.DATE);
        st.setNull("valueString", Types.LONGVARCHAR);
        st.setNull("valueText", Types.LONGVARCHAR);
        st.setNull("valueDecimal", Types.DECIMAL);
        st.setInt("skipHistoryCheck", 0);
        st.setInt("isParent", 1);
        st.registerOutParameter("attributeValueId", Types.INTEGER);
        st.registerOutParameter("err", Types.INTEGER);
        st.registerOutParameter("message", Types.VARCHAR);
        return st;
    }

    public int _procedureNextStep(Connection con, int previousProcessId, int variant) throws SQLException, ClassNotFoundException {
        String procedureName = "{call p_process_next(?,?,?,?,?,?,?,?,?,?,?)}";
        CallableStatement st = con.prepareCall(procedureName);
        st.setInt("previousProcessId", previousProcessId);
        st.setNull("userId", Types.INTEGER);
        st.setNull("originalUserId", Types.INTEGER);
        st.setInt("skipTransaction", 0);
        st.setInt("skipStepProcedure", 0);
        st.setInt("skipErrors", 0);
        st.setInt("variant", variant);
        st.registerOutParameter("err", Types.INTEGER);
        st.registerOutParameter("message", Types.VARCHAR);
        st.registerOutParameter("processInstanceIds", Types.LONGVARCHAR);
        st.registerOutParameter("token", Types.VARCHAR);
        st.execute();
        //W przyszłosci obsłużyć wielo wątkowość - wiele id po przecinku

        String processInstanceIds = st.getString("processInstanceIds");
        System.out.println("System processInstanceIds : " + processInstanceIds);
        String[] processInstanceIdsArray = processInstanceIds.split(",");
        int size = processInstanceIdsArray.length;
        System.out.println( "Wielkość tablicy z id procesów (size) : " + size );
        int correctID = 0;
        for (int i=size-1; i>=0; i=i-1) {
            System.out.println("id : " + Integer.parseInt(processInstanceIdsArray[i]));
            int id = Integer.parseInt(processInstanceIdsArray[i]);
            correctID = _procedurePTask(con, id);
            System.out.println("--------------------------");
            System.out.println("CorrectID : " + correctID);
            System.out.println("--------------------------");
            if (correctID > id) {
                break;
            }
        }
        return correctID;
    }

    public int createNewCaseFormulaSafety(String firstName, String lastName, String phoneNumber) throws SQLException, ClassNotFoundException {
        Connection con = connect();
        int previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, VW);
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        int newCaseIdT = _procedureNextStep(con, previousProcessId, 1);
        //on step enter driver data
        //set is driver
        int newCaseId = getGroupProcessIdByInstanceId(con, newCaseIdT);
        _procedurePFormControls(newCaseIdT, con);
        CallableStatement st = _procedureAttributeEdit(con, "424", newCaseId);
        st.setInt("valueInt", 1);
        st.execute();
         _procedureNextStep(con, newCaseIdT, 1);
        disconnect(con);
       return newCaseId;
    }

    public int getGroupProcessIdByInstanceId(Connection con, int caseId) throws SQLException {
        //get group process id
        Statement cs = con.createStatement();
        String query = "SELECT group_process_id FROM process_instance WHERE id=" + caseId;
        ResultSet parentID = cs.executeQuery(query);
        int newCaseId = 0;
        while (parentID.next()) {
            newCaseId =  parentID.getInt("group_process_id");
        }
        return newCaseId;
    }

    public int createNewCaseEnterDriverData(String firstName, String lastName, String phoneNumber) throws SQLException, ClassNotFoundException {
        Connection con = connect();
        int previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, VW);
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        int newCaseId = _procedureNextStep(con, previousProcessId, 1);
        newCaseId = getGroupProcessIdByInstanceId(con, newCaseId);
        disconnect(con);
        return newCaseId;
    }

    public String createNewCaseBasicVehicleData(String firstName, String lastName, String phoneNumber) throws SQLException, ClassNotFoundException{
        Connection con = connect();
        int previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, VW);
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        int newCaseIdT = _procedureNextStep(con, previousProcessId, 1);
        //on step enter driver data
        //set is driver
        int newCaseId = getGroupProcessIdByInstanceId(con, newCaseIdT);
        _procedurePFormControls(newCaseIdT, con);
        CallableStatement st = _procedureAttributeEdit(con, "424", newCaseId);
        st.setInt("valueInt", 1);
        st.execute();
        previousProcessId = _procedureNextStep(con, newCaseIdT, 1);
//        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
         _procedureNextStep(con, previousProcessId, 1);
        disconnect(con);
        String newCaseIdString = Integer.toString(newCaseId);
        return newCaseIdString;
    }

    public String getFirstName(String firstNameNumber) throws SQLException, ClassNotFoundException {
        Connection con = connect();
        //Creating statement object
        Statement st = con.createStatement();
        String selectQuery = "SELECT firstname FROM firstname_gender WHERE id = "+ firstNameNumber +";";
        //Executing the SQL Query and store the results in ResultSet
        ResultSet firstName = st.executeQuery(selectQuery);
        String nameString = null;
        while (firstName.next()) {
            return nameString = firstName.getString("firstname");
        }
        disconnect(con);
        return nameString;
    }

    public int createNewProces(String firstName, String lastName, String phoneNumber, Connection con, int platformNumber) throws SQLException, ClassNotFoundException {
        String telephoneAttributePath = "81,342,408,197";
        String firstNameAttributePath = "81,342,64";
        String lastNameAttributePath = "81,342,66";
        int caseId = createProcess("1012.001");
        int previousProcessId = _procedureNextStep(con, caseId, 1);
        CallableStatement st = _procedureAttributeEdit(con, firstNameAttributePath, caseId);
        setStringValue(st, firstName);
        st.execute();
        st = _procedureAttributeEdit(con, lastNameAttributePath, caseId);
        setStringValue(st, lastName);
        st.execute();
        st = _procedureAttributeEdit(con, telephoneAttributePath, caseId);
        setStringValue(st, phoneNumber);
        st.execute();
        st = _procedureAttributeEdit(con, "253", caseId);
        st.setInt("valueInt", platformNumber);
        st.execute();
        return previousProcessId;
    }

    public void updatJanuszNietypowy() throws SQLException, ClassNotFoundException {
        String updatQuery = "UPDATE fos_user SET firstname = NULL, lastname = NULL where id = 1426";
        Connection con = connect();
        Statement st = con.createStatement();
        st.executeUpdate(updatQuery);
        disconnect(con);
    }

    public void deleteCase(int caseId) throws SQLException, ClassNotFoundException {
        String procedureName = "{call delete_case(?)}";
        Connection con = connect();
        CallableStatement st = con.prepareCall(procedureName);
        st.setInt("instanceId", caseId);
        st.execute();
        disconnect(con);
    }

    public String createNewCaseRepairByPhone(String firstName, String lastName, String phoneNumber) throws SQLException, ClassNotFoundException, ParseException {
        Connection con = connect();
        int previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, VW);
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        int newCaseIdT = _procedureNextStep(con, previousProcessId, 1);
        //on step enter driver data
        //set is driver
        int newCaseId = getGroupProcessIdByInstanceId(con, newCaseIdT);
        int rootId = newCaseId;
        _procedurePFormControls(newCaseIdT, con);
        CallableStatement st = _procedureAttributeEdit(con, "424", newCaseId);
        st.setInt("valueInt", 1);
        st.execute();
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, newCaseIdT, 1);
//        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
//        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        newCaseId = _procedureNextStep(con, previousProcessId, 1);
        //Set Car
        Car car = new Car();
        setCarFull(con, previousProcessId, newCaseId, car);
//        go to next step
        previousProcessId = _procedureNextStep(con, newCaseId, 1);
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        Statement cs = con.createStatement();
        ResultSet stepIdResultList = cs.executeQuery("SELECT step_id FROM process_instance WHERE id = '" + previousProcessId + "'");
        String stepId = "";
        while (stepIdResultList.next()) {
             stepId = stepIdResultList.getString("step_id");
        }
        stepIdResultList.close();
        if (!stepId.equals("1011.018") ) {
            System.out.println("Nie jesteśmy na kroku naprawy przez telefon");
        previousProcessId = _procedureNextStep(con, previousProcessId, 2);
        }
        disconnect(con);
        String newCaseIdString = Integer.toString(rootId);
        return newCaseIdString;
    }

    private void setCarFull(Connection con, int previousProcessId, int newCaseId, Car car) throws SQLException, ClassNotFoundException, ParseException {
        CallableStatement st;
        //create form structure
        _procedurePFormControls(newCaseId, con);
        Statement cs = con.createStatement();
        ResultSet groupIds = cs.executeQuery("SELECT root_id FROM process_instance WHERE id = '" + previousProcessId + "'");

        int groupProcessId = 23;
        while (groupIds.next()) {
            groupProcessId = groupIds.getInt("root_id");
        }
        groupIds.close();
        String query = "SELECT id FROM attribute_value WITH (NOLOCK) WHERE attribute_path = '74' and group_process_instance_id = " + groupProcessId ;
        ResultSet parentID = cs.executeQuery(query);

        int parentAttributeValueId = 0;
        while (parentID.next()) {
            parentAttributeValueId =  parentID.getInt("id");
        }
//        Car car = new Car();
        //set registration number
        st = _procedureAttributeEdit(con, "74,72", groupProcessId);
        st.setString("valueString", car.getRegistrationNumber());
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.execute();
        //set vin
        st = _procedureAttributeEdit(con, "74,71", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        setStringValue(st, car.getVin());
        st.execute();
        //set first registration date
        //pars registration date from string to dateFormat
        Date registrationDate = Date.valueOf(car.getRegistrationDateSql());
        st = _procedureAttributeEdit(con, "74,233", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.setDate("valueDate",  registrationDate);
        st.execute();
        //set current mileage
        int maxMileage = Integer.parseInt(car.getMaxMileage());
        int mileage = maxMileage - 10;
        st = _procedureAttributeEdit(con, "74,75", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.setInt("valueInt", mileage);
        st.execute();
        //check correct vin
        st = _procedureAttributeEdit(con, "422", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.setInt("valueInt", 1);
        st.execute();
    }

    public String createNewCaseConfirmMileage(String firstName, String lastName, String phoneNumber) throws SQLException, ClassNotFoundException, ParseException {
        Connection con = connect();
        int previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, VW);
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        int newCaseIdT = _procedureNextStep(con, previousProcessId, 1);
        //on step enter driver data
        //set is driver
        int newCaseId = getGroupProcessIdByInstanceId(con, newCaseIdT);
        int rootId = newCaseId;
        _procedurePFormControls(newCaseIdT, con);
        CallableStatement st = _procedureAttributeEdit(con, "424", newCaseId);
        st.setInt("valueInt", 1);
        st.execute();
        //Next_procedure execute
//        previousProcessId = _procedureNextStep(con, newCaseIdT, 1);
        newCaseId = _procedureNextStep(con, newCaseIdT, 1);
////        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        newCaseId = _procedureNextStep(con, newCaseId, 1);
        //set car
        Car car = new Car();
        setCarFull(con, previousProcessId, newCaseId, car);
        //go to next step
        newCaseId = _procedureNextStep(con, newCaseId, 3);
        disconnect(con);
        String newCaseIdString = Integer.toString(rootId);
        return newCaseIdString;
    }

    public int getMaxDeclaredMileage(String vin) throws SQLException, ClassNotFoundException {
        String selectQuery = "SELECT MAX(value_int) FROM attribute_value where parent_attribute_value_id IN ( SELECT parent_attribute_value_id FROM attribute_value WHERE attribute_path = '74,71' and value_string ='" + vin + "') and attribute_path = '74,75'";
        Connection con = connect();
        Statement st = con.createStatement();
        ResultSet maxResultList = st.executeQuery(selectQuery);
        int declaredMileage = 15;
        while (maxResultList.next()){
            declaredMileage = maxResultList.getInt(1);
        }
        disconnect(con);
        return declaredMileage;
    }

    public String createNewCaseBwb(String firstName, String lastName, String phoneNumber, String[] car, String platformName) throws SQLException, ClassNotFoundException{
        Connection con = connect();
        int previousProcessId = 0;
        switch (platformName){
            case "vwOsobowe":
                previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 11);
                break;
            case "vwUżytkowe":
                previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 11);
                break;
            case "skoda":
                previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 6);
                break;
            case "audi":
                previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, 31);
                break;
        }
//        int previousProcessId = createNewProces(firstName, lastName, phoneNumber, con, platform);
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, previousProcessId, 1);
        int newCaseIdT = _procedureNextStep(con, previousProcessId, 1);
        //on step enter driver data
        //set is driver
        int newCaseId = getGroupProcessIdByInstanceId(con, newCaseIdT);
        int rootId = newCaseId;
        _procedurePFormControls(newCaseIdT, con);
        CallableStatement st = _procedureAttributeEdit(con, "424", newCaseId);
        st.setInt("valueInt", 1);
        st.execute();
        //Next_procedure execute
        previousProcessId = _procedureNextStep(con, newCaseIdT, 1);

        newCaseId = _procedureNextStep(con, previousProcessId, 1);
        //set car
        //create form structure
        _procedurePFormControls(newCaseId, con);
        Statement cs = con.createStatement();
        ResultSet groupIds = cs.executeQuery("SELECT root_id FROM process_instance WHERE id = '" + previousProcessId + "'");
        int groupProcessId = 23;
        while (groupIds.next()) {
            groupProcessId = groupIds.getInt("root_id");
        }
        groupIds.close();
        String query = "SELECT id FROM attribute_value WITH (NOLOCK) WHERE attribute_path = '74' and group_process_instance_id = " + groupProcessId ;
        ResultSet parentID = cs.executeQuery(query);
        int parentAttributeValueId = 0;
        while (parentID.next()) {
            parentAttributeValueId =  parentID.getInt("id");
        }
        //set registration number
        st = _procedureAttributeEdit(con, "74,72", groupProcessId);
        st.setString("valueString", car[0]);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.execute();
        //set vin
        st = _procedureAttributeEdit(con, "74,71", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        setStringValue(st, car[1]);
        st.execute();
        //pars registration date from string to dateFormat
        Date registrationDate = Date.valueOf(car[4]);
        st = _procedureAttributeEdit(con, "74,233", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.setDate("valueDate",  registrationDate);
        st.execute();
        //set current mileage
        st = _procedureAttributeEdit(con, "74,75", groupProcessId);
        st.setInt("parentAttributeValueId", parentAttributeValueId);
        st.setInt("valueInt", Integer.parseInt(car[3]));
        st.execute();
        previousProcessId = _procedureNextStep(con, newCaseId, 2);
        //Permission not found in base
        //Set Permission
        _procedurePFormControls(groupProcessId, con);
        st = _procedureAttributeEdit(con, "467", groupProcessId);
        st.setInt("valueInt", 1);
        st.execute();
        st = _procedureAttributeEdit(con, "468", groupProcessId);
        st.setInt("valueInt", 1);
        st.execute();
        //Go to BWB process
         _procedureNextStep(con, previousProcessId, 1);
        disconnect(con);
        String newCaseIdString = Integer.toString(rootId);
        return newCaseIdString;
    }

    private int _procedurePTask(Connection con, int id) throws SQLException {
        String procedureName = "{call p_tasks(?,?,?,?,?,?,?,?,?,?)}";
        CallableStatement st = con.prepareCall(procedureName);
        st.setInt("userId", 2597);
        st.setInt("orgUserId", 2597);
        st.setNull( "filter", Types.NVARCHAR);
        st.setInt("type", 1);
        st.setString("locale", "pl");
        st.setInt("instanceId", id);
        st.setInt("timeLine", 0);
        st.setInt("limit", 1);
        st.setInt("showInactive", 1);
        st.setInt("disablePostponeDate", 0);
        st.execute();
        ResultSet parentID = st.getResultSet();
        int newCaseId = 0;
        while (parentID.next()) {
            newCaseId =  parentID.getInt("id");
        }
        return newCaseId;
    }
}

