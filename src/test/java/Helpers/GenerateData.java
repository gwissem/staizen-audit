package Helpers;

import Tests.AbstractTest;
import org.apache.commons.lang3.RandomStringUtils;

import java.sql.SQLException;
import java.util.Random;

public class GenerateData extends AbstractTest {

    public static String phoneNumber(){
        Random randomGenerator = new Random();
        String phone = "";
        for (int i = 0;  i<9 ; i++) {
            int number = randomGenerator.nextInt(10);
//            phone = phone + Integer.toString(number);
                    phone = phone.concat(Integer.toString(number));
        }
       return phone;
    }

    public String firstName() {
        Random random = new Random();
        int firstNameInt = random.nextInt((486 - 1) + 1);
        String firstNameNumber = Integer.toString(firstNameInt);
        String firstName = "";
        try {
            firstName = new DbManagement(DB_NAME).getFirstName(firstNameNumber);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return firstName;
    }

    public String lastName() {
        return RandomStringUtils.randomAlphanumeric(10);
    }
}
