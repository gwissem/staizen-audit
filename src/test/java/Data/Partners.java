package Data;

import Helpers.DbManagement;
import Tests.AbstractTest;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class Partners extends AbstractTest {

    static DbManagement db = new DbManagement(DB_NAME);

    public void editPartnerPhoneNumber(String phoneNumber, String partnerPhoneID) throws SQLException, ClassNotFoundException {
        Connection con = db.connect();
        Statement st = con.createStatement();
        String updatePhone = "dbo.attribute_value SET value_string='" + phoneNumber + "' WHERE id='" + partnerPhoneID + "'";
        st.executeUpdate(updatePhone);
        db.disconnect(con);
    }

    public void editPartnerPriority(String priority, String partnerPriorityID) throws SQLException, ClassNotFoundException {
        Connection con = db.connect();
        Statement st = con.createStatement();
        String updatePhone = "dbo.attribute_value SET value_int='" + priority + "' WHERE id='" + partnerPriorityID + "'";
        st.executeUpdate(updatePhone);
        db.disconnect(con);
    }
}
