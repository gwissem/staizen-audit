package Data.Cars;

import Data.Cars.Car;

import java.util.Random;

public class VwUzytkowe extends Car {
    public int index = new Random().nextInt(1);

    public String getRegistrationNumber() {
        return cars[index][0];
    }

    public String getVin() {
        return cars[index][1];
    }

    public String getRegistrationDate() {
        return cars[index][2];
    }
    public String getMaxMileage() {return cars[index][3];}
    public String getRegistrationDateSql() {return cars[index][4];}

    String[][] cars ={  {"CT9435M","WV2ZZZ2KZGX119662", "13-04-2016", "80000", "2016-04-13"},
            {"OPO04157","WV2ZZZ7HZGH115748", "27-04-2016", "80300", "2016-04-27"} };
}
