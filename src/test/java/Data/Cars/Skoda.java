package Data.Cars;

import Data.Cars.Car;

public class Skoda extends Car {

    public String getRegistrationNumber() {
        return cars[0];
    }
    public String getVin() { return cars[1]; }
    public String getRegistrationDate() {
        return cars[2];
    }
    public String getMaxMileage() {return cars[3];}
    public String getRegistrationDateSql() {return cars[4];}

    String[] cars ={ "WZ7635U", "TMBAC7NE8J0150920", "10-03-2017",  "16000", "2017-03-10"};
}
