package Data.Cars;

import Data.Cars.Car;

import java.util.Random;

public class Audi extends Car {

    public int index = new Random().nextInt(2);

    public String getRegistrationNumber() { return cars[index][0];
    }
    public String getVin() { return cars[index][1]; }
    public String getRegistrationDate() { return cars[index][2];
    }
    public String getMaxMileage() {return cars[index][3];}

    public String getRegistrationDateSql() {return cars[index][4];}

    String[][] cars ={ {"PO9N199", "WAUZZZ4G3GN114027", "22-12-2015",  "60900", "2015-12-22"},
            {"KOS58474", "WAUZZZ8V4EA075428", "31-10-2013", "114500", "2013-10-31"}};
}
