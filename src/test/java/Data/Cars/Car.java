package Data.Cars;

import java.util.Random;

public class Car {
    public int index = new Random().nextInt(3);

    public String getRegistrationNumber() {
        return cars[index][0];
    }

    public String getVin() {
        return cars[index][1];
    }

    public String getRegistrationDate() {
        return cars[index][2];
    }
    public String getMaxMileage() {return cars[index][3];}

    public String getRegistrationDateSql() {return cars[index][4];}

    String[][] cars ={  {"PO650ux","WVWZZZ3CZDE529235", "21-09-2012", "112607", "2012-09-21"},
                        {"EZG05556","WVWZZZ3CZEE021633", "24-09-2013", "241158", "2013-09-24"},
                        {"SI55778","WVWZZZ1KZBP079033", "25-11-2010", "99053", "2010-11-25"},
                        {"WD9987L","WVWZZZ3CZHE095236", "09-01-2017", "59428", "2017-01-09"}};
}