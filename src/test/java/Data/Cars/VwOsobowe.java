package Data.Cars;

import Data.Cars.Car;

import java.util.Random;

public class VwOsobowe extends Car {
    public int index = new Random().nextInt(3);

    public String getRegistrationNumber() {
        return cars[index][0];
    }

    public String getVin() {
        return cars[index][1];
    }

    public String getRegistrationDate() {
        return cars[index][2];
    }
    public String getMaxMileage() {return cars[index][3];}

    public String getRegistrationDateSql() {return cars[index][4];}

    String[][] cars ={  {"WE876PK","WVWZZZ3CZHE181943", "04-05-2017", "11000", "2017-05-04"},
            {"PL77774","WVWZZZ3CZFE023699", "12-01-2015", "88842", "2015-12-01"},
            {"WN59345","WVGZZZ1TZ6W049634", "30-11-2005", "131000", "2005-11-30"},
            {"WW5988T","WVWZZZ6RZDY045057", "19-09-2012", "50400", "2012-09-19"}};
}

