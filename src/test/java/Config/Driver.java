package Config;

import java.util.Arrays;
import java.util.List;

public class Driver {

    /*
    Local configuration,
    Insert local path to chrome driver.
    ChromeDriver download from: https://chromedriver.storage.googleapis.com/index.html?path=2.33/
     */
//    public static String pathChromeDriver = "E:\\chromeDriver\\chromedriver.exe";
//    public static List<String> browserOptions = Arrays.asList("start-maximized");

    /*
    Server configuration
     */
    public static String pathChromeDriver = "/home/starter/chromeDriver/chromedriver";
    public static List<String> browserOptions = Arrays.asList("--window-size=1920,1080", "--headless");

}