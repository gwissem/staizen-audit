<?php

namespace CaseBundle\Twig\Extension;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\FormTemplate;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Program;
use CaseBundle\Service\FormTemplateGenerator;
use CaseBundle\Service\ProcessHandler;
use CaseBundle\Service\ServiceHandler;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use OperationalBundle\Service\UpdaterOfFormControlService;
use ToolBundle\Utils\ToolQueryManager;

class FormTemplateExtension extends \Twig_Extension
{

    private $jmsSerializer;

    private $em;

    private $formTemplateGenerator;

    private $toolQueryManager;

    private $updaterOfFormControl;

    /** @var ServiceHandler $serviceHandler */
    private $serviceHandler;

    /** @var ProcessHandler */
    private $processHandler;

    private $cachedQueryResults = [];

    /**
     * FormTemplateExtension constructor.
     * @param EntityManager $em
     * @param Serializer $jmsSerializer
     * @param FormTemplateGenerator $formTemplateGenerator
     * @param ToolQueryManager $toolQueryManager
     * @param UpdaterOfFormControlService $updaterOfFormControl
     * @param ServiceHandler $serviceHandler
     * @param ProcessHandler $processHandler
     */
    public function __construct(EntityManager $em, Serializer $jmsSerializer,
                                FormTemplateGenerator $formTemplateGenerator,
                                ToolQueryManager $toolQueryManager,
                                UpdaterOfFormControlService $updaterOfFormControl,
                                ServiceHandler $serviceHandler,
                                ProcessHandler $processHandler)
    {

        $this->em = $em;
        $this->jmsSerializer = $jmsSerializer;
        $this->formTemplateGenerator = $formTemplateGenerator;
        $this->toolQueryManager = $toolQueryManager;
        $this->updaterOfFormControl = $updaterOfFormControl;
        $this->serviceHandler = $serviceHandler;
        $this->processHandler = $processHandler;

    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('removeDoubleApostrophe', [$this, 'removeDoubleApostrophe']),
            new \Twig_SimpleFilter('commaToDash', [$this, 'commaToDash']),
        ];
    }

    public function commaToDash($text)
    {
        return str_replace(',', '-', $text);
    }

    public function removeDoubleApostrophe($text)
    {

        return str_replace("\"", "'", $text);

    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('prepareJsonOfFormTemplate', [$this, 'prepareJsonOfFormTemplate']),
            new \Twig_SimpleFunction('getOptionsOfQuery', [$this, 'getOptionsOfQuery']),
            new \Twig_SimpleFunction('getUniqueId', [$this, 'getUniqueId']),
            new \Twig_SimpleFunction('customStyles', [$this, 'customStyles']),
            new \Twig_SimpleFunction('isChecked', [$this, 'isChecked']),
            new \Twig_SimpleFunction('autoSetValue', [$this, 'autoSetValue']),
            new \Twig_SimpleFunction('getServices', [$this, 'getServices']),
            new \Twig_SimpleFunction('availableLocations', [$this, 'availableLocations']),
            new \Twig_SimpleFunction('generateCostsVerificationReport', [$this, 'generateCostsVerificationReport']),
            new \Twig_SimpleFunction('isActiveProcess', [$this, 'isActiveProcess']),
            new \Twig_SimpleFunction('serviceExtraPrograms', [$this, 'serviceExtraPrograms']),
            new \Twig_SimpleFunction('isSpecialDataCase', [$this, 'isSpecialDataCase'])
        ];
    }

    public function isChecked($controlValue, $compareValue) {

        if($controlValue === null || $controlValue === "") return "";

        return ($controlValue == $compareValue) ? 'checked' : '';

    }

    public function autoSetValue($attrValue, $defaultValue, $compare = null, $controlValue = null, $type = NULL) {
        $autoSet = false;

        if($type == 'select' && empty($attrValue)){
            $autoSet = true;
        }

        $isCompare = ($compare === null) ? true : (($compare == $controlValue) ? true : false);

        if($attrValue == null && $defaultValue != null && $isCompare) {
            $autoSet = true;
        }

        return $autoSet ? "data-auto-set=\"true\"" : "";

    }

    /**
     * @param FormControl | null $control
     * @param $tagName
     * @return string
     */
    public function customStyles($control, $tagName) {
        if(!$control) return '';
        $styles = $control->getCustomStyle();
        if(!is_array($styles)) return '';
        return (isset($styles[$tagName])) ? ('style="' . $styles[$tagName] . '"') : '';
    }

    public function getUniqueId() {
        return uniqid() . '-' . uniqid();
    }

    /**
     * @param $query
     * @param null $groupProcessId
     * @param null $processInstanceId
     * @param null $attributePath
     * @param bool $useCache
     * @return array
     */
    public function getOptionsOfQuery($query, $groupProcessId = null, $processInstanceId = null, $attributePath = null, $useCache = false) {

        if (empty($query)) return [];

        $key = null;

        if($useCache) {
            $key = base64_encode($query);
            if(isset($this->cachedQueryResults[$key])) {
                return $this->cachedQueryResults[$key];
            }
        }

        $result = $this->updaterOfFormControl->getOptions($groupProcessId, $query, $processInstanceId, $attributePath);

        if($useCache && $key) {
            $this->cachedQueryResults[$key] = $result;
        }

        return $result;

    }

    public function prepareJsonOfFormTemplate(FormTemplate $formTemplate)
    {

        $formTemplateJSON = $this->formTemplateGenerator->generateControlsOfFormTemplate($formTemplate);
        return $this->jmsSerializer->serialize($formTemplateJSON, 'json');

    }

    function getName()
    {
        return 'form_template_extension';
    }


    public function getServices($processInstanceId, $groupProcessId, $locale = 'pl'){
        return $this->serviceHandler->availableServices($processInstanceId, $groupProcessId, 1, $locale);

    }

    public function isActiveProcess($processInstanceId){
        if(!$processInstanceId){
            return false;
        }
        $active =  $this->em->getRepository(ProcessInstance::class)->find($processInstanceId)->getActive();
        if($active !== 1){
            return false;
        }
        return true;
    }

    public function generateCostsVerificationReport($groupProcessId, $name, $user){
        if(!$name){
            $name = 'p_rsa_costs_report';
        }

        return $this->serviceHandler->generateCostsVerificationReport($groupProcessId, $name, $user);
    }

    public function availableLocations($instanceId){
        return $this->serviceHandler->availableLocations($instanceId);
    }


    public function isSpecialDataCase($groupProcessId)
    {
        return $this->processHandler->getAttributeValue('1168', $groupProcessId, AttributeValue::VALUE_INT);
    }

}


