<?php

namespace CaseBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProcessEnterHistoryRepository extends EntityRepository
{
    /**
     * @param $rootId
     * @return array
     */
    public function getAllProcessFromRootId($rootId)
    {
        $qb = $this->createQueryBuilder('p')
            ->innerJoin('p.processInstance', 't');

        $qb->setParameter('rootId', $rootId)
            ->where('t.root = :rootId')
            ->andWhere('p.dateLeave IS NOT NULL')
            ->orderBy('p.dateEnter', 'DESC')
            ->setMaxResults(50);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $user
     * @param $processInstance
     * @return array
     * @throws \Exception
     */
    public function getLastLogOfUserByProcessInstance($user, $processInstance)
    {
        $qb = $this->createQueryBuilder('p');

        $lastHour = (new \DateTime())->modify('-1 hour');

        $qb->setParameter('user', $user)
            ->setParameter('process', $processInstance)
            ->setParameter('lastHour', $lastHour->format('Y-m-d H:i:s'))
            ->where('p.processInstance = :process')
            ->andWhere('p.user = :user')
            ->andWhere('p.dateEnter >= :lastHour')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults(1);

        $result = $qb->getQuery()->getResult();

        if(!empty($result)) {
            return $result[0];
        }

        return null;

    }

}
