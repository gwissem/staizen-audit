<?php

namespace CaseBundle\Repository;

use CaseBundle\Entity\ProcessInstance;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * ProcessInstanceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProcessInstanceRepository extends \Doctrine\ORM\EntityRepository
{

    CONST PROCESS_PARTNER_ID = '1091.011';

    public function findActiveProcessInstancesForRoot($rootProcessId)
    {
        $query = $this->createQueryBuilder('p')
            ->select('p.id')
            ->where('p.active = true')
            ->andWhere('p.root = :rootProcessId or p.groupProcess = :rootProcessId')
            ->setParameter('rootProcessId', $rootProcessId);
        $result = $query->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return $result;
    }

    public function getProcessInstanceDescription($processInstanceId, $locale = 'pl')
    {
        $query = $this->createQueryBuilder('p')
            ->select('p.id as id, p.active as active, pg.id as groupProcessId, r.id as rootId, s.id as stepId, st.name, st.description, p.description as instanceDescription, pdt.cancelDescription as cancelDescription, p.description, p.dateEnter, p.createdAt, pt.lifeTime, pt.percentage, s.referenceTime')
            ->innerJoin('p.step', 's')
            ->innerJoin('s.processDefinition', 'pd')
            ->innerJoin('pd.translations', 'pdt', 'pdt.translatable_id = pd.id')
            ->leftJoin('p.groupProcess', 'pg')
            ->leftJoin('p.root', 'r')
            ->innerJoin('s.translations', 'st', 'st.translatable_id = s.id')
            ->leftJoin('p.time', 'pt', 'p.id = pt.process_instance_id')
            ->where('p.id = :processInstanceId')
            ->andWhere('st.locale = :locale')
            ->andWhere('pdt.locale = :locale')
            ->setParameter('locale', $locale)
            ->setParameter('processInstanceId', $processInstanceId);

        $result = $query->getQuery()->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $result;
    }

    public function fetchInstancesByAttributeValue($value){
        $values = explode(' ', $value);
        $queryResults = [];
        $finalArray = [];
        foreach($values as $value) {
            $query = $this->createQueryBuilder('p')
                ->select('p.id, pa.attributePath')
                ->innerJoin('p.attributeValues', 'pa')
                ->where('pa.valueString LIKE :value')
                ->setParameter('value', $value . '%');

            $queryResults[$value] = $query->getQuery()->getArrayResult($query);
        }

        foreach($queryResults as $value => $results){
            foreach($results as $result) {
                $attributeIds = explode(',', $result['attributePath']);
                $prefix = $attributeIds[0];
                if(array_key_exists($result['id'], $finalArray)) {
                    foreach ($finalArray[$result['id']] as $path => $pathValue) {
                        if ($prefix && strpos($path, $prefix) === 0) {
                            $finalArray[$result['id']][$result['attributePath']] = $value;
                        }
                        else{
                            unset($finalArray[$result['id']][$result['attributePath']]);
                        }
                    }
                }
                else{
                    $finalArray[$result['id']][$result['attributePath']] = $value;
                }

            }
        }

        foreach($finalArray as $key => $element){
            if(count($element) !== count($values)){
                unset($finalArray[$key]);
            }
        }

        $output = [
            'ids' => array_keys($finalArray),
            'filters' =>  [
                'vin' => $this->findKeyValue($finalArray, '74,71'),
                'city' => $this->findKeyValue($finalArray, '85,87'),
                'registrationNumber' => $this->findKeyValue($finalArray, '74,72'),
                'caseNumber' => $this->findKeyValue($finalArray, '235'),
            ]
        ];

        return $output;
    }

    private function findKeyValue($array, $keySearch)
    {
        foreach ($array as $key => $item) {
            if ($key == $keySearch) {
                return $array[$key];
            }
            else {
                if (is_array($item)) {
                    return $this->findKeyValue($item, $keySearch);
                }
            }
        }
        return NULL;
    }

    public function updateLastRunInstances($rootId, $from, $to)
    {
        $qb = $this->createQueryBuilder('pi');

        $q = $qb->update(ProcessInstance::class, 'pi')
            ->set('pi.lastRun', 0)
            ->where('pi.id >= :from')
            ->andWhere('pi.id <= :to')
            ->andWhere('pi.root = :root')
            ->andWhere('pi.lastRun = 1')
            ->setParameter('root', $rootId)
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->getQuery();

        $q->execute();
    }

    public function findIdOfRootProcess($processInstanceId) {

        $qb = $this->createQueryBuilder('p')
            ->select('pr.id')
            ->leftJoin('p.root', 'pr')
            ->where('p.id = :id')
            ->setParameter('id', $processInstanceId);

        return $qb->getQuery()->getResult(Query::HYDRATE_SINGLE_SCALAR);

    }

    public function findIdOfActiveProcessesInGroup($groupProcessId) {

        $qb = $this->createQueryBuilder('p')
            ->select('p.id')
            ->where('p.active = 1')
            ->andWhere('p.groupProcess = :groupProcessId')
            ->setParameter('groupProcessId', $groupProcessId);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

    }

    public function findActiveProcessByToken($token) {

        $qb = $this->createQueryBuilder('p')
            ->select('p.id')
            ->where('p.active = 1')
            ->andWhere('p.token = :token')
            ->setParameter('token', $token)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();

    }

    public function findProcessByToken($token, $active = 1, $technical = null) {

        if($active == 1) {
            $statuses = [1];
        }
        else {
            $statuses = [0, 999];
        }

        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->leftJoin('p.step', 'ps')
            ->where('p.active IN (:active)')
            ->andWhere('p.token = :token')
            ->setParameter('token', $token)
            ->setParameter('active',$statuses)
            ->setMaxResults(1)
            ->orderBy('p.id', 'DESC');

        if($technical !== null){
            $qb->andWhere('ps.isTechnical = :technical')
                ->setParameter('technical', $technical);
        }

        return $qb->getQuery()->getOneOrNullResult();

    }

    public function findProcessByTokenAndGroupProcessId($token, $groupProcessId) {

        $qb = $this->createQueryBuilder('p')
            ->select('p.id')
            ->where('p.active = 1')
            ->andWhere('p.groupProcess = :groupProcessId')
            ->andWhere('p.token = :token')
            ->setParameter('token', $token)
            ->setParameter('groupProcessId', $groupProcessId);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

    }

    /**
     * @param $currentProcessId
     * @param $groupProcessId
     * @param $token
     * @return array
     */
    public function findPreviousProcess($currentProcessId, $groupProcessId, $token = null) {

        $qb = $this->createQueryBuilder('p')
            ->select('p.id')
            ->where('p.groupProcess = :groupProcessId')
            ->andWhere('p.id < :currentProcessId')
            ->setParameter('currentProcessId', $currentProcessId)
            ->setParameter('groupProcessId', $groupProcessId);


        if($token) {
            $qb->andWhere('p.token = :token')
                ->setParameter('token', $token);
        }

        $qb->orderBy('p.id', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

    }

    public function findAllProcessOfPartners() {

        $qb = $this->createQueryBuilder('p')
            ->where('p.step = :step_id')
            ->setParameter('step_id', self::PROCESS_PARTNER_ID);

        return $qb->getQuery()->getResult();
    }

    public function getAllProcessByIds($ids) {

        $qb = $this->createQueryBuilder('p')
            ->where('p.step = :step_id')
            ->andWhere('p.groupProcess IN (:ids)')
            ->setParameter('ids', $ids)
            ->setParameter('step_id', self::PROCESS_PARTNER_ID);

        return $qb->getQuery()->getResult();
    }

    public function getChoicedVariant($processInstanceId) {

        $qb = $this->createQueryBuilder('p')
            ->select('p.choiceVariant')
            ->where('p.id = :id')
            ->setParameter('id', $processInstanceId);

        return $qb->getQuery()->getResult(Query::HYDRATE_SINGLE_SCALAR);
    }

    public function getStepOfProcessInstanceId($processInstanceId) {

        $qb = $this->createQueryBuilder('p')
            ->select('IDENTITY(p.step) as stepId')
            ->where('p.id = :id')
            ->setParameter('id', $processInstanceId);

        return $qb->getQuery()->getResult(Query::HYDRATE_SINGLE_SCALAR);
    }

    public function filterProcessWithPermissionOfGroup($processInstanceIds, $groups) {

        $qb = $this->createQueryBuilder('p')
            ->select('p.id as processId, psg.id as groupId')
            ->leftJoin('p.step', 'ps')
            ->leftJoin('ps.permittedGroups', 'psg')
            ->where('psg.id IN (:groups)')
            ->andWhere('p.id IN (:processIds)')
            ->setParameter('groups', $groups)
            ->setParameter('processIds', $processInstanceIds);

        return $qb->getQuery()->getResult();

    }

    public function getDateCreateInstance($groupProcessInstanceId)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('r.createdAt')
            ->innerJoin('p.root','r')
            ->where('p.id = :groupProcessId')
            ->setParameter('groupProcessId', $groupProcessInstanceId);
        $root =  $qb->getQuery()->getOneOrNullResult();

        return $root ? $root['createdAt'] : null;
    }


    public function findFullProcessInstance($processInstanceId) {


        $qb = $this->createQueryBuilder('p')
            ->addSelect('ps', 'psd', 'pr', 'pg')
            ->leftJoin('p.step', 'ps')
            ->leftJoin('ps.processDefinition', 'psd')
            ->leftJoin('p.root', 'pr')
            ->leftJoin('p.groupProcess', 'pg')
            ->where('p.id = :id')
            ->setParameter('id', $processInstanceId);

        $result = $qb->getQuery()->getResult();

        if(count($result)) {
            return $result[0];
        }

        return null;

    }

    public function findLastGroupInstance($groupProcessInstanceId)
    {
        $query = $this->createQueryBuilder('p')
            ->select('p')
            ->innerJoin('p.step','s')
            ->where('s.id NOT LIKE :editStepId')
            ->andWhere('p.groupProcess = :groupProcessInstanceId')
            ->setMaxResults(1)
            ->orderBy('p.id','desc')
            ->setParameter('groupProcessInstanceId', $groupProcessInstanceId)
            ->setParameter('editStepId', '%.500');
        $result = $query->getQuery()->getResult();

        return $result;
    }

    /**
     * @param $rootId
     * @return ProcessInstance|null
     */
    public function findStepWithMatrix($rootId)
    {
        $query = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.step IN (:steps)')
            ->andWhere('p.root = :rootId')
            ->setMaxResults(1)
            ->orderBy('p.id','desc')
            ->setParameter('rootId', $rootId)
            ->setParameter('steps', ['1011.012', '1011.056']);
        $result = $query->getQuery()->getResult();

        if(count($result)) {
            return $result[0];
        }

        return null;

    }
}
