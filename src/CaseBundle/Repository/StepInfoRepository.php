<?php

namespace CaseBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Utils\StepInfoLocator;


class StepInfoRepository extends EntityRepository
{

    public function getByStepInfoLocator(StepInfoLocator $stepInfoLocator, $locale)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('info')
            ->select('info.text');
        $stepInfoLocator->setAlias('info');
        
        $qb = $stepInfoLocator->getWherePart($qb);
        $qb->andWhere('info.locale = :locale')
            ->setParameter('locale', $locale);
        $qb->orderBy('info.program');
        $result = $qb->getQuery()->getResult();
        if(count($result)){
            return $result[0];
        }else{
            return null;
        }
    }

}