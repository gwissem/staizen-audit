<?php

namespace CaseBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProcessFlowRepository extends EntityRepository
{
    public function fetchProcessFlows($processDefinition)
    {

        $qb = $this->createQueryBuilder('pf')
            ->join('pf.step', 's')
            ->where('s.processDefinition = :processDefinition')
            ->setParameter('processDefinition', $processDefinition);

        return $qb->getQuery()->getResult();
    }

    public function fetchFlowsByProgram($processDefinition, $programId = NULL)
    {
        $processDefinitionId = $processDefinition->getId();

        $qb = $this->createQueryBuilder('pf')
//            ->innerJoin('pf.translations', 't')
            ->leftJoin('pf.step','ps')
            ->innerJoin('pf.nextStep','ns')
            ->innerJoin('pf.processDefinition', 'definition')
            ->leftJoin('pf.programs','programs')
            ->where('definition.id = :id')
            ->andWhere('programs.id IS NULL OR programs.id = :programId')
            ->setParameter('id', $processDefinitionId)
            ->setParameter('programId', $programId);

        return $qb->getQuery()->getResult();
    }

}
