<?php

namespace CaseBundle\Repository;

class PlatformPhoneRepository extends \Doctrine\ORM\EntityRepository
{

    public function getAllHelplines()
    {

        $qb = $this->createQueryBuilder('pp')
            ->select('pp, ppp')
            ->leftJoin('pp.platform', 'ppp');

        return $qb->getQuery()->getResult();

    }

}
