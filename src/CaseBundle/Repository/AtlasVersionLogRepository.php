<?php

namespace CaseBundle\Repository;

/**
 * AtlasVersionLogRepository
 *
 */

class AtlasVersionLogRepository extends \Doctrine\ORM\EntityRepository
{

    public function getLastVersion() {

        $qb = $this->createQueryBuilder('avl');

        $qb->where('avl.version IS NOT NULL')
            ->setMaxResults(1)
            ->orderBy('avl.versionDate', 'desc');

        return $qb->getQuery()->getResult();

    }

    public function getLogs()
    {
        $qb = $this->createQueryBuilder('avl')
            ->orderBy('avl.id', 'DESC');

        return $qb->getQuery();

    }

    public function getLogsForTimeline(\DateTime $from, \DateTime $to)
    {

        $qb = $this->createQueryBuilder('avl')
            ->where('avl.version IS NOT NULL')
            ->andWhere('avl.versionDate >= :from')
            ->andWhere('avl.versionDate <= :to')
            ->setParameter('from', $from->format('Y-m-d 00:00:00'))
            ->setParameter('to', $to->format('Y-m-d 23:59:59'))
            ->orderBy('avl.versionDate', 'DESC');

        return $qb->getQuery()->getResult();

    }
}
