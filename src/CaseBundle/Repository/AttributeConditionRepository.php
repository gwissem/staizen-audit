<?php

namespace CaseBundle\Repository;
use CaseBundle\Entity\Step;
use Doctrine\ORM\AbstractQuery;

/**
 * ToolRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AttributeConditionRepository extends \Doctrine\ORM\EntityRepository
{

    public function getCondition($formControlId, $step)
    {

        $stepId = ($step instanceof Step) ? $step->getId() : $step;

        $key = 'attr_condition_' . $formControlId . '_' . $stepId;

        return $this->createQueryBuilder('ac')
            ->where('ac.step = :step')
            ->andWhere('ac.formControlId = :formControlId')
            ->setParameter('formControlId', $formControlId)
            ->setParameter('step', $step)
            ->getQuery()
            ->useQueryCache(true)
            ->setQueryCacheLifetime(600)
            ->setResultCacheLifetime(600)
//            ->setResultCacheId($key)
            ->useResultCache(true, 600, $key)
            ->getResult(AbstractQuery::HYDRATE_SIMPLEOBJECT);
    }

}
