<?php

namespace CaseBundle\Command;

use CaseBundle\Utils\TempFileHandler;
use Composer\Util\Filesystem;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use DocumentBundle\Utils\HandlerExtraFiles;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseItemIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfItemChangeDescriptionsType;
use jamesiarmes\PhpEws\Autodiscover;
use jamesiarmes\PhpEws\Enumeration\ConflictResolutionType;
use jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use jamesiarmes\PhpEws\Enumeration\UnindexedFieldURIType;
use jamesiarmes\PhpEws\Request\CreateAttachmentType;
use jamesiarmes\PhpEws\Request\SendItemType;
use jamesiarmes\PhpEws\Request\UpdateItemType;
use jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use jamesiarmes\PhpEws\Type\FileAttachmentType;
use jamesiarmes\PhpEws\Type\ItemChangeType;
use jamesiarmes\PhpEws\Type\ItemIdType;
use jamesiarmes\PhpEws\Type\ItemType;
use jamesiarmes\PhpEws\Type\PathToUnindexedFieldType;
use jamesiarmes\PhpEws\Type\SetItemFieldType;
use jamesiarmes\PhpEws\Type\TargetFolderIdType;
use MailboxBundle\Service\MailboxOutgoingEmailService;
use SocketBundle\Topic\TaskManagerTopic;
use SplFileObject;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;


use \jamesiarmes\PhpEws\Client;
use \jamesiarmes\PhpEws\Request\CreateItemType;
use \jamesiarmes\PhpEws\ArrayType\ArrayOfRecipientsType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAllItemsType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAttachmentsType;
use \jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use \jamesiarmes\PhpEws\Enumeration\MessageDispositionType;
use \jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use \jamesiarmes\PhpEws\Type\BodyType;
use \jamesiarmes\PhpEws\Type\EmailAddressType;
use \jamesiarmes\PhpEws\Type\MessageType;
use \jamesiarmes\PhpEws\Type\SingleRecipientType;


class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('atlas:test')
            ->setDescription('.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $this->testSend();

//        $userId = 1449;
//
//        $result = $this->getContainer()->get('snc_redis.default')->hscan(TaskManagerTopic::REDIS_KEY_CURRENT_TASKS, 0, [
//                'MATCH' => "user:" . $userId . ":*",
//                'COUNT' => 1000
//            ]);

    }

    private function testSend() {


// Replace with the path to the image file to be attached and the recipient
// information.

        $urlImg = 'http://atlas.starter24.test/images/email_assets/starter-footer-logo.png';
        $tempFile = TempFileHandler::fromUrl($urlImg);

        $file_path = '/home/dylesiu/www/sigmeo/atlas/web/img/remove-icon-small.png';
        $recipient_email = 'dylesiu@gmail.com';
        $recpient_name = 'Dylesiu';


// Set connection information.
        $host = 'mail.starter24.pl';
        $username = 'atlas_test';
        $password = 'St5:ArCx10';
        $version = Client::VERSION_2010;

        $client = new Client($host, $username, $password, $version);


// Open file handlers.
        $file = new SplFileObject($file_path);
        $finfo = finfo_open();
        $filename = $file->getBasename();

// Build the request.
        $request = new CreateItemType();
        $request->Items = new NonEmptyArrayOfAllItemsType();

// Save the message, but do not send it.
        $request->MessageDisposition = MessageDispositionType::SAVE_ONLY;

// Create the message.
        $message = new MessageType();
        $message->Subject = 'EWS Inline Image';
        $message->ToRecipients = new ArrayOfRecipientsType();
//        $message->Attachments = new NonEmptyArrayOfAttachmentsType();

// Set the sender.
        $message->From = new SingleRecipientType();
        $message->From->Mailbox = new EmailAddressType();
        $message->From->Mailbox->EmailAddress = $username;

// Set the recipient.
        $recipient = new EmailAddressType();
        $recipient->Name = $recpient_name;
        $recipient->EmailAddress = $recipient_email;
        $message->ToRecipients->Mailbox[] = $recipient;

// Set the message body.
        $message->Body = new BodyType();
        $message->Body->BodyType = BodyTypeType::HTML;
        $message->Body->_ = <<<BODY
<html>
  <head></head>
  <body>
    <p>This is the Exchange logo:</p>
    <img width="256" height="256" id="0" src="cid:abcde">
  </body>
</html>
BODY;

// Build the file attachment.
        $attachment = new FileAttachmentType();
        $attachment->IsInline = true;
//        $attachment->Content = file_get_contents('/home/dylesiu/www/sigmeo/atlas/web/img/remove-icon-small.png');
//        $attachment->Content = $file->openFile()->fread($file->getSize());
//        $attachment->Content = '/home/dylesiu/www/sigmeo/atlas/web/img/remove-icon-small.png';

        $data = file_get_contents($file_path);
        $type = pathinfo($file_path, PATHINFO_EXTENSION);
//

//        $attachment->ContentLocation = $file_path;
//        $attachment->Content = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $attachment->Content =  base64_encode($data);
        $attachment->Name = 'abcde';
        $attachment->ContentType = 'image/png';
        $attachment->ContentId = 'abcde';

// Add the message to the request.
        $request->Items->Message[] = $message;

        $response = $client->CreateItem($request);

// Iterate over the results, printing any error messages.
        $response_messages = $response->ResponseMessages->CreateItemResponseMessage;

        $id = null;

        foreach ($response_messages as $response_message) {

            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(STDERR, "Message failed to create with \"$code: $message\"\n");
                continue;
            }

            foreach ($response_message->Items->Message as $item) {

                $id = $item->ItemId->Id;

            }

            fwrite(STDOUT, "Message sent successfully.\n");
        }


        $this->addAttach($client, $id);
    }

    private function updateBody($id, $changedKey, $html, $client) {

        $request = new UpdateItemType();
        $request->ConflictResolution = ConflictResolutionType::ALWAYS_OVERWRITE;
        $request->MessageDisposition = 'SaveOnly';
        $request->ItemChanges = array();

        $change = new ItemChangeType();
        $change->ItemId = new ItemIdType();
        $change->ItemId->Id = $id;
        $change->ItemId->ChangeKey = $changedKey;
        $change->Updates = new NonEmptyArrayOfItemChangeDescriptionsType();
        $change->Updates->SetItemField = array();

        $field = new SetItemFieldType();
        $field->Item = new ItemType();
        $field->FieldURI = new PathToUnindexedFieldType();
        $field->FieldURI->FieldURI = UnindexedFieldURIType::ITEM_BODY;

        $body = new BodyType();
        $body->BodyType = BodyTypeType::HTML;
        $body->_ = $html;

        $field->Item->Body = $body;

        $change->Updates->SetItemField[] = $field;

        $request->ItemChanges[] = $change;

        $response = $client->UpdateItem($request);

        $response_messages = $response->ResponseMessages->UpdateItemResponseMessage;
        return $response_messages;

    }

    private function addAttach(Client $client, $id) {

        $request = new CreateAttachmentType();
        $request->ParentItemId = new ItemIdType();
        $request->ParentItemId->Id = $id;
        $request->Attachments = new NonEmptyArrayOfAttachmentsType();

        $file_path = '/home/dylesiu/www/sigmeo/atlas/web/img/remove-icon-small.png';

        $file = new \SplFileObject($file_path);
        $finfo = finfo_open();

        $attachment = new FileAttachmentType();
        $attachment->Content = $file->openFile()->fread($file->getSize());
        $attachment->Name = "abcde";
        $attachment->ContentId = "abcde";
        $attachment->IsInline = true;
        $attachment->ContentType = 'image/png';

        $request->Attachments->FileAttachment[] = $attachment;

        $response = $client->CreateAttachment($request);

        $response_messages = $response->ResponseMessages
            ->CreateAttachmentResponseMessage;

        $newId = null;
        $newChangeKey = null;

        foreach ($response_messages as $response_message) {

            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;

                return [
                    'success' => false,
                    'error_message' => "addAttachments() - " . $code . ": " . $message
                ];

            }

            foreach ($response_message->Attachments->FileAttachment as $attachment) {
                $newId = $attachment->AttachmentId->RootItemId;
                $newChangeKey = $attachment->AttachmentId->RootItemChangeKey;
            }
        }

        $html = <<<BODY
<html>
  <head></head>
  <body>
    <p>This is the Exchange logo:</p>
    <img width="256" height="256" id="0" src="cid:abcde">
    <p>I jeszcze coś tam</p>
  </body>
</html>
BODY;


        $res = $this->updateBody($id, $newChangeKey, $html, $client);


        foreach ($res as $response_message) {

            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;

                return [
                    'success' => false,
                    'error_message' => "addAttachments() - " . $code . ": " . $message
                ];

            }

            foreach ($response_message->Items->Message as $item) {

                $newId = $item->ItemId->Id;
                $newChangeKey = $item->ItemId->ChangeKey;

            }

        }

        $request = new SendItemType();
        $request->SaveItemToFolder = true;
        $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();

        $item = new ItemIdType();
        $item->Id = $newId;
        $item->ChangeKey = $newChangeKey;
        $request->ItemIds->ItemId[] = $item;

        $send_folder = new TargetFolderIdType();
        $send_folder->DistinguishedFolderId = new DistinguishedFolderIdType();
        $send_folder->DistinguishedFolderId->Id = DistinguishedFolderIdNameType::SENT;

        $request->SavedItemFolderId = $send_folder;

        $response = $client->SendItem($request);
        $response_messages = $response->ResponseMessages->SendItemResponseMessage;

    }

}

