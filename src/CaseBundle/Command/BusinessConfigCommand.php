<?php

namespace CaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BusinessConfigCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('atlas:case:update-business-config')
            ->setDescription('Aktulizacja konfiguracji programów.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $businessConfigService = $this->getContainer()->get('case.business.config.service');

        $io = new SymfonyStyle($input, $output);
        $io->title('Updating config platforms...');

        list($platformConfig, $programConfig) = $businessConfigService->updatePlatformsConfigs();

        $io->comment('Updated ' . $platformConfig . ' platforms config.');
        $io->comment('Updated ' . $programConfig . ' programs config.');

        $io->success('Done.');


    }
}
