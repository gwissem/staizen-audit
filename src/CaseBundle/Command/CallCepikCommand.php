<?php

namespace CaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CallCepikCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('atlas:case:call-cepik')
            ->setDescription('Call to cepik.')
            ->setHelp('This command allows you to call cepik and use it in job queue.')
            ->addArgument('groupProcessId', InputArgument::REQUIRED, 'The id of the group process.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $processHandler = $this->getContainer()->get('case.process_handler');
        $processHandler->cepikAttributes($input->getArgument('groupProcessId'));
    }
}