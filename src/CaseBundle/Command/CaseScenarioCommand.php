<?php

namespace CaseBundle\Command;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Service\ScenarioBuilder;
use CaseBundle\Utils\ScenarioConfig;
use Negotiation\Exception\InvalidArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use CaseBundle\Stages\StagePoint;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaseScenarioCommand extends ContainerAwareCommand
{

    //sf3 atlas:build_scenario -u 1
    //php bin/console atlas:build_scenario SimpleNewCase -u 1 -c Naprawa_Poznan
    //php bin/console atlas:build_scenario SimpleTowing -u 1449 -c Holowanie_Warszawa

    private $userId = null;

    /** @var SymfonyStyle */
    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:build_scenario')
            ->addArgument(
                'scenarioName',
                InputArgument::REQUIRED,
                'Name of scenario to run.',
                null
            )
            ->addOption(
                'config',
                'c',
                InputOption::VALUE_OPTIONAL,
                'Config with data for scenario.',
                null
            )
            ->addOption(
                'userId',
                'u',
                InputOption::VALUE_OPTIONAL,
                'Id user\'a where will be open new case.',
                null
            )
            ->addOption(
                'interactiveMode',
                'i',
                InputOption::VALUE_OPTIONAL,
                'Interactive Mode',
                false
            )
            ->setDescription('Builder scenarios');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->io = new SymfonyStyle($input, $output);
        $this->setConfig($input, $output);

        $scenarioName = $input->getArgument('scenarioName');

        if(empty($scenarioName)) throw new InvalidArgument('Argument ScenarioName is required!');

        $this->io->title(' Scenario <info>'.$scenarioName.'</info> is started...');

        $data = $this->buildScenario($scenarioName);
//        $data = $this->buildScenario('FromDiagnosisToServices');
//        $data = $this->buildScenario('SimplePhoneIsRinging');
//        $data = $this->buildScenario('Diagnosis');
//        $data = $this->buildScenario('SimplePhoneIsRinging');

        $this->io->block('');

        if ($data instanceof \Exception) {

            $this->io->writeln(' <error>Fail!</error>');
            $this->io->writeln(' <error>' . $data->getMessage() . '</error>');

        } else if ($data instanceof StagePoint) {

            $this->io->writeln(' <info>Success.</info> LastProcessId: <comment>' . $data->getId() . '</comment>, RootId: <comment>' . $data->getRootId() . '</comment>');
            $this->io->success($data->getName());
            $this->sendToUser($data);
        }

    }

    private function sendToUser(StagePoint $stagePoint)
    {

        if ($this->userId) {

            $data = [
                'users' => [$this->userId],
                'task' => $stagePoint->getDescription(),
                'action' => 'force-create'
            ];

            $this->getContainer()->get('gos_web_socket.zmq.pusher')->push($data, 'atlas_dashboard_case');

            $this->io->success('Pushed case to user: ' . $this->userId);

        }

    }

    private function setConfig(InputInterface $input, OutputInterface $output)
    {

        $this->userId = $input->getOption('userId');
        ScenarioBuilder::setInteractiveMode($input->getOption('interactiveMode'));
        ScenarioBuilder::$ioSymfony = $this->io;
        ScenarioBuilder::$input = $input;
        ScenarioBuilder::$output = $output;
        ScenarioBuilder::$questionHelper = $this->getHelper('question');

        $configName = $input->getOption('config');
        if(!empty($configName)) {
            ScenarioBuilder::$scenarioConfig = new ScenarioConfig($configName);
        }

        /** For ommit "Circular reference detected for service" */
        $this->getContainer()->get('twig')->getBaseTemplateClass();
        /** ----*/

        ScenarioBuilder::$operationDashboardInterface = $this->getContainer()->get('operational.interface');
//        ScenarioBuilder::$translator = $this->getContainer()->get('translator');

    }


    /**
     * @param $scenarioName
     * @return StagePoint|\Exception
     */
    private function buildScenario($scenarioName)
    {

        return $this->getContainer()->get('case.scenario_builder')->build($scenarioName);

    }
}
