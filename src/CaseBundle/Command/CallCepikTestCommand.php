<?php

namespace CaseBundle\Command;

use CaseBundle\Utils\Snoopy;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CallCepikTestCommand extends ContainerAwareCommand
{

    /** @var SymfonyStyle */
    public $symfonyIO;

    protected function configure()
    {
        $this
            ->setName('atlas:case:call-cepik-test')
            ->setDescription('Call to cepik.')
            ->setHelp('This command allows you to call cepik and use it in job queue.')
            ->addArgument('groupProcessId', InputArgument::REQUIRED, 'The id of the group process.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->symfonyIO = new SymfonyStyle($input, $output);

        $this->runCepik(intval($input->getArgument('groupProcessId')));

    }

    private function runCepik($groupProcessId, $alternativeDate = false)
    {

        $processHandler = $this->getContainer()->get('case.process_handler');

        try {

            $datePath = $alternativeDate ? '74,233' : '74,561';

            $regNo = $processHandler->getAttributeValue('74,72', $groupProcessId, 'string');
            $vin = $processHandler->getAttributeValue('74,71', $groupProcessId, 'string');
            $firstRegDate = $processHandler->getAttributeValue($datePath, $groupProcessId, 'date');

            $this->symfonyIO->comment('Run with: ' . $groupProcessId . ', ' . (empty($regNo) ? '-' : $regNo) . ', ' . (empty($vin) ? '-' : $vin) . ', ' . (empty($firstRegDate) ? '-' : $firstRegDate));

            if (!empty($regNo) && !empty($vin) && !empty($firstRegDate)) {
                $snoopy = new Snoopy();

                $snoopy->fetch('https://historiapojazdu.gov.pl/');
                $snoopy->setcookies();

                preg_match('!action="(https://historiapojazdu\.gov\.pl/strona\-glowna.[^"]+)"!', $snoopy->results, $matches);
                $post['URL'] = $matches[1];

                preg_match('!id="javax\.faces\.ViewState" value="(.[^"]+)"!', $snoopy->results, $matches);
                $post['ViewState'] = $matches[1];

                preg_match('!name="javax\.faces\.encodedURL" value="(.[^"]+)"!', $snoopy->results, $matches);
                $post['encodedURL'] = $matches[1];

                $snoopy->submit($post['encodedURL'], array(
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:formularz' => '_historiapojazduportlet_WAR_historiapojazduportlet_:formularz',
                    'javax.faces.encodedURL' => $post['encodedURL'],
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:rej' => $regNo,
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:vin' => $vin,
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:data' => date('d.m.Y', strtotime($firstRegDate)),
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:btnSprawdz' => 'Sprawdź pojazd »',
                    'javax.faces.ViewState' => $post['ViewState']));

                if (strpos($snoopy->results, 'nieZnalezionoHistori') === false) {

                    $portletId = "";
                    preg_match('!id="_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt([0-9]+)!', $snoopy->results, $matches);
                    if (count($matches) > 1) {
                        $portletId = trim($matches[1]);
                    }

                    preg_match('!<span>Dopuszczalna masa całkowita: </span> <strong> <span class="value">([0-9]+)!', $snoopy->results, $matches);
                    $this->logValue('DMC', $matches, 'int');

                    preg_match('!<span>Pojemność silnika: </span> <strong> <span class="value">([0-9]+)!', $snoopy->results, $matches);
                    $this->logValue('Pojemność silnika', $matches, 'int');

                    preg_match('!<span id="_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt' . $portletId . ':paliwo" class="lowercase">([0-9a-ząęćóśłńżź ]+)!', $snoopy->results, $matches);
                    $this->logValue('Paliwo', $matches, 'int', 'engineType');

//                    $make = '';
//                    $model = '';
//                    $makeModel = $processHandler->getAttributeValue('74,73', $groupProcessId, 'int');

                    preg_match('!<span id="_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt' . $portletId . ':marka" class="uppercase">([0-9_a-zA-Z ]+)!', $snoopy->results, $matches);
                    if (count($matches) > 1) {
                        $make = trim($matches[1]);
                        $this->symfonyIO->success('Marka: ' . $make);
                    }
                    preg_match('!<span id="_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt' . $portletId . ':model" class="uppercase">([0-9_a-zA-Z ]+)!', $snoopy->results, $matches);
                    if (count($matches) > 1) {
                        $model = trim($matches[1]);
                        $this->symfonyIO->success('Model: ' . $model);
                    }

                } else {

                    $this->symfonyIO->error('Nie znaleziono historii pojazdu. ');

                    if (!$alternativeDate) {
                        $this->runCepik($groupProcessId, true);
                    }
                }
            }
            else {
                $this->symfonyIO->error('Nie ma wszystkich wymaganych danych.');
                if (!$alternativeDate) {
                    $this->runCepik($groupProcessId, true);
                }
            }
        } catch (\Exception $e) {
            $this->symfonyIO->error( $e->getMessage());
        }

    }

    function logValue($name, $matches, $valueType, $dictionaryMatch = false) {

        if (count($matches) > 1) {
            $value = trim($matches[1]);
            if ($valueType == 'int' && !$dictionaryMatch) {
                $value = (int)str_replace(' ', '', $value);
            } else {
                $value = $this->mapCepikToDictionary($value);
            }

            $this->symfonyIO->success($name . ': ' . $value);
        }
        else {
            $this->symfonyIO->warning('Brak ' . $name);
        }
    }

    protected function mapCepikToDictionary($value)
    {
        $words = ['olej napędowy', 'energia elektryczna'];
        $replacements = ['diesel', 'EV'];

        return str_replace($words, $replacements, $value);
    }

}