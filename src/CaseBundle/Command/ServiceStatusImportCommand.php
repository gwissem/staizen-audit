<?php

namespace CaseBundle\Command;

use CaseBundle\Entity\ServiceStatusDictionary;
use CaseBundle\Entity\ServiceStatusDictionaryTranslation;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ServiceStatusImportCommand extends ContainerAwareCommand
{

    const SPARX_IN_PROGRESS = 'IN_PROGRESS';

    protected function configure()
    {
        $this
            ->setName('atlas:case:status-import')
            ->setDescription('Import statuses for ServiceStatusDictionary');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** NAPRAWA NA DRODZE */

//        $serviceFixing = $em->getRepository('CaseBundle:ServiceDefinition')->find(2);
//
//        foreach ($this->getFixingStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($serviceFixing);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//            $newStatusMessage = new ServiceStatusDictionaryTranslation();
//            $newStatusMessage->setMessage($status['messages']['pl']);
//            $newStatusMessage->setLocale('pl');
//            $newStatus->getTranslations()->set('pl', $newStatusMessage);
//            $newStatusMessage->setTranslatable($newStatus);
//
//            $em->persist($newStatusMessage);
//            $em->persist($newStatus);
//        }

        /** HOLOWANIE */

//        $serviceTowing = $em->getRepository('CaseBundle:ServiceDefinition')->find(1);
//
//        foreach ($this->getTowingStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($serviceTowing);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//            $newStatusMessage = new ServiceStatusDictionaryTranslation();
//            $newStatusMessage->setMessage($status['messages']['pl']);
//            $newStatusMessage->setLocale('pl');
//            $newStatus->getTranslations()->set('pl', $newStatusMessage);
//            $newStatusMessage->setTranslatable($newStatus);
//
//            $em->persist($newStatusMessage);
//            $em->persist($newStatus);
//        }

            /** CZĘŚCI ZAMIENNE */

//        $serviceReplacementParts = $em->getRepository('CaseBundle:ServiceDefinition')->find(8);
//
//        foreach ($this->getReplacementPartsStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($serviceReplacementParts);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//            $newStatusMessage = new ServiceStatusDictionaryTranslation();
//            $newStatusMessage->setMessage($status['messages']['pl']);
//            $newStatusMessage->setLocale('pl');
//            $newStatus->getTranslations()->set('pl', $newStatusMessage);
//            $newStatusMessage->setTranslatable($newStatus);
//
//            $em->persist($newStatusMessage);
//            $em->persist($newStatus);
//        }

        /** NOCLEG */

//        $service = $em->getRepository('CaseBundle:ServiceDefinition')->find(4);
//
//        foreach ($this->getHotelStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($service);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//
//            foreach ($status['messages'] as $language => $statusMessage) {
//                $newStatusMessage = new ServiceStatusDictionaryTranslation();
//                $newStatusMessage->setMessage($statusMessage);
//                $newStatusMessage->setLocale($language);
//                $newStatus->getTranslations()->set($language, $newStatusMessage);
//                $newStatusMessage->setTranslatable($newStatus);
//                $em->persist($newStatusMessage);
//            }
//
//            $em->persist($newStatus);
//        }

        /** TAXI */

//        $service = $em->getRepository('CaseBundle:ServiceDefinition')->find(5);
//
//        foreach ($this->getTaxiStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($service);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//
//            foreach ($status['messages'] as $language => $statusMessage) {
//                $newStatusMessage = new ServiceStatusDictionaryTranslation();
//                $newStatusMessage->setMessage($statusMessage);
//                $newStatusMessage->setLocale($language);
//                $newStatus->getTranslations()->set($language, $newStatusMessage);
//                $newStatusMessage->setTranslatable($newStatus);
//                $em->persist($newStatusMessage);
//            }
//
//            $em->persist($newStatus);
//        }


        /** Travel */

//        $service = $em->getRepository('CaseBundle:ServiceDefinition')->find(6);
//
//        foreach ($this->getTravelStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($service);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//
//            foreach ($status['messages'] as $language => $statusMessage) {
//                $newStatusMessage = new ServiceStatusDictionaryTranslation();
//                $newStatusMessage->setMessage($statusMessage);
//                $newStatusMessage->setLocale($language);
//                $newStatus->getTranslations()->set($language, $newStatusMessage);
//                $newStatusMessage->setTranslatable($newStatus);
//                $em->persist($newStatusMessage);
//            }
//
//            $em->persist($newStatus);
//        }

//        /** Credit */
//
//        $service = $em->getRepository('CaseBundle:ServiceDefinition')->find(9);
//
//        foreach ($this->getCreditStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($service);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//
//            foreach ($status['messages'] as $language => $statusMessage) {
//                $newStatusMessage = new ServiceStatusDictionaryTranslation();
//                $newStatusMessage->setMessage($statusMessage);
//                $newStatusMessage->setLocale($language);
//                $newStatus->getTranslations()->set($language, $newStatusMessage);
//                $newStatusMessage->setTranslatable($newStatus);
//                $em->persist($newStatusMessage);
//            }
//
//            $em->persist($newStatus);
//        }

        /** Transport */
//
//        $service = $em->getRepository('CaseBundle:ServiceDefinition')->find(7);
//
//        foreach ($this->getTransportStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($service);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//
//            foreach ($status['messages'] as $language => $statusMessage) {
//                $newStatusMessage = new ServiceStatusDictionaryTranslation();
//                $newStatusMessage->setMessage($statusMessage);
//                $newStatusMessage->setLocale($language);
//                $newStatus->getTranslations()->set($language, $newStatusMessage);
//                $newStatusMessage->setTranslatable($newStatus);
//                $em->persist($newStatusMessage);
//            }
//
//            $em->persist($newStatus);
//        }

        /** Auto zastępcze */
//
//        $service = $em->getRepository('CaseBundle:ServiceDefinition')->find(3);
//
//        foreach ($this->getReplacementCarStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($service);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//
//            foreach ($status['messages'] as $language => $statusMessage) {
//                $newStatusMessage = new ServiceStatusDictionaryTranslation();
//                $newStatusMessage->setMessage($statusMessage);
//                $newStatusMessage->setLocale($language);
//                $newStatus->getTranslations()->set($language, $newStatusMessage);
//                $newStatusMessage->setTranslatable($newStatus);
//                $em->persist($newStatusMessage);
//            }
//
//            $em->persist($newStatus);
//        }

        /** Parking */

//        $service = $em->getRepository('CaseBundle:ServiceDefinition')->find(14);
//
//        foreach ($this->getParkingStatuses() as $status) {
//
//            $newStatus = new ServiceStatusDictionary();
//            $newStatus->setProgress($status['progress']);
//            $newStatus->setService($service);
//            $newStatus->setSparxStatus($status['sparxStatus']);
//
//            foreach ($status['messages'] as $language => $statusMessage) {
//                $newStatusMessage = new ServiceStatusDictionaryTranslation();
//                $newStatusMessage->setMessage($statusMessage);
//                $newStatusMessage->setLocale($language);
//                $newStatus->getTranslations()->set($language, $newStatusMessage);
//                $newStatusMessage->setTranslatable($newStatus);
//                $em->persist($newStatusMessage);
//            }
//
//            $em->persist($newStatus);
//        }

        $em->flush();
    }

    private function getFixingStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'Czeka na organizację',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Kontraktor przyjął',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Kontraktor przyjął zlecenie. Szacowane przybycie o godzinie [godzina]',
                ],
                'progress' => 2,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Kontraktor na miejscu',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 6,
                'messages' => [
                    'pl' => 'Kontraktor naprawił',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 7,
                'messages' => [
                    'pl' => 'Kontraktor nie naprawił',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 8,
                'messages' => [
                    'pl' => 'Zakończono usługę naprawy na drodze',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 9,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 10,
                'messages' => [
                    'pl' => 'Pusty wyjazd',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 11,
                'messages' => [
                    'pl' => 'Samodzielna organizacja przez Klienta',
                ],
                'progress' => 6,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
        ];
    }

    private function getReplacementCarStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'Czeka na organizację',
                    'en' => 'Czeka na organizację',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Organizowane',
                    'en' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Wypożyczalnia przyjęła',
                    'en' => 'Wypożyczalnia przyjęła',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Pojazd zastępczy w drodze do Klienta Szacowane przybycie za [liczba minut] minut',
                    'en' => 'Pojazd zastępczy w drodze do Klienta Szacowane przybycie za [liczba minut] minut'
                ],
                'progress' => 2,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Pojazd zastępczy dotarł/ odebrany przez Klienta',
                    'en' => 'Pojazd zastępczy dotarł/ odebrany przez Klienta'
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 6,
                'messages' => [
                    'pl' => 'Wynajem trwa. Doba [liczba – która doba wynajmu], z [liczba – na ile dni wysłano gop] dób ‚ maksymalny okres wynajmu to (liczba) doby',
                    'en' => 'Wynajem trwa. Doba [liczba – która doba wynajmu], z [liczba – na ile dni wysłano gop] dób ‚ maksymalny okres wynajmu to (liczba) doby'
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 7,
                'messages' => [
                    'pl' => 'Zakończono wynajem',
                    'en' => 'Zakończono wynajem',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 8,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                    'en' => 'Anulowane bezkosztowo'
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 9,
                'messages' => [
                    'pl' => 'Odwołano z kosztami',
                    'en' => 'Odwołano z kosztami'
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 10,
                'messages' => [
                    'pl' => 'Samodzielna organizacja przez Klienta',
                    'en' => 'Samodzielna organizacja przez Klienta'
                ],
                'progress' => 6,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
        ];
    }

    private function getReplacementPartsStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'Możliwa do organizacji',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Czeka na organizację',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Zorganizowano',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Zakończono',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 6,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 7,
                'messages' => [
                    'pl' => 'Odwołano z kosztami',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ]
        ];
    }

    private function getHotelStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'Organizowane',
                    'en' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Zorganizowano',
                    'en' => 'Zorganizowano',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Zakończono',
                    'en' => 'Zakończono',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                    'en' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Odwołano z kosztami',
                    'en' => 'Odwołano z kosztami',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 6,
                'messages' => [
                    'pl' => 'Samodzielna organizacja przez Klienta',
                    'en' => 'Samodzielna organizacja przez Klienta',
                ],
                'progress' => 6,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
        ];
    }

    private function getTravelStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'Organizowane',
                    'en' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Zorganizowano',
                    'en' => 'Zorganizowano',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Zakończono',
                    'en' => 'Zakończono',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                    'en' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Odwołano z kosztami',
                    'en' => 'Odwołano z kosztami',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 6,
                'messages' => [
                    'pl' => 'Samodzielna organizacja przez Klienta',
                    'en' => 'Samodzielna organizacja przez Klienta',
                ],
                'progress' => 6,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
        ];
    }

    private function getTransportStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'Organizowane',
                    'en' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Zorganizowano',
                    'en' => 'Zorganizowano',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Zakończono',
                    'en' => 'Zakończono',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                    'en' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Odwołano z kosztami',
                    'en' => 'Odwołano z kosztami',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 6,
                'messages' => [
                    'pl' => 'Samodzielna organizacja przez Klienta',
                    'en' => 'Samodzielna organizacja przez Klienta',
                ],
                'progress' => 6,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
        ];
    }

    private function getCreditStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'Organizowane',
                    'en' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Zorganizowano',
                    'en' => 'Zorganizowano',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Zakończono',
                    'en' => 'Zakończono',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                    'en' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Odwołano z kosztami',
                    'en' => 'Odwołano z kosztami',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ]
        ];
    }

    private function getTaxiStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'Organizowane',
                    'en' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Kontraktor przyjął',
                    'en' => 'Kontraktor przyjął',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Kontraktor jedzie do klienta',
                    'en' => 'Kontraktor jedzie do klienta',
                ],
                'progress' => 2,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Kontraktor na miejscu',
                    'en' => 'Kontraktor na miejscu',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Zakończono',
                    'en' => 'Zakończono',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 6,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                    'en' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 7,
                'messages' => [
                    'pl' => 'Odwołano z kosztami',
                    'en' => 'Odwołano z kosztami',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 8,
                'messages' => [
                    'pl' => 'Samodzielna organizacja przez Klienta',
                    'en' => 'Samodzielna organizacja przez Klienta',
                ],
                'progress' => 6,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
        ];
    }

    private function getTowingStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                  'pl' => 'Czeka na organizację',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                  'pl' => 'Organizowane',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                  'pl' => 'Kontraktor przyjął',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                  'pl' => 'Kontraktor przyjął zlecenie. Szacowane przybycie o godzinie [godzina]',
                ],
                'progress' => 2,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                  'pl' => 'Kontraktor na miejscu',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 6,
                'messages' => [
                  'pl' => 'Kontraktor holuje',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 7,
                'messages' => [
                  'pl' => 'Holuje na parking',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 8,
                'messages' => [
                  'pl' => 'Na parkingu',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 9,
                'messages' => [
                  'pl' => 'Drugie holowanie',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 10,
                'messages' => [
                  'pl' => 'Zakończono holowanie. Pojazd w [nazwa ASO], [miejscowość]',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 11,
                'messages' => [
                  'pl' => 'Zakończono usługę holowania',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 12,
                'messages' => [
                  'pl' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 13,
                'messages' => [
                  'pl' => 'Pusty wyjazd',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 14,
                'messages' => [
                  'pl' => 'Samodzielna organizacja przez Klienta',
                ],
                'progress' => 6,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
        ];
    }


    private function getParkingStatuses() {

        return [
            [
                'id' => 1,
                'messages' => [
                    'pl' => 'W trakcie organizacji',
                    'en' => 'W trakcie organizacji',
                ],
                'progress' => 1,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 2,
                'messages' => [
                    'pl' => 'Zorganizowany parking w {#partnerNameFull(610)#}',
                    'en' => 'Zorganizowany parking w {#partnerNameFull(610)#}',
                ],
                'progress' => 3,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 3,
                'messages' => [
                    'pl' => 'Zakończono',
                    'en' => 'Zakończono',
                ],
                'progress' => 4,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 4,
                'messages' => [
                    'pl' => 'Anulowane bezkosztowo',
                    'en' => 'Anulowane bezkosztowo',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ],
            [
                'id' => 5,
                'messages' => [
                    'pl' => 'Odwołano z kosztami',
                    'en' => 'Odwołano z kosztami',
                ],
                'progress' => 5,
                'sparxStatus' => self::SPARX_IN_PROGRESS
            ]
        ];
    }

}
