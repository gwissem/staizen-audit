<?php

namespace CaseBundle\Command;

use CaseBundle\Entity\FormControl;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FormControlsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('case:form_controls')
            ->addOption('filter_js', 'f', InputOption::VALUE_OPTIONAL, 'Cześć kodu która będzie przeszukiwana w JS.')
//            ->addOption('save_validation_js', 's', InputOption::VALUE_OPTIONAL, 'Przekompilowanie JS dla kontrolki (dla wyników).')
            ->setDescription('Narzędzie dla Form Controls');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var SymfonyStyle $io */
        $io = new SymfonyStyle($input, $output);

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** For ommit "Circular reference detected for service" */
        $this->getContainer()->get('twig')->getBaseTemplateClass();
        /** ----*/

        /** @var FormControl[] $controls */
        $controls = $em->getRepository('CaseBundle:FormControl')->findAll();

        $filterJS = $input->getOption('filter_js');

        $rows = [];

        foreach ($controls as $control) {

            $javascript = $control->getJavaScript();

            if(!empty($javascript)) {
                if(!empty($filterJS)) {
                    if(strpos($javascript, $filterJS) !== FALSE) {
                        $rows[] = [
                            'id' => $control->getId(),
                            'control' => $control,
                            'javascript' => '<fg=green>✔</>'
                        ];
                    }
                }
                else {
                    $rows[] = [
                        'id' => $control->getId(),
                        'control' => $control,
                        'javascript' => '<fg=green>✔</>'
                    ];
                }
            }

        }

        if(!empty($filterJS)) {
            $io->title('Form Controls, które mają w JS słowo: "' . $filterJS .'"');
        }

        $table = new Table($io);
        $table->setHeaders(array('ID', 'Has JavaScript'));

        foreach ($rows as $control) {

            $table->addRow([
                $control['id'], $control['javascript']
            ]);

        }

        $table->render();

        $confirm = $io->confirm('Znaleziono ' . count($rows) . ' kontrolek. Czy zaktualizować JS?', false);

        if($confirm) {

            $attributeConditionService = $this->getContainer()->get('case.attribute_condition');

            foreach ($rows as $control) {
                $successSave = $attributeConditionService->trySaveValidationJS($control['control']);
                if($successSave) {
                    $em->persist($control['control']);
                    $em->flush();
                    $io->writeln('Zaktualizowano kontrolkę: ' . $control['id']);
                }
            }
        }

    }
}
