<?php

namespace CaseBundle\Command;

use CaseBundle\Entity\ServiceStatusDictionary;
use CaseBundle\Entity\ServiceStatusDictionaryTranslation;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Utils\Language;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ParserCommand extends ContainerAwareCommand
{

    // php bin/console atlas:case:parser -g 25283 -t '{@610@}'


    protected function configure()
    {
        $this
            ->setName('atlas:case:parser')
            ->addOption('groupProcessId', 'g', InputOption::VALUE_REQUIRED, 'The id of the group process.')
            ->addOption('text', 't', InputOption::VALUE_REQUIRED, 'Text for parse.')
            ->setDescription('Parser do testów');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->getContainer()->get('twig')->getBaseTemplateClass();
        $io = new SymfonyStyle($input, $output);

        $groupProcessId = intval($input->getOption('groupProcessId'));
        $text = $input->getOption('text');

        $helperParserService = $this->getContainer()->get('case.service.helper_parser_service');
        $result = $helperParserService->parseString($text, $groupProcessId);

        if(!$result->isParsedError()) {
            $io->success('ParsedValue: ' . $result->parsedValue);

            if(!$result->isEvaluateError()) {
                $io->success("Evaluate: " . $result->evaluateValue);
            }
            else {
                $io->warning('Fail evaluate...');
                $io->warning($result->evaluateErrorMessage);
            }

        }
        else {
            $io->error($result->errorMessage);
        }

        $io->newLine();

    }
}
