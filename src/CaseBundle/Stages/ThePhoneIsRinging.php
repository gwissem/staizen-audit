<?php

namespace CaseBundle\Stages;

use CaseBundle\Entity\AttributeValue;

class ThePhoneIsRinging extends Stage
{

    const PROCESS_PREFIX = '1012';

    /**
     * @return int
     * @throws \Exception
     */
    public function createNewProcess()
    {
        $id = parent::createNew('1012.002');
        $this->groupProcessInstanceId = $id;
        $this->printOk($id);
        return $id;
    }

//    public function step_002($controls) {
//
//    }

    public function controls_002()
    {

        return [
            '197' => [
                'value' => NULL,
                'forceNull' => false,
                'type' => AttributeValue::VALUE_STRING,
                'interactive' => false,
                'interactiveIfNull' => true,
                'desc' => 'Numer zgłaszającego'
            ],
            '253' => [
                'value' => 11,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Numer platformy'
            ]
        ];

    }

    public function controls_006()
    {

        return [
            '452' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy w istniejącej sprawie?'
            ],
            '235' => [
                'value' => -1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'ID sprawy w której dzwoni klient'
            ]
        ];

    }

    public function controls_11()
    {

        return [
            '404' => [
                'value' => -1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Dodaje nową osobę'
            ]
        ];

    }

    public function controls_013()
    {

        return [
            '81,342,66' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Nazwisko dzwoniącego'
            ],
            '81,342,64' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Imię dzwoniącego'
            ],
        ];

    }


    public function controls_014()
    {

        return [
            '81,342,66' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Nazwisko dzwoniącego'
            ],
            '81,342,64' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Imię dzwoniącego'
            ],
            '81,342,408,197' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Numer dzwoniacego'
            ]
        ];

    }


    public function controls_016()
    {

        return [
            '451' => [
                'value' => 1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy mam przyjemność rozmawiać z...'
            ]
        ];

    }

    public function controls_017()
    {

        return [
            '453' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy dzwoni Pan/Pani w sprawie...'
            ]
        ];

    }


}