<?php

namespace CaseBundle\Stages;

use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\CaseFormGenerator;
use CaseBundle\Service\ProcessHandler;
use CaseBundle\Service\ScenarioBuilder;
use Doctrine\ORM\EntityManager;
use OperationalBundle\Service\ErrorValidator;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Stage
{
    const PROCESS_PREFIX = '0000';
    const EMPTY_CONTROLS = 'EMPTY';

    /** @var ProcessHandler $processHandler */
    protected $processHandler;

    /** @var ConsoleOutput $output */
    protected $output;

    public $groupProcessInstanceId;

    /** @var string - obecnie przetwarzany krok */
    public $currentStepId;

    /**
     * Stage constructor.
     * @param ProcessHandler $processHandler
     * @param StagePoint|null $stagePoint
     */
    public function __construct(ProcessHandler $processHandler, StagePoint $stagePoint = null)
    {
        $this->processHandler = $processHandler;

        if ($stagePoint !== null) {
            $this->startFromStagePoint($stagePoint);
        }

        $this->output = new ConsoleOutput();
        $outputFormatter = new OutputFormatter(true);
        $this->output->setFormatter($outputFormatter);
    }

    /**
     * @param $stepId
     * @return int
     * @throws \Exception
     */
    protected function createNew($stepId)
    {

        $this->logStart($stepId);
        return $this->processHandler->start($stepId, NULL, ScenarioBuilder::USER_ID, ScenarioBuilder::ORIGINAL_USER_ID);

    }

//    /** @return array */
//    protected function availableSteps() {
//        return [];
//    }

    private function getControls($suffixStepId, $newControls)
    {

        $funName = 'controls_' . $suffixStepId;

        if (method_exists($this, $funName)) {

            $resolver = new OptionsResolver();
            $controls = call_user_func(array($this, $funName));
            $resolver->setDefaults($controls);

            /**
             * Keys in Control
             * REQUIRED!    'value' => '*'
             * REQUIRED!    'type' => AttributeValue::VALUE_INT, AttributeValue::VALUE_TEXT, AttributeValue::VALUE_DECIMAL, AttributeValue::VALUE_DATE, AttributeValue::VALUE_STRING
             *              'interactive' => false | true
             *              'interactiveIfNull' => false | true
             *              'desc' => string
             *              'forceNull' => false | true
             */

            if (!empty($newControls)) {

                foreach ($newControls as $path => $controlValue) {

                    $resolver->setDefault($path, function (Options $options, $previousValue) use ($controlValue) {

                        if ($previousValue === null) {
                            if (is_array($controlValue)) {
                                return $controlValue;
                            } else {
                                return ['value' => $controlValue];
                            }
                        } else {
                            if (is_array($controlValue)) {
                                foreach ($controlValue as $key => $value) {
                                    $previousValue[$key] = $value;
                                }
                            } else {
                                $previousValue['value'] = $controlValue;
                            }
                        }

                        return $previousValue;

                    });

                }

            }

            $controls = $resolver->resolve();

            // Przypisywanie wartości z config scenariusza (yaml)
            if(ScenarioBuilder::$scenarioConfig !== null){
                foreach ($controls as $path => $controlValue) {
                    $controls[$path]['value'] = ScenarioBuilder::$scenarioConfig->getScenarioValue($this->currentStepId, $path, $controls[$path]['value']);
                }
            }

            // Jeżeli nie ma interaktywnego trybu, to zwrotka
            if (!ScenarioBuilder::isInteractiveMode()) {
                return $controls;
            }

            foreach ($controls as $control) {
                if (!empty($control['interactive']) || (!empty($control['interactiveIfNull']) && $control['value'] === NULL)) {
                    ScenarioBuilder::$ioSymfony->writeln('');
                    $desc = (empty($control['desc'])) ? 'Change VALUE of control' : $control['desc'];
                    $answer = $this->simpleAsk($desc . ' (' . $control['value'] . '): ', $control['value']);
                    $control['value'] = $answer;
                }
            }

            return $controls;

        } else {
            throw new \Exception("Error in getOptions(). Stage " . $this::PROCESS_PREFIX . " don't has controls for step '" . $suffixStepId . "'");
        }

    }

    private function simpleAsk($message, $default = null)
    {
        $question = new Question($message, $default);
        return ScenarioBuilder::$questionHelper->ask(ScenarioBuilder::$input, ScenarioBuilder::$output, $question);
    }

    /**
     * @param int $processInstanceId
     * @param $suffixStepId
     * @param array $newControls
     * @param int|null $variant
     * @param null $customControls
     * @param null $customStep
     * @return \int[]
     * @throws \Exception
     */
    public function runStep(int $processInstanceId, $suffixStepId, array $newControls = array(), $variant = 1, $customControls = null, $customStep = null)
    {

        if ($this::PROCESS_PREFIX === "0000") throw new \Exception("Please, change CONST PROCESS_PREFIX in your new Stage");

        $this->currentStepId = $this::PROCESS_PREFIX . "." . $suffixStepId;
        $this->logStep();
        $this->formControls($processInstanceId);

        $customControlsSuffix = ($customControls !== null) ? $customControls : $suffixStepId;
        if($customControlsSuffix === self::EMPTY_CONTROLS) {
            $controls = [];
        }
        else {
            $controls = $this->getControls($customControlsSuffix, $newControls);
        }

        $customStepSuffix = ($customStep !== null) ? $customStep : $suffixStepId;
        $this->runThisStep($customStepSuffix, $controls);

        $this->printOk();

        if ($variant !== null) {

            $error = ScenarioBuilder::$operationDashboardInterface->validateProcess($processInstanceId);

            if ($error['error'] > 0) {

                ScenarioBuilder::$ioSymfony->error('Error Next Step! ' . $this->currentStepId);

                if(!empty($error['controls'] )) {
                    foreach ($error['controls'] as $control) {

                        ScenarioBuilder::$ioSymfony->error('ControlId: ' . $control['control'] . " - " . $control['desc'] . '. attributeValueId(' . $control['attributeValueId'] . ')');

                    }
                }
                elseif(!empty($error['semirequireds'] )) {
                    foreach ($error['semirequireds'] as $control) {

                        ScenarioBuilder::$ioSymfony->error('ControlId: ' . $control . " - is semirequireds");

                    }
                }

                throw new \Exception("");
            }

            $processIds = $this->next($processInstanceId, $variant);

            return $processIds;
        } else {
            return [$processInstanceId];
        }

    }

    private function runThisStep($suffixStepId, array $controls)
    {

        $funName = 'step_' . $suffixStepId;

        if (method_exists($this, $funName)) {
            return call_user_func(array($this, $funName), $controls);
        } else {

            foreach ($controls as $path => $control) {

                // Przypisywanie wartości z config scenariusza (yaml)
                if(ScenarioBuilder::$scenarioConfig !== null){
                    $control['value'] = ScenarioBuilder::$scenarioConfig->getScenarioValue($this->currentStepId, $path, $control['value']);
                }

                if ($control['value'] !== null && empty($control['forceNull'])) {

                    switch ($control['type']) {
                        case AttributeValue::VALUE_INT: {
                            $this->setInt($path, $control['value']);
                            break;
                        }
                        case AttributeValue::VALUE_TEXT: {
                            $this->setText($path, $control['value']);
                            break;
                        }
                        case AttributeValue::VALUE_DECIMAL: {
                            $this->setDecimal($path, $control['value']);
                            break;
                        }
                        case AttributeValue::VALUE_DATE : {
                            $this->setDate($path, $control['value']);
                            break;
                        }
                        case AttributeValue::VALUE_STRING : {
                            $this->setString($path, $control['value']);
                            break;
                        }
                        default : {
                            throw new \Exception('Type Control "' . $control['type'] . '" is not allowed!');
                        }
                    }

                }
            }

        }

    }

    public function startFromStagePoint(StagePoint $stagePoint)
    {
        $this->groupProcessInstanceId = $stagePoint->getGroupProcessId();
    }

    public function onlyNext($processInstanceId, $variant = 1) {

        $this->logNext($processInstanceId, $variant);
        $ids = $this->processHandler->next($processInstanceId, $variant, ScenarioBuilder::USER_ID, ScenarioBuilder::ORIGINAL_USER_ID);
        $this->output->write('. ');
        $this->printOk($ids);
        return $ids;

    }

    /**
     * @param $processInstanceId
     * @param $variant
     * @return array
     * @throws \Exception
     */
    protected function next($processInstanceId, $variant = 1)
    {

        $this->logNext($processInstanceId, $variant);
        $ids = $this->processHandler->next($processInstanceId, $variant, ScenarioBuilder::USER_ID, ScenarioBuilder::ORIGINAL_USER_ID);
        $choicedVariant = $this->processHandler->choicedVariant($processInstanceId);
        $this->output->write('(<comment>' . $choicedVariant . '</comment>). ');
        $this->printOk($ids);
        return $ids;

    }

    protected function formControls($processInstanceId)
    {
        $this->processHandler->formControls($processInstanceId);
    }

    protected function setText($path, $value)
    {
        $this->setValue($path, $value, AttributeValue::VALUE_TEXT);
    }

    protected function setDecimal($path, $value)
    {
        $this->setValue($path, $value, AttributeValue::VALUE_DECIMAL);
    }

    protected function setDate($path, $value)
    {
        $this->setValue($path, $value, AttributeValue::VALUE_DATE);
    }

    protected function setString($path, $value)
    {
        $this->setValue($path, $value, AttributeValue::VALUE_STRING);
    }

    protected function setInt($path, $value)
    {
        $this->setValue($path, $value, AttributeValue::VALUE_INT);
    }

    protected function setValue($path, $value, $type, $stepId = null)
    {

        $stepId = ($stepId === null) ? $this->currentStepId : $stepId;

        $result = $this->processHandler->editAttributeValue(
            $path,
            $this->groupProcessInstanceId,
            $value,
            $type,
            $stepId,
            ScenarioBuilder::USER_ID, ScenarioBuilder::ORIGINAL_USER_ID
        );

        $result = $result[$path];

        if ($result['err']) {
            throw new \Exception("Error in SetAttributeValue [path: " . $path . ", value: " . $value . ", type: " . $type . ", stepId: " . $stepId . ", groupProcessId: " . $this->groupProcessInstanceId . "] ! \n Error Code: " . $result['err'] . ", Message:" . $result['message']);
        }

        return $result;

    }

    // CONSOLE

    protected function info($msg)
    {
        $this->output->writeln("<info>" . $msg . "</info>");
    }

    protected function console($msg)
    {
        $this->output->writeln($msg);
    }

    protected function comment($msg)
    {
        $this->output->writeln('<comment>' . $msg . '</comment>');
    }

    protected function logNext($id, $variant)
    {
        $this->output->write(' --> Next on process <comment>' . $id . '</comment> with variant <comment>' . $variant . '</comment>');
    }

    protected function logStart($step)
    {
        $this->output->write(' --> Start ' . $step . '. ');
    }

    protected function logStep()
    {
        $this->output->write(' --> Run step ' . $this->currentStepId . '. ');
    }

    protected function printOk($id = null)
    {

        if (is_array($id)) {
            $id = implode(',', $id);
        }

        $this->output->writeln("<info>" . $id . " ✔</info>");
    }

}