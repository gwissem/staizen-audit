<?php

namespace CaseBundle\Stages;

use CaseBundle\Service\ProcessHandler;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Output\ConsoleOutput;

class StagePoint
{

    /** @var ProcessHandler $processHandler */
    protected $processHandler;

    /** @var ConsoleOutput $output */
    protected $output;

    protected $description;

    /**
     * Stage constructor.
     * @param ProcessHandler $processHandler
     * @param $processInstanceIds
     */
    public function __construct(ProcessHandler $processHandler, $processInstanceIds)
    {
        $this->processHandler = $processHandler;
        $this->createLogger();
        $this->setDescription($processInstanceIds);
    }

    private function createLogger() {
        $this->output = new ConsoleOutput();
        $outputFormatter = new OutputFormatter(true);
        $this->output->setFormatter($outputFormatter);
    }

    protected function setDescription($processInstanceIds) {

        if(empty($processInstanceIds)) {
            throw new \Exception('ProcessInstanceIds are empty.');
        }

        if(is_array($processInstanceIds)) {
            $processInstanceIds = implode(',', $processInstanceIds);
        }

        $this->description = $this->processHandler->getProcessInstanceDescription(intval($processInstanceIds));

    }

    public function getId() {
        return $this->description['id'];
    }

    public function getRootId() {
        return $this->description['rootId'];
    }

    public function getName() {
        return $this->description['name'] . ' (' . $this->description['stepId'] . ')';
    }

    public function getGroupProcessId() {
        return $this->description['groupProcessId'];
    }

    public function getDescription() {
        return $this->description;
    }

    public function checkAttributeValue($path, $type, $value) {

        $attributeValue = $this->processHandler->getAttributeValue($path, $this->getGroupProcessId(), $type);

        if($attributeValue === null) {
            return ($value === null);
        }
        else {
            return ($attributeValue == $value);
        }

    }

    public function checkStepId($expectedStep) {
        return ($this->description['stepId'] == $expectedStep);
    }

    public function expectedStep($stepId) {

        if(is_array($stepId)) {
            if (!in_array($this->description['stepId'], $stepId)) {
                throw new \Exception(self::class . ' - Expected step ids: ' . print_r($stepId) . ', but get ' . $this->description['stepId'] . ' (' . $this->description['id'] . ')');
            }
        }
        else if($this->description['stepId'] != $stepId) {
            throw new \Exception(self::class . ' - Expected step id: ' . $stepId . ', but get ' . $this->description['stepId'] . ' (' . $this->description['id'] . ')');
        }
    }
}