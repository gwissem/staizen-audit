<?php

namespace CaseBundle\Stages;

use CaseBundle\Entity\AttributeValue;

class TowingOrFixing extends Stage
{

    const PROCESS_PREFIX = '1009';

    /**
     * @return int
     */
    public function createNewProcess()
    {
        $id = parent::createNew('1009.034');
        $this->groupProcessInstanceId = $id;

//        if($rootId !== null && is_numeric($rootId)) {
//            $this->processHandler->queryManager->executeProcedure("UPDATE process_instance SET root_id = ".$rootId." where id = ". intval($id));
//        }

        $this->printOk($id);
        return $id;
    }

    /**
     * @return array
     */
    public function controls_026()
    {

        return [
            '215' => [
                'value' => 0,  // Porsche Warszawa Okęcie / Sekundowa 1 / Warszawa
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Na termin czy jak najszybciej?'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_029()
    {

        return [
            '522' => [
                'value' => 2789835,  // Porsche Warszawa Okęcie / Sekundowa 1 / Warszawa
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Do którego Partnera serwisowego przetransportować Auto'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_030()
    {

        return [];

    }

    /**
     * @return array
     */
    public function controls_031()
    {

        return [
            '551' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy może być trudność z załadunkiem samochodu na platformę lawety?'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_034()
    {

        return [
//            '79' => [
//                'value' => 1,
//                'type' => AttributeValue::VALUE_INT,
//                'desc' => 'Proszę o podanie liczby osób podróżujących samochodem w momencie zdarzenia'
//            ],
//            '492' => [
//                'value' => 0,
//                'type' => AttributeValue::VALUE_INT,
//                'desc' => 'Czy wszyscy podróżujący pojazdem to osoby dorosłe? (dzieci)'
//            ],
//            '428' => [
//                'value' => 1,
//                'type' => AttributeValue::VALUE_INT,
//                'desc' => 'Czy jest konieczny transport osób z miejsca zdarzenia? Jeśli tak to ilu?'
//            ]
        ];

    }



}