<?php

namespace CaseBundle\Stages;

use CaseBundle\Entity\AttributeValue;

class NewCase extends Stage
{

    const PROCESS_PREFIX = '1011';

    /**
     * @return int
     * @throws \Exception
     */
    public function createNewProcess()
    {
        $id = parent::createNew('1011.006');
        $this->groupProcessInstanceId = $id;
        $this->printOk($id);
        return $id;
    }

    /**
     * @return array
     */
    public function controls_002()
    {

        return [
            '74,72' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'NR',
                'interactiveIfNull' => true
            ],
            '74,71' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'VIN',
                'interactiveIfNull' => true
            ],
            '74,561' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_DATE,
                'desc' => 'Data pierwszej rejestracji (DD-MM-RRRR) - Okryty',
                'interactiveIfNull' => true
            ],
            '74,233' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_DATE,
                'desc' => 'Data pierwszej rejestracji (DD-MM-RRRR)',
                'interactiveIfNull' => true
            ],
            '74,75' => [
                'value' => 10000,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Aktualny przebieg',
                'interactiveIfNull' => true
            ],
            '422' => [
                'value' => 1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Poprawny VIN'
            ],
        ];

    }


    /**
     * @return array
     */
    public function controls_003()
    {

        return [
            '496' => [
                'value' => 1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Powód niezgodności VIN i NR rej'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_004()
    {

        return [
            '74,76' => [
                'value' => NULL,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'DMC'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_006()
    {

        return [
            '424' => [
                'value' => 1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy kierował Pan pojazdem w momencie zdarzenia?'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_007()
    {

        return [
            '269' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy wyraża Pan zgodę na udostępnianie Pana danych osobowych...'
            ]
        ];

    }


    /**
     * @return array
     */
    public function controls_008()
    {

        return [
            '101,85,92' => [
                'value' => '21.042629',
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Długość geograficzna'
            ],
            '101,85,93' => [
                'value' => '52.239334',
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Szerokość geograficzna'
            ],
            '101,85,86' => [
                'value' => 'Polska',
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Kraj'
            ],
            "101,85,87" => [
                'value' => null,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Miasto'
            ],
            "101,85,94" => [
                'value' => null,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Ulica'
            ],
            "101,85,95" => [
                'value' => null,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Numer ulicy'
            ],
            "101,85,88" => [
                'value' => null,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Województwo'
            ],
            "101,85,91" => [
                'value' => null,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Powiat'
            ],
            "101,85,90" => [
                'value' => null,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Gmina'
            ],
            "101,85,89" => [
                'value' => null,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Zip-code'
            ],
            "101,85,509" => [
                'value' => null,
                'type' => AttributeValue::VALUE_STRING,
                'desc' => 'Droga'
            ],
            "101,85,541" => [
                'value' => null,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy miejscowość?'
            ],

        ];

    }


    /**
     * @return array
     */
    public function controls_010()
    {

        return [
//            '269' => [
//                'value' => 0,
//                'type' => AttributeValue::VALUE_INT,
//                'desc' => 'Czy wyraża Pan zgodę na udostępnianie Pana danych osobowych...'
//            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_015()
    {

        // Domyślnie w sprawie awarii

        return [
            '419' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy kontaktuje się Pan w sprawie wypadku?'
            ],
            '513' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy auto znajduje się na autostradzie lub drodze szybkiego ruchu?'
            ],
            '491' => [
                'value' => 2,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy kontaktuje się Pan w sprawie wypadku? (Rodzaj zdarzenia)'
            ],
            '101,104' => [
                'value' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
                'type' => AttributeValue::VALUE_DATE,
                'desc' => 'Data zdarzenia'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_017()
    {

        return [

        ];

    }

    /**
     * @return array
     */
    public function controls_018()
    {

        return [
            '717' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Naprawa HPST nie udana.'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_037()
    {

        return [
            '79' => [
                'value' => 1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Proszę o podanie liczby osób podróżujących samochodem w momencie zdarzenia'
            ],
            '492' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy wszyscy podróżujący pojazdem to osoby dorosłe? (dzieci)'
            ],
            '428' => [
                'value' => 1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy jest konieczny transport osób z miejsca zdarzenia? Jeśli tak to ilu?'
            ]
        ];

    }


    /**
     * @return array
     */
    public function controls_052()
    {

        return [
            '878' => [
                'value' => 0,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy chcesz zmienić platformę na VW ...'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_040()
    {

        return [
            '187' => [
                'value' => 1,
                'type' => AttributeValue::VALUE_INT,
                'desc' => 'Czy po naszej poprzedniej interwencji usterka została usunięta u autoryzowanego Partnera Serwisowego?'
            ]
        ];

    }

    /**
     * @return array
     */
    public function controls_043()
    {

        return [];

    }


}