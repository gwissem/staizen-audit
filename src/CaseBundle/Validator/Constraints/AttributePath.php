<?php

namespace CaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class AttributePath extends Constraint
{
    public $message = 'The string should have right format.';
}