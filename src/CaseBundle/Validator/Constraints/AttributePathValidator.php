<?php

namespace CaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AttributePathValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (preg_match('/({@|@})/', $value)) {
            $wholePattern = preg_match_all('/{@[0-9]+(,[0-9]+)*@}/', $value);
            $openSign = preg_match_all('/{@/', $value);
            $closeSing = preg_match_all('/@}/', $value);
            if (!($wholePattern == $openSign && $wholePattern == $closeSing && $closeSing == $openSign)) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ string }}', $value)
                    ->addViolation();
            }
        }
    }
}