<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\PlatformPhoneRepository")
 * @ORM\Table(name="platform_phone")
 */
class PlatformPhone
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="welcome", type="text", nullable=true)
     */
    private $welcome;

    /**
     * @ORM\Column(type="integer", length=30)
     */
    protected $number;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Platform", inversedBy="phoneNumbers", cascade={"remove"})
     * @ORM\JoinColumn(name="platform_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $platform;

    /**
     * @var string
     *
     * @ORM\Column(name="ringtone", type="string", nullable=true, length=50)
     */
    private $ringtone;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     * @return PlatformPhone
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return Platform
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     * @return PlatformPhone
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWelcome()
    {
        return $this->welcome;
    }

    /**
     * @param mixed $welcome
     * @return PlatformPhone
     */
    public function setWelcome($welcome)
    {
        $this->welcome = $welcome;

        return $this;
    }

    /**
     * @return string
     */
    public function getRingtone()
    {
        return $this->ringtone;
    }

    /**
     * @param string $ringtone
     */
    public function setRingtone($ringtone)
    {
        $this->ringtone = $ringtone;
    }


}