<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class StepTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @Assert\NotBlank()
     * @var string
     * @ORM\Column(type="string", length=500)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $info;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $forwardVariantInfo;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return StepTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return StepTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return StepTranslation
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return string
     */
    public function getForwardVariantInfo()
    {
        return $this->forwardVariantInfo;
    }

    /**
     * @param string $forwardVariantInfo
     * @return StepTranslation
     */
    public function setForwardVariantInfo($forwardVariantInfo)
    {
        $this->forwardVariantInfo = $forwardVariantInfo;
        return $this;
    }




}
