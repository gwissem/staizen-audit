<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @UniqueEntity("processInstance")
 */
class ProcessInstanceProperty
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="CaseBundle\Entity\ProcessInstance", inversedBy="property")
     * @ORM\JoinColumn(name="process_instance_id", referencedColumnName="id")
     */
    private $processInstance;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $urgent = false;

    /**
     * @ORM\Column(type="smallint", name="priority", nullable=true)
     */
    private $priority;

    /**
     * @ORM\Column(type="datetime", name="priority_changed_at", nullable=true)
     */
    private $priorityChangedAt;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Platform")
     * @ORM\JoinColumn(name="platform_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $platform;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $step;

    /**
     * @return mixed
     */
    public function getProcessInstance()
    {
        return $this->processInstance;
    }

    /**
     * @param mixed $processInstance
     * @return ProcessInstanceTime
     */
    public function setProcessInstance($processInstance)
    {
        $this->processInstance = $processInstance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrgent()
    {
        return $this->urgent;
    }

    /**
     * @param mixed $urgent
     * @return ProcessInstanceProperty
     */
    public function setUrgent($urgent)
    {
        $this->urgent = $urgent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     * @return ProcessInstanceProperty
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriorityChangedAt()
    {
        return $this->priorityChangedAt;
    }

    /**
     * @param mixed $priorityChangedAt
     * @return ProcessInstanceProperty
     */
    public function setPriorityChangedAt($priorityChangedAt)
    {
        if(!$priorityChangedAt){
            $priorityChangedAt = new \DateTime();
        }
        $this->priorityChangedAt = $priorityChangedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     * @return ProcessInstanceProperty
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     * @return ProcessInstanceProperty
     */
    public function setStep($step)
    {
        if(!$step){
            $step = $this->processInstance->step;
        }
        $this->step = $step;
        return $this;
    }

}
