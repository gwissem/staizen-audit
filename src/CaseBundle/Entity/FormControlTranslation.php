<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table(name="form_control_translation", uniqueConstraints={@ORM\UniqueConstraint(name="unique_label_locale", columns={"translatable_id", "locale"})})
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\FormControlTranslationRepository")
  * @UniqueEntity(fields={"translatable", "locale"})
 */
class FormControlTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @Assert\NotBlank()
     * @var string
     * @ORM\Column(name="label", type="string", length=4000, nullable=true)
     */
    private $label;

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return FormControlTranslation
     */
    public function setLabel(string $label): FormControlTranslation
    {
        $this->label = $label;
        $this->getTranslatable()->setParsedLabel($label);
        return $this;
    }
}
