<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\PostponeProcessInstanceRepository")
 * @ORM\Table()
 */
class PostponeProcessInstance
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="process_instance_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $processInstance;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $reason;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    static function getReasons() {

        return [
            'Umówione na później' => 'Umówione na później',
            'Brak kontaktu' => 'Brak kontaktu',
            'Brak możliwości realizacji' => 'Brak możliwości realizacji',
            'Oczekiwanie na informację' => 'Oczekiwanie na informację',
            'Pomyłkowe otwarcie' => 'Pomyłkowe otwarcie',
            'Podgląd zadania' => 'Podgląd zadania',
            'Inne' => 'Inne'
        ];

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getProcessInstance()
    {
        return $this->processInstance;
    }

    /**
     * @param mixed $processInstance
     */
    public function setProcessInstance($processInstance)
    {
        $this->processInstance = $processInstance;
    }

}
