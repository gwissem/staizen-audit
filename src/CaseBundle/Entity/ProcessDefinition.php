<?php

namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use UserBundle\Entity\User;
use CaseBundle\Validator\Constraints as CaseAssert;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ProcessDefinitionRepository")
 * @ORM\Table()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class ProcessDefinition
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    const TYPE_STANDARD = 1;
    const TYPE_QUIZ = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\Step", mappedBy="processDefinition", orphanRemoval=true)
     */
    private $steps;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\ProcessFlow", mappedBy="processDefinition", orphanRemoval=true)
     */
    private $flows;

    /**
     * @var string
     * @CaseAssert\AttributePath
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stepNameSchema;

    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true)
     */
    private $type = 1;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var Step $cancelStep
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $cancelStep;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->steps = new \Doctrine\Common\Collections\ArrayCollection();
        $this->flows = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->translate()->getName();
    }

    /**
     * @return ArrayCollection
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param mixed $steps
     * @return ProcessDefinition
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;

        return $this;
    }

    public function getStepsForSelect() {
        $steps = [];

        /** @var Step $step */
        foreach ($this->steps as $step) {
            if ($step->getIsTechnical()) continue;

            $steps[] = [
                'id' => $step->getId(),
                'text' => '(' . $step->getId() . ') ' . $step->getName()
            ];
        }

        return $steps;
    }

    /**
     * @return mixed
     */
    public function getFlows()
    {
        return $this->flows;
    }

    /**
     * @param mixed $flows
     * @return ProcessDefinition
     */
    public function setFlows($flows)
    {
        $this->flows = $flows;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return ProcessDefinition
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     * @return ProcessDefinition
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Add step
     *
     * @param \CaseBundle\Entity\Step $step
     *
     * @return ProcessDefinition
     */
    public function addStep(\CaseBundle\Entity\Step $step)
    {
        $this->steps[] = $step;

        return $this;
    }

    /**
     * Remove step
     *
     * @param \CaseBundle\Entity\Step $step
     */
    public function removeStep(\CaseBundle\Entity\Step $step)
    {
        $this->steps->removeElement($step);
    }

    /**
     * @return mixed
     */
    public function getStepNameSchema()
    {
        return $this->stepNameSchema;
    }

    /**
     * @param mixed $stepNameSchema
     */
    public function setStepNameSchema($stepNameSchema)
    {
        $this->stepNameSchema = $stepNameSchema;
    }

    /**
     * Add flow
     *
     * @param \CaseBundle\Entity\ProcessFlow $flow
     *
     * @return ProcessDefinition
     */
    public function addFlow(\CaseBundle\Entity\ProcessFlow $flow)
    {
        $this->flows[] = $flow;

        return $this;
    }

    /**
     * Remove flow
     *
     * @param \CaseBundle\Entity\ProcessFlow $flow
     */
    public function removeFlow(\CaseBundle\Entity\ProcessFlow $flow)
    {
        $this->flows->removeElement($flow);
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return ProcessDefinition
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getInfo(){
        return $this->translate()->getInfo();
    }

    public function getAdditionalInfo(){
        return $this->translate()->getAdditionalInfo();
    }

    public function getFullInfo(){
        return $this->translate()->getFullInfo();
    }

    public function getCancelDescription(){
        return $this->translate()->getCancelDescription();
    }

    public function getViewerInfo(){
        return $this->translate()->getViewerInfo();
    }

    /**
     * @return Step
     */
    public function getCancelStep()
    {
        return $this->cancelStep;
    }

    /**
     * @param Step $cancelStep
     * @return ProcessDefinition
     */
    public function setCancelStep($cancelStep)
    {
        $this->cancelStep = $cancelStep;
        return $this;
    }




}
