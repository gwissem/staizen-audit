<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ProcessFlowRepository")
 * @ORM\Table()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class ProcessFlow
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $isPrimaryPath = true;

    /**
     * @ORM\Column(type="integer")
     */
    protected $variant = 1;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id")
     */
    private $step;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="next_step_id", referencedColumnName="id")
     */
    private $nextStep;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessDefinition", inversedBy="flows")
     * @ORM\JoinColumn(name="process_definition_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $processDefinition;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $graphPoints;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $curviness;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Program", inversedBy="flows")
     * @ORM\JoinTable(name="process_flow_program",
     *      joinColumns={@ORM\JoinColumn(name="process_flow_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="program_id", referencedColumnName="id")})
     */
    private $programs;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     * @return ProcessFlow
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNextStep()
    {
        return $this->nextStep;
    }

    /**
     * @param mixed $nextStep
     * @return ProcessFlow
     */
    public function setNextStep($nextStep)
    {
        $this->nextStep = $nextStep;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPrimaryPath()
    {
        return $this->isPrimaryPath;
    }

    /**
     * @param mixed $isPrimaryPath
     * @return ProcessFlow
     */
    public function setIsPrimaryPath($isPrimaryPath)
    {
        $this->isPrimaryPath = $isPrimaryPath;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGraphPoints()
    {
        return $this->graphPoints;
    }

    /**
     * @param mixed $graphPoints
     * @return ProcessFlow
     */
    public function setGraphPoints($graphPoints)
    {
        $this->graphPoints = $graphPoints;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurviness()
    {
        return $this->curviness;
    }

    /**
     * @param mixed $curviness
     * @return ProcessFlow
     */
    public function setCurviness($curviness)
    {
        $this->curviness = $curviness;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProcessDefinition()
    {
        return $this->processDefinition;
    }

    /**
     * @param mixed $processDefinition
     * @return ProcessFlow
     */
    public function setProcessDefinition($processDefinition)
    {
        $this->processDefinition = $processDefinition;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return ProcessFlow
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     * @return ProcessFlow
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getVariantDescription($truncateLimit = 50)
    {
        if ($this->processDefinition->getType() != 2) {
            return $this->variant;
        }
        if (!empty($this->getDescription())) {
            if (strlen($this->getDescription()) > $truncateLimit) {
                return $this->getVariant() . ' (' . mb_substr($this->getDescription(), 0, $truncateLimit) . '...)';
            }
            return $this->getVariant() . ' (' . $this->getDescription() . ')';
        }
        return $this->getVariant();
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->translate()->getDescription();
    }

    /**
     * @return mixed
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param mixed $variant
     * @return ProcessFlow
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return ProcessFlow
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * @param mixed $programs
     * @return ProcessFlow
     */
    public function setPrograms($programs)
    {
        $this->programs = $programs;
        return $this;
    }


}
