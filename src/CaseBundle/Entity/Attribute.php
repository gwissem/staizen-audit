<?php

namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\AttributeRepository")
 * @ORM\Table(name="attribute")
 */
class Attribute
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable,
        FormTemplateMethodsTrait;

    const INPUT_TYPE_TEXT = 'text';
    const INPUT_TYPE_TEXTAREA = 'textarea';
    const INPUT_TYPE_SELECT = 'select';
    const INPUT_TYPE_MULTISELECT = 'multiselect';
    const INPUT_TYPE_RADIO = 'radio';
    const INPUT_TYPE_CHECKBOX = 'checkbox';
    const INPUT_TYPE_NUMBER = 'number';
    const INPUT_TYPE_YESNO = 'yes-no';
    const INPUT_TYPE_DATETIME = 'datetime';
    const INPUT_TYPE_DATE = 'date';
    const INPUT_TYPE_UPLOAD = 'upload_file';
    const INPUT_TYPE_EMAIL = 'email';

    static $arrayOfTypes = [
        1 => 'string',
        2 => 'integer',
        3 => 'datetime',
        4 => 'text',
        5 => 'decimal',
        6 => 'boolean',
        7 => 'map',
        8 => 'company',
        9 => 'multi'
    ];

    static $arrayOfValueType = [
        'string' => 'string',
        'boolean' => 'int',
        'company' => 'int',
        'integer' => 'int',
        'datetime' => 'date',
        'text' => 'text',
        'decimal' => 'decimal'
    ];

    public $attributeValue;
    public $attributeMultiValues = [];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="integer", options={"default" = 1})
     */
    protected $type = 1;
    /**
     * @ORM\Column(type="boolean", options={"default" = 0}, nullable=true)
     */
    protected $isMulti = false;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $regex;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $validationRule;
    /**
     * @ORM\Column(type="string", options={"default" = "text"}, nullable=true)
     */
    protected $inputType = self::INPUT_TYPE_TEXT;
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=true})
     */
    protected $active = true;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $query;
    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Attribute", inversedBy="parents")
     * @ORM\JoinTable(name="attribute_attributes",
     *      joinColumns={@ORM\JoinColumn(name="parent_attribute_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="attribute_id", referencedColumnName="id")}
     * )
     */
    protected $children;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $defaultValue;
    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\FormTemplate")
     * @ORM\JoinTable(name="attribute_form_template",
     *      joinColumns={@ORM\JoinColumn(name="attribute_id",  referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="form_template_id", referencedColumnName="id")}
     * )
     * @var ArrayCollection $formTemplates ;
     */
    protected $formTemplates;
    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\AttributeCondition", mappedBy="attribute")
     */
    private $conditions;
    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\StepAttributeDefinition", mappedBy="attribute")
     */
    private $stepAttributeDefinitions;
    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Attribute", mappedBy="children")
     */
    private $parents;
    /**
     * @var User $createdBy
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdByOriginal;
    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;
    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;
    /**
     * @var User $createdBy
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedByOriginal;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->parents = new ArrayCollection();
        $this->conditions = new ArrayCollection();
        $this->formTemplates = new ArrayCollection();
        $this->setEmptyAttributeValue();
    }

    public function setEmptyAttributeValue()
    {
        $this->attributeValue = [
            'id' => "",
            'path' => "",
            'value' => null,
            'type' => "",
            'groupProcessId' => "",
            'stepId' => "",
            'parentAttributeValueId' => '',
            'isAuto' => false
        ];
    }

    public function setMultiValues($data)
    {


    }

    public function getValueType()
    {
        switch ($this->getNameType()) {

            case 'string': {
                return 'value_string';
            }
            case 'boolean' :
            case 'company' :
            case 'integer' : {
                return 'value_int';
            }
            case 'datetime' : {
                return 'value_date';
            }
            case 'text' : {
                return 'value_text';
            }
            case 'decimal' : {
                return 'value_decimal';
            }
        }

        return 'value_string';
    }

    public function getNameType()
    {

        if (isset(self::$arrayOfTypes[$this->type])) return self::$arrayOfTypes[$this->type];

        return '';
    }

    public function getAttrValue($name)
    {
        if ($this->attributeValue) {
            if (key_exists($name, $this->attributeValue)) {
                return $this->attributeValue[$name];
            }
        }

        return '';
    }

    public function setAttrValue($name, $val)
    {
        $this->attributeValue[$name] = $val;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Attribute
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getNameValueType()
    {
        $type = $this->getNameType();
        if (isset(self::$arrayOfValueType[$type])) return self::$arrayOfValueType[$type];

        return $type;
//        throw new \Exception('Nie obsługiwany typ:' . $type);

    }

    /**
     * @return mixed
     */
    public function getRegex()
    {
        return $this->regex;
    }

    /**
     * @param mixed $regex
     * @return Attribute
     */
    public function setRegex($regex)
    {
        $this->regex = $regex;

        return $this;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param mixed $attributes
     * @return Attribute
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Add child
     *
     * @param \CaseBundle\Entity\Attribute $child
     *
     * @return Attribute
     */
    public function addChild(\CaseBundle\Entity\Attribute $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \CaseBundle\Entity\Attribute $child
     */
    public function removeChild(\CaseBundle\Entity\Attribute $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add parent
     *
     * @param \CaseBundle\Entity\Attribute $parent
     *
     * @return Attribute
     */
    public function addParent(\CaseBundle\Entity\Attribute $parent)
    {
        $this->parents[] = $parent;

        return $this;
    }

    /**
     * Remove parent
     *
     * @param \CaseBundle\Entity\Attribute $parent
     */
    public function removeParent(\CaseBundle\Entity\Attribute $parent)
    {
        $this->parents->removeElement($parent);
    }

    /**
     * Get parents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     * @return Attribute
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return Attribute
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->translate()->getName();
    }

    public function getDefinitionName()
    {
        return '('.$this->id.') '.$this->translate()->getName();
    }

    /**
     * @return User
     */
    public function getCreatedByOriginal()
    {
        return $this->createdByOriginal;
    }

    /**
     * @param User $createdByOriginal
     * @return Attribute
     */
    public function setCreatedByOriginal($createdByOriginal)
    {
        $this->createdByOriginal = $createdByOriginal;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return Attribute
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     * @return Attribute
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedByOriginal()
    {
        return $this->updatedByOriginal;
    }

    /**
     * @param User $updatedByOriginal
     * @return Attribute
     */
    public function setUpdatedByOriginal($updatedByOriginal)
    {
        $this->updatedByOriginal = $updatedByOriginal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInputType()
    {
        return $this->inputType;
    }

    /**
     * @param mixed $inputType
     * @return Attribute
     */
    public function setInputType($inputType)
    {
        $this->inputType = $inputType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param mixed $conditions
     * @return Attribute
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param mixed $defaultValue
     * @return StepAttributeDefinition
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Add condition
     *
     * @param \CaseBundle\Entity\AttributeCondition $condition
     *
     * @return Attribute
     */
    public function addCondition(\CaseBundle\Entity\AttributeCondition $condition)
    {
        $this->conditions[] = $condition;

        return $this;
    }

    /**
     * Remove condition
     *
     * @param \CaseBundle\Entity\AttributeCondition $condition
     */
    public function removeCondition(\CaseBundle\Entity\AttributeCondition $condition)
    {
        $this->conditions->removeElement($condition);
    }

    /**
     * Add stepAttributeDefinition
     *
     * @param \CaseBundle\Entity\StepAttributeDefinition $stepAttributeDefinition
     *
     * @return Attribute
     */
    public function addStepAttributeDefinition(\CaseBundle\Entity\StepAttributeDefinition $stepAttributeDefinition)
    {
        $this->stepAttributeDefinitions[] = $stepAttributeDefinition;

        return $this;
    }

    /**
     * Remove stepAttributeDefinition
     *
     * @param \CaseBundle\Entity\StepAttributeDefinition $stepAttributeDefinition
     */
    public function removeStepAttributeDefinition(\CaseBundle\Entity\StepAttributeDefinition $stepAttributeDefinition)
    {
        $this->stepAttributeDefinitions->removeElement($stepAttributeDefinition);
    }

    /**
     * Get stepAttributeDefinitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStepAttributeDefinitions()
    {
        return $this->stepAttributeDefinitions;
    }

    /**
     * Get stepAttributeDefinitions
     *
     * @return array
     */
    public function getAttributeInStepDefinitions()
    {
        $attributeRootStepsArray = [];
        $attributeSteps = [];
        if ($this->getId()) {
            foreach ($this->getRoots($this) as $root) {
                foreach ($root->stepAttributeDefinitions as $stepAttributeDefinition) {
                    $attributeRootStepsArray[$stepAttributeDefinition->getAttribute()->getId()] = $stepAttributeDefinition;
                }
            }

            foreach ($this->stepAttributeDefinitions as $stepAttributeDefinition) {
                $attributeSteps[$stepAttributeDefinition->getAttribute()->getId()] = $stepAttributeDefinition;
            }
        }

        return [
            'attributeSteps' => $attributeSteps,
            'attributeRootSteps' => $attributeRootStepsArray,
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    protected function getRoots($attribute, $returnArray = [])
    {
        $parents = $attribute->getParents();
        if ($parents->count() > 0) {
            foreach ($parents as $parent) {
                $returnArray = $this->getRoots($parent, $returnArray);
            }
        } else {
            $returnArray[] = $attribute;
        }

        return $returnArray;
    }

    /**
     * @return mixed
     */
    public function getIsMulti()
    {
        return $this->isMulti;
    }

    /**
     * @param mixed $isMulti
     * @return Attribute
     */
    public function setIsMulti($isMulti)
    {
        $this->isMulti = $isMulti;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    /**
     * @param mixed $attributeValue
     * @return Attribute
     */
    public function setAttributeValue($attributeValue)
    {
        $this->attributeValue = $attributeValue;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributeMultiValues(): array
    {
        return $this->attributeMultiValues;
    }

    /**
     * @param array $attributeMultiValues
     * @return Attribute
     */
    public function setAttributeMultiValues(array $attributeMultiValues): Attribute
    {
        $this->attributeMultiValues = $attributeMultiValues;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidationRule()
    {
        return $this->validationRule;
    }

    /**
     * @param mixed $validationRule
     * @return Attribute
     */
    public function setValidationRule($validationRule)
    {
        $this->validationRule = $validationRule;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getFormTemplates(): ArrayCollection
    {
        return $this->formTemplates;
    }

    /**
     * @param ArrayCollection $formTemplates
     * @return Attribute
     */
    public function setFormTemplates(ArrayCollection $formTemplates): Attribute
    {
        $this->formTemplates = $formTemplates;
        return $this;
    }

    /**
     * @param $path
     * @return mixed
     */
    public static function getAttributeByPath($path) {

        if (strpos($path, ',') !== false) {
            $arr = explode(',', $path);
            return array_pop($arr);
        }

        return $path;

    }

}
