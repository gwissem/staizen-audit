<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * ServiceStatusUpdate
 *
 * @ORM\Table(name="service_status_update")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ServiceStatusUpdateRepository")
 */
class ServiceStatusUpdate
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ServiceDefinition
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ServiceDefinition")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $serviceId;

    /**
     * @var Step
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $stepId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ServiceDefinition
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @param ServiceDefinition $serviceId
     */
    public function setServiceId(ServiceDefinition $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return Step
     */
    public function getStepId()
    {
        return $this->stepId;
    }

    /**
     * @param Step $stepId
     */
    public function setStepId(Step $stepId)
    {
        $this->stepId = $stepId;
    }
}

