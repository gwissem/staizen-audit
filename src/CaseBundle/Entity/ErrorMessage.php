<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ErrorMessageRepository")
 * @ORM\Table(name="error_message", uniqueConstraints={@ORM\UniqueConstraint(name="step_error", columns={"step_id", "error_id"})})
 */
class ErrorMessage
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $step;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $errorId;

    /**
     * @ORM\Column(type="string", length=1000, name="message")
     */
    private $message;

    /**
     * @ORM\Column(type="integer", name="variant", nullable=true)
     */
    private $variant;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     * @return ErrorMessage
     */
    public function setStep($step)
    {
        $this->step = $step;
        return $this;
    }

    /**
     * @return int
     */
    public function getErrorId()
    {
        return $this->errorId;
    }

    /**
     * @param int $errorId
     * @return ErrorMessage
     */
    public function setErrorId($errorId)
    {
        $this->errorId = $errorId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param mixed $variant
     * @return ErrorMessage
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }




}
