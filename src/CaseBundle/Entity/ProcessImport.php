<?php

namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use UserBundle\Entity\User;
use CaseBundle\Validator\Constraints as CaseAssert;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ProcessImportRepository")
 * @ORM\Table()
 */
class ProcessImport
{
    use ORMBehaviors\Timestampable\Timestampable;

    const TYPE_STANDARD = 1;
    const TYPE_QUIZ = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessDefinition")
     * @ORM\JoinColumn(name="process_definition_id", referencedColumnName="id")
     */
    private $processDefinition;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true)
     */
    private $active = 1;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProcessDefinition()
    {
        return $this->processDefinition;
    }

    /**
     * @param mixed $processDefinition
     * @return ProcessImport
     */
    public function setProcessDefinition($processDefinition)
    {
        $this->processDefinition = $processDefinition;
        return $this;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProcessImport
     */
    public function setName(string $name): ProcessImport
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getActive(): string
    {
        return $this->active;
    }

    /**
     * @param string $active
     * @return ProcessImport
     */
    public function setActive(string $active): ProcessImport
    {
        $this->active = $active;
        return $this;
    }


}
