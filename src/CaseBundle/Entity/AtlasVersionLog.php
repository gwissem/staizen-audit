<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\AtlasVersionLogRepository")
 * @ORM\Table(name="atlas_version_log")
 */
class AtlasVersionLog
{
    use ORMBehaviors\Timestampable\Timestampable;

    const VERSION_TYPE_BUG = 'fix_bug';
    const VERSION_TYPE_FUTURES = 'futures';
    const VERSION_TYPE_UPGRADE = 'upgrade';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="description", type="text")
     */
    protected $description = "";

    /**
     * @var string
     * @ORM\Column(name="type", type="string", length=64, nullable=true)
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(name="version", type="string", length=16, nullable=true)
     */
    protected $version;

    /**
     * @var \DateTime
     * @ORM\Column(name="version_date", type="datetime", nullable=true)
     */
    protected $versionDate;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    static function getTypes() {
        return [
            self::VERSION_TYPE_BUG => 'Naprawa błędu',
            self::VERSION_TYPE_FUTURES => "Nowość",
            self::VERSION_TYPE_UPGRADE => "Usprawnienie"
        ];
    }

    public function getIconOfType() {

        switch ($this->getType()) {
            case self::VERSION_TYPE_BUG: {
                return "fa-bug";
                break;
            }
            case self::VERSION_TYPE_FUTURES: {
                return "fa-star";
                break;
            }
            case self::VERSION_TYPE_UPGRADE: {
                return 'fa-level-up';
                break;
            }
            default: {
                return "fa-star";
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return \DateTime
     */
    public function getVersionDate()
    {
        return $this->versionDate;
    }

    /**
     * @param \DateTime $versionDate
     */
    public function setVersionDate(\DateTime $versionDate)
    {
        $this->versionDate = $versionDate;
    }

    public function parseVersionToNumber() {
        $arr = explode('.', $this->getVersion());
        return ($arr[0] * 100000 + $arr[1] * 1000 + $arr[2]);
    }

    public function parseNumberToVersion($number) {

        $first = (intval($number / 100000));
        $second = intval( ($number % 100000) / 1000);
        $third = intval( ($number % 1000));

        return implode('.', [$first, $second, $third]);

    }

    public function upgradeVersion($up = 1) {

        $parsedVersion = $this->parseVersionToNumber();
        $parsedVersion = $parsedVersion + $up;
        $this->setVersion($this->parseNumberToVersion($parsedVersion));

    }

}
