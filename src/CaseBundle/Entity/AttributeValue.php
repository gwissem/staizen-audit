<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\AttributeValueRepository")
 * @ORM\Table(name="attribute_value", indexes={@ORM\Index(name="attribute_path_idx", columns={"attribute_path"}) })
 */
class AttributeValue
{
    use ORMBehaviors\Timestampable\Timestampable;

    const VALUE_STRING = 'string';
    const VALUE_INT = 'int';
    const VALUE_DECIMAL = 'decimal';
    const VALUE_TEXT = 'text';
    const VALUE_DATE = 'date';
    const VALUE_MULTI = 'multi';

    const VALUE_STRING_FULL = 'valueString';
    const VALUE_INT_FULL = 'valueInt';
    const VALUE_DECIMAL_FULL = 'valueDecimal';
    const VALUE_TEXT_FULL = 'valueText';
    const VALUE_DATE_FULL = 'valueDate';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $valueInt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $valueString;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $valueText;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=6, nullable=true)
     */
    protected $valueDecimal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $valueDate;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Attribute")
     * @ORM\JoinColumn(name="value_attribute_id", referencedColumnName="id")
     */
    protected $valueAttribute;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Attribute")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     */
    protected $attribute;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $attributePath;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\AttributeValue")
     * @ORM\JoinColumn(name="parent_attribute_value_id", referencedColumnName="id")
     */
    protected $parentAttributeValue;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance", inversedBy="attributeValues")
     * @ORM\JoinColumn(name="group_process_instance_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $groupProcessInstance;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="root_process_instance_id", referencedColumnName="id")
     */
    private $rootProcess;

    /**
     * @var User $createdBy
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdByOriginal;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;
    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @var User $createdBy
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedByOriginal;

    /**
     * @var $omitSemirequiredReason
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $omitSemirequiredReason;

    /**
     * @var $position
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $sparxUpdatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getValueInt()
    {
        return $this->valueInt;
    }

    /**
     * @param mixed $valueInt
     * @return AttributeValue
     */
    public function setValueInt($valueInt)
    {
        $this->valueInt = $valueInt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueString()
    {
        return $this->valueString;
    }

    /**
     * @param mixed $valueString
     * @return AttributeValue
     */
    public function setValueString($valueString)
    {
        $this->valueString = $valueString;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueText()
    {
        return $this->valueText;
    }

    /**
     * @param mixed $valueText
     * @return AttributeValue
     */
    public function setValueText($valueText)
    {
        $this->valueText = $valueText;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueDecimal()
    {
        return $this->valueDecimal;
    }

    /**
     * @param mixed $valueDecimal
     * @return AttributeValue
     */
    public function setValueDecimal($valueDecimal)
    {
        $this->valueDecimal = $valueDecimal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueDate()
    {
        return $this->valueDate;
    }

    /**
     * @param mixed $valueDate
     * @return AttributeValue
     */
    public function setValueDate($valueDate)
    {
        $this->valueDate = $valueDate;

        return $this;
    }

    /**
     * @return Attribute | null
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param mixed $attribute
     * @return AttributeValue
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueAttribute()
    {
        return $this->valueAttribute;
    }

    /**
     * @param mixed $valueAttribute
     * @return AttributeValue
     */
    public function setValueAttribute($valueAttribute)
    {
        $this->valueAttribute = $valueAttribute;

        return $this;
    }

    /**
     * @return ProcessInstance | null
     */
    public function getGroupProcessInstance()
    {
        return $this->groupProcessInstance;
    }

    /**
     * @param mixed $groupProcessInstance
     * @return AttributeValue
     */
    public function setGroupProcessInstance($groupProcessInstance)
    {
        $this->groupProcessInstance = $groupProcessInstance;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedByOriginal()
    {
        return $this->createdByOriginal;
    }

    /**
     * @param User $createdByOriginal
     * @return AttributeValue
     */
    public function setCreatedByOriginal($createdByOriginal)
    {
        $this->createdByOriginal = $createdByOriginal;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return AttributeValue
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     * @return AttributeValue
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedByOriginal()
    {
        return $this->updatedByOriginal;
    }

    /**
     * @param User $updatedByOriginal
     * @return AttributeValue
     */
    public function setUpdatedByOriginal($updatedByOriginal)
    {
        $this->updatedByOriginal = $updatedByOriginal;

        return $this;
    }

    /**
     * @return
     */
    public function getOmitSemirequiredReason()
    {
        return $this->omitSemirequiredReason;
    }

    /**
     * @param $omitSemirequiredReason
     */
    public function setOmitSemirequiredReason($omitSemirequiredReason)
    {
        $this->omitSemirequiredReason = $omitSemirequiredReason;
    }

    /**
     * @return AttributeValue
     */
    public function getParentAttributeValue()
    {
        return $this->parentAttributeValue;
    }

    /**
     * @param mixed $parentAttributeValue
     * @return AttributeValue
     */
    public function setParentAttributeValue($parentAttributeValue)
    {
        $this->parentAttributeValue = $parentAttributeValue;

        return $this;
    }

    /**
     * @return ProcessInstance | null
     */
    public function getRootProcess()
    {
        return $this->rootProcess;
    }

    /**
     * @param mixed $rootProcess
     * @return AttributeValue
     */
    public function setRootProcess($rootProcess)
    {
        $this->rootProcess = $rootProcess;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributePath()
    {
        return $this->attributePath;
    }

    /**
     * @param mixed $attributePath
     * @return AttributeValue
     */
    public function setAttributePath($attributePath)
    {
        $this->attributePath = $attributePath;

        return $this;
    }

    public function getCreatedAt()
    {
        return date('Y-m-d H:i:s', strtotime($this->createdAt));
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return AttributeValue
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSparxUpdatedAt()
    {
        return $this->sparxUpdatedAt;
    }

    /**
     * @param mixed $sparxUpdatedAt
     */
    public function setSparxUpdatedAt($sparxUpdatedAt)
    {
        $this->sparxUpdatedAt = $sparxUpdatedAt;
    }

    public function getValueByType($type) {

        return call_user_func([$this, 'get' . $this->camelize($type)]);

    }

    private function camelize($input, $separator = '_')
    {
        return str_replace($separator, '', ucwords($input, $separator));
//        return lcfirst();
    }

}