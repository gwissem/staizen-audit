<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * ServiceStatusDictionary
 *
 * @ORM\Entity
 */

class ServiceStatusDictionaryTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $shortMessage;

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ServiceStatusDictionaryTranslation
     */
    public function setMessage(string $message): ServiceStatusDictionaryTranslation
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortMessage()
    {
        return $this->shortMessage;
    }

    /**
     * @param string $shortMessage
     * @return ServiceStatusDictionaryTranslation
     */
    public function setShortMessage($shortMessage)
    {
        $this->shortMessage = $shortMessage;
        return $this;
    }


}

