<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class AttributeValueHistory
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\AttributeValue")
     * @ORM\JoinColumn(name="attribute_value_id", referencedColumnName="id")
     */
    protected $attributeValue;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $valueInt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $valueString;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $attributePath;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isDeleted;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $valueText;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=6, nullable=true)
     */
    protected $valueDecimal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $valueDate;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Attribute")
     * @ORM\JoinColumn(name="value_attribute_id", referencedColumnName="id")
     */
    protected $valueAttribute;

    /**
     * @var User $createdBy
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdByOriginal;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getValueInt()
    {
        return $this->valueInt;
    }

    /**
     * @param mixed $valueInt
     * @return AttributeValue
     */
    public function setValueInt($valueInt)
    {
        $this->valueInt = $valueInt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueString()
    {
        return $this->valueString;
    }

    /**
     * @param mixed $valueString
     * @return AttributeValue
     */
    public function setValueString($valueString)
    {
        $this->valueString = $valueString;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueText()
    {
        return $this->valueText;
    }

    /**
     * @param mixed $valueText
     * @return AttributeValue
     */
    public function setValueText($valueText)
    {
        $this->valueText = $valueText;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueDecimal()
    {
        return $this->valueDecimal;
    }

    /**
     * @param mixed $valueDecimal
     * @return AttributeValue
     */
    public function setValueDecimal($valueDecimal)
    {
        $this->valueDecimal = $valueDecimal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueDate()
    {
        return $this->valueDate;
    }

    /**
     * @param mixed $valueDate
     * @return AttributeValue
     */
    public function setValueDate($valueDate)
    {
        $this->valueDate = $valueDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param mixed $attribute
     * @return AttributeValue
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueAttribute()
    {
        return $this->valueAttribute;
    }

    /**
     * @param mixed $valueAttribute
     * @return AttributeValue
     */
    public function setValueAttribute($valueAttribute)
    {
        $this->valueAttribute = $valueAttribute;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    /**
     * @param mixed $attributeValue
     * @return AttributeValueHistory
     */
    public function setAttributeValue($attributeValue)
    {
        $this->attributeValue = $attributeValue;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedByOriginal()
    {
        return $this->createdByOriginal;
    }

    /**
     * @param User $createdByOriginal
     * @return AttributeValueHistory
     */
    public function setCreatedByOriginal($createdByOriginal)
    {
        $this->createdByOriginal = $createdByOriginal;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return AttributeValueHistory
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributePath()
    {
        return $this->attributePath;
    }

    /**
     * @param mixed $attributePath
     * @return AttributeValueHistory
     */
    public function setAttributePath($attributePath)
    {
        $this->attributePath = $attributePath;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     * @return AttributeValueHistory
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }



}