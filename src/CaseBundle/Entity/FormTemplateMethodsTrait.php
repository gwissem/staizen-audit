<?php

namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

trait FormTemplateMethodsTrait
{

    protected $formTemplates;

    /**
     * FormTemplateMethodsTrait constructor.
     */

    public function __construct()
    {
        /** THIS CONTROLLER ARE OVERRIDE. formTemplates MUST BE ADDED IN PARENT CONSTRUCTOR */

        $this->formTemplates = new ArrayCollection();
    }

    /**
     * @return FormTemplate|null
     */
    public function getFormTemplate()
    {

        if ($this->formTemplates->isEmpty()) return null;
        return $this->formTemplates->first();
    }

    /**
     * @return bool
     */
    public function hasFormTemplate()
    {
        return (!$this->formTemplates->isEmpty());
    }

    public function setFormTemplate(FormTemplate $formTemplate)
    {
        $this->formTemplates->add($formTemplate);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormTemplates()
    {
        return $this->formTemplates;
    }

    /**
     * @param mixed $formTemplates
     */
    public function setFormTemplates($formTemplates)
    {
        $this->formTemplates = $formTemplates;
    }

}