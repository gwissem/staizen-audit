<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
 * @ORM\Entity()
 * @ORM\Table(name="service_definition_platform")
 */
class ServiceDefinitionProgram
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var ServiceDefinition
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ServiceDefinition", inversedBy="programs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $serviceDefinition;

    /**
     * @var Program
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Program")
     * @ORM\JoinColumn(nullable=false)
     */
    private $program;

    /**
     * @var integer
     * @ORM\Column(name="position", type="integer", nullable=true, options={"default": 0})
     */
    protected $position;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return ServiceDefinition
     */
    public function getServiceDefinition()
    {
        return $this->serviceDefinition;
    }

    /**
     * @param ServiceDefinition $serviceDefinition
     */
    public function setServiceDefinition($serviceDefinition)
    {
        $this->serviceDefinition = $serviceDefinition;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Platform
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param Platform $program
     */
    public function setProgram($program)
    {
        $this->program = $program;
    }


}