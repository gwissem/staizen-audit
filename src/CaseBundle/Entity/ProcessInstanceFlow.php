<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ProcessInstanceFlowRepository")
 * @ORM\Table()
 */
class ProcessInstanceFlow
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="boolean", options={"defaults" : 0})
     */
    protected $active = 1;

    /**
     * @ORM\Column(type="integer", nullable= true)
     */
    protected $variant;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="previous_process_instance_id", referencedColumnName="id")
     */
    private $previousProcessInstance;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="next_process_instance_id", referencedColumnName="id")
     */
    private $nextProcessInstance;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPreviousProcessInstance()
    {
        return $this->previousProcessInstance;
    }

    /**
     * @param mixed $previousProcessInstance
     * @return ProcessInstanceFlow
     */
    public function setPreviousProcessInstance($previousProcessInstance)
    {
        $this->previousProcessInstance = $previousProcessInstance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNextProcessInstance()
    {
        return $this->nextProcessInstance;
    }

    /**
     * @param mixed $nextProcessInstance
     * @return ProcessInstanceFlow
     */
    public function setNextProcessInstance($nextProcessInstance)
    {
        $this->nextProcessInstance = $nextProcessInstance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return ProcessInstanceFlow
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param mixed $variant
     * @return ProcessInstanceFlow
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
        return $this;
    }


}
