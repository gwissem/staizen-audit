<?php

namespace CaseBundle\Entity;

use CaseBundle\Utils\ParserMethods;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ProcessInstanceRepository")
 * @ORM\Table()
 */
class ProcessInstance
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateEnter;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $postponeDate;
    /**
     * @var integer
     * @ORM\Column(type="integer", options={"default" = 0}, nullable = true)
     */
    private $postponeCount = 0;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateLeave;
    /**
     * @ORM\OneToOne(targetEntity="ProcessInstanceTime", mappedBy="processInstance")
     */
    protected $time;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $priority;

    /**
     * @ORM\OneToOne(targetEntity="CaseBundle\Entity\ProcessInstanceProperty", mappedBy="processInstance")
     */
    protected $property;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $step;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="previous_process_instance_id", referencedColumnName="id")
     */
    private $previousProcessInstance;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="processInstances")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $company;
    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\ProcessInstance", mappedBy="parent")
     */
    private $children;
    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\ProcessInstance", mappedBy="root")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $rootChildren;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var ProcessInstance $root
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance", inversedBy="rootChildren")
     * @ORM\JoinColumn(name="root_id", referencedColumnName="id")
     */
    private $root;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="group_process_id", referencedColumnName="id")
     */
    private $groupProcess;
    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\AttributeValue", mappedBy="groupProcessInstance")
     */
    private $attributeValues;
    /**
     * @var int
     * @ORM\Column(type="smallint", options={"default" = 1})
     */
    private $active = 1;

    /**
     * @ORM\Column(type="smallint", options={"default" = 1}, nullable=true)
     */
    private $lastRun = 1;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
    /**
     * @var User $createdBy
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdByOriginal;
    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;
    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;
    /**
     * @ORM\Column(type="integer", options={"default" = 0}, nullable=true)
     */
    private $workTime;
    /**
     * @var User $createdBy
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedByOriginal;

    /**
     * @ORM\Column(type="integer", name="choice_variant", nullable=true)
     */
    private $choiceVariant;



    /**
     * Przykład Tokenu: A3C92BDF-2BEA-4D78-88DD-07010800A683  (NEWID())
     * @ORM\Column(type="string", name="token", length=36, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="string", name="task_group", length=255, nullable=true)
     */
    private $taskGroup;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->attributeValues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string Przykład: A3C92BDF-2BEA-4D78-88DD-07010800A683
     */
    static function NEWID() {

        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < 8; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }
        $token .= "-";
        for ($i=0; $i < 4; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }
        $token .= "-";
        for ($i=0; $i < 4; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }
        $token .= "-";
        for ($i=0; $i < 4; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }
        $token .= "-";
        for ($i=0; $i < 12; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;

    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Step || null
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     * @return ProcessInstance
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return ProcessInstance
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     * @return ProcessInstance
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return ProcessInstance
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return ProcessInstance|null
     */
    public function getGroupProcess()
    {
        return $this->groupProcess;
    }

    /**
     * @param mixed $groupProcess
     * @return ProcessInstance
     */
    public function setGroupProcess($groupProcess)
    {
        $this->groupProcess = $groupProcess;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEnter()
    {
        return $this->dateEnter;
    }

    /**
     * @param mixed $dateEnter
     * @return ProcessInstance
     */
    public function setDateEnter($dateEnter)
    {
        $this->dateEnter = $dateEnter;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateLeave()
    {
        return $this->dateLeave;
    }

    /**
     * @param mixed $dateLeave
     * @return ProcessInstance
     */
    public function setDateLeave($dateLeave)
    {
        $this->dateLeave = $dateLeave;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateOpen()
    {
        return $this->dateOpen;
    }

    /**
     * @param mixed $dateOpen
     * @return ProcessInstance
     */
    public function setDateOpen($dateOpen)
    {
        $this->dateOpen = $dateOpen;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     * @return ProcessInstance
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributeValues()
    {
        return $this->attributeValues;
    }

    /**
     * @param mixed $attributeValues
     * @return ProcessInstance
     */
    public function setAttributeValues($attributeValues)
    {
        $this->attributeValues = $attributeValues;

        return $this;
    }

    /**
     * Add child
     *
     * @param \CaseBundle\Entity\ProcessInstance $child
     *
     * @return ProcessInstance
     */
    public function addChild(\CaseBundle\Entity\ProcessInstance $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \CaseBundle\Entity\ProcessInstance $child
     */
    public function removeChild(\CaseBundle\Entity\ProcessInstance $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Add attributeValue
     *
     * @param \CaseBundle\Entity\AttributeValue $attributeValue
     *
     * @return ProcessInstance
     */
    public function addAttributeValue(\CaseBundle\Entity\AttributeValue $attributeValue)
    {
        $this->attributeValues[] = $attributeValue;

        return $this;
    }

    /**
     * Remove attributeValue
     *
     * @param \CaseBundle\Entity\AttributeValue $attributeValue
     */
    public function removeAttributeValue(\CaseBundle\Entity\AttributeValue $attributeValue)
    {
        $this->attributeValues->removeElement($attributeValue);
    }

    /**
     * Get createdBy
     *
     * @return \UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param \UserBundle\Entity\User $createdBy
     *
     * @return ProcessInstance
     */
    public function setCreatedBy(\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy
     *
     * @param \UserBundle\Entity\User $updatedBy
     *
     * @return ProcessInstance
     */
    public function setUpdatedBy(\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return ProcessInstance
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     * @return ProcessInstance
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedByOriginal()
    {
        return $this->createdByOriginal;
    }

    /**
     * @param User $createdByOriginal
     * @return ProcessInstance
     */
    public function setCreatedByOriginal($createdByOriginal)
    {
        $this->createdByOriginal = $createdByOriginal;

        return $this;
    }

    /**
     * @return ProcessInstance|null
     */
    public function getPreviousProcessInstance()
    {
        return $this->previousProcessInstance;
    }

    /**
     * @param mixed $previousProcessInstance
     * @return ProcessInstance
     */
    public function setPreviousProcessInstance($previousProcessInstance)
    {
        $this->previousProcessInstance = $previousProcessInstance;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedByOriginal()
    {
        return $this->updatedByOriginal;
    }

    /**
     * @param User $updatedByOriginal
     * @return ProcessInstance
     */
    public function setUpdatedByOriginal($updatedByOriginal)
    {
        $this->updatedByOriginal = $updatedByOriginal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostponeDate()
    {
        return $this->postponeDate;
    }

    /**
     * @param mixed $postponeDate
     * @return ProcessInstance
     */
    public function setPostponeDate(\DateTime $postponeDate)
    {
        /** don't postpone follow up 21:00 - 9:00 */
        if($this->getStep()->getId() == '1006.003'){
            $hour = (int)$postponeDate->format('H');
            if($hour >= 21 || $hour < 9){
                $postponeDate = new \DateTime('tomorrow 09:15');
            }
        }
        $this->postponeDate = $postponeDate;

        $count = $this->getPostponeCount();
        $this->setPostponeCount($count+1);

        return $this;
    }

    /**
     * @return ProcessInstance|null
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @return int|null
     */
    public function getRootId()
    {
        return ($this->root) ? $this->root->getId() : null;
    }

    /**
     * @param mixed $root
     * @return ProcessInstance
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }


    /**
     * Add rootChild
     *
     * @param \CaseBundle\Entity\ProcessInstance $rootChild
     *
     * @return ProcessInstance
     */
    public function addRootChild(\CaseBundle\Entity\ProcessInstance $rootChild)
    {
        $this->rootChildren[] = $rootChild;

        return $this;
    }

    /**
     * Remove rootChild
     *
     * @param \CaseBundle\Entity\ProcessInstance $rootChild
     */
    public function removeRootChild(\CaseBundle\Entity\ProcessInstance $rootChild)
    {
        $this->rootChildren->removeElement($rootChild);
    }

    /**
     * Get rootChildren
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRootChildren()
    {
        return $this->rootChildren;
    }

    /**
     * @return mixed
     */
    public function getWorkTime()
    {
        return $this->workTime;
    }

    /**
     * @param mixed $workTime
     * @return ProcessInstance
     */
    public function setWorkTime($workTime)
    {
        $this->workTime = $workTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     * @return ProcessInstance
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChoiceVariant()
    {
        return $this->choiceVariant;
    }

    /**
     * @param mixed $choiceVariant
     */
    public function setChoiceVariant($choiceVariant)
    {
        $this->choiceVariant = $choiceVariant;
    }

    public function getLastRun()
    {
        return $this->lastRun;
    }

    /**
     * @return ProcessInstance
     */
    public function setLastRun($lastRun): ProcessInstance
    {
        $this->lastRun = $lastRun;
        return $this;
    }

    public function getDefinitionId(){
        return (int)explode('.', $this->getStep()->getId())[0];
    }

    /**
     * @return integer
     */
    public function getPostponeCount()
    {
        $count = $this->postponeCount ?: 0;

        return $count;
    }

    /**
     * @param mixed $postponeCount
     * @return ProcessInstance
     */
    public function setPostponeCount($postponeCount)
    {
        $this->postponeCount = $postponeCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $token
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }


    public function getIsTechnical(){
        return $this->getStep()->getIsTechnical();
    }

    /**
     * @return mixed
     */
    public function getPriorityChangedAt()
    {
        return $this->priorityChangedAt;
    }

    /**
     * @param mixed $priorityChangedAt
     * @return ProcessInstance
     */
    public function setPriorityChangedAt($priorityChangedAt)
    {
        $this->priorityChangedAt = $priorityChangedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrgent()
    {
        return $this->urgent;
    }

    /**
     * @param mixed $urgent
     * @return ProcessInstance
     */
    public function setUrgent($urgent)
    {
        $this->urgent = $urgent;
        return $this;
    }

    /**
     * @return ProcessInstanceProperty
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param mixed $property
     * @return ProcessInstance
     */
    public function setProperty($property)
    {
        $this->property = $property;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaskGroup()
    {
        return $this->taskGroup;
    }

    /**
     * @param mixed $taskGroup
     * @return ProcessInstance
     */
    public function setTaskGroup($taskGroup)
    {
        $this->taskGroup = $taskGroup;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCaseNumber() {

        if($this->getRoot()) {
            return ParserMethods::formattingCaseNumber($this->getRoot()->getId());
        }
        else if($this->getId()) {
            return ParserMethods::formattingCaseNumber($this->getId());
        }

        return null;

    }


}
