<?php

namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Form;

/**
 * FormTemplate
 *
 * @ORM\Table(name="form_template")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\FormTemplateRepository")
 */
class FormTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="config", type="json_array", nullable=true)
     */
    private $config;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\FormControl", mappedBy="formTemplate", fetch="EAGER")
     */

    private $controls;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Step", mappedBy="formTemplates")
     * @var ArrayCollection $steps ;
     */
    private $steps;


    public function __construct()
    {
        $this->controls = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get config
     *
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set config
     *
     * @param array $config
     *
     * @return FormTemplate
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getControls()
    {
        return $this->controls;
    }

    /**
     * @param mixed $controls
     */
    public function setControls($controls)
    {
        $this->controls = $controls;
    }

    public function getIdsOfControls()
    {

        /**
         * @param FormControl $control
         * @return mixed
         */
        $mapFunction = function ($control) {
            return ($control->getAttribute()) ? $control->getAttribute()->getId() : 0;
        };

        return $this->controls->map($mapFunction)->toArray();

    }

    public function getPathOfControls()
    {

        /**
         * @param FormControl $control
         * @return mixed
         */
        $mapFunction = function ($control) {
            return (!empty($control->getAttributePath()) ? $control->getAttributePath() : false);
        };

        /**
         * @param string|false
         * @return mixed
         */
        $filterFunction = function ($value) {
            return (!empty($value));
        };

        return $this->controls->map($mapFunction)->filter($filterFunction)->toArray();

    }

    public function addControl(FormControl $control)
    {

        $control->setFormTemplate($this);
        $this->controls->add($control);

        return $this;
    }

    public function removeControl(FormControl $control)
    {

        $this->controls->removeElement($control);

        return $this;
    }

    public function getControlById($id)
    {

        /**
         * @param FormControl $entry
         * @return mixed
         */
        $filterCriteria = function ($entry) use ($id) {
            return ($entry->getId() == $id);
        };

        $controls = $this->controls->filter($filterCriteria);

        return ($controls->count()) ? $controls->first() : null;
    }

    /**
     * @return ArrayCollection
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param ArrayCollection $steps
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
    }



}

