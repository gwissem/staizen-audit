<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 */
class RentalPriceListUsage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $platforms;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\RentalPriceList")
     * @ORM\JoinColumn(name="price_list_group_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $priceListGroup;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileUrl;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @param mixed $nip
     * @return RentalPriority
     */
    public function setNip($nip)
    {
        $this->nip = $nip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriceList()
    {
        return $this->priceList;
    }

    /**
     * @param mixed $priceList
     * @return RentalPriceList
     */
    public function setPriceList($priceList)
    {
        $this->priceList = $priceList;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlatforms()
    {
        return $this->platforms;
    }

    /**
     * @param mixed $platforms
     * @return RentalPriceListUsage
     */
    public function setPlatforms($platforms)
    {
        $this->platforms = $platforms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriceListGroup()
    {
        return $this->priceListGroup;
    }

    /**
     * @param mixed $priceListGroup
     * @return RentalPriceListUsage
     */
    public function setPriceListGroup($priceListGroup)
    {
        $this->priceListGroup = $priceListGroup;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }

    /**
     * @param mixed $fileUrl
     * @return RentalPriceListUsage
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;
        return $this;
    }




}