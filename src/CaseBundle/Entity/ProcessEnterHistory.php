<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ProcessEnterHistoryRepository")
 * @ORM\Table()
 */
class ProcessEnterHistory
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="process_instance_id", referencedColumnName="id")
     */
    protected $processInstance;

    /**
     * @ORM\Column(type="datetime", name="date_enter", nullable=true)
     */
    protected $dateEnter;

    /**
     * @ORM\Column(type="datetime", name="date_leave", nullable=true)
     */
    protected $dateLeave;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\Column(type="integer", name="time", options={"default" = 0}, nullable=true)
     */
    protected $time;

    /**
     * @ORM\Column(type="integer", name="real_time", nullable=true)
     */
    protected $realTime;

    /**
     * @ORM\Column(type="integer", name="flag2", nullable=true)
     */
    protected $flag2;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProcessInstance()
    {
        return $this->processInstance;
    }

    /**
     * @param mixed $processInstance
     */
    public function setProcessInstance($processInstance)
    {
        $this->processInstance = $processInstance;
    }

    /**
     * @return mixed
     */
    public function getDateEnter()
    {
        return $this->dateEnter;
    }

    /**
     * @param mixed $dateEnter
     */
    public function setDateEnter($dateEnter)
    {
        $this->dateEnter = $dateEnter;
    }

    /**
     * @return mixed
     */
    public function getDateLeave()
    {
        return $this->dateLeave;
    }

    /**
     * @param mixed $dateLeave
     */
    public function setDateLeave($dateLeave)
    {
        $this->dateLeave = $dateLeave;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getFlag2()
    {
        return $this->flag2;
    }

    /**
     * @param mixed $flag2
     */
    public function setFlag2($flag2)
    {
        $this->flag2 = $flag2;
    }

    /**
     * @return mixed
     */
    public function getRealTime()
    {
        return $this->realTime;
    }

    /**
     * @param mixed $realTime
     */
    public function setRealTime($realTime)
    {
        $this->realTime = $realTime;
    }

}
