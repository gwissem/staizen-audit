<?php
namespace CaseBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\StepInfoRepository")
 * @ORM\Table(name="step_info")
 */
class StepInfo{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $step;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Platform")
     * @ORM\JoinColumn(name="platform_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $platform;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Program")
     * @ORM\JoinColumn(name="program_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $program;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $text;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $locale;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     * @return StepInfo
     */
    public function setStep($step)
    {
        $this->step = $step;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     * @return StepInfo
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return StepInfo
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     * @return StepInfo
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param mixed $program
     */
    public function setProgram($program)
    {
        $this->program = $program;
    }


}