<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PushingTemplate
 *
 * @ORM\Table(name="pushing_template")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\PushingTemplateRepository")
 */
class PushingTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="CaseBundle\Entity\PushingDefinition", mappedBy="template")
     */
    private $pushingDefinition;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PushingTemplate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPushingDefinition()
    {
        return $this->pushingDefinition;
    }

    /**
     * @param mixed $pushingDefinition
     */
    public function setPushingDefinition($pushingDefinition)
    {
        $this->pushingDefinition = $pushingDefinition;
    }


}

