<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="make_model")
 */
class MakeModel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $active = 1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $dmc;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\MakeModelDictionary", mappedBy="makeModel")
     */
    private $dictionaries;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $programs;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumbers()
    {
        return $this->phoneNumbers;
    }

    /**
     * @param mixed $phoneNumbers
     * @return Platform
     */
    public function setPhoneNumbers($phoneNumbers)
    {
        $this->phoneNumbers = $phoneNumbers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * @param mixed $programs
     * @return Platform
     */
    public function setPrograms($programs)
    {
        $this->programs = $programs;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Platform
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return Platform
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtId()
    {
        return $this->extId;
    }

    /**
     * @param mixed $extId
     * @return Platform
     */
    public function setExtId($extId)
    {
        $this->extId = $extId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDictionaries()
    {
        return $this->dictionaries;
    }

    /**
     * @param mixed $dictionaries
     * @return MakeModel
     */
    public function setDictionaries($dictionaries)
    {
        $this->dictionaries = $dictionaries;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDmc()
    {
        return $this->dmc;
    }

    /**
     * @param mixed $dmc
     * @return MakeModel
     */
    public function setDmc($dmc)
    {
        $this->dmc = $dmc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return MakeModel
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }



}