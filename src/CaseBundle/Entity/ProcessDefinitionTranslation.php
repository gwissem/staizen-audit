<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class ProcessDefinitionTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @Assert\NotBlank()
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $info;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $additionalInfo;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $fullInfo;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $cancelDescription;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $viewerInfo;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AttributeTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     * @return ProcessDefinitionTranslation
     */
    public function setInfo($info)
    {
        $this->info = $info;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullInfo()
    {
        return $this->fullInfo;
    }


    /**
     * @param string $fullInfo
     * @return ProcessDefinitionTranslation
     */
    public function setFullInfo($fullInfo)
    {
        $this->fullInfo = $fullInfo;
        return $this;
    }


    public function getViewerInfo(){
        return $this->viewerInfo;
    }

    /**
     * @param string $viewerInfo;
     */
    public function setViewerInfo($viewerInfo){
        $this->viewerInfo = $viewerInfo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCancelDescription()
    {
        return $this->cancelDescription;
    }

    /**
     * @return string
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }

    /**
     * @param string $additionalInfo
     * @return ProcessDefinitionTranslation
     */
    public function setAdditionalInfo($additionalInfo)
    {
        $this->additionalInfo = $additionalInfo;
        return $this;
    }


    /**
     * @param string $cancelDescription
     * @return ProcessDefinitionTranslation
     */
    public function setCancelDescription($cancelDescription): ProcessDefinitionTranslation
    {
        $this->cancelDescription = $cancelDescription;
        return $this;
    }
}
