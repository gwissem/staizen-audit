<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @UniqueEntity(fields={"attributePath", "valueInt", "scope"})
 */
class AttributeValueTranslator
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $attributePath;

    /**
     * @ORM\Column(type="integer")
     */
    protected $valueInt;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    protected $output;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $scope;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAttributePath()
    {
        return $this->attributePath;
    }

    /**
     * @param mixed $attributePath
     * @return AttributeValueTranslator
     */
    public function setAttributePath($attributePath)
    {
        $this->attributePath = $attributePath;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueInt()
    {
        return $this->valueInt;
    }

    /**
     * @param mixed $valueInt
     * @return AttributeValueTranslator
     */
    public function setValueInt($valueInt)
    {
        $this->valueInt = $valueInt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param mixed $output
     * @return AttributeValueTranslator
     */
    public function setOutput($output)
    {
        $this->output = $output;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param mixed $scope
     * @return AttributeValueTranslator
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }


}