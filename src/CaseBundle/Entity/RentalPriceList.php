<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 */
class RentalPriceList
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\RentalPriceListGroup", inversedBy="priceLists")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $groupId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $type = 2;

    /**
     * @ORM\Column(type="integer")
     */
    protected $class;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $rentalClass;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    protected $makeModelIds;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $gearbox;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $features;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    protected $dailyCost;

    /**
     * @ORM\Column(type="integer")
     */
    protected $dayFrom;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $dayTo;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @param mixed $nip
     * @return RentalPriority
     */
    public function setNip($nip)
    {
        $this->nip = $nip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return RentalPriority
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     * @return RentalPriority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * @param mixed $programs
     * @return RentalPriority
     */
    public function setPrograms($programs)
    {
        $this->programs = $programs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return RentalPriceList
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     * @return RentalPriceList
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGearbox()
    {
        return $this->gearbox;
    }

    /**
     * @param mixed $gearbox
     * @return RentalPriceList
     */
    public function setGearbox($gearbox)
    {
        $this->gearbox = $gearbox;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDailyCost()
    {
        return $this->dailyCost;
    }

    /**
     * @param mixed $dailyCost
     * @return RentalPriceList
     */
    public function setDailyCost($dailyCost)
    {
        $this->dailyCost = $dailyCost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDayFrom()
    {
        return $this->dayFrom;
    }

    /**
     * @param mixed $dayFrom
     * @return RentalPriceList
     */
    public function setDayFrom($dayFrom)
    {
        $this->dayFrom = $dayFrom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDayTo()
    {
        return $this->dayTo;
    }

    /**
     * @param mixed $dayTo
     * @return RentalPriceList
     */
    public function setDayTo($dayTo)
    {
        $this->dayTo = $dayTo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param mixed $features
     * @return RentalPriceList
     */
    public function setFeatures($features)
    {
        $this->features = $features;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param mixed $groupId
     * @return RentalPriceList
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRentalClass()
    {
        return $this->rentalClass;
    }

    /**
     * @param mixed $rentalClass
     * @return RentalPriceList
     */
    public function setRentalClass($rentalClass)
    {
        $this->rentalClass = $rentalClass;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMakeModelIds()
    {
        return $this->makeModelIds;
    }

    /**
     * @param mixed $makeModelIds
     * @return RentalPriceList
     */
    public function setMakeModelIds($makeModelIds)
    {
        $this->makeModelIds = $makeModelIds;
        return $this;
    }




}