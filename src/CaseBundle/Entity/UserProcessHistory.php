<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * UserProcessHistory
 *
 * @ORM\Table(name="user_process_history")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\UserProcessHistoryRepository")
 */
class UserProcessHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="process_instance_id", referencedColumnName="id")
     */
    private $processInstance;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="original_user_id", referencedColumnName="id")
     */
    private $originalUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enter_at", type="datetime")
     */
    private $enterAt;

    /**
     * @var int
     *
     * @ORM\Column(name="time_spent", type="integer")
     */
    private $timeSpent;

    /**
     * UserProcessHistory constructor.
     * @param \DateTime $enterAt
     * @param int $timeSpent
     */
    public function __construct()
    {
        $this->enterAt = new \DateTime();
        $this->timeSpent = 0;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set processInstance
     *
     * @return UserProcessHistory
     */
    public function setProcessInstance($processInstance)
    {
        $this->processInstance = $processInstance;

        return $this;
    }

    /**
     * Get processInstance
     *
     * @return int
     */
    public function getProcessInstance()
    {
        return $this->processInstance;
    }

    /**
     * Set user
     *
     * @return UserProcessHistory
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set originalUser
     *
     * @return UserProcessHistory
     */
    public function setoriginalUser($originalUser)
    {
        $this->originalUser = $originalUser;

        return $this;
    }

    /**
     * Get originalUser
     *
     * @return int
     */
    public function getoriginalUser()
    {
        return $this->originalUser;
    }

    /**
     * Set enterAt
     *
     * @param \DateTime $enterAt
     *
     * @return UserProcessHistory
     */
    public function setEnterAt($enterAt)
    {
        $this->enterAt = $enterAt;

        return $this;
    }

    /**
     * Get enterAt
     *
     * @return \DateTime
     */
    public function getEnterAt()
    {
        return $this->enterAt;
    }

    /**
     * Set timeSpent
     *
     * @param integer $timeSpent
     *
     * @return UserProcessHistory
     */
    public function setTimeSpent($timeSpent)
    {
        $this->timeSpent = $timeSpent;

        return $this;
    }

    /**
     * Get timeSpent
     *
     * @return int
     */
    public function getTimeSpent()
    {
        return $this->timeSpent;
    }
}

