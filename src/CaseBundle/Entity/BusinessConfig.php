<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * BusinessConfig
 *
 * @ORM\Table(name="business_config", uniqueConstraints={@UniqueConstraint(name="business_config_unique", columns={"config_definition_id", "program_id", "platform_id"})})
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\BusinessConfigRepository")
 */
class BusinessConfig
{

    use  ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="config_definition_id", type="integer")
     */

    /**
     * @var BusinessConfigDefinition $configDefinition
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\BusinessConfigDefinition")
     * @ORM\JoinColumn(name="config_definition_id", referencedColumnName="id")
     */
    private $configDefinition;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=2000, nullable=true)
     */
    private $value;

    /**
     * @var Program
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Program")
     * @ORM\JoinColumn(name="program_id", referencedColumnName="id", nullable=true)
     */
    private $program;

    /**
     * @var Platform
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Platform")
     * @ORM\JoinColumn(name="platform_id", referencedColumnName="id")
     */
    private $platform;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public $parentValue;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set configDefinition
     *
     * @param BusinessConfigDefinition $configDefinition
     *
     * @return BusinessConfig
     */
    public function setConfigDefinition($configDefinition)
    {
        $this->configDefinition = $configDefinition;

        return $this;
    }

    /**
     * Get configDefinition
     *
     * @return BusinessConfigDefinition
     */
    public function getConfigDefinition()
    {
        return $this->configDefinition;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return BusinessConfig
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set program
     *
     * @param Program $program
     *
     * @return BusinessConfig
     */
    public function setProgram($program)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return Program
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set platform
     *
     * @param Platform $platform
     *
     * @return BusinessConfig
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Get platform
     *
     * @return Platform
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return BusinessConfig
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function copyValues(BusinessConfig $sourceConfig) {

        $this->setValue($sourceConfig->getValue());
        $this->setDescription($sourceConfig->getDescription());

    }

//    public function __clone()
//    {
//        $this->id = null;
//        $this->updatedAt = new \DateTime();
//    }


}

