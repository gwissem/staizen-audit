<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="service")
 */
class Service
{

    use ORMBehaviors\Timestampable\Timestampable;

    const STATUS_NEW = 0;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="integer")
     */
    protected $status = self::STATUS_NEW;
    /**
     * @ORM\Column(type="integer")
     */
    protected $price;
    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $currency;
    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $quantity;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Company", inversedBy="services")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $company;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ServiceDefinition")
     * @ORM\JoinColumn(name="service_definition_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $serviceDefinition;
    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\Service", mappedBy="master")
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\Cost", mappedBy="service")
     */
    private $costs;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Service", inversedBy="children")
     * @ORM\JoinColumn(name="master_id", referencedColumnName="id")
     */
    private $master;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @return mixed
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param mixed $program
     * @return Service
     */
    public function setProgram($program)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Service
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     * @return Service
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Service
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return Service
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return Service
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     * @return Service
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCosts()
    {
        return $this->costs;
    }

    /**
     * @param mixed $costs
     * @return Service
     */
    public function setCosts($costs)
    {
        $this->costs = $costs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * @param mixed $master
     * @return Service
     */
    public function setMaster($master)
    {
        $this->master = $master;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceDefinition()
    {
        return $this->serviceDefinition;
    }

    /**
     * @param mixed $serviceDefinition
     * @return Service
     */
    public function setServiceDefinition($serviceDefinition)
    {
        $this->serviceDefinition = $serviceDefinition;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt(): \DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return Service
     */
    public function setDeletedAt(\DateTime $deletedAt): Service
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }



}