<?php


namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ProgramRepository")
 * @ORM\Table(name="vin_program")
 */
class Program
{
    use ORMBehaviors\Translatable\Translatable;

    const STATUS_ACTIVE = 1;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\ServiceDefinitionProgram", mappedBy="program")
     */
    protected $services;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Platform", inversedBy="programs")
     * @ORM\JoinColumn(name="platform_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $platform;

    /**
     * @ORM\Column(type="string", length=255, name="fdds", nullable=true)
     */
    protected $fdds;

    /**
     * @ORM\Column(type="integer", options={"default"=1})
     */
    private $status = self::STATUS_ACTIVE;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Step", mappedBy="programs")
     */
    private $steps;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\ProcessFlow", mappedBy="programs")
     */
    private $flows;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isProducer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $accidentPriority;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $breakdownPriority;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $abroadTowingLimitEur;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Program
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Platform|null
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     * @return Program
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Program
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->translate()->getDescription();
    }



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add service
     *
     * @param \CaseBundle\Entity\ProgramService $service
     *
     * @return Program
     */
    public function addService(\CaseBundle\Entity\ProgramService $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \CaseBundle\Entity\ProgramService $service
     */
    public function removeService(\CaseBundle\Entity\ProgramService $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return mixed
     */
    public function getFdds()
    {
        return $this->fdds;
    }

    /**
     * @param mixed $fdds
     * @return Program
     */
    public function setFdds($fdds)
    {
        $this->fdds = $fdds;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param mixed $steps
     * @return Program
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlows()
    {
        return $this->flows;
    }

    /**
     * @param mixed $flows
     * @return Program
     */
    public function setFlows($flows)
    {
        $this->flows = $flows;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisProducer()
    {
        return $this->isProducer;
    }

    /**
     * @param mixed $isProducer
     * @return Program
     */
    public function setIsProducer($isProducer)
    {
        $this->isProducer = $isProducer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccidentPriority()
    {
        return $this->accidentPriority;
    }

    /**
     * @param mixed $accidentPriority
     * @return Program
     */
    public function setAccidentPriority($accidentPriority)
    {
        $this->accidentPriority = $accidentPriority;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBreakdownPriority()
    {
        return $this->breakdownPriority;
    }

    /**
     * @param mixed $breakdownPriority
     * @return Program
     */
    public function setBreakdownPriority($breakdownPriority)
    {
        $this->breakdownPriority = $breakdownPriority;
        return $this;
    }


    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAbroadTowingLimitEur()
    {
        return $this->abroadTowingLimitEur;
    }

    /**
     * @param mixed $abroadTowingLimitEur
     * @return Program
     */
    public function setAbroadTowingLimitEur($abroadTowingLimitEur)
    {
        $this->abroadTowingLimitEur = $abroadTowingLimitEur;
        return $this;
    }



}
