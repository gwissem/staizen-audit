<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class ProgramService
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Program", inversedBy="services")
     * @ORM\JoinColumn(name="program_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $program;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="start_step_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $startStep;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $icon;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param mixed $program
     * @return ProgramService
     */
    public function setProgram($program)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartStep()
    {
        return $this->startStep;
    }

    /**
     * @param mixed $startStep
     * @return ProgramService
     */
    public function setStartStep($startStep)
    {
        $this->startStep = $startStep;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     * @return ProgramService
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

}
