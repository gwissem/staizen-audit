<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ProcessInstanceTimeSpent")
 * @ORM\Table()
 * @UniqueEntity("processInstance")
 */
class ProcessInstanceTime
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="CaseBundle\Entity\ProcessInstance", inversedBy="time")
     * @ORM\JoinColumn(name="process_instance_id", referencedColumnName="id")
     */
    private $processInstance;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     */
    private $lifeTime;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     */
    private $percentage;

    /**
     * @return mixed
     */
    public function getProcessInstance()
    {
        return $this->processInstance;
    }

    /**
     * @param mixed $processInstance
     * @return ProcessInstanceTime
     */
    public function setProcessInstance($processInstance)
    {
        $this->processInstance = $processInstance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLifeTime()
    {
        return $this->lifeTime;
    }

    /**
     * @param mixed $lifeTime
     * @return ProcessInstanceTime
     */
    public function setLifeTime($lifeTime)
    {
        $this->lifeTime = $lifeTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param mixed $percentage
     * @return ProcessInstanceTime
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
        return $this;
    }


}
