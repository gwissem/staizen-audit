<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\PlatformRepository")
 * @ORM\Table(name="platform")
 */
class Platform
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @ORM\Column(name="extId", type="integer", nullable=true)
     */
    protected $extId;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\PlatformPhone", mappedBy="platform")
     */
    private $phoneNumbers;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\Program", mappedBy="platform")
     */
    private $programs;

    /**
     * @ORM\Column(type="string", length=255, name="official_line_number", nullable=true)
     */
    protected $officialLineNumber;

    /**
     * @ORM\Column(type="integer", name="partnerType", nullable=true)
     */
    protected $partnerType;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\PlatformGroup", inversedBy="platforms")
     * @ORM\JoinTable(name="platform_group_platforms",
     *      joinColumns={@ORM\JoinColumn(name="platform_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="platform_group_id", referencedColumnName="id")}
     * )
     */
    private $platformGroups;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User", mappedBy="platforms")
     */
    private $userPlatforms;


    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $smsSignature;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $iconPath;

    public function __construct() {
        $this->platformGroups = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->userPlatforms = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumbers()
    {
        return $this->phoneNumbers;
    }

    /**
     * @param mixed $phoneNumbers
     * @return Platform
     */
    public function setPhoneNumbers($phoneNumbers)
    {
        $this->phoneNumbers = $phoneNumbers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * @param mixed $programs
     * @return Platform
     */
    public function setPrograms($programs)
    {
        $this->programs = $programs;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Platform
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return Platform
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtId()
    {
        return $this->extId;
    }

    /**
     * @param mixed $extId
     * @return Platform
     */
    public function setExtId($extId)
    {
        $this->extId = $extId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOfficialLineNumber()
    {
        return $this->officialLineNumber;
    }

    /**
     * @param mixed $officialLineNumber
     */
    public function setOfficialLineNumber($officialLineNumber)
    {
        $this->officialLineNumber = $officialLineNumber;
    }

    /**
     * @return mixed
     */
    public function getPartnerType()
    {
        return $this->partnerType;
    }

    /**
     * @param mixed $partnerType
     */
    public function setPartnerType($partnerType)
    {
        $this->partnerType = $partnerType;
    }

    /**
     * @return mixed
     */
    public function getPlatformGroups()
    {
        return $this->platformGroups;
    }

    /**
     * @param mixed $platformGroups
     * @return Platform
     */
    public function setPlatformGroups($platformGroups)
    {
        $this->platformGroups = $platformGroups;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return mixed
     */
    public function getIconPath()
    {
        return $this->iconPath;
    }

    /**
     * @param mixed $iconPath
     * @return Platform
     */
    public function setIconPath($iconPath)
    {
        $this->iconPath = $iconPath;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSmsSignature()
    {
        return $this->smsSignature;
    }

    /**
     * @param mixed $smsSignature
     */
    public function setSmsSignature($smsSignature)
    {
        $this->smsSignature = $smsSignature;
    }

    /**
     * @return mixed
     */
    public function getUserPlatforms()
    {
        return $this->userPlatforms;
    }

    /**
     * @param mixed $userPlatforms
     */
    public function setUserPlatforms($userPlatforms)
    {
        $this->userPlatforms[] = $userPlatforms;
    }



}