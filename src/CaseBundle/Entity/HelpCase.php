<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="help_case")
 */
class HelpCase
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $note;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $diagnosis_summary;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $info_nh;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $info_mw;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $info_transport;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $mw_forms_003_030;

    public function __construct() {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getDiagnosisSummary()
    {
        return $this->diagnosis_summary;
    }

    /**
     * @param mixed $diagnosis_summary
     */
    public function setDiagnosisSummary($diagnosis_summary)
    {
        $this->diagnosis_summary = $diagnosis_summary;
    }

    /**
     * @return mixed
     */
    public function getInfoNh()
    {
        return $this->info_nh;
    }

    /**
     * @param mixed $info_nh
     */
    public function setInfoNh($info_nh)
    {
        $this->info_nh = $info_nh;
    }

    /**
     * @return mixed
     */
    public function getInfoMw()
    {
        return $this->info_mw;
    }

    /**
     * @param mixed $info_mw
     */
    public function setInfoMw($info_mw)
    {
        $this->info_mw = $info_mw;
    }

    /**
     * @return mixed
     */
    public function getInfoTransport()
    {
        return $this->info_transport;
    }

    /**
     * @param mixed $info_transport
     */
    public function setInfoTransport($info_transport)
    {
        $this->info_transport = $info_transport;
    }

    /**
     * @return mixed
     */
    public function getMwForms003030()
    {
        return $this->mw_forms_003_030;
    }

    /**
     * @param mixed $mw_forms_003_030
     */
    public function setMwForms003030($mw_forms_003_030)
    {
        $this->mw_forms_003_030 = $mw_forms_003_030;
    }



}