<?php
// src/Acme/ApiBundle/Entity/Client.php

namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="vin_package")
 */
class DictionaryPackage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255,)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\Dictionary", mappedBy="package", cascade={"persist"})
     */
    private $dictionaries;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isIncremental;

    public function __construct()
    {
        $this->dictionaries = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return DictionaryPackage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add dictionary
     *
     * @param \CaseBundle\Entity\Dictionary $dictionary
     *
     * @return DictionaryPackage
     */
    public function addDictionary(\CaseBundle\Entity\Dictionary $dictionary)
    {
        $this->dictionaries[] = $dictionary;

        return $this;
    }

    /**
     * Remove dictionary
     *
     * @param \CaseBundle\Entity\Dictionary $dictionary
     */
    public function removeDictionary(\CaseBundle\Entity\Dictionary $dictionary)
    {
        $this->dictionaries->removeElement($dictionary);
    }

    /**
     * Get dictionaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDictionaries()
    {
        return $this->dictionaries;
    }

    public function setDictionaries($dictionaries)
    {
        $this->dictionaries = $dictionaries;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsIncremental()
    {
        return $this->isIncremental;
    }

    /**
     * @param mixed $isIncremental
     * @return DictionaryPackage
     */
    public function setIsIncremental($isIncremental)
    {
        $this->isIncremental = $isIncremental;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }
}
