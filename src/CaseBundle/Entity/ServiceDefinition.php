<?php


namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="service_definition")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ServiceDefinitionRepository")
 */
class ServiceDefinition
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    const STATUS_NEW = 0;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Company", inversedBy="services")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="start_step_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $startStepId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $position;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $icon;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $sparxServiceName;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\ServiceDefinitionProgram", mappedBy="serviceDefinition", fetch="EXTRA_LAZY")
     */
    private $programs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $type = 1;

    /**
     * ServiceDefinition constructor.
     */
    public function __construct()
    {
        $this->programs   = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ServiceDefinition
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     * @return ServiceDefinition
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return ServiceDefinition
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     * @return ServiceDefinition
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartStepId()
    {
        return $this->startStepId;
    }

    /**
     * @param mixed $startStepId
     * @return ServiceDefinition
     */
    public function setStartStepId($startStepId)
    {
        $this->startStepId = $startStepId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSparxServiceName()
    {
        return $this->sparxServiceName;
    }

    /**
     * @param mixed $sparxServiceName
     */
    public function setSparxServiceName($sparxServiceName)
    {
        $this->sparxServiceName = $sparxServiceName;
    }

    public function getName() {
        return $this->translate()->getName();
    }

    function __toString()
    {
        return (string)$this->getName() . ' (' . $this->getSparxServiceName() . ')';
    }

    /**
     * @return mixed
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     *
     * @param ServiceDefinitionProgram $serviceDefinitionProgram
     * @return ServiceDefinition
     */
    public function addProgram(ServiceDefinitionProgram $serviceDefinitionProgram)
    {
        $this->programs[] = $serviceDefinitionProgram;
        $serviceDefinitionProgram->setServiceDefinition($this);
        return $this;
    }

    /**
     * Remove Program
     * @param ServiceDefinitionProgram $serviceDefinitionProgram
     */
    public function removeProgram(ServiceDefinitionProgram $serviceDefinitionProgram)
    {
        $this->programs->removeElement($serviceDefinitionProgram);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return ServiceDefinition
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


}