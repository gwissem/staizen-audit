<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 */
class RentalPriceListExtra
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\RentalPriceListGroup")
    * @ORM\JoinColumn(name="price_list_group_id", referencedColumnName="id", onDelete="CASCADE")
    */
    private $priceListGroup;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $crossBorderCost;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $extraHoursFrom;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $extraHoursTo;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $extraHoursSaturdayFrom;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $extraHoursSaturdayTo;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $afterHoursCost;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $startDistanceMeasureType = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $startReturnCosts;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $distanceCost;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $minDaysCostFreeReturn;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $minDaysDistanceMeasureType;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $minDaysReturnCosts;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, nullable=true)
     */
    private $minDaysDistanceCost;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPriceList()
    {
        return $this->priceList;
    }

    /**
     * @param mixed $priceList
     * @return RentalPriceListExtra
     */
    public function setPriceList($priceList)
    {
        $this->priceList = $priceList;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCrossBorderCost()
    {
        return $this->crossBorderCost;
    }

    /**
     * @param mixed $crossBorderCost
     * @return RentalPriceListExtra
     */
    public function setCrossBorderCost($crossBorderCost)
    {
        $this->crossBorderCost = $crossBorderCost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtraHoursFrom()
    {
        return $this->extraHoursFrom;
    }

    /**
     * @param mixed $extraHoursFrom
     * @return RentalPriceListExtra
     */
    public function setExtraHoursFrom($extraHoursFrom)
    {
        $this->extraHoursFrom = $extraHoursFrom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtraHoursTo()
    {
        return $this->extraHoursTo;
    }

    /**
     * @param mixed $extraHoursTo
     * @return RentalPriceListExtra
     */
    public function setExtraHoursTo($extraHoursTo)
    {
        $this->extraHoursTo = $extraHoursTo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtraHoursSaturdayFrom()
    {
        return $this->extraHoursSaturdayFrom;
    }

    /**
     * @param mixed $extraHoursSaturdayFrom
     * @return RentalPriceListExtra
     */
    public function setExtraHoursSaturdayFrom($extraHoursSaturdayFrom)
    {
        $this->extraHoursSaturdayFrom = $extraHoursSaturdayFrom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtraHoursSaturdayTo()
    {
        return $this->extraHoursSaturdayTo;
    }

    /**
     * @param mixed $extraHoursSaturdayTo
     * @return RentalPriceListExtra
     */
    public function setExtraHoursSaturdayTo($extraHoursSaturdayTo)
    {
        $this->extraHoursSaturdayTo = $extraHoursSaturdayTo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAfterHoursCost()
    {
        return $this->afterHoursCost;
    }

    /**
     * @param mixed $afterHoursCost
     * @return RentalPriceListExtra
     */
    public function setAfterHoursCost($afterHoursCost)
    {
        $this->afterHoursCost = $afterHoursCost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDistanceMeasureType()
    {
        return $this->startDistanceMeasureType;
    }

    /**
     * @param mixed $startDistanceMeasureType
     * @return RentalPriceListExtra
     */
    public function setStartDistanceMeasureType($startDistanceMeasureType)
    {
        $this->startDistanceMeasureType = $startDistanceMeasureType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartReturnCosts()
    {
        return $this->startReturnCosts;
    }

    /**
     * @param mixed $startReturnCosts
     * @return RentalPriceListExtra
     */
    public function setStartReturnCosts($startReturnCosts)
    {
        $this->startReturnCosts = $startReturnCosts;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistanceCost()
    {
        return $this->distanceCost;
    }

    /**
     * @param mixed $distanceCost
     * @return RentalPriceListExtra
     */
    public function setDistanceCost($distanceCost)
    {
        $this->distanceCost = $distanceCost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinDaysCostFreeReturn()
    {
        return $this->minDaysCostFreeReturn;
    }

    /**
     * @param mixed $minDaysCostFreeReturn
     * @return RentalPriceListExtra
     */
    public function setMinDaysCostFreeReturn($minDaysCostFreeReturn)
    {
        $this->minDaysCostFreeReturn = $minDaysCostFreeReturn;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinDaysDistanceMeasureType()
    {
        return $this->minDaysDistanceMeasureType;
    }

    /**
     * @param mixed $minDaysDistanceMeasureType
     * @return RentalPriceListExtra
     */
    public function setMinDaysDistanceMeasureType($minDaysDistanceMeasureType)
    {
        $this->minDaysDistanceMeasureType = $minDaysDistanceMeasureType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinDaysReturnCosts()
    {
        return $this->minDaysReturnCosts;
    }

    /**
     * @param mixed $minDaysReturnCosts
     * @return RentalPriceListExtra
     */
    public function setMinDaysReturnCosts($minDaysReturnCosts)
    {
        $this->minDaysReturnCosts = $minDaysReturnCosts;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinDaysDistanceCost()
    {
        return $this->minDaysDistanceCost;
    }

    /**
     * @param mixed $minDaysDistanceCost
     * @return RentalPriceListExtra
     */
    public function setMinDaysDistanceCost($minDaysDistanceCost)
    {
        $this->minDaysDistanceCost = $minDaysDistanceCost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriceListGroup()
    {
        return $this->priceListGroup;
    }

    /**
     * @param mixed $priceListGroup
     * @return RentalPriceListExtra
     */
    public function setPriceListGroup($priceListGroup)
    {
        $this->priceListGroup = $priceListGroup;
        return $this;
    }



}