<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BusinessConfigDefinitionTranslation
 *
 * @ORM\Table(name="business_config_definition_translation", uniqueConstraints={@ORM\UniqueConstraint(name="unique_id_name_locale", columns={"translatable_id", "locale", "name"})})
 * @ORM\Entity()
 * @UniqueEntity(fields={"locale", "name"})
 */

class BusinessConfigDefinitionTranslation
{

    use ORMBehaviors\Translatable\Translation;

    /**
     * @Assert\NotBlank()
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BusinessConfigDefinitionTranslation
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return BusinessConfigDefinitionTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


}

