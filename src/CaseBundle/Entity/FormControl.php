<?php

namespace CaseBundle\Entity;

use CaseBundle\Service\FormTemplateGenerator;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;

/**
 * FormAttribute
 *
 * @ORM\Table(name="form_control")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\FormControlRepository")
 */
class FormControl
{

    use Translatable;

    public $groupProcessId;
    public $processInstanceId;
    public $isVisible = true;
    public $notAllowEdit = false;
    public $controlType = AttributeCondition::TYPE_NOT_REQUIRED;
    public $escapeHtml = true;
    private $virtualLabel;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var FormTemplate
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\FormTemplate", inversedBy="controls")
     * @ORM\JoinColumn(name="form_template", referencedColumnName="id", onDelete="CASCADE")
     */
    private $formTemplate;
    /**
     * @var Attribute | null
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Attribute")
     * @ORM\JoinColumn(name="attribute", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $attribute;
    /**
     * @var string
     *
     * @ORM\Column(name="attribute_path", type="string", length=255, nullable=true)
     */
    private $attributePath;
    /**
     * @var int
     *
     * @ORM\Column(name="width", type="integer", options={"default": 4})
     */
    private $width = 4;
    /**
     * @var int
     *
     * @ORM\Column(name="height", type="integer", options={"default": null}, nullable=true)
     */
    private $height;
    /**
     * @var int
     *
     * @ORM\Column(name="position_x", type="integer", options={"default": 0})
     */
    private $x = 0;
    /**
     * @var int
     *
     * @ORM\Column(name="position_y", type="integer", options={"default": 0})
     */
    private $y = 0;
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="label", type="string", length=4000, nullable=true)
//     */
//    private $label;
    /**
     * @var string
     *
     * @ORM\Column(name="placeholder", type="string", length=255, nullable=true)
     */
    private $placeholder;
    /**
     * @var string
     *
     * @ORM\Column(name="widget", type="string", length=255, nullable=true)
     */
    private $widget;
    /**
     * @var string
     *
     * @ORM\Column(name="customStyle", type="text", nullable=true)
     */
    private $customStyle;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_class", type="string", length=255, nullable=true)
     */
    private $customClass;

    /**
     * @var string
     *
     * @ORM\Column(name="extra_data", type="json_array", nullable=true)
     */
    private $extraData;
    private $tempUniqueId;

    private $parsedLabel;

    public function hasAttribute()
    {

        return ($this->attribute);

    }

    /**
     * @return string
     */
    public function getPlaceholder(): string
    {
        return $this->placeholder;
    }

    /**
     * @param string $placeholder
     */
    public function setPlaceholder(string $placeholder)
    {
        $this->placeholder = $placeholder;
    }

    /**
     * @return string
     */
    public function getExtraData(): string
    {
        return $this->extraData;
    }

    /**
     * @param string $extraData
     */
    public function setExtraData(string $extraData)
    {
        $this->extraData = $extraData;
    }

    /**
     * @return FormTemplate
     */
    public function getFormTemplate(): FormTemplate
    {
        return $this->formTemplate;
    }

    /**
     * @param FormTemplate $formTemplate
     */
    public function setFormTemplate(FormTemplate $formTemplate)
    {
        $this->formTemplate = $formTemplate;
    }

    public function toArray()
    {

        $parsedLabel = $this->getWidget() == FormTemplateGenerator::WIDGET_DIV ? $this->getParsedLabel() : htmlspecialchars(trim(str_replace(["\r\n", "\r", "\n"], "<br/>", $this->getParsedLabel())));

        return [
            "id" => $this->getId(),
            "path" => $this->getAttributePath(),
            "attrId" => ($this->getAttribute()) ? $this->getAttribute()->getId() : null,
            "x" => $this->getX(),
            "y" => $this->getY(),
            "width" => $this->getWidth(),
            "height" => $this->getHeight(),
            "label" => $parsedLabel,
            "styles" => $this->getCustomStyle(),
            "widget" => $this->getWidget(),
            "extraData" => $this->getExtraDataArray(),
            "customClass" => $this->getCustomClass(),
            "isVisible" => $this->isVisible,
            "notAllowEdit" => $this->notAllowEdit,
            "controlType" => $this->controlType
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAttributePath()
    {
        return $this->attributePath;
    }

    /**
     * @param string $attributePath
     * @return FormControl
     */
    public function setAttributePath(string $attributePath): FormControl
    {
        $this->attributePath = $attributePath;
        return $this;
    }

    /**
     * @return Attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param mixed $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @param int $x
     */
    public function setX(int $x)
    {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @param int $y
     */
    public function setY(int $y)
    {
        $this->y = $y;
    }

    /**
     * Get width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return FormControl
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height)
    {
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        if($this->virtualLabel) {
            return $this->virtualLabel;
        }

        return $this->translate()->getLabel();
    }

    /**
     * @param $label
     */
    public function setVirtualLabel($label) {
        $this->virtualLabel = $label;
    }

//    /**
//     * @param string $label
//     */
//    public function setLabel(string $label)
//    {
//        $this->parsedLabel = $label;
//        $this->translate()->setLabel($label);
//    }

    /**
     * Get customStyle
     *
     * @return string
     */
    public function getCustomStyle()
    {
        if ($this->isJson($this->customStyle)) {
            return json_decode($this->customStyle, true);
        }
        return $this->customStyle;
    }

    /**
     * Set customStyle
     *
     * @param string $customStyle
     *
     * @return FormControl
     */
    public function setCustomStyle($customStyle)
    {
        if(is_array($customStyle)) {
            $this->customStyle = json_encode($customStyle);
        }
        else
            $this->customStyle = $customStyle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWidget()
    {
        return $this->widget;
    }

    /**
     * @param string $widget
     */
    public function setWidget(string $widget)
    {
        $widget = $widget === 'null' ? NULL : $widget;
        $this->widget = $widget;
    }

    public function isWidget() {
        return (!empty($this->widget));
    }

    public function getExtraDataArray()
    {
        $json = "";

        if($this->extraData !== NULL && $this->isJson($this->extraData)) {
            $json = json_decode($this->extraData, true);
        }

        return $json;
    }

    /**
     * @return mixed
     */
    public function getTempUniqueId()
    {
        return $this->tempUniqueId;
    }

    /**
     * @param mixed $tempUniqueId
     */
    public function setTempUniqueId($tempUniqueId)
    {
        $this->tempUniqueId = $tempUniqueId;
    }

    public function isReadOnly() : bool
    {
        return $this->controlType == AttributeCondition::TYPE_READ_ONLY;
    }

    public function isRequired(): bool
    {
        return $this->controlType == AttributeCondition::TYPE_REQUIRED;
    }

    private function isJson($string)
    {
        try {
            json_decode($string, true);
            return (json_last_error() == JSON_ERROR_NONE);
        }
        catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getCustomClass(): string
    {
        return ($this->customClass === null) ? '' : $this->customClass;
    }

    /**
     * @param string $customClass
     */
    public function setCustomClass(string $customClass)
    {
        $this->customClass = $customClass;
    }

    public function getJavaScript() {
        $extraData = $this->getExtraDataArray();

        if(!empty($extraData['javascript'])) {
            return base64_decode($extraData['javascript']);
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getParsedLabel()
    {
        if($this->parsedLabel === null && !empty($this->getLabel())) {
            return $this->getLabel();
        }
        return $this->parsedLabel;
    }

    /**
     * @param mixed $parsedLabel
     */
    public function setParsedLabel($parsedLabel)
    {
        $this->parsedLabel = $parsedLabel;
    }
}

