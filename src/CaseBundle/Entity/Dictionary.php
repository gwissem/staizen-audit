<?php
// src/Acme/ApiBundle/Entity/Client.php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity()
 * @ORM\Table(name="vin_dictionary", uniqueConstraints={@UniqueConstraint(name="description_package", columns={"description", "package_id"})})
 */
class Dictionary
{

    const STATUS_ACTIVE = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $type;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isKey;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $descriptionOrg;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $attributePath;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\DictionaryPackage", inversedBy="dictionaries")
     * @ORM\JoinColumn(name="package_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $package;

    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\DictionaryElement", mappedBy="dictionary")
     */
    private $elements;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Dictionary
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Dictionary
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Dictionary
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsKey()
    {
        return $this->isKey;
    }

    /**
     * @param mixed $isKey
     * @return Dictionary
     */
    public function setIsKey($isKey)
    {
        $this->isKey = $isKey;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     * @return Dictionary
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Dictionary
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescriptionOrg()
    {
        return $this->descriptionOrg;
    }

    /**
     * @param mixed $descriptionOrg
     * @return Dictionary
     */
    public function setDescriptionOrg($descriptionOrg)
    {
        $this->descriptionOrg = $descriptionOrg;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param mixed $elements
     * @return Dictionary
     */
    public function setElements($elements)
    {
        $this->elements = $elements;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param mixed $package
     * @return Dictionary
     */
    public function setPackage($package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributePath()
    {
        return $this->attributePath;
    }

    /**
     * @param mixed $attributePath
     */
    public function setAttributePath($attributePath)
    {
        $this->attributePath = $attributePath;
    }



}