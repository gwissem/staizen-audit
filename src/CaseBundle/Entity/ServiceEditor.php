<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="AtlasDB_def.dbo.service_editor")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ServiceEditorRepository")
 */
class ServiceEditor
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    const STATUS_NEW = 0;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ServiceDefinition", inversedBy="editor")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $serviceDefinition;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $step;

    /**
     * @return mixed
     */
    public function getServiceDefinition()
    {
        return $this->serviceDefinition;
    }

    /**
     * @param mixed $serviceDefinition
     * @return ServiceEditor
     */
    public function setServiceDefinition($serviceDefinition)
    {
        $this->serviceDefinition = $serviceDefinition;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param $step
     * @return ServiceEditor
     */
    public function setStep($step)
    {
        $this->step = $step;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    



}