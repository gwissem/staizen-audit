<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="vin_element",
 *     indexes={
 *     @ORM\Index(name="value_idx", columns={"value"}),
 *     @ORM\Index(name="_dta_index_vin_element_8_187863736__K4_K2_K1", columns={"value", "header_id", "id"})
 *     })
 */
class DictionaryElement
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=300)
     */
    protected $value;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\DictionaryHeader", cascade={"remove"})
     * @ORM\JoinColumn(name="header_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $header;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Dictionary", inversedBy="elements", cascade={"remove"})
     * @ORM\JoinColumn(name="dictionary_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $dictionary;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return DictionaryElement
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param mixed $header
     * @return DictionaryElement
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     * @param mixed $dictionary
     * @return DictionaryElement
     */
    public function setDictionary($dictionary)
    {
        $this->dictionary = $dictionary;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return DictionaryElement
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }


}