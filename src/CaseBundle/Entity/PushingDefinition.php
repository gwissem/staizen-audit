<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PushingDefinition
 *
 * @ORM\Table(name="pushing_definition")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\PushingDefinitionRepository")
 */
class PushingDefinition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToOne(targetEntity="CaseBundle\Entity\Step", inversedBy="pushingDefinition", fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $step;

    /**
     * @ORM\OneToOne(targetEntity="CaseBundle\Entity\PushingTemplate", inversedBy="pushingDefinition")
     */
    private $template;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority;

    /**
     * @var int
     *
     * @ORM\Column(name="user_group_id", type="integer")
     */
    private $userGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="step_priority", type="string")
     */
    private $stepPriority;

    /**
     * @return int
     */
    public function getUserGroup(): int
    {
        return $this->userGroup;
    }

    /**
     * @param int $userGroup
     */
    public function setUserGroup(int $userGroup)
    {
        $this->userGroup = $userGroup;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stepId
     *
     * @param integer $stepId
     *
     * @return PushingDefinition
     */
    public function setStep($stepId)
    {
        $this->step = $stepId;

        return $this;
    }

    /**
     * Get stepId
     *
     * @return int
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set templateId
     *
     * @param PushingTemplate $templateId
     *
     * @return PushingDefinition
     */
    public function setTemplate($templateId)
    {
        $this->template = $templateId;

        return $this;
    }

    /**
     * Get templateId
     *
     * @return int
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return PushingDefinition
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return string
     */
    public function getStepPriority(): string
    {
        return $this->stepPriority;
    }

    /**
     * @param string $stepPriority
     */
    public function setStepPriority(string $stepPriority)
    {
        $this->stepPriority = $stepPriority;
    }


}

