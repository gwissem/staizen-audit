<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * ServiceStatus
 *
 * @ORM\Table(name="service_status")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ServiceStatusRepository")
 */
class ServiceStatus
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ProcessInstance
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessInstance")
     * @ORM\JoinColumn(name="group_process_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $groupProcessId;

    /**
     * @var ServiceStatusDictionary
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ServiceStatusDictionary")
     * @ORM\JoinColumn(name="status_dictionary_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $statusDictionaryId;

    /**
     * @var int
     *
     * @ORM\Column(name="serviceId", type="integer")
     */
    private $serviceId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceId
     *
     * @param integer $serviceId
     *
     * @return ServiceStatus
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    /**
     * Get serviceId
     *
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @return ProcessInstance
     */
    public function getGroupProcessId()
    {
        return $this->groupProcessId;
    }

    /**
     * @param ProcessInstance $groupProcessId
     */
    public function setGroupProcessId($groupProcessId)
    {
        $this->groupProcessId = $groupProcessId;
    }

    /**
     * @return ServiceStatusDictionary
     */
    public function getStatusDictionaryId()
    {
        return $this->statusDictionaryId;
    }

    /**
     * @param ServiceStatusDictionary $statusDictionaryId
     */
    public function setStatusDictionaryId($statusDictionaryId)
    {
        $this->statusDictionaryId = $statusDictionaryId;
    }
}

