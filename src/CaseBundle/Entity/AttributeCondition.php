<?php

namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use FOS\UserBundle\Model\User;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use CaseBundle\Validator\Constraints as CaseAssert;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\AttributeConditionRepository")
 */
class AttributeCondition
{
    use ORMBehaviors\Timestampable\Timestampable;

    const TYPE_NOT_REQUIRED = 1;
    const TYPE_REQUIRED = 2;
    const TYPE_SEMI_REQIORED = 3;
    const TYPE_READ_ONLY = 4;
    const TYPE_DEFAULT = self::TYPE_SEMI_REQIORED;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="integer", options={"default" = 1})
     */
    protected $type = self::TYPE_REQUIRED;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $formControlId;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $visible;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $required;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $semiRequired;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $readOnly;

    /**
     * @var string
     * @Column(type="text", nullable=true, name="rules")
     */
    protected $rules;

    /**
     * @var string
     * @Column(type="text", name="not_allow_edit", nullable=true)
     */
    protected $notAllowEdit;



    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id")
     */
    protected $step;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;
    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return AttributeCondition
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormControlId()
    {
        return $this->formControlId;
    }

    /**
     * @param mixed $formControlId
     */
    public function setFormControlId($formControlId)
    {
        $this->formControlId = $formControlId;
    }

    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param mixed $condition
     * @return AttributeCondition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * @return Step
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     */
    public function setStep($step)
    {
        $this->step = $step;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return AttributeCondition
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     * @return AttributeCondition
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     * @return AttributeCondition
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @param mixed $required
     * @return AttributeCondition
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSemiRequired()
    {
        return $this->semiRequired;
    }

    /**
     * @param mixed $semiRequired
     * @return AttributeCondition
     */
    public function setSemiRequired($semiRequired)
    {
        $this->semiRequired = $semiRequired;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReadOnly()
    {
        return $this->readOnly;
    }

    /**
     * @param mixed $readOnly
     * @return AttributeCondition
     */
    public function setReadOnly($readOnly)
    {
        $this->readOnly = $readOnly;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNotAllowEdit()
    {
        return $this->notAllowEdit;
    }

    /**
     * @param mixed $notAllowEdit
     */
    public function setNotAllowEdit($notAllowEdit)
    {
        $this->notAllowEdit = $notAllowEdit;
    }

    /**
     * @return mixed
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param mixed $rules
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
    }


    /** ONE-TO-MANY BIDIRECTIONAL, INVERSE SIDE
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\CustomValidationMessageTranslation", mappedBy="translatable")
     */
    protected $customValidationMessage;





    /**
     * @return CustomValidationMessageTranslation
     */
    public function getCustomValidationMessage()
    {
        $locale = $this->getStep()->getCurrentLocale();
        return $this->customValidationMessage->filter(function ($single) use ($locale)
        {
            /** @var CustomValidationMessageTranslation $single */

            return $single->getLocale() == $locale;
        })->first();
    }




}
