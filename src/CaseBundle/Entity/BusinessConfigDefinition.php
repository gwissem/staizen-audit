<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * BusinessConfigDefinition
 *
 * @ORM\Table(name="business_config_definition")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\BusinessConfigDefinitionRepository")
 */
class BusinessConfigDefinition
{

    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="[key]", type="string", length=255)
     */
    private $key;

    /**
     * @var bool
     *
     * @ORM\Column(name="isProgram", type="boolean", nullable=true)
     */
    private $isProgram;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=64)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="query", type="text", nullable=true)
     */
    private $query;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=100, nullable=true)
     */
    private $category;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     *
     * @return BusinessConfigDefinition
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getName()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $this->translate()->getName();
    }

    /**
     * Set isProgram
     *
     * @param boolean $isProgram
     *
     * @return BusinessConfigDefinition
     */
    public function setIsProgram($isProgram)
    {
        $this->isProgram = $isProgram;

        return $this;
    }

    /**
     * Get isProgram
     *
     * @return bool
     */
    public function isProgram()
    {
        return $this->isProgram;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return BusinessConfigDefinition
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set query
     *
     * @param string $query
     *
     * @return BusinessConfigDefinition
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return BusinessConfigDefinition
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }
}

