<?php
// src/Acme/ApiBundle/Entity/Client.php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="vin_header", indexes={@ORM\Index(name="crcimport", columns={"id", "crc"})})
 */
class DictionaryHeader
{
    const STATUS_NEW = 2;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $query;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $crc;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $disableDate;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $startDate;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $endDate;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\DictionaryPackage", cascade={"remove"})
     * @ORM\JoinColumn(name="package_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $package;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Program", cascade={"remove"})
     * @ORM\JoinColumn(name="program_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $program;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return DictionaryHeader
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return DictionaryHeader
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     * @return DictionaryHeader
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCrc()
    {
        return $this->crc;
    }

    /**
     * @param mixed $crc
     * @return DictionaryHeader
     */
    public function setCrc($crc)
    {
        $this->crc = $crc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     * @return DictionaryHeader
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisableDate()
    {
        return $this->disableDate;
    }

    /**
     * @param mixed $disableDate
     * @return DictionaryHeader
     */
    public function setDisableDate($disableDate)
    {
        $this->disableDate = $disableDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     * @return DictionaryHeader
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     * @return DictionaryHeader
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param mixed $package
     * @return DictionaryHeader
     */
    public function setPackage($package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param mixed $program
     * @return DictionaryHeader
     */
    public function setProgram($program)
    {
        $this->program = $program;

        return $this;
    }

}