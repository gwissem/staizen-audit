<?php

namespace CaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;
use CaseBundle\Validator\Constraints as CaseAssert;

/**
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\StepRepository")
 * @ORM\Table()
 * @UniqueEntity("id")
 * @UniqueEntity(fields={"idSuffix", "processDefinition"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Step
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable,
        FormTemplateMethodsTrait;

    const ALWAYS_ACTIVE = 'xxx';

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=20)
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $idSuffix;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $query;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $isTechnical = false;
    /**
     * @ORM\Column(type="boolean")
     */
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $callSparx;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isFullTimeAccounted = false;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $interval;
    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\Group", inversedBy="steps")
     * @ORM\JoinTable(name="step_permissions",
     *      joinColumns={@ORM\JoinColumn(name="step_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_group_id", referencedColumnName="id")}
     * )
     */
    protected $permittedGroups;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $reassign = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="stop", nullable=true)
     */
    protected $stop = false;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\FormTemplate")
     * @ORM\JoinTable(name="step_form_template",
     *      joinColumns={@ORM\JoinColumn(name="step_id",  referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="form_template_id", referencedColumnName="id")}
     * )
     * @var ArrayCollection $formTemplates ;
     */
    protected $formTemplates;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinColumn(name="return_step_id", referencedColumnName="id")
     */
    private $returnTo;
    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ProcessDefinition", inversedBy="steps")
     * @ORM\JoinColumn(name="process_definition_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $processDefinition;
    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Step")
     * @ORM\JoinTable(name="step_waiting_for_step",
     *      joinColumns={@ORM\JoinColumn(name="step_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="waiting_for_step_id", referencedColumnName="id")}
     * )
     */
    private $waitingForSteps;
    /**
     * @ORM\OneToMany(targetEntity="CaseBundle\Entity\StepAttributeDefinition", mappedBy="step")
     */
    private $attributeDefinitions;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Group")
     * @ORM\JoinColumn(name="owner_group", referencedColumnName="id", onDelete="SET NULL")
     */
    private $owner;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $graphPoints;
    /**
     * @var string
     * @CaseAssert\AttributePath
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stepNameSchema;
    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;
    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;
    /**
     * @ORM\Column(type="integer", options={"default" = 0}, nullable=true)
     */
    private $referenceTime;
    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * ID's kontrolek które są wykluczone z walidacji na danym kroku
     *
     * @var array $excludedAttributes
     *
     * @ORM\Column(name="excluded_form_controls", type="simple_array", nullable=true)
     */
    private $excludedFormControls;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable = true)
     */
    private $postponeCount;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable = true)
     */
    private $defaultDelay = 0;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable = true)
     */
    private $forwardVariant = null;

    /**
     * @ORM\ManyToMany(targetEntity="SparxBundle\Entity\Attribute")
     * @ORM\JoinColumn(referencedColumnName="id")
     * @ORM\JoinTable(name="sparx_attributes_step",
     *      joinColumns={@ORM\JoinColumn(name="step_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="sparx_attribute_id", referencedColumnName="id")}
     *      )
     */
    private $sparxAttributes;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="can_previous", options={"default" = 0}, nullable=true)
     */
    private $canPrevious = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="reopen", options={"default" = 0})
     */
    private $reopen = false;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $priority;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $priorityBoostInterval;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $urgentPriorityBoost;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $maxPriority;

    /**
     * @ORM\ManyToMany(targetEntity="CaseBundle\Entity\Program", inversedBy="steps")
     * @ORM\JoinTable(name="step_program",
     *      joinColumns={@ORM\JoinColumn(name="step_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="program_id", referencedColumnName="id")})
     */
    private $programs;

    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\ApiCall", mappedBy="step", cascade={"persist"})
     */
    private $apiCalls;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="is_mobile_form", options={"default" = 0})
     */
    private $isMobileForm;

    /**
     * @var mixed
     *
     * @ORM\OneToOne(targetEntity="CaseBundle\Entity\PushingDefinition", mappedBy="step", fetch="EAGER")
     */
    private $pushingDefinition;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="is_automatic_closing", options={"default" = 0})
     */
    private $isAutomaticClosing;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->permittedGroups = new ArrayCollection();
        $this->waitingForSteps = new ArrayCollection();
        $this->attributeDefinitions = new ArrayCollection();
        $this->formTemplates = new ArrayCollection();
        $this->sparxAttributes = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Step
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     * @return Step
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsTechnical()
    {
        return $this->isTechnical;
    }

    /**
     * @param mixed $isTechnical
     * @return Step
     */
    public function setIsTechnical($isTechnical)
    {
        $this->isTechnical = $isTechnical;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCallSparx()
    {
        return $this->callSparx;
    }

    /**
     * @param mixed $callSparx
     */
    public function setCallSparx($callSparx)
    {
        $this->callSparx = $callSparx;
    }

    /**
     * @return mixed
     */
    public function getIsFullTimeAccounted()
    {
        return $this->isFullTimeAccounted;
    }

    /**
     * @param mixed $isFullTimeAccounted
     * @return Step
     */
    public function setIsFullTimeAccounted($isFullTimeAccounted)
    {
        $this->isFullTimeAccounted = $isFullTimeAccounted;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReturnTo()
    {
        return $this->returnTo;
    }

    /**
     * @param mixed $returnTo
     * @return Step
     */
    public function setReturnTo($returnTo)
    {
        $this->returnTo = $returnTo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @param mixed $interval
     * @return Step
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWaitingForSteps()
    {
        return $this->waitingForSteps;
    }

    /**
     * @param mixed $waitingForSteps
     * @return Step
     */
    public function setWaitingForSteps($waitingForSteps)
    {
        $this->waitingForSteps = $waitingForSteps;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHoldingStep()
    {
        return $this->holdingStep;
    }

    /**
     * @param mixed $holdingStep
     * @return Step
     */
    public function setHoldingStep($holdingStep)
    {
        $this->holdingStep = $holdingStep;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReassign()
    {
        return $this->reassign;
    }

    /**
     * @param mixed $reassign
     * @return Step
     */
    public function setReassign($reassign)
    {
        $this->reassign = $reassign;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     * @return Step
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPermittedGroups()
    {
        return $this->permittedGroups;
    }

    /**
     * @param mixed $permittedGroups
     * @return Step
     */
    public function setPermittedGroups($permittedGroups)
    {
        $this->permittedGroups = $permittedGroups;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGraphPoints()
    {
        return $this->graphPoints;
    }

    /**
     * @param mixed $graphPoints
     * @return Step
     */
    public function setGraphPoints($graphPoints)
    {
        $this->graphPoints = $graphPoints;

        return $this;
    }

    public function getGraphId()
    {
        return crc32($this->id);
    }

    /**
     * @return string
     */
    public function getStepNameSchema()
    {
        return $this->stepNameSchema;
    }

    /**
     * @param string $stepNameSchema
     */
    public function setStepNameSchema($stepNameSchema)
    {
        $this->stepNameSchema = $stepNameSchema;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->translate()->getName();
    }

    public function getDefinitionName(){
        return '('.$this->id.') '.$this->translate()->getName();
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->translate()->getInfo();
    }

    public function getGraphText()
    {
        return '('.$this->id.') \n'.$this->name;
    }

    public function getOwnerName()
    {
        return $this->owner ? $this->owner->getName() : '';
    }

    /**
     * Add permittedGroup
     *
     * @param \UserBundle\Entity\Group $permittedGroup
     *
     * @return Step
     */
    public function addPermittedGroup(\UserBundle\Entity\Group $permittedGroup)
    {
        $this->permittedGroups[] = $permittedGroup;

        return $this;
    }

    /**
     * Remove permittedGroup
     *
     * @param \UserBundle\Entity\Group $permittedGroup
     */
    public function removePermittedGroup(\UserBundle\Entity\Group $permittedGroup)
    {
        $this->permittedGroups->removeElement($permittedGroup);
    }

    /**
     * Add waitingForStep
     *
     * @return Step
     */
    public function addWaitingForStep($waitingForStep)
    {
        $this->waitingForSteps[] = $waitingForStep;

        return $this;
    }

    /**
     * Remove waitingForStep
     */
    public function removeWaitingForStep($waitingForStep)
    {
        $this->waitingForSteps->removeElement($waitingForStep);
    }

    /**
     * Get createdBy
     *
     * @return \UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param \UserBundle\Entity\User $createdBy
     *
     * @return Step
     */
    public function setCreatedBy(\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \UserBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy
     *
     * @param \UserBundle\Entity\User $updatedBy
     *
     * @return Step
     */
    public function setUpdatedBy(\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getDesignName()
    {
        return (string)$this->getProcessDefinition()->getId().'.'.$this->getIdSuffix();
    }

    /**
     * @return mixed
     */
    public function getProcessDefinition()
    {
        return $this->processDefinition;
    }

    /**
     * @param mixed $processDefinition
     * @return Step
     */
    public function setProcessDefinition($processDefinition)
    {
        $this->processDefinition = $processDefinition;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdSuffix()
    {
        return $this->idSuffix;
    }

    /**
     * @param mixed $idSuffix
     * @return Step
     */
    public function setIdSuffix($idSuffix)
    {
        $this->idSuffix = $this->addNullToIdSuffix($idSuffix);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributeDefinitions()
    {
        return $this->attributeDefinitions;
    }

    /**
     * @param mixed $attributeDefinitions
     * @return Step
     */
    public function setAttributeDefinitions($attributeDefinitions)
    {
        $this->attributeDefinitions = $attributeDefinitions;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return Step
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getFullProcessName()
    {
        return $this->id.' - '.$this->getName();
    }

    /**
     * @return mixed
     */
    public function getReferenceTime()
    {
        return $this->referenceTime;
    }

    /**
     * @param mixed $referenceTime
     * @return Step
     */
    public function setReferenceTime($referenceTime)
    {
        $this->referenceTime = $referenceTime;
        return $this;
    }


    /**
     * @param $excludedFormControl
     * @return $this
     */
    public function addExcludedFormControl($excludedFormControl)
    {
        if (!in_array($excludedFormControl, $this->excludedFormControls, true)) {
            $this->excludedFormControls[] = $excludedFormControl;
        }

        return $this;
    }

    /**
     * @param $excludedFormControl
     * @return $this
     */
    public function removeExcludedFormControl($excludedFormControl)
    {
        if (false !== $key = array_search($excludedFormControl, $this->excludedFormControls, true)) {
            unset($this->excludedFormControls[$key]);
            $this->excludedFormControls = array_values($this->excludedFormControls);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getExcludedFormControls()
    {
        return $this->excludedFormControls;
    }

    /**
     * @param array $excludedFormControls
     * @return $this
     */
    public function setExcludedFormControls(array $excludedFormControls)
    {
        $this->excludedFormControls = array();

        foreach ($excludedFormControls as $excludedFormControl) {
            $this->addExcludedFormControl($excludedFormControl);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isStop()
    {
        return $this->stop;
    }

    /**
     * @param bool $stop
     */
    public function setStop(bool $stop)
    {
        $this->stop = $stop;
    }

    /**
     * @return mixed
     */
    public function getPostponeCount()
    {
        return $this->postponeCount;
    }

    /**
     * @param mixed $postponeCount
     * @return Step
     */
    public function setPostponeCount($postponeCount): Step
    {
        $this->postponeCount = $postponeCount;
        return $this;
    }

    /**
     * @return boolean
     */
    public function canPrevious()
    {
        return $this->canPrevious;
    }

    /**
     * @param boolean $canPrevious
     */
    public function setCanPrevious($canPrevious)
    {
        $this->canPrevious = $canPrevious;
    }

    /**
     * @return mixed
     */
    public function getSparxAttributes()
    {
        return $this->sparxAttributes;
    }

    /**
     * @param mixed $sparxAttributes
     */
    public function setSparxAttributes($sparxAttributes)
    {
        $this->sparxAttributes = $sparxAttributes;
    }

    /**
     * @return int
     */
    public function getDefaultDelay()
    {
        return $this->defaultDelay;
    }

    /**
     * @param int $defaultDelay
     * @return Step
     */
    public function setDefaultDelay($defaultDelay)
    {
        $this->defaultDelay = $defaultDelay;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReopen()
    {
        return $this->reopen;
    }

    /**
     * @param bool $reopen
     */
    public function setReopen($reopen)
    {
        $this->reopen = $reopen;
    }

    public function addNullToIdSuffix($idSuffix)
    {
        if (strlen($idSuffix) == 1) {
            return '00'.$idSuffix;
        }
        elseif(strlen($idSuffix) == 2) {
            return '0'.$idSuffix;
        }

        return $idSuffix;
    }

    /**
     * @return int
     */
    public function getForwardVariant()
    {
        return $this->forwardVariant;
    }

    /**
     * @param int $forwardVariant
     * @return Step
     */
    public function setForwardVariant($forwardVariant)
    {
        $this->forwardVariant = $forwardVariant;
        return $this;
    }

    public function getForwardVariantInfo(){
        return $this->translate()->getForwardVariantInfo();
    }

    /**
     * @return ArrayCollection
     */
    public function getFormTemplates()
    {
        return $this->formTemplates;
    }

    /**
     * @param ArrayCollection $formTemplates
     * @return Step
     */
    public function setFormTemplates($formTemplates)
    {
        $this->formTemplates = $formTemplates;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     * @return Step
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriorityBoostInterval()
    {
        return $this->priorityBoostInterval;
    }

    /**
     * @param mixed $priorityBoostInterval
     * @return Step
     */
    public function setPriorityBoostInterval($priorityBoostInterval)
    {
        $this->priorityBoostInterval = $priorityBoostInterval;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxPriority()
    {
        return $this->maxPriority;
    }

    /**
     * @param mixed $maxPriority
     * @return Step
     */
    public function setMaxPriority($maxPriority)
    {
        $this->maxPriority = $maxPriority;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrgentPriorityBoost()
    {
        return $this->urgentPriorityBoost;
    }

    /**
     * @param mixed $urgentPriorityBoost
     * @return Step
     */
    public function setUrgentPriorityBoost($urgentPriorityBoost)
    {
        $this->urgentPriorityBoost = $urgentPriorityBoost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * @param mixed $programs
     * @return Step
     */
    public function setPrograms($programs)
    {
        $this->programs = $programs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApiCalls()
    {
        return $this->apiCalls;
    }

    /**
     * @param mixed $apiCalls
     * @return Step
     */
    public function setApiCalls($apiCalls)
    {
        $this->apiCalls = $apiCalls;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMobileForm()
    {
        return $this->isMobileForm;
    }

    /**
     * @param bool $isMobileForm
     */
    public function setIsMobileForm($isMobileForm)
    {
        $this->isMobileForm = $isMobileForm;
    }

    /**
     * @return mixed
     */
    public function getPushingDefinition()
    {
        return $this->pushingDefinition;
    }

    /**
     * @param mixed $pushingDefinition
     */
    public function setPushingDefinition($pushingDefinition)
    {
        $this->pushingDefinition = $pushingDefinition;
    }

    /**
     * @return bool
     */
    public function isAutomaticClosing(): bool
    {
        return $this->isAutomaticClosing;
    }

    /**
     * @param bool $isAutomaticClosing
     */
    public function setIsAutomaticClosing(bool $isAutomaticClosing)
    {
        $this->isAutomaticClosing = $isAutomaticClosing;
    }
}
