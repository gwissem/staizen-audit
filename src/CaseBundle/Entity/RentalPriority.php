<?php


namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 */
class RentalPriority
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="integer")
     */
    protected $priority;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $programs;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\RentalPriceListGroup")
     * @ORM\JoinColumn(name="price_list_group_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $priceListGroup;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     * @return RentalPriority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * @param mixed $programs
     * @return RentalPriority
     */
    public function setPrograms($programs)
    {
        $this->programs = $programs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRentalGroup()
    {
        return $this->rentalGroup;
    }

    /**
     * @param mixed $rentalGroup
     * @return RentalPriority
     */
    public function setRentalGroup($rentalGroup)
    {
        $this->rentalGroup = $rentalGroup;
        return $this;
    }



}