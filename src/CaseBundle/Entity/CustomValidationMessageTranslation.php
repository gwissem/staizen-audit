<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 *
 * @ORM\Table(name="custom_validation_message_translation", uniqueConstraints={@ORM\UniqueConstraint(name="unique_id_name_locale", columns={"translatable_id", "locale", "message"})})
 *
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\CustomValidationMessageTranslationRepository")
 */
class CustomValidationMessageTranslation
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;



    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\AttributeCondition")
     * @ORM\Column(name="translatable_id", type="integer")
     */
    protected $translatable;


    /**
     * @ORM\Column(name="locale",type="string",length=3)
     */
    protected $locale;


    /**
     * @var string
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    protected $message = null;

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTranslatable()
    {
        return $this->translatable;
    }

    /**
     * @param mixed $translatable
     */
    public function setTranslatable($translatable)
    {
        $this->translatable = $translatable;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }




}

