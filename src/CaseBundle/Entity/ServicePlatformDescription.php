<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServicePlatformDescription
 *
 * @ORM\Table(name="service_platform_description")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ServicePlatformDescriptionRepository")
 */
class ServicePlatformDescription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Platform")
     * @ORM\JoinColumn(name="platform_id", referencedColumnName="id")
     */
    private $platform;


    /**
     * @ORM\ManyToOne(targetEntity="ServiceDefinition")
     * @ORM\JoinColumn(name="service_definition_id", referencedColumnName="id")
     */
    private $serviceDefinition;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }

    /**
     * @return mixed
     */
    public function getServiceDefinition()
    {
        return $this->serviceDefinition;
    }

    /**
     * @param mixed $serviceDefinition
     */
    public function setServiceDefinition($serviceDefinition)
    {
        $this->serviceDefinition = $serviceDefinition;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ServicePlatformDescription
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}

