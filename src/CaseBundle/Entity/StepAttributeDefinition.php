<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use UserBundle\Entity\User;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class StepAttributeDefinition
{

    use ORMBehaviors\Timestampable\Timestampable;

    const UPDATABLE = 1;
    const UPDATABLE_NOT = 0;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $attributePath;

    /**
     * @ORM\Column(type="integer", options={"default"=1})
     */
    protected $updatable = self::UPDATABLE;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Step", inversedBy="attributeDefinitions")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id")
     */
    private $step;

    /**
     * @var Attribute
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Attribute", inversedBy="stepAttributeDefinitions")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $attribute;

    /**
     * @var Attribute
     *
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\Attribute")
     * @ORM\JoinColumn(name="parent_attribute_id", referencedColumnName="id")
     */
    private $parentAttribute;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var User $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $updatedBy;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     * @return StepAttributeDefinition
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * @return Attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param mixed $attribute
     * @return StepAttributeDefinition
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return Attribute
     */
    public function getParentAttribute()
    {
        return $this->parentAttribute;
    }

    /**
     * @param Attribute $parentAttribute
     * @return StepAttributeDefinition
     */
    public function setParentAttribute($parentAttribute)
    {
        $this->parentAttribute = $parentAttribute;

        return $this;
    }


    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return StepAttributeDefinition
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     * @return StepAttributeDefinition
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     * @return SoftDeletableProperties
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatable()
    {
        return $this->updatable;
    }

    /**
     * @param mixed $updatable
     * @return StepAttributeDefinition
     */
    public function setUpdatable($updatable)
    {
        $this->updatable = $updatable;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributePath()
    {
        return $this->attributePath;
    }

    /**
     * @param mixed $attributePath
     * @return StepAttributeDefinition
     */
    public function setAttributePath($attributePath)
    {
        $this->attributePath = $attributePath;
        return $this;
    }



}