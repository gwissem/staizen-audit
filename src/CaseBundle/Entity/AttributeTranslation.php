<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tool
 *
 * @ORM\Table(name="attribute_translation", uniqueConstraints={@ORM\UniqueConstraint(name="unique_name_locale", columns={"locale", "name"})})
 * @ORM\Entity()
 * @UniqueEntity(fields={"locale", "name"})
 */
class AttributeTranslation
{
    use ORMBehaviors\Translatable\Translation,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @Assert\NotBlank()
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="validation_description", type="string", length=255, nullable=true)
     */
    private $validationDescription;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AttributeTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getValidationDescription()
    {
        return $this->validationDescription;
    }

    /**
     * @param string $validationDescription
     */
    public function setValidationDescription($validationDescription)
    {
        $this->validationDescription = $validationDescription;
    }

}
