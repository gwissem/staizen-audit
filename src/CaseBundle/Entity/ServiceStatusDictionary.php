<?php

namespace CaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * ServiceStatusDictionary
 *
 * @ORM\Table(name="service_status_dictionary")
 * @ORM\Entity(repositoryClass="CaseBundle\Repository\ServiceStatusDictionaryRepository")
 */
class ServiceStatusDictionary
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="progress", type="integer", nullable=true)
     */
    private $progress;

    /**
     * @var ServiceDefinition
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\ServiceDefinition")
     * @ORM\JoinColumn(name="service", referencedColumnName="id", onDelete="SET NULL")
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="sparxStatus", type="string", length=64, nullable=true)
     */
    private $sparxStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="sparx_update", type="text", nullable=true)
     */
    private $sparxUpdate;

    /**
     * @var int
     *
     * @ORM\Column(name="service_report", type="integer", nullable=true)
     */
    private $serviceReport;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set progress
     *
     * @param integer $progress
     *
     * @return ServiceStatusDictionary
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * Get progress
     *
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * Set sparxStatus
     *
     * @param string $sparxStatus
     *
     * @return ServiceStatusDictionary
     */
    public function setSparxStatus($sparxStatus)
    {
        $this->sparxStatus = $sparxStatus;

        return $this;
    }

    /**
     * Get sparxStatus
     *
     * @return string
     */
    public function getSparxStatus()
    {
        return $this->sparxStatus;
    }

    /**
     * @return ServiceDefinition
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param ServiceDefinition $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->translate()->getMessage();
    }

    /**
     * @param string $message
     * @return ServiceStatusDictionary
     */
    public function setMessage($message): ServiceStatusDictionary
    {
        $this->translate()->setMessage($message);
        return $this;
    }

    /**
     * @return int
     */
    public function getServiceReport()
    {
        return $this->serviceReport;
    }

    /**
     * @param int $serviceReport
     */
    public function setServiceReport($serviceReport)
    {
        $this->serviceReport = $serviceReport;
    }

    /**
     * @return string
     */
    public function getSparxUpdate()
    {
        return $this->sparxUpdate;
    }

    /**
     * @param string $sparxUpdate
     * @return ServiceStatusDictionary
     */
    public function setSparxUpdate($sparxUpdate)
    {
        $this->sparxUpdate = $sparxUpdate;
        return $this;
    }


}

