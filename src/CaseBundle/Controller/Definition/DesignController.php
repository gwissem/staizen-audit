<?php

namespace CaseBundle\Controller\Definition;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeCondition;
use CaseBundle\Entity\CustomValidationMessageTranslation;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Program;
use CaseBundle\Entity\Step;
use CaseBundle\Entity\StepAttributeDefinition;
use CaseBundle\Form\Type\AttributeConditionType;
use CaseBundle\Form\Type\DeployRequestType;
use CaseBundle\Repository\CustomValidationMessageTranslationRepository;
use Doctrine\ORM\EntityNotFoundException;
use ManagementBundle\Entity\DevLog;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DesignController extends Controller
{
    /**
     * @Route("/chart/{id}/{programId}", name="admin_process_design_chart", requirements={"id": "\d+"}, options={"expose" : true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @ParamConverter("processDefinition", class="CaseBundle:ProcessDefinition")
     */
    public function chartAction(Request $request, $processDefinition, $programId = NULL)
    {
        if (!$processDefinition) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono definicji procesu'));
        }

        $chart = $this->get('case.process_chart_builder')->build($processDefinition, $programId);
        $programs = $this->getDoctrine()->getRepository(Program::class)->findAll();

        $requestDeployForm = $this->createForm(DeployRequestType::class);

        return $this->render(
            'CaseBundle:Admin/design:chart.html.twig',
            array(
                'chart' => $chart,
                'programs' => $programs,
                'requestDeployForm' => $requestDeployForm->createView(),
                'processDefinition' => $processDefinition,
                'programId' => $programId
            )
        );
    }

    /**
     * @Route(
     *     "/attrCond/{step}/{formControlId}",
     *     name="admin_attribute_condition",
     *     requirements={
     *         "step": "\d+\.\d+",
     *         "formControlId": "\d+"
     *     },
     *     options={"expose":true}
     * )
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @param Request $request
     * @param string $step
     * @param string $formControlId
     * @return JsonResponse|Response
     */
    public function attributeConditionAction(Request $request, $step, $formControlId)
    {

//        if($request->isXmlHttpRequest() && $request->isMethod('POST')) {
//            if(empty($step) || empty($formControlId)) {
//                return new JsonResponse([
//                    'msg' => 'Brak wymaganych parametrów',
//                    'title' => 'Błąd zapisu'
//                ], 403);
//            }
//        }

        $this->getDoctrine()->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);

        $attributeCondition = $this->getDoctrine()->getRepository('CaseBundle:AttributeCondition')->findOneBy(
            ['step' => $step, 'formControlId' => $formControlId]
        );

        if (!$attributeCondition) {
            $attributeCondition = new AttributeCondition();
        }

        /**
         *  Pobranie ostatniego ProcessInstance dla parsowania
         */

        /** @var ProcessInstance[] $processInstances */
        $processInstances = $this->getDoctrine()->getRepository(ProcessInstance::class)->findBy([], [
            'id' => 'DESC'
        ], 1);


        $this->get('case.attribute_parser.service')->setGroupProcessInstanceId($processInstances[0]->getId());
        $this->get('case.attribute_parser.service')->setProcessInstanceId($processInstances[0]->getId());


        $validationMessage = $this->getDoctrine()->getRepository('CaseBundle:CustomValidationMessageTranslation')->findOneBy(
            [
                'translatable' => $attributeCondition->getId(),
                'locale'=> $request->getLocale()
            ]
        );

        if($validationMessage)
        {

            $validationMessage = $validationMessage->getMessage();
        }


        $form = $this->createForm(AttributeConditionType::class, $attributeCondition, [
            'attributeParser' => $this->get('case.attribute_parser.service'),
            'validationMessage' =>$validationMessage
        ]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $stepEntity = $em->getRepository(Step::class)->find($step);
            $attributeCondition->setStep($stepEntity);
            $attributeCondition->setFormControlId($formControlId);
            $validationMessage= $form->get('customValidationMessage')->getData();

            /** @var CustomValidationMessageTranslationRepository $validationMessageRepo */
            $validationMessageRepo = $em->getRepository('CaseBundle:CustomValidationMessageTranslation');



            $devLog = DevLog::UPDATE_CONTROL_CONDITION($attributeCondition);
            $em->persist($devLog);
            $em->persist($attributeCondition);
            $em->flush();
            $em->refresh($attributeCondition);
            $validationMessageRepo->saveOrNew($stepEntity->getCurrentLocale(), $attributeCondition, $validationMessage);

            /** Usunięcie cache */
            $redisCache = $this->get('doctrine.orm.default_result_cache');
            $key = 'attr_condition_'.$formControlId.'_'.$stepEntity->getId();

            if($redisCache->contains($key)) {
                $redisCache->delete($key);
            }

        }

        if ($request->isXmlHttpRequest()) {

            $errors = $form->getErrors(true);

            $isSuccess = ($errors->count() === 0);

            return new JsonResponse([
                'success' => $isSuccess,
                'form' => $this->renderView('@Case/Admin/design/attribute-conditional-form.html.twig',
                    [
                        'form' => $form->createView(),
                        'stepId' => $step,
                        'formControlId' => $formControlId
                    ])
            ]);

        } else {

            return $this->render(
                'CaseBundle:Admin/design:attribute-conditional-edit.html.twig',
                [
                    'form' => $form->createView(),
                ]
            );

        }

    }

    /**
     * @Route(
     *     "/attr-cond/{step}/{formControlId}/{isVisible}",
     *     name="admin_attribute_condition_visible",
     *     requirements={
     *         "step": "\d+\.\d+",
     *         "formControlId": "\d+"
     *     },
     *     options={"expose":true}
     * )
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @param Request $request
     * @param string $step
     * @param string $formControlId
     * @param $isVisible
     * @return JsonResponse|Response
     */
    public function attributeConditionVisibleAction(Request $request, $step, $formControlId, $isVisible)
    {

        $attributeCondition = $this->getDoctrine()->getRepository('CaseBundle:AttributeCondition')->findOneBy(
            ['step' => $step, 'formControlId' => $formControlId]
        );

        $em = $this->getDoctrine()->getManager();

        if (!$attributeCondition) {
            $attributeCondition = new AttributeCondition();
            $step = $em->getRepository(Step::class)->find($step);
            $attributeCondition->setStep($step);
            $attributeCondition->setFormControlId($formControlId);
        }

        $attributeCondition->setVisible($isVisible);

        $em->persist($attributeCondition);
        $em->flush();

        return new JsonResponse([
            'status' => "ok"
        ]);

    }

    /**
     * @Route("/chart/save/{id}", name="admin_process_chart_save", requirements={"id": "\d+"}, options={"expose" = true})
     * @Method({"POST"})
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @ParamConverter("processDefinition", class="CaseBundle:ProcessDefinition")
     */
    public function chartSaveAction(Request $request, $processDefinition)
    {

        if (!$processDefinition) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono definicji procesu'));
        }
        $data = json_decode($request->request->get('diagramData'));
        $this->get('case.process_chart_builder')->save($data, $processDefinition);

        return new Response();
    }

    /**
     * @Route("/attribute-chart/{id}", name="admin_attribute_chart", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @param null $id
     * @return Response
     */
    public function attributeChartAction(Request $request, $id = null)
    {
        $attributeRepository = $this->getDoctrine()->getRepository(Attribute::class);
        $attributeDefinition = null;
        if ($id) {
            $attributeDefinition = $attributeRepository->find($id);
        }

        $chart = $this->get('case.attribute_chart_builder')->build($attributeDefinition);
        $rootAttributes = $attributeRepository->fetchRootAttributes($request->getLocale());

        //$rootAttributes = $attributeRepository->fetchAllAttributes();

        return $this->render(
            'CaseBundle:Admin/design:attribute-chart.html.twig',
            array(
                'chart' => $chart,
                'attribute' => $attributeDefinition,
                'rootAttributes' => $rootAttributes,
            )
        );
    }

    /**
     * @Route("/attribute-diagram-model/{id}", name="admin_attribute_diagram_model", options={"expose" = true})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @Method("POST")
     * @ParamConverter("attribute", class="CaseBundle:Attribute")
     * @param Request $request
     * @param $attribute
     * @return Response
     */
    public function attributeDiagramModelAction(Request $request, $attribute)
    {
        $diagramData = $request->request->get('diagramData', false);
        if ($diagramData) {
            $data = json_decode($request->request->get('diagramData'));

            if (!$attribute) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono atrybutu'));
            }
            $model = $this->get('case.attribute_chart_builder')->merge($data, $attribute);
        } else {
            $model = $this->get('case.attribute_chart_builder')->build($attribute);
        }
        return new JsonResponse($model);
    }

    /**
     * @Route("/attribute-chart-save", name="admin_attribute_chart_save", options={"expose" = true})
     * @Method({"POST"})
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @param Request $request
     * @return Response
     */
    public function attributeChartSaveAction(Request $request)
    {
        $data = json_decode($request->request->get('diagramData'));
        $this->get('case.attribute_chart_builder')->save($data);

        return new Response();
    }

    /**
     * @Route("/step-attributes-chart/{id}/{attributeId}", name="admin_step_attribute_chart", options={"expose" = true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @param Request $request
     * @param $step
     * @param null $attributeId
     * @return Response
     * @throws EntityNotFoundException* @ParamConverter("step", class="CaseBundle:Step")
     */
    public function stepAttributesChartAction(Request $request, $step, $attributeId = null)
    {
        $attributeRepository = $this->getDoctrine()->getRepository(Attribute::class);
        $attributeDefinition = null;
        $processDefinitionAttributes = null;
        $processId = $request->query->get('processId', null);
        $rootAttributes = $attributeRepository->fetchRootAttributes($request->getLocale());

        if (!$step) {
            throw new EntityNotFoundException();
        }
        if ($attributeId) {
            $attributeDefinition = $attributeRepository->find($attributeId);
        }
        $chart = $this->get('case.attribute_chart_builder')->build($attributeDefinition);

        return $this->render(
            'CaseBundle:Admin/design:step-attributes-chart.html.twig',
            array(
                'chart' => $chart,
                'attribute' => $attributeDefinition,
                'rootAttributes' => $rootAttributes,
                'step' => $step,
                'processId' => $processId,
            )
        );
    }

    /**
     * @Route("/step-attributes/add/{stepId}", name="admin_step_attribute_add", options={"expose" = true})
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @Method({"POST"})
     * @param Step $step
     * @return Response
     * @internal param Request $request
     * @ParamConverter("step", class="CaseBundle:Step", options={"mapping": {"stepId" : "id"}})
     */
    public function stepAttributeAddAction(Request $request, Step $step)
    {
        $attributes = $request->request->get('attributes', []);

        if (!$step || empty($attributes)) {
            throw new NotFoundHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');

        $stepAttributesArray = $this->get('case.process_definition_attributes_handler')->attachAttributesToStep(
            $step,
            $attributes
        );

        return new JsonResponse($stepAttributesArray);
    }

    /**
     * @Route("/step-attributes/save/{id}", name="admin_step_attribute_save", requirements={"id": "\d+"}, options={"expose" = true})
     * @Method({"POST"})
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @param Request $request
     * @ParamConverter("step", class="CaseBundle:Step")
     * @return Response
     */
    public
    function stepAttributeSaveAction(
        Request $request,
        $step
    ) {
        $attributeRepository = $this->getDoctrine()->getRepository(Attribute::class);
        $attributeDefinition = null;
        if (!$step) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse('OK');
    }



}
