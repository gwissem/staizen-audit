<?php

namespace CaseBundle\Controller\Definition;

use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Form\Type\ProcessDefinitionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefinitionController extends Controller
{
    /**
     * @Route("/", name="admin_process_definition_index", options={"expose" = true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $em = $this->getDoctrine()->getManager();
        $perPage = 40;

        if ($request->isXmlHttpRequest()) {

            $processList = $em->getRepository('CaseBundle:ProcessDefinition')->findAll();

            $arr = [];

            /** @var ProcessDefinition $item */
            foreach ($processList as $item) {
                $arr[] = [
                    'id' => $item->getId(),
                    'text' => $item->getName()
                ];
            }

            return new JsonResponse([
                'results' => $arr
            ]);
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('CaseBundle:ProcessDefinition')->findAll(),
            $page,
            $perPage
        );

        return $this->render(
            'CaseBundle:Admin:definition/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @Route("/editor/{id}", name="admin_process_definition_editor", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     */

    public function editorAction(Request $request, $id = null)
    {


        $em = $this->getDoctrine()->getManager();

        if ($id) {
            $entity = $em->getRepository('CaseBundle:ProcessDefinition')->find($id);
            if (!$entity) {
                throw $this->createNotFoundException(
                    $this->get('translator')->trans('Nie znaleziono definicji procesu')
                );
            }
        } else {
            $entity = new ProcessDefinition();
        }

        $form = $this->createForm(
            ProcessDefinitionType::class,
            $entity,
            ['locale' => $request->getLocale(), 'id' => $id]
        );

        $form->setData($entity);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Definicja procesu "%name%" została pomyślnie utworzona',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_process_design_chart', ['id' => $entity->getId()]));
        }

        return $this->render(
            'CaseBundle:Admin/definition:editor.html.twig',
            array(
                'form' => $form->createView(),
                'entity' => $entity,
            )
        );

    }

    /**
     * @Route("/delete/{id}", name="admin_process_definition_delete")
     * @Method({"GET"})
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('CaseBundle:ProcessDefinition')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono definicji procesu'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Definicja procesu "%name%" została pomyślnie usunięta',
                ['%name%' => $entity->getName()]
            )
        );

        return $this->redirect($this->generateUrl('admin_process_definition_index'));

    }

    /**
     * @Route("/{id}/object-definition", name="admin_process_definition_object_index", requirements={"id": "\d+"})
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @ParamConverter("processDefinition", class="CaseBundle:ProcessDefinition")
     */
    public function processObjectDefinitionIndexAction(Request $request, $processDefinition, $page = 1)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();
        $perPage = 20;

        $pagination = $this->get('knp_paginator')->paginate(
            $processDefinition ? $em->getRepository('CaseBundle:ProcessObjectDefinition')->findBy(
                ['processDefinition' => $processDefinition]
            ) : $em->getRepository('CaseBundle:ProcessObjectDefinition')->findAll(),
            $page,
            $perPage
        );

        return $this->render(
            'CaseBundle:Admin:definition/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

}
