<?php

namespace CaseBundle\Controller\Definition;

use CaseBundle\Entity\BusinessConfig;
use CaseBundle\Entity\BusinessConfigDefinition;
use CaseBundle\Entity\Platform;
use CaseBundle\Utils\BusinessConfigTab;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use ToolBundle\Entity\Tool;

/**
 * @Route("/business-config")
 */
class BusinessConfigController extends Controller
{
    /**
     * @Route("/", name="business_config_index")
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $platforms = $this->getDoctrine()->getRepository('CaseBundle:Platform')->findBy([
            'active' => 1
        ]);

        $allPrograms = $this->getDoctrine()->getRepository('CaseBundle:Program')->findActivePrograms();

        $categories = $this->get('app.query_manager')->executeProcedure('SELECT category FROM AtlasDB_def.dbo.business_config_definition group by category');

        return $this->render(
            'CaseBundle:Admin/BusinessConfig:index.html.twig',
            [
                'KeyCategories' => $categories,
                'platforms' => $platforms,
                'allPrograms' => $allPrograms
            ]
        );
    }

    /**
     * @Route("/get-programs/{platform}", name="business_config_get_programs", options = { "expose" = true })
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @ParamConverter("platform", class="CaseBundle:Platform")
     * @param Platform $platform
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProgramsAction(Platform $platform)
    {

        $programs = $this->getDoctrine()->getRepository('CaseBundle:Program')->findBy([
            'platform' => $platform,
            'status' => 1
        ]);

        $programsArray = [];
        $programsArray[] = ['id' => '', 'text' => ''];

        foreach ($programs as $program) {
            $programsArray[] = [
                'id' => $program->getId(),
                'text' => $program->getName()
            ];
        }

        return new JsonResponse([
            'options' => $programsArray
        ]);
    }

    /**
     * @Route("/render-content/{platform}/{programId}",
     *     defaults={"program": 0},
     *     name="business_config_render_content",
     *     options = { "expose" = true })
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @ParamConverter("platform", class="CaseBundle:Platform")
     * @param Platform $platform
     * @param null $programId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderContentAction(Platform $platform, $programId = null)
    {

        $businessConfigService = $this->get('case.business.config.service');

        /** @var BusinessConfig[] $configs */
        $configs = $this->getDoctrine()->getRepository('CaseBundle:BusinessConfig')->findConfigs(
            $platform->getId(),
            $programId
        );

        if($programId) {

            $platformConfig = $this->getDoctrine()
                ->getRepository('CaseBundle:BusinessConfig')
                ->getPlatformConfigs($platform->getId());

            if(!empty($platformConfig)) {

                foreach ($configs as $config) {
                    if(isset($platformConfig[$config->getConfigDefinition()->getId()])) {
                        $config->parentValue = $platformConfig[$config->getConfigDefinition()->getId()];
                    }
                }

            }
        }

        /** @var BusinessConfigTab[] $tabs */
        $tabs = $businessConfigService->aggregateConfigs($configs);

        return $this->render(
            'CaseBundle:Admin/BusinessConfig:tabs.html.twig',
            [
                'tabs' => $tabs
            ]
        );

    }

    /**
     * @Route("/refresh-configs",
     *     name="business_config_refresh",
     *     options = { "expose" = true })
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function refreshConfigAction()
    {

        $businessConfigService = $this->get('case.business.config.service');
        list($platformConfig, $programConfig) = $businessConfigService->updatePlatformsConfigs();

        return new JsonResponse([
            'platforms' => $platformConfig,
            'programs' => $programConfig
        ]);

    }

    /**
     * @Route("/add-new-key",
     *     name="business_config_key_add",
     *     options = { "expose" = true })
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addConfigKeyAction(Request $request)
    {
        $keyDetails  = $request->query->all();


        $businessConfigService = $this->get('case.business.config.service');
        $businessConfigService->addNewKey($keyDetails);


        return new JsonResponse([
            'status' =>'ok'
        ]);

    }

    /**
     * @Route("/save-config/{businessConfig}",
     *     name="business_config_save",
     *     options = { "expose" = true })
     * @Method("POST")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @param Request $request
     * @param $businessConfig
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("businessConfig", class="CaseBundle:BusinessConfig")
     */
    public function saveConfigAction(Request $request, $businessConfig)
    {

        if(!empty($businessConfig)) {

            $type = $request->get('type', 'value');

            if($type === "value") {
                $this->get('case.business.config.service')->saveConfig($businessConfig, $request->request->get('value', null));
            }
            elseif($type === "description") {
                $this->get('case.business.config.service')->saveDescription($businessConfig, $request->request->get('value', null));
            }

        }

        return new JsonResponse([
            'status' => true
        ]);

    }

    /**
     * @Route("/copy-configs",
     *     name="business_config_copy",
     *     options = { "expose" = true })
     * @Method("POST")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function copyConfigsAction(Request $request)
    {

        $data = $request->request->all();

        if(empty($data['config-source']) || empty($data['config-target'])) {
            return new JsonResponse([
                'msg' => 'Brak wymaganych danych'
            ], 400);
        }

        if($data['config-source'] == $data['config-target']) {

            return new JsonResponse([
                'status' => false,
                'message' => 'Wybrano te same programy.'
            ]);

        }

        $source = $this->getDoctrine()->getRepository('CaseBundle:Program')->find(intval($data['config-source']));
        $target = $this->getDoctrine()->getRepository('CaseBundle:Program')->find(intval($data['config-target']));

        $copyPlatform = (isset($data['config-copy-platform'])) ? filter_var($data['config-copy-platform'], FILTER_VALIDATE_BOOLEAN) : false;

        $configServiceAmount = $this->get('case.business.config.service')->copyConfigsProgramToProgram($source, $target, $copyPlatform);

        return new JsonResponse([
            'status' => true,
            'message' => 'Skopiowano ' . $configServiceAmount . ' kluczy.'
        ]);

    }

}
