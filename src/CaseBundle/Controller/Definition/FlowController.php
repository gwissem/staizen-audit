<?php

namespace CaseBundle\Controller\Definition;

use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessFlow;
use CaseBundle\Entity\Step;
use CaseBundle\Form\Type\ProcessFlowType;
use CaseBundle\Form\Type\StepType;
use ManagementBundle\Entity\DevLog;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;

class FlowController extends Controller
{

    /**
     * @Route("/flow/edit/{processId}/{id}", name="admin_case_process_flow_editor", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     *
     */
    public function flowEditor(Request $request, $processId, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id) {
            $flow = $em->getRepository('CaseBundle:ProcessFlow')->find($id);
        } else {
            $flow = new ProcessFlow();
        }

        $processDefinition = $em->getRepository('CaseBundle:ProcessDefinition')->find($processId);
        $flow->setProcessDefinition($processDefinition);
        $form = $this->createForm(
            ProcessFlowType::class,
            $flow,
            ['processDefinitionId' => $processId]
        );

        $errors = false;
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $this->container->get('security.authorization_checker')->isGranted(
                'ROLE_PROCESS_EDITOR'
            )) {

            if ($form->isValid()) {

                if($flow->getId()) {
                    $devLog = DevLog::UPDATED_FLOW($flow);
                    $em->persist($devLog);
                }

                $em->persist($flow);
                $em->flush();

            } else {
                $errors = $this->get('app.form_processing')->getFormErrors($form);
            }

            return new JsonResponse(['errors' => $errors, 'id' => $flow->getId()]);
        }

        $template = $request->isXmlHttpRequest(
        ) ? 'CaseBundle:Admin:flow/edit-form.html.twig' : 'CaseBundle:Admin:flow/edit.html.twig';

        return $this->render(
            $template,
            [
                'form' => $form->createView(),
                'processId' => $processId,
                'id' => $id,
            ]
        );

    }

}
