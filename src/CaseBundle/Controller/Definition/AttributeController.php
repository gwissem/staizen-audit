<?php

namespace CaseBundle\Controller\Definition;

use AppBundle\Entity\ValueDictionary;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Form\Type\AttributeType;
use CaseBundle\Form\Type\StepAttributeDefinitionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AttributeController extends Controller
{

    /**
     * @Route("/attribute/edit/{id}", name="admin_attribute_editor", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     *
     */
    public function attributeEditor(Request $request, $id = null)
    {

        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');
        $attributeStepDefinitions = null;

        if ($id) {
            $attribute = $em->getRepository(Attribute::class)->find($id);
            $attributeStepDefinitions = $this->get('case.process_handler')->attributeStepDefinitions($id);
        } else {
            $attribute = new Attribute();
        }

        $form = $this->createForm(
            AttributeType::class,
            $attribute,
            ['types' => $em->getRepository(ValueDictionary::class)->getAttributeTypes()]
        );

        $errors = false;
        $form->handleRequest($request);
        if ($request->isMethod('POST')) {
            if ($form->isValid() && $this->container->get('case.validator')->isValid($form)) {
                $em->persist($attribute);
                $em->flush();

                $id = $attribute->getId();

            } else {
                $errors = $this->get('app.form_processing')->getFormErrors($form);
                $id = null;
            }

            return new JsonResponse(['errors' => $errors, 'id' => $id]);
        }

        $template = $request->isXmlHttpRequest(
        ) ? 'CaseBundle:Admin:attribute/edit-form.html.twig' : 'CaseBundle:Admin:attribute/edit.html.twig';

        return $this->render(
            $template,
            [
                'form' => $form->createView(),
                'id' => $attribute->getId(),
                'attribute' => $attribute,
                'attributeStepDefinitions' => $attributeStepDefinitions
            ]
        );

    }

    /**
     * @Route("/attribute/search/{value}/{addToStep}", name="admin_attribute_search", options={"expose"=true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @param Request $request
     * @param string $value
     * @param boolean $addToStep
     * @return Response
     */
    public function attributeSearch(Request $request, $value = '', $addToStep = false)
    {
        $em = $this->getDoctrine()->getManager();
        $attributes = $em->getRepository(Attribute::class)->searchAttributes(
            strtolower($value),
            $request->getLocale()
        );

        return $this->render(
            'CaseBundle:Admin:attribute/search.html.twig',
            [
                'attributes' => $attributes,
                'addToStep' => (int)$addToStep,
            ]
        );

    }

    /**
     * @Route("/step-attribute/remove/{id}", name="admin_step_attribute_remove", requirements={"id": "\d+"}, options={"expose" = true})
     * @Method("POST")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @return Response
     * @ParamConverter("stepAttributeDefinition", class="CaseBundle:StepAttributeDefinition")
     */
    public function stepAttributeRemoveAction($stepAttributeDefinition)
    {
        if (!$stepAttributeDefinition) {
            throw new NotFoundHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($stepAttributeDefinition);
        $em->flush();

        return new JsonResponse('OK');
    }

    /**
     * @Route("/step-attribute/editor/{id}", name="admin_step_attribute_editor", requirements={"id": "\d+"}, options={"expose" = true})
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @return Response
     * @ParamConverter("stepAttributeDefinition", class="CaseBundle:StepAttributeDefinition")
     */
    public function stepAttributeEditorAction(Request $request, $stepAttributeDefinition)
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(
            StepAttributeDefinitionType::class,
            $stepAttributeDefinition,
            [
                'parentAttributes' => $em->getRepository(Attribute::class)->fetchParentAttributes(
                    $stepAttributeDefinition->getAttribute()->getId(),
                    $request->getLocale(),
                    true
                ),
            ]
        );

        $errors = false;
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $this->container->get('security.authorization_checker')->isGranted(
                'ROLE_PROCESS_EDITOR'
            )
        ) {
            if ($form->isValid()) {
                $em->persist($stepAttributeDefinition);
                $em->flush();
            } else {
                $errors = $this->get('app.form_processing')->getFormErrors($form);
            }

            return new JsonResponse(['errors' => $errors, 'id' => $stepAttributeDefinition->getId()]);
        }

        return $this->render(
            'CaseBundle:Admin:attribute/step-attribute-edit-form.html.twig',
            [
                'form' => $form->createView(),
                'id' => $stepAttributeDefinition->getId(),
            ]
        );

    }
}
