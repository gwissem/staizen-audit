<?php

namespace CaseBundle\Controller\Definition;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ServiceDefinition;
use CaseBundle\Entity\ServiceStatusUpdate;
use CaseBundle\Entity\Step;
use CaseBundle\Entity\StepAttributeDefinition;
use CaseBundle\Form\Type\StepType;
use Doctrine\Common\Collections\Collection;
use ManagementBundle\Entity\DevLog;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StepController extends Controller
{
    /**
     * @Route("/step", name="admin_case_process_step_index")
     *
     */
    public function indexAction(Request $request)
    {

    }

    /**
     * @Route("/step/edit/{processId}/{id}/{init}", name="admin_case_process_step_editor", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     *
     */
    public function toolDataEditor(Request $request, $processId, $id = null, $init = false)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');

        $isNew = false;
        if ($id) {
            /** @var Step $step */
            $step = $em->getRepository('CaseBundle:Step')->find($id);
            if (!$step) {
                $step = new Step();
                $step->setId($id);
            }
        } else {
            $isNew = true;
            $step = new Step();
        }

        $processDefinition = $em->getRepository('CaseBundle:ProcessDefinition')->find($processId);
        $step->setProcessDefinition($processDefinition);

        $controls = $this->getControlsOfStep($step);

        $form = $this->createForm(
            StepType::class,
            $step,
            [
                'isNew' => $isNew,
                'processDefinitionId' => $processId,
                'controlsOfStep' => $controls,
                'selectedServices' => $em->getRepository('CaseBundle:ServiceDefinition')->findAllServiceForStep($step),
                'highestIdSuffix' => $em->getRepository('CaseBundle:Step')->findFirstEmptySuffix($processId)
            ]
        );

        $errors = false;
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $this->container->get('security.authorization_checker')->isGranted(
                'ROLE_PROCESS_EDITOR'
            )) {
            if ($form->isValid()) {

                if ($isNew) {
                    $step->setId($processId.'.'.$step->getIdSuffix());
                }

                // Troche to słabo zrobione... no ale cóż...
                $this->updateServiceStatusUpdateTable($step, $form->get('updateStatusService')->getData());

                $em->persist($step);
                $em->flush();

                /** DEV_LOG */
                    if ($isNew) {
                        $devLog = DevLog::CREATED_STEP($step, $processDefinition);
                    }
                    else {
                        $devLog = DevLog::UPDATED_STEP($step);
                    }
                    $em->persist($devLog);
                    $em->flush();
                /** DEV_LOG */

                $em->getFilters()->enable('softdeleteable');
            } else {
                $errors = $this->get('app.form_processing')->getFormErrors($form);
            }

            return new JsonResponse(['errors' => $errors]);
        }

        $template = $request->isXmlHttpRequest(
        ) ? 'CaseBundle:Admin:step/edit-form.html.twig' : 'CaseBundle:Admin:step/edit.html.twig';

        return $this->render(
            $template,
            [
                'form' => $form->createView(),
                'processId' => $processId,
                'id' => $id,
            ]
        );

    }

    /**
     * @param Step $step
     * @param ServiceDefinition[] $serviceDefinition
     */
    private function updateServiceStatusUpdateTable(Step $step, $serviceDefinition) {

        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('CaseBundle:ServiceStatusUpdate');

        $currentServicesStatus = $repository->findAllServiceStatusIdsInStep($step);

        foreach ($serviceDefinition as $item) {

            $key = false;

            foreach ($currentServicesStatus as $k => $status) {
                if($status['service_id'] == $item->getId()) {
                    $key = $k;
                    break;
                }
            }

//            $key = array_search($item->getId(), array_column($currentServicesStatus, 'service_id'));

            if($key === false) {
                $newServiceStatusUpdate = new ServiceStatusUpdate();
                $newServiceStatusUpdate->setStepId($step);
                $newServiceStatusUpdate->setServiceId($item);
                $em->persist($newServiceStatusUpdate);
            }
            else {
                unset($currentServicesStatus[$key]);
            }

        }

        foreach ($currentServicesStatus as $servicesStatus) {
            $em->remove($repository->find($servicesStatus['id']));
        }
//
        $em->flush();

    }

    protected function getControlsOfStep(Step $step) {

        $controlsArr = [];

        /** @var FormControl[] $controls */
        if($step->getFormTemplate()) {
            $controls = $step->getFormTemplate()->getControls();

            foreach ($controls as $control) {
                $attribute = $control->getAttribute();

                if ($attribute !== null) {
                    if ($attribute->hasFormTemplate()) {
                        $this->recursiveGetControls($attribute, $controlsArr);
                    } else {
                        $controlsArr[$attribute->getName() . ' (' . $control->getId().')'] = $control->getId();
                    }

                }
            }
        }
        return $controlsArr;
    }

    private function recursiveGetControls(Attribute $attribute, &$controlsArr) {

        /** @var FormControl[] $attrControls */
        $attrControls = $attribute->getFormTemplate()->getControls();

        foreach ($attrControls as $attrControl) {

            $attribute2 = $attrControl->getAttribute();
            if($attribute2 !== null) {

                if($attribute2->hasFormTemplate()) {
                    $this->recursiveGetControls($attribute2, $controlsArr);
                }
                else {
                    $controlsArr[$attribute2->getName()] = $attrControl->getId();
                }

            }

        }

    }

}
