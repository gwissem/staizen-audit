<?php

namespace CaseBundle\Controller;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Repository\AttributeValueRepository;
use CaseBundle\Service\AttributeParserService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMapping;
use PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use UserBundle\Entity\User;

/**
 * @Route("/partner-interface")
 */

class PartnerInterfaceController extends Controller
{

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->getDoctrine()->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);
    }

    private $filters = [
        "595,597" => "{@595,597,85,87@} {@595,597,85,94@} {@595,597,84@}",
        "595,597,642" => "{@595,597,642,84@}",
        "595,597,611" => "{@595,597,611,612@}",
        "595,597,611,642" => '{@595,597,611,642,84@}',
        "595,597,611,605" => "{@595,597,611,605,606@}",
        "595,597,611,648,677,605" => "{@595,597,611,648,677,605,606@}",
        "595,597,85" => "Adres do realizacji usługi",
    ];

    private $servicesPath = [
        1 => "595,597,611,648", // naprawa warsztatowa
        2 => "595,597,611,649", // naprawa na drodze
        3 => "595,597,611,650", // holowanie
        4 => "595,597,611,652", // taxi
        5 => "595,597,611,653", // pojazd zastepczy
        6 => "595,597,611,654", // nocleg
        // 7 =>Podróż
        8 => "595,597,611,655" // parking
        // 9 => Inna usługa
    ];

    private $formOrder;

    /** @var  AttributeParserService */
    private $attributeParserService;
    /** @var  AttributeValueRepository $repositoryAttributeValue */
    private $repositoryAttributeValue;
    /** @var  EntityManager $em */
    private $em;

    private $levelOpened = [
        0 => true,
        1 => true,
        2 => true,
        3 => false,
        4 => false,
        5 => false,
        6 => false,
        7 => false,
        8 => false,
    ];

    private $showLeaf = false;
    private $devMode = false;
    private $serviceType = null;
    private $isRoleEditor = false;

    /**
     * @Route("/get-root/{groupProcessId}", name="partner_interface_get_root", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_VIEWER')")
     * @param Request $request
     * @param $groupProcessId
     * @return Response
     */
    public function getRootTree(Request $request, $groupProcessId)
    {

        $this->setConfig($request);
        $avRepo = $this->getDoctrine()->getManager()->getRepository('CaseBundle:AttributeValue');

        /** @var AttributeValue[] $attributesRoot */
        $attributesRoot = $avRepo->findAllByParentId($groupProcessId, null);

        $attributesRootArray = [];

        foreach ($attributesRoot as $item) {

            $newRoot = $this->prepareAttributeValue($item, $item->getAttribute(), false, true);

            $newRoot['children'] = $this->getTreeOfAttributes($groupProcessId, $item->getId(), 0);

            $this->recursiveGetChild($newRoot['children'], $groupProcessId);

            $newRoot['type'] = "root";
            $newRoot['state']["opened"] = true;

            $attributesRootArray[] = $newRoot;
        }

        $this->decorateTree($attributesRootArray);

        return new JsonResponse($attributesRootArray, 200);

    }

    /**
     * @Route("/get-child/{groupProcessId}/{parent}/{isMulti}",  defaults={"isMulti"=0}, name="partner_interface_get_child", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_VIEWER')")
     * @param Request $request
     * @param $groupProcessId
     * @param $parent
     * @param $isMulti
     * @return Response
     */
    public function getChildTree(Request $request, $groupProcessId, $parent, $isMulti)
    {

        $this->setConfig($request);

        $child = $this->getTreeOfAttributes($groupProcessId, $parent, $isMulti);

        $this->recursiveGetChild($child, $groupProcessId);

        $this->decorateTree($child);

        return new JsonResponse($child, 200);

    }

    /**
     * @Route("/get-node-attribute-value/{id}",
     *     defaults={"isMulti"=0},
     *     name="partner_interface_get_node_attribute_value",
     *     options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_VIEWER')")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function getNodeOfAttributeValue(Request $request, $id)
    {

        $this->setConfig($request);

        /** @var AttributeValue $attributeValue */
        $attributeValue = $this->repositoryAttributeValue->find($id);

        $arrayValue = $this->prepareAttributeValue($attributeValue, $attributeValue->getAttribute(), false, false, true);

        return new JsonResponse($arrayValue, 200);

    }

    /**
     * @Route("/clear-cache/{location}", name="partner_interface_location_cache_clear", options={"expose": true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_VIEWER')")
     * @param Request $request
     * @param $location
     */
    public function clearPartnerLocationCache(Request $request, $location)
    {
        /** @var EntityManager $em */
        $parameters = [
            [
                'key' => 'location_id',
                'value' => $location,
                'type' => PDO::PARAM_INT
            ]
        ];
        $this->container->get('app.query_manager')->executeProcedure('EXEC [dbo].[partnersRefreshLoc] @partnerLocId  = :location_id', $parameters, false);
        return new JsonResponse('ok');
    }


    /**
     * @Route("/get-form/{groupProcessId}/{attributeId}", name="partner_interface_get_form", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_VIEWER')")
     * @param Request $request
     * @param $groupProcessId
     * @param $attributeId
     * @return Response
     */

    public function getForm(Request $request, $groupProcessId, $attributeId)
    {

        $this->setConfig($request);

        /** @var AttributeValue $parent */
        $parent = $this->repositoryAttributeValue->find($attributeId);

        if(!$parent) {
            return new JsonResponse([
                'controls' => []
            ], 200);
        }


        /** @var AttributeValue[] $attributesOfParent */
        $attributesOfParent = $this->repositoryAttributeValue->findAllByParentId($groupProcessId, $attributeId);

        $controls = [];

        foreach ($attributesOfParent as $attributeValue) {

            $attribute = $attributeValue->getAttribute();

            if($attribute->getIsMulti() || count($attribute->getChildren()) ) continue;

            $controls[] = [
                'id' => $attributeValue->getId(),
                'name' => $attribute->getName(),
                'path' => $attributeValue->getAttributePath(),
                'html' => $this->buildFormControl($attribute, $attributeValue, $groupProcessId)
            ];

        }

        if(isset($this->formOrder['paths'][$parent->getAttributePath()])) {
            $orderByParent =  $this->formOrder['paths'][$parent->getAttributePath()];

            usort($controls, function($key1, $key2) use ($orderByParent) {
                return (array_search($key1['path'], $orderByParent) > array_search($key2['path'],  $orderByParent));
            });

        }

        /** Add map widget */

        $map = null;
        if( strpos($parent->getAttributePath(), ',85') !== false && $this->isRoleEditor) {
            $map = $this->get('twig')->render('@Operational/Default/mini-map-widget.html.twig', [
                'pathLocation' => $parent->getAttributePath()
            ]);

        }

        return new JsonResponse([
            'controls' => $controls,
            'map' => $map
        ], 200);

    }

    /**
     * @Route("/ajax/get-partners", name="partner_interface_get_partners", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_VIEWER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPartnersAction(Request $request)
    {

        $searchTag = $request->query->get('searchTag', null);

        $result = $this->findPartners($searchTag);

        return new JsonResponse(
            [
                'partners' => $result
            ]
        );

    }



    private function findPartners($searchTag) {

        $queryManager = $this->container->get('app.query_manager');

        $parameters = [
            [
                'key' => 'SEARCH',
                'value' => $searchTag,
                'type' => PDO::PARAM_STR
            ]
        ];

        $results = $queryManager->executeProcedure("exec [p_searchPartner] @SEARCH =:SEARCH", $parameters);

        return $results;

    }

    private function decorateTree(&$tree) {

//        // Wyrzucanie gałęzi gdzie jest tylko 1 child
        foreach ($tree as $key => $child) {

            if($child['isMulti']) continue;

            if(is_array($child['children'])
                && count($child['children'])
                && $child['amountChildren'] == 1
                && $child['children'][0]['group'] !== "leaf"
            ) {
                $tree[$key]['otherParentId'] = $child['children'][0]['id'];
                $tree[$key]['otherParentPath'] = $child['children'][0]['path'];
                $tree[$key]['children'] = $child['children'][0]['children'];
            }

            if(is_array($tree[$key]['children'])) {
                $this->decorateTree($tree[$key]['children']);
            }

        }

    }

    private function setConfig(Request $request) {

        $this->attributeParserService = $this->get('case.attribute_parser.service');
        $this->repositoryAttributeValue = $this->getDoctrine()->getManager()->getRepository('CaseBundle:AttributeValue');
        $this->em = $this->getDoctrine()->getManager();

        $this->isRoleEditor = $this->container->get('security.authorization_checker')->isGranted(User::ROLE_PARTNER_EDITOR);

        $this->showLeaf =  filter_var($request->query->get('showLeaf', true), FILTER_VALIDATE_BOOLEAN);
        $this->devMode =  filter_var($request->query->get('devMode', false), FILTER_VALIDATE_BOOLEAN);

        $fs = new Filesystem();

        if($fs->exists( __DIR__ . '/../../../app/Resources/partnerInterface/form_order.yml')) {

            try {
                $this->formOrder = Yaml::parse(file_get_contents(__DIR__ . '/../../../app/Resources/partnerInterface/form_order.yml'));
            } catch (ParseException $e) {
                throw new InvalidConfigurationException(printf("Unable to parse the YAML string: %s", $e->getMessage()));
            }

        }
        else {
            throw new NotFoundResourceException('Nie znaleziono konfiguracji formularza.');
        }

    }

    private function recursiveGetChild(&$children, $groupProcessId, $fullDeep = false) {

        foreach ($children as $key => $leaf) {

            $children[$key]['children'] = $this->getTreeOfAttributes($groupProcessId, $leaf['id'], (($leaf['isMulti']) ? "1" : "0"));

            if(count($children[$key]['children']) > 0 && !$leaf['isMulti']) {
                $this->recursiveGetChild($children[$key]['children'], $groupProcessId);
            }
        }

    }

    private function getTreeOfAttributes($groupProcessId, $parent, $isMulti) {

        $child = [];
        $parent = str_replace('_multi', '', $parent);

        if($isMulti == "1") {

            /** @var AttributeValue $parentAttributeValue */
            $attributeValue = $this->repositoryAttributeValue->find($parent);

            /** @var AttributeValue $parent */
            $parentAttributeValue = $attributeValue->getParentAttributeValue();

            /** @var AttributeValue[] $allChild */
            $allChild = $this->repositoryAttributeValue->findAllChildByPath($groupProcessId, $parentAttributeValue, $attributeValue->getAttributePath());

            foreach ($allChild as $av) {

                $attribute = $av->getAttribute();
                $childOfParent = $this->prepareAttributeValue($av, $attribute,false, true, true);
                $childOfParent['parentAttribute'] = $parent;
                $child[] = $childOfParent;

            }

        }
        else {

            /** @var AttributeValue $parentAttributeValue */
            $parentAttributeValue = $this->repositoryAttributeValue->find(intval($parent));

            if($parentAttributeValue->getAttributePath() == "595,597,611") {
                $serviceType = $this->repositoryAttributeValue->findOneBy(['parentAttributeValue' => intval($parent), 'attributePath' => '595,597,611,612']);
                $this->serviceType = $serviceType->getValueByType($serviceType->getAttribute()->getValueType());
            }

            $multiAttributes = [];

            /** @var AttributeValue[] $attributesOfParent */
            $attributesOfParent = $this->repositoryAttributeValue->findAllByParentId($groupProcessId, $parent);

            foreach ($attributesOfParent as $attributeValue) {

                // Jeżeli jest to atrybut "usługi", to sprawdza czy dodać dodatkowo atrybut "Możliwość...."

                if($this->serviceType !== null && in_array($attributeValue->getAttributePath(), $this->servicesPath)) {

                    if(key_exists($this->serviceType, $this->servicesPath)) {
                        if($this->servicesPath[$this->serviceType] !== $attributeValue->getAttributePath()) continue;
                    }
                    else {
                        continue;
                    }
                }

                $attribute = $attributeValue->getAttribute();

                if($attribute->getIsMulti() ) {
                    if(in_array($attributeValue->getAttributePath(), $multiAttributes)) continue;

                    $multiAttributes[] = $attributeValue->getAttributePath();
                    $childOfParent = $this->prepareAttributeValue($attributeValue, $attribute,true, true);
                    $childOfParent['parentAttribute'] = $parent;
                    $child[] = $childOfParent;

                }
                elseif(count($attribute->getChildren())) {
                    $childOfParent = $this->prepareAttributeValue($attributeValue, $attribute, false, true);
                    $childOfParent['parentAttribute'] = $parent;
                    $child[] = $childOfParent;
                }
                elseif($this->showLeaf) {
                    $childOfParent = $this->prepareAttributeValue($attributeValue, $attribute, false, false);
                    $childOfParent['parentAttribute'] = $parent;
                    $child[] = $childOfParent;
                }

            }
        }

        return $child;
    }

    private function recursiveGetValue($value = "", $parentId, $parentPath, $childPath) {

        if (strpos($childPath, ',') !== false) {

            $arrayPath = (explode(',', $childPath));
            $prefixPath = array_shift($arrayPath);

            /** @var AttributeValue $attr */
            $attr = $this->repositoryAttributeValue->findOneBy(['parentAttributeValue' => $parentId, 'attributePath' => $parentPath . ',' . $prefixPath]);

            if(!$attr) {
                return $value;
            }
            else {
                return $this->recursiveGetValue($value, $attr->getId(), $parentPath . ',' . $prefixPath, implode(',', $arrayPath));
            }

        }
        else {

            /** @var AttributeValue $attr */
            $attr = $this->repositoryAttributeValue->findOneBy(['parentAttributeValue' => $parentId, 'attributePath' => ($parentPath . ',' . $childPath)]);

            if($attr) {
                $av = $attr->getValueByType($attr->getAttribute()->getValueType());
                $av = $this->attributeParserService->getValueFromAttributeQuery($attr->getAttribute()->getQuery(), $av);
                $value .= ($value !== "") ? (" " .$av) : $av;
            }
            else {
                $value = "-";
            }

            return $value;

        }

    }

    private function prepareAttributeValue(AttributeValue $attributeValue, Attribute $attribute, $isMulti = false, $children = false, $rename = false) {

        $id = $attributeValue->getId() . (($isMulti) ? '_multi' : '');
        $path = $attributeValue->getAttributePath();

        $class = '';
        if ($path === '595,597' && false === $isMulti) {
            if (true === $this->isInaccessiblePartner($attributeValue->getId())) {
                $class = 'partner-element-red';
            }
        }

//        if($rename) {
            if(key_exists($path, $this->filters) && !$isMulti) {

                $valueOfOpts = $this->filters[$path];
                $params = self::getStringBetween($this->filters[$path]);

                foreach ($params as $param) {

                    $childPath = str_replace(($path . ','), '', $param);
                    $val = $this->recursiveGetValue("", $id, $path, $childPath);
                    $valueOfOpts = str_replace('{@'.$param.'@}', $val, $valueOfOpts);

                }

                $text = $valueOfOpts;

            }
            else {
                $text = $attribute->getName();
            }

//        }
//        else {
//            $text = $attribute->getName();
//        }

        if($isMulti) {
            $text .= $this->getMultiActions($attribute->getId(), $attributeValue->getId(), $attributeValue->getParentAttributeValue()->getId(), true);
        }
        elseif(!$isMulti && $attribute->getIsMulti()) {
            if($this->devMode) {
                $text .= ' ('.$attributeValue->getId().') ';
            }
            $text .= $this->getMultiActions($attribute->getId(), $attributeValue->getId(), $attributeValue->getParentAttributeValue()->getId(), false, true);
        }

        if($isMulti) {
            $icon = 'fa fa-list-ul font-grey';
        }
        elseif(!$isMulti && !$children) {
            $icon = 'fa fa-leaf font-green-turquoise';
        }
        else {
            $icon = $this->getIcon($attributeValue->getAttributePath());
        }

        $priority = (!$isMulti && !$children) ? "6" : ( ($isMulti) ? "2" : '4' );
        $group = (!$isMulti && !$children) ? "leaf" : ( ($isMulti) ? "multi" : 'parent' );
        $hasChildWithChild = $this->hasChild($attributeValue->getId());

        if(!$hasChildWithChild && !$isMulti && $priority != "6") {
            $priority = '1';
        }

        if($this->showLeaf) {
            $childrenValue = ($isMulti || $children);
        }
        else {
            $childrenValue = ($isMulti || $hasChildWithChild);
        }

//        if($rename) {
//            $childrenValue = true;
//        }

        return [
            'id' => $id,
            'text' => $text,
            'path' => $path,
            'icon' => $icon,
            'group' => $group,
            'amountChildren' => $this->amountChild($attributeValue->getId()),
            'priority' => $priority,
            'state' => [
                "opened" => $this->getStateOpened($path, $isMulti),
                "disabled" => $isMulti
            ],
//            'children' => ($isMulti || $this->hasChild($attributeValue->getId())),
//            'children' => ($isMulti || $children),
            'children' => $childrenValue,
            'isMulti' => $isMulti,
            'a_attr' => [
                'data-path' => $attributeValue->getAttributePath(),
                'class' => $class
            ],
            'li_attr' => [
                'is-multi' => ($isMulti) ? '1' : '0',
                'data-priority' => $priority
            ]
        ];

    }

    private function isInaccessiblePartner($pareAttributeValueId)
    {
        $avRepo = $this->getDoctrine()->getManager()->getRepository('CaseBundle:AttributeValue');
        $parentId = $avRepo->findOneBy([
            'parentAttributeValue' => $pareAttributeValueId,
            'attributePath' => '595,597,613'
        ]);

        $inaccessibleValues = $avRepo->findBy([
            'parentAttributeValue' => $parentId->getId(),
            'attributePath' => ['595,597,613,67','595,597,613,68']
        ]);

        $inaccessible = false;
        $date = new \DateTime();

        /** @var AttributeValue $inaccessibleValue */
        foreach ($inaccessibleValues as $inaccessibleValue) {
            if ($inaccessibleValue->getValueDate() !== null) {
                if ($inaccessibleValue->getAttributePath() === '595,597,613,67'
                    && $inaccessibleValue->getValueDate() <= $date) {
                    $inaccessible = true;
                } elseif ($inaccessibleValue->getAttributePath() === '595,597,613,68'
                    && $inaccessibleValue->getValueDate() >= $date) {
                    $inaccessible = true;
                }
            }
        }

        return $inaccessible;
    }

    private function getStateOpened($path, $isMulti) {

        if($path === "595,597" && !$isMulti) return false;
        return $this->levelOpened[count(explode(',', $path))];

    }

    private function getMultiActions($attributeId, $attributeValueId, $parentId, $add = false, $remove = false) {

        if(!$this->isRoleEditor) return '';

        $content = '';

        if($add) {
            $content .= $this->getAddButton($attributeId, $parentId);
        }

        if($remove) {
            $content .= $this->getRemoveButton($attributeValueId, $parentId);
        }

        return '<div class="_actions">'.$content.'</div>';


    }

    private function getAddButton($attributeId, $parentId) {
        return '<button type="button" data-spinner-color="blue" data-style="zoom-in" data-size="xs" data-attr-id="'.$attributeId.'"  data-parent-attr-id="'.$parentId.'" class="append-multi ladda-button btn btn-primary">
                <span class="ladda-label">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </span>
            </button>';

    }

    private function getRemoveButton($attributeValueId, $parentId) {
        return '<button type="button" data-spinner-color="red" data-style="zoom-in" data-size="xs" data-attr-value-id="'.$attributeValueId.'"  data-parent-attr-id="'.$parentId.'" title="Na pewno?" class="remove-multi ladda-button btn btn-danger">
                <span class="ladda-label">
                    <i class="fa fa-remove" aria-hidden="true"></i>
                </span>
            </button>';
    }

    private function amountChild($id) {

        $query = $this->em->createQuery(
            'SELECT count(av.id) as amount FROM CaseBundle:AttributeValue as av
                  WHERE av.parentAttributeValue = :parentId'
        )->setParameter('parentId', $id);

        $result = $query->getResult();

        return $result[0]['amount'];

    }

    /** has Child Of Child */
    private function hasChild($id) {

        $query = $this->em->createQuery(
            'SELECT count(av.id) as amount FROM CaseBundle:AttributeValue as av
                  WHERE av.parentAttributeValue IN (
                    SELECT av2.id FROM CaseBundle:AttributeValue as av2 WHERE av2.parentAttributeValue = :parentId)'
        )->setParameter('parentId', $id);

        $result = $query->getResult();

        return !($result[0]['amount'] == "0");

    }

    private function getIcon($path) {
        switch ($path) {
            case "595,624,626":
            case "595,625,626":
            case "595,628,626":
            case "595,597,637,626":
            case "595,597,628,626":
            case "595,597,85": {
                return "fa fa-map-marker font-blue";
            }
            case "595,597,611,652": {
                return "fa fa-taxi font-yellow-gold";
            }
            case "595,597": {
                return "fa fa-building-o font-blue";
            }
            case "595,597,611": {
                return "fa fa-wrench font-blue";
            }
            case "595": {
                return "fa fa-child font-blue-ebonyclay";
            }
            case "595,597,628":
            case "595,628": {
                return "fa fa-envelope-o";
            }
            case "595,597,637":
            case "595,597,611,637":
            case "595,625": {
                return "fa fa-calculator";
            }
            case "595,624": {
                return "fa fa-home font-yellow-gold";
            }
            case "595,597,611,642":
            case "595,597,642": {
                return "fa fa-phone";
            }
            case "595,597,611,613":
            case "595,597,613":
            case "595,613": {
                return "fa fa-clock-o";
            }
            case "595,633": {
                return "fa fa-user font-green-dark";
            }
            default : {
                return "fa fa-star font-yellow";
            }
        }
    }

    private function buildFormControl(Attribute $attribute, AttributeValue $attributeValue, $groupProcessId) {

        $formTemplateGenerator = $this->get('form_template.generator');
        $formTemplateGenerator->setFullMode();

        $attribute->setAttrValue('path', $attributeValue->getAttributePath());
        $attribute->setAttrValue('stepId', "xxx");
        $attribute->setAttrValue('groupProcessId', $groupProcessId);
        $attribute->setAttrValue('id', $attributeValue->getId());
        $attribute->setAttrValue('value', $attributeValue->getValueByType($attribute->getValueType()));
        $attribute->setAttrValue('parentAttributeValueId', $attributeValue->getParentAttributeValue()->getId());
        $attribute->setAttrValue('isAuto', false);

        $virtualFormControl = new FormControl();

        if(!$this->isRoleEditor) {
            $virtualFormControl->notAllowEdit = true;
        }

        // TOTALNIE NA SZTYWNO, ŻEBY ZGASIĆ UWAGĘ :P
        if($attributeValue->getAttributePath() === "595,597,84") {
            $virtualFormControl->translate()->setLabel("Nazwa oddziału");
        }

        $virtualFormControl->setAttribute($attribute);
        $virtualFormControl->setAttributePath($attributeValue->getAttributePath());
        $virtualFormControl->groupProcessId = $groupProcessId;

        return $formTemplateGenerator->renderHtmlOfFormControl($virtualFormControl, false);

    }

    static function getStringBetween($str, $start = '{@', $end = '@}', $regex = null)
    {
        if ($start === $end) {
            $string = explode($start, $str, 3);
            return isset($string[1]) ? $string[1] : '';
        }

        $matches = [];
        $regex = ($regex === null) ? "/".$start."([a-zA-Z0-9_\-,]*)".$end."/i" : $regex;
        preg_match_all($regex, $str, $matches);

        return $matches[1];
    }
}
