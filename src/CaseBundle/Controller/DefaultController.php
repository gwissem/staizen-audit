<?php

namespace CaseBundle\Controller;

use AppBundle\Entity\Ticket;
use AppBundle\Form\Type\AdminTicketType;
use AppBundle\Form\Type\InsuranceDataImportType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="case_index")
     *
     */
    public function indexAction(Request $request)
    {
        return new Response('TODO');
    }

}
