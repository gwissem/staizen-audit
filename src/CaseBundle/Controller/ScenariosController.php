<?php

namespace CaseBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\Console\Application;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class ScenariosController
 * @package CaseBundle\Controller
 * @Route("/scenarios")
 */

class ScenariosController extends Controller
{

    /**
     * @Route("/run", name="scenariosRun", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function StartScenarioAction(Request $request)
    {

        set_time_limit(300);

        $scenarioName = $request->request->get('scenarioName', null);
        $configName = $request->request->get('configName', null);
        $push = $request->request->get('push', 1);

//        php bin/console atlas:build_scenario SimpleNewCase -u 1 -c test

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $arguments = [
            'command' => 'atlas:build_scenario',
            'scenarioName' => $scenarioName
        ];

        if(!empty($configName)) {
            $arguments['-c'] = $configName;
        }

        if($push == "1") {
            $user = $this->getUser();
            $arguments['-u'] = $user->getId();
        }

        $input = new ArrayInput($arguments);
        $output = new BufferedOutput();

        $application->run($input, $output);
        $content = $output->fetch();

        return new JsonResponse([
//            'response' => ''
            'response' => $content
        ]);
    }

}
