<?php

namespace CaseBundle\Controller\FormGenerator;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\FormTemplate;
use CaseBundle\Entity\Step;
use CaseBundle\Entity\StepAttributeDefinition;
use CaseBundle\Entity\FormControl;
use CaseBundle\Service\AttributeConditionService;
use CaseBundle\Service\CaseFormGenerator;
use CaseBundle\Service\FormTemplateGenerator;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\PersistentCollection;
use ManagementBundle\Entity\DevLog;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class DefaultController extends Controller
{

    /** array of id's  */
    protected $excludedControls;

    /** @var  CaseFormGenerator */
    protected $caseFormGenerator;

    /** @var  AttributeConditionService */
    public $attributeConditionService;

    /**
     * DefaultController constructor.
     */
    public function __construct()
    {
        $this->excludedControls = [];
    }

    /**
     * @Route("/form-generator/{type}/{id}", name="admin_process_form_generator", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @param Request $request
     * @param null $type
     * @param null $id
     * @return Response
     */
    public function indexAction(Request $request, $type = null, $id = null)
    {

        if (!$type || !$id) {
            throw new BadRequestHttpException();
        }

        $entity = $this->findOr404($type, $id);

        /** @var FormTemplate $formTemplate */
        $formTemplate = $entity->getFormTemplate();

        $this->caseFormGenerator = $this->get('case.form.generator');

        /** Potem poprawka wydajności */
//        $formTemplate = $this->getDoctrine()->getRepository('CaseBundle:FormTemplate')->findFormTemplateFullFetch($entity);

        if ($formTemplate) {
            $this->excludedControls = $formTemplate->getPathOfControls();
        }

        $arrayOfAttributes = $this->prepareArrayOfAttributes($entity);

        return $this->render(
            '@Case/Admin/design/generator-form.html.twig',
            [
                'listAttributes' => $arrayOfAttributes,
                'listWidget' => $this->getAvailableWidgets()
            ]
        );

    }


    /**
     * @Route("/form-generator/{type}/{id}/json-form", name="admin_process_json_form", options={"expose"=true})
     * @Method({"GET"})
     * @Security("is_granted('ROLE_PROCESS_VIEWER')")
     * @param Request $request
     * @param null $type
     * @param null $id
     * @return Response
     */
    public function jsonFormGeneratorAction(Request $request, $type = null, $id = null)
    {

        if (!$type || !$id) {
            throw new BadRequestHttpException();
        }

        $entity = $this->findOr404($type, $id);

        /** @var FormTemplate $formTemplate */
        $formTemplate = $entity->getFormTemplate();

        $this->caseFormGenerator = $this->get('case.form.generator');

        $formTemplateGenerator = $this->get('form_template.generator');
        $formTemplateJSON = ($formTemplate) ? $formTemplateGenerator->generateControlsOfFormTemplate($formTemplate) : [];

        return new JsonResponse([
            'formGeneratorJson' => $formTemplateJSON
        ]);

    }


    private function findOr404($type, $id)
    {

        $entity = null;
        
        switch ($type) {
            case "attribute" : {

                $entity = $this->getDoctrine()->getRepository('CaseBundle:Attribute')->find($id);

                break;
            }
            case "step" : {

                $entity = $this->getDoctrine()->getRepository('CaseBundle:Step')->find($id);

                break;

            }
            default : {

                throw new BadRequestHttpException('This type: "' . $type . '" is not supported.');

                break;
            }
        }

        if (!$entity) {
            throw new EntityNotFoundException();
        }

        return $entity;

    }

    /**
     * @param $entity
     * @return array
     */
    protected function prepareArrayOfAttributes($entity)
    {

        if ($entity instanceof Step) {
            /** @var PersistentCollection $attributesDefinition */
            $attributesDefinition = $entity->getAttributeDefinitions();
        } elseif ($entity instanceof Attribute) {
            $attributesDefinition = $entity->getChildren();
        } else {
            return [];
        }

        $attributesArray = [];

        /** @var StepAttributeDefinition | Attribute $item */
        foreach ($attributesDefinition as $item) {

            if ($item instanceof Attribute) {
                $newAttributes = $this->createNewAttribute($item, "");
                $childrenAttributes = $item->getChildren();
            } elseif ($item instanceof StepAttributeDefinition) {

                /** //TODO - TUTAJ DAĆ $newAttributes = $item->getPath()  - teraz chwilowa zaślepka */

                $parentPath = ($item->getParentAttribute()) ? $item->getParentAttribute()->getId() : '';

                $newAttributes = $this->createNewAttribute($item->getAttribute(), $parentPath);
                $childrenAttributes = $item->getAttribute()->getChildren();
            } else {
                break;
            }

            if ($childrenAttributes->count()) {
                $this->recursiveGetAttributes($newAttributes['path'], $childrenAttributes, $newAttributes['children']);
            }

            $newAttributes['parents_name'] = $this->caseFormGenerator->getNameOfParents($newAttributes['path']);

            $attributesArray[] = $newAttributes;

        }

        return $attributesArray;

    }

    private function createNewAttribute(Attribute $attribute, $parentPath)
    {

        $attribute->getTranslations();
        $cloneAttribute = clone($attribute);

        $cloneAttribute->setAttrValue('path', $this->appendPath($parentPath, $attribute->getId()));

        return [
            'id' => $cloneAttribute->getId(),
            'used' => (in_array($cloneAttribute->getAttrValue('path'), $this->excludedControls)) ? "1" : "0",
            'path' => $cloneAttribute->getAttrValue('path'),
            'entity' => $cloneAttribute,
            'children' => [],
            'parents_name' => ''
        ];

    }

    private function appendPath($parentPath, $id)
    {
        return ($parentPath) ? ($parentPath . ',' . $id) : $id;
    }

    protected function recursiveGetAttributes($parentPath, $parentChildrenAttributes, &$attributesArray)
    {

        /** @var Attribute[] $parentChildrenAttributes */
        foreach ($parentChildrenAttributes as $childrenAttribute) {

            $newAttributes = $this->createNewAttribute($childrenAttribute, $parentPath);

            $childrenAttributes = $childrenAttribute->getChildren();

            if ($childrenAttributes->count()) {
                $this->recursiveGetAttributes($newAttributes['path'], $childrenAttributes, $newAttributes['children']);
            }

            $newAttributes['parents_name'] = $this->caseFormGenerator->getNameOfParents($newAttributes['path']);

            $attributesArray[] = $newAttributes;

        }

    }

    protected function getAvailableWidgets()
    {

        $widgets = [];

        foreach (FormTemplateGenerator::getAvailableWidgets() as $key => $availableWidget) {
            $widgets[] = [
                'name' => $key,
                'label' => $availableWidget,
                'html' => $this->renderView('CaseBundle:FormTemplate:renderWidget.html.twig', [
                    'widget_name' => $key,
                    'widget_label' => $availableWidget
                ])
            ];
        }

        return $widgets;
    }

    /**
     * @Route("/form-generator/{type}/{id}/update", name="admin_process_form_generator_update", options={"expose"=true})
     * @Method({"POST"})
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     * @param Request $request
     * @param null $type
     * @param null $id
     * @return JsonResponse
     */
    public function updateFormAction(Request $request, $type = null, $id = null)
    {

        if (!$request->isXmlHttpRequest()) throw new BadRequestHttpException('Only Ajax Method.');

        if (!$type || !$id) {
            throw new BadRequestHttpException();
        }

        $entity = $this->findOr404($type, $id);

        $this->attributeConditionService = $this->get('case.attribute_condition');
        $requestFormGenerator = $request->request->get('formGenerator', null);
        if ($requestFormGenerator === null) throw new NotFoundResourceException('Nie znaleziono pola "formGenerator".');

        $em = $this->getDoctrine()->getManager();
        $formTemplate = null;

        if ($entity instanceof Attribute || $entity instanceof Step) {

            if ($entity->hasFormTemplate()) {
                $formTemplate = $entity->getFormTemplate();
            } else {
                $formTemplate = new FormTemplate();
                $entity->setFormTemplate($formTemplate);
            }

        }

        $isNew = (!$formTemplate->getId());
        $updatedControls = [];
        $newsControls = [];

        if (isset($requestFormGenerator['controls']) && is_array($requestFormGenerator['controls'])) {
            foreach ($requestFormGenerator['controls'] as $control) {

                $this->validControl($control, $isNew);

                if (!$isNew && $control['id']) {
                    $formControl = $formTemplate->getControlById($control['id']);
                } else {
                    $formControl = $this->createFormControl($control);
                    $em->persist($formControl);
                    $newsControls[] = $formControl;
                }

                $this->updateFormControl($formControl, $control);

                if ($formControl->getId()) {
                    $updatedControls[] = $formControl->getId();
                } else {
                    $formTemplate->addControl($formControl);
                }

                $formControl->mergeNewTranslations();
                $em->persist($formControl);
            }
        }

        $formTemplate->setConfig(($requestFormGenerator['config']) ?: []);

        $this->removeNoUsedControls($formTemplate, $updatedControls, $em);

        if($isNew) {
            $devLog = DevLog::CREATED_FORM_TEMPLATE($entity);
            if($devLog) {
                $em->persist($devLog);
            }
        }
        else {
            $devLog = DevLog::UPDATED_FORM_TEMPLATE($entity);
            if($devLog) {
                $em->persist($devLog);
            }
        }

        $em->persist($formTemplate);
        $em->persist($entity);
        $em->flush();

        $news = [];

        /** @var FormControl $item */
        foreach ($newsControls as $item) {
            $news[] = [
                'uniqueId' => $item->getTempUniqueId(),
                'id' => $item->getId()
            ];
        }

        return new JsonResponse(
            [
                'news' => $news
            ], 200);

    }

    private function validControl($data, $isNew)
    {
        if (!array_key_exists('id', $data)) {
            throw new NotFoundResourceException('Key "id" is required in FormControl.');
        }

        if ($isNew && $data['id']) throw new NotFoundResourceException('Coś jest nie tak.');
    }

    /**
     * @param $data
     * @return FormControl
     * @throws EntityNotFoundException
     */
    private function createFormControl($data)
    {

        $formControl = new FormControl();

        if (!$data['widget']) {
            $attribute = $this->getDoctrine()->getRepository('CaseBundle:Attribute')->find($data['attrId']);
            if (!$attribute) throw new EntityNotFoundException('Attribute with id "' . $data['attrId'] . '" not exists.');
            $formControl->setAttribute($attribute);
            $formControl->setAttributePath($data['path']);
        }

        if (!empty($data['uniqueId'])) {
            $formControl->setTempUniqueId($data['uniqueId']);
        }

        return $formControl;

    }

    private function updateFormControl(FormControl $formControl, $data)
    {

        $formControl->setHeight($data['height']);
        $formControl->setAttributePath($data['path']);
        $formControl->setWidth($data['width']);
        $formControl->setX($data['x']);
        $formControl->setY($data['y']);

        if(!empty($data['widget'])) {
            $formControl->setWidget($data['widget']);
        }

        $formControl->translate(null, false)->setLabel($data['label']);

        $formControl->setCustomClass((isset($data['customClass']) ? $data['customClass'] : ''));
        $formControl->setCustomStyle((isset($data['styles']) ? $data['styles'] : []));
        $formControl->setExtraData($this->reJsonExtraData($data['extraData']));

        $this->attributeConditionService->trySaveValidationJS($formControl);

        return $formControl;
    }

    private function reJsonExtraData($extraData)
    {

        if ($this->isJson($extraData)) {
            return json_encode(json_decode($extraData, true));
        }

        return "";
    }

    private function isJson($string)
    {
        json_decode($string, true);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    private function removeNoUsedControls(FormTemplate $formTemplate, $updatedControls, ObjectManager $em)
    {

        /**
         * @param FormControl $formControl
         * @return bool
         */
        $criteria = function ($formControl) use ($updatedControls) {
            if (!$formControl->getId()) return false;
            return (!in_array($formControl->getId(), $updatedControls));
        };

        $controls = $formTemplate->getControls()->filter($criteria);

        foreach ($controls as $control) {
            $formTemplate->removeControl($control);
            $em->remove($control);
        }
    }
}
