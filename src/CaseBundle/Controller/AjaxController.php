<?php

namespace CaseBundle\Controller;

use AppBundle\Entity\Log;
use AppBundle\Entity\ValueDictionary;
use AppBundle\Form\Type\InsuranceDataImportType;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\PostponeProcessInstance;
use CaseBundle\Entity\ProcessEnterHistory;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\ProcessInstanceTime;
use CaseBundle\Entity\ProcessPageTime;
use CaseBundle\Entity\ServiceDefinition;
use CaseBundle\Entity\ServiceStatusDictionary;
use CaseBundle\Entity\Step;
use CaseBundle\Form\Type\DeployRequestType;
use CaseBundle\Service\CaseFormGenerator;
use CaseBundle\Service\ProcessHandler;
use Doctrine\Common\Collections\ArrayCollection;
use DocumentBundle\Entity\Document;
use HttpRequest;
use MailboxBundle\Controller\MailboxController;
use PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use UserBundle\Entity\Company;
use UserBundle\Entity\User;

class AjaxController extends Controller
{
    const NOTE_ID = 226;
    const NOTE_PHONE = '406,226,197';
    const NOTE_EMAIL = '406,226,198';
    const NOTE_CONTENT = '406,226,227';
    const NOTE_TYPE = '406,226,229';
    const NOTE_DIRECTION = '406,226,407';
    const NOTE_SUBJECT = '406,226,409';
    const NOTE_CC = '406,226,410';
    const NOTE_SMS_TYPE = 'sms';
    const SERVICE_PANEL_STEP_ID = '1011.010';
    const ATTR_MUTE = '860';
    const ATTR_FAQ_ANSWERS = '996';

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->getDoctrine()->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);
    }

    /**
     * @Route("/ajax-attribute-options", name="case_ajax_attribute_options", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function attributeOptionsAction(Request $request)
    {
        $attributePath = $request->request->get('attributePath');
        $options = [];
        if ($attributePath) {
            $queryManager = $this->get('app.query_manager');
            $exploded = explode(',', $attributePath);
            $id = end($exploded);
            $attribute = $this->getDoctrine()->getManager()->getRepository(Attribute::class)->find($id);
            if ($attribute && $attribute->getQuery()) {
                $results = $queryManager->executeProcedure($attribute->getQuery());
                foreach ($results as $result) {
                    $option = [];
                    $result = array_values($result);
                    $option['id'] = $result[0];
                    $option['text'] = (count($result) > 1 && $result[1] != '') ? $result[1] : $result[0];
                    $options[] = $option;
                }
            }
        }

        return new JsonResponse($options);
    }

    /**
     * @Route("/ajax-case-info/{groupProcessId}", name="ajax_case_info", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return Response
     */
    public function caseInfo(Request $request, $groupProcessId)
    {
        $caseInfo = $this->getDoctrine()->getManager()->getRepository(ProcessInstance::class)->getCaseInfo($groupProcessId);

        return $this->render('CaseBundle:Dashboard/widgets:case-info.html.twig',
            [
                'caseInfo' => $caseInfo
            ]);

    }

    /**
     * @Route("/ajax-report-user-at-instance", name="case_report_user_at_instance", options={"expose" = true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return Response
     */
    public function reportUserAtInstanceAction(Request $request)
    {
        $userId = $request->request->get('userId', NULL);
        $instanceId = $request->request->get('instanceId', NULL);
        $this->get('snc_redis.default_client')->setex(ProcessHandler::REDIS_USER_AT_INSTANCE_PREFIX . $instanceId, 60, $userId);

        return new Response();
    }

    /**
     * @Route("/ajax-local-emergency-unit", name="case_ajax_local_emergency_unit", options={"expose" = true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     */
    public function getLocalEmergencyUnitAction(Request $request)
    {
        $district = $request->request->get('district', NULL);
        $province = $request->request->get('province', NULL);
        $county = $request->request->get('county', NULL);

        $output = [];
        if ($district && $province && $county) {
            $output = $this->get('case.car_call_handler')->getLocalEmergencyUnit($district, $county, $province);
        }

        if (empty($output)) {
            $output = [
                'name' => $this->getParameter('local_emergency_unit_name'),
                'ESAddress' => $this->getParameter('local_emergency_unit_address'),
                'number' => '112'
            ];
        } else {
            $output = $output[0];
        }

        return new JsonResponse($output);
    }

    /**
     * @Route("/new-note/{rootNoteId}/{groupProcessId}", defaults={"groupProcessId"=0}, name="case_new_note", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @param $rootNoteId
     * @param $groupProcessId
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    public function newNoteAction(Request $request, $rootNoteId, $groupProcessId) {

        $dataNote = $request->request->all();

        if(empty($dataNote['type']) || empty($dataNote['content'])) {
            throw new NotFoundResourceException('Brak wymaganych danych!');
        }

        if(($dataNote['type'] === 'phone' && empty($dataNote['number'])) || ($dataNote['type'] === 'sms' && empty($dataNote['number']))) {
            if(!isset($dataNote['extraData']['undefinedNumber'])) {
                throw new NotFoundResourceException('Brak wymaganych danych!');
            }
        }

        if($dataNote['type'] === 'text') {
            $dataNote['number'] = null;
        }

        $rootNote = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find(intval($rootNoteId));

        if(!$rootNote) {
            throw new NotFoundResourceException('Nie znaleziono notatki w sprawie!');
        }

        $noteHandler = $this->get('note.handler');

        if(!empty($dataNote['extraData'])) {
            foreach ($dataNote['extraData'] as $key => $extraDatum) {
                $dataNote[$key] = $extraDatum;
            }
            unset($dataNote['extraData']);
        }

        $note = $noteHandler->newNote($dataNote, $rootNote, $groupProcessId);

        return new JsonResponse(
            [
                'rootNoteId' => $rootNoteId,
                'note' => $note,
                'rootProcessInstanceId' => $rootNote->getRootProcess()->getId()
            ]
        );

    }

    /**
     * @Route("/update-phone-note", name="update_phone_note", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return Response
     */

    public function updatePhoneNoteAction(Request $request) {

        $direction = $request->request->get('direction', 1);
        $content = $request->request->get('content', null);
        $noteId = $request->request->get('noteId', null);

        if(empty($content) || empty($noteId)) {
            throw new NotFoundResourceException('Brak wymaganych danych!');
        }

        $note = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find(intval($noteId));

        if(!$note) {
            throw new NotFoundResourceException('Nie znaleziono notatki w sprawie!');
        }

        $noteHandler = $this->get('note.handler');

        $note = $noteHandler->updatePhoneNote($note, $content, $direction);

        return new JsonResponse(
            [
                'note' => $note
            ]
        );

    }

    /**
     * @Route("/ajax-add-note", name="case_ajax_add_note", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return Response
     */
    public function addNote(Request $request)
    {
        $type = $request->request->get('type', 'text');
        $content = $request->request->get('content', NULL);
        $numberPhone = $request->request->get('numberPhone', NULL);

        $groupProcessId = $request->request->get('groupProcessId', NULL);
        $rootId = $rootId = $request->request->get('rootId', NULL);
        $processHandler = $this->get('case.process_handler');

        if ($rootId && $groupProcessId && $content) {
            $structure = $processHandler->attributeAddStructure($rootId, self::NOTE_ID);
            $parser = $this->get('case.attribute_parser.service');
            $parser->setGroupProcessInstanceId($groupProcessId);
            $parsedContent = $parser->parseString($content);

            foreach ($structure as $value) {
                $data[$value['attribute_path']] = $value;
            }

            $noteId = $data[self::NOTE_CONTENT]['parent_attribute_value_id'];
            $userId = $this->getUser()->getId();
            $processHandler->setAttributeValue([self::NOTE_CONTENT => $data[self::NOTE_CONTENT]['id']], $parsedContent, AttributeValue::VALUE_TEXT, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);
            $processHandler->setAttributeValue([self::NOTE_TYPE => $data[self::NOTE_TYPE]['id']], $type, AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

            if ($type == self::NOTE_SMS_TYPE) {

                if($numberPhone !== NULL) {
                    $processHandler->setAttributeValue([self::NOTE_PHONE => $data[self::NOTE_PHONE]['id']], $numberPhone, AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);
                }

                $this->get('sms_handler')->sendSms($noteId, $groupProcessId);

            }

            return new JsonResponse(
                [
                    'id' => $noteId,
                    'content' => $parsedContent,
                    'direction' => 1
                ]
            );
        }

        return new Response();
    }

    /**
     * @Route("/ajax-send-sms", name="case_ajax_send_sms", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return Response
     */
    public function sendSms(Request $request)
    {
        $id = $request->request->get('id');
        $groupProcessId = $request->request->get('groupProcessId');

        if ($id && $groupProcessId) {
            $this->get('sms_handler')->sendSms($id, $groupProcessId);
        }
        return new Response();
    }

    /**
     * @Route("/ajax-atp-test-case/{eventType}", name="ajax_atp_test_case", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     */
    public function atpExampleCaseAction(Request $request, $eventType = 'update')
    {
        $xml = $this->getParameter('kernel.root_dir') . '/../data/examples/' . $eventType . '-test-atp-event.xml';
        $content = file_get_contents($xml);

        $decodedRequest = preg_replace('/ ([-\w]+\:)?(mustUnderstand=")(1|true)(")/', '', $content);

        ini_set("soap.wsdl_cache_enabled", "0");
        $server = new \SoapServer(
            $this->getParameter('kernel.root_dir') . '/../data/wsdl/AtpEventHandler.20160817.wsdl',
            [
                'classmap' => $this->get('apt_event_handler')->getClassMap(),
                'soap_version' => SOAP_1_1
            ]
        );

        $server->setObject($this->get('apt_event_handler'));

        $response = new Response();
        $response->headers->set('Content-Type', 'application/soap+xml; charset=utf-8');

        try {
            ob_start();
            //This explicitely passes clear request content to the service handler
            $server->handle($decodedRequest);
            $response->setContent(@ob_get_clean());
        } catch (\Exception $e) {
            $this->get('logger')->err($e->getMessage());
            $response->setContent($e->getMessage());
        }

        $this->get('monolog.logger.atp_websocket')->log('notice', $request->getContent());
        return $response;


    }

    /**
     * @Route("/ajax-get-book-numbers/{rootProcessId}", name="case_ajax_get_book_numbers", options={"expose" = true})
     * @Method("GET")
     * @param $rootProcessId
     * @return Response
     * @Security("is_granted('ROLE_USER')")
     */
    public function getBookNumbersAction($rootProcessId)
    {
        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$rootProcessId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'EXEC dbo.p_case_contact @rootId = :rootId, @email = 0';

        $bookNumbers = $this->get('app.query_manager')->executeProcedure($query, $parameters);

        return new JsonResponse([
            'bookNumbers' => $bookNumbers
        ]);

    }

    /**
     * @Route("/ajax-add-note-structure/{rootNoteId}/{type}/{stepId}/{groupProcessId}", name="case_ajax_add_note_structure", options={"expose" = true})
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @param $root
     * @param $stepId
     * @param $groupProcessId
     * @param string $type
     * @return Response
     */
    public function addNoteStructureAction(Request $request, $root, $stepId, $groupProcessId, $type = 'text')
    {
        $processHandler = $this->get('case.process_handler');
        $instanceId = $processHandler->getActiveInstanceForRoot($groupProcessId);
        $emptyNoteId = false;
        $structure = [];
        $data = [];

        $rootId = $request->request->get('rootId', null);

        if ($instanceId) {
            $formControls = $processHandler->formControls($instanceId);
            foreach ($formControls as $formControl) {
                if ($formControl['attribute_path'] == self::NOTE_CONTENT && empty($formControl['value_text'])) {
                    $emptyNoteId = $formControl['parent_attribute_value_id'];
                }
            }
            if ($emptyNoteId) {
                foreach ($formControls as $formControl) {
                    if ($formControl['parent_attribute_value_id'] == $emptyNoteId) {
                        $formControl['id'] = $formControl['attribute_value_id'];
                        $structure[] = $formControl;
                    }
                }
            }
        }

        if (empty($structure)) {
            $structure = $this->get('case.process_handler')->attributeAddStructure($root, self::NOTE_ID);
        }

        foreach ($structure as $value) {
            $data[$value['attribute_path']] = $value;
        }

        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'EXEC dbo.p_case_contact @rootId = :rootId, @email = 0';

        $bookNumbers = $this->get('app.query_manager')->executeProcedure($query, $parameters);

        return new JsonResponse([
            'html' => $this->renderView('@Operational/Default/notifications-form.html.twig',
                [
                    'data' => $data,
                    'groupProcessId' => $groupProcessId,
                    'stepId' => $stepId,
                    'type' => $type
                ]
            ),
            'bookNumbers' => $bookNumbers
        ]);

    }

    /**
     * @Route("/ajax-get-note-html/{noteId}", name="case_ajax_get_note", options={"expose" = true})
     * @Method("GET")
     * @param $noteId
     * @return Response
     */
    public function getNoteStructureAction($noteId)
    {

        return new JsonResponse([
            'note' => $this->get('note.handler')->getNoteStructure($noteId)
        ]);

    }

    /**
     * @Route("/ajax-clear-and-back-process/{processInstanceId}", name="case_ajax_clear_and_back_process", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @param $processInstanceId
     * @return JsonResponse
     */
    public function clearAndBackProcessAction(Request $request, $processInstanceId)
    {

        $user = $this->getUser();

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => (int)$processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => (int)$user->getId(),
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'EXEC dbo.p_clear_all_until_instance @processInstanceId = :processInstanceId, @userId = :userId';

        $this->get('app.query_manager')->executeProcedure($query, $parameters, false);

        return new JsonResponse([
            'processInstanceId' => $processInstanceId
        ]);

    }


    /**
     * @Route("/ajax-reactivate-process", name="case_ajax_reactivate_process", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function reactivateProcessAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $processRepo = $em->getRepository(ProcessInstance::class);
        $id = $request->request->get('id');
        $previousId = $request->request->get('previousId');
        $inactive = $processRepo->find($id);
        $toDeactivate = $processRepo->find($previousId);
        $token = null;

        if(($token = $request->headers->get('Process-Token'))) {

            if($toDeactivate->getToken() == $token) {

                $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(['username' => 'virtual_partner']);
                if(!$user instanceof User) {
                    throw new NotFoundResourceException('Not found Virtual Partner!');
                }
                $this->get('user.info')->setVirtualUser($user);

            }
            else {
                throw new NotFoundResourceException('Not found active Token!');
            }

        }
        else {
            if(!$this->isGranted('ROLE_OPERATIONAL_DASHBOARD')) {
                throw new AccessDeniedHttpException();
            }
        }

        $rootId = $inactive->getRoot()->getId();
        $newId = NULL;

        if ($inactive && $toDeactivate) {
            $now = new \DateTime();

            $user = $this->get('user.info')->getUser();
            $originalUser = $this->get('user.info')->getOriginalUser();

            $processRepo->updateLastRunInstances($rootId, $id, $previousId);

            $toActive = new ProcessInstance();
            $toActive->setStep($inactive->getStep());
            $toActive->setCompany($inactive->getCompany());
            $toActive->setGroupProcess($inactive->getGroupProcess());
            $toActive->setRoot($inactive->getRoot());
            $toActive->setParent($inactive->getParent());
            $toActive->setActive(true);
            $toActive->setCreatedAt($now);
            $toActive->setUpdatedAt($now);
            $toActive->setCreatedBy($user);
            $toActive->setCreatedByOriginal($originalUser);

            if($token) {
                $toActive->setToken($token);
            }

            if($toDeactivate->getActive() == true && $toDeactivate->getStep()->getId() !== self::SERVICE_PANEL_STEP_ID) {
                $toDeactivate->setActive(false);
            }

            $em->persist($toActive);
            $em->flush();

            $time = new ProcessInstanceTime();
            $time->setProcessInstance($toActive);
            $time->setLifeTime(0);
            $time->setPercentage(0);
            $em->persist($time);
            $em->flush();

            $toActive->setTime($time);
            $em->persist($toActive);
            $em->persist($toDeactivate);
            $em->flush();

            $newId = $toActive->getId();
        }

        return new JsonResponse($newId);
    }

    /**
     * @Route("/ajax-reactivate-prev-step", name="case_ajax_reactivate_prev_step", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function reactivatePrevStepAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $processRepo = $em->getRepository("CaseBundle:ProcessInstance");
        $id = $request->request->get('id');
        $previousId = $request->request->get('previousId');

        /** @var ProcessInstance $currentProcessInstance */
        $currentProcessInstance = $processRepo->find($id);
        /** @var ProcessInstance $prevProcessInstance */
        $prevProcessInstance = $processRepo->find($previousId);

        $token = null;

        if(empty($currentProcessInstance) || empty($prevProcessInstance)) {
            throw new NotFoundResourceException('Not found steps!');
        }

        if(($token = $request->headers->get('Process-Token'))) {

            if($currentProcessInstance->getToken() == $token) {

                $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(['username' => 'virtual_partner']);
                if(!$user instanceof User) {
                    throw new NotFoundResourceException('Not found Virtual Partner!');
                }
                $this->get('user.info')->setVirtualUser($user);

            }
            else {
                throw new NotFoundResourceException('Not found active Token!');
            }

        }
        else {
            if(!$this->isGranted('ROLE_OPERATIONAL_DASHBOARD')) {
                throw new AccessDeniedHttpException();
            }
        }

        $prevProcessInstance->setActive(1);
        $currentProcessInstance->setActive(0);

        $em->persist($prevProcessInstance);
        $em->persist($currentProcessInstance);

        $em->flush();

        return new JsonResponse([
            'processInfo' => [
                'id' => $previousId,
                'stepId' => $prevProcessInstance->getStep()->getId(),
                'groupProcessId' => $prevProcessInstance->getGroupProcess()->getId(),
                'rootId' => $prevProcessInstance->getRoot()->getId()
            ]
        ]);

    }

    /**
     * @Route("/ajax-process-search", name="case_ajax_process_search", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function processSearchAction(Request $request)
    {
        $value = $request->request->get('value', null);
        $results = ['ids' => [], 'filters' => []];
        $toolId = $this->getParameter('process_search_tool_id', null);

        if (!empty($value)) {
            $results = $this->getDoctrine()->getRepository(ProcessInstance::class)->fetchInstancesByAttributeValue($value);
        }

        return new JsonResponse([
            'ids' => $results['ids'],
            'filters' => $results['filters'],
            'toolId' => $toolId
        ]);
    }

    /**
     * @Route("/ajax-copy-structure", name="case_ajax_copy_structure", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function copyStructureAction(Request $request)
    {
        $source = $request->request->get('sourceId', null);
        $target = $request->request->get('targetId', null);
        $instance = $request->request->get('instanceId', null);
        $targetPath = $request->request->get('targetPath', null);
        $groupProcessId = $request->request->get('groupProcessId', null);

        if(!$target && $targetPath && $groupProcessId){
            $attributeValue = $this->get('case.process_handler')->getAttributeValue($targetPath, $groupProcessId);
            if($attributeValue && count($attributeValue) > 0) {
                $target = $attributeValue[0]['id'];
            }
        }

        if($source && $target && $instance){
            $this->get('case.process_handler')->attributeCopyStructure($source, $target, $instance);
        }

        return new JsonResponse('OK');
    }

    /**
     * @Route("/ajax-postpone-task", name="ajax_postpone_task", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function postponeTaskAction(Request $request)
    {

        $data = $request->request->all();

        /** @var ProcessInstance $processInstance */
        $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($data['processInstanceId']));

        if(!$processInstance) {
            throw new NotFoundHttpException("ProcessInstance don't exists.");
        }

        /** @var PostponeProcessInstance $postponeProcess */
        $postponeProcess = $this->getDoctrine()->getRepository('CaseBundle:PostponeProcessInstance')->findOneBy([
            'processInstance' => $processInstance
        ]);


        try {

            if(!$postponeProcess) {
                $postponeProcess = new PostponeProcessInstance();
                $postponeProcess->setProcessInstance($processInstance);
            }

            $user = $this->getUser();
            $now = new \DateTime();
            $processInstance->setPostponeDate($now->modify($data['time']));

            $description = (!empty($data['description'])) ? $data['description'] : '.';
            $postponeProcess->setDescription($description);


//            JEŚLI TO POSTPONE REKLAMACJI FINANSOWEJ
            if($processInstance->getStep()->getId() == '1214.006' ){

                $parameters = [
                    [
                        'key' => 'processInstanceId',
                        'value' => (int)$processInstance->getId(),
                        'type' => PDO::PARAM_INT
                    ],[
                        'key' => 'reason',
                        'value' => $data['reason']??'' .' ' . $description??'',
                        'type' => PDO::PARAM_STR
                    ],
                ];

                $this->container->get('app.query_manager')->executeProcedure("EXEC p_send_email_to_df_ZO_postponse @processInstanceId =:processInstanceId, @reason= :reason", $parameters, false);
            }


            $postponeProcess->setReason($data['reason']);
            $postponeProcess->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($processInstance);
            $em->persist($postponeProcess);
            $em->flush();

            $result = [
                'error' => 0,
                'message' => $this->get('translator')->trans('Task has been successfully postponed.')
            ];

        }
        catch (\Throwable $exception) {

            $result = [
                'error' => 1,
                'message' => $this->get('translator')->trans('Error while postponing task')
            ];

        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/ajax-postpone-modal-form/{processInstanceId}", name="ajax_postpone_modal_form", options={"expose" = true})
     * @Method("GET")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @param $processInstanceId
     * @return JsonResponse
     */
    public function postponeModalFormAction(Request $request, $processInstanceId)
    {

        $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($processInstanceId));

        if(!$processInstance) {
            throw new NotFoundHttpException("ProcessInstance don't exists.");
        }

        $postponeProcess = $this->getDoctrine()->getRepository('CaseBundle:PostponeProcessInstance')->findOneBy([
            'processInstance' => $processInstance
        ]);

        $html = $this->renderView('@Case/Dashboard/Modal/postpone-form.html.twig', [
            'description' => ($postponeProcess) ? $postponeProcess->getDescription() : '',
            'reason' =>  ($postponeProcess) ? $postponeProcess->getReason() : '',
            'reasons' => PostponeProcessInstance::getReasons(),
            'user' => ($postponeProcess) ? $postponeProcess->getUser() : null
        ]);

        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @Route("/ajax-service-programs/{groupProcessId}/{startStepId}", name="ajax_service_programs", options={"expose" = true})
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function serviceProgramsAction(Request $request, $groupProcessId, $startStepId = null)
    {
        $results = [];
        $results['programs'] = $this->get('case.service_handler')->getExtraPrograms($groupProcessId);
        $results['related'] = [];
        if(strpos($startStepId, '1007.') !== false) {
            $results['related'] = $this->get('case.service_handler')->getRelatedServices($groupProcessId);
        }
        return new JsonResponse($results);
    }

    /**
     * @Route("/ajax-special-organisation/{groupProcessId}", name="ajax_special_organisation", options={"expose" = true})
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function specialOrganisationAjax(Request $request, $groupProcessId)
    {
        $results = $this->get('case.parser_methods')->config($groupProcessId, 'service_special_organisation_path');
        $results = $results ?? 0;
        return new JsonResponse($results);
    }


    /**
     * @Route("/ajax-service-program", name="ajax_service_program", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function serviceProgramAction(Request $request)
    {
        $programId = (string)$request->request->get('programId');
        $relatedService = $request->request->get('relatedService');
        $postFactum = filter_var($request->request->get('postFactum',false),FILTER_VALIDATE_BOOLEAN);
        $specialOrganisation = filter_var($request->request->get('specialOrganisation',false),FILTER_VALIDATE_BOOLEAN);
        $serviceStepId = $request->request->get('startStepId');
        $processInstanceId = $request->request->get('processInstanceId');

        $userId = $this->getUser()->getId();
        $instance = $this->getDoctrine()->getManager()->getRepository(ProcessInstance::class)->find($processInstanceId);

        $rootId = $instance->getRoot()->getId();
        $this->get('case.process_handler')->editAttributeValue('849', $rootId, $programId, 'string','xxx', $userId, $userId);

        if($postFactum) {
            $this->get('case.process_handler')->editAttributeValue('859', $rootId, (int)(bool)$postFactum, 'int', 'xxx', $userId, $userId);
        }

        if($specialOrganisation) {
            $this->get('case.process_handler')->editAttributeValue('1010', $rootId, (int)(bool)$specialOrganisation, 'int', 'xxx', $userId, $userId);
        }

        if($relatedService){
            $this->get('case.process_handler')->editAttributeValue('232', $rootId, $relatedService, 'int', 'xxx', $userId, $userId);
        }

        $response = 'FAIL';
        if($serviceStepId && $processInstanceId) {
            $this->get('case.service_handler')->runService($serviceStepId, $processInstanceId, $rootId);
            $response = 'OK';
        }

        return new Response($response);
    }

    /**
     * @Route("/ajax-activate-service", name="case_ajax_activate_service", options={"expose" = true})
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function activateServiceAction(Request $request)
    {
        $serviceStepId = $request->request->get('startStepId');
        $processInstanceId = $request->request->get('processInstanceId');
        $groupProcessId = $request->request->get('groupProcessId');
        $attributePath = $request->request->get('attributePath');
        $value = $request->request->get('value');

        $response = 'FAIL';
        if($serviceStepId && $processInstanceId) {
            $this->get('case.service_handler')->runService($serviceStepId, $processInstanceId, $groupProcessId, $attributePath, $value);

            $response = 'OK';
        }

        return new Response($response);
    }

    /**
     * @Route("/ajax-dynamic-edit-attribute/{groupProcessId}/{path}", name="case_ajax_dynamic_edit_attribute", options={"expose" = true})
     * @Method({"POST", "GET"})
     * @param Request $request
     * @param $groupProcessId
     * @param $path
     * @return JsonResponse
     */
    public function dynamicEditAttributeAction(Request $request, $groupProcessId, $path)
    {

        $idAttribute = $path;

        if (strpos($idAttribute, ',') !== false) {

            $parentsPath = explode(',', $idAttribute);
            $idAttribute = array_pop($parentsPath);

        }

        $attribute = $this->getDoctrine()->getRepository('CaseBundle:Attribute')->find($idAttribute);
        if (!$attribute) throw new NotFoundResourceException('Nie znaleziono atrybutu!');

        if($request->isMethod('POST')) {

            /** Zapisanie wartości */

            $id = $request->request->get('id', NULL);
            $value = $request->request->get('value', NULL);

            $userInfo = $this->get('user.info')->getInfo();

            $this->get('case.process_handler')->setAttributeValue(
                [$path => $id],
                $value,
                $attribute->getValueType(),
                'xxx',
                $groupProcessId,
                $userInfo['userId'],
                $userInfo['originalUserId']
            );

            return new JsonResponse([
                'status' => 'ok'
            ]);

        }
        else {
            /** Wygenerowanie formularza */

            $formTemplateGenerator = $this->get('form_template.generator');
            $formTemplateGenerator->setFullMode();

            $attributeValue = $this->get('case.process_handler')->getAttributeValue($path, $groupProcessId);

            if(!$attributeValue) {
                return new JsonResponse([
                    'status' => false,
                    'msg' => $this->get('translator')->trans('Nie można edytować tej wartości.'),
                    'path' => $path
                ]);
            }

//            if(!$attributeValue) throw new NotFoundResourceException('Nie znaleziono AttributeValue!');

            if(is_array($attributeValue)) $attributeValue = $attributeValue[0];

            $attribute->setAttrValue('path', $path);
            $attribute->setAttrValue('stepId', "xxx");
            $attribute->setAttrValue('groupProcessId', $groupProcessId);
            $attribute->setAttrValue('id', $attributeValue['id']);
            $attribute->setAttrValue('value', $attributeValue[$attribute->getValueType()]);
            $attribute->setAttrValue('parentAttributeValueId', null);
            $attribute->setAttrValue('isAuto', false);

            $virtualFormControl = new FormControl();
            $virtualFormControl->setAttribute($attribute);
            $virtualFormControl->setAttributePath($path);
            $virtualFormControl->groupProcessId = $groupProcessId;

            $html = $formTemplateGenerator->renderHtmlOfFormControl($virtualFormControl);

            return new JsonResponse([
                'status' => true,
                'html' => $html,
                'path' => $path,
                'type' => $attribute->getInputType(),
                'attributeValueId' => $attributeValue['id']
            ]);

        }

//        $status = $this->get('case.service_handler')->runService($serviceId, $processInstanceId, true);


    }


    /** WIDGET PANEL EDIT ATTRIBUTE */

    /**
     * @Route("/ajax-widget-panel-tree/get-root/{groupProcessId}", name="ajax_widget_panel_tree_get_root", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $groupProcessId
     * @return Response
     */
    public function widgetPanelTreeGetRoot(Request $request, $groupProcessId)
    {


        /** @var AttributeValue[] $attributesRoot */
        $attributesRoot = $this->getDoctrine()->getManager()->getRepository('CaseBundle:AttributeValue')->findAllByParentId($groupProcessId, null);

        $attributesRootArray = [];

        foreach ($attributesRoot as $item) {
            $attributesRootArray[] = [
                'id' => $item->getId(),
                'name' => $item->getAttribute()->getName(),
                'path' => $item->getAttributePath(),
                'isMulti' => $item->getAttribute()->getIsMulti()
            ];
        }

        return new JsonResponse($attributesRootArray, 200);

    }

    /**
     * @Route("/ajax-widget/remove-file/{fileUId}/{documentId}", name="ajax_widget_remove_file", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $fileUId
     * @param $documentId
     * @return Response
     */
    public function widgetPanelRemoveFile(Request $request, $fileUId, $documentId)
    {

        $document = $this->getDoctrine()->getRepository('DocumentBundle:Document')->find($documentId);

        $file = $this->getDoctrine()->getRepository('DocumentBundle:File')->findOneBy([
            'uuid' => $fileUId
        ]);

        if($document && $file) {

            $filePath = $this->getParameter('files_directory').$file->getPath();

            $em = $this->getDoctrine()->getManager();
            $document->removeFile($file);
            $em->remove($file);
            $em->persist($document);
            $em->flush();

            unlink($filePath);

        }

        return new JsonResponse([
            'status' => "ok"
        ], 200);

    }

    /**
     * @Route("/ajax-widget/add-document/{groupProcessId}", name="ajax_widget_add_document", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $groupProcessId
     * @return Response
     */
    public function widgetPanelAddDocument(Request $request, $groupProcessId)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $document = new Document();
        $document->setName('process_form_'.$groupProcessId);
        $document->setCreatedBy($user);

        /** @var Company $userCompany */
        $userCompany = $user->getCompany();

        if($userCompany) {
            $document->addCompany($userCompany);
        }

        if($userCompany->getId() !== 1) {
            $starterCompany =  $em->getRepository('UserBundle:Company')->find(1);
            $document->addCompany($starterCompany);
        }

        $em->persist($document);
        $em->flush();

        return new JsonResponse([
            'documentId' => $document->getId()
        ], 200);

    }

    /**
     * @Route("/ajax-widget-panel-tree/get-attributes/{groupProcessId}/{parentId}", name="ajax_widget_panel_tree_get_attributes", options={"expose":true})
     * @Method("GET")
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $groupProcessId
     * @param int $parentId
     * @return Response
     */
    public function widgetPanelTreeGetAttributes(Request $request, $groupProcessId, $parentId)
    {

        $controls = [];
        $structure = [];

        $parentId = ($parentId === 0) ? null : $parentId;
        /** @var AttributeValue[] $attributesOfParent */
        $attributesOfParent = $this->getDoctrine()->getManager()->getRepository('CaseBundle:AttributeValue')->findAllByParentId($groupProcessId, $parentId);

        foreach ($attributesOfParent as $attributeValue) {
            if($attributeValue->getAttribute()->getIsMulti()) {

                $key = array_search($attributeValue->getAttributePath(), array_column($structure, 'path'));

                if($key !== false) {
                    $structure[$key]['multi'][] = [
                        'id' => $attributeValue->getId(),
                        'name' => $attributeValue->getId()
                    ];
                }
                else {
                    $structure[] = [
                        'id' => null,
                        'name' => $attributeValue->getAttribute()->getName(),
                        'path' => $attributeValue->getAttributePath(),
                        'attrId' => $attributeValue->getAttribute()->getId(),
                        'multi' => [
                            [
                                'id' => $attributeValue->getId(),
                                'name' => $attributeValue->getId()
                            ]
                        ],
                        'isMulti' => true
                    ];
                }

            }
            elseif(count($attributeValue->getAttribute()->getChildren())) {
                $structure[] = [
                    'id' => $attributeValue->getId(),
                    'name' => $attributeValue->getAttribute()->getName(),
                    'path' => $attributeValue->getAttributePath(),
                    'isMulti' => false
                ];
            }
            else {
                $controls[] = [
                    'id' => $attributeValue->getId(),
                    'name' => $attributeValue->getAttribute()->getName(),
                    'path' => $attributeValue->getAttributePath(),
                    'html' => $this->buildFormControl($attributeValue->getAttribute(), $attributeValue, $groupProcessId)
                ];
            }
        }

        $this->decorateTreeAttribute($structure, $groupProcessId);

        return new JsonResponse([
            'controls' => $controls,
            'structure' => $structure
        ], 200);

    }

    private function decorateTreeAttribute(&$structure, $groupProcessId) {

        $filters = [
            "595,597" => "595,597,84|595,597,689",
            "595,597,642" => "595,597,642,84",
            "595,597,611" => "595,597,611,612",
            "595,597,611,642" => '595,597,611,642,84',
            "595,597,611,605" => "595,597,611,605,606",
            "595,597,611,648,677,605" => "595,597,611,648,677,605,606",
            "595,597,611,653,657" => "595,597,611,653,657,606",
        ];

        $repository = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue');
        $processHandler = $this->get('case.attribute_parser.service');
        $processHandler->setGroupProcessInstanceId($groupProcessId);

        foreach ($structure as $key => $item) {
            if(key_exists($item['path'], $filters)) {
                foreach ($item['multi'] as $key2 => $value) {

                    $opts = (explode('|', $filters[$item['path']]));

                    $valueOfOpts = "";

                    foreach ($opts as $path) {
                        /** @var AttributeValue $attr */
                        $attr = $repository->findOneBy(['parentAttributeValue' => $value['id'], 'attributePath' => $path]);

                        if($attr) {
                            $attributeValue = $attr->getValueByType($attr->getAttribute()->getValueType());
                            $attributeValue = $processHandler->getValueFromAttributeQuery($attr->getAttribute()->getQuery(), $attributeValue);
                            $valueOfOpts .= ($valueOfOpts !== "") ? (" " .$attributeValue) : $attributeValue;
                        }
                        else {
                            $valueOfOpts = "-";
                        }

                    }

                    $structure[$key]['multi'][$key2]['name'] = ($valueOfOpts) . ' (' . $value['id'] . ')';
                }
            }
        }
    }


    private function buildFormControl(Attribute $attribute, AttributeValue $attributeValue, $groupProcessId) {

        $formTemplateGenerator = $this->get('form_template.generator');
        $formTemplateGenerator->setFullMode();

        $attribute->setAttrValue('path', $attributeValue->getAttributePath());
        $attribute->setAttrValue('stepId', "xxx");
        $attribute->setAttrValue('groupProcessId', $groupProcessId);
        $attribute->setAttrValue('id', $attributeValue->getId());
        $attribute->setAttrValue('value', $attributeValue->getValueByType($attribute->getValueType()));
        $attribute->setAttrValue('parentAttributeValueId', null);
        $attribute->setAttrValue('isAuto', false);

        $virtualFormControl = new FormControl();
        $virtualFormControl->setAttribute($attribute);
        $virtualFormControl->setAttributePath($attributeValue->getAttributePath());
        $virtualFormControl->groupProcessId = $groupProcessId;

        return $formTemplateGenerator->renderHtmlOfFormControl($virtualFormControl, false);

    }

    /**
     * @Route("/reopen-closed-case/{rootId}", name="ajax_reopen_case", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $rootId
     * @return Response
     */
    public function reopenClosedCaseAction(Request $request, $rootId)
    {

        $this->get('case.process_handler')->reopenCase($rootId);

        return new JsonResponse([
            'message' => $this->get('translator')->trans('casen.reopened.successfully')
        ], 200);

    }

    /**
     * @Route("/change-service-status", name="ajax_change_service_status", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @param $rootId
     * @return Response
     */
    public function changeServiceStatus(Request $request)
    {

        $processInstanceId = $request->request->get('processInstanceId');
        $serviceId = $request->request->get('serviceId');
        $statusId = $request->request->get('statusId');
        $serviceName = $this->getDoctrine()->getRepository(ServiceDefinition::class)->find($serviceId)->getName();
        $this->get('case.process_handler')->changeServiceStatus($processInstanceId, $serviceId, $statusId);

        return new JsonResponse([
            'message' => $serviceName.': '. $this->get('translator')->trans('service.status.changed.')
        ], 200);
    }

    /**
     * @Route("/new-version-request", name="admin_project_new_version_request", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_PROCESS_EDITOR')")
     */
    public function chartAction(Request $request)
    {

        $mailer = $this->get('mailer');
        $https['ssl']['verify_peer'] = false;
        $https['ssl']['verify_peer_name'] = false;
        $mailer->getTransport()->setStreamOptions($https);

        $form = $this->createForm(DeployRequestType::class);
        $form->handleRequest($request);
        $response = 'OK';

        $date = $form->get('date')->getData();
        $info = $form->get('info')->getData();
        $adminMail = $this->getParameter('admin_email');

        if ($form->isSubmitted() && $form->isValid()) {
            $body = '<b>Data: '.$date.'</b><br/>'.
                    '<b>Użytkownik: '.$this->getUser()->getName().'</b><br/><br/>'.
                    $info;

            $message = \Swift_Message::newInstance()
                ->setSubject('Zlecenie nowej wersji ATLAS - '.$date)
                ->setFrom('atlas-noreply@starter24.pl')
                ->setTo($adminMail)
                ->setBody($body, 'text/html');

            $mailer->send($message);
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/set-foreign-partner-contact-data/{id}/{instanceId}", name="ajax_set_foreign_partner_contact_data", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_CASE_SERVICE_ACTIONS')")
     */
    public function foreignPartnerContactDataAction(Request $request, $id, $instanceId)
    {

        $groupProcessId = $this->getDoctrine()->getRepository(ProcessInstance::class)->find($instanceId)->getGroupProcess()->getId();
        $processHandler = $this->get('case.process_handler');
        $serviceId = $processHandler->getServiceIdFromGroupId($groupProcessId);

        $serviceType = $this->getDoctrine()->getRepository(ValueDictionary::class)->getPartnerServiceTypeByServiceId($serviceId);

        $processHandler = $this->get('case.process_handler');
        $email = $processHandler->getPartnerContact($id, $serviceType, 4, $groupProcessId);
        $phone = $processHandler->getPartnerContact($id, $serviceType, 2, $groupProcessId);
        $userId = $this->getUser()->getId();

        $processHandler->editAttributeValue('767,773,368',$groupProcessId, $email,'string','xxx', $userId, $userId);
        $processHandler->editAttributeValue('767,773,197',$groupProcessId, $phone,'string','xxx', $userId, $userId);

        $replacementCarGroup = $processHandler->serviceTopProgressGroup($groupProcessId, 3);

        if(!empty($replacementCarGroup)){
            $email = $processHandler->getPartnerContact($id, 5, 4, $groupProcessId);
            $phone = $processHandler->getPartnerContact($id, 5, 2, $groupProcessId);
            $processHandler->editAttributeValue('837,773,704',$replacementCarGroup, $id,'int','xxx', $userId, $userId);
            $processHandler->editAttributeValue('837,773,368',$replacementCarGroup, $email,'string','xxx', $userId, $userId);
            $processHandler->editAttributeValue('837,773,197',$replacementCarGroup, $phone,'string','xxx', $userId, $userId);
        }

        return new JsonResponse($email);
    }

    /**
     * @Route("/get-partner-contact/{partnerId}/{instanceId}/{contactType}/{serviceType}", name="ajax_partner_contact", options={"expose":true})
     * @Method("GET")
     * @param $partnerId
     * @param $instanceId
     * @param $contactType
     * @param $serviceType
     * @return JsonResponse
     */
    public function partnerContactAction($partnerId, $instanceId, $contactType, $serviceType = null)
    {
        $processHandler = $this->get('case.process_handler');
        $groupProcessId = $this->getDoctrine()->getRepository(ProcessInstance::class)->find($instanceId)->getGroupProcess()->getId();
        $serviceId = $processHandler->getServiceIdFromGroupId($groupProcessId);
        if(!$serviceType) {
            $serviceType = $this->getDoctrine()->getRepository(ValueDictionary::class)->getPartnerServiceTypeByServiceId($serviceId);
        }

        $contact = $this->get('case.process_handler')->getPartnerContact($partnerId, $serviceType, $contactType, $groupProcessId);

        return new JsonResponse($contact);
    }

    /**
     * @Route("/get-mute-modal-content/{groupProcessInstanceId}", name="ajax_mute_modal_content", options={"expose":true})
     * @Method("GET")
     * @param $groupProcessInstanceId
     * @return JsonResponse
     */
    public function muteContentAction($groupProcessInstanceId)
    {
        $value = (int)$this->get('case.process_handler')->getAttributeValue(self::ATTR_MUTE,$groupProcessInstanceId,'int');
        $options = [
            ['value' => '', 'label' => $this->get('translator')->trans('Odblokowana komunikacja')],
            ['value' => 2, 'label' => $this->get('translator')->trans('Zablokowane powiadomienia do klienta w całej sprawie')],
            ['value' => 1, 'label' => $this->get('translator')->trans('Zablokowane popwiadomienia do klienta i partnerów w całej sprawie')]
        ];

        $html = $this->renderView('@Operational/Default/partials/modals/mute-process-modal.html.twig',
        [
            'value' => $value,
            'options' => $options
        ]);


        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @Route("/edit-attribute-value/{path}/{groupProcessInstanceId}/{type}/{value}", name="ajax_edit_attribute_value", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_CONSULTANT')")
     * @param Request $request
     * @param $path
     * @param $groupProcessInstanceId
     * @param $type
     * @param $value
     * @return JsonResponse
     */
    public function editAttributeValueAction(Request $request, $path, $groupProcessInstanceId, $type, $value)
    {
        $processHandler = $this->get('case.process_handler');
        $userId = $this->getUser()->getId();
        $processHandler->editAttributeValue($path, $groupProcessInstanceId, $value, $type, 'xxx', $userId, $userId);


        return new JsonResponse();
    }

    /**
     * @Route("/update-case-mute/{rootId}/{value}", name="ajax_update_case_mute", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_CONSULTANT')")
     * @param Request $request
     * @param $rootId
     * @param null $value
     * @return JsonResponse
     */
    public function updateCaseMuteAction(Request $request, $rootId, $value = null)
    {

        $processHandler = $this->get('case.process_handler');
        $noteHandler = $this->get('note.handler');
        $caseHandler = $this->get('case.handler');
        $user = $this->getUser();
        $userId = $this->getUser()->getId();
        $processHandler->editAttributeValue(self::ATTR_MUTE, $rootId, $value, 'int', 'xxx', $userId, $userId);
        if($value == 1){
            $content = $this->get('translator')->trans('W sprawie zablokowano powiadomienia do klienta i partnerów (%username%)',
                [
                    '%username%' => $user->getName()
                ]
            );
        }
        else if ($value == 2){
            $content = $this->get('translator')->trans('W sprawie zablokowano powiadomienia do klienta (%username%)',
                [
                    '%username%' => $user->getName()
                ]);
        }
        else{
            $content = $this->get('translator')->trans('W sprawie odblokowano powiadomienia (%username%)',
                [
                    '%username%' => $user->getName()
                ]);
        }

        $noteHandler->addNote($rootId, 'system', $content);
        $caseHandler->log($content, $rootId, $userId, 'user_action', $filter = 'notification_change');

        return new JsonResponse();
    }

    /**
     * @Route("/ajax-faq-content/{type}/{groupProcessInstanceId}", name="faq_content", options={"expose":true})
     * @Method("GET")
     * @param $type
     * @param $groupProcessInstanceId
     * @return JsonResponse
     */
    public function faqContentAction($type, $groupProcessInstanceId)
    {
        $value = null;
        $active = 0;

        $dictionaryRepo = $this->getDoctrine()->getRepository(ValueDictionary::class);

        if($groupProcessInstanceId) {
            $value = $this->get('case.process_handler')->getAttributeValue(self::ATTR_FAQ_ANSWERS, $groupProcessInstanceId, 'string');
            $activeInstances = $this->getDoctrine()->getRepository(ProcessInstance::class)->findBy(['step' => '1011.010', 'groupProcess' => $groupProcessInstanceId, 'active' => 1]);
            $active = count($activeInstances) > 0 ? 1 : 0;
        }

        /** @var ArrayCollection $categories */
        $items = $dictionaryRepo->findBy(['type' => $type]);

        $html = $this->renderView('@Operational/Default/partials/faq-content.html.twig',
        [
            'items' => $items,
            'active' => $active,
            'value' => $value
        ]);

        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @Route("/ajax-case-trash-check/{processInstanceId}", name="ajax_case_trash_check", options={"expose":true})
     * @Method("GET")
     * @param $processInstanceId
     * @return JsonResponse
     */
    public function caseTrashCheckAction($processInstanceId)
    {
        $check = $this->get('case.handler')->caseTrashCheck($processInstanceId);
        return new JsonResponse($check);
    }

    /**
     * @Route("/save-monitor-page", name="ajax_save_monitor_page", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @return JsonResponse
     */
    public function saveMonitorPageAction(Request $request)
    {

        $canMonitor = \WebBundle\Controller\DefaultController::_checkMonitorPage($this->getUser(), $this->get('app.query_manager'));

        if($canMonitor) {

            $processInstanceId = $request->request->get('process-instance-id', null);

            $pages = $request->request->get('pages');

            $processInstance = null;
            $lastProcessEnterHistory = null;

            if($processInstanceId) {

                $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find($processInstanceId);

                /** @var ProcessEnterHistory $lastProcessEnterHistory */
                $lastProcessEnterHistory = $this->getDoctrine()->getRepository('CaseBundle:ProcessEnterHistory')->getLastLogOfUserByProcessInstance($this->getUser(), $processInstance);

            }

            $em = $this->getDoctrine()->getManager();

            foreach ($pages as $page) {

                $pageLog = new ProcessPageTime();
                $pageLog->setProcessInstance($processInstance);
                $pageLog->setProcessEnterHistory($lastProcessEnterHistory);
                $pageLog->setUser($this->getUser());
                $pageLog->setTime(intval($page['time']));
                $pageLog->setHost($page['host']);

                if(isset($page['startDate'])) {

                    try {
                        $pageLog->setStartDate((new \DateTime($page['startDate'])));
                    }
                    catch (\Exception $exception) {}

                }

                $em->persist($pageLog);

            }

            $em->flush();

        }

        return new JsonResponse([
            'success' => true
        ]);

    }


    /**
     * @Route("/parse-text", name="ajax_parse_text", options={"expose":true})
     * @Method("POST")
     * @Security("is_granted('ROLE_DASHBOARD_INTERFACE')")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function parseTextAction(Request $request)
    {

        $text = $request->request->get('text');
        $processInstanceId = $request->request->get('processInstanceId');

        $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find($processInstanceId);

        $attributeParser = $this->get('case.attribute_parser.service');
        $attributeParser->setProcessInstanceId($processInstance->getId());
        $attributeParser->setGroupProcessInstanceId($processInstance->getGroupProcess()->getId());

        $parsedText = $attributeParser->parse($text);

        return new JsonResponse([
            'success' => true,
            'text' => $parsedText
        ]);

    }

    /**
     * Results in "should" or "should not" show the comments in RS
     * @Route("/get-rs-validation", name="get_rs_validation", options={"expose":true})
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse $response
     */
    public function getServiceRSCommentValidationResultAjax(Request $request)
    {
        $attributeValueRepo = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue');
        $serviceGroupIdAttributeValueId = $request->get('attribute_value_id');
        $result = false;

        try{
            $serviceGroupIdAttributeValue = $attributeValueRepo->find($serviceGroupIdAttributeValueId);
            if($serviceGroupIdAttributeValue){

                $serviceGroup = $serviceGroupIdAttributeValue->getValueInt();
            }else{
                throw new \Exception('ServiceGroup Attribute Not found');
            }
            if (!is_null($serviceGroup)) {
                $platform = $attributeValueRepo->findOneBy(
                    ['groupProcessInstance' => $serviceGroup,
                        'attributePath' => '253']
                );
                if ($platform ?? false) {
                    $platform = $platform->getValueInt();
                } else {
                    $root = $this->get('case.process_handler')->getRootId($serviceGroup);
                    $platform = $attributeValueRepo->findOneBy(
                        ['groupProcessInstance' => $root,
                            'attributePath' => '253']
                    );
                    if ($platform ?? false) {
                        $platform = $platform->getValueInt();
                    }
                }
                if (!is_null($platform)) {

                    if (in_array($platform, [6,
                            10,
                            11,
                            31,
                            76]
                    )) {

                        if ($platform == 6) {
                            $program = $attributeValueRepo->findOneBy(
                                [
                                    'groupProcessInstance' => $serviceGroup,
                                    'attributePath' => '202'
                                ]
                            );
                            if(!is_null($program)){
                                $program = $program->getValueString();
                            }else{
                                throw new \Exception('Program attribute Not found');
                            }
//                    Checking programId
                            if ($program == '402' || $program == '421') {
                                $result = false;
                            } else {
                                $result = true;
                            }
                        } else {
                            $result = true;
                        }
                    }
                }
                else{
                    throw  new \Exception('Platform Not found');
                }
            }  else{
                throw  new \Exception('ServiceGroup Not found');
            }
        }catch (\Exception $exception)
        {
            $result = false;
        }

        return new JsonResponse(['result'=>$result]);
    }

}
