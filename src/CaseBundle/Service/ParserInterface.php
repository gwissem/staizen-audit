<?php

namespace CaseBundle\Service;

interface ParserInterface
{
    public function parse(string $input): string;

    public function getSchemaRegex(): string;
}