<?php

namespace CaseBundle\Service;

use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;

class SimulationTaskService
{

    protected $em;
    protected $pusher;

    public function __construct(EntityManager $em, PusherInterface $pusher)
    {
        $this->em = $em;
        $this->pusher = $pusher;
    }

}