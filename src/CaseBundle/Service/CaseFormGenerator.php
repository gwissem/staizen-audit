<?php

namespace CaseBundle\Service;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\FormTemplate;
use CaseBundle\Entity\PostponeProcessInstance;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Entity\StepAttributeDefinition;
use CaseBundle\Utils\ParserMethods;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use OperationalBundle\Service\DashboardStepBuilderService;
use OperationalBundle\StepBuilderComponent\VirtualStepInstance;
use PDO;
use SoapBundle\Model\Caller;
use SoapBundle\Model\CallReport;
use SoapBundle\Model\DiagnosticInfo;
use SoapBundle\Model\Event;
use SoapBundle\Model\EventCorrelation;
use SoapBundle\Model\Location;
use SoapBundle\Model\Position;
use SoapBundle\Model\Vehicle;
use SoapBundle\Model\VehicleStatus;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Twig_Environment;
use UserBundle\Entity\User;
use UserBundle\UserBundle;


/**
 * Service do generowania HTML'a formularzy konkretnej instancji kroku
 *
 * Class CaseFormGenerator
 * @package CaseBundle\Service
 */
class CaseFormGenerator
{

    const USER_AUTOMAT_ID = 1;
    const NOTES = '406';
    const NOTE = '406,226';
    const NOTE_TYPE = '406,226,229';
    const NOTE_PHONE = '406,226,197';
    const NOTE_EMAIL = '406,226,198';
    const NOTE_CONTENT = '406,226,227';
    const NOTE_DIRECTION = '406,226,407';
    const NOTE_ATTACHMENT = '406,226,228';
    const NOTE_SUBJECT = '406,226,409';
    const NOTE_STATUS = '406,226,734';
    const NOTE_EMAIL_SENDER = '406,226,735';
    const NOTE_EMAIL_CONTENT = '406,226,737';
    const NOTE_SPECIAL_DATA = '406,226,1168';


    const ATTR_SPECIAL_SITUATION_ID = "415";
    const ATTR_MUTE = '860';

    const NOTE_TYPE_TEXT = 'text';
    const NOTE_TYPE_CHAT = 'chat';
    const NOTE_TYPE_EMAIL = 'email';
    const NOTE_TYPE_SMS = 'sms';
    const NOTE_TYPE_PHONE = 'phone';
    const NOTE_TYPE_SYSTEM = 'system';

    const NOTE_SPECIAL = '406,226,930';

    const STEP_OF_PARTNER_INTERFACE = '1091.011';
    const STEP_OF_SERVICES = '1011.010';
    const GROUP_SUPERVISOR = 3360;

    /** @var ContainerInterface $container */
    protected $entityManager;

    /** @var Twig_Environment  */
    protected $twig;

    /** @var ProcessHandler  */
    protected $processHandler;

    /** @var NoteHandler */
    protected $noteHandler;

    /** @var FormTemplateGenerator  */
    protected $formTemplateGenerator;

    /** @var DashboardStepBuilderService  */
    protected $dashboardStepBuilder;

    private $currentStepId;
    private $currentGroupProcessId;
    private $rootId;

    private $queryManager;

    private $processToken = null;

    public function __construct(EntityManager $entityManager,
                                Twig_Environment $twig,
                                ProcessHandler $processHandler,
                                FormTemplateGenerator $formTemplateGenerator,
                                QueryManager $queryManager,
                                NoteHandler $noteHandler,
                                DashboardStepBuilderService $dashboardStepBuilderService
    )
    {
        $this->entityManager = $entityManager;
        $this->twig = $twig;
        $this->processHandler = $processHandler;
        $this->formTemplateGenerator = $formTemplateGenerator;
        $this->queryManager = $queryManager;
        $this->noteHandler = $noteHandler;
        $this->dashboardStepBuilder = $dashboardStepBuilderService;
    }

    /**
     * @param array $opts
     * @return array
     */
    private function configureGenerateOptions($opts = [])
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults(
            [
                'overview' => false,
                'displayNotes' => false,
                'publicForm' => false,
                'process_token' => null,
                'previewMode' => false
            ]
        );
        return $resolver->resolve($opts);
    }

    /**
     * @param $processInstance
     * @param array $opts
     * @return array
     */
    public function generateFormOfStepInstance(ProcessInstance $processInstance, $opts = [])
    {

        $options = $this->configureGenerateOptions($opts);

        $this->processToken = $options['process_token'];
        $this->currentGroupProcessId = $processInstance->getGroupProcess()->getId();
        $this->rootId = $processInstance->getRoot()->getId();
        $this->currentStepId = $processInstance->getStep()->getId();

        /**
         * $params = [
         *  id, active, groupProcessId, rootId, stepId, name, description, cancelDescription, dateEnter, lifeTime, percentage, referenceTime
         * ]
         */
        $params = $this->processHandler->getProcessInstanceDescription($processInstance->getId());
        $params['choice_variant'] = $processInstance->getChoiceVariant();

        // change for performance
        /** @var Step $step */
        $step = $this->entityManager->getRepository('CaseBundle:Step')->find($processInstance->getStep()->getId());
//        $step = $this->entityManager->getRepository('CaseBundle:Step')->findStepWithFormTemplateAndControls($processInstance->getStep()->getId());

        /**
         * Interface / Creator Partnera
         */

        if($params['stepId'] === self::STEP_OF_PARTNER_INTERFACE) {
            return $this->getPartnerInterface($params, $step);
        }

        $formControlValues = $this->processHandler->formControls($params['id']);

        /**
         * Sprawdzenie czy eCall
         */
        if ($params['stepId'] == "1023.002" || $params['stepId'] == "1023.003") {
            return [
                'id' => $processInstance->getId(),
                'html' => $this->magicRenderForEcall($params, $this->currentGroupProcessId),
                'controls' => [],
                'params' => $params,
                'prependHtml' => '',
                'type' => 'ECALL'
            ];
        }

        /**
         *  Sprawdzenie czy OverView - chociaż od jakiegos czasu jest wyłączone
         */

        if($options['overview']) {
            return [
                'id' => 'xxx',
                'html' => null,
                'controls' => null,
                'params' => $params,
                'prependHtml' => null,
                'type' => 'OVERVIEW'
            ];
        }

        if($step->getProcessDefinition()->getType() == ProcessDefinition::TYPE_QUIZ) {

            $this->formTemplateGenerator->setAdditionalData([
                'groupProcessId' => $this->currentGroupProcessId,
                'processInstanceId' => $processInstance->getId(),
                'stepId' => $step->getId()
            ]);

            $this->formTemplateGenerator->setFullMode();

            return [
                'id' => $step->getId(),
                'html' => $this->generateWrapperFormOfStep($step, $params, $options['publicForm'], $options['previewMode']),
                'controls' => [],
                'params' => $params,
                'prependHtml' => $this->formTemplateGenerator->generateQuizHtml($step, $params),
                'type' => 'FORM'
            ];

        }

        if (!$step->hasFormTemplate()) throw new NotFoundResourceException('Krok (' . $step->getId() . ') nie posiada formularza');

        /** @var FormTemplate $formTemplate */
        $formTemplate = $step->getFormTemplate();

        /** TODO - NOWY SYSTEM GENEROWANIA FORMULARZA - in dev */

//            $virtualStep = $this->dashboardStepBuilder->createFormBuilder([
//                'processInstance' => $processInstance
//            ]);
//
//            $virtualStep->initFormControls();

        /** TODO - KONIEC NOWEGO SYSTEMU */

        $this->formTemplateGenerator->setAdditionalData([
            'groupProcessId' => $this->currentGroupProcessId,
            'processInstanceId' => $processInstance->getId(),
            'formControlsAttribute' => $formControlValues,
            'stepId' => $step->getId()
        ]);

        $formTemplateJSON = $this->formTemplateGenerator->generate($formTemplate, FormTemplateGenerator::MODE_FULL);

        return [
            'id' => $step->getId(),
            'html' => $this->generateWrapperFormOfStep($step, $params, $options['publicForm'], $options['previewMode']),
            'controls' => $formTemplateJSON,
            'params' => $params,
            'prependHtml' => '',
            'type' => 'FORM'
        ];

    }

    private function getPartnerInterface($params, Step $step) {

        return [
            'id' => $params['id'],
            'html' => $this->renderPartnerInterface($params, $step),
            'controls' => [],
            'notes' => [],
            'params' => $params,
            'prependHtml' => '',
            'type' => 'PARTNER_INTERFACE'
        ];

    }

    /**
     * @param $params
     * @param Step $step
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function renderPartnerInterface($params, Step $step) {

        $partnerName = $this->processHandler->getAttributeValue('595,614', $params['groupProcessId'], 'string');

        return $this->twig->render('@Case/FormTemplate/templates/partner-interface.html.twig', [
            'formTitle' => $step->getName(),
            'formId' => $step->getId(),
            'params' => $params,
            'partnerName' => $partnerName
        ]);

    }

    public function generateNotesList($formControlValues, $groupProcessInstanceId)
    {

        $noteParents = [];

        $root = $this->processHandler->getAttributeValue(self::NOTES, $groupProcessInstanceId);

        foreach ($formControlValues as $key => $formControl) {
            $path = $formControl['attribute_path'];
            if (strpos($path, self::NOTE, 0) !== false) {
                $parentId = $formControl['parent_attribute_value_id'];
                $noteParents[$parentId][$formControl['attribute_path']] = $formControl;
            }
        }

        $notes = $this->noteHandler->prepareNoteList($noteParents, $this->rootId);

        return [
            'list' => $notes,
            'root' => $root ? $root[0]['id'] : NULL
        ];

    }

    public function getNameOfParents($attributePath)
    {

        $name = '';

        if (strpos($attributePath, ',') !== false) {

            $parentsPath = explode(',', $attributePath);
            array_pop($parentsPath);
            /** Wyrzucam ostatnią ścieżkę z tablicy - jest to ścieżka tego atrybutu */

            $parentsName = $this->entityManager->getRepository('CaseBundle:Attribute')->findAttributesByIds($parentsPath);

            $name .= '(';
            $i = 0;
            $n = count($parentsName);
            foreach ($parentsName as $parent) {
                $name .= ($parent['name'] . ((++$i !== $n) ? ', ' : ''));
            }

            $name .= ')';
        }

        return $name;
    }

    /**
     * @param $params
     * @param $groupProcessId
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function magicRenderForEcall($params, $groupProcessId) {

        $twig = '';
        $data = [];

        if ($params['stepId'] == "1023.002") {
            $locations = $this->processHandler->getAttributeValue(Location::PATH_LOCATION, $groupProcessId);
            $traces = [];
            if (!empty($locations)) {
                foreach ($locations as $key => $location) {
                    $parentAttributeValueId = $location['id'];
                    if ($key == 0) {
                        $locationSet = $this->setupLocationParameters($groupProcessId, $params['stepId'], $parentAttributeValueId);
                    } else {
                        $trace = [
                            'LATITUDE' => $this->processHandler->getAttributeValue(Location::PATH_LATITUDE, $groupProcessId, AttributeValue::VALUE_STRING, NULL, $parentAttributeValueId),
                            'LONGITUDE' => $this->processHandler->getAttributeValue(Location::PATH_LONGITUDE, $groupProcessId, AttributeValue::VALUE_STRING, NULL, $parentAttributeValueId)
                        ];
                        $traces[] = $trace;
                    }
                }
            } else {
                $locationSet = $this->setupLocationParameters($groupProcessId, $params['stepId']);
            }

            $data['LOCATION']['CURRENT'] = $locationSet;
            $data['LOCATION']['TRACES'] = json_encode($traces);
            $data['ROLLED'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_ROLLED, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['THEFT_TRACK'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_THEFT_TRACK, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['SIDE_CRASH'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_SIDE_CRASH, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['REAR_CRASH'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_REAR_CRASH, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['PASSENGER_SIDE_CRASH'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_PASSENGER_SIDE_CRASH, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['NUMBER_OF_TRIGGERED_AIRBAGS'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_NUMBER_OF_TRIGGERED_AIRBAGS, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['NUMBER_OF_FASTEN_SEAT_BELTS'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_NUMBER_OF_FASTEN_SEAT_BELTS, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['NUMBER_OF_OCCUPIED_SEATS'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_NUMBER_OF_OCCUPIED_SEATS, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['MOVING'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_MOVING, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['IN_ALARM'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_IN_ALARM, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['IGNITION_ON'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_IGNITION_ON, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['FRONT_CRASH'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_FRONT_CRASH, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['ENGINE_ON'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_ENGINE_ON, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['DRIVER_SIDE_CRASH'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_DRIVER_SIDE_CRASH, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['DECELERATION'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_DECELERATION, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['CRASHED'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_CRASHED, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['AIRBAG_TRIGGERED'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_AIRBAG_TRIGGERED, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['VICTIMS'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_VICTIMS, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['DAMAGE_TYPE'] = $this->processHandler->prepareDataForAttribute(VehicleStatus::PATH_DAMAGE_TYPE, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);

            $data['COLOR'] = $this->processHandler->prepareDataForAttribute(Vehicle::PATH_COLOR, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['MAKE'] = $this->processHandler->prepareDataForAttribute(Vehicle::PATH_MAKE, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['MODEL'] = $this->processHandler->prepareDataForAttribute(Vehicle::PATH_MODEL, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['LICENCE_PLATE'] = $this->processHandler->prepareDataForAttribute(Vehicle::PATH_LICENCE_PLATE, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['VIN'] = $this->processHandler->prepareDataForAttribute(Vehicle::PATH_VIN, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['CAR_PHONE'] = $this->processHandler->prepareDataForAttribute(Event::PATH_CALLER_ID, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['ENERGY'] = $this->processHandler->prepareDataForAttribute(Vehicle::PATH_ENERGY, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);

            $data['FIRSTNAME'] = $this->processHandler->prepareDataForAttribute(Caller::PATH_FIRSTNAME, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['LASTNAME'] = $this->processHandler->prepareDataForAttribute(Caller::PATH_LASTNAME, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['CALLBACK_PHONE'] = $this->processHandler->prepareDataForAttribute(Caller::PATH_CALLBACK_PHONE, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['CALLBACK_PHONE_2'] = $this->processHandler->prepareDataForAttribute(Caller::PATH_CALLBACK_PHONE_2, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);

            $data['EVENT_ATP_ID'] = $this->processHandler->prepareDataForAttribute(EventCorrelation::PATH_ATP_EVENT_ID, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            $data['EVENT_CREATED_AT'] = $this->processHandler->prepareDataForAttribute(EventCorrelation::PATH_CREATED_AT, AttributeValue::VALUE_DATE, $groupProcessId, $params['stepId']);
            $data['EVENT_ACTIVATION_METHOD'] = $this->processHandler->prepareDataForAttribute(Event::PATH_ACTIVATION, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);

            $data['ENERGY'] = $this->processHandler->prepareDataForAttribute(Vehicle::PATH_ENERGY, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);

            /** FROM step 1023.003 */
            $data['CAUSE'] = $this->processHandler->prepareDataForAttribute(CallReport::PATH_CAUSE, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['QUALIFICATION'] = $this->processHandler->prepareDataForAttribute(CallReport::PATH_QUALIFICATION, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['REDUNDANT_ID'] = $this->processHandler->prepareDataForAttribute(CallReport::PATH_REDUNDANT_ID, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);
            /** End */

            $twig = $this->twig->render('@Socket/Forms/tests.html.twig', [
                'data' => $data,
                'params' => $params
            ]);

        } /** Pobranie drugiego formularza */
        elseif ($params['stepId'] == "1023.003") {

            $data['CAUSE'] = $this->processHandler->prepareDataForAttribute(CallReport::PATH_CAUSE, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['QUALIFICATION'] = $this->processHandler->prepareDataForAttribute(CallReport::PATH_QUALIFICATION, AttributeValue::VALUE_STRING, $groupProcessId, $params['stepId']);
            $data['REDUNDANT_ID'] = $this->processHandler->prepareDataForAttribute(CallReport::PATH_REDUNDANT_ID, AttributeValue::VALUE_INT, $groupProcessId, $params['stepId']);

            $twig = $this->twig->render('@Socket/Forms/tests2.html.twig', [
                'data' => $data,
                'params' => $params
            ]);
        }

        return $twig;
    }

    protected function setupLocationParameters($groupProcessId, $stepId, $parentAttributeValueId = NULL)
    {
        $locationSet = [
            'LATITUDE' => $this->processHandler->prepareDataForAttribute(Location::PATH_LATITUDE, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'LONGITUDE' => $this->processHandler->prepareDataForAttribute(Location::PATH_LONGITUDE, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'CITY' => $this->processHandler->prepareDataForAttribute(Location::PATH_CITY, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'COUNTRY' => $this->processHandler->prepareDataForAttribute(Location::PATH_COUNTRY, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'AREA_NAME_1' => $this->processHandler->prepareDataForAttribute(Location::PATH_AREA_NAME_1, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'AREA_NAME_2' => $this->processHandler->prepareDataForAttribute(Location::PATH_AREA_NAME_2, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'AREA_NAME_3' => $this->processHandler->prepareDataForAttribute(Location::PATH_AREA_NAME_3, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'STREET_NAME' => $this->processHandler->prepareDataForAttribute(Location::PATH_STREET_NAME, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'STREET_NUMBER' => $this->processHandler->prepareDataForAttribute(Location::PATH_STREET_NUMBER, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'ZIP' => $this->processHandler->prepareDataForAttribute(Location::PATH_ZIP, AttributeValue::VALUE_STRING, $groupProcessId, $stepId, $parentAttributeValueId),
            'COMMENT' => $this->processHandler->prepareDataForAttribute(Location::PATH_COMMENT, AttributeValue::VALUE_TEXT, $groupProcessId, $stepId, $parentAttributeValueId)
        ];

        return $locationSet;
    }

    /**
     * @param Step $step
     * @param $params
     * @param bool $publicForm
     * @param bool $previewMode
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function generateWrapperFormOfStep($step, $params, $publicForm = false, $previewMode = false)
    {

        $stepNumber = explode(".", $step->getId());
        $situationSelect = null;
        $previousProcessId = NULL;
        $isRunningServices = false;
        $canReopenClosedCase = false;
        $muteCommunication = false;

        $isQuiz = $step->getProcessDefinition()->getType() == ProcessDefinition::TYPE_QUIZ;
        if (!$step->hasFormTemplate() && !$isQuiz) throw new NotFoundResourceException('Krok (' . $step->getId() . ') nie posiada formularza');

        if($previewMode) {

            return $this->twig->render('@Operational/Forms/preview-base.html.twig', [
                'formTitle' => $step->getName(),
                'formId' => $step->getId(),
                'stepNumber' => $stepNumber[1],
                'params' => $params,
                'previewMode' => $previewMode
            ]);

        }
        else {

            if(in_array($stepNumber[0], ["1011", "1012"])) {
                $situationSelect = $this->getSelectOfSpecialSituation($params);
            }

            $muteCommunication = (int)$this->processHandler->getAttributeValue(self::ATTR_MUTE,$params['groupProcessId'],'int');

            if($step->canPrevious() && $this->processToken) {

                if($step->isMobileForm()) {

                    /** @var ProcessInstance $currentProcess */
                    $currentProcess = $this->entityManager->getRepository('CaseBundle:ProcessInstance')->find($params['id']);
                    $previousProcessId = $currentProcess->getPreviousProcessInstance()->getId();

                }
                else {
                    // TODO - trzeba się zastanowić, czy to szukanie poprzedniego kroku na pewno jest prawidłowe - jak nie to wykorzystać klasyczne, powyższe
                    $prevProcess = $this->entityManager->getRepository('CaseBundle:ProcessInstance')->findPreviousProcess($params['id'], $params['groupProcessId'], $this->processToken);

                    if(count($prevProcess) > 0) {
                        $previousProcessId = $prevProcess[0]['id'];
                    }

                }

            }

            if(isset($params['groupProcessId'])) {
                $parameters = [
                    [
                        'key' => 'groupProcessInstanceId',
                        'value' => (int)$params['groupProcessId'],
                        'type' => PDO::PARAM_INT
                    ],
                    [
                        'key' => 'services',
                        'value' => '',
                        'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                        'length' => 100
                    ]
                ];

                $result = $this->queryManager->executeProcedure('EXEC p_running_services @groupProcessInstanceId = :groupProcessInstanceId, @servicesIds = :services', $parameters);

                if(!empty($result)) {
                    $isRunningServices = (!empty($result["services"]));
                }
            }

//            if($step->getId() == self::STEP_OF_SERVICES){
//                $canReopenClosedCase = $this->processHandler->canReopenCase($params['rootId']);
//            }

            $postponeProcess = null;

            if(!empty($params['id'])) {
                /** @var PostponeProcessInstance $postponeProcess */
                $postponeProcess = $this->entityManager->getRepository('CaseBundle:PostponeProcessInstance')->findOneBy([
                    'processInstance' => $params['id']
                ]);

            }

            $platformName = '';
            try {


                $platformId = $this->processHandler->getAttributeValue('253', $this->rootId);
                if(count($platformId))
                {
                    $platformId = $platformId[0]['value_int'];
                    $platform = $this->entityManager->getRepository('CaseBundle:Platform')->find($platformId);
                    $platformName = $platform->getSmsSignature() != '' ? $platform->getSmsSignature():$platform->getName() ;
                }
            }catch (\Exception $exception )
            {
            }


            return $this->twig->render('@Operational/Forms/base.html.twig', [
                'formTitle' => $step->getName(),
                'availableConfigs' => ParserMethods::$availableConfigKeys,
                'formId' => $step->getId(),
                'stepNumber' => $stepNumber[1],
                'params' => $params,
                'previewMode' => $previewMode,
                'isPublic' => $publicForm,
                'allServicesClosed' => !$isRunningServices,
                'situationSelect' => $situationSelect,
                'stop' => $step->isStop(),
                'canReopen' => ((!$isRunningServices && $step->isReopen()) || ($step->isReopen() && ($stepNumber[0] == "1028" || in_array("1204", $stepNumber)))),
                'canPrevStep' => $step->canPrevious(),
                'canMobilePrevStep' => ($previousProcessId && $step->isMobileForm() && $params['active']),
                'previousProcessId' => $previousProcessId,
                'forwardVariant' => $step->getForwardVariant(),
                'forwardVariantInfo' => $step->getForwardVariantInfo(),
                'canReopenClosedCase' => $canReopenClosedCase,
                'postponeProcess' => $postponeProcess,
                'platformName' =>$platformName ?? '',
                'mute' => $muteCommunication,
                'isMobileForm' => $step->isMobileForm()
            ]);

        }

    }

    /**
     * TODO - trzeba to gdzieś do bazy wrzucić - żeby konfiguralne było
     *
     * @param Step $step
     * @return bool
     */
    private function isMobileStep(Step $step) {
        return in_array($step->getId(), [
            '1011.088',
            '1011.089',
            '1011.093',
            '1011.094'
        ]);
    }

    private function getSelectOfSpecialSituation($params) {

        $attribute = $this->entityManager->getRepository('CaseBundle:Attribute')->find(self::ATTR_SPECIAL_SITUATION_ID);
        $options = $this->queryManager->executeProcedure($attribute->getQuery());
        if(isset($params['groupProcessId']) && isset($params['stepId'])) {
            $situation = $this->processHandler->prepareDataForAttribute('415', AttributeValue::VALUE_INT, $params['groupProcessId'], $params['stepId']);
        }
        else {
            $situation = [
                'type' => '',
                'id' => '',
                'path' => '',
                'groupProcessId' => '',
                'stepId' => '',
                'value' => '',
            ];
        }

        return [
            'options' => $options,
            'attribute' => $situation
        ];

    }
}