<?php

namespace CaseBundle\Service;

use AppBundle\Entity\Log;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\HelpCase;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\ServiceDefinition;
use CaseBundle\Utils\StaticCache;
use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use Moment\Moment;
use MssqlBundle\PDO\PDO;
use Predis\Client;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;
use UserBundle\Service\UserInfoService;
use CaseBundle\Utils\Snoopy;

class ProcessHandler
{

    const REDIS_USER_AT_INSTANCE_PREFIX = 'user_at_instance_';
    const MEDICAL_PERMISSION_INVALID = 2;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var QueryManager
     */
    public $queryManager;

    /**
     * @var Client
     */
    protected $redis;

    /**
     * @var UserInfoService
     */
    protected $userInfo;

    /**
     * @var PusherInterface
     */
    protected $pusher;

    /**
     * @var StaticCache
     */
    protected $staticCache;

    /** @var boolean */
    private $enableLogCepik = true;

    const PROCESS_ID_REPLACEMENT_CAR = 1007;
    const SERVICE_TYPE_REPLACEMENT_CAR = 5;
    const CONTACT_TYPE_EMAIL = 4;

    public function __construct(UserInfoService $userInfo, EntityManager $em, QueryManager $queryManager, PusherInterface $pusher, Client $redis)
    {
        $this->userInfo = $userInfo;
        $this->queryManager = $queryManager;
        $this->em = $em;
        $this->pusher = $pusher;
        $this->redis = $redis;
        $this->staticCache = new StaticCache($redis);
    }

    public static function parseMaxInt($number) {

        return (intval($number) > 2147483647) ? 2147483646 : intval($number);

    }

    public function getAttributeStructureValue($attributeValueId) {

        $parameters = [
            [
                'key' => 'attributeValueId',
                'value' => (int)$attributeValueId,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            'EXEC dbo.p_attribute_structure_value @attribute_value_id= :attributeValueId',
            $parameters);

    }

    /**
     * @param $attributeValueId
     * @return array
     */
    public function getAttributeHistory($attributeValueId)
    {
        $parameters = [
            [
                'key' => 'attributeValueId',
                'value' => $attributeValueId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "SELECT * FROM dbo.f_attribute_value_history(:attributeValueId)",
            $parameters
        );

        return $result;

    }

    /**
     * @param $userId
     * @param string $locale
     * @return array
     */
    public function userHistory($userId, $locale = 'pl')
    {
        $parameters = [
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_history @userId = :userId, @locale = :locale ",
            $parameters
        );

        return $result;

    }

    /**
     * @param $stepId
     * @param null $previousProcessId
     * @param null $userId
     * @param null $originalUserId
     * @return integer // processInstanceId
     * @throws \Exception
     */
    public function start($stepId, $previousProcessId = NULL, $userId = NULL, $originalUserId = NULL)
    {
        $parameters = [
            [
                'key' => 'stepId',
                'value' => $stepId,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'previousProcessId',
                'value' => $previousProcessId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'originalUserId',
                'value' => $originalUserId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'processInstanceId',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
        ];

        $result = $this->queryManager->executeProcedure(
            'EXEC dbo.p_process_new @stepId = :stepId, @err = :err, @message = :message, @processInstanceId = :processInstanceId, ' .
            ' @userId = :userId, @originalUserId = :originalUserId, @previousProcessId = :previousProcessId',
            $parameters
        );

        if ($result['err'] !== 0) {
            throw new \Exception($result['message']);
        }

        return $result['processInstanceId'];
    }

    /**
     * @param $processInstanceId
     * @param int $priority
     */
    public function setPriorityOfProcess($processInstanceId, $priority = 10) {

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'priority',
                'value' => $priority,
                'type' => PDO::PARAM_INT
            ]
        ];

        $this->queryManager->executeProcedure('UPDATE dbo.process_instance SET priority = :priority WHERE id = :processInstanceId',$parameters, false);

    }

    /**
     * @param $userId
     * @param string $locale
     * @param null $processInstanceId
     * @return null
     */
    public function task($userId, $locale = 'pl', $processInstanceId = null)
    {
        $parameters = [
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'instanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            "EXEC dbo.p_task @userId = :userId, @locale = :locale, @instanceId = :instanceId",
            $parameters
        );

    }

    public function tasks($userId, $orgUserId = NULL, $filter = NULL, $type = 1, $locale = 'pl', $instanceId = 0, $timeLine = 0, $showInactive = 0,  $advancedFiltersArray = [])
    {

        $resolve = new OptionsResolver();

        $resolve->setDefaults([
            'postponeEnabled' => false,
            'vipEnabled' => false,
            'mailEnabled' => false,
            'dateFrom' => null,
            'dateTo' => null,
            'programs' => [],
            'attributeFilter' => null,
            'inactiveEnabled' => false,
            'category' => null
        ]);

        $advancedFilters = $resolve->resolve($advancedFiltersArray);

        $parameters = [
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'orgUserId',
                'value' => $orgUserId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'filter',
                'value' => $filter,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'type',
                'value' => $type,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'instanceId',
                'value' => $instanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'timeLine',
                'value' => $timeLine,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'showInactive',
                'value' => filter_var($showInactive, FILTER_VALIDATE_BOOLEAN),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'disablePostponeDate',
                'value' => filter_var($advancedFilters['postponeEnabled'], FILTER_VALIDATE_BOOLEAN),
                'type' => PDO::PARAM_INT
            ],

            [
                'key' => 'programs',
                'value' => (is_array($advancedFilters['programs'])) ? implode(",", $advancedFilters['programs']) : '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'vipFilter',
                'value' => filter_var($advancedFilters['vipEnabled'], FILTER_VALIDATE_BOOLEAN),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'dateFrom',
                'value' => $advancedFilters['dateFrom'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'dateTo',
                'value' => $advancedFilters['dateTo'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'attributeFilter',
                'value' => $advancedFilters['attributeFilter'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'mailFilter',
                'value' => filter_var($advancedFilters['mailEnabled'], FILTER_VALIDATE_BOOLEAN),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'categoryFilter',
                'value' => empty($advancedFilters['category']) ? null : $advancedFilters['category'],
                'type' => PDO::PARAM_INT
            ],
        ];

        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_tasks
             @userId = :userId,
              @orgUserId = :orgUserId, 
              @filter = :filter,
               @type = :type, 
               @locale = :locale, 
               @instanceId = :instanceId, 
               @timeLine = :timeLine, 
               @showInactive = :showInactive, 
               @disablePostponeDate = :disablePostponeDate, 
               @programs = :programs, 
               @dateFrom = :dateFrom, 
               @dateTo = :dateTo,
               @vipFilter = :vipFilter,
               @mailFilter = :mailFilter,
               @attributeFilter = :attributeFilter,
               @categoryFilter = :categoryFilter",
            $parameters
        );

        return $result;
    }

    public function checkPermission($userId, $processInstanceId, $checkActive = 0) {

        $parameters = [
            [
                'key' => 'userId',
                'value' => intval($userId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'processInstanceId',
                'value' => intval($processInstanceId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'checkActive',
                'value' => intval($checkActive),
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "SELECT dbo.f_processPermission (:processInstanceId, :userId, :checkActive) permission",
            $parameters
        );

        if(count($result) && isset($result[0]['permission'])) {
            if($this::MEDICAL_PERMISSION_INVALID === (int)$result[0]['permission']) {
                return 0;
            }

            return  filter_var($result[0]['permission'], FILTER_VALIDATE_BOOLEAN);
        }

        return false;
    }

    public function next($previousProcessInstanceId, $variant = 1, $userId = NULL, $originalUserId = NULL, $skipTransaction = 0, $skipStepFunction = 0, $skipErrors = true)
    {
        $variant = ($variant === null) ? 1 : $variant;

        $err = 0;
        $message = '';
        $notification = null;

        $parameters = [
            [
                'key' => 'previousProcessId',
                'value' => (int)$previousProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'skipTransaction',
                'value' => $skipTransaction,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'skipStepProcedure',
                'value' => $skipStepFunction,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'skipErrors',
                'value' => $skipErrors,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'originalUserId',
                'value' => $originalUserId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'variant',
                'value' => $variant,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'processInstanceIds',
                'value' => "",
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
            [
                'key' => 'token',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 50,
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_process_next @processInstanceIds = :processInstanceIds, @previousProcessId = :previousProcessId, " .
            " @userId = :userId, @originalUserId = :originalUserId, @skipTransaction = :skipTransaction, @skipStepProcedure = :skipStepProcedure, " .
            " @skipErrors = :skipErrors, @variant = :variant, @err = :err, @message = :message, @token= :token",
            $parameters
        );

        if ($output['err'] !== 0) {
            $err = $output['err'];
            $message = $output['message'];
        }
        else {
            $notification = $output['message'];
        }

        $ids = explode(',', $output['processInstanceIds']);
        $token = $output['token'];

        if($skipErrors){
            return $ids;
        }
        else {
            return [
                'err' => $err,
                'message' => $message,
                'notification' => $notification,
                'ids' => $ids,
                'token' => $token
            ];
        }

    }

    public function previous($processId, $userId = NULL, $originalUserId = NULL, $skipTransaction = 0)
    {
        $parameters = [
            [
                'key' => 'processId',
                'value' => $processId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'skipTransaction',
                'value' => $skipTransaction,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'originalUserId',
                'value' => $originalUserId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'processInstanceId',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_process_previous @processInstanceId = :processInstanceId, @processId = :processId, " .
            " @userId = :userId, @originalUserId = :originalUserId, @skipTransaction = :skipTransaction, @err = :err, @message = :message",
            $parameters
        );

        return $output;
    }

    public function changeProcessAncestors($groupProcessId, $parentProcessId, $rootProcessId, $userId = NULL, $originalUserId = NULL, $skipTransaction = 0)
    {
        $parameters = [
            [
                'key' => 'groupProcessId',
                'value' => $groupProcessId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'parentProcessId',
                'value' => $parentProcessId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'rootProcessId',
                'value' => $rootProcessId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'skipTransaction',
                'value' => $skipTransaction,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'originalUserId',
                'value' => $originalUserId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_process_change_ancestors @groupProcessId = :groupProcessId, @parentProcessId = :parentProcessId, " .
            " @rootProcessId = :rootProcessId, @skipTransaction = :skipTransaction, @userId = :userId, " .
            " @originalUserId = :originalUserId, @err = :err, @message = :message",
            $parameters
        );

        return $output;
    }

    public function prepareDataForAttribute($path, $valueType, $groupProcessId, $stepId, $parentAttributeValueId = NULL)
    {
        $attributeValue = $this->getAttributeValue($path, $groupProcessId, NULL, NULL, $parentAttributeValueId);

        if($valueType === "guess" && !empty($attributeValue)) {

            if(strpos($path, ",") !== false) {
                $ids = explode(",", $path);
                $attributeId = array_pop($ids);
                $attribute = $this->em->getRepository('CaseBundle:Attribute')->find(intval($attributeId));
            }
            else {
                $attribute = $this->em->getRepository('CaseBundle:Attribute')->find(intval($path));
            }

            if($attribute) {
                $value = $attributeValue[0][$attribute->getValueType()];
            }
            else {
                $value = (!empty($attributeValue)) ? $attributeValue[0]['value_' . $valueType] : "";
            }

        }
        else {
            $value = (!empty($attributeValue)) ? $attributeValue[0]['value_' . $valueType] : "";
        }

        return [
            'id' => (!empty($attributeValue)) ? $attributeValue[0]['id'] : "",
            'path' => $path,
            'value' => $value,
            'type' => $valueType,
            'groupProcessId' => $groupProcessId,
            'stepId' => $stepId
        ];
    }

    public function getAttributeValue($path, $groupProcessInstanceId, $valueType = NULL, $attributeValueId = NULL, $parentAttributeId = NULL, $limit = NULL, $forceUTCTimeZone = false)
    {

        $cacheOutput = $this->staticCache->getAttrValue($path, $groupProcessInstanceId, $valueType);

        if($cacheOutput === false) {

            $parameters = [
                ['key' => 'attributePath', 'value' => $path, 'type' => PDO::PARAM_STR],
                ['key' => 'parentAttributeId', 'value' => $parentAttributeId, 'type' => PDO::PARAM_INT],
                [
                    'key' => 'groupProcessInstanceId',
                    'value' => $groupProcessInstanceId,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ],
                [
                    'key' => 'attributeValueId',
                    'value' => $attributeValueId,
                    'type' => PDO::PARAM_INT,
                    'length' => 100
                ]
            ];

            $output = $this->queryManager->executeProcedure(
                "EXEC dbo.p_attribute_get2 @attributePath = :attributePath, @groupProcessInstanceId = :groupProcessInstanceId," .
                "@attributeValueId = :attributeValueId, @parentAttributeId = :parentAttributeId"
                ,
                $parameters
            );

            if (!empty($output[0]['value_date'])) {
                $date = date('Y-m-d H:i:s', strtotime($output[0]['value_date']));
                if($forceUTCTimeZone){
                    $moment = new Moment($date,'Europe/Warsaw');
                    $date = str_replace('+0000','Z',$moment->setTimezone('UTC')->format());
                }
                $output[0]['value_date'] = $date;
            }
            else if (!empty($output[0]['value_decimal'])) {
                $output[0]['value_decimal'] = $output[0]['value_decimal'] + 0;
            }


            if (count($output) > 0 && $valueType) {
                $output = $output[0]['value_' . $valueType];
            }
            else if (count($output) > 0 && $limit) {
                foreach ($output[0] as $key => $value) {
                    if ($value) {
                        $output[0][$key] = mb_substr($value, 0, $limit);
                    }
                }
            }

            $this->staticCache->setAttrValue($path, $groupProcessInstanceId, $valueType, $output);

        }
        else {
            $output = $cacheOutput;
        }

        return $output;
    }

    public function setProcessInstanceName($processInstanceId, $stepId)
    {
        $parameters = [
            ['key' => 'stepId', 'value' => $stepId, 'type' => PDO::PARAM_STR],
            ['key' => 'processInstanceId', 'value' => $processInstanceId, 'type' => PDO::PARAM_INT],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_step_name @step_id = :stepId, @processInstanceId = :processInstanceId," .
            " @message = :message"
            ,
            $parameters
        );

        return $output;
    }

    public function getRootForAttributeValue($path, $value, $valueType)
    {
        $parameters = [
            ['key' => 'attributePath', 'value' => $path, 'type' => PDO::PARAM_STR],
            ['key' => 'value', 'value' => $value, 'type' => $this->queryManager->castTypeToPdoType($valueType)],

        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_isAtributeInActiveProcess @attributePath = :attributePath, @" . $this->getValueTypeProperty($valueType) . ' = :value',
            $parameters
        );

        return $output;

    }

    public function getValueTypeProperty($type)
    {
        return 'value' . ucfirst(strtolower($type));
    }

    public function attributeAddStructure($parentValueId, $attributeId)
    {
        $parameters = [
            [
                'key' => 'parentValueId',
                'value' => $parentValueId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'attributeId',
                'value' => $attributeId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.P_attribute_add_structure @parent_attribute_value_id = :parentValueId, @def_attribute_id = :attributeId",
            $parameters
        );

        return $output;
    }

    public function attributeCopyStructure($sourceAttributeValueId, $targetAttributeValueId, $processInstanceId)
    {
        $parameters = [
            [
                'key' => 'sourceAttributeValueId',
                'value' => $sourceAttributeValueId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'targetAttributeValueId',
                'value' => $targetAttributeValueId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'processInstanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $this->queryManager->executeProcedure(
            "EXEC dbo.P_attribute_copy_structure @source_attribute_value_id = :sourceAttributeValueId, @target_attribute_value_id = :targetAttributeValueId, @process_instance_id = :processInstanceId",
            $parameters,
            false
        );

    }

    public function formControls($processInstanceId, $parentAttributeValueId = null, $returnWithNote = 1)
    {
        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'parentAttributeValueId',
                'value' => $parentAttributeValueId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'returnWithNote',
                'value' => $returnWithNote,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_form_controls @instance_id = :processInstanceId, @parentAttributeValueId = :parentAttributeValueId, @returnWithNote = :returnWithNote",
            $parameters
        );

        foreach ($output as &$attribute) {
            if (!empty($attribute['value_date'])) {
                $attribute['value_date'] = date('Y-m-d H:i:s', strtotime($attribute['value_date']));
            } else if (!empty($attribute['value_decimal'])) {
//                $attribute['value_decimal'] = number_format($attribute['value_decimal'], 2) + 0;
                $attribute['value_decimal'] =$attribute['value_decimal'] + 0;
            }
        }

        return $output;
    }

    public function pushNewTaskToUserGroup(Group $userGroup, $processInstanceId, $action = 'create')
    {
        $userIds = [];
        foreach ($userGroup->getUsers() as $user) {
            $userIds[] = $user->getId();
        }

        if (!empty($userIds)) {
            $this->pushNewTask($userIds, $processInstanceId, $action);
        }

        return true;
    }

    protected function pushNewTask($userIds, $processInstanceId, $action = 'create')
    {

        $processDescription = $this->getProcessInstanceDescription($processInstanceId);
        $data = [
            'users' => $userIds,
            'task' => $processDescription,
            'action' => $action
        ];
        $this->pusher->push($data, 'atlas_dashboard_case');

    }

    public function getProcessInstanceDescription($processInstanceId)
    {
        return $this->em->getRepository(ProcessInstance::class)->getProcessInstanceDescription($processInstanceId);
    }

    public function getActiveInstanceForRoot($groupProcessId)
    {
        $processInstanceRepository = $this->em->getRepository(ProcessInstance::class);
        $activeProcessInstances = $processInstanceRepository->findActiveProcessInstancesForRoot($groupProcessId);
        return !empty($activeProcessInstances) ? $activeProcessInstances[0]['id'] : NULL;
    }

    public function pushNewTaskToUserAtInstance($processInstanceId)
    {
        $userId = $this->getUserAtInstance($processInstanceId);
        if ($userId && $processInstanceId) {
            $this->pushNewTaskToUser($userId, $processInstanceId, 'update');
        }
    }

    public function getUserAtInstance($processInstanceId)
    {
        if ($processInstanceId) {
            return $this->redis->get(self::REDIS_USER_AT_INSTANCE_PREFIX . $processInstanceId);
        }
        return NULL;
    }

    public function pushNewTaskToUser($user, $processInstanceId, $action = 'create')
    {

        if ($user instanceof User) {
            $id = $user->getId();
        } else {
            $id = $user;
        }

        $this->pushNewTask([$id], $processInstanceId, $action);

    }

    /**
     * @param $groupProcessId
     * @param bool $alternativeDate
     * @param bool $enableLog
     */
    public function cepikAttributes($groupProcessId, $alternativeDate = false, $enableLog = true)
    {

        $this->enableLogCepik = $enableLog;

        try {

            if($this->enableLogCepik) {
                $this->logCepikSearch('Rozpoczęto przeszukiwanie Cepika dla GPI: ' . $groupProcessId, ($alternativeDate) ? '1' : '0' );
            }

            $stepId = 'xxx';
            $userInfo = $this->userInfo->getInfo();

            $datePath = $alternativeDate ? '74,233' : '74,561';

            $regNo = $this->getAttributeValue('74,72', $groupProcessId, 'string');
            $vin = $this->getAttributeValue('74,71', $groupProcessId, 'string');
            $firstRegDate = $this->getAttributeValue($datePath, $groupProcessId, 'date');

            if (!empty($regNo) && !empty($vin) && !empty($firstRegDate)) {
                $snoopy = new Snoopy();

                $snoopy->fetch('https://historiapojazdu.gov.pl/');
                $snoopy->setcookies();

                preg_match('!action="(https://historiapojazdu\.gov\.pl/strona\-glowna.[^"]+)"!', $snoopy->results, $matches);
                $post['URL'] = $matches[1];

                preg_match('!id="javax\.faces\.ViewState" value="(.[^"]+)"!', $snoopy->results, $matches);
                $post['ViewState'] = $matches[1];

                preg_match('!name="javax\.faces\.encodedURL" value="(.[^"]+)"!', $snoopy->results, $matches);
                $post['encodedURL'] = $matches[1];

                $snoopy->submit($post['encodedURL'], array(
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:formularz' => '_historiapojazduportlet_WAR_historiapojazduportlet_:formularz',
                    'javax.faces.encodedURL' => $post['encodedURL'],
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:rej' => $regNo,
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:vin' => $vin,
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:data' => date('d.m.Y', strtotime($firstRegDate)),
                    '_historiapojazduportlet_WAR_historiapojazduportlet_:btnSprawdz' => 'Sprawdź pojazd »',
                    'javax.faces.ViewState' => $post['ViewState']));

                if (strpos($snoopy->results, 'nieZnalezionoHistori') === false) {

                    $portletId = "";
                    preg_match('!id="_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt([0-9]+)!', $snoopy->results, $matches);
                    if (count($matches) > 1) {
                        $portletId = trim($matches[1]);
                    }

                    preg_match('!<span>Dopuszczalna masa całkowita: </span> <strong> <span class="value">([0-9]+)!', $snoopy->results, $matches);
                    $this->snoopyMatchToAttributeValue($matches, '74,76', $groupProcessId, 'int', $stepId, $userInfo['userId'], $userInfo['originalUserId']);

                    preg_match('!<span>Pojemność silnika: </span> <strong> <span class="value">([0-9]+)!', $snoopy->results, $matches);
                    $this->snoopyMatchToAttributeValue($matches, '74,434', $groupProcessId, 'int', $stepId, $userInfo['userId'], $userInfo['originalUserId']);

                    preg_match('!<span id="_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt' . $portletId . ':paliwo" class="lowercase">([0-9a-ząęćóśłńżź ]+)!', $snoopy->results, $matches);
                    $this->snoopyMatchToAttributeValue($matches, '74,120', $groupProcessId, 'int', $stepId, $userInfo['userId'], $userInfo['originalUserId'], 'engineType');

//                    preg_match('!<p class="km">Ostatni zarejestrowany stan licznika: <span class="strong">([0-9 ]+) km!', $snoopy->results, $matches);
//                    $this->snoopyMatchToAttributeValue($matches, '74,456', $groupProcessId, 'int', $stepId, $userInfo['userId'], $userInfo['originalUserId']);

                    $make = '';
                    $model = '';
                    $makeModel = $this->getAttributeValue('74,73', $groupProcessId, 'int');

                    preg_match('!<span id="_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt' . $portletId . ':marka" class="uppercase">([0-9_a-zA-Z ]+)!', $snoopy->results, $matches);
                    if (count($matches) > 1) {
                        $make = trim($matches[1]);
                    }
                    preg_match('!<span id="_historiapojazduportlet_WAR_historiapojazduportlet_:j_idt' . $portletId . ':model" class="uppercase">([0-9_a-zA-Z ]+)!', $snoopy->results, $matches);
                    if (count($matches) > 1) {
                        $model = trim($matches[1]);
                    }

                    if (!$makeModel && !empty($make) && !empty($model)) {
                        $this->editAttributeValue('74,73', $groupProcessId, $make . ' ' . $model, 'int', $stepId, $userInfo['userId'], $userInfo['originalUserId'], 'makeModel');
                    }

                    if($this->enableLogCepik) {
                        $this->logCepikSearch('Znaleziono historię auta i zakończono zapisywanie atrybutów dla GPI: ' . $groupProcessId, ($alternativeDate) ? '1' : '0' );
                    }

                } else {

                    if($this->enableLogCepik) {
                        $this->logCepikSearch('Nie znaleziono historii pojazdu.' . 'GPI: ' . $groupProcessId, $regNo, $vin, $firstRegDate);
                    }

                    if (!$alternativeDate) {
                        $this->saveOptionalDMC('74,73', $groupProcessId, $userInfo);
                        $this->cepikAttributes($groupProcessId, true, $this->enableLogCepik);
                    }
                }
            }
            else {

                if($this->enableLogCepik) {
                    $this->logCepikSearch('Nie ma wszystkich wymaganych danych.' . 'GPI: ' . $groupProcessId, $regNo, $vin, $firstRegDate);
                }

                if (!$alternativeDate) {
                    $this->cepikAttributes($groupProcessId, true, $this->enableLogCepik);
                }
            }
        } catch (\Exception $e) {

            if($this->enableLogCepik) {
                $this->logCepikSearch('Wyjątek:' . $e->getMessage() . '. GPI: ' . $groupProcessId);
            }

        }
    }

    private function logCepikSearch($error, $regNo = null, $vin = null, $firstRegDate = null) {

        $log = new Log();

        $log->setName('CEPIK_SEARCH');
        $log->setContent($error);
        $log->setParam1($regNo);
        $log->setParam2($vin);
        $log->setParam3($firstRegDate);

        $this->em->persist($log);
        $this->em->flush();

    }

    private function saveOptionalDMC($path, $groupProcessId, $userInfo)
    {

        $makeModel = $this->getAttributeValue($path, $groupProcessId);

        if (!empty($makeModel)) {
            $value = $makeModel[0]['value_int'];

            if ($value) {
                $dictionaryMatches = $this->queryManager->executeProcedure("SELECT * FROM dictionary WHERE typeD = 'makeModel' AND value = " . $value);

                if (!empty($dictionaryMatches)) {

                    $model = $dictionaryMatches[0]['textD'];

                    $models = [
                        'Volkswagen Up' => 1330,
                        'Skoda Favorit' => 1345,
                        'Skoda Felicia' => 1345,
                        'Skoda Forman' => 1345,
                        'Volkswagen Lupo' => 1444,
                        'Audi A1' => 1481,
                        'Audi A2' => 1551,
                        'Volkswagen Fox' => 1559,
                        'Skoda Citigo' => 1567,
                        'Volkswagen Polo' => 1685,
                        'Volkswagen Vento' => 1685,
                        'Skoda Fabia' => 1703,
                        'Skoda Rapid' => 1729,
                        'Volkswagen New-Beetle' => 1767,
                        'Audi 80' => 1780,
                        'Audi 90' => 1800,
                        'Audi Tt' => 1816,
                        'Audi Tt-Rs' => 1850,
                        'Audi Tt-S' => 1850,
                        'Volkswagen Scirocco' => 1874,
                        'Volkswagen Beetle' => 1875,
                        'Volkswagen Bora' => 1887,
                        'Volkswagen Golf' => 1901,
                        'Skoda Octavia' => 1909,
                        'Volkswagen Golf-Sportsvan' => 1910,
                        'Volkswagen Eos' => 1976,
                        'Volkswagen Jetta' => 2002,
                        'Audi Q2' => 2040,
                        'Audi A3' => 2051,
                        'Volkswagen Golf-Plus' => 2054,
                        'Audi S3' => 2125
                    ];

                    if (isset($models[$model])) {

                        $DMC = $models[$model];

                        $attributeValues = $this->getAttributeValue('74,76', $groupProcessId);

                        $id = NULL;
                        if (!empty($attributeValues)) {
                            $id = $attributeValues[0]['id'];
                        }

                        if(!empty($DMC)) {
                            $this->setAttributeValue(['74,76' => $id], $DMC, 'int', 'xxx', $groupProcessId, $userInfo['userId'], $userInfo['originalUserId']);
                        }

                    }

                }
            }
        }

    }

    protected function snoopyMatchToAttributeValue($matches, $path, $groupProcessId, $valueType, $stepId, $userId, $originalUserId, $dictionaryMatch = false)
    {
        if (count($matches) > 1) {
            $value = trim($matches[1]);
            if ($valueType == 'int' && !$dictionaryMatch) {
                $value = (int)str_replace(' ', '', $value);
            } else {
                $value = $this->mapCepikToDictionary($value);
            }

            if(!empty($value)) {
                $this->editAttributeValue($path, $groupProcessId, $value, $valueType, $stepId, $userId, $originalUserId, $dictionaryMatch);
            }
        }
        else {
            if($this->enableLogCepik) {
                $this->logCepikSearch('Nie znaleziono danych dla: '. $path . '. GPI: ' . $groupProcessId, implode(',', $matches));
            }
        }
    }

    /**
     * @param $path
     * @param $groupProcessId
     * @param $value
     * @param $valueType
     * @param $stepId
     * @param $userId
     * @param $originalUserId
     * @param bool $dictionaryMatch
     * @return array
     */
    public function editAttributeValue($path, $groupProcessId, $value, $valueType, $stepId, $userId, $originalUserId, $dictionaryMatch = false)
    {
        $attributeValues = $this->getAttributeValue($path, $groupProcessId);
        $id = NULL;
        $pathFind = $path;
        if (!empty($attributeValues)) {
            $id = $attributeValues[0]['id'];
            $pathFind = [$path => $id];
        }

        if ($path == '74,73' && !is_numeric($value)) {
            $value = $this->makeModelStandard($value);
        } elseif ($dictionaryMatch) {
            $value = $this->matchHammingValue($value, $dictionaryMatch);
        }

        return $this->setAttributeValue($pathFind, $value, $valueType, $stepId, $groupProcessId, $userId, $originalUserId);

    }

    protected function makeModelStandard($value)
    {
        $results = $this->queryManager->executeProcedure("select dbo.f_makeModelStandard('$value', 0)");
        if (count($results) > 0) {
            reset($results[0]);
            $first_key = key($results[0]);
            $value = $results[0][$first_key];
        }

        return $value;
    }

    protected function matchHammingValue($value, $functionName)
    {
        $dictionaryMatches = $this->queryManager->executeProcedure("select dbo.f_dictionaryHamming('$value','$functionName')");
        if (count($dictionaryMatches) > 0) {
            reset($dictionaryMatches[0]);
            $first_key = key($dictionaryMatches[0]);
            $value = $dictionaryMatches[0][$first_key];
        }

        return $value;
    }

    protected function mapCepikToDictionary($value)
    {
        $words = ['olej napędowy', 'energia elektryczna'];
        $replacements = ['diesel', 'EV'];

        return str_replace($words, $replacements, $value);
    }

    /**
     * @param $attributeValues
     * @param $value
     * @param $valueType
     * @param $stepId
     * @param $groupProcessInstanceId
     * @param null $userId
     * @param null $originalUserId
     * @param null $attributeSourceId
     * @param int $skipTransaction
     * @param int $skipHistoryCheck
     * @return array
     */
    public function setAttributeValue($attributeValues, $value, $valueType, $stepId, $groupProcessInstanceId, $userId = NULL, $originalUserId = NULL, $attributeSourceId = NULL, $skipTransaction = 0, $skipHistoryCheck = 0)
    {

        if ($valueType == AttributeValue::VALUE_MULTI) {
            $valueType = AttributeValue::VALUE_STRING;
        }

        if (!is_array($attributeValues)) {
            $attributeValues = [$attributeValues => NULL];
        }

        if($valueType == AttributeValue::VALUE_DECIMAL && $value !== "" && $value !== NULL){
            $value = number_format((float)str_replace(',', '.', $value), 2, '.', '');
        }
        else if ($valueType == AttributeValue::VALUE_INT && $value !== "" && $value !== NULL){
            $value = (int)floor($value);
        }


        $return = [];
        $level = 0;
        $pathMaxLevel = count($attributeValues) - 1;
        $parentId = NULL;

        foreach ($attributeValues as $path => $attributeValueId) {

            $this->staticCache->delAttrValue($path, $groupProcessInstanceId, $valueType);

            if ($valueType == AttributeValue::VALUE_DATE) {
                if (!empty($value)) {
                    if ($value instanceof \DateTime) {
                        $value = $value->format('Y-m-d H:i:s');
                    } else {
                        $value = date('Y-m-d H:i:s', strtotime($value));
                    }
                } else {
                    $value = NULL;
                }

            }

            if (is_array($value)) {
                $value = implode(",", $value);
            }

            if ($value === "" && ($valueType === AttributeValue::VALUE_DECIMAL || $valueType === AttributeValue::VALUE_INT)) {
                $value = NULL;
            }

            if ($level !== $pathMaxLevel && $attributeValueId) {
                $parentId = $attributeValueId;
                $output = [
                    'attributeValueId' => $attributeValueId,
                    'err' => 0,
                    'message' => ''
                ];
            } else {
                $parameters = [
                    ['key' => 'attributePath', 'value' => $path, 'type' => PDO::PARAM_STR],
                    [
                        'key' => 'groupProcessInstanceId',
                        'value' => (int)$groupProcessInstanceId,
                        'type' => PDO::PARAM_INT,
                        'length' => 100
                    ],
                    [
                        'key' => 'stepId',
                        'value' => $stepId,
                        'type' => PDO::PARAM_STR
                    ],
                    ['key' => 'attributeSourceId', 'value' => $attributeSourceId, 'type' => PDO::PARAM_INT],
                    [
                        'key' => 'parentAttributeValueId',
                        'value' => $parentId,
                        'type' => PDO::PARAM_INT
                    ],
                    ['key' => 'skipTransaction', 'value' => $skipTransaction, 'type' => PDO::PARAM_INT],
                    ['key' => 'skipHistoryCheck', 'value' => $skipHistoryCheck, 'type' => PDO::PARAM_INT],
                    ['key' => 'userId', 'value' => $userId, 'type' => PDO::PARAM_INT],
                    ['key' => 'originalUserId', 'value' => $originalUserId, 'type' => PDO::PARAM_INT],
                    [
                        'key' => 'attributeValueId',
                        'value' => (!empty($attributeValueId)) ? (int)$attributeValueId : NULL,
                        'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                        'length' => 100
                    ],
                    [
                        'key' => 'err',
                        'value' => 0,
                        'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                        'length' => 100
                    ],
                    [
                        'key' => 'message',
                        'value' => '',
                        'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                        'length' => 255,
                    ]
                ];

                $procedureStatement = "EXEC dbo.p_attribute_set2 @attributePath = :attributePath, @parentAttributeValueId = :parentAttributeValueId, " .
                    " @userId = :userId, @originalUserId = :originalUserId, @attributeSourceId = :attributeSourceId, @skipTransaction = :skipTransaction, " .
                    " @skipHistoryCheck = :skipHistoryCheck, @groupProcessInstanceId = :groupProcessInstanceId, @stepId = :stepId, @err = :err, @message = :message, " .
                    " @attributeValueId = :attributeValueId";

                if ($level == $pathMaxLevel && $valueType) {
                    $parameters[] = [
                        'key' => $valueType,
                        'value' => $value,
                        'type' => $this->queryManager->castTypeToPdoType($valueType)
                    ];
                    $procedureStatement = $procedureStatement . ", @" . $this->getValueTypeProperty($valueType) . ' = :' . $valueType;
                } else if (!$attributeValueId || ($level == $pathMaxLevel && !$valueType)) {
                    $procedureStatement = $procedureStatement . ", @isParent = 1";
                }

                $output = $this->queryManager->executeProcedure(
                    $procedureStatement,
                    $parameters
                );
            }

            $return[$path] = $output;
            ++$level;

        }

        return $return;

    }

    /**
     * Used as setAttributeValue but with nullable parentID
     * @param $attributeValues
     * @param $value
     * @param $valueType
     * @param $stepId
     * @param $parentId
     * @param $groupProcessInstanceId
     * @param null $userId
     * @param null $originalUserId
     * @param null $attributeSourceId
     * @param int $skipTransaction
     * @param int $skipHistoryCheck
     * @return array
     */
    public function editAttributeValueNew(
        $attributeValues,
        $value,
        $valueType,
        $stepId,
        $groupProcessInstanceId,
        $parentId = NULL,
        $userId = NULL,
        $originalUserId = NULL,
        $attributeSourceId = NULL,
        $skipTransaction = 0,
        $skipHistoryCheck = 0)
    {
        if ($valueType == AttributeValue::VALUE_MULTI) {
            $valueType = AttributeValue::VALUE_STRING;
        }

        if (!is_array($attributeValues)) {
            $attributeValues = [$attributeValues => NULL];
        }

        if($valueType == AttributeValue::VALUE_DECIMAL && $value !== "" && $value !== NULL){
            $value = number_format((float)str_replace(',', '.', $value), 2, '.', '');
        }
        else if ($valueType == AttributeValue::VALUE_INT && $value !== "" && $value !== NULL){
            $value = (int)floor($value);
        }


        $return = [];
        $level = 0;
        $pathMaxLevel = count($attributeValues) - 1;

        foreach ($attributeValues as $path => $attributeValueId) {

            $this->staticCache->delAttrValue($path, $groupProcessInstanceId, $valueType);

            if ($valueType == AttributeValue::VALUE_DATE) {
                if (!empty($value)) {
                    if ($value instanceof \DateTime) {
                        $value = $value->format('Y-m-d H:i:s');
                    } else {
                        $value = date('Y-m-d H:i:s', strtotime($value));
                    }
                } else {
                    $value = NULL;
                }

            }

            if (is_array($value)) {
                $value = implode(",", $value);
            }

            if ($value === "" && ($valueType === AttributeValue::VALUE_DECIMAL || $valueType === AttributeValue::VALUE_INT)) {
                $value = NULL;
            }

            if ($level !== $pathMaxLevel && $attributeValueId) {
                $parentId = $attributeValueId;
                $output = [
                    'attributeValueId' => $attributeValueId,
                    'err' => 0,
                    'message' => ''
                ];
            } else {
                $parameters = [
                    ['key' => 'attributePath', 'value' => $path, 'type' => PDO::PARAM_STR],
                    [
                        'key' => 'groupProcessInstanceId',
                        'value' => (int)$groupProcessInstanceId,
                        'type' => PDO::PARAM_INT,
                        'length' => 100
                    ],
                    [
                        'key' => 'stepId',
                        'value' => $stepId,
                        'type' => PDO::PARAM_STR
                    ],
                    ['key' => 'attributeSourceId', 'value' => $attributeSourceId, 'type' => PDO::PARAM_INT],
                    [
                        'key' => 'parentAttributeValueId',
                        'value' => $parentId,
                        'type' => PDO::PARAM_INT
                    ],
                    ['key' => 'skipTransaction', 'value' => $skipTransaction, 'type' => PDO::PARAM_INT],
                    ['key' => 'skipHistoryCheck', 'value' => $skipHistoryCheck, 'type' => PDO::PARAM_INT],
                    ['key' => 'userId', 'value' => $userId, 'type' => PDO::PARAM_INT],
                    ['key' => 'originalUserId', 'value' => $originalUserId, 'type' => PDO::PARAM_INT],
                    [
                        'key' => 'attributeValueId',
                        'value' => (!empty($attributeValueId)) ? (int)$attributeValueId : NULL,
                        'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                        'length' => 100
                    ],
                    [
                        'key' => 'err',
                        'value' => 0,
                        'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                        'length' => 100
                    ],
                    [
                        'key' => 'message',
                        'value' => '',
                        'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                        'length' => 255,
                    ]
                ];

                $procedureStatement = "EXEC dbo.p_attribute_edit 
                @attributePath = :attributePath,
                 @parentAttributeValueId = :parentAttributeValueId, " .
                    " @userId = :userId,
                     @originalUserId = :originalUserId,
                      @attributeSourceId = :attributeSourceId, 
                      @skipTransaction = :skipTransaction, " .
                    " @skipHistoryCheck = :skipHistoryCheck,
                     @groupProcessInstanceId = :groupProcessInstanceId,
                      @stepId = :stepId,
                       @err = :err,
                        @message = :message,
                         " .
                    " @attributeValueId = :attributeValueId";

                if ($level == $pathMaxLevel && $valueType) {
                    $parameters[] = [
                        'key' => $valueType,
                        'value' => $value,
                        'type' => $this->queryManager->castTypeToPdoType($valueType)
                    ];
                    $procedureStatement = $procedureStatement . ", @" . $this->getValueTypeProperty($valueType) . ' = :' . $valueType;
                } else if (!$attributeValueId || ($level == $pathMaxLevel && !$valueType)) {
                    $procedureStatement = $procedureStatement . ", @isParent = 1";
                }

                $output = $this->queryManager->executeProcedure(
                    $procedureStatement,
                    $parameters
                );
            }

            $return[$path] = $output;
            ++$level;

        }

        return $return;

    }


    public function sortAttributeValue($attributeValueId, $move)
    {
        $parameters = [
            [
                'key' => 'attributeValueId',
                'value' => $attributeValueId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'move',
                'value' => $move,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_attribute_sort @attributeValueId = :attributeValueId, @move = :move",
            $parameters
        );

        return $output;
    }


    public function getActiveServiceById($rootId, $serviceId, $allSteps = 1)
    {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceId',
                'value' => $serviceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'onlyWithStatus',
                'value' => 1,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'allSteps',
                'value' => $allSteps,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_get_active_service_by_id @rootId = :rootId, @serviceId = :serviceId, @allSteps = :allSteps, @onlyWithStatus = :onlyWithStatus",
            $parameters
        );

        foreach ($output as $key => $activeService) {
            // pierwszy warunek na wypadek, jakby kroku danej usługi były w tym samym procesie co "odcinek" nowej sprawy
            if($activeService['group_process_id'] == $activeService['root_id']) {
                unset($output[$key]);
            }
        }

        return $output;
    }

    public function deleteAttributeValue($attributeValueId, $userId = NULL, $originalUserId = NULL)
    {
        $parameters = [
            [
                'key' => 'attributeValueId',
                'value' => $attributeValueId,
                'type' => PDO::PARAM_INT
            ],
            ['key' => 'userId', 'value' => $userId, 'type' => PDO::PARAM_INT],
            ['key' => 'originalUserId', 'value' => $originalUserId, 'type' => PDO::PARAM_INT]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_attribute_delete @attributeValueId = :attributeValueId, @userId = :userId, @originalUserId = :originalUserId",
            $parameters, false
        );

        return $output;
    }

    public function syncUserGroups($userId) {

        $parameters = [
            [
                'key' => 'userId',
                'value' => intval($userId),
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = "EXEC dbo.p_sync_user_groups @userId = :userId";

        $this->queryManager->executeProcedure($query, $parameters, false);

    }

    public function serviceActionRun($stepId, $instanceId, $actionId, $userId)
    {

        $parameters = [
            [
                'key' => 'stepId',
                'value' => $stepId,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
            [
                'key' => 'processInstanceId',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'callback',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
            [
                'key' => 'instanceId',
                'value' => (int)$instanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => (int)$userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'actionId',
                'value' => (int)$actionId,
                'type' => PDO::PARAM_INT
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_service_action_run @stepId = :stepId, @instanceId = :instanceId, @actionId = :actionId, @err = :err, @message = :message, @userId = :userId, @processInstanceId = :processInstanceId, @callback = :callback",
            $parameters
        );

        return $output;
    }

    public function choicedVariant($processInstanceId) {
        return $this->em->getRepository(ProcessInstance::class)->getChoicedVariant($processInstanceId);
    }

    public function getStepsForFilterGroup($groupId){
        $query = "select pdt.name processName, st.name stepName, st.translatable_id stepId FROM step_translation st
                  inner join step s ON st.translatable_id = s.id
                  inner join process_definition_translation pdt on LEFT(s.id,4) = pdt.translatable_id and pdt.locale = 'pl'
                  inner join step_permissions sp on sp.step_id = s.id
                  inner join process_import pin on pin.name = 'diagnosis' and active = 1 
                  where $groupId IN (sp.user_group_id) 
                  and (pdt.translatable_id <> pin.process_definition_id OR s.id = CAST(pin.process_definition_id AS NVARCHAR(10))+'.0002') and pdt.locale = 'pl' and st.locale = 'pl'
                  ";

        return $this->queryManager->executeProcedure($query);
    }

    /**
     * @param $processInstanceId
     * @param string $locale
     * @param int $showAll
     * @return array
     */
    public function getStatusServices($processInstanceId, $locale = 'pl', $showAll = 0)
    {

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'showAll',
                'value' => $showAll,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR,
                'length' => 10,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            'EXEC [dbo].[p_get_status_active_services] @processInstanceId = :processInstanceId, @locale = :locale, @showAll = :showAll',
            $parameters
        );

        return $output;
    }

    /**
     * @param $processInstanceId
     * @param string $locale
     * @param int $showAll
     * @return array
     */
    public function getStatusServicesViewer($processInstanceId, $locale = 'pl', $showAll = 0)
    {

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'showAll',
                'value' => $showAll,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR,
                'length' => 10,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            'EXEC [dbo].[p_get_status_active_services_viewer] @processInstanceId = :processInstanceId, @locale = :locale, @showAll = :showAll',
            $parameters
        );

        return $output;
    }

    public function canReopenCase($rootId){
        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'check',
                'value' => 1,
                'type' => PDO::PARAM_INT
            ],
        ];

        $result = $this->queryManager->executeProcedure("exec p_reopen_case @rootId = :rootId, @check = :check", $parameters);
        return count($result) == 0 ? true : false;

    }

    public function taskReport($groupPlatformId, $groupId = null, $column = null, $count = 1){

        $parameters = [
            [
                'key' => 'group_platform_id',
                'value' => $groupPlatformId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'group_id',
                'value' => $groupId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'column',
                'value' => $column,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'count',
                'value' => $count,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = "EXEC dbo.p_task_report
                    @group_platform_id = :group_platform_id,
                    @group_id = :group_id,
                    @column = :column,
                    @count = :count";

        return $this->queryManager->executeProcedure($query, $parameters);

    }

    public function reopenCase($rootId){
        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ],
        ];

        $this->queryManager->executeProcedure("exec p_reopen_case @rootId = :rootId", $parameters, false);

    }


    public function taskNameOfProcessInstance($processInstanceId){

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => intval($processInstanceId),
                'type' => PDO::PARAM_INT
            ],
        ];

        $query = 'SELECT 
            sp.step_id stepId,
            ug.name groupName,
            ug.id groupId
            FROM dbo.process_instance pi with(nolock)
            INNER JOIN dbo.step_permissions sp with(nolock) on sp.step_id = pi.step_id
            INNER JOIN dbo.user_group ug with(nolock) on ug.id = sp.user_group_id
            WHERE pi.id = :processInstanceId AND ug.type = 1';

        $output = $this->queryManager->executeProcedure($query, $parameters);

        if(!empty($output)) {
            return $output[0];
        }

        return null;

    }

    public function getEditorSteps(ServiceDefinition $serviceDefinition, ProcessInstance $groupProcess){
        $query = 'select id, step_id, name, description from 
                    (
                        select se.id editorId, pin.id, pin.step_id, sett.name, sett.description, 
                        (row_number() over (partition by pin.step_id order by pin.id desc)) rowN
                        from dbo.process_instance pin
                        inner join dbo.step s on s.id = pin.step_id
                        inner join dbo.service_editor se on se.step_id = pin.step_id
                        inner join dbo.service_editor_translation sett on sett.translatable_id = se.id
                        where se.service_definition_id = :serviceDefinitionId
                        and pin.group_process_id = :groupProcessId
                    ) a 
                    where a.rowN = 1
                    order by a.editorId';

        $parameters = [
            [
                'key' => 'serviceDefinitionId',
                'value' => $serviceDefinition->getId(),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'groupProcessId',
                'value' => $groupProcess->getId(),
                'type' => PDO::PARAM_INT
            ],
        ];

        return $this->queryManager->executeProcedure($query, $parameters);

    }

    public function getCaseId($instanceId){
        $parameters = [
            [
                'key' => 'instanceId',
                'value' => intval($instanceId),
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "SELECT dbo.f_caseId(:instanceId)",
            $parameters
        );

        if(count($result) > 0){
            return $result[0][''];
        }

        return '';
    }

    /**
     * Get root id by instance ID
     * @param $instanceId
     * @return string
     */
    public function getRootId($instanceId)
    {
        $parameters = [
            [
                'key' => 'instanceId',
                'value'=>intval($instanceId),
                'type'=> PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "SELECT top 1 root_id from dbo.process_instance with (nolock ) where id = :instanceId",
            $parameters
        );

        if(count($result) > 0){
            return $result[0]['root_id'];
        }

        return false;
    }

    public function getPartnerContact($partnerId, $serviceTypeId, $contactType, $groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'partnerId',
                'value' => intval($partnerId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => intval($groupProcessInstanceId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceTypeId',
                'value' => $serviceTypeId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'contactType',
                'value' => $contactType,
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "SELECT dbo.f_partner_contact(:groupProcessInstanceId, :partnerId, :serviceTypeId, :contactType)",
            $parameters
        );

        if(count($result) > 0){
            return $result[0][''] ?: '';
        }
        return '';
    }

    public function getGopEmail($groupProcessId, $processDefinition, $crossBorder){
        $definitionId = $processDefinition->getId();


        if($definitionId == self::PROCESS_ID_REPLACEMENT_CAR){
            $useLocal = $this->getAttributeValue('206',$groupProcessId,'int') ?? false;

            if($crossBorder && !$useLocal){

                $partnerId = $this->getAttributeValue('837,773,704', $groupProcessId, 'int');
            }else {
                $partnerId = $this->getAttributeValue('764,742', $groupProcessId, 'int');
            }
        }
        else{
            $partnerId = $this->getAttributeValue('610', $groupProcessId, 'int');;
            if($crossBorder){
                $partnerId = $this->getAttributeValue('741', $groupProcessId, 'int');
            }
        }

        if($partnerId){
            return $this->getPartnerContact($partnerId, self::SERVICE_TYPE_REPLACEMENT_CAR, self::CONTACT_TYPE_EMAIL, $groupProcessId);
        }

        return '';
    }

    public function sendGop($groupProcessInstanceId, $final = 0, $email = null, $preview = false){

        $userName = $this->userInfo->getUser()->getName();

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => intval($groupProcessInstanceId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'final',
                'value' => intval($final),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'email',
                'value' => $email,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ],
            [
                'key' => 'preview',
                'value' => intval($preview),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userName',
                'value' => $userName,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_send_gop @groupProcessInstanceId = :groupProcessInstanceId, @final = :final, @email = :email, @preview = :preview, @username = :userName",
            $parameters,
            $preview ? true : false

        );

        if(count($result) > 0){
            return $result[0][''];
        }

        return '';
    }

    public function changeServiceStatus($processInstanceId, $serviceId, $statusId)
    {

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceId',
                'value' => intval($serviceId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'statusId',
                'value' => intval($statusId),
                'type' => PDO::PARAM_INT
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC [dbo].[p_change_service_status] @processInstanceId = :processInstanceId, @statusId = :statusId, @serviceId = :serviceId",
            $parameters,
            false
        );

        return $output;
    }

    public function isSameTask($prevProcessInstanceId, $nextProcessInstanceId){

        $parameters = [
            [
                'key' => 'instanceId',
                'value' => intval($prevProcessInstanceId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'nextInstanceId',
                'value' => intval($nextProcessInstanceId),
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'SELECT dbo.f_is_same_task(:instanceId, :nextInstanceId) result';

        $output = $this->queryManager->executeProcedure($query, $parameters);

        if(!empty($output)) {
            return filter_var($output[0]['result'], FILTER_VALIDATE_BOOLEAN);
        }

        return false;

    }

    public function getServiceIdFromGroupId($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => intval($groupProcessInstanceId),
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "SELECT dbo.f_group_service_id(:groupProcessInstanceId)",
            $parameters
        );

        if(count($result) > 0){
            return $result[0][''];
        }

        return null;
    }

    public function serviceTopProgressGroup($groupProcessInstanceId, $serviceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => intval($groupProcessInstanceId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceId',
                'value' => intval($serviceId),
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "SELECT dbo.f_service_top_progress_group(:groupProcessInstanceId, :serviceId)",
            $parameters
        );

        if(count($result) > 0){
            return $result[0][''];
        }

        return null;
    }

    public function getBusinessConfig($key, $groupProcessInstanceId, $platformId = null, $programId = null, $returnDescription = false){

        $parameters = [
            [
                'key' => 'businessKey',
                'value' => $key,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => intval($groupProcessInstanceId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'platformId',
                'value' => $platformId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'programId',
                'value' => $programId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'value',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255
            ],
            [
                'key' => 'description',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255
            ]
        ];


        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_get_business_config @key = :businessKey, @groupProcessInstanceId = :groupProcessInstanceId, @platformId = :platformId, @programId = :programId, @description = :description, @value = :value",
            $parameters
        );

        if(!$returnDescription){
            return $result['value'];
        }

        return $result;

    }

    public function attributeStepDefinitions($id, $locale = 'pl'){

        $parameters = [
            [
                'key' => 'id',
                'value' => $id,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ],


        ];

        $sql = 'select distinct sft.step_id id, st.name name 
        from dbo.form_control fc 
        inner join dbo.form_template ft on fc.form_template = ft.id
        inner join dbo.step_form_template sft on sft.form_template_id = ft.id
        inner join dbo.step_translation st on st.translatable_id = sft.step_id
        where fc.attribute = :id and st.locale = :locale';

        $result = $this->queryManager->executeProcedure(
            $sql, $parameters
        );

        return $result;

    }

    public function getCaseSpecialInstruction($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'category',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
            [
                'key' => 'text',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_case_special_instruction @groupProcessInstanceId = :groupProcessInstanceId, @category = :category, @text = :text",
            $parameters
        );

        if(count($result) > 0){
            return $result;
        }

        return null;
    }


    /**
     * @param $groupProcessInstanceId
     * @return array|null
     */
    public function getExtraTextForTop5($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $rows = $this->queryManager->executeProcedure(
            "EXEC dbo.p_extra_content_for_top5 @groupProcessInstanceId = :groupProcessInstanceId",
            $parameters
        );

        return $rows;

    }

    public function sumAttributeValue($groupProcessInstanceId, $attributePath){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'attributePath',
                'value' => $attributePath,
                'type' => PDO::PARAM_STR,
                'length' => 255,
            ],
            [
                'key' => 'result',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_sum_attribute_value @groupProcessInstanceId = :groupProcessInstanceId, @attributePath = :attributePath, @result = :result",
            $parameters
        );

        if(count($result) > 0){
            return $result['result'];
        }

        return null;



    }

    /**
     * @param $code
     * @return array
     */
    public function getDiagnosisFromCode($code) {

        $parameters = [
            [
                'key' => 'arccode',
                'value' => $code,
                'type' => PDO::PARAM_STR
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_diagnosis_from_code @ARCCODE = :arccode",
            $parameters
        );

        if(count($result) > 0){
            return $result;
        }

        return [];

    }

    /**
     * @param $attributePath
     * @param $value
     * @param bool $translateBack
     * @return array
     */
    public function translateAttributeValue($attributePath, $value, $translateBack = false) {

        $parameters = [
            [
                'key' => 'attributePath',
                'value' => str_replace('*','',$attributePath),
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'value',
                'value' => $value,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'translated',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_translate_attribute_value @attributePath = :attributePath, @value = :value, @translated = :translated",
            $parameters
        );

        if(count($result) > 0){
            return $result['translated'];
        }

        return $value;

    }

    /**
     * @param $instanceId
     * @param int $isParagraph
     * @return array
     */
    public function stepDescription($instanceId, $isParagraph = 1) {

        $parameters = [
            [
                'key' => 'instanceId',
                'value' => $instanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'isParagraph',
                'value' => $isParagraph,
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "EXEC dbo.p_step_description @instanceId = :instanceId, @isParagraph = :isParagraph",
            $parameters
        );

        if(count($result) > 0){
            return $result[0][''];
        }

        return '';
    }

    /**
     * @param String $platformIds
     * @param array $filters
     * @param bool $onlyHeaders
     * @param null $companyId
     * @return array
     */
    public function findCFMCases(String $platformIds, $filters = [], $onlyHeaders = false) {

        $parameters = [
            [
                'key' => 'platformsId',
                'value' => $platformIds,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'returnOnlyHeaders',
                'value' => ($onlyHeaders) ? 1 : 0,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'rootId',
                'value' => (!empty($filters['case_number'])) ? self::parseMaxInt(CaseHandler::parseCaseNumber($filters['case_number'])) : NULL,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'program',
                'value' => (!empty($filters['program'])) ? implode(',', $filters['program']): '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'eventType',
                'value' => (!empty($filters['event_type'])) ? implode(',', $filters['event_type']): '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'subEventType',
                'value' => (!empty($filters['sub_event_type'])) ? implode(',', $filters['sub_event_type']): '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'dateFrom',
                'value' => (!empty($filters['date_from'])) ? $filters['date_from']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'dateTo',
                'value' => (!empty($filters['date_to'])) ? $filters['date_to']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'carRegisterNumber',
                'value' => (!empty($filters['car_registration_number'])) ? $filters['car_registration_number']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'driver',
                'value' => (!empty($filters['driver'])) ? $filters['driver']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'coordinator',
                'value' => (!empty($filters['coordinator'])) ? intval($filters['coordinator']) : NULL,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'endDateRent',
                'value' => (!empty($filters['end_date_rent'])) ? $filters['end_date_rent'] : NULL,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'status',
                'value' => (!empty($filters['status'])) ? $filters['status'] : NULL,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'limit',
                'value' => (!empty($filters['limit'])) ? $filters['limit'] : 200,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'location',
                'value' => (!empty($filters['location'])) ? $filters['location']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'vin',
                'value' => (!empty($filters['vin'])) ? $filters['vin']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'pzuCaseNumber',
                'value' => (!empty($filters['pzu_case_number'])) ? $filters['pzu_case_number']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'caseCategories',
                'value' => (!empty($filters['case_category'])) ? implode(',', $filters['case_category']): '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'country',
                'value' => (!empty($filters['country'])) ? $filters['country']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'carRentalStatus',
                'value' => (!empty($filters['car_rental_status'])) ? $filters['car_rental_status']: '',
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'towingOrFixing',
                'value' => (!empty($filters['basic_service'])) ? $filters['basic_service']: null,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'makeModel',
                'value' => (!empty($filters['make_model'])) ? $filters['make_model']: '',
                'type' => PDO::PARAM_STR
            ]
        ];

        return $this->queryManager->executeProcedure(
            'EXEC [dbo].[p_find_cfm_cases] 
             @platformIds = :platformsId, 
             @rootId = :rootId,
             @returnOnlyHeaders = :returnOnlyHeaders, 
             @programIds = :program,
             @dateFrom = :dateFrom, 
             @dateTo = :dateTo,
             @eventType = :eventType,
             @subEventType = :subEventType,
             @carRegisterNumber = :carRegisterNumber,
             @driver = :driver,
             @status = :status,
             @coordinator = :coordinator,
             @endDateRent = :endDateRent,
             @limit = :limit,
             @location = :location,
             @vin = :vin,
             @caseCategories = :caseCategories,
             @country = :country,
             @carRentalStatus = :carRentalStatus,
             @pzuCaseNumber = :pzuCaseNumber,
             @towingOrFixing = :towingOrFixing,
             @makeModel = :makeModel',
            $parameters
        );

    }

    /**
     * @param array $platformsId
     * @return array
     */
    public function getCoordinatorsByPlatforms($platformsId = []) {

        $parameters = [
            [
                'key' => 'platformsId',
                'value' => implode(',', $platformsId),
                'type' => PDO::PARAM_STR
            ]
        ];

        return $this->queryManager->executeProcedure(
            "SELECT id, name, username FROM dbo.f_getCoordinatorsByPlatforms(:platformsId)",
            $parameters
        );

    }

    /**
     * @param $rootId
     * @param $noteId
     * @param $options
     * @param null $userId
     * @return string|null
     */
    public function createTaskFromNote($rootId, $noteId, $options = [], $userId = null) {

        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'isComplaint' => 0,
            "category" => null,
            "priority" => 1
        ]);

        $data = $resolver->resolve($options);

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'noteId',
                'value' => $noteId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'currentUserId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'isComplaint',
                'value' => $data['isComplaint'],
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'category',
                'value' => $data['category'],
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'priority',
                'value' => $data['priority'],
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC [dbo].[p_create_task_from_note] @rootId = :rootId, @noteId = :noteId, @currentUserId = :currentUserId, @isComplaint = :isComplaint, @category = :category, @priority = :priority",
            $parameters
        );

        if(!empty($output)) {
            return $output[0]['processInstanceId'];
        }

        return null;
    }

    /**
     * Zwraca platformy do których użytkownik ma upraweniania - tabelka: company_platform_permissions
     *
     * @param int $userId
     * @return array
     */
    public function getPlatformsForUser(int $userId) {

        $parameters = [
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT [dbo].[f_get_platformsForUser](:userId) as platformsId",
            $parameters
        );

        if(!empty($output) && !empty($output[0]['platformsId'])) {
            return explode(',', $output[0]['platformsId']);
        }

        return [];

    }

    /**
     *
     * @param User $user
     * @return array
     */
    public function getCompanyPlatformPermissions(User $user) {

        $default = [[], ''];

        if(!$user->getCompany()) {
            return $default;
        }

        $parameters = [
            [
                'key' => 'companyId',
                'value' => $user->getCompany()->getId(),
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            'SELECT platforms, company_key FROM dbo.company_platform_permissions perm with(nolock) where perm.companyId = :companyId',
            $parameters
        );

        if(!empty($output)) {

            $platformsId = explode(',', $output[0]['platforms']);

            return [
                $platformsId,
                $output[0]['company_key']
            ];

        }

        return $default;

    }

    public function getRootPlatform($rootId) {

        $platformId = $this->getAttributeValue('253', $rootId, AttributeValue::VALUE_INT);

        if(empty($platformId)) return null;

        return $this->em->getRepository('CaseBundle:Platform')->find($platformId);

    }

    /**
     * @param $fileUniqueId
     * @param $userId
     * @return mixed
     */
    public function checkPermissionForFile($fileUniqueId, $userId)  {

        $parameters = [
            [
                'key' => 'fileUniqueId',
                'value' => $fileUniqueId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            "SELECT [dbo].[f_checkPermissionForFile](:fileUniqueId, :userId) as permission",
            $parameters
        );

        if(count($result) && isset($result[0]['permission'])) {
            return  filter_var($result[0]['permission'], FILTER_VALIDATE_BOOLEAN);
        }

    }


    /**
     * @param $stepId
     * @param $platformId
     * @param int $onlyOnline
     * @return mixed
     */
    public function getUsersWithAccessToStep($stepId, $platformId, $onlyOnline = 0)  {

        $parameters = [
            [
                'key' => 'stepId',
                'value' => $stepId,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'platformId',
                'value' => $platformId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'onlyOnline',
                'value' => $onlyOnline,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            "EXEC [dbo].[p_getUsersWithAccessToStep] @stepId = :stepId, @platformId = :platformId, @onlyOnline = :onlyOnline",
            $parameters
        );

    }


    /**
     * @param $groupProcessInstanceId
     * @param bool $returnName
     * @return mixed
     */
    public function carType($groupProcessInstanceId, $returnName = false)  {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'returnName',
                'value' => $returnName,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'carType',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ]

        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC [dbo].[p_carType] @groupProcessInstanceId = :groupProcessInstanceId, @returnName = :returnName, @r = :carType",
            $parameters
        );

        if(!empty($output)) {
            return $output['carType'];
        }

    }

    /**
     * @param $caseNumber
     * @param int $simpleResult
     * @return array
     */
    public function getDocumentsInVDesk($caseNumber, $simpleResult = 1) {

        $parameters = [
            [
                'key' => 'caseNumber',
                'value' => $caseNumber,
                'type' => PDO::PARAM_STR,
                'length' => 50
            ],
            [
                'key' => 'simpleResult',
                'value' => $simpleResult,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            "EXEC [dbo].[f_showDocumentsInVdesk] @caseNumber = :caseNumber, @simpleResult = :simpleResult",
            $parameters
        );

    }

    /**
     * @param $groupProcessInstanceId
     * @param string $column
     * @return int|string
     */
    public function isHelp($groupProcessInstanceId, $column = '') {

        if(empty($column)) return '';

        $helpId = $this->getAttributeValue('1067', $groupProcessInstanceId, AttributeValue::VALUE_INT);

        if(empty($helpId)) {
            $isVip = $this->getAttributeValue('1004', $groupProcessInstanceId, AttributeValue::VALUE_STRING);
            if($isVip){
                $vipHelp = $this->em->getRepository(HelpCase::class)->findByName('Vip');
                if(!empty($vipHelp)) {
                    $helpId = $vipHelp[0]->getId();
                }
            }
            else {
                return 0;
            }
        }

        $parameters = [
            [
                'key' =>'id',
                'value'=> $helpId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure('SELECT * FROM dbo.help_case WHERE id = :id',
            $parameters
        );

        if(!empty($output) && isset($output[0][$column])) {
            if($output[0][$column] == 1) {
                return 1;
            }
        }

        return 0;

    }

    /**
     * @param $groupProcessInstanceId
     * @return boolean
     */
    public function canExtendRental($groupProcessInstanceId) {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'canExtend',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC [dbo].[p_viewer_can_extend_rental] @groupProcessInstanceId = :groupProcessInstanceId, @canExtend = :canExtend",
            $parameters
        );

        if(!empty($output) && isset($output['canExtend'])) {

            return filter_var($output['canExtend'], FILTER_VALIDATE_BOOLEAN);

        }

        return false;

    }

    /**
     * @param $groupProcessInstanceId
     * @return boolean
     */
    public function afterExtendRental($groupProcessInstanceId) {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            "EXEC [dbo].[p_viewer_after_extend_rental] @groupProcessInstanceId = :groupProcessInstanceId",
            $parameters,
            false
        );

    }

    /**
     * @param $groupProcessInstanceId
     * @return int
     */
    public function totalCostOfService($groupProcessInstanceId) {

        $parameters = [
            [
                'key' =>'groupProcessInstanceId',
                'value'=> $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure('EXEC dbo.p_serviceTotalCost @groupId = :groupProcessInstanceId',
            $parameters
        );

        if(!empty($output)) {

            return $output[0]['Price'];
        }

        return null;

    }

    /**
     * @param $processInstanceId
     * @param null $userId
     * @return array
     */
    public function getAvailableExtraActions($processInstanceId, $userId = null) {

        $parameters = [
            [
                'key' =>'processInstanceId',
                'value'=> $processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value'=> $userId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure('EXEC dbo.P_availableExtraActions @processInstanceId = :processInstanceId, @userId = :userId',
            $parameters
        );

        $actions = [];

        if(!empty($output)) {

            foreach ($output as $action) {
                $actions[$action['action_name']] = filter_var($action['enabled'], FILTER_VALIDATE_BOOLEAN);
            }

        }

        return $actions;

    }

    /**
     * @param $callNumber
     * @param $platformNumber
     * @return boolean
     */
    public function createCaseFromMobile($callNumber, $platformNumber) {

        $parameters = [
            [
                'key' => 'callNumber',
                'value' => $callNumber,
                'type' => PDO::PARAM_STR,
                'length' => 50
            ],
            [
                'key' => 'platformCallNumber',
                'value' => $platformNumber,
                'type' => PDO::PARAM_STR,
                'length' => 50
            ],
            [
                'key' => 'token',
                'value' => "",
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 50
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC [dbo].[P_createNewCaseFromMobile] @callNumber = :callNumber, @platformCallNumber = :platformCallNumber, @token = :token",
            $parameters
        );

        if(!empty($output) && isset($output['token'])) {

            return $output['token'];

        }

        return "";

    }

    /**
     * @param $processInstanceId
     */
    public function updateDateEnterIfEmpty($processInstanceId) {

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => intval($processInstanceId),
                'type' => PDO::PARAM_INT
            ]
        ];

        $this->queryManager->executeProcedure(
            "EXEC dbo.P_update_date_enter @processInstanceId = :processInstanceId",
            $parameters,
            false
        );

    }

    /**
     * @param $platformId
     * @param $rootId
     * @return array
     */
    public function getContactsForChatNote($platformId, $rootId = null) {

        $parameters = [
            [
                'key' => 'platformId',
                'value' => intval($platformId),
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'rootId',
                'value' => intval($rootId),
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            'EXEC dbo.P_get_contacts_for_chat_note @platformId = :platformId, @rootId = :rootId',
            $parameters
        );

    }

    /**
     * @param $name
     * @param $rootId
     * @param $content
     * @param null $userId
     */
    public function simpleAddCaseLog($name, $rootId, $content, $userId = null) {

        $parameters = [
            [
                'key' => 'name',
                'value' => $name,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'createdById',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'outputContent',
                'value' => $content,
                'type' => PDO::PARAM_STR
            ]
        ];

        $this->queryManager->executeProcedure(
            "EXEC  [dbo].[p_add_case_log] @name = :name,
	            @createdById = :createdById,
                @rootId = :rootId,
                @outputContent = :outputContent",
            $parameters,
            false
        );

    }

    /**
     * Sprawdza, czy istnieje macierz
     *
     * @param $rootId
     * @return bool|mixed
     */
    public function matrixExists($rootId) {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $result = $this->queryManager->executeProcedure(
            'SELECT [dbo].[f_matrixExists](:rootId) as isMatrix',
            $parameters
        );

        if(!empty($result) && isset($result[0]['isMatrix'])) {
            return filter_var($result[0]['isMatrix'], FILTER_VALIDATE_BOOLEAN);
        }

        return false;

    }


    /**
     * @param $processInstanceId
     * @param $userId
     * @return mixed
     */
    public function getControlsFormExtraForm($processInstanceId, $userId)  {

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            'EXEC [dbo].[P_getControlsForExtraForm] @processInstanceId = :processInstanceId, @userId = :userId',
            $parameters
        );

    }


}