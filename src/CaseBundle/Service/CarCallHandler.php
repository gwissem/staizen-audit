<?php

namespace CaseBundle\Service;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\StepAttributeDefinition;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use MediaMonks\MssqlBundle\PDO\PDO;
use SoapBundle\Model\Caller;
use SoapBundle\Model\Event;
use SoapBundle\Model\Location;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use ToolBundle\Utils\ToolQueryManager;
use UserBundle\Service\UserInfoService;

class CarCallHandler extends ProcessHandler
{

    const ATTRIBUTE_CALLER_ID = "357,197";
    const STEP_START_ID = '1023.001';
    const STEP_MANAGE_ID = '1023.002';
    const STEP_CALL_START_ID = '1012.001';

    public function handleEcallSoapRequest($callerId, $parameters = [])
    {
        $rootProcessId = $this->fetchRootProcess($callerId);
        $currentProcessInstances = $this->fetchCurrentProcessInstances($rootProcessId);

        $this->mapSoapToAttributeValues($parameters, $rootProcessId);

        return $currentProcessInstances;
    }

    private function fetchRootProcess($callerId)
    {
        $rootProcesses = $this->getRootForCallerId($callerId);

        if (!empty($rootProcesses)) {
            $rootProcessId = $rootProcesses[0]['id'];
        } else {
            $rootProcessId = $this->startNewCase();
            $this->setAttributeValue('357,197', $callerId, AttributeValue::VALUE_STRING, self::STEP_START_ID, $rootProcessId, 1, 1);
            $this->next($rootProcessId);
        }

        return $rootProcessId;
    }

    private function getRootForCallerId($callerId)
    {
        $tCallerId = ltrim($callerId, '0+');
        $roots = $this->getRootForAttributeValue(self::ATTRIBUTE_CALLER_ID, '0' . $tCallerId, AttributeValue::VALUE_STRING);
        if (empty($roots)) {
            $roots = $this->getRootForAttributeValue(self::ATTRIBUTE_CALLER_ID, '000' . $tCallerId, AttributeValue::VALUE_STRING);
        }
        // for number without leading zeroes
        if (empty($roots)) {
            $roots = $this->getRootForAttributeValue(self::ATTRIBUTE_CALLER_ID, $tCallerId, AttributeValue::VALUE_STRING);
        }
        // for number with '+'
        if (empty($roots)) {
            $roots = $this->getRootForAttributeValue(self::ATTRIBUTE_CALLER_ID, '+' . $tCallerId, AttributeValue::VALUE_STRING);
        }
        // unusual number
        if (empty($roots)) {
            $roots = $this->getRootForAttributeValue(self::ATTRIBUTE_CALLER_ID, $callerId, AttributeValue::VALUE_STRING);
        }

        return $roots;
    }

    public function startNewCase()
    {
        return $this->start(self::STEP_START_ID);
    }

    private function fetchCurrentProcessInstances($rootProcessId)
    {
        $processInstanceRepository = $this->em->getRepository(ProcessInstance::class);
        return $processInstanceRepository->findActiveProcessInstancesForRoot($rootProcessId);
    }

    public function mapSoapToAttributeValues($parameters, $rootProcessId)
    {
        /** @var Event $event */
        $event = $parameters->getEvent();
        $caller = $event->getCaller();
        $correlation = $event->getCorrelation();
        $vehicle = $event->getVehicle();
        $diagnosticInfo = $vehicle->getDiagnosticInfo();
        $vehicleStatus = $vehicle->getStatus();
        $location = $event->getLocation();
        $position = $location->getPosition();
        $traces = $location->getPositionTraces();

        $this->mapObjectToAttributeValues($event, $rootProcessId, self::STEP_MANAGE_ID);
        $this->mapObjectToAttributeValues($correlation, $rootProcessId, self::STEP_MANAGE_ID);
        $this->mapObjectToAttributeValues($caller, $rootProcessId, self::STEP_MANAGE_ID);
        $this->mapObjectToAttributeValues($vehicle, $rootProcessId, self::STEP_MANAGE_ID);
        $this->mapObjectToAttributeValues($diagnosticInfo, $rootProcessId, self::STEP_MANAGE_ID);
        $this->mapObjectToAttributeValues($vehicleStatus, $rootProcessId, self::STEP_MANAGE_ID);
        $this->mapObjectToAttributeValues($location, $rootProcessId, self::STEP_MANAGE_ID, 1);

    }

    public function mapObjectToAttributeValues($object, $groupProcessInstanceId, $stepId = self::STEP_MANAGE_ID, $skipHistoryCheck = 0)
    {
        if ($object) {
            if ($object instanceof Location) {
                foreach ($object->mapPropertiesToAttributes() as $location) {
                    $parents = $this->setAttributeValue([Location::PATH_LOCATION => NULL], NULL, NULL, $stepId, $groupProcessInstanceId);
                    foreach ($location as $path => $value) {
                        $this->setAttributeValue([Location::PATH_LOCATION => $parents[Location::PATH_LOCATION]['attributeValueId'], $path => NULL], $value['value'], $value['type'], $stepId, $groupProcessInstanceId, NULL, NULL, NULL, 0, $skipHistoryCheck);
                    }
                }
            } else {
                foreach ($object->mapPropertiesToAttributes() as $path => $value) {
                    $this->setAttributeValue($path, $value['value'], $value['type'], $stepId, $groupProcessInstanceId, NULL, NULL, NULL, 0, $skipHistoryCheck);
                }
            }
        }
    }

    public function handleEcallPhoneRequest($callerId, $userInfo, $platformId)
    {
        $rootProcessId = $this->fetchRootProcess($callerId);
        $currentProcessInstances = $this->fetchCurrentProcessInstances($rootProcessId);
        $this->setAttributeValue(self::ATTRIBUTE_CALLER_ID, $callerId, AttributeValue::VALUE_STRING, self::STEP_MANAGE_ID, $rootProcessId, $userInfo['userId'], $userInfo['originalUserId']);

        $callRingingProcess = $this->start(self::STEP_CALL_START_ID);
        $this->setAttributeValue('253', $platformId, AttributeValue::VALUE_INT, self::STEP_CALL_START_ID, $callRingingProcess, $userInfo['userId'], $userInfo['originalUserId']);
        $this->setAttributeValue('342,197', $callerId, AttributeValue::VALUE_STRING, self::STEP_CALL_START_ID, $callRingingProcess, $userInfo['userId'], $userInfo['originalUserId']);

        $this->changeProcessAncestors($callRingingProcess, $rootProcessId, $rootProcessId, $userInfo['userId'], $userInfo['originalUserId']);
        $this->next($callRingingProcess, 1, $userInfo['userId'], $userInfo['originalUserId']);

        return $currentProcessInstances;
    }

    public function getLocalEmergencyUnit($district, $county, $province)
    {
        $parameters = [
            ['key' => 'district', 'value' => $district, 'type' => PDO::PARAM_STR],
            ['key' => 'county', 'value' => $county, 'type' => PDO::PARAM_STR],
            ['key' => 'province', 'value' => $province, 'type' => PDO::PARAM_STR]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC aus.P_emergency @district = :district, @county = :county, @province = :province", $parameters
        );

        return $output;
    }

}