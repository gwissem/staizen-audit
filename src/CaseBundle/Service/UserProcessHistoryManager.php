<?php

namespace CaseBundle\Service;


use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\UserProcessHistory;
use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;
use UserBundle\Service\UserInfoService;

class UserProcessHistoryManager
{
    private $em;
    private $userInfo;

    public function __construct(EntityManager $em, UserInfoService $userInfo)
    {
        $this->em = $em;
        $this->userInfo = $userInfo;
    }

    public function logProcess(ProcessInstance $processInstance)
    {
        $processInstanceHistory = $this->getProcessInstanceHistory($processInstance);

        if (empty($processInstanceHistory)) {
            $userProcessHistory = new UserProcessHistory();
            $userProcessHistory->setUser($this->getUser());
            $userProcessHistory->setOriginalUser($this->getOriginalUser());
            $userProcessHistory->setProcessInstance($processInstance);

            $this->em->persist($userProcessHistory);
            $this->em->flush();

            return $userProcessHistory->getId();
        }

        return $processInstanceHistory->getId();
    }

    private function getUser()
    {
        return $this->em->getRepository(User::class)->find($this->userInfo->getUser()->getId());
    }

    private function getOriginalUser()
    {
        return $this->em->getRepository(User::class)->find($this->userInfo->getOriginalUser()->getId());
    }

    private function getProcessInstanceHistory($processInstance)
    {
        return $this->em->getRepository(UserProcessHistory::class)->findOneBy([
            'processInstance' => $processInstance,
              'user' => $this->userInfo->getUser(),
            'originalUser' => $this->userInfo->getOriginalUser(),
        ]);
    }

    public function getProcessHistory($limit = 10)
    {
        $result = $this->em->getRepository(UserProcessHistory::class)
            ->createQueryBuilder('uph')
            ->select('uph, pi')
            ->leftJoin('uph.processInstance','pi')
            ->where('pi.active = 1')
            ->setMaxResults($limit)
            ->getQuery()->getResult();

        return $result;
    }
    
}