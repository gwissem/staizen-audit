<?php

namespace CaseBundle\Service;

use AppBundle\Entity\Log;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Program;
use CaseBundle\Entity\ServiceDefinition;
use CaseBundle\Entity\ServiceStatus;
use CaseBundle\Repository\ProcessInstanceRepository;
use CaseBundle\Utils\StaticCache;
use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\DataCollector\PusherDecorator;
use Gos\Bundle\WebSocketBundle\Pusher\Amqp\AmqpPusher;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use MssqlBundle\PDO\PDO;
use Predis\Client;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;
use UserBundle\Service\UserInfoService;
use CaseBundle\Utils\Snoopy;

class CaseHandler
{

    protected $userInfo;
    protected $em;
    protected $queryManager;
    protected $pusher;
    protected $redis;
    protected $staticCache;
    protected $processHandler;

    public function __construct(UserInfoService $userInfo, EntityManager $em, QueryManager $queryManager, PusherInterface $pusher, Client $redis, ProcessHandler $processHandler)
    {
        $this->userInfo = $userInfo;
        $this->queryManager = $queryManager;
        $this->em = $em;
        $this->pusher = $pusher;
        $this->redis = $redis;
        $this->staticCache = new StaticCache($redis);
        $this->processHandler = $processHandler;
    }

    static function parseCaseNumber($caseNumber) {

        return intval(preg_replace('/[AT][0]*/', "", $caseNumber));

    }

    public function isCrossBorder($groupProcessId){
        $eventLocation = $this->processHandler->getAttributeValue('101,85,86',$groupProcessId,'string');
        $crossBorder = false;
        if(!empty($eventLocation) && $eventLocation !== 'Polska'){
            $crossBorder = true;
        }
        return $crossBorder;

    }

    public function log($content, $rootId, $userId, $name = 'user_action', $filter = null){
        $log = new Log();
        $log->setName($name);
        $log->setContent($content);
        $log->setParam1($rootId);
        $log->setParam2($filter);
        $log->setParam3($userId);

        $this->em->persist($log);
        $this->em->flush();
    }

    public function getCrossBorderGopRemarks($groupProcessInstanceId, $serviceId = null){

        $info = '';
        $serviceId = (int)$serviceId;

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => intval($groupProcessInstanceId),
                'type' => PDO::PARAM_INT
            ]
        ];

        $platformID = (int)$this->processHandler->getAttributeValue('253',$groupProcessInstanceId,'int');

        $rootProcessInstanceId = $this->processHandler->getRootId($groupProcessInstanceId);

        if($serviceId === 1 || $serviceId === 2){

            if($serviceId === 2 && !($platformID === 87 || $platformID === 88)  ){
                $info .= ' [In case of roadside assistance failure] ';
            }

            $towageDestination = $this->queryManager->executeProcedure("SELECT dbo.f_addressText(id) from dbo.attribute_value WITH(NOLOCK) where group_process_instance_id=:groupProcessInstanceId and attribute_path='211,85'", $parameters);
            $towageDestinationText = (count($towageDestination) > 0) ? $towageDestination[0][''] : '';

            $instructions = (int)$this->processHandler->getAttributeValue('767,1128',$groupProcessInstanceId,'int');

            if(!empty($towageDestinationText) && $instructions !== 5){

                if( ($platformID === 87 || $platformID === 88) && $serviceId === 2)
                {
                    $info .= 'Please send the mechanic first. If you can\'t fix it, tow to:' . $towageDestinationText . '<br/>';
                }else{
                    $info .= 'Towage destination: ' . $towageDestinationText . '<br/>';
                }

                $acceptedAboveLimit = (int)$this->processHandler->getAttributeValue('562',$groupProcessInstanceId, 'int');
                $programId = $this->processHandler->getAttributeValue('202',$groupProcessInstanceId,'string');
                $program = $this->em->getRepository(Program::class)->find($programId);

                $limitInfo = '';

                if ($program instanceof Program && $program->getAbroadTowingLimitEur() == -1)
                {
                    $limitInfo = 'Towing only to the nearest authorised workshop limit';

                }else if ($acceptedAboveLimit === 1 || $acceptedAboveLimit === 0) {

                    $programLimit = '';
                    if($program) {
                        $programLimit = $program->getAbroadTowingLimitEur().' euro';
                    }

                    if ($acceptedAboveLimit === 1) {
                        $limitInfo = 'We cover towing up to the limit stated below. Client accepted to cover costs above the limit accepted';
                    } else {
                        $limitInfo = 'We cover towing up to the limit stated below. Client DID NOT accepted to cover costs above the limit accepted';
                    }


                }

                $info .= ' ' . $limitInfo . '<br/>';

            }
            else{

                $limitKm = $this->processHandler->getAttributeValue('1123',$groupProcessInstanceId,'int');
                $queryManager = $this->queryManager;

                if($instructions === 1){
                    if(($limitKm??false) && ((int) $limitKm > 0 )){
                        $info .= $queryManager->translate('Please tow towards Poland, max. towing limit: ') . $limitKm . ' km.<br/>';
                    }else{
                        $info .= $queryManager->translate('Please tow towards Poland.') . '<br/>';
                    }
                }
                else if($instructions === 2){
                    if(($limitKm??false) && ((int) $limitKm > 0 )){
                        $info .= $queryManager->translate('Please tow to closest workshop, max. towing limit: ') . $limitKm . ' km.<br/>';
                    }else{
                        $info .= $queryManager->translate('Please tow to closest workshop.') . '<br/>';
                    }
                }
                else if($instructions === 4)
                {
                    $info .= $queryManager->translate('Please tow to the nearest authorised garage.') . '<br/>';
                }
                elseif ($instructions === 5) {
                    $info .= 'Only Roadside Assistance!<br/>';
                }
                else {
                    $workshopAddress = $this->processHandler->getAttributeValue('882',$groupProcessInstanceId,'text');
                    $destination = empty($workshopAddress) ? 'Closest authorized workshop' : $workshopAddress;
                    $info .= 'Towage destination: '.$destination.'<br/>';
                }

            }

            $difficultiesWithLocations = (int)$this->processHandler->getAttributeValue('551', $groupProcessInstanceId, 'int');

            if (($platformID == 87 || $platformID == 88 ) && $difficultiesWithLocations > 0) {
                $parameters = [
                    [
                        'key' => 'towingProblem',
                        'value' => $difficultiesWithLocations,  
                        'type' => PDO::PARAM_INT
                    ]
                ];

                $dictionaryValue = $this->queryManager->executeProcedure("SELECT TOP 1 argument5 translated from dbo.dictionary with (nolock) where typed ='towingProblems' and value = :towingProblem", $parameters);
                if (count($dictionaryValue) > 0) {
                    $translation = $dictionaryValue[0]['translated']??'';
                    $info = $info . '<br/>'. $this->queryManager->translate(' Towing problems:') . $translation . '<br/>';
                }
            }

            $numberOfPeopleToTransport = $this->processHandler->getAttributeValue('428',$groupProcessInstanceId, 'int');
            if($numberOfPeopleToTransport > 0){
                $info = $info.$this->queryManager->translate('Transport of beneficiaries included.').'<br/>';
            }


            if(($platformID == 87 || $platformID == 88) && $rootProcessInstanceId !== false ){
                $diagnosisInfo = $this->processHandler->getAttributeValue('417', $rootProcessInstanceId,AttributeValue::VALUE_TEXT);
                $info.='<br/>'.$diagnosisInfo;
            }

        }
        if($serviceId == 3){

            $customerCoversDropOff = $this->processHandler->getAttributeValue('283,111,158',$groupProcessInstanceId, 'int');
            $customerCoversPickUp = $this->processHandler->getAttributeValue('283,110,158',$groupProcessInstanceId, 'int');
            if($customerCoversDropOff == 1){
                $info = $info.'Customer covers the drop off cost<br/>';
            }

            $pickUpLocation = $this->queryManager->executeProcedure("SELECT dbo.f_addressText(id) from dbo.attribute_value 
                             where group_process_instance_id=:groupProcessInstanceId and attribute_path='283,110,85'", $parameters);
            $pickUpLocationText = (count($pickUpLocation) > 0) ? $pickUpLocation[0][''] : '';

            if($customerCoversPickUp == 1){
                $info = $info.'Customer covers the pick up cost<br/>';
            }

            if(!empty($pickUpLocationText)){
                $info = $info.'Pick up location: '.$pickUpLocationText.'<br/>';
            }

            $dropOffLocation = $this->queryManager->executeProcedure("SELECT dbo.f_addressText(id) from dbo.attribute_value 
                             where group_process_instance_id=:groupProcessInstanceId and attribute_path='283,111,85'", $parameters);
            $dropOffLocationText = (count($dropOffLocation) > 0) ? $dropOffLocation[0][''] : '';

            if(!empty($dropOffLocationText)){
                $info = $info.'Drop off location: '.$dropOffLocationText.'<br/>';
            }

            $startDate = $this->processHandler->getAttributeValue('283,285', $groupProcessInstanceId, 'date');

            if($startDate){
                $info = $info.'<br/>Pick up scheduled at: '.$startDate.'<br/>';
            }

            $remarks = $this->processHandler->getAttributeValue('283,63', $groupProcessInstanceId, 'text');

            if($remarks){
                $info = $info.'<br/>Client wishes: '.$remarks.'<br/>';
            }


        }
        if($serviceId == 4){

            $startLocation = $this->queryManager->executeProcedure("SELECT dbo.f_addressText(id) from dbo.attribute_value 
                             where group_process_instance_id=:groupProcessInstanceId and attribute_path='137,85'", $parameters);
            $startLocationText = (count($startLocation) > 0) ? $startLocation[0][''] : '';

            $remarks = $this->processHandler->getAttributeValue('137,63', $groupProcessInstanceId, 'text');
            $startDate = $this->processHandler->getAttributeValue('137,753', $groupProcessInstanceId, 'date');
            $people = $this->processHandler->getAttributeValue('152,154', $groupProcessInstanceId, 'int');

            if(!empty($people)){
                $info = $people.' person(s)<br/>';
            }
            if(!empty($startDate)){
                $info = $info.'On '.$startDate.'<br/>';
            }
            if(!empty($startLocationText)){
                $info = $info.' Near the address '.$startLocationText.'<br/>';
            }
            if(!empty($remarks)){
                $info = $info.$remarks;
            }

        }
        else if($serviceId == 5){

            $startLocation = $this->queryManager->executeProcedure("SELECT dbo.f_addressText(id) from dbo.attribute_value 
                             where group_process_instance_id=:groupProcessInstanceId and attribute_path='152,175,85'", $parameters);
            $startLocationText = (count($startLocation) > 0) ? $startLocation[0][''] : '';

            $endLocation = $this->queryManager->executeProcedure("SELECT dbo.f_addressText(id) from dbo.attribute_value 
                             where group_process_instance_id=:groupProcessInstanceId and attribute_path='152,320,85'", $parameters);
            $endLocationText = (count($endLocation) > 0) ? $endLocation[0][''] : '';

            $people = $this->processHandler->getAttributeValue('152,154', $groupProcessInstanceId, 'int');

            $info = 'Please organize taxi for '.$people.' persons<BR>'.
                'From: '.$startLocationText.'<BR>'.
                'To: '.$endLocationText.'<BR>';

        }

        $specialInfo =  $this->processHandler->getAttributeValue('838,63', $groupProcessInstanceId, 'text');
        if(!empty($specialInfo)){
            $info =  $info.'<br/>'.$specialInfo;
        }

        $additionalInfo = $this->queryManager->executeProcedure(
            "EXEC dbo.p_request_abroad_info @groupProcessInstanceId = :groupProcessInstanceId, @text = :text",
            [
                [
                    'key' => 'groupProcessInstanceId',
                    'value' => $groupProcessInstanceId,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'text',
                    'value' => null,
                    'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                    'length' => 1000
                ]
            ]
        );

        if (!empty($additionalInfo['text'])) {
            $info = $info.'<br>'.$additionalInfo['text'];
        }

        return $info;
    }

    public function isSparxCase($groupProcessInstanceId){
        $sparxId = $this->processHandler->getAttributeValue('592',$groupProcessInstanceId,'string');
        if(!empty($sparxId)){
            return true;
        }
        return false;
    }

    public function getCaseCosts($rootId, $locale = 'pl')
    {
        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR
            ]
        ];

        return $this->queryManager->executeProcedure(
            "EXEC dbo.p_cost_limit_report @rootId = :rootId, @locale = :locale",
            $parameters
        );

    }

    public function autoSkip($rootId)
    {
        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            "EXEC dbo.p_auto_skip @rootId = :rootId",
            $parameters
        );

    }

    public function autoSimulateRental($groupProcessInstanceId, $startDate)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'newStartDate',
                'value' => $startDate,
                'type' => PDO::PARAM_STR
            ]
        ];

        return $this->queryManager->executeProcedure(
            "EXEC dbo.p_rental_simulate_date @groupProcessInstanceId = :groupProcessInstanceId, @newStartDate = :newStartDate",
            $parameters, false
        );

    }


    public function sparxStatusesCount($statusId, $groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'statusId',
                'value' => $statusId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
            "SELECT dbo.f_sparx_same_statuses_count(:statusId,:groupProcessInstanceId)",
            $parameters
        );

        return (int)$output[0][''];

    }

    /**
     * @param $rootId
     */
    public function getInfoFromVinDatabase($rootId)
    {

        $tabFormated = [];
        $parameters = [
            [
                'key' => 'rootId',
                'value' => intval($rootId),
                'type' => PDO::PARAM_INT
            ]
        ];
        $table  = $this->queryManager->executeProcedure("EXEC p_get_vin_headers_by_rootId @root_id = :rootId", $parameters);


        foreach ($table as $option)
        {

            if($desc = $option['description']??false) {
                $trimed = preg_replace('/[^\w]/', '', $desc);
                if (strtolower($trimed) == 'program') {

                    $tabFormated[$option['header_id']]['program'] = $option['value'];
                }
            }
            $tabFormated[$option['header_id']]['options'][] = $option;
        }

        return $tabFormated;
    }

    /**
     * @param $name
     * @param array $data
     */
    public function addCaseLog($name, $data = array()){

        $resolve = new OptionsResolver();

        $resolve->setDefaults([
            'createdById' => NULL,
            'rootId' => NULL,
            'groupId' => NULL,
            'inputContent' => NULL,
            'outputContent' => NULL,
            'param1' => NULL,
            'param2' => NULL,
            'param3' => NULL
        ]);

        $resolve->addAllowedTypes('createdById',['null', 'int']);
        $resolve->addAllowedTypes('rootId',['null', 'int']);
        $resolve->addAllowedTypes('groupId',['null', 'int']);

        $options = $resolve->resolve($data);

        unset($data);

        $parameters = [
            [
                'key' => 'name',
                'value' => $name,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'createdById',
                'value' => $options['createdById'],
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'rootId',
                'value' => $options['rootId'],
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'groupId',
                'value' => $options['groupId'],
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'inputContent',
                'value' => $options['inputContent'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'outputContent',
                'value' => $options['outputContent'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'param1',
                'value' => $options['param1'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'param2',
                'value' => $options['param2'],
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'param3',
                'value' => $options['param3'],
                'type' => PDO::PARAM_STR
            ]
        ];

        unset($options);

        $this->queryManager->executeProcedure(
            "EXEC  [dbo].[p_add_case_log] @name = :name,
	            @createdById = :createdById,
                @rootId = :rootId,
                @groupId = :groupId,
                @inputContent = :inputContent,
                @outputContent = :outputContent,
                @param1 = :param1,
                @param2 = :param2,
                @param3 = :param3",
            $parameters,
            false
        );

        unset($parameters);

    }

    public function caseTrashCheck($processInstanceId){

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => $processInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
            "SELECT * from dbo.f_case_trash_check(:processInstanceId)",
            $parameters
        );

        return $output[0];

    }


    public function getProgramFileByPlatformProgram($platformId, $programID = null)
    {
        if (is_null($programID))
        {
            $parameters = [
                [
                    'key' => 'platformId',
                    'value' => $platformId,
                    'type' => PDO::PARAM_INT
                ]
            ];

            $output =  $this->queryManager->executeProcedure(
                "SELECT pf.*, vin_program.name from AtlasDB_v.dbo.program_info_file pf with (nolock) 
join vin_program with (nolock) on vin_program.id = pf.program_id
where pf.platform_id = :platformId",
                $parameters
            );

            return $output;
        }else{
            $parameters = [
                [
                    'key' => 'platformId',
                    'value' => $platformId,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'programId',
                    'value' => $programID,
                    'type' => PDO::PARAM_INT
                ]
            ];

            $output =  $this->queryManager->executeProcedure(
                "SELECT * from AtlasDB_v.dbo.program_info_file with (nolock) where platform_id = :platformId and program_id = :programId",
                $parameters
            );

            return $output;
        }
    }

    public function getActiveRentalAutomat($rootId){
        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
             "SELECT top 1 id from dbo.process_instance with (nolock) 
                    where step_id = '1007.053' and active = 1 and root_id = :rootId",
            $parameters
        );

        if(count($output)>0) {
            return $output[0]['id'];
        }
        return null;
    }

    /**
     * @param $orderId
     * @param null $lastSaveAt
     * @param int $statusPoint
     * @return array
     */
    public function getOrderCoordinates($orderId, $lastSaveAt = NULL, $statusPoint = 4) {

        $parameters = [
            [
                'key' => 'orderId',
                'value' => $orderId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'lastSaveAt',
                'value' => $lastSaveAt,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'statusPoint',
                'value' => $statusPoint,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
            "EXEC dbo.P_map_monitoring_coordinates @orderId = :orderId, @lastSaveAt = :lastSaveAt, @status = :statusPoint",
            $parameters
        );

        return $output;

    }


    /**
     * @param int $status
     * @return array
     */
    public function getOrders($status = 4) {

        $parameters = [
            [
                'key' => 'status',
                'value' => $status,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
            'EXEC dbo.P_map_monitoring_orders @status = :status',
            $parameters
        );

        return $output;

    }


    /**
     * @param $orderId
     * @return array
     */
    public function getInfoOrderInTeka($orderId) {

        $parameters = [
            [
                'key' => 'orderId',
                'value' => $orderId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
            "EXEC dbo.P_map_monitoring_data @orderId = :orderId",
            $parameters
        );

        return $output[0];

    }

    /**
     * @param $orderId
     * @return array
     */
    public function getIdOrderByNumber($number) {

        $parameters = [
            [
                'key' => 'number',
                'value' => $number,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
            "SELECT id FROM AtlasDB_ics.dbo.orders WITH(NOLOCK) WHERE [number] = :number",
            $parameters
        );

        if(empty($output)) {
            return null;
        }

        return $output[0]['id'];

    }

    /**
     * @param $rootId
     * @return array
     */
    public function getCoordinatesOfService($rootId): array
    {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
            "EXEC dbo.P_coordinates_of_service @rootId = :rootId",
            $parameters
        );

        if(empty($output)) {
            return [];
        }

        return $output[0];

    }

    /**
     * @param $rootId
     * @return array
     */
    public function getICSOrderDetails($orderId): array
    {

        $parameters = [
            [
                'key' => 'orderId',
                'value' => $orderId,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure(
            "EXEC dbo.P_ics_order_details @orderId = :orderId",
            $parameters
        );

    }

}