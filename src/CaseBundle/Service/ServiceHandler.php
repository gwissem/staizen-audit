<?php

namespace CaseBundle\Service;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use Doctrine\ORM\EntityManager;
use MssqlBundle\PDO\PDO;
use Symfony\Component\Stopwatch\Stopwatch;
use UserBundle\Entity\User;
use UserBundle\Service\UserInfoService;

class ServiceHandler
{

    const STEP_ID_FIXING = '1009.054';
    const STEP_ID_TOWING = '1009.034';
    protected $userInfo;
    protected $em;
    protected $queryManager;

    /** @var ProcessHandler $processHandler */
    protected $processHandler;
    protected $attributeParser;

    /**
     * ServiceHandler constructor.
     * @param UserInfoService $userInfo
     * @param EntityManager $em
     * @param QueryManager $queryManager
     * @param ProcessHandler $processHandler
     * @param AttributeParserService $attributeParser
     */
    public function __construct(UserInfoService $userInfo, EntityManager $em, QueryManager $queryManager,
                                ProcessHandler $processHandler, AttributeParserService $attributeParser)
    {
        $this->userInfo = $userInfo;
        $this->em = $em;
        $this->queryManager = $queryManager;
        $this->processHandler = $processHandler;
        $this->attributeParser = $attributeParser;

    }

    /**
     * @param $processInstanceId
     * @param $groupProcessInstanceId
     * @param int $init
     * @param string $locale
     * @return array
     */
    public function availableServices($processInstanceId, $groupProcessInstanceId, $init = 1, $locale = 'pl')
    {

        if($processInstanceId === NULL && $init = 1){

            $platformId = 11;

            $query = "SELECT sd.id, sdt.name, 0 active, '' message, sdpd.description description, sd.icon, sd.start_step_id start_step_id, 0 progress, '' status_message
                    FROM service_definition sd
                    INNER JOIN service_definition_translation sdt ON sd.id = sdt.translatable_id
                    LEFT JOIN service_platform_description sdpd ON sdpd.service_definition_id = sd.id AND sdpd.platform_id = $platformId 
                    WHERE sd.active = 1
                    AND sdt.locale = 'pl'
                    AND ISNULL(sd.type,1) = 1 
                    ORDER BY sd.position";
            $services = $this->queryManager->executeProcedure($query);
        }
        else {
            $parameters = [
                [
                    'key' => 'processInstanceId',
                    'value' => (int)$processInstanceId,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'init',
                    'value' => (int)$init,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'locale',
                    'value' => $locale,
                    'type' => PDO::PARAM_STR
                ]
            ];

            $services = $this->queryManager->executeProcedure(
                'EXEC dbo.p_check_available_services @processInstanceId = :processInstanceId, @init = :init, @locale = :locale',
                $parameters
            );
        }

        /** Aktualizacja statusu usługi */
        $this->statusOfAvailableServices($groupProcessInstanceId, $services, $locale);

        /** Ustawienie 'confirmation' na odpalaniu usługi */
        $this->setExtraInfoOfServices($services, $groupProcessInstanceId, $locale);

        /** Aktualizacja zadań do zrobienia w sprawie */
//        $this->activeStepsInService($groupProcessInstanceId, $activeServices, $output, $locale);

        $useHelper = $this->processHandler->getBusinessConfig('use_helper', $groupProcessInstanceId);

        if($useHelper === '1') {
            $this->setHelpWarning($groupProcessInstanceId, $services);
        }

        return $services;

    }

    public function setHelpWarning($groupProcessInstanceId, &$services) {

        $helpCompany = $this->processHandler->getAttributeValue('981,438',$groupProcessInstanceId, AttributeValue::VALUE_STRING);
        $isVip = $this->processHandler->getAttributeValue('1004',$groupProcessInstanceId, AttributeValue::VALUE_STRING);

        if(!$helpCompany && $isVip){
            $helpCompany = $isVip;
        }

        foreach ($services as $key => $service) {

            $isHelp = false;
            $text = '';

            if(in_array($service['id'], [1,2,3,7])) {

                switch ($service['id']) {

                    case 1: {}
                    case 2: {
                        $isHelp = $this->processHandler->isHelp($groupProcessInstanceId, 'info_nh');
                        $text = 'Uwaga! Dla tego klienta '.$helpCompany.' mogą być przewidziane specjalne warunki obsługi Naprawy lub Holowania. ZAPOZNAJ SIĘ Z NOTATKĄ KLUCZOWĄ, W KTÓREJ ZNAJDZIESZ TREŚĆ HELPA I ZASTOSUJ PODCZAS OBSŁUGI TEGO ZLECENIA.';
                        break;
                    }
                    case 3: {
                        $isHelp = $this->processHandler->isHelp($groupProcessInstanceId, 'info_mw');
                        $text = 'Uwaga! Dla tego klienta '.$helpCompany.' mogą być przewidziane specjalne warunki organizacji Auta zastępczego. ZAPOZNAJ SIĘ Z NOTATKĄ KLUCZOWĄ, W KTÓREJ ZNAJDZIESZ TREŚĆ HELPA I ZASTOSUJ PODCZAS OBSŁUGI TEGO ZLECENIA.';
                        break;
                    }
                    case 7: {
                        $isHelp = $this->processHandler->isHelp($groupProcessInstanceId, 'info_transport');
                        $text = 'Uwaga! Dla tego klienta mogą być przewidziane specjalne warunki organizacji Transportu.  ZAPOZNAJ SIĘ Z NOTATKĄ KLUCZOWĄ, W KTÓREJ ZNAJDZIESZ TREŚĆ HELPA I ZASTOSUJ PODCZAS OBSŁUGI TEGO ZLECENIA.';
                        break;
                    }
                }

            }

            if($isHelp) {
                $services[$key]['helper'] = $text;
            }

        }

    }

    /**
     * @param $groupProcessInstanceId
     * @param $services
     * @param string $locale
     */
    public function statusOfAvailableServices($groupProcessInstanceId, &$services, $locale = 'pl') {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => (string)$locale,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            'EXEC dbo.p_status_services @groupProcessInstanceId = :groupProcessInstanceId, @locale = :locale',
            $parameters
        );

        foreach ($services as $key => $service) {

            $services[$key]['status_message'] = '';
            $services[$key]['progress'] = 0;

            if(!empty($output)) {

                $found_key = array_search($service['id'], array_column($output, 'service'));

                if($found_key !== false ) {
                    $this->attributeParser->setGroupProcessInstanceId($output[$found_key]['group_process_id']);
                    $services[$key]['status_message'] = $this->attributeParser->parseString($output[$found_key]['message']);
                    $services[$key]['progress'] = $output[$found_key]['progress'];
                    $services[$key]['actions_step'] = $this->getServiceActions($output[$found_key]['group_process_id'], $service['id']);

                }

            }

        }

    }

    /**
     * @param $groupProcessInstanceId
     * @param $services
     * @param string $locale
     */
    public function setExtraInfoOfServices(&$services, $groupProcessInstanceId, $locale = 'pl') {

        $ids = array_map(static function ($ele) {
            return $ele['id'];
        }, $services);

        $parameters = [
            [
                'key' => 'servicesIds',
                'value' => implode(',', $ids),
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            'EXEC  [dbo].[P_extra_info_of_services] @servicesIds = :servicesIds, @groupProcessInstanceId = :groupProcessInstanceId, @locale = :locale',
            $parameters
        );

        foreach ($output as $item) {

            foreach ($services as $key => $service) {

                if($item['id'] === $service['id']) {
                    $services[$key]['service_start_confirmation'] = $item['service_start_confirmation'];
                    $services[$key]['alias'] = $item['alias'];
                    continue;
                }

            }

        }

    }

    public function getServiceActions($groupProcessId, $serviceId) {

        $actionsStep = [];

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceId',
                'value' => (int)$serviceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            'EXEC dbo.p_service_actions @groupProcessInstanceId = :groupProcessInstanceId, @serviceId = :serviceId',
            $parameters
        );

        if(!empty($output)) {

            foreach ($output as $item) {
                $actionsStep[] = [
                    'name' => $item['message'],
                    'data' => \GuzzleHttp\json_encode($item)
                ];
            }

        }

        return $actionsStep;

    }

    public function activeTaskInServices($groupProcessInstanceId, $servicesIds, $locale = 'pl') {

        $userId = $this->userInfo->getUser()->getId();
        $originalUserId = $this->userInfo->getOriginalUser()->getId();

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => (int)$userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'originalUserId',
                'value' => (int)$originalUserId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => (string)$locale,
                'type' => PDO::PARAM_STR
            ],
            [
                'key' => 'servicesIds',
//                'value' => implode(",", $servicesIds),
                'value' => NULL,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_active_steps_in_services @groupProcessInstanceId = :groupProcessInstanceId, @locale = :locale, @servicesIds = :servicesIds, @originalUserId = :originalUserId, @userId = :userId",
            $parameters
        );

        $tasks = [];
        $now = new \DateTime();

        foreach ($output as $item) {

            $hasAccess = $this->processHandler->checkPermission($userId, $item['id']);

            if ($hasAccess) {

                if(!isset($tasks[$item['service_id']])) {
                    $tasks[$item['service_id']] = [
                        'service_id' => $item['service_id'],
                        'timeout' => false,
                        'tasks' => []
                    ];
                }

                $process = $this->processHandler->getProcessInstanceDescription($item['id']);

                $process['postpone_date'] = $item['postpone_date'];
                $process['service_id'] = $item['service_id'];
                $process['timeout'] = null;
                $process['timeout_minutes'] = null;

                if($process['postpone_date']) {
                    $postponeDate = new \DateTime($process['postpone_date']);

                    if($postponeDate < $now) {
                        $process['timeout'] = true;
                        $tasks[$item['service_id']]['timeout'] = true;
                        $process['timeout_minutes'] = round(abs($postponeDate->getTimestamp() - $now->getTimestamp()) / 60,0);
                    }
                }
                else {
                    if($process['createdAt'] < $now) {
                        $tasks[$item['service_id']]['timeout'] = true;
                    }
                }

                $tasks[$item['service_id']]['tasks'][] = $process;


            }
        }

        return $tasks;

    }

    public function activeStepsInService($groupProcessInstanceId, $servicesIds, &$services, $locale = 'pl') {

        $servicesWithActiveTasks = $this->activeTaskInServices($groupProcessInstanceId, $servicesIds, $locale);

        foreach ($services as $key => $service) {
            $services[$key]['active_tasks'] = [];
        }

        foreach ($servicesWithActiveTasks as $serviceId => $item) {

            $found_key = array_search($serviceId, array_column($services, 'id'));

            if($found_key !== false ) {
                $services[$found_key]['active_tasks'] = $item['tasks'];
            }

        }

    }

    public function runService($nextStepId, $processInstanceId, $parentId = NULL, $attributePath = null, $value = null){

        $parameters = [
            [
                'key' => 'previousProcessId',
                'value' => (int)$processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'nextStepId',
                'value' => $nextStepId,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ],
            [
                'key' => 'doNext',
                'value' => 0,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'deactivateCurrent',
                'value' => 0,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'rootId',
                'value' => null,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'parentId',
                'value' => $parentId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'groupId',
                'value' => null,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'variant',
                'value' => null,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
            [
                'key' => 'newProcessInstanceId',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_process_jump @previousProcessId = :previousProcessId, @nextStepId = :nextStepId, @doNext = :doNext, @deactivateCurrent = :deactivateCurrent, @parentId = :parentId, @rootId = :rootId, @groupId = :groupId, @variant = :variant, @err = :err, @message = :message, @newProcessInstanceId = :newProcessInstanceId",
            $parameters
        );

        if($attributePath && $value){

            $newProcessInstanceId = $output['newProcessInstanceId'];
            $this->processHandler->editAttributeValue($attributePath, $newProcessInstanceId, $value, 'int', 'xxx' , 1, 1);
        }

        return $output;
    }

    /**
     * @param $processInstanceId
     * @return array
     */
    public function availableLocations($processInstanceId)
    {

        $parameters = [
            [
                'key' => 'processInstanceId',
                'value' => (int)$processInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'init',
                'value' => 1,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'skipCurrent',
                'value' => 1,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_available_locations @processInstanceId = :processInstanceId, @init = :init, @skipCurrent = :skipCurrent",
            $parameters
        );

        return $output;

    }

    public function generateCostsVerificationReport($groupProcessInstanceId, $name = 'p_rsa_costs_report', $user){

        /** @var User $userId */
        $userId = $user ? $user->getId() : null;

        if($groupProcessInstanceId) {
            $parameters = [
                [
                    'key' => 'groupProcessInstanceId',
                    'value' => (int)$groupProcessInstanceId,
                    'type' => PDO::PARAM_INT
                ]
            ];

            $query = "EXEC dbo.".$name." @groupProcessInstanceId = :groupProcessInstanceId";


            if($name == 'p_rsa_costs_report'){

                $parameters[] = [
                    'key' => 'userId',
                    'value' => $userId,
                    'type' => PDO::PARAM_INT
                ];

                $query = "EXEC dbo.".$name." @groupProcessInstanceId = :groupProcessInstanceId, @userId = :userId";
            }

            return $this->queryManager->executeProcedure(
                $query,
                $parameters
            );

        }

    }


    public function getExtraPrograms($groupProcessInstanceId){
        $programIds = $this->processHandler->getAttributeValue('204',$groupProcessInstanceId,'string');
        $platform = $this->processHandler->getAttributeValue('253',$groupProcessInstanceId,'int');

        if(empty($programIds)){
            $programId = $this->processHandler->getAttributeValue('202',$groupProcessInstanceId,'string');
            $query = "SELECT id, name FROM vin_program where platform_id IS NULL or (platform_id = $platform AND is_extra = 1) or id = $programId";
        }
        else{
            $query = "SELECT id, name FROM vin_program where platform_id IS NULL or (platform_id = $platform AND is_extra = 1) OR dbo.f_exists_in_split('$programIds',id) = 1";
        }

        return $this->queryManager->executeProcedure($query);
    }

    public function getRelatedServices($rootId)
    {
        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'EXEC [dbo].[p_case_services_groups] @rootId = :rootId';

        return $this->queryManager->executeProcedure($query, $parameters);
    }

    public function getCdnInfo($rootProcessId) {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$rootProcessId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'EXEC [dbo].[p_services_in_cdn] @rootId = :rootId';

        return $this->queryManager->executeProcedure($query, $parameters);

    }


    /**
     * @param $rootId
     * @return array
     */
    public function getAll_RS_inCase($rootId) {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'SELECT * FROM dbo.f_getAll_RS_inCase(:rootId)';

        return $this->queryManager->executeProcedure($query, $parameters);

    }

    /**
     * @param $rootId
     * @return array
     */
    public function getAllRZWinCase($rootId) {

        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        return $this->queryManager->executeProcedure('SELECT group_process_id, token FROM dbo.f_getAll_RZW_inCase(:rootId)', $parameters);

    }



    /**
     * @param $groupProcessId
     * @param $serviceId
     * @return array
     */
    public function getExtraDataService($groupProcessId, $serviceId) {

        $parameters = [
            [
                'key' => 'groupProcessId',
                'value' => (int)$groupProcessId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceId',
                'value' => (int)$serviceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = 'EXEC dbo.p_extra_data_of_overview_service @groupProcessId = :groupProcessId, @serviceId = :serviceId';

        return $this->queryManager->executeProcedure($query, $parameters);

    }

}