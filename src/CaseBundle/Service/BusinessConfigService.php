<?php

namespace CaseBundle\Service;

use CaseBundle\Entity\BusinessConfig;
use CaseBundle\Entity\BusinessConfigDefinition;
use CaseBundle\Entity\BusinessConfigDefinitionTranslation;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\Program;
use CaseBundle\Repository\BusinessConfigRepository;
use CaseBundle\Utils\BusinessConfigTab;
use Doctrine\ORM\EntityManager;

class BusinessConfigService
{


    /** @var  EntityManager $em */
    protected $em;

    /** @var  BusinessConfigDefinition[] $platformConfigs */
    private $platformConfigs;

    /** @var  BusinessConfigDefinition[] $platformConfigs */
    private $programConfigs;

    private $addedPlatformConfig = 0;
    private $addedProgramConfig = 0;

    private $orderTabs = [
        "Ogólna konfiguracja",
        "Naprawa warsztatowa",
        "Holowanie",
        "Auto zastępcze",
        "Nocleg",
        "Taxi",
        "TNP",
        "TNNP",
        "Podróż",
        "Parking",
        "Części zastępcze",
        "Kredyt",
        "Informacja",
        "Holowanie poszkodowanego",
        "Naprawa na drodze"
    ];

    private $orderKeys = [
        "enabled_when_active",
        "enabled_when_active_abroad",
        "disabled_when_active",
        "disabled_when_active_abroad"
    ];

    /**
     * BusinessConfigService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }


    /**
     * Grupuje konfigi per kategorię
     *
     * @param BusinessConfig[] $configs
     * @return BusinessConfigTab[]
     */
    public function aggregateConfigs($configs)
    {

        /** @var BusinessConfigTab[] $tabs */
        $tabs = [];

        foreach ($configs as $config) {

            $categoryName = ($config->getConfigDefinition()->getCategory()) ?: 'Inne';

            if(!isset($tabs[$categoryName])) {
                $tabs[$categoryName] = new BusinessConfigTab($categoryName);
            }

            $tabs[$categoryName]->addConfig($config);

        }

        $this->sortKeys($tabs);
        $this->sortTabs($tabs);

        return $tabs;
    }

    private function sortKeys(&$tabs) {

        $order = $this->orderKeys;

        /** @var BusinessConfigTab[] $tabs */
        foreach ($tabs as $tab) {

            uasort($tab->configs, function($conf1, $conf2) use ($order) {

                /** @var BusinessConfig $conf1*/
                /** @var BusinessConfig $conf2*/

                $o1 = 200;
                $o2 = 200;

                $key1 = $conf1->getConfigDefinition()->getKey();
                $key2 = $conf2->getConfigDefinition()->getKey();

                if(strpos($key1, "enabled_when_") !== FALSE || strpos($key1, "disabled_when_") !== FALSE) {
                    $keyName = (explode('.', $key1))[1];
                    $o1 = array_search($keyName, $order);
                }

                if(strpos($key2, "enabled_when_") !== FALSE || strpos($key2, "disabled_when_") !== FALSE) {
                    $keyName = (explode('.', $key2))[1];
                    $o2 = array_search($keyName, $order);
                }

                return $o1 >  $o2;

            });

        }

    }

    private function sortTabs(&$tabs) {

        $order = $this->orderTabs;

        uasort($tabs, function($tab1, $tab2) use ($order) {

            $key1 = array_search($tab1->name, $order);
            $key2 = array_search($tab2->name, $order);

            return (($key1 !== false) ? $key1 : 100) >  (($key2 !== false) ? $key2 : 100);

        });

    }

    /**
     * @param BusinessConfig $businessConfig
     * @param $value
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveConfig(BusinessConfig $businessConfig, $value) {

        if($businessConfig->getConfigDefinition()->getType() === "multiselect") {
            if(is_array($value)) {
                $value = implode(",", $value);
            }
        }
        elseif($businessConfig->getConfigDefinition()->getType() === "checkbox") {
            $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
            $value = ($value) ? 1 : 0;
        }

        $businessConfig->setValue($value);
        $this->em->persist($businessConfig);
        $this->em->flush();

    }

    /**
     * @param BusinessConfig $businessConfig
     * @param $description
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveDescription(BusinessConfig $businessConfig, $description) {

        $businessConfig->setDescription($description);
        $this->em->persist($businessConfig);
        $this->em->flush();

    }

    /**
     * @param array|null $platformsId
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updatePlatformsConfigs(array $platformsId = null) {

        if(empty($platformsId)) {
            $platformsId = $this->em->getRepository('CaseBundle:Platform')->findBy([
                'active' => 1
            ]);
        }

        if(empty($platformsId)) return [0,0];

        $this->loadConfigs();

        /** pętla po platformach i sprawdzenie czy mają konfigurację */
        foreach ($platformsId as $platform) {

            if(!$platform instanceof Platform) {
                $platform = $this->em->getRepository('CaseBundle:Platform')->find(intval($platform));
            }

            if(!$platform) continue;

            $this->checkPlatformConfigs($platform);
            $this->checkProgramsConfigs($platform);

            $this->em->flush();

        }

        return [$this->addedPlatformConfig, $this->addedProgramConfig];

    }

    /**
     * Pobranie definicji konfiguracji
     */
    private function loadConfigs() {

//        $this->platformConfigs = $this->em->getRepository('CaseBundle:BusinessConfigDefinition')->findBy([
//            'isProgram' => null
//        ]);

        $this->platformConfigs = $this->em->getRepository('CaseBundle:BusinessConfigDefinition')->findAll();

        $this->programConfigs = $this->em->getRepository('CaseBundle:BusinessConfigDefinition')->findBy([
            'isProgram' => true
        ]);

    }

    /**
     * Sprawdzenie czy platforma ma wszystkie konfiguracje i aktualizacja
     *
     * @param Platform $platform
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function checkPlatformConfigs(Platform $platform) {

        $this->addNewConfigs($this->platformConfigs, $platform);
//        $this->removeConfigs($platform);

    }

    /**
     * @param Platform $platform
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function checkProgramsConfigs(Platform $platform) {


        /** @var Program $program */
        foreach ($platform->getPrograms() as $program) {

            $this->addNewConfigs($this->programConfigs, $platform, $program);

        }

    }

    /**
     * Dodanie brakujących konfiguracji
     *
     * @param BusinessConfigDefinition[] $defConfigs
     * @param Platform $platform
     * @param Program|null $program
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addNewConfigs($defConfigs, Platform $platform, Program $program = null) {

        /** @var BusinessConfig[] $currentConfigs */
        $currentConfigs = $this->em->getRepository('CaseBundle:BusinessConfig')->findBy([
            'platform' => $platform,
            'program' => $program
        ]);

        foreach ($defConfigs as $defConfig) {

            $hasConfig = false;

            foreach($currentConfigs as $currentConfig) {
                if ($currentConfig->getConfigDefinition()->getId() == $defConfig->getId()) {
                    $hasConfig = true;
                    break;
                }
            }

            if(!$hasConfig) {
                if($program){
                    $this->addConfig($defConfig, $platform, $program);
                }
                else {
                    $this->addConfig($defConfig, $platform);
                }
            }

        }

        $this->em->flush();
        $this->em->clear(BusinessConfig::class);

    }

    public function addNewKey($keyDetails)
    {
        try{

            $key = new BusinessConfigDefinition();
            $key->setKey($keyDetails['key-id']);
            $key->setType($keyDetails['key-type']);
            $key->setQuery($keyDetails['key-query']);
            $key->setCategory($keyDetails['key-category']);
            $key->setQuery($keyDetails['key-query']);
            $key->setIsProgram($keyDetails['key-isProgram']?? 0);

            $this->em->persist($key);
            $this->em->flush();

            $translation = new BusinessConfigDefinitionTranslation();
            $translation->setName($keyDetails['key-name']);
            $translation->setDescription($keyDetails['key-desc']);
            $translation->setTranslatable($key);
            $translation->setLocale('pl');
            $this->em->persist($translation);
            $this->em->flush();

        }catch (\Exception $exception){

        }


    }


//    private function removeConfigs(Platform $platform, Program $program = null) {
//
//        /** @var BusinessConfig[] $currentConfigs */
//        $currentConfigs = $this->em->getRepository('CaseBundle:BusinessConfig')->findBy([
//            'platform' => $platform,
//            'program' => $program
//        ]);
//
//        if($program) {
//            if(count($currentConfigs) !== count($this->programConfigs)) {
//                $this->checkForRemove($currentConfigs, $this->programConfigs);
//            }
//        }
//        else {
//            if(count($currentConfigs) !== count($this->platformConfigs)) {
//                $this->checkForRemove($currentConfigs, $this->platformConfigs);
//            }
//        }
//
//    }

//    private function checkForRemove($currentConfigs, $defConfigs) {
//
//        foreach($currentConfigs as $currentConfig) {
//
//            $hasConfig = false;
//
//            foreach ($defConfigs as $defConfig) {
//                if ($currentConfig->getConfigDefinition()->getId() == $defConfig->getId()) {
//                    $hasConfig = true;
//                    break;
//                }
//            }
//
//            if(!$hasConfig) {
//                $this->em->remove($currentConfig);
//            }
//
//        }
//
//        $this->em->flush();
//
//    }

    /**
     * @param BusinessConfigDefinition $businessConfigDefinition
     * @param Platform $platform
     * @param Program|null $program
     */
    private function addConfig(BusinessConfigDefinition $businessConfigDefinition, Platform $platform, Program $program = null) {

        $newConfig = new BusinessConfig();
        $newConfig->setConfigDefinition($businessConfigDefinition);
        $newConfig->setPlatform($platform);

        if($businessConfigDefinition->isProgram() && $program) {
            $newConfig->setProgram($program);
            $this->addedProgramConfig++;
        }
        else {
            $this->addedPlatformConfig++;
        }

        $this->em->persist($newConfig);

    }


    /**
     * @param Program $source
     * @param Program $target
     * @param false $copyPlatformConfigs
     * @return int
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function copyConfigsProgramToProgram(Program $source, Program $target, $copyPlatformConfigs = false) {

        /** @var BusinessConfig[] $mapSourceConfigs */
        /** @var BusinessConfig[] $mapTargetConfigs */
        list($mapSourceConfigs, $mapTargetConfigs) = $this->getMappedConfigs($source, $target);

        $i = 0;

        foreach ($mapSourceConfigs as $key => $sourceConfig) {
            if(key_exists($key, $mapTargetConfigs)) {
                $mapTargetConfigs[$key]->copyValues($sourceConfig);
                $this->em->persist($mapTargetConfigs[$key]);
                $i++;
            }
        }

        $this->em->flush();

        $isSamePlatform = ($source->getPlatform() == $target->getPlatform());

        // Kopiowanie konfiguracji platform
        if($copyPlatformConfigs && !$isSamePlatform) {

            list($mapSourceConfigs, $mapTargetConfigs) = $this->getMappedConfigs($source, $target, true);

            foreach ($mapSourceConfigs as $key => $sourceConfig) {
                if(key_exists($key, $mapTargetConfigs)) {
                    $mapTargetConfigs[$key]->copyValues($sourceConfig);
                    $this->em->persist($mapTargetConfigs[$key]);
                    $i++;
                }
            }

            $this->em->flush();

        }

        return $i;

    }


    /**
     * @param Program $source
     * @param Program $target
     * @param bool $onlyPlatform
     * @return array
     */
    private function getMappedConfigs(Program $source, Program $target, $onlyPlatform = false) {

        /** @var BusinessConfigRepository $repository */
        $repository = $this->em->getRepository('CaseBundle:BusinessConfig');

        /** @var BusinessConfig[] $mapTargetConfigs */
        $mapSourceConfigs = [];
        /** @var BusinessConfig[] $mapTargetConfigs */
        $mapTargetConfigs = [];

        if($onlyPlatform) {
            $sourceConfigs = $repository->findConfigs($source->getPlatform());
            $targetConfigs = $repository->findConfigs($target->getPlatform());
        }
        else {
            $sourceConfigs = $repository->findConfigs($source->getPlatform(), $source);
            $targetConfigs = $repository->findConfigs($target->getPlatform(), $target);
        }

        /** @var BusinessConfig $config */
        foreach ($sourceConfigs as $config) {
            $mapSourceConfigs[$config->getConfigDefinition()->getKey()] = $config;
        }

        /** @var BusinessConfig $config */
        foreach ($targetConfigs as $config) {
            $mapTargetConfigs[$config->getConfigDefinition()->getKey()] = $config;
        }

        unset($sourceConfigs);
        unset($targetConfigs);

        return [$mapSourceConfigs, $mapTargetConfigs];
    }

}