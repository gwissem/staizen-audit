<?php

namespace CaseBundle\Service;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValueTranslator;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Utils\Language;
use CaseBundle\Utils\ParserMethods;
use CaseBundle\Utils\StaticCache;
use Doctrine\ORM\EntityManagerInterface;
use CaseBundle\Entity\ProcessInstance;
use OperationalBundle\Service\OperationDashboardInterface;
use ReflectionMethod;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\FirstnameGender;
use UserBundle\Service\UserInfoParserService;
use AppBundle\Service\DynamicTranslationService;
use Utils\StepInfoLocator;

class AttributeParserService implements ParserInterface
{
    const PREFIX = '{@';
    const QUOTATION_PREFIX = '"{@';
    const SUFFIX = '@}';
    const QUOTATION_SUFFIX = '@}"';
    const WORD_PREFIX = '{#';
    const WORD_QUOTATION_PREFIX = '"{#';
    const WORD_SUFIX = '#}';
    const WORD_QUOTATION_SUFFIX = '#}"';
    const CODE_SEPARATOR = '```';
    const REGEX_VALIDATION = '[0-9]+(,[0-9]+)*';
    const REGEX_CUSTOM_FUNCTION = "/" . self::WORD_PREFIX . "([\\/-a-zA-ZążźćśęńłóąŻŹĆŚĘŃŁÓ0-9*:()=',|\" {@^}._-]*)" . self::WORD_SUFIX . "/i";
    const VALUE_NOT_FOUND = '';
    const GENDER = 'gender';
    const HELLO = 'hello';
    const PLATFORM_PHONE = 'platformphone';
    const SERVER_HOST = 'serverhost';
    const PLATFORM_PHONE_PREFIX = '618319';
    const PLATFORM_NAME = 'platformname';
    const CASE_ID = 'caseid';
    const ROOT_PROCESS_ID = 'rootid';
    const STEP_ID = 'stepid';
    const INSTANCE_ID = 'instanceid';
    const GROUP_ID = 'groupid';
    const SPARX_ID = 'sparxid';
    const PATH_SPARX_ID = '592';
    const VIP_ATTR_ID = '1004';

    public $queryManager;

    /**
     * @var ProcessHandler
     */
    public  $processHandler;
    protected $groupProcessInstanceId;
    protected $em;
    protected $twig;
    protected $translator;
    private $parserMethods;
    protected $processInstanceId;
    private $userInfoParser;
    private $dynamicTranslationService;
    private $serverHost;

    /**
     * @var StaticCache
     */
    protected $staticCache;

    public function __construct(QueryManager $queryManager,
                                ProcessHandler $processHandler,
                                EntityManagerInterface $em,
                                \Twig_Environment $twig,
                                TranslatorInterface $translator,
                                ParserMethods $parserMethods,
                                UserInfoParserService $userInfoParser,
                                DynamicTranslationService $dynamicTranslationService,
                                $serverHost)
    {
        $this->queryManager = $queryManager;
        $this->processHandler = $processHandler;
        $this->em = $em;
        $this->twig = $twig;
        $this->translator = $translator;
        $this->parserMethods = $parserMethods;
        $this->parserMethods->setAttributeParser($this);
        $this->userInfoParser = $userInfoParser;
        $this->dynamicTranslationService = $dynamicTranslationService;
        $this->serverHost = $serverHost;
        $this->staticCache = new StaticCache();
    }

    public function getGroupProcessInstanceId()
    {
        return $this->groupProcessInstanceId;
    }

    public function getRootProcessInstanceId()
    {
        $groupProcess =  $this->em->getRepository(ProcessInstance::class)->find($this->groupProcessInstanceId);
        return $groupProcess->getRoot()->getId();
    }

    public function setGroupProcessInstanceId($groupProcessInstanceId)
    {
        $this->groupProcessInstanceId = $groupProcessInstanceId;

        return $this;
    }

    public function setProcessInstanceId($processInstanceId)
    {
        $this->processInstanceId = $processInstanceId;

        return $this;
    }

    public function parse(string $attributeSchema): string
    {
        return $this->parseString($attributeSchema);
    }


    public function isSchemaFormat(string $attributeSchema, bool $validEmpty = false): bool
    {
        $patternMatches = preg_match_all('/' . self::PREFIX . self::REGEX_VALIDATION . self::SUFFIX . '/', $attributeSchema);
        $openSignMatches = preg_match_all('/' . self::PREFIX . '/', $attributeSchema);
        $closeSingMatches = preg_match_all('/' . self::SUFFIX . '/', $attributeSchema);

        if ($validEmpty && empty($patternMatches)) {
            return false;
        }

        return (bool)($patternMatches == $openSignMatches
            && $patternMatches == $closeSingMatches
            && $closeSingMatches == $openSignMatches);
    }

    private function doParse(string $attributePath, $limit = NULL, $emptyApostrophe = false, $forceUTCTimezone = false, $isValidation=false, $escape = false)
    {
        if (empty($this->groupProcessInstanceId)) {
            throw new \Exception('$groupProcessInstanceId is empty, before using parse()
                argument should be declared by function setGroupProcessInstanceId()');
        }
        $groupProcessId = $this->groupProcessInstanceId;

        $checkInRoot = (strpos($attributePath, '^') !== false);

        if ($checkInRoot) {
            $attributePath = str_replace('^', '', $attributePath);
            $groupProcessId = $this->em->getRepository(ProcessInstance::class)->find($this->groupProcessInstanceId)->getRoot()->getId();
        }

        $hasStar = (strpos($attributePath, '*') !== false);

        if ($hasStar) {
            $attributePath = str_replace('*', '', $attributePath);
        }

        // For JavaScript
        $escapeQuotes = (strpos($attributePath, '%') !== false);

        if ($escapeQuotes) {
            $attributePath = str_replace('%', '', $attributePath);
        }

        $ids = explode(',', $attributePath);
        $lastId = end($ids);

        $result = $this->processHandler->getAttributeValue($attributePath, $groupProcessId, NULL, NULL, NULL, null, $forceUTCTimezone);

        if ($result) {

            $attributeValue = $this->getAttributeResult($result, $hasStar, $forceUTCTimezone, $isValidation );

            $attribute = $this->em->getRepository(Attribute::class)->find($lastId);

            if (!$hasStar) {

                $attributeValue = $this->getValueFromAttributeQuery($attribute->getQuery(), $attributeValue);

                if ($limit) {
                    if (strpos($limit, '-') === 0) {
                        $attributeValue = substr($attributeValue, $limit);
                    } else {
                        $attributeValue = substr($attributeValue, 0, $limit);
                    }
                }
            }
            elseif($hasStar && $attribute->getInputType() == Attribute::INPUT_TYPE_YESNO){
                if($attributeValue == 1){
                    return $this->translator->trans('radio_button.yes');
                }
                elseif($attributeValue == 0){
                    return $this->translator->trans('radio_button.no');
                }
            }
            elseif($hasStar) {
                if(strpos($attributeValue,'"') !== FALSE) {
                    $attributeValue = str_replace('\"',"''", $attributeValue);
                }
            }

            if($attributeValue && $escapeQuotes) {
                $attributeValue = str_replace(array('\'', '"'), array('\\\'', '\"'), $attributeValue);
            }

            if($emptyApostrophe && $attributeValue === ''){
                return '""';
            }

            return $attributeValue;
        }
        return self::VALUE_NOT_FOUND;
    }

    public function getValueFromAttributeQuery($query, $attributeValue) {

        if (is_numeric($attributeValue) && $query) {
            $array = $this->queryManager->executeProcedure($this->parseString($query));
            $returnArray = [];
            foreach ($array as $element) {
                $elementArray = array_values($element);
                $value = $elementArray[0];
                $label = count($elementArray) > 1 ? $elementArray[1] : $value;
                $returnArray[$value] = $label;
            }
            if (array_key_exists($attributeValue, $returnArray)) {
                $attributeValue = $returnArray[$attributeValue];
            }

        }
        return $attributeValue;
    }

    private function getAttributeResult($result, $hasStar = false, $forceUtc = false, $isValidation = false): string
    {
        $return = '';
        foreach ($result[0] as $key => $value) {
            if ($key != 'id' && $value !== null) {
                if ($key == 'value_date') {
                    if($forceUtc){
                        return $value;
                    }
                    $date = new \DateTime($value);
                    if($hasStar){
                        return $date->format('Y-m-d H:i:s');
                    }
                    else {
                        $value = $date->format('d-m-Y H:i:s');
                    }
                }
                if($key == 'value_string' && $isValidation === true)
                {
                    $value = str_replace('"', '', $value);
                    $value = str_replace('\'', '', $value);
                }
                $return = (string)$value;
            }
        }
        return $return;
    }

    public function getSchemaRegex(): string
    {
        return '/' . self::PREFIX
            . self::REGEX_VALIDATION . self::SUFFIX . '/';
    }

    public function parseString($string, $countReplaces = false, $emptyApostrophe = false, $decorateParam = null, $forceUTCTimezone = false, $isValidation = false , $escapeStrings = false)
    {
        $count = 0;
        $string = $this->parseTranslationString($string, $count);
        $string = $this->parseSpecialWords($string, $count);
        $string = $this->parseCustomFunction($string, $count, $escapeStrings);
        $params = $this->getStringBetween(self::PREFIX, self::SUFFIX, $string, null);

        foreach ($params as $param) {

            $paramBefore = $param;

            $param = str_replace(' ', '', $param);
            $limit = NULL;
            $parsed = '';

            if (strpos($param, '|') !== false) {

                $optionalValues = explode('|', $param);

                $matchesTrans = preg_grep('/trans|trans(\w*)$/i', $optionalValues);

                if (count($matchesTrans)) {
                    preg_match('/\(([^\]]*)\)/i', $matchesTrans[1], $args);
                    $arg = $args[1] ?? false;
                    $valCopy = $optionalValues;

                    foreach ($matchesTrans as $key => $value) {
                        unset($valCopy[$key]);
                    }
                    $path = implode('', $valCopy);
                    $valueInt = $this->processHandler->getAttributeValue($path, $this->processInstanceId, 'int');

                    $translatorRepository = $this->em->getRepository(AttributeValueTranslator::class);

                    if ($arg) {
                        $attrValueTranslator = $translatorRepository->findOneBy([
                            'scope' => $arg,
                            'attributePath' => $path,
                            'valueInt' => $valueInt
                        ]);
                    } else {
                        $attrValueTranslator = $translatorRepository->findOneBy([
                            'attributePath' => $path,
                            'valueInt' => $valueInt
                        ]);
                    }

                    if ($attrValueTranslator) {
                        $parsed = $attrValueTranslator->getOutput();
                    }
                    else{
                        $parsed = $this->processHandler->translateAttributeValue($path,$this->doParse($path, null,false,false,$isValidation,$escapeStrings));
                    }
                } else {
                    foreach ($optionalValues as $optionalValue) {
                        $parsed = $this->doParse($optionalValue, $limit, $emptyApostrophe, $forceUTCTimezone,$isValidation);
                        // stop if value found already
                        if ($parsed && $parsed !== self::VALUE_NOT_FOUND) {
                            break;
                        }
                    }
                }
            }else{
                preg_match('#\((.*?)\)#', $param, $matches);
                if (!empty($matches)) {
                    $limit = $matches[1];
                    $param = str_replace('(' . $limit . ')', '', $param);
                }
                /** Dodanie prefixu i suffixu do {@123@} - np. dla Overview Case */
                if($decorateParam !== NULL) {
                    if(is_callable($decorateParam)) {
                        $decoratedParam = call_user_func($decorateParam, self::PREFIX  . $paramBefore . self::SUFFIX, $param);
                        $string = str_replace(self::PREFIX . $paramBefore . self::SUFFIX, $decoratedParam, $string);

                    }
                }

            }
            if($parsed==''){
                $parsed = $this->doParse($param, $limit, $emptyApostrophe, $forceUTCTimezone,$isValidation);
            }

            if($escapeStrings){
                $parsed = $this->escapeJsonString($parsed);
            }
            $string = str_replace(self::PREFIX . $paramBefore . self::SUFFIX, $parsed, $string);


            if (!empty($parsed) && $parsed !== '""') {
                $count++;
            }
        }

        $string = $this->parseCode($string);

        if ($countReplaces) {
            return [
                'text' => $string,
                'count' => $count
            ];
        }

        $returnString = ($string === "") ? '""' : $string;

        if ($emptyApostrophe) {
            $returnString = str_replace('""""', '""', $returnString);
        }
        return $returnString;
    }

    /**
     * @param $string
     * @param null $count
     * @return mixed
     */
    private function parseTranslationString($string, &$count = null)
    {
        $prefix = '{-';
        $suffix = '-}';

        preg_match_all('/'.$prefix.'(.*?)'.$suffix.'/i', $string,$matchArray);

        if(count($matchArray)){

            foreach ($matchArray[1] as $translationString) {

                $translatedString =  $this->dynamicTranslationService->getDynamicText($translationString, $this);
                $string = str_replace($prefix.$translationString.$suffix, $translatedString, $string);
                $count++;

            }

        }

        return $string;
    }

    private function chooseGender($param)
    {
        if (strpos($param, '|') !== false) {
            $optionalValues = explode('|', $param);
            foreach ($optionalValues as $optionalValue) {
                $firstName = $this->processHandler->getAttributeValue(
                    $optionalValue,
                    $this->groupProcessInstanceId,
                    'string'
                );
                $firstNameGender = $this->em->getRepository(FirstnameGender::class)->findOneByFirstname($firstName);
                if ($firstNameGender) {
                    return $firstNameGender->isMale();
                }
            }

            return 1;
        }

        return $this->getGender($param);
    }


    /**
     * @param $string
     * @param null $count
     * @return mixed|null
     * @throws \ReflectionException
     */
    public function parseCustomFunction($string, &$count = null, $escape = false) {

        $words = $this->getStringBetween(self::WORD_PREFIX, self::WORD_SUFIX, $string, self::REGEX_CUSTOM_FUNCTION);

        if (!empty($words)) {

            foreach ($words as $word) {

                $parsed = '';

                if(strpos($word, '(') !== false && strpos($word, ')') !== false) {   /** Odpalanie customowych funkcji */

                    $functionName = substr($word, 0, strpos($word, '('));
                    $arguments = $this->parseString(substr($word, strpos($word, '(') + 1, (strrpos($word, ')') - strpos($word, '(') - 1) ));

                    if(method_exists($this->parserMethods, $functionName)) {

                        $args = [];

                        if($this->processInstanceId) {
                            /** jeżeli w metodzie istnieje argument "processInstanceId", to przekazuje obecny processInstanceId */
                            $r = new ReflectionMethod('CaseBundle\Utils\ParserMethods', $functionName);
                            $params = $r->getParameters();
                            foreach ($params as $param) {
                                if($param->getName() === "processInstanceId") {
                                    $args[] = $this->processInstanceId;
                                }
                            }
                        }

                        /** jeżeli w metodzie istnieje argument "groupProcessInstanceId", to przekazuje obecny groupProcessInstanceId */
                        $r = new ReflectionMethod('CaseBundle\Utils\ParserMethods', $functionName);
                        $params = $r->getParameters();
                        foreach ($params as $param) {
                            if($param->getName() === "groupProcessInstanceId") {
                                $args[] = $this->groupProcessInstanceId;
                            }
                        }

                        /** Reszta argumentów */

                        if(!empty($arguments) || $arguments === "0") {
                            $argumentsArr = explode('|', $arguments);
                            $args = array_merge($args, $argumentsArr);
                        }

                        $parsed = $this->parseFunctionFromCache($functionName, $args);

                        // TODO - tu trzeba dopisać, że jak zwróci jakiś magiczny string, to żeby parsowało jako pusty string
                    }

                }

                if($count !== null && !empty($parsed) && $parsed !== '""' ) {
                    $count++;
                }
                if($escape){
                    $result = json_decode($parsed);
                    if(empty($result)) {
                        $parsed = $this->escapeJsonString($parsed);
                    }
                }
//                $parsed = addcslashes($parsed, "'");
                $string = str_replace(self::WORD_PREFIX . $word . self::WORD_SUFIX, $parsed, $string);

            }

        }

        // Sprawdzenie  czy na pewno wszystkie dynamic translation zostały przeparsowane
        if(strpos($string, '{-') !== FALSE) {
            $string = $this->parseTranslationString($string, $count);
        }

        // Sprawdzenie czy na pewno wszystkie specjalne wyrazy zostały przeparsowane
        if(strpos($string, self::WORD_PREFIX) !== FALSE && strpos($string, self::WORD_SUFIX) !== FALSE) {
            $string = $this->parseSpecialWords($string, $count);
        }

        // Sprawdzenie  czy na pewno wszystkie funkcje zostały przeparsowane
        $words = $this->getStringBetween(self::WORD_PREFIX, self::WORD_SUFIX, $string, self::REGEX_CUSTOM_FUNCTION);

        if(count($words) > 0) {
            $string = $this->parseCustomFunction($string, $count);
        }

        if($string == "") return null;

        return $string;

    }

    /**
     * @param $functionName
     * @param $args
     * @return mixed
     */
    private function parseFunctionFromCache($functionName, $args) {

        list($value,$key) = $this->staticCache->getCachedFunction($functionName, $args);

        if($value !== StaticCache::NOT_EXISTS) {
            return $value;
        }

        $value = call_user_func_array(array($this->parserMethods,$functionName), $args);

        $this->staticCache->setCachedFunction($key, $value);

        return $value;

    }

    /**
     * @param $string
     * @param null $count
     * @return mixed
     */
    public function parseSpecialWords($string, &$count = null)
    {
        $words = $this->getStringBetween(self::WORD_PREFIX, self::WORD_SUFIX, $string);

        if (!empty($words)) {

            foreach ($words as $word) {
                $parsed = '';
                $originalWord = $word;
                $word = str_replace(' ', '', $word);

                if (strtolower($word) == self::HELLO) {
                    $parsed = $this->getHello();
                }
                elseif (strpos($word, self::GENDER) !== false) {
                    preg_match('/\(([0-9,]*)(\|([0-9,]*))*\)/', $word, $pathMatches);
                    if (!empty($pathMatches)) {
                        $parsed = $this->chooseGender(trim($pathMatches[0], '()'));
                    }
                }
                elseif (strtolower($word) == self::GROUP_ID) {

                    $parsed = $this->getGroupProcessInstanceId();
                }
                elseif (strtolower($word) == self::CASE_ID) {
                    $parsed = 'A' . substr('0000000' . $this->getRootProcessInstanceId(), -8);
                }
                elseif (strtolower($word) == self::STEP_ID) {
                    if($this->processInstanceId) {
                        if($this->processInstanceId instanceof ProcessInstance) {
                            $parsed = $this->processInstanceId->getStep()->getId();
                        }
                        else {
                            $process = $this->em->getRepository(ProcessInstance::class)->find($this->processInstanceId);
                            $parsed = $process->getStep()->getId();
                        }
                    }
                }
                elseif (strtolower($word) == self::INSTANCE_ID) {
                    if($this->processInstanceId) {
                        $parsed = $this->processInstanceId;
                    }
                }
                elseif (strtolower($word) == self::ROOT_PROCESS_ID) {
                    $parsed = $this->getRootProcessInstanceId();
                } elseif (strtolower($word) == self::PLATFORM_NAME) {
                    $platformId = $this->processHandler->getAttributeValue('253', $this->getRootProcessInstanceId(), 'int');
                    if ($platformId) {
                        $platform = $this->em->getRepository(Platform::class)->find($platformId);
                        if ($platform) {
                            $parsed = $platform->getName();
                        }
                    }
                }
                elseif (strtolower($word) == self::SERVER_HOST) {
                    $parsed = $this->serverHost;
                }
                elseif (strtolower($word) == self::PLATFORM_PHONE) {
                    $platformId = $this->processHandler->getAttributeValue('253', $this->groupProcessInstanceId, 'int');
                    if ($platformId) {
                        $platform = $this->em->getRepository(Platform::class)->find($platformId);
                        if ($platform) {

                            $parsed = (string)$platform->getOfficialLineNumber();
//
//                            $platformNumbers = $platform->getPhoneNumbers();
//                            if ($platformNumbers->count() > 0) {
//                                $parsed = self::PLATFORM_PHONE_PREFIX . (string)$platformNumbers->first()->getNumber();
//                            }
                        }
                    }

                }
                elseif(in_array($word, UserInfoParserService::$paramsMapping)){
                    $parsed = $this->userInfoParser->parse($word);
                }
                elseif (strtolower($word) == self::SPARX_ID) {
                    $parsed = $this->processHandler->getAttributeValue(self::PATH_SPARX_ID, $this->groupProcessInstanceId, 'string');
                    if(empty($parsed)){
                        $parsed = '';
                    }
                }
                else {
                    $parsed = self::WORD_PREFIX . $originalWord . self::WORD_SUFIX;
                }

                if (!empty($parsed) && $parsed !== '""') {
                    $count++;
                }

                $string = str_replace(self::WORD_PREFIX . $originalWord . self::WORD_SUFIX, $parsed, $string);
            }
        }
        return $string;
    }

    public function parseCode($string)
    {
        $codes = $this->getStringBetween(self::CODE_SEPARATOR, self::CODE_SEPARATOR, $string);
        $parsed = '';
        if (!empty($codes)) {
            foreach ($codes as $key => $code) {
                if ($key % 2 == 1) {
                    $originalCode = $code;
                    $lang = new Language();
                    if (!empty($code)) {
                        try {
                            $parsed = $lang->evaluate($code);
                        } catch (\Exception $e) {
                            $parsed = '[' . $e->getMessage() . ']';
                        }
                    }
                    $string = str_replace(self::CODE_SEPARATOR . $originalCode . self::CODE_SEPARATOR, $parsed, $string);
                }
            }
        }
        return $string;
    }

    public function getParamsBetween($string) {
        return $this->getStringBetween(self::PREFIX, self::SUFFIX, $string);
    }

    public function getSpecialWordsBetween($string) {
        return $this->getStringBetween(self::WORD_PREFIX, self::WORD_SUFIX, $string);
    }

    public function getFunctionBetween($string) {
        return $this->getStringBetween(self::WORD_PREFIX, self::WORD_SUFIX, $string, self::REGEX_CUSTOM_FUNCTION);
    }

    protected function getStringBetween($start, $end, $str, $regex = null)
    {
        if ($start === $end) {
            if ($start == self::CODE_SEPARATOR) {
                $arr = explode($start, $str);
                return $arr;
            }

            $string = explode($start, $str, 3);
            return isset($string[1]) ? $string[1] : '';
        }

        $matches = [];
        $regex = ($regex === null) ? "/$start([sparxid|companyid|groupid|stepid|gender|hello|caseid|rootid|platformphone|platformname|%^0-9,|()*\- ]*)$end/i" : $regex;

        preg_match_all($regex, $str, $matches);

        return $matches[1];
    }

    public function getCaseInfo(ProcessInstance $instance)
    {
        $infoRows = 0;
        $groupProcess = $instance->getGroupProcess();
        $groupProcessInstanceId = $groupProcess->getId();
        $info = $groupProcess->getStep()->getProcessDefinition()->getInfo();
        if (!$info) {
            return false;
        }

        $this->setGroupProcessInstanceId($groupProcessInstanceId);
        $lines = explode(PHP_EOL, $info);
        $output = '';

        $specialSituation = $this->processHandler->getAttributeValue(OperationDashboardInterface::SPECIAL_SITUATION, $groupProcess->getId(), 'int');

        switch ($specialSituation) {
            case '1' : {
                $icon = 'fa-car';
                break;
            }
            case '2' :
            case '3' : {
                $icon = 'fa-child';
                break;
            }
            case '4' : {
                $icon = 'fa-clock-o';
                break;
            }
            case '5' : {
                $icon = 'fa-exclamation';
                break;
            }
            case '7' : {
                $icon = 'fa-star font-yellow-crusta';
                break;
            }
            default : {
                $icon = false;
            }
                $vip = $this->processHandler->getAttributeValue(self::VIP_ATTR_ID, $groupProcess->getId(), 'string');


            if(!is_null($vip)&& !is_array($vip)){
                $specialSituation = '7';
                $icon = 'fa-star font-yellow-crusta';//                Todo:różne ikonki w zależności od poziomu ?
            }
        }

        $extraRows = $this->processHandler->getExtraTextForTop5($groupProcessInstanceId);
        $caseInfo = $this->processHandler->getCaseSpecialInstruction($groupProcessInstanceId);

        if(is_array($extraRows)) {
            foreach ($extraRows as $extraRow) {
                $output .= '<p class="' . $extraRow['class'] . '"><strong>' . $extraRow['label'] . '</strong>' . $extraRow['value'] . '</p>';
            }
        }

        foreach ($lines as $row => $line) {

            if ($icon && $row == 0) {
                $output .= '<i data-id-of-special-situation="' . $specialSituation . '" class="caseIcon fa ' . $icon . ' fa-2x"></i>';
            }

            if($row == 0 && $caseInfo){
                $output .= '<p class="font-red bold"><span class="badge badge-danger">'.$caseInfo['category'].'</span> '.$caseInfo['text'].'</p>';
            }

            $parsed = str_replace('&nbsp;',' ', $this->parseString($line, true));

            $isEmpty = (empty(strip_tags(str_replace(["\n", "\r", "\t", " "], "", $parsed['text']))));

            if ($parsed['count'] > 0 && !$isEmpty) {
                if ($infoRows == 5) {
                    $output .= '<div class="more-info"></div><div class="more-info-container">';
                }
                $infoRows++;
                $output .= $parsed['text'];
            }

            if ($infoRows > 5 && end($lines) === $line) {
                $output .= '</div>';
            }
        }

        return $output;
    }

    public function getAdditionalCaseInfo(ProcessInstance $instance)
    {
        $platform = null;
        $program = null;

        $groupProcess = $instance->getGroupProcess();
        $locale = $this->translator->getLocale();
        $singleStep  = $instance->getStep();

        $platformId = $this->processHandler->getAttributeValue('253', $groupProcess->getId(), 'int');
        if($platformId) {
            $platform = $this->em->getRepository('CaseBundle:Platform')->find($platformId);
        }

        $programID = $this->processHandler->getAttributeValue('202', $groupProcess->getId(),'string');
        if($programID){
            $program = $this->em->getRepository('CaseBundle:Program')->find($programID);
        }

        $stepInfoLocator = new StepInfoLocator($singleStep,$platform,$program);
        $info= $this->em->getRepository('CaseBundle:StepInfo')->getByStepInfoLocator($stepInfoLocator, $locale);

        if(!empty($info)){
            $info = $info['text'];
        } else{
            $info = $groupProcess->getStep()->getProcessDefinition()->getAdditionalInfo();
        }

        $customInfo = $this->processHandler->stepDescription($instance->getId(),0);
        if(!empty($customInfo)){
            $info = '<p>'.$customInfo.'</p>'.$info;
        }
        else if (!$info) {
            return false;
        }

        $this->setGroupProcessInstanceId($groupProcess->getId());
        $parsed = $this->parseString($info);
        if ($parsed == '') {
            return false;
        }

        return $parsed;
    }

    public function getOverview($rootProcessId, $dynamicEditAttributes = true)
    {
        $info = [];
        /** @var ProcessInstance $root */
        $root = $this->em->getRepository(ProcessInstance::class)->find($rootProcessId);
        $this->setGroupProcessInstanceId($rootProcessId);
        $info[] = $this->prepareProcessOverview($root, $dynamicEditAttributes);
        $caseId = $this->processHandler->getAttributeValue('235', $root->getId(), 'string');
        $caseId = !empty($caseId) ? $caseId[0] : 'A' . substr('00000000' . $root->getId(), -8);

        /** @var ProcessInstance $childProcess */
//        foreach ($root->getRootChildren() as $childProcess) {
//            if (!array_key_exists($childProcess->getGroupProcess()->getId(), $info)) {
//                $processOverview = $this->prepareProcessOverview($childProcess, $dynamicEditAttributes);
//                if ($processOverview) {
//                    $info[$childProcess->getGroupProcess()->getId()] = $processOverview;
//                }
//            }
//        }
        return $this->getOverviewHtml($info, $caseId);
    }

    public function getInfoForCaseViewer($rootProcessId, $dynamicEditAttributes = true)
    {
        $info = [];
        /** @var ProcessInstance $root */
        $root = $this->em->getRepository(ProcessInstance::class)->find($rootProcessId);
        $this->setGroupProcessInstanceId($rootProcessId);
        $info[] = $this->prepareInfoForCaseViewer($root, $dynamicEditAttributes);
        $caseId = $this->processHandler->getAttributeValue('235', $root->getId(), 'string');
        $caseId = !empty($caseId) ? $caseId[0] : 'A' . substr('00000000' . $root->getId(), -8);

        return $this->getOverviewHtml($info, $caseId);
    }



    protected function prepareInfoForCaseViewer(ProcessInstance $process, $dynamicEditAttributes = true)
    {
        $this->setGroupProcessInstanceId($process->getGroupProcess()->getId());
        $definitionId = $process->getDefinitionId();
        /** @var ProcessDefinition $definition */
        $definition = $this->em->getReference(ProcessDefinition::class, $definitionId);

        /** Diagnosis */
        if ($definitionId == 1026) {
            return false;
        } else {

            if($dynamicEditAttributes) {
                $parsedInfo = $this->parseString($definition->getViewerInfo(), false, false, function($prefixSuffixParam, $path) {
                    $path = str_replace('*', '', $path);
                    return '<span data-dynamic-edit-attr="'.$path.'"><span class="attr-value">'.$prefixSuffixParam.'</span></span>';
                });
            }
            else {
                $parsedInfo = $this->parseString($definition->getViewerInfo(), false, false);
            }

            return [
                'id' => $process->getId(),
                'groupProcessId' => $this->getGroupProcessInstanceId(),
                'definitionName' => $definition->getName(),
                'createdAt' => $process->getCreatedAt(),
//                'parsedInfo' => $this->parseString($definition->getFullInfo())
                'parsedInfo' => $parsedInfo
            ];
        }
    }


    protected function prepareProcessOverview(ProcessInstance $process, $dynamicEditAttributes = true)
    {
        $this->setGroupProcessInstanceId($process->getGroupProcess()->getId());
        $definitionId = $process->getDefinitionId();
        /** @var ProcessDefinition $definition */
        $definition = $this->em->getReference(ProcessDefinition::class, $definitionId);

        /** Diagnosis */
        if ($definitionId == 1026) {
            return false;
        } else {

            if($dynamicEditAttributes) {
                $parsedInfo = $this->parseString($definition->getFullInfo(), false, false, function($prefixSuffixParam, $path) {
                    $path = str_replace('*', '', $path);
                    return '<span data-dynamic-edit-attr="'.$path.'"><span class="attr-value">'.$prefixSuffixParam.'</span></span>';
                });
            }
            else {
                $parsedInfo = $this->parseString($definition->getFullInfo(), false, false);
            }

            return [
                'id' => $process->getId(),
                'groupProcessId' => $this->getGroupProcessInstanceId(),
                'definitionName' => $definition->getName(),
                'createdAt' => $process->getCreatedAt(),
//                'parsedInfo' => $this->parseString($definition->getFullInfo())
                'parsedInfo' => $parsedInfo
            ];
        }
    }

    protected function getOverViewHtml($info, $caseId)
    {
        return $this->twig->render("@Operational/Default/case-overview.html.twig", [
                'caseId' => $caseId,
                'processes' => $info
            ]
        );
    }

    public function getGender($path)
    {
        $firstName = $this->processHandler->getAttributeValue($path, $this->groupProcessInstanceId, 'string');
        $firstNameGender = $this->em->getRepository(FirstnameGender::class)->findOneByFirstname($firstName);
        if ($firstNameGender) {
            return $firstNameGender->isMale();
        }
        return 1;
    }

    public function getHello()
    {
        $now = new \DateTime();
        $hour = (int)$now->format('H');

        if ($hour >= 0 && $hour < 12) {
            return ucfirst($this->translator->trans('good.morning'));
        } elseif ($hour >= 12 && $hour < 19) {
            return ucfirst($this->translator->trans('good.afternoon'));
        } else {
            return ucfirst($this->translator->trans('good.evening'));
        }
    }

    private function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}
}