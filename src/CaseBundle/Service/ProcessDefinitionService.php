<?php

namespace CaseBundle\Service;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\StepAttributeDefinition;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ProcessDefinitionService
{

    public function __construct(EntityManagerInterface $em, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function attachAttributesToStep($step, $attributes)
    {
        $response = [];

        foreach ($attributes as $attributeData) {
            $attribute = $this->em->getReference(Attribute::class, $attributeData['id']);
            if ($attribute) {
                $parent = $attributeData['rootId'] ? $this->em->getRepository(Attribute::class)->find(
                    $attributeData['rootId']
                ) : null;
                $parentName = $attributeData['rootName'] ? ' ('.$attributeData['rootName'].')' : '';
                $stepAttribute = $this->em->getRepository(StepAttributeDefinition::class)->findOneBy(
                    [
                        'step' => $step,
                        'attribute' => $attribute,
                        'parentAttribute' => $parent,
                    ]
                );
                if (!$stepAttribute) {
                    $stepAttribute = new StepAttributeDefinition();
                    $stepAttribute->setStep($step);
                    $stepAttribute->setAttribute($attribute);
                    if ($parent) {
                        $stepAttribute->setParentAttribute($parent);
                    }
                    $this->em->persist($stepAttribute);
                    $this->em->flush();

                    $response[] = [
                        'id' => $stepAttribute->getId(),
                        'name' => $stepAttribute->getAttribute()->getName(),
                        'parentName' => $parentName,
                        'attributeId' => $stepAttribute->getAttribute()->getId()
                    ];
                } else {
                    if ($stepAttribute->getDeletedAt() !== null) {
                        $stepAttribute->setDeletedAt(null);
                        $this->em->persist($stepAttribute);
                        $this->em->flush();


                        $response[] = [
                            'id' => $stepAttribute->getId(),
                            'name' => $stepAttribute->getAttribute()->getName(),
                            'parentName' => $parentName,
                            'attributeId' => $stepAttribute->getAttribute()->getId()
                        ];
                    }
                }
            }
        }
        return $response;

    }
}