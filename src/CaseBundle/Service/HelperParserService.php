<?php

namespace CaseBundle\Service;

use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessFlow;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Utils\Language;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use ManagementBundle\Entity\DevLog;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HelperParserService
{

    /** @var ContainerInterface  */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $string
     * @param null $processInstanceId
     * @return HelperParserResult
     */
    public function parseString($string, $processInstanceId = null, $isValidation = false) {

        return $this->parse($string, $processInstanceId, $isValidation);

    }

    /**
     * @param $string
     * @param $processInstanceId
     * @return HelperParserResult
     */
    private function parse($string, $processInstanceId = null, $isValidation = false) {

        $errorMessage = null;
        $parsedValue = "";
        $evaluateErrorMessage = null;
        $evaluateValue = "";
        $compiled = "";

        try {

            /** @var AttributeParserService $attributeParser */
            $attributeParser = $this->container->get('case.attribute_parser.service');

            if(empty($processInstanceId)) $processInstanceId = 1;

            /** @var EntityManager $em */
            $em = $this->container->get('doctrine.orm.entity_manager');

            $processInstance = $em->getRepository(ProcessInstance::class)->find(intval($processInstanceId));

            if($processInstance) {
                $attributeParser->setGroupProcessInstanceId($processInstance->getGroupProcess()->getId());
                $attributeParser->setProcessInstanceId($processInstanceId);
            }

            $parsedValue = $attributeParser->parseString($string, false, false,null,false ,$isValidation);


            try {

                $lang = new Language();

                $result = $lang->evaluate($parsedValue);
                $compiled = $lang->compile($parsedValue);

                if($result === true || $result === false) {
                    $evaluateValue = (($result) ? "true" : "false");
                }
                else {
                    $evaluateValue = $result;
                }

            }
            catch (\Exception $exception) {
                $evaluateErrorMessage = $exception->getMessage();
            }

        }
        catch (\Exception $exception) {
            $errorMessage = $exception->getMessage();

        }

        return new HelperParserResult($errorMessage, $parsedValue, $evaluateErrorMessage, $evaluateValue, $compiled);

    }

}

class HelperParserResult {

    public $errorMessage;
    public $parsedValue;
    public $evaluateErrorMessage;
    public $evaluateValue;
    public $compiled;

    /**
     *  constructor.
     * @param $errorMessage
     * @param $parsedValue
     * @param $evaluateErrorMessage
     * @param $evaluateValue
     * @param string $compiled
     */
    public function __construct($errorMessage, $parsedValue, $evaluateErrorMessage, $evaluateValue, $compiled = "")
    {
        $this->errorMessage = $errorMessage;
        $this->parsedValue = $parsedValue;
        $this->evaluateErrorMessage = $evaluateErrorMessage;
        $this->evaluateValue = $evaluateValue;
        $this->compiled = $compiled;
    }

    public function isParsedError() {
        return (!empty($this->errorMessage));
    }

    public function isEvaluateError() {
        return (!empty($this->evaluateErrorMessage));
    }

}