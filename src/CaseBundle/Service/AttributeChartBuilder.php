<?php

namespace CaseBundle\Service;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessFlow;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AttributeChartBuilder
{

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function merge($model, $attribute)
    {

        $modelNodeArray = [];
        $modelLinkArray = [];

        $attributeModel = $this->build($attribute, true);

        foreach ($model->nodeDataArray as $modelNode) {
            $modelNodeArray[$modelNode->id] = (array)$modelNode;
        }

        foreach ($model->linkDataArray as $modelLink) {
            $modelLinkArray[$modelLink->from.'-'.$modelLink->to] = (array)$modelLink;
        }

        $model->nodeDataArray = array_values($modelNodeArray + $attributeModel['nodeDataArray']);
        $model->linkDataArray = array_values($modelLinkArray + $attributeModel['linkDataArray']);

        return $model;
    }


    /**
     * @param null $attribute
     * @return string
     */
    public function build($attribute = null, $singleNode = false)
    {
        $data = $this->prepareData($attribute, $singleNode);
        if ($singleNode) {
            return $data;
        }

        return json_encode($data);
    }

    /**
     * @param $attribute Attribute
     * @param $singleNode
     * @return array
     */
    protected function prepareData($attribute, $singleNode)
    {
        $data = [
            'nodeDataArray' => [],
            'linkDataArray' => [],
        ];

        if (!$singleNode) {
            $data['nodeKeyProperty'] = 'id';
        }
        if ($attribute) {
            $data = $this->crawlAttributesForData($attribute, $data, true);
            if (!$singleNode) {
                $data['nodeDataArray'] = array_values($data['nodeDataArray']);
                $data['linkDataArray'] = array_values($data['linkDataArray']);
            }
        }

        return $data;
    }

    protected function crawlAttributesForData(Attribute $attribute, $data, $root = false)
    {

        if ($root) {
            $data['nodeDataArray'][$attribute->getId()] = [
                'id' => $attribute->getId(),
                'name' => $attribute->getDefinitionName(),
            ];
        }

        foreach ($attribute->getChildren() as $child) {
            $data['nodeDataArray'][$child->getId()] = [
                'id' => $child->getId(),
                'name' => $child->getDefinitionName(),
            ];

            $data['linkDataArray'][$attribute->getId().'-'.$child->getId()] = [
                'from' => $attribute->getId(),
                'to' => $child->getId(),
            ];

            $data = $this->crawlAttributesForData($child, $data);
        }

        return $data;
    }

    /**
     * @param $data \stdClass
     * @param $processDefinition ProcessDefinition
     */
    public function save($data)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $attributeRepository = $em->getRepository('CaseBundle:Attribute');

        foreach ($data->nodeDataArray as $nodeData) {

            $attribute = $attributeRepository->find($nodeData->id);
            if ($attribute) {
                $attribute->getChildren()->clear();
                $em->persist($attribute);
            }
        }

        foreach ($data->linkDataArray as $linkData) {
            /** @var Attribute $parent */
            $parent = $em->getReference(Attribute::class, $linkData->from);
            $child = $em->getReference(Attribute::class, $linkData->to);

            $parent->addChild($child);
            $em->persist($parent);

        }

        $em->flush();
    }

    protected function prepareFlows($flows)
    {
        $flowArray = [];

        /** @var ProcessFlow $flow */
        foreach ($flows as $key => $flow) {
            if ($flow->getStep() && $flow->getNextStep()) {
                $flowArray[$key] = [
                    'id' => $flow->getId(),
                    'from' => $flow->getStep()->getGraphId(),
                    'to' => $flow->getNextStep()->getGraphId(),
                    'variant' => $flow->getVariant(),
                    'isPrimaryPath' => $flow->getIsPrimaryPath(),
                    'description' => $flow->getDescription(),
                ];
                if ($flow->getGraphPoints()) {
                    $flowArray[$key]['points'] = json_decode($flow->getGraphPoints());
                }
                if ($flow->getCurviness()) {
                    $flowArray[$key]['curviness'] = $flow->getCurviness();
                }
            }
        }

        return $flowArray;
    }
}