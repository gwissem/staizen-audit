<?php

namespace CaseBundle\Service;

use AppBundle\Entity\Job;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\Step;
use CaseBundle\Utils\ParserMethods;
use Doctrine\ORM\EntityManager;
use Gedmo\Translator\Translation;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use MailboxBundle\Entity\ConnectedMailbox;
use MailboxBundle\Service\SimpleOutlookSenderService;
use OperationalBundle\Service\ApiTaskService;
use PDO;
use SmsBundle\Service\SmsHandler;
use SocketBundle\Topic\AtlasTopic;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\Translator;
use Twig_Environment;
use UserBundle\Entity\User;
use UserBundle\Service\UserInfoService;


/**
 *
 * Class NoteHandler
 * @package NoteHandler\Service
 */
class NoteHandler
{

    const NOTE_ID = 226;
    const NOTE_SMS_TYPE = 'sms';
    const NOTE_EMAIL_TYPE = 'email';

    // TODO - fajnie by było przenieść CONST o notatkach z CaseFormGenerator tutaj

    /** @var ContainerInterface $container */
    protected $entityManager;

    protected $twig;

    /**
     * @var ProcessHandler
     */
    protected $processHandler;

    protected $formTemplateGenerator;

    /** @var QueryManager  */
    private $queryManager;

    private $rootId;

    /** @var AttributeParserService  */
    private $attributeParser;

    /** @var UserInfoService */
    private $userInfo;

    private $smsHandler;

    /** @var  AuthorizationChecker */
    private $authChecker;

    /** @var Translator */
    private $translator;

    private $atlasEnvironment;

    /** @var Router $router */
    private $router;

    /** @var PusherInterface  */
    private $pusher;

    /** @var ApiTaskService  */
    private $apiTaskService;

    public function __construct(EntityManager $entityManager,
                                Twig_Environment $twig,
                                ProcessHandler $processHandler,
                                FormTemplateGenerator $formTemplateGenerator,
                                QueryManager $queryManager,
                                AttributeParserService $attributeParserService,
                                UserInfoService $userInfo,
                                SmsHandler $smsHandler,
                                AuthorizationChecker $authorizationChecker,
                                Router $router,
                                $atlasEnvironment,
                                Translator $translator,
                                PusherInterface $pusherDecorator,
                                ApiTaskService $apiTaskService
    )
    {
        $this->entityManager = $entityManager;
        $this->twig = $twig;
        $this->processHandler = $processHandler;
        $this->formTemplateGenerator = $formTemplateGenerator;
        $this->queryManager = $queryManager;
        $this->attributeParser = $attributeParserService;
        $this->userInfo = $userInfo;
        $this->smsHandler = $smsHandler;
        $this->authChecker = $authorizationChecker;
        $this->atlasEnvironment = $atlasEnvironment;
        $this->router = $router;
        $this->translator = $translator;
        $this->pusher = $pusherDecorator;
        $this->apiTaskService = $apiTaskService;
    }

    /**
     * @param $string
     * @return string
     */
    private function trans($string) {
        /** @Ignore */
        return $this->translator->trans($string);
    }

    /**
     * @param $rootNote
     * @param $to
     * @param $from
     * @param $subject
     * @param $body
     * @param null $cc
     * @param null $bcc
     * @return array|mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function newEmail($rootNote, $to, $from, $subject, $body, $cc = null, $bcc = null) {

        if(!$rootNote instanceof AttributeValue) {
            $rootNote = $this->entityManager->getRepository('CaseBundle:AttributeValue')->find(intval($rootNote));
        }

        $data = [
            'content' => $body,
            'type' => 'email',
            'email' => $to,
            'sender' => $from,
            'subject' => $subject
        ];

        return $this->newNote($data, $rootNote);

    }

    /**
     * @param $data
     * @param $rootNote
     * @param null $groupProcessId
     * @return array|mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function newNote($data, $rootNote, $groupProcessId = null) {


        if(!$rootNote instanceof AttributeValue) {
            $rootNote = $this->entityManager->getRepository('CaseBundle:AttributeValue')->find(intval($rootNote));
        }

        if(empty(intval($groupProcessId))) {
            $groupProcessId = $rootNote->getGroupProcessInstance()->getId();
        }

        $resolver = new OptionsResolver();

        $resolver->setDefaults(array(
            'number' => NULL,
            'email' => NULL,
            'subject' => NULL,
            'sender' => NULL,
            'type' => 'text',
            'content' => "Empty",
            'direction' => 1,
            'undefinedNumber' => false,
            'special' =>0,
            'extraData' => NULL,
        ));

        $noteData = $resolver->resolve($data);

        $structure = $this->createNoteStructure($rootNote);

        $this->attributeParser->setGroupProcessInstanceId($groupProcessId);
        $parsedContent = $this->attributeParser->parseString($noteData['content']);

        $parsedContent = nl2br($parsedContent);
        $noteId = $structure[CaseFormGenerator::NOTE_CONTENT]['parent_attribute_value_id'];

        $userId = $this->userInfo->getUser()->getId();

        /** Typ notatki */
        $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_TYPE => $structure[CaseFormGenerator::NOTE_TYPE]['id']], $noteData['type'], AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

        /** Kierunek */
        $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_DIRECTION => $structure[CaseFormGenerator::NOTE_DIRECTION]['id']], $noteData['direction'], AttributeValue::VALUE_INT, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

        /** Zabezpieczenie wysyłki maila na prawdziwy adres w przypadku notatki na devie  */
        if (isset($noteData['email'])) {
            if ($this->atlasEnvironment !== 'prod'
                &&
                (
                    strpos($noteData['email'], '@starter24.pl') === false
                    &&
                    strpos($noteData['email'], '@sigmeo.pl') === false)
                && (isset($noteData['email']) && $noteData['email'] !=='')
            ) {
                if($noteData['email'] !== ''){
                    $noteData['content'] = 'W wersji produkcyjnej email zostałby wysłany na adres: '.$noteData['email'].'<br/>'.$noteData['content'];
                }
                $noteData['email'] = 'atlas_test@starter24.pl';
            }
        }

        if($noteData['type'] === self::NOTE_EMAIL_TYPE) {

            $body = str_replace('="../','="'.$this->router->getContext()->getBaseUrl().'/',$parsedContent); // tinymce forward e-mail bug
            $parsedContent = strip_tags($parsedContent);
            $parsedContent = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $parsedContent);
            $shortContent = (mb_strlen($parsedContent) > 200) ? (mb_substr($parsedContent, 0, 200) . '...') : $parsedContent;

            /** Tekst Notatki */
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_CONTENT => $structure[CaseFormGenerator::NOTE_CONTENT]['id']], $shortContent, AttributeValue::VALUE_TEXT, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

            /** Body E-maila */
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_EMAIL_CONTENT => $structure[CaseFormGenerator::NOTE_EMAIL_CONTENT]['id']], $body, AttributeValue::VALUE_TEXT, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

            /** Ustawienie statusu pending */
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_STATUS => $structure[CaseFormGenerator::NOTE_STATUS]['id']], 'pending', AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

            /** Subject */
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_SUBJECT => $structure[CaseFormGenerator::NOTE_SUBJECT]['id']], $noteData['subject'], AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

            /** Odbiorca - email */
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_EMAIL => $structure[CaseFormGenerator::NOTE_EMAIL]['id']], $noteData['email'], AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

            /** sender - email */
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_EMAIL_SENDER => $structure[CaseFormGenerator::NOTE_EMAIL_SENDER]['id']], $noteData['sender'], AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

        }
        elseif($noteData['type'] === CaseFormGenerator::NOTE_TYPE_CHAT) {

            /** Odbiorca - email */
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_EMAIL => $structure[CaseFormGenerator::NOTE_EMAIL]['id']], $noteData['email'], AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

            /** Tekst Notatki */
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_CONTENT => $structure[CaseFormGenerator::NOTE_CONTENT]['id']], $parsedContent, AttributeValue::VALUE_TEXT, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

        }
        else {
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_CONTENT => $structure[CaseFormGenerator::NOTE_CONTENT]['id']], $parsedContent, AttributeValue::VALUE_TEXT, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);
        }


        if(isset($structure[CaseFormGenerator::NOTE_SPECIAL])){

            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_SPECIAL => $structure[CaseFormGenerator::NOTE_SPECIAL]['id']], $noteData['special'], AttributeValue::VALUE_INT, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);

        }
        else {
            $this->processHandler->editAttributeValueNew(
                CaseFormGenerator::NOTE_SPECIAL,
                $noteData['special'],
                AttributeValue::VALUE_INT,
                Step::ALWAYS_ACTIVE,
                $groupProcessId,
                $noteId,
                $userId,
                $userId
            );
        }

        if ($noteData['number'] !== NULL) {
            $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_PHONE => $structure[CaseFormGenerator::NOTE_PHONE]['id']], $noteData['number'], AttributeValue::VALUE_STRING, Step::ALWAYS_ACTIVE, $groupProcessId, $userId, $userId);
        }

        if ($noteData['type'] === self::NOTE_SMS_TYPE) {

            $this->smsHandler->sendSms($noteId, $groupProcessId);

        }
        elseif($noteData['type'] === self::NOTE_EMAIL_TYPE) {

            $this->addEmailToJob($noteId);

        }
        elseif($noteData['type'] === CaseFormGenerator::NOTE_TYPE_CHAT) {


            /** Dodawana jest wiadomość przez ST24 do pracowników Viewera */
            if(!empty($noteData['email'] && $noteData['direction'] !=2)) {
                $this->sendNotificationForChatNote($rootNote, $noteData['email'], $parsedContent);
            }
            else {
                /** Stawiamy zadanie, bo dodawana jest wiadomość przez pracowników Viewera do St24 */
                $this->pushNewNoteNotificationToUsers($rootNote->getGroupProcessInstance()->getId());
                $this->processHandler->createTaskFromNote($rootNote->getRootProcess()->getId(), $noteId, [], $userId);
            }
        }


        return $this->getNoteStructure($noteId);

    }

    /**
     * @param AttributeValue $rootNote
     * @param $to
     * @param string $content
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function sendNotificationForChatNote(AttributeValue $rootNote, $to, $content = '') {

        /** @var integer $rootId */
        $rootId = $rootNote->getRootProcess()->getId();

        $platformId = $this->processHandler->getAttributeValue('253', $rootId, 'int');

        $isPZU = in_array($platformId, ['87', '88'], true);

        $registerNumber = $this->processHandler->getAttributeValue('74,72', $rootId, 'string');

        $url = $this->router->generate('ccv_index', [
            '_fr' => '',
            'case-number' => $rootId
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $from = ($this->atlasEnvironment === 'prod') ? 'atlas-noreply@starter24.pl' : 'atlas_test@starter24.pl';

        $body = $this->twig->render('@App/Email/notification_chat_note.html.twig', [
            'url' => $url,
            'registerNumber' => $registerNumber,
            'case_number' => ParserMethods::formattingCaseNumber($rootId),
            'isPZU' => $isPZU,
            'to' => ($to !== 'zagranica.assistance@pzu.pl') ? $to : ''
        ]);

        // Przy PZU, e-mail jest kierowany zawsze na jedną skrzynkę
        if($isPZU) {
            $to = 'zagranica.assistance@pzu.pl';
        }

        $title = 'Otrzymano nową wiadomość od Starter24 w aplikacji Atlas Viewer - ' . ParserMethods::formattingCaseNumber($rootId) . ' / ' . $registerNumber;

        if(($this->atlasEnvironment !== 'prod')) {
            $to = 'atlas_test@starter24.pl';
        }

        $_data = [
            'content' => $body,
            'type' => 'email',
            'email' => $to,
            'sender' => $from,
            'subject' => $title
        ];

        $this->newNote($_data, $rootNote);

        $user = $this->userInfo->getUser();
        $userId = ($user instanceof User) ? $user->getId() : null;

        $this->processHandler->simpleAddCaseLog('send_notification_about_chat_note',
            $rootId,
            'Send to: ' . $to .'. ' . $content,
            $userId);

    }

    private function pushNewNoteNotificationToUsers(int $rootId)
    {
        $activeUsersInCase = $this->apiTaskService->getActiveUsersInCase($rootId);

        $activeUsersInCaseIds = [];
        if(false === empty($activeUsersInCase)) {
            foreach ($activeUsersInCase as $userInfo) {
                $activeUsersInCaseIds[] = intval($userInfo['user_id']);
            }
        }

        $data = [
            'data' => [
                'title' =>  "W sprawie pojawiła się nowa wiadomość chat",
                'timeout' => 60000,
                'withIcon' => true,
                'type' => 'info',
                'users' => $activeUsersInCaseIds,
                'refresh' => 'note',
                'rootId' => $rootId
            ]
        ];

        $this->sendPushNotification($data, 'atlas_main_topic');
    }

    private function sendPushNotification(array $data, string $routeName)
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'action' => AtlasTopic::ACTION_NOTIFICATION,
            'data' => [
                'title' => '',
                'tapToDismiss' => false,
                'message' => '',
                'timeout' => 30000,
                'withIcon' => false,
                'type' => '',
                'users' => [],
                'extraClass' => '',
                'forAll' => false,
                'closeButton' => true,
                'refresh' => '',
                'rootId' => ''
            ]
        ]);

        $pushData = $resolver->resolve($data);

        $this->pusher->push($pushData, $routeName);
    }

    /**
     * @param $emailNoteId
     * @return mixed
     */
    public function copyNote($emailNoteId) {

        /** @var AttributeValue $emailNote */
        $emailNote = $this->entityManager->getRepository('CaseBundle:AttributeValue')->find(intval($emailNoteId));

        /** @var AttributeValue $rootNote */
        $rootNote = $emailNote->getParentAttributeValue();

        $structure = $this->createNoteStructure($rootNote);
        $newNoteId = $structure[CaseFormGenerator::NOTE_CONTENT]['parent_attribute_value_id'];

        return [$newNoteId, $structure];

    }


    public function updatePhoneNote(AttributeValue $note, $content, $direction) {

        $user = $this->userInfo->getUser();
        $userOriginal = $this->userInfo->getOriginalUser();
        $groupProcessId = $note->getGroupProcessInstance()->getId();
        $attributes = $this->processHandler->getAttributeStructureValue($note->getId());

        $attributesArray = [];

        foreach ($attributes as $item) {
            $attributesArray[$item['attribute_path']] = $item;
        }

        $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_CONTENT => $attributesArray[CaseFormGenerator::NOTE_CONTENT]['id']], $content, AttributeValue::VALUE_TEXT, Step::ALWAYS_ACTIVE, $groupProcessId, $user->getId(), $userOriginal->getId());
        $this->processHandler->setAttributeValue([CaseFormGenerator::NOTE_DIRECTION => $attributesArray[CaseFormGenerator::NOTE_DIRECTION]['id']], $direction, AttributeValue::VALUE_INT, Step::ALWAYS_ACTIVE, $groupProcessId, $user->getId(), $userOriginal->getId());

        return $this->getNoteStructure($note);

    }

    /**
     * @param $rootNote
     * @return array
     */
    public function createNoteStructure($rootNote) {

        if(!$rootNote instanceof AttributeValue) {
            $rootNote = $this->entityManager->getRepository('CaseBundle:AttributeValue')->find(intval($rootNote));
        }

//        $instanceId = $this->processHandler->getActiveInstanceForRoot($rootNote->getRootProcess()->getId());

        $emptyNoteId = false;
        $structure = [];
        $data = [];

//        if ($instanceId) {

        /** TODO - FAJNIE BY BYŁO ZROBIĆ PROCEDURE, ŻEBY SPRAWDZIŁA PUSTĄ NOTATKE */

        // zakomentowałem bo najprawdopobonie ta część nadpisywałą notatki - do sprawdzenia

//            $formControls = $this->processHandler->formControls($instanceId);
//
//            foreach ($formControls as $formControl) {
//                if ($formControl['attribute_path'] == CaseFormGenerator::NOTE_CONTENT && empty($formControl['value_text'])) {
//                    $emptyNoteId = $formControl['parent_attribute_value_id'];
//                    break;
//                }
//            }
//
//            if ($emptyNoteId) {
//                foreach ($formControls as $formControl) {
//                    if ($formControl['parent_attribute_value_id'] == $emptyNoteId) {
//                        $formControl['id'] = $formControl['attribute_value_id'];
//                        $structure[] = $formControl;
//                    }
//                }
//            }
//
//        }

        if (empty($structure)) {
            $structure = $this->processHandler->attributeAddStructure($rootNote->getId(), self::NOTE_ID);
        }

        foreach ($structure as $value) {
            $data[$value['attribute_path']] = $value;
        }

        return $data;

    }

    public function copyNoteData($sourceNoteId, $targetNoteId, $rootId, $userId = NULL) {

        $sourceNote = $this->getMappedNote($sourceNoteId);
        $targetNote = $this->getMappedNote($targetNoteId);

        foreach ($sourceNote as $item) {

            if($item['parent_attribute_value_id'] == $sourceNoteId) {

                $path = $item['attribute_path'];
                $value = NULL;
                $type = NULL;

                foreach ($item as $key => $val) {
                    if(strpos($key, 'value_') === 0 && $val !== NULL ) {
                        $value = $val;
                        $type = str_replace('value_', '', $key);
                    }
                }

                if($value === NULL) continue;

                $this->processHandler->editAttributeValueNew(
                    [$path => $targetNote[$path]['id']], $value, $type, Step::ALWAYS_ACTIVE, $rootId, $userId, $userId);

            }

        }

        return $targetNote;

    }

    public function getMappedNote($noteId) {

        $attributesArray = [];

        foreach ( $this->processHandler->getAttributeStructureValue($noteId) as $item) {
            $attributesArray[$item['attribute_path']] = $item;
        }

        return $attributesArray;

    }

    /**
     * @param $rootNote
     * @return array|mixed
     */
    public function getNoteStructure($rootNote) {

        if(!$rootNote instanceof AttributeValue) {
            $rootNote = $this->entityManager->getRepository('CaseBundle:AttributeValue')->find(intval($rootNote));
        }

        $attributes = $this->processHandler->getAttributeStructureValue($rootNote->getId());

        $attributesArray = [];

        foreach ($attributes as $item) {
            $attributesArray[$item['attribute_path']] = $item;
        }

        $noteStructure = $this->prepareNoteList([$attributesArray], $rootNote->getRootProcess()->getId() );

        if(empty($noteStructure)) {
            return [];
        }

        $noteStructure[0]['id'] = $rootNote->getId();

        return $noteStructure[0];

    }


    /**
     * @param $rootNote
     * @return array|bool|mixed
     */
    public function getEmailContent($rootNote) {

        if(!$rootNote instanceof AttributeValue) {
            $rootNote = $this->entityManager->getRepository('CaseBundle:AttributeValue')->find(intval($rootNote));
        }

        return $this->processHandler->getAttributeValue(
            '406,226,737',
            $rootNote->getRootProcess()->getId(),
            AttributeValue::VALUE_TEXT,
            null,
            $rootNote->getId());


    }


    /**
     * @param $notes
     * @param null $rootId
     * @return array
     */
    public function prepareNoteList($notes, $rootId = null) {

        /** @var User $user */
        $user = $this->userInfo->getUser();
        $this->rootId = $rootId;
        $list = [];
        $tempDictionary = [];

        $canListenRecording = $this->authChecker->isGranted('ROLE_RECORDING_CALL');
        $hasSpecialDataRole = $this->authChecker->isGranted('ROLE_RODO_MEDICAL');
        $isViewer = $user->hasRole('ROLE_CASE_VIEWER');
        $notePermissions = $this->getNotePermissions($user->getId());

        foreach ($notes as $id => $note) {

            $noteType = $note[CaseFormGenerator::NOTE_TYPE]['value_string'];

            if(!in_array($noteType, $notePermissions['allowed_note_types'], true)) {
                continue;
            }

            $specialData = (false === empty($note[CaseFormGenerator::NOTE_SPECIAL_DATA])) ? $note[CaseFormGenerator::NOTE_SPECIAL_DATA]['value_int'] : '0';

            if ((int)$specialData === 1 && false === $hasSpecialDataRole) {
                continue;
            }

            $newNote = [
                'attachment' => null,
                'content' => strip_tags($note[CaseFormGenerator::NOTE_CONTENT]['value_text'], '<table><strong><td><tr><thead><tbody><p><br><b><ul><li><span><a><i><div><strong>'),
                'direction' => $note[CaseFormGenerator::NOTE_DIRECTION]['value_int'] ?? 1,
                'icon' => 'phone',
                'status' => null,
                'subject' => null,
                'title' => 'Title',
                'special' => $note[CaseFormGenerator::NOTE_SPECIAL]['value_int']??0, // @todo:add Special Status Attribute 930
                'type' => $noteType,
                'date'=> '',
                'updated_by_id' => $note[CaseFormGenerator::NOTE_CONTENT]['updated_by_id'],
                'value' => '',
                'sender' => '',
                'special_data' => $hasSpecialDataRole ? $specialData : null
            ];

            $date = $note[CaseFormGenerator::NOTE_CONTENT]['created_at'];
            $newNote['date'] = $date;
            $createdBy = 'System';

            if(in_array($noteType, [
                CaseFormGenerator::NOTE_TYPE_TEXT, CaseFormGenerator::NOTE_TYPE_SMS, CaseFormGenerator::NOTE_TYPE_PHONE
            ])){

                $updatedById = $note[CaseFormGenerator::NOTE_CONTENT]['updated_by_id'];

                if ($updatedById && $updatedById != CaseFormGenerator::USER_AUTOMAT_ID) {
                    $createdBy = $this->entityManager->getRepository(User::class)->find($updatedById);
                    $createdBy = (string)$createdBy;
                }

            }

            switch ($noteType) {

                case CaseFormGenerator::NOTE_TYPE_PHONE: {

                    $contact = $this->getNameOfNumber($note[CaseFormGenerator::NOTE_PHONE]['value_string'], $tempDictionary);

                    $newNote['value'] = $note[CaseFormGenerator::NOTE_PHONE]['value_string'];

                    if($newNote['direction'] == "2") {

                        $newNote['title'] = $this->getTitleForNote($date, $contact, 'Atlas - '.$createdBy??'', $this->trans('note.caller'));
                    }
                    else {
                        $newNote['title'] = $this->getTitleForNote($date, $createdBy, $contact, $this->trans('note.caller'));
                    }

                    if($canListenRecording && !empty($note[CaseFormGenerator::NOTE_SUBJECT]['value_string'])) {
                        $audio = '<audio class="record-audio" controls><source src="'.$note[CaseFormGenerator::NOTE_SUBJECT]['value_string'].'" type="video/mp4">Your browser does not support the audio element.</audio>';
                        $newNote['content'] .= "<br>" . $audio;
                    }

                    break;
                }
                case CaseFormGenerator::NOTE_TYPE_EMAIL: {

                    if($isViewer && strpos(strtolower($note[CaseFormGenerator::NOTE_SUBJECT]['value_string']),'gwarancja płatności') !== false){
                        continue 2;
                    }

                    $newNote['icon'] = 'envelope-o';
                    $newNote['attachment'] = $note[CaseFormGenerator::NOTE_ATTACHMENT]['value_int'];
                    $newNote['status'] = $note[CaseFormGenerator::NOTE_STATUS]['value_string'];
                    $newNote['subject'] = $note[CaseFormGenerator::NOTE_SUBJECT]['value_string'];
                    $newNote['value'] = $note[CaseFormGenerator::NOTE_EMAIL]['value_string'];

                    if($newNote['direction'] == "2") {

                        $newNote['title'] = $this->getTitleForNote($date, $note[CaseFormGenerator::NOTE_EMAIL_SENDER]['value_string'], $note[CaseFormGenerator::NOTE_EMAIL]['value_string'] );

                    }
                    else {

                        $updatedById = $note[CaseFormGenerator::NOTE_SUBJECT]['updated_by_id'];

                        if ($updatedById && $updatedById != CaseFormGenerator::USER_AUTOMAT_ID) {
                            $createdBy = $this->entityManager->getRepository(User::class)->find($updatedById);
                            $createdBy = (string)$createdBy;
                        }

                        // emaili, może być więcej - loop

                        $emails = $note[CaseFormGenerator::NOTE_EMAIL]['value_string'];
                        $delimiter = (strpos($emails, ",") ? "," : (strpos($emails, ";") ? ";" : null));
                        $contact = '';

                        if($delimiter !== null) {

                            $emails = explode($delimiter, $emails);

                            foreach ($emails as $email) {
                                $contact .= $this->getNameOfNumber($email, $tempDictionary) . " ";
                            }

                        }
                        else {
                            $contact = $this->getNameOfNumber($note[CaseFormGenerator::NOTE_EMAIL]['value_string'], $tempDictionary);
                        }

                        unset($emails);

                        $to = $note[CaseFormGenerator::NOTE_EMAIL_SENDER]['value_string'];

                        $newNote['sender'] = $to;
                        $newNote['title'] = $this->getTitleForNote($date, $createdBy .' (' . $to . ')', $contact);

                    }

                    break;
                }
                case CaseFormGenerator::NOTE_TYPE_SMS: {

                    $contact = $this->getNameOfNumber($note[CaseFormGenerator::NOTE_PHONE]['value_string'], $tempDictionary);

                    $newNote['value'] = $note[CaseFormGenerator::NOTE_PHONE]['value_string'];

                    if($newNote['direction'] == "2") {
                        $newNote['title'] = $this->getTitleForNote($date, $contact, "Atlas", $this->trans('note.from:'), null);
                    }
                    else {
                        $newNote['title'] = $this->getTitleForNote($date, $createdBy, $contact);
                    }

                    $newNote['icon'] = 'commenting-o';

                    break;
                }
                case CaseFormGenerator::NOTE_TYPE_TEXT: {

                    $newNote['title'] = $this->getTitleForNote($date, $createdBy, $note[CaseFormGenerator::NOTE_PHONE]['value_string'], $this->trans('note.added_by'), null);
                    $newNote['icon'] = 'sticky-note-o';

                    break;
                }
                case CaseFormGenerator::NOTE_TYPE_CHAT: {

                    $updatedById = $note[CaseFormGenerator::NOTE_CONTENT]['updated_by_id'];

                    if ($updatedById && $updatedById != CaseFormGenerator::USER_AUTOMAT_ID) {

                        /** @var User $createdBy */
                        $createdBy = $this->entityManager->getRepository(User::class)->find($updatedById);

                        $newNote['chat_sender'] = $createdBy->getEmail();

                        $createdBy = (string)$createdBy;

                    }

                    $prefixTo = (!empty($note[CaseFormGenerator::NOTE_EMAIL]['value_string'])) ? "Do:" : null;

                    $newNote['title'] = $this->getTitleForNote($date, $createdBy, $note[CaseFormGenerator::NOTE_EMAIL]['value_string'], 'Od:', $prefixTo);
                    $newNote['icon'] = 'comments-o';

                    break;
                }
                case CaseFormGenerator::NOTE_TYPE_SYSTEM: {

                    $newNote['title'] = date('d-m-Y, H:i', strtotime($date)).': ' . $this->trans('note.system_notification');
                    $newNote['icon'] = 'bell-o';
                    $newNote['direction'] = 2;
                    $newNote['system'] = 1 ;

                    break;
                }

            }
            if($createdBy == 'System'){
                $newNote['system'] = 1;
            }else{
                $newNote['system'] = 0;
            }
            $list[$id] = $newNote;

        }

        return $list;

    }

    /** PRIVATES */

    /**
     * @param $date
     * @param $from
     * @param $to
     * @param string $prefixFrom
     * @param string $prefixTo
     * @return false|string
     */

    private function getTitleForNote($date, $from, $to, $prefixFrom = 'note.from', $prefixTo = 'note.to') {


        if($prefixFrom === 'note.from') {
            $prefixFrom = $this->trans('note.from');
        }

        if($prefixTo === 'note.to') {
            $prefixTo = $this->trans('note.to');
        }

        $title =  date('d-m-Y, H:i', strtotime($date));

        if($prefixFrom) {
            $title .= " | ".$prefixFrom." " . $from;
        }

        if($prefixTo) {
            $title .= " | ".$prefixTo." " . $to;
        }

        return  $title;

    }

    private function getNameOfNumber($number, &$tempDictionary) {

        if(array_key_exists($number, $tempDictionary)) {
            return $tempDictionary[$number];
        }

        $name = '';

        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$this->rootId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'number',
                'value' => $number,
                'type' => PDO::PARAM_STR
            ]
        ];

        $result = $this->queryManager->executeProcedure('EXEC dbo.p_contact_info @numberValue = :number, @rootId = :rootId', $parameters);

        if(!empty($result) && !empty($result[0]['name'])) {
            $name = $result[0]['name'];
            $tempDictionary[$number] = $name;
        }

        if(!empty($name)) {
            return $name;
        }

        return $number;

    }

    /**
     * @param AttributeValue $note
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function resendEmail(AttributeValue $note ) {

        $statusAttributeId = $this->setNoteValue($note, '406,226,734', 'pending', AttributeValue::VALUE_STRING);

        if($statusAttributeId) {
            $this->addEmailToJob($note->getId());
        }

        return $statusAttributeId;

    }

    /**
     * @param AttributeValue $note
     * @param $path
     * @param $value
     * @param $type
     * @return bool|int
     */
    public function setNoteValue(AttributeValue $note, $path, $value, $type) {

        $rootId = $note->getRootProcess()->getId();

        $attr = $this->processHandler->getAttributeValue($path, $rootId, null, null, $note->getId());

        if(!empty($attr)) {

            $id = $attr[0]['id'];

            $this->processHandler->setAttributeValue([
                $path => $id
            ], $value,
                $type,
                Step::ALWAYS_ACTIVE,
                $rootId,
                $this->userInfo->getUser()->getId(),
                $this->userInfo->getOriginalUser()->getId()
            );

            return $id;

        }

        return null;

    }


    /**
     * @param $noteId
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addEmailToJob($noteId) {

        $job =  new Job('atlas:mailbox:note-send', ['noteId' => $noteId]);
        $this->entityManager->persist($job);
        $this->entityManager->flush();

    }

    public function addNote($groupProcessId, $type, $content, $phoneNumber = null, $email = null, $userId = 1, $special = 0){
        $parameters = [
            [
                'key' => 'groupProcessId',
                'value' => (int)$groupProcessId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'email',
                'value' => $email,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ],
            [
                'key' => 'phoneNumber',
                'value' => $phoneNumber,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ],
            [
                'key' => 'type',
                'value' => $type,
                'type' => PDO::PARAM_STR,
                'length' => 50,
            ],
            [
                'key' => 'content',
                'value' => $content,
                'type' => PDO::PARAM_STR,
                'length' => 4000,
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'err',
                'value' => 0,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],            [
                'key' => 'special',
                'value' => 0,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ]
        ];

        $this->queryManager->executeProcedure('EXEC dbo.p_note_new @groupProcessId = :groupProcessId, @type = :type, 
        @content = :content, @phoneNumber = :phoneNumber, @email = :email, @userId = :userId, @err = :err, @message = :message, @special = :special',
        $parameters, false);

    }


    /**
     * @param $userId
     * @return array
     */
    private function getNotePermissions($userId): array
    {

        $result = $this->queryManager->executeProcedure('EXEC [dbo].[P_note_permissions] @userId = :userId', [
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ]
        ]);

        $permissions = [];

        foreach ($result as $item) {

            if($item['name'] === 'allowed_note_types') {
                $permissions[$item['name']] = explode(',', $item['value']);
            }
            else {
                $permissions[$item['name']] = $item['value'];
            }

        }

        return $this->mergePermissions($permissions);

    }

    /**
     * @param $permissions
     * @return array
     */
    private function mergePermissions($permissions): array
    {

        $resolver = (new OptionsResolver())->setDefaults(array(
            'allowed_note_types' => [
                CaseFormGenerator::NOTE_TYPE_EMAIL,
                CaseFormGenerator::NOTE_TYPE_PHONE,
                CaseFormGenerator::NOTE_TYPE_TEXT,
                CaseFormGenerator::NOTE_TYPE_SMS,
                CaseFormGenerator::NOTE_TYPE_CHAT,
                CaseFormGenerator::NOTE_TYPE_SYSTEM
            ]
        ));

        return $resolver->resolve($permissions);

    }

}