<?php

namespace CaseBundle\Service;

use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessFlow;
use CaseBundle\Entity\Step;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use ManagementBundle\Entity\DevLog;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessChartBuilder
{

    /** @var  EntityManager $em */
    protected $em;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    public function build(ProcessDefinition $processDefinition, $programId)
    {
        $steps = $this->em->getRepository(Step::class)->fetchStepsByProgram($processDefinition,$programId);
        $flows = $this->em->getRepository(ProcessFlow::class)->fetchFlowsByProgram($processDefinition,$programId);

        $stepArray = $this->prepareSteps($steps);
        $flowArray = $this->prepareFlows($flows);

        return json_encode(
            [
                'nodeKeyProperty' => 'graphId',
                'nodeDataArray' => $stepArray,
                'linkDataArray' => $flowArray,
            ]
        );
    }

    protected function prepareSteps($steps)
    {
        $stepArray = [];
        /** @var Step $step */
        foreach ($steps as $key => $step) {

            $stepArray[$key] = [
                'graphId' => $step->getGraphId(),
                'id' => $step->getId(),
                'name' => $step->getName(),
                'ownerName' => $step->getOwnerName(),
                'isTechnical' => $step->getIsTechnical(),
                'query' => $step->getQuery(),
            ];
            if ($step->getGraphPoints()) {
                $stepArray[$key]['loc'] = $step->getGraphPoints();
            }
        }

        return $stepArray;
    }

    protected function prepareFlows($flows)
    {
        $flowArray = [];

        /** @var ProcessFlow $flow */
        foreach ($flows as $key => $flow) {
            if ($flow->getStep() && $flow->getNextStep()) {
                $flowArray[$key] = [
                    'id' => $flow->getId(),
                    'from' => $flow->getStep()->getGraphId(),
                    'to' => $flow->getNextStep()->getGraphId(),
                    'variant' => $flow->getVariant(),
                    'isPrimaryPath' => $flow->getIsPrimaryPath(),
                    'description' => $flow->getDescription(),
                    'variantDesc' => $flow->getVariantDescription()
                ];
                if ($flow->getGraphPoints()) {
                    $flowArray[$key]['points'] = json_decode($flow->getGraphPoints());
                }
                if ($flow->getCurviness()) {
                    $flowArray[$key]['curviness'] = $flow->getCurviness();
                }
            }
        }

        return $flowArray;
    }

    /**
     * @param $data \stdClass
     * @param $processDefinition ProcessDefinition
     */
    public function save($data, $processDefinition)
    {

        $stepRepository = $this->em->getRepository('CaseBundle:Step');
        $flowRepository = $this->em->getRepository('CaseBundle:ProcessFlow');

        $stepsIdsArray = [];
        $stepsToDelete = new ArrayCollection();
        $flowsToDelete = new ArrayCollection();

        foreach ($processDefinition->getSteps() as $step) {
            $stepsToDelete->add($step);
        }
        foreach ($processDefinition->getFlows() as $flow) {
            $flowsToDelete->add($flow);
        }

        foreach ($data->nodeDataArray as $nodeData) {

            if (!property_exists($nodeData, 'id')) {
                continue;
            }

            $step = $stepRepository->find($nodeData->id);
            if (!$step) {
                $step = new Step();
                $step->setId($nodeData->id);
                $step->setName($nodeData->name);
                $step->setProcessDefinition($processDefinition);

                /** TODO - TO JEST W OGÓLE UŻYWANE? */

            }
            if (property_exists($nodeData, 'loc')) {
                $step->setGraphPoints($nodeData->loc);
            }
            $this->em->persist($step);

            $stepsIdsArray[$nodeData->graphId] = $step;
            $stepsToDelete->removeElement($step);
        }

        foreach ($data->linkDataArray as $linkData) {

            if (property_exists($linkData, 'id')) {
                /** @var ProcessFlow $flow */
                $flow = $flowRepository->find($linkData->id);
                if (!$flow) {
                    $flow = new ProcessFlow();
                } else {
                    $flowsToDelete->removeElement($flow);
                }
            } else {
                $flow = new ProcessFlow();
            }

            if (array_key_exists($linkData->from, $stepsIdsArray) && array_key_exists($linkData->to, $stepsIdsArray)) {

                /** DEV LOG */
                if(!$flow->getId()) {
                    $devLog = DevLog::CREATED_FLOW($processDefinition, $stepsIdsArray[$linkData->from], $stepsIdsArray[$linkData->to]);
                    $this->em->persist($devLog);
                }
                else {
                    if($flow->getStep() && $flow->getNextStep()) {
                        if(($flow->getStep() !== $stepsIdsArray[$linkData->from]) || ($flow->getNextStep() !== $stepsIdsArray[$linkData->to])) {
                            $devLog = DevLog::UPDATED_PATH_FLOW($processDefinition, $flow, $stepsIdsArray[$linkData->from], $stepsIdsArray[$linkData->to]);
                            $this->em->persist($devLog);
                        }
                    }
                }

                $flow->setProcessDefinition($processDefinition);
                $flow->setStep($stepsIdsArray[$linkData->from]);
                $flow->setNextStep($stepsIdsArray[$linkData->to]);
                $flow->setVariant($linkData->variant);
                if (property_exists($linkData, 'points')) {
                    $flow->setGraphPoints(json_encode($linkData->points));
                }
                if (property_exists($linkData, 'curviness')) {
                    $flow->setCurviness($linkData->curviness);
                }
                $this->em->persist($flow);
            }

        }

        foreach ($stepsToDelete as $step) {
            /** DEV_LOG */
            $devLog = DevLog::DELETED_STEP($step, $processDefinition);
            $this->em->persist($devLog);
            /** ------- */
            $processDefinition->removeStep($step);
        }

        /** @var ProcessFlow $flow */
        foreach ($flowsToDelete as $flow) {
            /** DEV_LOG */
            $devLog = DevLog::DELETED_FLOW($processDefinition, $flow->getStep(), $flow->getNextStep());
            $this->em->persist($devLog);
            /** ------- */
            $processDefinition->removeFlow($flow);
        }

        $this->em->flush();
    }

}