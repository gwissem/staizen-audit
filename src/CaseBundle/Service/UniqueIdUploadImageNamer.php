<?php

namespace CaseBundle\Service;

use Oneup\UploaderBundle\Uploader\File\FileInterface;
use Oneup\UploaderBundle\Uploader\Naming\NamerInterface;

class UniqueIdUploadImageNamer implements NamerInterface
{
    public function name(FileInterface $file)
    {
        $name = uniqid();
        $folderPath = sprintf('%s/%s/', substr($name, 0, 2), substr($name, 2, 2));
        return $folderPath . sprintf('%s.%s', $name, $file->getExtension());
    }
}
