<?php

namespace CaseBundle\Service;


use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\Regex;

class ValidatorService
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function isValid(Form $form): bool
    {
        if (!$this->validRegexPattern($form)) {
            return false;
        }

        return true;
    }

    private function validRegexPattern(Form $form)
    {
        if ($form->has('regex')) {
            if (!$this->isRegexPatternValid($form->get('regex')->getData())) {
                $form->get('regex')->addError(
                    new FormError($this->translator->trans(/** @Ignore */
                        (new Regex(""))->message, [], 'validators')
                    ));
                return false;
            }
        }
        return true;
    }

    private function isRegexPatternValid($value): bool
    {
        return !(@preg_match($value, null) === false) || empty($value);
    }

}