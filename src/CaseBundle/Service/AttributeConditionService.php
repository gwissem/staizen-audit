<?php

namespace CaseBundle\Service;


use CaseBundle\Controller\FormGenerator\DefaultController;
use CaseBundle\Entity\AttributeCondition;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Utils\Language;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\ExpressionLanguage\SyntaxError;
use Symfony\Component\Process\Process;
use ToolBundle\Service\DateParserService;
use UserBundle\Service\UserInfoParserService;

class AttributeConditionService
{
    const JAVASCRIPT_COMMENT = '// <-- CONDITION JS -->';
    const DEFAULT_VALUE = true;

    private $condition;
    private $lastType;
    private $attributePattern;
    private $datePattern;
    private $userInfoPattern;
    private $attributeParserService;
    private $dateParserService;
    private $userInfoParserService;

    /** @var EntityManager */
    private $em;

    const CONDITION_TYPE_VISIBLE = 'visible';

    const FUNCTIONS_WHITELIST = [
      'config'
    ];

    public function __construct(
        ContainerInterface $container
    )
    {
        $this->attributeParserService = $container->get('case.attribute_parser.service');
        $this->dateParserService = $container->get('tool.date_parser.service');
        $this->userInfoParserService = $container->get('user.info_parser.service');
        $this->em = $container->get('doctrine.orm.entity_manager');

        $this->attributePattern = $this->attributeParserService->getSchemaRegex();
        $this->datePattern = $this->dateParserService->getSchemaRegex();
        $this->userInfoPattern = $this->userInfoParserService->getSchemaRegex();
        $this->condition = '';

    }

    public function setCondition($condition)
    {
        $this->condition = $condition;
        $this->standardizeLogicOperators();

        return $this;
    }

    private function standardizeLogicOperators()
    {
        return str_replace(
            ["'", ' like ', ' and ', ' AND ', ' or ', ' OR '],
            ['"', ' LIKE ', ' && ', ' && ', ' || ', ' || '],
            trim($this->condition)
        );
    }

    public function checkCondition($type, $formControlId, $processInstance)
    {

        $defaultReturn = $type == self::CONDITION_TYPE_VISIBLE ? true : false;

        if(!$processInstance instanceof ProcessInstance) {
            $processInstance = $this->em->getRepository(ProcessInstance::class)->find($processInstance);
        }

        /** Dodane cachowanie wyników AttributeCondition na 10 min */

        /** @var  $attributeCondition[] */
        $formControlCondition = $this->em->getRepository('CaseBundle:AttributeCondition')
            ->getCondition($formControlId,$processInstance->getStep());

//
//        /** @var  $attributeCondition */
//        $formControlCondition = $this->em->getRepository(AttributeCondition::class)
//                        ->findOneBy([
//                            'formControlId' => $formControlId,
//                            'step' => $processInstance->getStep()
//                        ]);

        if(!empty($formControlCondition) || $type == 'notAllowEdit') {

            $condition = '';

            if(!empty($formControlCondition)) {
                /** @var AttributeCondition $formControlCondition */
                $formControlCondition = $formControlCondition[0];

                $getter = 'get' . ucfirst($type);
                $condition = $formControlCondition->$getter();
            }

            if($type == 'notAllowEdit'){
                $condition =  empty($condition) ?  '"{#hasRole(ROLE_OPERATIONAL_DASHBOARD_READ_ONLY)#}" == "1"' : '('.$condition.') || "{#hasRole(ROLE_OPERATIONAL_DASHBOARD_READ_ONLY)#}" == "1"';
            }

            if (empty($condition))
                return $defaultReturn;

            $parser = $this->attributeParserService;
            $parser->setProcessInstanceId($processInstance);
            $parser->setGroupProcessInstanceId($processInstance->getGroupProcess()->getId());
            $lang = new Language();

            try {
                return $lang->evaluate($parser->parseString($condition, NULL, true));
            }
            catch (SyntaxError $exception) {
                throw new SyntaxError($exception->getMessage() . ' FormControlId: ' . $formControlCondition->getFormControlId());
            }

        }

        return $defaultReturn;
    }

    public function conditionResult($groupProcessInstance): bool
    {
        if (!$this->condition) {
            return self::DEFAULT_VALUE;
        }

        $this->replacePatterns($groupProcessInstance);

        $lang = new Language();

        return $lang->evaluate($this->condition);
    }

    private function replacePatterns($groupProcessInstance)
    {
        $this->parseCondition($this->attributePattern, $this->attributeParserService->setGroupProcessInstanceId($groupProcessInstance));
        $this->parseCondition($this->datePattern, $this->dateParserService);
        $this->parseCondition($this->userInfoPattern, $this->userInfoParserService);
    }

    private function parseCondition(string $pattern, ParserInterface $parserService)
    {
        $matches = [];
        if (preg_match_all($pattern, $this->condition, $matches)) {
            foreach ($matches[0] as $match) {
                $this->condition = preg_replace($pattern, $parserService->parse($match), $this->condition, 1);
            }
        }
    }

    public function getLastAttributeType()
    {
        return ($this->lastType) ?: AttributeCondition::TYPE_NOT_REQUIRED;
    }

    public function trySaveValidationJS(FormControl $formControl) {

        if(!$formControl->getId()) return false;

        $javascript = $formControl->getJavaScript();

        $commentPosition = strpos($javascript, self::JAVASCRIPT_COMMENT);

        if($javascript === null || $commentPosition !== false)
        {
            /** @var AttributeCondition $formControlCondition */
            $formControlCondition = $this->em->getRepository('CaseBundle:AttributeCondition')->findOneBy([
                'formControlId' => $formControl->getId()
            ]);

            if($formControlCondition) {

                if(!empty($formControlCondition->getVisible())) {
                    $visibleJs = $this->createJavaScriptOfCondition($formControl, $formControlCondition->getVisible());
                }
                else {
                    $visibleJs = "";
                }

                if($commentPosition !== false) {
                    $javascript = substr($javascript, 0,  $commentPosition) . $visibleJs;
                }
                else {
                    $javascript = $visibleJs;
                }

                $extraData = $formControl->getExtraDataArray();
                $extraData['javascript'] = base64_encode($javascript);
                $formControl->setExtraData(\GuzzleHttp\json_encode($extraData));

                return true;
            }

        }

        return false;

    }

    public function createJavaScriptOfCondition(FormControl $formControl, string $condition)
    {

        $javaScript = '';

        $functions = $this->attributeParserService->getFunctionBetween($condition);
        $specialWords = $this->attributeParserService->getSpecialWordsBetween($condition);
        $params = $this->attributeParserService->getParamsBetween($condition);

        $checkedFunctions = $this->checkFunctions($functions);

        /**
         * Warunki z warunkami i specjalnymi danymi są walidowane ajaxowo, pod warunkiem, że jest w warunku ścieżka z formularza
         */

        if(!empty($params) && (!empty($specialWords) || (!$checkedFunctions && !empty($functions)))) {
//        if((!empty($functions) || !empty($specialWords)) && !empty($params)) {

            $condition = str_replace("'", "\"", $condition);
            $condition = str_replace("*\"", "\"", $condition); // Usunięcie gwiazdki

            $javaScript = $this->generateAsyncJavaScript($params, $condition, $formControl->getId());

        }
        else {

            $parsedCondition = $condition;

            /** Transformacja zwykłych zmiennych - {@123@} */
            if(!empty($params)) {

                foreach ($params as $param) {

                    if(strpos($parsedCondition, AttributeParserService::QUOTATION_PREFIX . $param . AttributeParserService::QUOTATION_SUFFIX) !== FALSE) {
                        $parsedCondition = str_replace(AttributeParserService::QUOTATION_PREFIX . $param . AttributeParserService::QUOTATION_SUFFIX, '"' . $param . '"|valueOfPath', $parsedCondition);
                    }
                    else {
                        $parsedCondition = str_replace(AttributeParserService::PREFIX . $param . AttributeParserService::SUFFIX, '"' . $param . '"|valueOfPath', $parsedCondition);
                    }

                    // Usunięcie gwiazdki
                    $parsedCondition = str_replace("*\"", "\"", $parsedCondition);

                }
            }

            /** Transformacja funkcji, np: {#config(...)#}  */
            if($checkedFunctions) {

                foreach ($functions as $function) {

                    $functionName = substr($function, 0, strpos($function, '('));

                    if($functionName == "config") {

                        $configKey = substr($function, strpos($function, '(') + 1);
                        $configKey = substr($configKey, 0, strlen($configKey) - 1);

                        if(strpos($parsedCondition, AttributeParserService::WORD_QUOTATION_PREFIX . $function . AttributeParserService::WORD_QUOTATION_SUFFIX) !== FALSE) {
                            $parsedCondition = str_replace(AttributeParserService::WORD_QUOTATION_PREFIX. $function . AttributeParserService::WORD_QUOTATION_SUFFIX, '"' . $configKey . '"|config', $parsedCondition);
                        }
                        else {
                            $parsedCondition = str_replace(AttributeParserService::WORD_PREFIX . $function . AttributeParserService::WORD_SUFIX, '"' . $configKey . '"|config', $parsedCondition);
                        }

                    }

                }

            }

            $javaScript = $this->generateJavaScript($params, $parsedCondition, $formControl->getId());

        }

        return $javaScript;
    }

    /**
     * Sprawdza, czy występują tylko funkcje, które mogą być parsowane na front'cie
     *
     * @param $functions
     * @return bool
     */
    private function checkFunctions($functions) {

        if(empty($functions)) return true;

        $check = true;

        foreach ($functions as $function) {

            $functionName = substr($function, 0, strpos($function, '('));

            if(!in_array($functionName, self::FUNCTIONS_WHITELIST)) {
                $check = false;
                break;
            }

        }

        return $check;
    }

    private function generateSelector($paths) {

        $selector = '';

        foreach ($paths as $key => $path) {
            if($key !== 0) {
                $selector .= ',';
            }
            $path = str_replace(array('*', '%'), '', $path);
            $selector .= '[data-attribute-path="' . $path . '"]';
        }

        return $selector;
    }

    private function generateAsyncJavaScript($paths, $condition, $controlId) {

        $js = self::JAVASCRIPT_COMMENT . "\n\n";
        $js .= "try {\n";
        $js .= "\t validatorModule.addAsyncVisibilityCondition('" . $this->generateSelector($paths) . "', '". $condition ."', " . $controlId . ");";
        $js .= "\n } catch(e) { console.error(e) }";

        return $js;

    }

    private function generateJavaScript($paths, $parsedCondition, $controlId) {

        $parsedCondition = str_replace("'", "", $parsedCondition);

        $js = self::JAVASCRIPT_COMMENT . "\n\n";
        $js .= "try {\n";
        $js .= "\t validatorModule.addVisibilityCondition('" . $this->generateSelector($paths) . "', '" . $parsedCondition . "', " . $controlId . ");";
        $js .= "\n } catch(e) { console.error(e) }";

        return $js;

    }


}