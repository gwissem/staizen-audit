<?php

namespace CaseBundle\Service;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeCondition;
use CaseBundle\Entity\FormControl;
use CaseBundle\Entity\FormTemplate;
use CaseBundle\Entity\ProcessFlow;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Twig_Environment;

class FormTemplateGenerator
{

    const WIDGET_DIVIDER_HORIZONTAL = "divider_horizontal";
    const WIDGET_DIVIDER_VERTICAL = "divider_vertical";
    const WIDGET_DIV = "div";
    const WIDGET_PARAGRAPH = "paragraph";
    const WIDGET_HEADER = "header";
    const WIDGET_IMAGE = "image";
    const WIDGET_EMBEDDED_TOOL = "embedded_tool";
    const WIDGET_IFRAME = "iframe";
    const WIDGET_BUTTON = "button";
    const WIDGET_MAP = "map";
    const WIDGET_SERVICES = "services";
    const WIDGET_PANEL_EDIT_ATTRIBUTE = "panel_edit_attribute";
    const WIDGET_RSA_COSTS_VERIFICATION = 'rsa_costs_verification';
    const WIDGET_EMAIL_PREVIEW = 'email_preview';
    const WIDGET_MATRIX = 'matrix';

    const MODE_NONE = '';
    const MODE_PREVIEW = 'mode_preview';
    const MODE_FULL = 'full';

    /** @var EntityManager $em */
    protected $em;

    /** @var  ProcessHandler */
    protected $processHandler;

    protected $twig;
    protected $attributeConditionService;
    protected $attributeParserService;
    protected $container;
    protected $controlsArray;

    private $mode = null;
    private $groupProcessId = null;
    private $processInstanceId = null;
    private $formControlsAttribute;
    private $stepId;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->twig = $container->get('twig');
        $this->attributeConditionService = $container->get('case.attribute_condition');
        $this->attributeParserService = $container->get('case.attribute_parser.service');
        $this->processHandler = $container->get('case.process_handler');
        $this->controlsArray = [];
        $this->formControlsValue = [];
    }

    static function getAvailableWidgets()
    {
        return [
            self::WIDGET_DIVIDER_HORIZONTAL => 'Separator poziomy',
            self::WIDGET_DIVIDER_VERTICAL => 'Separator pionowy',
            self::WIDGET_HEADER => 'Nagłówek',
            self::WIDGET_PARAGRAPH => 'Paragraf',
            self::WIDGET_DIV => 'DIV',
            self::WIDGET_IMAGE => 'Obraz',
            self::WIDGET_IFRAME => 'Iframe',
            self::WIDGET_BUTTON => 'Przycisk',
            self::WIDGET_MAP => 'Mapa',
            self::WIDGET_SERVICES => 'Usługi',
            self::WIDGET_RSA_COSTS_VERIFICATION => 'Weryfikacja kosztów kontraktora',
            self::WIDGET_PANEL_EDIT_ATTRIBUTE => 'Panel edycji atrybutów',
            self::WIDGET_EMAIL_PREVIEW => 'Podgląd E-mail',
            self::WIDGET_MATRIX => 'Matrix'
        ];
    }

    public function setAdditionalData($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function setFullMode() {
        $this->mode = self::MODE_FULL;
        $this->twig->addGlobal('modeFormGenerator', self::MODE_FULL);
    }

    public function generate(FormTemplate $formTemplate, $mode = self::MODE_NONE)
    {

        if ($this->mode === null) {
            $this->mode = $mode;
            $this->twig->addGlobal('modeFormGenerator', $mode);
        }

        return $this->generateControlsOfFormTemplate($formTemplate);

//        $this->attributeConditionService->isVisible(1591, '357,397,64')
    }

    /**
     * @param FormTemplate $formTemplate
     * @return array
     */
    public function generateControlsOfFormTemplate(FormTemplate $formTemplate)
    {

        $this->controlsArray = [];

        /** @var FormControl $control */
        foreach ($formTemplate->getControls() as $control) {

            $children = [];
            $childFormTemplate = null;
            $this->setTempData($control);

            if ($control->getAttribute() && $control->getAttribute()->hasFormTemplate()) {

                $childFormTemplate = $control->getAttribute()->getFormTemplate();
                $this->recursivePrepareControls($childFormTemplate->getControls(), $children);

            }

            $this->controlsArray[] = $this->prepareControl($control, $children, $childFormTemplate);
        }

        usort($this->controlsArray, function ($a, $b){ return ($a['y'] * 100 + $a['x']) > ($b['y'] * 100 + $b['x']); });

        return $this->controlsArray;

    }

    private function setTempData(FormControl $formControl)
    {

        if ($this->mode === self::MODE_FULL) {

            $formControl->groupProcessId = $this->groupProcessId;
            $formControl->processInstanceId = $this->processInstanceId;

            $formControl->isVisible = $this->attributeConditionService->checkCondition('visible', $formControl->getId(), $this->processInstanceId);
            $formControl->notAllowEdit = $this->attributeConditionService->checkCondition('notAllowEdit', $formControl->getId(), $this->processInstanceId);

            $formControl->controlType = $this->getFormControlHtmlProperty($formControl->getId(), $this->processInstanceId);

            if ($formControl->hasAttribute()) {

                $attribute = $formControl->getAttribute();

                $formControlAttributeData = $this->findFormControlAttribute($formControl->getId());

                if (count($formControlAttributeData) >= 1 && $attribute->getIsMulti()) {
                    /** Dla multi */

                    /** Multi który posiada dzieci */
                    $isNested = ($attribute->getChildren()->count()) ? true : false;

                    /** Ustalenie Position dla NULL'i */
                    usort($formControlAttributeData, function($a, $b) {

                        if($a['position'] === $b['position']) {
                            return ($a['attribute_value_id'] > $b['attribute_value_id']);
                        }

                        if($a['position'] === null && $b['position'] === null) return -1;

                        $A = ($a['position'] === null) ? 100 : $a['position'];
                        $B = ($b['position'] === null) ? 100 : $b['position'];

                        return ($A > $B);
                    });
                    /** ------------ */

                    $first = $formControlAttributeData[key($formControlAttributeData)];

                    $this->setAttr($attribute, $first);

                    foreach ($formControlAttributeData as $item) {

                        $newArr = [
                            'attribute_value_id' => $item['attribute_value_id'],
                            'parent_attribute_value_id' => $item['parent_attribute_value_id'],
                            'attribute_path' => $item['attribute_path'],
                            'position' => $item['position'],
                            'value' => $item[$attribute->getValueType()],
                            'group_process_id' => $this->groupProcessId,
                            'step_id' => $this->stepId,
                            'children' => null
                        ];

                        if ($isNested) {
                            $this->recursiveGetMultiValues($newArr);
                        }

                        $attribute->attributeMultiValues[$item['attribute_value_id']] = $newArr;

                    }

                } elseif (count($formControlAttributeData) === 1) {

                    $formControlAttributeData = current($formControlAttributeData);

                    $this->setAttr($attribute, $formControlAttributeData);

                } else {

                    // TODO - OGARNAC TO!
                    //TODO zgłosić do loggera
                }
            }
        }

    }

    private function findFormControlAttribute($id)
    {

        return array_filter($this->formControlsAttribute, function ($item) use ($id) {
            return $item['id'] == $id;
        });

    }

    private function setAttr(Attribute $attribute, $arr)
    {

        $attribute->setAttrValue('path', $arr['attribute_path']);
        $attribute->setAttrValue('stepId', $this->stepId);
        $attribute->setAttrValue('groupProcessId', $this->groupProcessId);
        $attribute->setAttrValue('id', $arr['attribute_value_id']);
        $attribute->setAttrValue('value', $arr[$attribute->getValueType()]);
        $attribute->setAttrValue('parentAttributeValueId', $arr['parent_attribute_value_id']);
        $attribute->setAttrValue('isAuto', $this->isAutoValue($attribute, $arr));
    }

    private function isAutoValue(Attribute $attribute, $arr) {
        return (int)($arr['updated_by_id'] == 1 || $arr['updated_by_id'] == NULL) && !empty($arr[$attribute->getValueType()]) ;
    }

    private function recursiveGetMultiValues(&$multiArray)
    {

        $childrenFormControlValues = $this->findFormControlChildren($multiArray['attribute_value_id']);

        if (!empty($childrenFormControlValues)) {
            $multiArray['children'] = [];

            foreach ($childrenFormControlValues as $item) {

                $attr = $this->getAttributeFromPath($item['attribute_path']);

                /** Te parametry dostępne są w multiValuesChildren  na froncie */

                $newArr = [
                    'attribute_value_id' => $item['attribute_value_id'],
                    'parent_attribute_value_id' => $item['parent_attribute_value_id'],
                    'attribute_path' => $item['attribute_path'],
                    'value' => $item[$attr->getValueType()],
                    'group_process_id' => $this->groupProcessId,
                    'step_id' => $this->stepId,
                    'children' => null,
                    'isAuto' => $this->isAutoValue($attr, $item)
                ];

                $this->recursiveGetMultiValues($newArr);

                $multiArray['children'][$attr->getId()] = $newArr;

            }
        }

    }

    private function findFormControlChildren($attributeValueId)
    {

        return array_filter($this->formControlsAttribute, function ($item) use ($attributeValueId) {
            return $item['parent_attribute_value_id'] == $attributeValueId;
        });
    }

    /**
     * @param $path
     * @return Attribute|null|object
     */
    public function getAttributeFromPath($path)
    {

        if (strpos($path, ',') !== false) {

            $parentsPath = explode(',', $path);
            $id = array_pop($parentsPath);

            $attribute = $this->em->getRepository('CaseBundle:Attribute')->find($id);

            if (!$attribute) throw new NotFoundResourceException('Nie znaleziono attrybutu! Coś nie tak...');

            return $attribute;

        }

        /** TODO - CHYBA DO POPRAWY, nie powinno zwracac Path tylko Attribute Object */

        return $path;
    }

    /**
     * @param ArrayCollection $controls
     * @param $parentChildren
     */
    private function recursivePrepareControls($controls, &$parentChildren)
    {

        /** @var FormControl $control */
        foreach ($controls as $control) {

            $children = [];
            $control->groupProcessId = $this->groupProcessId;
            $this->setTempData($control);

            if ($control->getAttribute() && $control->getAttribute()->hasFormTemplate()) {
                $this->recursivePrepareControls($control->getAttribute()->getFormTemplate()->getControls(), $children);
            }

            $parentChildren[] = $this->prepareControl($control, $children);

        }

    }

    /**
     * @param FormControl $formControl
     * @param $children
     * @param null|FormTemplate $formTemplate
     * @return array
     */
    private function prepareControl(FormControl $formControl, $children, $formTemplate = null)
    {

        if ($this->mode === self::MODE_FULL) {
            $this->parseLabel($formControl);
        }

        $arrayOfControl = $formControl->toArray();

        if ($this->mode === self::MODE_FULL) {

            if ($formControl->hasAttribute()) {

                $attr = $formControl->getAttribute();

                $arrayOfControl['attribute_value_id'] = $attr->getAttrValue('id');
                if ($attr->getIsMulti()) {

                    $arrayOfControl['multi'] = [
                        'parentValueId' => $attr->getAttrValue('parentAttributeValueId'),
                        'attributeId' => $attr->getId(),
                        'multiValues' => $attr->attributeMultiValues
                    ];

                }
            }
        }

        if ($formTemplate) {
            $config = $formTemplate->getConfig();
            if (!empty($config['height'])) $arrayOfControl['height'] = $config['height'];
            if (!empty($config['width'])) $arrayOfControl['width'] = $config['width'];
        }

        if (!empty($children)) {
            $arrayOfControl['children'] = $children;
        } else {
            $arrayOfControl['html'] = $this->renderFormControl($formControl);
        }

        return $arrayOfControl;

    }

    private function parseLabel(FormControl $formControl) {

//        if($formControl->getWidget() === self::WIDGET_PARAGRAPH && $formControl->getLabel()) {

            $this->attributeParserService->setGroupProcessInstanceId($this->groupProcessId);
            $this->attributeParserService->setProcessInstanceId($this->processInstanceId);
            $formControl->setParsedLabel($this->attributeParserService->parseString($formControl->getLabel()));

//        }

    }

    /**
     * @param FormControl $formControl
     * @return string
     */
    public function renderFormControl(FormControl $formControl)
    {

        return $this->twig->render('CaseBundle:FormTemplate:base.html.twig', [
            'formControl' => $formControl,
            'processInstanceId' => $this->processInstanceId
        ]);

    }

    /**
     * @param FormControl $formControl
     * @param bool $withForm
     * @return string
     */
    public function renderHtmlOfFormControl(FormControl $formControl, $withForm = true)
    {

        return $this->twig->render('CaseBundle:FormTemplate:attribute-html.html.twig', [
            'formControl' => $formControl,
            'withForm' => $withForm,
            'processInstanceId' => $this->processInstanceId
        ]);

    }

    /**
     * @param FormControl $formControl
     * @return string
     */
    public function renderHtmlOfWidget(FormControl $formControl)
    {

        return $this->twig->render('CaseBundle:FormTemplate:widget-html.html.twig', [
            'formControl' => $formControl,
            'processInstanceId' => $this->processInstanceId
        ]);

    }

    public function generateControlFormOfStep($step, $path, $groupProcessId)
    {

        if(!$step instanceof Step) {
            $step = $this->em->getRepository('CaseBundle:Step')->find($step);
        }

        /** @var ProcessInstance[] $processInstance */
        $processInstance = $this->em->getRepository('CaseBundle:ProcessInstance')->findBy([
            'groupProcess' => $groupProcessId,
            'step' => $step
        ]);

        if(!empty($processInstance)) {

            $this->formControlsAttribute = $this->processHandler->formControls($processInstance[0]->getId(),null,0);

            $formTemplate = $this->em->getRepository('CaseBundle:FormTemplate')->findFirstTemplateIdOfStep($step);

            /** @var FormControl[] $formControl */
            $formControl = $this->em->getRepository('CaseBundle:FormControl')->findBy([
                'formTemplate' => $formTemplate,
                'attributePath' => $path
            ]);

            if(!empty($formControl)) {

                $this->setTempData($formControl[0]);
                $formControl[0]->escapeHtml = false;
                return $this->prepareControl($formControl[0], []);

            }

        }

        return null;

    }

    public function generateQuizHtml(Step $step, $processData = [])
    {

        $controlOfDescriptionQuiz = $this->generateControlFormOfStep('1011.009', '417', $processData['rootId']);

        $question = $step->getName();
        $answers = $this->em->getRepository(ProcessFlow::class)->findBy(['step' => $step, 'deletedAt' => NULL]);
        $src = NULL;
        $instanceId = NULL;
        $uId = 'quiz';
        $isActive = 1;

        if(!empty($processData)) {
            $instanceId = $processData['id'];
            $groupProcessId = $processData['rootId'];
            $src = '/tools/embed/' . $this->container->getParameter('diagnosis_summary_tool') . '?groupprocessid=' . $groupProcessId;
            $uId = 'quiz-'.$processData['id'];
            $isActive = $processData['active'];
        }

        return $this->twig->render("@Case/FormTemplate/templates/quiz.html.twig",
            [
                'question' => $question,
                'answers' => $answers,
                'src' => $src,
                'choice_variant' => (isset($processData['choice_variant'])) ? $processData['choice_variant'] : null,
                'instanceId' => $instanceId,
                'isActive' => $isActive,
                'uId' => $uId,
                'controlDescription' => $controlOfDescriptionQuiz
            ]
        );

    }

    private function getFormControlHtmlProperty($formControlId, $processInstanceId){
        if($this->attributeConditionService->checkCondition('required', $formControlId, $processInstanceId)){
            return AttributeCondition::TYPE_REQUIRED;
        }
        else if($this->attributeConditionService->checkCondition('readOnly', $formControlId, $processInstanceId)){
            return AttributeCondition::TYPE_READ_ONLY;
        }
        return AttributeCondition::TYPE_NOT_REQUIRED;
    }

}