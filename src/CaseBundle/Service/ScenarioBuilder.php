<?php

namespace CaseBundle\Service;

use CaseBundle\Entity\Attribute;
use CaseBundle\Stages\Diagnosis;
use CaseBundle\Stages\NewCase;
use CaseBundle\Stages\Stage;
use CaseBundle\Stages\StagePoint;
use CaseBundle\Stages\ThePhoneIsRinging;
use CaseBundle\Stages\TowingOrFixing;
use CaseBundle\Utils\ScenarioConfig;
use Doctrine\ORM\EntityManager;
use MapBundle\Controller\DefaultController;
use OperationalBundle\Service\AttributeValidatorService;
use OperationalBundle\Service\OperationDashboardInterface;
use OperationalBundle\Service\UpdaterOfFormControlService;
use Sensio\Bundle\GeneratorBundle\Command\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

class ScenarioBuilder
{

    const USER_ID = 1;
    const ORIGINAL_USER_ID = 1;
    const DIAGNOSIS_PREFIX = '1140';

    const SERVICE_TOWING = 1;
    const SERVICE_FIXING = 2;

    static private $interactiveMode = false;

    /** @var SymfonyStyle */
    static public $ioSymfony;

    /** @var  QuestionHelper */
    static public $questionHelper;

    /** @var  InputInterface */
    static public $input;

    /** @var  OutputInterface */
    static public $output;

    /** @var  OperationDashboardInterface */
    static public $operationDashboardInterface;

    /** @var ScenarioConfig */
    static public $scenarioConfig = null;

//    /** @var  Translator */
//    static public $translator;

    /**
     * Dostępne scenariusze:
     *
     *  - założenie nowej sprawy z dzwoni telefon (SimpleNewCase)
     */

    /** @var ContainerInterface $container */
    protected $container;

    /** @var ProcessHandler $processHandler */
    protected $processHandler;

    /** @var  UpdaterOfFormControlService */
    protected $updaterControl;

    /** @var  EntityManager */
    protected $entityManager;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->processHandler = $container->get('case.process_handler');
    }

    /**
     * @param $nameScenario
     * @return \Exception | StagePoint
     */
    public function build($nameScenario)
    {

        try {
            return call_user_func(array($this, 'build_' . $nameScenario));
        } catch (\Exception $exception) {
            return $exception;
        }

    }

    protected function build_SimpleTowing() {

        /** Stworzenie nowej sprawy i odpalenie holowanie */
        $stageStartPoint = $this->build_SimpleNewCase();
        $stageStartPoint->expectedStep(['1011.010']);

        /** Odpalenie Holowania na jakimś istniejącym kroku */
//        $stageStartPoint = new StagePoint($this->processHandler, 43095);

        // -- blok tworzenia usługi
        $stage = new TowingOrFixing($this->processHandler);
        $processInstanceIds = $this->startService(self::SERVICE_TOWING, $stageStartPoint);
        $checkPoint = $this->getStagePoint($processInstanceIds);
        $stage->groupProcessInstanceId = $checkPoint->getGroupProcessId();
        // -- -----------------------

        if($checkPoint->checkStepId('1009.029')) {

            $partnersLocation = $this->getOptionsOfSelect(522, $stageStartPoint);

            $processInstanceIds = $stage->runStep($checkPoint->getId(), '029', [
                '522' => (!empty($partnersLocation)) ? reset($partnersLocation) : null
            ]);

            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '030');
        }

        $checkPoint = $this->getStagePoint($processInstanceIds);

        if($checkPoint->checkStepId('1009.031')) {
            $processInstanceIds = $stage->runStep($checkPoint->getId(), '031');
        }

        $checkPoint = $this->getStagePoint($processInstanceIds);

        if($checkPoint->checkStepId('1009.026')) {
            $processInstanceIds = $stage->runStep($checkPoint->getId(), '026');
        }

        $stageEndPoint = $this->getStagePoint($processInstanceIds);
        $stageEndPoint->expectedStep(['1011.010']);

        return $stageEndPoint;

    }

    protected function build_SimpleNewCase() {

        $stageStartPoint = $this->build_FromDiagnosisToServices();
        $stageStartPoint->expectedStep(['1011.010']);

        return $stageStartPoint;

    }

    protected function build_FromDiagnosisToServices()
    {

        $stageStartPoint = $this->build_Diagnosis();
        $stageStartPoint->expectedStep(['1011.017']);

        $stage = new NewCase($this->processHandler, $stageStartPoint);

        $processInstanceIds = $stage->runStep($stageStartPoint->getId(), '017');

        $checkPoint = new StagePoint($this->processHandler, $processInstanceIds);

        if($checkPoint->checkStepId('1011.040')) {
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '040');
        }

        $dmc = $this->processHandler->getAttributeValue('74,76', $stage->groupProcessInstanceId, 'int');

        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '004', [
            '74,76' => (empty($dmc)) ? 2050 : $dmc
        ]);

        $checkPoint = new StagePoint($this->processHandler, $processInstanceIds);

        if($checkPoint->checkStepId('1011.039')) {
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '039', [], 1, Stage::EMPTY_CONTROLS);
        }

        if($checkPoint->checkStepId('1011.052')) {
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '052');
        }

        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '037');

        $pathWithValue = [
            '101,85,92' => '',
            '101,85,93' => ''
        ];

        if(ScenarioBuilder::$scenarioConfig !== null){
            $pathWithValue['101,85,92'] = ScenarioBuilder::$scenarioConfig->getScenarioValue('1011.008', '101,85,92', '21.042629');
            $pathWithValue['101,85,93'] = ScenarioBuilder::$scenarioConfig->getScenarioValue('1011.008', '101,85,93', '52.239334');
        }

//        try {
            $infoAboutMap = $this->getLocationByPoints($pathWithValue['101,85,93'],$pathWithValue['101,85,92']);
//        }
//        catch (\Throwable $e) {
//            $infoAboutMap = [];
//        }

        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '008', array_merge($pathWithValue, $infoAboutMap));

        $stageEndPoint = new StagePoint($this->processHandler, $processInstanceIds);
        $stageEndPoint->expectedStep('1011.010');

        return $stageEndPoint;

    }


    /**
     * Diagnoza, najprostsza - aż do Wyniku diagnozy
     */

    protected function build_Diagnosis()
    {

        $stageStartPoint = $this->build_FromBasicInformationToDiagnosis();

        // TODO - że expected graf diagnozy
//        $stageStartPoint->expectedStep([self::DIAGNOSIS_PREFIX . '.107']);

        $stage = new Diagnosis($this->processHandler, $stageStartPoint);

        if(self::$scenarioConfig !== null && self::$scenarioConfig->hasDiagnosis()) {

            $diagnosis = self::$scenarioConfig->getDiagnosis();

            $processInstanceId = $stageStartPoint->getId();

            foreach ($diagnosis as $variant) {
                $ids = $stage->onlyNext($processInstanceId, $variant);
                $processInstanceId = $this->getFirstId($ids);
            }

        }
        else {

            $notEnd = true;
            $processInstanceId = $stageStartPoint->getId();

            while ($notEnd) {

                $processInstanceIds = $stage->onlyNext($processInstanceId);

                $stageCheckPoint = new StagePoint($this->processHandler, $processInstanceIds);
                if($stageCheckPoint->checkStepId('1011.017')) {
                    $notEnd = false;
                }

                $processInstanceId = $stageCheckPoint->getId();
            }

        }

//        $processInstanceIds = $stage->runStep($stageStartPoint->getId(), '107', [], 1, Stage::EMPTY_CONTROLS);
//        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '108', [], 1, Stage::EMPTY_CONTROLS);
//        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '109', [], 1, Stage::EMPTY_CONTROLS);
//        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '110', [], 1, Stage::EMPTY_CONTROLS);
//        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '111', [], 1, Stage::EMPTY_CONTROLS);
//        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '112', [], 1, Stage::EMPTY_CONTROLS);

        $stageEndPoint = new StagePoint($this->processHandler, $processInstanceId);
        $stageEndPoint->expectedStep('1011.017');

        return $stageEndPoint;

    }

    /**
     * Od potwierdzenia uprawnień do Diagnozy
     */

    protected function build_FromBasicInformationToDiagnosis()
    {

        $stageStartPoint = $this->build_SampleVehicleBasicInformation();
        $stageStartPoint->expectedStep('1011.003');

        $stage = new NewCase($this->processHandler, $stageStartPoint);

        if($stageStartPoint->checkAttributeValue('493', 'int', "0")) {
            $processInstanceIds = $stage->runStep($stageStartPoint->getId(), '003');
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds),'003', [], 1, Stage::EMPTY_CONTROLS);
        }
        else {
            $processInstanceIds = $stage->runStep($stageStartPoint->getId(), '003', [], 1, Stage::EMPTY_CONTROLS);

        }
        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '018');

        $stageEndPoint = new StagePoint($this->processHandler, $processInstanceIds);

        return $stageEndPoint;

    }

    /**
     * Wprowadzenie podstawowych danych pojazdu
     */

    protected function build_SampleVehicleBasicInformation()
    {

        $stageStartPoint = $this->build_VehicleBasicInformation();
        $stageStartPoint->expectedStep('1011.002');

        $stage = new NewCase($this->processHandler, $stageStartPoint);

        $processInstanceIds = $stage->runStep($stageStartPoint->getId(), '002', [
                '74,72' => 'po650ux',
                '74,71' => 'WVWZZZ3CZDE529235',
                '74,561' => '2012-09-21',
                '74,233' => '2012-09-21',
                '74,75' => '10000'
            ]
        );

        // Odpalenie Cepika ;)
        $this->processHandler->cepikAttributes($stageStartPoint->getGroupProcessId());

        $checkPoint = new StagePoint($this->processHandler, $processInstanceIds);

        if($checkPoint->checkStepId('1011.043')) {
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '043');
        }

        $stageEndPoint = new StagePoint($this->processHandler, $processInstanceIds);
        $stageEndPoint->expectedStep('1011.003');

        return $stageEndPoint;

    }

    /**
     * Do momentu - wprowadź dane podstawowe auta
     */

    protected function build_VehicleBasicInformation()
    {

        $stageStartPoint = $this->build_EmptyNewCase();
        $stageStartPoint->expectedStep('1011.006');

        $stage = new NewCase($this->processHandler, $stageStartPoint);

        $processInstanceIds = $stage->runStep($stageStartPoint->getId(), '006');
        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '015');

        $checkPoint = $this->getStagePoint($processInstanceIds);

        if($checkPoint->checkStepId('1011.007')) {
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '007');
        }

        $stageEndPoint = new StagePoint($this->processHandler, $processInstanceIds);
        $stageEndPoint->expectedStep('1011.002');

        return $stageEndPoint;

    }

    /**
     * Początek Nowej Sprawy
     */
    protected function build_EmptyNewCase()
    {

        $stageStartPoint = $this->build_SimplePhoneIsRinging();
        $stageStartPoint->expectedStep('1011.006');

        return $stageStartPoint;

    }

    /**
     * Prosty scenariusz Dzwoni Telefon - jako numer zastrzeżony
     */
    protected function build_SimplePhoneIsRinging()
    {

        $stage = new ThePhoneIsRinging($this->processHandler);

        $processInstanceId = $stage->createNewProcess();

        $processInstanceIds = $stage->runStep($processInstanceId, '002', [
//            '197' => $this->getRandomNumber()
        ]);

        $checkPoint = new StagePoint($this->processHandler, $processInstanceIds);

        if($checkPoint->checkStepId('1012.014')) {
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '014', [
                '81,342,64' => $this->generateRandomName(10, true),
                '81,342,66' => $this->generateRandomName(10, true),
                '81,342,408,197' => $this->getRandomNumber()
            ]);
        }
        elseif($checkPoint->checkStepId('1012.11')) {

            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '11', [
//                '404' => 155521   // ID konkretnej osoby
            ]);

            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '013', [
                '81,342,64' => $this->generateRandomName(10, true),
                '81,342,66' => $this->generateRandomName(10, true),
            ]);

        }
        elseif($checkPoint->checkStepId('1012.013')) {

            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '013', [
                '81,342,64' => $this->generateRandomName(10, true),
                '81,342,66' => $this->generateRandomName(10, true),
            ]);

        }
        elseif($checkPoint->checkStepId('1012.016')) {
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '016');
        }

        $checkPoint = new StagePoint($this->processHandler, $processInstanceIds);

        if($checkPoint->checkStepId('1012.017')) {
            $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '017');
        }

        $processInstanceIds = $stage->runStep($this->getFirstId($processInstanceIds), '006');

        $stageEndPoint = new StagePoint($this->processHandler, $processInstanceIds);

        return $stageEndPoint;

    }

    // -----------------------
    // Helpers
    // -----------------------

    protected function getOptionsOfSelect($attributeId, StagePoint $stagePoint) {

        if($this->updaterControl === null){
            $this->updaterControl = $this->container->get('updater.form.control');
        }

        if($this->entityManager === null) {
            $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
        }

        /** @var Attribute $attribute */
        $attribute = $this->entityManager->getRepository('CaseBundle:Attribute')->find($attributeId);

        if(!$attribute) return [];

        return $this->updaterControl->getOptions($stagePoint->getGroupProcessId(), $attribute->getQuery(), $stagePoint->getId());

    }

    /**
     * @param $processInstanceIds
     * @return StagePoint
     */
    protected function getStagePoint($processInstanceIds) {
        return new StagePoint($this->processHandler, $processInstanceIds);
    }

    protected function startService($serviceId, StagePoint $stagePoint) {

        $stagePoint->expectedStep(['1011.010']);

        switch ($serviceId) {
            case self::SERVICE_TOWING: {

                $this->container->get('case.service_handler')->runService('1009.034', $stagePoint->getId(), $stagePoint->getGroupProcessId());
                $stage = new NewCase($this->processHandler, $stagePoint);
                return $stage->onlyNext($stagePoint->getId(), 1);

                break;
            }
        }

        return null;

    }

    protected function setRoot($id, $rootId) {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $processInstance = $em->getRepository('CaseBundle:ProcessInstance')->find($id);
        $processInstanceRoot = $em->getRepository('CaseBundle:ProcessInstance')->find($rootId);

        if($processInstance && $processInstanceRoot) {
            $processInstance->setRoot($processInstanceRoot);
            $em->persist($processInstance);
            $em->flush();
        }
    }

    protected function getLocationByPoints($lat = '52.239334', $long = '21.042629') {

        $mapController = new DefaultController();
        $mapController->setContainer($this->container);
        $response = $mapController->getLocation(new Request(), $lat, $long);

        $arrayResponse = json_decode($response->getContent());

        if(empty($arrayResponse)) return [];

        $localizationPath = '101,85,';

        return [
            $localizationPath . '87' => $arrayResponse->city,
            $localizationPath . '94' => $arrayResponse->street,
            $localizationPath . '95' => $arrayResponse->streetNumber,
            $localizationPath . '88' => $arrayResponse->voivodeship,
            $localizationPath . '91' => $arrayResponse->county,
            $localizationPath . '90' => $arrayResponse->commune,
            $localizationPath . '86' => $arrayResponse->country,
            $localizationPath . '89' => $arrayResponse->zipCode,
            $localizationPath . '509' => $arrayResponse->road,
            $localizationPath . '541' => $arrayResponse->inCity
        ];

    }
    protected function getFirstId($ids)
    {
        if (is_array($ids)) {
            return (!empty($ids)) ? intval($ids[0]) : NULL;
        } else {
            return intval($ids);
        }
    }

    protected function generateRandomName($length = 10, $capitalize = false)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        if($capitalize) {
            return ucfirst($randomString);
        }

        return $randomString;
    }

    protected function getRandomNumber()
    {
        return rand(100000000, 999999999);
    }

    static public function isInteractiveMode()
    {
        return self::$interactiveMode;
    }

    static public function setInteractiveMode($mode = false)
    {
        self::$interactiveMode = $mode;
    }

}