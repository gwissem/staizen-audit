<?php

namespace CaseBundle\Service;

class IframeSrcParserService
{
    const PREFIX = '{@';
    const SUFFIX = '@}';
    private $parameters = [
        '{@groupprocessid@}'
    ];

    private $groupProcessInstanceId;

    public function setGroupProcessInstanceId($groupProcessInstanceId)
    {
        $this->groupProcessInstanceId = $groupProcessInstanceId;

        return $this;
    }

    public function parse(string $parameter): string
    {
        $pos = strpos($parameter, $this->parameters[0]);

        if ($pos !== false) {
            return $this->doParse($parameter);
        }

        return $parameter;
    }

    private function doParse($parameter): string
    {
        if (empty($this->groupProcessInstanceId)) {
            throw new \Exception('$groupProcessInstanceId is empty, before using parse()
                function should me declared in setGroupProcessInstanceId()');
        }

        return str_replace($this->parameters[0], $this->groupProcessInstanceId, $parameter);
    }
}