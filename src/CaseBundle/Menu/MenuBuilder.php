<?php

namespace CaseBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MenuBuilder
{
    private $factory;

    /** @var  EntityManager */
    private $em;
    private $requestStack;

    /** @var  ContainerInterface */
    private $container;

    private $translator;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(
        FactoryInterface $factory,
        $em,
        $requestStack,
        ContainerInterface $container,
        $translator
    ) {
        $this->factory = $factory;
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->container = $container;
        $this->translator = $translator;
    }

    public function createAdminProcessDefinitionMenu(array $options)
    {
        $authChecker = $this->container->get('security.authorization_checker');

        $currentRoute = $this->requestStack->getCurrentRequest()->get('_route');
        $id = $this->requestStack->getCurrentRequest()->get('id');

        $menu = $this->factory->createItem(
            'toolActionBar',
            array(
                'childrenAttributes' => array(
                    'class' => 'nav nav-tabs',
                    'id' => 'tool-action-bar',
                ),
            )
        );

        if ($authChecker->isGranted('ROLE_PROCESS_EDITOR')) {
            $definition = $menu->addChild(
                $this->translator->trans('Definicja'),
                array(
                    'route' => 'admin_process_definition_editor',
                    'routeParameters' => array(
                        'id' => $id,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );

            $attachedRoutes = array(
                'admin_process_definition_editor',
            );

            if (in_array($currentRoute, $attachedRoutes)) {
                $definition->setCurrent(true);
            }
        }

        if ($id) {
            $menu->addChild(
                $this->translator->trans('Projektowanie'),
                array(
                    'route' => 'admin_process_design_chart',
                    'routeParameters' => array(
                        'id' => $id,
                    ),
                    'attributes' => array(
                        'class' => 'nav-item',
                    ),
                    'linkAttributes' => array(
                        'class' => 'nav-link p-x-3',
                    ),
                )
            );


        } else {
            $this->addDisabledTab($menu, $this->translator->trans('Projektowanie'));

        }

        return $menu;
    }

    private function addDisabledTab($menu, $name, $class = "")
    {
        $menu->addChild(
            $name,
            array(
                'uri' => '#',
                'attributes' => array(
                    'class' => 'nav-item disabled '.$class,
                ),
                'linkAttributes' => array(
                    'class' => 'nav-link p-x-3',
                ),
            )
        );
    }
}