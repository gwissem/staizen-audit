<?php

namespace CaseBundle\Form\Type;

use ApiBundle\Entity\ApiCallJsonDefinition;
use ApiBundle\Repository\ApiCallJsonDefinitionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApiCallType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $groups = $options['groups'];

        $builder
            ->add(
                'groupProcessId',
                ChoiceType::class,
                [
                    'label' => 'case.form.group_process_id',
                    'choices' => $groups
                ]
            )
            ->add(
                'actions',
                EntityType::class,
                [
                    'class' => ApiCallJsonDefinition::class,
                    'query_builder' => function(ApiCallJsonDefinitionRepository $repository) {
                        return $repository->getRootDefinitions();
                    },
                    'label' => 'case.form.actions',
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect', 'multiple' => false],
                ]
            );
//            ->add(
//                'body',
//                TextareaType::class,
//                [
//                    'label' => 'case.form.body',
//                    'required' => true,
//                ]
//            );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'groups' => [],
            ]
        );
    }

}