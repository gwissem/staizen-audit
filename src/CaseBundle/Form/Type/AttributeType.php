<?php

namespace CaseBundle\Form\Type;

use CaseBundle\Entity\Attribute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

class AttributeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [
                    'fields' => [
                        'name' => [
                            'field_type' => TextType::class,
                            'label' => 'name',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => true,
                                    'constraints' => new NotBlank(),
                                ),
                            ),
                            //'required' => true,

                        ],
                        'validationDescription' => [
                            'field_type' => TextType::class,
                            'label' => 'validationDescription',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => false
                                ),
                            )
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                    'constraints' => [new Valid()],
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => 'type',
                    'required' => true,
                    'choices' => $options['types'],
                    'attr' => ['class' => 'select2-deselect'],
                ]
            )
            ->add(
                'isMulti',
                CheckboxType::class,
                [
                    'label' => 'is multi',
                    'required' => false,
                ]
            )
            ->add(
                'inputType',
                ChoiceType::class,
                [
                    'label' => 'app.form.form_control',
                    'required' => true,
                    'choices' => [
                        Attribute::INPUT_TYPE_TEXT => Attribute::INPUT_TYPE_TEXT,
                        Attribute::INPUT_TYPE_SELECT => Attribute::INPUT_TYPE_SELECT,
                        Attribute::INPUT_TYPE_MULTISELECT => Attribute::INPUT_TYPE_MULTISELECT,
                        Attribute::INPUT_TYPE_TEXTAREA => Attribute::INPUT_TYPE_TEXTAREA,
                        Attribute::INPUT_TYPE_CHECKBOX => Attribute::INPUT_TYPE_CHECKBOX,
                        Attribute::INPUT_TYPE_RADIO => Attribute::INPUT_TYPE_RADIO,
                        Attribute::INPUT_TYPE_NUMBER => Attribute::INPUT_TYPE_NUMBER,
                        Attribute::INPUT_TYPE_YESNO => Attribute::INPUT_TYPE_YESNO,
                        Attribute::INPUT_TYPE_DATETIME => Attribute::INPUT_TYPE_DATETIME,
                        Attribute::INPUT_TYPE_DATE => Attribute::INPUT_TYPE_DATE,
                        Attribute::INPUT_TYPE_UPLOAD => Attribute::INPUT_TYPE_UPLOAD,
                        Attribute::INPUT_TYPE_EMAIL => Attribute::INPUT_TYPE_EMAIL

                    ],
                    'attr' => ['class' => 'select2-deselect', 'placeholder' => 'mandatory'],
                ]
            )
            ->add(
                'defaultValue',
                TextType::class,
                [
                    'label' => 'app.form.default_value',
                    'attr' => ['placeholder' => 'default_value'],
                ]
            )
            ->add(
                'query',
                TextareaType::class,
                [
                    'label' => 'query',
                    'required' => false,
                    'attr' => [
                        'rows' => 10
                    ]
                ]
            )
//            ->add(
//                'regex',
//                TextareaType::class,
//                [
//                    'label' => 'regex',
//                    'required' => false,
//                ]
//            )
            ->add(
                'validationRule',
                TextareaType::class,
                [
                    'label' => 'validationRule',
                    'required' => false,
                ]
            )
            ->add(
                'active',
                CheckboxType::class,
                [
                    'label' => 'app.form.is_active',
                    'required' => false,
                ]
            );

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Attribute::class,
                'cascade_validation' => true,
                'types' => []
            ]
        );
    }

}