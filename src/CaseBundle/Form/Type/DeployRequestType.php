<?php

namespace CaseBundle\Form\Type;

use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessFlow;
use CaseBundle\Repository\ProcessDefinitionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;

class DeployRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add(
                'date',
                TextType::class,
                [
                    'label' => 'date',
                    'attr' => ['class' => 'date-time-picker']
                ]
            )
            ->add(
                'info',
                TextareaType::class,
                [
                    'label' => 'info',
                    'required' => false,
                ]
            );
    }



}