<?php

namespace CaseBundle\Form\Type;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\StepAttributeDefinition;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StepAttributeDefinitionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $parentAttributes = $options['parentAttributes'];
        $parentAttributesNumber = count($parentAttributes->getQuery()->getArrayResult());

        if ($parentAttributesNumber > 0) {
            $class = $parentAttributesNumber == 1 ? 'hidden' : '';
            $builder->add(
                'parentAttribute',
                EntityType::class,
                [
                    'query_builder' => $parentAttributes,
                    'class' => Attribute::class,
                    'label' => false,
                    'attr' => [
                        'class' => 'select2-deselect '.$class,
                        'placeholder' => 'app.form.processAttributeDefinition',
                    ],
                ]
            );
        }

        $builder
            ->add(
                'updatable',
                ChoiceType::class,
                [
                    'label' => 'type',
                    'required' => true,
                    'choices' => [
                        'app.form.updatable' => 1,
                        'app.form.not_updatable' => 0,
                    ],
                    'attr' => ['placeholder' => 'updatable'],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => StepAttributeDefinition::class,
                'parentAttributes' => null,
            ]
        );
    }

}