<?php

namespace CaseBundle\Form\Type;

use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\ProcessFlow;
use CaseBundle\Entity\Program;
use CaseBundle\Repository\ProcessDefinitionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProcessFlowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $processDefinitionId = $options['processDefinitionId'];

        $builder
            ->add(
                'variant',
                TextType::class,
                [
                    'label' => 'case.form.variant',
                ]
            )
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [
                    'fields' => [
                        'description' => [
                            'field_type' => TextareaType::class,
                            'label' => 'description',
                            'required' => false,

                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                ]
            )
            ->add(
                'programs',
                EntityType::class,
                [
                    'class' => Program::class,
                    'label' => 'case.form.programs',
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect', 'multiple' => true],
                    'multiple' => true,
                ]
            )
            ->add(
                'isPrimaryPath',
                CheckboxType::class,
                [
                    'label' => 'case.form.is_primary_path',
                    'required' => false,
                ]
            );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => ProcessFlow::class,
                'processDefinitionId' => null,
            ]
        );
    }

}