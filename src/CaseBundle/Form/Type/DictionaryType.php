<?php

namespace CaseBundle\Form\Type;

use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Types\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;
use ToolBundle\Entity\FormField;

class DictionaryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'description',
                TextType::class,
                [
                    'label' => 'description',
                    'attr' => ['readonly' => true],
                ]
            )
            ->add(
                'descriptionOrg',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'form.description_org',
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => 'type',
                    'choices' => [
                        'Tekst' => FormField::TYPE_TEXT,
                        'Liczba' => FormField::TYPE_NUMBER,
                        'Data' => FormField::TYPE_DATE,
                    ],
                ]
            )
            ->add(
                'isKey',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'form.is_key',
                ]
            );
        //->add('status', TextType::class)
        //->add('creationDate', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'CaseBundle\Entity\Dictionary',
            ]
        );
    }

}