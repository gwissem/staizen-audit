<?php

namespace CaseBundle\Form\Type;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\Program;
use CaseBundle\Entity\ServiceDefinition;
use CaseBundle\Entity\Step;
use CaseBundle\Repository\ServiceDefinitionRepository;
use CaseBundle\Repository\StepRepository;
use Doctrine\DBAL\Types\ArrayType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use SparxBundle\Repository\AttributesRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use UserBundle\Entity\Group;

class StepType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isNew = $options['isNew'];
        $processDefinitionId = $options['processDefinitionId'];
        $selectedServices = $options['selectedServices'];

        $builder
            ->add(
                'idPrefix',
                TextType::class,
                [
                    'mapped' => false,
                    'label' => false,
                    'data' => $processDefinitionId,
                    'attr' => ['readonly' => 'readonly'],
                ]
            )
            ->add(
                'idSuffix',
                TextType::class,
                [
                    'label' => 'id',
                    'attr' => ['readonly' => $isNew ? false : 'readonly']
                ]
            )
            ->add(
                'id',
                HiddenType::class,
                [
                    'label' => 'id',
                ]
            )
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [
                    'fields' => [
                        'name' => [
                            'field_type' => TextType::class,
                            'label' => 'name',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => true,
                                    'constraints' => [new NotBlank()],
                                ),
                            ),
                        ],
                        'description' => [
                            'field_type' => TextareaType::class,
                            'label' => 'description',
                            'required' => false,
                        ],
                        'forwardVariantInfo' => [
                            'field_type' => TextType::class,
                            'label' => 'forward.variant.info',
                            'required' => false,
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                        'info' => ['display' => false],
                    ],
                    'label' => false,
                ]
            )
            ->add(
                'isTechnical',
                CheckboxType::class,
                [
                    'label' => 'case.form.is_technical',
                    'required' => false,
                ]
            )

            ->add(
                'owner',
                EntityType::class,
                [
                    'class' => Group::class,
                    'label' => 'case.form.owner',
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect'],
                ]
            )
            ->add(
                'permittedGroups',
                EntityType::class,
                [
                    'class' => Group::class,
                    'label' => 'case.form.permitted_groups',
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect', 'multiple' => true],
                    'multiple' => true,
                ]
            )
            ->add(
                'programs',
                EntityType::class,
                [
                    'class' => Program::class,
                    'label' => 'case.form.programs',
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect', 'multiple' => true],
                    'multiple' => true,
                ]
            )
            ->add(
                'stepNameSchema',
                TextType::class,
                [
                    'label' => 'step name',
                    'attr' => [
                        'placeholder' => 'e.g. {@XXX,YYY,ZZZ@}',
                    ],
                    'required' => false,
                ]
            )
            ->add('excludedFormControls',ChoiceType::class,
                [
                    'label' => 'step.type.excluded_form_controls',
                    'choices'  => $options['controlsOfStep'],
                    'multiple' => true,
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect', 'multiple' => true]
                ]
            )
            ->add(
                'postponeCount',
                NumberType::class,
                [
                    'label' => 'postpone.count',
                    'required' => false,
                ]
            )
            ->add(
                'updateStatusService',
                EntityType::class,
                [
                    'class' => ServiceDefinition::class,
                    'query_builder' => function (ServiceDefinitionRepository $serviceDefinitionRepository) {
                        return $serviceDefinitionRepository->fetchServiceDefinition(true);
                    },
                    'label' => 'choice_services_for_update',
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect'],
                    'multiple' => true,
                    'mapped' => false,
                    'choice_value'  => 'id',
                    'data' => $selectedServices
                ]
            )
            ->add(
                'forwardVariant',
                NumberType::class,
                [
                    'label' => 'step.type.forward_variant',
                    'required' => false,
                ]
            )
            ->add(
                'priority',
                TextType::class,
                [
                    'label' => 'step.type.priority',
                    'required' => false,
                ]
            )
            ->add(
                'maxPriority',
                TextType::class,
                [
                    'label' => 'step.type.maxpriority',
                    'required' => false,
                ]
            )
            ->add(
                'priorityBoostInterval',
                TextType::class,
                [
                    'label' => 'step.type.priority.boost',
                    'required' => false,
                ]
            )
            ->add(
                'urgentPriorityBoost',
                TextType::class,
                [
                    'label' => 'step.type.priority.urgent.boost',
                    'required' => false,
                ]
            )
            ->add(
                'stepNameSchema',
                TextType::class,
                [
                    'label' => 'step name',
                    'attr' => [
                        'placeholder' => 'e.g. {@XXX,YYY,ZZZ@}',
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'stepNameSchema',
                TextType::class,
                [
                    'label' => 'step name',
                    'attr' => [
                        'placeholder' => 'e.g. {@XXX,YYY,ZZZ@}',
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'reopen',
                CheckboxType::class,
                [
                    'label' => 'step.type.reopen',
                    'required' => false,
                ]
            )
            ->add('stop', CheckboxType::class, [
                'label' => 'step.form.stop',
                'required' => false,
            ])
            ->add(
                'callSparx',
                CheckboxType::class,
                [
                    'label' => 'Call Sparx',
                    'required' => false,
                ]
            )
            ->add('canPrevious', CheckboxType::class,
                [
                    'label' => 'step.type.can_previous',
                    'required' => false
                ])
            ->add('isMobileForm', CheckboxType::class,
                [
                    'label' => 'step.type.is_mobile_form',
                    'required' => false
                ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            $step            = $event->getData();
            $form            = $event->getForm();
            $options         = $form->getConfig()->getOptions();
            $highestSuffixId = $options['highestIdSuffix'];

            if($highestSuffixId instanceof Step) {
                
                $idSuffix        = $highestSuffixId->getIdSuffix() + 1;

                if($step->getIdSuffix() == NULL) {
                    $step->setIdSuffix($idSuffix);
                }
            }

        });

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Step::class,
                'isNew' => null,
                'processDefinitionId' => null,
                'highestIdSuffix' => null,
                'controlsOfStep' => [],
                'selectedServices' => []
            ]
        );
    }

}