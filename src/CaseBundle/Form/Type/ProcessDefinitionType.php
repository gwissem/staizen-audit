<?php

namespace CaseBundle\Form\Type;

use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\ProcessDefinition;
use CaseBundle\Entity\Step;
use CaseBundle\Repository\AttributeRepository;
use CaseBundle\Repository\StepRepository;
use CaseBundle\Validator\Constraints\StepSchemaValidator;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProcessDefinitionType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locale = $options['locale'];
        $attributes = $this->setAttributesData($options['attributeDefinitions']);
        $processDefinitionId = $options['id'];

        $builder
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [
                    'fields' => [
                        'name' => [
                            'field_type' => TextType::class,
                            'label' => 'name',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => true,
                                    'constraints' => new NotBlank(),
                                ),
                            ),
                        ],
                        'cancelDescription' => [
                            'field_type' => TextareaType::class,
                            'label' => 'cancel.description',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => false
                                ),
                                'en' => array(
                                    'required' => false
                                ),
                            )
                        ],
                        'info' => [
                            'field_type' => TextareaType::class,
                            'label' => 'case.form.info',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => false
                                ),
                                'en' => array(
                                    'required' => false
                                ),
                            ),
                            'attr' => [
                                'class' => "tinymce-word",
                                "data-theme" => "atlas_extrasimple",
                            ],
                        ],
                        'additionalInfo' => [
                            'field_type' => TextareaType::class,
                            'label' => 'case.form.additional_info',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => false
                                ),
                                'en' => array(
                                    'required' => false
                                ),
                            ),
                            'attr' => [
                                'class' => "tinymce-word",
                                "data-theme" => "atlas_extrasimple",
                            ],
                        ],
                        'viewerInfo' => [
                            'field_type' => TextareaType::class,
                            'label' => 'case.form.text_case_viewer',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => false
                                ),
                                'en' => array(
                                    'required' => false
                                ),
                            ),
                            'attr' => [
                                'class' => "tinymce-word",
                                "data-theme" => "atlas_simple",
                            ],
                        ],
                        'fullInfo' => [
                            'field_type' => TextareaType::class,
                            'label' => 'case.form.full_info',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => false
                                ),
                                'en' => array(
                                    'required' => false
                                ),
                            ),
                            'attr' => [
                                'class' => "tinymce-word",
                                "data-theme" => "atlas_simple",
                            ],
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => 'typ procesu',
                    'choices' => $this->getTypes()
                ]
            )
            ->add(
                'cancelStep',
                EntityType::class,
                [
                    'class' => Step::class,
                    'query_builder' => function (StepRepository $repository) use ($processDefinitionId) {
                        return $repository->fetchStepsByProcessDefinitionId($processDefinitionId, true, '1011.98');
                    },
                    'label' => 'case.form.cancel.step',
                    'choice_label' => 'fullProcessName',
                    'required' => false,
                    'attr' => ['class' => 'select2-deselect']
                ]
            )
            ->add(
                'stepNameSchema',
                TextType::class,
                [
                    'label' => 'step name',
                    'attr' => [
                        'placeholder' => 'e.g. {@XXX,YYY,ZZZ@}',
                    ],
                    'required' => false,
                ]
            );
    }

    protected function setAttributesData($attributeDefinitions)
    {

        $attributes = new ArrayCollection();
        if ($attributeDefinitions) {
            foreach ($attributeDefinitions as $definition) {
                if (!$definition->getDeletedAt()) {
                    $attributes->add($definition->getAttribute());
                }
            }
        }

        return $attributes;
    }

    protected function getTypes()
    {
        return [
            'standard' => ProcessDefinition::TYPE_STANDARD,
            'quiz' => ProcessDefinition::TYPE_QUIZ
        ];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => ProcessDefinition::class,
                'locale' => null,
                'attributeDefinitions' => null,
                'allow_extra_fields' => true,
                'id' => null
            ]
        );
    }

}