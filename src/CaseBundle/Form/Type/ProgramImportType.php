<?php

namespace CaseBundle\Form\Type;

use AppBundle\Form\Type\EntityHiddenType;
use CaseBundle\Entity\Dictionary;
use CaseBundle\Entity\DictionaryPackage;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProgramImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $fileId = $options['fileId'];
        $importerId = $options['fileId'];

        $builder
            ->add(
                'dictionaries',
                CollectionType::class,
                [
                    'entry_type' => DictionaryType::class,
                    'label' => false,
                    'allow_add' => true,
                    'prototype' => true,
                ]
            )
//            ->add(
//                'platform',
//                EntityHiddenType::class,
//                array(
//                    'label' => false,
//                    'class' => 'CaseBundle\Entity\Platform',
//                )
//            )
            ->add('fileId', HiddenType::class, ['mapped' => false, 'data' => $fileId])
            ->add('importerId', HiddenType::class, ['mapped' => false, 'data' => $importerId]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => DictionaryPackage::class,
                'fileId' => null,
                'importerId' => null,
                'csrf_protection' => false,
                'allow_extra_fields' => true,
            )
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }

}