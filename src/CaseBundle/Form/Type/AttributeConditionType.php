<?php

namespace CaseBundle\Form\Type;

use CaseBundle\Entity\AttributeCondition;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Utils\Language;
use Symfony\Component\ExpressionLanguage\SyntaxError;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttributeConditionType extends AbstractType
{

    /**
     * @var AttributeParserService
     */
    protected $attributeParser;
    protected $validationMessage;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->attributeParser = $options['attributeParser'];

        $this->validationMessage = $options['validationMessage']??'';

        $builder
            ->add('visible', TextType::class, [
                'required' => false,
                'empty_data' => '',
                /** @Desc("Widoczność") */
                'label' => 'form.attribute_condition.visibility'
            ])
            ->add('required', TextType::class, [
                'required' => false,
                'empty_data' => '',
                /** @Desc("Wymagalność") */
                'label' => 'form.attribute_condition.required'
            ])
            ->add('semiRequired', TextType::class, [
                'required' => false,
                'empty_data' => '',
                /** @Desc("Pół-wymagalność") */
                'label' => 'form.attribute_condition.semi_req'
            ])
            ->add('readOnly', TextType::class, [
                'required' => false,
                'empty_data' => '',
                /** @Desc("Tylko do odczytu") */
                'label' => 'form.attribute_condition.read_only'
            ])
            ->add('rules', TextType::class, [
                'required' => false,
                'empty_data' => '',
                /** @Desc("Reguły walidacji") */
                'label' => 'form.attribute_condition.rules'
            ])
            ->add('customValidationMessage', TextType::class,[
                'label'=> 'form.attribute_condition.custom_validation_message',
                'mapped'=>false,
                'data'=>$this->validationMessage,
                'required'=>false
        ])
            ->add('notAllowEdit', TextType::class, [
                'required' => false,
                'empty_data' => '',
                /** @Desc("Czy edytowalny na podglądzie") */
                'label' => 'form.attribute_condition.not_allow_edit'
            ]);


        $builder->get('visible')->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'validationExpresion'));
        $builder->get('required')->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'validationExpresion'));
        $builder->get('semiRequired')->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'validationExpresion'));
        $builder->get('readOnly')->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'validationExpresion'));
        $builder->get('rules')->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'validationExpresion'));
        $builder->get('notAllowEdit')->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'validationExpresion'));

    }

    public function validationExpresion(FormEvent $event) {

        $element = $event->getForm();
        $condition = $event->getData();

        if (!empty($condition)) {

            try {

                $condition = str_replace('{#value#}', "", $condition);
                $condition = str_replace('{#path#}', "0", $condition);

                $langExpression = new Language();
                $langExpression->evaluate($this->attributeParser->parseString($condition, false, false, null, false, true));

            } catch (\Exception $exception) {

                $element->addError(new FormError($exception->getMessage()));

            }

        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => AttributeCondition::class,
                'attributeParser' => null,
                'validationMessage' =>null
            ]
        );
    }
}