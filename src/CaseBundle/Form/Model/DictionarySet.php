<?php
// src/Acme/ApiBundle/Entity/Client.php

namespace CaseBundle\Form\Model;

use CaseBundle\Entity\Dictionary;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

class DictionarySet
{
    protected $dictionaries;

    public function __construct($dictionaries, $program)
    {
        $this->dictionaries = $dictionaries;
        $this->program = $program;
    }


    /**
     * @return ArrayCollection
     */
    public function getDictionaries()
    {
        return $this->dictionaries;
    }

    /**
     * @param ArrayCollection $dictionaries
     * @return DictionarySet
     */
    public function setDictionaries($dictionaries)
    {
        $this->dictionaries = $dictionaries;

        return $this;
    }

    public function addDictionary(Dictionary $dictionary)
    {
        $this->dictionaries[] = $dictionary;

        return $this;
    }
}