<?php

namespace CaseBundle\Listener;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Reader\XLSX\Reader;
use CaseBundle\Entity\Dictionary;
use CaseBundle\Entity\DictionaryPackage;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\File;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Oneup\UploaderBundle\Event\PreUploadEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Router;

class FormTemplateImageUploadListener
{
    const PATH_IMAGES = '/../web/files/form_template_images';
    const WEB_PATH_IMAGES = '/files/form_template_images';

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var ContainerInterface
     */
    private $container;

    /** @var Router $router */
    private $router;

    public function __construct(EntityManager $em, ContainerInterface $container, Router $router)
    {
        $this->em = $em;
        $this->container = $container;
        $this->router = $router;
    }

    public function onPreUpload(PreUploadEvent $event)
    {
        $directory = $this->container->getParameter('kernel.root_dir') . self::PATH_IMAGES;
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    public function onUpload(PostPersistEvent $event)
    {

        /** @var \Symfony\Component\HttpFoundation\File\File $file */
        $file = $event->getFile();
        $fileName = $file->getFilename();

        $folderPath = sprintf('/%s/%s/', substr($fileName, 0, 2), substr($fileName, 2, 2));

        $path = self::WEB_PATH_IMAGES . $folderPath . $fileName;

        $response = $event->getResponse();
        $response['path'] = $path;

        return $response;

    }

}