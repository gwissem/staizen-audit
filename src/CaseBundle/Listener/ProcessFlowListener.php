<?php

namespace CaseBundle\Listener;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\ProcessFlow;
use CaseBundle\Entity\Step;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ProcessFlowListener
{
    /** @var QueryManager */
    private $qm;

    public function __construct(QueryManager $qm)
    {
        $this->qm = $qm;
    }

    public function postPersist(ProcessFlow $processFlow, LifecycleEventArgs $args)
    {

        if(!($processFlow->getStep() instanceof Step)) {
            return;
        }

        $procedureName = 's_' . str_replace('.', '_', $processFlow->getStep()->getId());

        if(true === $this->qm->existsProcedure($procedureName)) {
            return;
        }

         $this->qm->executeProcedure(
             'CREATE PROCEDURE [dbo].[' . $procedureName .']
( 
    @previousProcessId INT,
    @variant TINYINT OUTPUT, 
    @currentUser INT, 
    @errId INT = 0 OUTPUT
) 
AS 
BEGIN
    
    SET NOCOUNT ON
    
    DECLARE @err INT
    DECLARE @message NVARCHAR(255)
    DECLARE @rootId INT
    DECLARE @groupProcessInstanceId INT
    DECLARE @values Table (id INT, value_int INT, value_string NVARCHAR(255), value_text NVARCHAR(MAX), value_date DATETIME, value_decimal DECIMAL(18,5))
    
    SELECT @groupProcessInstanceId = p.group_process_id, @rootId = p.root_id
    FROM dbo.process_instance p WITH(NOLOCK)
    WHERE p.id = @previousProcessId 
    
END',
             [], false, false);
    }
}
