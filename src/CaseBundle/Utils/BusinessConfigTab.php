<?php

namespace CaseBundle\Utils;


use CaseBundle\Entity\BusinessConfig;

class BusinessConfigTab
{

    /** @var  string */
    public $name;

    /** @var BusinessConfig[] */
    public $configs;

    /**
     * BusinessConfigTab constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->configs = [];
    }

    /**
     * @param $config
     */
    public function addConfig($config) {
        $this->configs[] = $config;
    }

}