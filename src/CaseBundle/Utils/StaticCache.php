<?php

namespace CaseBundle\Utils;

use CaseBundle\Entity\ProcessInstance;
use Predis\Client;

class StaticCache {

    private $cachedAttributes = [];
    private $cachedFunctions = [];
    private $isCli = false;

    /** @var  Client */
    private $redis;

    static $counter = 0;

    const NOT_EXISTS = '__NOT_EXISTS__';

    function __construct(Client $client = null)
    {

        if (php_sapi_name() == "cli") {
            $this->isCli = true;
        }

        $this->redis = $client;
    }

    public function getAll() {
        return $this->cachedAttributes;
    }

    /**
     * @param $path
     * @param $groupProcessId
     * @param $valueType
     * @return bool|mixed
     */
    public function getAttrValue($path, $groupProcessId, $valueType) {

        if($this->isCli) return false;

        $key = $path . '_' . $groupProcessId . '_' . $valueType;

        if(isset($this->cachedAttributes[$key])) {
            self::$counter++;
            return $this->cachedAttributes[$key];
        }

        return false;

    }

    /**
     * @param $path
     * @param $groupProcessId
     * @param $valueType
     * @param $value
     * @return $this
     */
    public function setAttrValue($path, $groupProcessId, $valueType, $value) {

        $this->cachedAttributes[$path . '_' . $groupProcessId . '_' . $valueType] = $value;
        return $this;

    }

    /**
     * @param $path
     * @param $groupProcessId
     * @param $valueType
     */
    public function delAttrValue($path, $groupProcessId, $valueType) {

        if(isset($this->cachedAttributes[$path . '_' . $groupProcessId . '_' . $valueType])) {
            unset($this->cachedAttributes[$path . '_' . $groupProcessId . '_' . $valueType]);
        }

        if(isset($this->cachedAttributes[$path . '_' . $groupProcessId . '_'])) {
            unset($this->cachedAttributes[$path . '_' . $groupProcessId . '_']);
        }
    }

    /**
     * @param $functionName
     * @param $args
     * @return array|string
     */
    public function getCachedFunction($functionName, $args) {

        $key = $this->keyOfFunction($functionName, $args);

        if(isset($this->cachedFunctions[$key])) {
            self::$counter++;
            return [$this->cachedFunctions[$key], $key];
        }

        return [self::NOT_EXISTS, $key];

    }

    /**
     * @param $functionName
     * @param $args
     * @return string
     */
    private function keyOfFunction($functionName, $args) {

        $key = $functionName;

        foreach ($args as $arg) {
            if($arg instanceof ProcessInstance) {
                $key .= '_' . $arg->getId();
            }
            else {
                $key .= '_' . $arg;
            }
        }

        return hash('sha512', $key);

    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setCachedFunction($key, $value) {

        $this->cachedFunctions[$key] = $value;
        return $this;
    }

}
