<?php

namespace CaseBundle\Utils;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class ScenarioConfig
{

    const SCENARIOS_PATH = __DIR__ . '/../../../app/Resources/scenarios/';

    private $config = [];

    function __construct($configSource = null)
    {
        if($configSource === null) return false;

        if(is_string($configSource)) {

            $fs = new Filesystem();

            if($fs->exists(self::SCENARIOS_PATH . $configSource . '.yml')) {

                try {
                    $this->config = Yaml::parse(file_get_contents(self::SCENARIOS_PATH . $configSource . '.yml'));
                } catch (ParseException $e) {
                    throw new InvalidConfigurationException(printf("Unable to parse the YAML string: %s", $e->getMessage()));
//                    printf("Unable to parse the YAML string: %s", $e->getMessage());
                }

            }
            else {
                throw new NotFoundResourceException('Nie znaleziono konfiguracji scenariusza.');
            }

        }
        else if(is_array($configSource)) {
            $this->config = $configSource;
        }
    }


    public function getScenarioValue($step, $path, $default = null) {
        if(isset($this->config[$step]) && isset($this->config[$step][$path]) && isset($this->config[$step][$path]['value'])) {
            return $this->config[$step][$path]['value'];
        }
        elseif(isset($this->config['global'][$path]['value'])) {
            return $this->config['global'][$path]['value'];
        }

        return $default;
    }

    public function isAutoDiagnosis() {

        if(isset($this->config['diagnosis']) && key_exists('auto', $this->config['diagnosis'])) {
            return filter_var($this->config['diagnosis']['auto'], FILTER_VALIDATE_BOOLEAN);
        }

        return false;

    }

    public function hasDiagnosis() {

        return (isset($this->config['diagnosis']) && key_exists('values', $this->config['diagnosis']));

    }

    public function getDiagnosis() {

        if(isset($this->config['diagnosis']) && key_exists('values', $this->config['diagnosis'])) {
            return $this->config['diagnosis']['values'];
        }

        return [];
    }

    public function getConfig() {
        return $this->config;
    }
}