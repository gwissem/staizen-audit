<?php

namespace CaseBundle\Utils;

use AppBundle\Entity\Config;
use AppBundle\Entity\ValueDictionary;
use AppBundle\Repository\ValueDictionaryRepository;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\Dictionary;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Program;
use CaseBundle\Entity\ServiceStatus;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Service\ProcessHandler;
use Doctrine\Common\Collections\Expr\Value;
use Doctrine\ORM\EntityManager;
use Moment\Moment;
use OperationalBundle\Service\OperationDashboardInterface;
use PDO;
use ReflectionMethod;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints\DateTime;
use UserBundle\Entity\User;
use UserBundle\Entity\Vip;
use UserBundle\Service\UserInfoService;
use Yasumi\Yasumi;

class ParserMethods
{

    /** @var QueryManager */
    protected $queryManager;

    /** @var EntityManager */
    protected $entityManager;

    /** @var ProcessHandler $processHandler */
    protected $processHandler;

    /** @var AttributeParserService $processHandler */
    protected $attributeParser;

    /** @var RequestStack */
    protected $requestStack;

    /** @var UserInfoService  */
    protected $userInfo;

    /** @var AuthorizationChecker $authorizationChecker */
    protected $authorizationChecker;

    /** @var ParameterBagInterface */
    protected $parameterBag;

    /** @var OperationDashboardInterface */
    protected $operationDashboardInterface;

    /** @var ValueDictionaryRepository */
    protected $dictionaryRepository;

    const SERVICE_STATUS_TOWING = 39;
    const CASE_NUMBER_LEN = 8;

    static $availableConfigKeys = [];

    public function __construct(QueryManager $queryManager, RequestStack $requestStack, EntityManager $entityManager,
                                ProcessHandler $processHandler, UserInfoService $userInfo, AuthorizationChecker $authorizationChecker,
                                ParameterBagInterface $parameterBag, OperationDashboardInterface $operationDashboardInterface)
    {
        $this->queryManager = $queryManager;
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->processHandler = $processHandler;
        $this->userInfo = $userInfo;
        $this->authorizationChecker = $authorizationChecker;
        $this->parameterBag = $parameterBag;
        $this->operationDashboardInterface = $operationDashboardInterface;
    }

    public function setAttributeParser(AttributeParserService $attributeParser)
    {
        $this->attributeParser = $attributeParser;
    }

    public function hello($groupProcessInstanceId)
    {
        return 'Hello';
    }

    public function isVirtualDashboard()
    {

        return ($this->requestStack->getMasterRequest()->headers->get('Process-Token', null) !== null);

    }

    public function showDatetime($date = null, $withTime = 0, $format = 'd-m-Y')
    {

        if (empty($date)) return null;

        try {

            $withTime = intval($withTime);
            $parsedDate = new \DateTime($date);
            $customFormat = $format;

            if ($withTime && $format === 'd-m-Y') {
                $customFormat .= ' H:i';
            }

            return $parsedDate->format($customFormat);

        } catch (\Exception $e) {
            return null;
        }

    }

    public function upper($string)
    {
        return strtoupper($string);
    }

    public function diffMinute($date, $withMark = true)
    {

        if ($withMark === null) $withMark = true;
        else {
            $withMark = filter_var($withMark, FILTER_VALIDATE_BOOLEAN);
        }

        try {
            $now = new \DateTime();
            $toTime = strtotime($date);

            if($withMark) {
                $plusOrMinus = ($now->getTimestamp() < $toTime) ? "<span style='color:green;font-weight: 700'>+" : "<span style='color:red;font-weight: 700'>-";
            }
            else {
                $plusOrMinus = "<span>";
            }

            return $plusOrMinus . round(abs($toTime - $now->getTimestamp()) / 60, 0) . ' min.</span>';

        } catch (\Exception $e) {
            return null;
        }

    }

    public function displayLocalizationMini($groupProcessInstanceId, $path = "")
    {
        $parameters = [
            [
                'key' => 'attributePath',
                'value' => $path,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locationString',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 1000,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_location_mini_string @attributePath = :attributePath, @groupProcessInstanceId = :groupProcessInstanceId, @locationString = :locationString",
            $parameters
        );

        if (isset($output['locationString'])) {
            return $output['locationString'];
        }

        return '';
    }


    public function displayLocalization($groupProcessInstanceId, $path = "101,85")
    {

        if (empty($path)) $path = "101,85";

        $parameters = [
            [
                'key' => 'attributePath',
                'value' => $path,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locationString',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 400,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_location_string @attributePath = :attributePath, @groupProcessInstanceId = :groupProcessInstanceId, @locationString = :locationString",
            $parameters
        );

        if (isset($output['locationString'])) {
            return $output['locationString'];
        }

        return '';

    }

    /**
     * @param $processInstanceId
     * @return int
     */
    public function isActive($processInstanceId)
    {

        /** @var ProcessInstance $processInstance */
        $processInstance = $this->entityManager->getRepository(ProcessInstance::class)->find($processInstanceId);

        if($processInstance) {
            return $processInstance->getActive();
        }

        return 0;

    }

    public function processInfo($groupProcessInstanceId, $step = null, $column = 'created_at')
    {

        if(empty($step)){
            $step = null;
        }

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
        ];
        $query = "SELECT TOP 1 * FROM dbo.process_instance WHERE group_process_id = :groupProcessInstanceId order by id";
        if($step){
            $parameters[] =
                [
                    'key' => 'step',
                    'value' => $step,
                    'type' => PDO::PARAM_STR
                ];
            $query = "SELECT TOP 1 * FROM dbo.process_instance WHERE group_process_id = :groupProcessInstanceId AND step_id = :step  order by id";
        }

        $result = $this->queryManager->executeProcedure(
            $query,
            $parameters
        );

        $data = null;

        if (!empty($result)) {
            if (key_exists($column, $result[0])) {
                $data = $result[0][$column];
            } else {
                $data = "Column don't exists.";
            }
        }

        return $data;

    }

    /**
     * @param $groupId
     * @param string $path
     * @return string
     */
    public function getPartnerName($groupId, $path = '522') {
        return $this->partnerName($groupId, $path, 1);
    }

    /**
     * @param $groupProcessInstanceId
     * @param $path
     * @param int $usePath
     * @return string
     */
    public function partnerName($groupProcessInstanceId, $path, $usePath = 1)
    {

        if (!is_string($path) && !is_numeric($path)) return '';

        if ($usePath !== 1) {
            $usePath = filter_var($usePath, FILTER_VALIDATE_BOOLEAN);
        }

        if ($usePath) {
            $partnerLocationId = $this->processHandler->getAttributeValue($path, $groupProcessInstanceId, 'int');
        } else {
            $partnerLocationId = $path;
        }

        $parameters = [
            [
                'key' => 'partnerId',
                'value' => (int)$partnerLocationId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_partnerName(:partnerId)",
            $parameters
        );

        return count($output) > 0 ? (($output[0][''] === null) ? '""' : $output[0][''])  : '""';

    }

    public function partnerAdress($groupProcessInstanceId, $PartnerLocationId)
    {
        if (!is_string($PartnerLocationId) && !is_numeric($PartnerLocationId)) return '';

        $parameters = [
            [
                'key' => 'partnerLocationId',
                'value' => (int)$PartnerLocationId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_partnerAdress(:partnerLocationId)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';
    }


    public function partnerStarter24Code($groupProcessInstanceId, $PartnerLocationId)
    {
        if (!is_string($PartnerLocationId) && !is_numeric($PartnerLocationId)) return '';

        $parameters = [
            [
                'key' => 'partnerLocationId',
                'value' => (int)$PartnerLocationId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_PartnerLocation_starter24Code(:partnerLocationId)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';
    }

    public function towingDestination($groupProcessInstanceId, $pathToPartnerLocationId = '522', $pathToCustomLocation = '211,85')
    {

        if (!is_string($pathToPartnerLocationId) && !is_numeric($pathToPartnerLocationId) && !$pathToCustomLocation) return '';

        $partnerLocationId = $this->processHandler->getAttributeValue($pathToPartnerLocationId, $groupProcessInstanceId, 'int');
        $partnerLocationId = empty($partnerLocationId) ? NULL : $partnerLocationId;

        $parameters = [
            [
                'key' => 'partnerId',
                'value' => $partnerLocationId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'attributePath',
                'value' => $pathToCustomLocation,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ],
            [
                'key' => 'locationString',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_towing_destination @partnerId = :partnerId, @groupProcessInstanceId = :groupProcessInstanceId, @attributePath = :attributePath, @output = :locationString",
            $parameters
        );

        return count($output) > 0 ? $output['locationString'] : '';

    }


    /***
     * Wyciąganie danych partnera po attribute_path
     * Przykład użycia:
     * {#partnerLocationInfoDetail(610|595,597,83)#}
     * {#partnerLocationInfoDetail(522|595,597,83)#}
     * {#partnerLocationInfoDetail(522|595,597,85)#}
     *
     * @param $groupProcessInstanceId
     * @param $partnerLocationPath
     * @param $attrPath
     * @param $valueType
     * @return string
     */
    public function partnerLocationInfoDetail($groupProcessInstanceId, $partnerLocationPath, $attrPath): string
    {
        $partnerLocationId = $this->processHandler->getAttributeValue($partnerLocationPath, $groupProcessInstanceId, 'int');
        if (is_array($partnerLocationId) && empty($partnerLocationId)) {
            $root = $this->processHandler->getRootId($groupProcessInstanceId);
            $partnerLocationId = $this->processHandler->getAttributeValue($partnerLocationPath, $root, 'int');
        }
        if ($partnerLocationId)
            try {
                $results = $this->partnerLocationInfo($partnerLocationId);
                if(array_key_exists($attrPath,$results)) {
                    $item = $results[$attrPath];
                    foreach ($item as $column => $value) {
                        if (strpos($column, 'value_') !== false) {
                            if (!is_null($value)) {
                                return strval($value);
                            }
                        }
                    }
                }
            } catch (\Exception $exception) {
                return $exception->getMessage();
            }
        return '';
    }


    public function rentalRequestInformations($groupProcessInstanceId)
    {

        $dictionaryRepo = $this->getDictionaryRepo();

        $content = '';
        $gearbox = $this->processHandler->getAttributeValue('283,431', $groupProcessInstanceId, 'int');
        $engineType = $this->processHandler->getAttributeValue('283,120', $groupProcessInstanceId, 'int');


        $childChair = $this->processHandler->getAttributeValue('283,284', $groupProcessInstanceId, 'int');
        $isoFix = $this->processHandler->getAttributeValue('283,121', $groupProcessInstanceId, 'int');

        $others = $this->processHandler->getAttributeValue('283,785', $groupProcessInstanceId, 'string');
        $abroadTrip = $this->processHandler->getAttributeValue('577', $groupProcessInstanceId, 'string');


        if ($gearbox) {
            /** @var ValueDictionary $dictionaryValue */
            $dictionaryValue = $dictionaryRepo->findOneBy([
                'type' => 'gearbox',
                'value' => $gearbox
            ]);

            if ($dictionaryValue) {
                $gearboxText = 'Skrzynia biegów: ' . $dictionaryValue->getText();

            }
        }


        if ($engineType) {
            $dictionaryValue = $dictionaryRepo->findOneBy([
                'type' => 'engineType',
                'value' => $engineType
            ]);
            if ($dictionaryValue) {
                $engineTypeText = 'Rodzaj silnika: ' . $dictionaryValue->getText();
            }
        }

        if ($gearbox) {
            $content = $gearboxText;
        }

        if ($engineType) {

            $content .= ($content != '') ? ' / ' . $engineTypeText : $engineTypeText;
        }

        if ($childChair) {
            $content .= ($content != '') ? ' / ' : '';
            $content .= 'Fotelik : TAK';
        }

        if ($isoFix) {
            $content .= ($content != '') ? ' / ' : '';
            $content .= 'IsoFix : TAK';

        }

        if ($others) {
            $list = explode(',', $others);

            $othersText = '';
            $othersArray = $dictionaryRepo->createQueryBuilder('e')
                ->where("e.type = 'vehicleFeatures'")
                ->andWhere("e.value IN (:values)")
                ->setParameter('values', $list)
                ->getQuery()
                ->getResult();

            /** @var ValueDictionary $otherOption */
            foreach ($othersArray as $otherOption) {
                $othersText .= ($othersText == '' ? $otherOption->getText() : ' / ' . $otherOption->getText());
            }

        }

        if ($others) {
            $content .= ($content != '') ? ' / ' : '';
            $content .= $othersText;
        }



        if (!is_null($abroadTrip) && !empty($abroadTrip)) {
            $content .= ($content != '') ? ' / ' : '';
            $content .= '<b>Wyjazd za granicę</b>';
        }


        return $content;


    }


    /**
     * $type = 0    - default
     * $type = 1    - dla Viewera (RENAULT)
     *
     * @param $groupProcessInstanceId
     * @param $pathToPartnerLocationId
     * @param int $type
     * @return string
     */
    public function partnerNameFull($groupProcessInstanceId, $pathToPartnerLocationId, $type = 0)
    {

        if (!is_string($pathToPartnerLocationId) && !is_numeric($pathToPartnerLocationId)) {
            return '';
        }

        $partnerLocationId = $this->processHandler->getAttributeValue($pathToPartnerLocationId, $groupProcessInstanceId, 'int');

        $parameters = [
            [
                'key' => 'partnerId',
                'value' => (int)$partnerLocationId,
                'type' => PDO::PARAM_INT
            ]
        ];

        if($type === '1') {
            $output = $this->queryManager->executeProcedure(
                "SELECT dbo.f_partnerNameFullRenault(:partnerId)",
                $parameters
            );
        }
        else {
            $output = $this->queryManager->executeProcedure(
                "SELECT dbo.f_partnerNameFull(:partnerId)",
                $parameters
            );
        }


        return count($output) > 0 ? $output[0][''] : '';

    }


    public function partnerContact($groupProcessInstanceId, $pathToPartnerLocationId, $serviceType, $contactType)
    {

        if (!is_string($pathToPartnerLocationId) && !is_numeric($pathToPartnerLocationId)) return '';

        $partnerLocationId = $this->processHandler->getAttributeValue($pathToPartnerLocationId, $groupProcessInstanceId, 'int');

        $parameters = [
            [
                'key' => 'partnerId',
                'value' => (int)$partnerLocationId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceType',
                'value' => (int)$serviceType,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'contactType',
                'value' => (int)$contactType,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_partner_contact(:groupProcessInstanceId, :partnerId, :serviceType, :contactType)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';

    }


    public function partnerPhoneNumber($groupProcessInstanceId, $partnerId)
    {
        $partnerId = $this->processHandler->getAttributeValue($partnerId, $groupProcessInstanceId, 'int');

        $parameters = [
            [
                'key' => 'partnerId',
                'value' => (int)$partnerId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_partnerPhoneNumber(:partnerId)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';
    }

    // Wyświetla koszt po dwóch miejscach po przecinku 0.00
    public function showCost($groupProcessInstanceId, $costValue)
    {
        return number_format((float)$costValue, 2, '.', '');
    }

    public function diagnosisDescription($groupProcessInstanceId, $withPrefix = false, $locale = 'pl')
    {

        if ($withPrefix !== false) $withPrefix = filter_var($withPrefix, FILTER_VALIDATE_BOOLEAN);

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'locale',
                'value' => $locale,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_diagnosis_description(:groupProcessInstanceId,:locale)",
            $parameters
        );

        $diagnosis = count($output) > 0 ? $output[0][''] : '';
        $prefix = '';

        if ($withPrefix) {

            $eventTypeId = $this->processHandler->getAttributeValue('491', $groupProcessInstanceId, 'int');
            $eventType = $this->entityManager->getRepository(ValueDictionary::class)->findOneBy(
                [
                    'type' => 'eventType',
                    'value' => $eventTypeId
                ]
            );

            if($eventType){
                $prefix = $eventType->getText();
            }

            return $prefix . ' ('.$diagnosis.')';

        } else {
            return $diagnosis;
        }

    }

    public function showICSMap($icsId, $width = "100%", $height="400")
    {

        if ($icsId != -1 && !empty($icsId) && $icsId != '""'){

            $parameters = [
                [
                    'key' => 'icsId',
                    'value' => $icsId,
                    'type' => PDO::PARAM_INT
                ],
            ];

            $output = $this->queryManager->executeProcedure(
                "SELECT number FROM AtlasDB_ics.dbo.orders where id = :icsId",
                $parameters
            );

            $number =  count($output) > 0 ? $output[0]['number'] : 0;

            return '<iframe src="/map/order/'.$number.'" height="'.$height.'" width="'.$width.'" frameborder="0"></iframe>';
//            return '<iframe src="http://teka.starter24.pl/Sprawa.php?nrSprawy=U498751&amp;teka=2&amp;uid=727334B1-73FD-4D3E-9088-D6DBD4ACA529" width="300" height="300" frameborder="0"></iframe>';
        }

        return '';
    }

    /**
     * Sprawdza pierwszy warunek a następnie w zależonści od warunku wyświetla pierwszą lub drugą date
     *
     * @param $conditionDate
     * @param $firstDate
     * @param $secondDate
     * @return mixed
     */
    public function estimatedDate($conditionDate, $firstDate = null, $secondDate = null)
    {

        if (!$firstDate && !$secondDate) {
            return '';
        }

        if ($conditionDate === "" || $conditionDate === '""') {
            $conditionDate = 0;
        }

        if (intval($conditionDate)) {
            return $firstDate;
        }

        return $secondDate;

    }

    /**
     * COMPARE TWO DATES
     * D1 > D2 =1
     * D1  == D2  =0
     * D! <D2 = -1
     * @param $firstDate
     * @param $secondDate
     * @return int
     */
    public function compareDates($firstDate, $secondDate){
        $firstDate = strtotime($firstDate);
        $secondDate = strtotime($secondDate);
        if($firstDate> $secondDate){
            return 1;
        }else if($firstDate == $secondDate){
            return 0 ;
        }else{
            return -1;
        }
    }

    public function travelLimitText($groupProcessInstanceId){
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],

        ];

        $output = $this->queryManager->executeProcedure(
            " EXEC p_travel_limit_text @groupProcessInstanceId = :groupProcessInstanceId",
            $parameters
        );

        return $output[0]['text'];
    }

    /**
     * @param $groupProcessInstanceId
     * @param $key
     * @return mixed
     */
    public function rentalSummary($groupProcessInstanceId, $key)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],

        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_replacement_car_summary @groupProcessInstanceId = :groupProcessInstanceId, @final = 0, @useTempTable = 0",
            $parameters
        );

        return $output[0][$key];

    }

    public function serviceHadStatus($groupProcessInstanceId, $status)
    {

        $statuses = $this->entityManager->getRepository(ServiceStatus::class)->findBy(['groupProcessId' => $groupProcessInstanceId, 'statusDictionaryId' => $status]);

        if (!empty($statuses)) {
            return 1;
        }
        return 0;

    }

    public function partnerReason($groupProcessInstanceId, $partnerId)
    {

        $parameters = [
            [
                'key' => 'partnerId',
                'value' => $partnerId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'reason',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 400,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_partner_reason @partnerId = :partnerId, @groupProcessInstanceId = :groupProcessInstanceId, @reason = :reason",
            $parameters
        );

        if (isset($output['reason'])) {
            return $output['reason'];
        }

        return '';
    }

    public function length($groupProcessInstanceId, $string)
    {
        return strlen($string);
    }

    public function replacementCarTips($groupProcessInstanceId)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'content',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_replacement_car_tips @groupProcessInstanceId = :groupProcessInstanceId, @content = :content",
            $parameters
        );

        if (isset($output['content'])) {
            return $output['content'];
        }

        return '';
    }

    public function rsaTips($groupProcessInstanceId)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'content',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_rsa_tips @groupProcessInstanceId = :groupProcessInstanceId, @content = :content",
            $parameters
        );

        if (isset($output['content'])) {
            return $output['content'];
        }

        return '';
    }

    public function displayProgram($groupProcessInstanceId)
    {

        $programNotFound = $this->processHandler->getAttributeValue('573', $groupProcessInstanceId, 'int');

        $this->attributeParser->setGroupProcessInstanceId($groupProcessInstanceId);

        if ($programNotFound == "-1") {
            $returnProgramText = '<b>BWB</b> (' . $this->attributeParser->parseString('{@202@}') . ')';
        }
        else if ($programNotFound == "0") {
            $returnProgramText = '<b class="font-red">BWB NEGATYWNY</b> (' . $this->attributeParser->parseString('{@202@}') . ')';
        }
        else {
            $returnProgramText = $this->attributeParser->parseString('{@202@}');
            if ($returnProgramText == '""') $returnProgramText = null;
        }

        return $returnProgramText;

    }


    public function getMWCarClassFromBL($groupProcessInstanceId)
    {
        $rootId = $this->entityManager->getRepository(ProcessInstance::class)->find($groupProcessInstanceId)->getRoot()->getId();;


        $parameters = [
            [
                'key' => 'rootId',
                'value' => intval($rootId),
                'type' => PDO::PARAM_INT
            ]
        ];
        $table = $this->queryManager->executeProcedure("EXEC p_get_vin_headers_by_rootId @root_id = :rootId", $parameters);

        if (is_array($table) && !empty($table)) {
            foreach ($table as $single) {
                if ($single['description'] == 'Klasa') {
                    return $single['value'];
                }
            }
        }
        return "";
    }

    public function attributeValueCount($groupProcessInstanceId, $attributePath)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'attributePath',
                'value' => $attributePath,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT count(id) FROM attribute_value where group_process_instance_id = :groupProcessInstanceId and attribute_path = :attributePath",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : 0;
    }

    static function formattingCaseNumber($caseNumber)
    {
        return "A" . str_repeat('0', self::CASE_NUMBER_LEN - strlen($caseNumber)) . $caseNumber;
    }

    public function formatCaseNumber($caseNumber)
    {
        return self::formattingCaseNumber($caseNumber);
    }

    /** Miejsce docelowe holowania
     * @param $groupProcessInstanceId
     * @return string
     */
    public function towingPlace($groupProcessInstanceId)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'towingPlace',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 1000,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_towing_place @groupProcessInstanceId = :groupProcessInstanceId, @towingPlace = :towingPlace",
            $parameters
        );

        return isset($output['towingPlace']) ? $output['towingPlace'] : '';

    }

    public function towingPlaceInfo($groupProcessInstanceId)
    {
        $rootId = $this->entityManager->getRepository(ProcessInstance::class)->find($groupProcessInstanceId)->getRoot()->getId();

        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $query = "select top 1 isnull(d.textD,'') as name
                  from dbo.attribute_value a
                  join dbo.dictionary d on d.value = a.value_int and d.typeD = 'locationType'
                  where a.attribute_path = '211,85,257'
                  and a.value_int is not null
                  and a.root_process_instance_id = :rootId
                  order by a.id desc";

        $output = $this->queryManager->executeProcedure(
            $query,
            $parameters
        );

        if(!empty($output) && !empty($output[0]["name"])) {
            return $output[0]["name"];
        }

        return '';
    }


    public function displayMulti($groupProcessInstanceId, $multiPath, $extraData)
    {

        $args = func_get_args();
        array_shift($args);
        array_shift($args);
        array_shift($args);

        $class = '';
        $emptyRow = true;
        $headers = [];
        $orderArray = [];

        if (!empty($extraData)) {
            $data = explode(',', $extraData);
            $extraData = [];
            if (!empty($data)) {
                foreach ($data as $datum) {
                    $values = explode(':', $datum);
                    $extraData[$values[0]] = $values[1];
                }
            }
        }

        if (isset($extraData['class'])) {
            $class = $extraData['class'];
        }

        if (isset($extraData['empty-row'])) {
            $emptyRow = filter_var($extraData['empty-row'], FILTER_VALIDATE_BOOLEAN);
        }

        $html = '<div class="' . $class . '"></div><table class="multi-table table table-bordered table-striped table-hover"><thead class="multi-table-header"><tr>__HEADER__</tr></thead><tbody class="multi-table-body">__BODY__</tbody></table></div>';

        $paths = [];
        $repository = $this->entityManager->getRepository('CaseBundle:AttributeValue');

        foreach ($args as $arg) {
            $split = explode('=', $arg);
            $orderArray[] = $split[0];
            $headers[$split[0]] = [
                'path' => $split[0],
                'name' => (isset($split[1])) ? $split[1] : 'Brak nazwy',
                'parser' => (isset($split[2])) ? $split[2] : null
            ];
            $paths[] = $multiPath . ',' . $split[0];
        }

        $headerHtml = '';

        foreach ($headers as $header) {
            $headerHtml .= '<th class="multi-table-th">' . $header['name'] . '</th>';
        }

        $html = str_replace('__HEADER__', $headerHtml, $html);

        $multiAttributes = $repository->findBy([
            'attributePath' => $multiPath,
            'groupProcessInstance' => $groupProcessInstanceId
        ]);

        $bodyHtml = '';

        $amountRow = 0;

        /** @var AttributeValue $multiAttribute */
        foreach ($multiAttributes as $key => $multiAttribute) {

            $cellAttributes = $repository->findBy([
                'parentAttributeValue' => $multiAttribute->getId(),
                'groupProcessInstance' => $groupProcessInstanceId,
                'attributePath' => $paths
            ]);

            $allNull = true;
            $cells = [];

            foreach ($cellAttributes as $cellAttribute) {

                /** @var Attribute $attribute */
                $attribute = $cellAttribute->getAttribute();

                $attributeValue = $cellAttribute->getValueByType($attribute->getValueType());
                $attributeValue = $this->attributeParser->getValueFromAttributeQuery($attribute->getQuery(), $attributeValue);

                if ($headers[$attribute->getId()]['parser'] !== null && $attributeValue !== null) {
                    $attributeValue = $this->tryParse($headers[$attribute->getId()]['parser'], $attributeValue, $groupProcessInstanceId);
                }

                if ($attributeValue !== null) {
                    $allNull = false;
                }

                $cells[$attribute->getId()] = $attributeValue;

            }

            $cells = array_replace(array_flip($orderArray), $cells);

            if (!$allNull) {

                $newRow = '<tr>';

                foreach ($cells as $key2 => $cell) {
                    $newRow .= '<td data-path="' . $key2 . '">' . $cell . '</td>';
                }

                $newRow .= '</tr>';
                $amountRow++;
                $bodyHtml .= $newRow;
            }

        }

        if ($amountRow === 0 && $emptyRow) {
            $bodyHtml = '<tr><td colspan="' . count($headers) . '">Brak wyników</td></tr>';
        }

        $html = str_replace('__BODY__', $bodyHtml, $html);

        return $html;

    }

    /**
     * Towing Limit for program
     * @param $groupProcessInstanceId
     * @param string $key
     * @return mixed
     */
    public function towingLimit($groupProcessInstanceId, $key = 'distance'){

        $parameters = [
            [
                'key' =>'groupProcessInstanceId',
                'value'=>$groupProcessInstanceId,
                'type' =>PDO::PARAM_INT
            ],
            [
                'key' => 'extraMessage',
                'value' => '',
                'type' =>  PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 250
            ],
            [
                'key' => 'distance',
                'value' => 0,
                'length' => 100,
                'type' =>  PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
            ],
            [
                'key' => 'radius',
                'value' => 0,
                'length' => 100,
                'type' =>  PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
            ]
        ];
        $output = $this->queryManager->executeProcedure('EXEC p_programs_max_towing_distance @groupProcessInstanceId = :groupProcessInstanceId, @extraMessage = :extraMessage, @distance= :distance, @radius = :radius',
            $parameters
        );

        return $output[$key];
    }

    private function tryParse($parser, $value, $groupProcessId)
    {

        if ($parser == "partnerName") {

            return $this->partnerName($groupProcessId, $value, 0);

        } else if (strpos($parser, 'd_') === 0) {

            $dictionaryType = str_replace('d_', '', $parser);
            $dictionaryMatches = $this->queryManager->executeProcedure("SELECT * FROM dictionary WHERE typeD = '" . $dictionaryType . "' AND active=1 AND value = " . $value);
            if (!empty($dictionaryMatches)) {
                return $dictionaryMatches[0]['textD'];
            }
        }
        else {
            // Try find method

            if(method_exists($this, $parser)) {

                /** jeżeli w metodzie istnieje argument "groupProcessInstanceId", to przekazuje obecny groupProcessInstanceId */
                $r = new ReflectionMethod('CaseBundle\Utils\ParserMethods', $parser);
                $params = $r->getParameters();

                $args = [];

                foreach ($params as $param) {
                    if($param->getName() === "groupProcessInstanceId") {
                        $args[] = $groupProcessId;
                    }
                }

                /** Reszta argumentów */

                $args = array_merge($args, [$value]);

                return call_user_func_array(array($this, $parser), $args);

            }

        }

        return $value;

    }

    /**
     * @param $type
     * @param $value
     * @param string $return
     * @return string
     */
    public function dictionary($type, $value, $return = 'textD', $valueColumn = 'value')
    {

        if(strpos($value, ',') !== FALSE) {

            $dictionaryMatches = $this->queryManager->executeProcedure("SELECT " . $return . " FROM dictionary WHERE typeD = '" . $type . "' AND active = 1 AND ".$valueColumn." IN (" . $value. ")");

            if (!empty($dictionaryMatches)) {

                $string = '';
                $amount = count($dictionaryMatches);

                foreach ($dictionaryMatches as $key => $dictionaryMatch) {

                    $string .= $dictionaryMatch[$return];

                    if(($key + 1) < $amount) {
                        $string .= ', ';
                    }

                }

                return $string;

            }

        }
        else {
            $dictionaryMatches = $this->queryManager->executeProcedure("SELECT " . $return . " FROM dictionary WHERE typeD = '" . $type . "' AND active=1 AND ".$valueColumn." = '" . $value. "'");

            if (!empty($dictionaryMatches)) {
                return $dictionaryMatches[0][$return];
            }
        }

        return '';
    }

    public function vipInfo($groupProcessInstanceId,$phoneNumber = false, $type = 0)
    {

        $output = "";
        if($phoneNumber){
            /** @var Vip $vip */
            $vip = $this->entityManager->getRepository('UserBundle:Vip')->findByPhoneNumber($phoneNumber);

            if(!empty($vip)){ /** Is VIP  */
                if($type == 1){
                    return '<i class="fa fa-star"></i>';
                }
                if($type == 2){
                    $output = '<br/><span class="font-red"><i class="fa fa-exclamation-circle"></i> '.$vip->getSupervisorMessage().'</span>';
                }
                else{
                    $output = '<i class="fa fa-star"></i>  <b>'. $vip->getPosition().':</b>';
                }
            }else{
                $output =  null;


            }
        }

        $vipAttr = $this->processHandler->getAttributeValue(AttributeParserService::VIP_ATTR_ID, $groupProcessInstanceId, 'string');
        if (!(is_array($vipAttr)) && !is_null($vipAttr)){

            $output = '<i class="fa fa-star"></i>  <b>'. $vipAttr.'</b>';
        }



        return $output;
    }
    public function vipText($groupProcessInstanceId,$phoneNumber = false)
    {
        $output = "";
        if($phoneNumber){
            $vip = $this->entityManager->getRepository('UserBundle:Vip')->findByPhoneNumber($phoneNumber);
            if($vip){ /** Is VIP  */
                /** @var Vip $vip */
                $output = "<div class='vip_txt'><strong class='strong' style='width: 25%; display: inline-block'><i class='fa fa-trophy'></i> Informacje VIP:</strong> ".$vip->getCompanyName()." - ". $vip->getPosition()."</div>";
            }else{
                $output =  null;
            }
        }
        $vipAttr = $this->processHandler->getAttributeValue(AttributeParserService::VIP_ATTR_ID, $groupProcessInstanceId, 'string');
        if (!(is_array($vipAttr)) && !is_null($vipAttr)){

            $output = '<i class="fa fa-star"></i>  <b>'. $vipAttr.'</b>';
        }

        return $output;
    }

    public function leaseplanVipText($groupProcessInstanceId){
        $vipAttr = $this->processHandler->getAttributeValue(AttributeParserService::VIP_ATTR_ID, $groupProcessInstanceId, 'string');
        $output= '';
        if (!(is_array($vipAttr)) && !is_null($vipAttr)){
            if($vipAttr == 'VIP relationship' || $vipAttr == 'VIP special care') {

                $output = '<i class="fa fa-star"></i>';
                $output = '<b>VIP – Stosuj odrębną procedurę.</b><i class="fa fa-star"></i>';
                $output .= '<br />';
                $output .= '- udostępnienie pojazdu zastępczego w klasie pojazdu naprawianego na okres zgodny z pakietem Assistance. *W przypadku, gdy w chwili awarii, szkody nie jest dostępny pojazd zgodny z klasą zostanie udostępniony pojazd zastępczy klasy E do czasu możliwości wynajęcia samochodu w klasie naprawianego. <br />
- wyłączenie obowiązku przygotowania przez użytkownika pojazdu do zwrotu wypożyczalni tj. mycie, sprzątanie, tankowanie. <br/>
- wydłużenie okresu na zwrot pojazdu zastępczego po zakończeniu naprawy do 24h.<br/>
- transport naprawionego pojazdu na życzenie użytkownika w ramach d2d lub lawetą. – brak warunku „minimum 100km dla TNP ” <br />';
            }else if ($vipAttr === 'VIP standard'){
                $output =  '<b>Klient VIP STANDARD, obsługa na najwyższym poziomie</b>';
            }
        }

        return $output;
    }

    public function rentalFile($groupProcessInstanceId, $path, $platformId = null)
    {
//        $platformId = $this->processHandler->getAttributeValue('253',$groupProcessInstanceId,'int');
        $partnerId = $this->processHandler->getAttributeValue($path, $groupProcessInstanceId, 'int');
        $parameters = [
            [
                'key' => 'partnerId',
                'value' => (int)$partnerId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'platformId',
                'value' => $platformId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure('SELECT dbo.f_rental_priceList_file_url(:partnerId, :platformId)',
            $parameters
        );

        return $output[0][''];

    }

    public function userNameById($id = null)
    {

        if(!$id){
            return '';
        }
        elseif($id == 1){
            return 'AUTOMAT';
        }
        $user = $this->entityManager->getRepository(User::class)->find($id);
        if(!$user){
            return '';
        }
        return $user->getFirstname() . ' ' . $user->getLastname();

    }

    public function rentalDurationCheck($groupProcessInstanceId, $path, $value)
    {

        $return = true;
        $final = 0;
        if (in_array($path, ['789,786,240,153', '812,789,786,240,153'])) {

            if ($path == '812,789,786,240,153') {
                $final = 1;
            }
            $parameters = [
                [
                    'key' => 'groupProcessInstanceId',
                    'value' => (int)$groupProcessInstanceId,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'final',
                    'value' => $final,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'useTempTable',
                    'value' => 0,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'value',
                    'value' => $value,
                    'type' => PDO::PARAM_INT
                ]
            ];
            $output = $this->queryManager->executeProcedure('EXEC dbo.p_replacement_car_summary @groupProcessInstanceId = :groupProcessInstanceId, @final = :final, @useTempTable = :useTempTable, @value = :value',
                $parameters
            );
            if ($output[0]['days_left'] < 0) {
                $return = false;
            }

        }

        return $return;
    }

    public function postponeCount($groupProcessInstanceId, $stepId)
    {

        $instance = $this->entityManager->getRepository(ProcessInstance::class)->findBy(['groupProcess' => $groupProcessInstanceId, 'step' => $stepId]);

        if (count($instance) > 0) {
            return $instance[0]->getPostponeCount();
        }
        return null;
    }

    public function replacementCarGroupCosts($groupProcessInstanceId, $type = 1)
    {

        $rentalGroup = $this->processHandler->getAttributeValue('168,839',$groupProcessInstanceId,'int');
        $parameters = [
            [
                'key' => 'rentalGroup',
                'value' => (int)$rentalGroup,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT av.group_process_instance_id 
                    from (select *, row_number()over(partition by group_process_id order by id desc) rowN FROM dbo.service_status ) ss
                    inner join dbo.attribute_value av ON ss.group_process_id = av.group_process_instance_id 
                    inner join dbo.attribute_value av2 on av2.group_process_instance_id = av.group_process_instance_id and av2.attribute_path = '168,839' and av2.value_int = :rentalGroup
                    inner join dbo.service_status_dictionary ssd ON ssd.id = ss.status_dictionary_id
                    left join dbo.attribute_value av5 ON av5.group_process_instance_id = av.group_process_instance_id and av5.attribute_path = '168,159'
                    where av.attribute_path = '812,789,786,240,153'
                    and rowN = 1
                    and ssd.progress NOT IN (5,6)
                    and isnull(av5.value_int,0) = 0
                    order by av.group_process_instance_id desc",$parameters);

        $summary = '';
        foreach($output as $element){
            $groupProcessInstanceId = $element['group_process_instance_id'];
            $programId = $this->processHandler->getAttributeValue('202',$groupProcessInstanceId, 'string');
            $programName = $this->programNames($programId);
            $summary .= $this->replacementCarCosts($groupProcessInstanceId, $type, 'Program: <b>'.$programName.'</b>', 'table-bordered');
        }

        return $summary;

    }

    public function replacementCarCosts($groupProcessInstanceId, $type = 1, $prependHtml = null, $class = '')
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'useTempTable',
                'value' => 0,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'gop',
                'value' => $type,
                'type' => PDO::PARAM_INT

            ]
        ];

        $output = $this->queryManager->executeProcedure('EXEC dbo.p_replacement_car_costs_report @groupProcessInstanceId = :groupProcessInstanceId, @gop = :gop, @useTempTable = :useTempTable',
            $parameters
        );

        $table = '<table class="table '.$class.'">';

        if($prependHtml){
            $table .= '<thead><tr><td colspan="2">'.$prependHtml.'</td></tr></thead>';
        }

        $table .= '<tbody>';

        foreach ($output as $row) {
            $table .= '<tr><td width="80%">' . $row['name'] . '</td><td>' . $row['cost'] . '</td></tr>';
        }
        $table .= '</tbody></table>';

        return $table;

    }

    public function replacementCarCostsViewer($groupProcessInstanceId, $type = 1, $prependHtml = null, $class = '')
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'useTempTable',
                'value' => 0,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'gop',
                'value' => $type,
                'type' => PDO::PARAM_INT

            ]
        ];

        $output = $this->queryManager->executeProcedure('EXEC dbo.p_replacement_car_costs_report_viewer @groupProcessInstanceId = :groupProcessInstanceId, @gop = :gop, @useTempTable = :useTempTable',
            $parameters
        );

        $table = '<table class="table '.$class.'">';

        if($prependHtml){
            $table .= '<thead><tr><td colspan="2">'.$prependHtml.'</td></tr></thead>';
        }

        $table .= '<tbody>';

        foreach ($output as $row) {
            $table .= '<tr><td width="80%">' . $row['name'] . '</td><td>' . $row['cost'] . '</td></tr>';
        }
        $table .= '</tbody></table>';

        return $table;

    }

    public function rsaRefusalsTable($groupProcessInstanceId)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]

        ];

        $output = $this->queryManager->executeProcedure('EXEC dbo.p_partner_reasons @groupProcessInstanceId = :groupProcessInstanceId',
            $parameters
        );

        $table = '<table class="table">';

        foreach ($output as $row) {
            $table .= '<tr><td>' . $row['partnerName'] . '</td><td>' . $row['reasonText'] . '</td></tr>';
        }
        $table .= '</table>';

        return $table;

    }

    public function showStatusDate($groupProcessInstanceId, $statusId)
    {
        $query = "SELECT TOP 1 created_at FROM service_status where group_process_id = $groupProcessInstanceId and status_dictionary_id = $statusId order by id desc";

        $output = $this->queryManager->executeProcedure(
            $query
        );

        return count($output) > 0 ? $output[0]['created_at'] : '';

    }

    public function caseStartDate($groupProcessInstanceId)
    {
        $caseStartDate = $this->entityManager->getRepository(ProcessInstance::class)->getDateCreateInstance($groupProcessInstanceId);
        return $caseStartDate ? $caseStartDate->format('Y-m-d H:i:s') : '';
    }

    public function caseEndDate($groupProcessInstanceId, $state =1 )
    {
        $rootId = $this->entityManager->getRepository(ProcessInstance::class)->find($groupProcessInstanceId)->getRoot()->getId();

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        if($state ==1 )
        {
//            Pierwszy
            $output = $this->queryManager->executeProcedure(
                "SELECT top 1 date_leave from process_instance with (nolock) where root_id = :rootId and step_id ='1011.023' order by date_leave asc",
                $parameters
            );
        }else{
//            Ostatni
            $output = $this->queryManager->executeProcedure(
                "SELECT top 1 date_leave from process_instance with (nolock) where root_id = :rootId and step_id ='1011.023' order by date_leave desc",
                $parameters
            );
        }


        return (is_array($output)&&!empty($output))?substr($output[0]['date_leave'],0,19):'';


    }


    public function rsaCloseDateCheck($groupProcessInstanceId, $path, $value, $stepId)
    {

        $caseStart = strtotime($this->caseStartDate($groupProcessInstanceId));
        $return = true;
        if($stepId = '1009.006') {
            if ($path == '638,214' && !empty($value)) {
                if (strtotime($value) > strtotime('now') || strtotime($value) < $caseStart) {
                    return false;
                }
            } else if ($path == '638,130' && !empty($value)) {
                $arrivalDate = $this->processHandler->getAttributeValue('638,214', $groupProcessInstanceId, 'date');
                if (strtotime($value) > strtotime('now') || strtotime($value) < $caseStart || strtotime($value) < strtotime($arrivalDate)) {
                    return false;
                }
            } else if ($path == '638,268' && !empty($value)) {
                $arrivalDate = $this->processHandler->getAttributeValue('638,214', $groupProcessInstanceId, 'date');
                $isTowing = $this->processHandler->getAttributeValue('560', $groupProcessInstanceId, 'int');
                $fixDate = $this->processHandler->getAttributeValue('638,130', $groupProcessInstanceId, 'date');
                if (strtotime($value) > strtotime('now') || strtotime($value) < $caseStart || strtotime($value) < strtotime($arrivalDate) || (strtotime($value) < strtotime($fixDate) && $isTowing == 1)) {
                    return false;
                }

            }
        }
        return $return;

    }

    public function hasRole($role){

        if($this->authorizationChecker->isGranted($role)) {
            return 1;
        }
        else {
            return 0;
        }

    }

    public function isCaseOpened($rootId){

        $parameters = [
            [
                'key' => 'rootId',
                'value' => (int)$rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_is_case_opened(:rootId)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';

    }

    public function config($groupProcessInstanceId, $key){

        if(key_exists($key, self::$availableConfigKeys)) {
            return self::$availableConfigKeys[$key];
        }

        $processHandler = $this->processHandler;
        $platformId = $processHandler->getAttributeValue('253',$groupProcessInstanceId,'int');
        $programId = $processHandler->getAttributeValue('202',$groupProcessInstanceId,'string');

        $rootId = $this->entityManager->getRepository(ProcessInstance::class)->find($groupProcessInstanceId)->getRoot()->getId();

        if(in_array($key, ['replacement_car.rules.allow_higher_class','replacement_car.rules.max_class'] ) && $this->platformGroup($rootId) == 'CFM'){
            $programId = $processHandler->getAttributeValue('202',$rootId,'string');
            $platformId = $processHandler->getAttributeValue('253',$rootId,'int');
        }

        $parameters = [
            [
                'key' => 'key',
                'value' => $key,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ],
            [
                'key' => 'platformId',
                'value' => (int)$platformId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'programId',
                'value' => (int)$programId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_get_platform_key(:key, :platformId, :programId)",
            $parameters
        );

        $value = count($output) > 0 ? $output[0][''] : '';

        self::$availableConfigKeys[$key] = $value;

        return $value;

    }


    public function makeModelClass($makeModelId){


        if(empty($makeModelId)){
            return '';
        }

        $parameters = [
            [
                'key' => 'makeModelId',
                'value' => (int)$makeModelId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dc.textD makeModelClass
                    FROM dbo.dictionary d
                    INNER JOIN dbo.dictionary dc ON TRY_PARSE(d.argument3 AS INT) = dc.value AND dc.typeD = 'makeModelClass'
                    WHERE d.typeD = 'makeModel'
                    AND d.value = :makeModelId",
            $parameters
        );

        return count($output) > 0 ? $output[0]['makeModelClass'] : '';

    }

    public function sumAttributeValue($groupProcessInstanceId, $attributePath){
        return $this->processHandler->sumAttributeValue($groupProcessInstanceId, $attributePath);
    }

    public function diagnosisSummary($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_diagnosis_summary(:groupProcessInstanceId)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';

    }

    public function sparxServiceName($groupProcessInstanceId, $overrideGroupId = null){

        $groupProcessInstanceId = $overrideGroupId ?: $groupProcessInstanceId;

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_group_process_sparx_service_name(:groupProcessInstanceId)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';

    }

    public function sparxEtaName($groupProcessInstanceId, $etaType){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'etaType',
                'value' => $etaType,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_group_process_sparx_eta_name(:groupProcessInstanceId, :etaType)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';

    }

    public function rsaVehicleDetails($groupProcessInstanceId, $property){

//        $orderId = $this->processHandler->getAttributeValue('609', $groupProcessInstanceId, 'int');
//
//        if(!$orderId || $orderId < 0){
//            return '';
//        }

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_get_rsa_vehicle_details @groupProcessInstanceId = :groupProcessInstanceId",
            $parameters
        );

        return count($output) > 0 ? $output[0][$property] : '';
    }

    public function rsaFinishType($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'type',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_rsa_finnish_type @groupProcessInstanceId = :groupProcessInstanceId, @type = :type",
            $parameters
        );

        return count($output) > 0 ? $output['type'] : '';
    }

    public function programNames($programIds){
        $parameters = [
            [
                'key' => 'programIds',
                'value' => $programIds,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_program_names(:programIds)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';
    }


    public function arccodepart($value, $part){
        $parameters = [
            [
                'key' => 'value',
                'value' => $value,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'part',
                'value' => $part,
                'type' => PDO::PARAM_STR,
                'length' => 255
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT argument2 FROM dbo.dictionary where value = :value and typeD = :part ",
            $parameters
        );

        if(count($output) > 0){
            return $output[0]['argument2'];
        }
        return '';
    }

    public function rentalProgramUsed($groupProcessInstanceId){
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_rental_program_used @groupProcessInstanceId = :groupProcessInstanceId",
            $parameters
        );

        if(count($output) > 0){
            $table = '<table class="table">';

            foreach ($output as $row) {
                $table .= '<tr><td>' . $row['name'] . '</td><td>' . $row['days'] . '</td></tr>';
            }
            $table .= '</table>';

            return $table;
        }
        return '';
    }

    public function getCaseStatus($rootId)
    {
        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output =  $this->queryManager->executeProcedure(
            "SELECT dbo.f_case_status(:rootId)",
            $parameters
        );

        return $output[0][''];

    }

    /**
     * @param $groupProcessInstanceId
     * @param $key
     * @return mixed
     */
    public function sumRentalLimits($groupProcessInstanceId, $key = null)
    {

        $key = $key ?: 'maxDays';

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'finalEndDate',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 400,
            ],
            [
                'key' => 'maxDays',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100,
            ],
            [
                'key' => 'finalProgramIds',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 400,
            ],

        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_sum_rental_limits @groupProcessInstanceId = :groupProcessInstanceId, @maxDays = :maxDays, @finalEndDate = :finalEndDate, @finalProgramIds = :finalProgramIds",
            $parameters
        );

        return $output[$key];

    }

    /**
     * @param $groupProcessInstanceId
     * @param null $minMinutes
     * @param null $startDate
     * @return mixed
     */
    public function sparxCalculateEtc($groupProcessInstanceId, $minMinutes = null, $startDate = null)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'startDate',
                'value' => $startDate,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'minMinutes',
                'value' => $minMinutes,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'eta',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 400,
            ],

        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_sparx_calculate_etc @groupProcessInstanceId = :groupProcessInstanceId, 
            @startDate = :startDate, @minMinutes = :minMinutes, @eta = :eta",
            $parameters
        );

        return $output['eta'];

    }

    /**
     * @param $groupProcessInstanceId
     * @param $startLocationRootPath
     * @param $endLocationRootPath
     * @return mixed
     */
    public function routeEta($groupProcessInstanceId, $startLocationRootPath, $endLocationRootPath, $startDate = null)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'startAttributeRootPath',
                'value' => $startLocationRootPath,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'endLocationRootPath',
                'value' => $endLocationRootPath,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'startDate',
                'value' => $startDate,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'eta',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 400,
            ],

        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_map_route_time @groupProcessInstanceId = :groupProcessInstanceId, 
            @startAttributeRootPath = :startAttributeRootPath, @endAttributeRootPath = :endLocationRootPath, 
            @startDate = :startDate, @eta = :eta",
            $parameters
        );

        return $output['eta'];

    }

    /**
     * @param $date
     * @param string $format
     * @param null $timezone
     * @return mixed
     * @throws \Moment\MomentException
     */
    public function moment($date = null, $timezone = 'Europe/Warsaw', $format = 'Y-m-d H:i:s', $homeTimeZone = 'Europe/Warsaw')
    {
        if(empty($date)){
            $date = date($format);
        }

        $moment = new Moment($date, $homeTimeZone);
        $moment->setTimezone($timezone);

        if($timezone == 'UTC') {
            $date = str_replace('+0000', 'Z', $moment->format());
        }
        else{
            $date = $moment->format($format);
        }

        return $date;
    }

    public function runningServices($groupProcessInstanceId)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'servicesIds',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 400,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_running_services @groupProcessInstanceId = :groupProcessInstanceId, @servicesIds = :servicesIds",
            $parameters
        );

        if (isset($output['servicesIds'])) {
            return $output['servicesIds'];
        }

        return '';

    }

    public function serviceProgress($groupProcessInstanceId)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'progress',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 400,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_get_service_progress @groupProcessInstanceId = :groupProcessInstanceId, @progress = :progress",
            $parameters
        );

        if (isset($output['progress'])) {
            return $output['progress'];
        }

        return '';

    }

    public function attributeValue($groupProcessId, $path, $type){
        $value =  $this->processHandler->getAttributeValue($path, $groupProcessId, $type);
        return $value ?: '';

    }

    public function serviceGroup($groupProcessInstanceId, $serviceId){
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceId',
                'value' => $serviceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_service_top_progress_group(:groupProcessInstanceId, :serviceId)",
            $parameters
        );


        return count($output) > 0 ? $output[0][''] : '';

    }

    public function isArcPartner($partnerId){
        $parameters = [
            [
                'key' => 'partnerId',
                'value' => $partnerId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_is_arc_partner(:partnerId)",
            $parameters
        );


        return count($output) > 0 ? $output[0][''] : 0;

    }

    public function caseAbroad($groupProcessInstanceId){
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_abroad_case(:groupProcessInstanceId)",
            $parameters
        );


        return count($output) > 0 ? $output[0][''] : 0;

    }


    public function towingFixingConfirmation($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'message',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 1000,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_towing_fixing_confirmation @groupProcessInstanceId = :groupProcessInstanceId, @message = :message",
            $parameters
        );

        if (isset($output['message'])) {
            return $output['message'];
        }

    }

    public function platformGroup($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'name',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_platform_group_name @groupProcessInstanceId = :groupProcessInstanceId, @name = :name",
            $parameters
        );

        if (isset($output['name'])) {
            return $output['name'];
        }

    }

    public function stepDescription($processInstanceId){

        return $this->processHandler->stepDescription($processInstanceId);

    }


    public function sparxCancellationStatus($groupProcessInstanceId, $key, $overrideGroupId = null, $statusId = null){

        $groupProcessInstanceId = $overrideGroupId ?: $groupProcessInstanceId;

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'statusId',
                'value' => $statusId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'code',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
            [
                'key' => 'comment',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 1000,
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_sparx_status_reason @groupProcessInstanceId = :groupProcessInstanceId, @code = :code, @comment = :comment, @statusId = :statusId",
            $parameters
        );

        if (isset($output[$key])) {
            return $output[$key];
        }

    }

    public function makeAndModel($valueId, $key){

        $parameters = [
            [
                'key' => 'valueId',
                'value' => $valueId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'make',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
            [
                'key' => 'model',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 255,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_makeAndModelByValue @valueId = :valueId, @make = :make, @model = :model",
            $parameters
        );

        if (isset($output[$key])) {
            return $output[$key];
        }

    }

    public function canWorkshopSwapRental($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'canSwap',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_can_workshop_swap_rental @groupProcessInstanceId = :groupProcessInstanceId, @canSwap = :canSwap",
            $parameters
        );


        if (isset($output['canSwap'])) {
            return $output['canSwap'];
        }

        return 0;
    }

    public function getDomain() {
        return $this->parameterBag->get('server_host');
    }

    public function getEnv() {
        return $this->parameterBag->get('atlas_environment');
    }

    public function producerProgram($groupProcessInstanceId){

        $programIds = $this->processHandler->getAttributeValue('204',$groupProcessInstanceId,'string');

        if(!empty($programIds)) {

            $parameters = [
                [
                    'key' => 'programIds',
                    'value' => $programIds,
                    'type' => PDO::PARAM_STR,
                    'length' => 255
                ]
            ];

            $output = $this->queryManager->executeProcedure(
                "SELECT dbo.f_producer_program(:programIds)",
                $parameters
            );

            return count($output) > 0 ? $output[0][''] : '';
        }

        return '';

    }

    public function producerPlatform($groupProcessInstanceId){

        $programId = $this->producerProgram($groupProcessInstanceId);

        if($programId) {
            $program = $this->entityManager->getRepository(Program::class)->find($programId);
            return ($program) ? $program->getPlatform()->getId() : null;
        }

        return null;

    }

    public function consultWorkshopRouting($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'consult',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_lp_consult_workshop_routing @groupProcessInstanceId = :groupProcessInstanceId, @consult = :consult",
            $parameters
        );


        if (isset($output['consult'])) {
            return $output['consult'];
        }

        return 0;
    }


    public function diagnosisCode($groupProcessInstanceId, $part = null){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT,
                'length' => 255
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_diagnosis_code(:groupProcessInstanceId)",
            $parameters
        );

        if($part && count($output) > 0){
            if($part == 1){
                return substr($output[0][''],0,3);
            }
            else if($part == 2){
                return substr($output[0][''],3,2);
            }
            else if($part == 3){
                return substr($output[0][''],5,2);
            }
        }

        return count($output) > 0 ? $output[0][''] : '';
    }

    public function getStringTrueFalse($value) {

        return (filter_var($value, FILTER_VALIDATE_BOOLEAN)) ? "TRUE" : "FALSE";

    }

    public function getVaryonCountry($country) {

        /** @var ValueDictionary $dictionaryValue */
        $dictionaryValue = $this->getDictionaryRepo()->findOneBy([
            'type' => 'europeanCountry',
            'text' => $country
        ]);

        return ($dictionaryValue instanceof ValueDictionary) ? $dictionaryValue->getArgument2() : '';

    }

    /**
     * @param $groupProcessInstanceId
     * @param $iCalculusId - np: 7169#60#13
     * @param int $returnXmlNode
     * @return mixed|null
     */
    public function iCalculusValue($groupProcessInstanceId, $iCalculusId, $returnXmlNode = 0) {

        $returnXmlNode = filter_var($returnXmlNode, FILTER_VALIDATE_BOOLEAN);

        $iCalculusId = str_replace('*', "#", $iCalculusId);

        /** @var ValueDictionary $dictionaryValue */
        $dictionaryValue = $this->getDictionaryRepo()->findOneBy([
            'type' => 'iCalculusCode',
            'argument2' => $iCalculusId
        ]);

        if(empty($dictionaryValue)) {
            return '--- NIE ZNALEZIONO ' . $iCalculusId . ' ---';
        }

        if($returnXmlNode) {
            return $this->getVaryonXmlNode($iCalculusId, $dictionaryValue->getArgument1());
        }
        else {
            return $dictionaryValue->getArgument1();
        }

    }

    private function getVaryonXmlNode($id, $v) {

        return "\r<ical:ArgValue>\r\t<ical:id>" . $id . "</ical:id>\r\t<ical:v>" . htmlspecialchars($v) . "</ical:v>\r</ical:ArgValue>";

    }

    /**
     * @return ValueDictionaryRepository|\Doctrine\ORM\EntityRepository
     */
    private function getDictionaryRepo() {

        if($this->dictionaryRepository === null) {
            $this->dictionaryRepository = $this->entityManager->getRepository(ValueDictionary::class);
        }

        return $this->dictionaryRepository;
    }

    /**
     * np: {#serviceLocRefCode({@522*@})#}
     * Zwraca wartosc z atrybutu 595,597,630, z lokalizacji serwisu
     *
     * @param $serviceId
     * @return string
     */
    public function serviceLocRefCode($serviceId) {

        $parameters = [
            [
                'key' => 'serviceId',
                'value' => (int)$serviceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT [dbo].[s_service_ref_code](:serviceId) as codeRef",
            $parameters
        );

        if(!empty($output)) {
            return $output[0]['codeRef'];
        }

        return '';

    }

    public function canRentalContinue($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'canContinue',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_can_rental_continue @groupProcessInstanceId = :groupProcessInstanceId, @canContinue = :canContinue",
            $parameters
        );


        if (isset($output['canContinue'])) {
            return $output['canContinue'];
        }

        return 0;
    }

    /**
     * Sprawdza, czy można wysłać XML'a do Varyoa
     * można go tam wysłać tylko raz
     *
     * @param $rootId
     * @return int
     */
    public function canSendXmlToVaryon($rootId){

        $parameters = [
            [
                'key' => 'rootId',
                'value' => $rootId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT [dbo].[f_canSendXMLtoVaryon](:rootId) as canSend",
            $parameters
        );

        if(!empty($output)) {
            if (isset($output[0]['canSend'])) {
                return $output[0]['canSend'];
            }
        }

        return 0;
    }

    private function partnerLocationInfo($partnerLocationId) {

        $parameters = [
            [
                'key' => 'partnerLocationId',
                'value' => $partnerLocationId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.P_partnerInfo @partnerLocationId = :partnerLocationId",
            $parameters
        );

        $result = [];

        foreach ($output as $item) {
            $result[$item['attribute_path']] = $item;
        }

        unset($output);
        unset($parameters);

        return $result;

    }

    /**
     * @param $list
     * @param $path
     * @param $type
     * @return null
     */
    private function getPartnerValue($list, $path, $type) {

        if(isset($list[$path])) {
            return $list[$path]['value_' . $type];
        }

        return null;

    }

    public function renderTowingXML($towingGroupId) {

        $towingPartnerId =  $this->processHandler->getAttributeValue('610', $towingGroupId,'int');
        $serviceLocId =  $this->processHandler->getAttributeValue('522', $towingGroupId,'int');

        if(!empty($towingPartnerId)) {
            $towingPartnerInfo = $this->partnerLocationInfo($towingPartnerId);
            $partnerName = $this->partnerName($towingGroupId, $towingPartnerId, 0);
        }
        else {
            $towingPartnerInfo = [];
            $partnerName = '';
        }

        if(!empty($serviceLocId)) {
            $servicePartnerInfo = $this->partnerLocationInfo($serviceLocId);
        }
        else {
            $servicePartnerInfo = [];
        }

        $partnerFullName = $this->partnerNameFull($towingGroupId, "522");

        $phone = $this->partnerContact($towingGroupId, '522', 1, 2);

        if(empty($phone)) $phone = '';

        return '<towing>
                <order />
                <serviceneeded>TRUE</serviceneeded>
                <id>' . $this->getPartnerValue($towingPartnerInfo, '595,597,631', 'string') . '</id>
                <name>' . $partnerName . '</name>
                <address>
                  <description>' . $partnerFullName. '</description>
                  <street>' . $this->getPartnerValue($servicePartnerInfo, '595,597,85,94', 'string') . '</street>
                  <number>' . $this->getPartnerValue($servicePartnerInfo, '595,597,85,95', 'string') . '</number>
                  <zip>' . $this->getPartnerValue($servicePartnerInfo, '595,597,85,89', 'string') . '</zip>
                  <locality>' . $this->getPartnerValue($servicePartnerInfo, '595,597,85,87', 'string') . '</locality>
                  <land>' . $this->getPartnerValue($servicePartnerInfo, '595,597,85,86', 'string') . '</land>
                  <phonetype>Tel.</phonetype>
                  <phone>' . $phone . '</phone>
                </address>
              </towing>';

    }

    /**
     * @param $groupProcessInstanceId
     * @param bool $asName
     * @return mixed
     */
    public function carType($groupProcessInstanceId, $asName = false) {

        $asName = filter_var($asName, FILTER_VALIDATE_BOOLEAN);

        return $this->processHandler->carType($groupProcessInstanceId, $asName);

    }

    /**
     * @param $groupProcessInstanceId
     * @param int $type
     * @param bool $lastRow
     * @return int
     */
    public function replacementSummaryCarCosts($groupProcessInstanceId, $type = 1, $lastRow = false) {

        $lastRow = filter_var($lastRow, FILTER_VALIDATE_BOOLEAN);

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'useTempTable',
                'value' => 0,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'gop',
                'value' => $type,
                'type' => PDO::PARAM_INT

            ]
        ];

        $output = $this->queryManager->executeProcedure('EXEC dbo.p_replacement_car_costs_report @groupProcessInstanceId = :groupProcessInstanceId, @gop = :gop, @useTempTable = :useTempTable',
            $parameters
        );

        if(!empty($output) && is_array($output)) {

            if($lastRow) {
                $last = end($output);
                return $last['cost'];
            }
            else {
                foreach ($output as $key => $item) {

                    if($item['name'] === "RAZEM") {
                        return $item['cost'];
                    }

                }
            }

        }

        return 0;

    }

    public function getMake($groupProcessInstanceId)
    {
        $markModel = $this->processHandler->getAttributeValue('74,73', $groupProcessInstanceId, 'int');
        $makeModelDict = $this->entityManager->getRepository(ValueDictionary::class)->findOneBy(
            [
                'type' => 'makeModel',
                'value' => $markModel
            ]);
        return explode(' ',$makeModelDict->getText())[0];

    }

    public function PSAType($groupProcessInstanceId)
    {
        $platform = $this->processHandler->getAttributeValue('253', $groupProcessInstanceId, 'int');
        if (in_array($platform, [79,
            81])) {
            return 1;
        } else {
            return 2;
        }
    }


    public function rentalLimitWarning($groupProcessInstanceId, $key){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'text',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000
            ],
            [
                'key' => 'status',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_rental_limit_warning @groupProcessInstanceId = :groupProcessInstanceId, @text = :text, @status = :status",
            $parameters
        );


        if (isset($output[$key])) {
            return $output[$key];
        }

        return 0;
    }

    /**
     *
     * Example: {#displayTableOfValueHistory(129,128|Data,Osoba,Wartość Historyczna)#}
     *
     * @param $groupProcessInstanceId
     * @param $path
     * @param string $columnNames
     * @return string
     */
    public function displayTableOfValueHistory($groupProcessInstanceId, $path, $columnNames = 'Data,Osoba,Wartość') {

        $attributeArr = $this->processHandler->getAttributeValue($path, $groupProcessInstanceId);

        if(empty($attributeArr)) {
            return "";
        }

        $attributeValue = $this->entityManager->getRepository('CaseBundle:AttributeValue')->find($attributeArr[0]['id']);

        if(empty($attributeValue)) {
            return "";
        }

        $processInstance = $this->entityManager->getRepository('CaseBundle:ProcessInstance')->find($groupProcessInstanceId);
        $response = $this->operationDashboardInterface->attributeValueHistory($attributeValue, $processInstance);

        $columns = explode(',', $columnNames);

        $table = '<div class="attribute-history-tables"><table class="table table-bordered table-condensed"><thead><tr><th>' . $columns[0] . '</th><th>' . $columns[1] . '</th><th>' . $columns[2] . '</th></tr></thead><tbody>';

        foreach ($response['histories'] as $history) {
            $row = '<tr>';

            $row .= '<td>' . $history['date'] . '</td>';
            $row .= '<td>' . $history['userName'] . '</td>';
            $row .= '<td>' . $history['value'] . '</td>';

            $row .= "</tr>";

            $table .= $row;
        }

        $table .= '</tbody></table></div>';

        return $table;

    }

    /** Miejsce naprawy warsztatowej
     * @param $groupProcessInstanceId
     * @return string
     */
    public function workshopName($groupProcessInstanceId)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'workshopName',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 1000,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_workshop_name @groupProcessInstanceId = :groupProcessInstanceId, @workshopName = :workshopName",
            $parameters
        );

        return isset($output['workshopName']) ? $output['workshopName'] : '';

    }

    public function existsInSplit($needle, $haystack)
    {

        $parameters = [
            [
                'key' => 'needle',
                'value' => $needle,
                'type' => PDO::PARAM_STR,
                'length' => 1000,
            ],
            [
                'key' => 'haystack',
                'value' => $haystack,
                'type' => PDO::PARAM_STR,
                'length' => 1000,
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_exists_in_split(:needle,:haystack)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';

    }

    public function replacementCarOfferStatus($groupProcessInstanceId)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'content',
                'value' => '',
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000,
            ],
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_replacement_car_offer_status @groupProcessInstanceId = :groupProcessInstanceId, @content = :content",
            $parameters
        );

        if (isset($output['content'])) {
            return $output['content'];
        }

        return '';
    }

    /**
     * Towing Limit for program
     * @param $groupProcessInstanceId
     * @param string $key
     * @return mixed
     */
    public function serviceCostLimit($groupProcessInstanceId, $key = 'info'){

        $parameters = [
            [
                'key' =>'groupProcessInstanceId',
                'value'=>$groupProcessInstanceId,
                'type' =>PDO::PARAM_INT
            ],
            [
                'key' => 'info',
                'value' => null,
                'type' =>  PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 250
            ],
            [
                'key' => 'cost',
                'value' => null,
                'length' => 100,
                'type' =>  PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
            ]
        ];
        $output = $this->queryManager->executeProcedure('EXEC p_service_cost_limit @groupProcessInstanceId = :groupProcessInstanceId, @cost = :cost, @info= :info',
            $parameters
        );

        return $output[$key];
    }

    public function rsaPhotos($groupProcessInstanceId)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_rsa_photos(:groupProcessInstanceId)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';

    }


    public function isProducerWarranty($groupProcessInstanceId)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'warranty',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000,
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_is_producer_warranty @groupProcessInstanceId = :groupProcessInstanceId, @warranty = :warranty",
            $parameters
        );

        if (isset($output['warranty'])) {
            return $output['warranty'];
        }

        return '';

    }

    public function producerHelpline($makeModel)
    {

        $parameters = [
            [
                'key' => 'makeModel',
                'value' => $makeModel,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_partner_helpline(:makeModel)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';


    }

    /**
     * @param $groupProcessInstanceId
     * @param string $name
     * @return string
     */
    public function showHelp($groupProcessInstanceId, $name = '') {

        if(empty($name)) {
            $name = $this->processHandler->getAttributeValue('981,438', $groupProcessInstanceId, AttributeValue::VALUE_STRING);
        }

        $output = [];

        if(!empty($name) && $name !== '""') {

            $parameters = [
                [
                    'key' =>'name',
                    'value'=> $name,
                    'type' =>PDO::PARAM_STR
                ]
            ];

            $output = $this->queryManager->executeProcedure('SELECT note FROM dbo.help_case WITH(NOLOCK) WHERE name = :name',
                $parameters
            );

        }
        else {

            $helpId = $this->processHandler->getAttributeValue('1067', $groupProcessInstanceId, AttributeValue::VALUE_INT);

            if(!empty($helpId)) {

                $parameters = [
                    [
                        'key' => 'id',
                        'value' => $helpId,
                        'type' => PDO::PARAM_INT
                    ]
                ];

                $output = $this->queryManager->executeProcedure('SELECT note FROM dbo.help_case WITH(NOLOCK) WHERE id = :id',
                    $parameters
                );

            }

        }

        $outputText = '';

        if(!empty($output)) {
            $outputText = $output[0]['note'];
        }

        $isVip = $this->processHandler->getAttributeValue('1004', $groupProcessInstanceId, AttributeValue::VALUE_STRING);
        if(!empty($isVip)){
            $outputVip = $this->queryManager->executeProcedure("SELECT note FROM dbo.help_case WITH(NOLOCK) WHERE name = 'Vip'");
            $outputText .= ($outputText == '' ? '' : '<br>'). $outputVip[0]['note'];
        }

        return $outputText;

    }

    /**
     * @param $groupProcessInstanceId
     * @param string $column
     * @return string
     */
    public function isHelp($groupProcessInstanceId, $column = '') {

        return $this->processHandler->isHelp($groupProcessInstanceId, $column);

    }


    public function isInDateRange($groupProcessInstanceId, $monFridayFrom = null, $monFridayTo = null, $saturdayFrom = null, $saturdayTo = null, $sundayFrom = null, $sundayTo = null ){
        $dateFrom = null;
        $dateTo = null;
        $dayOfWeek = date('w');
        $now = new \DateTime();


        if($monFridayFrom && $monFridayTo && $dayOfWeek >= 1 && $dayOfWeek <= 5){
            $dateFrom = new \DateTime('today'.$monFridayFrom);
            $dateTo = new \DateTime('today '.$monFridayTo);
        }
        if($saturdayFrom && $saturdayTo && $dayOfWeek == 6){
            $dateFrom = new \DateTime('today'.$saturdayFrom);
            $dateTo = new \DateTime('today '.$saturdayTo);
        }
        else if($sundayFrom && $sundayTo && $dayOfWeek == 7){
            $dateFrom = new \DateTime('today'.$sundayFrom);
            $dateTo = new \DateTime('today '.$sundayTo);
        }

        if(!$dateFrom || !$dateTo){
            return false;
        }

        return $now >= $dateFrom && $now <= $dateTo;
    }



    public function dayOfWeek($groupProcessInstanceId, $date = null)
    {
        $format= 'Y-m-d H:i:s';
        if(empty($date)){
            $date = date($format);
        }
        $dayofweek = date('w', strtotime($date));
        return $dayofweek;
    }

    public function hour($groupProcessInstanceId, $date = null){
        $format= 'Y-m-d H:i:s';
        if(empty($date)){
            $date = date($format);
        }
        $moment = new Moment($date,'Europe/Warsaw');
        $moment->setTimezone('Europe/Warsaw');
        $hour = $moment->format('H');
        return $hour;
    }

    /**
     * Usage: {#dateInNumbers(74,233|1)#}
     * Change date to days/hours/years till now
     * @param $grupProcessInstanceId
     * @param $dateAttribute
     * @param int $dateType
     */
    public function dateInNumbers($groupProcessInstanceId, $dateAttribute, $dateType =1)
    {


        $date=  $this->processHandler->getAttributeValue($dateAttribute, $groupProcessInstanceId,AttributeValue::VALUE_DATE);

        if($date) {
            $dateMoment = Moment::createFromFormat('Y-m-d H:i:s', $date);
            $now = new \DateTime();
            $interval = $dateMoment->diff($now);
            switch ($dateType) {
                case 1: //days
                    return $interval->format('%a');
                    break;
                case 2: // months
                    return $interval->format('%m');
                    break;
                case 3: //years
                    return $interval->format('%y');
                default:
                    return $interval->format('%a');
                    break;
            }
        }else{
            return 0;
        }
    }

    /**
     * @param $groupProcessInstanceId
     * @return int
     */
    public function showTotalCostOfService($groupProcessInstanceId) {
        return $this->processHandler->totalCostOfService($groupProcessInstanceId);
    }

    public function rentalOfferMaxClass($groupProcessInstanceId)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'class',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000,
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_rental_offer_max_class @groupProcessInstanceId = :groupProcessInstanceId, @class = :class",
            $parameters
        );

        if (isset($output['class'])) {
            return $output['class'];
        }

        return '';

    }

    public function  DCRequestType($groupProcessInstanceId){
        $processHandler = $this->processHandler;
        $requestID = $processHandler->getAttributeValue('883,890',$groupProcessInstanceId,'int');

        $request = '';
        if ($requestID)
        {
            /** @var ValueDictionary $dict */
            $dict = $this->getDictionaryRepo()->findOneBy(['type' =>'serviceReportAskType', 'value'=>$requestID]);
            $request = $dict->getText();
        }

        if ($request == '')
        {
            $platformId = $processHandler->getAttributeValue('253',$groupProcessInstanceId,'int');

            if(in_array($platformId,[11,6])){
                $request = 'Prośba o udostępnienie pojazdu';
            }
        }
        return $request;
    }

    /**
     * @param $groupProcessInstanceId
     * @param int $final
     * @return mixed
     */
    public function rentalTotalDuration($groupProcessInstanceId, $final = 0)
    {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'final',
                'value' => $final,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'rentalDuration',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100,
            ]

        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_rental_start_and_duration @groupProcessInstanceId = :groupProcessInstanceId, @final = :final, @rentalDuration = :rentalDuration",
            $parameters
        );

        if (isset($output['rentalDuration'])) {
            return $output['rentalDuration'];
        }

        return '';

    }

    public function canWorkshopExtendRental($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'canExtend',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_can_workshop_extend_rental @groupProcessInstanceId = :groupProcessInstanceId, @canExtend = :canExtend",
            $parameters
        );


        if (isset($output['canExtend'])) {
            return $output['canExtend'];
        }

        return 0;
    }

    public function localWorkshopforCrossBorder($groupProcessInstanceId, $key = 'result'){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'result',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'message',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 4000
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_use_local_workshop_for_cross_border @groupProcessInstanceId = :groupProcessInstanceId, @result = :result, @message = :message",
            $parameters
        );

        return $output[$key];
    }


    public function caseRentalInfo ($groupProcessInstanceId){

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'info',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 1000
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_case_rental_info @groupProcessInstanceId = :groupProcessInstanceId, @info = :info",
            $parameters
        );


        if (isset($output['info'])) {
            return $output['info'];
        }

        return '';
    }


    public function hasProgram($groupProcessInstanceId, $programId)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'programId',
                'value' => $programId,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ],
            [
                'key' => 'result',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100,
            ]

        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC p_has_program @groupProcessInstanceId = :groupProcessInstanceId, @programId = :programId, @result = :result",
            $parameters
        );

        if (isset($output['result'])) {
            return $output['result'];
        }

        return 0;
    }

    public function date($format)
    {
        try{
            return  date($format);
        }catch (\Exception $exception)
        {
            return '';
        }
    }

    /**
     * @param $groupProcessInstanceId
     * @param bool $onlyIds
     * @return int
     */
    public function invoiceItems($groupProcessInstanceId, $onlyIds = false) {

        $qm = $this->queryManager;

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => (int)$groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'onlyIds',
                'value' => (int)$onlyIds,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure('EXEC dbo.p_invoice_items @groupProcessInstanceId = :groupProcessInstanceId, @onlyIds = :onlyIds',
            $parameters
        );

        if($onlyIds and count($output) > 0){
            if (isset($output[0]['entityIds'])) {
                return $output[0]['entityIds'];
            }
        }
        else if(!empty($output) && is_array($output)) {

            $table = '<div id="invoice-items" class="table-responsive"><table class="table table-striped table-hover table-bordered table-vam">'
                .'<thead><tr><th>'.$qm->translate('LP').'</th><th>'.$qm->translate('Nazwa towaru / usługi').'</th>'
                .'<th>'.$qm->translate('Ilość').'</th><th>'.$qm->translate('Cena netto').'</th>'
                .'<th>'.$qm->translate('wartość netto').'</th><th>VAT</th><th>'.$qm->translate('Kwota VAT').'</th><th>'
                .$qm->translate('Wartość brutto').'</th></tr></thead><tbody id="users-tbody">';

            foreach ($output as $key => $item) {
                $total = $item['name'] == $qm->translate("RAZEM") ? 1 : 0;

                if($total){
                    $table .= '<tr><td></td><td colspan="3"><strong>'.$item['name'].'</strong></td><td><strong>'
                        .$item['valueTotal'].'</strong></td><td></td><td><strong>'.$item['vatValue'].'</strong></td><td><strong>'
                        .$item['valueTotalWithVat'].'</strong></td></tr>';
                }
                else{
                    $table .= '<tr><td>'.$item['id'].'</td><td>'.$item['name'].'</td><td>'.$item['quantity'].'</td><td>'
                        .$item['value'].'</td><td>'.$item['valueTotal'].'</td><td>'.$item['vat'].'%</td><td>'.$item['vatValue']
                        .'</td><td>'.$item['valueTotalWithVat'].'</td></tr>';
                }
            }

            $table .= '</tbody></table></div>';
            return $table;
        }

        return '';

    }

    public function costServicePZU($groupProcessInstanceId, $serviceId = 0) {

        if(empty($serviceId) || $serviceId === 0) {
            return '';
        }

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'serviceId',
                'value' => $serviceId,
                'type' => PDO::PARAM_INT,
                'length' => 100
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            'EXEC dbo.pzu_viewer @groupProcessId = :groupProcessInstanceId, @serviceId = :serviceId',
            $parameters
        );

        if(!empty($output) && isset($output[0]['value'])) {
            return $output[0]['value'];
        }

        return '';

    }

    public function isHolidayInCountry(string $country, string $date = 'now')
    {
        $date = new \DateTime($date);
        $dateYear = $date->format('Y');

        /** @var ValueDictionary $countryDictionary */
        $countryDictionary = $this->entityManager->getRepository(ValueDictionary::class)->findOneBy(
            [
                'type' => 'europeanCountry',
                'text' => $country,
            ]
        );

        $holidays = Yasumi::create(str_replace(' ', '', $countryDictionary->getArgument3()), $dateYear);

        return (int)$holidays->isHoliday($date);
    }

    /**
     * @return string
     */
    public function companyKey(): string
    {

        if(!$this->userInfo->getUser()) {
            return '';
        }

        if($companyId = $this->userInfo->getUser()->getCompanyId()) {

            $parameters = [
                [
                    'key' => 'companyId',
                    'value' => $companyId,
                    'type' => PDO::PARAM_INT
                ]
            ];

            $output = $this->queryManager->executeProcedure(
                'SELECT cpp.company_key FROM dbo.company_platform_permissions as cpp WITH(NOLOCK) WHERE cpp.companyId = :companyId',
                $parameters
            );

            if(!empty($output) && !empty($output[0]['company_key'])) {
                return $output[0]['company_key'];
            }

        }

        return '';

    }

    public function isInvoiceNumberUnique($groupProcessInstanceId, $invoiceNumber, $partnerId){
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'partnerId',
                'value' => $partnerId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'invoiceNumber',
                'value' => $invoiceNumber,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "SELECT dbo.f_is_invoice_number_unique(:groupProcessInstanceId, :invoiceNumber, :partnerId)",
            $parameters
        );

        if(array_key_exists('', $output[0])) {
            return $output[0][''];
        }

        return 1;

    }

    public function matrixRecord($groupProcessInstanceId, $entityId, $column){
        $parameters = [
            [
                'key' => 'groupProcessId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'entityId',
                'value' => $entityId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "select * from SYNC.TraElem where entityDefId = :entityId and groupProcessId = :groupProcessId",
            $parameters
        );

        if(is_array($output)&& ($output[0]??false)) {
            if (array_key_exists($column, $output[0])) {
                return $output[0][$column];
            }
        }

        return '';

    }

    public function numberLength($data)
    {
        $numbers = preg_replace('/[^0-9]/', '', $data);

        return strlen($numbers);
    }

    public function dateIsLater($groupProcessInstanceId, $date1, $date2) {
        if (!$date1 instanceof \DateTime) {
            $date1 = new \DateTime($date1);
        }

        if (!$date2 instanceof \DateTime) {
            $date2 = new \DateTime($date2);
        }

        return (int)( ($date1->getTimestamp() - $date2->getTimestamp() ) > 0 );
    }

    public function canChoosePreferredServices($groupProcessInstanceId)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'canChoosePreferredServices',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 10
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_can_choose_preferred_services @groupProcessInstanceId = :groupProcessInstanceId, @canChoosePreferredServices = :canChoosePreferredServices",
            $parameters
        );

        return $output['canChoosePreferredServices'];
    }

    public function prefixedPhoneNumber($number)
    {
        if(strpos($number,'+') !== false){
            return $number;
        }
        else if(strpos($number,'000') === 0){
            return '+'.substr($number,3);
        }
        else if(strpos($number,'00') === 0){
            return '+'.substr($number,2);
        }
        else {
            if(strpos($number,'0') === 0){
                $number = substr($number,1);
            }
            /** @var Config $prefix */
            $prefix = $this->entityManager->getRepository(Config::class)->findOneByKey('home_country_phone_prefix');
            if ($prefix) {
                $number = '+'.$prefix->getValue().$number;
            }
            return $number;
        }
    }


    public function showCarefleetParkingText($groupProcessInstanceId)
    {

        try {
            $serviceId = $this->processHandler->getServiceIdFromGroupId($groupProcessInstanceId);
            if ($serviceId == 1) {

                $partnerId = $this->processHandler->getAttributeValue('522', $groupProcessInstanceId, 'int');

                $platformId = $this->processHandler->getAttributeValue('253', $groupProcessInstanceId, 'int');
                if ($platformId == 78 && !is_null($partnerId)) {
                    $query = 'SELECT top 1 dbo.f_exists_in_split(locationType,202) from partners_services_cache where id = :partnerId ';
                    $parameters = [
                        [
                            'key' => 'partnerId',
                            'value' => $partnerId,
                            'type' => PDO::PARAM_STR
                        ]
                    ];
                    $result = $this->queryManager->executeProcedure(
                        $query,
                        $parameters
                    );
                    if (!empty($result)) {
                        return true;
                    } else {
                        return false;
                    }

                }
                return false;
            }
        }catch (\Exception $e)
        {
            return false;
        }
        return false;
    }

    public function carefleetCallToBRSService($groupProcessInstanceId)
    {
        try {
            $platformId = $this->processHandler->getAttributeValue('253', $groupProcessInstanceId, 'int');
            $eventType = $this->processHandler->getAttributeValue('491', $groupProcessInstanceId, 'int');
            $query = "SELECT top 1 dbo.f_exists_in_split(locationType, '85,94,95,98,99,100,101,196,197,202') exist from partners_services_cache where id = :partnerId ";
            $contractor = $this->processHandler->getAttributeValue('610', $groupProcessInstanceId, 'int') ?? 0;
            $parameters = [
                [
                    'key' => 'partnerId',
                    'value' => $contractor,
                    'type' => PDO::PARAM_STR
                ]
            ];
            $result = $this->queryManager->executeProcedure(
                $query,
                $parameters
            );


            if (
                !empty($result) &&
                isset($result[0])&&
                isset($result[0]['exist'])&&
                $result[0]['exist'] >1
            ) {
                $partnerType = true;
            } else {
                $partnerType = false;
            }

            return (int) ($platformId == 78 && in_array($eventType, [1, 5, 8]) && $partnerType === true);

        } catch (\Exception $exception) {
            return false;
        }
    }

    public function arcMakeModel($makeModelValueId)
    {
        $parameters = [
            [
                'key' => 'value',
                'value' => $makeModelValueId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "select dbo.f_arcMakeModelCode(:value)",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';
    }

    public function rentalSummaryInfo($groupProcessInstanceId, $final = 1, $key = 'rentalDuration')
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'final',
                'value' => $final,
                'type' => PDO::PARAM_INT,
                'length' => 10
            ],
            [
                'key' => 'startDate',
                'value' => null,
                'type' => PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT,
                'length' => 100
            ],
            [
                'key' => 'rentalDuration',
                'value' => null,
                'type' => PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT,
                'length' => 10
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "EXEC dbo.p_rental_start_and_duration @groupProcessInstanceId = :groupProcessInstanceId, @final = :final, @rentalStartDate = :startDate, @rentalDuration = :rentalDuration",
            $parameters
        );

        return $output[$key];
    }

    public function sparxRentalSummary($groupProcessInstanceId)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->queryManager->executeProcedure(
            "exec dbo.p_sparx_rental_summary @groupProcessInstanceId = :groupProcessInstanceId",
            $parameters
        );

        return count($output) > 0 ? $output[0][''] : '';
    }


    public function caseTotalCost($groupProcessInstanceId) {

        $rootId = $this->processHandler->getRootId($groupProcessInstanceId);
        $parameters = [

            [

                'key' => 'rootId',

                'value' => (int)$rootId,

                'type' => PDO::PARAM_INT

            ],

        ];

        $output = $this->queryManager->executeProcedure(

            'EXEC dbo.caseTotalCost @rootId=:rootId',

            $parameters);

        if(!empty($output) && isset($output[0]['Price'])) {

            return $output[0]['Price'];

        }

        return '';

    }

    public function countCasesWithThatVin($groupProcessInstanceId)
    {

        $rootId = $this->processHandler->getRootId($groupProcessInstanceId);

        $vin = $this->processHandler->getAttributeValue('74,71', $groupProcessInstanceId, 'string');


        if ($vin) {
            $query = "SELECT count(root_process_instance_id) counter from attribute_value with (nolock) where attribute_path='74,71'
                                                                                         and value_string =:vin
                                                                                         and root_process_instance_id <> :rootId
    and root_process_instance_id = group_process_instance_id";
            $parameters = [
                [
                    'key' => 'vin',
                    'value' => $vin,
                    'type' => PDO::PARAM_STR,
                    'length' => 100
                ],
                [
                    'key' => 'rootId',
                    'value' => (int)$rootId,
                    'type' => PDO::PARAM_INT
                ]
            ];
            $count = $this->queryManager->executeProcedure($query, $parameters);
            if (!empty($count)) {

                return $count[0]['counter'] ?? 0;
            } else {
                return 0;
            }
        }

        return 0;


    }

    public function caseEventDate($groupProcessInstanceId)
    {
        $rootId = $this->processHandler->getRootId($groupProcessInstanceId);
        $parameters = [['key' => 'rootId', 'value' => (int)$rootId, 'type' => PDO::PARAM_INT],];
        $output = $this->queryManager->executeProcedure('EXEC dbo.p_case_event_date @rootId=:rootId', $parameters);
        if (!empty($output) && isset($output[0]['EventDate'])) {
            return $output[0]['EventDate'];
        }
        return '';
    }
    public function executorPhoneNumber($groupProcessInstanceId)
    {
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ]
        ];
        $output = $this->queryManager->executeProcedure('EXEC dbo.p_executorPhoneNumber @groupProcessInstanceId = :groupProcessInstanceId', $parameters);
        if (!empty($output) && isset($output[0]['phone'])) {
            return $output[0]['phone'];
        }
        return '';
    }


    /**
     * Returns the type of the PSA Program ( i.e - for BWB form )
     * example of use : "{#PSAProgramType({@202*@})#}" == "3"
     *
     * @param $groupProcessInstanceId
     * @param null $programId
     * @return int
     */
    public function PSAProgramType($groupProcessInstanceId, $programId = null): int
    {


        if (is_null($programId)) {
            $programId = $this->processHandler->getAttributeValue('202', $groupProcessInstanceId, 'string');
        }
        $programId = (int)$programId;


        switch ($programId) {
            case (in_array($programId, [
                593,
                597,
                601,
                614,
                617,
                620
            ])):
//                GWARANCJA UMOWNA
                return 1;
                break;
            case (in_array($programId, [
                616,
                619,
                622,
                629,
                630
            ])):
//                CAM DS, Citroen, Peugeot(programy „od 2019”):
                return 2;
                break;
            case (in_array($programId, [
                594,
                598,
                602,
                615,
                618,
                621
            ])):
//                UMOWA SERWISOWA: PCD
                return 3;
                break;
            default:
                return 0;
        }
    }



    public function countMulti($groupProcessInstanceId, $attributePath, $notEmpty = false){
        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $groupProcessInstanceId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'attributePath',
                'value' => $attributePath,
                'type' => PDO::PARAM_STR,
                'length' => 100
            ],
        ];

        if($notEmpty){
            $output = $this->queryManager->executeProcedure(
                "SELECT id FROM dbo.attribute_value where group_process_instance_id = :groupProcessInstanceId and attribute_path = :attributePath and value_int > 0",
                $parameters
            );
        }
        else{
            $output = $this->queryManager->executeProcedure(
                "SELECT id FROM dbo.attribute_value where group_process_instance_id = :groupProcessInstanceId and attribute_path = :attributePath",
                $parameters
            );
        }

        return count($output);
    }

    public function dateAdd($date, $interval){
        $date = new \DateTime($date);
//        $date->add(new \DateInterval($interval));
        return $date->format('Y-m-d H:i:s');
    }

}



