<?php

namespace CaseBundle\Utils;

use SplFileObject;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

class TempFileHandler {

    /** @var SplFileObject $fileObject */
    public $fileObject;

    public $source;

    public $fileBasename;

    public $fileName;

    public $fileExtension;

    public $path;

    /**
     * TempFileHandler constructor.
     */
    public function __construct()
    {
        // Stworzenie temp file
        $this->path = tempnam(sys_get_temp_dir(), 'ATLAS_TMP_');

    }

    public function __destruct()
    {
        $this->removeTempFile();
    }

    static function fromFile($pathFile) {
        return (new TempFileHandler())->createFromFile($pathFile);
    }

    static function fromUrl($url) {
        return (new TempFileHandler())->createFromUrl($url);
    }

    static function strposa($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }

    private function create() {

        // Stworzenie obiektu pliku
        $this->fileObject = new SplFileObject($this->path);

        $this->setFileInfo(pathinfo($this->source));

        return $this;

    }

    public function createFromFile($source) {

        $this->source = $source;
        copy($this->source, $this->path);
        return $this->create();

    }

    public function createFromUrl($source) {

        $this->source = $source;

        // Zapisanie zdjęcia do pliku tymczasowego
        file_put_contents($this->path, file_get_contents($this->source));

        return $this->create();

    }

    public function removeTempFile() {

        if($this->path && file_exists($this->path)) {
            unlink($this->path);
        }

    }

    /**
     * @param $pathInfo
     */
    private function setFileInfo($pathInfo) {

        $this->fileBasename = $pathInfo['basename'];
        $this->fileName = $pathInfo['filename'];

        if(!isset($pathInfo['extension'])) {
            $extension = (str_replace("image/", "", $this->getContentType()));
        }
        else {
            $extension = $pathInfo['extension'];
        }

        $this->fileExtension = $extension;

    }

    public function getSize() {
        return $this->fileObject->getSize();
    }

    public function getContentType() {

        return (MimeTypeGuesser::getInstance())->guess($this->path);
//        return finfo_file(finfo_open(), $this->tempPath);

    }

    public function getContent() {
        return $this->fileObject->openFile()->fread($this->getSize());
    }

}
