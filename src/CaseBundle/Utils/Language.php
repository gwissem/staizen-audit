<?php

namespace CaseBundle\Utils;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

final class Language extends ExpressionLanguage
{
    const VGP_PLATFORM_ID = 11;

    protected function registerFunctions()
    {

        $this->register('date', function ($date, $format) {
            return sprintf('%s', $date);
        }, function (array $values, $date, $format) {
            $returnDate = new \DateTime($date);
            return $returnDate->format($format);
        });

        $this->register('dateDiff', function ($date1, $date2) {
            return sprintf('%d');
        }, function (array $values, $date1, $date2) {
            return (strtotime($date1) - strtotime($date2));
        });

        $this->register('strtotime', function ($date) {
            return sprintf('%d');
        }, function (array $values, $date) {
            return strtotime($date);
        });

        $this->register('dateIsEarlier', function ($date1, $date2) {
            return sprintf('true|false');
        }, function ($arguments, $date1, $date2) {

            if (!$date1 instanceof \DateTime) {
                $date1 = new \DateTime($date1);
            }

            if (!$date2 instanceof \DateTime) {
                $date2 = new \DateTime($date2);
            }

            return ( ($date2->getTimestamp() - $date1->getTimestamp() ) > 0 );
        });

        $this->register('dateIsLater', function ($date1, $date2) {
            return sprintf('true|false');
        }, function ($arguments, $date1, $date2) {

            if (!$date1 instanceof \DateTime) {
                $date1 = new \DateTime($date1);
            }

            if (!$date2 instanceof \DateTime) {
                $date2 = new \DateTime($date2);
            }

            return ( ($date1->getTimestamp() - $date2->getTimestamp() ) > 0 );

        });

        $this->register('dateTime', function ($date) {
            return sprintf('(new \DateTime(%s))', $date);
        }, function (array $values, $date) {
            return new \DateTime($date);
        });

        $this->register('productionYearVin', function () {
            return sprintf('true|false');
        }, function (array $values, $platformId, $vin, $date) {
            if ((int)$platformId == self::VGP_PLATFORM_ID) {
                return $this->productionYears($vin, $date);
            }
            return true;
        });

        $this->register('date_modify', function ($date, $modify) {
            return sprintf('%s->modify(%s)', $date, $modify);
        }, function (array $values, $date, $modify) {
            if (!$date instanceof \DateTime) {
                throw new \RuntimeException('date_modify() expects parameter 1 to be a Date');
            }
            return $date->modify($modify);
        });

        /** PARSE DECIMAL */

        $this->register('parseDecimal', function ($number) {
            return sprintf('floatval(%s)', $number);
        }, function($arguments, $number){
           return floatval($number);
        });

        /** PARSE INT */

        $this->register('parseInt', function ($number) {
            return sprintf('intval(%s)', $number);
        }, function($arguments, $number){
           return intval($number);
        });

        $this->register('isNumeric', function($string) {
                return 'true|false';
            },
            function($arguments, $string) {
                return is_numeric($string);
            }
        );

        $this->register('rentalDuration', function($string) {
            return 'true|false';
        },
            function($arguments, $program, $value, $path) {
                return $this->rentalDurationCheck($program, $value, $path);
            }
        );

        $this->register('strpos', function () {
            return 'true|false';
        }, function ($arguments, $haystack, $needle) {
            return strpos($haystack, $needle);
        });

        /** Data przeglądu
         * dateCarReview("{#value#}", "{@74,233@}", "{@74,106@}", "{@74,427@}")
         */
        $this->register('dateCarReview', function () {
            return sprintf('true|false');
        }, function ($arguments, $date, $DPR, $releaseDate, $productionYear) {

            try {
                $date = new \DateTime($date);
            }
            catch (\Exception $exc){
                return false;
            }

            $now = new \DateTime();
            if($date > $now) return false;

            //        - DPR
            //        - Data wydania
            //        - 1 stycznia roku produkcji
            //        - 1.1.2000

            if($DPR) {
                if($date < new \DateTime($DPR)) return false;
            }
            elseif($releaseDate) {
                if($date < new \DateTime($releaseDate)) return false;
            }
            elseif($productionYear) {
                if($date < new \DateTime('01-01-' . (int)$productionYear)) return false;
            }
            else {
                return ($date > new \DateTime('2000-01-01'));
            }

            return true;
        });

        /** VALIDATE EMAIL   */

        $this->register('validateEmail', function ($email) {
            return sprintf('true|false');
        }, function($arguments,$email){
            return filter_var(trim($email), FILTER_VALIDATE_EMAIL) == false ? false : true;
        });

    }

    protected function productionYears($vin, $date)
    {
        /*
         * A-1980, B-1981, C-1982, D-1983, E-1984,F-1985, G-1986, H-1987, J-1988, K-1989, L-1990, M-1991, N-1992, P-1993, R-1994, S-1995, T-1996, V-1997, W-1998, X-1999,
         * Y-2000, 1-2001, 2-2002, 3-2003, 4-2004, 5-2005, 6-2006, 7-2007, 8-2008, 9-2009, A-2010, B-2011, C-2012, D-2013, E-2014, F-2015, G-2016, H-2017, J-2018, K-2019,
         * L-2020, M-2021, N-2022, P-2023, R-2024, S-2025, T-2026, V-2027, W-2028, X-2029,Y-2030, 1-2031, 2-2032, 3-2033, 4-2034, 5-2035, 6-2036, 7-2037, 8-2038, 9-2039
         */

        $years = [
            'A' => 0,
            'B' => 1,
            'C' => 2,
            'D' => 3,
            'E' => 4,
            'F' => 5,
            'G' => 6,
            'H' => 7,
            'J' => 8,
            'K' => 9,
            'L' => 10,
            'M' => 11,
            'N' => 12,
            'P' => 13,
            'R' => 14,
            'S' => 15,
            'T' => 16,
            'V' => 17,
            'W' => 18,
            'X' => 19,
            'Y' => 20,
            1 => 21,
            2 => 22,
            3 => 23,
            4 => 24,
            5 => 25,
            6 => 26,
            7 => 27,
            8 => 28,
            9 => 29
        ];

        $char = substr($vin, 9,1);
        $yearDifference = $years[$char];
        $startYear = 1980;
        return $yearDifference == ($date - $startYear) % 30;
    }

    protected function rentalDurationCheck($program, $duration, $path)
    {
        $return = true;
        if(in_array($path,['789,786,240,153','812,789,786,240,153'])) {
            if ((in_array($program, ['414', '415', '419', '420']) && $duration > 5) || (in_array($program, ['417', '418', '402', '416', '']) && $duration > 3)) {
                $return = false;
            }
        }

        return $return;
    }


}

?>