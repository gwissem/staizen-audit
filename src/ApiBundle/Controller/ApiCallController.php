<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\ApiCall;
use ApiBundle\Form\Type\ApiCallCollectionType;
use ApiBundle\Form\Type\ApiCallFormType;
use AppBundle\Form\Type\ApiCallType;
use CaseBundle\Entity\Step;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class ApiCallController extends Controller
{


    /**
     * Displays a form to edit an existing ApiCall entity.
     *
     * @Route("/editor/{stepId}", name="admin_api_call_editor", options={"expose":"true"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $stepId)
    {

//        Get all for this step

        $doctrine = $this->getDoctrine();
        $step = null;
        if ($stepId) {
            $step = $doctrine->getRepository(Step::class)->find($stepId);
        }

        if (!$step) {
            return $this->createNotFoundException('Entity not found.');
        }
        $em = $doctrine->getManager();

        $editForm = $this->createForm(ApiCallCollectionType::class, $step);

        if($request->isMethod('POST')){

            $editForm->handleRequest($request);
            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $entities = $step->getApiCalls();
                /** @var ApiCall $entity */
                foreach ($entities as $entity) {
                    if($entity->getMethod()) {
                        $entity->setStep($step);
                        $entity->setUrl($entity->getType()->getUrl());
                        $em->persist($entity);
                        $em->flush();
                    }
                }
            }
        }



        return $this->render('ApiBundle:Admin/api-call:editor.html.twig', array(
            'form' => $editForm->createView(),
            'stepId' => $stepId
        ));
    }



    /**
     * Displays a form to add new  ApiCall entity.
     *
     * @Route("/editor/removeApiCall/{id}", name="admin_api_call_remove_api_call", options={"expose":"true"})
     * @Method({"DELETE"})
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function removeApiCallAction(Request $request, $id)
    {
        $apicall = $this->getDoctrine()->getRepository(ApiCall::class)->find($id);
        if ($apicall) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($apicall);
            $em->flush();
        }
        return new JsonResponse(['msg' => 'ApiCall has been removed']);
    }

}
