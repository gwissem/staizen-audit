<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ToolBundle\Entity\Tool;
use ToolBundle\Form\Type\UserToolDataModifyType;

class ToolController extends FOSRestController
{

    const API_LIMIT = 10000;

    /**
     * SHOW tool data
     *
     * @ApiDoc(
     *  section="Tools",
     *  description="SHOW tool data",
     *  requirements={
     *     {
     *      "name"="toolId",
     *      "dataType"="integer",
     *      "requirement"="\d+",
     *      "description"="tool id"
     *     }
     *  },
     *  parameters={
     *     {
     *      "name"="",
     *      "dataType"="string",
     *      "required"=false
     *     }
     *  },
     *  statusCodes={
     *     200="Returned when successful",
     *     403="Returned when the user is not permitted to view the tool result",
     *     404="Returned when tool is not found"
     *  }
     * )
     *
     * @View()
     *
     * @Route("/{toolId}", name="api_tool_show", requirements={"toolId": "\d+"} )
     * @Method({"GET"})
     * @ParamConverter("tool", class="ToolBundle:Tool", options={"mapping":{"toolId"="id"}})
     */

    public function showAction(Request $request, Tool $tool)
    {

        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $this->denyAccessUnlessGranted('read', $tool);
        $requestFilters = $request->query->all();
        unset($requestFilters['_format']);

        $filters = $requestFilters ?: $tool->getFormFilters();

        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool, $filters, self::API_LIMIT);

        if ($requestFilters) {
            $toolQueryManager->setFilters($filters);
        }


        $toolQueryManager->initQuery();
        $results = $toolQueryManager->getResults();

        //remove hidden columns from response
        foreach ($results as $i => $result) {
            foreach ($result as $key => $value) {
                foreach ($tool->getStyles() as $style) {
                    if ($style->getColumnName() === $key && $style->getHidden()) {
                        unset($result[$key]);
                        $results[$i] = $result;
                    }
                }
            }
        }

        $errors = $toolQueryManager->getErrors();

        return empty($errors) ? $results : $errors;
    }

    /**
     * EDIT tool row data. Data sent with API will overwrite current row data (even if empty/null data is sent).
     * Parsed UPDATE query is displayed as response body.
     *
     * @ApiDoc(
     *  section="Tools",
     *  description="EDIT tool row data",
     *  requirements={
     *     {
     *      "name"="toolId",
     *      "dataType"="integer",
     *      "requirement"="\d+",
     *      "description"="tool id"
     *     },
     *     {
     *      "name"="rowId",
     *      "dataType"="integer",
     *      "requirement"="\d+",
     *      "description"="row id"
     *     }
     *  },
     *  parameters={
     *     {
     *      "name"="",
     *      "dataType"="integer",
     *      "required"=false
     *     }
     *  },
     *  statusCodes={
     *     200="Returned when successful",
     *     403="Returned when the user is not permitted to edit the data",
     *     404="Returned when tool is not found"
     *  }
     * )
     *
     * @View(statusCode=200)
     *
     * @Route("/edit-data/{toolId}/{rowId}", name="api_tool_data_editor", requirements={"toolId": "\d+", "rowId": "\d+"})
     * @Method("POST")
     * @ParamConverter("tool", class="ToolBundle:Tool", options={"mapping":{"toolId"="id"}})
     * @Security("is_granted('ROLE_USER')")
     */

    public function toolDataUpdate(Request $request, $tool, $rowId)
    {
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $idName = $tool->getEditIdColumn() ?: 'id';

        if (!$idName) {
            return new Response('Id column name was not set for the tool', 200);
        }

        $this->denyAccessUnlessGranted('write', $tool);

        $data = [];
        $toolQueryManager = $this->get('tool.query_manager');

        $toolQueryManager->setParams($tool, [$idName => $rowId, $idName.'--op' => '='], 1);
        $toolQueryManager->initQuery();
        $results[] = $toolQueryManager->getResults();


        if (!empty($results)) {
            $data = !empty($results[0]) ? $results[0][0] : [];
        }

        $form = $this->createForm(
            $data,
            [
                'fields' => $tool->getFormFields(),
                'toolQueryManager' => $toolQueryManager,
                'id' => $rowId,
                'batch' => false,
                'idName' => $idName,
            ]
        );

        $form->handleRequest($request);

        if (($request->isMethod('POST') || $request->isMethod('PUT')) && $form->isValid()) {

            $formData = $this->get('api.form.tool_data_handler')->processForm($form, $request->files);
            $formData[$idName] = $rowId;

            $parsedQuery = $this->get('tool.query_parser')->parseQuery($tool->getUpdateQuery(), $formData);

            $toolQueryManager->executeCustomQuery($parsedQuery);

            return new Response($parsedQuery, 200);
        }

        return new Response('UNEXPECTED ERROR', 405);

    }

    /**
     * CREATE tool row data.
     * Parsed INSERT query is displayed as response body.
     *
     * @ApiDoc(
     *  section="Tools",
     *  description="CREATE tool row data",
     *  requirements={
     *     {
     *      "name"="toolId",
     *      "dataType"="integer",
     *      "requirement"="\d+",
     *      "description"="tool id"
     *     }
     *  },
     *  parameters={
     *     {
     *      "name"="",
     *      "dataType"="integer",
     *      "required"=false
     *     }
     *  },
     *  statusCodes={
     *     200="Returned when successful",
     *     403="Returned when the user is not permitted to edit the data",
     *     404="Returned when tool is not found"
     *  }
     * )
     *
     * @View(statusCode=200)
     *
     * @Route("/edit-data/{toolId}", name="api_tool_data_create", requirements={"toolId": "\d+"})
     * @Method("POST")
     * @ParamConverter("tool", class="ToolBundle:Tool", options={"mapping":{"toolId"="id"}})
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @param Tool $tool
     * @return Response
     */

    public function toolDataCreate(Request $request, $tool)
    {
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $idName = $tool->getEditIdColumn() ?: 'id';

        if (!$idName) {
            return new Response('Id column name was not set for the tool', 200);
        }

        $this->denyAccessUnlessGranted('write', $tool);

        $data = [];
        $toolQueryManager = $this->get('tool.query_manager');
        $toolQueryManager->setParams($tool);

        $form = $this->createForm(
            UserToolDataModifyType::class,
            $data,
            [
                'fields' => $tool->getFormFields(),
                'toolQueryManager' => $toolQueryManager,
                'id' => null,
                'batch' => false,
                'idName' => $idName,
                'dateParser' => $this->get('tool.date_parser.service'),
                'isDisableTrim' => $tool->isDisableTrim()
            ]
        );

        $form->handleRequest($request);

        if (($request->isMethod('POST') || $request->isMethod('PUT')) && $form->isValid()) {

            $formData = $this->get('api.form.tool_data_handler')->processForm($form, $request->files);
            $parsedQuery = $this->get('tool.query_parser')->parseQuery($tool->getInsertQuery(), $formData);

            $toolQueryManager->executeCustomQuery($parsedQuery);

            return new Response($parsedQuery, 200);
        }

        return new Response('UNEXPECTED ERROR', 405);

    }

    /**
     * DELETE row from the tool.
     * Parsed DELETE query is displayed as response body.
     *
     * @ApiDoc(
     *  section="Tools",
     *  description="DELETE row from the tool",
     *  requirements={
     *     {
     *      "name"="toolId",
     *      "dataType"="integer",
     *      "requirement"="\d+",
     *      "description"="tool id"
     *     },
     *     {
     *      "name"="rowId",
     *      "dataType"="integer",
     *      "requirement"="\d+",
     *      "description"="data row id",
     *      "required"="true"
     *     }
     *  },
     *  statusCodes={
     *     200="Returned when successful. Parsed delete query is displayed",
     *     403="Returned when the user is not permitted to remove the data",
     *     404="Returned when tool is not found"
     *  }
     * )
     *
     * @View(statusCode=200)
     *
     * @Route("/delete-data/{toolId}/{rowId}", name="api_tool_data_delete")
     * @Method("DELETE")
     * @ParamConverter("tool", class="ToolBundle:Tool", options={"mapping":{"toolId"="id"}})
     * @Security("is_granted('ROLE_USER')")
     */
    public function toolDataDeleteAction(Request $request, $toolId, $rowId)
    {
        $tool = $this->getDoctrine()
            ->getRepository('ToolBundle:Tool')
            ->find($toolId);
        if (!$tool) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono narzędzia'));
        }

        $this->denyAccessUnlessGranted('write', $tool);

        $deleteQuery = $tool->getDeleteQuery();
        if ($deleteQuery) {
            $toolQueryManager = $this->get('tool.query_manager');
            $toolQueryManager->setParams($tool);

            $parser = $this->get('tool.query_parser');

            $params = [$tool->getEditIdColumn() => $rowId];
            $parsedQuery = $parser->parseQuery($deleteQuery, $params);

            $toolQueryManager->executeCustomQuery($parsedQuery);

        } else {
            return new Response('No delete method set for the tool', 405);
        }

        return new Response($parsedQuery, 200);
    }
}
