<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Log;
use CaseBundle\Entity\AttributeValue;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SimpleXMLElement;
use SparxBundle\Entity\SparxIncoming;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SparxController extends FOSRestController
{

    /**
     * SHOW tool data
     *
     * @ApiDoc(
     *  section="Sparx",
     *  description="Create New Case from Sparx",
     *  statusCodes={
     *     200="Returned when successful",
     *     403="Returned when the user is not permitted to create case"
     *  }
     * )
     *
     *
     * @Route("/case/{eventType}/{sparxId}/{caseId}", name="sparx_api_case" )
     * @Method({"POST"})
     * @param Request $request
     * @param string $eventType
     * @param null $caseId
     * @param null $sparxId
     * @return JsonResponse
     */

    public function caseAction(Request $request, $eventType = 'create', $caseId = null, $sparxId = null)
    {

        try {

            $sparxIdAttribute = $this->getDoctrine()->getRepository(AttributeValue::class)->findBy(['valueString' => $sparxId]);

            if($sparxIdAttribute){
                $caseId = $sparxIdAttribute[0]->getRootProcess()->getId();
            }
//            else if($caseId){
//                $caseId = $this->get('case.handler')::parseCaseNumber($caseId);
//            }

            $data = $request->request->all();

            $sparxIncoming = new SparxIncoming();
            $json = \GuzzleHttp\json_encode($data, JSON_PRETTY_PRINT);

            try {
                $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><sparx_case></sparx_case>");
                $this->array_to_xml($data, $xml);
                $xml = $xml->saveXML();
            }
            catch (\Exception $e) {
                $xml = null;
            }

            $em = $this->getDoctrine()->getManager();

            $log = new Log();
            $log->setContent($json);
            $log->setName('sparx');
            $log->setParam1($sparxId);
            $log->setParam2($caseId);
            $log->setParam3($eventType);
            $em->persist($log);
            $em->flush();

            $sparxIncoming->setJson($json);
            $sparxIncoming->setXml($xml);
            $sparxIncoming->setStatus(1);
            $sparxIncoming->setEventType($eventType);
            $sparxIncoming->setSparxId($sparxId);
            $sparxIncoming->setGroupProcessId($caseId);

            $em->persist($sparxIncoming);
            $em->flush();

            return new JsonResponse(
                [
                    'status' => 'ok'
                ]
            );

        }
        catch (\Exception $e) {
            return new JsonResponse(
                [
                    'err' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

    }

    private function array_to_xml($array, &$xml_user_info) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xml_user_info->addChild("$key");
                    $this->array_to_xml($value, $subnode);
                }else{
                    $subnode = $xml_user_info->addChild("item$key");
                    $this->array_to_xml($value, $subnode);
                }
            }else {
                $xml_user_info->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }


}
