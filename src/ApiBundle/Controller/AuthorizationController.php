<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\AccessToken;
use ApiBundle\Entity\Client;
use AppBundle\Entity\Log;
use CaseBundle\Entity\AttributeValue;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class AuthorizationController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *  section="Authorization",
     *  description="Receive user info on success",
     *  statusCodes={
     *     200="Returned when successful",
     *     401="Unauthorized"
     *  }
     * )
     *
     *
     * @Route("/login", name="auth_api_login" )
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */

    public function loginAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        try {

            $em = $this->getDoctrine()->getManager();
            $client = $em->getRepository('ApiBundle:Client')->findOneBy(['name' => Client::NAME_API_ICS]);
            $token = null;
            $role = null;
            $companyId = null;

            if ($client) {
                $now = new \DateTime();
                $expires = $now->modify('+10 years');
                $user = $this->getUser();
                $isNew = false;

                $accessTokens = $em->getRepository(AccessToken::class)->findBy(['user' => $user], ['id' => 'desc']);

                if (empty($accessTokens)) {
                    /** @var AccessToken $accessToken */
                    $accessToken = new AccessToken();
                    $accessToken->setClient($client);
                    $accessToken->setUser($user);
                    $accessToken->setExpiresAt($expires->getTimestamp());
                    $accessToken->setScope(AccessToken::ICS_SCOPE);
                    $accessToken->setToken($this->get('user.info')->genApiAccessToken());
                    $em->persist($accessToken);
                    $em->flush();
                }
                else{
                    $accessToken = $accessTokens[0];
                }

                $token = $accessToken->getToken();

                if($user->hasGroup(Group::DRIVERS)){
                    $role = 'Driver';
                }

                if($user->hasGroup(Group::DISPOSITORS)){
                    $role = 'Dispositor';
                }

                $companyId = $user->getCompany() ? $user->getCompany()->getId() : null;

            }

            return new JsonResponse(
                [
                    'id' => $user->getId(),
                    'name' => $user->getName(),
                    'locale' => $user->getLocale(),
                    'token' => $token,
                    'companyId' => $companyId,
                    'role' => $role
                ]
            );

        }
        catch (\Exception $e) {
            return new JsonResponse(
                [
                    'err' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

    }
}
