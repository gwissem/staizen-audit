<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Log;
use CaseBundle\Entity\AttributeValue;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

class IcsController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *  section="Status",
     *  description="Trigger on new status",
     *  statusCodes={
     *     200="Returned when successful",
     *     401="Unauthorized"
     *  }
     * )
     *
     *
     * @Route("/new-status/{orderId}/{statusId}", name="api_ics_new_status" )
     * @Method({"POST"})
     * @param Request $request
     * @param $orderId int
     * @param $statusId int
     * @return JsonResponse
     */

    public function newStatusAction(Request $request, $orderId, $statusId)
    {
        try {

            $parameters = [
                [
                    'key' => 'orderId',
                    'value' => $orderId,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'statusId',
                    'value' => $statusId,
                    'type' => PDO::PARAM_INT
                ],
            ];

            $this->get('app.query_manager')->executeProcedure('exec dbo.p_ics_new_status @orderId = :orderId, @statusId = :statusId', $parameters, false);

            return new JsonResponse();
        }
        catch (\Exception $e) {
            return new JsonResponse(
                [
                    'err' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }

    }
}
