<?php

namespace ApiBundle\Controller;

use AppBundle\Utils\UCCXModel\Queues;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiUCCXController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *     section="UCCX",
     *     description="List Queues",
     *     views = { "uccx" }
     * )
     *
     *
     * @Route("/queue", name="uccx_queues" )
     * @Method({"GET"})
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public function queuesAction(): JsonResponse
    {

        $uccxService = $this->get('uccx.service');
        $queuesObj = $uccxService->getQueues();

        if($queuesObj instanceof Queues) {
            $queues = array_map(static function ($queue) use($uccxService) {
                return $uccxService->toArray($queue);
            }, $queuesObj->getQueues());
        }
        else {
            $queues = [];
        }

        return new JsonResponse(
            $queues
        );

    }

    /**
     *
     * @ApiDoc(
     *     section="UCCX",
     *     description="Get Queue",
     *     views = { "uccx" },
     *     requirements={
     *      {"name"="id", "dataType"="integer", "required"="\d+", "description"="Id of queue"}
     *     }
     * )
     *
     *
     * @Route("/queue/{id}", name="uccx_queue" )
     * @Method({"GET"})
     * @param $id
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public function queueAction($id): JsonResponse
    {

        $uccxService = $this->get('uccx.service');

        return new JsonResponse(
            $uccxService->toArray($uccxService->getQueue($id))
        );

    }

    /**
     *
     * @ApiDoc(
     *     section="UCCX",
     *     description="Set skill of queue",
     *     views = { "uccx" },
     *     requirements={
     *      {"name"="id", "dataType"="integer", "required"="\d+", "description"="Id of queue"}
     *     },
     *     parameters={
     *      {"name"="skill_id", "dataType"="integer", "required"="\d+", "description"="Id of skill"},
     *      {"name"="skill_competence_level", "dataType"="integer", "required"=false, "description"="Competence level of skill"},
     *      {"name"="skill_weight", "dataType"="integer", "required"=false, "description"="Weight of skill"},
     *     }
     * )
     *
     *
     * @Route("/queue/{id}", name="uccx_queue_skills" )
     * @Method({"POST"})
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public function setSkillOfQueueAction(Request $request, $id): JsonResponse
    {

        $uccxService = $this->get('uccx.service');

        $skillId = $request->request->get('skill_id');
        $skillPriority = $request->request->get('skill_competence_level', 5);
        $skillWeight = $request->request->get('skill_weight', 1);

        $uccxService->setSkillsOfQueue($id, [
            [
                'id' => (int)$skillId,
                'priority' => $skillPriority,
                'weight' => $skillWeight
            ]
        ]);

        return new JsonResponse(
            ['success' => true]
        );

    }

    /**
     *
     * @ApiDoc(
     *     section="UCCX",
     *     description="Get skill",
     *     views = { "uccx" },
     *     requirements={
     *      {"name"="id", "dataType"="integer", "required"="\d+", "description"="Id of skill"}
     *     }
     * )
     *
     *
     * @Route("/skill/{id}", name="uccx_skill" )
     * @Method({"GET"})
     * @param $id
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public function skillAction($id): JsonResponse
    {

        $uccxService = $this->get('uccx.service');

        return new JsonResponse(
            $uccxService->toArray($uccxService->getSkill($id))
        );

    }

    /**
     *
     * @ApiDoc(
     *     section="UCCX",
     *     description="Get stats",
     *     views = { "uccx" }
     * )
     *
     *
     * @Route("/agent-stats", name="uccx_agent_stats" )
     * @Method({"GET"})
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public function agentStatsAction(): JsonResponse
    {

        $uccxService = $this->get('uccx.service');

        return new JsonResponse(
            $uccxService->toArray($uccxService->getAgentStats())
        );

    }

    /**
     *
     * @ApiDoc(
     *     section="UCCX",
     *     description="Get agent",
     *     views = { "uccx" }
     * )
     *
     *
     * @Route("/agent/{userName}", name="uccx_agent" )
     * @Method({"GET"})
     * @param $userName
     * @return JsonResponse
     */

    public function getAgentAction($userName): JsonResponse
    {

        $uccxService = $this->get('uccx.service');

        $agents = $uccxService->getResources([$userName]);

        return new JsonResponse(
            $uccxService->toArray(reset($agents))
        );

    }

    /**
     *
     * @ApiDoc(
     *     section="UCCX",
     *     description="Set skills of Agent",
     *     views = { "uccx" },
     *     parameters={
     *      {"name"="skills", "dataType"="array", "required"=true, "description"="Array of skills [id,priority]"}
     *     }
     * )
     *
     *
     * @Route("/agent/{userName}/skills", name="uccx_agent_skills" )
     * @Method({"POST"})
     * @param Request $request
     * @param $userName
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public function setSkillsOfAgentAction(Request $request, $userName): JsonResponse
    {

        /**
         * Example of Skills Array
            skills[0][id]:157
            skills[0][priority]:5
            skills[1][id]:165
            skills[1][priority]:5
            skills[2][id]:177
            skills[2][priority]:5
         */

        $uccxService = $this->get('uccx.service');

        $skills = $request->request->get('skills', []);

        if(!empty($skills)) {
            $uccxService->setSkillsOfUsers([$userName], $skills);
        }

        return new JsonResponse(
            ['success' => true]
        );

    }

}
