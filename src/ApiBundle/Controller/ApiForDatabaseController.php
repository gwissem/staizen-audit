<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\ApiCall;
use ApiBundle\Form\Type\ApiCallCollectionType;
use ApiBundle\Form\Type\ApiCallFormType;
use AppBundle\Form\Type\ApiCallType;
use CaseBundle\Entity\Step;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class ApiForDatabaseController extends Controller
{

    const SECRET_TOKEN = '21AD4FFD-B18D-4123-841C-759954643953';

    /**
     * @Route("/parse-string", name="atlas_parse_string")
     * @Method("POST")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */

    public function parseStringAction(Request $request)
    {

        $token = $request->request->get('token', null);

        if(!$token || $token != self::SECRET_TOKEN) {
            return new Response('', 404);
        }

        try {

            $this->getDoctrine()->getConnection()->beginTransaction();
            $this->getDoctrine()->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);

            $string = $request->request->get('string', "");
            $processInstanceId = $request->request->get('processInstanceId', null);

            $result = $this->get('case.service.helper_parser_service')->parseString($string, $processInstanceId);

            if(!empty($result->parsedValue)) {
                return new Response($result->parsedValue);
            }

            $this->getDoctrine()->getConnection()->commit();

        } catch (\Exception $e) {
            $this->getDoctrine()->getConnection()->rollback();
            throw $e;
        }

        return new Response('');

    }

}
