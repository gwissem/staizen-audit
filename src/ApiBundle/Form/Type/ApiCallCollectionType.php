<?php
namespace ApiBundle\Form\Type;

use CaseBundle\Entity\Step;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApiCallCollectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('apicalls', CollectionType::class, array(
            'entry_type' => ApiCallFormType::class,
            'entry_options' => array('label' => false),
            'allow_add' => true,
            'allow_delete'=>true
        ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Step::class,
            'csrf_protection' => false,
        ));
    }

}