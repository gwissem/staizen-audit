<?php

namespace ApiBundle\Form\Type;

use ApiBundle\Entity\ApiCall;
use ApiBundle\Entity\ApiCallType;
use CaseBundle\Entity\Step;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApiCallFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('condition',TextType::class,  array(
                'label' => 'Warunek',
                'required' => false
            ))
            ->add('variant',NumberType::class,  array(
                'label' => 'Variant',
                'required' => false
            ))
            ->add('method',ChoiceType::class,  array(
                'label' => 'Metoda',
                'choices' => $this->availableMethods()
            ))
            ->add('dataType', ChoiceType::class, array(
                'label'=> 'Typ danych',
                'choices' => $this->availableDataTypes()
            ))
            ->add('type',EntityType::class,  array(
                'label' => 'Rodzaj API',
                'class' => ApiCallType::class
            ))
            ->add('body',TextareaType::class,  array(
                'label' => 'Treść',
                'attr' => [
                    'class' => 'json-codemirror',
                    'rows' => 20
                ]
            ))
            ->add('delete',ButtonType::class,array(
                'label'=>' Usuń',
                'attr'=> [
                    'class'=>'btn btn-danger btn-remove',
                ]
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ApiCall::class
        ));
    }

    private function availableDataTypes(){
        return [
            'JSON (application/json)' => 'json',
            'XML (application/xml)' => 'xml',
            'XML (text/xml)' => 'text/xml'
        ];
    }

    private function availableMethods(){
        return [
            'PUT' => 'PUT',
            'POST' => 'POST',
            'DELETE' => 'DELETE'
        ];
    }
}
