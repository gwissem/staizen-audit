<?php

namespace ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ApiCallJsonDefinitionRepository extends \Doctrine\ORM\EntityRepository
{
    public function getRootDefinitions()
    {
        return $this->createQueryBuilder('a')
            ->where('a.isRoot = 1');

    }
}
