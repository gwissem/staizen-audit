<?php

namespace ApiBundle\Service;

use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use DocumentBundle\Uploader\FileUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UserBundle\Service\UserInfoService;

class ToolDataFormHandler
{

    public function __construct(EntityManager $em, UserInfoService $userInfo, FileUploader $fileUploader)
    {
        $this->em = $em;
        $this->userInfo = $userInfo;
        $this->fileUploader = $fileUploader;
    }

    public function processForm($form, $files)
    {

        $formData = $form->getData();

        // file upload and document creation
        if ($files) {
            $em = $this->em;
            foreach ($files as $name => $fileInput) {
                if (is_array($fileInput) && $fileInput[0] !== null || $fileInput instanceof UploadedFile) {
                    $document = new Document();
                    $documentColumnName = str_replace('__files', '', $name);
                    $document->setName($documentColumnName.'_'.date('Ymd_His'));
                    $document->addCompany($this->userInfo->getUser()->getCompany());
                }

                if (is_array($fileInput)) {
                    foreach ($fileInput as $uploadedFile) {
                        $file = $this->fileUploader->upload($uploadedFile);
                        $file->setDocument($document);
                        $em->persist($file);
                    }
                } else {
                    $file = $this->fileUploader->upload($fileInput);
                    $file->setDocument($document);
                    $em->persist($file);
                }

                $em->persist($document);
                $em->flush();

                $formData[$documentColumnName] = $document->getId();
            }
        }

        return $formData;
    }
}