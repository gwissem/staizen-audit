<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Answer
 *
 * @ORM\Table(name="survey_answer")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\AnswerRepository")
 */
class Answer
{
    use ORMBehaviors\Timestampable\Timestampable;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\Column(name="survey_id", type="integer")
     */
    private $surveyId;

    /**
     * @var string
     * @ORM\Column(name="checked_button", type="string", length=255, nullable=true)
     */
    private $checkedButton;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set surveyId
     *
     * @param integer $surveyId
     *
     * @return Answer
     */
    public function setSurveyId($surveyId)
    {
        $this->surveyId = $surveyId;

        return $this;
    }

    /**
     * Get surveyId
     *
     * @return int
     */
    public function getSurveyId()
    {
        return $this->surveyId;
    }

    /**
     * Set checkedButton
     *
     * @param string $checkedButton
     *
     * @return Answer
     */
    public function setCheckedButton($checkedButton)
    {
        $this->checkedButton = $checkedButton;

        return $this;
    }

    /**
     * Get checkedButton
     *
     * @return string
     */
    public function getCheckedButton()
    {
        return $this->checkedButton;
    }
}

