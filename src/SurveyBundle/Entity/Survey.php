<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
/**
 * Survey
 *
 * @ORM\Table(name="survey")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\SurveyRepository")
 */
class Survey
{
    use ORMBehaviors\Timestampable\Timestampable;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var $uid
     *
     * @ORM\Column(name="uid", type="string", unique=true)
     */
    private $uid;

    /**
     * @var int
     *
     * @ORM\Column(name="rootId", type="integer", unique=true)
     */
    private $rootId;


    /**
     * @var string
     * @ORM\Column(name="phone_number", type="string", nullable=false)
     */
    private $phoneNumber;


    /**
     * @var int
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var int
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var int
     *
     * @ORM\Column(name="recommendation_rating", type="integer", nullable=true)
     */
    private $recommendationRating;


    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean",nullable=false)
     */
    private $active =true;


    /**
     * @var int $version
     * @ORM\Column(name="version", type="integer", nullable= false)
     */
    private $version = 1;

    /**
     * @var string $moreText
     * @ORM\Column(name="more_text", type="text", nullable=true)
     */
    private $moreText;


    /**
     * @var int
     * @ORM\Column(name="sms_inbox_id", type="integer", nullable=true)
     */
    private $smsInboxId = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return Survey
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set rootId
     *
     * @param integer $rootId
     *
     * @return Survey
     */
    public function setRootId($rootId)
    {
        $this->rootId = $rootId;

        return $this;
    }

    /**
     * Get rootId
     *
     * @return int
     */
    public function getRootId()
    {
        return $this->rootId;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Survey
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Survey
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }



    /**
     * Set recommendationRating
     *
     * @param integer $recommendationRating
     *
     * @return Survey
     */
    public function setRecommendationRating($recommendationRating)
    {
        $this->recommendationRating = $recommendationRating;

        return $this;
    }

    /**
     * Get recommendationRating
     *
     * @return int
     */
    public function getRecommendationRating()
    {
        return $this->recommendationRating;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getMoreText()
    {
        return $this->moreText;
    }

    /**
     * @param string $moreText
     */
    public function setMoreText($moreText)
    {
        $this->moreText = $moreText;
    }

    /**
     * @return int
     */
    public function getSmsInboxId()
    {
        return $this->smsInboxId;
    }

    /**
     * @param int $smsInboxId
     */
    public function setSmsInboxId($smsInboxId)
    {
        $this->smsInboxId = $smsInboxId;
    }




}

