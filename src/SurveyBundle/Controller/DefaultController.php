<?php

namespace SurveyBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\Annotations as Rest;
use MssqlBundle\PDO\PDO;
use SurveyBundle\Entity\Answer;
use SurveyBundle\Entity\Survey;
use Symfony\Component\HttpFoundation\JsonResponse;
Use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * Class DefaultController
 * @package SurveyBundle\Controller
 * @Rest\Route("/surveys")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/{uid}", name="showFormForSurvey")
     */
    public function indexAction($uid)
    {
        /** @var Survey $survey */
        $survey = $this->get('doctrine')->getRepository('SurveyBundle:Survey')->findOneBy(['uid' => $uid]);

        if (is_null($survey))
        {
         throw  $this->createNotFoundException();
        }
        else{
            if($survey->getState() >2)
            {
                $showThanks = 1;
           }else{
                $showThanks = 0;

            }
            if(!$survey->isActive() && $survey->getState() <3)
            {
                $showContact = 1;
            }
        }

        $rating = $survey->getRating() ?? 0;
        $type = $survey->getType();
        $buttonsList = $this->getButtonsList($type, $rating, $survey);

        $showRecommendText ='Na ile prawdopodobne jest, że polecisz nasze usługi swoim znajomym?  Oceń w skali 1-10, gdzie 1 „nie polecam”, a 10 „zdecydowanie polecam”.';


        $buttonsHeaderText = $this->getButtonsHeaderText($type,$rating);
        $showRecommend = 0;





        $version = $survey->getVersion() ?? 1;

        $config = [
            'buttonsHeaderText' =>$buttonsHeaderText,
            'buttonsList' =>$buttonsList,
            'rating' =>$rating,
            'uid'=>$uid,
            'showRecommend' =>$showRecommend??0,
            'showThanks' =>$showThanks??0,
            'showContact' =>$showContact??0,
            'showRecommendText' =>$showRecommendText,
            'version' =>$version

        ];

        return $this->render('SurveyBundle:Default:index.html.twig',$config);
    }


    /**
     * @Rest\Route("/setRating/{uid}", name="surveySetRating", options={"expose":true})
     * @Method("POST")
     * @param $uid
     */
    public function surveySetRating(Request $request, $uid){
        /** @var Survey $survey */
        $survey = $this->get('doctrine')->getRepository('SurveyBundle:Survey')->findOneBy(['uid' => $uid]);
        if(is_null($survey))
        {
            throw  $this->createNotFoundException();
        }

        $rating = (int) $request->request->get('rating');

        $survey->setRating($rating);
        $survey->setUpdatedAt(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($survey);
        $em->flush();

        return new JsonResponse([
            'status'=>'ok',
            'buttonsList' => $this->getButtonsList($survey->getType(),$rating,$survey),
            'buttonsHeaderText'=>$this->getButtonsHeaderText($survey->getType(),$rating)
        ]);
    }


    /**
     * @Method("POST")
     * @Rest\Route("/contact/{uid}", name="sendContactForm", options={"expose":true})
     * @param Request $request
     * @param $uid
     * @return JsonResponse
     */
    public function sendContactForm(Request $request, $uid){
        /** @var Survey $survey */
        $survey = $this->get('doctrine')->getRepository('SurveyBundle:Survey')->findOneBy(['uid' => $uid]);
        $noteHandler = $this->get('note.handler');

        if (is_null($survey))
        {
            throw  $this->createNotFoundException();
        }
        $survey->setState(5); // ContactFormSend


        $content = 'Wiadomość z formularza kontaktowego: ';
        $contentText = $request->request->get('text');

        $content = $content.' '.$contentText;

        $kernel = $this->get('kernel');
        $devMode = $kernel->isDebug();
        if(!$devMode) {
            $noteHandler->addNote(
                $survey->getRootId(),
                'email',
                $content,
                null,
                'dbt@starter24.pl'
            );

        }
        $survey->setUpdatedAt(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($survey);
        $em->flush();


        return new JsonResponse(['status'=>'ok']);
    }

    /**
     * @Method("POST")
     * @Rest\Route("/collect/{uid}", name="collectSurvey", options={"expose":true})
     * @param Request $request
     * @param $uid
     * @return JsonResponse
     */
    public function collectSurvey(Request $request, $uid){
        /** @var Survey $survey */
        $survey = $this->get('doctrine')->getRepository('SurveyBundle:Survey')
            ->findOneBy(
                ['uid' => $uid]
            );

        if (is_null($survey))
        {
            throw  $this->createNotFoundException();
        }


        $recommendRating = $request->request->get('recommendValue');
        $buttonsClickedList = $request->request->get('buttonsActivated');



        if($recommendRating??false)
        {
            $survey = $survey->setRecommendationRating($recommendRating);
        }

        $moreText = $request->request->get('moreText');


        if(!is_null($moreText) && !empty($moreText))
        {
//            More text exists
            $survey->setMoreText($moreText);
        }

        $survey= $survey
            ->setState(3);

        $survey->setActive(0);
        $survey->setUpdatedAt(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($survey);
        $em->flush();


        foreach ($buttonsClickedList as $button)
        {
            $answer = new Answer();
            $answer->setSurveyId($survey->getId());
            $answer->setCheckedButton($button);
            $em->persist($answer);
        }

        $em->flush();


        return new JsonResponse(['status'=>'ok']);
    }


    /**
     * @param $type
     * @param $rating
     * @return string
     */
    private function getButtonsHeaderText($type, $rating)
    {
        if($rating<=3)
        {
            $buttonsHeaderText ='Powiedz nam co poszło nie tak?';


        }else{
            $buttonsHeaderText = 'Powiedz nam co Ci się najbardziej spodobało?';
        }
        return $buttonsHeaderText;
    }

    /***
     * @param $type
     * @param $rating
     * @return array
     */
    private function getButtonsList($type,$rating, $survey)
    {
        $buttonsList = [];
        if ($type == 1  )
        {
            if ($rating <= 3) {
                $buttonsList = [
                    ['text' => 'Długo czekałem na połączenie z infolinią'],
                    ['text' => 'Konsultant był nieuprzejmy'],
                    ['text' => 'Konsultant był niekompetentny'],
                    ['text' => 'Moja sprawa nie została rozwiązana'],
                ];
            }else{
                $buttonsList = [
                    ['text' => 'Szybko połączyłem / połączyłam się z infolinią'],
                    ['text' => 'Konsultant był uprzejmy'],
                    ['text' => 'Konsultant był kompetentny'],
                    ['text' => 'Moja sprawa została rozwiązana'],

                ];
            }
        }else if ($type == 2)
        {
            if($rating <=3)
            {
                $buttonsList = [
                    ['text'=>'Długo czekałem/am na przyjazd mechanika'],
                    ['text'=>'Mechanik był nieuprzejmy'],
                    ['text'=>'Mechanik był niekompetenty'],
                    ['text'=>'Usterka nie została naprawiona'],
                ];
            }else{
                $buttonsList =[
                    ['text'=> 'Mechanik przyjechał szybko'],
                    ['text'=>'Mechanik był uprzejmy'],
                    ['text'=>'Mechanik był kompetenty'],
                    ['text'=>'Usterka została usunięta'],
                ];
            }
        }else if ($type == 3)
        {
            if($rating <=3)
            {
                $buttonsList = [
                    ['text'=>'Długo czekałem/am na przyjazd holownika'],
                    ['text'=>'Kierowca holownika był nieuprzejmy'],
                    ['text'=>'Kierowca holownika był niekompetenty'],
                ];
            }else{
                $buttonsList =[
                    ['text'=> 'Holownik przyjechał szybko'],
                    ['text'=>'Kierowca holownika był uprzejmy'],
                    ['text'=>'Kierowca holownika był kompetenty']
                ];
            }
        }else if ($type == 4)
        {
            if($rating <=3)
            {
                $buttonsList = [
                    ['text'=>'Długo czekałem/am na pojazd zastępczy'],
                    ['text'=>'Przedstawiciel wypożyczalni był nieuprzejmy']
                ];
            }else{
                $buttonsList =[
                    ['text'=> 'Szybko podstawiono pojazd zastępczy'],
                    ['text'=>'Przedstawiciel wypożyczalni był uprzejmy'],

                ];
            }
        }else if ($type ==5)
        {
            $parameters = [
                [
                    'key' => 'rootId',
                    'value' => (int)$survey->getRootId(),
                    'type' => PDO::PARAM_INT
                ]
            ];

            $query = 'EXEC dbo.get_services_names_by_root @rootId = :rootId';
            $result = $this->get('app.query_manager')->executeProcedure(
                $query, $parameters);

            $buttonsList = [];
            foreach ($result as $single)
            {
                if (! in_array((int) $single['serviceId'],[12,13])) // wykluczanie Monit + odwołanie
                {
                    $buttonsList[] = ['text'=>$single['name']];
                }
            }


        }

        $buttonsList[]=   ['text' => 'Inne', 'isMore' => 1];
        return $buttonsList;
    }
}
