var buttonToCheck = {
    props: ['bus', "text", "isMoreActivator"],
    data: function () {
        return {
            classes: 'btn btn-block',
            isActive: false,
            isMore: this.isMoreActivator
        }
    },
    template: '<div class="single-button" @click="toggleActive">' +
        '<button :class ="classes" type="button">{{text}}' +
        '<input type="checkbox" ref="checkbox" name="buttonClicked[]" :value="text" :data-isMore="isMoreActivator" class="hidden"/> '
        + '</button></div>',
    methods: {
        toggleActive: function () {


            if (this.isActive) {
                this.classes = 'btn btn-block';
                this.isActive = false;
                this.$refs.checkbox.checked = false;
                this.$root.optionChanged(-1);

            } else {
                this.classes = 'btn btn-block btn-success';
                this.isActive = true;
                this.$refs.checkbox.checked = true;
                if(this.isMore)
                {
                    this.$root.showSendButtonFromButtons = false;
                }
                this.$root.optionChanged(1);
            }

            if (this.isMoreActivator) {
                this.showMoreClicked();
            }

        },
        showMoreClicked: function () {
            this.$root.toggleShowMore();
        }

    }
};


var star = {
    props: ['position', 'active', 'bus'],
    data: function () {
        return {
            isActive: false
        }
    },
    template: '<div class="star" @click="changeRating">' +
        '<img v-if="isActive"  src="/bundles/survey/img/star-full.png" class="star-image star-full"/>' +
        '<img v-else src="/bundles/survey/img/star-empty.png" class="star-image star-empty" /> ' +
        '</div>',
    methods: {
        changeRating: function () {
            this.$root.changeRating(this.position);
        },
        setRating: function (rating) {

            if (this.position <= rating) {
                this.isActive = 1;
            } else {
                this.isActive = false;
            }
        }
    },
    mounted: function () {
        var that = this;

        this.bus.$on('setRating', function (rating) {
            that.setRating(rating);
        });
    },
};

var ratingOptions = {
    props: ['bus'],
    template: '<div class="rating">' +
        '<div class="rating-holder">' +
        '<div @click="changeRating(index)" :data-index="index" ref="ratingList" v-for="index in 10" :key = "index" :class="ratingClass">{{index}}</div>' +
        '</div>' +
        '<div class="rating-description">' +
        '<div class="pull-left">nie polecam</div>' +
        '<div class="pull-right">zdecydowanie polecam</div>' +
        '</div>' +
        '</div>',
    data: function () {
        return {
            rating: null,
            ratingClass: "rating-single"
        }
    },
    methods: {
        changeRating: function (clicked) {
            var allRatingOptions = (typeof this.$refs.ratingList !== "undefined") ? this.$refs.ratingList : [];

            allRatingOptions.forEach(function (single) {
                if (parseInt(single.dataset.index) === parseInt(clicked)) {
                    single.classList.add('active');
                } else {
                    single.classList.remove('active');
                }
            });
            this.rating = clicked;
            this.$root.setRecommendOptionValue(clicked);
        }
    }
};


var contactForm = {
    props: ['bus'],
    template: '<div class="contactForm">' +
        '<div class="contactFormHolder border-holder">' +
        '   <h3>Skontaktuj się z nami</h3>' +
        '    <h4>Twoja ankieta wygasła, ale możesz przesłać nam swoją opinię!</h4>' +
        '    <div class="arrow_box">' +
        '      <textarea class="form-control"  v-model="text" placeholder="Podziel się Twoimi wrażeniami..." tabindex="5" required></textarea>' +
        '    </div>' +
        '    <div>' +
        '<button class="btn btn-success send-survey" @click="sendForm">Gotowe</button>' +
        '</div>' +
        '</div>' +
        '</div>',
    data: function () {
        return {
            isSend: false,
            text: ''
        }
    },
    methods: {
        sendForm: function () {

            var dataToSend = {
                'text': this.text
            };
            var that = this;
            axios.post(Routing.generate('sendContactForm', {uid: uid}), dataToSend).then(function (response) {
                if (response.data.status === 'ok') {
                    that.changeViewToThanks();
                }
            });

        },
        changeViewToThanks: function () {
            this.$root.changeViewToThanks();
        }
    }
};

Vue.component('buttonToCheck', buttonToCheck);
Vue.component('contactForm', contactForm);
Vue.component('ratingOptions', ratingOptions);
Vue.component('star', star);


var app = new Vue({
    el: '#app',
    template:
        '<div class="appHolder">' +
        '<div v-if="showStars && !showThanks">' +
        '<div class="header header-list">Jak oceniasz otrzymaną pomoc?</div>' +
        '<div class="stars">' +
        '<star v-for="index in 5" :bus="bus" :position="index" :key="index" :active = false ></star>' +
        '</div>' +
        '</div>' +
        '<div v-if="showButtons">' +
        '<div class="border-holder">' +
        '<div class="header-list">{{buttonsHeaderText}}</div>' +
        '<div class="buttonHolder">' +
        '<buttonToCheck v-for="(button, key) in buttonsList" :bus="bus" :key="key" :text ="button.text" :isMoreActivator="button.isMore" ></buttonToCheck>' +
        '</div>' +
        '<div v-if="showMore" class="arrow_box">' +
        '<textarea class="form-control" v-model="moreText" name="moreTextarea" id="moreTextarea" placeholder="Powiedz nam co się stało..."></textarea>' +
        '</div>' +
        '</div>' +
        '<div v-if="showRecommend" class="showRecommended">' +
        '                   <div class="recommendedHeader">{{showRecommendText}} </div>' +
        '<ratingOptions :bus="bus"></ratingOptions>' +
        '</div>' +
        '<button  v-if="showSendButton && showSendButtonFromButtons && (!showMore || (moreText.length>0))" class="btn btn-success send-survey" @click="sendSurvey">Gotowe</button>' +
        '</div>' +
        '<div v-else-if="showContact" class="contactForm">' +
        '<contactForm :bus="bus"></contactForm>' +
        '   </div>' +
        '<div v-else-if="showThanks">' +
        '<div class="thanks-image">' +
        '<div class="image">' +
        '<img src="/bundles/survey/img/thanks.png"  class="thanksImage" /> ' +
        '</div>' +
        '<div class="thanks-text">Dziękujemy za Twoją opinię! </div>' +
        '</div>' +
        '</div>' +
        '</div>',
    data: {
        moreText: '',
        bus: new Vue(),
        rating: window.rating,
        buttonsList: window.buttonsList,
        showButtons: false,
        showMore: false,
        showContact: window.showContact,
        showSendButton: false,
        showSendButtonFromButtons: false,
        buttonsHeaderText: window.buttonsHeaderText,
        showRecommend: window.showRecommend,
        showRecommendText: window.showRecommendText,
        recommendValue: 0,
        uid: window.uid,
        showThanks: window.showThanks,
        showStars: 0,
        version: window.version
    },
    methods: {
        changeRating: function (rating) {
            // console.log(emit);
            this.bus.$emit('setRating', rating);
            this.rating = rating;
            this.setConfigForResult(rating)

        },
        setConfigForResult: function (rating) {
            var that = this;
            var data = {
                rating: rating
            };
            axios.post(Routing.generate('surveySetRating', {uid: uid}), data)
                .then(function (response) {
                    var dataAjax = response.data;
                    // handle success
                    if (dataAjax.status === "ok") {
                        that.showButtons = true;
                        that.buttonsList = dataAjax.buttonsList;
                        that.buttonsHeaderText = dataAjax.buttonsHeaderText

                    }
                })
        },
        changeViewToThanks: function () {
            this.showContact = 0;
            this.showButtons = 0;
            this.showSendButtonFromButtons = 0;
            this.showStars = 0;
            this.showThanks = 1;
        },
        optionChanged: function (buttonChecked) {
            var buttonsActivated = document.querySelectorAll('[name="buttonClicked[]"]:checked');

            if (buttonsActivated.length > 0) {

                this.showRecommend = true;
                this.showSendButtonFromButtons = true;
            } else {
                this.showSendButtonFromButtons = false;
                this.showRecommend = false;
            }
        },
        setRecommendOptionValue: function (recommendValue) {
            this.recommendValue = recommendValue
            this.showSendButton = true;
        },
        toggleShowMore: function () {
            this.showMore = !this.showMore;
        },
        sendSurvey: function () {
            var buttonsActivated = document.querySelectorAll('[name="buttonClicked[]"]:checked');

            buttonsActivated = Array.prototype.slice.call(buttonsActivated);
            buttonsActivated = buttonsActivated.filter(function (value) {
                return value.dataset.ismore != 1;
            });
            buttonsActivated = buttonsActivated.map(function (single) {
                return single.getAttribute('value');

            });

            if (this.showMore === false) {
                this.moreText = '';
            }

            var dataToSend = {
                'recommendValue': this.recommendValue,
                'buttonsActivated': buttonsActivated,
                'moreText': this.moreText
            };

            var uid = this.uid;
            var that = this;
            axios.post(Routing.generate('collectSurvey', {uid: uid}), dataToSend).then(function (response) {
                if (response.data.status === 'ok') {
                    that.changeViewToThanks();
                }
            });

        }
    },
    watch: {
    },
    mounted: function () {
        if (!this.showContact) {
            if (this.version == 2) {
                this.showStars = 1;
            } else {

                this.showButtons = 1;
            }

            if (this.showThanks == 1) {
                this.changeViewToThanks();
            }
        }
    }

});