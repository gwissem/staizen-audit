<?php

namespace SmsBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Poi
 *
 * @ORM\Table(name="sms")
 * @ORM\Entity(repositoryClass="SmsBundle\Repository\SmsRepository")
 */
class Sms
{

    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="sms_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_number", type="string", length=12)
     */
    private $number;

    /**
     * @ORM\Column(name="sms_text", type="text")
     */
    private $text;

    /**
     * @ORM\Column(name="sms_unicode", type="boolean", nullable=true)
     */
    private $unicode = true;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="sms_send_by", referencedColumnName="id")
     */
    private $sendBy;

    /**
     * @ORM\Column(name="delivered", type="boolean")
     */
    private $isDelivered = false;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_delivered_date", type="datetime", nullable=true)
     */
    private $deliveredDate;

    /**
     * @var int
     *
     * @ORM\Column(name="sms_delivery_date", type="datetime", nullable=true)
     */
    private $deliveryDate;

    /**
     * @var int
     *
     * @ORM\Column(name="ext_id", type="integer", nullable=true)
     */
    private $extId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Sms
     */
    public function setNumber($number): Sms
    {
        $parsedNumber = ltrim($number,0);
        $this->number = $parsedNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return Sms
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnicode()
    {
        return $this->unicode;
    }

    /**
     * @param mixed $unicode
     * @return Sms
     */
    public function setUnicode($unicode)
    {
        $this->unicode = $unicode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSendBy()
    {
        return $this->sendBy;
    }

    /**
     * @return Sms
     */
    public function setSendBy($sendBy)
    {
        $this->sendBy = $sendBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDelivered()
    {
        return $this->isDelivered;
    }

    /**
     * @param mixed $isDelivered
     * @return Sms
     */
    public function setIsDelivered($isDelivered)
    {
        $this->isDelivered = $isDelivered;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveredDate(): string
    {
        return $this->deliveredDate;
    }

    /**
     * @param string $deliveredDate
     * @return Sms
     */
    public function setDeliveredDate($deliveredDate): Sms
    {
        $this->deliveredDate = $deliveredDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryDate(): int
    {
        return $this->deliveryDate;
    }

    /**
     * @return Sms
     */
    public function setDeliveryDate($deliveryDate): Sms
    {
        $this->deliveryDate = $deliveryDate;
        return $this;
    }

    public function getExtId()
    {
        return $this->extId;
    }

    /**
     * @param int $extId
     * @return Sms
     */
    public function setExtId(int $extId): Sms
    {
        $this->extId = $extId;
        return $this;
    }




}

