<?php
// src/Acme/ApiBundle/Entity/Client.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ValueDictionaryRepository")
 * @ORM\Table(name="dictionary")
 */
class ValueDictionary
{

    const TYPED_ATTRIBUTE_TYPE = 'attributeType';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $value;

    /**
     * @ORM\Id
     * @ORM\Column(name="typeD", type="string", length=50)
     */
    protected $type;

    /**
     * @ORM\Column(name="textD", type="string", length=200)
     */
    protected $text;

    /**
     * @ORM\Column(type="smallint", length=3, options={"default" = 1})
     */
    protected $active;

    /**
     * @ORM\Column(name="descriptionD", type="string", length=500)
     */
    protected $description;

    /**
     * @ORM\Column(name="argument1", type="string", length=500)
     */
    protected $argument1;

    /**
     * @ORM\Column(name="argument2", type="string", length=500)
     */
    protected $argument2;

    /**
     * @ORM\Column(name="argument3", type="string", length=500)
     */
    protected $argument3;

    /**
     * @ORM\Column(name="argument4", type="string", length=500)
     */
    protected $argument4;

    /**
     * @ORM\Column(name="modDate", type="datetime")
     */
    protected $modDate;

    /**
     * @ORM\Column(name="orderBy", type="integer", nullable=true)
     */
    protected $orderBy;

    public function __construct($type, $value)
    {
        $this->type = $type;
        $this->value = $value;
        $this->active = 1;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return ValueDictionary
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return ValueDictionary
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return ValueDictionary
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return ValueDictionary
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModDate()
    {
        return $this->modDate;
    }

    /**
     * @param mixed $modDate
     * @return ValueDictionary
     */
    public function setModDate($modDate)
    {
        $this->modDate = $modDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param mixed $orderBy
     * @return ValueDictionary
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return ValueDictionary
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArgument1()
    {
        return $this->argument1;
    }

    /**
     * @param mixed $argument1
     * @return ValueDictionary
     */
    public function setArgument1($argument1)
    {
        $this->argument1 = $argument1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArgument2()
    {
        return $this->argument2;
    }

    /**
     * @param mixed $argument2
     * @return ValueDictionary
     */
    public function setArgument2($argument2)
    {
        $this->argument2 = $argument2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArgument3()
    {
        return $this->argument3;
    }

    /**
     * @param mixed $argument3
     * @return ValueDictionary
     */
    public function setArgument3($argument3)
    {
        $this->argument3 = $argument3;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArgument4()
    {
        return $this->argument4;
    }

    /**
     * @param mixed $argument4
     * @return ValueDictionary
     */
    public function setArgument4($argument4)
    {
        $this->argument4 = $argument4;
        return $this;
    }

}