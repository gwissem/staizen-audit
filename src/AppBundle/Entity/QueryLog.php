<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use UserBundle\Entity\User;

/**
 * Class QueryLog
 * @package AppBundle\Entity
 * @ORM\Table(name="query_log")
 * @ORM\Entity()
 */

class QueryLog
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\QueryHashLog")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $hashId;

    /**
     * @var User $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $routing;

    /**
     * @var User $updatedBy
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=12, scale=6)
     */
    private $minTime;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=12, scale=6)
     */
    private $avgTime;


    /**
     * @var float
     * @ORM\Column(type="decimal", precision=12, scale=6)
     */
    private $maxTime;

    /**
     * QueryLog constructor
     */
    public function __construct()
    {
        $this->createdAt = new DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHashId()
    {
        return $this->hashId;
    }

    /**
     * @param mixed $hashId
     */
    public function setHashId($hashId)
    {
        $this->hashId = $hashId;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getRouting()
    {
        return $this->routing;
    }

    /**
     * @param mixed $routing
     */
    public function setRouting($routing)
    {
        $this->routing = $routing;
    }

    /**
     * @return User
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param User $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getMinTime(): float
    {
        return $this->minTime;
    }

    /**
     * @param float $minTime
     */
    public function setMinTime(float $minTime)
    {
        $this->minTime = $minTime;
    }

    /**
     * @return float
     */
    public function getAvgTime(): float
    {
        return $this->avgTime;
    }

    /**
     * @param float $avgTime
     */
    public function setAvgTime(float $avgTime)
    {
        $this->avgTime = $avgTime;
    }

    /**
     * @return float
     */
    public function getMaxTime(): float
    {
        return $this->maxTime;
    }

    /**
     * @param float $maxTime
     */
    public function setMaxTime(float $maxTime)
    {
        $this->maxTime = $maxTime;
    }


}