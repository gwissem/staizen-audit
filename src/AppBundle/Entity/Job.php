<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JobRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"job" = "Job", "base_job" = "JMS\JobQueueBundle\Entity\Job"})
 * @ORM\Table(name = "jms_jobs")
 */
class Job extends \JMS\JobQueueBundle\Entity\Job
{

}
