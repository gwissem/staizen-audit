<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class QueryHashLog
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name = "query_hash_log", indexes = {
 *     @ORM\Index("hash_search_index", columns = {"hash"})
 * })
 */

class QueryHashLog
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $hash;

    /**
     * @ORM\Column(type="text")
     */
    protected $query;

    /**
     * QueryHashLog constructor.
     * @param $hash
     * @param $query
     */
    public function __construct($hash, $query)
    {
        $this->hash = $hash;
        $this->query = $query;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }




}