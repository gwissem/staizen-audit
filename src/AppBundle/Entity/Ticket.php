<?php
// src/Acme/ApiBundle/Entity/Client.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TicketRepository")
 */
class Ticket
{
    use ORMBehaviors\Timestampable\Timestampable;

    const TYPE_ISSUE = 'issue';
    const SCOPE_TOOL = 'tool';
    const STATUS_NEW = 'new';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_REJECTED = 'rejected';
    const STATUS_CLOSED = 'closed';

    const TICKET_LEVEL_CRITICAL = 'level_critical';
    const TICKET_LEVEL_ERROR = 'level_error';
    const TICKET_LEVEL_SUGGESTION = 'level_suggestion';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $browser;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $screenResolution;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $requestInfo;

    /**
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $status = self::STATUS_NEW;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $type = self::TYPE_ISSUE;
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $locale;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $assignedTo;
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdByOriginal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $screenShoot;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notificationNumber;

    /**
     * @ORM\Column(name="ticket_level", type="string", length=64, nullable=true)
     */
    private $ticketLevel;

    static function getStatuses() {

        return [
            'nowy' => Ticket::STATUS_NEW,
            'w trakcie' => Ticket::STATUS_IN_PROGRESS,
            'zamknięty' => Ticket::STATUS_CLOSED,
            'odrzucony' => Ticket::STATUS_REJECTED
        ];

    }

    static function getTicketLevels() {

        return [
            'Usprawnienie/sugestia' => self::TICKET_LEVEL_SUGGESTION,
            'Nie działa prawidłowo' => self::TICKET_LEVEL_ERROR,
            'Błąd krytyczny' => self::TICKET_LEVEL_CRITICAL
        ];

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Issue
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return Issue
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Ticket
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     * @return Issue
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAssignedTo()
    {
        return $this->assignedTo;
    }

    /**
     * @param mixed $assignedTo
     * @return Ticket
     */
    public function setAssignedTo($assignedTo)
    {
        $this->assignedTo = $assignedTo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return Ticket
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Ticket
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Ticket
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * @param mixed $browser
     * @return Ticket
     */
    public function setBrowser($browser)
    {
        $this->browser = $browser;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getScreenResolution()
    {
        return $this->screenResolution;
    }

    /**
     * @param mixed $screenResolution
     * @return Ticket
     */
    public function setScreenResolution($screenResolution)
    {
        $this->screenResolution = $screenResolution;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestInfo($decode = true)
    {
        return $decode ? (array)json_decode($this->requestInfo) : $this->requestInfo;
    }

    /**
     * @param mixed $requestInfo
     * @return Ticket
     */
    public function setRequestInfo($requestInfo)
    {
        $this->requestInfo = $requestInfo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedByOriginal()
    {
        return $this->createdByOriginal;
    }

    /**
     * @param mixed $createdByOriginal
     * @return Ticket
     */
    public function setCreatedByOriginal($createdByOriginal)
    {
        $this->createdByOriginal = $createdByOriginal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getScreenShoot()
    {
        return $this->screenShoot;
    }

    /**
     * @param mixed $screenShoot
     */
    public function setScreenShoot($screenShoot)
    {
        $this->screenShoot = $screenShoot;
    }

    /**
     * @return mixed
     */
    public function getNotificationNumber()
    {
        return $this->notificationNumber;
    }

    /**
     * @param mixed $notificationNumber
     */
    public function setNotificationNumber($notificationNumber)
    {
        $this->notificationNumber = $notificationNumber;
    }

    public function displayStatus() {
        $statuses = array_flip(Ticket::getStatuses());

        if(isset($statuses[$this->getStatus()])) {
            return $statuses[$this->getStatus()];
        }

        return $this->getStatus();
    }

    /**
     * @return mixed
     */
    public function getTicketLevel()
    {
        return $this->ticketLevel;
    }

    /**
     * @param mixed $ticketLevel
     */
    public function setTicketLevel($ticketLevel)
    {
        $this->ticketLevel = $ticketLevel;
    }


    public function displayLevel() {
        $statuses = array_flip(Ticket::getTicketLevels());

        if(isset($statuses[$this->getTicketLevel()])) {
            return $statuses[$this->getTicketLevel()];
        }

        return $this->getTicketLevel();
    }

}