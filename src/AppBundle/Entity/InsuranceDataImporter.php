<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InsuranceDataImporterRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class InsuranceDataImporter
{
    use ORMBehaviors\Timestampable\Timestampable;


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="boolean")
     */
    private $isIncremental;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $connectionString;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    private $query;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="integer", options={"default" = 86400})
     */
    private $interval = 86400;

    /**
     * @var string
     * @ORM\Column(type="time", nullable=true)
     */
    private $jobStartTime;

    /**
     * @var string
     * @ORM\Column(type="boolean", options={"default" = 1})
     */
    private $active = true;

    /**
     * @ORM\ManyToOne(targetEntity="CaseBundle\Entity\DictionaryPackage")
     * @ORM\JoinColumn(name="package_id", referencedColumnName="id")
     */
    private $package;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $nextRun;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $newDate = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return InsuranceDataImporter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param mixed $package
     * @return InsuranceDataImporter
     */
    public function setPackage($package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsIncremental()
    {
        return $this->isIncremental;
    }

    /**
     * @param string $isIncremental
     * @return InsuranceDataImporter
     */
    public function setIsIncremental($isIncremental)
    {
        $this->isIncremental = $isIncremental;

        return $this;
    }

    /**
     * @return string
     */
    public function getConnectionString()
    {
        return $this->connectionString;
    }

    /**
     * @param string $connectionString
     * @return InsuranceDataImporter
     */
    public function setConnectionString($connectionString)
    {
        $this->connectionString = $connectionString;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     * @return InsuranceDataImporter
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }


    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param string $active
     * @return InsuranceDataImporter
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @param string $interval
     * @return InsuranceDataImporter
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;

        return $this;
    }

    /**
     * @return string
     */
    public function getJobStartTime()
    {
        return $this->jobStartTime;
    }

    /**
     * @param string $jobStartTime
     * @return InsuranceDataImporter
     */
    public function setJobStartTime($jobStartTime)
    {
        $this->jobStartTime = $jobStartTime;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getNextRun()
    {
        return $this->nextRun;
    }

    /**
     * @param \DateTime $lastRun
     * @return InsuranceDataImporter
     */
    public function setNextRun($nextRun)
    {
        $this->nextRun = $nextRun;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return InsuranceDataImporter
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }


}