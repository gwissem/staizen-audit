<?php

namespace AppBundle\Utils\QueryMonitorUtils;

use AppBundle\Entity\QueryHashLog;
use AppBundle\Entity\QueryLog;

class SingleQueryMonitor
{

    /** @var array */
    private $times;

    /** @var integer */
    private $amount;

    /** @var string */
    private $hash;

    /** @var string */
    private $query;

    /**
     * SingleQueryMonitor constructor.
     * @param $times
     * @param $hash
     * @param $query
     */
    public function
    __construct($hash, $query)
    {
        $this->times = [];
        $this->amount = 0;
        $this->hash = $hash;
        $this->query = $query;
    }

    public function addTime($time) {

        $this->times[] = $time;
        $this->amount++;

        return $this;

    }

    public function getTimes(){

        return $this->getTimes();

    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    public function getAvgTime() {
        return array_sum($this->times)/count($this->times);
    }

    public function getMinTime() {
        return min($this->times);
    }

    public function getMaxTime() {
        return max($this->times);
    }

    public function getAmount() {
        return $this->amount;
    }

    public function __toString()
    {
        return $this->hash . ' ' . strval(array_sum($this->times)/count($this->times));
    }


    /**
     * @param QueryHashLog $queryHashLog
     * @return QueryLog
     */
    public function createQueryLog(QueryHashLog $queryHashLog)
    {

        $queryLog = new QueryLog();

        $queryLog->setAmount($this->amount);
        $queryLog->setMinTime($this->getMinTime());
        $queryLog->setAvgTime($this->getAvgTime());
        $queryLog->setMaxTime($this->getMaxTime());
        $queryLog->setHashId($queryHashLog);

        return $queryLog;

    }

    public function createQueryHashLog() {

        return new QueryHashLog($this->hash, $this->query);

    }
}