<?php

namespace AppBundle\Utils;


class ServiceBrokerMessage
{

    protected $message_enqueue_time = 1;
    protected $message_type_name;
    protected $service_name;
    protected $message_body;

    public function __construct($data = []) {

        if(!empty($data)) {

            foreach ($data as $key => $datum) {
                $this->{$key} = $datum;
            }

        }
    }

    /**
     * @return mixed
     */
    public function getMessageEnqueueTime()
    {
        return new \DateTime($this->message_enqueue_time);
    }

    /**
     * @return mixed
     */
    public function getMessageTypeName()
    {
        return $this->message_type_name;
    }

    /**
     * @return mixed
     */
    public function getServiceName()
    {
        return $this->service_name;
    }

    /**
     * @return mixed
     */
    public function getMessageBody()
    {
        return $this->message_body;
    }

    public function getBody()
    {
        return json_decode($this->message_body);
    }

}