<?php

namespace AppBundle\Utils;


use CaseBundle\Service\ProcessHandler;

class XmlIncomingToArrayHelper
{

    private $mapToAttribute = [];

    private $type = '';

    private $xmlArray = [];

    /** @var ProcessHandler $processHandler */
    private $processHandler;

    /**
     * XmlIncomingToArrayHelper constructor.
     * @param $type
     * @param $processHandler
     */
    public function __construct($type, $processHandler)
    {
        $this->type = $type;
        $this->processHandler = $processHandler;

        if($type === "e_call_psa") {
            $this->setEcallPSAMapper();
        }

    }

    private function transformTelephonNumber($value) {

        if(is_string($value)) {

            if(substr($value, 0, 3) == "+48") {
                return "0" . substr($value, 3, strlen($value));
            }
            elseif ($value[0] == "+") {
                return "000" . substr($value, 1, strlen($value));
            }

        }

        return $value;

    }

    private function setEcallPSAMapper() {

        $this->mapToAttribute= [
            'rtime' => [
                'path' => '654,104',
                'transform' => function($value) { return ((new \DateTime($value))->format('Y-m-d H:i:s')); }
            ],
            'manual' => [
                'path' => '654,1036',
                'transform' => function($value) { return (filter_var($value, FILTER_VALIDATE_BOOLEAN)) ? 1 : 2; }
            ],
            'caller' => [
                'path' => '654,197',
                'transform' => function($value) {
                    return $this->transformTelephonNumber($value);
                }
            ],
            'viewcl' => [
                'path' => '654,1037',
                'transform' => function($value) { return ($value == "L") ? 2 : 1; }
            ],
            'voicelocal' => [
                'path' => '654,398',
                'transform' => function($value) {

                    if(empty($value)) {
                        return $this->xmlArray['calldescription']['caller'];
                    }

                    return $this->transformTelephonNumber($value);

                }
            ],
            'make' => [
                'path' => '654,566',
            ],
            'model' => [
                'path' => '654,576',
            ],
            'vis' => [
                'path' => '654,71',
                'transform' => function($value) {
                    return ($this->xmlArray['vehicledescription']['wmi'] . $this->xmlArray['vehicledescription']['vds'] . $this->xmlArray['vehicledescription']['vis']);
                }
            ],
            'registration' => [
                'path' => '654,72'
            ],
            'energy' => [
                'path' => '654,669'
            ],
            'color' => [
                'path' => '654,343'
            ],
            'rawlongitude' => [
                'path' => '654,85,92',
                'transform' => function($value) { return (intval($value)/3600000 ); }
            ],
            'rawlatitude' => [
                'path' => '654,85,93',
                'transform' => function($value) { return (intval($value)/3600000 ); }
            ],
            'headvalue' => [
                'path' => '654,85,63',
                'transform' => function($value) {

                    $return = '';

                    if(!empty($this->xmlArray['']['rawest'])) {
                        $return .= 'Dokładność: ' . $this->xmlArray['']['rawest'] . ' m, ';
                    }

                    $return .= 'kierunek ' . $value . ' stopni';

                    return $return;

                }
            ]
        ];

    }

    public function mapData($xmlArray) {

        $arrayData = [];

        $this->xmlArray = $xmlArray;

        $this->recursiveMapData($xmlArray, $arrayData);

        return $arrayData;
    }

    private function recursiveMapData($array, &$data) {

        foreach ($array as $key => $item) {

            if(is_array($item) && count($item)) {
                $this->recursiveMapData($item, $data);
            }
            else {
                if(array_key_exists($key, $this->mapToAttribute)) {

                    $newRow = [
                        'path' => $this->mapToAttribute[$key]['path'],
                        'nodeName' => $key
                    ];

                    if(isset($this->mapToAttribute[$key]['transform']) && is_callable($this->mapToAttribute[$key]['transform'])) {
                        $newRow['value'] = $this->doTransform($item, $this->mapToAttribute[$key]['transform']);
                    }
                    else {
                        $newRow['value'] = $item;
                    }

                    $data[$key] = $newRow;

                }
            }

        }

    }

    private function doTransform($item, $funTransform) {

        $args = [
            'value' => $item
        ];

//        $refFunc = new ReflectionFunction($funTransform);

//        foreach ($refFunc->getParameters() as $parameter) {
//            if($parameter->getName() === "xmlArray") {
//                $args[] = $this->tempData;
//            }
//            elseif ($parameter->getName() === "processHandler") {
//                $args[] = $this->processHandler;
//            }
//            elseif ($parameter->getName() === "value") {
//                $args[] = $item;
//            }
//        }

        return call_user_func_array($funTransform, $args);

    }

}