<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlAttribute;
use JMS\Serializer\Annotation\XmlElement;

class Skill
{

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $self;

    /**
     * @Type("integer")
     */
    private $skillId;
    /**
     * @Type("string")
     */
    private $skillName;

    /**
     * @return mixed
     */
    public function getSelf()
    {
        return $this->self;
    }

    /**
     * @param mixed $self
     */
    public function setSelf($self)
    {
        $this->self = $self;
    }

    /**
     * @return mixed
     */
    public function getSkillId()
    {
        return $this->skillId;
    }

    /**
     * @param mixed $skillId
     */
    public function setSkillId($skillId)
    {
        $this->skillId = $skillId;
    }

    /**
     * @return mixed
     */
    public function getSkillName()
    {
        return $this->skillName;
    }

    /**
     * @param mixed $skillName
     */
    public function setSkillName($skillName)
    {
        $this->skillName = $skillName;
    }


    public function getSkillNameUriPar() {

        $s = new skillNameUriPair();
        $s->setRefURL($this->getSelf());
        $s->setName($this->getSkillName());

        return $s;
    }

}