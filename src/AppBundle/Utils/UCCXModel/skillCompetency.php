<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;

class skillCompetency
{

    /**
     * @Type("integer")
     */
    private $competencelevel;

    /**
     * @Type("AppBundle\Utils\UCCXModel\skillNameUriPair")
     */
    private $skillNameUriPair;

    /**
     * @Type("integer")
     * @XmlElement(cdata=false)
     */
    private $weight;

    /**
     * @return mixed
     */
    public function getCompetencelevel()
    {
        return $this->competencelevel;
    }

    /**
     * @param mixed $competencelevel
     */
    public function setCompetencelevel($competencelevel)
    {
        $this->competencelevel = $competencelevel;
    }

    /**
     * @return mixed
     */
    public function getSkillNameUriPair()
    {
        return $this->skillNameUriPair;
    }

    /**
     * @param mixed $skillNameUriPair
     */
    public function setSkillNameUriPair($skillNameUriPair)
    {
        $this->skillNameUriPair = $skillNameUriPair;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

}