<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * @XmlRoot("Resource")
 */
class Resource
{
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $self;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $userID;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $firstName;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $lastName;

    /**
     * @Type("integer")
     */
    private $extension;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $alias;

    /**
     * @Type("boolean")
     */
    private $autoAvailable;

    /**
     * @Type("integer")
     */
    private $type;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $primarySupervisorOf;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $secondarySupervisorOf;

    /**
     * @Type("array<AppBundle\Utils\UCCXModel\skillCompetency>")
     * @XmlList(entry="skillCompetency")
     */

    private $skillMap;

    /**
     * @Type("AppBundle\Utils\UCCXModel\team")
     */
    private $team;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return bool
     */
    public function isAutoAvailable(): bool
    {
        return $this->autoAvailable;
    }

    /**
     * @param bool $autoAvailable
     */
    public function setAutoAvailable(bool $autoAvailable)
    {
        $this->autoAvailable = $autoAvailable;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPrimarySupervisorOf()
    {
        return $this->primarySupervisorOf;
    }

    /**
     * @param mixed $primarySupervisorOf
     */
    public function setPrimarySupervisorOf($primarySupervisorOf)
    {
        $this->primarySupervisorOf = $primarySupervisorOf;
    }

    /**
     * @return mixed
     */
    public function getSecondarySupervisorOf()
    {
        return $this->secondarySupervisorOf;
    }

    /**
     * @param mixed $secondarySupervisorOf
     */
    public function setSecondarySupervisorOf($secondarySupervisorOf)
    {
        $this->secondarySupervisorOf = $secondarySupervisorOf;
    }

    /**
     * @return mixed
     */
    public function getSelf()
    {
        return $this->self;
    }

    /**
     * @param mixed $self
     */
    public function setSelf($self)
    {
        $this->self = $self;
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @param mixed $userID
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
    }

    public function getSkillMap()
    {
        return $this->skillMap;
    }

    public function setSkillMap($skillMap)
    {
        $this->skillMap = $skillMap;
    }

    public function addSkillMap($skillMap)
    {
        $this->skillMap[] = $skillMap;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param mixed $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }

}