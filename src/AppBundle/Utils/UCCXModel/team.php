<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlAttribute;
use JMS\Serializer\Annotation\XmlElement;

class team
{

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $refURL;

    /**
     * @XmlAttribute
     * @Type("string")
     */
    private $name;

    /**
     * @return mixed
     */
    public function getRefURL()
    {
        return $this->refURL;
    }

    /**
     * @param mixed $refURL
     */
    public function setRefURL($refURL)
    {
        $this->refURL = $refURL;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}