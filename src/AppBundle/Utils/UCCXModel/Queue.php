<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * @XmlRoot("csq")
 */
class Queue
{
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $self;

    /**
     * @Type("integer")
     * @XmlElement(cdata=false)
     */
    private $id;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $name;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $queueAlgorithm;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $queueType;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $routingType;

    /**
     * @Type("boolean")
     * @XmlElement(cdata=false)
     */
    private $autoWork;

    /**
     * @Type("integer")
     * @XmlElement(cdata=false)
     */
    private $wrapupTime;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $resourcePoolType;

    /**
     * @Type("integer")
     * @XmlElement(cdata=false)
     */
    private $serviceLevel;

    /**
     * @Type("integer")
     * @XmlElement(cdata=false)
     */
    private $serviceLevelPercentage;

    /**
     * @Type("AppBundle\Utils\UCCXModel\poolSpecificInfo")
     */
    private $poolSpecificInfo;

    /**
     * @return mixed
     */
    public function getSelf()
    {
        return $this->self;
    }

    /**
     * @param mixed $self
     */
    public function setSelf($self)
    {
        $this->self = $self;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getQueueType()
    {
        return $this->queueType;
    }

    /**
     * @param mixed $queueType
     */
    public function setQueueType($queueType)
    {
        $this->queueType = $queueType;
    }

    /**
     * @return mixed
     */
    public function getRoutingType()
    {
        return $this->routingType;
    }

    /**
     * @param mixed $routingType
     */
    public function setRoutingType($routingType)
    {
        $this->routingType = $routingType;
    }

    /**
     * @return mixed
     */
    public function getAutoWork()
    {
        return $this->autoWork;
    }

    /**
     * @param mixed $autoWork
     */
    public function setAutoWork($autoWork)
    {
        $this->autoWork = $autoWork;
    }

    /**
     * @return mixed
     */
    public function getWrapupTime()
    {
        return $this->wrapupTime;
    }

    /**
     * @param mixed $wrapupTime
     */
    public function setWrapupTime($wrapupTime)
    {
        $this->wrapupTime = $wrapupTime;
    }

    /**
     * @return mixed
     */
    public function getResourcePoolType()
    {
        return $this->resourcePoolType;
    }

    /**
     * @param mixed $resourcePoolType
     */
    public function setResourcePoolType($resourcePoolType)
    {
        $this->resourcePoolType = $resourcePoolType;
    }

    /**
     * @return mixed
     */
    public function getServiceLevel()
    {
        return $this->serviceLevel;
    }

    /**
     * @param mixed $serviceLevel
     */
    public function setServiceLevel($serviceLevel)
    {
        $this->serviceLevel = $serviceLevel;
    }

    /**
     * @return mixed
     */
    public function getServiceLevelPercentage()
    {
        return $this->serviceLevelPercentage;
    }

    /**
     * @param mixed $serviceLevelPercentage
     */
    public function setServiceLevelPercentage($serviceLevelPercentage)
    {
        $this->serviceLevelPercentage = $serviceLevelPercentage;
    }

    /**
     * @return poolSpecificInfo
     */
    public function getPoolSpecificInfo()
    {
        return $this->poolSpecificInfo;
    }

    /**
     * @param mixed $poolSpecificInfo
     */
    public function setPoolSpecificInfo($poolSpecificInfo)
    {
        $this->poolSpecificInfo = $poolSpecificInfo;
    }

    /**
     * @return mixed
     */
    public function getQueueAlgorithm()
    {
        return $this->queueAlgorithm;
    }

    /**
     * @param mixed $queueAlgorithm
     */
    public function setQueueAlgorithm($queueAlgorithm)
    {
        $this->queueAlgorithm = $queueAlgorithm;
    }

    /**
     * @return array
     */
    public function _toSimpleArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }

    /**
     * @return array
     */
    public function _toArray(): array
    {
        return [
            'self' => $this->self,
            'id' => $this->id,
            'name' => $this->name
        ];
    }

}