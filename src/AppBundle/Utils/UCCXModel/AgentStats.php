<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;

class AgentStats
{

    /**
     * @Type("integer")
     */
    private $loggedIn;

    /**
     * @Type("integer")
     */
    private $ready;

    /**
     * @Type("integer")
     */
    private $notReady;

    /**
     * @Type("integer")
     */
    private $talking;

    /**
     * @return mixed
     */
    public function getLoggedIn()
    {
        return $this->loggedIn;
    }

    /**
     * @param mixed $loggedIn
     */
    public function setLoggedIn($loggedIn)
    {
        $this->loggedIn = $loggedIn;
    }

    /**
     * @return mixed
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * @param mixed $ready
     */
    public function setReady($ready)
    {
        $this->ready = $ready;
    }

    /**
     * @return mixed
     */
    public function getNotReady()
    {
        return $this->notReady;
    }

    /**
     * @param mixed $notReady
     */
    public function setNotReady($notReady)
    {
        $this->notReady = $notReady;
    }

    /**
     * @return mixed
     */
    public function getTalking()
    {
        return $this->talking;
    }

    /**
     * @param mixed $talking
     */
    public function setTalking($talking)
    {
        $this->talking = $talking;
    }



}