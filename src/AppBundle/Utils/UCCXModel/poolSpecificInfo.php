<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlAttribute;
use JMS\Serializer\Annotation\XmlElement;

class poolSpecificInfo
{

    /**
     * @Type("AppBundle\Utils\UCCXModel\skillGroup")
     */
    private $skillGroup;

    /**
     * @return skillGroup
     */
    public function getSkillGroup()
    {
        return $this->skillGroup;
    }

    /**
     * @param mixed $skillGroup
     */
    public function setSkillGroup($skillGroup)
    {
        $this->skillGroup = $skillGroup;
    }

}