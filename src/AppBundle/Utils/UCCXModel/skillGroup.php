<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlList;

class skillGroup
{

    /**
     * @Type("array<AppBundle\Utils\UCCXModel\skillCompetency>")
     * @XmlList(inline = true, entry="skillCompetency")
     */

    private $skillCompetency;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $selectionCriteria;

    /**
     * @return mixed
     */
    public function getSelectionCriteria()
    {
        return $this->selectionCriteria;
    }

    /**
     * @param mixed $selectionCriteria
     */
    public function setSelectionCriteria($selectionCriteria)
    {
        $this->selectionCriteria = $selectionCriteria;
    }

    /**
     * @return mixed
     */
    public function getSkillCompetency()
    {
        return $this->skillCompetency;
    }

    /**
     * @param mixed $skillCompetency
     */
    public function setSkillCompetency($skillCompetency)
    {
        $this->skillCompetency = $skillCompetency;
    }

    /**
     * @param $skillCompetency
     */
    public function addSkillCompetency($skillCompetency)
    {
        $this->skillCompetency[] = $skillCompetency;
    }

}