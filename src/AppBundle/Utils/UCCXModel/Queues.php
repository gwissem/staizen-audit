<?php

namespace AppBundle\Utils\UCCXModel;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * @XmlRoot("csqs")
 */
class Queues
{

    /**
     * @Type("array<AppBundle\Utils\UCCXModel\Queue>")
     * @XmlList(inline = true, entry="csq")
     */

    private $queues;

    /**
     * @return mixed
     */
    public function getQueues()
    {
        return $this->queues;
    }

    /**
     * @param mixed $queues
     */
    public function setQueues($queues)
    {
        $this->queues = $queues;
    }


    /**
     * @param mixed $queue
     */
    public function addQueue($queue)
    {
        $this->queues = $queue;
    }

}