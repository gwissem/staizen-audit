<?php

namespace AppBundle\Utils;

use AppBundle\Utils\QueryMonitorUtils\SingleQueryMonitor;
use Symfony\Component\HttpKernel\Kernel;

final class QueryMonitor
{
    /**
     * @var QueryMonitor
     */
    private static $instance;

    /** @var SingleQueryMonitor[] */
    private $queries;

    /** @var bool  */
    private $enable = false;

    /** @var float  */
    private $limitTime;

    /**
     * gets the instance via lazy initialization (created on first usage)
     */
    public static function getInstance(): QueryMonitor
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct()
    {
        $this->queries = [];

        if (php_sapi_name() != "cli") {

            global $kernel;

            if ($kernel instanceof Kernel) {

                $limit = $kernel->getContainer()->getParameter('query_monitor_limit_time');

                if ($limit !== null) {
                    $this->limitTime = floatval($kernel->getContainer()->getParameter('query_monitor_limit_time'));
                    $this->enable = true;
                }

            }
        }

    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone()
    {
    }

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup()
    {
    }

    public function addQuery($query, $executedTime = 0) {

        if(!$this->enable) return;

        if($executedTime < $this->limitTime) return;

        $hash = hash('ripemd160', $query);

        if(!key_exists($hash, $this->queries)) {
            $this->queries[$hash] = new SingleQueryMonitor($hash, $query);
        }

        $this->queries[$hash]->addTime($executedTime);

    }

    public function getQueries() {

        return $this->queries;

    }

}