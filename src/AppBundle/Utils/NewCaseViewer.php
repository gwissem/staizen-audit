<?php

namespace AppBundle\Utils;


use AppBundle\Entity\ValueDictionary;
use AppBundle\Form\Type\newCaseViewerType;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Service\NoteHandler;
use CaseBundle\Service\ProcessHandler;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class NewCaseViewer
{

    const START_STEP_ID = '1011.001';
    const FORM_STEP_ID = '1011.071';

    /**
     * @var Request
     */
    protected $currentRequest;

    /** @var FormFactory */
    protected $formFactory;

    /**
     * @var ProcessHandler
     */
    protected $processHandler;

    protected $platformId;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var NoteHandler
     */
    protected $noteHandler;


    /**
     * NewCaseViewer constructor.
     * @param Request $request
     * @param FormFactory $formFactory
     * @param ProcessHandler $processHandler
     * @param User $user
     * @param NoteHandler $noteHandler
     */
    public function __construct(Request $request,
                                FormFactory $formFactory,
                                ProcessHandler $processHandler,
                                User $user,
                                NoteHandler $noteHandler
    )
    {
        $this->currentRequest = $request;
        $this->formFactory = $formFactory;
        $this->processHandler = $processHandler;
        $this->user = $user;
        $this->noteHandler = $noteHandler;
    }

    /**
     * @param $id
     * @return NewCaseViewer
     */
    public function setPlatform($id)
    {
        $this->platformId = $id;
        return $this;
    }

    public function createForm()
    {

        $data = [
            'platformId' => $this->platformId
        ];

        $options = [
            'makeModel' => $this->processHandler->em->getRepository(ValueDictionary::class)->getDictionaryForSelect("makeModel", true),
            'eventTypes' => $this->processHandler->em->getRepository(ValueDictionary::class)->getDictionaryForSelect("eventType", true, [], [3, 5])
        ];

//        if($this->user->isAdmin()) {
//            $data = array_merge($data, $this->getRandomData());
//        }

        return $this->formFactory->create(newCaseViewerType::class, $data, $options);

    }

    private function getRandomData()
    {

        return [

            'driver_first_name' => 'Jan',
            'driver_second_name' => 'Kowalski',
            'driver_phone' => '111111111',

            'event_type' => '1',
            'event_description' => 'Uderzenie w artony.',
            'event_create_date' => '2019-05-20 10:00:00',

            'car_register_number' => 'WW891XK',
            'car_vin' => 'JMZGJ696661352628',
            'car_dpr' => '2016-06-13',

            'location_event' => '61-584 Poznań, Wierzbięcice 2a',

            'description' => 'Jakiś dodatkowy opis'
        ];

    }

    /**
     * @param FormInterface $form
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function submitForm(FormInterface $form)
    {

        $form->handleRequest($this->currentRequest);

        if ($form->isSubmitted() && $form->isValid()) {

            $processInstance = $this->createCase();
            $this->setAttributes($form, $processInstance->getGroupProcess()->getId());

            $ids = $this->processHandler->next($processInstance->getId(), 3, $this->user->getId(), $this->user->getId());

            if (is_array($ids)) {
                $this->processHandler->formControls($ids[0]);
            }

            return $processInstance->getRoot()->getId();

        }

        return null;

    }

    /**
     * @param FormInterface $form
     * @return ProcessInstance
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createCase()
    {

        /**
         *  Utworzenie pierwszego kroku
         */
        $processInstanceId = $this->processHandler->start(
            self::START_STEP_ID,
            NULL,
            $this->user->getId(),
            $this->user->getId());

        $this->processHandler->formControls($processInstanceId);

        /**
         *  Dezaktywacja kroku i dodanie notatki
         */

        $processInstance = $this->processHandler->em->getRepository(ProcessInstance::class)->find($processInstanceId);
        $processInstance->setActive(0);

        $this->processHandler->em->persist($processInstance);
        $this->processHandler->em->flush();

        $this->noteHandler->addNote($processInstance->getId(), 'text', 'Utworzenie sprawy z Viewera.', $this->user->getId());

        /**
         *  Utworzenie kroku z formularzem
         */

        $processInstanceId = $this->processHandler->start(
            self::FORM_STEP_ID,
            $processInstance->getId(),
            $this->user->getId(),
            $this->user->getId());

        $this->processHandler->formControls($processInstanceId);

        $processInstance = $this->processHandler->em->getRepository(ProcessInstance::class)->find($processInstanceId);

        /**
         * Ustawienie, że jest to nietypowa ścieżka
         */

        $this->processHandler->editAttributeValue(
            '115',
            $processInstance->getGroupProcess()->getId(),
            2,
            AttributeValue::VALUE_INT,
            Step::ALWAYS_ACTIVE,
            $this->user->getId(),
            $this->user->getId()
        );

        /**
         * Ustawienie, że stworzono przez Atlas Viewer
         */

        $this->processHandler->editAttributeValue(
            '1072',
            $processInstance->getGroupProcess()->getId(),
            1,
            AttributeValue::VALUE_INT,
            Step::ALWAYS_ACTIVE,
            $this->user->getId(),
            $this->user->getId()
        );

        return $processInstance;

    }

    /**
     * @param FormInterface $form
     * @param $groupProcessId
     */
    private function setAttributes(FormInterface $form, $groupProcessId)
    {

        $data = $form->getData();

        $availableField = newCaseViewerType::getAvailableField();

        $noteContent = "";

        foreach ($availableField as $name => $path) {

            if (key_exists($name, $data)) {

                if (strpos($path, 'NOTE') !== FALSE) {

                    if (empty($data[$name])) {
                        continue;
                    }

                    list($a, $label) = explode(':', $path);
                    $noteContent .= $label . ": " . $data[$name] . "\n";

                } else {

                    $attributeId = Attribute::getAttributeByPath($path);
                    $attribute = $this->processHandler->em->getRepository(Attribute::class)->find($attributeId);

                    if (!empty($attribute)) {

                        $this->processHandler->editAttributeValue(
                            $path,
                            $groupProcessId,
                            $data[$name],
                            $attribute->getNameValueType(),
                            Step::ALWAYS_ACTIVE,
                            $this->user->getId(),
                            $this->user->getId()
                        );

                    }

                }

            }

        }

        if (!empty($noteContent)) {

            $this->processHandler->editAttributeValue(
                '994',
                $groupProcessId,
                $noteContent,
                Attribute::INPUT_TYPE_TEXT,
                Step::ALWAYS_ACTIVE,
                $this->user->getId(),
                $this->user->getId()
            );

        }

    }

}