<?php

namespace AppBundle\Utils;

use CaseBundle\Entity\AttributeValue;
use Doctrine\DBAL\Connection;
use MssqlBundle\PDO\PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ToolBundle\Utils\QueryParser;
use UserBundle\Service\UserInfoService;

class ArrayUtils
{

    public function removeEmptyElements($input){
        if($input == null){
            return $input;
        }

        foreach ($input as &$value)
        {
            if (is_array($value))
            {
                $value = $this->removeEmptyElements($value);
            }
        }

        return array_filter($input, function($value) { return (is_array($value) && count($value) !== 0) || (!is_array($value) && $value !== ''); });
    }

}