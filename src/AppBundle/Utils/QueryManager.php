<?php

namespace AppBundle\Utils;

use CaseBundle\Entity\AttributeValue;
use Doctrine\DBAL\Connection;
use MssqlBundle\PDO\PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ToolBundle\Utils\QueryParser;
use UserBundle\Entity\User;
use UserBundle\Service\UserInfoService;

class QueryManager
{

    private $container;
    private $results;
    private $query;
    private $errors = [];
    private $parser;
    private $translator;
    private $pdo;

    /** @var QueryMonitor */
    private $queryMonitor;

    /** @var UserInfoService */
    public $userInfo;

    /** @var \ToolBundle\Service\DateParserService $dateParser */
    private $dateParser;

    public function __construct(
        ContainerInterface $container,
        UserInfoService $userInfo,
        QueryParser $parser,
        $translator
    )
    {
        $this->container = $container;
        $this->userInfo = $userInfo;
        $this->parser = $parser;
        $this->dateParser = $container->get('tool.date_parser.service');
        $this->translator = $translator;
        $this->pdo = $this->container->get('doctrine.dbal.default_connection');
        $this->queryMonitor = QueryMonitor::getInstance();

    }

    public function reConnect()
    {

        if ($this->pdo !== null && $this->pdo->ping() === false) {
            $this->pdo->close();
            $this->pdo->connect();
        }

    }

    public function executeProcedure(
        $query,
        $parameters = [],
        $fetchResults = true,
        $predefinedQueries = true
    )
    {
        $output = [];
        $user = $this->userInfo->getUser();
        $queryProperties = '';

        if (true === $predefinedQueries) {
            $queryProperties = 'SET NOCOUNT ON; ';
            if($user instanceof User){
                $queryProperties .= " EXEC sp_set_session_context 'user_locale', '".$user->getLocale()."'; ";
            }
        }
        $statement = $this->pdo->prepare($queryProperties . $query);

        foreach ($parameters as $parameter) {
            ${$parameter['key']} = $parameter['value'];
            $length = array_key_exists('length', $parameter) && $this->isOutputParam($parameter['type']) ? $parameter['length'] : null;
            $statement->bindParam(':' . $parameter['key'], ${$parameter['key']}, $parameter['type'], $length);
        }

        $executionStartTime = microtime(true);

//        $this->pdo->setTransactionIsolation(Connection::TRANSACTION_READ_UNCOMMITTED);
        $statement->execute();
//        $this->pdo->setTransactionIsolation(Connection::TRANSACTION_READ_COMMITTED);

        $this->queryMonitor->addQuery($query, microtime(true) - $executionStartTime);

        if (!$fetchResults) {
            return $output;
        }

        foreach ($parameters as $param) {
            if ($this->isOutputParam($param['type'])) {
                $output[$param['key']] = ${$param['key']};
            }
        }

        return empty($output && $fetchResults) ? $statement->fetchAll(PDO::FETCH_ASSOC) : $output;
    }

    protected function isOutputParam($type)
    {
        if (PDO::PARAM_INPUT_OUTPUT < 0)
            return $type < 0 ? true : false;
        return $type >= PDO::PARAM_INPUT_OUTPUT ? true : false;
    }

    public function castTypeToPdoType($type)
    {

        $mapTable = [
            AttributeValue::VALUE_INT => PDO::PARAM_INT
        ];

        return array_key_exists($type, $mapTable) ? $mapTable[$type] : PDO::PARAM_STR;
    }

    protected function setupPDO()
    {

        $pdo = new PDO(
            $this->container->getParameter('mssql_database_host'),
            $this->container->getParameter('mssql_database_port'),
            $this->container->getParameter('mssql_database_name'),
            $this->container->getParameter('mssql_database_user'),
            $this->container->getParameter('mssql_database_password'),
            []
        );

        return $pdo;
    }

    public function existsProcedure($procedureName)
    {

        $parameters = [
            [
                'key' => 'procedureName',
                'value' => $procedureName,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->executeProcedure("SELECT count(name) as amount FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID(:procedureName)", $parameters);

        if (!empty($output)) {
            return filter_var($output[0]['amount'], FILTER_VALIDATE_BOOLEAN);
        }

        return false;

    }

    /**
     * @param $type
     * @return int
     */
    public static function getPDOParam($type) {

        switch ($type) {
            case 'INT': {
                return  PDO::PARAM_INT;
            }
            case 'STR': {
                return PDO::PARAM_STR;
            }
            case 'INT_OUT': {
                return  PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT;
            }
            case 'STR_OUT': {
                return PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT;
            }
            default: {
                return PDO::PARAM_STR;
            }
        }

    }

    /**
     * @param String $string
     * @return String
     */
    public function translate($string){
        $parameters = [
            [
                'key' => 'string',
                'value' => $string,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->executeProcedure("SELECT dbo.f_translate(:string,default)", $parameters);

        return $output[0][''];

    }

}