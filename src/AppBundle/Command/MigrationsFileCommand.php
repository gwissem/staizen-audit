<?php

namespace AppBundle\Command;

use DocumentBundle\Repository\FileRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class MigrationsFileCommand extends ContainerAwareCommand
{

    /**
     * Przykład: php bin/console files:migrations /mnt/pliki/temp_files --debug=0
     */

    protected function configure()
    {
        $this
            ->setName('files:migrations')
            ->addArgument('target', InputArgument::REQUIRED, '')
            ->addArgument('date', InputArgument::OPTIONAL, '')
            ->addOption('debug', 'd',InputArgument::OPTIONAL, '', 0)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $target = $input->getArgument('target');
        $date = $input->getArgument('date');
        $debug = filter_var($input->getOption('debug'), FILTER_VALIDATE_BOOLEAN);

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $dir = $this->getContainer()->getParameter('files_directory');
        $fs = new Filesystem();

        /**
         * @var FileRepository $repo
         */
        $repo = $em->getRepository('DocumentBundle:File');

        if(empty($date)) {
            $date =  '2019-06-04 07:00:00';
        }

        $files = $repo->getFilesFromDate( $date);
        $i = 0;
        $progress = new ProgressBar($output);
        $progress->setBarWidth(count($files));
        $ids = [];

        foreach ($files as $file) {

            $progress->advance();

            if($fs->exists($dir . $file['path'])) {

                if(!$fs->exists($target . $file['path']) ) {
                    $i++;
                    if(!$debug) {
                        $fs->copy($dir . $file['path'], $target . $file['path'], true);
                    }
                }

            }
            else {
                $ids[] = $file['id'];
            }

        }

        $progress->finish();

        $output->writeln('Done: ' . $i . ' / ' . count($files));

        $idsString = implode(',', $ids);
        $output->writeln("Nie znaleziono: " . $idsString);

    }
}
