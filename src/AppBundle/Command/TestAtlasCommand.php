<?php

namespace AppBundle\Command;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Repository\AttributeValueRepository;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Listener\DocumentUploadListener;
use MapBundle\Controller\DefaultController;
use Moment\Moment;
use Monolog\Handler\ElasticSearchHandler;
use Monolog\Logger;
use SocketBundle\Service\RedisListListenerService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Response;

class TestAtlasCommand extends ContainerAwareCommand
{

    const GENERATE_PDF_TEST = 'generate_pdf_test';
    const ALL = 'all';

    /** @var  SymfonyStyle */
    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:run-test')
            ->addArgument('name', InputArgument::REQUIRED, 'Name of test.')
            ->addOption('processId', 'p', InputOption::VALUE_OPTIONAL,
                'Id processu, z którego ma być wyrenderowany PDF.',
                NULL)
            ->setDescription('Komenda do testowania pewnych funkcjonalności Atlasa.');
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->io = new SymfonyStyle($input, $output);

        $testName = $input->getArgument('name');

        switch ($testName) {

            case self::ALL : {

                $this->testGeneratePdfFromForm($input);

                break;
            }

            case self::GENERATE_PDF_TEST : {

                $this->testGeneratePdfFromForm($input);

                break;
            }

            default: {

                $this->io->error('Test with name ". ' . $testName . '" not exists.');

            }
        }

    }

    /**
     * @param InputInterface $input
     * @return bool
     */
    private function testGeneratePdfFromForm(InputInterface $input) {

        $processInstanceId = $input->getOption('processId');

        if(empty($processId)) {

            $processInstances = $this->getContainer()->get('doctrine')->getRepository('CaseBundle:ProcessInstance')->findBy([
                'step' => '1021.015',
                'active' => 1
            ], [
                'createdAt' => 'DESC'
            ], 1);

            if(empty($processInstances)) {
                $this->io->error('Option "processId" is required for this test. Can"t find active 1021.015');
                return false;
            }
            else {

                $processInstanceId = $processInstances[0]->getId();

            }

        }

        $this->io->title('Test "testGeneratePdfFromForm" is run for processInstanceId: ' . $processInstanceId);

        $pdfConverter = $this->getContainer()->get('knp_snappy.pdf');

        $directory = $this->getContainer()->getParameter('kernel.root_dir') . DocumentUploadListener::PATH_TEMP_FILES;

        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        $name = uniqid();
        $fullPath = $directory . '/' . $name . '.pdf';

        $pdfConverter->setTimeout(60);
        $pdfConverter->setOption('zoom', 0.7);
        $pdfConverter->setOption('viewport-size', '1920x1080');
        $pdfConverter->setOption('enable-javascript', true);
        $pdfConverter->setOption('window-status', 'form-loaded');
        $pdfConverter->setOption('debug-javascript', true);

        $host = $this->getContainer()->getParameter('server_host');

        $url = $host . $this->getContainer()->get('router')->generate('public_operational_dashboard_print', [
                'processInstanceId' => $processInstanceId
            ]);

        $pdfConverter->generate($url, $fullPath, [], true);

        $this->io->write('Path to PDF: ' . $fullPath);
        $this->io->success('Test pass!');

    }

}
