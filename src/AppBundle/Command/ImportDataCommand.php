<?php

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;
use MailboxBundle\Service\MailboxOutlook;
use MailboxBundle\Service\SimpleOutlookSenderService;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use UserBundle\Entity\User;

class ImportDataCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:import-data')
            ->addArgument('name', InputArgument::REQUIRED, 'The id of the group process.')
            ->setDescription('Import custom data to atlas');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container = $this->getContainer();
        $io = new SymfonyStyle($input, $output);
        $importName = $input->getArgument('name');

        if($importName === "cfm-users") {

            $this->startImportCFMUsers($io, $container);

        }
        elseif ($importName === "recreate-lp") {
            $this->startRecreateLP($io, $container);
        }
        elseif ($importName === "resent-cfm") {
            $this->resentToCFMUsers($io, $container);
        }

    }

    /**
     * @param SymfonyStyle $io
     * @param ContainerInterface $container
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function resentToCFMUsers($io, $container) {

        $io->title('Start import...');

        $isProd = ($container->get('assetic.parameter_bag')->get('atlas_environment') === "prod");

        /** @var EntityManager $em*/
        $em = $container->get('doctrine.orm.default_entity_manager');

        $companyRepo = $em->getRepository('UserBundle:Company');

        $twig = $container->get('twig');

        $mailboxNoReply = $em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => 'atlas-noreply@starter24.pl'
        ]);

        $simpleSender = new SimpleOutlookSenderService($mailboxNoReply);

        $um = $container->get('fos_user.user_manager');

        foreach ($this->getCfmData($isProd) as $data) {

            $company = $companyRepo->find($data['id']);

            $io->title('Wysyłanie e-maili do firmy: ' . $data['name']);

            $progressBar = $io->createProgressBar(count($data['users']));

            foreach ($data['users'] as $email) {

                /** @var User $user */
                $user = $um->createUser();
                $user = $this->createUser($email , $user);
                $user->setCompany($company);

                $um->updateUser($user);
//
//                /** @var User $user */
//                $user = $em->getRepository('UserBundle:User')->findOneBy([
//                    'email' => $email
//                ]);
//
//                if(!$user) {
//                    $io->writeln('User with email '. $email . ' not exists');
//                    continue;
//                }
//
//                if(!$user->getConfirmationToken()) {
//                    $io->writeln('User was logged. ' . $email);
//                    continue;
//                }

//                $url = $container->get('assetic.parameter_bag')->get('server_host') . '/resetting/reset/' . $user->getConfirmationToken();
                $url = 'https://atlas.starter24.pl/resetting/reset/' . $user->getConfirmationToken();

                $title = 'ATLAS Viewer - Dostęp';

                $view = $twig->render('@Mailbox/EmailTemplate/modern_email.html.twig', [
                    'lang_footer' => 'pl',
                    'title' => $title,
                    'content_title' => $title,
                    'content' => 'Szanowni Państwo, </br></br>

                        Uprzejmie przepraszamy za brak możliwości dostępu do ATLAS Viewer.</br></br>
                        
                        Aby zalogować się do podglądu spraw ATLAS Viewer prosimy stosować się do poniższej instrukcji:</br>
                        1.    Wejść w link <a href="'. $url .'">' . $url . '</a> i wprowadzić prywatne hasło dostępu do swojego konta oraz je potwierdzić i następnie nacisnąć przycisk „Zresetuj hasło”.</br>
                        2.    Zaakceptować regulamin.</br>
                        3.    Po przejściu do ekranu głównego należy w lewym górnym roku wybrać fioletową ikonę z lupą.</br>
                        4.    Viewer jest gotowy do działania.</br></br>
                        
                        Po wylogowaniu z ATLAS Viewer aby ponownie skorzystać z systemu należy wejść na poniższy link.</br>
                        Link do ATLAS Viewer: <a href="https://atlas.starter24.pl">https://atlas.starter24.pl</a></br>
                        Login: '. $email. '</br>
                        Hasło: ustalone wcześniej</br>
                    '
                ]);

                $view = str_replace('__PLATFORM_NAME__', 'CFM', $view);
                $view = str_replace('__EMAIL__', 'cfm@starter24.pl', $view);

                if(!$isProd) {
                    $email = 'atlas-noreply@starter24.pl';
                }

                $simpleSender->send($email, $title, $view);

                $progressBar->advance();
                $progressBar->display();

                if(!$isProd) {
                    $progressBar->finish();
                    $io->newLine();
                    $io->success('Imported!');
                    die();
                }

            }

            $progressBar->finish();
            $io->newLine();
            $io->newLine();

        }

        $io->success('Imported!');

    }

    /**
     * @param SymfonyStyle $io
     * @param ContainerInterface $container
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function startRecreateLP($io, $container) {

        $io->title('Start import...');

        $isProd = ($container->get('assetic.parameter_bag')->get('atlas_environment') === "prod");

        /** @var EntityManager $em*/
        $em = $container->get('doctrine.orm.default_entity_manager');

        $companyRepo = $em->getRepository('UserBundle:Company');

        $twig = $container->get('twig');

        $mailboxNoReply = $em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => 'atlas-noreply@starter24.pl'
        ]);

        $simpleSender = new SimpleOutlookSenderService($mailboxNoReply);

        $um = $container->get('fos_user.user_manager');

        foreach ($this->getRecreateLP($isProd) as $data) {

            $company = $companyRepo->find($data['id']);

            $io->title('Wysyłanie e-maili do firmy: ' . $data['name']);

            $progressBar = $io->createProgressBar(count($data['users']));

            foreach ($data['users'] as $email) {

                /** @var User $user */
                $user = $um->createUser();
                $user = $this->createUser($email , $user);
                $user->setCompany($company);

                $um->updateUser($user);

//                $url = $container->get('router')->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
                $url = $container->get('assetic.parameter_bag')->get('server_host') . '/resetting/reset/' . $user->getConfirmationToken();

                $view = $twig->render('@Mailbox/EmailTemplate/modern_email.html.twig', [
                    'lang_footer' => 'pl',
                    'title' => 'Utworzono nowe konto w systemie Atlas',
                    'content_title' => 'Utworzono nowe konto w systemie Atlas',
                    'content' => 'Dzień dobry, </br>
                        zostało dodane nowe konto dla użytkownika o adresie e-mail: <b>' . $email . '</b>. </br></br>
                        Aby zacząć korzystać z Atlasa, prosimy wejść w poniższy adres, zresetować hasło a następnie zaakceptować regulamin.<br><br>
                        <a href="'. $url .'">Link do Atlasa</a>
                    '
                ]);

                $view = str_replace('__PLATFORM_NAME__', 'CFM', $view);
                $view = str_replace('__EMAIL__', 'cfm@starter24.pl', $view);

                if(!$isProd) {
                    $email = 'atlas-noreply@starter24.pl';
                }

                $simpleSender->send($email, "Starter24 - nowe konto w systemie Atlas", $view);

                $progressBar->advance();
                $progressBar->display();

                if(!$isProd) {
                    $progressBar->finish();
                    $io->newLine();
                    $io->success('Imported!');
                    die();
                }

            }

            $progressBar->finish();
            $io->newLine();
            $io->newLine();

        }

        $io->success('Imported!');

    }

    /**
     * @param SymfonyStyle $io
     * @param ContainerInterface $container
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function startImportCFMUsers($io, $container) {

        $io->title('Start import...');

        $isProd = ($container->get('assetic.parameter_bag')->get('atlas_environment') === "prod");

        /** @var EntityManager $em*/
        $em = $container->get('doctrine.orm.default_entity_manager');

        $companyRepo = $em->getRepository('UserBundle:Company');

        $twig = $container->get('twig');

        $mailboxNoReply = $em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => 'atlas-noreply@starter24.pl'
        ]);

        $simpleSender = new SimpleOutlookSenderService($mailboxNoReply);

        $um = $container->get('fos_user.user_manager');

        foreach ($this->getCfmData($isProd) as $data) {

            $company = $companyRepo->find($data['id']);

            $io->title('Wysyłanie e-maili do firmy: ' . $data['name']);

            $progressBar = $io->createProgressBar(count($data['users']));

            foreach ($data['users'] as $email) {

                /** @var User $user */
                $user = $um->createUser();
                $user = $this->createUser($email , $user);
                $user->setCompany($company);

                $um->updateUser($user);

//                $url = $container->get('router')->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
                $url = $container->get('assetic.parameter_bag')->get('server_host') . '/resetting/reset/' . $user->getConfirmationToken();

                $view = $twig->render('@Mailbox/EmailTemplate/modern_email.html.twig', [
                    'lang_footer' => 'pl',
                    'title' => 'Utworzono nowe konto w systemie Atlas',
                    'content_title' => 'Utworzono nowe konto w systemie Atlas',
                    'content' => 'Dzień dobry, </br>
                        zostało dodane nowe konto dla użytkownika o adresie e-mail: <b>' . $email . '</b>. </br></br>
                        Aby zacząć korzystać z Atlasa, prosimy wejść w poniższy adres, zresetować hasło a następnie zaakceptować regulamin.<br><br>
                        <a href="'. $url .'">Link do Atlasa</a>
                    '
                ]);

                $view = str_replace('__PLATFORM_NAME__', 'CFM', $view);
                $view = str_replace('__EMAIL__', 'cfm@starter24.pl', $view);

                if(!$isProd) {
                    $email = 'atlas-noreply@starter24.pl';
                }

                $simpleSender->send($email, "Starter24 - nowe konto w systemie Atlas", $view);

                $progressBar->advance();
                $progressBar->display();

                if(!$isProd) {
                    $progressBar->finish();
                    $io->newLine();
                    $io->success('Imported!');
                    die();
                }

            }

            $progressBar->finish();
            $io->newLine();
            $io->newLine();

        }

        $io->success('Imported!');

    }

    private function createUser($email, UserInterface $user) {

        /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
        $tokenGenerator = $this->getContainer()->get('fos_user.util.token_generator');
        $password = substr($tokenGenerator->generateToken(), 0, 12);
        $user->setPassword($password);

        $user->setConfirmationToken($tokenGenerator->generateToken());

        $user->setEmail($email);
        $user->setUsername($email);
        $user->setEnabled(1); //enable or disable
        $user->setPasswordRequestedAt(new \DateTime());
        $user->addRole(User::ROLE_CFM_CASE_VIEWER);
        $user->addRole(User::ROLE_OPERATIONAL_DASHBOARD);

        return $user;

    }

    // DEV:
    // 7900	ALD Automotive
    // 7899	Business Lease
    // 7898	LeasePlan
    // 7896	Alphabet

    // PROD:
    // 7904	ALD Automotive
    // 7903	Alphabet
    // 7902	Business Lease
    // 7901	LeasePlan

    private function getCfmData($isProd) {

        return [
            [
            "name" => 'LeasePlan',
            "id" => ($isProd) ? 7901 : 7898,
            "users" => [
                'karol.walasek@leaseplan.com',
                'michal.matys@leaseplan.com'
                ]
            ]
        ];

        return [

            [
                "name" => 'Alphabet',
                "id" => ($isProd) ? 7903 : 7896,
                "users" => [
                    'Daniel.Safianczuk@partner.alphabet.pl',
                    'piotr.szewczak@alphabet.pl',
                    'marcin.bogucki@alphabet.pl',
                    'robert.malazewski@partner.alphabet.pl',
                    'maciej.raczynski@alphabet.pl',
                    'idalia.guryn@partner.alphabet.pl',
                    'janusz.pawlowski@alphabet.pl',
                    'piotr.belus@alphabet.pl',
                    'leszek.paczkowski@partner.alphabet.pl',
                    'rafal.romaldowski@alphabet.pl',
                    'michal.kuzmierowski@alphabet.pl',
                    'Jaroslaw.Zapasnik@partner.alphabet.pl',
                    'tomasz.kropielnicki@partner.alphabet.pl',
                    'marta.pyzowska@partner.alphabet.pl',
                    'artur.smetek@partner.alphabet.pl',
                    'Przemyslaw.Kalinowski@partner.alphabet.pl',
                    'daniel.iwan@partner.alphabet.pl',
                    'aneta.witon@alphabet.pl',
                    'marcin.hardej@partner.alphabet.pl',
                    'adam.kosciuk@partner.alphabet.pl',
                    'Mateusz.Pogorzelski@partner.alphabet.pl',
                    'Tomasz.Rosa@alphabet.pl'
                ]
            ],
            [
                "name" => 'ALD Automotive',
                "id" => ($isProd) ? 7904 : 7900,
                "users" => [
                    'marek.sekula@aldautomotive.com',
                    'robert.pliszka@aldautomotive.com',
                    'michal.salek@aldautomotive.com',
                    'emilia.wilczynska@aldautomotive.com',
                    'artur.konski@aldautomotive.com',
                    'maciej.arwaj@aldautomotive.com',
                    'grzegorz.kwiatkowski@aldautomotive.com',
                    'andrzej.ruminski@aldautomotive.com',
                    'malgorzata.grzanka@aldautomotive.com',
                    'paulina.kuranowska@aldautomotive.com',
                    'adam.laton@aldautomotive.com',
                    'marcin.jedrasik@aldautomotive.com',
                    'tomasz.kropielnicki@aldautomotive.com',
                    'adrianna.adamowicz@aldautomotive.com',
                    'mikolaj.kowalczyk@aldautomotive.com',
                    'justyna.pudlo@aldautomotive.com',
                    'arkadiusz.wrzalkowski@aldautomotive.com',
                    'adam.ratynski@aldautomotive.com',
                    'malgorzata.lukasik@aldautomotive.com',
                    'adam.piwko@aldautomotive.com',
                    'krzysztof.polom@aldautomotive.com',
                    'klaudia.gawryjolek@aldautomotive.com',
                    'joanna.niewiadomska@aldautomotive.com',
                    'michal.zielinski@aldautomotive.com',
                    'marta.balicka@aldautomotive.com'
                ]
            ],
            [
                "name" => 'Business Lease',
                "id" => ($isProd) ? 7902 : 7899,
                "users" => [
                    'd.turkowski@businesslease.pl',
                    'j.kozlik@businesslease.pl',
                    'k.lania@businesslease.pl',
                    'i.sech@businesslease.pl',
                    'd.zemlak@businesslease.pl',
                    'j.szewczyk@businesslease.pl',
                    'm.stasiak@businesslease.pl',
                    'r.szmolke@businesslease.pl',
                    'l.gut@businesslease.pl',
                    'm.paczkowska@businesslease.pl',
                    'm.stepien@businesslease.pl',
                    'p.stacherski@businesslease.pl',
                    'm.przybysz@businesslease.pl'
                ]
            ],
            [
                "name" => 'LeasePlan',
                "id" => ($isProd) ? 7901 : 7898,
                "users" => [
                    'tomasz.slawinski@leaseplan.com',
                    'tomasz.gejcyg@leaseplan.com',
                    'jakub.dymus@leaseplan.com',
                    'piotr.kleczkowski@leaseplan.com',
                    'kamil.rachubinski@leaseplan.com',
                    'monika.drozdz@leaseplan.com',
                    'piotr.getka@leaseplan.com',
                    'marcin.oczynski@leaseplan.com',
                    'pawel.socha@leaseplan.com',
                    'michal.jankowski@leaseplan.com',
                    'jakub.zielinski@leaseplan.com',
                    'grzegorz.rozlucki@leaseplan.com',
                    'miroslaw.grzegorek@leaseplan.com',
                    'karolina.olejnik@leaseplan.com',
                    'iwona.majewska@fams.pl',
                    'dorota.kalisz@fams.pl',
                    'monika.radosz@fams.pl',
                    'katarzyna.ziolkowska@fams.pl',
                    'kacper.rozek@leaseplan.com',
                    'maksymilian.kabulski@leaseplan.com',
                    'przemyslaw.granat@leaseplan.com',
                    'szymon.pawelek@leaseplan.com',
                    'michal.koziol@leaseplan.com',
                    'maciej.panasiuk@leaseplan.com',
                    'karolinaa.olejnik@leaseplan.com',
                    'karoll.walasek@leaseplan.com',
                    'Joanna.mackowiak@leaseplan.com',
                    'magdalena.witek@leaseplan.com',
                    'Michal.Sobocinski@leaseplan.com',
                    'artur.maruszak@fams.pl',
                    'lukasz.zadykowicz@leaseplan.com',
                    'marcin.stepniak@fams.pl',
                    'radoslaw.lewandowski@fams.pl'
                ]
            ],

        ];

    }


    private function getRecreateLP($isProd) {

        return [
            [
                "name" => 'LeasePlan',
                "id" => ($isProd) ? 7901 : 7898,
                "users" => [
                    'tomasz.slawinski@leaseplan.com',
                    'pawel.socha@leaseplan.com',
                    'miroslaw.grzegorek@leaseplan.com',
                    'karolina.olejnik@leaseplan.com',
                    'maksymilian.kabulski@leaseplan.com',
                    'przemyslaw.granat@leaseplan.com',
                    'szymon.pawelek@leaseplan.com',
                    'michal.koziol@leaseplan.com',
                    'maciej.panasiuk@leaseplan.com',
                    'karolinaa.olejnik@leaseplan.com',
                    'karoll.walasek@leaseplan.com',
                    'Joanna.mackowiak@leaseplan.com',
                    'magdalena.witek@leaseplan.com',
                    'Michal.Sobocinski@leaseplan.com',
                    'artur.maruszak@fams.pl',
                    'lukasz.zadykowicz@leaseplan.com'
                ]
            ],

        ];

    }

}
