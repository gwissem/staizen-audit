<?php

namespace AppBundle\Command;

use AppBundle\Service\SqlServiceBroker;
use AppBundle\Utils\ServiceBrokerMessage;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MssqlServiceBrokerCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('mssql-service-broker')
            ->setDescription('...');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $io = new SymfonyStyle($input, $output);

        $io->title('Service Broker is running...');

        $container = $this->getContainer();

        /** @var EntityManager $mssqlLocal */
        $mssqlLocalConnection = $container->get('doctrine.dbal.local_mssql_connection');

        $sqlServiceBroker = new SqlServiceBroker($mssqlLocalConnection);

        $sqlServiceBroker->runLoop('test_persons_queue', function ($message) use ($io) {

            /** @var $message ServiceBrokerMessage */

            $data = $message->getBody();

            if($data[0]->action === "inserted") {
                $io->success("Został dodany " . $data[0]->name . ", który ma " . $data[0]->age . " lat.");
            }
            else {

                $io->warning("Został usunięty " . $data[0]->name . ", który miał " . $data[0]->age . " lat.");
            }

            unset($result);

        });

        $output->writeln(sprintf("%s\n", "<info>Finished</info>"));
    }
}
