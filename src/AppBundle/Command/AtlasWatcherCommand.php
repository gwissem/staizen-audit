<?php

namespace AppBundle\Command;

use Dubture\Monolog\Reader\LogReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AtlasWatcherCommand extends ContainerAwareCommand
{

    //scp -rp starter@10.10.77.145:/var/www/atlas_production/shared/var/logs/stopwatch.log /home/dylesiu/www/sigmeo/atlas/var/logs/stopwatch2.log
    // sf3 atlas:watcher:parse-log 2018-09-14

    const STATS_SAVE_PATH = __DIR__ . '/../../../var/logs/';
    const LOG_PATH = __DIR__ . '/../../../var/logs/stopwatch2.log';

    /** @var null | \DateTime */
    private $parseDay = null;

    private $amountDays = 30;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:watcher:parse-log')
            ->setDescription('Display logs from Atlas')
            ->addArgument('day', InputArgument::OPTIONAL, 'Day for parsing')
            ->addOption('days', 'd', InputOption::VALUE_OPTIONAL, 'Amount of last days for parsing', 30);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->io = new SymfonyStyle($input, $output);

        if(($day = $input->getArgument('day'))) {
            $this->parseDay = new \DateTime($day);
        }

        if(($days = $input->getOption('days'))) {
            $this->amountDays = intval($days);
        }

        $this->parseLog();

    }

    private function parseLog() {

        $reader = new LogReader(self::LOG_PATH, $this->amountDays);

        /** @var StatWatch[] $stats */
        $stats = [
            'WATCHER_GET_TASK' => new StatWatch('WATCHER_GET_TASK', $this->io),
            'WATCHER_NEXT_STEP' => new StatWatch('WATCHER_NEXT_STEP', $this->io)
        ];

        $withDay = ($this->parseDay !== null);
        $formatDay = ($withDay) ? $this->parseDay->format('Y-m-d') : '';

        $this->io->writeln('In progress...');

        $amountLogs = 0;

//        $progressBar = $this->io->createProgressBar($reader->count());
//        $progressBar->start();
        foreach ($reader as $log) {

            /**
             * $log => [
             *  date,
             *  logger,
             *  level,
             *  message,
             *  context => [
             *          all,
             *          id,
             *          stepId,
             *          rootId
             *      ]
             *
             * ]
             */

            if(!empty($log)) {
                if($log['date'] instanceof \DateTime) {

                    if($withDay && $log['date']->format('Y-m-d') !== $formatDay) {
                        continue;
                    }

                    if(!empty($log['message']) && isset($stats[$log['message']])) {

                        $amountLogs++;
                        $stats[$log['message']]->addLog($log);

                    }

                }
            }
//            $progressBar->advance();
        }
//        $progressBar->finish();

        foreach ($stats as $stat) {
            $stat->display();
            $stat->saveToFile();
        }

        $this->io->writeln('Prepared: ' . $amountLogs . ' logs.');
        $this->io->writeln('Done.');

        $memory = memory_get_peak_usage() / 1024 / 1024;
        $usageMemory = number_format($memory, 3);

        $this->io->writeln('Used memory: ' . $usageMemory);


    }

}

class StatWatch {

    private $name;

    /** @var DayStats[] $days */
    private $days = [];

    /** @var StepStats[] $steps */
    private $steps = [];

    /**
     * @var null|SymfonyStyle
     */
    private $io;

    /**
     * StatWatch constructor.
     * @param string $name
     * @param SymfonyStyle|null $io
     */
    function __construct($name = '', SymfonyStyle $io = null)
    {
        $this->io = $io;
        $this->name = $name;
    }

    /**
     * @param $log
     */
    public function addLog($log) {
        if(!empty($log['context'])) {

            $dayFormat = $log['date']->format('Y-m-d');

            if(!isset($this->days[$dayFormat])) {
                $this->days[$dayFormat] = new DayStats($log['date']);
            }

            $this->days[$dayFormat]->addLog($log);

            if(isset($log['context']['stepId'])) {
                $stepId = $log['context']['stepId'];

                if(!isset($this->steps[$stepId])) {
                    $this->steps[$stepId] = new StepStats($stepId);
                }

                $this->steps[$stepId]->addLog($log);
            }

        }
    }

    public function display() {

        if($this->io) {

            $this->io->title('RESULT PER DAYS: ' . $this->name);
            $table = new Table($this->io);
            $table->setHeaders(array('Day', 'Min (ms)', 'Avg (ms)', 'Max (ms)', 'Amount'));

            foreach ($this->days as $day) {

                $table->addRow($day->getArrayResult());

            }

            $table->render();

            $this->io->title('RESULT PER STEPS: ' . $this->name);
            $table = new Table($this->io);
            $table->setHeaders(array('StepId', 'Min (ms)', 'Avg (ms)', 'Max (ms)', 'Amount'));

            foreach ($this->steps as $step) {
                $step->calculateAvg();
            }

            usort($this->steps, function($a, $b){
                return $a->getAvg() < $b->getAvg();
            });

            foreach ($this->steps as $step) {
                $table->addRow($step->getArrayResult());
            }

            $table->render();

        }

    }

    public function saveToFile() {

        // SAVE STEPS

        $data = [];

        foreach ($this->steps as $step) {
            $data[] = $step->getArrayResult();
        }

        usort($data, function($a, $b){
            return $a['avg'] < $b['avg'];
        });

        $fileName = $this->name . '_steps.csv';

        $this->save($data, ['StepId', 'Min (ms)', 'Avg (ms)', 'Max (ms)', 'Amount'], $fileName);

        // SAVE DAYS

        $data = [];

        foreach ($this->days as $day) {
            $data[] = $day->getArrayResult();
        }

        $fileName = $this->name . '_days.csv';

        $this->save($data, ['Day', 'Min (ms)', 'Avg (ms)', 'Max (ms)', 'Amount'], $fileName);

        // Save All

        $data = [];

        foreach ($this->days as $day) {
            $data = array_merge($data, $day->getLogs());
        }

        $this->saveLogs($data, $this->name . '_all_process.csv');

    }

    public function save($data, $headers, $fileName = null) {

        $file = fopen(AtlasWatcherCommand::STATS_SAVE_PATH . $fileName, 'w');

        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($file, $headers);

        foreach ($data as $row)
        {
            try {
                fputcsv($file, $row);
            }
            catch (\Exception $exception) {
                echo 'Error (file): ' . $row[0];
            }

        }

        fclose($file);

    }

    public function saveLogs($data, $fileName = null) {

        $file = fopen(AtlasWatcherCommand::STATS_SAVE_PATH . $fileName, 'w');

        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($file, ['Date', 'Type', 'Id', 'StepId', 'Time']);

        foreach ($data as $row)
        {
            try {

                $rowData = [
                    $row['date']->format('Y-m-d'), $row['message'], $row['context']['id'], $row['context']['stepId'], $row['context']['all']
                ];

                fputcsv($file, $rowData);
            }
            catch (\Exception $exception) {

                echo "Error (file): \n";
                echo $exception->getMessage();
            }

        }

        fclose($file);

    }

}


class StepStats extends TempStats {

    private $stepId;

    /**
     * StepStats constructor.
     * @param $stepId
     */
    public function __construct($stepId)
    {
        $this->stepId = $stepId;
    }

    public function getArrayResult() {
        if(!$this->calculated) $this->calculateAvg();

        return [
            'stepId' => $this->stepId,
            'min' => $this->min,
            'avg' => intval($this->avg),
            'max' => $this->max,
            'count' => $this->count
        ];
    }

}

class DayStats extends TempStats {

    private $date;

    /**
     * DayStats constructor.
     * @param $date
     */
    public function __construct($date)
    {
        $this->date = $date;
    }

    public function getArrayResult() {
        if(!$this->calculated) $this->calculateAvg();

        return [
            'day' => $this->date->format('Y-m-d'),
            'min' => $this->min,
            'avg' => intval($this->avg),
            'max' => $this->max,
            'count' => $this->count
        ];
    }
}


class TempStats {

    const ERROR_LIMIT = 0.05;

    protected $logs = [];
    protected $calculated = false;
    protected $count;
    protected $avg;
    protected $times;

    public $min;
    public $max;

    public function addLog($log){
        $this->logs[] = $log;
        $this->addTime($log['context']['all']);
    }

    /**
     * @param int $milliseconds
     */
    protected function addTime(int $milliseconds) {
        $this->count++;
        $this->times[] = $milliseconds;
    }

    public function calculateAvg() {

        if($this->calculated) return $this;

        usort($this->times, function ($a, $b) {
            return $a > $b;
        });

        $tempTimes = $this->times;

        if($this->count > 20) {

            $limit = intval(floor($this->count * self::ERROR_LIMIT));
            if($limit > 0) {
                $tempTimes = array_slice($tempTimes, $limit);
                $tempTimes = array_slice($tempTimes, 0, count($tempTimes) - $limit);
            }

        }

        $this->avg = array_sum($tempTimes)/count($tempTimes);

        $this->min = min($tempTimes);
        $this->max = max($tempTimes);

        unset($tempTimes);
        $this->calculated = true;

        return $this;

    }

    public function getAvg() {
        return $this->avg;
    }

    public function getLogs() {
        return $this->logs;
    }
}