<?php

namespace AppBundle\Command;

use ApiBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OauthClientCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('api:create-client')
            ->addArgument('name', InputArgument::REQUIRED, 'client name')
            ->setDescription('Create Client oAuth2');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $name = $input->getArgument('name');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var Client $client */
        $client = $em->getRepository('ApiBundle:Client')->findOneBy(['name' => $name]);

        if($client) {
            echo "Public Id for API CLIENT : " . $client->getPublicId() . "\n\r";
            die();
        }

        $clientManager = $this->getContainer()->get('fos_oauth_server.client_manager.default');

        /** @var Client $client */
        $client = $clientManager->createClient();
//        $client->setRedirectUris(array());
        $client->setAllowedGrantTypes(array('token', 'password'));
        $client->setName($name);
        $clientManager->updateClient($client);

        echo "Public Id for API CLIENT : " . $client->getPublicId() . "\n\r";

    }
}