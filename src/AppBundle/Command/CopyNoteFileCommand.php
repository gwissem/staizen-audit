<?php

namespace AppBundle\Command;

use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use MssqlBundle\PDO\PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CopyNoteFileCommand extends ContainerAwareCommand
{

    /**
     * Przykład: php bin/console note:copy 6528304 dev
     */

    protected function configure()
    {
        $this
            ->setName('note:copy')
            ->addArgument('noteId', InputArgument::REQUIRED, 'ID notatki')
            ->addArgument('source', InputArgument::OPTIONAL, 'Z jakiego serwera przekopiować plik', 'dev')
            ->addArgument('target', InputArgument::OPTIONAL, 'W jakie miejsce przekopiować', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $noteId = $input->getArgument('noteId');
        $source = $input->getArgument('source');
        $target = $input->getArgument('target');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $qm = $this->getContainer()->get('app.query_manager');

        $parameters = [
            [
                'key' => 'noteId',
                'value' => $noteId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $documentId = $qm->executeProcedure("
            SELECT value_int
            FROM dbo.attribute_value WITH(NOLOCK)
            WHERE parent_attribute_value_id = :noteId
            AND attribute_path = '406,226,228'
            ",
            $parameters);

        if(empty($documentId) || empty($documentId[0]['value_int'])) {
            $output->writeln('Notatka nie posiada pliku.');
            exit();
        }
        else {
            $documentId = $documentId[0]['value_int'];
        }

        if($source === "dev") {
            $sourceIp = 'starter@10.10.77.160:/var/www/atlas_staging/shared/web/files';
        }
        elseif ($source === "prod") {
            $sourceIp = 'starter@10.10.77.145:/var/www/atlas_production/shared/web/files';
        }
        else {
            $output->writeln('Podany source nie istnieje.');
            exit();
        }

        if(!empty($target)) {
            $localFilesDir = $target;
        }
        else {
            $localFilesDir = $this->getContainer()->getParameter('files_directory');
        }

        /** @var Document $document */
        $document = $em->getRepository('DocumentBundle:Document')->find($documentId);

        /** @var File $file */
        $file = $document->getFiles()->first();

        if(empty($file)) {
            $output->writeln('Notatka nie posiada pliku.');
            exit();
        }

        $pathFile = $file->getPath();

        list($empty, $dir1, $dir2, $fileName) =  explode("/", $pathFile);

        if(!file_exists($localFilesDir . '/' . $dir1 . '/' . $dir2)) {
            mkdir($localFilesDir . '/' . $dir1 . '/' . $dir2, 0777, true);
        }

        $command = 'scp -rp ' . $sourceIp . $file->getPath() . ' ' . $localFilesDir . $file->getPath();

        $output->writeln($command);

    }
}
