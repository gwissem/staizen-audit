<?php

namespace AppBundle\Command;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Repository\AttributeValueRepository;
use Doctrine\ORM\EntityManager;
use MapBundle\Controller\DefaultController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class NotesCommand extends ContainerAwareCommand
{
    /** @var QueryManager */
    private $queryManager;

    /** @var EntityManager */
    private $em;

    /** @var  SymfonyStyle */
    private $io;

    // CRON FOR PRODUCTION: 0 */2 * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console atlas:notes:clear-editable-notes --env=prod >> /var/www/atlas_production/shared/var/logs/cron_clear_editable_notes.txt

    /**
     * ONLY VIEW: atlas:notes:clear-editable-notes --onlyView=1
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:notes:clear-editable-notes')
            ->setDescription('')
            ->addOption('onlyView', 'o', InputOption::VALUE_OPTIONAL,
                'Only view query result.',
                0);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->queryManager = $this->getContainer()->get('app.query_manager');
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->io = new SymfonyStyle($input, $output);

        $onlyView = filter_var($input->getOption('onlyView', null), FILTER_VALIDATE_BOOLEAN);

        $this->clearNotes('Połączenie przychodzące', $onlyView);
        $this->clearNotes('Połączenie wychodzące', $onlyView);
    }

    private function clearNotes($text, $onlyView) {

        try {

            $anHourAgo = (new \DateTime())->modify('-2 hour')->format('Y-m-d H:i');
            $today = (new \DateTime())->format('Y-m-d');
            $now = (new \DateTime())->format('Y-m-d H:i:s');

            $query = "SELECT count(av.id) as amount
            FROM dbo.attribute_value av WITH(NOLOCK)
            WHERE av.value_text = '<span data-for-edit=\"true\">".$text.".</span>'
            AND av.attribute_path = '406,226,227'
            AND av.created_at >= '" . $today . "'
            AND av.created_at < '" . $anHourAgo . "'";

            $output = $this->queryManager->executeProcedure($query);

            if(!empty($output)) {

                $amount = intval($output[0]['amount']);

                if($onlyView) {
                    $this->io->writeln($now . ' ' . $text. '. Notatek do wyczyszczenia: ' . $amount);
                }
                else {
                    if($amount) {

                        $query = "UPDATE dbo.attribute_value
                            SET value_text = '".$text.".'
                            WHERE value_text = '<span data-for-edit=\"true\">".$text.".</span>'
                            AND attribute_path = '406,226,227'
                            AND created_at >= '" . $today . "'
                            AND created_at < '" . $anHourAgo . "'";

                        $this->queryManager->executeProcedure($query, [], false);

                        $this->io->writeln($now . ' ' . $text. '. Zostało wyczyszczone ' . $amount . ' notatek.');
                    }
                    else {
                        $this->io->writeln($now . ' ' . $text. '. Brak notatek do czyszczenia.');
                    }
                }

            }

        }
        catch (\Exception $exception) {

            $this->io->error($now . ' ' . $text. '. ' . $exception->getMessage());

        }

    }
}
