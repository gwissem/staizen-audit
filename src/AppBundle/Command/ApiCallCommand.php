<?php

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SparxBundle\Entity\Status as SparxStatus;

class ApiCallCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('api:call')
            ->addArgument('instanceId', InputArgument::REQUIRED, 'Instance of step that got API call configured')
            ->setDescription('This command is to do API call after going through the step')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $instanceId = $input->getArgument('instanceId');
        $output->writeln('Doing API CALL FOR INSTANCE ID: '.$instanceId);
        $response = $this->getContainer()->get('api_call.manager')->sendCallForInstance($input->getArgument('instanceId'));
        $output->writeln($response);
        $output->writeln('Doing API CALL finished');
    }


}
