<?php

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class RemoveSupervisorRoleCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('remove-supervisors')
            ->setDescription('Remove supervisor role from users');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln([
            '',
            '<comment>Running...</comment>',
            '===================',
            '',
        ]);

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $users = $this->getContainer()->get('doctrine')->getRepository(User::class)->findByRole('ROLE_SUPERVISOR');
        foreach($users as $user){
            /** @var User $user */
            $user->removeRole('ROLE_SUPERVISOR');
            $em->persist($user);
        }
        $em->flush();

        $em->clear(User::class);

        // Create Group Konsultant
        /** @var Group $consultantGroup */
        $consultantGroup = $this->getContainer()->get('doctrine')->getRepository('UserBundle:Group')->findOneBy([
            'name' => 'Konsultant'
        ]);

        if(!$consultantGroup) {

            $consultantGroup = new Group('Konsultant', [
                User::ROLE_CONSULTANT, User::ROLE_OPERATIONAL_DASHBOARD
            ]);

            $em->persist($consultantGroup);
            $em->flush();
        }

        $users = $this->getContainer()->get('doctrine')->getRepository(User::class)->findByRole(User::ROLE_OPERATIONAL_DASHBOARD);

        $i = 0;

        /** @var User $user */
        foreach($users as $user){

            $company = $user->getCompany();

            if($company && in_array($company->getId(),[1,2])) {
                $user->addGroup($consultantGroup);
                $user->removeRole(User::ROLE_OPERATIONAL_DASHBOARD);
                $em->persist($user);
                $i++;
            }

            if($i > 20) {
                $em->flush();
                $i = 0;
            }

        }

        $em->flush();

        $this->getContainer()->get('app.query_manager')->executeProcedure("
        
        begin tran

        insert into dbo.process_instance_property (process_instance_id,step_id,priority,priority_changed_at,platform_id)
        select pin.id, pin.step_id, 0, getdate(), max(av.value_int)
        from dbo.process_instance pin
        left join dbo.attribute_value av on av.root_process_instance_id = pin.root_id and av.attribute_path = '253'
        left join dbo.process_instance_property pip on pip.process_instance_id = pin.id 
        where pip.process_instance_id is null
        GROUP by pin.id, pin.step_id
        
        update dbo.process_instance set active = 0 where step_id = '1091.011' and active = 1
        
        
        declare @id INT
        
        declare kur cursor LOCAL for
            SELECT id from dbo.attribute_value where attribute_id = 152
        OPEN kur;
        FETCH NEXT FROM kur INTO @id;
        WHILE @@FETCH_STATUS=0
        BEGIN
            
            EXECUTE dbo.P_attribute_refresh @attribute_value_id = @id
            
        FETCH NEXT FROM kur INTO @id;
        END
        CLOSE kur
        DEALLOCATE kur

        commit tran
        
        ",[] ,false);

        $output->writeln(sprintf("%s\n", "<info>Finished</info>"));
    }
}
