<?php

namespace AppBundle\Command;

use ApiBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use FOS\OAuthServerBundle\Util\Random;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OauthTokenCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:oauth:create-token')
            ->addArgument('clientId', InputArgument::REQUIRED, 'Client Id')
            ->addArgument('userId', InputArgument::REQUIRED, 'User Id')
            ->setDescription('Create Token oAuth2');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $clientId = $input->getArgument('clientId');
        $userId = $input->getArgument('userId');

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var Client $client */
        $client = $em->getRepository('ApiBundle:Client')->findOneBy(['id' => $clientId]);
        $user = $em->getRepository('UserBundle:User')->find($userId);

        if($client && $user) {

            $tokenManager = $this->getContainer()->get('fos_oauth_server.access_token_manager.default');
            $timestamp = strtotime('01-01-2030');

            $token = $tokenManager->createToken();
            $token->setToken($this->randomToken());
            $token->setUser($user);
            $token->setClient($client);
            $token->setExpiresAt($timestamp);
            $token->setScope($client->getName());

            $tokenManager->updateToken($token);

            $output->writeln('Token: ' . $token->getToken());

        }
    }

    private function randomToken() {

        $randomData = mt_rand() . mt_rand() . mt_rand() . uniqid(mt_rand(), true) . microtime(true) . uniqid(
                mt_rand(),
                true
            );

        return rtrim(strtr(base64_encode(hash('sha256', $randomData)), '+/', '-_'), '=');

    }
}
