<?php

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SparxBundle\Entity\Status as SparxStatus;

class SparxServiceStatusUpdateCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('api:sparx:service:update')
            ->addArgument('serviceStatusId', InputArgument::REQUIRED, 'Service Status Id')
            ->setDescription('This command is to do SPARX service update after change in ATLAS')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $serviceStatusId = $input->getArgument('serviceStatusId');
//        $output->writeln('Doing API CALL FOR SERVICE STATUS: '.$serviceStatusId);
        $response = $this->getContainer()->get('api_call.manager')->sendCallForStatusUpdate($serviceStatusId);
        $output->writeln($response);
//        $output->writeln('Doing API CALL finished');
    }


}
