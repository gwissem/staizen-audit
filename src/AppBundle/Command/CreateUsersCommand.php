<?php

namespace AppBundle\Command;

use AppBundle\Entity\UserToCreate;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManager;
use MailboxBundle\Service\MailboxOutlook;
use MailboxBundle\Service\SimpleOutlookSenderService;
use UserBundle\Entity\Company;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

class CreateUsersCommand extends ContainerAwareCommand
{

    /**
     * Example use:
     *
     * php bin/console atlas:create-users
     *
     *
     * With force (without confirm action):
     *
     * php bin/console atlas:create-users -f 1
     *
     */

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:create-users')
            ->addOption('force_create', 'f', InputOption::VALUE_OPTIONAL, 'Set if want create without asking')
            ->setDescription('Skrypt do automatycznego tworzenia użytkowników');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container = $this->getContainer();
        $io = new SymfonyStyle($input, $output);
        /** @var EntityManager $em */
        $em = $container->get('doctrine.orm.entity_manager');

        $userToCreateCollection = $em->getRepository('AppBundle:UserToCreate')->findBy(['sentDate' => null]);

        $amountUserForCreate = count($userToCreateCollection);
        $isForceCreate = filter_var($input->getOption('force_create'), FILTER_VALIDATE_BOOLEAN);

        if($amountUserForCreate === 0) {
            $io->note('No one use for create. Exit...');
            exit();
        }

        $io->note("Exists $amountUserForCreate for create.");

        if(!$isForceCreate) {

            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('Are You sure? (y/n): ', false);

            if (!$helper->ask($input, $output, $question)) {
                $io->newLine();
                $io->note('Abort.');
                return;
            }
        }

        $io->newLine();
        $io->note('Start creating...');

        /** @var UserManager $um */
        $um = $container->get('fos_user.user_manager');

        if (false === empty($userToCreateCollection)) {
            foreach ($userToCreateCollection as $userToCreate) {
                /** @var User $user */
                $user = $um->createUser();
                $user = $this->createUser($userToCreate, $user);
                $this->sendEmail($userToCreate, $user);
            }
        }

        $io->newLine();
        $io->success('Success.');
    }

    private function createUser(UserToCreate $userToCreate, User $user)
    {

        /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
        $tokenGenerator = $this->getContainer()->get('fos_user.util.token_generator');
        /** @var UserManager $um */
        $um = $this->getContainer()->get('fos_user.user_manager');
        $password = substr($tokenGenerator->generateToken(), 0, 12);
        $user->setPassword($password);

        $user->setConfirmationToken($tokenGenerator->generateToken());

        $user->setEmail($userToCreate->getEmail());
        $user->setUsername($userToCreate->getUsername());
        $user->setFirstname($userToCreate->getFirstname());
        $user->setLastname($userToCreate->getLastname());
        $this->setCompany($userToCreate->getCompanyId(), $user);
        $user->setEnabled(1); //enable or disable
        $user->setPasswordRequestedAt(new \DateTime());

        $this->addRoles($userToCreate->getRoles(), $user);
        $this->addGroups($userToCreate->getGroupsId(), $user);

        $um->updateUser($user);

        return $user;
    }

    private function setCompany(int $companyId, User $user)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var Company $company */
        $company = $em->getRepository('UserBundle:Company')->findOneBy(['id' => $companyId]);

        $user->setCompany($company);
    }

    private function addRoles(string $roles, User $user)
    {

        if(empty($roles)) {
            return;
        }

        $rolesArray = explode(',', $roles);

        if (false === empty($rolesArray)) {
            foreach ($rolesArray as $role) {
                $user->addRole($role);
            }
        }
    }

    private function addGroups(string $groups, User $user)
    {

        if(empty($groups)) {
            return;
        }

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $groupsArray = explode(',', $groups);

        if (false === empty($groupsArray)) {
            foreach ($groupsArray as $group) {
                /** @var Group $groupEntity */
                $groupEntity = $em->getRepository('UserBundle:Group')->findOneBy(['id' => $group]);
                $user->addGroup($groupEntity);
            }
        }
    }

    private function sendEmail(UserToCreate $userToCreate, User $user)
    {
        $container = $this->getContainer();

        /** @var EntityManager $em */
        $em = $container->get('doctrine.orm.entity_manager');
        $twig = $container->get('twig');
        $mailboxNoReply = $em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => 'atlas-noreply@starter24.pl'
        ]);
        $simpleSender = new SimpleOutlookSenderService($mailboxNoReply);
        $email = $user->getEmail();
        $url = $container->get('assetic.parameter_bag')->get('server_host') . '/resetting/reset/' . $user->getConfirmationToken();

        $view = $twig->render('@Mailbox/EmailTemplate/modern_email.html.twig', [
            'lang_footer' => 'pl',
            'title' => 'Utworzono nowe konto w systemie Atlas',
            'content_title' => 'Utworzono nowe konto w systemie Atlas',
            'content' => 'Dzień dobry, </br>
                        zostało dodane nowe konto dla użytkownika o adresie e-mail: <b>' . $email . '</b>. </br></br>
                        Aby zacząć korzystać z Atlasa, prosimy wejść w poniższy adres, zresetować hasło a następnie zaakceptować regulamin.<br><br>
                        <a href="'. $url .'">Link do Atlasa</a>
                    '
        ]);

        $isSended = $simpleSender->send($email, "Starter24 - nowe konto w systemie Atlas", $view);

        if (true === $isSended) {
            $userToCreate->setSentDate(new \DateTime());
            $em->persist($userToCreate);
            $em->flush();
        }
    }
}
