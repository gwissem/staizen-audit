<?php

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;

class EcallUsersCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('e-call:users:update-list')
            ->setDescription('Update permissions for users');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln([
            '',
            '<comment>Running...</comment>',
            '===================',
            '',
        ]);

        $users = $this->getUsersForLeasPlan();

//        $users = $this->getUsers();

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $userRepo = $em->getRepository('UserBundle:User');
        $groupRepo = $em->getRepository('UserBundle:Group');

        /** Grupy dla użytkowników */

        $telefonia = $groupRepo->find(2045);
        $telefoniaCC = $groupRepo->find(2047);
        $ecall = $groupRepo->find(3357);
        $wszyscy = $groupRepo->find(9);
        $pracownicy = $groupRepo->find(10);
        $centrumZgloszeniowe = $groupRepo->find(33);

        if(!$telefonia || !$telefoniaCC || !$ecall || !$centrumZgloszeniowe) {
            $output->writeln(sprintf("%s\n", "<error>Nie znaleziono grup.</error>"));
        }
        else {

            foreach ($users as $userName) {

                $user = $userRepo->findOneBy(['username' => $userName]);

                if(!$user) {

                    $output->writeln(sprintf("%s:\t %s",$userName, "<error>Nie znaleziono w bazie.</error>"));

                }
                else if(!$user->isEnabled()) {
                    $output->writeln(sprintf("%s:\t %s",$userName, "<error>Konto nieaktywne.</error>"));
                }
                else {

                    if($user->hasGroup(2045)) {

                        if(!$user->hasGroup($centrumZgloszeniowe)){
                            $user->addGroup($centrumZgloszeniowe);
                            $user->setGroupsToken($user->getGroupsToken() . ',33');
                            $em->persist($user);
                            $output->writeln(sprintf("%s:\t %s",$userName, "<comment>TYLKO CentrumZgloszeniowe DODANE.</comment>"));
                        }
                        else {
                            $output->writeln(sprintf("%s:\t %s",$userName, "<comment>JUŻ DODANE.</comment>"));
                        }

                    }
                    else {
                        $user->setAcceptedRegulations(1);
                        $user->addGroup($telefonia);
                        $user->addGroup($telefoniaCC);
                        $user->addGroup($ecall);

                        if(!$user->hasGroup($wszyscy)) {
                            $user->addGroup($wszyscy);
                            $user->addGroup($pracownicy);
                        }

                        $user->addGroup($centrumZgloszeniowe);
                        $user->setGroupsToken('9,10,33,2045,2047,3357');
                        $user->addRole(User::ROLE_OPERATIONAL_DASHBOARD);

                        $em->persist($user);

                        $output->writeln(sprintf("%s:\t %s",$userName, "<info>DODANY</info>"));

                    }

                }

            }

            $em->flush();

        }

        $output->writeln(sprintf("%s\n", "<info>End.</info>"));
    }

    private function getUsersForLeasPlan() {

        $usersList = [
//            'atlas.michal.baranow' => 'michal.baranowski',
//            'atlas.renata.bartz' => 'renata.bartz',
//            'atlas.mateusz.brozdo' => 'mateusz.brozdowski',
//            'atlas.lukasz.dudek' => 'Lukasz.Dudek',
//            'atlas.arkadiusz.jani' => 'arkadiusz.jankowski',
////          'atlas.kamila.bruczyn' => 'kamila.bruczynska',
//            'atlas.krzysztof.warc' => 'krzysztof_w',
//            'atlas.lukasz.rugowsk' => 'Lukasz.Rugowski',
//            'atlas.martyna.mydlar' => 'martyna.cichowicz',
//            'atlas.michal.budnik' => 'Michal.Budnik',
//            'atlas.mikolaj.manick' => 'mikolaj.manicki',
//            'atlas.natalia.kazmie' => 'Natalia.Kazmierczak',
////          'atlas.adam.dykas' => 'adam.dykas',
//            'atlas.blanka.skiba' => 'blanka_s',
//            'atlas.jan.hylla' => 'Jan.Hylla',
//            'atlas.natalia.wolacz' => 'Natalia.Wolaczynska',
//            'atlas.norbert.matcza' => 'norbert.matczak',
//            'atlas.oliwia.kudlack' => 'oliwia.kudlacka',
//            'atlas.paulina.sieber' => 'paulina.siebert',
//            'atlas.piotr.kacki' => 'Piotr.Kacki',
//            'atlas.piotr.Liszkows' => 'Piotr.Liszkowski',
//            'atlas.piotr.baluszyn' => 'piotr.baluszynski',
//            'atlas.adrian.kossows' => 'adrian.kossowski',
//            'atlas.aldona.kozlows' => 'aldona_s',
//            'atlas.jakub.gmiterek' => 'jakub.gmiterek',
//            'atlas.jakub.przybyls' => 'jakub.przybylski',
//            'atlas.justyna.ettis' => 'justyna.ettis',
//            'atlas.kacper.swiercz' => 'kacper.swierczynski',
//            'atlas.karolina.skrzy' => 'karolina.skrzypczak',
//            'atlas.katarzyna.kopr' => 'katarzyna_ko',
//            'atlas.kornelia.wasza' => 'kornelia.waszak',
//            'atlas.krzysztof.wrze' => 'krzysztof.wrzesinski',
//            'atlas.lia.vorobieva' => 'lija_w',
//            'atlas.maciej.melech' => 'maciej.melech',
//            'atlas.malgorzata.dud' => 'malgorzata.dudek',
//            'atlas.marcin.leoniak' => 'marcin_le',
//            'atlas.marta.godawa' => 'marta_g',
//            'atlas.mateusz.beczek' => 'mateusz.beczek',
//            'atlas.natalia.fluks' => 'natalia.fluks',
//            'atlas.pamela.szczesn' => 'pamela.szczesna',
//            'atlas.patryk.kolodzi' => 'patryk.kolodziejczak',
//            'atlas.paulina.jarmus' => 'paulina.jarmuszka',
//            'atlas.paulina.lewick' => 'paulina.lewicka',
//            'atlas.piotr.feliksia' => 'piotr.feliksiak',
//            'atlas.sylwia.kolibab' => 'sylwia.kolibaba',
//            'atlas.agnieszka.wars' => 'agnieszka.warszawska',
//            'atlas.alicja.rymkiew' => 'alicja.rymkiewicz',
//            'atlas.alina.juskowia' => 'alina.juskowiak',
//            'atlas.aneta.nowicka' => 'aneta.nowicka',
//            'atlas.arkadiusz.wawr' => 'arkadiusz.wawrzyniak',
//            'atlas.filipina.wycis' => 'Filipina.Wycisk',
//            'atlas.iwona.robaszyn' => 'iwona.robaszynska',
//            'atlas.iwona.zalewska' => 'iwona.zaleska',
//            'atlas.jedrzej.strzod' => 'jadrzej.strzodka',
//            'atlas.kamil.ochocins' => 'kamil.ochocinski',
//            'atlas.lechoslaw.szym' => 'lechoslaw.szymanski',
//            'atlas.lukasz.wiacek' => 'Lukasz.Wiacek',
//            'atlas.maciej.wojciec' => 'maciej.wojciechowski',
//            'atlas.marcin.markows' => 'marcin_m',
//            'atlas.marek.pankowsk' => 'marek_p',
//            'atlas.milena.dybowsk' => 'milena.dybowska',
//            'atlas.rafal.wietrzyn' => 'rafal.wietrzynski',
//            'atlas.adam.jaroszyns' => 'Adam.Jaroszynski',
//            'atlas.iwona.zaleska' => 'iwona.zaleska',
//            'atlas.aleksander.kai' => 'aleksander.kaim',
//            'atlas.ewelina.caban' => 'ewelina.caban',
//            'atlas.michal.kuchars' => 'michal.kucharski',
//            'atlas.anna.massiel' => 'anna.massiel',
            'atlas.patryk.kusz' => 'patryk.kusz'

        ];

        $userNameList = [];

        foreach ($usersList as $key => $user) {
            $userNameList[] = $key;
        }

        return $userNameList;

    }

    private function getUsers() {
        return [
            'atlas.adam.dykas',
            'atlas.arkadiusz.jani',
            'atlas.blanka.skiba',
            'atlas.jan.hylla',
            'atlas.kamila.bruczyn',
            'atlas.krzysztof.warc',
            'atlas.lukasz.rugowsk',
            'atlas.martyna.mydlar',
            'atlas.michal.budnik',
            'atlas.mikolaj.manick',
            'atlas.natalia.kazmie',
            'atlas.natalia.wolacz',
            'atlas.norbert.matcza',
            'atlas.oliwia.kudlack',
            'atlas.paulina.sieber',
            'atlas.piotr.kacki',
            'atlas.piotr.Liszkows'
        ];
    }

}
