<?php

namespace AppBundle\Command;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Repository\AttributeValueRepository;
use Doctrine\ORM\EntityManager;
use MapBundle\Controller\DefaultController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CheckLocalizationsCommand extends ContainerAwareCommand
{
    /** @var QueryManager */
    private $queryManager;

    /** @var EntityManager */
    private $em;

    /** @var AttributeValueRepository */
    private $attributeValueRepository;

    /** @var  DefaultController */
    private $mapController;

    /**mapController
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:check-localization')
            ->setDescription('');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->queryManager = $this->getContainer()->get('app.query_manager');
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->attributeValueRepository = $this->em->getRepository('CaseBundle:AttributeValue');
        $this->mapController = new DefaultController();
        $this->mapController->setContainer($this->getContainer());
        $io = new SymfonyStyle($input, $output);

        $logFile = __DIR__ . '/../../../var/logs/check_locations.csv';

        $query = "select 
        partnerLoc.id partnerLocId, 
        locationCodeSO.value_string partnerSOId, 
        longitude.value_string longitude, 
        latitude.value_string latitude, 
        city.value_string city,
        street.value_string street, 
        number.value_string streetNumber, 
        zip.value_string zipCode,
        voivodeship.value_string voivodeship
        from dbo.attribute_value partnerLoc
        inner join dbo.attribute_value location on location.parent_attribute_value_id = partnerLoc.id and location.attribute_path = '595,597,85'
        inner join dbo.attribute_value locationCodeSO on locationCodeSO.parent_attribute_value_id = partnerLoc.id and locationCodeSO.attribute_path = '595,597,631'
        inner join dbo.attribute_value longitude on longitude.parent_attribute_value_id = location.id and longitude.attribute_path = '595,597,85,92'
        inner join dbo.attribute_value latitude on latitude.parent_attribute_value_id = location.id and latitude.attribute_path = '595,597,85,93'
        inner join dbo.attribute_value city on city.parent_attribute_value_id = location.id and city.attribute_path = '595,597,85,87'
        inner join dbo.attribute_value street on street.parent_attribute_value_id = location.id and street.attribute_path = '595,597,85,94'
        inner join dbo.attribute_value number on number.parent_attribute_value_id = location.id and number.attribute_path = '595,597,85,95'
        inner join dbo.attribute_value zip on zip.parent_attribute_value_id = location.id and zip.attribute_path = '595,597,85,89'
        inner join dbo.attribute_value country on country.parent_attribute_value_id = location.id and country.attribute_path = '595,597,85,86'
        inner join dbo.attribute_value voivodeship on voivodeship.parent_attribute_value_id = location.id and voivodeship.attribute_path = '595,597,85,88'
        where partnerLoc.attribute_path = '595,597'
        and country.value_string = 'Polska'";

        $results = [];

        $localizations = $this->queryManager->executeProcedure($query);

        /**
         *
         * $localizations[0] =>
        "partnerLocId" => "2210567"
        "partnerSOId" => "3456"
        "longitude" => "22.2637748718262"
        "latitude" => "52.1673698425293"
        "city" => "Siedlce"
        "street" => "Warszawska"
        "streetNumber" => "3"
        "zipCode" => "08-110"
         */

        $i = 0;

        $progressBar = new ProgressBar($output, count($localizations));
        $progressBar->start();

        foreach ($localizations as $loc) {

            $i++;

//            if($i < 30) {
//                continue;
//            }

//            partnerLocId, partnerSOId, longitude, latitude, city, street, number

            if(!empty($loc['longitude']) && !empty($loc['latitude'])) {

                try {

                    $points = [
                        [
                            'id' => $loc['partnerLocId'],
                            'lng' => $loc['longitude'],
                            'lat' => $loc['latitude']
                        ]
                    ];

                    $address = [
                        'country' => 'Polska',
                        'city' => trim($loc['city']),
                        'voivodeship' => trim($loc['voivodeship']),
                        'zipCode' => trim($loc['zipCode']),
                        'street' => trim($loc['street']),
                        'streetNumber' => trim($loc['streetNumber'])
                    ];

                    $resultAddress = $this->mapController->searchLocationByAddress($address);

                    if(!empty($resultAddress)) {

                        if(!empty($resultAddress[0]['latitude']) && !empty($resultAddress[0]['longitude'])) {

                            $resultRoute = $this->mapController->calculateRoute([$resultAddress[0]['latitude'], $resultAddress[0]['longitude']], $points);

                            if(!empty($resultRoute)) {

                                $results[] = [
                                    $loc['partnerLocId'],
                                    $loc['partnerSOId'],
                                    $loc['latitude'] . "," . $loc['longitude'],
                                    $resultAddress[0]['latitude'] . "," . $resultAddress[0]['longitude'],
                                    intval($resultRoute[0]['TotalRouteLength']),
                                    $this->calculateDistance([$loc['latitude'], $loc['longitude']], [$resultAddress[0]['latitude'], $resultAddress[0]['longitude']]),
                                    '',
                                    '',
                                    $this->getUrl($loc['partnerLocId'])
                                ];

//                        $io->writeln($loc['partnerLocId'] . ": (" . $loc['longitude'] . " + " . $loc['latitude'] . ") <==> (" . $resultAddress[0]['latitude'] . " + " . $resultAddress[0]['longitude'] . " = " . $resultRoute[0]['TotalRouteLength']);

                            }
                            else {
//                        $io->error('Nie można policzyć trasy');
                                $results[] = [
                                    $loc['partnerLocId'],
                                    $loc['partnerSOId'],
                                    $loc['latitude'] . "," . $loc['longitude'],
                                    $resultAddress[0]['latitude'] . "," . $resultAddress[0]['longitude'],
                                    '',
                                    $this->calculateDistance([$loc['latitude'], $loc['longitude']], [$resultAddress[0]['latitude'], $resultAddress[0]['longitude']]),
                                    'Nie można policzyć trasy',
                                    '',
                                    $this->getUrl($loc['partnerLocId'])
                                ];
                            }

                        }
                        else {

                            $desc = "Podany: " . $address['city'] . " [" . $address['zipCode'] . "] " . $address['street'] . " " . $address['streetNumber'];
                            $desc .= " (" . $address['voivodeship'] . ")\r\n";
                            $desc .= "Propozycja: " . $resultAddress[0]['city'] . " [" . $resultAddress[0]['zipCode'] . "] " . $resultAddress[0]['street'];
                            $desc .= " (" . $resultAddress[0]['voivodeship'] . ", " . $resultAddress[0]['county'] . ", " . $resultAddress[0]['commune'] . ")";

                            $results[] = [
                                $loc['partnerLocId'],
                                $loc['partnerSOId'],
                                $loc['latitude'] . "," . $loc['longitude'],
                                '',
                                '',
                                '',
                                'Nie można określić dokładnej lokalizacji',
                                $desc,
                                $this->getUrl($loc['partnerLocId'])
                            ];
                        }

                    }
                    else {
                        $results[] = [
                            $loc['partnerLocId'],
                            $loc['partnerSOId'],
                            $loc['latitude'] . "," . $loc['longitude'],
                            '',
                            '',
                            '',
                            'Nie ma wyników po degeolokalizacji',
                            '',
                            $this->getUrl($loc['partnerLocId'])
                        ];
//                    $io->error('Nie ma wyników po degeolokalizacji - ' . $loc['partnerLocId']);
                    }

                }
                catch (\Exception $exception) {

                    $results[] = [
                        $loc['partnerLocId'],
                        $loc['partnerSOId'],
                        $loc['latitude'] . "," . $loc['longitude'],
                        '',
                        '',
                        '',
                        'ERROR',
                        $exception->getMessage() . ", Line: " . $exception->getLine(),
                        $this->getUrl($loc['partnerLocId'])
                    ];

                }

            }
            else {

                $results[] = [
                    $loc['partnerLocId'],
                    $loc['partnerSOId'],
                    '',
                    '',
                    '',
                    '',
                    'Brak współrzędnych!',
                    '',
                    $this->getUrl($loc['partnerLocId'])
                ];

                $io->error('Brak lokalizacji w ' . $loc['partnerLocId']);
            }


            $progressBar->advance();

//            if($i >= 100) {
//                break;
//            }

        }

        $progressBar->finish();

        $this->saveToFile($results, $logFile);

//        $table = new Table($output);
//        $table
//            ->setHeaders(array('ID Atlas', 'ID SO', 'Coordinates in DB', 'Geocode Coordinates', 'Distance', 'Distance (line)', 'Error', 'Description'))
//        ;
//
//        foreach ($results as $row) {
//            $table->addRow($row);
//        }
//
//        $table->render();

        $io->writeln("Memory Usage: ".(memory_get_peak_usage(true)/1024/1024)." MiB\n\n");

    }

    private function saveToFile($results, $filePath) {

        $file = fopen($filePath, 'w');

        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($file, array('ID', 'ID SO', 'Coordinates in DB', 'Geocode Coordinates', 'Distance (street)', 'Distance (line)', 'Error', 'Description', 'Url'));

        foreach ($results as $row)
        {
            try {
                fputcsv($file, $row);
            }
            catch (\Exception $exception) {
                echo 'Error (file): ' . $row[0];
            }

        }

        fclose($file);

    }

    private function getUrl($partnerLocId) {

        /** @var AttributeValue $attr */
        $attr = $this->attributeValueRepository->find(intval($partnerLocId));

        if($attr) {
            $partnerId = $attr->getParentAttributeValue()->getId();
            if($partnerId) {
                return 'https://atlas.starter24.pl/operational-dashboard/partner?partner-id='.$partnerId.'&attribute-id=' . $partnerLocId;
            }
        }

        return '';

    }

    function calculateDistance($ini, $fin) {
        $R = 6371; // km
        $dLat = deg2rad($fin[0]-$ini[0]);
        $dLon = deg2rad($fin[1]-$ini[1]);
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($ini[0])) * cos(deg2rad($fin[0])) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $d = $R * $c;
        return intval($d * 1000);
    }

}
