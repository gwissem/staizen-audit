<?php

namespace AppBundle\Command;

use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ApiScheduledMssqlJobCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('api-scheduled-job:mssql:run')
            ->addArgument('procedureName', InputArgument::REQUIRED, 'ProcedureName')
            ->addArgument('args', InputArgument::OPTIONAL, 'Arguments in JSON')
            ->setDescription('This command Exec specific procedure with arguments')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $procedureName = $input->getArgument('procedureName');
        $argumentsJson = $input->getArgument('args');

        $args = [];

        if(!empty($argumentsJson)) {
            $args = json_decode($argumentsJson, true);
        }

        $this->getContainer()->get('doctrine')->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);
        $queryManager = $this->getContainer()->get('app.query_manager');

        $parameters = [];

        foreach ($args as $key => $arg) {
            $parameters[] = [
                'key' => $arg['name'],
                'value' => $arg['value'],
                'type' => $this->getArgType($arg['type'])
            ];
        }

        $query = "EXEC " . $procedureName . " ";

        foreach ($parameters as $key => $parameter) {

            $query .= '@'.$parameter['key'] . ' = :' . $parameter['key'];

            if((count($parameters)-1) != $key) {
                $query .= ", ";
            }

        }

        $queryManager->executeProcedure(
            $query, $parameters, false
        );


        $parametersPrint = print_r($parameters, true);

        $response = "Success Executed.\n";
        $response .= "Query: " . $query . "\n";
        $response .= "Parameters: \n" . $parametersPrint;

        $output->writeln($response);

    }


    private function getArgType($type) {

        switch ($type) {
            case 'INT': {
                return  PDO::PARAM_INT;
            }
            case 'STR': {
                return PDO::PARAM_STR;
            }
            case 'INT_OUT': {
                return PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT;
            }
            case 'STR_OUT': {
                return PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT;
            }
            default: {
                return PDO::PARAM_STR;
            }
        }

    }

}
