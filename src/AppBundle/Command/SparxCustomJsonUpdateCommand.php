<?php

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SparxBundle\Entity\Status as SparxStatus;

class SparxCustomJsonUpdateCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('api:sparx:jsonDefinition:update')
            ->addArgument('instanceId', InputArgument::REQUIRED, 'instanceId')
            ->addArgument('definitionId', InputArgument::REQUIRED, 'Json Definition Id')
            ->setDescription('This command is to do SPARX service update after change in ATLAS')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $instanceId = $input->getArgument('instanceId');
        $definitionId = $input->getArgument('definitionId');

        $response = $this->getContainer()->get('api_call.manager')->sendCallForCustomJsonDefinition($instanceId, $definitionId);
        $output->writeln($response);
    }


}
