<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportInsuranceDataCommand extends ContainerAwareCommand
{
    const COMMAND_NAME = 'vin:import';

    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Import insurance XLS data')
            ->setHelp("This command allows you parse xls file and insert headers and elements into the database")
            ->addArgument('packageId', InputArgument::REQUIRED, 'Package ID of parsed XLS')
            ->addArgument('fileId', InputArgument::OPTIONAL, 'File ID', 0)
            ->addArgument('importerId',  InputArgument::OPTIONAL, 'Importer ID ', 0);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('app.insurance_data_import')->process(
            $input->getArgument('packageId'),
            $input->getArgument('fileId'),
            $input->getArgument('importerId'),
            $output
        );
    }
}