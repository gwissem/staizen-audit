<?php

namespace AppBundle\Command;

use AppBundle\Entity\Job;
use AppBundle\Utils\QueryManager;
use AppBundle\Utils\UCCXModel\Resource;
use AppBundle\Utils\UCCXModel\skillCompetency;
use AppBundle\Utils\UCCXModel\skillMap;
use AppBundle\Utils\UCCXModel\skillNameUriPair;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Repository\AttributeValueRepository;
use CaseBundle\Utils\Language;
use Doctrine\ORM\EntityManager;
use MailboxBundle\Entity\ConnectedMailbox;
use MailboxBundle\Service\MailboxManagementService;
use MailboxBundle\Service\MailboxOutlook;
use MailboxBundle\Service\OutlookProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Promise;
use function GuzzleHttp\Promise\settle;
use GuzzleHttp\Psr7\Request;
use JMS\Serializer\SerializerBuilder;
use MapBundle\Controller\DefaultController;
use Moment\Moment;
use Monolog\Handler\ElasticSearchHandler;
use Monolog\Logger;
use PhpMimeMailParser\Parser;
use SocketBundle\Service\RedisListListenerService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Stopwatch\Stopwatch;
use UserBundle\Entity\User;

class TestCommand extends ContainerAwareCommand
{
    /** @var QueryManager */
    private $queryManager;

    /** @var EntityManager */
    private $em;

    /** @var  SymfonyStyle */
    private $io;

    // CRON FOR PRODUCTION: 0 */2 * * * /usr/bin/php7.0 /var/www/atlas_production/current/bin/console atlas:notes:clear-editable-notes --env=prod >> /var/www/atlas_production/shared/var/logs/cron_clear_editable_notes.txt

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:testing')
            ->setDescription('');
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // $this->testCrawler();

//        $this->textUccxService();

        die();

//        /** @var EntityManager $em */
//        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
//
//        $em->getRepository('AppBundle:Job');
//
//        $job = new Job('api-scheduled-job:mssql:run', array(
//            'procedureName' => 'test_procedure_123',
//            'args' => '[{"name":"processInstanceId","type":"INT","value":"123123"}]'
//        ), true, "mssql_cron_queue");
//
//        $date = new \DateTime();
//        $date->add(new \DateInterval('PT10S'));
//        $job->setExecuteAfter($date);
//
//        $em->persist($job);
//        $em->flush();


//        /** @var EntityManager $em */
//        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
//
//        $em->getConnection()->setTransactionIsolation(\Doctrine\DBAL\Connection::TRANSACTION_READ_UNCOMMITTED);
//
//        $all = $em->getRepository('CaseBundle:Cost')->findAll();

//        $config = [
//            'host' => '10.10.77.154',
//            'port' => '9200'
//        ];
//
//        $client = new \Elastica\Client($config);
//
//        $options = array(
//            'index' => 'elastic_index_name',
//            'type' => 'elastic_doc_type',
//        );
//
//        $handler = new ElasticSearchHandler($client, $options);
//        $log = new Logger('application');
//        $log->pushHandler($handler);
//
//        $log->log('INFO', 'test-log', [
//            'test'=> 123
//        ]);


//        $esLog = $this->getContainer()->get('app.elasticsearch.log');
//        $esLog->log('case.monitor.get', ['time.full' => 0.2311]);


//        $job = [
//            'redis_listener.websocket_connections' => 58
//        ];
//
//        $this->getContainer()->get('snc_redis.default')->rpush(
//            RedisListListenerService::ATLAS_ELASTICA_MONITOR,
//            [
//                serialize($job)
//            ]
//        );

    }

    /**
     *
     */
    function testCrawler() {

        $xml = '<Dialog>
  <associatedDialogUri/>
  <fromAddress>1</fromAddress>
  <id>32286551</id>
  <mediaProperties>
    <DNIS>750</DNIS>
    <callType>ACD_IN</callType>
    <callvariables>
      <CallVariable>
        <name>callVariable1</name>
        <value>PZUSA</value>
      </CallVariable>
      <CallVariable>
        <name>callVariable2</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>callVariable3</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>callVariable4</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>callVariable5</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>callVariable6</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>callVariable7</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>callVariable8</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>callVariable9</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>callVariable10</name>
        <value/>
      </CallVariable>
      <CallVariable>
        <name>userCSQid</name>
        <value>649</value>
      </CallVariable>
    </callvariables>
    <dialedNumber>758</dialedNumber>
    <mediaId>1</mediaId>
    <outboundClassification/>
    <queueName>PZU</queueName>
    <queueNumber>182</queueNumber>
  </mediaProperties>
  <mediaType>Voice</mediaType>
  <participants>
    <Participant>
      <actions>
        <action>TRANSFER_SST</action>
        <action>CONSULT_CALL</action>
        <action>HOLD</action>
        <action>UPDATE_CALL_DATA</action>
        <action>SEND_DTMF</action>
        <action>START_RECORDING</action>
        <action>DROP</action>
      </actions>
      <mediaAddress>1292</mediaAddress>
      <mediaAddressType/>
      <startTime>2019-11-19T08:04:35.639Z</startTime>
      <state>ACTIVE</state>
      <stateCause/>
      <stateChangeTime>2019-11-19T08:04:35.639Z</stateChangeTime>
    </Participant>
    <Participant>
      <actions>
        <action>TRANSFER_SST</action>
        <action>CONSULT_CALL</action>
        <action>HOLD</action>
        <action>UPDATE_CALL_DATA</action>
        <action>SEND_DTMF</action>
        <action>START_RECORDING</action>
        <action>DROP</action>
      </actions>
      <mediaAddress>750</mediaAddress>
      <mediaAddressType>AGENT_DEVICE</mediaAddressType>
      <startTime>2019-11-19T08:04:51.732Z</startTime>
      <state>ACTIVE</state>
      <stateCause/>
      <stateChangeTime>2019-11-19T08:04:51.732Z</stateChangeTime>
    </Participant>
    <Participant>
      <actions>
        <action>TRANSFER_SST</action>
        <action>CONSULT_CALL</action>
        <action>HOLD</action>
        <action>UPDATE_CALL_DATA</action>
        <action>SEND_DTMF</action>
        <action>START_RECORDING</action>
        <action>DROP</action>
      </actions>
      <mediaAddress>1</mediaAddress>
      <mediaAddressType/>
      <startTime>2019-11-19T08:04:35.639Z</startTime>
      <state>ACTIVE</state>
      <stateCause/>
      <stateChangeTime>2019-11-19T08:04:45.682Z</stateChangeTime>
    </Participant>
  </participants>
  <state>ACTIVE</state>
  <toAddress>758</toAddress>
  <uri>/finesse/api/Dialog/32286551</uri>
</Dialog>';

        $crawler = new Crawler();
        $crawler->addXmlContent($xml);

        $existAnyDialogs = (bool) ($crawler->filter('Dialog')->first()->count());

        $dialogDetails = [];

        if ($existAnyDialogs)
        {

            $csqId = null;

            if($crawler->filter('Dialog > mediaProperties > callvariables')->count()) {

                $variables = $crawler->filter('Dialog > mediaProperties > callvariables')->children();

                if($variables->count() > 0) {
                    foreach ($variables as $child) {
                        /** @var \DOMElement $child */
                        $name = $child->getElementsByTagName('name')->item(0)->nodeValue;
                        if($name === 'userCSQid') {
                            $csqId = $child->getElementsByTagName('value')->item(0)->nodeValue;
                        }
                    }
                }
            }

            $dialogDetails = [
                'fromAddress' => $crawler->filter('Dialog > fromAddress')->first()->text(),
                'CSQid' => $csqId,
                'toAddress' => $crawler->filter('Dialog > toAddress')->first()->text(),
                'idDialog' => $crawler->filter('Dialog > id')->first()->text(),
                'type' => $crawler->filter('Dialog > mediaProperties > callType')->first()->text(),
                'status' => $crawler->filter('Dialog > state')->first()->text(),
                'departamentCode' => $crawler->filter('Dialog > mediaProperties > DNIS')->first()->text()
            ];

        }
        else {
            echo 'Nie ma';
        }

    }

    function testForwardEmail() {

        $mailboxOutgoingService = $this->getContainer()->get('atlas.mailbox.outgoing_email.service');

        /** @var User $user */
        $user = $this->getContainer()->get('doctrine')->getEntityManager()->getRepository('UserBundle:User')->find(1449);

        $this->getContainer()->get('user.info')->setVirtualUser($user);

        $to = '';
        $cc = '';

        $mailboxOutgoingService->forwardEmailNote(10210376, 'atlas_test@starter24.pl', $to, $cc, null, 'Jakiś dodatkowy tekst');

    }

    function textUccxService() {


//        $uccxService = $this->getContainer()->get('uccx.service');

//        $uccxService->createQueue();
//        $res = $uccxService->getQueue('206');
//        $res = $uccxService->getQueues();


//        $uccxService->setSkillsOfQueue(146, [
//            [
//                'id' => 157,
//                'priority' => 5,
//                'weight' => 1
//            ]
//        ]);

//        $result = $uccxService->getResources([
//            'janusz.nietypowy'
//        ]);


//        $res = $uccxService->customRequest('team', 'GET', '18');
//
//        $uccxService->setSkillsForUsers(['janusz.nietypowy'], [
//            [
//                'id' => 157,
//                'priority' => 6
//            ],
//            [
//                'id' => 165,
//                'priority' => 7
//            ],
//            [
//                'id' => 162,
//                'priority' => 5
//            ],
//            [
//                'id' => 177,
//                'priority' => 8
//            ]
//
//        ]);

    }

}
