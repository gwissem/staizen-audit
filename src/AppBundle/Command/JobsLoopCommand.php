<?php

namespace AppBundle\Command;

use ApiBundle\Entity\AccessToken;
use AppBundle\Entity\Config;
use AppBundle\Utils\QueryManager;
use Doctrine\ORM\EntityManager;
use DOMElement;
use Machines\CentralMachine;
use MssqlBundle\PDO\PDO;
use Predis\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use UserBundle\Entity\TelephonyAgentStatus;
use UserBundle\Entity\User;

class JobsLoopCommand extends ContainerAwareCommand
{

    /**
     * Examples:
     *
     * php bin/console job:loop get_xml_incoming 5
     *
     * php bin/console job:loop mssql_monitor 1
     *
     *  Opcjonalnie: --env_atlas=prod   - nadpisuje atlas_env - np, żeby używać XMLi z produkcji (SO)
     *
     *
     * FOR Supervisor:
     *
        [program:ecall_psa_job]
        command=php current/bin/console job:loop get_xml_incoming 5 --env=prod
        process_name=%(program_name)s
        numprocs=1
        directory=/var/www/atlas_staging
        autostart=true
        autorestart=true
        user=www-data
        redirect_stderr=false
        stdout_logfile=/var/www/atlas_staging/current/var/logs/ecall_psa_job.out.log
        stdout_capture_maxbytes=1MB
        stderr_logfile=/var/www/atlas_staging/current/var/logs/ecall_psa_job.error.log
        stderr_capture_maxbytes=1MB

     */

    const JOB_REFRESH_TELEPHONY_STATS = 'refresh_telephony_stats';
    const JOB_GET_XML_INCOMING = 'get_xml_incoming';
    const JOB_MSSQL_MONITOR = 'mssql_monitor';

    /** @var QueryManager */
    private  $qm;

    private $debugMode = false;

    private $envAtlas = null;

    /** @var EntityManager  */
    private $entityManager;

    /** @var Crawler  */
    private $crawler;

    /** @var Client  */
    private $redis;

    protected function configure()
    {
        $this
            ->setName('job:loop')
            ->setDescription('Create loop executing commands each X seconds for specific job')
            ->addArgument('name', InputArgument::REQUIRED, 'Name of Job')
            ->addArgument('timer', InputArgument::REQUIRED, 'Time interval in seconds')
            ->addOption('debug_mode', 'd', InputOption::VALUE_OPTIONAL, 'Set 1 if want DEBUG MODE')
            ->addOption('env_atlas', 'a', InputOption::VALUE_OPTIONAL, 'Environment of Atlas  (dev/prod/test)');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $loop = \React\EventLoop\Factory::create();
        $timer = $input->getArgument('timer');
        $jobName = $input->getArgument('name');
        $this->qm = $this->getContainer()->get('app.query_manager');

        if($input->hasOption('debug_mode')) {
            $this->debugMode = filter_var($input->getOption('debug_mode'), FILTER_VALIDATE_BOOLEAN);
        }

        if($input->hasOption('env_atlas')) {
            $this->envAtlas = $input->getOption('env_atlas');
        }

        if($timer && is_numeric($timer) ) {

            $output->writeln('Job ' . $jobName . ' run... Periodic time: ' . $timer);

            if($jobName === self::JOB_GET_XML_INCOMING) {

                $eCallPSAService = $this->getContainer()->get('app.service.ecall_psa');
                $eCallPSAService->setLogger($output);

                if($this->envAtlas) {
                    $eCallPSAService->setProdMode($this->envAtlas);
                }

                $loop->addPeriodicTimer($timer, function () use ($eCallPSAService, $output) {

                    try{
                        $eCallPSAService->loop();
                    }
                    catch (\Exception $exception) {
                        $output->writeln('Error in loop of get_xml_incoming. Error: ' . $exception->getMessage() . ", line: " .  $exception->getLine() . ", file: ".  $exception->getFile());
                        throw $exception;
                    }

                });

                $eCallPSAService->loop();

            }
            elseif ($jobName === self::JOB_REFRESH_TELEPHONY_STATS) {

                $logTelephony = filter_var($this->getContainer()->getParameter('log_telephony_status'), FILTER_VALIDATE_BOOLEAN);

                if($logTelephony) {
                    $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
                    $this->crawler = new Crawler();
                    $this->redis = $this->getContainer()->get('snc_redis.default_client');
                }

                $loop->addPeriodicTimer($timer, function () use($logTelephony) {

                    $this->updateTerminatedNotReady();

                    $this->refreshTelephonyStats();

                    if($logTelephony) {
                        $this->logTelephonyStats();
                    }

                });

            }
            elseif( $jobName === self::JOB_MSSQL_MONITOR) {

                try {

                    $esLogService = $this->getContainer()->get('app.elasticsearch.log');
                    $queryManager = $this->getContainer()->get('app.query_manager');

                }
                catch (\Exception $exception) {

                    $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' ERROR. Error message: ' . $exception->getMessage());
                    exit();

                }

                $loop->addPeriodicTimer($timer, function () use ($esLogService, $queryManager, $output) {

//                    $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' Loop MSSQL.');
                    $esLogService->LoopMssqlLog($queryManager, $output);

                });

                $loop->addPeriodicTimer(60, function () use ($esLogService, $output) {
                    $esLogService->checkChangeIndex($output);
                });

                $esLogService->LoopMssqlLog($queryManager, $output);

            }
            else {

                $output->writeln('Job ' . $jobName . ' is not supported.');
                exit();

            }

            $loop->addPeriodicTimer(60, function() use ($output) {
                $this->printUsageMemory($output);
            });

            $loop->run();

        }

    }

    private function printUsageMemory(OutputInterface $output) {
        $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' Used memory: ' . number_format((memory_get_usage() / 1024 / 1024), 3));
    }

    /** iteracja po telephony_agent_status i zmiana na ready */
    private function updateTerminatedNotReady(){

        $workTimeConfig = $this->entityManager->getRepository(Config::class)->findOneByKey('telephony_work_status_time');
        $workTimeConfigValue = 0;
        if($workTimeConfig){
            $workTimeConfigValue = (int)$workTimeConfig->getValue();
        }

        if($workTimeConfigValue > 0){
            $agents = $this->entityManager->getRepository(TelephonyAgentStatus::class)->findBy(['codeId' => 5, 'state' => 'NOT_READY']);

            /** @var TelephonyAgentStatus $agent */
            foreach($agents as $agent){
                $now = new \DateTime();
                $last = $agent->getStateChangeTime();
                if(!$last){
                    $last = $now;
                }
                $diff = $now->getTimestamp() - $last->getTimestamp();
                if($diff > $workTimeConfigValue){
                    $username = $agent->getUsername();
                    $isOnline = $this->entityManager->getRepository(User::class)->findOneByUsername($username)->isOnline();
                    if($isOnline) {
                        $url = $this->getContainer()->getParameter('server_host').$this->getContainer()->get('router')->generate('jabberVoiceSetStatusReadyUser',['username' => $username]);
                        $token = $this->entityManager->getRepository(AccessToken::class)->findOneBy(['scope' => 'api_jobs', 'user' => 1]);
                        if($token){
                            $headers = [
                                'Authorization' => 'Bearer ' . $token->getToken()
                            ];
                            $client = new \GuzzleHttp\Client([
                                'base_uri' => $url,
                                'timeout' => 20,
                                'verify' => false
                            ]);
                            $response = $client->request('PUT',$url,[
                                'headers' => $headers
                            ]);
                        }
                    }
                }
            }
        }
    }
    /**
     * Aktualizuje tabeleTelephonyAgentStatus
     */
    private function logTelephonyStats() {

        $xmlResponse = $this->redis->get(CentralMachine::TELEPHONY_AGENTS);

        $this->entityManager->clear();
        $this->crawler->clear();
        $this->crawler->addXmlContent($xmlResponse);

        if(!empty($xmlResponse)) {

            $memberOfTeam = $this->crawler->filter('User');
            $users = [];
            $timeZone = new \DateTimeZone('Europe/Warsaw');

            /** @var DOMElement $member */
            foreach ($memberOfTeam->getIterator() as $key => $member) {

                $loginId = $member->getElementsByTagName('loginId')->item(0)->nodeValue;

                $users[$loginId] = [
                    'state' => $member->getElementsByTagName('state')->item(0)->nodeValue,
                    'loginId' => $loginId,
                    'number' => $member->getElementsByTagName('extension')->item(0)->nodeValue,
                    'codeId' => null,
                    'stateChangeTime' => $member->getElementsByTagName('stateChangeTime')->item(0)->nodeValue,
                ];

                if(!empty($users[$loginId]['stateChangeTime'])) {
                    $users[$loginId]['stateChangeTime'] = new \DateTime($users[$loginId]['stateChangeTime']);
                    $users[$loginId]['stateChangeTime']->setTimezone($timeZone);
                }
                else {
                    $users[$loginId]['stateChangeTime'] = null;
                }

                if($users[$loginId]['state'] == CentralMachine::NOT_READY) {
                    if($member->getElementsByTagName('reasonCode')->length) {
                        $codeId = intval($member->getElementsByTagName('reasonCode')->item(0)->getElementsByTagName('code')->item(0)->nodeValue);
                        $users[$loginId]['codeId'] = $codeId;
                    }
                }

            }

            if(count($users)) {

                /** @var TelephonyAgentStatus[] $telephonyStatus */
                $telephonyStatus = $this->entityManager->getRepository('UserBundle:TelephonyAgentStatus')->findAll();

                $i = 0;

                foreach ($telephonyStatus as $status) {

                    if(isset($users[$status->getUsername()])) {
                        $u = $users[$status->getUsername()];
                        if($u['stateChangeTime'] != $status->getStateChangeTime()) {
                            $status->setStateChangeTime($u['stateChangeTime']);
                            $status->setState($u['state']);
                            if(!empty($u['codeId']) || $u['state'] == CentralMachine::READY) {
                                $status->setCodeId($u['codeId']);
                            }
                            $this->entityManager->persist($status);
                            $i++;
                        }
                        unset($users[$status->getUsername()]);
                    }

                }

                $this->entityManager->flush();

                try {

                    /** Dodawanie nowych użytkowników */

                    foreach ($users as $key => $user) {

                        $newTelephonyAgentStatus = new TelephonyAgentStatus();
                        $newTelephonyAgentStatus->setUsername($key);
                        $newTelephonyAgentStatus->setState($user['state']);

                        if($user['number']) {
                            $newTelephonyAgentStatus->setNumber($user['number']);
                        }

                        $newTelephonyAgentStatus->setCodeId($user['codeId']);

                        if(!empty($user['stateChangeTime'])) {
                            $newTelephonyAgentStatus->setStateChangeTime($user['stateChangeTime']);
                        }

                        $this->entityManager->persist($newTelephonyAgentStatus);
                    }

                    $this->entityManager->flush();

                }
                catch (\Throwable $exception) {
                    echo $exception->getMessage();
                }

            }

        }

    }

    private function refreshTelephonyStats(){

        $query = 'SELECT tabName from AtlasDB_calls.dbo.counters';
        $queryManager = $this->getContainer()->get('app.query_manager');
        $countersList =  $queryManager->executeProcedure($query);

        foreach ($countersList as  $value)
        {
            $params =[
                [
                    'key' => 'tabname',
                    'value' => $value['tabName'],
                    'type' => PDO::PARAM_STR,
                    'length' => 100,
                ]
                ];
            $this->qm->executeProcedure("EXEC dbo.p_statCallManager @tabName = :tabname",$params, false);
        }

    }
}