<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VinImportCronCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('vin:import:run-cron')
            ->setDescription('Cron for vin database imports');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln([
            '',
            '<comment>Cron is running...</comment>',
            '===================',
            '',
        ]);

        $importService = $this->getContainer()->get('app.insurance_data_import');
        $result = $importService->startCron($output);

        $output->writeln([
            '<info>Result</info>',
            '==================='
        ]);

        $output->writeln([
            '',
            $result,
            ''
        ]);

        $output->writeln(sprintf("%s\n", "<info>End process.</info>"));
    }
}
