<?php

namespace AppBundle\Command;

use AppBundle\Entity\Log;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Listener\DocumentUploadListener;
use MailboxBundle\Service\MailboxManagementService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class CleanTempFilesCommand extends ContainerAwareCommand
{

    /**
     * Przykład: php bin/console cleaner:temp-files --env=prod
     */

    const LIMIT_DAYS = 1;

    private $limit;

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var EntityManager */
    private $em;

    protected function configure()
    {
        $this
            ->setName('cleaner:temp-files');

        $this->limit = time() - (3600 * 24 * self::LIMIT_DAYS);

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->input = $input;
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $this->clean('cleanTempFiles');
        $this->clean('cleanEmailAttachments');

    }

    private function clean($nameMethod)
    {

        $this->output->writeln('Start clean: ' . $nameMethod);

        $start = microtime(true);

        try {

            $counter = $this->{$nameMethod}();
            $end = microtime(true) - $start;
            $log = Log::create('clean_temp_files', 'success', null, $nameMethod, $counter, $end);

        } catch (\Exception $exception) {

            $end = microtime(true) - $start;
            $log = Log::create('clean_temp_files', $exception->getMessage(), null, $nameMethod, $end);

        }

        $this->output->writeln('Done clean.');

        $this->em->persist($log);
        $this->em->flush();

    }

    /**
     * Clean files from "data/temp-files"
     * There are cache of generated PDFs and others upload files
     *
     * @return int
     */

    private function cleanTempFiles()
    {

        $tempFilesPath = $this->getContainer()->getParameter('kernel.root_dir') . DocumentUploadListener::PATH_TEMP_FILES;

        $counter = 0;

        /*
         * Można by zastąpić wydajną komendą bashową
         *
         * find ./ -maxdepth 1 -type d -mtime +1 -exec rm -r "{}" \;
         *
         */

        if (is_dir($tempFilesPath)) {

            $dh = opendir($tempFilesPath);

            if ($dh === false) {
                return 0;
            }

            while (($filename = readdir($dh)) !== false) {

                $file = $tempFilesPath . '/' . $filename;

                if ($filename === '.gitkeep' || !is_file($file)) {
                    continue;
                }

                if (filemtime($file) < $this->limit) {
                    $counter++;
                    unlink($file);
                }

            }

            closedir($dh);

        }

        return $counter;

    }

    /**
     * Clean files from "web/files/email_attachments"
     * There are cached attachments of email
     *
     * @return int
     */
    private function cleanEmailAttachments()
    {

        $path = $this->getContainer()->getParameter('kernel.root_dir') . MailboxManagementService::EMAILS_ATTACHMENT_PATH;

        $counter = 0;

        $fs = new Filesystem();

        /*
         * Można by zastąpić wydajną komendą bashową
         *
         * find ./ -type f -mtime +1 -delete
         *
         */

        if (is_dir($path)) {

            $dh = opendir($path);

            if ($dh === false) {
                return 0;
            }

            while (($filename = readdir($dh)) !== false) {

                if($filename === '.' || $filename === '..') {
                    continue;
                }

                $file = $path . '/' . $filename;

                if (!is_dir($file)) {
                    continue;
                }

                if (filemtime($file) < $this->limit) {

                    $counter++;
                    $fs->remove($file);

                }

            }

            closedir($dh);

        }

        return $counter;

    }

}
