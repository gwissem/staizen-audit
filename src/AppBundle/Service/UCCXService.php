<?php

namespace AppBundle\Service;

use AppBundle\Utils\UCCXModel\AgentStats;
use AppBundle\Utils\UCCXModel\poolSpecificInfo;
use AppBundle\Utils\UCCXModel\Queue;
use AppBundle\Utils\UCCXModel\Queues;
use AppBundle\Utils\UCCXModel\Resource;
use AppBundle\Utils\UCCXModel\Skill;
use AppBundle\Utils\UCCXModel\skillCompetency;
use AppBundle\Utils\UCCXModel\skillGroup;
use Elastica\Exception\NotFoundException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Twig\Environment;
use Predis\Client as RedisClient;

class UCCXService {

    const DOCS_LINK = 'https://developer.cisco.com/docs/contact-center-express/#!system-configuration-apis';

    const REDIS_SKILL_PREFIX = 'uccx:skill_';
    const REDIS_AGENT_STATS = 'uccx:agent_stats';

    /** @var string */
    private $login;

    /** @var string */
    private $password;

    private $uccxIP;

    /**
     * @var Environment
     */
    private $twig;

    /** @var Skill[]  */
    private $skills = [];

    /** @var RedisClient  */
    private $redis;

    /**
     * @var \JMS\Serializer\Serializer
     */
    private $serializer;

    /**
     * UCCXService constructor.
     * @param Environment $twig
     * @param RedisClient $redis
     * @param string $login
     * @param string $password
     * @param $uccxIP
     */
    public function __construct(Environment $twig, RedisClient $redis, string $login, string $password, $uccxIP)
    {

        $this->login = $login;
        $this->password = $password;
        $this->twig = $twig;
        $this->uccxIP = $uccxIP;
        $this->redis = $redis;

        $serializeBuilder = SerializerBuilder::create();
        $serializeBuilder->setPropertyNamingStrategy(new \JMS\Serializer\Naming\IdenticalPropertyNamingStrategy());
        $this->serializer = $serializeBuilder->build();

    }

    /**
     * Ustawienie skilli użytkownikom
     *
     * @param array $users
     * @param array $skills
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setSkillsOfUsers(array $users, array $skills): array
    {

        $skillMap = $this->createSkillMap($skills);

        /** @var Resource[] $resources */
        $resources = $this->getResources($users);

        foreach ($resources as $resource) {

            $resource->setSkillMap($skillMap);

        }

        $mapResources = array_map(function ($resource) {

            /** @var Resource $resource */

            return [
                'username' => $resource->getUserID(),
                'xml' =>  $this->serializer->serialize($resource, 'xml')
            ];

        }, $resources);

        return $this->putAsyncResources($mapResources);

    }

    /**
     * Pobranie danych o podanych użytkownikach
     *
     * @param $users
     * @return array
     */
    public function getResources($users): array
    {

        // TODO - Add CACHING

        $resourcesObject = [];

        foreach ($this->getAsyncResources($users) as $index => $response) {

            if($response['success']) {

                $resourcesObject[] = $this->serializer->deserialize($response['response'], Resource::class, 'xml');

            }

        }

        return $resourcesObject;

    }

    /**
     * Pobranie danych o skill'u
     *
     * @param $skillId
     * @return Skill
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSkill($skillId): Skill
    {

        if(!isset($this->skills[$skillId])) {

            $xml = $this->redis->get(self::REDIS_SKILL_PREFIX . $skillId);

            if(empty($xml)) {
                $xml = $this->getSkillFromUCCX($skillId);
                $this->redis->setex(self::REDIS_SKILL_PREFIX . $skillId, 86400, $xml);
            }

            $this->skills[$skillId] = $this->serializer->deserialize($xml, Skill::class, 'xml');

        }

        return $this->skills[$skillId];

    }

    /**
     * Pobiera status użytkowników telefoni
     *
        - loggedIn
        - ready
        - notReady
        - talking
     *
     * @return null|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAgentStats() {

        $xml = $this->redis->get(self::REDIS_AGENT_STATS);

        if(empty($xml)) {
            $xml = $this->getAgentStatsFromUCCX();
            $this->redis->setex(self::REDIS_AGENT_STATS, 5, $xml);
        }

        return $this->serializer->deserialize($xml, AgentStats::class, 'xml');

    }

    /**
     * @param $queueId
     * @return null|Queue
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getQueue($queueId) {

        $client = $this->getClient();
        $baseUrl = $this->getBaseUrl('csq');
        $response =  $client->send(new Request('GET', $baseUrl . $queueId));

        if($response->getStatusCode() === 200) {
            $xml = $response->getBody()->getContents();
            return $this->serializer->deserialize($xml, Queue::class, 'xml');
        }

        return null;

    }

    /**
     * @return null|Queues
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getQueues() {

        $client = $this->getClient();
        $baseUrl = $this->getBaseUrl('csq');
        $response =  $client->send(new Request('GET', $baseUrl));

        if($response->getStatusCode() === 200) {
            $xml = $response->getBody()->getContents();
            return $this->serializer->deserialize($xml, Queues::class, 'xml');
        }

        return null;

    }

    /**
     * @return null|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setSkillsOfQueue($queueId, array $skills) {

        $skillMap = $this->createSkillMap($skills);

        /** @var Queue $queue*/
        $queue = $this->getQueue($queueId);

        if(!$queue) {
            throw new NotFoundResourceException('Not found queue.');
        }

        $queue->getPoolSpecificInfo()->getSkillGroup()->setSkillCompetency($skillMap);

        $client = $this->getClient();
        $baseUrl = $this->getBaseUrl('csq');

        $xml = $this->serializer->serialize($queue, 'xml');

        return  $client->send(new Request('PUT', $baseUrl . $queueId, [
            'Content-Type' => 'application/xml'
        ], $xml));

    }

    /**
     * Stworzenie nowej kolejki - tylko dla testów
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createQueue() {

        die('Only for test');

        $skill = $this->getSkill(157);

        $queue = new Queue();
        $queue->setName('TEST_QUEUE');
        $queue->setQueueType('VOICE');
        $queue->setQueueAlgorithm('FIFO');
        $queue->setAutoWork(true);
        $queue->setWrapupTime(1);
        $queue->setResourcePoolType('SKILL_GROUP');
        $queue->setServiceLevel(5);
        $queue->setServiceLevelPercentage(70);

        $skillCompetency = new skillCompetency();
        $skillCompetency->setSkillNameUriPair($skill->getSkillNameUriPar());
        $skillCompetency->setCompetencelevel(5);
        $skillCompetency->setWeight(5);

        $skillsGroup = new skillGroup();
        $skillsGroup->setSelectionCriteria('Longest Available');
        $skillsGroup->addSkillCompetency($skillCompetency);

        $poolSpecificInfo = new poolSpecificInfo();
        $poolSpecificInfo->setSkillGroup($skillsGroup);

        $queue->setPoolSpecificInfo($poolSpecificInfo);
        $xml = $this->serializer->serialize($queue, 'xml');

        $client = $this->getClient();

        $response = $client->send(new Request('POST', $this->getBaseUrl('csq'), [
            'Content-Type' => 'application/xml'
        ], $xml));

        // TODO - pobrać i zwrócić ID stworzonej kolejki

//        "Location" => array:1 [
//            0 => "http://10.10.77.101/adminapi/csq/206"
//        ]

    }

    /**
     * @param $urlName
     * @param string $method
     * @param string $suffixUrl
     * @return null|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function customRequest($urlName, $method = 'GET', $suffixUrl = '') {

        $client = $this->getClient();
        $baseUrl = $this->getBaseUrl($urlName);

        $response =  $client->send(new Request($method, $baseUrl . $suffixUrl));

        if($response->getStatusCode() === 200) {
            return $response->getBody()->getContents();
        }

        return null;

    }

    /**
     * @param $skills
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function createSkillMap($skills) {

        $skillsObject = [];

        foreach ($skills as $skillData) {

            $skill = $this->getSkill($skillData['id']);

            $skillCompetency = new skillCompetency();
            $skillCompetency->setSkillNameUriPair($skill->getSkillNameUriPar());

            if(!empty($skillData['priority'])) {
                $skillCompetency->setCompetencelevel($skillData['priority']);
            }

            if(!empty($skillData['weight'])) {
                $skillCompetency->setWeight($skillData['weight']);
            }

            $skillsObject[] = $skillCompetency;

        }

        return $skillsObject;

    }

    /**
     * @return null|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getAgentStatsFromUCCX() {

        $client = $this->getClient();
        $baseUrl = $this->getBaseUrl('agentstats');

        $response =  $client->send(new Request('GET', $baseUrl));

        if($response->getStatusCode() === 200) {
            return $response->getBody()->getContents();
        }

        return null;

    }

    /**
     * @param array $userNames
     * @return array
     */
    private function getAsyncResources(array $userNames) {

        $result = [];

        $client = $this->getClient();
        $baseUrl = $this->getBaseUrl('resource');

        $requests = function ($userNames) use($baseUrl) {

            foreach ($userNames as $key => $userName) {
                yield new Request('GET', $baseUrl . $userName);
            }

        };

        $pool = new Pool($client, $requests($userNames), [
            'concurrency' => 5,
            'fulfilled' => function ($response, $index) use (&$result) {

                /** @var Response $response */

                $result[$index] = [
                    'success' => true,
                    'statusCode' => $response->getStatusCode(),
                    'response' => $response->getBody()->getContents()
                ];

            },
            'rejected' => function ($reason, $index) {

                /** @var ClientException $reason */

                $result[$index] = [
                    'success' => false,
                    'code' => $reason->getCode(),
                    'reason' => $reason->getMessage()
                ];

            },
        ]);

        $promise = $pool->promise();
        $promise->wait();

        return $result;

    }

    /**
     * @param array $resources
     * @return array
     */
    private function putAsyncResources(array $resources) {

        $result = [];

        $client = $this->getClient();
        $baseUrl = $this->getBaseUrl('resource');

        $requests = function ($resources) use($baseUrl) {

            foreach ($resources as $key => $resource) {
                yield new Request('PUT', $baseUrl . $resource['username'], [
                    'Content-Type' => 'application/xml'
                ], $resource['xml']);
            }

        };

        $pool = new Pool($client, $requests($resources), [
            'concurrency' => 5,
            'fulfilled' => function ($response, $index) use (&$result) {

                /** @var Response $response */

                $result[$index] = [
                    'success' => true,
                    'statusCode' => $response->getStatusCode(),
                    'response' => $response->getBody()->getContents()
                ];

            },
            'rejected' => function ($reason, $index) {

                /** @var ClientException $reason */

                $result[$index] = [
                    'success' => false,
                    'code' => $reason->getCode(),
                    'reason' => $reason->getMessage()
                ];

            },
        ]);

        $promise = $pool->promise();
        $promise->wait();

        return $result;

    }

    /**
     * @param $skillId
     * @return string|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getSkillFromUCCX($skillId) {

        $client = $this->getClient();
        $baseUrl = $this->getBaseUrl('skill');

        $response =  $client->send(new Request('GET', $baseUrl . $skillId));

        if($response->getStatusCode() === 200) {
            return $response->getBody()->getContents();
        }

        return null;

    }

    /**
     * @return Client
     */
    private function getClient() {

        return new Client([
            'auth' => [$this->login, $this->password],
            'headers' => [
                'Content-Type' => 'application/xml'
            ]
        ]);
    }

    /**
     * @param $name
     * @return string
     */
    private function getBaseUrl($name) {

        return 'http://' . $this->uccxIP . '/adminapi/'.$name.'/';

    }

    public function toArray($object) {
        return $this->serializer->toArray($object);
    }
}