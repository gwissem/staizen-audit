<?php
namespace AppBundle\Service;

use AppBundle\Entity\InsuranceDataImporter;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use CaseBundle\Entity\Dictionary;
use CaseBundle\Entity\DictionaryHeader;
use CaseBundle\Entity\DictionaryPackage;
use CaseBundle\Entity\Program;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use JMS\JobQueueBundle\Entity\Job;
use PhpImap\Exception;
use Realestate\MssqlBundle\Driver\PDODblib\Connection;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ToolBundle\Utils\ToolQueryManager;

class ImportInsuranceDataService
{

    const MAX_ROWNUMBER = 5000;

    /** @var  ContainerInterface */
    protected $container;

    /** @var  EntityManagerInterface */
    protected $em;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public function process($packageId, $fileId, $importerId, OutputInterface $output)
    {
        set_time_limit(0);

        $em = $this->em;
        $connection = $em->getConnection();
        $dictionaryRepository = $em->getRepository('CaseBundle:Dictionary');
        $package = $em->getRepository(DictionaryPackage::class)->find($packageId);

        if (!empty($fileId)) {
            $file = $em->getRepository('DocumentBundle:File')->find($fileId);
            $this->processFile($connection, $dictionaryRepository, $package, $file, $output);
        } elseif (!empty($importerId)) {
            $importer = $em->getRepository('AppBundle:InsuranceDataImporter')->find($importerId);
            if ($importer) {
                if ($importer->getActive()) {
                    $this->processImporter($connection, $package, $importer, $output);
                }
            }
        }
    }

    public function processFile($connection, $dictionaryRepository, $package, $file, $output)
    {

        $filePath = $this->container->getParameter('kernel.root_dir').'/..'.$file->getPath();
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($filePath);

        $headers = [];
        $processedRow = 0;
        $processedColumn = 0;
        try {
            foreach ($reader->getSheetIterator() as $sheetIndex => $sheet) {
                if ($sheetIndex == 1) {
                    $rowNumber = 0;
                    foreach ($sheet->getRowIterator() as $rowIndex => $row) {
                        ++$processedRow;
                        if (count(array_values(array_filter($row))) > 0) {
                            $rowNumber++;
                            $this->insertHeader($connection, $package->getId());
                            $headerId = $connection->lastInsertId();
                            $processedColumn = 0;
                            foreach ($row as $column => $cell) {
                                ++$processedColumn;
                                $cell = is_string($cell) ? trim($cell) : $cell;
                                if ($rowIndex == 1 && $cell != '') {
                                    $dictionary = $dictionaryRepository->findOneBy(
                                        ['package' => $package, 'description' => $cell]
                                    );
                                    if (!$dictionary) {
                                        throw new EntityNotFoundException();
                                    }
                                    $headers[$column] = $dictionary->getId();
                                } elseif (array_key_exists($column, $headers)) {
                                    $value = $cell instanceof \DateTime ? $cell->format('Y-m-d H:i:s') : $cell;
                                    $this->insertElement($connection, $headerId, $headers[$column], $value);
                                }
                            }
                        }
                    }
                }
            }
            $reader->close();
        } catch (EntityNotFoundException $e) {
            $this->displayErrorInfo($output, $processedRow, $processedColumn);
        } catch (\Exception $e) {
            $this->displayErrorInfo($output, $processedRow, $processedColumn);
            $connection->delete(
                'vin_header',
                [
                    'status' => DictionaryHeader::STATUS_NEW,
                ]
            );

            return $e->getMessage();
        }
    }

    protected function insertHeader($connection, $packageId)
    {
        $now = new \DateTime();
        $connection->insert(
            'vin_header',
            [
                'creation_date' => $now->format('Y-m-d H:i:s'),
                'status' => DictionaryHeader::STATUS_NEW,
                'package_id' => $packageId,
            ]
        );
    }

    protected function insertElement($connection, $headerId, $dictionaryId, $value)
    {
        if(is_string($value) && strlen($value) > 300){
            $values = str_split($value, 300);
            foreach($values as $value){
                $this->insertElement($connection, $headerId, $dictionaryId, $value);
            }
        }
        else {
            $connection->insert(
                'vin_element',
                [
                    'header_id' => $headerId,
                    'dictionary_id' => $dictionaryId,
                    'value' => $value,
                ]
            );
        }
    }

    protected function displayErrorInfo($output, $processedRow, $processedColumn)
    {
        $output->writeln('Error at line: '.$processedRow.', column: '.$processedColumn);
    }

    public function processImporter(
        \Doctrine\DBAL\Connection $connection,
        $package,
        InsuranceDataImporter $importer,
        $output
    )
    {

        //performance: do not log sql queries
        $connection->getConfiguration()->setSQLLogger(null);
        gc_enable();

        $this->setNextRunDate($importer);

        /** @var ToolQueryManager $queryManager * */
        $queryManager = $this->container->get('tool.query_manager');

        $baseQuery = $importer->getQuery();
        $connectionString = $importer->getConnectionString();

        try {

            // get Number of elements returned by query
            $rowNumber = $queryManager->rowCount($baseQuery, $connectionString);

            //divide elements by defined MAXROW
            $chunks = $this->chunkRowNumber($rowNumber);

            $headers = [];
            $rows = [];
            $packageId = $package->getId();

            foreach ($chunks as $i => $chunk) {
                $query = $queryManager->appendLimit(
                    $baseQuery,
                    $chunk['from'],
                    $chunk['to'] - $chunk['from'],
                    $connectionString
                );
                unset($rows);
                gc_collect_cycles();

                $rows = $queryManager->executeCustomQuery(
                    $query,
                    true,
                    $connectionString
                );

                foreach ($rows as $rowIndex => $row) {
                    $queryBulk = '';
                    $this->insertHeader($connection, $packageId);

                    $headerId = $connection->lastInsertId();
                    foreach ($row as $column => $value) {

                        if (!array_key_exists($column, $headers)) {
                            $dictionaryId = $connection->fetchColumn(
                                "SELECT id FROM vin_dictionary WHERE package_id = " . $packageId . " AND description = '" . $column . "'"
                            );
                            if ($dictionaryId) {
                                $headers[$column] = $dictionaryId ?: null;
                            }
                        } elseif ($headers[$column] != null) {

                            if ($value instanceof \DateTime) {
                                $value->format('Y-m-d H:i:s');
                            } elseif ($value === null) {
                                $value = '';
                            } elseif (is_string($value) && mb_strlen($value) > 300) {
                                $value = mb_substr($value, 0, 300);
                            }
                            $queryBulk .= $this->insertElementQuery($connection, $headerId, $headers[$column], $value);
                        }
                    }
                    if ($queryBulk != '') {
                        $connection->executeQuery($queryBulk);
                    }
                }
            }
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }

    }

    protected function setNextRunDate($importer)
    {
        $newDate = new \DateTime();
        $nextRunDate = $newDate->modify(
            '+' . $importer->getInterval() . ' seconds'
        );
        $importer->setNextRun($nextRunDate);
        $this->em->persist($importer);
        $this->em->flush();
    }

    protected function chunkRowNumber($rowNumber)
    {

        $divided = $rowNumber / self::MAX_ROWNUMBER;
        $chunks = [];
        for ($i = 0; $i < ceil($divided); $i++) {
            $chunks[$i]['from'] = $i * self::MAX_ROWNUMBER;
            $chunks[$i]['to'] = floor($divided) == $i ? $rowNumber : $chunks[$i]['from'] + self::MAX_ROWNUMBER;
        }

        return $chunks;
    }

    protected function insertElementQuery($connection, $headerId, $dictionaryId, $value)
    {
        return "INSERT INTO vin_element (header_id, dictionary_id, value) VALUES (" . $headerId . ", " . $dictionaryId . ", " . $connection->quote(
                $value
            ) . ");";
    }

    /**
     * @param InsuranceDataImporter $importer
     * @param ToolQueryManager $queryManager
     * @return array
     */
    public function fetchNewDictionariesForImporter(InsuranceDataImporter $importer, ToolQueryManager $queryManager)
    {
        $dictionaryRepository = $this->em->getRepository(Dictionary::class);
        $columns = $queryManager->fetchColumnsForCustomQuery($importer->getQuery(), $importer->getConnectionString());
        $dictionaries = [];
        foreach ($columns as $column) {
            $dictionaryExists = $dictionaryRepository->findOneBy(
                ['description' => $column, 'package' => $importer->getPackage()]
            );
            if (!$dictionaryExists) {
                $dictionaries[] = $column;
            }
        }

        return $dictionaries;
    }

    public function startCron($output)
    {
        try {
            $imports = $this->em->getRepository('AppBundle:InsuranceDataImporter')->fetchActiveImports();
            $currentDate = new \DateTime();
            foreach ($imports as $import) {
                $dependentJob = null;
                $package = $import->getPackage();
                if ($import->getNextRun() < $currentDate) {
                    $args = [
                        'packageId' => $package->getId(),
                        'fileId' => '',
                        'importerId' => $import->getId(),
                    ];

                    $packageOpenedJobs = $this->em->getRepository(
                        \AppBundle\Entity\Job::class
                    )->findOpenedJobsForRelatedEntity('vin:import', $package);


                    $nextRun = new \DateTime();
                    $job = new Job(
                        'vin:import',
                        $args
                    );

                    if ($packageOpenedJobs) {
                        $dependentJob = $packageOpenedJobs[0];
                        $job->addDependency($dependentJob);
                        //check if importer did not generate another job manually by form edit
                        if ($dependentJob->getArgs() === $job->getArgs()) {
                            $job->setState(Job::STATE_CANCELED);
                        }
                    }

                    $job->setExecuteAfter($nextRun);
                    $job->addRelatedEntity($package);
                    $job->addDependency($dependentJob);

                    $this->em->persist($job);
                    $this->em->flush();

                }
            }

            return $this->container->get('translator')->trans('Db imports have been successfully completed');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
