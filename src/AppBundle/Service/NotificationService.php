<?php
namespace AppBundle\Service;

use ApiBundle\Entity\ApiCall;
use ApiBundle\Entity\ApiCallJsonDefinition;
use ApiBundle\Entity\ApiCallType;
use AppBundle\Entity\Log;
use AppBundle\Utils\ArrayUtils;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\ServiceDefinition;
use CaseBundle\Entity\ServiceStatus;
use CaseBundle\Entity\ServiceStatusDictionary;
use CaseBundle\Entity\Step;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Service\CaseHandler;
use CaseBundle\Utils\Language;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use JMS\JobQueueBundle\Entity\Job;
use PDO;
use SimpleXMLElement;
use SocketBundle\Topic\AtlasTopic;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NotificationService
{


    /** @var  ContainerInterface */
    protected $container;

    /** @var  ContainerInterface */
    protected $em;

    /**
     * @param ContainerInterface $container
     * @param EntityManager $em
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public function sendToUser($userId, $message, $type, $timeout = 0){
        $pusher = $this->container->get('gos_web_socket.zmq.pusher');

        $data = [
            'action' => AtlasTopic::ACTION_NOTIFICATION,
            'data' => [
                'message' => $message,
                'type' => $type,
                'withIcon' => (isset($data['with-icon'])),
                'timeout' => $timeout,
                'users' => [$userId],
                'forAll' => false
            ]
        ];

        $pusher->push($data, 'atlas_main_topic');
    }

}
