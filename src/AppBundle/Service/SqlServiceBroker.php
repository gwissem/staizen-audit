<?php

namespace AppBundle\Service;

use AppBundle\Utils\ServiceBrokerMessage;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Connection as DriverConnection;
use MssqlBundle\PDO\PDO;

class SqlServiceBroker {


    /** @var  Connection */
    private $conn;

    protected $timeout = 5000;

    /**
     * SqlServiceBroker constructor.
     * @param DriverConnection $connection
     */
    public function __construct(DriverConnection $connection)
    {
        $this->conn = $connection;
    }

    public function updateConnection($connection) {
        $this->conn = $connection;
    }

    public function runLoop($queueName, callable $callback, $timeout = 5000, $withSleep = false) {

        $statement = $this->prepareStatement($queueName, $timeout);

        while (true) {

            $statement->execute();
            $results = $statement->fetchAll();

            if(count($results)) {
                if(is_callable($callback)) {

                    call_user_func($callback, new ServiceBrokerMessage($results[0]));

                }
            }

            unset($results);

//            echo "Current memory usage: " . number_format((memory_get_usage() / 1024 / 1024), 3) . "\n";

            if($withSleep) {
                usleep(100000); // 100ms
            }

        }

    }

    protected function prepareStatement($queueName, $timeout) {

        $query = 'WAITFOR (  
            RECEIVE TOP (1)
            service_name,
            CAST(message_body as XML) as message_body
          FROM '.$queueName.'
        ), TIMEOUT :timeout';

        $statement = $this->conn->prepare($query);
        $statement->bindParam(':timeout', $timeout,PDO::PARAM_INT);

        return $statement;

    }
}