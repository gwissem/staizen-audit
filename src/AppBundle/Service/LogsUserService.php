<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Log;

class LogsUserService {

    CONST CONTENT_LOGIN = 'login';
    CONST CONTENT_LOGOUT = 'logout';

    private $em;

    /**
     * LogsUserService constructor.
     * @param EntityManager $em
     * @param Log $logEntity
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createLoginLog($user = null)
    {
        if ($this->countLoginLog($user) != 0) {
            $log = $this->getLoginLog($user);
            if($log) {
                $log->setParam1($log->getParam1() + 1);
                $log->setUpdatedBy($user);
                $this->em->flush();
            }
        } else {
            $this->createNewLog($user, self::CONTENT_LOGIN);
        }

        if ($this->countLogoutLog($user) != 0) {
            $log = $this->getLogoutLog($user);
            if($log) {
                $this->em->remove($log);
                $this->em->flush();
            }
        }

    }

    public function createLogoutLog($user = null)
    {
        $log = $this->getLoginLog($user);
        $log->setParam1($log->getParam1() + 1);
        $log->setUpdatedBy($user);
        $this->em->flush();

        $this->createNewLog($user, self::CONTENT_LOGOUT);
    }

    public function createNewLog($user, $content)
    {
        $log = new Log();
        $log->setName('USER_LOG');
        $log->setContent($content);
        $log->setCreatedBy($user);
        $this->em->persist($log);
        $this->em->flush();
    }

    public function getLoginLog($user)
    {
        return $this->em->getRepository(Log::class)->findLogOneHourAgo($user->getId(), self::CONTENT_LOGIN);
    }

    public function countLoginLog($user)
    {
        return $this->em->getRepository(Log::class)->countLogOneHourAgo($user->getId(),self::CONTENT_LOGIN);
    }

    public function getLogoutLog($user)
    {
        return $this->em->getRepository(Log::class)->findLogOneHourAgo($user->getId(),self::CONTENT_LOGOUT);
    }

    public function countLogoutLog($user)
    {
        return $this->em->getRepository(Log::class)->countLogOneHourAgo($user->getId(),self::CONTENT_LOGOUT);
    }
}