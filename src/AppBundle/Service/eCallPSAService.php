<?php

namespace AppBundle\Service;

use AppBundle\Utils\QueryManager;
use AppBundle\Utils\XmlIncomingToArrayHelper;
use CaseBundle\Entity\Attribute;
use CaseBundle\Entity\Platform;
use CaseBundle\Repository\AttributeRepository;
use CaseBundle\Service\ProcessHandler;
use CaseBundle\Utils\ParserMethods;
use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use OperationalBundle\Service\ApiTaskService;
use PDO;
use SimpleXMLElement;
use SocketBundle\Topic\AtlasTopic;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class eCallPSAService {

    const ECALL_PSA_START_STEP = '1200.001';
    const CALL_TYPE_ECALL = 'ECALL';

    /** @var QueryManager */
    private  $queryManager;

    /** @var PusherInterface */
    private $pusher;

    /** @var ProcessHandler */
    private $processHandler;

    /** @var EntityManager */
    private $entityManager;

    /** @var ApiTaskService */
    private $apiTaskService;

    private $lastId = null;

    private $isProdMode = false;

    /** @var OutputInterface|null */
    private $oi = null;

    /** @var Platform */
    private $ecallPSAPlatform = null;

    /** @var AttributeRepository */
    private $attributeRepository = null;

    /**
     *
     * eCallPSAService constructor.
     * @param QueryManager $queryManager
     * @param PusherInterface $pusher
     * @param ProcessHandler $processHandler
     * @param $atlasEnv
     * @param EntityManager $entityManager
     * @param ApiTaskService $apiTaskService
     */
    public function __construct(QueryManager $queryManager, PusherInterface $pusher, ProcessHandler $processHandler, $atlasEnv, EntityManager $entityManager, ApiTaskService $apiTaskService)
    {
        $this->queryManager = $queryManager;
        $this->pusher = $pusher;
        $this->processHandler = $processHandler;
        $this->entityManager = $entityManager;
        $this->apiTaskService = $apiTaskService;
        $this->setProdMode($atlasEnv);
    }

    public function getKeyId() {
        return ($this->isProdMode) ? "Id" : "id";
    }

    public function setProdMode($env) {
        $this->isProdMode = ($env === "prod");
    }

    public function setLogger(OutputInterface $oi) {
        $this->oi = $oi;
    }

    public function loop() {

        if($this->lastId === null) {
            $this->lastId = $this->getLastId();
        }

        foreach ($this->getXmlIncoming() as $xmlRow) {

            if($this->oi) {
                $this->oi->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' XML incoming...');
            }

            $this->manageXmlRow($xmlRow);
            $this->lastId = intval($xmlRow[$this->getKeyId()]);

        }

    }

    /**
     * @param $row
     */
    private function manageXmlRow($row) {

        $this->loadComponents();

        if($this->oi) {
            $this->oi->writeln('New case to manage. Event: ' . $row['EventType'] . ", id: " . $row[$this->getKeyId()]);
        }

        $xml = $row['Xml'];
        $xmlArray = json_decode(json_encode((array)simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?>' . $xml)),true);

        if(isset($xmlArray['calldescription'] ) && isset($xmlArray['calldescription']['calltype'])) {

            $callType = $xmlArray['calldescription']['calltype'];

            if($callType == self::CALL_TYPE_ECALL) {

                if ($row['EventType'] == 1) {

                    try {

                        $this->createCase($row, $xmlArray);

                    } catch (\Exception $exception) {

                        if($this->oi) {
                            $this->oi->writeln('Error in createCase ' . $row[$this->getKeyId()] . " " . $exception->getMessage() . ", line: " . $exception->getLine() . ", file: " . $exception->getFile());
                        }

                        if (!$this->isProdMode) {
                            $this->queryManager->executeProcedure("UPDATE log SET param3 = 'completed' WHERE id = " . $row['id'] . " AND name = 'XML_INCOMING_DEV'", [], false);
                        }

                    }

                } elseif ($row['EventType'] == 2) {

                    $this->updateCase($row);

                }

            }
            else {

                if($this->oi) {
                    $this->oi->writeln('Bad calltype. "' . $callType . '" is not supported.');
                }

            }

            unset($callType);

        }
        else {
            if($this->oi) {
                $this->oi->writeln('Invalid XML format.');
            }
        }

        unset($xml);
        unset($xmlArray);

    }

    private function loadComponents() {
        if($this->attributeRepository == null) {
            $this->attributeRepository = $this->entityManager->getRepository('CaseBundle:Attribute');
        }

        if($this->ecallPSAPlatform === null) {
            $this->ecallPSAPlatform = $this->entityManager->getRepository('CaseBundle:Platform')->find(77);
        }
    }

    private function updateCase($row) {

        $this->log('Update ' . $row['Ccid']);

        $rootId = $this->getLastRootIdByCcid($row['Ccid']);

        // WYSYŁA POWIADOMIENIA DO UŻYTKOWNIKA KTÓRY SIEDZI W TEJ SPRAWIE

        if($rootId) {
            $caseNumberText = ", numer sprawy: " . ParserMethods::formattingCaseNumber($rootId);

            $usersTasksInfo = $this->apiTaskService->getActiveUsersInCase($rootId);

            if(!empty($usersTasksInfo)) {

                $onlineUsersWithAccess = [];

                foreach ($usersTasksInfo as $userInfo) {
                    $onlineUsersWithAccess[] = intval($userInfo['user_id']);
                }

                if(!empty($onlineUsersWithAccess)) {

                    $this->pusher->push([
                        'action' => AtlasTopic::ACTION_NOTIFICATION,
                        'data' => [
                            'title' =>  "Aktualizacja zgłoszenia eCall PSA",
                            'tapToDismiss' => true,
                            'message' => "Nastąpiła aktualizacja zgłoszenia eCall PSA! Numer CCID: " . $row['Ccid'] . $caseNumberText . ". Proszę sprawdzić nowe dane w serwisie ARCTwin.",
                            'timeout' => 30000,
                            'withIcon' => true,
                            'type' => 'warning',
                            'users' => $onlineUsersWithAccess,
                            'extraClass' => 'ecall-notification',
                            'forAll' => false,
                            'closeButton' => true
                        ]
                    ], 'atlas_main_topic');

                }

                unset($onlineUsersWithAccess);
            }

            unset($caseNumberText);
            unset($usersTasksInfo);

        }
//        else {
            //        $onlineUsersWithAccess = $this->processHandler->getUsersWithAccessToStep(self::ECALL_PSA_START_STEP, $this->ecallPSAPlatform->getId(), 1);
//
//        $onlineUsersWithAccess = array_map(function ($ele) {
//            return intval($ele['id']);
//        }, $onlineUsersWithAccess);
//        }

        if(!$this->isProdMode) {
            $this->queryManager->executeProcedure("UPDATE log SET param3 = 'completed' WHERE id = " . $row['id'] . " AND name = 'XML_INCOMING_DEV'", [], false);
        }

        unset($rootId);

    }

    /**
     * @param $row
     * @param $xmlArray
     * @return null
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createCase($row, $xmlArray) {

        $this->log('Create ' . $row['Ccid']);

        if(isset($xmlArray['calldescription'] ) && isset($xmlArray['calldescription']['caller'])) {

            $bannedContact = $this->entityManager->getRepository('CaseBundle:BannedContact')->findBy([
                'contact' => $xmlArray['calldescription']['caller']
            ]);

//            if($array['calldescription']['caller'] == "+337000030169568") {
            if(!empty($bannedContact )) {

                if($this->oi) {
                    $this->oi->writeln('Caller is banned ' . $xmlArray['calldescription']['caller']  );
                }

                unset($bannedContact);

                return null;

            }
        }

        $xmlIncomingHelper = new XmlIncomingToArrayHelper('e_call_psa', $this->processHandler);

        $arrayData = $xmlIncomingHelper->mapData($xmlArray);

        $processInstanceId = $this->processHandler->start(self::ECALL_PSA_START_STEP);
        $this->processHandler->formControls($processInstanceId, null, 0);

        $this->processHandler->editAttributeValue('654,373', $processInstanceId, $row['Ccid'], 'int', 'xxx', 1, 1);
        $this->processHandler->editAttributeValue('253', $processInstanceId, $this->ecallPSAPlatform->getId(), 'int', 'xxx', 1, 1);

        $arrayData['make_model'] = [
            'nodeName' => 'make_model',
            'path' => '654,73',
            'value' => $this->tryGetMakeModel($arrayData)
        ];

        foreach ($arrayData as $arrayDatum) {

            $arr = explode(',', $arrayDatum['path']);
            $attribute = $this->attributeRepository->find(intval(end($arr)));
            $this->processHandler->editAttributeValue($arrayDatum['path'], $processInstanceId, $arrayDatum['value'], $attribute->getNameValueType(), 'xxx', 1, 1);

        }

        $processProperty = $this->entityManager->getRepository('CaseBundle:ProcessInstanceProperty')->find($processInstanceId);
        $processProperty->setPlatform($this->ecallPSAPlatform);
        $processProperty->setPriority(1);
        $this->entityManager->persist($processProperty);
        $this->entityManager->flush();

        if(!$this->isProdMode) {
            $this->queryManager->executeProcedure("UPDATE log SET param3 = 'completed' WHERE id = " . $row['id'] . " AND name = 'XML_INCOMING_DEV'", [], false);
        }

        $onlineUsersWithAccess = $this->processHandler->getUsersWithAccessToStep(self::ECALL_PSA_START_STEP, $this->ecallPSAPlatform->getId(), 1);

        if(!empty($onlineUsersWithAccess)) {

            $onlineUsersWithAccess = array_map(function ($ele) {
                return intval($ele['id']);
            }, $onlineUsersWithAccess);


//           </br></br><button data-process-instance-id='".$processInstanceId."' class='btn btn-green-meadow' onclick='_openCase(this);'>Przejdź do zadania</button>

            $this->pusher->push([
                'action' => AtlasTopic::ACTION_NOTIFICATION,
                'data' => [
                    'title' =>  "Nowe zgłoszenie eCall PSA",
                    'tapToDismiss' => false,
                    'message' => "Przyszło nowe zgłoszenie dla eCall PSA! Numer sprawy: " . ParserMethods::formattingCaseNumber($processInstanceId),
                    'timeout' => 60000,
                    'withIcon' => true,
                    'type' => 'info',
                    'users' => $onlineUsersWithAccess,
                    'extraClass' => 'ecall-notification notification-id-' . $processInstanceId,
                    'forAll' => false,
                    'closeButton' => true,
                    'callback' => [
                        'type' => 'getTask',
                        'id' => $processInstanceId
                    ]
                ]
            ], 'atlas_main_topic');

        }


        unset($xmlIncomingHelper, $arrayData, $onlineUsersWithAccess, $processInstanceId, $processProperty);

    }

    private function tryGetMakeModel($arrayData) {

        if(isset($arrayData['model']) && isset($arrayData['make'])) {

            $typeD = 'makeModel';
            $textD = $arrayData['make']['value'] . " " . str_replace(' ', '-', $arrayData['model']['value']);

            $result = $this->processHandler->queryManager->executeProcedure("SELECT value FROM dictionary WHERE typeD = '" . $typeD . "' AND active = 1 AND textD = '" . $textD . "'");

            if (!empty($result)) {
                return $result[0]['value'];
            }

        }

        return null;

    }

    /**
     * @throws \Exception
     */
    private function getLastId() {

        $output = $this->queryManager->executeProcedure('EXEC [dbo].[p_getXmlIncoming] @prodMode = :prodMode', [
            [
                'key' => 'prodMode',
                'value' => ($this->isProdMode) ? 1 : 0,
                'type' => PDO::PARAM_INT
            ]
        ]);

        if(empty($output)) {
            throw new \Exception("Nie znalazło żadnego XML'a w bazie!");
        }

        if($this->oi) {
            $this->oi->writeln('Last ccid: ' . intval($output[0]['Ccid']));
            $this->oi->writeln('Last id: ' . intval($output[0][$this->getKeyId()]));
        }

        return intval($output[0][$this->getKeyId()]);

    }

    /**
     * @param $ccid
     * @return int|null
     */
    private function getLastRootIdByCcid($ccid) {

        $output = $this->queryManager->executeProcedure('EXEC [dbo].[p_getXmlIncoming] @lastId = :lastId, @prodMode = :prodMode, @findRootId = 1', [
            [
                'key' => 'lastId',
                'value' => (int)$ccid,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'prodMode',
                'value' => ($this->isProdMode) ? 1 : 0,
                'type' => PDO::PARAM_INT
            ]
        ]);

        if(!empty($output)) {
            return $output[0]['rootId'];
        }

        return null;

    }

    private function getXmlIncoming() {

        $this->queryManager->reConnect();

        return $this->queryManager->executeProcedure('EXEC [dbo].[p_getXmlIncoming] @lastId = :lastId, @prodMode = :prodMode', [
            [
                'key' => 'lastId',
                'value' => (int)$this->lastId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'prodMode',
                'value' => ($this->isProdMode) ? 1 : 0,
                'type' => PDO::PARAM_INT
            ]
        ]);

    }

    private function log($message) {
        if(!$this->isProdMode && $this->oi) {
            $this->oi->writeln($message);
        }
    }

}