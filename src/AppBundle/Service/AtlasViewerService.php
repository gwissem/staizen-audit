<?php

namespace AppBundle\Service;

use AppBundle\Form\Type\CareFleetAuthorizationType;
use AppBundle\Form\Type\ExtendRentalType;
use AppBundle\Form\Type\GCMType;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Service\NoteHandler;
use CaseBundle\Service\ProcessHandler;
use CaseBundle\Utils\ParserMethods;
use Doctrine\ORM\EntityManagerInterface;
use PDO;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Util\StringUtil;
use UserBundle\Entity\User;


class  AtlasViewerService
{

    const PLATFORM_CAREFLEET = 78;
    const PLATFORMS_RDA = [84,85,86];

    // COMPANY KEYS

    const COMPANY_KEY_STARTER = 'STARTER';
    const COMPANY_KEY_RDA = 'RDA';
    const COMPANY_KEY_PZU = 'PZU';
    const COMPANY_KEY_PZU_SA = 'PZU_SA';
    const COMPANY_KEY_PZU_POMOC = 'PZU_POMOC';

    /** @var  EntityManagerInterface */
    protected $em;

    /**
     * @var ProcessHandler
     */
    protected $processHandler;

    /** @var FormFactory */
    protected $formFactory;

    /** @var NoteHandler */
    protected $noteHandler;

    /** @var ParserMethods*/
    protected $parserMethods;

    public $lastPlatform;

    /**
     * @param $companyKey
     * @return bool
     */
    public static function companyIsPZU($companyKey): bool
    {

        return in_array($companyKey, [
            self::COMPANY_KEY_PZU,
            self::COMPANY_KEY_PZU_SA,
            self::COMPANY_KEY_PZU_POMOC
        ], true);

    }

    /**
     * AtlasViewerService constructor.
     * @param EntityManagerInterface $em
     * @param ProcessHandler $processHandler
     * @param FormFactory $formFactory
     * @param NoteHandler $noteHandler
     * @param ParserMethods $parserMethods
     */
    public function __construct(EntityManagerInterface $em, ProcessHandler $processHandler, FormFactory $formFactory, NoteHandler $noteHandler, ParserMethods $parserMethods)
    {
        $this->em = $em;
        $this->processHandler = $processHandler;
        $this->formFactory = $formFactory;
        $this->noteHandler = $noteHandler;
        $this->parserMethods = $parserMethods;
    }

    /**
     * Funkcja sprawdza, czy dla tej grupy i serwisu ma być wygenerowany jakiś Formularz,
     * jak tak to zwraca jego CLASS NAME
     *
     * @param ProcessInstance $groupProcess
     * @param null $serviceId
     * @return bool|string
     */
    public function isFormForGenerator(ProcessInstance $groupProcess, $serviceId = null) {

        $this->lastPlatform = $this->processHandler->getRootPlatform($groupProcess->getRoot()->getId());

        if($this->lastPlatform->getId() === self::PLATFORM_CAREFLEET) {

            if($serviceId == 3) {

                if($this->processHandler->canExtendRental($groupProcess->getId())) {

                    return ExtendRentalType::class;

                }

            }

//            return CareFleetAuthorizationType::class;

        }
        else if(in_array($this->lastPlatform->getId(), self::PLATFORMS_RDA)) {

            if($serviceId == 30) {

                return GCMType::class;

            }

        }

        return false;

    }

    /**
     * Obsługa zapisywania formularza - zapisanie danych do bazy
     *
     * @param $type
     * @param FormInterface $form
     * @param User $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveForm($type, FormInterface $form, User $user) {

        $data = $form->getData();
        if(empty($data)) return;

        if($type === CareFleetAuthorizationType::class) {

            $this->processHandler->setAttributeValue(
                [CareFleetAuthorizationType::PATH_CODE_AUTHORIZATION => null],
                $data['code'],
                AttributeValue::VALUE_STRING,
                'xxx',
                $data['groupProcessId'], $user->getId(), $user->getId()
                );

        }
        elseif($type === ExtendRentalType::class) {

            $groupProcessInstance = $this->em->getRepository(ProcessInstance::class)->find($data['groupProcessId']);

            $this->processHandler->editAttributeValue(
                ExtendRentalType::PATH_EXTEND_RENTAL_DAYS,
                $groupProcessInstance->getId(),
                $data['days'],
                AttributeValue::VALUE_INT,
                Step::ALWAYS_ACTIVE, $user->getId(), $user->getId()
            );

            $rootNote = $this->em->getRepository('CaseBundle:AttributeValue')->findOneBy([
                'rootProcess' => $groupProcessInstance->getRoot()->getId(),
                'attributePath' => '406'
            ]);

            $endDate = $this->parserMethods->rentalSummary($groupProcessInstance->getId(), 'end_date');
            $endDate = $this->parserMethods->showDatetime($endDate, 1);

            $note = [
                'type' => 'text',
                'special' => 1,
                'content' => "Wprowadzono przedłużenie dla usługi Auto Zastępcze (" . $groupProcessInstance->getId() . "). <br>Wynajem zostanie przedłużony dnia " . $endDate . " o " . $data['days'] . " dni.",
                'direction' => 1
            ];

            $this->noteHandler->newNote($note, $rootNote, $groupProcessInstance->getRoot()->getId());

            $this->processHandler->afterExtendRental($groupProcessInstance->getId());

        }
        elseif($type === GCMType::class) {

            $groupProcessInstance = $this->em->getRepository(ProcessInstance::class)->find($data['groupProcessId']);

            $this->processHandler->editAttributeValue(
                GCMType::PATH_GCM,
                $groupProcessInstance->getId(),
                $data['isGCM'],
                AttributeValue::VALUE_INT,
                Step::ALWAYS_ACTIVE, $user->getId(), $user->getId()
            );

        }
    }

    /**
     * Wyświetlenie extra notatki pod formularzem
     *
     * @param ProcessInstance $groupInstance
     * @param $type
     * @return string
     */
    public function getExtraContentForForm(ProcessInstance $groupInstance, $type) {

        if($type === ExtendRentalType::class) {

            $days = $this->processHandler->getAttributeValue(ExtendRentalType::PATH_EXTEND_RENTAL_DAYS, $groupInstance->getId(), AttributeValue::VALUE_INT);

            if(!empty($days)) {

                $endDate = $this->parserMethods->rentalSummary($groupInstance->getId(), 'end_date');
                $endDate = $this->parserMethods->showDatetime($endDate, 1);

                return '<div class="alert alert-info">Wynajem zostanie przedłużony dnia <strong>'.$endDate.'</strong></div>';
            }

        }

        return '';

    }

    /**
     * Stworzenie formularza - np uzupełenienie go danymi
     *
     * @param ProcessInstance $groupInstance
     * @param $type
     * @param null $data
     * @param array $options
     * @return FormInterface
     */
    public function createFormForService(ProcessInstance $groupInstance, $type, $data = null, $options = []) {

        if($type === CareFleetAuthorizationType::class) {

            $code = $this->processHandler->getAttributeValue(CareFleetAuthorizationType::PATH_CODE_AUTHORIZATION, $groupInstance->getId(), AttributeValue::VALUE_STRING);

            $data = [
                'groupProcessId' => $groupInstance->getId(),
                'code' => $code
            ];

        }
        elseif($type === ExtendRentalType::class) {

            $days = $this->processHandler->getAttributeValue(ExtendRentalType::PATH_EXTEND_RENTAL_DAYS, $groupInstance->getId(), AttributeValue::VALUE_INT);

            $data = [
                'groupProcessId' => $groupInstance->getId(),
                'days' => $days
            ];

        }
        elseif($type === GCMType::class) {

            $isGCM = $this->processHandler->getAttributeValue(GCMType::PATH_GCM, $groupInstance->getId(), AttributeValue::VALUE_INT);

            $data = [
                'groupProcessId' => $groupInstance->getId(),
                'isGCM' => $isGCM
            ];

        }

        $name = StringUtil::fqcnToBlockPrefix($type) . "_" . $groupInstance->getId();
        $form = $this->createForm($name, $type, $data, $options);

        return $form ;

    }

    protected function createForm($name, $type, $data = null, array $options = array())
    {
        return $this->formFactory->createNamed($name, $type, $data, $options);
    }

    /**
     * @param $userId
     * @param $companyKey
     * @return bool|mixed
     */
    public function canExportData($userId, $companyKey) {

        $parameters = [
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'companyKey',
                'value' => $companyKey,
                'type' => PDO::PARAM_STR
            ]
        ];

        $output = $this->processHandler->queryManager->executeProcedure(
            'SELECT [dbo].[A_VIEWER_canExportData](:userId, :companyKey) as canExport',
            $parameters
        );

        if(!empty($output) && !empty($output[0]['canExport'])) {
            return filter_var($output[0]['canExport'], FILTER_VALIDATE_BOOLEAN);
        }

        return false;

    }

    /**
     * @param $platformId
     * @param $userId
     * @return bool|mixed
     */
    public function isChatEnabled($platformId, $userId) {

        $parameters = [
            [
                'key' => 'platformId',
                'value' => $platformId,
                'type' => PDO::PARAM_INT
            ],
            [
                'key' => 'userId',
                'value' => $userId,
                'type' => PDO::PARAM_INT
            ]
        ];

        $output = $this->processHandler->queryManager->executeProcedure(
            'SELECT [dbo].[A_VIEWER_chatIsEnabled](:platformId, :userId) as chatIsEnabled',
            $parameters
        );

        if(!empty($output) && !empty($output[0]['chatIsEnabled'])) {
            return filter_var($output[0]['chatIsEnabled'], FILTER_VALIDATE_BOOLEAN);
        }

        return false;

    }

}