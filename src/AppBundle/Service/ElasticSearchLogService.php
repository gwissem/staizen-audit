<?php

namespace AppBundle\Service;

use AppBundle\Utils\QueryManager;
use FOS\ElasticaBundle\Configuration\ConfigManager;
use FOS\ElasticaBundle\Elastica\Index;
use FOS\ElasticaBundle\Index\MappingBuilder;
use Monolog\Handler\ElasticSearchHandler;
use Monolog\Logger;
use SimpleXMLElement;
use Symfony\Component\Console\Output\OutputInterface;

class ElasticSearchLogService {

    /** @var  Logger */
    private $logger;

    private $host = '10.10.77.154';

    private $port = '9200';

    private $env = null;

    /** @var \Elastica\Client */
    private $client;

    /** @var Index */
    private $elasticaIndex;

    /** @var string */
    private $currentIndex;

    /** @var ConfigManager */
    private $configManager;

    /** @var MappingBuilder */
    private $mappingBuilder;

    private $wasError = false;

    private $elasticaIsActive = false;

    /**
     * ElasticSearchLogService constructor.
     * @param $env
     * @param Index $elasticaIndex
     * @param ConfigManager $configManager
     * @param MappingBuilder $mappingBuilder
     * @throws \Exception
     */
    public function __construct($env, Index $elasticaIndex, ConfigManager $configManager, MappingBuilder $mappingBuilder)
    {

        $this->elasticaIndex = $elasticaIndex;
        $this->configManager = $configManager;
        $this->mappingBuilder = $mappingBuilder;
        $this->env = $env;

        try {

            $config = [
                'host' => $this->host,
                'port' => $this->port
            ];

            $this->currentIndex = $this->getIndexName();

            $this->client = new \Elastica\Client($config);

            $this->elasticaIndex->overrideName($this->currentIndex);

            $indexExists = $this->elasticaIndex->exists();

            if(!$indexExists) {
                $this->createMapping();
            }

            $options = array(
                'index' => $this->currentIndex,
                'type' => 'atlas_logger',
            );

            $this->setLogger($options);

            $this->elasticaIsActive = true;

        }
        catch (\Exception $exception) {

        }

    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getIndexName() {
        return 'atlas-elastica-' . $this->env . '-' . (new \DateTime())->format('Y.m.d');
    }

    private function createMapping() {

        $indexConfig = $this->configManager->getIndexConfiguration('atlas_logger');
        $mapping = $this->mappingBuilder->buildIndexMapping($indexConfig);
        $this->elasticaIndex->create($mapping, false);

    }

    /**
     * @param OutputInterface|null $output
     * @throws \Exception
     */
    public function checkChangeIndex(OutputInterface $output = null) {

        $newIndex = $this->getIndexName();

        if($this->currentIndex !== $newIndex) {

            $this->changeIndex($newIndex);
            $this->currentIndex = $newIndex;

            if($output !== null) {
                $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' ChangeIndex - ' . $newIndex);
            }

        }

        unset($newIndex);

    }

    private function changeIndex($newIndex) {

        $this->elasticaIndex->overrideName($newIndex);

        $this->createMapping();

        $options = array(
            'index' => $newIndex,
            'type' => 'atlas_logger',
        );

        $this->setLogger($options);

    }

    private function setLogger($options) {

        $handler = new ElasticSearchHandler($this->client, $options);
        $this->logger = new Logger('atlas_logger');
        $this->logger->pushHandler($handler);

    }

    public function log($message, $context, $level = 'INFO') {
        if($this->elasticaIsActive) {
            $this->logger->log($level, $message, $context);
        }
    }

    public function reconnectClient() {

        if(!$this->client->hasConnection()) {
            $this->client->connect();
        }

    }

    /**
     * @param QueryManager $queryManager
     * @param OutputInterface|null $output
     * @throws \Exception
     */
    public function LoopMssqlLog(QueryManager $queryManager, OutputInterface $output = null) {

        try {

            if($this->wasError) {
                $queryManager->reConnect();
            }

            $this->reconnectClient();

            $this->wasError = false;

            $logData = [];

            $logData = array_merge($logData, $this->getServerDiagnostics($queryManager));
            $logData = array_merge($logData, $this->getMssqlAtlasMonitor($queryManager));

            $this->log('mssql_monitor', $logData);

            unset($logData);

        }
        catch (\Exception $exception) {

            if($output) {
                $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' ERROR. Exception message: ' . $exception->getMessage());
            }

            $this->wasError = true;

            sleep(10);

        }

    }

    private function getMssqlAtlasMonitor(QueryManager $queryManager) {

        $output = $queryManager->executeProcedure('EXEC dbo.P_mssql_atlas_monitor');

        $logData = [];

        foreach ($output as $item) {
            $logData[$item['name']] = $item['value'];
        }

        unset($output);

        return $logData;

    }

    private function getServerDiagnostics(QueryManager $queryManager) {

        $output = $queryManager->executeProcedure('EXEC sp_server_diagnostics');

        $data = [];

        foreach ($output as $key => $item) {

            $data[$item['component_name']] = [];

            $xml = new SimpleXMLElement($item['data']);

            if($xml->attributes()->count()) {

                foreach ($xml->attributes() as $attributeName => $attribute) {
                    $data[$item['component_name']][$attributeName] = strval($attribute);
                }

            }

            unset($xml);

        }

        $logData = [];

        $this->prepareLogArray($data, 'mssql_monitor', $logData);

        // TODO - Pobierać tylko pola zmappowane w configu
        $availableLogs = [
            'mssql_monitor.system.sqlCpuUtilization',
            'mssql_monitor.system.systemCpuUtilization',
            'mssql_monitor.query_processing.pendingTasks'
        ];

        foreach ($logData as $key => $item) {
            if(!in_array($key, $availableLogs)) {
                unset($logData[$key]);
            }
        }

        unset($availableLogs, $data, $output);

        return $logData;
    }

    private function prepareLogArray($array, $name, &$result) {

        foreach ($array as $key => $item) {
            if(is_array($item)) {
                $this->prepareLogArray($item, ($name . '.' . $key), $result);
            }
            else {
                $result[$name . '.' . $key] = $item;
            }
        }

    }

}