<?php
namespace AppBundle\Service;

use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;
use Box\Spout\Writer\Style\StyleBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CasesToXlsService
{

    /** @var  ContainerInterface */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $rows
     * @param $headers
     * @param null $fileName
     * @param array $options
     * @return null|\Symfony\Component\HttpFoundation\Response
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Exception
     */
    public function generateXlsResponse($rows, $headers, $fileName = null, $options = [])
    {

        set_time_limit(0);
        ini_set('memory_limit', '2G');
        $fileType = Type::XLSX;

        if(!$fileName) {
            $fileName = uniqid(rand(), true);
        }

        /** Tworzenie XLS */
        $spout = $this->container->get('stev_spout.box_spout');

        $spoutWriter = $spout->initWriter($fileType);
        $spoutWriter->setShouldUseInlineStrings(true);

        $headerBorder = (new BorderBuilder())
            ->setBorderBottom('e7ecf1',Border::WIDTH_THIN,Border::STYLE_SOLID)
            ->setBorderLeft('e7ecf1',Border::WIDTH_THIN,Border::STYLE_SOLID)
            ->setBorderRight('e7ecf1',Border::WIDTH_THIN,Border::STYLE_SOLID)
            ->build();

        $styleHeaderColumns = (new StyleBuilder())
            ->setFontBold()
            ->setFontColor('FFFFFF')
            ->setBackgroundColor('3B3F51')
            ->setBorder($headerBorder)
            ->setShouldWrapText(false)
            ->build();

        $spoutWriter->openToBrowser($fileName . '.' . $fileType);

        try{

            $spoutWriter->addRowWithStyle($headers, $styleHeaderColumns);

            foreach ($rows as $row):
                foreach ($row as $key =>$cell ):
                    $row[$key] = strip_tags(preg_replace('#<br\s*/?>#i', "\n", $cell));
                endforeach;

                $spoutWriter->addRow($row);

            endforeach;

            $spoutWriter->close();
            exit();

        } catch (\Exception $exception) {

            $spoutWriter->close();

            throw $exception;

        }

    }

}
