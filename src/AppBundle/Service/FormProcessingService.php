<?php
namespace AppBundle\Service;

use Symfony\Component\Form\Form;

class FormProcessingService
{

    public function getFormErrors(Form $form)
    {
        $errors = $this->processFormErrors($form);
        return $this->arrayFlatten($errors);
    }

    protected function processFormErrors(Form $form)
    {
        $errors = [];
        /** @var Form $f */
        foreach ($form->getIterator() as $f) {
            if ($f instanceof Form) {
                $childErrors = $this->processFormErrors($f);
                if ($childErrors) {
                    $errors[$f->getName()] = $childErrors;
                }
            }
        }
        $selfErrors = [];
        foreach ($form->getErrors() as $e) {
            $selfErrors[] = $e->getMessage();
        }
        if ($selfErrors) {
            $errors[] = $selfErrors;
        }

        return $errors;
    }

    function arrayFlatten($array, $combinedKey = '')
    {

        if (!is_array($array)) {
            return false;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_string($key)) {
                $combinedKey = $combinedKey != '' ? $combinedKey.'_'.$key : $key;
            }
            if (is_array($value)) {
                $result = array_merge($result, $this->arrayFlatten($value, $combinedKey));
            } else {
                $result[$combinedKey] = $value;
            }
        }

        return $result;
    }
}