<?php


namespace AppBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use PDO;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Group;
use UserBundle\Entity\User;

/**
 *Service for checking
 */
class IpAddressCheckingService
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * IpAddressCheckingService constructor.
     * @param ContainerInterface $container
     * @param EntityManager $em
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isDomainIP(Request $request): bool
    {

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $configDomainIp = $em->getRepository('AppBundle:Config')->findOneBy([
            'key' => 'domain_ip_prefix'
        ]);

        /** Jeżeli nie ma tego skonfigurowanego, to zakładamy, że zawsze jest domenowe */

        if(empty($configDomainIp)) {
            return true;
        }

        $allowedIps = explode(',', $configDomainIp->getValue());
        $ip = $this->getClientIP($request);

        if(strpos($ip,'10.10.70.') === 0){
            return true;
        }

        foreach ($allowedIps as $allowedIp) {

            if (strpos($ip, $allowedIp) === 0) {
                return true;
            }
        }

        return false;

    }

    /**
     * @param Request|null $request
     * @return mixed|null|string
     */
    public function getClientIP(Request $request = null) {

        if($request === null) {
            $request = $this->container->get('request_stack')->getMasterRequest();
        }

        // Cloudflare
        if($request->server->has('HTTP_CF_CONNECTING_IP')) {
            return $request->server->get('HTTP_CF_CONNECTING_IP');
        }

        if($request->server->has('HTTP_X_FORWARDED_FOR')) {

            $ip = $request->server->get('HTTP_X_FORWARDED_FOR');

            if(strpos($ip, ',') !== FALSE) {
                $ips = explode(',', $ip);
                foreach ($ips as $_ip) {
                    if(!empty(trim($_ip))) {
                        return trim($_ip);
                    }
                }
            }
            else {
                return $ip;
            }
        }

        return $request->getClientIp();

    }

    /**
     * @param User $user
     * @param Request|null $request
     * @return bool
     */
    public function isUserRestricted(User $user, Request $request = null): bool
    {

        $ip = $this->getClientIP($request);

        /** @var PersistentCollection $groups */
        $groups = $user->getGroups();

        if(
            $groups === null ||
            (($groups instanceof PersistentCollection || $groups instanceof ArrayCollection) && $groups->isEmpty())
        )
        {
            return false;
        }

        $userRestrictedGroups = array_map(static function ($group) {
            /** @var Group $group */
            return $group->getId();
        }, $groups->getValues());

        $restrictedIpsEntity = $this->container->get('app.query_manager')
            ->executeProcedure('SELECT value, type FROM restricted_ip_access where group_id in ('. implode(',', $userRestrictedGroups).')');

        foreach ($restrictedIpsEntity as $singleEntity) {

            $valueToCheckList = explode(',', $singleEntity['value']);

            // dostęp tylko z załączonych ipków

            if ($singleEntity['type'] === '2' && !in_array($ip, $valueToCheckList, true)) {
                return true;
            }

            //nie pozwalaj na połączenie z załączonych ipków

            if ($singleEntity['type'] === '1' && in_array($ip, $valueToCheckList, true)) {
                return true;
            }

        }

        return false;

    }

}