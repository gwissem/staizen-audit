<?php
namespace AppBundle\Service;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Utils\Language;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\ExpressionLanguage\SyntaxError;

class DynamicTranslationService
{
    /** @var  ContainerInterface */
    protected $container;

    /** @var  EntityManagerInterface */
    protected $em;

    /**
     * @param ContainerInterface $container
     * @param EntityManager $em
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public function getDynamicText($key, AttributeParserService $attributeParser){

        $translations = $this->em->getRepository('AppBundle:DynamicTranslation')->getTranslations($key);

        $langExpression = new Language();

        $result = '';

        /** @var \AppBundle\Entity\DynamicTranslation $translation */
        foreach ($translations as $translation)
        {
            $condition = $translation->getValidation();

            if($condition) {;

                try {
                    $evalCondition = $langExpression->evaluate($attributeParser->parseString($condition,false ,false,null,false,true));
                }
                catch (SyntaxError $exception) {
                    throw new SyntaxError($exception->getMessage() . ' DynamicTranslations: ' . $translation->getId());
                }

                if ($evalCondition) {
                    $result = $translation->getText();
                    break;
                }

            } else{
                $result = $translation->getText();
            }
        }

        return $result;
    }
}