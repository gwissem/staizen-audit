<?php
namespace AppBundle\Service;

use ApiBundle\Entity\ApiCall;
use ApiBundle\Entity\ApiCallJsonDefinition;
use ApiBundle\Entity\ApiCallType;
use AppBundle\Entity\Log;
use AppBundle\Utils\ArrayUtils;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\ServiceDefinition;
use CaseBundle\Entity\ServiceStatus;
use CaseBundle\Entity\ServiceStatusDictionary;
use CaseBundle\Entity\Step;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Service\CaseHandler;
use CaseBundle\Utils\Language;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use JMS\JobQueueBundle\Entity\Job;
use PDO;
use SimpleXMLElement;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiCallManager
{

    const SPARX_UPDATE = 1;
    const SPARX_CREATE = 2;
    const SPARX_SMARTRSA_CREATE = 3;
    const TIMEOUT = 20;
    const SPARX_ID_PATH = '592';
    const VARYON_API = 'varyon';

    /** @var  ContainerInterface */
    protected $container;

    /** @var  EntityManagerInterface */
    protected $em;

    /** @var QueryManager $queryManager */
    protected $queryManager;

    /** @var CaseHandler $caseHandler */
    protected $caseHandler;

    /**
     * @param ContainerInterface $container
     * @param EntityManager $em
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
        $this->queryManager = $container->get('app.query_manager');
        $this->caseHandler = $container->get('case.handler');
    }

    public function sendCallForInstance($instanceId){

        /** @var ProcessInstance $instance */
        $instance = $this->em->getRepository(ProcessInstance::class)->find($instanceId);

        $output = "";

        if($instance){

            $groupProcessInstanceId = $instance->getGroupProcess()->getId();

            /** @var Step $step */
            $step = $instance->getStep();
            $apiCalls = $step->getApiCalls();

            foreach($apiCalls as $apiCall) {

                $condition = $this->checkCondition($apiCall, $groupProcessInstanceId);
                $variant = $apiCall->getVariant();

                if (!$condition || ($variant != null && $instance->getChoiceVariant() != $variant)) {

                    $output .= 'Conditions for API CALL not met. ';
                    continue;

                }

                /** @var $apiCall ApiCall */
                if ($apiCall->getType()->getId() == self::SPARX_UPDATE) {

                    $sparxId = $this->container->get('case.process_handler')->getAttributeValue(self::SPARX_ID_PATH, $groupProcessInstanceId, 'string');

                    if (!$sparxId) {
                        $output .= 'The Case is not SPARX case. ' ;
                        continue;
                    }

                }

                // Tymczasowy wyłom dla Ubench Prod
                if(strpos(strtolower($apiCall->getType()->getName()), 'ubench_prod') !== FALSE) {
                    $output .= $this->sendUbench($instance, $apiCall);
                }
                else {
                    $output .= $this->send($instance, $apiCall);
                }


            }
        }

        return $output;
    }

    private function checkCondition(ApiCall $apiCall, $groupProcessInstanceId){

        $condition = $apiCall->getCondition();

        if(empty($condition)){
            return true;
        }
        else{
            $parser = $this->container->get('case.attribute_parser.service');
            $parser->setGroupProcessInstanceId($groupProcessInstanceId);

            $lang = new Language();
            
            return $lang->evaluate($parser->parseString($condition, NULL, true));
        }

    }

    public function sendCallForCustomJsonDefinition($instanceId , $jsonId){
        $em = $this->em;
        /** @var ProcessInstance $processInstance */
        $processInstance = $em->getRepository(ProcessInstance::class)->find($instanceId);
        /** @var ApiCallJsonDefinition $jsonDefinition */
        $jsonDefinition = $em->getRepository(ApiCallJsonDefinition::class)->find($jsonId);
        $groupProcessInstance = $processInstance->getGroupProcess();
        $rootId = $groupProcessInstance->getRootId();
        $method = $jsonDefinition->getMethod();
        $isSparx = $this->container->get('case.handler')->isSparxCase($groupProcessInstance->getId());
        if(!$isSparx && $method == 'PUT'){
            return 'Aborted. Case is not Sparx shared case.';
        }

        $options['body'] = $this->getParsedBody($processInstance, $jsonDefinition->getJson());
        $options['headers'] = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => $this->getAuthorizationToken(self::SPARX_UPDATE)
        ];

        $url = $this->em->getRepository(ApiCallType::class)->find(self::SPARX_UPDATE)->getUrl();
        $parsedUrl = $this->parseString($url, $groupProcessInstance);
        return $this->sendRequest($parsedUrl, $method, $options, $jsonId, 'customJsonUpdate',$rootId);
    }

    public function sendCallForStatusUpdate($serviceStatusId){
        $em = $this->em;
        /** @var ServiceStatus $serviceStatus */
        $serviceStatus = $em->getRepository(ServiceStatus::class)->find($serviceStatusId);
        /** @var ProcessInstance $groupProcessInstance */
        $groupProcessInstance = $serviceStatus->getGroupProcessId();
        $groupProcessInstanceId = $groupProcessInstance->getId();

        $isSparx = $this->container->get('case.handler')->isSparxCase($groupProcessInstanceId);
        if(!$isSparx){
            return 'Aborted. Case is not Sparx shared case.';
        }
        $progress = $serviceStatus->getStatusDictionaryId()->getProgress();
        $cancellationRequested = $this->container->get('case.process_handler')->getAttributeValue('1136',$groupProcessInstanceId,'int');
        
        if($progress != 5 && $cancellationRequested == 1){
            return 'Aborted. Service is requested to be cancelled. Tried "'.$serviceStatus->getStatusDictionaryId()->getMessage().'" instead.';
        }

        /** @var ServiceStatusDictionary $status */
        $status = $serviceStatus->getStatusDictionaryId();
        $sparxUpdate = $status->getSparxUpdate();
        if($sparxUpdate){
            $json = $this->getParsedBody($groupProcessInstance, $sparxUpdate);
            if(strpos($json,'$type') === false){
                return 'Service is not configured for sparx update';
            }
        }
        else {
            $serviceId = $serviceStatus->getServiceId();
            $service = $em->getRepository(ServiceDefinition::class)->find($serviceId);

            $sparxStatus = $status->getSparxStatus();
            if($status->getId() < 0 && $this->container->get('case.parser_methods')->caseAbroad($groupProcessInstanceId)){
               $sparxStatus =  'CancellationRequested';
            }
            $sparxServiceName = $service->getSparxServiceName();

            $statusCount = $this->container->get('case.handler')->sparxStatusesCount($serviceStatusId, $groupProcessInstanceId);

            if($statusCount > 1){
                return 'This status has been already sent';
            }

            if(empty($sparxServiceName) || empty($sparxStatus)){
                return 'Service is not configured for sparx update';
            }

            $json = '{
                "Services" : [
                    {
                        "$type" : "' . $sparxServiceName . '",
                        "Status" : "' . $sparxStatus . '",
                        "ExternalReferences" : [{
                            "Reference" : "' . $groupProcessInstanceId . '",
                            "Source" : "ServiceProvider",
                            "OrganisationCode": "STARTER"
                        }]
                    }
                ]
            }';
        }

        $options['body'] = $json;
        $options['headers'] = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => $this->getAuthorizationToken(self::SPARX_UPDATE)
        ];

        $url = $this->em->getRepository(ApiCallType::class)->find(self::SPARX_UPDATE)->getUrl();
        $parsedUrl = $this->parseString($url, $groupProcessInstance);
        return $this->sendRequest($parsedUrl, 'PUT', $options, $serviceStatusId, 'statusUpdate');

    }

    private function prepareOptions(ProcessInstance $instance, ApiCall $apiCall){

        $options = [];

        if($instance && $apiCall) {

            $type = $apiCall->getType();
            $isVaryon = (strpos(strtolower($apiCall->getType()->getName()), 'varyon') !== FALSE);

            if($isVaryon) {
                $options['body'] = $this->getParsedVaryonXml($instance, $apiCall->getBody());
            }
            else {
                $options['body'] = $this->getParsedBody($instance, $apiCall->getBody(), $apiCall->getDataType());
            }

            $auth = $this->getAuthorizationToken($type->getId());

            // taki wyłom z typem danych, żeby nie psuć obecnej aplikacji
            if($apiCall->getDataType() === "text/xml") {

                $options['headers'] = [
                    'Accept' => $apiCall->getDataType(),
                    'Content-Type' => $apiCall->getDataType()
                ];

                if(!empty($auth)) {
                    $options['headers']['Authorization'] = $auth;
                }

            }
            else {
                $options['headers'] = [
                    'Accept' => 'application/'.$apiCall->getDataType(),
                    'Content-Type' => 'application/'.$apiCall->getDataType(),
                    'Authorization' => $this->getAuthorizationToken($type->getId())
                ];
            }

            if($isVaryon) {

                $isUpdate = (strpos(strtolower($apiCall->getType()->getName()), 'update') !== FALSE);

                if($isUpdate) {
                    $options['headers']['SOAPAction'] = 'http://atechno.pl/iCalculus/ICalculation2/ModifyDataSet';
                }
                else {
                    $options['headers']['SOAPAction'] = 'http://atechno.pl/iCalculus/ICalculation2/RegisterPolicy';
                }

            }

        }

        return $options;
    }

    private function parseString($string, ProcessInstance $instance, $escape = false){
        $groupProcessInstanceId = $instance->getGroupProcess()->getId();
        $attributeParser = $this->container->get('case.attribute_parser.service');
        $attributeParser->setGroupProcessInstanceId($groupProcessInstanceId);
        return $attributeParser->parseString($string,false,false,null,true,false,$escape);
    }

    /**
     * Metoda parsuje i replacuje zawartość XML
     *
     * @param ProcessInstance $instance
     * @param $xmlString
     * @return array|mixed|string
     */
    private function getParsedVaryonXml($instance, $xmlString) {


        $attributeParser = $this->container->get('case.attribute_parser.service');
        $attributeParser->setProcessInstanceId($instance->getId());
        $attributeParser->setGroupProcessInstanceId($instance->getGroupProcess()->getId());

        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xmlString);
        $xml = new SimpleXMLElement($response);

        $values = $xml->xpath('//icalvalues')[0];

        $array = json_decode(json_encode((array)$values), TRUE);

        if(is_array($array['parsedValue'])) {

            foreach ($array['parsedValue'] as $key => $item) {
                $this->parseXmlByItem($xmlString, $item, $attributeParser);
            }

        }
        else {

            $this->parseXmlByItem($xmlString, $array['parsedValue'], $attributeParser);

        }

        $xmlString = $attributeParser->parseString($xmlString, false,false, null, false);

        return $xmlString;

    }

    private function parseXmlByItem(&$xmlString, $item, AttributeParserService $attributeParser) {

        $node = '<parsedValue>' . $item . '</parsedValue>';

        $stringToParse = '{#iCalculusValue(' . str_replace('#', '*', $item) . ')#}';

        $parsedValue = $attributeParser->parseString($stringToParse, false,false, null, false);

        if(empty($parsedValue) || $parsedValue === '""') {
            $xmlString = str_replace($node, '', $xmlString);
        }
        else {
            $xmlString = str_replace($node, $this->getVaryonXmlNode($item, $parsedValue), $xmlString);
        }

    }

    private function getVaryonXmlNode($id, $v) {
        return "<ical:ArgValue><ical:id>" . $id . "</ical:id><ical:v>" . trim(htmlspecialchars($v)) . "</ical:v></ical:ArgValue>";
    }

    private function getParsedBody($instance, $string, $type = 'json'){

        $string = $this->parseString($string, $instance,true);
        $string = str_replace("\n",'',str_replace("\t",'',str_replace("\r",'',$string)));

        if($type === 'json') {
            $arr = json_decode($string, true);
            $utils = new ArrayUtils();
            $arr = $utils->removeEmptyElements($arr);
            $string = json_encode($arr);
        }

        return $string;
    }

    private function getParsedUrl($instance, $apiCall){
        $string = $apiCall->getUrl();
        return $this->parseString($string, $instance);
    }

    private function getAuthorizationToken($typeId){

        $auth = '';

        if(in_array($typeId, [self::SPARX_CREATE, self::SPARX_UPDATE])){
            $authResponse = json_decode($this->container->get('sparx.sparx_manager')->getAuthResponse());
            $auth = implode(' ', [$authResponse->token_type, $authResponse->access_token]);
        }
        else if(in_array($typeId, [self::SPARX_SMARTRSA_CREATE])){
            $auth = 'apikey '.$this->container->getParameter('sparx_smartrsa_apikey');
        }

        return $auth;
    }

    /**
     * @param ProcessInstance $instance
     * @param ApiCall $apiCall
     * @return mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function sendUbench(ProcessInstance $instance, ApiCall $apiCall){

        $xml = '';

        try {

            $xml = $this->getParsedBody($instance, $apiCall->getBody(), $apiCall->getDataType());
            $xml = str_replace('&','&amp;', $xml);

            $xml = $this->replaceSpecialChars($xml);

            $parameters = [
                [
                    'key' => 'processId',
                    'value' => (int)$instance->getId(),
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' => 'XML',
                    'value' => (string)$xml,
                    'type' => PDO::PARAM_STR
                ]
            ];

            $this->queryManager->executeProcedure(
                "EXEC [dbo].[p_AtlasUbench] @processId = :processId, @XML = :XML",
                $parameters, false
            );

            $this->caseHandler->addCaseLog('api_call_success', [
                'param3' => ($apiCall) ? $apiCall->getType()->getName() : null,
                'param2' => 200,
                'param1' => intval($instance->getId()),
                'outputContent' => $xml,
                'rootId' => $instance->getRoot()->getId()
            ]);

            $log = new Log();
            $log->setName('api_call_success');
            $log->setContent($xml);
            $log->setParam1($instance->getId());
            $log->setParam2('sent_to_so');
            $log->setParam3($instance->getRoot()->getId());
            $this->em->persist($log);
            $this->em->flush();

        }
        catch (\Exception $exception) {

            $this->caseHandler->addCaseLog('api_call_error', [
                'param3' => ($apiCall) ? $apiCall->getType()->getName() : null,
                'param2' => $exception->getCode(),
                'param1' => intval($instance->getId()),
                'inputContent' => $exception->getMessage(),
                'outputContent' => $xml,
                'rootId' => $instance->getRoot()->getId()
            ]);

            $log = new Log();
            $log->setName('api_call_error');
            $log->setContent($exception->getMessage());
            $log->setParam1($instance->getId());
            $log->setParam2($exception->getCode());
            $log->setParam3($instance->getRoot()->getId());
            $this->em->persist($log);
            $this->em->flush();

            return 'ERROR sendUbench. ' . $exception->getMessage();

        }

        return 'Success send to Ubench by SO';

    }

    private function replaceSpecialChars($xml) {

        return str_replace('&','&amp;', $xml);

    }

    /**
     * @param ProcessInstance $instance
     * @param ApiCall $apiCall
     * @return mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(ProcessInstance $instance, ApiCall $apiCall){

        $options = $this->prepareOptions($instance, $apiCall);
        $method = $apiCall->getMethod();
        $url = $this->getParsedUrl($instance, $apiCall);

        if($url){
            if(strpos($url,'sparx') !== false && $this->container->getParameter('sparx_enable') == false){
                return 'Sparx API disabled';
            }
        }

        return $this->sendRequest($url, $method, $options, $instance->getId(), '', $instance->getRoot()->getId(), $apiCall);

    }

    private function getVaryonItemId($responseString) {

        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $responseString);
        $xml = new SimpleXMLElement($response);

        if(isset($xml->sBody) && isset($xml->sBody->RegisterPolicyResponse) && isset($xml->sBody->RegisterPolicyResponse->RegisterPolicyResult)) {

            $result = (array)$xml->sBody->RegisterPolicyResponse->RegisterPolicyResult;

            if(is_array($result)) {

                if(array_key_exists('code', $result)) {
                    if($result['code'] == "0") {
                        return $result['itemId'];
                    }
                }

            }

        }

        return null;

    }

    /**
     * @param $url
     * @param $method
     * @param $options
     * @param null $logId
     * @param string $logInfo
     * @param null $rootId
     * @param ApiCall $apiCall
     * @return mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendRequest($url, $method, $options, $logId = NULL, $logInfo = 'apiCall', $rootId = null, $apiCall = null){

        $client = new Client([
            'base_uri' => $url,
            'timeout'  => self::TIMEOUT,
            'verify' => false
        ]);

        try {

            $response = $client->request($method, $url, $options);
            $responseBody = $response->getBody()->getContents();

            $isVaryon = false;

            if($apiCall) {
                $isVaryon = (strpos(strtolower($apiCall->getType()->getName()), self::VARYON_API) !== FALSE);
            }

            // Zapisanie ID z Varyon'a
            if($isVaryon && $response->getStatusCode() == "200") {

                $itemId = $this->getVaryonItemId($responseBody);

                if($itemId){

                    // TODO - trzeba to jeszcze przetestować.
//                    $value = $this->container->get('case.process_handler')->getAttributeValue('116,400', $rootId, 'int');
//                    if(empty($value)) {
//                        $this->container->get('case.process_handler')->editAttributeValue('116,400', $rootId, intval($itemId),'int','xxx',1,1);
//                    }
//
                    $this->container->get('case.process_handler')->editAttributeValue('116,400', $rootId, intval($itemId),'int','xxx',1,1);

                    if(empty($logInfo)) {
                        $logInfo = $itemId;
                    }
                }

            }
            else {

                $responseObj = json_decode($responseBody);

                if(!empty($responseObj->CaseId) && $rootId){
                    $sparxCaseId = $responseObj->CaseId;
                    $this->container->get('case.process_handler')->editAttributeValue('592',$rootId, $sparxCaseId,'string','xxx',1,1);
                }

            }

            $this->caseHandler->addCaseLog('api_call_success', [
                'param3' => ($apiCall) ? $apiCall->getType()->getName() : null,
                'param2' => $response->getStatusCode(),
                'param1' => intval($logId),
                'inputContent' => $responseBody,
                'outputContent' => (isset($options['body'])) ? $options['body'] : null,
                'rootId' => $rootId
            ]);

            $log = new Log();
            $log->setName('api_call_success');
            $log->setContent($responseBody);
            $log->setParam1($logId);
            $log->setParam2($response->getStatusCode());
            $log->setParam3($logInfo);
            $this->em->persist($log);
            $this->em->flush();

            return $options['body'];

        }
        catch(\Exception $e){


            $this->caseHandler->addCaseLog('api_call_error', [
                'param3' => ($apiCall) ? $apiCall->getType()->getName() : null,
                'param2' =>$e->getCode(),
                'param1' => intval($logId),
                'inputContent' => $e->getMessage(),
                'outputContent' => (isset($options['body'])) ? $options['body'] : null,
                'rootId' => $rootId
            ]);

            $log = new Log();
            $log->setName('api_call_error');
            $log->setContent($e->getMessage());
            $log->setParam1($logId);
            $log->setParam2($e->getCode());
            $log->setParam3($logInfo);
            $this->em->persist($log);
            $this->em->flush();
        }
    }

    public function queueApiCall(ProcessInstance $processInstance){
        if(count($processInstance->getStep()->getApiCalls()) > 0){
            $job = new Job(
                'api:call',
                ['instanceId' => $processInstance->getId()]
            );
            $this->em->persist($job);
            $this->em->flush();
        }
    }


}
