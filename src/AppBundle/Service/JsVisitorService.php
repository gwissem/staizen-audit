<?php
namespace AppBundle\Service;
use JMS\TranslationBundle\Model\FileSource;
use JMS\TranslationBundle\Model\Message;
use JMS\TranslationBundle\Model\MessageCatalogue;
use JMS\TranslationBundle\Translation\Extractor\FileVisitorInterface;
class JsVisitorService implements FileVisitorInterface
{

    public function visitFile(\SplFileInfo $file, MessageCatalogue $catalogue)
    {

        if ('.js' !== substr($file, -3)) {
            return;
        }

        $handleFile = fopen($file->getRealPath(), "r");

        if ($handleFile) {
            while (($line = fgets($handleFile)) !== false) {

                preg_match_all('/Translator.trans[\(](?<key>.*?)[\)]/', $line, $transFound);
                preg_match_all('/(\/\/[ ]*@desc\([\'"])(?<key>.*)[\'"]\)/', $line, $descFound);

                if(!empty($transFound['key'])) {

                    foreach ($transFound['key'] as $trans) {

                        if (false === (bool)strpos((string)$file, 'js_translate')) {
                            preg_match_all('/([^{\s]|^)[\'"](?<key>[a-zA-Z0-9._-]*?)[\'"]/', $trans, $found);
                        } else {
                            $found = $transFound;
                        }

                        $message = new Message($found['key'][0],'js');
                        $message->addSource(new FileSource((string)$file));

                        if(!empty($descFound["key"])) {
                            $message->setDesc($descFound["key"][0]);
                        }

                        $catalogue->add($message);

                    }

                }

            }

            fclose($handleFile);
        }


    }
    public function visitPhpFile(\SplFileInfo $file, MessageCatalogue $catalogue, array $ast) { }
    public function visitTwigFile(\SplFileInfo $file, MessageCatalogue $catalogue, \Twig_Node $node) { }

}