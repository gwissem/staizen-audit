<?php
namespace AppBundle\Service;

use AppBundle\Utils\QueryManager;
use CaseBundle\Service\CaseHandler;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DataWarehouseService
{

    /** @var  ContainerInterface */
    protected $container;

    /** @var  EntityManagerInterface */
    protected $em;

    /** @var QueryManager $queryManager */
    protected $queryManager;

    /** @var CaseHandler $caseHandler */
    protected $caseHandler;

    /**
     * @param ContainerInterface $container
     * @param EntityManager $em
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
        $this->queryManager = $container->get('app.query_manager');
        $this->caseHandler = $container->get('case.handler');
    }

    public function getDimensionRefreshDate(){

        $query = "SELECT dbo.FN_VDateHour(min(lastSynchroDate)) FROM SETTE.AtlasDB_rep.dbo.definition where active = 1";
        $result = $this->queryManager->executeProcedure($query);
        return $result[0][''];

    }

}
