<?php

namespace AppBundle\Service;

use AppBundle\Entity\ValueDictionary;
use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\AttributeParserService;
use CaseBundle\Service\ProcessHandler;
use CaseBundle\Utils\ParserMethods;
use MssqlBundle\PDO\PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Service\UserInfoParserService;


class  CaseForViewerService extends AttributeParserService
{

    /** @var  ContainerInterface */
    protected $container;

    /** @var  EntityManagerInterface */
    protected $em;

    public function getCaseInfo(ProcessInstance $instance, $dynamicEditAttributes = true)
    {

        $groupProcess = $instance->getGroupProcess();
        $groupProcessInstanceId = $groupProcess->getId();
        $this->setGroupProcessInstanceId($groupProcessInstanceId);
        $root = $instance->getRoot() ?? $instance;
        $caseId = $this->processHandler->getAttributeValue('235', $root->getId(), 'string');
        $caseId = !empty($caseId) ? $caseId[0] : 'A' . substr('00000000' . $root->getId(), -8);

        $info = [
            'Nr Sprawy' => $caseId,
            'Program główny' => '{#displayProgram()#}',
            'Status sprawy' => '({#getCaseStatus({#rootid#})#})',
            'Typ zdarzenia' => '{@883,892@}{@491@}',
            'Marka i model pojazdu' => '{@74,73@}',
            'Nr rej.' => '{@74,72@}',
            'Nr VIN' => '{@74,71@}',
            'Rodzaj pojazdu' => $this->getCarType(),
            'Data pierwszej rej.' => '{-first_registration-}',
            'Przebieg' => '{@74,75@}',
            'Lokalizacja aktualna' => ' {#displayLocalization()#}',
            'Lokalizacja docelowa' => '{#towingPlace()#}',
            'Diagnoza' => '{#diagnosisDescription(true)#}',
            'Imię, nazwisko i nr tel. kierowcy' => '{@80,342,64@} {@80,342,66@}, {@80,342,408,197@}',
            'E-mail kierowcy' => '{@80,342,368@}',
            'Data i godzina otwarcia sprawy' => $this->getCaseStartInfo(),
        ];
        if ($endingDate = $this->getCaseEndInfo()) {
            $info['Data i godzina zamknięcia sprawy'] = $endingDate;
        }


        $html = $this->twig->render("@App/CaseViewer/CaseTools/case_info.html.twig", [
            'info' => $info
        ]);

        $parsedHtml = $this->parseString($html);

        return $parsedHtml;

    }

    /**
     * @param $groupProcessInstanceId
     * @param $tableName
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getTableOf($groupProcessInstanceId, $tableName) {

        if($tableName === "ClaimForm") {

            $claimForm  = $this->em->getRepository('CaseBundle:ProcessInstance')->findBy([
                'groupProcess' => $groupProcessInstanceId,
                'step' => '1155.001'
            ]);

            if(empty($claimForm)) return null;

        }

        $this->setGroupProcessInstanceId(intval($groupProcessInstanceId));

        $rows = $this->getRowsOfTable($tableName);

        $html = $this->twig->render('@App/CFMCaseViewer/Templates/template_of_procedure_table.html.twig', [
            'rows' => $rows
        ]);

        return $html;

    }

    /**
     * @param $tableName
     * @return array
     */
    public function getRowsOfTable($tableName) {

        $parameters = [
            [
                'key' => 'groupProcessInstanceId',
                'value' => $this->getGroupProcessInstanceId(),
                'type' => PDO::PARAM_INT
            ]
        ];

        $rows = $this->queryManager->executeProcedure(
            "EXEC [dbo].[p_getTableOf_" . $tableName . "] @groupProcessInstanceId = :groupProcessInstanceId",
            $parameters
        );

        foreach ($rows as $key => $row) {

            if($row['label'] !== "separator") {
                $rows[$key]['value'] = $this->parseStringOrError($row['value']);
                if($rows[$key]['value'] === '""') {
                    $rows[$key]['value'] = "";
                }
            }

        }

        return $rows;

    }

    public function parseStringOrError($string)
    {
        try {
            return $this->parseString($string);
        } catch (\Exception $exception) {
            return "--- Parsing error ---";
        }
    }

    /**
     * @return string
     */
    public function getCarType()
    {

        $makeModel = (int)$this->processHandler->getAttributeValue('74,73', $this->groupProcessInstanceId, 'int');

        $type = $this->em->getRepository(ValueDictionary::class)->findOneBy([
            'value' => $makeModel,
            'type' => 'makeModel'
        ]);

        $result = ' - ';

        if ($type) {
            $result = (int)$type->getArgument2() == 1 ? 'Osobowy' : 'Użytkowy';
        }

        return $result;

    }


    /**
     * @return string
     */
    public function getCaseStartInfo()
    {
        $result = '';
        $rootProcessInstance = $this->em
            ->getRepository('CaseBundle:ProcessInstance')
            ->find($this->getRootProcessInstanceId());
        if ($rootProcessInstance) {
            $date = $rootProcessInstance->getCreatedAt();
            $result = $date->format('d-m-Y H:i');
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getCaseEndInfo()
    {

        $result = '';
        $processInstance = $this->em->getRepository('CaseBundle:ProcessInstance')->findOneBy(
            [

                'step' => '1011.024',
                'groupProcess' => $this->groupProcessInstanceId

            ]);

        if ($processInstance) {
            $date = $processInstance->getCreatedAt();
            $result = $date->format('d-m-Y H:i');
        }


        return $result;

    }
}