<?php

namespace AppBundle\Doctrine\DBAL;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection as BaseConnection;
use Doctrine\DBAL\Driver;

class Connection extends BaseConnection
{
    public function __construct(array $params, Driver $driver, $config = null, $eventManager = null)
    {
        parent::__construct($params, $driver, $config, $eventManager);

//        $this->executeUpdate('SET TRANSACTION ISOLATION LEVEL SNAPSHOT');

    }

}