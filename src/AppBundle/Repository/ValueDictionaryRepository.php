<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ValueDictionary;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ValueDictionaryRepository extends \Doctrine\ORM\EntityRepository
{
    const TYPE_PARTNER_SERVICE_TYPE = 'PartnerServiceType';

    public function getAttributeTypes()
    {
        $resultArray = [];
        $qb = $this->baseQueryBuilder();
        $qb->select('d.value, d.text')
            ->where('d.type = :attributeType')
            ->setParameter('attributeType', ValueDictionary::TYPED_ATTRIBUTE_TYPE)
            ->orderBy('d.value', 'ASC');

        $queryResult = $qb->getQuery()->getArrayResult();
        foreach ($queryResult as $row) {
            $resultArray[$row['text']] = $row['value'];
        }

        return $resultArray;

    }

    public function getPartnerServiceTypeByServiceId($serviceId)
    {

        $qb = $this->baseQueryBuilder();
        $qb->select('d.value')
            ->where('d.type = :type')
            ->andWhere('d.description = :serviceId')
            ->setParameter('type', self::TYPE_PARTNER_SERVICE_TYPE)
            ->setParameter('serviceId', $serviceId)
            ->orderBy('d.value', 'ASC');

        $queryResult = $qb->getQuery()->getArrayResult();
        if (count($queryResult) > 0) {
            return $queryResult[0]['value'];
        }

        return null;


    }

    public function baseQueryBuilder()
    {
        $qb = $this->createQueryBuilder('d');

        return $qb;
    }

    public function getDictionaryForSelect($type, $withEmptyOption = false, $valueIn = [], $valueNotIn = [])
    {

        $qb = $this->createQueryBuilder('d');

        $qb->select('d.value, d.text')
            ->where('d.type = :type')
            ->setParameter('type', $type)
            ->andWhere('d.active = 1');

        if (!empty($valueIn)) {
            $qb->andWhere('d.value IN (:valueIn)')
                ->setParameter('valueIn', $valueIn);
        }

        if (!empty($valueNotIn)) {
            $qb->andWhere('d.value NOT IN (:valueNotIn)')
                ->setParameter('valueNotIn', $valueNotIn);
        }

        $events = $qb->getQuery()->getResult();

        if (!empty($events)) {

            $eventsArray = [];

            if ($withEmptyOption) {
                $eventsArray['---'] = null;
            }

            foreach ($events as $item) {
                $eventsArray[$item['text']] = $item['value'];
            }

            return $eventsArray;

        }

        return [];

    }

}
