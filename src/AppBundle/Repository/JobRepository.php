<?php

/*
 * Copyright 2012 Johannes M. Schmitt <schmittjoh@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Job;
use CaseBundle\Entity\DictionaryPackage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class JobRepository extends \JMS\JobQueueBundle\Entity\Repository\JobRepository
{


    public function findOpenedJobsForRelatedEntity($command, $relatedEntity, $exceptIds = NULL)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata('JMSJobQueueBundle:Job', 'j');
        $params = new ArrayCollection();
        if($exceptIds) {
            $sql = "SELECT j.* FROM jms_jobs j INNER JOIN jms_job_related_entities r ON r.job_id = j.id WHERE r.related_class = :relClass AND r.related_id = :relId AND j.command = :command AND j.state IN (:states) AND j.id NOT IN (:exceptIds) ORDER BY j.executeAfter DESC";
            $params->add(new Parameter('exceptIds', $exceptIds));
        }
        else{
            $sql = "SELECT j.* FROM jms_jobs j INNER JOIN jms_job_related_entities r ON r.job_id = j.id WHERE r.related_class = :relClass AND r.related_id = :relId AND j.command = :command AND j.state IN (:states) ORDER BY j.executeAfter DESC";
        }

        $params->add(new Parameter('command', $command));
        $params->add(new Parameter('relClass', DictionaryPackage::class));
        $params->add(new Parameter('relId', json_encode(['id' => $relatedEntity->getId()])));
        $params->add(
            new Parameter('states', [Job::STATE_RUNNING, Job::STATE_PENDING, Job::STATE_NEW])
        );

        return $this->_em->createNativeQuery($sql, $rsm)
            ->setParameters($params)
            ->getResult();
    }

    public function getJobs()
    {
        $qb = $this->baseQueryBuilder();
        $qb->select('j')
            ->where($qb->expr()->isNull('j.originalJob'))
            ->orderBy('j.id', 'desc');

        return $qb->getQuery();
    }

    public function baseQueryBuilder()
    {
        $qb = $this->createQueryBuilder('j');

        return $qb;
    }
}
