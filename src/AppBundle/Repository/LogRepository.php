<?php

namespace AppBundle\Repository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LogRepository extends \Doctrine\ORM\EntityRepository
{
    public function countLogOneHourAgo($user, $content)
    {
        $date = new \DateTime();
        $date->modify('-1 hour');

        $db = $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->innerJoin('p.createdBy', 'u')
            ->where('u.id = :user')
            ->andWhere('p.updatedAt > :date')
            ->andWhere('p.content = :content')
            ->setParameter('user', $user)
            ->setParameter(':date', $date)
            ->setParameter(':content', $content)
            ->getQuery()
            ->getSingleScalarResult();

        return $db;
    }

    public function findLogOneHourAgo($user, $content)
    {
        $date = new \DateTime();
        $date->modify('-1 hour');

        $db = $this->createQueryBuilder('p')
            ->innerJoin('p.createdBy', 'u')
            ->where('u.id = :user')
            ->andWhere('p.updatedAt > :date')
            ->andWhere('p.content = :content')
            ->setParameter('user', $user)
            ->setParameter(':date', $date)
            ->setParameter(':content', $content)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return $db;
    }
}
