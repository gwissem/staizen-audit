<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Log;
use SocketBundle\Topic\AtlasTopic;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use UserBundle\Entity\Group;
use UserBundle\Entity\TelephonyAgentStatus;
use UserBundle\Entity\User;

class AdminPanelController extends Controller
{
    /**
     * @Route("/admin-panel/telephony-status", name="adminPanelTelephonyStatus", options={"expose"=true})
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */

    public function indexAction()
    {

        $em  = $this->getDoctrine()->getManager();

        /** @var TelephonyAgentStatus[] $statuses */
        $statuses = $em->getRepository('UserBundle:TelephonyAgentStatus')->findAll();

        $statusList = [
            'ready' => 0,
            'not-ready' => 0,
            'talking' => 0,
            'other' => 0,
            'logout' => 0
        ];

        foreach ($statuses as $status) {

            switch ($status->getState()) {
                case 'READY' : {
                    $statusList['ready']++;
                    break;
                }
                case 'NOT_READY': {
                    $statusList['not-ready']++;
                    break;
                }
                case 'LOGOUT' : {
                    $statusList['logout']++;
                    break;
                }
                case 'TALKING' : {
                    $statusList['talking']++;
                    break;
                }
                default: {
                    $statusList['other']++;
                }
            }

        }


        return new JsonResponse([
            "statuses" => $statusList
        ]);

    }

    /**
     * @Route("/admin-panel/send-notification", name="adminPanelSendNotification", options={"expose"=true})
     * @Method("POST")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return JsonResponse
     */

    public function sendNotificationAction(Request $request)
    {

        $data = $request->request->all();

        $pusher = $this->get('gos_web_socket.zmq.pusher');

        $usersId = [];
        $forAll = false;

        if($data['type-receivers'] === "users") {
            $usersId = (isset($data['users'])) ? $data['users'] : [];
        }
        else if($data['type-receivers'] === "groups") {

            $groups = $this->getDoctrine()->getRepository('UserBundle:Group')->findBy([
                'id' => (isset($data['groups'])) ? $data['groups'] : []
            ]);

            foreach ($groups as $group) {

                foreach ($group->getUsers() as $user) {
                    $usersId[] = $user->getId();
                }

            }

        }
        else {
            $forAll = true;
        }

        $data = [
            'action' => AtlasTopic::ACTION_NOTIFICATION,
            'data' => [
                'message' => ($data['notification-content']) ? $data['notification-content'] : 'Wiadomość.',
                'type' => $data['type-notification'],
                'withIcon' => (isset($data['with-icon'])),
                'timeout' => intval($data['notification-timeout']),
                'users' => $usersId,
                'forAll' => $forAll
            ]
        ];

        $pusher->push($data, 'atlas_main_topic');

        return new JsonResponse([
            "status" => true,
            "data" => $data,
            "amount" => count($usersId)
        ]);

    }

    /**
     * @Route("/admin-panel/find-users", name="adminPanelFindUsers", options={"expose"=true})
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return JsonResponse
     */

    public function findUsersAction(Request $request)
    {

        $users = [];

        $term = $request->query->get('term', '');

        if(!empty($term)) {

            /** @var User[] $usersArray */
            $usersArray = $this->getDoctrine()->getRepository('UserBundle:User')->getQueryOfAllUser($term)->getResult();

            foreach ($usersArray as $user) {
                $users[] = [
                    'id' => $user->getId(),
                    'text' => $user->getName() . ' (' . $user->getUsername() . ')'
                ];
            }

        }

        return new JsonResponse([
            "results" => $users
        ]);

    }
    /**
     * @Route("/admin-panel/find-users-for-tools", name="adminPanelFindUsersForToolsPermissionsAction", options={"expose"=true})
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return JsonResponse
     */

    public function findUsersForToolsPermissionsAction(Request $request)
    {

        $users = [];

        $term = $request->query->get('term', '');

        if(!empty($term)) {


            /** @var User[] $usersArray */
            $usersArray = $this->getDoctrine()
                ->getRepository('UserBundle:User')
                ->getEnabledUsersByCompany($term,1)
                ->getResult();


            foreach ($usersArray as $user) {
                $users[] = [
                    'id' => $user->getId(),
                    'text' => $user->getName() . ' (' . $user->getUsername() . ')'
                ];
            }

        }

        return new JsonResponse([
            "results" => $users
        ]);

    }

    /**
     * @Route("/admin-panel/find-groups", name="adminPanelFindGroups", options={"expose"=true})
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return JsonResponse
     */

    public function findGroupsAction(Request $request)
    {

        $groups = [];

        $term = $request->query->get('term', '');

        if(!empty($term)) {

            /** @var Group[] $groupsArray */
            $groupsArray = $this->getDoctrine()->getRepository('UserBundle:Group')->findAllForToolByPhrase($term)->getResult();

            foreach ($groupsArray as $group) {
                $groups[] = [
                    'id' => $group->getId(),
                    'text' => $group->getName()
                ];
            }

        }

        return new JsonResponse([
            "results" => $groups
        ]);

    }

}
