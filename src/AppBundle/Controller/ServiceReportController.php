<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Log;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UserBundle\Entity\TelephonyAgentStatus;

class ServiceReportController extends Controller
{
    /**
     * @Route("/dc", name="serviceRaport")
     * @Route("/DC", name="serviceRaport2")
     * @Route("/Dc", name="serviceRaport3")
     * @Route("/dC", name="serviceRaport4")
     * @Method("GET")
     *
     */

    public function indexAction()
    {

        $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy(['token' => 'raport-serwisowy']);

        if(empty($processInstances)) {
            throw new NotFoundHttpException();
        }

        return $this->render('OperationalBundle:PublicDashboard:index.html.twig',
            [
                'processToken' => 'raport-serwisowy'
            ]
        );

    }


    /**
     * @Route("/CUPRA", name="serviceCupraRaport")
     * @Route("/cupra", name="serviceCupraRaport2")
     * @Route("/Cupra", name="serviceCupraRaport3")
     * @Method("GET")
     *
     */

    public function indexCupraAction()
    {

        $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy(['token' => 'dc-cupra']);

        if(empty($processInstances)) {
            throw new NotFoundHttpException();
        }

        return $this->render('OperationalBundle:PublicDashboard:index.html.twig',
            [
                'processToken' => 'dc-cupra'
            ]
        );

    }

    /**
     * @Route("/rs", name="serviceRaportFord")
     * @Route("/RS", name="serviceRaportFord2")
     * @Route("/rS", name="serviceRaportFord3")
     * @Route("/Rs", name="serviceRaportFord4")
     * @Method("GET")
     *
     */

    public function indexFordAction()
    {

        $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy(['token' => 'dc-ford']);

        if(empty($processInstances)) {
            throw new NotFoundHttpException();
        }

        return $this->render('OperationalBundle:PublicDashboard:index.html.twig',
            [
                'processToken' => 'dc-ford'
            ]
        );

    }

    /**
     * @Route("/rda", name="serviceRaportRDA")
     * @Route("/RDA", name="serviceRaportRDA2")
     * @Route("/rdA", name="serviceRaportRDA3")
     * @Route("/rDa", name="serviceRaportRDA4")
     * @Route("/rDa", name="serviceRaportRDA5")
     * @Route("/rDA", name="serviceRaportRDA6")
     * @Route("/rDa", name="serviceRaportRDA7")
     * @Route("/Rda", name="serviceRaportRDA8")
     * @Route("/RdA", name="serviceRaportRDA9")
     * @Method("GET")
     *
     */
    public function indexRDAAction()
    {

        $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy(['token' => 'dc-rda']);

        if(empty($processInstances)) {
            throw new NotFoundHttpException();
        }

        return $this->render('OperationalBundle:PublicDashboard:index.html.twig',
            [
                'processToken' => 'dc-rda'
            ]
        );

    }


    /**
     * @Route("/rzw", name="rental_end_step")
     * @Method("GET")
     *
     */

    public function rentalEndStepAction()
    {

        $processInstances = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy(['token' => 'rzw']);

        if(empty($processInstances)) {
            throw new NotFoundHttpException();
        }

        return $this->render('OperationalBundle:PublicDashboard:index.html.twig',
            [
                'processToken' => 'rzw'
            ]
        );

    }
}
