<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ticket;
use AppBundle\Form\Type\AdminTicketType;
use AppBundle\Form\Type\TicketFilterType;
use AppBundle\Form\Type\UserTicketType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TicketController extends Controller
{
    /**
     * @Route("/request/{type}", name="request_ticket")
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     *
     */
    public function requestTicketAction(Request $request, $type = null)
    {

        $em = $this->getDoctrine()->getManager();

        $ticket = new Ticket();
        $form = $this->createForm(
            UserTicketType::class
        );

        $screenData = $request->request->get('screenShootFileData');
        $caseInfo = $request->request->get('case_info');
        $ticketLevel = $request->request->get('ticketLevel');

        $form->setData($ticket);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $ticket->getName() && $ticket->getContent()) {
            $user = $this->getUser();
            $originalUserInfo = $this->get('user.info')->getOriginalUser();
            $originalUser = $originalUserInfo ? $em->getRepository('UserBundle:User')->find(
                $originalUserInfo->getId()
            ) : $user;

            if(!empty($screenData)) {
                $img = str_replace('data:image/jpeg;base64,', '', $screenData);
                $img = str_replace(' ', '+', $img);
                $fileData  = base64_decode($img);
                $uploader = $this->container->get('document.uploader');
                $screenPath = $uploader->uploadFromSource($fileData);
            }
            else {
                $screenPath = null;
            }

            if($caseInfo) {
                $ticket->setName($ticket->getName() . " (" . $caseInfo . ")");
            }

            if($ticketLevel) {
                $ticket->setTicketLevel($ticketLevel);
            }

            $ticket->setScreenShoot($screenPath);
            $ticket->setLocale($request->getLocale());
            $ticket->setCreatedBy($user);
            $ticket->setCreatedByOriginal($originalUser);
            $ticket->setType($type);
            $ticket->setBrowser($request->server->get('HTTP_USER_AGENT'));
            $ticket->setRequestInfo(json_encode($request->server->all()));

            $em->persist($ticket);
            $em->flush();

            return new Response('OK');
        }

    }

    /**
     * @Route("/list/{page}", requirements={"page" = "\d+"}, name="admin_ticket_index", options={"expose"=true})
     * @Method("GET")
     * @Security("is_granted('ROLE_SUPPORT')")
     * @param Request $request
     * @param int $page
     * @return Response
     */
    public function indexAction(Request $request, $page = 1)
    {

        $statuses = $request->query->get('statuses', [Ticket::STATUS_NEW]);

        $em = $this->getDoctrine()->getManager();
        $perPage = 10;

        $filterForm = $this->createForm(TicketFilterType::class, ['statuses' => $statuses]);

        $usersQuery = $em->getRepository('AppBundle:Ticket')->getTicketsQuery($statuses);
        $pagination = $this->get('knp_paginator')->paginate($usersQuery, $page, $perPage);

        return $this->render(
            'AppBundle:Admin:ticket/index.html.twig',
            array(
                'pagination' => $pagination,
                'statuses' => json_encode(array_flip(Ticket::getStatuses())),
                'status' => $statuses,
                'page' => $page,
                'filterForm' => $filterForm->createView(),
            )
        );
    }

    /**
     * @Route("/list/update_status", name="admin_ticket_list_update_status")
     * @Method("POST")
     * @Security("is_granted('ROLE_SUPPORT')")
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function updateStatusAction(Request $request)
    {

        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();
        /** @var Ticket $entity */
        $entity = $em->getRepository('AppBundle:Ticket')->find($data['pk']);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono zgłoszenia'));
        }

        $entity->setStatus($data['value']);
        $em->persist($entity);
        $em->flush();

        return new JsonResponse($data);

    }


    /**
     * @Route("/editor/{ticketId}", name="admin_ticket_editor", requirements={"ticketId": "\d+"})
     * @Security("is_granted('ROLE_SUPPORT')")
     */

    public function editorAction(Request $request, $ticketId = null)
    {
        $em = $this->getDoctrine()->getManager();

        if ($ticketId) {
            $entity = $em->getRepository('AppBundle:Ticket')->find($ticketId);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono zgłoszenia'));
            }
        } else {
            $entity = new Ticket();
            $entity->setCreatedBy($this->getUser());
            $entity->setLocale($request->getLocale());
            $entity->setType(Ticket::TYPE_ISSUE);
        }

        $form = $this->createForm(AdminTicketType::class, $entity);

        $form->setData($entity);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Zgłoszenie "%name%" została pomyślnie zapisana',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_ticket_index'));
        }

        return $this->render(
            'UserBundle:Admin/company:editor.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }

    /**
     * @Route("/details/{ticketId}", name="admin_ticket_show", requirements={"ticketId": "\d+"})
     * @Route("/error_ticket/details/{ticketId}", name="admin_ticket_detail", requirements={"ticketId": "\d+"})
     * @Security("is_granted('ROLE_SUPPORT')")
     * @param null $ticketId
     * @return Response
     */

    public function showAction($ticketId = null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Ticket')->find($ticketId);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono zgłoszenia'));
        }

        return $this->render(
            'AppBundle:Admin:ticket/show.html.twig',
            array(
                'entity' => $entity,
            )
        );
    }

    /**
     * @Route("/delete/{id}", name="admin_ticket_delete")
     * @Security("is_granted('ROLE_SUPPORT')")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('AppBundle:Ticket')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono zgłoszenia'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Zgłoszenie "%name%" zostało pomyślnie usunięte',
                ['%name%' => $entity->getName()]
            )
        );

        return $this->redirect($this->generateUrl('admin_ticket_index'));

    }
}
