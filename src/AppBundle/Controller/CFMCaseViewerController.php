<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Log;
use AppBundle\Entity\ValueDictionary;
use AppBundle\Form\Type\CFMSearcherCaseType;
use AppBundle\Form\Type\ExtendRentalType;
use AppBundle\Service\AtlasViewerService;
use AppBundle\Service\CasesToXlsService;
use AppBundle\Utils\NewCaseViewer;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Entity\Step;
use CaseBundle\Utils\ParserMethods;
use FOS\RestBundle\Controller\Annotations as Rest;
use MailboxBundle\Entity\ConnectedMailbox;
use MailboxBundle\Service\SimpleOutlookSenderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Stopwatch\Stopwatch;
use UserBundle\Entity\User;

/**
 * // TODO - przepisać to wszystko na SERWIS
 *
 * Class CaseViewerController
 * @package ManagementBundle\Controller
 * @Rest\Route("/cfm-case-viewer")
 */
class CFMCaseViewerController extends Controller
{

    const CLAIM_SERVICE_ID = '31';

    private $statusServices = [];
    private $statusProgress = [];

    /**
     * @Rest\Route("/index", name="ccv_index")
     * @Method("GET")
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $processHandler = $this->get('case.process_handler');
        $atlasViewerService = $this->get('app.atlas_viewer');

        /** @var User $user */
        $user = $this->getUser();
        $companyId = ($user->getCompany()) ? $user->getCompany()->getId() : null;
        
        /** @var array $platformPermission */
        list($platformPermission, $companyKey) = $processHandler->getCompanyPlatformPermissions($this->getUser());

        $isRenault = ($companyKey === AtlasViewerService::COMPANY_KEY_RDA);

//        $platformPermission = $processHandler->getPlatformsForUser($this->getUser()->getId());

        $platforms = null;

        $displayPlatformFilter = $this->canFilterPlatforms($companyKey);

        if ($displayPlatformFilter) {
            $platforms = $this->getDoctrine()->getRepository('CaseBundle:Platform')->findBy([
                'id' => $platformPermission
            ]);
        }

        $programs = $this->getDoctrine()->getRepository('CaseBundle:Program')->findBy([
            'platform' => $platformPermission,
            'status' => 1
        ]);

        if(AtlasViewerService::companyIsPZU($companyKey)){

            $eventTypes = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'EventType',
                'active' => 1,
                'value' => [1, 2, 3, 6]
            ]);

        }
        elseif (in_array('25', $platformPermission, true)) {

            $eventTypes = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'EventType',
                'active' => 1,
                'value' => [1, 2]
            ]);

        } else {

            $eventTypes = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'EventType',
                'active' => 1
            ]);

        }

        if (in_array('25', $platformPermission, true)) {

            $eventTypesLp = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'ExtraEventTypeLP',
                'active' => 1
            ]);

            $eventTypes = array_merge($eventTypes, $eventTypesLp);

            usort($eventTypes, static function ($a, $b) {

                /** @var ValueDictionary $a */
                /** @var ValueDictionary $b */

                return $a->getOrderBy() > $b->getOrderBy();
            });

        }

        /** Dodawanie reklamacji */

        $complaintCategories = [];
        $canAddComplaint = $this->checkPermissionForClaim($processHandler);

        if ($canAddComplaint) {

            $complaintCategories = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'viewerComplaintType',
                'active' => 1
            ]);

        }

        $displaySubEvent = !(in_array('25', $platformPermission, true) && $companyId !== 1);

        $caseCategories = [];
        $countries = [];

        if(AtlasViewerService::companyIsPZU($companyKey)) {
            $displaySubEvent = false;

            $countries = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'europeanCountry',
                'active' => 1
            ]);

            $this->sortCountries($countries);

        }

        if($isRenault) {
            $displaySubEvent = false;
            $caseCategories = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'caseCategory',
                'active' => 1
            ]);
        }

        if ($displaySubEvent) {
            $subEventTypes = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'stagesLiquidationDamage',
                'active' => 1
            ]);
        } else {
            $subEventTypes = [];
        }

        $canCreateCase = (in_array('78', $platformPermission, true));

        $canAddSpecialRequest = $this->canAddSpecialRequest($platformPermission);

        $columns = $processHandler->findCFMCases(implode(',', $platformPermission), [], true);

        $displayStatus = $this->displayStatusControl($platformPermission);
        $availableStatus = [];

        if ($displayStatus) {

            $availableStatus = $this->getDoctrine()->getRepository('AppBundle:ValueDictionary')->findBy([
                'type' => 'caseStatus',
                'active' => 1
//                'argument1' => 25 // TODO - Na razie wszystkie CFMy widdzą status
            ]);

        }

        $searcherForm = $this->createForm(CFMSearcherCaseType::class, null, [
            'programs' => $programs,
            'eventTypes' => $eventTypes,
            'coordinates' => $this->getCoordinatorsFromCompany($user->getCompany()),
            'subEventTypes' => $subEventTypes,
            'companyId' => $user->getCompany()->getId(),
            'platforms' => $platforms,
            'displaySubEvent' => $displaySubEvent,
            'availableStatus' => $availableStatus,
            'platformPermission' => $platformPermission,
            'displayPlatformFilter' => $displayPlatformFilter,
            'isRenault' => $isRenault,
            'caseCategories' => $caseCategories,
            'companyKey' => $companyKey,
            'countries' => $countries
        ]);

        return $this->render('@App/CFMCaseViewer/index.html.twig', [
            'searcherForm' => $searcherForm->createView(),
            'tableColumns' => json_encode($columns),
            'displayStatusControl' => $displayStatus,
            'canAddComplaint' => $canAddComplaint,
            'complaintCategories' => $complaintCategories,
            'canCreateCase' => $canCreateCase,
            'canAddSpecialRequest' => $canAddSpecialRequest,
            'isRenault' => $isRenault,
            'canExportData' => $atlasViewerService->canExportData($user->getId(), $companyKey)
        ]);

    }

    /**
     * @param $countryName
     * @return int
     */
    private function getCountryPriority($countryName) {

        switch ($countryName) {

            case 'Niemcy': {
                return 50;
            }
            case 'Francja': {
                return 40;
            }
            case 'Włochy': {
                return 30;
            }
            case 'Austria': {
                return 20;
            }
            default: {
                return 1;
            }
        }

    }

    /**
     * @param $countries
     */
    private function sortCountries(&$countries) {

        usort($countries, static function ($country1, $country2)  {

            /** @var ValueDictionary $country1 */
            /** @var ValueDictionary $country2 */

            $a = self::getCountryPriority($country1->getText());
            $b = self::getCountryPriority($country2->getText());

            if($a === $b) {

                if($country1->getText() < $country2->getText()) {
                    return -1;
                }

                return 1;

            }

            if($a > $b) {
                return -1;
            }

            return 1;

        });

    }

    /**
     * @param $platformPermission
     * @return bool
     */
    private function canAddSpecialRequest($platformPermission): bool
    {
        return (count(array_intersect($platformPermission, ['78', '84', '85', '86'])) > 0);
    }

    /**
     * @param $platformPermission
     * @return bool
     */
    private function displayStatusControl($platformPermission): bool
    {

        if (count($platformPermission) === 1 && in_array('78', $platformPermission, true)) {
            return false;
        }

        return true;

    }

    /**
     * @Rest\Route("/find-cases", name="ccv_find_cases", options={"expose":true})
     * @Method("GET")
     * @param Request $request
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function findCasesAction(Request $request)
    {

        $processHandler = $this->get('case.process_handler');

        $filters = $request->query->get('case_search');

        $em = $this->getDoctrine()->getManager();
        $stopWatch = new Stopwatch();

        /** @var User $user */
        $user = $this->getUser();

        $event = $stopWatch->start('find_cases');

        /** @var array $platformPermission */
        list($platformPermission, $companyKey) = $processHandler->getCompanyPlatformPermissions($user);

        $canFilterPlatforms = $this->canFilterPlatforms($companyKey);

        if ($canFilterPlatforms && !empty($filters['platform'])) {
            if(!is_array($filters['platform'])) {
                $platformPermission = explode(',', $filters['platform']);
            }
            else {
                $platformPermission = $filters['platform'];
            }
        }

        $cases = $processHandler->findCFMCases(implode(',', $platformPermission), $filters);

        $event->stop();

        $log = new Log();
        $log->setName('ccv_find_cases');
        $log->setContent(json_encode($filters));
        $log->setParam1($event->getDuration());
        $log->setParam2(implode(',', $platformPermission));
        $log->setParam3(count($cases));
        $log->setCreatedBy($user);

        $em->persist($log);
        $em->flush();

        return new JsonResponse([
            'cases' => $cases
        ]);

    }

    /**
     * @param $companyKey
     * @return bool
     */
    private function canFilterPlatforms($companyKey): bool
    {

        return in_array($companyKey, [
            AtlasViewerService::COMPANY_KEY_STARTER,
            AtlasViewerService::COMPANY_KEY_RDA,
            AtlasViewerService::COMPANY_KEY_PZU], true);

    }

    /**
     * @Rest\Route("/get-form-of-service/{groupProcessId}/{serviceId}", name="get_form_of_service", options={"expose":true})
     * @Method("GET")
     * @param $groupProcessId
     * @param $serviceId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getFormForServiceAction($groupProcessId, $serviceId)
    {

        /** @var ProcessInstance $groupProcess */
        $groupProcess = $this->getDoctrine()->getRepository(ProcessInstance::class)->find($groupProcessId);

        if (empty($groupProcess)) {
            return new JsonResponse([
                'msg' => 'Nie znaleziono usługi.',
            ], 401);
        }

        if (!$this->checkPermissionForCase($groupProcess->getRoot()->getId())) {
            return new JsonResponse([
                'msg' => 'Brak uprawnień',
            ], 401);
        }

        $atlasViewerService = $this->get('app.atlas_viewer');

        if ($formType = $atlasViewerService->isFormForGenerator($groupProcess, $serviceId)) {

            $form = $atlasViewerService->createFormForService($groupProcess, $formType, null, [
                'action' => $this->generateUrl('save_form_of_service', [
                    'groupProcessId' => $groupProcess->getId(),
                    'serviceId' => $serviceId
                ])
            ]);

            $view = $this->renderView('@App/CFMCaseViewer/Templates/service_extra_form.html.twig', [
                'form' => $form->createView(),
                'formType' => $formType,
                'content' => $atlasViewerService->getExtraContentForForm($groupProcess, $formType)
            ]);

            return new JsonResponse([
                'form' => $view
            ]);

        }

        return new JsonResponse([
            'form' => null
        ]);

    }


    /**
     * @Rest\Route("/save-form-of-service/{groupProcessId}/{serviceId}", name="save_form_of_service", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @param $groupProcessId
     * @param $serviceId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveFormForServiceAction(Request $request, $groupProcessId, $serviceId)
    {

        /** @var ProcessInstance $groupProcess */
        $groupProcess = $this->getDoctrine()->getRepository(ProcessInstance::class)->find($groupProcessId);

        if (empty($groupProcess)) {
            return new JsonResponse([
                'msg' => 'Nie znaleziono usługi.',
            ], 401);
        }

        if (!$this->checkPermissionForCase($groupProcess->getRoot()->getId())) {
            return new JsonResponse([
                'msg' => 'Brak uprawnień',
            ], 401);
        }

        $atlasViewerService = $this->get('app.atlas_viewer');

        $formType = $request->request->get('form_type');

        if (empty($formType)) {
            return new JsonResponse([
                'msg' => 'Nie znaleziono usługi.',
            ], 401);
        }

        $form = $atlasViewerService->createFormForService($groupProcess, $formType, null, [
            'action' => $this->generateUrl('save_form_of_service', [
                'groupProcessId' => $groupProcess->getId(),
                'serviceId' => $serviceId
            ])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $atlasViewerService->saveForm($formType, $form, $this->getUser());

            if($formType === ExtendRentalType::class) {

                $view = '<div class="alert alert-success">Wynajem został przedłużony. </div>';

            }
            else {

                $view = $this->renderView('@App/CFMCaseViewer/Templates/service_extra_form.html.twig', [
                    'form' => $form->createView(),
                    'formType' => $formType,
                    'content' => $atlasViewerService->getExtraContentForForm($groupProcess, $formType)
                ]);

            }

            return new JsonResponse([
                'valid' => true,
                'form' => $view,
            ]);

        } else {

            $view = $this->renderView('@App/CFMCaseViewer/Templates/service_extra_form.html.twig', [
                'form' => $form->createView(),
                'formType' => $formType,
                'content' => $atlasViewerService->getExtraContentForForm($groupProcess, $formType)
            ]);

            return new JsonResponse([
                'valid' => false,
                'form' => $view
            ]);

        }

    }


    /**
     * @Rest\Route("/get-refresh-dimension-date", name="get_refresh_dimension_date", options={"expose":true})
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRefreshDimensionDateAction()
    {

        $date = $this->get('app.data_warehouse_service')->getDimensionRefreshDate();
        return new JsonResponse($date);

    }


    /**
     * @Rest\Route("/change-coordinator", name="ccv_change_coordinator", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeCoordinatorInCaseAction(Request $request)
    {

        $coordinatorId = $request->request->get('coordinator_id');
        $rootId = intval($request->request->get('root_id'));

        if (!$this->checkPermissionForCase($rootId)) {
            return new JsonResponse([
                'msg' => 'Brak uprawnień',
            ], 401);
        }

        $processHandler = $this->get('case.process_handler');
        $userInfo = $this->get('user.info');
        
        $processHandler->editAttributeValue('166', $rootId, $coordinatorId, 'int', 'xxx', $userInfo->getUserId(), $userInfo->getOriginalUserId());

        return new JsonResponse([
            'status' => true,
            'message' => 'Koordynator został zmieniony'
        ]);

    }

    /**
     * @Rest\Route("/change-claim-number", name="ccv_change_claim_number", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     */
    public function changeClaimNumberInCaseAction(Request $request)
    {

        $refresh = false;
        $name = $request->request->get('name');

        if ($name === 'case_number') {

            $claimNumber = $request->request->get('value');
            $rootId = $request->request->get('pk');

        } else {
            $claimNumber = $request->request->get('claim_number');
            $rootId = intval($request->request->get('root_id'));
        }

        if (!$this->checkPermissionForCase($rootId)) {
            return new JsonResponse([
                'msg' => 'Brak uprawnień',
            ], 401);
        }

        $processHandler = $this->get('case.process_handler');
        $userInfo = $this->get('user.info');

        /** @var ProcessInstance $rootProcessInstance */
        $rootProcessInstance = $this->getDoctrine()->getRepository(ProcessInstance::class)->find($rootId);

        $processHandler->editAttributeValue('1045',
            $rootProcessInstance->getId(),
            $claimNumber,
            AttributeValue::VALUE_STRING,
            Step::ALWAYS_ACTIVE,
            $userInfo->getUser()->getId(),
            $userInfo->getOriginalUser()->getId());

        $platformId = $processHandler->getAttributeValue('253', $rootProcessInstance->getId(), AttributeValue::VALUE_INT);

        /** Przy CareFleet wstawiamy kod autoryzacji - trzeba dodać notatkę */
        if($platformId == "78") {

            $rootNote = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->findOneBy([
                'rootProcess' => $rootProcessInstance->getId(),
                'attributePath' => '406'
            ]);

            $note = [
                'type' => 'text',
                'special' => 1,
                'content' => 'Został wprowadzony kod autoryzacji do sprawy: ' . $claimNumber,
                'direction' => 1
            ];

            $this->get('note.handler')->newNote($note, $rootNote, $rootProcessInstance->getId());

            $refresh = true;

        }

        return new JsonResponse([
            'status' => true,
            'message' => 'Pomyślnie zapisano',
            'refresh' => $refresh
        ]);

    }

    /**
     * @Rest\Route("/change-status-case", name="ccv_change_status_case", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changStatusCaseAction(Request $request)
    {

        $isClosed = filter_var($request->request->get('is_closed'), FILTER_VALIDATE_BOOLEAN);
        $rootId = intval($request->request->get('root_id'));

        if (!$this->checkPermissionForCase($rootId)) {
            return new JsonResponse([
                'msg' => 'Brak uprawnień',
            ], 401);
        }

        $processHandler = $this->get('case.process_handler');
        $userInfo = $this->get('user.info');

        // Jeżeli jest zamknięta to otwieramy, a jak otwiera to zamyka

        $value = ($isClosed) ? 0 : 1;
        $message = ($isClosed) ? 'Sprawa została otwarta.' : 'Sprawa została zamknięta';

        $processHandler->editAttributeValue('465', $rootId, $value, 'int', 'xxx', $userInfo->getUserId(), $userInfo->getOriginalUserId());

        return new JsonResponse([
            'status' => true,
            'message' => $message
        ]);

    }

    /**
     * @Rest\Route("/add-note-to-case", name="ccv_add_note", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNoteToCaseAction(Request $request)
    {

        $noteData = $request->request->get('note');

        $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($noteData['case_id']));

        if (!$processInstance || $processInstance->getId() !== $processInstance->getRoot()->getId() || empty($noteData['content'])) {
            return new JsonResponse([
                'msg' => 'Nie znaleziono sprawy',
            ], 404);
        }

        if (!$this->checkPermissionForCase($processInstance->getId())) {
            return new JsonResponse([
                'msg' => 'Brak uprawnień',
            ], 401);
        }

        $noteHandler = $this->get('note.handler');
        $userInfo = $this->get('user.info');

        $rootNote = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->findOneBy([
            'rootProcess' => $processInstance,
            'attributePath' => '406'
        ]);

        // TODO - zabezpieczenie, żeby niektóre firmy nie mogły dodawac reklamacji

        $isComplaint = (isset($noteData['complaint']) && filter_var($noteData['complaint'], FILTER_VALIDATE_BOOLEAN));

        $isSpecialRequest = (isset($noteData['special_request']) && filter_var($noteData['special_request'], FILTER_VALIDATE_BOOLEAN));

        $complaintCategoryValue = null;
        $complaintPriority = 1;

        if ($isComplaint) {

            $noteData['content'] = 'Reklamacja: <br>' . $noteData['content'];

            $complaintCategoryValue = (!empty($noteData['category'])) ? intval($noteData['category']) : null;
            $complaintPriority = (!empty($noteData['priority'])) ? intval($noteData['priority']) : 1;

            $noteData['content'] .= "<br><br>Priorytet: " . $this->getPriorityName($complaintPriority);

        }

        $noteStructure = $noteHandler->newNote([
            'type' => 'text',
            'content' => $noteData['content']
        ], $rootNote, $processInstance->getId());

        if (!empty($noteStructure)) {

            $options = [
                'isComplaint' => ($isComplaint) ? 1 : 0,
                'category' => $complaintCategoryValue,
                'priority' => $complaintPriority
            ];

            $this->get('case.process_handler')->createTaskFromNote($processInstance->getId(), $noteStructure['id'], $options, $userInfo->getUser()->getId());

        }

        if ($isSpecialRequest) {

            $this->sendSpecialRequest($this->getUser(), $processInstance, $noteData['content']);

        }

        return new JsonResponse([
            'status' => true,
            'message' => 'Notatka została dodana'
        ]);

    }

    /**
     * @Rest\Route("/special_request", name="ccv_send_special_request", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     */
    public function specialRequestInCaseAction(Request $request)
    {
        $rootId = $request->request->get('rootId');
        $content = $request->request->get('content', '');

        $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($rootId));

        if (!$processInstance || $processInstance->getId() !== $processInstance->getRoot()->getId()) {
            return new JsonResponse([
                'msg' => 'Nie znaleziono sprawy',
            ], 404);
        }

        if (!$this->checkPermissionForCase($processInstance->getId())) {
            return new JsonResponse([
                'msg' => 'Brak uprawnień',
            ], 401);
        }

        $this->sendSpecialRequest($this->getUser(), $processInstance, $content);

        return new JsonResponse([
            'status' => true
        ]);

    }

    private function sendSpecialRequest(User $user, ProcessInstance $processInstance, $content) {

        $rootNote = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->findOneBy([
            'rootProcess' => $processInstance,
            'attributePath' => '406'
        ]);

        if ($this->getParameter('atlas_environment') === 'prod') {
            $from = 'atlas-noreply@starter24.pl';
            $to = 'lz@starter24.pl,lzip@starter24.pl';
        } else {
            $from = 'atlas_test@starter24.pl';
            $to = 'atlas_test@starter24.pl';
        }

        $subject = 'Nietypowa prośba w sprawie ' . $processInstance->getCaseNumber();

        $body = $this->renderView('@App/Email/notification_about_special_request.html.twig', [
            'username' => $user->getName(),
            'content' => $content
        ]);

        $this->get('note.handler')->newEmail($rootNote, $to, $from, $subject, $body);

    }

    /**
     * @Rest\Route("/finish-rental", name="ccv_finish_rental", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     */
    public function finishRentalAction(Request $request)
    {

        $taskId = $request->request->get('taskId');
        $userId = $this->getUser()->getId();
        $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find($taskId);

        if (!$processInstance) {
            return new JsonResponse([
                'msg' => 'Nie znaleziono sprawy',
            ], 404);
        }

        $this->container->get('case.process_handler')->next($taskId,20,$userId);


        return new JsonResponse([
            'status' => true,
            'message' => 'Zadanie na zakończenie wynajmu zostało pomyślnie utworzone'
        ]);

    }

    private function getPriorityName($priority)
    {

        switch ($priority) {
            case 1:
                {
                    return 'Niski';
                }
            case 5:
                {
                    return 'Średni';
                }
            case 10:
                {
                    return 'Wysoki';
                }
        }

    }

    /**
     * @Rest\Route("/find-case/{rootId}", name="ccv_find_case", options={"expose":true})
     * @Method("GET")
     * @param Request $request
     * @param $rootId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Exception
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     */
    public function findCaseAction(Request $request, $rootId)
    {

        $processHandler = $this->get('case.process_handler');
        $attributeParser = $this->get('case.attribute_parser.service');
        $atlasViewerService = $this->get('app.atlas_viewer');

        /** @var User $user */
        $user = $this->getUser();
        $html = '';

        $isRoot = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy([
            'root' => intval($rootId)
        ], [], 1);

        if (!empty($isRoot)) {

            if (!$this->checkPermissionForCase($rootId)) {
                return new JsonResponse([
                    'msg' => 'Brak uprawnień',
                ], 401);
            }

            $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($rootId));

            if ($processInstance) {

                /** @var string $platformId */
                $platformId = $processHandler->getAttributeValue('253', $processInstance->getRootId(), 'int');
                $showDetailedStatus = $processHandler->getBusinessConfig('viewer_detailed_service_status',$processInstance->getGroupProcess()->getId());
                $chatMessageIsEnabled = $atlasViewerService->isChatEnabled($platformId, $user->getId());

                $this->setStatusServices();
                $this->setStatusProgress();

                $services = $processHandler->getStatusServicesViewer($processInstance->getId(), $request->getLocale(), 1);

                if (count($services) > 0) {
                    foreach ($services as $key => $service) {

                        // POPR2554 - W PZU nie pokazuje usługi Reklamacji
                        if($service['serviceId'] === self::CLAIM_SERVICE_ID) {
                            unset($services[$key]);
                            continue;
                        }

                        $attributeParser->setGroupProcessInstanceId($service['group_process_id']);
                        if($showDetailedStatus) {
                            $services[$key]['status_label'] = $attributeParser->parseString($service['status_label']);
                        }
                        else {
                            $services[$key]['status_label'] = $attributeParser->parseString($this->getStatusServiceForCFM($service['serviceId'], $service['status_dictionary_id'], $service['status_label']));
                        }
                        $services[$key]['status_progress'] = $this->getOtherProgress($service['status_dictionary_id'], $service['status_progress']);
                        $services[$key]['program_name'] = $attributeParser->parseString('{@202@}');

                        if($platformId === '78') {
                            $services[$key]['total_cost'] = $processHandler->totalCostOfService($service['group_process_id']);
                        }

                    }
                }

                $servicesTable = $this->renderView('@App/CFMCaseViewer/Templates/list_services.html.twig', [
                    'services' => $services,
                    'platformId' => $platformId
                ]);

                $infoViewer = $this->container->get('case.info_for_viewer');

                $caseInfo = $infoViewer->getTableOf(intval($rootId), 'CaseInfo');

                $claimInfoTable = $infoViewer->getTableOf(intval($rootId), 'ClaimForm');

                $canAddComplain = $this->checkPermissionForClaim($processHandler);

                $canSetTotalDamage = ($platformId === '25');

                $canAddSpecialRequest = $this->canAddSpecialRequest([$platformId]);

                $addClaimControl = (in_array($platformId, ['25', '78'], true));

                $claimNumber = null;
                $isClosed = 0;
                $whoClose = '';
                $isTotalDamage = 0;

                if ($addClaimControl) {
                    $claimNumber = $processHandler->getAttributeValue('1045', $processInstance->getRootId(), 'string');
                }

                if($canSetTotalDamage) {
                    $isTotalDamage  = $processHandler->getAttributeValue('1088', $processInstance->getRootId(), 'int');
                }
                /**
                 * Wszystkie platformy oprócz CareFleet mogą zamykać sprawy
                 */
                $canCloseCase = (!in_array($platformId, ['78', '84', '85', '86'], true));

                if ($canCloseCase) {

                    $isClosedArray = $processHandler->getAttributeValue('465', $processInstance->getRootId());

                    if (is_array($isClosedArray)) {

                        if (empty($isClosedArray)) {
                            $isClosed = 0;
                        } else {
                            $isClosed = $isClosedArray[0]['value_int'];

                            $attributeValue = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->find($isClosedArray[0]['id']);

                            /** @var User $user */
                            $userTemp = $attributeValue->getUpdatedBy();

                            if (empty($userTemp)) {
                                $userTemp = $attributeValue->getCreatedBy();
                            }

                            if (!empty($userTemp) && $userTemp instanceof User) {

                                $company = $userTemp->getCompany();

                                if ($userTemp->getId() === 1) {
                                    $whoClose = 'System';
                                } elseif ($company && $company->getId() === 1) {
                                    $whoClose = 'Pracownik Starter24.pl';
                                } else {
                                    $whoClose = $userTemp->getUsername();
                                }

                            } else {
                                $whoClose = '?';
                            }

                        }
                    }

                }

                $canSetCoordinator = (!in_array($platformId, ['84', '85', '86'], true));

                $updatePreviewCase = ($platformId === '25');

                if ($updatePreviewCase && !$user->hasRole(User::ROLE_USER_AS_SYSTEM)) {
                    $processHandler->editAttributeValue('339', $processInstance->getRootId(), (new \DateTime()), AttributeValue::VALUE_DATE, 'xxx', $user->getId(), $user->getId());
                }

                $canDownloadDocuments = ($platformId === '25');
                $documentStatus = null;

                if ($canDownloadDocuments) {
                    $documentStatus = $processHandler->getAttributeValue('399', $processInstance->getRootId(), 'int');
                    if (is_array($documentStatus) && empty($documentStatus)) {
                        $documentStatus = null;
                    }
                }

                $finishRentalId = null;

                if($platformId === '78'){
                    $finishRentalId = $this->container->get('case.handler')->getActiveRentalAutomat($rootId);
                }

                $html = $this->renderView('@App/CFMCaseViewer/Templates/case_details.html.twig',
                    [
                        'services' => $servicesTable,
                        'platformId' => $platformId,
                        'root_id' => $rootId,
                        'case_info' => $caseInfo,
                        'active_coordinator' => $processHandler->getAttributeValue('166', $rootId, 'int'),
                        'coordinators' => $this->getCoordinatorsFromCompany($user->getCompany()),
                        'can_add_complain' => $canAddComplain,
                        'claim_table' => $claimInfoTable,
                        'claim_control' => $addClaimControl,
                        'claimNumber' => (empty($claimNumber)) ? null : $claimNumber,
                        'isClosed' => $isClosed,
                        'whoClose' => $whoClose,
                        'canCloseCase' => $canCloseCase,
                        'canDownloadDocuments' => $canDownloadDocuments,
                        'documentStatus' => $documentStatus,
                        'canAddSpecialRequest' => $canAddSpecialRequest,
                        'finish_rental_id' => $finishRentalId,
                        'canSetTotalDamage' => $canSetTotalDamage,
                        'isTotalDamage' => $isTotalDamage,
                        'chatMessageIsEnabled' => $chatMessageIsEnabled,
                        'canSetCoordinator' => $canSetCoordinator,
                        'isRDA' => (in_array($platformId, ['84', '85', '86'], true))
                    ]
                );

            }

        } else {
            return new JsonResponse([
                'msg' => 'Nie znaleziono sprawy'
            ], 404);
        }

        return new JsonResponse([
            'html' => $html,
            'rootId' => $rootId,
        ]);

    }

    /**
     * @Rest\Route("/create-case", name="ccv_create_case", options={"expose":true})
     * @param Request $request
     * @param $rootId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Exception
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     */
    public function createCaseFromViewer(Request $request)
    {

        $processHandler = $this->get('case.process_handler');

        /** @var array $platformPermission */
        $platformPermission = $processHandler->getPlatformsForUser($this->getUser()->getId());

        // TYLKO DLA CAREFLEET
        if ($platformPermission !== '78') {
            $this->createAccessDeniedException();
        }

        $platformId = reset($platformPermission);

        $caseViewer = new NewCaseViewer(
            $request,
            $this->get('form.factory'),
            $this->get('case.process_handler'),
            $this->getUser(),
            $this->get('note.handler')
        );

        $caseViewer->setPlatform($platformId);

        /** @var Platform $platform */
        $platform = $this->get('doctrine.orm.entity_manager')->getRepository(Platform::class)->find($platformId);

        /** @var FormInterface $form */
        $form = $caseViewer->createForm();

        if ($request->isMethod(Request::METHOD_GET)) {

            return $this->render('@App/CFMCaseViewer/Templates/create_case_from_viewer.html.twig', [
                'form' => $form->createView()
            ]);

        } elseif ($request->isMethod(Request::METHOD_POST)) {

            $rootId = $caseViewer->submitForm($form);

            $data = $form->getData();

            $formHtml = '';

            if (empty($rootId)) {
                $formHtml = $this->renderView('@App/CFMCaseViewer/Templates/create_case_from_viewer.html.twig', ['form' => $form->createView()]);
            }

            if (!empty($rootId)) {

                $registerNumber = ($data['car_register_number'] ?? '');

                /**
                 * Na końcu wysłanie e-maila do KZ
                 */

                $from = ($this->getParameter('atlas_environment') === 'prod') ? 'atlas-noreply@starter24.pl' : 'atlas_test@starter24.pl';

                /** @var ConnectedMailbox $mailboxNoReply */
                $mailboxNoReply = $this->get('doctrine.orm.entity_manager')->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
                    'name' => $from
                ]);

                $simpleSender = new SimpleOutlookSenderService($mailboxNoReply);

                $body = $this->renderView('@App/Email/notification_about_new_case.html.twig', [
                    'data' => $data,
                    'case_number' => ParserMethods::formattingCaseNumber($rootId)
                ]);

                $parser = $this->get('case.attribute_parser.service');
                $parser->setGroupProcessInstanceId($rootId);

                $body = $parser->parse($body);

                $to = ($this->getParameter('atlas_environment') === 'prod') ? 'lz@starter24.pl,lzip@starter24.pl' : 'atlas_test@starter24.pl';

                $simpleSender->send($to, 'Nowa sprawa ' . $platform->getName() . ' z Atlas Viewer - ' . ParserMethods::formattingCaseNumber($rootId) . '/' . $registerNumber, $body);

            }

            return new JsonResponse([
                'rootId' => $rootId,
                'form' => $formHtml
            ]);

        }

        $this->createNotFoundException();

        return new JsonResponse([]);

    }

    /**
     * @param $serviceId
     * @param $statusDictionaryId
     * @param string $default
     * @return string
     */
    private function getStatusServiceForCFM($serviceId, $statusDictionaryId, $default = '')
    {

        if (isset($this->statusServices[$serviceId])) {
            if (isset($this->statusServices[$serviceId][$statusDictionaryId])) {
                return $this->statusServices[$serviceId][$statusDictionaryId];
            }
        }

        return $default;

    }

    /**
     * @param $statusDictionaryId
     * @param int $defaultProgress
     * @return int|mixed
     */
    private function getOtherProgress($statusDictionaryId, $defaultProgress = 1)
    {

        if (isset($this->statusProgress[$statusDictionaryId])) {
            return $this->statusProgress[$statusDictionaryId];
        }

        return $defaultProgress;

    }

    private function setStatusProgress()
    {

        $this->statusProgress = [
            43 => 4,
            36 => 2,
            37 => 2,
            28 => 4,
            30 => 4,
            92 => 2,
            1033 => 4,
            65 => 2,
            1007 => 4,
            1011 => 4
        ];

    }

    private function setStatusServices()
    {

        $this->statusServices = [];

        $this->statusServices[1] = [
            '-1' => 'Anulowano',
            '34' => 'Czeka na organizację',
            '35' => 'Czeka na organizację',
            '36' => 'W trakcie realizacji usługi',
            '37' => 'W trakcie realizacji usługi',
            '38' => 'W trakcie realizacji usługi',
            '39' => 'W trakcie realizacji usługi',
            '40' => 'W trakcie realizacji usługi',
            '41' => 'W trakcie realizacji usługi',
            '42' => 'W trakcie realizacji usługi',
            '43' => 'Zakończono holowanie.',
            '44' => 'Zakończono holowanie',
            '45' => 'Anulowano',
            '46' => 'Anulowano',
            '55' => 'W trakcie realizacji usługi'
        ];

        $this->statusServices[2] = [
            '-1' => 'Anulowano',
            '23' => 'Czeka na organizację',
            '24' => 'Czeka na organizację',
            '25' => 'W trakcie realizacji usługi',
            '26' => 'W trakcie realizacji usługi',
            '27' => 'W trakcie realizacji usługi',
            '28' => 'Kontraktor naprawił pojazd na miejscu',
            '29' => 'Kontraktor nie naprawił pojazdu na miejscu',
            '30' => 'Kontraktor naprawił pojazd na miejscu',
            '31' => 'Anulowane',
            '32' => 'Anulowane',
            '33' => 'Anulowane',
            '56' => 'W trakcie realizacji usługi',
            '1000' => 'Zakończone naprawą przez telefon',
            '1005' => 'Zakończono usługę naprawy na drodze'
        ];

        $this->statusServices[3] = [
            '-1' => 'Anulowano',
            '90' => 'Czeka na organizację',
            '91' => 'Czeka na organizację',
            '92' => 'W trakcie realizacji usługi',
            '93' => 'W trakcie realizacji usługi',
            '94' => 'Wynajem trwa. Liczba dni wynajmu: {#rentalSummary(real_duration)#}, koniec wynajmu: {#rentalSummary(end_date)#}',
            '95' => 'Wynajem trwa. Liczba dni wynajmu: {#rentalSummary(real_duration)#}, koniec wynajmu: {#rentalSummary(end_date)#}',
            '96' => 'Zakończono wynajem',
            '97' => 'Anulowano',
            '98' => 'Anulowano',
            '99' => 'Anulowano',
            '1001' => 'W trakcie realizacji usługi',
            '1033' => 'Zakończono wynajem',
            '1034' => 'W trakcie realizacji usługi',
        ];

        $this->statusServices[4] = [
            '-1' => 'Anulowano',
            '58' => 'Czeka na organizację',
            '61' => 'Anulowano',
            '62' => 'Anulowano',
            '63' => 'Anulowano'
        ];

        $this->statusServices[5] = [
            '-1' => 'Anulowano',
            '64' => 'Czeka na organizację',
            '65' => 'W trakcie realizacji usługi',
            '66' => 'W trakcie realizacji usługi',
            '67' => 'W trakcie realizacji usługi',
            '69' => 'Anulowano',
            '70' => 'Anulowano',
            '71' => 'Anulowano'
        ];

        $this->statusServices[6] = [
            '-1' => 'Anulowano',
            '72' => 'Czeka na organizację',
            '75' => 'Anulowano',
            '76' => 'Anulowano',
            '77' => 'Anulowano'
        ];

        $this->statusServices[7] = [
            '-1' => 'Anulowano',
            '83' => 'Czeka na organizację',
            '86' => 'Anulowano',
            '87' => 'Anulowano',
            '88' => 'Anulowano',
            '1035' => 'Anulowano'
        ];

        $this->statusServices[14] = [
            '-1' => 'Anulowano',
            '1009' => 'Anulowano',
            '1010' => 'Anulowano',
            '1007' => 'Zakończono',
            '1011' => 'Zakończono'
        ];

    }


    private function getCoordinatorsFromCompany($companyId)
    {

        return $this->getDoctrine()->getRepository('UserBundle:User')->findBy([
            'company' => $companyId,
            'enabled' => 1
        ]);

    }

    /**
     * @param int $rootId
     * @return array
     */
    private function getCoordinatorsForCase(int $rootId)
    {

        $platformOfCase = $this->get('case.process_handler')->getAttributeValue('253', $rootId, 'int');
        return $this->getCoordinators([$platformOfCase]);

    }

    /**
     * @param array $platformsId
     * @return array
     */
    private function getCoordinators(array $platformsId)
    {

        return $this->get('case.process_handler')->getCoordinatorsByPlatforms($platformsId);

    }

    /**
     * Sprawdza czy użytkownik ma uprawnienia do podglądu zadania
     *
     * @param $rootId
     * @param null $processHandler
     * @return bool
     */
    private function checkPermissionForCase($rootId, $processHandler = null)
    {

        if ($this->getUser()->isAdmin()) return true;

        if ($processHandler === null) $processHandler = $this->get('case.process_handler');

        $platformPermission = $processHandler->getPlatformsForUser($this->getUser()->getId());
        $platformOfCase = $processHandler->getAttributeValue('253', $rootId, 'int');

        if (in_array($platformOfCase, $platformPermission)) {

            return true;
        }

        return false;
    }

    /**
     * Sprawdza czy użytkownik ma opcję "Dodaj reklamacje"
     *
     * @param $processHandler
     * @param null $platformPermission
     * @return bool
     */
    private function checkPermissionForClaim($processHandler = null)
    {

        if ($this->getUser()->isAdmin()) {
            return true;
        }

        if ($processHandler === null) {
            $processHandler = $this->get('case.process_handler');
        }

        $platformPermission = $processHandler->getPlatformsForUser($this->getUser()->getId());

        if (in_array('53', $platformPermission, true)) {
            return true;
        }

        return false;

    }

    /**
     * @Rest\Route("/change-total-damage", name="ccv_change_total_damage", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeTotalDamageAction(Request $request)
    {

//        $name = $request->request->get('name');
        $value = $request->request->get('value');
        $rootId = $request->request->get('root_id');

        return $this->changeValueInCase($rootId, '1088', $value, 'int');

    }

    /**
     * @Rest\Route("/change-coordinators-note", name="ccv_change_coordinators_note", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeCoordinatorsNoteAction(Request $request)
    {

//        $name = $request->request->get('name');
        $value = $request->request->get('value');
        $rootId = $request->request->get('pk');

        return $this->changeValueInCase($rootId, '1020', $value, 'text');

    }


    /**
     * @Rest\Route("/documents-request", name="ccv_documents_request", options={"expose":true})
     * @Method("POST")
     * @param Request $request
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sendRequestAboutDocumentsAction(Request $request)
    {

        $rootId = $request->request->get('root_id');
        $action = $request->request->get('action');

        if ($action === 'send_request') {
            return $this->changeValueInCase($rootId, '399', 1, 'int');
        }

        return new Response();

    }

    /**
     * @param $rootId
     * @param $path
     * @param $value
     * @param $type
     * @return Response
     */
    private function changeValueInCase($rootId, $path, $value, $type) {

        if (!$this->checkPermissionForCase($rootId)) {
            return new Response('Brak uprawnień', 401);
        }

        $processHandler = $this->get('case.process_handler');
        $userInfo = $this->get('user.info');

        $processHandler->editAttributeValue($path, $rootId, $value, $type, 'xxx', $userInfo->getUserId(), $userInfo->getOriginalUserId());

        return new Response();

    }

    /**
     * @Rest\Route("/documents-preview/{rootId}", name="ccv_documents_preview", options={"expose":true})
     * @Method("GET")
     * @param $rootId
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     */
    public function previewDocumentsAction($rootId)
    {

        if (!$this->checkPermissionForCase($rootId)) {
            return new Response('Brak uprawnień', 401);
        }

        $processHandler = $this->get('case.process_handler');

        $documentsStatus = $processHandler->getAttributeValue('399', $rootId, 'int');

        if (intval($documentsStatus) !== 2) {
            return new Response('Brak uprawnień', 401);
        }

        $documents = $processHandler->getDocumentsInVDesk(ParserMethods::formattingCaseNumber($rootId), 1);

        foreach ($documents as $key => $document) {
            $documents[$key]['documentId'] = hash('ripemd160', $document['documentId']);
        }

        return $this->render('@App/CFMCaseViewer/Templates/vdesk_documents_preview.html.twig', [
            'documents' => $documents,
            'rootId' => $rootId
        ]);


    }

    /**
     * @Rest\Route("/documents-preview/{rootId}/{hashDocumentId}", name="ccv_documents_download", options={"expose":true})
     * @Method("GET")
     * @param $rootId
     * @param $hashDocumentId
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     */
    public function downloadDocumentsAction($rootId, $hashDocumentId)
    {

        if (!$this->checkPermissionForCase($rootId)) {
            return new Response('Brak uprawnień', 401);
        }

        $processHandler = $this->get('case.process_handler');

        $documents = $processHandler->getDocumentsInVDesk(ParserMethods::formattingCaseNumber($rootId), 1);

        $documentId = null;
        $documentName = null;

        foreach ($documents as $key => $document) {

            if ($hashDocumentId == hash('ripemd160', $document['documentId'])) {
                $documentId = $document['documentId'];
                $documentName = $document['name'];
                break;
            }

        }

        if ($documentId === null) {
            return new Response('Dokument nie istnieje', 404);
        }

        return $this->downloadDocumentFromVDesk($documentId, $documentName);

    }

    /**
     * @param $documentId
     * @param $documentName
     * @return Response
     */
    private function downloadDocumentFromVDesk($documentId, $documentName)
    {

        // TODO - można by TOKEN i DOMENE przenieść do parameters.yml

        $url = 'http://vdesk.starter24.pl/vdeskimp/index.php?page=document.download&sx_session_token=70A57FED-493A-42E0-AD30-231735484BB0&DocumentId=' . $documentId;

        $pdf = file_get_contents($url);

        $response = new Response($pdf);

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $documentName
        );

        $response->headers->set('Content-length', strlen($pdf));
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', 'application/pdf');
        $response->headers->set('Content-Disposition', $disposition);

        return $response;

    }

//    /**
//     * @Rest\Route("/change-number-case", name="ccv_change_number_case", options={"expose":true})
//     * @Method("POST")
//     * @param Request $request
//     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
//     * @return \Symfony\Component\HttpFoundation\Response
//     */
//    public function changeNumberCaseAction(Request $request){
//
//
////        $name = $request->request->get('name');
//        $value = $request->request->get('value');
//        $rootId = $request->request->get('pk');
//
//        if(!$this->checkPermissionForCase($rootId)) {
//            return new Response('Brak uprawnień', 401);
//        }
//
//        $processHandler = $this->get('case.process_handler');
//        $userInfo = $this->get('user.info');
//
//        $processHandler->editAttributeValue('1020', $rootId, $value, 'text', 'xxx', $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());
//
//        return new Response();
//
//    }


    /**
     * @Rest\Route("/export-view", name="ccv_export_view", options={"expose":true})
     * @Method("GET")
     * @param Request $request
     * @return Response|HttpException
     * @throws \Box\Spout\Common\Exception\IOException
     * @Security("is_granted('ROLE_CFM_CASE_VIEWER')")
     */
    public function exportViewAction(Request $request)
    {

        set_time_limit(0);
        ini_set('memory_limit', '1G');
        ini_set('mssql.connect_timeout', -1);

        $processHandler = $this->get('case.process_handler');
        $filters = $request->query->get('case_search');

        try {

            /** @var User $user */
            $user = $this->getUser();

            if (!empty($filters['platform']) && $user->getCompanyId() === 1) {

                if(is_array($filters['platform'])){
                    $platformPermission = $filters['platform'];
                }
                else {
                    $platformPermission = explode(',', $filters['platform']);
                }

            } else {
                $platformPermission = $processHandler->getPlatformsForUser($this->getUser()->getId());
            }

            $filters['limit'] = 10000;

            $cases = [];
            $casesData = $processHandler->findCFMCases(implode(',', $platformPermission), $filters);
            $columnsData = $processHandler->findCFMCases(implode(',', $platformPermission), [], true);

            $columnsData = array_filter($columnsData, static function ($a) {
                return ($a['data'] !== 'id');
            });

            $columns = array_map(static function ($col) {
                return $col['title'];
            }, $columnsData);

            $newRow = [];

            /** Przebudowanie tablicy, żeby się zgadzała z kolumnami. */
            foreach ($casesData as $item) {

                unset($newRow);
                $newRow = [];

                foreach ($columnsData as $columnsDatum) {
                    $newRow[] = $item[$columnsDatum['data']];
                }

                $cases[] = $newRow;

            }

            unset($casesData);

            $xmlService = new CasesToXlsService($this->container);

            $fileName = 'Atlas_Viewer_Raport_' . (new \DateTime())->format('Ymd_His');

            return $xmlService->generateXlsResponse($cases, $columns, $fileName);

        } catch (\Exception $exception) {

            $em = $this->getDoctrine()->getManager();

            $log = new Log();
            $log->setName('ccv_export_view');
            $log->setContent($exception->getTraceAsString());
            $log->setParam1($exception->getMessage());
            $log->setCreatedBy($this->getUser());
            $log->setParam2(json_encode($filters));

            $em->persist($log);
            $em->flush();

            return new Response('Wystąpił problem z pobraniem raportu. Administrator został już powiadomiony.', 500);

        }

    }
}