<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Log;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class JsLoggerController extends Controller
{
    /**
     * @Route("/js-logger", name="js_logger", options={"expose"=true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */

    public function indexAction(Request $request)
    {
        $data = $request->request->all();
        $error = json_encode($data);

        $em  = $this->getDoctrine()->getManager();
        $log = new Log();
        $log->setName('JS ERROR');
        $log->setContent($error);

        $em->persist($log);
        $em->flush();

        return new JsonResponse([
            "status" => true
        ]);

    }

}
