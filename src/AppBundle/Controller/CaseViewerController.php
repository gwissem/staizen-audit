<?php
namespace AppBundle\Controller;

use AppBundle\Utils\QueryManager;
use CaseBundle\Entity\AttributeValue;
use CaseBundle\Entity\ProcessInstance;
use CaseBundle\Service\CaseHandler;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use FOS\RestBundle\Controller\Annotations as Rest;
use PhpMimeMailParser\Parser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use MssqlBundle\PDO\PDO;
/**
 * Class CaseViewerController
 * @package ManagementBundle\Controller
 * @Rest\Route("/caseViewer")
 */
class CaseViewerController extends Controller
{

    /** @var QueryManager */
    private $queryManager;

    /**
     * @Rest\Route("/index", name="defaultCaseViewer")
     * @Method("GET")
     * @param Request $request
     * @Security("is_granted('ROLE_CASE_VIEWER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request){

        return $this->render('AppBundle:CaseViewer:index.html.twig');


    }

    /**
     * @Rest\Route("/get-services-table", name="caseViewerSearch", options={"expose":true})
     * @Security("is_granted('ROLE_CASE_VIEWER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request)
    {
        $this->queryManager = $this->container->get('app.query_manager');
        $casesFound = [];
        $processInstanceMatrix = null;

        $searchQuery = $request->query->get('searchQuery', null);
        $searchType = (int)$request->query->get('searchType', 0);

        if($searchQuery){

            if($searchType == 0){
                $searchQuery =  CaseHandler::parseCaseNumber($searchQuery);
            }

            $user=  $this->getUser();
            $userId = $user->getId();

            $parameters = [
                [
                    'key' => 'searchQuery',
                    'value' => $searchQuery,
                    'type' => PDO::PARAM_STR,
                    'length' => 200
                ],
                [
                    'key' => 'searchType',
                    'value' => (int)$searchType,
                    'type' => PDO::PARAM_INT
                ],
                [
                    'key' =>'userId',
                    'value' => (int)$userId,
                    'type' => PDO::PARAM_INT
            ]
                ];

            $casesFound = $this->queryManager->executeProcedure(
                "EXECUTE dbo.p_searchCaseForViewer @searchQuery = :searchQuery, @searchType = :searchType, @userId = :userId",
                $parameters
            );

        }
        return new JsonResponse([
            'casesFound' => $casesFound,
        ]);


    }

    /**
     * @Rest\Route("/case-viewer/save-attributes", name="caseViewerSaveAttributes", options={"expose":true})
     * @Security("is_granted('ROLE_CASE_VIEWER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAttributesAction(Request $request)
    {

        $processHandler = $this->get('case.process_handler');

        $groupProcessId = $request->request->get('group-process-id', null);
        $attributes = $request->request->get('attributes', []);

        // Nie chciało mi się inaczej :D
        $types = [
            "985,986" => "int",
            "985,317" => "string"
        ];

        $userInfo = $this->get('user.info');

        if(!empty($attributes) && !empty($groupProcessId)) {

            foreach ($attributes as $path => $value) {

                $type = (isset($types[$path])) ? $types[$path] : 'string';

                $processHandler->editAttributeValue($path, $groupProcessId, $value, $type, 'xxx', $userInfo->getUser()->getId(), $userInfo->getOriginalUser()->getId());

            }

            try {

                // Dodanie notatki, że zmieniono autoryzacji dni wynajmu

                $noteHandler = $this->get('note.handler');

                $authCode = $attributes["985,317"];
                $days = $attributes["985,986"];

                /** @var ProcessInstance $groupProcess */
                $groupProcess = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find($groupProcessId);

                $rootNote = $this->getDoctrine()->getRepository('CaseBundle:AttributeValue')->findOneBy([
                    'rootProcess' => $groupProcess->getRoot(),
                    'attributePath' => '406'
                ]);

                $note = [
                    'type' => 'text',
                    'special' => 1,
                    'content' => "Wprowadzono autoryzację dla usługi Auto Zastępcze (" . $groupProcessId . "). <br>Łączna ilość dni wynajmu: " . $days . "<br>Kod autoryzacji: " . $authCode,
                    'direction' => 1
                ];

                $noteHandler->newNote($note, $rootNote, $groupProcess->getRoot()->getId());

            }
            catch (\Exception $exception) {

            }

        }

        return new JsonResponse([]);

    }

    /**
     * @Rest\Route("/viewcase", name="caseViewerView",options={"expose":true})
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @Security("is_granted('ROLE_CASE_VIEWER')")
     */
    public function viewAction(Request $request)
    {
        $servicesTable = false;
        $caseInfo = false;
        $processInstanceMatrix = false;
        $rootId = $request->query->get('rootId', null);

        $isGrantedFinanceView = $this->isGranted('ROLE_FINANCE_VIEW');
        $caseToolsService = $this->get('management.case_tools.service');

        if($rootId) {

            $rootId = CaseHandler::parseCaseNumber($rootId);

            $isRoot =  $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findBy([
                'root' => intval($rootId)
            ], [], 1);

            if(!empty($isRoot)) {

                $processInstance = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->find(intval($rootId));

                if($processInstance) {

                    $processHandler = $this->get('case.process_handler');
                    $attributeParser = $this->get('case.attribute_parser.service');

                    $services = $processHandler->getStatusServicesViewer($processInstance->getId(), $request->getLocale(), 1);

                    if(count($services) > 0) {
                        foreach ($services as $key => $service) {
                            $attributeParser->setGroupProcessInstanceId($service['group_process_id']);
                            $services[$key]['status_label'] = $attributeParser->parseString($service['status_label']);
                        }
                    }

                    $servicesTable = $this->renderView('AppBundle:CaseViewer/CaseTools:list_services_of_case_table.html.twig', [
                        'services' => $services
                    ]);

                    $caseInfo = $this->container->get('case.info_for_viewer')->getCaseInfo($processInstance);

                    if($processHandler->matrixExists($rootId)) {

                        $matrixStep = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance')->findStepWithMatrix($processInstance);

                        if($matrixStep instanceof ProcessInstance) {
                            $processInstanceMatrix = $matrixStep->getId();
                        }
                        else {
                            $processInstanceMatrix = $rootId;
                        }

                    }

                }

            }

        }

        return new JsonResponse([
            'services' => $servicesTable,
            'caseInfo' => $caseInfo,
            'matrixProcessInstance' => $processInstanceMatrix,
            'rootId' => $rootId,
            'rsList' => ($isGrantedFinanceView) ? $caseToolsService->getRsList($rootId) : '',
            'rzwList' => ($isGrantedFinanceView) ? $caseToolsService->getRZWList($rootId) : '',
            'emailAttachments' => ($isGrantedFinanceView) ? $caseToolsService->getEmailAttachmentsForCase($rootId) : '',
            'documents' => ($isGrantedFinanceView) ? $caseToolsService->getOtherDocumentsForCase($rootId) : ''
        ]);
    }


}