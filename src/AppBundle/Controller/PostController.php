<?php

namespace AppBundle\Controller;

use AppBundle\Listener\BlogFileManagerListener;
use DocumentBundle\Entity\Document;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Post;

/**
 * @Route("/post/")
 */
class PostController extends Controller
{
    /**
     * Lists all Post entities.
     *
     * @Route("/{page}", requirements={"page" = "\d+"}, name="admin_post_index")
     * @Security("is_granted('ROLE_BLOGGER')")
     * @Method({"GET"})
     * @param Request $request
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = 1)
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('AppBundle:Post')->getPosts();
        $perPage = 30;
        $pagination = $this->get('knp_paginator')->paginate($posts, $page, $perPage);

        return $this->render('AppBundle:Admin:post/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new Post entity.
     *
     * @Route("/new", name="admin_post_new")
     * @Security("is_granted('ROLE_BLOGGER')")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $postRepo = $em->getRepository('AppBundle:Post');
        $fileUploader = $this->container->get('document.uploader');

        $post = new Post();
        $form = $this->createForm('AppBundle\Form\Type\PostType', $post, [
            'typeOptions' => $postRepo->getPostTypeOptions(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $files = $form->get('document')->getData();

            $document = new Document();
            $name = $post->getTitle();
            $document->setName('blog_' . $name . '_' . date('Ymd_His'));
            $em->persist($document);
            $em->flush();

            if ($files) {
                foreach ($files as $fileInput) {
                    $file = $fileUploader->upload($fileInput);
                    $file->setDocument($document);
                    $em->persist($file);
                }
                /** W przypadku bloga, pliki może pobierać każdy */
                if($document->getCompanies()->count()) {
                    $document->getCompanies()->clear();
                }
            }
            $post->setDocument($document);

            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('admin_post_index');

        }

        return $this->render('AppBundle:Admin:post/editor.html.twig', array(
            'entity' => $post,
            'form' => $form->createView(),
        ));

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("{id}/edit", name="admin_post_edit")
     * @Security("is_granted('ROLE_BLOGGER')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $postRepo = $em->getRepository('AppBundle:Post');
        $fileUploader = $this->container->get('document.uploader');

        if($post->getDocument()){
            $documentId = $post->getDocument()->getId();
            $document = $em->getRepository('DocumentBundle:Document')->find($documentId);
        }
        else{
            $document = new Document();
            $document->setName('blog_' . $post->getTitle() . '_' . date('Ymd_His'));
        }

        $editForm = $this->createForm('AppBundle\Form\Type\PostType', $post, [
            'typeOptions' => $postRepo->getPostTypeOptions(),
        ]);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $files = $editForm->get('document')->getData();

            if (!empty($files)) {

                $name = $post->getTitle();

                if (null === $document) {
                    $document = new Document();
                    $document->setName('blog_' . $name . '_' . date('Ymd_His'));
                }

                foreach ($files as $fileInput) {
                    $file = $fileUploader->upload($fileInput);
                    $file->setDocument($document);
                    $em->persist($file);
                }

                $em->persist($document);
                $em->flush();

            }

            /** W przypadku bloga, pliki może pobierać każdy */
            if($document) {
                if($document->getCompanies()->count()) {
                    $document->getCompanies()->clear();
                }

                $post->setDocument($document);
            }

            $em->persist($post);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Post "%name%" został pomyślnie zedytowany',
                    ['%name%' => $post->getTitle()]
                )

            );

            return $this->redirect($request->headers->get('referer'));

        }

        return $this->render('AppBundle:Admin:post/editor.html.twig', array(
            'entity' => $post,
            'form' => $editForm->createView(),
        ));

    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/delete/{id}", name="admin_post_delete")
     * @Security("is_granted('ROLE_BLOGGER')")
     */
    public function deleteAction(Request $request, Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_post_index'));
    }


    /**
     * @Route("/file-manager-view", name="admin_post_file_manager_view", options={"expose"=true})
     * @param Request $request
     * @return JsonResponse
     * @Security("is_granted('ROLE_SUPPORT')")
     */
    public function fileManagerViewAction(Request $request)
    {

        $view = $this->renderView('@Web/Utils/upload_file_box.html.twig',
            [
                'files' => $this->getImageFiles()
            ]);

        return new JsonResponse([
            'view' => $view
        ]);

    }

    /**
     * @Route("/file-manager-list", name="admin_post_file_manager_list", options={"expose"=true})
     * @param Request $request
     * @Security("is_granted('ROLE_SUPPORT')")
     * @return JsonResponse
     */
    public function fileManagerListAction(Request $request)
    {

        $list = $this->renderView('@Web/Utils/upload_file_list.html.twig', [
            'files' => $this->getImageFiles()
        ]);

        return new JsonResponse([
            'list' => $list
        ]);

    }

    /**
     * @Route("/file-manager-remove-file", name="admin_post_file_manager_remove_file", options={"expose"=true})
     * @param Request $request
     * @return JsonResponse
     * @Security("is_granted('ROLE_SUPPORT')")
     */
    public function fileManagerRemoveFileAction(Request $request)
    {

        $name = $request->request->get('name');

        if($name) {

            $rootPath = $this->getParameter('kernel.root_dir');
            $path = $rootPath . BlogFileManagerListener::PATH_FILES;

            if(file_exists($path . '/' . $name)) {
                unlink ( $path . '/' . $name);
            }

        }

        return new JsonResponse([
            'success' => true
        ]);

    }

    private function getImageFiles() {

        $files = [];
        $finder = new Finder();
        $rootPath = $this->getParameter('kernel.root_dir');
        $baseUrl = $this->getParameter('server_host');

        if (!file_exists($rootPath . BlogFileManagerListener::PATH_FILES)) {
            mkdir($rootPath . BlogFileManagerListener::PATH_FILES, 0777, true);
        }

        $finder->files()->in($rootPath . BlogFileManagerListener::PATH_FILES);

        foreach ($finder as $file) {

            $files[] = [
                'url' => $baseUrl. '/files/blog/' . $file->getFilename(),
                'name' => $file->getFilename(),
                'time' => $file->getCTime()
            ];

        }

        usort($files, function($a,$b) {
            return $a['time'] < $b['time'];
        });

        return $files;
    }

}
