<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CustomContent;
use AppBundle\Entity\ValueDictionary;
use AppBundle\Form\Type\CustomContentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Form\Type\SearcherType;

/**
 * @Route("/admin/custom-content")
 */
class CustomContentController extends Controller
{
    /**
     * @Route("/{page}", requirements={"page" = "\d+"}, name="admin_custom_content_index")
     * @Method({"GET"})
     */
    public function indexAction(Request $request, $page = 1)
    {

        $em = $this->getDoctrine()->getManager();

        $searchForm = $this->createForm(SearcherType::class);
        $searchForm->handleRequest($request);

        $perPage = 20;
        $phrase = $searchForm->get('phrase')->getData();
//        $em->getFilters()->disable('softdeleteable');
        $query = $em->getRepository(CustomContent::class)->findAllByPhrase($phrase);
        $pagination = $this->get('knp_paginator')->paginate($query, $page, $perPage);

        $template = ($request->isXmlHttpRequest(
        )) ? 'AppBundle:Admin:custom-content/partials/list.html.twig' : 'AppBundle:Admin:custom-content/index.html.twig';

        return $this->render(
            $template,
            array(
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView(),
            )
        );
    }

    /**
     * @Route("/editor/{id}/{plainText}", name="admin_custom_content_editor", defaults={"id" = null})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */

    public function editorAction(Request $request, $id = null, $plainText = false)
    {
        $em = $this->getDoctrine()->getManager();

        if ($id) {
            $entity = $em->getRepository(CustomContent::class)->find($id);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('defined_content_not_found'));
            }
        } else {
            $entity = new CustomContent();
        }
        $form = $this->createForm(CustomContentType::class, $entity, ['safeKeys' => $this->getParameter('non_deletable_content_keys')]);

        $form->setData($entity);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Defined content "%name%" has been successfully edited',
                    ['%name%' => $entity->getKey()]
                )
            );

            return $this->redirect($this->generateUrl('admin_custom_content_index'));
        }

        return $this->render(
            'AppBundle:Admin:custom-content/editor.html.twig',
            array(
                'form' => $form->createView(),
                'entity' => $entity,
                'plainText' => $plainText
            )
        );

    }

    /**
     * @Route("/delete/{id}", name="admin_custom_content_delete")
     * @ParamConverter("customContent", class="AppBundle:CustomContent")
     * @Method({"GET"})
     */
    public function deleteAction(Request $request, CustomContent $customContent)
    {
        $em = $this->getDoctrine()->getManager();

        if (in_array($customContent->getKey(), $this->getParameter('non_deletable_content_keys'))) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans(
                    'Treść "%name%" nie może zostać usunięta',
                    ['%name%' => $customContent->getKey()]
                )
            );
        } else {
            $em->remove($customContent);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Słownik "%name%" został pomyślnie usunięty',
                    ['%name%' => $customContent->getKey()]
                )
            );
        }

        return $this->redirect($request->headers->get('referer'));

    }


}
