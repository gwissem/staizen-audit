<?php

namespace AppBundle\Controller;

use AppBundle\Entity\InsuranceDataImporter;
use AppBundle\Form\Type\InsuranceDataImportDatabaseType;
use AppBundle\Form\Type\InsuranceDataImportFileType;
use AppBundle\Form\Type\InsuranceDataImportType;
use AppBundle\Form\Type\PlatformType;
use CaseBundle\Entity\Dictionary;
use CaseBundle\Entity\DictionaryPackage;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\Program;
use CaseBundle\Form\Type\ProgramImportType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Job;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Asset\Package;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class InsuranceProgramController extends Controller
{

    const FLUSH_THRESHOLD = 50;
    const HEADER_ROW = 1;

    private static $importedRows = 0;

    /**
     * @Route("/", name="admin_insurance_programs_index")
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $importers = $em->getRepository('AppBundle:InsuranceDataImporter')->findAll();

        return $this->render(
            'AppBundle:Admin/insurance-program:index.html.twig',
            array(
                'importers' => $importers,
            )
        );

    }

    /**
     * @Route("/import-file", name="admin_insurance_programs_import_file")
     * @Method({"POST", "GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function importFromFileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $platforms = $em->getRepository('CaseBundle:Platform')->findAll();
        if (!$platforms) {
            $this->get('session')->getFlashBag()->add(
                'info',
                $this->get('translator')->trans('You need to define at least one platform before import program data')
            );

            return $this->redirect($this->generateUrl('admin_insurance_edit_platform'));
        }

        $form = $this->createForm(InsuranceDataImportFileType::class, null);
        $form->handleRequest($request);

        return $this->render(
            'AppBundle:Admin/insurance-program:import-file.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }

    /**
     * @Route("/import-from-database/{id}", name="admin_insurance_programs_import_database", defaults={"id" = null})
     * @Method({"POST", "GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     * @ParamConverter("importer", class="AppBundle:InsuranceDataImporter", isOptional="true")
     * @param Request $request
     * @param $importer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importFromDatabaseAction(Request $request, InsuranceDataImporter $importer = null)
    {

        if (!$importer) {
            $importer = new InsuranceDataImporter();
        }
        $preInterval = $importer->getInterval();
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(InsuranceDataImportDatabaseType::class, $importer);
        $form->setData($importer);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            if (!$importer->getPackage()) {
                $package = new DictionaryPackage();
                $package->setName($form->get('name')->getData());
                $package->setIsIncremental($form->get('isIncremental')->getData());
                $em->persist($package);

                $importer->setPackage($package);
            }

            $interval = $importer->getInterval();

            $em->persist($importer);
            $em->flush();

            $dictionaries = $this->get('app.insurance_data_import')->fetchNewDictionariesForImporter(
                $importer,
                $this->get('tool.query_manager')
            );

            if (!empty($dictionaries)) {
                $request->request->set('dictionaries', $dictionaries);
                $request->request->set('importerId', $importer->getId());

                return $this->forward(
                    'AppBundle:InsuranceProgram:completeDictionary',
                    ['packageId' => $importer->getPackage()->getId()]
                );
            } else {
                return $this->forward(
                    'AppBundle:InsuranceProgram:importProcess',
                    [
                        'packageId' => $importer->getPackage()->getId(),
                        'importerId' => $importer->getId(),
                    ]
                );
            }

        }

        return $this->render(
            'AppBundle:Admin/insurance-program:import-database.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/file-import-process/{packageId}/{fileId}", name="admin_insurance_programs_import_process_file")
     * @Route("/database-import-process/{packageId}/{importerId}", name="admin_insurance_programs_import_process_database")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function importProcessAction(Request $request, $packageId, $importerId = null, $fileId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $package = $em->getRepository(DictionaryPackage::class)->find($packageId);

        $job = new Job(
            'vin:import',
            ['packageId' => $packageId, 'fileId' => $fileId, 'importerId' => $importerId]
        );

        $packageOpenedJobs = $em->getRepository(
            Job::class
        )->findOpenedJobsForRelatedEntity('vin:import', $package);

        if ($importerId) {
            $importer = $em->getRepository(InsuranceDataImporter::class)->find($importerId);
            $job->setExecuteAfter($importer->getNextRun());
            if (!$importer->getActive()) {
                if ($packageOpenedJobs) {
                    foreach ($packageOpenedJobs as $openedJob) {
                        $openedJob->setState(Job::STATE_CANCELED);
                        $em->persist($openedJob);
                    }
                    $em->flush();
                }

                return $this->redirect($this->generateUrl('admin_insurance_programs_index'));
            }
        }

        $dependentJob = null;
        if ($packageOpenedJobs) {
            $dependentJob = $packageOpenedJobs[0];
        }

        //set current job priority
        if ($dependentJob) {
            if ($dependentJob->getArgs() === $job->getArgs()) {
                $dependentJob->setState(
                    $dependentJob->getState() === JOb::STATE_RUNNING ? Job::STATE_TERMINATED : Job::STATE_CANCELED
                );
                $em->persist($dependentJob);
            } else {
                $job->addDependency($dependentJob);
            }
        }

        $job->addRelatedEntity($package);

        $em->persist($job);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_job_show', ['jobId' => $job->getId()]));
    }

    /**
     * @Route("/complete-dictionary/{packageId}", name="admin_insurance_programs_complete_dictionary")
     * @Method("POST")
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function completeDictionaryAction(Request $request, $packageId = null)
    {

        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();

        $fileId = $request->request->get('fileId', null);
        $name = $request->request->get('name', '');
        $isIncremented = $request->request->get('isIncemental', null);
        $importerId = $request->request->get('importerId', null);
        $persist = $request->query->get('persist', null);
        $dictionaries = $request->request->get('dictionaries');

        if (!$persist) {
            $this->get('session')->set('fileId', $fileId);
            $this->get('session')->set('importerId', $importerId);
            $this->get('session')->set('name', $name);
            $this->get('session')->set('isIncremental', $isIncremented);
        }

        if ($packageId) {
            $package = $em->getRepository('CaseBundle:DictionaryPackage')->find($packageId);
        } else {
            $package = new DictionaryPackage();
            $package->setName($this->get('session')->get('name'));
            $package->setIsIncremental($this->get('session')->get('isIncremental'));
        }

        if (is_string($dictionaries)) {
            $dictionaries = json_decode($request->request->get('dictionaries'));
        }

        if ($dictionaries) {
            $package->setDictionaries(new ArrayCollection());
            $now = new \DateTime();
            foreach ($dictionaries as $name) {
                $dictionary = new Dictionary();
                $dictionary->setDescription($name);
                $dictionary->setStatus(Dictionary::STATUS_ACTIVE);
                $dictionary->setCreationDate($now);
                $dictionary->setPackage($package);
                $package->addDictionary($dictionary);
            }
        }

        $form = $this->createForm(
            ProgramImportType::class,
            $package,
            ['fileId' => $fileId, 'importerId' => $importerId]
        );

        if ($persist) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em->persist($package);
                $em->flush();
                $route = $this->get('session')->get(
                    'fileId'
                ) ? 'admin_insurance_programs_import_process_file' : 'admin_insurance_programs_import_process_database';

                return $this->redirect(
                    $this->generateUrl(
                        $route,
                        [
                            'packageId' => $package->getId(),
                            'fileId' => $this->get('session')->get('fileId'),
                            'importerId' => $this->get('session')->get('importerId'),
                        ]
                    )
                );
            }
        }

        return $this->render(
            'AppBundle:Admin/insurance-program:complete-dictionaries.html.twig',
            [
                'form' => $form->createView(),
                'packageId' => $packageId,
            ]
        );
    }


    /**
     * @Route("/edit-platform/{platformId}", name="admin_insurance_edit_platform")
     * @Method({"POST", "GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function editPlatformAction(Request $request, $platformId = null)
    {
        $em = $this->getDoctrine()->getManager();

        if ($platformId) {
            $entity = $em->getRepository('CaseBundle:Platform')->find($platformId);
            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Platform not found'));
            }
        } else {
            $entity = new Platform();
        }

        $form = $this->createForm(PlatformType::class, $entity);

        $form->setData($entity);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $toolId = $entity->getId();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans(
                    'Platform "%name%" has been successfully saved',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_insurance_programs_import_file'));
        }

        return $this->render(
            '@App/Admin/insurance-program/platform-editor.html.twig',
            array(
                'form' => $form->createView(),
                'entity' => $entity,
            )
        );
    }

    /**
     * @Route("/delete/{id}", name="admin_insurance_programs_import_database_delete")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()
            ->getRepository('AppBundle:InsuranceDataImporter')
            ->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Database import data not found'));
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Database import data was successfully deleted'
            )
        );

        return $this->redirect($this->generateUrl('admin_insurance_programs_index'));

    }
}
