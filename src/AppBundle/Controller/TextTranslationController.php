<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DynamicTranslation;
use AppBundle\Form\Type\TranslationCollectionType;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\Annotation\ReadOnly;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/translations/")
 */
class TextTranslationController extends Controller
{

    /**
     * @Rest\Route("ajaxForm/{key}",name="ajaxTextFormTranslation", options={"expose"=true})
     * @param string $key
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function formAjaxAction(Request $request, $key)
    {


        //prefix
        $key = str_replace('{-', '', $key);
        //suffix
        $key = str_replace('-}', '', $key);

        $keys = $this->getDoctrine()
            ->getRepository('AppBundle:DynamicTranslation')
            ->findBy(
                [
                    'translationKey' => $key
                ]
            );


        $editForm = $this->createForm(TranslationCollectionType::class, ['keys' => $keys]);

        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $editForm->handleRequest($request);
            if($editForm->isValid()){
                $entitites = $editForm->getData();
                /** @var DynamicTranslation $sendKey */
                foreach ($entitites['keys']  as $sendKey) {
                    $sendKey->setLocale($request->getLocale());
                    $sendKey->setTranslationKey($key);
                    $em->persist($sendKey);
                    $em->flush();
                }
            }
        }

        $form = $editForm->createView();
        $html = $this->renderView(
            '@App/translations/form.twig.html',
            [
                'form' => $form,
                'key' => $key
            ]
        );

        return new JsonResponse(['html' => $html, 'key' => $key]);
    }

    /**
     * @Rest\Route("delete/{id}", name="deleteDynamicTranslationKey", options={"expose" = true})
     * @Method({"DELETE"})
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function removeTranslationKey(Request $request, $id)
    {
        $doctrine = $this->getDoctrine();
        $key = $doctrine->getRepository('AppBundle:DynamicTranslation')->find($id);

        if ($key) {
            $em = $doctrine->getManager();
            $em->remove($key);
            $em->flush();
        }
        return new JsonResponse(['Msg' => 'Usunięte']);
    }
}