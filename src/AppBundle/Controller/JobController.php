<?php

namespace AppBundle\Controller;

use AppBundle\Entity\InsuranceDataImporter;
use AppBundle\Entity\Job;
use Doctrine\ORM\AbstractQuery;
use DocumentBundle\Entity\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class JobController extends Controller
{

    /**
     * @Route("/admin/queue-jobs/{page}", requirements={"page" = "\d+"}, name="admin_job_index")
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function indexAction(Request $request, $page = 1)
    {

        $em = $this->getDoctrine()->getManager();
        $perPage = 3;

        $query = $em->getRepository('JMSJobQueueBundle:Job')->createQueryBuilder('j')
            ->select('job, dependency')->from('JMSJobQueueBundle:Job', 'job')
            ->leftJoin('job.dependencies', 'dependency')
            ->where('job.command LIKE :command_name')
            ->setParameter('command_name', 'vin:import%')
            ->orderBy('job.createdAt', 'DESC')
            ->groupBy('job, dependency')
            ->setMaxResults(100)
            ->getQuery();

        $pagination = $query->getResult(AbstractQuery::HYDRATE_ARRAY);

        //$pagination = $this->get('knp_paginator')->paginate($query, $page, $perPage);

        return $this->render(
            'AppBundle:Admin:job/index.html.twig',
            array(
                'pagination' => $pagination,
                'page' => $page,
            )
        );
    }

    /**
     * @Route("/admin/queue-jobs/show/{jobId}", name="admin_job_show", requirements={"jobId": "\d+"})
     * @Security("is_granted('ROLE_ADMIN')")
     */

    public function showAction(Request $request, $jobId = null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JMSJobQueueBundle:Job')->find($jobId);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono zadania'));
        }

        $jobArgs = $entity->getArgs();
        $fileId = $jobArgs['fileId'];
        $importerId = $jobArgs['fileId'];

        $file = !empty($fileId) ? $em->getRepository(File::class)->find($fileId) : null;
        $importer = !empty($importerId) ? $em->getRepository(InsuranceDataImporter::class)->find($importerId) : null;
        $package = $em->getRepository('CaseBundle:DictionaryPackage')->find($jobArgs['packageId']);

        return $this->render(
            'AppBundle:Admin:job/show.html.twig',
            array(
                'entity' => $entity,
                'package' => $package,
                'file' => $file,
                'importer' => $importer,
            )
        );
    }

    /**
     * @Route("/admin/queue-jobs/force-end/{id}/{state}", name="admin_job_force_change_status", requirements={"id": "\d+"})
     * @Security("is_granted('ROLE_ADMIN')")
     */

    public function forceAction(Request $request, $id, $state)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JMSJobQueueBundle:Job')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Nie znaleziono zadania'));
        }

        $entity->setState($state);
        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                'Job "%id%" changed state to "%state%"',
                ['%id%' => $entity->getId(), '%state%' => strtoupper($state)]
            )
        );

        return new RedirectResponse($this->generateUrl('admin_job_index'));

    }

}
