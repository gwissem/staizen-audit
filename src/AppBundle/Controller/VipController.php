<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\VipType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use UserBundle\Entity\Company;
use UserBundle\Entity\Vip;

/**
 * Class VipController
 * @package AppBundle\Controller
 */
class VipController extends Controller
{
    /**
     * @Rest\Route("/vip-users/", name="vip_users")
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_EDITOR')")
     */
    public function indexAction(Request $request)
    {
        return $this->redirectToRoute("vip_users_list",
            [
                'Request' => $request
            ]
        );
    }

    /**
     * @Route(
     *     "/vip-users/list/{page}",
     *     name="vip_users_list" , requirements={"page" ="\d+"})
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_EDITOR')")
     */
    public function listAction($page = 1)
    {
        $em = $this->getDoctrine()->getManager();

        $perPage = 30;
        $query = $this->getDoctrine()->getRepository("UserBundle:Vip")->findAll();
        $pagination = $this->get('knp_paginator')->paginate($query, $page, $perPage);

        return $this->render(
            "AppBundle:Admin/vip:list.html.twig",
            [
                'pagination' => $pagination,
            ]
        );
    }

    /**
     * @Route(
     *     "/vip-users/{id}/edit",
     *     name="vip_users_edit",
     *     requirements={ "id":"\d+"})
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_EDITOR')")
     */
    public function editAction($id)
    {
        $companies = $this->getDoctrine()->getRepository('UserBundle:Company')->findAll();
        array_push($companies,[' - '=>null]);
        $entity = $this->getDoctrine()->getRepository("UserBundle:Vip")->find($id);
        if ($entity) {
            /** @var Company $activeCompany */
            $form = $this->createForm(VipType::class, $entity, [
                'action' => $this->generateUrl("vip_users_submit",
                    [
                        'id' => $id
                    ]
                ),

                'companies'=>$companies
            ]);
            $form = $form->setData($entity);

            return
                $this->render(
                    "AppBundle:Admin/vip:editor.html.twig",
                    [
                        'form' => $form->createView(),
                        'new' => false
                    ]
                );
        }


    }

    /**
     * @Route("/vip-users/add",name="vip_users_add")
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_EDITOR')")
     */
    public function addAction()
    {
        $companies = $this->getDoctrine()->getRepository('UserBundle:Company')->findAll();
        array_push($companies,[' - '=>null]);
        $form = $this->createForm(VipType::class, null, [
            'action' => $this->generateUrl("vip_users_submit", ['id' => null]),
            'companies'=>$companies
        ]);
        return
            $this->render(
                "AppBundle:Admin/vip:editor.html.twig",
                [
                    'form' => $form->createView(),
                    'new' => true
                ]
            );
    }

    /**
     * @Rest\Route("/vip-users/save/{id}",name="vip_users_submit")
     * @Method("POST")
     * @Security("is_granted('ROLE_PARTNER_EDITOR')")
     * @param $id
     * @param Request $request
     */
    public function submitAction(Request $request, $id = null)
    {
        $companies = $this->getDoctrine()->getRepository('UserBundle:Company')->findAll();
        array_push($companies,[' - '=>null]);
        if($id){

            $vip = $this->getDoctrine()->getRepository('UserBundle:Vip')->find($id);

        }else{
            $vip = new Vip();
        }

        $vipForm = $this->createForm(
            VipType::class,
            $vip,
            [
                'companies'=>$companies
            ]
        );

        $vipForm->handleRequest($request);



        if ($request->isMethod('POST') && $vipForm->isValid()) {

            $companyId = $request->request->get('company');
            if(isset($companyId) && $companyId!= '')
            {
                $company = $this->getDoctrine()->getRepository('UserBundle:Company')->find(intval($companyId));
                $vip->setCompany($company);
            }
            $em = $this->getDoctrine()->getManager();

            $em->persist($vip);
            $em->flush();
            $this->addFlash(
                'success',
                'Vip został zapisany'
            );
        }
        return $this->redirectToRoute("vip_users_list");
    }

    /**
     * @Route("/vip-users/{id}/delete",name="vip_users_delete")
     * @Method("GET")
     * @Security("is_granted('ROLE_PARTNER_EDITOR')")
     */
    public function deleteAction($id)
    {
        $vip = $this->getDoctrine()->getRepository('UserBundle:Vip')->find($id);
        if ($vip) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vip);
            $em->flush();

            $this->addFlash(
                'success',
                'Vip został usunięty'
            );
        }

        return $this->redirectToRoute("vip_users_list");
    }
}
