<?php

namespace AppBundle\Listener;

use AppBundle\Entity\InsuranceDataImporter;
use Doctrine\ORM\EntityManager;
use JMS\JobQueueBundle\Entity\Job;
use JMS\JobQueueBundle\Event\StateChangeEvent;
use PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobListener
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onStateChange(StateChangeEvent $event)
    {
        $newState = $event->getNewState();
        if ($newState != Job::STATE_PENDING) {
            try {
                $job = $event->getJob();
                if($job->getCommand() == 'vin:import') {
                    if ($newState == Job::STATE_FINISHED || $newState == Job::STATE_FAILED) {
                        $jobArgs = $job->getArgs();
                        if (!empty($jobArgs['importerId'])) {
                            $interval = $newState == Job::STATE_FAILED ? 3600 : false;
                            $this->createNewIntervalJob($job, $interval);
                        }

                        $this->executeAfterImportProcedure($jobArgs['packageId']);

                    }
                    $this->sendNotificationEmail($job, $newState);
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
    }

    protected function createNewIntervalJob($job, $interval = false)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $jobArgs = $job->getArgs();

        $importerId = $jobArgs['importerId'];
        $importer = !empty($importerId) ? $em->getRepository(InsuranceDataImporter::class)->find($importerId) : null;

        if (!$interval) {
            $interval = $importer->getInterval();
        }

        $nextRun = $job->getStartedAt()->modify('+ ' . $interval . ' seconds');
        $package = $importer->getPackage();

        $newJob = new \AppBundle\Entity\Job(
            'vin:import',
            $jobArgs
        );

        $packageOpenedJobs = $em->getRepository(\AppBundle\Entity\Job::class
        )->findOpenedJobsForRelatedEntity('vin:import', $package, [$job->getId()]);

        if ($packageOpenedJobs) {
            $dependentJob = $packageOpenedJobs[0];
            $newJob->addDependency($dependentJob);
            //check if importer did not generate another job manually by form edit
            if ($dependentJob->getArgs() === $job->getArgs()) {
                $newJob->setState(Job::STATE_CANCELED);
            }
        }

        $newJob->setExecuteAfter($nextRun);
        $newJob->addRelatedEntity($package);

        $em->persist($newJob);
        $em->flush();

    }

    public function executeAfterImportProcedure($packageId)
    {
        /** @var EntityManager $em */
        $parameters = [
            [
                'key' => 'package_id',
                'value' => $packageId,
                'type' => PDO::PARAM_INT
            ]
        ];
        $this->container->get('app.query_manager')->executeProcedure('EXEC dbo.vin_after_import @package_id = :package_id', $parameters, false);

    }

    public function sendNotificationEmail(Job $job, $state)
    {

        $https['ssl']['verify_peer'] = false;
        $https['ssl']['verify_peer_name'] = false; // seems to work fine without this line so far
        /** @var \Swift_Transport_EsmtpTransport $transport */
        $transport = $this->container->get('mailer')->getTransport();
        $transport->setStreamOptions($https);

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $jobArgs = $job->getArgs();
        $fileId = $jobArgs['fileId'];
        $importerId = $jobArgs['importerId'];

        $file = !empty($fileId) ? $em->getRepository(File::class)->find($fileId) : null;
        $importer = !empty($importerId) ? $em->getRepository(InsuranceDataImporter::class)->find($importerId) : null;
        $package = $em->getRepository('CaseBundle:DictionaryPackage')->find($jobArgs['packageId']);

        $message = \Swift_Message::newInstance()
            ->setSubject('[QUEUE JOB #'.$job->getId().'] STATUS CHANGED TO: '.strtoupper($state))
            ->setFrom('atlas-noreply@starter24.pl')
            ->setTo($this->container->getParameter('job_queue_mail'))
            ->setContentType("text/html")
            ->setBody(
                $this->container->get('templating')->render(
                    '@App/Email/job-status-change.html.twig',
                    [
                        'entity' => $job,
                        'file' => $file,
                        'package' => $package,
                        'importer' => $importer,
                        'newState' => $state,
                    ]
                )
            );
        $this->container->get('mailer')->send($message);
    }
}

