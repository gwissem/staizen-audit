<?php

namespace AppBundle\Listener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

class ForceRedirectToIframeListener
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage, Router $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if($event->getRequest()->query->has('_fr')) {

            $uri = str_replace('_fr', '', $event->getRequest()->getUri());
            $url = $this->router->generate('index') . '#redirectTo=' . $uri;

            $response = new RedirectResponse($url);
            $event->setResponse($response);

        }

    }

}
