<?php

namespace AppBundle\Listener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

class AcceptRegulationsListener
{

    const OAUTH_PATH_INFO = "/oauth/v2/token";
    const SPARX_API_PATH_INFO = "/sparx-api/v1";

    /**
     * @var Router
     */
    private $router;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage, Router $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        // Sprawdzenie czy idzie strzał do API
        if(strpos($event->getRequest()->server->get('HTTP_AUTHORIZATION', null), 'Bearer') !== FALSE) {
            return;
        }

        if(in_array($event->getRequest()->getPathInfo(),["/oauth/v2/token", "/api/auth/login"] )) {
            return;
        }


        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $eventRoute = $event->getRequest()->get('_route');
        $regulationsRoute = 'atlas_access_regulations';
        if ($user instanceof User && !$user->hasRole('ROLE_ADMIN') && (!$user->getCompany()
                || $user->getCompany()->getId() !== 1) && !$user->getAcceptedRegulations()
            && $eventRoute !== $regulationsRoute
        ) {
            $response = new RedirectResponse($this->router->generate($regulationsRoute));
            $event->setResponse($response);
        }
    }
}
