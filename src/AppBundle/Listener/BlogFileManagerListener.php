<?php

namespace AppBundle\Listener;

use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\Document;
use DocumentBundle\Entity\File;
use DocumentBundle\Uploader\FileUploader;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Oneup\UploaderBundle\Event\PreUploadEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class BlogFileManagerListener
{

    const PATH_FILES = '/../web/files/blog';

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var ContainerInterface
     */
    private $container;

    /** @var  FileUploader $fileUploader */
    private $fileUploader;

    public function __construct(EntityManager $em, ContainerInterface $container, FileUploader $fileUploader)
    {
        $this->em = $em;
        $this->container = $container;
        $this->fileUploader = $fileUploader;
    }

    public function onPreUpload(PreUploadEvent $event)
    {
        $directory = $this->container->getParameter('kernel.root_dir') . self::PATH_FILES;
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    public function onUpload(PostPersistEvent $event)
    {

        /** @var \Symfony\Component\HttpFoundation\File\File $file */
        $file = $event->getFile();
        $directory = $this->container->getParameter('kernel.root_dir') . self::PATH_FILES;

        /** @var UploadedFile $originalFile */
        $originalFiles = $event->getRequest()->files->get('files');
        $originalFile = $originalFiles[0];
        $originalName = $originalFile->getClientOriginalName();

//        if(file_exists($directory . '/' . $originalName)) {
//
//        }

        $file->move($directory, $originalName);

        $response = $event->getResponse();
        $response['fileUrl'] = self::PATH_FILES . '/' . $originalName;

    }

}
