<?php

namespace AppBundle\Listener;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Reader\XLSX\Reader;
use CaseBundle\Entity\Dictionary;
use CaseBundle\Entity\DictionaryPackage;
use Doctrine\ORM\EntityManager;
use DocumentBundle\Entity\File;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Oneup\UploaderBundle\Event\PreUploadEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Router;

class InsuranceDataUploadListener
{
    const HEADER_ROW = 1;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var ContainerInterface
     */
    private $container;

    /** @var Router $router */
    private $router;

    public function __construct(EntityManager $em, ContainerInterface $container, Router $router)
    {
        $this->em = $em;
        $this->container = $container;
        $this->router = $router;
    }

    public function onPreUpload(PreUploadEvent $event)
    {
        $directory = $this->container->getParameter('kernel.root_dir').'/../data';
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    public function onUpload(PostPersistEvent $event)
    {
        $request = $event->getRequest();
        $em = $this->container->get('doctrine.orm.entity_manager');
        /** @var UploadedFile $file */
        $file = $event->getFile();
        $filePath = $file->getPathName();

        $originalName = $request->files->get('file')->getClientOriginalName();
        $pathArray = explode('..', $filePath);
        $relativePath = $pathArray[1];

        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($file);
        $packageId = $request->get('package', null);
        $package = $packageId ? $this->em->getRepository('CaseBundle:DictionaryPackage')->find($packageId) : null;
        $dictionaries = $this->checkForNewDictionaries($reader, $package);

        $newFile = new File();
        $newFile->setName($originalName);
        $newFile->setFileSize($file->getSize());
        $newFile->setMimeType($file->getMimeType());
        $newFile->setPath($relativePath);
        $em->persist($newFile);
        $em->flush();

        $response = $event->getResponse();

        $response['fileId'] = $newFile->getId();
        $response['dictionaries'] = $dictionaries;
        $routeName = empty($dictionaries) ? 'admin_insurance_programs_import_process_file' : 'admin_insurance_programs_complete_dictionary';
        $response['action'] = $this->router->generate(
            $routeName,
            ['packageId' => $packageId, 'fileId' => $newFile->getId()]
        );
        return $response;
    }

    protected function checkForNewDictionaries(Reader $reader, DictionaryPackage $package = null)
    {
        $dictionaries = [];
        $dictionaryRepository = $this->em->getRepository('CaseBundle:Dictionary');
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            if ($key == 1) {
                foreach ($sheet->getRowIterator() as $rowIndex => $row) {
                    if ($rowIndex == self::HEADER_ROW) {
                        foreach ($row as $columnIndex => $column) {
                            if (!empty($column)) {
                                if ($package) {
                                    $dictionaryExists = $dictionaryRepository->findOneBy(
                                        ['description' => $column, 'package' => $package]
                                    );
                                    if (!$dictionaryExists) {
                                        $dictionaries[] = $column;
                                    }
                                } else {
                                    $dictionaries[] = $column;
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
        return $dictionaries;
    }


}