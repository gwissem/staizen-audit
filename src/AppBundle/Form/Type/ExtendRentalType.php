<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ExtendRentalType extends AbstractType
{

    const PATH_EXTEND_RENTAL_DAYS = '168,153';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('days',TextType::class,  array(
                'label' => 'O ile dni przedłóżyć z programu extra',
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ]
            ))
            ->add('groupProcessId', HiddenType::class, array(
                'label' => 'Id grupy',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Brakuje GROUP ID"
                    ])
                ]
            ))
        ;

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'csrf_protection' => false
        ));
    }

}
