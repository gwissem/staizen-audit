<?php

namespace AppBundle\Form\Type;

use CaseBundle\Entity\DictionaryPackage;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\Program;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class InsuranceDataImportFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST');
        $builder
            ->add(
                'file',
                FileType::class,
                [
                    'label' => 'app.form.file',
                    'attr' => array(
                        'class' => 'advanced-upload'
                    )
                ]
            )
            ->add(
                'fileId',
                HiddenType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'dictionaries',
                HiddenType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'package',
                EntityType::class,
                [
                    'class' => DictionaryPackage::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                            ->orderBy('p.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'required' => false,
                    'placeholder' => 'app.form.new_package',
                    'label' => 'app.form.choose_package',
                    'attr' => ['class' => 'select2', 'placeholder' => 'app.form.new_package'],
                ]
            )
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'app.form.package_name',
                )
            )
            ->add(
                'isIncremental',
                CheckboxType::class,
                array(
                    'label' => 'app.form.package_is_incremental',
                    'required' => false,
                )
            );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPresetDataEvent'));
        $builder->get('package')->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmitEvent'));

    }

    public function onPresetDataEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $this->updateFormElements($form, $data, 'preSetData');
    }

    protected function updateFormElements($builder, $package, $eventType = 'preSetData')
    {

        if ($package) {
            $builder->remove('isIncremental');
            $builder->remove('name');
        } else {
            $builder->add(
                'name',
                TextType::class,
                array(
                    'label' => 'app.form.package_name',

                )
            )
                ->add(
                    'isIncremental',
                    CheckboxType::class,
                    array(
                        'label' => 'app.form.package_is_incremental',
                        'required' => false,
                    )
            );
        }
    }

    public function onPostSubmitEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $form->getData();

        $this->updateFormElements($form->getParent(), $data, 'postSubmit');
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return '';
    }

}