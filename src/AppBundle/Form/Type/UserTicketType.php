<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserTicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user = $options['user'];

        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => false,
                    'required' => true,
                    'constraints' => [new NotBlank()],
                    'attr' => array(
                        'placeholder' => 'Tytuł zgłoszenia',
                    ),
                )
            )
            ->add(
                'content',
                TextareaType::class,
                array(
                    'label' => false,
                    'required' => true,
                    'constraints' => [new NotBlank()],
                    'attr' => array(
                        'placeholder' => 'Opis problemu',
                    ),
                )
            )
            ->add(
                'url',
                HiddenType::class,
                array(
                    'required' => false,
                    'label' => false,
                )
            )
            ->add(
                'screenResolution',
                HiddenType::class,
                array(
                    'required' => false,
                    'label' => false,
                )
            )
            ->add('screenShoot', HiddenType::class, array(
                'required' => false,
                'label' => false
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Ticket',
                'user' => null,
            )
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }

}