<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\DynamicTranslation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'validation',
            TextType::class,
            [
                'label' => 'Warunek',
                'required' => false,
            ]
        );

        $builder->add('text',
            TextareaType::class,
            [
                'label' =>'Zawartość',
                'required'=>true
            ]
        )
            ->add('delete',
                ButtonType::class,
                [
                    'label'=>'Usuń',
                    'attr'=>[
                        'class'=>'btn btn-remove btn-danger pull-right'
                    ]
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\DynamicTranslation',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }

}