<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ValueDictionary;
use AppBundle\Service\AtlasViewerService;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\Program;
use UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CFMSearcherCaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $coordinators = [];

        /** @var User $coordinate */
        foreach ($options['coordinates'] as $coordinate) {
            $coordinators[$coordinate->getName()] = $coordinate->getId();
        }

        if ($options['displayPlatformFilter']) {

            $builder->add(
                'platform',
                ChoiceType::class,
                [
                    'label' => 'case_searcher.platform',
                    'required' => false,
                    'multiple' => true,
                    'choice_value' => static function (Platform $entity = null) {
                        return $entity ? $entity->getId() : null;
                    },
                    'choice_label' => static function (Platform $entity = null) {
                        return $entity ? $entity->getName() : '---';
                    },
                    'choices' => $options['platforms'],
                    'attr' => [
                        'class' => 'select2'
                    ]
                ]
            );

        }

        $builder
            ->add(
                'case_number',
                TextType::class,
                [
                    'label' => 'case_searcher.case_number',
                    'required' => false
                ]
            )
            ->add(
                'program',
                ChoiceType::class,
                [
                    'label' => 'case_searcher.program',
                    'required' => false,
                    'multiple' => true,
                    'choice_value' => static function (Program $entity = null) {
                        return $entity ? $entity->getId() : null;
                    },
                    'choice_label' => static function (Program $entity = null) {
                        return $entity ? $entity->getName() : '---';
                    },
                    'choices' => $options['programs'],
                    'attr' => [
                        'class' => 'select2'
                    ]
                ]
            );

        if(AtlasViewerService::companyIsPZU($options['companyKey'])){

            $builder
                ->add(
                    'pzu_case_number',
                    TextType::class,
                    [
                        'label' => 'case_searcher.pzu_case_number',
                        'required' => false
                    ]
                )
                ->add(
                    'country',
                    ChoiceType::class,
                    [
                        'label' => 'case_searcher.country',
                        'required' => false,
                        'placeholder' => '--- wszystkie ---',
                        'choice_value' => static function (ValueDictionary $entity = null) {
                            return $entity ? $entity->getText() : null;
                        },
                        'choice_label' => static function (ValueDictionary $entity = null) {
                            return $entity ? $entity->getText() : '---';
                        },
                        'choices' => $options['countries'],
                        'attr' => [
                            'class' => 'select2'
                        ]
                    ]
                )
                ->add(
                    'car_rental_status',
                    ChoiceType::class,
                    [
                        'label' => 'case_searcher.car_rental_status',
                        'required' => false,
                        'placeholder' => '--- wszystkie ---',
                        'choices' => [
                            'Otwarty' => 'open',
                            'Zamknięty' => 'closed'
                        ],
                        'attr' => [
                            'class' => 'select2'
                        ]
                    ]
                )
                ->add(
                    'basic_service',
                    ChoiceType::class,
                    [
                        'label' => 'case_searcher.basic_service',
                        'required' => false,
                        'placeholder' => '--- wszystkie ---',
                        'choices' => [
                            'Holowanie' => 1,
                            'Naprawa' => 2
                        ],
                        'attr' => [
                            'class' => 'select2'
                        ]
                    ])
                ->add(
                    'make_model',
                    TextType::class,
                    [
                        'label' => 'case_searcher.car_model',
                        'required' => false
                    ]
                );

        }
        else {
            $builder->add(
                'end_date_rent',
                TextType::class,
                [
                    'label' => 'case_searcher.end_date_rent',
                    'required' => false,
                    'attr' => [
                        'class' => 'date-picker'
                    ]
                ]
            );
        }

        $builder->add(
            'status',
            ChoiceType::class,
            [
                'label' => 'case_searcher.status',
                'required' => false,
                'placeholder' => '--- wszystkie ---',
                'choice_value' => static function (ValueDictionary $entity = null) {
                    return $entity ? $entity->getValue() : null;
                },
                'choice_label' => static function (ValueDictionary $entity = null) {
                    return $entity ? $entity->getText() : '---';
                },
                'choices' => $options['availableStatus'],
                'attr' => [
                    'class' => 'select2'
                ]
            ]
        );

        $builder->add(
            'event_type',
            ChoiceType::class,
            [
                'label' => 'case_searcher.event_type',
                'required' => false,
                'multiple' => true,
                'choice_value' => static function (ValueDictionary $entity = null) {
                    return $entity ? $entity->getValue() : null;
                },
                'choice_label' => static function (ValueDictionary $entity = null) {
                    return $entity ? $entity->getText() : '---';
                },
                'choices' => $options['eventTypes'],
                'attr' => [
                    'class' => 'select2'
                ]
            ]
        );

        if ($options['displaySubEvent']) {

            $builder->add(
                'sub_event_type',
                ChoiceType::class,
                [
                    'label' => 'case_searcher.sub_event_type',
                    'required' => false,
                    'multiple' => true,
                    'choice_value' => static function (ValueDictionary $entity = null) {
                        return $entity ? $entity->getValue() : null;
                    },
                    'choice_label' => static function (ValueDictionary $entity = null) {
                        return $entity ? $entity->getText() : '---';
                    },
                    'choices' => $options['subEventTypes'],
                    'attr' => [
                        'class' => 'select2'
                    ]
                ]
            );

        }

        if (!$options['isRenault']) {

            $builder->add(
                'coordinator',
                ChoiceType::class,
                [
                    'label' => 'case_searcher.coordinator',
                    'required' => false,
                    'placeholder' => '---',
                    'choices' => $coordinators,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ]
            );

        }

        $builder->add(
            'date_from',
            TextType::class,
            [
                'label' => 'case_searcher.date_from',
                'required' => false,
                'attr' => [
                    'class' => 'date-time-picker'
                ]
            ]
        )
            ->add(
                'date_to',
                TextType::class,
                [
                    'label' => 'case_searcher.date_to',
                    'required' => false,
                    'attr' => [
                        'class' => 'date-time-picker'
                    ]
                ]
            )
            ->add(
                'car_registration_number',
                TextType::class,
                [
                    'label' => 'case_searcher.car_registration_number',
                    'required' => false
                ]
            )
            ->add(
                'driver',
                TextType::class,
                [
                    'label' => 'case_searcher.driver',
                    'required' => false
                ]
            );

        if ($options['isRenault'] || $options['companyId'] === 1) {
            $builder
                ->add(
                    'location',
                    ChoiceType::class,
                    [
                        'label' => 'case_searcher.location',
                        'required' => false,
                        'placeholder' => '--- wszędzie ---',
                        'choices' => [
                            'W kraju' => 'locally',
                            'Za granicą' => 'abroad'

                        ],
                        'attr' => [
                            'class' => 'select2'
                        ]
                    ]
                )
                ->add('vin',
                    TextType::class,
                    [
                        'label' => 'VIN',
                        'required' => false
                    ])
                ->add('case_category',
                    ChoiceType::class,
                    [
                        'label' => 'case_searcher.case_category',
                        'required' => false,
                        'multiple' => true,
                        'placeholder' => '--- wszystkie ---',
                        'choices' => $options['caseCategories'],
                        'attr' => [
                            'class' => 'select2'
                        ],
                        'choice_value' => static function (ValueDictionary $entity = null) {
                            return $entity ? $entity->getValue() : null;
                        },
                        'choice_label' => static function (ValueDictionary $entity = null) {
                            return $entity ? $entity->getText() : '---';
                        },
                    ]
                );
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => null,
                'programs' => [],
                'eventTypes' => [],
                'subEventTypes' => [],
                'coordinates' => [],
                'csrf_protection' => false,
                'companyId' => null,
                'platforms' => [],
                'displaySubEvent' => true,
                'availableStatus' => [],
                'platformPermission' => [],
                'displayPlatformFilter' => false,
                'isRenault' => false,
                'caseCategories' => [],
                'companyKey' => '',
                'countries' => []
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'case_search';
    }

}