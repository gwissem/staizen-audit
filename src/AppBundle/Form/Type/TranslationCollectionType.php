<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\DynamicTranslation;
use CaseBundle\Entity\Step;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TranslationCollectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('keys', CollectionType::class,
            array(

                'entry_type' => TranslationType::class,
                'entry_options' => array(
                    'attr' => array('class' => 'list-box')
                ),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
            ))
            ->add('submit', SubmitType::class);
    }


    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => null
        ));
    }

}