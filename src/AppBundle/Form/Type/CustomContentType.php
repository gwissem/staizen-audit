<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\CustomContent;
use AppBundle\Entity\ValueDictionary;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CustomContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $readonlyKey = false;
        $safeKeys = $options['safeKeys'];
        $data = $builder->getData();
        if($data->getKey() && in_array($data->getKey(), $safeKeys)){
            $readonlyKey = true;
        }
        $builder
            ->add(
                'key',
                TextType::class,
                [
                    'constraints' => [new NotBlank()],
                    'label' => 'app.form.key',
                    'attr' => [
                        'readonly' => $readonlyKey
                    ]
                ]
            )
            ->add(
                'webUrl',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'web url',
                ]
            )
            ->add(
                'translations',
                'A2lix\TranslationFormBundle\Form\Type\TranslationsType',
                [
                    'fields' => [
                        'content' => [
                            'field_type' => TextareaType::class,
                            'attr' => [
                                'class' => "tinymce-word",
                                "data-theme" => "atlas_simple",
                            ],
                            'label' => 'app.form.content',
                            'locale_options' => array(
                                'pl' => array(
                                    'required' => true,
                                    'constraints' => [new NotBlank()],
                                ),
                            ),
                        ],
                        'createdAt' => ['display' => false],
                        'updatedAt' => ['display' => false],
                    ],
                    'label' => false,
                ]
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => CustomContent::class,
                'safeKeys' => []
            )
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }

}