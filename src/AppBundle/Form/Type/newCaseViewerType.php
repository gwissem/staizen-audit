<?php

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class newCaseViewerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $data = $builder->getData();

        $builder
            ->add('platformId', HiddenType::class,
            [
                'label' => 'platforma',
                'required' => true,
                'data' => $data['platformId'],
                'constraints' => [new NotBlank()]
            ]
            )
            ->add('driver_first_name', TextType::class, [
                'label' => 'Imię',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('driver_second_name', TextType::class, [
                'label' => 'Nazwisko',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('driver_phone', TextType::class, [
                'label' => 'Telefon',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])

            ->add('event_type', ChoiceType::class, [
                'label' => 'Typ',
                'choices' => $options['eventTypes'],
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('event_description', TextareaType::class, [
                'label' => 'Opis',
                'required' => false,
            ])
            ->add('event_create_date', TextType::class, [
                'label' => 'Data zdarzenia',
                'required' => true,
                'constraints' => [new NotBlank()],
                'attr' => [
                    'class' => 'datetime-picker'
                ]
            ])

            ->add('car_register_number', TextType::class, [
                'label' => 'Numer rejestracyjny',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('car_vin', TextType::class, [
                'label' => 'Numer VIN',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('car_dpr', TextType::class, [
                'label' => 'Data I rejestracji',
                'required' => true,
                'constraints' => [new NotBlank()],
                'attr' => [
                    'class' => 'date-picker'
                ]
            ])
            ->add('car_make_model', ChoiceType::class, [
                'label' => 'Marka / Model',
                'required' => true,
                'choices' => $options['makeModel'],
                'constraints' => [new NotBlank()],
                'attr' => [
                    'class' => 'select2'
                ]
            ])

            ->add('location_event', TextType::class, [
                'label' => 'Lokalizacja aktualna (zdarzenia)',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])
            ->add('location_target', TextType::class, [
                'label' => 'Lokalizacja docelowa',
                'required' => true,
                'constraints' => [new NotBlank()]
            ])

            ->add('description', TextareaType::class, [
                'label' => 'Uwagi',
                'required' => false,
                'attr' => [
                    'rows' => 5
                ]
            ])

            ->add('create', SubmitType::class, [
                'label' => 'Załóż nową sprawę',
                'attr' => [
                    'class' => 'btn btn-green-meadow float-right ladda-button',
                    'data-style' => 'zoom-in',
                    'data-size' => 's'
                ]
            ])
        ;

    }

    public static function getAvailableField() {

        return [
            'platformId' => '253',

            'driver_first_name' => '80,342,64',
            'driver_second_name' => '80,342,66',
            'driver_phone' => '80,342,408,197',

            'event_type' => '491',
            'event_description' => 'NOTE:Opis zdarzenia',
            'event_create_date' => '101,104',

            'car_register_number' => '74,72',
            'car_vin' => '74,71',
            'car_dpr' => '74,233',
            'car_make_model' => '74,73',

            'location_event' => 'NOTE:Aktualna lokalizacja',
            'location_target' => 'NOTE:Docelowa lokalizacja',

            'description' => 'NOTE:Uwagi'
        ];

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => null,
                'csrf_protection' => false,
                'makeModel' => [],
                'eventTypes' => []
            ]
        );
    }

}