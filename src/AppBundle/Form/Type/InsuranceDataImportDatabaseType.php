<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\InsuranceDataImporter;
use CaseBundle\Entity\DictionaryPackage;
use CaseBundle\Entity\Platform;
use CaseBundle\Entity\Program;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class InsuranceDataImportDatabaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST');
        $builder
            ->add(
                'connectionString',
                TextType::class,
                array(
                    'label' => 'connection_string',
                    'required' => false
                )
            )
            ->add(
                'query',
                TextareaType::class,
                array(
                    'label' => 'query',
                    'required' => false,
                    'attr' => array(
                        'rows' => 6,
                        'class' => 'sql-codemirror',
                    ),
                )
            )
            ->add(
                'package',
                EntityType::class,
                [
                    'class' => DictionaryPackage::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                            ->orderBy('p.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'required' => false,
                    'placeholder' => 'app.form.new_package',
                    'label' => 'app.form.choose_package',
                    'attr' => ['class' => 'select2', 'placeholder' => 'app.form.new_package'],
                ]
            )
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'app.form.package_name',
                    'mapped' => false,
                )
            )
            ->add(
                'dictionaries',
                HiddenType::class,
                [
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'isIncremental',
                CheckboxType::class,
                array(
                    'label' => 'app.form.package_is_incremental',
                    'required' => false,
                )
            )
            ->add(
                'interval',
                ChoiceType::class,
                array(
                    'label' => 'app.form.interval',
                    'required' => true,
                    'choices' => [
                        '1 day' => 86400,
                        '3 days' => 259200,
                        '5 days' => 432000,
                        '1 week' => 604800,
                        '2 weeks' => 1209600,
                    ],
                )
            )
            ->add(
                'nextRun',
                DateTimeType::class,
                array(
                    'label' => 'app.form.first_job_start_time',
                    'required' => false,
                )
            )
            ->add(
                'active',
                CheckboxType::class,
                array(
                    'label' => 'app.form.active',
                    'required' => false,
                )
            );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPresetDataEvent'));
        $builder->get('package')->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmitEvent'));

    }

    public function onPresetDataEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $this->updateFormElements($form, $data, 'preSetData');
    }

    protected function updateFormElements($builder, $element, $eventType = 'preSetData')
    {

        if (($element && $eventType == 'postSubmit') || ($builder->get('package')->getData() !== NULL && $eventType=='preSetData')) {
            $builder->remove('isIncremental');
            $builder->remove('name');
        } else {
            $builder->add(
                'name',
                TextType::class,
                array(
                    'label' => 'app.form.package_name',
                    'mapped' => false,
                )
            )
            ->add(
                'isIncremental',
                CheckboxType::class,
                array(
                    'label' => 'app.form.package_is_incremental',
                    'required' => false,
                )
            );
        }
    }

    public function onPostSubmitEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $form->getData();

        $this->updateFormElements($form->getParent(), $data, 'postSubmit');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => InsuranceDataImporter::class,
                'user' => null,
            )
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }

}