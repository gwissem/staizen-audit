<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CareFleetAuthorizationType extends AbstractType
{

    const PATH_CODE_AUTHORIZATION = '1068';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('code',TextType::class,  array(
                'label' => 'Kod autoryzacji',
                'required' => false,
                'constraints' => [
                    new NotBlank()
                ]
            ))
            ->add('groupProcessId', HiddenType::class, array(
                'label' => 'Id grupy',
                'required' => true
            ))
        ;

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'csrf_protection' => false
        ));
    }

}
