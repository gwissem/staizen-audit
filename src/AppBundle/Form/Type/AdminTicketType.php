<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminTicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => [new NotBlank()],
                    'label' => 'Tytuł zgłoszenia',
                ]
            )
            ->add(
                'content',
                TextareaType::class,
                [
                    'required' => true,
                    'constraints' => [new NotBlank()],
                    'label' => 'Opis problemu',
                ]
            )
            ->add(
                'notificationNumber',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'ticket.number_notification',
                ]
            )
            ->add(
                'status',
                ChoiceType::class,
                array(
                    'label' => 'status',
                    'choices' => Ticket::getStatuses(),
                )
            )
            ->add(
                'ticketLevel',
                ChoiceType::class,
                array(
                    'label' => 'Waga błędu',
                    'choices' => Ticket::getTicketLevels(),
                )
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Ticket',
                'user' => null,
            )
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }

}