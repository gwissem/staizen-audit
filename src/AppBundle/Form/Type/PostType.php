<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',null,  array(
                'label' => 'Tytuł'
            ))
            ->add('content', null,  array(
                'label' => 'Kontent',
                'required' => true,
                'attr' => [
                    'class' => 'tinymce-word',
                    'data-theme' => 'atlas_blog',
                ]
            ))
            ->add('document',
                FileType::class,
                [
                    'label' => 'Pliki',
                    'required' => false,
                    'multiple' => true,
                    'attr' => ['class' => 'advanced-upload']
                ]
            )
            ->add('type',
                ChoiceType::class,
                [
                    'label' => 'Typ wpisu',
                    'choices' => $options['typeOptions'],
                    'required' => true,
                ]
            )
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post',
            'typeOptions' => null,
        ));
    }
}
