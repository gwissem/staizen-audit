<?php

namespace AppBundle\Form\Type;

use UserBundle\Entity\Company;
use UserBundle\Entity\Vip;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class VipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $companiesList = $options['companies'];

        $companies = $companiesList;



        $builder
            ->add(
                'firstname',
                TextType::class,
                [
                    'label' => 'Imię',
                    'required' => false,
//                    'constraints' => [new NotBlank()]
                ]
            )
            ->add(
                'lastname',
                TextType::class,
                [
                    'label' => 'Nazwisko',
                    'required' => false,
//                    'constraints' => [new NotBlank()]
                ]
            )
            ->add("companyName",
                TextType::class,
                [
                    'label' => 'Nazwa firmy',
                    'required' => true,
                    'constraints' => [new NotBlank()]
                ]
            )
            ->add(
                'position',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'Stanowisko',
                ]
            )
            ->add('phoneNumber',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'Numer',
                    'constraints' => [new NotBlank()]
                ]
            )
            ->add("type",
                ChoiceType::class,
                [
                    'choices' => [
                        Vip::VIP_TYPE_PERSON_TEXT => Vip::VIP_PERSON,
                        Vip::VIP_TYPE_COMPANY_TEXT => Vip::VIP_COMPANY,
                    ],
                ]
            )
            ->add('company', ChoiceType::class,
                [
                    'choices' => $companies,
//                    'val'       => '-',
                    'empty_data'        => null,
                    'choice_label' => function(Company $entity = null){
                return $entity?(($entity->getFullName() !='')?$entity->getFullName():$entity->getName()):' - ';
                    },
                    'choice_value' => function (Company $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                ])
            ->add('vin', TextType::class,
                ['required' => false, 'label' => 'Nr Vin']
            )
            ->add('regNumber', TextType::class,
                ['required' => false, 'label' => 'Nr rejestracyjny']
            )
            ->add("welcomeMessage",
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'Wiadomość na powitanie',
                ]
            )
            ->add("supervisorMessage",
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'Instrukcja dla konsultanta',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'UserBundle\Entity\Vip',
                'companies' => [],


            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }

}