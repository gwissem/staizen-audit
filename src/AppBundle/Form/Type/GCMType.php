<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class GCMType extends AbstractType
{

    const PATH_GCM = '323';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('isGCM',ChoiceType::class,  array(
                'label' => 'Rozliczenie GCM?',
                'choices' => [
                    'Tak' => 1,
                    'Nie' => 0
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ]
            ))
            ->add('groupProcessId', HiddenType::class, array(
                'label' => 'Id grupy',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Brakuje GROUP ID"
                    ])
                ]
            ))
        ;

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'csrf_protection' => false
        ));
    }

}
