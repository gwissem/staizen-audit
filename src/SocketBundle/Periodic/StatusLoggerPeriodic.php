<?php

namespace SocketBundle\Periodic;

use CaseBundle\Entity\ProcessEnterHistory;
use Doctrine\ORM\EntityManager;
use DOMElement;
use Gos\Bundle\WebSocketBundle\Periodic\PeriodicInterface;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use Machines\CentralMachine;
use Predis\Client;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampConnection;
use SocketBundle\EventListener\WebSocketEventListener;
use SocketBundle\Service\RedisListListenerService;
use SocketBundle\Topic\AtlasTopic;
use SocketBundle\Topic\ProcessManagerTopic;
use SocketBundle\Topic\TaskManagerTopic;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Stopwatch\Stopwatch;
use UserBundle\Entity\TelephonyAgentStatus;

require __DIR__.'/../../CentralMachine/index.php';

class StatusLoggerPeriodic implements PeriodicInterface
{

    /** @var PusherInterface  */
    private $pusher;
    /** @var Client  */
    private $redis;
    /** @var EntityManager  */
    private $entityManager;
    /** @var Crawler  */
    private $crawler;

    /** @var int */
    private $limitTime = 3600; // 1h
    const LIVE_TIME_CONNECTION = 60; // 60 sec

    private $timeout;
    private $logTelephony;
    private $elasticaLogging;

    /** @var  AtlasTopic */
    private $atlasTopic;

    /** @var  TaskManagerTopic */
    private $taskManagerTopic;

    /** @var  TaskManagerTopic */
    private $processManagerTopic;

    static $connections = 0;

    static $subscribe = 0;
    static $publish = 0;
    static $push = 0;
    static $rpc = 0;

    /**
     * TaskManagerPeriodic constructor.
     * @param PusherInterface $pusherDecorator
     * @param Client $redis
     * @param EntityManager $entityManager
     * @param $logTelephony
     * @param AtlasTopic $atlasTopic
     * @param TaskManagerTopic $taskManagerTopic
     * @param ProcessManagerTopic $processManagerTopic
     * @param $elasticaLogging
     */
    public function __construct(PusherInterface $pusherDecorator,
                                Client $redis,
                                EntityManager $entityManager,
                                $logTelephony,
                                AtlasTopic $atlasTopic,
                                TaskManagerTopic $taskManagerTopic,
                                ProcessManagerTopic $processManagerTopic,
                                $elasticaLogging
    )
    {

        $this->pusher = $pusherDecorator;
        $this->timeout = 5;
        $this->redis = $redis;
        $this->entityManager = $entityManager;
        $this->crawler = new Crawler();
        $this->logTelephony = $logTelephony;
        $this->atlasTopic = $atlasTopic;
        $this->taskManagerTopic = $taskManagerTopic;
        $this->processManagerTopic = $processManagerTopic;
        $this->elasticaLogging = $elasticaLogging;
    }

    public function tick()
    {

        $this->pingDatabase();

        $this->checkPings();

        if($this->elasticaLogging) {

            try {
                $this->logTraffic();
            } catch (\Exception $exception) {
                echo $exception->getMessage();
            }

        }

    }

    static function incrementConnections() {
        self::$connections++;
    }

    private function pingDatabase() {

        if ($this->entityManager !== null && $this->entityManager->getConnection()->ping() === false) {
            $this->entityManager->getConnection()->close();
            $this->entityManager->getConnection()->connect();
        }

    }

    private function logTraffic() {

        $connections = (self::$connections > 0) ? (self::$connections / $this->timeout) : 0;

        $job = [
            'redis_listener.websocket_usage_memory' => number_format((memory_get_usage() / 1024 / 1024), 3),
            'redis_listener.websocket_connections' => $connections,
            'redis_listener.websocket_all_connected_users' => WebSocketEventListener::$connectedUsers
        ];

        $this->redis->rpush(
            RedisListListenerService::ATLAS_ELASTICA_MONITOR,
            [
                serialize($job)
            ]
        );

        self::$connections = 0;

        unset($connections, $job);

    }

    private function checkPings() {

        try {

            $this->atlasTopic->checkPings();
            $this->taskManagerTopic->checkPings();
            $this->processManagerTopic->checkPings();

        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }

    }

    /**
     * {@inheritdoc}
     */
    public function getTimeout()
    {
        /** in seconds */
        return $this->timeout;
    }
}