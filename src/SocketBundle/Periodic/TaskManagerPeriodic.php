<?php

namespace SocketBundle\Periodic;

use CaseBundle\Entity\ProcessEnterHistory;
use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Periodic\PeriodicInterface;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use Predis\Client;
use SocketBundle\EventListener\WebSocketEventListener;
use SocketBundle\Topic\TaskManagerTopic;
use SocketBundle\Utils\CurrentTaskRedisValue;

class TaskManagerPeriodic implements PeriodicInterface
{

    private $pusher;
    private $timeout;
    private $redis;
    private $taskManagerTopic;
    /** @var EntityManager  */
    private $entityManager;

    /**
     * TaskManagerPeriodic constructor.
     * @param PusherInterface $pusherDecorator
     * @param int $timeoutTick
     * @param TaskManagerTopic $taskManagerTopic
     * @param Client $redis
     * @param EntityManager $entityManager
     */
    public function __construct(PusherInterface $pusherDecorator, $timeoutTick = 60, TaskManagerTopic $taskManagerTopic,
                                Client $redis, EntityManager $entityManager)
    {

        $this->pusher = $pusherDecorator;
//        $this->timeout = 10;
        $this->timeout = $timeoutTick;
        $this->redis = $redis;
        $this->taskManagerTopic = $taskManagerTopic;
        $this->entityManager = $entityManager;
    }

    public function tick()
    {

        $this->clearBlockedTasks();
        /** Jednak każdy sam odświeża sobie zadania */
//        $this->refreshTasks();
//        $this->logConnectedUsersOnDashboard();
    }

    private function refreshTasks() {

        $data = [
            'action' => 'refresh_tasks'
        ];

        $this->pusher->push($data, 'atlas_task_manager');

    }

    private function logConnectedUsersOnDashboard() {

//        dump($this->taskManagerTopic->getActiveConnectionsPerUsers());
    }

    /**
     * Czyszczenie redis'a z zarezerwowanych zadań, które tam utknęły (WebSocket został zresetowany).
     */

    private function clearBlockedTasks() {

        try {

            $reservedTasksInWebSocket = $this->taskManagerTopic->getReservedTasksInWebSocket();
            $reservedTasksInRedis = $this->redis->hkeys(TaskManagerTopic::RESERVED_TASKS);

            $diff = array_diff($reservedTasksInRedis, $reservedTasksInWebSocket);

            foreach ($diff as $taskId) {
                $this->redis->hDel(TaskManagerTopic::RESERVED_TASKS, $taskId);
            }

            /** @var CurrentTaskRedisValue[] $tasks */
            $redisTasks = $this->redis->hgetall(TaskManagerTopic::REDIS_KEY_CURRENT_TASKS);

            if(!empty($redisTasks)) {

                /** tablica połączonych użytkowników */
                $idsOfConnectedUsers = $this->taskManagerTopic->getIdsOfConnectedUsers();

                /** @var CurrentTaskRedisValue[] $tasks */
                $tasks = [];

                foreach ($redisTasks as $key => $redisTask) {
                    $tasks[$key] = unserialize($redisTask);
                }

                foreach ($tasks as $key => $task) {
                    /** Jeżeli jest zadanie dla użytkownika, który nie jest połączony, to wyrzucenie tego zadania */
                    if(!in_array($task->userId, $idsOfConnectedUsers)) {
                        $this->redis->hdel(TaskManagerTopic::REDIS_KEY_CURRENT_TASKS, [$key]);
                        $this->redis->set(TaskManagerTopic::REDIS_KEY_DATE_LAST_LEAVE_TASK . $task->userId, (new \DateTime())->getTimestamp());
                    }
                }

                unset($tasks, $idsOfConnectedUsers);

            }

            unset($redisTasks);

        }
        catch (\Exception $exception) {

        }

    }

    /**
     * {@inheritdoc}
     */
    public function getTimeout()
    {
        /** in seconds */
        return $this->timeout;
    }
}