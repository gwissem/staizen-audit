<?php

namespace SocketBundle\Utils;

use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Predis\Client;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Ratchet\Wamp\WampConnection;
use UserBundle\Entity\User;

/**
 * Już nie używane
 */

class WebsocketHelper
{
    /**
     * Sprawdzanie, czy na pewno połączył się autoryzowany użytkownik
     *
     * @param ClientManipulatorInterface $clientManipulator
     * @param ConnectionInterface $connection
     * @param callable $callback
     * @return mixed
     */
    static function checkConnection(ClientManipulatorInterface $clientManipulator, ConnectionInterface $connection, $callback)
    {

        $user = $clientManipulator->getClient($connection);

        if ($user instanceof User) {

            /** @var WampConnection $connection */
            return call_user_func($callback, $connection->resourceId, $user);

        } else {

            /** Połączył się ktoś nieznany, zamykanie połaczenia */
            $connection->close();
            return [];
        }

    }
}