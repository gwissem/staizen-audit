<?php

namespace SocketBundle\Utils;

class RedisListJob
{

    const E_TASK = 'enter_on_task';
    const L_TASK = 'leave_task';
    const PING_TIME = 'ping_real_time';

    public $action;

    /** @var \DateTime */
    public $date;

    public $processInstanceId;

    public $rootId;

    public $userId;

    public $resourceId; // WebSocket connection id

    public $time; // czas na zadaniu przy pingu

    /**
     * CurrentTaskRedisValue constructor.
     * @param $action
     * @param $userId
     * @param $processInstanceId
     * @param $resourceId
     * @param null $rootId
     * @param null $time
     */
    public function __construct($action, $userId, $processInstanceId, $resourceId, $rootId = null, $time = null)
    {

        $this->action = $action;
        $this->date = new \DateTime();
        $this->userId = $userId;
        $this->processInstanceId = $processInstanceId;
        $this->resourceId = $resourceId;
        $this->rootId = $rootId;
        $this->time = $time;

    }

}