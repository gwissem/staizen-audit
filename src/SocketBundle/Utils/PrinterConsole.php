<?php

namespace SocketBundle\Utils;

use Symfony\Component\Console\Style\SymfonyStyle;

class PrinterConsole extends SymfonyStyle implements PrinterInterface
{

   public function printLine($message) {
       $this->writeln($message);
   }

   public function printLineError($message) {
       $this->error($message);
   }

}