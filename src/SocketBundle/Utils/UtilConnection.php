<?php

namespace SocketBundle\Utils;

use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampConnection;
use SocketBundle\Exception\AtlasWebsocketException;
use SocketBundle\Periodic\StatusLoggerPeriodic;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

trait UtilConnection
{

    /** @var ClientManipulatorInterface */
    protected  $clientManipulator;
    /** @var EntityManager */
    protected  $em;

    /**
     * Sprawdzanie, czy na pewno połączył się autoryzowany użytkownik i czy jest połączenie z bazą danych
     *
     * @param ConnectionInterface $connection
     * @param $data array
     * @param callable $callback
     * @return mixed
     * @throws AtlasWebsocketException
     */
    private function checkConnection(ConnectionInterface $connection, $data = [], $callback)
    {

        if(array_key_exists('request', $data) && isset($this->container)) {
            $this->setRequestLocale($data['request']);
        }

//        if ($this->em !== null && $this->em->getConnection()->ping() === false) {
//            $this->em->getConnection()->close();
//            $this->em->getConnection()->connect();
//        }

        $user = $this->clientManipulator->getClient($connection);

        StatusLoggerPeriodic::incrementConnections();

        if ($user instanceof User) {

            try {

                return call_user_func($callback, $connection->resourceId, $user);

            } catch (\Throwable $exception) {

                $this->logException($exception);
                throw new AtlasWebsocketException($exception, $user->isAdmin(), 'Error');

            }


        } else {

            /** Połączył się ktoś nieznany, zamykanie połaczenia */

            $connection->close();
            return [];
        }

    }

    private function setRequestLocale(WampRequest $request){
        $currentRequest = $this->container->get('request_stack')->getCurrentRequest();
        if (empty($currentRequest)) {
            $currentRequest = new Request();
            $this->container->get('request_stack')->push($currentRequest);
        }
        $currentRequest->setLocale($request->getAttributes()->has('locale') ? $request->getAttributes()->get('locale') : $this->container->getParameter('locale'));
    }

    protected function logException($exception) {}

    /** HOW USE */

    /**

        $response = $this->checkConnection($connection, function ($resourceId, User $user) use ($params) {

                // YOUR CODE

                return array(
                    'data' => "foo"
                );

            });

        return $response;

     */
}
