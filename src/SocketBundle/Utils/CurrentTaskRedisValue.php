<?php

namespace SocketBundle\Utils;

class CurrentTaskRedisValue
{

    /** @var \DateTime */
    public $enterDate;

    public $groupName;

    public $groupId;

    public $processInstanceId;

    public $rootProcessInstanceId;

    public $groupProcessInstanceId;

    public $userId;

    /**
     * CurrentTaskRedisValue constructor.
     * @param $userId
     * @param $taskName
     * @param $processDescription
     */
    public function __construct($userId, $taskName, $processDescription)
    {

        $this->enterDate = new \DateTime();

        $this->groupName = $taskName['groupName'];
        $this->groupId = intval($taskName['groupId']);

        $this->userId = $userId;

        $this->processInstanceId = $processDescription['id'];
        $this->rootProcessInstanceId = $processDescription['rootId'];
        $this->groupProcessInstanceId = $processDescription['groupProcessId'];

    }


    public function getSeconds() {

        if(!$this->enterDate) return 0;

        return (new \DateTime())->getTimestamp() - $this->enterDate->getTimestamp();

    }

}