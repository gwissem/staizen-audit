<?php

namespace SocketBundle\Utils;

interface PrinterInterface
{

   public function printLine($message);

   public function printLineError($message);

}