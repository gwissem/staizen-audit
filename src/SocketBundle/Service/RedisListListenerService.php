<?php

namespace SocketBundle\Service;

use AppBundle\Service\ElasticSearchLogService;
use AppBundle\Utils\QueryManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use Predis\Client;
use SocketBundle\Command\RedisListListenerCommand;
use SocketBundle\Topic\TaskManagerTopic;
use SocketBundle\Utils\PrinterInterface;
use SocketBundle\Utils\RedisListJob;

class RedisListListenerService
{

    const TIMEOUT_BLPOP = 3;
    const PING_AMOUNT_TIME = 5;
    const LIMIT_TIME_ON_CASE = 3600; // 1h
    const LOOP_TIMEOUT = 30;
    const LOOP_TIMEOUT_STATISTIC = 60; // 1 min

    const ENTER_AND_LEAVE_TASKS = 'enter_and_leave_tasks';
    const ATLAS_ELASTICA_MONITOR = 'atlas_elastica_monitor';
    const ONLINE_USER = 'online_user';
    const ELASTICA_LOG = 'elastica_log';

    /** @var Client */
    protected $redis;

    /** @var Connection */
    protected $conn;

    /** @var QueryManager */
    protected $queryManager;

    /** @var PusherInterface */
    protected $pusher;

    /** @var PrinterInterface|null */
    protected $printer;

    /** @var integer */
    private $loopTimestamp;

    /** @var integer */
    private $loopStatisticTimestamp;

    /** @var int */
    private $operations = 0;

    /** @var ElasticSearchLogService */
    private $elasticSearchLog;

    private $wasError = false;

    /**
     * RedisListSubscriberService constructor.
     * @param Client $predis
     * @param Connection $conn
     * @param QueryManager $queryManager
     * @param PusherInterface $pusher
     */
    public function __construct(Client $predis, Connection $conn, QueryManager $queryManager, PusherInterface $pusher)
    {
        $this->redis = $predis;
        $this->conn = $conn;
        $this->queryManager = $queryManager;
        $this->pusher = $pusher;

    }

    public function setElasticSearchLog(ElasticSearchLogService $elasticSearchLog) {
        $this->elasticSearchLog = $elasticSearchLog;
    }

    public function setPrinter(PrinterInterface $printer)
    {
        $this->printer = $printer;
    }

    public function flushListeners() {

        $this->redis->del([self::ENTER_AND_LEAVE_TASKS, self::ATLAS_ELASTICA_MONITOR, self::ONLINE_USER, self::ELASTICA_LOG]);

        $this->print('Success flush listeners.');

    }

    /**
     * @param $list
     * @param RedisListListenerCommand $command
     * @throws DBALException
     */
    public function startListener($list, RedisListListenerCommand $command) {

        if(!in_array($list, [self::ENTER_AND_LEAVE_TASKS, self::ATLAS_ELASTICA_MONITOR, self::ONLINE_USER, self::ELASTICA_LOG], true)) {
            die('This list is not supported.');
        }

        $this->loopTimestamp = time();
        $this->loopStatisticTimestamp = time();

        while($command->isRunning()) {

            $response = $this->redis->blPop([$list], self::TIMEOUT_BLPOP);

            if ($response) {

                list($listName, $serializedJob) = $response;

                if($list === self::ENTER_AND_LEAVE_TASKS) {
                    $this->doJob($serializedJob);
                }
                elseif($list === self::ELASTICA_LOG) {
                    $this->elasticaLog($serializedJob);
                }
                elseif($list === self::ATLAS_ELASTICA_MONITOR) {
                    $this->logJob($serializedJob);
                }
                elseif ($list === self::ONLINE_USER) {
                    $this->doArrayJob($serializedJob, $listName);
                }

                unset($listName, $serializedJob, $response);

            }

            $this->checkLoop($list);

        }

    }

    private function elasticaLog($serializedJob) {

        try {

            $this->checkElasticaConnect();

            list($message, $data) = array_values(json_decode($serializedJob, true));

            $this->elasticSearchLog->log($message, $data);

            $this->operations++;

            unset($job, $message, $data);

            $this->wasError = false;

        }
        catch (\Exception $exception) {

            $this->printError(RedisListListenerCommand::now() . ' ' .$exception->getMessage());
            $this->print('Sleep for 5s');
            $this->wasError = true;

            sleep(5);

        }

    }

    private function logJob($serializedJob) {

        try {

            $this->checkElasticaConnect();

            /** @var RedisListJob $job */
            $job = unserialize($serializedJob);

            $this->elasticSearchLog->log('redis_listener', $job);

            $this->operations++;

            unset($job);

            $this->wasError = false;

        }
        catch (\Exception $exception) {

            $this->printError(RedisListListenerCommand::now() . ' ' .$exception->getMessage());
            $this->print('Sleep for 5s');
            $this->wasError = true;

            sleep(5);

        }

    }

    private function checkElasticaConnect() {
        if($this->wasError) {
            $this->print('Try reconnect');
            $this->elasticSearchLog->reconnectClient();
        }
    }

    private function doArrayJob($serializedJob, $channelName) {

        $job = unserialize($serializedJob);

        if($channelName === self::ONLINE_USER) {
            $this->setOnline($job['userId'], $job['online']);
        }

        $this->operations++;

        unset($job);

    }


    private function setOnline($userId, $online) {

        $stmt = $this->conn->prepare("UPDATE dbo.fos_user SET online = :online WHERE id = :userId");

        $stmt->execute([
            'online' => intval($online),
            'userId' => intval($userId)
        ]);

        unset($stmt);

    }

    /**
     * @param $serializedJob
     * @throws DBALException
     */
    private function doJob($serializedJob) {

        /** @var RedisListJob $job */
        $job = unserialize($serializedJob);

        if($job instanceof RedisListJob) {
            switch ($job->action) {

                case RedisListJob::E_TASK : {
                    $this->onEnterTask($job);
                    break;
                }
                case RedisListJob::L_TASK : {
                    $this->onLeaveTask($job);
                    break;
                }
                case RedisListJob::PING_TIME : {
                    $this->onPingTask($job);
                    break;
                }
            }
        }

        $this->operations++;

        unset($job);

    }

    /**
     * @param RedisListJob $job
     * @throws \Doctrine\DBAL\DBALException
     */
    private function onEnterTask(RedisListJob $job) {

        $this->redis->hSet(
            TaskManagerTopic::ENTER_ON_TASK,
            $job->resourceId . $job->processInstanceId,
            $job->processInstanceId .'|'. $job->userId . '|' .$job->date->getTimestamp() . '|' . $job->rootId);

        $stmt = $this->conn->prepare("INSERT INTO dbo.process_enter_history
            (process_instance_id, user_id, date_enter, date_leave, [time], flag1, flag2, real_time)
            VALUES (:processInstanceId, :userId, :dateEnter, null, 0, null, null, 0)");

        $stmt->execute([
            'processInstanceId' => $job->processInstanceId,
            'userId' => $job->userId,
            'dateEnter' => $job->date->format('Y-m-d H:i:s')
        ]);

        unset($stmt);
    }

    /**
     * @param RedisListJob $job
     * @throws \Doctrine\DBAL\DBALException
     */
    private function onLeaveTask(RedisListJob $job) {

        $this->removeEnterOnTaskKey($job);

        $stmt = $this->conn->prepare("UPDATE dbo.process_enter_history
          SET date_leave = :dateLeave, [time] = DATEDIFF(s, date_enter, :dateLeave2)
          WHERE id = (SELECT TOP 1 id FROM dbo.process_enter_history WHERE process_instance_id = :processInstanceId AND user_id = :userId AND date_leave IS NULL AND date_enter > :lastHour ORDER BY id DESC)");

        $stmt->execute([
            'processInstanceId' => $job->processInstanceId,
            'userId' => $job->userId,
            'dateLeave' => $job->date->format('Y-m-d H:i:s'),
            'dateLeave2' => $job->date->format('Y-m-d H:i:s'),
            'lastHour' => (new \DateTime())->modify('-1 hour')->format('Y-m-d H:i:s')
        ]);
        
        unset($stmt);
    }

    private function removeEnterOnTaskKey(RedisListJob $job) {

        $this->redis->hdel(TaskManagerTopic::ENTER_ON_TASK, [$job->resourceId . $job->processInstanceId]);

    }

    /**
     * @param RedisListJob $job
     * @throws \Doctrine\DBAL\DBALException
     */
    private function onSetOnline(RedisListJob $job) {

        $stmt = $this->conn->prepare("UPDATE dbo.process_enter_history
          SET real_time = real_time + :pingTime
          WHERE id = (SELECT TOP 1 id FROM dbo.process_enter_history WHERE process_instance_id = :processInstanceId AND user_id = :userId AND date_leave IS NULL AND date_enter > :lastHour ORDER BY id DESC)");

        $stmt->execute([
            'processInstanceId' => $job->processInstanceId,
            'userId' => $job->userId,
            'pingTime' => intval($job->time),
            'lastHour' => (new \DateTime())->modify('-1 hour')->format('Y-m-d H:i:s')
        ]);
        
        unset($stmt);
    }

    /**
     * @param RedisListJob $job
     * @throws \Doctrine\DBAL\DBALException
     */
    private function onPingTask(RedisListJob $job) {

        $stmt = $this->conn->prepare("UPDATE dbo.process_enter_history
          SET real_time = real_time + :pingTime
          WHERE id = (SELECT TOP 1 id FROM dbo.process_enter_history WHERE process_instance_id = :processInstanceId AND user_id = :userId AND date_leave IS NULL AND date_enter > :lastHour ORDER BY id DESC)");

        $stmt->execute([
            'processInstanceId' => $job->processInstanceId,
            'userId' => $job->userId,
            'pingTime' => intval($job->time),
            'lastHour' => (new \DateTime())->modify('-1 hour')->format('Y-m-d H:i:s')
        ]);

        unset($stmt);
    }

    /**
     * @param $processInstanceId
     * @param $userId
     * @param int $flagValue
     * @throws \Doctrine\DBAL\DBALException
     */
    private function flagTask($processInstanceId, $userId, $flagValue = 1) {

        $stmt = $this->conn->prepare("UPDATE dbo.process_enter_history
          SET flag1 = :flagValue
          WHERE id = (
            SELECT TOP 1 id FROM dbo.process_enter_history 
            WHERE process_instance_id = :processInstanceId 
            AND user_id = :userId 
            AND date_leave IS NULL 
            AND date_enter < :oneHour 
            AND flag1 IS NULL 
            ORDER BY id DESC)");

        $stmt->execute([
            'processInstanceId' => $processInstanceId,
            'userId' => $userId,
            'flagValue' => $flagValue,
            'oneHour' => (new \DateTime())->modify('+1 hour')->format('Y-m-d H:i:s')
        ]);
        
        unset($stmt);
    }

    /**
     * Metoda, która uruchamia skrypt nie częściej niż raz na 30/60 sekund
     *
     * @param $list
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    private function checkLoop($list) {

        if((time() - $this->loopTimestamp) > self::LOOP_TIMEOUT) {

            if($list === self::ENTER_AND_LEAVE_TASKS) {
                $this->checkTasks();
            }

            if($list === self::ATLAS_ELASTICA_MONITOR) {
                $this->elasticSearchLog->checkChangeIndex();
            }

            $this->loopTimestamp = time();
        }

        if((time() - $this->loopStatisticTimestamp) > self::LOOP_TIMEOUT_STATISTIC) {

            $this->printOperationsAmount();
            $this->printUsageMemory();

            $this->loopStatisticTimestamp = time();
        }

    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    private function checkTasks() {

        $enterTasks = $this->redis->hGetAll(TaskManagerTopic::ENTER_ON_TASK);

        $now = (new \DateTime())->getTimestamp();

        foreach ($enterTasks as $key => $enterTask) {

            $enterData = explode('|', $enterTask);

            /**  Jeżeli znajdzie krok w którym ktoś jest 1h + 1 min to usuwa z Redisa (nie udało się nawet wyrzucić) */

            if(($now - intval($enterData[2])) > (self::LIMIT_TIME_ON_CASE + 60)) {

                $this->redis->hdel(TaskManagerTopic::ENTER_ON_TASK, [$key]);

                $this->flagTask(intval($enterData[0]), intval($enterData[1]), 2);

            }

            /** Jeżeli znajdzie krok w którym ktoś jest 1h próbuje dać kick'a */

            elseif(($now - intval($enterData[2])) > self::LIMIT_TIME_ON_CASE) {

                try {

                    $this->flagTask(intval($enterData[0]), intval($enterData[1]), 1);

                    /** Wyrzucenie osoby z formularza */
                    $pushEvent = [
                        'event' => [
                            'processInstanceId' => intval($enterData[0]),
                            'kickUserId' => intval($enterData[1]),
                            'whoKicked' => 'System',
                            'reason' => 'timeout',
                            'action' => TaskManagerTopic::ACTION_KICK
                        ]
                    ];

                    $this->pusher->push($pushEvent, 'atlas_task_manager');
                    unset($pushEvent);
                    $this->print('Kick user ' . $enterData[1]);

                }
                catch (\Exception $exception) {

                    $this->printError(RedisListListenerCommand::now() . ' Error in kick User. ' . $exception->getMessage());

                }

            }

            unset($enterData);
        }

        unset($enterTasks);
        unset($now);

    }

    private function printOperationsAmount() {

        $this->print(RedisListListenerCommand::now() . ' Op/s: ' . (($this->operations > 0) ? (number_format((float)($this->operations / (time() - $this->loopStatisticTimestamp)), 2, '.', '')) : 0) . ' (' . $this->operations . ' operations in ' . (time() - $this->loopStatisticTimestamp) . ' seconds.)');
        $this->operations = 0;

    }

    private function printUsageMemory() {
        $this->print(RedisListListenerCommand::now() . ' Used memory: ' . number_format((memory_get_usage() / 1024 / 1024), 3));
    }

    private function printError($message) {
        if($this->printer !== null) {
            $this->printer->printLineError($message);
        }
    }

    private function print($message) {
        if($this->printer !== null) {
            $this->printer->printLine($message);
        }
    }

}
