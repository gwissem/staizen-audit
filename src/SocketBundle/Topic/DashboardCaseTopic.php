<?php

namespace SocketBundle\Topic;

use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Ratchet\Wamp\WampConnection;
use SocketBundle\Periodic\StatusLoggerPeriodic;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

class DashboardCaseTopic implements TopicInterface, PushableTopicInterface
{
    protected $clientManipulator;
    protected $tokenStorage;

    /**
     * @param ClientManipulatorInterface $clientManipulator
     */
    public function __construct(ClientManipulatorInterface $clientManipulator, TokenStorage $tokenStorage)
    {
        $this->clientManipulator = $clientManipulator;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * This will receive any Subscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {

//        $users = $this->clientManipulator->getAll($topic);
//
//        foreach ($users as $userAndConnection) {
//
//            /** @var User $user */
//            $user = $userAndConnection['client'];
//            echo $user->getUsername();
//
//        }
//
//        $janusz = $this->clientManipulator->findByUsername($topic,'janusz.nietypowy');


        //this will broadcast the message to ALL subscribers of this topic.
        /* $topic->broadcast(['msg' => $connection->resourceId . " has joined " . $topic->getId()]);*/
    }

    /**
     * This will receive any UnSubscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {
        //this will broadcast the message to ALL subscribers of this topic.
        /*$topic->broadcast(['msg' => $connection->resourceId . " has left " . $topic->getId()]);*/
    }


    /**
     * This will receive any Publish requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @param $event
     * @param array $exclude
     * @param array $eligible
     * @return mixed|void
     */
    public function onPublish(ConnectionInterface $connection, Topic $topic, WampRequest $request, $event, array $exclude, array $eligible)
    {
        /*
            $topic->getId() will contain the FULL requested uri, so you can proceed based on that

            if ($topic->getId() === 'acme/channel/shout')
               //shout something to all subs.
        */

        $topic->broadcast([
            'msg' => $event,
        ]);
    }

    /**
     * Wysyła nowe zadanie dla konsultantów
     *
     * @param Topic $topic
     * @param WampRequest $request
     * @param array|string $data
     * @param string $provider The name of pusher who push the data
     * @return bool|void
     */
    public function onPush(Topic $topic, WampRequest $request, $data, $provider)
    {

        /**
         * SEND FOR SPECIALS USERS
         */

        StatusLoggerPeriodic::incrementConnections();

        $action = array_key_exists('action', $data) && isset($data['action']) ? $data['action'] : 'create';

        if (empty($data['task'])) {
            /** Brak zadania */
        } else {

            $broadcastData = ['task' => $data['task'], 'action' => $action];

            if (isset($data['users']) && !empty($data['users'])) {

                $clients = [];

                /** @var WampConnection $client * */
                foreach ($topic as $client) {

                    $user = $this->clientManipulator->getClient($client);
                    if ($user instanceof User) {
                        if (in_array($user->getId(), $data['users'])) {
                            $clients[] = $client->WAMP->sessionId;
                        }
                    }

                }

                // $users = $this->clientManipulator->getAll($topic);

                $topic->broadcast($broadcastData, [], $clients);
            } else {

                $topic->broadcast($broadcastData);

            }

        }
    }

    /**
     * Like RPC is will use to prefix the channel
     * @return string
     */
    public function getName()
    {
        return 'atlas.dashboard.case';
    }
}