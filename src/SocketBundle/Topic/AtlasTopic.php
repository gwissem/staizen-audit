<?php

namespace SocketBundle\Topic;

use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Monolog\Logger;
use Predis\Client;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use SocketBundle\Exception\AtlasWebsocketException;
use SocketBundle\Periodic\StatusLoggerPeriodic;
use SocketBundle\Service\RedisListListenerService;
use SocketBundle\Utils\UtilConnection;
use Symfony\Component\Debug\Exception\UndefinedMethodException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;
use AppBundle\Service\LogsUserService;

class AtlasTopic implements TopicInterface, PushableTopicInterface
{
    use ActiveUsersTrait,
        UtilConnection;

    const ACTION_NOTIFICATION = 'action_notification';

    protected $clientManipulator;

    /** @var  EntityManager */
    protected $em;

    protected $logUserService;

    /** @var  Logger */
    protected $monolog;

    /** @var  Client */
    protected $redis;

    /**
     * @param ClientManipulatorInterface $clientManipulator
     * @param EntityManager $entityManager
     * @param LogsUserService $logUserService
     * @param Logger $logger
     * @param Client $redis
     */
    public function __construct(ClientManipulatorInterface $clientManipulator,
                                EntityManager $entityManager,
                                LogsUserService $logUserService,
                                Logger $logger,
                                Client $redis
    )
    {
        $this->clientManipulator = $clientManipulator;
        $this->em = $entityManager;
        $this->logUserService = $logUserService;
        $this->monolog = $logger;
        $this->redis = $redis;
    }

    /**
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @throws \SocketBundle\Exception\AtlasWebsocketException
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {

        $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($topic, $connection) {

            $logIn = $this->connectUser($user, $resourceId, $connection);

            $connection->lastPing = new \DateTime();

            /** Pierwsze połączenie, trzeba ustawić, że użytkownik Online */
            if ($logIn) {

                $this->redis->rpush(
                    RedisListListenerService::ONLINE_USER,
                    [
                        serialize([
                            'userId' => $user->getId(),
                            'online' => 1
                        ])
                    ]
                );

//              $this->logUserService->createLoginLog($entityUser);

            }

            unset($logIn);

        });
    }

    /**
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @throws \SocketBundle\Exception\AtlasWebsocketException
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {
        $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($topic) {

            $this->disconnectUser($user->getId(), $resourceId, function ($disconnected, $activeTask) use ($user) {

                if ($disconnected) {
                    /** Użytkownik wyszedł z Atlasa - na każdej zakładce */
                    $this->setUserOffline($user->getId());
                }

            });
        });
    }

    public function setUserOffline($userId) {

        $this->redis->rpush(
            RedisListListenerService::ONLINE_USER,
            [
                serialize([
                    'userId' => $userId,
                    'online' => 0
                ])
            ]
        );

//        $this->redis->del([TaskManagerTopic::REDIS_KEY_DATE_LAST_LEAVE_TASK . $userId]);
//        $this->logUserService->createLogoutLog($entityUser);

    }

    /**
     * This will receive any Publish requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @param $event
     * @param array $exclude
     * @param array $eligible
     * @return mixed|void
     */
    public function onPublish(ConnectionInterface $connection, Topic $topic, WampRequest $request, $event, array $exclude, array $eligible)
    {

        $connection->lastPing = new \DateTime();

        /*
         *
            $topic->getId() will contain the FULL requested uri, so you can proceed based on that

            if ($topic->getId() === 'acme/channel/shout')
               //shout something to all subs.
        */

//        $topic->broadcast([
//            'msg' => $event,
//        ]);
    }

    /**
     * @param Topic $topic
     * @param WampRequest $request
     * @param array|string $data
     * @param string $provider The name of pusher who push the data
     */
    public function onPush(Topic $topic, WampRequest $request, $data, $provider)
    {

        StatusLoggerPeriodic::incrementConnections();

        if (isset($data['action'])) {

            $action = $data['action'];
            $event = $data['data'];

            /** Wysłanie powiadomień do userów */
            if ($action === self::ACTION_NOTIFICATION) {

                $callback = null;

                if(isset($event['callback']) && !empty($event['callback']) && is_array($event['callback'])) {
                    $resolverCallback = new OptionsResolver();
                    $resolverCallback->setDefaults([
                        'type' => 'getTask',
                        'id' => null
                    ]);
                    $callback = $resolverCallback->resolve($event['callback']);
                    unset($resolverCallback);
                }

                $notification = [
                    'action' => 'notification',
                    'message' => $event['message'],
                    'timeout' => $event['timeout'],
                    "tapToDismiss" => isset($event['tapToDismiss']) ? $event['tapToDismiss'] : true,
                    'withIcon' => $event['withIcon'],
                    'title' => isset($event['title']) ? $event['title'] : '',
                    'extraClass' => isset($event['extraClass']) ? $event['extraClass'] : '',
                    'type' => $event['type'],
                    'closeButton' => isset($event['closeButton']) ? $event['closeButton'] : false,
                    'callback' => $callback,
                    'refresh' => isset($event['refresh']) ? $event['refresh'] : '',
                    'rootId' => isset($event['rootId']) ? $event['rootId'] : ''
                ];

                if (isset($event['forAll']) && $event['forAll']) {

                    $topic->broadcast($notification);

                } else {

                    $clients = [];

                    $allClients = $this->clientManipulator->getAll($topic);

                    if($allClients !== false) {
                        /** @var User $user */
                        foreach ($allClients as $user) {

                            if(in_array($user['client']->getId(), $event['users']) ) {
                                $clients[] = $user['connection']->WAMP->sessionId;
                            }

                        }
                    }

                    if(count($clients) > 0) {
                        $topic->broadcast($notification, [], $clients);
                    }

                    unset($callback, $clients, $allClients);

                }


            }

        }

//        $clients = [];

        /** @var ConnectionInterface $client * */
//        foreach($topic as $client){
//            $clients[] = $this->clientManipulator->getClient($client)->getUsername();
//        }

//        $users = $this->clientManipulator->getAll($topic);

//        $topic->broadcast($data);
    }

    protected function logException($exception) {
        $this->monolog->log('ERROR', AtlasWebsocketException::ExceptionToJson($exception));
    }

    /**
     * Like RPC is will use to prefix the channel
     * @return string
     */
    public function getName()
    {
        return 'atlas.topic';
    }
}