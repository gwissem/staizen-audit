<?php

namespace SocketBundle\Topic;

use Ratchet\Wamp\WampConnection;
use SocketBundle\Periodic\StatusLoggerPeriodic;
use UserBundle\Entity\User;

trait ActiveUsersTrait
{

    /**
     *
     * activeUsersList: [
     *      userId: [
     *          connections: [
     *                 resourceId: processInstanceId
     *          ],
     *          sockets: [
     *                 resourceId: connection
     *          ],
     *          user: User
     *      ]
     * ]
     *
     */

    protected $activeUsersList = [];

    protected function connectUser($user, $resourceId, $connection, $resource = null)
    {
        if ($user instanceof User) {

            if (isset($this->activeUsersList[$user->getId()])) {
                $this->activeUsersList[$user->getId()]['connections'][$resourceId] = $resource;
                $this->activeUsersList[$user->getId()]['sockets'][$resourceId] = $connection;
            } else {
                $this->activeUsersList[$user->getId()] = [
                    'sockets' => [
                        $resourceId => $connection
                    ],
                    'connections' => [
                        $resourceId => $resource
                    ],
                    'user' => $user
                ];
            }

            return (count($this->activeUsersList[$user->getId()]['connections']) === 1);

        }

        return null;
    }

    public function getIdsOfConnectedUsers() {

        $ids = [];

        foreach ($this->activeUsersList as $userId => $item) {
            if(isset($item['connections']) && count($item['connections']) > 0) {
                $ids[] = $userId;
            }
        }

        return $ids;

    }

    public function getAllConnectedUsers() {
        return $this->activeUsersList;
    }

    public function disconnectUser($userId, $resourceId, $callback)
    {
        if (isset($this->activeUsersList[$userId])) {

            $activeTask = $this->activeUsersList[$userId]['connections'][$resourceId];

            unset($this->activeUsersList[$userId]['connections'][$resourceId]);
            unset($this->activeUsersList[$userId]['sockets'][$resourceId]);

            call_user_func($callback, (count($this->activeUsersList[$userId]['connections']) == 0), $activeTask);

        }
    }

    protected function countConnectionsOfUser($user)
    {

        if ($user instanceof User) {
            if (isset($this->activeUsersList[$user->getId()])) {
                return count($this->activeUsersList[$user->getId()]['connections']);
            } else {
                return 0;
            }

        }

        return false;
    }

    protected function getResourceIdByTaskId($processInstanceId) {

        foreach ($this->activeUsersList as $user) {

            foreach ($user['connections'] as $key => $resourceId) {
                if(intval($resourceId) === $processInstanceId) {
                    return $key;
                }
            }

        }

        return null;

    }

    protected function getCurrentReserve($userId, $resourceId) {

        return $this->activeUsersList[$userId]['connections'][$resourceId];

    }

    protected function cancelCurrentReserved($userId, $resourceId)
    {
        if (!isset($this->activeUsersList[$userId])) return null;

        $oldReservedTask = $this->activeUsersList[$userId]['connections'][$resourceId];

        $this->activeUsersList[$userId]['connections'][$resourceId] = null;

        return $oldReservedTask;

    }

    protected function reserveTask($userId, $resourceId, $processInstanceId, $callback)
    {

        if (isset($this->activeUsersList[$userId])) {

            $this->activeUsersList[$userId]['connections'][$resourceId] = $processInstanceId;
            return call_user_func($callback);

        }

        return false;

    }

    public function checkPings() {

        $now = (new \DateTime())->getTimestamp();

        foreach ($this->getAllConnectedUsers() as $userId => $connectedUser) {
            /** @var WampConnection $item */
            foreach ($connectedUser['sockets'] as $resource => $item) {

                try {
                    if($item->lastPing) {
                        if($now - $item->lastPing->getTimestamp() > StatusLoggerPeriodic::LIVE_TIME_CONNECTION) {
                            $item->close();
                        }
                    }
                }
                catch (\Exception $exception) {

                }

            }
        }
    }

    /**
     * @param $userId
     * @param $processInstanceId
     * @param $callback
     * @return null|string
     */
    public function kickUserFromReservedTask($userId, $processInstanceId, $callback) {

        $resource = null;

        if(isset($this->activeUsersList[$userId]) && count($this->activeUsersList[$userId]['connections']) > 0) {

            foreach ($this->activeUsersList[$userId]['connections'] as $resourceId => $processId) {
                if($processId == $processInstanceId) {
                    $resource = $resourceId;
                    $this->activeUsersList[$userId]['connections'][$resourceId] = null;
                    break;
                }
            }

        }

        call_user_func($callback, $processInstanceId, $resource);

        return $resource;
    }

    /**
     * @param $userId
     * @return null|User
     */
    public function getActiveUser($userId) {
        return (isset($this->activeUsersList[$userId])) ? $this->activeUsersList[$userId]['user'] : null;
    }

    public function getReservedTasksInWebSocket() {

        $tasks = [];

        foreach ($this->activeUsersList as $connectedUser) {
            if(!empty($connectedUser['connections'])) {
                foreach ($connectedUser['connections'] as $connection) {

                    if($connection !== null) {
                        $tasks[] = $connection;
                    }

                }
            }
        }

        return $tasks;

    }

    public function getActiveConnectionsPerUsers() {

        $users = [];

        foreach ($this->activeUsersList as $key => $connectedUser) {

            if(!empty($connectedUser['connections'])) {

                $newUser = [
                    'username' => $connectedUser['user']->getUsername(),
                    'connections' => 0,
                    'tasks' => []
                ];

                foreach ($connectedUser['connections'] as $connection) {
                    $newUser['connections']++;
                    $newUser['tasks'][] = $connection;
                }

                $users[] = $newUser;

            }
        }

        return $users;

    }

}