<?php

namespace SocketBundle\Topic;

use CaseBundle\Service\ProcessHandler;
use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Predis\Client;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Ratchet\Wamp\WampConnection;
use SocketBundle\Service\RedisListListenerService;
use SocketBundle\Utils\CurrentTaskRedisValue;
use SocketBundle\Utils\RedisListJob;
use SocketBundle\Utils\UtilConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use UserBundle\Entity\User;

class ProcessManagerTopic implements TopicInterface, PushableTopicInterface
{
    use ActiveUsersTrait,
        UtilConnection;

    protected $clientManipulator;

    /** @var Client  */
    protected $redis;

    /** @var ContainerInterface  */
    protected $container;

    private $cos;

    /**
     * @param ClientManipulatorInterface $clientManipulator
     * @param Client $redis
     * @param ContainerInterface $container
     */
    public function __construct(ClientManipulatorInterface $clientManipulator, Client $redis, ContainerInterface $container)
    {
        $this->clientManipulator = $clientManipulator;
        $this->redis = $redis;
        $this->container = $container;

    }

    /**
     * This will receive any Subscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     * @throws \SocketBundle\Exception\AtlasWebsocketException
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {

        $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($topic, $connection, $request) {

            $message = [
                'type' => 'info',
                'message' => $user->getName() . ' wszedł na ten process.'
            ];

            /** @var WampConnection $connection */
            $connection->lastPing = new \DateTime();

            $resource = new \stdClass();
            $resource->process = $request->getAttributes()->get('process');
            $resource->step = null;

            $users = $this->checkReservedResource('process', $resource->process);

            $this->connectUser($user, $resourceId, $connection, $resource);

            $excluded = [
                $connection->WAMP->sessionId
            ];

            $topic->broadcast($message, $excluded);

            if(count($users)) {
                $connection->event($topic->getId(), [
                    'type' => 'whoUserProcess',
                    'users' => $users
                ]);
            }

        });

    }

    /**
     * This will receive any UnSubscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     * @throws \SocketBundle\Exception\AtlasWebsocketException
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {

        $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($topic) {

            $this->disconnectUser($user->getId(), $resourceId, function ($disconnected, $activeResource) use ($topic, $user, $resourceId) {

                if($disconnected) {

                    $message = [
                        'type' => 'exit',
                        'message' => $user->getName() . ' wyszedł z tego procesu.'
                    ];

                    $topic->broadcast($message);

                }

            });

        });

    }

    /**
     * This will receive any Publish requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @param array $event ['action' => '', 'processInstanceId' => '']
     * @param array $exclude
     * @param array $eligible
     * @return mixed|void
     * @throws \SocketBundle\Exception\AtlasWebsocketException
     * @throws \Exception
     */
    public function onPublish(ConnectionInterface $connection, Topic $topic, WampRequest $request, $event, array $exclude, array $eligible)
    {

        $connection->lastPing = new \DateTime();

        /** @var WampConnection $connection */
        array_push($exclude, $connection->WAMP->sessionId);

        $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($connection, $exclude, $event, $topic) {

            $type = (isset($event['type'])) ? $event['type'] : null;
            $stepId = (isset($event['stepId'])) ? $event['stepId'] : null;

            if($stepId) {

                $currentReserve = $this->getCurrentReserve($user->getId(), $resourceId);

                if($type === "close") {
                    if(isset($currentReserve->step)) {
                        $currentReserve->step = null;
                    }
                }

                $users = $this->checkReservedResource('step', $stepId);
                $usersConnections = $this->checkReservedResource('step', $stepId, true);

                $response = [];

                if($type === "open") {

                    $currentReserve->step = $stepId;

                    $response = [
                        'message' => 'Użytkownik ' . $user->getName() . ' wszedł na krok ' . $stepId,
                        'type' => 'info'
                    ];

                    if(count($users)) {
                        $connection->event($topic->getId(), [
                            'type' => 'whoUserStep',
                            'users' => $users
                        ]);
                    }


                }
                else if($type === "close") {

                    $response = [
                        'message' => 'Użytkownik ' . $user->getName() . ' wyszedł z kroku ' . $stepId,
                        'type' => 'exit'
                    ];

                }

                foreach ($usersConnections as $usersConnection) {
                    $usersConnection->event($topic->getId(), $response);
                }

            }

        });

    }

    /**
     *
     * @param Topic $topic
     * @param WampRequest $request
     * @param array|string $data
     * @param string $provider The name of pusher who push the data
     * @return bool|void
     */
    public function onPush(Topic $topic, WampRequest $request, $data, $provider)
    {


    }



    protected function cancelCurrentReservedResource($userId, $resourceId)
    {
        if (!isset($this->activeUsersList[$userId])) return null;

        $oldReservedResources = $this->activeUsersList[$userId]['connections'][$resourceId];

        $this->activeUsersList[$userId]['connections'][$resourceId] = null;

        return $oldReservedResources;

    }

    protected function reserveResource($userId, $resourceId, $resource, $callback)
    {

        if (isset($this->activeUsersList[$userId])) {

            $this->activeUsersList[$userId]['connections'][$resourceId] = $resource;
            return call_user_func($callback);

        }

        return false;

    }

    /**
     * @param $type
     * @param $id
     * @param bool $returnConnections
     * @return array
     */
    protected function checkReservedResource($type, $id, $returnConnections = false) {

        $whoReserve = [];

        foreach ($this->activeUsersList as $connectedUser) {
            if(!empty($connectedUser['connections'])) {
                foreach ($connectedUser['connections'] as $resourceId => $connection) {

                    if($connection !== null) {

                        if($connection->$type == $id) {

                            if($returnConnections){
                                $whoReserve[] = $connectedUser['sockets'][$resourceId];
                            }
                            else {
                                if(empty($whoReserve[$connectedUser['user']->getName()])) {
                                    $whoReserve[] = $connectedUser['user']->getName();
                                }
                            }

                        }

                    }

                }
            }
        }

        return $whoReserve;

    }

    /**
     * Like RPC is will use to prefix the channel
     * @return string
     */
    public function getName()
    {
        return 'atlas.process.manager';
    }
}