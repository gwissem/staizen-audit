<?php

namespace SocketBundle\Topic;

use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Ratchet\Wamp\WampConnection;
use UserBundle\Entity\User;

class AtlasTelephony implements TopicInterface, PushableTopicInterface
{

    const PUSH_ACTION_REFRESH_STATUSES = 'refresh-statuses';

    protected $clientManipulator;

    /**
     * @param ClientManipulatorInterface $clientManipulator
     */
    public function __construct(ClientManipulatorInterface $clientManipulator)
    {
        $this->clientManipulator = $clientManipulator;
    }

    /**
     * This will receive any Subscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {

    }

    /**
     * This will receive any UnSubscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {

    }


    /**
     * This will receive any Publish requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @param $event
     * @param array $exclude
     * @param array $eligible
     * @return mixed|void
     */
    public function onPublish(ConnectionInterface $connection, Topic $topic, WampRequest $request, $event, array $exclude, array $eligible)
    {

    }

    /**
     * @param Topic        $topic
     * @param WampRequest  $request
     * @param array|string $data
     * @param string       $provider The name of pusher who push the data
     */
    public function onPush(Topic $topic, WampRequest $request, $data, $provider)
    {

        /**
         * SEND FOR SPECIALS USERS
         */

        $action = array_key_exists('action', $data) && isset($data['action']) ? $data['action'] : 'create';
        $clients = [];

        if($action === self::PUSH_ACTION_REFRESH_STATUSES) {

            if (isset($data['users']) && !empty($data['users'])) {

                /** Wymuszenie odświeżenia dostępnych stautusów telefoni */

                /**
                 * @var WampConnection $client
                 */
                foreach ($topic as $client) {

                    $user = $this->clientManipulator->getClient($client);
                    if ($user instanceof User) {
                        if (in_array($user->getId(), $data['users'])) {
                            $clients[] = $client->WAMP->sessionId;
                        }
                    }

                }

                if(!empty($clients)) {
                    $topic->broadcast([
                        'action' => $action
                    ], [], $clients);
                }

            }

        }
        elseif (empty($data['task'])) {

            /** Brak zadania */
        } else {

            $broadcastData = ['task' => $data['task'], 'action' => $action];

            if (isset($data['users']) && !empty($data['users'])) {

                if($action === "force-create") {
                    /**
                     * Dla "Force-create" szuka tylko pierwszego lepszego połączenia
                     *
                     * @var WampConnection $client
                     */
                    foreach ($topic as $client) {

                        $user = $this->clientManipulator->getClient($client);
                        if ($user instanceof User) {
                            if (in_array($user->getId(), $data['users'])) {
                                $clients[] = $client->WAMP->sessionId;
                                break;
                            }
                        }

                    }
                }
                else {
                    /** @var WampConnection $client * */
                    foreach ($topic as $client) {

                        $user = $this->clientManipulator->getClient($client);
                        if ($user instanceof User) {
                            if (in_array($user->getId(), $data['users'])) {
                                $clients[] = $client->WAMP->sessionId;
                            }
                        }

                    }
                }

                // $users = $this->clientManipulator->getAll($topic);

                if(!empty($clients)) {
                    $topic->broadcast($broadcastData, [], $clients);
                }

            }

        }

        unset($action);
        unset($clients);

    }

    /**
     * Like RPC is will use to prefix the channel
     * @return string
     */
    public function getName()
    {
        return 'atlas.telephony';
    }
}