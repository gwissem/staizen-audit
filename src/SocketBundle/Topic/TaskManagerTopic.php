<?php

namespace SocketBundle\Topic;

use CaseBundle\Service\ProcessHandler;
use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Predis\Client;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Ratchet\Wamp\WampConnection;
use SocketBundle\Service\RedisListListenerService;
use SocketBundle\Utils\CurrentTaskRedisValue;
use SocketBundle\Utils\RedisListJob;
use SocketBundle\Utils\UtilConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use UserBundle\Entity\User;

class TaskManagerTopic implements TopicInterface, PushableTopicInterface
{
    use ActiveUsersTrait,
        UtilConnection;

    const RESERVED_TASKS = 'reserved_tasks';
    const ENTER_ON_TASK = 'enter_on_task';
    const LEAVE_TASK = 'leave_task';
    const UPDATE_LOCATION = 'update_location_on_map';
    const ACTION_RESERVE = 'reserve_task';
    const ACTION_CANCEL_RESERVED = 'cancel_reserved_task';
    const ACTION_KICK = 'kick_user';
    const REDIS_KEY_CURRENT_TASKS = 'current.tasks';
    const REDIS_KEY_DATE_LAST_LEAVE_TASK = 'date.last.tasks:';

    protected $clientManipulator;

    /** @var Client  */
    protected $redis;

    /** @var ContainerInterface  */
    protected $container;

    /** @var  ProcessHandler */
    protected $processHandler;

    /** @var EntityManager */
    protected $em;

    protected $redisChannelOfValue;
    protected $saveRealTime = false;


    /**
     * @param ClientManipulatorInterface $clientManipulator
     * @param Client $redis
     */
    public function __construct(ClientManipulatorInterface $clientManipulator, Client $redis, ContainerInterface $container)
    {
        $this->clientManipulator = $clientManipulator;
        $this->redis = $redis;
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->processHandler = $this->container->get('case.process_handler');

        $this->saveRealTime = $this->container->getParameter('save_real_time_in_case');

        $channel = $this->container->getParameter('redis_channel');

        if(empty($channel)) {
            $this->redisChannelOfValue = 0;
        }
        else {
            $this->redisChannelOfValue = str_replace('/', '', $channel);
        }

    }

    /**
     * This will receive any Subscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     * @throws \SocketBundle\Exception\AtlasWebsocketException
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {

        $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($topic, $connection) {

            $connection->lastPing = new \DateTime();
            $this->connectUser($user, $resourceId, $connection);

        });

        //this will broadcast the message to ALL subscribers of this topic.
        /* $topic->broadcast(['msg' => $connection->resourceId . " has joined " . $topic->getId()]);*/

    }

    /**
     * This will receive any UnSubscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     * @throws \SocketBundle\Exception\AtlasWebsocketException
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {

        $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($topic) {

            $this->disconnectUser($user->getId(), $resourceId, function ($disconnected, $activeTask) use ($user, $resourceId) {

                /** wywalenie jego aktywnego zadania (jeżeli miał) z redisa */

//                $reservedTasks = $this->redis->hGetAll(self::RESERVED_TASKS);

                $this->redis->hDel(self::RESERVED_TASKS, [$activeTask]);

                /** Zapisanie wyjście z kroku */
                if($activeTask){
                    $this->removeCurrentTask($user->getId(), $resourceId);

                    $this->leaveTask($user->getId(), $activeTask, $resourceId);
//                    $this->redis->hSet(self::LEAVE_TASK, mt_rand() . time(), $activeTask .'|'. $user->getId() . '|' .(new \DateTime())->getTimestamp());

                }

                if ($disconnected) {
                    /** Użytkownik wyszedł z Operation Dashboard - na każdej zakładce */
                }

            });

        });

    }

    /**
     * This will receive any Publish requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @param array $event ['action' => '', 'processInstanceId' => '']
     * @param array $exclude
     * @param array $eligible
     * @return mixed|void
     * @throws \SocketBundle\Exception\AtlasWebsocketException
     */
    public function onPublish(ConnectionInterface $connection, Topic $topic, WampRequest $request, $event, array $exclude, array $eligible)
    {

        $connection->lastPing = new \DateTime();

        /** @var WampConnection $connection */
        $exclude[] = $connection->WAMP->sessionId;

        $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($exclude, $event, $topic) {

            $action = $event['action'] ?? null;

            switch ($action) {
                case self::ACTION_CANCEL_RESERVED : {

                    if ($reservedTask = $this->cancelCurrentReserved($user->getId(), $resourceId)) {

                        $this->redis->hDel(self::RESERVED_TASKS, $reservedTask);

                        $this->leaveTask($user->getId(), $reservedTask, $resourceId);
//                        $this->redis->hSet(self::LEAVE_TASK, mt_rand() . time(), $reservedTask .'|'. $user->getId() . '|' .(new \DateTime())->getTimestamp());

                        $this->removeCurrentTask($user->getId(), $resourceId);

                    }

                    break;
                }
                case 'ping' : {

                    if(isset($event['processInstanceId'], $event['time'])) {
                        $this->pingTask($user->getId(), $event['processInstanceId'], $resourceId, $event['time']);
                    }

                }
            }

        });

    }

    /**
     *
     * @param Topic $topic
     * @param WampRequest $request
     * @param array|string $data
     * @param string $provider The name of pusher who push the data
     * @return bool|void
     */
    public function onPush(Topic $topic, WampRequest $request, $data, $provider)
    {

        if (isset($data['event'])) {

            $exclude = [];

            if (isset($data['exclude'])) {
                array_push($exclude, $data['exclude']);
            }

            $event = $data['event'];
            $userId = (isset($data['userId'])) ? $data['userId'] : null;
            $resourceId = (isset($data['resourceId'])) ? $data['resourceId'] : null;

            if ($event['action'] === self::ACTION_RESERVE) {

                $processInstanceId = intval($event['processInstanceId']);
                $taskNameArr = $this->processHandler->taskNameOfProcessInstance($processInstanceId);

                $processDescription = $this->processHandler->getProcessInstanceDescription($processInstanceId);
                $isSameTask = false;

                /** Usunięcie z Redisa starego zadania */
                if ($oldReservedTask = $this->cancelCurrentReserved($userId, $resourceId)) {

                    $this->redis->hDel(self::RESERVED_TASKS, $oldReservedTask);

                    $this->leaveTask($userId, $oldReservedTask, $resourceId);
//                    $this->redis->hSet(self::LEAVE_TASK, mt_rand() . time(), $oldReservedTask .'|'. $userId . '|' .(new \DateTime())->getTimestamp());

                    $isSameTask = $this->processHandler->isSameTask($oldReservedTask, $processInstanceId);
                    if(!$isSameTask) {
                        $this->removeCurrentTask($userId, $resourceId);
                    }
                    unset($oldReservedTask);
                }

                if (!$this->redis->hget(self::RESERVED_TASKS, $processInstanceId)) {

                    $successReserved = $this->reserveTask($userId, $resourceId, $processInstanceId, function () use ($event, $userId, $processDescription, $processInstanceId, $isSameTask, $resourceId) {

                        /** Dodanie do redisa - zarezerwowanie zadania */

                        /** Panelu Świadczeń nie da się zarezerwować */
                        if($processDescription['stepId'] !== '1011.010') {
                            $this->redis->hSet(self::RESERVED_TASKS, $processInstanceId, $userId);
                        }

                        $this->enterOnTask($userId, $processInstanceId, $resourceId, $processDescription['rootId']);
//                        $this->redis->hSet(self::ENTER_ON_TASK, mt_rand() . time(), $processInstanceId .'|'. $userId . '|' .(new \DateTime())->getTimestamp() . '|' . $processDescription['rootId']);

                        return true;

                    });

                    if($successReserved && !$isSameTask) {

                        $currentTaskKey = $this->createKey($userId, $resourceId, $processInstanceId, $taskNameArr['groupId']);
                        $currentTaskValue = new CurrentTaskRedisValue($userId, $taskNameArr, $processDescription);

                        $this->addCurrentTask($currentTaskKey, $currentTaskValue);
                    }

                    $topic->broadcast($event, $exclude);

                }

                unset($processInstanceId, $processDescription, $taskNameArr, $isSameTask);

            }
            else if($event['action'] === self::UPDATE_LOCATION) {

                /** Zaktualizowanie lokalizacji na mapie u konsultanta */

                $resources = [];

                foreach ($event['processesId'] as $processInstanceId) {

                    $resource = $this->getResourceIdByTaskId($processInstanceId);
                    if($resource !== null) $resources[] = intval($resource);

                }

                foreach ($topic->getIterator() as $client) {

                    if(in_array($client->resourceId, $resources)) {

                        $clients[] = $client->WAMP->sessionId;

                        $topic->broadcast($event, [], $clients);
                        break;
                    }
                }

            }
            else if($event['action'] === self::ACTION_KICK && $event['kickUserId']) {

                /** Wyrzucenie usera z zadania */

                $resource = $this->kickUserFromReservedTask($event['kickUserId'], $event['processInstanceId'], function ($processInstanceId, $resourceId) use($event) {

                    $this->redis->hDel(self::RESERVED_TASKS, $processInstanceId);
                    $this->leaveTask($event['kickUserId'], $processInstanceId, $resourceId);
//                    $this->redis->hSet(self::LEAVE_TASK, mt_rand() . time(), $processInstanceId .'|'. $event['kickUserId'] . '|' .(new \DateTime())->getTimestamp());

                });

                foreach ($topic->getIterator() as $client) {
                    if($client->resourceId == $resource) {
//                        $clients[] = $client->WAMP->sessionId;
                        $topic->broadcast($event, [], [$client->WAMP->sessionId]);
                        break;
                    }
                }

            }

        } else {
            $topic->broadcast($data);
        }

    }

    public function leaveTask($userId, $oldReservedTask, $resourceId) {

        $this->callTask(new RedisListJob(RedisListJob::L_TASK, $userId, intval($oldReservedTask), $resourceId));

    }

    public function enterOnTask($userId, $oldReservedTask, $resourceId, $rootId) {

        $this->callTask(new RedisListJob(RedisListJob::E_TASK, $userId, intval($oldReservedTask), $resourceId, intval($rootId)));

    }

    public function pingTask($userId, $oldReservedTask, $resourceId, $time) {

        $this->callTask(new RedisListJob(RedisListJob::PING_TIME, $userId, intval($oldReservedTask), $resourceId, null, $time));

    }

    private function callTask(RedisListJob $job) {

        if(!$this->saveRealTime) return;

        $this->redis->rpush(
            RedisListListenerService::ENTER_AND_LEAVE_TASKS,
            [
                serialize($job)
            ]
        );

    }

    /**
     * user:{userId}:{resourceSocketId}:{processInstanceId}:{groupId}
     *
     * @param $userId
     * @param $resourceId
     * @param $processInstanceId
     * @param $groupId
     * @return string
     */
    private function createKey($userId, $resourceId, $processInstanceId, $groupId) {
        return 'user:' . $userId . ':' . $resourceId . ':' . $processInstanceId . ':' . $groupId;
    }

    /**
     * @param $userId
     * @param $resourceId
     */
    private function removeCurrentTask($userId, $resourceId) {

        $count = 1000;

        try {

            $infoRedis = $this->redis->info('Keyspace');

            if(array_key_exists('Keyspace', $infoRedis)) {

                $dbKey = 'db' . $this->redisChannelOfValue;

                if(!empty($infoRedis['Keyspace'])) {

                    if(array_key_exists($dbKey, $infoRedis['Keyspace']) && array_key_exists('keys', $infoRedis['Keyspace'][$dbKey])) {

                        $count = intval($infoRedis['Keyspace'][$dbKey]['keys']) + 10;
                        if($count > 100000) {
                            $count = 1000;
                        }

                    }
                }
            }

        }
        catch (\Exception $exception) {

        }

        $tasks = $this->redis->hscan(self::REDIS_KEY_CURRENT_TASKS, 0, [
            'MATCH' => 'user:' . $userId . ':' . $resourceId . ':*',
            'COUNT' => $count
        ]);

        if(!empty($tasks[1])) {
            $keys = [];

            foreach ($tasks[1] as $key => $field) {
                $keys[] = $key;
            }

            $this->redis->hdel(self::REDIS_KEY_CURRENT_TASKS, $keys);
        }

        $this->redis->set(self::REDIS_KEY_DATE_LAST_LEAVE_TASK . $userId, (new \DateTime())->getTimestamp());

    }

    private function addCurrentTask($currentTaskKey, $currentTaskRedisValue) {

        $this->redis->hset(self::REDIS_KEY_CURRENT_TASKS, $currentTaskKey, serialize($currentTaskRedisValue));

    }

    /**
     * Like RPC is will use to prefix the channel
     * @return string
     */
    public function getName()
    {
        return 'atlas.task.manager';
    }
}