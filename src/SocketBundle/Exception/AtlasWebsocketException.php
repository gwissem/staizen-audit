<?php

namespace SocketBundle\Exception;

use Gos\Component\WebSocketClient\Exception\WebsocketException;

class AtlasWebsocketException extends WebsocketException {


    /**
     * AtlasWebsocketException constructor.
     * @param \Throwable $exception
     * @param bool $isAdmin
     * @param int $code
     * @param null $message
     */
    public function __construct(\Throwable $exception = null, $isAdmin = false, $message = null, $code = null)
    {

//        $code = ($code === null && $exception !== null) ? $exception->getCode() : 404;

        if($isAdmin) {
            if($exception !== null) {
                $message = $exception->getMessage() . '. Line: ' . $exception->getLine() . '. File: ' . $exception->getFile();
            }
            else {
                $message = $this->getMessage() . '. Line: ' . $this->getLine() . '. File: ' . $this->getFile();
            }
        }
        else {
            if($message === null) {
                $message = $this->getMessage();
            }
        }

        parent::__construct($message, 0, $exception);
    }

    static function ExceptionToJson(\Throwable $exception) {

        $data = [
            'message' => $exception->getMessage(),
            'stack_trace' => $exception->getTraceAsString()
        ];

        return json_encode($data);

    }
}
