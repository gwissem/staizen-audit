<?php

namespace SocketBundle\Exception;

use Gos\Component\WebSocketClient\Exception\WebsocketException;

class NotFoundUserConnectionException extends WebsocketException {

}
