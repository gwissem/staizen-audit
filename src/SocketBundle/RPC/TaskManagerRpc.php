<?php

namespace SocketBundle\RPC;

use FOS\UserBundle\Model\UserManager;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use Predis\Client;
use Ratchet\ConnectionInterface;
use Gos\Bundle\WebSocketBundle\RPC\RpcInterface;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Ratchet\Wamp\WampConnection;
use SocketBundle\Topic\AtlasTopic;
use SocketBundle\Topic\TaskManagerTopic;
use SocketBundle\Utils\UtilConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\User;

class TaskManagerRpc implements RpcInterface
{

    use UtilConnection;

    protected $clientManipulator;
    protected $pusher;
    protected $redis;
    protected $userManager;
    protected $container;
    protected $taskManagerTopic;
    protected $connectedUserTemp = [];

    /** @var AtlasTopic*/
    protected $atlasTopic;

    /**
     * @param ClientManipulatorInterface $clientManipulator
     * @param PusherInterface $pusherDecorator
     * @param Client $redis
     * @param UserManager $userManager
     * @param ContainerInterface $container
     */
    public function __construct(ClientManipulatorInterface $clientManipulator,
                                PusherInterface $pusherDecorator,
                                Client $redis,
                                UserManager $userManager,
                                ContainerInterface $container
    )
    {
        $this->clientManipulator = $clientManipulator;
        $this->pusher = $pusherDecorator;
        $this->redis = $redis;
        $this->userManager = $userManager;
        $this->container = $container;
        $this->taskManagerTopic = $container->get('atlas.topic.task.manager');
        $this->atlasTopic = $container->get('atlas.topic_service');
    }

    public function tryReserve(ConnectionInterface $connection, WampRequest $request, $params)
    {

        /** @var WampConnection $connection */
        $exclude = $connection->WAMP->sessionId;

        $response = $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($exclude, $params) {

            $userIdWhoReserve = $this->redis->hget(TaskManagerTopic::RESERVED_TASKS, $params['processInstanceId']);
            $reserveSameUser = ($userIdWhoReserve == $user->getId());

            $whoReserve = null;

            $isReserved = ($userIdWhoReserve !== null);
            $isForce = (isset($params['forceReserve']) && $params['forceReserve']);

            /** Jeżeli nie jest zarezerwowane lub jest i ma możliwość wyrzucenia */

            if (!$isReserved || ($isReserved && $isForce)) {

                if($isReserved) {

                    /** wyrzucenie osoby która rezerwuje */

                    $data = [
                        'exclude' => $exclude,
                        'resourceId' => $resourceId,
                        'userId' => $user->getId(),
                        'event' => [
                            'processInstanceId' => $params['processInstanceId'],
                            'kickUserId' => $userIdWhoReserve,
                            'whoKicked' => $user->getName(),
                            'action' => TaskManagerTopic::ACTION_KICK
                        ]
                    ];

                    $this->pusher->push($data, 'atlas_task_manager');

                }

                $data = [
                    'exclude' => $exclude,
                    'resourceId' => $resourceId,
                    'userId' => $user->getId(),
                    'event' => [
                        'processInstanceId' => $params['processInstanceId'],
                        'action' => TaskManagerTopic::ACTION_RESERVE
                    ]
                ];

                /** Zarezerwowanie zadania */

                $this->pusher->push($data, 'atlas_task_manager');

            } else {

                //TODO - socket_block

                /** @var User $whoReserve */
                $whoReserve = $this->userManager->findUserBy(['id' => $userIdWhoReserve]);

                if($whoReserve) {
                    $this->userManager->reloadUser($whoReserve);
                }

            }

            return array(
                'isReserved' => ($isReserved && !$isForce),
                'isSameUser' => $reserveSameUser,
                'whoReserved' => ($whoReserve) ? $whoReserve->getName() : ''
            );

        });

        return $response;

    }

    /** Nie używane */
    public function cancelReservation(ConnectionInterface $connection, WampRequest $request, $params) {

        $response = $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($params) {

            /** Zapisanie wyjście z kroku */
//            $this->redis->hSet(self::LEAVE_TASK, $activeTask, $user->getId() . '|' .(new \DateTime())->getTimestamp());
//
//            $userIdWhoReserve = $this->redis->hget(TaskManagerTopic::RESERVED_TASKS, $params['processInstanceId']);

            return array(
                'isCancel' => true
            );

        });

        return $response;

    }

    public function getReservation(ConnectionInterface $connection, WampRequest $request, $params)
    {

        $response = $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($params) {

            return array(
                'reservation' => $this->taskManagerTopic->getActiveConnectionsPerUsers()
            );

        });

        return $response;

    }

    public function getConnectedUsers(ConnectionInterface $connection, WampRequest $request, $params)
    {

        $response = $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($params) {

            unset($this->connectedUserTemp);

            $this->connectedUserTemp = [
                'home' => [],
                'dashboard' => []
            ];

            foreach ($this->atlasTopic->getAllConnectedUsers() as $userId => $connectedUser) {
                $this->connectedUserTemp['home'][$userId] = 0;
                foreach ($connectedUser['sockets'] as $resource => $item) {
                    $this->connectedUserTemp['home'][$userId]++;
                }
            }

            foreach ($this->taskManagerTopic->getAllConnectedUsers() as $userId => $connectedUser) {
                $this->connectedUserTemp['dashboard'][$userId] = 0;
                foreach ($connectedUser['sockets'] as $resource => $item) {
                    $this->connectedUserTemp['dashboard'][$userId]++;
                }
            }

            return $this->connectedUserTemp;

        });

        return $response;

    }

    /**
     *
     * @return string
     */
    public function getName()
    {
        return 'atlas.rpc.task.manager';
    }
}