<?php

namespace SocketBundle\RPC;

use CaseBundle\Service\CarCallHandler;
use CaseBundle\Service\ProcessHandler;
use Doctrine\ORM\EntityManager;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use function PHPSTORM_META\type;
use Predis\Client;
use Ratchet\ConnectionInterface;
use Gos\Bundle\WebSocketBundle\RPC\RpcInterface;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use SocketBundle\Exception\AtlasWebsocketException;
use SocketBundle\Periodic\StatusLoggerPeriodic;
use SocketBundle\Utils\UtilConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\User;

class AtlasTelephonyRpc implements RpcInterface
{

    use UtilConnection;

    const REDIS_PREFIX_ID_DIALOG = 'jabber_id_dialog_';
    const FOLLOW_UP_START = '1006.001';
    const CALL_PHONE_PROCESS_ID = '1012.002';
    const PATH_NUMBER_OF_CLIENT = '197';
    const PATH_PLATFORM_ID = '253';
    const PATH_WELCOME = '403';
    const PATH_CASE_ID = '235';


    protected $clientManipulator;
    protected $carCallHandler;
    protected $processHandler;
    protected $pusher;
    protected $eCallPlatformId;
    protected $redis;
    protected $em;
    protected $container;
    protected $monolog;
    protected $translator;

    /** @var array */
    protected $limitProgress = 12;
    protected $progressStatus = [];

    /**
     * @param ClientManipulatorInterface $clientManipulator
     * @param CarCallHandler $carCallHandler
     * @param ProcessHandler $processHandler
     * @param PusherInterface $pusherDecorator
     * @param $eCallPlatformId
     * @param Client $redis
     * @param EntityManager $em
     * @param ContainerInterface $container
     */
    public function __construct(ClientManipulatorInterface $clientManipulator,
                                CarCallHandler $carCallHandler,
                                ProcessHandler $processHandler,
                                PusherInterface $pusherDecorator,
                                $eCallPlatformId,
                                Client $redis,
                                EntityManager $em,
                                ContainerInterface $container
    )
    {
        $this->clientManipulator = $clientManipulator;
        $this->carCallHandler = $carCallHandler;
        $this->processHandler = $processHandler;
        $this->pusher = $pusherDecorator;

        $this->eCallPlatformId = $eCallPlatformId;
        $this->redis = $redis;
        $this->em = $em;
        $this->container = $container;
        $this->monolog = $container->get('monolog.logger.dashboard_websocket');
        $this->translator = $container->get('translator');

    }
//
//    public function checkStatus(ConnectionInterface $connection, WampRequest $request, $params)
//    {
//
//        $response = $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($params) {
//
//            $Error = false;
//
//            try {
//                $data = $this->redis->hgetall(\JabberBundle\Controller\DefaultController::INDEX_JABBER_REDIS . $user->getId());
////                $inRedis = !empty($data);
//
//                $data['id'] = $user->getId();
//                $data['username'] = $user->getUsername();
//
//                $client = (new ClientRequest(new CUCUMUser(null, null, $data)))
//                    ->setActionUrl(sprintf('User/%s', $user->getUsername()))
//                    ->execute('get');
//
//                $status = ($client->getStatusCode() === Response::HTTP_OK);
//                $value = false;
//                $reasonCodeId = false;
//
//                if ($status) {
//                    $crawler = new Crawler();
//                    $crawler->addXmlContent($client->getResponse());
//                    $value = $crawler->filter('User > state')->first()->text();
//
//                    if ($value == ApiProfileController::STATUS_NOT_READY) {
//                        $reasonCodeId = $crawler->filter('User > reasonCodeId')->first()->text();
//                    }
//                }
//            } catch (\Exception $exception) {
//                $Error = $exception->getMessage() . ', ' . $exception->getFile() . ', ' . $exception->getLine();
//                $status = false;
//                $value = ApiProfileController::STATUS_NOT_READY;
//                $reasonCodeId = -1;
//            }
//
//            /** Logger */
//
//            /** Zapisywanie statusu użytkownika co $limit * 10 sec */
//
////            if ($this->progressCheckStatus($user->getId()) && $Error === false) {
////                $log = 'username: ' . $user->getUsername() . ', inRedis: ' . $inRedis . ', status: ' . $value;
////                if ($reasonCodeId) {
////                    $log .= ", reasonCodeId: " . $reasonCodeId;
////                }
////
////                $entityLog = new Log();
////                $entityLog->setContent($log);
////                $entityLog->setName('JABBER_STATUS_' . $user->getId());
////                $this->em->persist($entityLog);
////                $this->em->flush();
////            } else if ($Error !== false) {
//            if ($Error !== false) {
//
//                $entityLog = new Log();
//                $entityLog->setContent($Error);
//                $entityLog->setName('JABBER_STATUS_' . $user->getId());
//                $this->em->persist($entityLog);
//                $this->em->flush();
//
//            }
//
//            /** END Logger */
//
//            return array(
//                'status' => $status,
//                'message' => $value,
//                'reasonCodeId' => $reasonCodeId
//            );
//
//        });
//
//        return $response;
//
//    }

    protected function pushNewTask(User $user, $processId, $force = false, $routeName = 'atlas_dashboard_case')
    {

        StatusLoggerPeriodic::incrementConnections();

        /**  SPRAWDZENIE UPRAWNIEN */
        $tasks = $this->processHandler->tasks($user->getId(), $user->getId(), NULL, 1, 'pl', $processId);

        if (empty($tasks)) return false;

        $processDescription = $this->processHandler->getProcessInstanceDescription($processId);

        $data = [
            'users' => [$user->getId()],
            'task' => $processDescription
        ];

        if($force === true) {
            $data['action'] = 'force-create';
        }

        $this->pusher->push($data, $routeName);
        return $processDescription;
    }

    /**
     * @param ConnectionInterface $connection
     * @param WampRequest $request
     * @param array $params
     * @return array
     */
    public function redirectTask(ConnectionInterface $connection, WampRequest $request, $params)
    {

        $response = $this->checkConnection($connection, ['request' => $request], function ($resourceId, User $user) use ($params) {

            if (empty($params['ticketId']) || empty($params['agentNumber'])) {
                return ["error" => true, "desc" => $this->translator->trans('Error! No data required')];
            }

            $agent = $this->em->getRepository('UserBundle:User')->findBy(['cucumExtension' => $params['agentNumber']]);

            if(is_array($agent) && !empty($agent)) {
                $agent = $agent[0];
            }
            else {
                $agent = null;
            }

            if($agent instanceof User) {
                $this->pushNewTask($agent, $params['ticketId'], true, 'atlas_telephony');
            }

            return ['status' => 'ok'];

        });

        return $response;

    }

//    /**
//     * @param $userId
//     * @return bool
//     */
//    private function progressCheckStatus($userId)
//    {
//        if (isset($this->progressStatus[$userId])) {
//            $this->progressStatus[$userId]++;
//            if ($this->progressStatus[$userId] >= $this->limitProgress) {
//                $this->progressStatus[$userId] = 0;
//                return true;
//            }
//        } else {
//            $this->progressStatus[$userId] = 1;
//            return true;
//        }
//
//        return false;
//    }

    private function logException($exception) {

        $this->monolog->log('ERROR', AtlasWebsocketException::ExceptionToJson($exception));

    }

    /**
     * Name of RPC, use for pubsub router (see step3)
     *
     * @return string
     */
    public function getName()
    {
        return 'atlas.telephony.rpc';
    }
}