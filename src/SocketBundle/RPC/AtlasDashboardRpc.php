<?php

namespace SocketBundle\RPC;

use Gos\Bundle\WebSocketBundle\RPC\RpcInterface;

class AtlasDashboardRpc implements RpcInterface
{

    /**
     */
    public function __construct(

    )
    {

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'atlas.dashboard.rpc';
    }

}