<?php

namespace SocketBundle\EventListener;

use Gos\Bundle\WebSocketBundle\Client\ClientManipulator;
use Gos\Bundle\WebSocketBundle\Event\ClientEvent;
use Gos\Bundle\WebSocketBundle\Event\ServerEvent;
use Monolog\Logger;
use Predis\Client;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Security\Core\User\UserInterface;
use UserBundle\Entity\User;

class WebSocketEventListener
{

    static public $connectedUsers = 0;

    /**
     * @var Logger|NullLogger
     */
    public $logger;

    /**
     * @var \Redis
     */
    public $redis;

    /** @var ClientManipulator  */
    public $clientManipulator;

    public $users = [];

    /**
     * WebSocketEventListener constructor.
     * @param Client $redis
     * @param LoggerInterface $logger
     */
    public function __construct(Client $redis, LoggerInterface $logger, ClientManipulator $clientManipulator)
    {
        $this->redis = $redis;
        $this->logger = $logger ?? new NullLogger();
        $this->clientManipulator = $clientManipulator;
        $this->users['anon'] = [
            'connections' => 0
        ];
    }

    /**
     * Called whenever a client connects
     *
     * @param ClientEvent $event
     */
    public function onClientConnect(ClientEvent $event)
    {

        self::$connectedUsers++;

        try {

            /** @var User $user */
            $user = $this->clientManipulator->getClient($event->getConnection());

            if($user instanceof UserInterface) {
                if(!array_key_exists($user->getId(), $this->users)) {
                    $this->users[$user->getId()] = [
                        'username' => $user->getUsername(),
                        'connections' => 0
                    ];
                }

                $this->users[$user->getId()]['connections']++;
            }
            else {
                $this->users['anon']['connections']++;
            }

        }
        catch (\Exception $exception) {
            $this->logger->error('ERROR IN onClientConnect. ' . $exception->getMessage());
        }
        catch (\Throwable $exception) {
            $this->logger->error('ERROR IN onClientConnect. ' . $exception->getMessage());
        }

//        $conn = $event->getConnection();
//        echo $conn->resourceId . " connected" . PHP_EOL

    }

    /**
     * Called whenever a client disconnects
     *
     * @param ClientEvent $event
     */
    public function onClientDisconnect(ClientEvent $event)
    {

        self::$connectedUsers--;

        try {

            /** @var User $user */
            $user = $this->clientManipulator->getClient($event->getConnection());

            if($user instanceof UserInterface) {
                if(array_key_exists($user->getId(), $this->users)) {

                    $this->users[$user->getId()]['connections']--;

                    if($this->users[$user->getId()]['connections'] === 0) {
                        unset($this->users[$user->getId()]);
                    }

                }
            }
            else {
                $this->users['anon']['connections']--;
            }

        }
        catch (\Exception$exception) {
            $this->logger->error('ERROR IN onClientDisconnect. ' . $exception->getMessage());
        }
        catch (\Throwable $exception) {
            $this->logger->error('ERROR IN onClientDisconnect. ' . $exception->getMessage());
        }


//        $conn = $event->getConnection();
//        echo $conn->resourceId . " disconnected" . PHP_EOL;
    }

//    /**
//     * Called whenever a client errors
//     *
//     * @param ClientErrorEvent $event
//     */
//    public function onClientError(ClientErrorEvent $event)
//    {
//        $conn = $event->getConnection();
//        $e = $event->getException();
//
//        echo "connection error occurred: " . $e->getMessage() . PHP_EOL;
//    }
//
    /**
     * Called whenever server start
     *
     * @param ServerEvent $event
     */
    public function onServerStart(ServerEvent $event)
    {

        // Tak, żeby na szybko można było sprawdzić ilu jest użykowników i zużytej pamięci
        // kill -10  PID_ID

        pcntl_signal(SIGUSR1, function() {

            try{

                $this->redis->publish('websocket.info', json_encode([
                    'connected_users_amount' => self::$connectedUsers,
                    'connected_users' => $this->users,
                    'usage_memory' => number_format((memory_get_usage() / 1024 / 1024), 3)
                ]));

            }
            catch (\Exception $exception){
                $this->logger->error('ERROR IN SIGUSR1. ' . $exception->getMessage());
            }

        });

    }
//
//    /**
//     * Called whenever client is rejected by application
//     *
//     * @param ClientRejectedEvent $event
//     */
//    public function onClientRejected(ClientRejectedEvent $event)
//    {
//        $origin = $event->getOrigin;
//
//        echo 'connection rejected from '. $origin . PHP_EOL;
//    }

}