<?php

namespace SocketBundle\Controller;

use CaseBundle\Entity\Step;
use Doctrine\ORM\EntityManager;
use FilesystemIterator;
use GuzzleHttp\Client;
use Proxy\Adapter\Guzzle\GuzzleAdapter;
use Proxy\Proxy;
use RecursiveIteratorIterator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zend\Diactoros\ServerRequestFactory;

class DefaultController extends Controller
{

    /**
     * @Route("/test-pusher-123123")
     */
    public function indexAction()
    {

//        $data = [
//            'users' => [1426, 1],
//            'task' => [
//                'id' => '987654321',
//                'name' => 'Testowe zadanie',
//                'inne' => 'inne'
//            ]
//        ];
//
//        $pusher = $this->container->get('gos_web_socket.zmq.pusher.base');
//        $pusher->push($data, 'atlas_dashboard_case');

        return $this->render('SocketBundle:Default:index.html.twig');
    }


    /**
     * @Route("/supervisor-panel/{uri}", name="socket_admin_panel_supervisor", defaults={"uri"=""})
     * @param Request $request
     * @param $uri
     * @return Response
     */
    public function supervisorAction(Request $request, $uri)
    {

        $addressSupervisor = $this->getParameter('supervisor_address');

        if(empty($addressSupervisor)) {
            return new Response('Widok dla Supervisora jest wyłączony.');
        }

        $guzzle = new Client([
            'auth' => [
                $this->getParameter('supervisor_login'),
                $this->getParameter('supervisor_password')
            ]
        ]);

        if($request->query->has('asset')) {

            $res = $guzzle->get($addressSupervisor . '/' . $request->query->get('asset'));

            $content = $res->getBody()->getContents();
            $content = str_replace(array('url("..'), array('url("' . $request->getPathInfo() . '?asset='), $content);

            $response = new Response();
            $response->headers->set('Content-Type', $res->getHeader('Content-Type'));
            $response->headers->set('Content-Length', $res->getHeader('Content-Length'));
            $response->setContent($content);

            return $response;

        }

        $action = '';

        if($request->query->has('a_action')) {
            $action = str_replace('a_action=', '', $request->getQueryString());
        }

        $res = $guzzle->get($addressSupervisor . '/' . $action);

        $body = $res->getBody()->getContents();

        $body = str_replace(array('src="', 'link href="'), array('src="' . $request->getPathInfo() . '?asset=', 'link href="' . $request->getPathInfo() . '?asset='), $body);
        $body = str_replace('a href="', 'a href="' . $request->getPathInfo() . '?a_action=', $body);

        return new Response($body);

    }

    /**
     * @Route("/admin-panel", name="socket_admin_panel_index", options = { "expose" = true } )
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function socketAdminPanelAction(Request $request)
    {

//        $pusher = $this->get('gos_web_socket.zmq.pusher')->push('atlas.rpc.task.manager/')

        $addressSupervisor = $this->getParameter('supervisor_address');

        return $this->render('@Socket/Admin/index.html.twig', [
                'enabledSupervisor' => !empty($addressSupervisor)
            ]
        );
    }

    /**
     * @Route("/admin-panel/reservation", name="socket_admin_panel_reservation", options = { "expose" = true } )
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function reservationTaskAdminPanelAction(Request $request)
    {

        $reservation = $request->request->get('reservation');
        $list = [];

        $processRepo = $this->getDoctrine()->getRepository('CaseBundle:ProcessInstance');

        $now = (new \DateTime())->getTimestamp();

        if($reservation) {
            foreach ($reservation as $item) {

                $newRow = [
                    'username' => $item['username'],
                    'connections' => $item['connections'],
                    'tasks' => []
                ];

                foreach ($item['tasks'] as $taskId) {

                    $process = $processRepo->find(intval($taskId));

                    if($process) {
                        /** @var Step $step */
                        $step = $process->getStep();

                        $newRow['tasks'][] = [
                            'id' => $process->getId(),
                            'stepId' => $step->getId(),
                            'stepName' => $step->getName(),
                            'during' => '//todo'
//                        'during' => $this->humanTime($now, $process->getDateEnter())
                        ];
                    }

                }

                $list[] = $newRow;

            }
        }

        return new JsonResponse([
            'reservation' => $list
        ]);

    }

    /**
     * @param $now
     * @param \DateTime|null $dataEnter
     * @return null
     */
    private function humanTime($now, $dataEnter){

        if($dataEnter) {
            $seconds = ($now - $dataEnter->getTimestamp());
            return floor(($seconds / 60) % 60).":".($seconds % 60);
        }

        return null;

    }

    /**
     * @Route("/admin-panel/project-size", name="socket_admin_panel_project_size", options = { "expose" = true } )
     * @Security("is_granted('ROLE_ADMIN')")
     *
     */
    public function projectSizeAdminPanelAction()
    {

        $rootDir = $this->get('kernel')->getRootDir();

        $freeSpace = $this->human_filesize(disk_free_space($rootDir . '/../'));

        $fs = new Filesystem();

        $dirForCheck = [
            [
                'path' => $rootDir . '/../data/temp-files',
                'name' => 'Pliki tymczasowe'
            ],
            [
                'path' => $rootDir . '/../web/files/email_attachments',
                'name' => 'Tymczasowe załączniki do e-maili'
            ],
            [
                'path' => $rootDir . '/../web/files',
                'name' => 'Wszystkie pliki (web/files)'
            ],
            [
                'path' => $this->get('kernel')->getCacheDir(),
                'name' => "Cache"
            ],
            [
                'path' => $this->get('kernel')->getLogDir(),
                'name' => "Log"
            ]
        ];

        foreach ($dirForCheck as $key => $dir) {
            if($fs->exists($dir['path'])) {
                $dirForCheck[$key]['size'] = $this->sumAll($dir['path']);
            }
            else {
                $dirForCheck[$key]['size'] = '-';
            }
        }

        usort($dirForCheck, function ($a, $b) {
            return $a['size'] < $b['size'];
        });

        foreach ($dirForCheck as $key => $dir) {
            if($dirForCheck[$key]['size'] !== "-") {
                $dirForCheck[$key]['size'] = $this->human_filesize($dir['size'], 2);
            }
        }

        return new JsonResponse([
            'free' => $freeSpace,
            'fileSize' => $dirForCheck
        ]);

    }

    private function sumAll($filePath){
        $total = 0;
        $d = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($filePath, FilesystemIterator::KEY_AS_PATHNAME),
            RecursiveIteratorIterator::SELF_FIRST
        );

        foreach($d as $file){
            $total += $file->getSize();
        }

        return $total;
    }

    private function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

}
