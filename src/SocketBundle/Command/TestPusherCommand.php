<?php

namespace SocketBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestPusherCommand extends ContainerAwareCommand
{

    /**
     *  Example: php bin/console atlas:pusher:test -m '{"msg": "Siemka."}'
     */

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('atlas:pusher:test')
            ->setDescription('Testowy pusher')
            ->addOption('msg', '-m', InputOption::VALUE_OPTIONAL, 'Wysyłana wiadomość.', '{"msg": "Testowa wiadomość."}')
            ->addOption('channel', '-ch', InputOption::VALUE_OPTIONAL, 'Nazwa kanału', 'atlas/dashboard/case');

    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = $input->getOption('msg');
        $channel = $input->getOption('channel');

        $data = json_decode($data, true);
        $channel = str_replace('/', "_", $channel);

        $pusher = $this->getContainer()->get('gos_web_socket.zmq.pusher');
        $pusher->push($data, $channel);

        $output->writeln("Wysłano! ");
        $output->writeln(var_dump($data));

        return true;

    }
}
