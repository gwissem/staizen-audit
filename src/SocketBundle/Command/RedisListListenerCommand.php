<?php

namespace SocketBundle\Command;

use SocketBundle\Service\RedisListListenerService;
use SocketBundle\Utils\PrinterConsole;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RedisListListenerCommand extends ContainerAwareCommand
{

    /**
     * Process, który zapisuje do bazy wejścia i wyjścia z kroków. Dzięki niemu wiadomo kto w jakim kroku siedzi.
     *
     *  Example:
     *
     *  (enter_and_leave_tasks)
     *  php bin/console redis:listener-list:tasks --env=prod
     *
     *  (atlas_elastica_monitor)
     *  php bin/console redis:listener-list:tasks --channel=atlas_elastica_monitor --env=prod
     *
     *  (online_user)
     *  php bin/console redis:listener-list:tasks --channel=online_user --env=prod
     *
     *  (wyczyszczenie kolejek)
     *  php bin/console redis:listener-list:tasks --flush=true --env=prod
     *
        [program:redis_listener_elastica_monitor]
        command=php current/bin/console redis:listener-list:tasks --channel=atlas_elastica_monitor --env=prod
        process_name=%(program_name)s
        numprocs=1
        directory=/var/www/atlas_staging
        autostart=true
        autorestart=true
        startretries=30
        user=www-data
        redirect_stderr=false
        stdout_logfile=/var/www/atlas_staging/current/var/logs/redis_listener_elastica_monitor.out.log
        stdout_capture_maxbytes=1MB
        stderr_logfile=/var/www/atlas_staging/current/var/logs/redis_listener_elastica_monitor.error.log
        stderr_capture_maxbytes=1MB
     */

    private $running = true;

    /** @var PrinterConsole */
    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('redis:listener-list:tasks')
            ->addOption('channel', '-c', InputOption::VALUE_OPTIONAL, 'Kanał jaki nasłuchiwać. Domyślnie "enter_and_leave_tasks"', 'enter_and_leave_tasks')
            ->addOption('flush', '-f', InputOption::VALUE_OPTIONAL, 'Wyczyszczenie kolejek w redisie.', false)
            ->setDescription('Listener on leave and enter on Task');

    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $channel = $input->getOption('channel');

        $this->io = new PrinterConsole($input, $output);

        $this->io->title(self::now() . ' Start listener ' . $channel . '...');

        $isFlush = filter_var($input->getOption('flush'), FILTER_VALIDATE_BOOLEAN);

        usleep(1000000);

        try {

            $redisListSub = $this->getContainer()->get('socket.service.redis_list_subscriber');

            if(in_array($channel, [RedisListListenerService::ATLAS_ELASTICA_MONITOR, RedisListListenerService::ELASTICA_LOG], true)) {
                $redisListSub->setElasticSearchLog($this->getContainer()->get('app.elasticsearch.log'));
            }

            $redisListSub->setPrinter($this->io);

            if($isFlush) {
                $redisListSub->flushListeners();
            }
            else {
                $redisListSub->startListener($channel, $this);
            }

        }
        catch (\Exception $exception) {
            $this->io->error(RedisListListenerCommand::now() . '. ' . get_class($exception) . ', ' . $exception->getMessage() . ', ' . $exception->getCode());
            throw $exception;
        }

        $this->io->writeln('Done.');

    }

    public function isRunning() {
        return $this->running;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public static function now() {
        return (new \DateTime())->format('Y-m-d H:i:s');
    }
}
