<?php

namespace MssqlBundle\Helper;

use MediaMonks\MssqlBundle\Helper\PlatformHelper;
use MediaMonks\MssqlBundle\Helper\ConnectionHelper as BaseConnectionHelper;

class ConnectionHelper extends BaseConnectionHelper
{
    /**
     * @param $connection
     */
    public static function setConnectionOptions(\PDO $connection)
    {
        if (PlatformHelper::isWindows()) {
            return;
        }

        $connection->exec('SET ANSI_WARNINGS ON');
        $connection->exec('SET ANSI_PADDING ON');
        $connection->exec('SET ANSI_NULLS ON');
        $connection->exec('SET ARITHABORT ON');
        $connection->exec('SET NUMERIC_ROUNDABORT OFF');
        $connection->exec('SET QUOTED_IDENTIFIER ON');
        $connection->exec('SET CONCAT_NULL_YIELDS_NULL ON');
        $connection->exec('SET ANSI_NULL_DFLT_ON ON');
        $connection->exec('SET NOCOUNT ON');
    }
}
