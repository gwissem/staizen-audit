<?php

namespace MssqlBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MssqlBundle extends Bundle
{
    public function getParent()
    {
        return 'MediaMonksMssqlBundle';
    }
}
