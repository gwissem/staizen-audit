<?php

namespace ReportBundle\Service;

use Doctrine\ORM\EntityManager;
use MailboxBundle\Service\SimpleOutlookSenderService;
use Psr\Container\ContainerInterface;
use ReportBundle\Entity\Report;
use Symfony\Component\Routing\Route;
use ToolBundle\Entity\Tool;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use ToolBundle\Utils\ToolQueryManager;

class ReportSendingService
{
    const UNLIMITED = 99999999;
    /** @var  EntityManager */
    protected $em;
    protected $translator;
    protected $container;


    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }


    /**
     * @param $reportId int
     */
    public function sendReport(int $reportId)
    {
        /** @var  $reportRepository */
        $reportRepository = $this->em->getRepository('ReportBundle:Report');


        $report = $reportRepository->find($reportId);
//


        $files = $this->generateReportFiles($report);


        $body = $report->getBody() ?? '';
        $addresses = $report->getEmailAddress() ?? '';

        $link = '';
        foreach ($files as $file) {
            $link .= '<a href="' . $file['url'] . '" target="_blank">' . $file['name'] . '</a><br />';
        }


        $body = str_replace('__LINK__', $link, $body);

        /*    SEND MAIL
        @address = $report->

                */


        $subject = $report->getSubject() ?? 'Raport';


        $cc = $report->getCcEmailAddress();
        $bcc = $report->getBcEmailAddress();
        $mailboxNoReply = $this->em->getRepository('MailboxBundle:ConnectedMailbox')->findOneBy([
            'name' => 'atlas-noreply@starter24.pl'
        ]);
        $simpleSender = new SimpleOutlookSenderService($mailboxNoReply);

       $simpleSender->send($addresses, $subject, $body, $cc, $bcc);



    }


    private function generateReportFiles(Report $report): array
    {

        $toolsList = $report->getTools() ?? '';
        $type = $report->getFileType();
        $toolsList = explode(',', $toolsList);


        $toolsRepo = $this->em->getRepository('ToolBundle:Tool');
        $fileList = [];

        foreach ($toolsList as $toolId) {

            $tool = $toolsRepo->find(intval($toolId));

            if (!is_null($tool)) {
                $uuid = $this->generateSingleToolFile($tool, $type);
                $url = $this->container->getParameter('server_host') . $this->container->get('router')->generate('download_report', [
                        'uuid' => $uuid]);
                $fileList[] = ['name' => $tool->getName(), 'url' => $url];
            }


        }

        /** @var  $spout */


        return $fileList;
    }

    public function generateSingleToolFile(Tool $tool, $type = 'xls',$filters = null)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        ini_set('mssql.connect_timeout', -1);

        if (!$tool) {
            return;
        }

        /** Pobieranie odpowiednich danych narzędzia */

        $user = $this->em->getRepository('UserBundle:User')->find(1);
        $userInfo = $this->container->get('user.info');
        $userInfo->setVirtualUser($user);
        /** @var ToolQueryManager $toolQueryManager */
        $toolQueryManager = $this->container->get('tool.query_manager');


        $toolQueryManager->setParams($tool, $filters, self::UNLIMITED);
        $table = $toolQueryManager->getTable();

        if ($type == 'xls'){
            return $this->container->get('tool.data.xlsspout')->generateXlsResponseToFile($tool, $table);
        }else{
            return $this->container->get('tool.data.xlsspout')->generateCSVResponseToFile($tool,$table);
        }
    }
}