<?php

namespace ReportBundle\Entity;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="ReportBundle\Repository\ReportRepository")
 */
class Report
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", length=1000)
     */
    private $emailAddress;


    /**
     * @var string
     *
     * @ORM\Column(name="cc_address", type="string", length=1000, nullable=true)
     */
    private $ccEmailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="bcc_address", type="string", length=1000, nullable=true)
     */
    private $bcEmailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=1000)
     */
    private $subject;
    /**
     * @var string
     *
     * @ORM\Column(name="tools_list", type="string", length=1000)
     */
    private $tools;
    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="file_type", type="text", nullable=true)
     */
    private $fileType;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     *
     * @return Report
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Report
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getTools()
    {
        return $this->tools;
    }

    /**
     * @param string $tools
     */
    public function setTools($tools)
    {
        $this->tools = $tools;
    }

    /**
     * @return string
     */
    public function getCcEmailAddress()
    {
        return $this->ccEmailAddress;
    }

    /**
     * @param string $ccEmailAddress
     */
    public function setCcEmailAddress($ccEmailAddress)
    {
        $this->ccEmailAddress = $ccEmailAddress;
    }

    /**
     * @return string
     */
    public function getBcEmailAddress()
    {
        return $this->bcEmailAddress;
    }

    /**
     * @param string $bcEmailAddress
     */
    public function setBcEmailAddress($bcEmailAddress)
    {
        $this->bcEmailAddress = $bcEmailAddress;
    }


    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * @param string $fileType
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;
    }


}

