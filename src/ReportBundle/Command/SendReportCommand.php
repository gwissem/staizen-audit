<?php

namespace ReportBundle\Command;

use ReportBundle\Entity\Report;
use ReportBundle\Service\ReportSendingService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('report:send')
            ->setDescription('Wyślij raport o konkretnym ID')
            ->addArgument('reportId', InputArgument::OPTIONAL, 'ID raportu');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $reportId = $input->getArgument('reportId');
        $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' Odpalono wysyłanie raportu o id: '. $reportId);

        if ($reportId == null)
        {
            $output->writeln('Brak id raportu');
            return;
        }

        $reportService =  $this->getContainer()->get('report.sending');
        $em =  $this->getContainer()->get('doctrine')->getManager();

        $reportRepository = $em->getRepository('ReportBundle:Report');


        /** @var Report $report */
        $report = $reportRepository->find($reportId);
//
        if (is_null($report)) {
            $output->writeln(['Nie znaleziono raportu o takim id']);
            return;
        }


        $response = $reportService->sendReport($reportId);

        if(is_array($response) && $response['success'] !== true)
        {
            $output->writeln('Coś nie wyszło');
            $output->writeln($response['error_message']);
        }else{
            $output->writeln($report->getSubject() .' : wysłany');
        }

    }

}
