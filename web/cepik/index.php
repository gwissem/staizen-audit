<?php

ini_set( 'display_errors', '0' );
ini_set( 'max_execution_time', '0' );
error_reporting( E_ALL );
ini_set( 'memory_limit', '1024M' );

include_once 'simple_html_dom.php';
include_once 'Snoopy.class.php';

mssql_connect( '10.10.77.93:1433', 'SigmeoAtlas', 'gfd3W4%*$' );
mssql_select_db( 'AtlasDB' );

$query = mssql_query( 'SELECT * FROM vin.Cepik WHERE enginePower IS NULL ORDER BY (SELECT(1)) OFFSET 0 ROWS FETCH NEXT 100 ROWS ONLY' );

$cars = array();
while( $car = mssql_fetch_array( $query ) )
{
	$cars[] = $car;
}

$snoopy = new Snoopy();

$success = 0;

if( $cars != array() )
{
	foreach( $cars as &$data )
	{
		$snoopy->fetch( 'https://historiapojazdu.gov.pl/' );
		$snoopy->setcookies();

		preg_match( '!action="(https://historiapojazdu\.gov\.pl/strona\-glowna\?.[^"]+)"!', $snoopy->results, $matches );
		$post['URL'] = $matches[1];

		preg_match( '!id="javax\.faces\.ViewState" value="(.[^"]+)"!', $snoopy->results, $matches );
		$post['ViewState'] = $matches[1];

		preg_match( '!name="javax\.faces\.encodedURL" value="(.[^"]+)"!', $snoopy->results, $matches );
		$post['encodedURL'] = $matches[1];

		$snoopy->submit( $post['encodedURL'], array(
			'_historiapojazduportlet_WAR_historiapojazduportlet_:formularz' => '_historiapojazduportlet_WAR_historiapojazduportlet_:formularz',
			'javax.faces.encodedURL' => $post['encodedURL'],
			'_historiapojazduportlet_WAR_historiapojazduportlet_:rej' => $data['registrationNumber'],
			'_historiapojazduportlet_WAR_historiapojazduportlet_:vin' => $data['vin'],
			'_historiapojazduportlet_WAR_historiapojazduportlet_:data' => date( 'd.m.Y', strtotime( $data['firstRegistrationDate'] ) ),
			'_historiapojazduportlet_WAR_historiapojazduportlet_:btnSprawdz' => 'Sprawdź pojazd »',
			'javax.faces.ViewState' => $post['ViewState'] ) );

		preg_match( '!<span>Moc silnika: </span> <strong> <span class="value">([0-9]+)!', $snoopy->results, $matches );
		$data['enginePower'] = trim( $matches[1] );

		preg_match( '!<span>Pojemność silnika: </span> <strong> <span class="value">([0-9]+)!', $snoopy->results, $matches );
		$data['engineCapacity'] = trim( $matches[1] );

		preg_match( '!<span>Masa własna pojazdu: </span> <strong> <span class="value">([0-9]+)!', $snoopy->results, $matches );
		$data['carWeight'] = trim( $matches[1] );

		preg_match( '!<span>Dopuszczalna masa całkowita: </span> <strong> <span class="value">([0-9]+)!', $snoopy->results, $matches );
		$data['maxPermissibleMass'] = trim( $matches[1] );

		mssql_query( 'UPDATE vin.Cepik SET updatedAt=getdate(), enginePower = "' . $data['enginePower'] . '", engineCapacity = "' . $data['engineCapacity'] . '", carWeight = "' . $data['carWeight'] . '", maxPermissibleMass = "' . $data['maxPermissibleMass'] . '" WHERE vin = "' . $data['vin'] . '"' );

		$success++;

		sleep( 1 );
	}
}

die( 'Success: ' . $success );

