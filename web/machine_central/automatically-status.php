<?php

require_once __DIR__."/../../src/CentralMachine/index.php";

$machine = new Machines\CentralMachine($argv);
$machine->runLoop();