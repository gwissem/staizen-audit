<?php $winhash = $linuxhash = array (
  'directory' => 
  array (
    'admin' => '4562d639808311ab56f2a555ce0893d2',
    'api' => 'eecd87444360d55a15407319ac361024',
    'colors' => '053d7da88f3c40b717dbd1988c12feb7',
    'css' => '74be16979710d4c4e7c6647856088456',
    'extensions/ads' => '7655657de1e093ae35648a46f20c5812',
    'extensions/bots' => 'e5cf990d9221816e49b296b778e3d3d1',
    'extensions/desktop' => '685d16b212eb5ba5033d2e803fb692ff',
    'extensions/mobileapp' => '2b4312ce510d12dfdacf30776ce8e4cc',
    'functions' => '5aebac8462535520412ec4e61f9d3d03',
    'images' => 'd8f8ef131cbd263c732fea3d74e5eed8',
    'js' => '00ecc2babbc1866e5b8a548f578b2e6d',
    'mobile' => '37cb8d19c97f694fc1aa3d2ded67ffe9',
    'modules/announcements' => '41c2b51be1c3804ee8a123930ed8666e',
    'modules/broadcastmessage' => '847da1dd98fc0924b8988ead7744b715',
    'modules/chatrooms' => 'e858a6e40bfbc442f1db0830e2c113e2',
    'modules/facebook' => '90628eb48417710858434d40edd8cbc0',
    'modules/games' => 'd4f451c7f8539865cce7948d5e207207',
    'modules/home' => 'de77cdde3da694d6bf1e3716d311d270',
    'modules/realtimetranslate' => '1b5e10ac174e7c4018c77bbd5be1c25c',
    'modules/scrolltotop' => '9c713693a657c71f0f05d531681442fd',
    'modules/share' => '9b3f0c63c8d722d1b9f7ac674cf8d1bc',
    'modules/translate' => '6c50f00d05dd7afe9dfb8bc803201810',
    'modules/twitter' => 'f4d85cb93e46cc3623d1e75256dbcae4',
    'plugins/audiochat' => 'd69079fbb913426a69d8b95f0bf8812a',
    'plugins/avchat' => '7904de63b5affdcee90df4f829ee03e8',
    'plugins/block' => '8daf54d06f469acc4b6508efa3976809',
    'plugins/broadcast' => '45295775e41329de8b9d72a0cf8c8f78',
    'plugins/chathistory' => '9cb8721176ea423e86c5434fa34760d3',
    'plugins/clearconversation' => '5d2c93d17630e77670644b2737e08c8e',
    'plugins/filetransfer' => '7587b1d4c5e578f156dac4961ae5eee5',
    'plugins/handwrite' => '5ba7b1c0817f7ebf01a2e63b87880e4a',
    'plugins/report' => '865a9ae614742412492fc24264b496e4',
    'plugins/save' => '51f360ed3a92316f1d16754c5b86d058',
    'plugins/screenshare' => '998fb71b61a959a33f68c8d41615b1f8',
    'plugins/smilies' => '5a55b60c2167ba731a1c048cf1118d21',
    'plugins/stickers' => '8b2f0e984616cb43363d781433500745',
    'plugins/style' => 'b4d2cd5e73835653b6ccaf6e0d25cda7',
    'plugins/transliterate' => 'acbf1bc9e52b8f7df513e97bc40d1250',
    'plugins/whiteboard' => 'c0ab10c9a55aa62f5efdf09df73e08ad',
    'plugins/writeboard' => '6b906446793e8f2216873097c4ba37aa',
    'sounds' => '401113ef9c346e5e3e229a1f2e7d9a29',
    'temp' => '935a2cc4cbccccb9cfaeedc80472b44a',
    'themes' => '0b2b0d2d0b07df35643f833ba17d5ba1',
    'transports/cometservice' => 'bdd58cdf617d59fa98ac1dd7618704d4',
    'updates' => '71586713ee6aac335c3fa637d469b684',
  ),
  'files' => 
  array (
    0 => '.htaccess',
    1 => 'CHANGELOG.txt',
    2 => 'EULA.txt',
    3 => 'VERSION.txt',
    4 => 'ccauth.php',
    5 => 'cometchat_cache.php',
    6 => 'cometchat_check.php',
    7 => 'cometchat_checkmobileapp.php',
    8 => 'cometchat_delete.php',
    9 => 'cometchat_embedded.php',
    10 => 'cometchat_getid.php',
    11 => 'cometchat_guests.php',
    12 => 'cometchat_init.php',
    13 => 'cometchat_isfriend.php',
    14 => 'cometchat_login.php',
    15 => 'cometchat_logout.php',
    16 => 'cometchat_popout.php',
    17 => 'cometchat_receive.php',
    18 => 'cometchat_send.php',
    19 => 'cometchat_session.php',
    20 => 'cometchat_shared.php',
    21 => 'cometchatcss.php',
    22 => 'cometchatjs.php',
    23 => 'config.php',
    24 => 'cron.php',
    25 => 'css.php',
    26 => 'index.html',
    27 => 'install.php',
    28 => 'integration.bak',
    29 => 'js.php',
    30 => 'jsmin.php',
    31 => 'lang.php',
    32 => 'modules.php',
    33 => 'php4functions.php',
    34 => 'plugins.php',
  ),
  'fileshash' => '2474c5884604fedb50eb66ce0b3e46ee',
); ?>