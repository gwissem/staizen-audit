# Instrukcja Docker Atlas 

## Instalacja aplikacji docker i docker-compose 

Docker:
- https://docs.docker.com/install/linux/docker-ce/ubuntu/

```bash

sudo apt-get update

sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
    
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io

```

Docker-compose:
- https://docs.docker.com/compose/install/

```bash

sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

```
Dodać:
```bash
sudo usermod -aG docker your-user
```

Potem trzeba zrobić relogin

## Pobranie repozytorium z projektem

```
mkdir -p ~/clone_repo && cd ~/clone_repo && git clone https://sigmeo.codebasehq.com/starter24/atlas.git .
```

## Zbudowanie podstawowego obrazu UBUNTU & APACHE2 & PHP

1. Przejść do katalogu docker/main_image
2. Uruchomić:

```
docker build -t apache_and_php .
```

## Zbudowanie obrazu pod Docker'a DEV 

1. Przejść do katalogu docker/dev/web
2. Uruchomić:

```
docker build -t atlas_dev .
```

## Zbudowanie obrazu pod Docker'a PROD

1. Przejść do katalogu docker/prod/web
2. Uruchomić:

```
docker build -t atlas_prod .
```

## Przed uruchomieniem (w katalogu docker)

```bash
cp .env.dist .env
```

## Uruchomienie kontenera

```
docker-compose up -d
```

## wejście do kontenera

- DEV:

```
docker exec -it atlas_dev_web bash
```

- PROD:

```
docker exec -it atlas_prod_web bash
```


## Dodatkowe komendy:

- dodanie cron'ów:

```
docker exec atlas_prod_web /usr/sbin/set-cron-job.sh
```

## DODATKI:

### Ustawienie hasła na Redis

```bash
docker exec atlas_prod_web /usr/sbin/init-env.sh
```

### Dodanie użytkownika SSH dla Docker'a  (np. żeby móc robić Deploy)

Upewnij się, że przed uruchomieniem Kontenera w .env były uzupełnione:

```bash
SSH_LOGIN=
SSH_PASS=
```
Następnie uruchomić:
```bash
docker exec atlas_prod_web /usr/sbin/ssh-account.sh
```

Ręczny sposób, dopisać do /etc/environment
```bash
export SSH_LOGIN=starter
export SSH_PASS=

```
i odpalić: "source"


### FOR DEVELOPMENT

#### Uruchomienie Composera

(jeżeli na dokerze wystepuje inny user, to wpisz jego name zamiast $(whoami))

```
docker exec atlas_dev_web /usr/sbin/composer-install.sh $(whoami)
```

#### uruchomienie Gulp'a

```
docker exec atlas_dev_web /usr/sbin/gulp.sh $(whoami)
```
